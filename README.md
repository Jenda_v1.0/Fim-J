# Fim-J #


## Základní informace ##

Aplikace Fim-J vznikla jako bakalářská práce na FIM UHK. Jedná se desktopovou aplikaci napsanou v jazyce Java. Je zaměřena na podporu výuky objektově orientovaného programování v jazyce Java. Nadále pokračuji ve vývoji aplikace pro vlastní účely. Například implementuji nebo testuji nové funkce, vylepšuji postupy, doplňuji "věci", které jsem v rámci bakalářské práce nestihl apod.


### Větvě ###

* [master](https://gitlab.com/Jenda_v1.0/Fim-J/tree/master) -> Stabilní verze aplikace.

* [development](https://gitlab.com/Jenda_v1.0/Fim-J/tree/development) -> Vývojová verze aplikace *(může obsahovat chyby)*.

* [wiki](https://gitlab.com/Jenda_v1.0/Fim-J/tree/wiki) -> Soubory, na které je odkazováno z Wiki *(obrázky pro zobrazení a soubory ke stažení)*.

* [bakalarka](https://gitlab.com/Jenda_v1.0/Fim-J/tree/bakalarka) -> Odevzdaná aplikace v rámci bakalářské práce.


**Pro více informací viz [Wiki](https://gitlab.com/Jenda_v1.0/Fim-J/wikis/home).**

*Z časových důvodů nemusí být informace uvedené ve wiki vždy aktuální (týká se pouze implementovaných funkcí).*



## Basic information ##

The Fim-J application was created as a bachelor's thesis at FIM UHK. This is a desktop application written in Java. It is aimed at supporting the teaching of object-oriented programming in Java. I continue to develop the application for my own purposes. For example, I implement or test new features, improve procedures, add "things" that I did not do in my bachelor thesis, etc.


### Branches ###

* [master](https://gitlab.com/Jenda_v1.0/Fim-J/tree/master) -> Stable verion of the application.

* [development](https://gitlab.com/Jenda_v1.0/Fim-J/tree/development) -> Development verion of application *(may contain errors)*.

* [wiki](https://gitlab.com/Jenda_v1.0/Fim-J/tree/wiki) -> Files that are referenced from the Wiki *(images to display and files for download)*.

* [bakalarka](https://gitlab.com/Jenda_v1.0/Fim-J/tree/bakalarka) -> Handed application within bachelor thesis.


**For more information, see [Wiki](https://gitlab.com/Jenda_v1.0/Fim-J/wikis/home).**

*For reasons of time, information in the wiki may not always be up to date (it only applies to implemented functions).*