package cz.uhk.fim.fimj.parameters_for_method;

/**
 * Tento výčet slouží pouze pro "určení" hodnoty, která sen achází v konkrétní instanci třídy ParameterValue. Výčtový
 * typ (tento z tohoto výčtu) je zde potřeba kvůli "zjištění", která hodnota se má vzít, resp. hodnota z jaké proměnné
 * se má vzít a taky v jaké syntaxi se mají ty hodnoty vypisovat do cmb, resp. v jaké syntaxi se poskládá text v metodě
 * toString a tento text bude zobrazen v cmb pro výběr parametru.
 * <p>
 * Hlavně se jedná o určení toho, zda se jedná o hodnotu, kterou zadá uživatel, tj. nějaká hodnota v textu, se kterou se
 * bude dále pracovat, například přetypování na číslo, pole, ... ale to se již zde neřeší. Další možností je, že se bude
 * jednat o proměnnou z nějaké třídy (popř. instance třídy) a nebo nějaká proměnná, kterou uživatel vytvořil v editoru
 * příkazů, například "klasická" proměnná typu int, double, float, ... nebo List, či pole apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public enum KindOfVariable {

    /**
     * Proměnná, která reprezentuje proměnnou, kterou zadal uživatel jako parametr.
     */
    TEXT_VARIABLE,

    /**
     * Proměnná, která byla získána z nějaké třídy, popřípadě instance třídy.
     */
    FIELD_VARIABLE,

    /**
     * Proměnná, která reprezentuje proměnnou, kterou uživatel vytvořil pomocí editoru příkazů.
     */
    COMMAND_EDITOR_VARIABLE
}