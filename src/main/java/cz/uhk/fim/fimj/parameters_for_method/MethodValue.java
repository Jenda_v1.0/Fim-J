package cz.uhk.fim.fimj.parameters_for_method;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.List;
import java.util.Properties;

import cz.uhk.fim.fimj.reflection_support.ParameterToText;
import cz.uhk.fim.fimj.forms.GetParametersMethodForm;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;
import cz.uhk.fim.fimj.instances.Called;

/**
 * Tato třída slouží pouze jako hodnota v komponentě JCmoboBox, která reprezentuje nějakou metodu, kterou lze zavolat.
 * <p>
 * O metodě je známo, že vrací potřebný datový typ a tato třída je pouze takový "prostředník / schránka" pro udržení
 * metod a případně pro její zavolání, pak se vrátí příslušná hodnota, kterou vrátí zavolaná metoda.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class MethodValue extends CmbValueAncestor {

	/**
	 * Metoda, kterou reprezentuje konkrétní instance této třídy a která vrací
	 * požadované datový typ nějakého parametru metody nebo konstruktoru.
	 */
	private final Method method;
	
	/**
	 * Instance, ve které se metoda method nachází, nebo null, pokud je metoda
	 * method statická.
	 */
	private final Object objInstance;	
	
	
	/**
	 * Referencena instanci objInstance v diagramu instancí.
	 */
	private final String reference;
	
	
	
	/**
	 * Třída instance objInstance nebo jen třída, kde byla získaná metoda method.
	 * Tato proměnná je potřeba pro předání do dialogu pro zavolání metody, aby se
	 * vědělo, ke které třídě se mají načíst jaké metody, například chráněné apod.
	 */
	private final Class<?> clazz;
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param method
	 *            - metoda, kterou reprezentuje konkrétní instance této třídy.
	 * 
	 * @param objInstance
	 *            - instance nějaké třídy z diagramu tříd, která obsahuje metodu
	 *            method.
	 * 
	 * @param reference
	 *            - reference na instanci třídy, v diagramu instancí, resp.
	 *            referencena instanci objInstance.
	 */
	MethodValue(final Method method, final Object objInstance, final String reference) {
		this.method = method;
		this.objInstance = objInstance;
		this.reference = reference;
		clazz = objInstance.getClass();
	}
	
	
	

	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * Tento konstruktor slouží pouze pro naplnění metod, jedná se o statickou
	 * metodu, na kterou není třeba reference ani ji volat nad instancí nějaké
	 * třídy.
	 * 
	 * @param method
	 *            - metoda, kterou reprezentuje konkrétní instance této třídy.
	 * 
	 * @param clazz
	 *            - třída, ve které se nachází metoda method, jedná se o to, že ta
	 *            metoda je statická, tak není potřeba načítat instance, stačí ta
	 *            třída.
	 */
	MethodValue(final Method method, final Class<?> clazz) {
		this.method = method;
		this.clazz = clazz;
		objInstance = null;
		reference = null;
	}
	
	
	
	

	
	/**
	 * Metoda, která slouží pro zavolání metody v konkrétní instanci této třídy,
	 * resp. jedná se o nějakou specifickou položku v komponentě JcomboBox v dialogu
	 * pro zavolání metody nebo konstruktoru.
	 * 
	 * Tato metroda slouží pro zavolání příslušné metody. Otestuje se, zda se jedná
	 * o zavolání metody s nebo bez parametru, pak se zavolá (popř. vyplní
	 * parametry) a nakonec se vrátí příslušná hodnota, kterou vrátí zavolaná
	 * metoda.
	 * 
	 * 
	 * 
	 * Note:
	 * Bylo by možné zde ještě předávat editor výstupů, kdyby nastala například
	 * nějaká chyba apod. Ale už jej zde předávat nebudu, protože by uživatel mohl
	 * například zavolat 10 takových metod a došlo by k velké spuště výpisů, proto
	 * se bude vždy vypisovat pouze informace z té první zavolané metody, i z ní se
	 * vypíše nějaký chyba, ke které by mohlo dojít, vetšinou se bude stejně jednat
	 * pouze o chybně zadané parametry nebo něco v tom smyslu.
	 * 
	 * @param instanceDiagram
	 *            - reference na diagram instancí.
	 * 
	 * @param makeVariablesAvailable
	 *            - logická proměnná, která značí, zda se mají zpřístupnit proměnné
	 *            pro předání její hodnoty do parametru metody nebo ne. Tuto položku
	 *            nemkhu předávat, protože se může lišit v diagramu tříd a v
	 *            diagramu instancí.
	 * 
	 * @param showCreateNewInstanceOption
	 *            - logická proměnná, která značí, zda se mají u příslušných
	 *            parametrů zobrazit položky pro možnost vytvoření nové instance
	 *            nějaké třídy z diagramu tříd, aby se přidala do parametru nějaké
	 *            metody nebo konstruktoru. Tuto položku nemkhu předávat, protože se
	 *            může lišit v diagramu tříd a v diagramu instancí.
	 * 
	 * @param makeMethodsAvailable
	 *            - logická proměnná, která značí, zda se mají zpřístupnit proměnné
	 *            pro předání jejich návratového hodnoty do parametru metody. Tuto
	 *            položku nemkhu předávat, protože se může lišit v diagramu tříd a v
	 *            diagramu instancí.
	 * 
	 * @param languageProperties
	 *            - objekt s texty pro tuto aplikaci ve zvoleném jazyce.
	 */
	public final Object callMethod(final GraphInstance instanceDiagram, final boolean makeVariablesAvailable,
			final boolean showCreateNewInstanceOption, final boolean makeMethodsAvailable,
			final Properties languageProperties) {

		if (method.getParameterCount() == 0) {
			// Metoda neobsahuje žádný parametr, tak ji mohu zavolat:
			final Called returnValue = FlexibleCountOfParameters.operationWithInstances
					.callMethodWithoutParameters(method, objInstance, true);

			if (returnValue != null && returnValue.isWasCalled())
				return returnValue.getObjReturnValue();
		}

		else {
			// Zde metoda obsahuje alespoň jeden parametr, tak ho musím načíst od uživatele:
			// Původní:
			final GetParametersMethodForm gpmf = new GetParametersMethodForm(method, languageProperties, null,
					makeVariablesAvailable, showCreateNewInstanceOption, instanceDiagram, clazz, reference,
					makeMethodsAvailable);

			final List<Object> parametersOfMethod = gpmf.getParameters(method.getName());

			if (parametersOfMethod != null && !parametersOfMethod.isEmpty()) {
				// Zjistím si návratovou hodnotu a případně ji vypíšu do editoru
				final Called returnValue = FlexibleCountOfParameters.operationWithInstances
						.callMethodWithParameters(method, objInstance, true, parametersOfMethod.toArray());

				// Otestuji, zda se něco vrátilo a pokud ano, vypíši to do editoru výstupů

				if (returnValue != null && returnValue.isWasCalled())
					return returnValue.getObjReturnValue();
			}
		}

		return null;
	}
	
	
	
	
	

	
	
	@Override
	public String toString() {
		/*
		 * Výsledná syntaxe:
		 * 
		 * reference -> ClassName: methodName(parameters) : returnType
		 * 
		 * nebo (bez reference):
		 * 
		 * ClassName: methodName(parameters) : returnType
		 */
		
		
		/*
		 * Proměnná, do které se vloži text metody.
		 */
		String text = "";

		/*
		 * Pokud se jedná o metodu z nějaké instance třídy, tj. proěmná reference není
		 * null, pak ji zde uvednu
		 */
		if (reference != null)
			text += reference + " -> ";

		// Název třídy:
		text += clazz.getSimpleName() + ": ";

		// název metody:
		text += method.getName();

		// Parametry metody:
		text += "(" + getParametersInText(method) + ") : ";


		// Návratový datový typ metody:
		text += ParameterToText.getMethodReturnTypeInText(method, false);

		return text;
	}
	
	
	
	
	

	/**
	 * Metoda, která slouží pro převedení parametrů metody method do textové podoby
	 * do takové syntaxe, že jednotlivé parametry budou oddělené desetinnou čárkou.
	 * 
	 * @param method
	 *            - metoda, jejiž parametry se mají převést do textové podoby
	 *            oddělené desetinnou čátkou.
	 * 
	 * @return parametr metody method v textové podobě, že budou jednotlivé
	 *         parametry oddelené desetinnou čárkou nebo prádzný text v případě, že
	 *         metoda žádné parametry nemá.
	 */
	private static String getParametersInText(final Method method) {
		/*
		 * Získám si parametry metody.
		 */
		final Parameter[] parameters = method.getParameters();

		/*
		 * V případě, že metoda žádné parametry nemá, nemá smysl pokračovat a vrátí se
		 * prázdný text.
		 */
		if (parameters.length == 0)
			return "";

		return ParameterToText.getParametersInText(parameters, false);
	}





	/**
	 * Getr na referenci na instanci, kde se příslušná metoda nachází, případně null, pokud se jedná například
	 * ostatikou
	 * metoudu.
	 *
	 * @return null pokud se jedná o statickou metodu, jinak název reference na nějakou instanci v daigramu instanci,
	 * kde se nachází příslušná metoda, kterou tato konkrétní instancei této třídy reprezentuje.
	 */

	public String getReference() {
		return reference;
	}


	/**
	 * Getr na metodu, kterou instance této třídy reprezentuje.
	 *
	 * @return příslušná metoda.
	 */
	public Method getMethod() {
		return method;
	}
}
