package cz.uhk.fim.fimj.parameters_for_method;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.RenderingHints;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.security.AuthProvider;
import java.security.Provider;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Stack;
import java.util.TreeMap;
import java.util.Vector;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.jar.Attributes;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.management.openmbean.TabularDataSupport;
import javax.print.attribute.standard.PrinterStateReasons;
import javax.script.SimpleBindings;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.UIDefaults;
import javax.swing.plaf.metal.MetalComboBoxButton;

import cz.uhk.fim.fimj.file.*;
import cz.uhk.fim.fimj.reflection_support.MapHelper;
import cz.uhk.fim.fimj.reflection_support.ParameterToText;
import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.commands_editor.MakeOperation;
import cz.uhk.fim.fimj.commands_editor.Variable;
import cz.uhk.fim.fimj.commands_editor.VariableArray;
import cz.uhk.fim.fimj.commands_editor.VariableList;
import cz.uhk.fim.fimj.commands_editor.ParseToJavaTypes.BooleanTypeParser;
import cz.uhk.fim.fimj.commands_editor.ParseToJavaTypes.ByteTypeParser;
import cz.uhk.fim.fimj.commands_editor.ParseToJavaTypes.CharacterTypeParser;
import cz.uhk.fim.fimj.commands_editor.ParseToJavaTypes.DoubleTypeParser;
import cz.uhk.fim.fimj.commands_editor.ParseToJavaTypes.FloatTypeParser;
import cz.uhk.fim.fimj.commands_editor.ParseToJavaTypes.IntegerTypeParser;
import cz.uhk.fim.fimj.commands_editor.ParseToJavaTypes.LongTypeParser;
import cz.uhk.fim.fimj.commands_editor.ParseToJavaTypes.ShortTypeParser;
import cz.uhk.fim.fimj.forms.DataTypeOfJTextField;
import cz.uhk.fim.fimj.forms.GetParametersMethodForm;
import cz.uhk.fim.fimj.forms.ListCellRenderer;
import cz.uhk.fim.fimj.forms.NewInstanceForm;
import cz.uhk.fim.fimj.forms.SourceDialogForm;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;
import cz.uhk.fim.fimj.instances.Instances;
import cz.uhk.fim.fimj.instances.OperationsWithInstancesInterface;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;

/**
 * Třída slouží pro deklarani některých hodnot a metod, které se budou opakovat pro zadání libovolného početu parametrů
 * metody nebo kontruktoru ve třídách GetParametersMethodForm a NewInstanceForm
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class FlexibleCountOfParameters extends SourceDialogForm {

	private static final long serialVersionUID = 1L;

	
	
	/**
	 * Tato proměnná slouží pouze jako výchozí šířka pro komponentu JCmoboBox
	 * obsahující položky, jejichž hodnoty je možné předat do parametru metody nebo
	 * konstruktoru.
	 */
	private static final int CMB_DEFAULT_WIDTH = 250;

	/**
	 * Tato proměnná slouží pouze jako výchozí výška pro komponentu JCmoboBox
	 * obsahující položky, jejichž hodnoty je možné předat do parametru metody nebo
	 * konstruktoru.
	 */
	private static final int CMB_DEFAULT_HEIGHT = 25;
	
	
	
	
	
	
	
	
	
	/**
	 * Pole, které obsahuje veškeré možné implementace Mapy, které je možé vložit do
	 * proměnné typu Map v parametru metody nebo konstruktoru.
	 */
	private static final Class<?>[] MAP_DATA_TYPES = { Map.class, AbstractMap.class, Attributes.class, AuthProvider.class,
			ConcurrentHashMap.class, ConcurrentSkipListMap.class, EnumMap.class, HashMap.class, Hashtable.class,
			IdentityHashMap.class, LinkedHashMap.class, PrinterStateReasons.class, Properties.class, Provider.class,
			RenderingHints.class, SimpleBindings.class, TabularDataSupport.class, TreeMap.class, UIDefaults.class,
			WeakHashMap.class };
	
	
	
	
	
	
	
	
	
	
	/**
	 * List, do kterého si budu vkládat všechny metody, které jsem již využil, abych
	 * neměl nějaké duplicity, které nechci - v případě statických metod, jinak
	 * duplicity konkrétně v tomto listu nebudou, ale budou obecně, pokud bude
	 * například více instancí jedné třídy a ta bude obsahovat potřebnou metodu, tak
	 * se ta metoda vezme z každé instance.
	 */
	private static final List<Method> USED_METHODS = new ArrayList<>();
	
	
	
	
	
	
	
	
	
	
	/**
	 * Regulární výraz, který slouží pro otestování, zda se příslušný textnachází
	 * (je obalen) dojtými uvozovkami.
	 * 
	 * Například: "xxx".
	 * 
	 * Toto je potřeba v případě, že se předává text z textového pole, pak se ten
	 * text bere i z uvozovkami, což je nežádoucí, text se nepředává s uvozovkami.
	 */
	private static final String REG_EX_TEXT_IN_QUOTATION_MARKS = "^\\s*\\\".*\\\"\\s*$";
	
	
	
	
	/**
	 * Zde může být v podstatě libovolná znak, protože nevím zda se do pole zadavá
	 * číslo text, logicka hodnota...
	 */
	protected static final String REG_EX_FOR_PARAMEER = "^\\s*.+\\s*$";
	
	
	/**
	 * Regulární výraz pro číslo typu byte:
	 */
	private static final String REG_EX_FOR_NUMBER = "^\\s*-?\\d+\\s*$";
	
	
	
	/**
	 * Výraz pro číslo typu double
	 */
	private static final String REG_EX_FOR_DOUBLE_NUMBER = "^\\s*-?\\d+e?\\d*\\.?\\d*(e-?\\d+)?[dD]?\\s*$";
	

	
	/**
	 * Regulární výraz pro číslo typu float
	 */
	private static final String REG_EX_FOR_FLOAT_NUMBER = "^\\s*-?\\d+e?\\d*\\.?\\d*(e-?\\d+)?[fF]?\\s*$";
	
	
	/**
	 * Regulární výraz pro char.
	 * 
	 * Může obsahovat escapovazí znaky a v podstatě libovolé jiné.
	 */
	private static final String REG_EX_FOR_CHAR = "^\\s*('.'|'\\\\t'|'\\\\b'|'\\\\n'|'\\\\r'|'\\\\f'|'\\\\''|'\\\\\"'|'\\\\\\\\')\\s*$";
	
	
	/**
	 * Regulární výraz pro String:
	 */
	private static final String REG_EX_FOR_STRING = "^\\s*\".*\"\\s*$";
	
	
	

	/**
	 * Regulární výraz pro testování zadaných hodnotu pro hodnoty typu Byte v Listu
	 * a v poli: Syntaxe: [ -1, -5, 2, 35, ... ] - hodnoty -128 - +127 Nemusí zde
	 * být i zadna hodnota: [ ], v takovem pripade se pole vytvori s výchozi
	 * hodnotou, ale pro list to neplatí, ten se akorát vytvoří a bude prázdny
	 * 
	 * Může být v poli / listu zadáno: žádná hodnota - popř bíle znaky = prázdný
	 * list / pole nebo jedna hodnota nebo více jak jedna hodnota těchto více
	 * hodnota je odděleno desetinnými čárkami: Syntaxe: {1, 2, 3, ...} nebo: [ ] ->
	 * prazdne pole / list nebo [ 1 ]
	 */
	private static final String REG_EX_FOR_LIST_AND_ARRAY_BYTE = "^\\s*\\[\\s*(\\s*|-?\\s*(\\d\\s*){1,3}|-?\\s*(\\d\\s*){1,3}\\s*(,\\s*-?\\s*(\\d\\s*){1,3}\\s*)+)\\s*\\]\\s*$";
	
	
	
	
	/**
	 * Regulární výraz pro testování zadaných hodnotu pro hodnoty typu Short v Listu
	 * a v poli: Syntaxe: [ -5, 56, 10, ... ] - hodnoty v intervalu: -32768 až
	 * +32767 Opět zde platí, že se pro pole vytvoří výchozí hodnota a pro list bude
	 * prázdny:
	 */
	private static final String REG_EX_FOR_LIST_AND_ARRAY_SHORT = "^\\s*\\[\\s*(\\s*|-?\\s*(\\d\\s*){1,5}|-?\\s*(\\d\\s*){1,5}\\s*(,\\s*-?\\s*(\\d\\s*){1,5}\\s*)+)\\s*\\]\\s*$";
	
	
	
	/**
	 * Regulární výraz pro testování zadaných hodnotu pro hodnoty typu Integer v
	 * Listu a v poli: Syntaxe: [ -8, 40000, 5, ... ] - hodnoty v intervalu:
	 * -2147483648 až +2147483647 Opět zde platí, že se pro pole vytvoří výchozí
	 * hodnotu a list bude prázdny:
	 */
	private static final String REG_EX_FOR_LIST_AND_ARRAY_INTEGER = "^\\s*\\[\\s*(\\s*|-?\\s*(\\d\\s*){1,10}|-?\\s*(\\d\\s*){1,10}\\s*(,\\s*-?\\s*(\\d\\s*){1,10}\\s*)+)\\s*\\]\\s*$";
	
	
	
	
	/**
	 * Regulární výraz pro testování zadaných hodnotu pro hodnoty typu Long v Listu
	 * a v poli: Syntaxe: [ -5. 7, 5050505 ] - hodnoty v intervalue:
	 * -9223372036854775808 až +9223372036854775807 Opět zde platí, že se pro pole
	 * vytvoří výchozí hodnotu a list bude prázdny:
	 */
	private static final String REG_EX_FOR_LIST_AND_ARRAY_LONG = "^\\s*\\[\\s*(\\s*|-?\\s*(\\d\\s*){1,19}|-?\\s*(\\d\\s*){1,19}\\s*(,\\s*-?\\s*(\\d\\s*){1,19}\\s*)+)\\s*\\]\\s*$";
	
	
	
	/**
	 * Regulární výraz pro testování zadaných hodnotu pro hodnoty typu Float v Listu
	 * a v poli: Syntaxe: [ -11. 52 , 51. 999f, -5.9F ], apod. Opět zde platí, že se
	 * pro pole vytvoří výchozí hodnotu a list bude prázdny:
	 * 
	 * Nemusí tam být žádný znak - popř. bíle znaky coby prázdné pole / List, nebo
	 * právě jedno číslo, nebo více jak jedno číslo
	 */
	private static final String REG_EX_FOR_LIST_AND_ARRAY_FLOAT = "^\\s*\\[\\s*(\\s*|-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(f|F)?|-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(f|F)?\\s*(\\s*,\\s*-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(f|F)?)+)\\s*\\]\\s*$";
	
	
	

	/**
	 * Regulární výraz pro testování zadaných hodnotu pro hodnoty typu double v
	 * Listu a v poli: Syntaxe: [ -5, 25, 5.15D, -52.148d, ... ] apod. Opět zde
	 * platí, že se pro pole vytvoří výchozí hodnotu a list bude prázdny:
	 * 
	 * Nemusí tam být žádný znak - popř. bíle znaky coby prázdné pole / List, nebo
	 * právě jedno číslo, nebo více jak jedno číslo
	 */
	private static final String REG_EX_FOR_LIST_AND_ARRAY_DOUBLE = "^\\s*\\[\\s*(\\s*|-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(d|D)?|-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(d|D)?\\s*(\\s*,\\s*-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(d|D)?)+)\\s*\\]\\s*$";
	
	
	
	
	/**
	 * Regulární výraz pro testování zadaných hodnot pro hodnoty typu Character v
	 * Listu a v poli: Syntaxe: [ ';', 'X', '\n', ... ] - hodnoty může být 1 nebo 2
	 * znaky, například: \n, apod. Opět zde platí, že se pro pole vytvoří výchozí
	 * hodnotu a list bude prázdny:
	 * 
	 * Dále v tom znaku mohou být veškeré escapovací výrazy, jako jsou \t, \b, \n,
	 * \r, \f, \', \", \\ zbytek je vždy po jednom znaku.
	 * 
	 * Nemusí tam být žádný znak - prázdný list / pole, nebo právě jeden znak , nebo
	 * více jak jeden znak:
	 */
	private static final String REG_EX_FOR_LIST_AND_ARRAY_CHARACTER = "^\\s*\\[\\s*('.'|'\\\\t'|'\\\\b'|'\\\\n'|'\\\\r'|'\\\\f'|'\\\\''|'\\\\\"'|'\\\\\\\\')\\s*(,\\s*('.'|'\\\\t'|'\\\\b'|'\\\\n'|'\\\\r'|'\\\\f'|'\\\\''|'\\\\\"'|'\\\\\\\\')\\s*)*\\s*\\]\\s*$";
	
	
	
	/**
	 * Regulární výraz pro testování zadaných hodnotu pro hodnoty typu Boolean v
	 * Listu a v poli: Syntaxe: [ true, false, true, ... ] Opět zde platí, že se pro
	 * pole vytvoří výchozí hodnotu a list bude prázdny:
	 * 
	 * nemusí tam byt žádná hodnota jedna, nebo více:
	 */
	private static final String REG_EX_FOR_LIST_AND_ARRAY_BOOLEAN = "^\\s*\\[\\s*(\\s*|(true|false)\\s*|(true|false)\\s*(,\\s*(true|false)\\s*)+)\\s*\\]\\s*$";
	
	
	
	
	/**
	 * Regulární výrazy pro testování zadaných hodnot pro hodnoty typu String v
	 * Listu a v poli: Syntaxe: ["some text", "", "", ...] - v uvozovkách mohou být
	 * libovolné další znaky kromě dvojitých uvozovek (dalších) Opět zde platí, že
	 * se pro pole vytvoří výchozí hodnotu a list bude prázdny:
	 * 
	 * Nemusí tam být žádný text - prázdné pole / list, nebo právě jeden text, nebo
	 * více jak jeden text pro naplnění pole / listu:
	 */
	private static final String REG_EX_FOR_LIST_AND_ARRAY_STRING = "^\\s*\\[\\s*(\\s*|\"[^\"]*\"|\"[^\"]*\"\\s*(,\\s*\"[^\"]*\"\\s*)+)\\s*\\]\\s*$";
	
	
	
	
	
	
	
	// Regulární výrazy pro zadání hodnot pro dvojrozměrné pole:
	
	/**
	 * Regulární výraz pro zadání hodnoty do dvojrozměrného pole typu Byte: Syntaxe:
	 * [ ] - prázdné pole, [{value1, value2}] jedno pole v poli a , [ {value1,
	 * value2}, {value3, value4, ...}, {}, ... ] - více poli v poli
	 */
	private static final String REG_EX_FOR_TWO_DIM_ARRAY_BYTE = "^\\s*\\[\\s*(\\s*|\\{\\s*(\\s*|-?\\s*(\\d\\s*){1,3}|-?\\s*(\\d\\s*){1,3}\\s*(,\\s*-?\\s*(\\d\\s*){1,3}\\s*)+)\\s*\\}|\\{\\s*(\\s*|-?\\s*(\\d\\s*){1,3}|-?\\s*(\\d\\s*){1,3}\\s*(,\\s*-?\\s*(\\d\\s*){1,3}\\s*)+)\\s*\\}\\s*(,\\s*\\{\\s*(\\s*|-?\\s*(\\d\\s*){1,3}|-?\\s*(\\d\\s*){1,3}\\s*(,\\s*-?\\s*(\\d\\s*){1,3}\\s*)+)\\s*\\}\\s*)+)\\s*\\]\\s*$";
	
	
	/**
	 * Regulární výraz pro zadání hodnoty dvojrozměrného pole typu Short: Syntaxe: [
	 * ] - prázdné pole, [{value1, value2}] jedno pole v poli a , [ {value1,
	 * value2}, {value3, value4, ...}, {}, ... ] - více poli v poli
	 */
	private static final String REG_EX_FOR_TWO_DIM_ARRAY_SHOWRT = "^\\s*\\[\\s*(\\s*|\\{\\s*(\\s*|-?\\s*(\\d\\s*){1,5}|-?\\s*(\\d\\s*){1,5}\\s*(,\\s*-?\\s*(\\d\\s*){1,5}\\s*)+)\\s*\\}|\\{\\s*(\\s*|-?\\s*(\\d\\s*){1,5}|-?\\s*(\\d\\s*){1,5}\\s*(,\\s*-?\\s*(\\d\\s*){1,5}\\s*)+)\\s*\\}\\s*(,\\s*\\{\\s*(\\s*|-?\\s*(\\d\\s*){1,5}|-?\\s*(\\d\\s*){1,5}\\s*(,\\s*-?\\s*(\\d\\s*){1,5}\\s*)+)\\s*\\}\\s*)+)\\s*\\]\\s*$";
	
	
	
	/**
	 * Regulární výraz pro zadání hodnoty dvojrozměrného pole typu Integer: Syntaxe:
	 * [ ] - prázdné pole, [{value1, value2}] jedno pole v poli a , [ {value1,
	 * value2}, {value3, value4, ...}, {}, ... ] - více poli v poli
	 */
	private static final String REG_EX_FOR_TWO_DIM_ARRAY_INTEGER = "^\\s*\\[\\s*(\\s*|\\{\\s*(\\s*|-?\\s*(\\d\\s*){1,10}|-?\\s*(\\d\\s*){1,10}\\s*(,\\s*-?\\s*(\\d\\s*){1,10}\\s*)+)\\s*\\}|\\{\\s*(\\s*|-?\\s*(\\d\\s*){1,10}|-?\\s*(\\d\\s*){1,10}\\s*(,\\s*-?\\s*(\\d\\s*){1,10}\\s*)+)\\s*\\}\\s*(,\\s*\\{\\s*(\\s*|-?\\s*(\\d\\s*){1,10}|-?\\s*(\\d\\s*){1,10}\\s*(,\\s*-?\\s*(\\d\\s*){1,10}\\s*)+)\\s*\\}\\s*)+)\\s*\\]\\s*$";
	
	
	/**
	 * Regulární výraz pro zadání hodnoty dvojrozměrného pole typu Long: Syntaxe: [
	 * ] - prázdné pole, [{value1, value2}] jedno pole v poli a , [ {value1,
	 * value2}, {value3, value4, ...}, {}, ... ] - více poli v poli
	 */
	private static final String REG_EX_FOR_TWO_DIM_ARRAY_LONG = "^\\s*\\[\\s*(\\s*|\\{\\s*(\\s*|-?\\s*(\\d\\s*){1,19}|-?\\s*(\\d\\s*){1,19}\\s*(,\\s*-?\\s*(\\d\\s*){1,19}\\s*)+)\\s*\\}|\\{\\s*(\\s*|-?\\s*(\\d\\s*){1,19}|-?\\s*(\\d\\s*){1,19}\\s*(,\\s*-?\\s*(\\d\\s*){1,19}\\s*)+)\\s*\\}\\s*(,\\s*\\{\\s*(\\s*|-?\\s*(\\d\\s*){1,19}|-?\\s*(\\d\\s*){1,19}\\s*(,\\s*-?\\s*(\\d\\s*){1,19}\\s*)+)\\s*\\}\\s*)+)\\s*\\]\\s*$";
	
	
	
	/**
	 * Regulární výraz pro zadání hodnoty dvojrozměrného pole typu Float: Syntaxe: [
	 * ] - prázdné pole, [{value1, value2}] jedno pole v poli a , [ {value1,
	 * value2}, {value3, value4, ...}, {}, ... ] - více poli v poli
	 */
	private static final String REG_EX_FOR_TWO_DIM_ARRAY_FLOAT = "^\\s*\\[\\s*(\\s*|\\{\\s*(\\s*|-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(f|F)?|-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(f|F)?\\s*(,\\s*-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(f|F)?)+)\\s*\\}|\\{\\s*(\\s*|-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(f|F)?|-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(f|F)?\\s*(,\\s*-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(f|F)?)+)\\s*\\}\\s*(,\\s*\\{\\s*(\\s*|-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(f|F)?|-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(f|F)?\\s*(,\\s*-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(f|F)?)+)\\s*\\}\\s*)+)\\s*\\]\\s*$";
	
	
	/**
	 * Regulární výraz pro zadání hodnoty dvojrozměrného pole typu Double: Syntaxe:
	 * [ ] - prázdné pole, [{value1, value2}] jedno pole v poli a , [ {value1,
	 * value2}, {value3, value4, ...}, {}, ... ] - více poli v poli
	 */
	private static final String REG_EX_FOR_TWO_DIM_ARRAY_DOUBLE = "^\\s*\\[\\s*(\\s*|\\{\\s*(\\s*|-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(d|D)?|-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(d|D)?\\s*(,\\s*-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(d|D)?)+)\\s*\\}|\\{\\s*(\\s*|-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(d|D)?|-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(d|D)?\\s*(,\\s*-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(d|D)?)+)\\s*\\}\\s*(,\\s*\\{\\s*(\\s*|-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(d|D)?|-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(d|D)?\\s*(,\\s*-?\\s*((\\d+\\s*)+|(\\d+\\s*)+\\.\\s*(\\d+\\s*)+)\\s*(d|D)?)+)\\s*\\}\\s*)+)\\s*\\]\\s*$";
	
	
	/**
	 * Regulární výraz pro zadání hodnoty dvojrozměrného pole typu Character:
	 * Syntaxe: [ ] - prázdné pole, [{value1, value2}] jedno pole v poli a , [
	 * {value1, value2}, {value3, value4, ...}, {}, ... ] - více poli v poli
	 */
	private static final String REG_EX_FOR_TWO_DIM_ARRAY_CHARACTER = "^\\s*\\[\\s*(\\s*|\\{\\s*(\\s*|'[^']{1,2}'\\s*|('[^']{1,2}')\\s*(,\\s*'[^']{1,2}'\\s*)+)\\s*\\}|\\{\\s*(\\s*|'[^']{1,2}'\\s*|('[^']{1,2}')\\s*(,\\s*'[^']{1,2}'\\s*)+)\\s*\\}\\s*(,\\s*\\{\\s*(\\s*|'[^']{1,2}'\\s*|('[^']{1,2}')\\s*(,\\s*'[^']{1,2}'\\s*)+)\\s*\\}\\s*)+)\\s*\\]\\s*$";
	
	
	/**
	 * Regulární výraz pro zadání hodnoty dvojrozměrného pole typu Boolean: Syntaxe:
	 * [ ] - prázdné pole, [{value1, value2}] jedno pole v poli a , [ {value1,
	 * value2}, {value3, value4, ...}, {}, ... ] - více poli v poli
	 */
	private static final String REG_EX_FOR_TWO_DIM_ARRAY_BOOLEAN = "^\\s*\\[\\s*(\\s*|\\{\\s*(\\s*|(true|false)\\s*|(true|false)\\s*(,\\s*(true|false)\\s*)+)\\s*\\}|\\{\\s*(\\s*|(true|false)\\s*|(true|false)\\s*(,\\s*(true|false)\\s*)+)\\s*\\}\\s*(,\\s*\\{\\s*(\\s*|(true|false)\\s*|(true|false)\\s*(,\\s*(true|false)\\s*)+)\\s*\\}\\s*)+)\\s*\\]\\s*$";
	
	
	/**
	 * Regulární výraz pro zadání hodnoty dvojrozměrného pole typu String: Syntaxe:
	 * [ ] - prázdné pole, [{value1, value2}] jedno pole v poli a , [ {value1,
	 * value2}, {value3, value4, ...}, {}, ... ] - více poli v poli
	 */
	private static final String REG_EX_FOR_TWO_DIM_ARRAY_STRING = "^\\s*\\[\\s*(\\s*|\\{\\s*(\\s*|\"[^\"]*\"|\"[^\"]*\"\\s*(,\\s*\"[^\"]*\"\\s*)+)\\s*\\}|\\{\\s*(\\s*|\"[^\"]*\"|\"[^\"]*\"\\s*(,\\s*\"[^\"]*\"\\s*)+)\\s*\\}\\s*(,\\s*\\{\\s*(\\s*|\"[^\"]*\"|\"[^\"]*\"\\s*(,\\s*\"[^\"]*\"\\s*)+)\\s*\\}\\s*)+)\\s*\\]\\s*$";
	
	
	
	
	
	
	
	
	/**
	 * Regulártní výraz pro rozdělení zadaného textu pro dvojrozměrné pole u textu
	 * nebo znaku, protoze se spičatá uzavírací závorka může vysktyovat jak v textu
	 * tak u ve znaku: Jedná se o rozdělení dle spičaté uzavírací závorky:
	 */
	private static final String REG_EX_FOR_SPLIT_OF_TEXT_AND_CHAR_ARRAY = "\\s*\\}\\s*,\\s*";

	/**
	 * Výraz pro nahrazení "menšítka / otevíracího tagu" za jiný znak. To je potřeba v případě, že se vkládá text do
	 * html tagů. V takovém případě je třeba nahradit otevírací a zavírací tagy za speciální znaky. Aby se mohly tagy
	 * zobrazit na stránce, nebrali se jako část kódu.
	 */
	private static final Pattern COMPILE = Pattern.compile("<",
			Pattern.LITERAL);
	/**
	 * Výraz pro nahrazení "většítka / uzavíracího tagu" za jiný znak. To je potřeba v případě, že se vkládá text do
	 * html tagů. V takovém případě je třeba nahradit otevírací a zavírací tagy za speciální znaky. Aby se mohly tagy
	 * zobrazit na stránce, nebrali se jako část kódu.
	 */
	private static final Pattern PATTERN = Pattern.compile(">", Pattern.LITERAL);


	/**
	 * Refence na outputEditor pro předání do metody na získání parametrů po klinutí
	 * na tlačítko OK, tak může nastat chyba při vytváření instance // nějaké třídy,
	 * kterou uživatel zvolí, pak je třeba to oznámit uživteli pomocí tohoto
	 * editoru:
	 */
	protected OutputEditorInterface outputEditor;
	
	
	
	
	
	
	
	
	/**
	 * Pole coby model pro zadání logické hodnoty typu boolean:
	 */
	private static final List<BooleanValue> MODEL_FOR_CMB_BOOLEAN = Collections
			.unmodifiableList(Arrays.asList(new BooleanValue(true), new BooleanValue(false)));
	
	
	
	
	protected JLabel lblInfoText, lblTextInfo;

	
	
	
	/**
	 * Proměnná, do které vložím text s chybovým hlášením o tom, zda se v parametru
	 * metody či konstruktoru vyskytuje nějaký parametr jehož datový typ není
	 * podrporován, v takovém případě nepůjde daná metody či konstruktor zavolat:
	 */
	protected String errorTextForBadTypeOfParameter;
	
	
	

	// Proměnné, do kterých vložím hodnoty do chybového hlášení, pokud nebudou zadány hodnoty ve správném formátu:
	protected String badFormatSomeParameterTitle, badFormatSomeParameterText;
	
	
	
	
	
	/**
	 * JList pro chybové hlášky - pokud je v nějakém pola zadána chybná hodnota
	 */
	private JList<Object> errorList;
	
	
	private static final String txtByteInterval = ": <-128 ; +127>.", txtShortInterval = ": <-32 768 ; +32 768>.",
			txtIntegerInterval = ": <-2 147 483 648 ; +2 147 483 647>.",
			txtLongInterval = ": <-9 223 372 036 854 775 808 ; +9 223 372 036 854 775 807>.",
			txtFloatInterval = ": <-3.40282e+38 ; +3.40282e+38>.",
			txtDoubleInterval = ": <-1.79769e+308 ; +1.79769e+308>.";
	
	
	
	// Proměnné typu String, do kterých se vloži výchozí hodnoty pro jejich vložení do textových polí coby výchozí hodnoty
	// parametru a Jlabelů coby popisků hodnot parametrů a případně i chyby:
	private static String txtByteFormatError, txtShortFormatError, txtIntFormatError, txtLongFormatError,
			txtFloatFormatError, txtDoubleFormatError, txtCharFormatError, txtStringFormatError,
			txtDefaultValueForStringField,
			
			// Texty pro List:
			txtListStringFormatError, txtListByteFormatError, txtListShortFormatError, txtListIntegerFormatError,
			txtListLongFormatError, txtListCharacterFormatError, txtListBooleanFormatError, txtListFloatFormatError,
			txtListDoubleFormatError,

			// Texty pro kolekce:
			txtCollectionByteFormatError, txtCollectionShortFormatError, txtCollectionIntegerFormatError,
			txtCollectionLongFormatError, txtCollectionFloatFormatError, txtCollectionDoubleFormatError,
			txtCollectionCharacterFormatError, txtCollectionBooleanFormatError, txtCollectionStringFormatError,
		
			// Texty pro jednorozměrné pole:
			txtArrayFormatErrorForAll1, txtArrayFormatErrorAll2, txtArrayCharacterFormatError2,
			txtArrayBooleanFormatError2, txtArrayStringFormatError2,
			
			// Výchozí text pro pole a list:
			txtDefaultTextForStringListAndArray1, txtDefaultTextForStringListAndArray2,
			
			// Hodnoty pro dvojrozměrné pole - chybové hlášky ohledně správného formátu syntaxe pro dvojrozměrné pole:
			txt2ArrayFormatErrorAll1,
			
			// Proměnné - resp. texty do chybového oznámení, pokud se jedná o nepodporovaný datový typ parametru:
			txtDataTypeOfParametr, txtIsNotSupportedForConstructor, txtIsNotSupportedForMethod,
			txtConstructorCannotCall, txtMethodCannotCall,
			
			// Chybové hlášky do editoru výstupů v případě, že se nepodaří vytvořit instanci třídy:
			txtFailedToCreateInstanceOfClass, txtPossibleErrors, txtPossibleErrorsWithPrivateType,

			// Texty pro chybové oznámení, že nebyla nalezena žádná instance, kterou by bylo
			// možné předat do parametru metody nebo knostruktoru:
			txtInstanceNotFoundMethodCantCall, txtInstanceNotFoundConstructorCantCall;
			
	
	
	// Texty pro chyby v uživatelem zvoleném jazyce:
	protected String txtFieldContainError, txtErrorForErrorList, txtPnlMistakesBorderTitle, txtChcbWrapLyrics;
	
	
	
	
	
	
	/*
	 * Musím vytvořit List, komponent - Jtextfieldů a Jcomboboxů, které do něj budu
	 * vkládat a ze kterého si následné vezmu zadané hodnoty od uživatelele
	 */
	protected List<JComponent> listOfParametersValues;
	
	
	
	
	
	/**
	 * Kolekce, do které vložím labely, s chybovým hláškami, tyto labely jsou
	 * zobrazny v případě, že hodnota která patří do nějakého pole, nění zadána ve
	 * sprvném formátu tento label se zobrazí a po kliknutí na tlačítko OK nebo
	 * stisknutí enteru se tatko kolekce prohledá a zjistí se, zda jesou všechny
	 * labely neviditelné, jinak se vypíše hláška, že nějaká hodnota parametru nění
	 * zadána ve sprvném formátu.
	 */
	protected List<String> errorTextList;
	
	
	
	
	/**
	 * Kolekce Jlabelů pro informace, jaká hodnota se tam má zada, abych jich
	 * naplnim přesný pčet, jaký se jich vytvoří, daám je při vytvoření do
	 * následující kolekcee a při nastavení textů ji projdu a nastavím získný text
	 */
	protected List<JLabel> labelsList;
	
	
	
	
	
	
	
	/**
	 * Tento list slouží pouze pro dočasné uložení nalezených proměnných v případě,
	 * že se jedná o parametr metody nebo konstruktoru, který není v této aplikaci
	 * tak úplně podporován, ale ve smyslu, že jej nelze přímo "zadat" od uživatele.
	 * 
	 * V případě, že se jedná o nějaký typ parametru metody nebo konstruktoru, který
	 * není testován, tak se hledají proměnné stejného datového typu ve všech
	 * třídách a instancích v diagramu tříd a instancí, a list nalezených proměnných
	 * se dočasně uloží do tohoto listu, a ten se pak testuje, zda není prázdný,
	 * popřípadě null (toto by nastat nemělo, vždy by se měl vrátát minimálně
	 * prázdný list). A pokud není prázdný list, tak je podmínka splněna, a jedná se
	 * o nějaký "nehlídaný" datový typ parametru metody a byla nalezena hodnota,
	 * kterou je možné do toho parametru předat, tak nechám uživatele si vybrat
	 * jakou hodnotu zvolit.
	 * 
	 * Jinak tento list není nějak extra potřeba, je zde jen pro to, aby se nevolala
	 * jedna metoda pro nalezení výše zmíněných proměnných dvakrát.
	 */
	private List<ParameterValue> tempListForOthersFields = new ArrayList<>();
	
	
	
	/**
	 * Tento list slouží pro "dočasné" uložení metod, které je možné zavolat, které
	 * vrací příslušnou hodnotu.
	 * 
	 * Tento list je není nezbytný, ale pro přehlednost je lepší, aby se snáze
	 * testovali nalezené metody a zda se vůbec nějaké našly.
	 */
	private List<MethodValue> tempListForOthersMethods = new ArrayList<>();
	
	
	
	
	
	
	
	
	/**
	 * Proměnná, která slouží pro uložení textu s textem příslušné metody nebo
	 * konstruktoru pro zavolání, budou zde obsaženy i jeho / její parametry apod.
	 * Aby uživatel věděl, co vlastné bude volat za konstruktor.
	 */
	protected String textWithParameters;
	
	
	
	
	
	
	
	/**
	 * Proměnná, která slouží pro "dočasné" uložení reference na dialog pro zavolání
	 * metody, to je potřeba pouze kvůli tomu, aby bylo možné zavolat metodu
	 * 'testData' když se v příslušné komponentě stiskne klávesa Enter.
	 * 
	 * Pak se otestují zadaná data a pokusí se zavolat metoda.
	 */
	private GetParametersMethodForm methodParametersForm;
	
	
	/**
	 * Proměnná, která slouží pro "dočasné" uložení reference na dialog pro zavolání
	 * konstruktoru.
	 * 
	 * Toto potřeba pouze pro to, že když se stiskne klávesa Enter, tak se zavolá
	 * metoda 'testData', která otestuje zadaná data v dialogu a případě se zavolá
	 * konstruktor.
	 */
	private NewInstanceForm newInstanceForm;
	
	
	
	
	
	
	/**
	 * Rozhraní obsahující metody, které slouží pro zavolání metod, konstruktorů
	 * apod.
	 * 
	 * Zde je to potřeba pouze pro zavolání metod, pokud uživatel vybere, že chce
	 * zavolat metodu a návratovou hodnotou příslušné metody chce naplnit parametr
	 * zavolané metody nebo konstruktoru.
	 */
	protected static OperationsWithInstancesInterface operationWithInstances;
	
	
	
	
	
	
	/**
	 * Reference na objekt, který obsahuje text pro tuto aplikaci v uživatelem
	 * zvoleném jazyce, zde to je pouze pro předán do zavolání metody.
	 */
	private Properties languageProperties;
	
	
	
	
	
	
	
	
	
	/**
	 * Reference na diagram instancí.
	 */
	protected static GraphInstance instanceDiagram;
	
	
	
	
	
	
	
	
	/**
	 * logická proměnná, která slouží pro určení toho, zda se mají pro zadání
	 * parametrů od uživatele (tedy ty, které je třeba zadat od uživatele a nejsou
	 * na výběr), tak, zda se pro ně mají zobrazit textová pole (hodntoa false) nebo
	 * komponenta JCombobox (hodnota true). Pokud se mají zobrazit textová pole, tak
	 * uživatel může pouze zadat samotnou hodnotu (typu String), která se pozdějí
	 * přetypuje na požadovaný datový typa předá do parametru metody nebo
	 * konstruktoru, ale pokud se zvolí hodnota false - tj. že sepro zadání
	 * parametru zobrazí komponenty JComboBox, pak bude moci uživatel vždy do první
	 * položky zadat tu samotnou hodnotu stejně jako do textového pole, ale navíc
	 * bude mít možnost vybrat si hodnotu z vytvořených proměnných v / pomocí
	 * editoru příkazů nebo z veřejných a chráněných statických proměnných z tříd v
	 * diagramu tříd nebo veřejných a chráněncýh proměnných v diagramu instancí.
	 */
	private final boolean makeVariablesAvailable;
	
	/**
	 * logická proměnná, která značí, zda se má v případě že se bude jednat o
	 * parametru metody typu nějaké třídy, která se nachází v diagramu tříd, tak,
	 * zda se má nabídnout do cmb možnost pro vytvoření nové instance příslušné
	 * třídy nebo ne. Hodnota true značí, že ano, tj. bude v příslušném cmb
	 * nabídnuta možnost pro vytvoření nové instance a v případě, že hodnota této
	 * proměnné bude false, tak se nebudou nabízet možnosti pro vytvoření nové
	 * instance příslušné třídy.
	 */
	private final boolean showMakeNewInstanceOption;
	
	/**
	 * Logická proměnná, která značí zda se mají zpřístupnit metody pro zavolání u
	 * proměnných nebo ne. Tj., aby byla možnost do parametru metody nebo
	 * konstruktoru předat návratovou hodnotu metody.
	 */
	private final boolean makeMethodsAvailable;
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param makeVariablesAvailable
	 *            - zda se mají zpřístupnit proměnné pro předání do parametru metody
	 *            nebo konstruktoru.
	 * 
	 * @param showMakeNewInstanceOption
	 *            - zda se má zobrazit možnost pro vytvoření nové instance u
	 *            parametrů datového typu nějaké třídy z diagramu tříd.
	 * 
	 * @param makeMethodsAvailable
	 *            - zda se má zpřístupnit možnost pro zavolání metody pro předání
	 *            její návratové hodnoty do příslušného parametru metody nebo
	 *            konstruktoru.
	 */
	public FlexibleCountOfParameters(final boolean makeVariablesAvailable, final boolean showMakeNewInstanceOption,
			final boolean makeMethodsAvailable) {

		this.makeVariablesAvailable = makeVariablesAvailable;
		this.showMakeNewInstanceOption = showMakeNewInstanceOption;
		this.makeMethodsAvailable = makeMethodsAvailable;
	}
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	
	
	/**
	 * Metoda, která dle počtu a typu parametrů v parametru metody vloží do dialogu
	 * příslušný počet komponent pro získání hodnot od uživatele do parametrů
	 * kontruktoru
	 * 
	 * @param parametersOfConstructor
	 *            = jednorozměrné pole, které obsahuje veškeré informace o
	 *            parametrech které potřebuji pro tento dialog - infomace, jako např
	 *            počet, a typ parametrů
	 * 
	 * @param isInstance
	 *            - logická hodnota o tom, zde se jedná o zadání parametrů pro
	 *            zavolání metody nebo konstruktoru, tato proměnná je v podstatě
	 *            potřeba akorát kvůli upřesnění některých chybových výpisů, které
	 *            se zobrazí v příslušném dialogu, ale pro samotnou funkci ohledně
	 *            získání parametrů nemá tato proměnná žádný význam.
	 * 
	 * @param classOrInstanceList
	 *            - list zkompilovanych trid
	 * 
	 * @param availableFieldsList
	 *            - list, který obsahuje veškeré získané proměnné (veřejné a
	 *            chráněné) z vytvořených instancí tříd, dále veřejné a chráněné
	 *            statické prměnné z tříd z diagramu tříd a veškeré proměnné, které
	 *            uživatel vytvořil pomocí editoru příkazů.
	 * 
	 * @param methodParametersForm
	 *            - reference na dialog pro zavolání metody, je potřeba pouze pro
	 *            to, aby se zavolal metoda po stisknutí klávesy enter - tj. aby se
	 *            pokusial zavolat příslušná metoda.
	 * 
	 * @param newInstanceForm
	 *            - reference na diaog pro zavolání konstruktoru (vytvoření nové
	 *            instance), je zde potřeba aby se zavolala příslušná metoda po
	 *            stisknutí klávesy Enter.
	 * 
	 * @param clazz
	 *            - třída, ve které se má zavolat nějaká metoda nebo konstruktor.
	 *            Tato proměnná je zde potřeba, protože kvůli zjištění metod, které
	 *            se mají využít, myslím, například chráněné metody, které se mohou
	 *            načíst - potřebuji vědět, ze které třídy.
	 * 
	 * @param reference
	 *            - text, resp. reference na instanci v diagramu instancí, nad
	 *            kterou se má zavolat nějaká metoda, tato proměná bude naplněna
	 *            pouze v případě, že se jedná o zavolání nějaké metody nad instancí
	 *            v diagramu instancí, jinak pokud se jedná o zavolání statické
	 *            metody nad třídou v diagramu tříd pak bude tato proměná null.
	 * 
	 * @param methodNameOnly
	 *            - proměnná typu String, která bude obsahovat null hodnotu, pokud
	 *            není zavolání setr, ale pokud je zavolán setr na nějakou
	 *            proměnnou, resp. metoda, která splňuje nějaké podmínky a mimo jiné
	 *            hlavně, že ta metoda začíná slovem set, pak v tomto parametru bude
	 *            celý zbytek názvu té metody (setru), například když bude setr
	 *            (metoda) s názvem setSomeVar, tak se v tomto parametru bude
	 *            nacházet text 'SomeVar'
	 * 
	 * @return panel s komponentami pro zadání parametrů
	 */
	protected final JPanel addFieldForValuesToDialog(final Parameter[] parametersOfConstructor,
			final boolean isInstance, final List<ClassOrInstanceForParameter> classOrInstanceList,
			final List<ParameterValue> availableFieldsList, final GetParametersMethodForm methodParametersForm,
			final NewInstanceForm newInstanceForm, final Class<?> clazz, final String reference,
			final String methodNameOnly) {

		this.methodParametersForm = methodParametersForm;
		this.newInstanceForm = newInstanceForm;
		
		// Index řádku, kam se má umístit komponenta / ty - pole a popisek:
		int x = -1;
		// Index sloupců, kam se mají umístit komponenty:
		// y1 = první sloupec, tj. pro popisek - Label s typem hodnoty, y2 = druhý sloupec - pro pole, resp. zadání či výběr hodnoty
		final int y1 = 0, y2 = 1;
		
		
		// Komponenta, do které se budou vkládat  komponenty pro zadání hodnot, slouží pro rozmístění komponent v Layoutu
		final GridBagConstraints gbc = createGbc();
		
		// panel, do kterého se budou vkládat komponenty a ten se vrátí s již naplněnýma komponentama pro zadání hodnot
		final JPanel pnlValues = new JPanel(new GridBagLayout());


        /*
         * List, do kterého vložím instance třídy a nebo načtenou zkompilovanou třídu - .class soubor, pokud odpovídá
         * parametru metody či konstruktoru
         */
        List<ClassOrInstanceForParameter> correspondingTypesList;
		
		
		
		
		
		for (final Parameter p : parametersOfConstructor) {
			// Otestuji si , jaký typ hodnoty se má zadávat a dle toho vytvořím přislušné komponenty:
			// Note: abych nemusel testovat několik podmínek pro stejne datové typy: byte a Byte - jako jeden typ, tak si převedu 
			// typ parametru do textu a otestuji:
			
			if (p.getType().equals(byte.class) || p.getType().equals(Byte.class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);
				
				// Otestuji, zda se má vytvořit komponenty JTextfield pro zadání hodnoty od
				// uživatele nebo JComboBox i s možnostmí pro převzetí hodnoty z nějaké
				// proměnné.
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadán hodnoty:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, "0", pnlValues);

					addKeyListenerToTxtField(txtField, REG_EX_FOR_NUMBER, txtByteFormatError, DataTypeOfJTextField.BYTE,
							x);
				}

				// Zde se jedná o JComboBox s možností převzetí hodnoty z nějaké proměnné:
				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();
					
					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

					// Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, "0"));

					/*
					 * V případě, že se mají přidat proměnné se pokusí najít proměnné a na začátek
					 * přidat položka pro zadání hodnoty od uživatele.
					 */
					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu:
						fieldValues.addAll(getModelForCmb(availableFieldsList, byte.class, Byte.class));
					}

					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());
						
						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). se jedná o setr na nějakou proměnnou v
						 * té instanci, pokud jsou tyto podmínky splněny, pak se vyfiltrují metody tak,
						 * aby nebyly v setru (který se aktuálně volá) nabídky pro getr, tj. metoda se
						 * stejným název jako setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						
					
					
					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, p.getType().equals(Byte.class));					
					
					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_NUMBER, txtByteFormatError,
							DataTypeOfJTextField.BYTE, x, p.getType().equals(Byte.class));
				}
			}
			
			
			else if (p.getType().equals(short.class) || p.getType().equals(Short.class)) {	
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);
				
				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadán hodnoty:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, "0", pnlValues);

					addKeyListenerToTxtField(txtField, REG_EX_FOR_NUMBER, txtShortFormatError,
							DataTypeOfJTextField.SHORT, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();
					
					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

					// Přidání textového pole pro "ruční" zadání hodnoty:
					fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, "0"));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu:
						fieldValues.addAll(getModelForCmb(availableFieldsList, short.class, Short.class));
					}
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). se jedná o setr na nějakou proměnnou v
						 * té instanci, pokud jsou tyto podmínky splněny, pak se vyfiltrují metody tak,
						 * aby nebyly v setru (který se aktuálně volá) nabídky pro getr, tj. metoda se
						 * stejným název jako setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						


					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, p.getType().equals(Short.class));

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_NUMBER, txtShortFormatError,
							DataTypeOfJTextField.SHORT, x, p.getType().equals(Short.class));
				}
			}
			
			
			else if (p.getType().equals(int.class) || p.getType().equals(Integer.class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadán hodnoty:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, "0", pnlValues);

					addKeyListenerToTxtField(txtField, REG_EX_FOR_NUMBER, txtIntFormatError, DataTypeOfJTextField.INT,
							x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();
					
					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, "0"));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu:
						fieldValues.addAll(getModelForCmb(availableFieldsList, int.class, Integer.class));						
					}
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). se jedná o setr na nějakou proměnnou v
						 * té instanci, pokud jsou tyto podmínky splněny, pak se vyfiltrují metody tak,
						 * aby nebyly v setru (který se aktuálně volá) nabídky pro getr, tj. metoda se
						 * stejným název jako setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						


					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, p.getType().equals(Integer.class));

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_NUMBER, txtIntFormatError,
							DataTypeOfJTextField.INT, x, p.getType().equals(Integer.class));
				}
			}
			
			else if (p.getType().equals(long.class) || p.getType().equals(Long.class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);
				
				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadán hodnoty:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, "0", pnlValues);

					addKeyListenerToTxtField(txtField, REG_EX_FOR_NUMBER, txtLongFormatError, DataTypeOfJTextField.LONG,
							x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();

					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, "0"));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu:
						fieldValues.addAll(getModelForCmb(availableFieldsList, long.class, Long.class));						
					}
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). se jedná o setr na nějakou proměnnou v
						 * té instanci, pokud jsou tyto podmínky splněny, pak se vyfiltrují metody tak,
						 * aby nebyly v setru (který se aktuálně volá) nabídky pro getr, tj. metoda se
						 * stejným název jako setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						


					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, p.getType().equals(Long.class));

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_NUMBER, txtLongFormatError,
							DataTypeOfJTextField.LONG, x, p.getType().equals(Long.class));
				}
			}
			
			
			else if (p.getType().equals(float.class) || p.getType().equals(Float.class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadán hodnoty:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, "0.0", pnlValues);

					addKeyListenerToTxtField(txtField, REG_EX_FOR_FLOAT_NUMBER, txtFloatFormatError,
							DataTypeOfJTextField.FLOAT, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();


					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, "0.0"));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu:
						fieldValues.addAll(getModelForCmb(availableFieldsList, float.class, Float.class));						
					}
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). se jedná o setr na nějakou proměnnou v
						 * té instanci, pokud jsou tyto podmínky splněny, pak se vyfiltrují metody tak,
						 * aby nebyly v setru (který se aktuálně volá) nabídky pro getr, tj. metoda se
						 * stejným název jako setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, p.getType().equals(Float.class));

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_FLOAT_NUMBER, txtFloatFormatError,
							DataTypeOfJTextField.FLOAT, x, p.getType().equals(Float.class));
				}
			}
			
			
			else if (p.getType().equals(double.class) || p.getType().equals(Double.class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);
				
				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadán hodnoty:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, "0.0", pnlValues);

					addKeyListenerToTxtField(txtField, REG_EX_FOR_DOUBLE_NUMBER, txtDoubleFormatError,
							DataTypeOfJTextField.DOUBLE, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();

					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, "0.0"));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu:
						fieldValues.addAll(getModelForCmb(availableFieldsList, double.class, Double.class));						
					}

					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). se jedná o setr na nějakou proměnnou v
						 * té instanci, pokud jsou tyto podmínky splněny, pak se vyfiltrují metody tak,
						 * aby nebyly v setru (který se aktuálně volá) nabídky pro getr, tj. metoda se
						 * stejným název jako setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
							methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, p.getType().equals(Double.class));

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_DOUBLE_NUMBER, txtDoubleFormatError,
							DataTypeOfJTextField.DOUBLE, x, p.getType().equals(Double.class));
				}
			}
			
			
			
			else if (p.getType().equals(char.class) || p.getType().equals(Character.class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);
				
				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadán hodnoty:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, "'X'", pnlValues);

					addKeyListenerToTxtField(txtField, REG_EX_FOR_CHAR, txtCharFormatError, DataTypeOfJTextField.CHAR,
							x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();

					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, "'X'"));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu:
						fieldValues.addAll(getModelForCmb(availableFieldsList, char.class, Character.class));						
					}
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). se jedná o setr na nějakou proměnnou v
						 * té instanci, pokud jsou tyto podmínky splněny, pak se vyfiltrují metody tak,
						 * aby nebyly v setru (který se aktuálně volá) nabídky pro getr, tj. metoda se
						 * stejným název jako setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, p.getType().equals(Character.class));

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_CHAR, txtCharFormatError,
							DataTypeOfJTextField.CHAR, x, p.getType().equals(Character.class));
				}
			}
			
			
			else if (p.getType().equals(boolean.class) || p.getType().equals(Boolean.class)) {				
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

				final List<CmbValueAncestor> fieldValues = new ArrayList<>(MODEL_FOR_CMB_BOOLEAN);

				/*
				 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
				 * je možné zavolat i metody.
				 */
				final List<MethodValue> methodValues = new ArrayList<>();
				
				if (makeVariablesAvailable)
					// Přidám do modelu nalezené položky stejného datového typu:
					fieldValues.addAll(getModelForCmb(availableFieldsList, boolean.class, Boolean.class));
				
				/*
				 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
				 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
				 * předala do parametru metody.
				 */
				if (makeMethodsAvailable) {
					// Získám si metody, které lze pro příslušný typ zavolat:
					tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

					/*
					 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
					 * příslušným názvem metody - mimo jiné). se jedná o setr na nějakou proměnnou v
					 * té instanci, pokud jsou tyto podmínky splněny, pak se vyfiltrují metody tak,
					 * aby nebyly v setru (který se aktuálně volá) nabídky pro getr, tj. metoda se
					 * stejným název jako setr - akorát s get nebo is na začátku:
					 */
					if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                        methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

					// Zde prostě přidám všechny výše získané metody:
					else
						methodValues.addAll(tempListForOthersMethods);
				}
					


				final JComboBox<CmbValueAncestor> cmbBoolean = new JComboBox<>();

				final DefaultComboBoxModel<CmbValueAncestor> tempModel = (DefaultComboBoxModel<CmbValueAncestor>) cmbBoolean
						.getModel();

				fieldValues.forEach(tempModel::addElement);

				methodValues.forEach(tempModel::addElement);

				if (p.getType().equals(Boolean.class))
					tempModel.addElement(new NullValue());

				cmbBoolean.setModel(tempModel);

				
				
				cmbBoolean.addKeyListener(new KeyAdapter() {

					@Override
					public void keyPressed(KeyEvent e) {
						if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
							variable = false;
							dispose();
						}

						else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
							/*
							 * Pokud se jedná o zavolání metody, pak otestuji zadaná data a případně zkusím
							 * zavolat metodu:
							 */
							if (methodParametersForm != null)
								methodParametersForm.testData();

							/*
							 * Pokud se jedná o zavolání konstruktoru, tak otestuji zadaná data a případně
							 * zkusím zavolat konstruktor:
							 */
							else if (newInstanceForm != null)
								newInstanceForm.testData();
						}
					}
				});
				
				
				
				
				listOfParametersValues.add(cmbBoolean);

				setGbc(gbc, x, y2, cmbBoolean, pnlValues);
			}
			
			
			
			else if (p.getType().equals(String.class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);
				
				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadán hodnoty:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2,
							("\"" + txtDefaultValueForStringField + "\""), pnlValues);

					addKeyListenerToTxtField(txtField, REG_EX_FOR_STRING, txtStringFormatError,
							DataTypeOfJTextField.STRING, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();

					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, "\"" + txtDefaultValueForStringField + "\""));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu:
						fieldValues.addAll(getModelForCmb(availableFieldsList, String.class));
					}
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). se jedná o setr na nějakou proměnnou v
						 * té instanci, pokud jsou tyto podmínky splněny, pak se vyfiltrují metody tak,
						 * aby nebyly v setru (který se aktuálně volá) nabídky pro getr, tj. metoda se
						 * stejným název jako setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_STRING, txtStringFormatError,
							DataTypeOfJTextField.STRING, x, true);
				}
			}
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			/*
			 * Test, zda se jedná o parametr typu List:
			 */
			else if (p.getType().equals(List.class)) {
				// defaultValue - Proměnná, do které se vloží výchozí hodnota pro pole pro zadání hodnoty od uživatele.
				// txtRegEx - regulární výraz pro otestování zadané syntaxe od uživatele.
				// txtErrorInfoText - chybová hláška do dialogu ohledně chybné syntaxe.
				final String defaultValue, txtRegEx, txtErrorInfoText;
				
				/*
				 * Výčtový typ pro typ hodnoty, na kterou se má přetypovat zadaný text od uživatele.
				 */
				final DataTypeOfJTextField kindOfField;
				
				
				
				
				
			
				
				// Zjistím si datový typ hodnot, které lze vkládat do Listu:
				final DataTypeOfList dataTypeOfListValues = ReflectionHelper.getDataTypeOfList(p);
				
				/*
				 * Zde se otestuje, zda je typ příslušného listu neznámý datový typ, nebo mu
				 * uživatel nezadal datový typ, v těchto případech mohu najít listy libovolného
				 * datového typu.
				 */
				if (dataTypeOfListValues.getDataTypeOfListEnum() == DataTypeOfListEnum.WILDCARD_TYPE
						|| dataTypeOfListValues.getDataTypeOfListEnum() == DataTypeOfListEnum.WITHOUT_DATA_TYPE) {
					/*
					 * Toto by stačilo pouze k tomu, abych oznámil uživateli, že příslušná metoda
					 * nebo konstruktor nelze zavolat, ale místo toho níže otestuji, zda se najdou
					 * proměnné a nebo metody (pokud je to povoleno) a najdu si Listy libovolného
					 * datového typu.
					 */
//					addInfoUnsupportedParameter(isInstance, p, null);
//					continue;
					
					/*
					 * Zde vím, že se jedná o list, takže budu prohledávat všechny listy, které jsou
					 * libolného datového typu.
					 */
					if ((makeVariablesAvailable || makeMethodsAvailable)
							&& ((tempListForOthersFields = getListFieldsWithAnyType(availableFieldsList)) != null
									&& (tempListForOthersMethods = getAvailableMethodsForMethod_ListGenericCollection(
											clazz, reference)) != null)
							&& (!tempListForOthersFields.isEmpty() || !tempListForOthersMethods.isEmpty())) {

						// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
						// zadání hodnoty:
						createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

						/*
						 * V případě, že se nemají načíst hodnoty pro proměnné, tak ten list vymažu, aby
						 * se případně do JComboBoxu přidaly pouze metody.
						 */
						if (!makeVariablesAvailable)
							tempListForOthersFields.clear();

						/*
						 * V případě, že se nemají načíst metody pro získání hodnoty, tak ten list
						 * vymažu, aby se případně do JComboBoxu přidaly pouze proměnné.
						 */
						if (!makeMethodsAvailable)
							tempListForOthersMethods.clear();

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						else if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
							tempListForOthersMethods = filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p);					
						
						
						// Vytvořím a přidám cmb do dialogu tak, aby uživatel mohl pouze označit položku
						// pro předání.
						createAndAddCmbForObjectValue(gbc, x, y2, pnlValues, tempListForOthersFields,
								tempListForOthersMethods, true);

						continue;
					}
					
					else {
						/*
						 * Zde se výše nanašli ani proměnné ani metody (pokud byly vůbec povoleny), aby
						 * se mohl zavolat nebo předat hodnoty, tak musím oznámit uživateli, že tu
						 * metodu nebo konstruktor nelze zavolat.
						 */
						addInfoUnsupportedParameter(isInstance, p, null);
						continue;
					}
				}
				
				
	
				
				
				
				
				
				
				
				/*
				 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
				 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
				 * proměnné, které jsou stejného datového typu.
				 */
				final List<ParameterValue> fieldValues = new ArrayList<>();
				
				
				/*
				 * Vytvořím si hodnotu,která bude v JComboBxu sloužit jako položka pro zadání
				 * textové hodnoty od uživatele, ale přidá se do cmb pouze v případě, že se mají
				 * přidat položky pro přidání proměnné.
				 */
				final ParameterValue pValue;

				if (makeVariablesAvailable) {
					pValue = new ParameterValue(KindOfVariable.TEXT_VARIABLE, "");
					fieldValues.add(pValue);
				} else
					pValue = null;

				
				
				/*
				 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
				 * je možné zavolat i metody.
				 */
				final List<MethodValue> methodValues = new ArrayList<>();


				
						
						
				
				
				/*
				 * Tato logická proměnná slouží pouze jako takové "řešení nouzové situace",
				 * která může nastat pokud je v parametru metody nebo konstruktoru list, který
				 * není žádného testovaného datového typu a ani nebyla v žádné třídě nalezena
				 * příslušná proměnná, stejného datového typu jako příslušný list příslušného
				 * datové typu.
				 * 
				 * V takovém případě buď nelze konstruktor nebo metodu zavolt nebo lze pouze
				 * předat null, v tomto případě je doplněno předání null hodnoty, takže musím
				 * obebrat tu první položku, která slouží pro zadání textu od uživatele.
				 */
				boolean clearCmbModel = false;
				
				
				
				
				
				
				
				
				
				
				
				
				/*
				 * Postup pro získání položek s listem je takový, že do listu listOfLists si
				 * vložím veškeré nalezené proměnné z tříd a editoru příkazů, které jsou typu
				 * List, prostě list, konkrétní generický typ listu neřeším (ta proměnná list
				 * nesmí být null, pokud je, bude přeskočena).
				 * 
				 * Pak do listu listOfListsWithDataType si vyfiltruji pouze položky, které jsou
				 * již konkrétního datového typu tak, že si v pomocí metody getVarsWithDataTypes
				 * vyfiltruji pouze ty položky, které mají konkrétní datový typ. Takže
				 * proiteruji list listOfLists a z něj si u každé položky zjistím datový typ
				 * položky, který lze do toho listu vkládat, když najdu ten potřebný, pak jej
				 * vložím do výsledného listu, který se vrátí a vloží do proměnné
				 * listOfListsWithDataType.
				 */
				
				
				
				/*
				 * Do tohoto listu si vložím pouze ty položky, které obsahují hodnotu typu List.
				 * 
				 * Jedná se o to, že si ze všech proměnných získaných z tříd a editoru příkazů
				 * "vyfiltruji" pouze ty položky, které jsou načteny z třídy nebo editoru
				 * příkazů a jedná se o hodnotu typu List -> resp. jednu z jeho implementací (a
				 * není null).
				 * 
				 * V parametru v metodě getModelForCmb už by nemusel být parametr List.class,
				 * ale kdyby náhodou byl v nějaké třídě implementován celý list, tak aby se také
				 * načetl.
				 */
				final List<ParameterValue> listOfLists = getModelForCmb(availableFieldsList, List.class, 
						ArrayList.class,  CopyOnWriteArrayList.class,
						LinkedList.class, Stack.class, Vector.class);

				
				
				
				
				
				
				
				// Otestuji datový typ hodnot listu, abych mohl nastavit správný regulární výraz
				// pro rozpoznání hodnot:
				// Note: V listu se mohou nacházet pouze objektové typy hodnot, tak testuji
				// pouze je.
				if (dataTypeOfListValues.getDataType().equals(Byte.class)) {
					// Nastavím výchozí hodnotu jako ukázku hodnot, které se mají zadat:
					defaultValue = "[0, 1, -2]";
										
					
	
					if (makeVariablesAvailable)
						// Vyhledám položky stejného datového typu - Listu typu Byte:
						
						/*
						 * List, do kterého si vložím veškeré položky (načtené proměnné z tříd a editoru
						 * příkazů), které jsou příslušného typu - například Byte, Integer apod. Prostě
						 * typu, který je níže podporován pro předání parametru.
						 * 
						 * Pokud je nastaveno, že se budou data vkládat do přílušného cmb, pak se data v
						 * tomto listu vloží do listu cmbModel, který slouží jako model položek pro
						 * konkrétní cmb pro zvolení parametru pro předání do metody nebo konstruktoru.
						 * 
						 * ("Z listu listOfLists se vyfiltrují pouze listy (položky), do kterých se
						 * mohou vkládat položky typu Byte.")
						 * 
						 * Note:
						 * Bylo by možné vložit nejprve data do nějakého listu, ale to už je spíše
						 * takový "prostředník", resp. krok navíc, který není potřeba.
						 */
						fieldValues.addAll(getVarsWithDataType(listOfLists, Byte.class));
						
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 * 
					 * Metody musejí vracet List příslušného datového typu.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod_List(clazz, reference, List.class,
								Byte.class);

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}

					
					
					// Vložím si potřebné hodnoty do proměnných, abych mohl provést inicializaci na
					// jednom místě:
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_BYTE;
					txtErrorInfoText = txtListByteFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_BYTE;
				}


				else if (dataTypeOfListValues.getDataType().equals(Short.class)) {
					// Nastavím výchozí hodnotu jako ukázku hodnot, které se mají zadat:
					defaultValue = "[0, 1, -2]";

					
					if (makeVariablesAvailable)
						// Vyhledám položky stejného datového typu - Listu typu Short:

						/*
						 * List, do kterého si vložím veškeré položky (načtené proměnné z tříd a editoru
						 * příkazů), které jsou příslušného typu - například Byte, Integer apod. Prostě
						 * typu, který je níže podporován pro předání parametru.
						 * 
						 * Pokud je nastaveno, že se budou data vkládat do přílušného cmb, pak se data v
						 * tomto listu vloží do listu cmbModel, který slouží jako model položek pro
						 * konkrétní cmh pro zvolení parametrupro předání do metody nebo konstruktoru.
						 * 
						 * ("Z listu listOfLists se vyfiltrují pouze listy (položky), do kterých se
						 * mohou vkládat položky typu Short.")
						 * 
						 * Note:
						 * Bylo by možné vložit nejprve data do nějakého listu, ale to už je spíše
						 * takový "prostředník", resp. krok navíc, který není potřeba.
						 * 
						 * Note:
						 * Tento komentář bude stejný pro tyto metody v následujících podmínkách, akorát
						 * pro jiný datovýpoložek pro vkládání do listu, tak už jej uvdádět nebudu.
						 */
						fieldValues.addAll(getVarsWithDataType(listOfLists, Short.class));
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 * 
					 * Metody musejí vracet List příslušného datového typu.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod_List(clazz, reference, List.class,
								Short.class);

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}

					
					// Vložím si potřebné hodnoty do proměnných, abych mohl provést inicializaci na
					// jednom místě:
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_SHORT;
					txtErrorInfoText = txtListShortFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_SHORT;
				}


				else if (dataTypeOfListValues.getDataType().equals(Integer.class)) {
					// Nastavím výchozí hodnotu jako ukázku hodnot, které se mají zadat:
					defaultValue = "[0, 1, -2]";
					
					
					if (makeVariablesAvailable)
						// Vyhledám položky stejného datového typu - Listu typu Integer:
						fieldValues.addAll(getVarsWithDataType(listOfLists, Integer.class));
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 * 
					 * Metody musejí vracet List příslušného datového typu.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod_List(clazz, reference, List.class,
								Integer.class);

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}

					
					// Vložím si potřebné hodnoty do proměnných, abych mohl provést inicializaci na
					// jednom místě:
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_INTEGER;
					txtErrorInfoText = txtListIntegerFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_INTEGER;
				}


				else if (dataTypeOfListValues.getDataType().equals(Long.class)) {
					// Nastavím výchozí hodnotu jako ukázku hodnot, které se mají zadat:
					defaultValue = "[0, 1, -2]";
					
					
					if (makeVariablesAvailable)
						// Vyhledám položky stejného datového typu - Listu typu Long:
						fieldValues.addAll(getVarsWithDataType(listOfLists, Long.class));
						
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 * 
					 * Metody musejí vracet List příslušného datového typu.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod_List(clazz, reference, List.class,
								Long.class);

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}

					
					
					// Vložím si potřebné hodnoty do proměnných, abych mohl provést inicializaci na
					// jednom místě:
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_LONG;
					txtErrorInfoText = txtListLongFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_LONG;
				}


				else if (dataTypeOfListValues.getDataType().equals(Float.class)) {
					// Nastavím výchozí hodnotu jako ukázku hodnot, které se mají zadat:
					defaultValue = "[0.1, 2.3f, -4.5F]";
					
										
					if (makeVariablesAvailable)
						// Vyhledám položky stejného datového typu - Listu typu Float:
						fieldValues.addAll(getVarsWithDataType(listOfLists, Float.class));
						
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 * 
					 * Metody musejí vracet List příslušného datového typu.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod_List(clazz, reference, List.class,
								Float.class);

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}

					
					
					// Vložím si potřebné hodnoty do proměnných, abych mohl provést inicializaci na
					// jednom místě:
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_FLOAT;
					txtErrorInfoText = txtListFloatFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_FLOAT;
				}


				else if (dataTypeOfListValues.getDataType().equals(Double.class)) {
					// Nastavím výchozí hodnotu jako ukázku hodnot, které se mají zadat:
					defaultValue = "[0.1, 2.3d, -4.5D]";

					
					if (makeVariablesAvailable)
						// Vyhledám položky stejného datového typu - Listu typu Double:
						fieldValues.addAll(getVarsWithDataType(listOfLists, Double.class));
						
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 * 
					 * Metody musejí vracet List příslušného datového typu.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod_List(clazz, reference, List.class,
								Double.class);

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}

					
					
					// Vložím si potřebné hodnoty do proměnných, abych mohl provést inicializaci na
					// jednom místě:
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_DOUBLE;
					txtErrorInfoText = txtListDoubleFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_DOUBLE;
				}


				else if (dataTypeOfListValues.getDataType().equals(Character.class)) {
					// Nastavím výchozí hodnotu jako ukázku hodnot, které se mají zadat:
					defaultValue = "[';', 'X', '-']";

					
					if (makeVariablesAvailable)
						// Vyhledám položky stejného datového typu - Listu typu Character:
						fieldValues.addAll(getVarsWithDataType(listOfLists, Character.class));
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 * 
					 * Metody musejí vracet List příslušného datového typu.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod_List(clazz, reference, List.class,
								Character.class);

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}

					
					// Vložím si potřebné hodnoty do proměnných, abych mohl provést inicializaci na
					// jednom místě:
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_CHARACTER;
					txtErrorInfoText = txtListCharacterFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_CHARACTER;
				}


				else if (dataTypeOfListValues.getDataType().equals(Boolean.class)) {
					// Nastavím výchozí hodnotu jako ukázku hodnot, které se mají zadat:
					defaultValue = "[true, false, true]";
					
					
					if (makeVariablesAvailable)
						// Vyhledám položky stejného datového typu - Listu typu Boolean:
						fieldValues.addAll(getVarsWithDataType(listOfLists, Boolean.class));
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 * 
					 * Metody musejí vracet List příslušného datového typu.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod_List(clazz, reference, List.class,
								Boolean.class);

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}

					
					// Vložím si potřebné hodnoty do proměnných, abych mohl provést inicializaci na
					// jednom místě:
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_BOOLEAN;
					txtErrorInfoText = txtListBooleanFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_BOOLEAN;
				}


				else if (dataTypeOfListValues.getDataType().equals(String.class)) {
					// Nastavím výchozí hodnotu jako ukázku hodnot, které se mají zadat:
					defaultValue = "[\"" + txtDefaultTextForStringListAndArray1 + " \", \""
							+ txtDefaultTextForStringListAndArray2 + "\"]";

					
					if (makeVariablesAvailable)
						// Vyhledám položky stejného datového typu - Listu typu String:
						fieldValues.addAll(getVarsWithDataType(listOfLists, String.class));
						
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 * 
					 * Metody musejí vracet List příslušného datového typu.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod_List(clazz, reference, List.class,
								String.class);

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}

					
					// Vložím si potřebné hodnoty do proměnných, abych mohl provést inicializaci na
					// jednom místě:
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_STRING;
					txtErrorInfoText = txtListStringFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_STRING;
				}
				
				
				else {
					/*
					 * K této časí jsem se ještě nikdy nedostal, tak ani nevím, jaké hodnoty tam mám
					 * dát, ale dám tam číslo, protože jej lze vložit číselných datových typů i
					 * Stringu, akorát boolean a char budou mít možná problémy, ale jak jsem již
					 * uvedl, tato podmínka se mi ještě nikdy neprovedla.
					 */
					
					// Naplním proměnnou prázdnou hodnotou:
					defaultValue = "";
					
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_LONG;
					txtErrorInfoText = txtListLongFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_LONG;
					
					/*
					 * V tomto případě jsem nenašel datový typ listu, tak se níže otestují příslušné
					 * proměnné (nalezené) a když se ani v nich nenajde příslušný list, tak je třeba
					 * vymazat položku pro zadání hodnoty od uživatele.
					 */
					clearCmbModel = true;

                    /*
                     * Zde vím, že se jedná o list, takže budu prohledávat všechny listy, které jsou
                     * příslušného datového typu.
                     *
                     * Note:
                     * K části podmínky, kde se testují, jestli nejsou listy tempListForOthersFields a
                     * tempListForOthersMethods null, tak tam musí být "and / &&", aby se naplnily oba listy, resp.
                     * zavolaly metody pro jejich naplnění,
                     * protože, kdyby tam bylo "nebo / || ", tak by se mohla splnit vždy jen jedna část podmínky.
                     * Tedy naplnit pouze jeden z listů, takže by se ve výsledku mohli zobrazit například pouze
                     * proměnné a ne metody, protože by po naplnění listu s proměnnými byla podmínka splněna.
                     */
					if ((makeVariablesAvailable || makeMethodsAvailable)
							&& ((tempListForOthersFields = getListFieldsWithSpecificDataType(availableFieldsList,
									dataTypeOfListValues.getDataType())) != null
									&& (tempListForOthersMethods = getAvailableMethodsForMethod_List(clazz, reference,
											List.class, dataTypeOfListValues.getDataType())) != null)
							&& (!tempListForOthersFields.isEmpty() || !tempListForOthersMethods.isEmpty())) {
							
						// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
						// zadání hodnoty:
						createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

						/*
						 * V případě, že se nemají načíst hodnoty pro proměnné, tak ten list vymažu, aby
						 * se případně do JComboBoxu přidaly pouze metody.
						 */
						if (!makeVariablesAvailable)
							tempListForOthersFields.clear();

						/*
						 * V případě, že se nemají načíst metody pro získání hodnoty, tak ten list
						 * vymažu, aby se případně do JComboBoxu přidaly pouze proměnné.
						 */
						if (!makeMethodsAvailable)
							tempListForOthersMethods.clear();

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						else if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
							tempListForOthersMethods = filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p);
						
						
						// Vytvořím a přidám cmb do dialogu tak, aby uživatel mohl pouze označit položku
						// pro předání.
						createAndAddCmbForObjectValue(gbc, x, y2, pnlValues, tempListForOthersFields,
								tempListForOthersMethods, true);

						continue;
					}
				}

				
				
				
				
				
				
				/*
				 * Následuje vytvoření textového pole nebo JComboBoxu pro příslušné hodnoty:
				 */
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					if (clearCmbModel) {
						/*
						 * Zde v se jedná o nepodporovaný parametr, resp. nepodporovaný datový typ
						 * parametru, tak do okna s chybami vypíšu hlášení, že příslušnou metodu nebo
						 * konstruktor nebude možné zavolat.
						 * 
						 * Konkrétně jde o to, že jsem našel nějaký list, ke kterému nelze prostě zadat
						 * hodnotu "ručně" od uživatele v dialogu, takže jej tento parametr nebude možné
						 * předat do parametru konstruktoru nebo metody.
						 */
						addInfoUnsupportedParameter(isInstance, p, null);

						continue;
					}

					// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
					// zadání hodnoty:
					createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

					/*
					 * Textové pole pro zadání hodnoty od uživatele:
					 */
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, defaultValue, pnlValues);

					addKeyListenerToTxtField(txtField, txtRegEx, txtErrorInfoText, kindOfField, x);
				}

				else {
					if (pValue != null)
						pValue.setTextValue(defaultValue);

					/*
					 * Pokud se jedná o případě, kdy se nepodařilo nalézt ani proměnnou ve třídách
					 * stejného datového typu jako příslušný list, pak jej nelze zavolat, takže
					 * nezbývá než vytvořit cmb pouze s null hodnotou.
					 */
					if (clearCmbModel) {
						// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
						// zadání hodnoty:
						createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

						// Cmb pro zadání hodnoty:
						createAndAddCmbForObjectValue(gbc, x, y2, pnlValues, new ArrayList<>(), new ArrayList<>(),
								true);

						continue;
					}

					// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
					// zadání hodnoty:
					createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, txtRegEx, txtErrorInfoText, kindOfField, x,
							true);
				}
			}
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			// Jednotlivé instance listu (kolekce):
			else if (p.getType().equals(ArrayList.class) || p.getType().equals(CopyOnWriteArrayList.class)
					|| p.getType().equals(LinkedList.class) || p.getType().equals(Stack.class)
					|| p.getType().equals(Vector.class)) {
				
				
				/*
				 * Zjistím si datový typ hodnot, které lze vkládat do příslušné kolekce:
				 * Například kolekce typu String, Integer apod.
				 */
				final DataTypeOfList dataTypeOfListValues = ReflectionHelper.getDataTypeOfList(p);
				
				/*
				 * Poud se jedná o neznámý datový typ nebo ten datový typ uživatel nezadal, pak
				 * je možné vyhledat veškeré proměnné a metody, které vracejí příslušnou
				 * implementaci Listu, ale s libovolným datovým typem.
				 */
				if (dataTypeOfListValues.getDataTypeOfListEnum() == DataTypeOfListEnum.WILDCARD_TYPE
						|| dataTypeOfListValues.getDataTypeOfListEnum() == DataTypeOfListEnum.WITHOUT_DATA_TYPE) {
					/*
					 * Zde se jedná o generický datový typ, tak mohu prohledat všechny listy, které
					 * vracejí příslušný List typu p.getType a ten list může být libovolného datového
					 * typu.
					 */
//					addInfoUnsupportedParameter(isInstance, p, null);
//					continue;
					
					
					/*
					 * Zde vím, že se jedná o list, takže budu prohledávat všechny implementace
					 * listu, které jsou libovolného datového typu.
					 */
					if ((makeVariablesAvailable || makeMethodsAvailable)
							&& ((tempListForOthersFields = getListFieldsWithAnyType(availableFieldsList,
									p.getType())) != null
									&& (tempListForOthersMethods = getAvailableMethodsForMethod_ListGeneric(clazz,
											reference, p.getType())) != null)
							&& (!tempListForOthersFields.isEmpty() || !tempListForOthersMethods.isEmpty())) {

						// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
						// zadání hodnoty:
						createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

						// Pokud se nemají načíst proměnné, tak vymažu načtený list s proměnnými:
						if (!makeVariablesAvailable)
							tempListForOthersFields.clear();
						
						// Pokud se nemají načíst metody, tak vymažu načtený list s metodami:
						if (!makeMethodsAvailable)
							tempListForOthersMethods.clear();
						
						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						else if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
							tempListForOthersMethods = filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p);
						
						
						// Vytvořím a přidám cmb do dialogu tak, aby uživatel mohl pouze označit položku
						// pro předání.
						createAndAddCmbForObjectValue(gbc, x, y2, pnlValues, tempListForOthersFields,
								tempListForOthersMethods, true);

						continue;
					}
					
					else {
						/*
						 * Zde se nenašli proměnné nebo metody, které vracejí příslušný list libovolného
						 * datového typu, nebo ty metody nebo proměnné nejsou povoleny. Tak vypíšu
						 * oznámení, že metodu nebo konstruktor nelze zavolat, protože ty parametru
						 * nelze naplnit.
						 *
						 * Zde by byla možnost předat v každém případě null hodnotu.
						 */
						addInfoUnsupportedParameter(isInstance, p, null);
						continue;
					}
				}
				
				
				
				
				
				
				

				
				
				/*
				 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
				 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
				 * proměnné, které jsou stejného datového typu.
				 */
				final List<ParameterValue> fieldValues = new ArrayList<>();
				
				final ParameterValue pValue;
				
				if (makeVariablesAvailable) {
					pValue = new ParameterValue(KindOfVariable.TEXT_VARIABLE, "");
					fieldValues.add(pValue);
					
					/*
					 * Získám si list, který bude obsahovat získané proměnné pouze z tříd v diagramu
					 * tříd a vytvořených instancí v diagramu instancí. A tyto proměnné jsou jednoho
					 * z konkrétních typů kolekcí, například Arraylist, LinkedList, Vector, Stack
					 * apod. (viz podmínka výše). A ta kolekce je konkrétního datového typu
					 * dataTypeOfListValues, tj. jedná se například o kolekci typu String, Integer
					 * apod.
					 * 
					 * 
					 * Note:
					 * Bylo by možné využít metodu 'getVarsWithDataType' například v následující
					 * syntaxi:
					 * 
					 * cmbModel.addAll(getVarsWithDataType(listOfLists, Byte.class));
					 * 
					 * - která je využita pro získání typů listů, ale aby to bylo "přímo" zamřené a
					 * o podmínku rychlejší, tak jsem napsal novou metodu: 'getFieldsWithDataTypes',
					 * která vyhledává potřebná data pouze z proměnných načtených z tříd, protože
					 * tyto proměnné nelze v aplikaci například v editoru příkazů vytvořit. Tak nemá
					 * smysl jej testovat.
					 */
					final List<ParameterValue> listOfLists = getFieldsWithDataTypes(availableFieldsList, p.getType(),
							dataTypeOfListValues.getDataType());

					fieldValues.addAll(listOfLists);
				}
				else pValue = null;

				
				
				/*
				 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
				 * je možné zavolat i metody.
				 */
				final List<MethodValue> methodValues = new ArrayList<>();
				
				
				

				
				
				
				/*
				 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
				 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
				 * předala do parametru metody.
				 * 
				 * Metody musejí vracet List (jednu z jeho implementací, například ArrayList,
				 * LinkedList apod.) příslušného datového typu.
				 */
				if (makeMethodsAvailable) {
					tempListForOthersMethods = getAvailableMethodsForMethod_List(clazz, reference, p.getType(),
							dataTypeOfListValues.getDataType());

					/*
					 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
					 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
					 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
					 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
					 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
					 * setr - akorát s get nebo is na začátku:
					 */
					if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                        methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

					else
						methodValues.addAll(tempListForOthersMethods);
				}

				
				
				
				
				/*
				 * Proměnná, dle které se pozná, zda se má nebo nemá vymazat model a dát pouze
				 * hodnota null, nebo zda je vůbec možné zavolat metodu či konstruktor s
				 * příslušným parametrem apod.
				 * 
				 * Tato hodnota bude true, když se zjistí, že se nejdená o jeden z možných
				 * datových typů a že nebyla nalezan proměnná v nějaké třídě, ze které by bylo
				 * možné předat hodnotu.
				 */
				boolean clearCmbModel = false;
				
				
				
				
				
				
				
				// defaultValue - Proměnná, do které se vloží výchozí hodnota pro pole pro zadání hodnoty od uživatele.
				// txtRegEx - regulární výraz pro otestování zadané syntaxe od uživatele.
				// txtErrorInfoText - chybová hláška do dialogu ohledně chybné syntaxe.
				final String defaultValue, txtRegEx, txtErrorInfoText;
				
				/*
				 * Výčtový typ pro typ hodnoty, na kterou se má přetypovat zadaný text od uživatele.
				 */
				final DataTypeOfJTextField kindOfField;
				
				
				
				
				
				
				
				
				// Otestuji datový typ hodnot listu, abych mohl nastavit správný regulární výraz
				// pro rozpoznání hodnot:
				// Note: V listu se mohou nacházet pouze objektové typy hodnot, tak testuji
				// pouze je.

				if (dataTypeOfListValues.getDataType().equals(Byte.class)) {
					// Nastavím výchozí hodnotu jako ukázku hodnot, které se mají zadat:
					defaultValue = "[0, 1, -2]";

					
					// Vložím si potřebné hodnoty do proměnných, abych mohl provést inicializaci na
					// jednom místě:
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_BYTE;
					txtErrorInfoText = txtCollectionByteFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_BYTE;
				}


				else if (dataTypeOfListValues.getDataType().equals(Short.class)) {
					// Nastavím výchozí hodnotu jako ukázku hodnot, které se mají zadat:
					defaultValue = "[0, 1, -2]";

					
					// Vložím si potřebné hodnoty do proměnných, abych mohl provést inicializaci na
					// jednom místě:
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_SHORT;
					txtErrorInfoText = txtCollectionShortFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_SHORT;
				}


				else if (dataTypeOfListValues.getDataType().equals(Integer.class)) {
					// Nastavím výchozí hodnotu jako ukázku hodnot, které se mají zadat:
					defaultValue = "[0, 1, -2]";

					
					// Vložím si potřebné hodnoty do proměnných, abych mohl provést inicializaci na
					// jednom místě:
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_INTEGER;
					txtErrorInfoText = txtCollectionIntegerFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_INTEGER;
				}


				else if (dataTypeOfListValues.getDataType().equals(Long.class)) {
					// Nastavím výchozí hodnotu jako ukázku hodnot, které se mají zadat:
					defaultValue = "[0, 1, -2]";

					
					// Vložím si potřebné hodnoty do proměnných, abych mohl provést inicializaci na
					// jednom místě:
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_LONG;
					txtErrorInfoText = txtCollectionLongFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_LONG;
				}


				else if (dataTypeOfListValues.getDataType().equals(Float.class)) {
					// Nastavím výchozí hodnotu jako ukázku hodnot, které se mají zadat:
					defaultValue = "[0.1, 2.3f, -4.5F]";

					
					// Vložím si potřebné hodnoty do proměnných, abych mohl provést inicializaci na
					// jednom místě:
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_FLOAT;
					txtErrorInfoText = txtCollectionFloatFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_FLOAT;
				}


				else if (dataTypeOfListValues.getDataType().equals(Double.class)) {
					// Nastavím výchozí hodnotu jako ukázku hodnot, které se mají zadat:
					defaultValue = "[0.1, 2.3d, -4.5D]";

					
					// Vložím si potřebné hodnoty do proměnných, abych mohl provést inicializaci na
					// jednom místě:
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_DOUBLE;
					txtErrorInfoText = txtCollectionDoubleFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_DOUBLE;
				}


				else if (dataTypeOfListValues.getDataType().equals(Character.class)) {
					// Nastavím výchozí hodnotu jako ukázku hodnot, které se mají zadat:
					defaultValue = "[';', 'X', '-']";

					
					// Vložím si potřebné hodnoty do proměnných, abych mohl provést inicializaci na
					// jednom místě:
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_CHARACTER;
					txtErrorInfoText = txtCollectionCharacterFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_CHARACTER;
				}


				else if (dataTypeOfListValues.getDataType().equals(Boolean.class)) {
					// Nastavím výchozí hodnotu jako ukázku hodnot, které se mají zadat:
					defaultValue = "[true, false, true]";

					
					// Vložím si potřebné hodnoty do proměnných, abych mohl provést inicializaci na
					// jednom místě:
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_BOOLEAN;
					txtErrorInfoText = txtCollectionBooleanFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_BOOLEAN;
				}


				else if (dataTypeOfListValues.getDataType().equals(String.class)) {
					// Nastavím výchozí hodnotu jako ukázku hodnot, které se mají zadat:
					defaultValue = "[\"" + txtDefaultTextForStringListAndArray1 + " \", \""
							+ txtDefaultTextForStringListAndArray2 + "\"]";

					
					// Vložím si potřebné hodnoty do proměnných, abych mohl provést inicializaci na
					// jednom místě:
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_STRING;
					txtErrorInfoText = txtCollectionStringFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_STRING;
				}
				
				
				else {
					/*
					 * K této časí jsem se ještě nikdy nedostal, tak ani nevím, jaké hodnoty tam mám
					 * dát, ale dám tam číslo, protože jej lze vložit číselných datových typů i
					 * Stringu, akorát boolean a char budou mítmožná problémy, ale jak jsem již
					 * uvedl, tato podmínka se mi ještě nikdy neprovedla.
					 */
					
					// Naplním proměnnou prázdnou hodnotou:
					defaultValue = "";
					
					txtRegEx = REG_EX_FOR_LIST_AND_ARRAY_LONG;
					txtErrorInfoText = txtListLongFormatError;
					kindOfField = DataTypeOfJTextField.LIST_AND_ARRAY_LONG;
					
					
					/*
					 * V tomto případě jsem nenašel datový typ listu, tak se níže otestují příslušné
					 * proměnné (nalezené) a když se ani v nich nenajde příslušný list, tak je třeba
					 * vymazat položku pro zadání hodnoty od uživatele.
					 */
					clearCmbModel = true;
					
					/*
					 * Zde vím, že se jedná o list, takže budu prohledávat všechny listy, které jsou
					 * příslušného datového typu.
					 */
					if ((makeVariablesAvailable || makeMethodsAvailable)
							&& ((tempListForOthersFields = getListFieldsWithSpecificDataType(availableFieldsList,
									dataTypeOfListValues.getDataType())) != null
									|| (tempListForOthersMethods = getAvailableMethodsForMethod_List(clazz, reference,
											p.getType(), dataTypeOfListValues.getDataType())) != null)
							&& (!tempListForOthersFields.isEmpty() || !tempListForOthersMethods.isEmpty())) {
						
						
						// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
						// zadání hodnoty:
						createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

						// Pokud se nemají načíst proměnné, tak vymažu načtený list s proměnnými:
						if (!makeVariablesAvailable)
							tempListForOthersFields.clear();
						
						// Pokud se nemají načíst metody, tak vymažu načtený list s metodami:
						if (!makeMethodsAvailable)
							tempListForOthersMethods.clear();

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						else if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
							tempListForOthersMethods = filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p);
						
						// Vytvořím a přidám cmb do dialogu tak, aby uživatel mohl pouze označit položku
						// pro předání.
						createAndAddCmbForObjectValue(gbc, x, y2, pnlValues, tempListForOthersFields,
								tempListForOthersMethods, true);

						continue;
					}
				}

				
				
				
				
				
				
				/*
				 * Následuje vytvoření textového pole nebo JComboBoxu pro příslušné hodnoty:
				 */
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					if (clearCmbModel) {
						/*
						 * Zde v se jedná o nepodporovaný parametr, resp. nepodporovaný datový typ
						 * parametru, tak do okna s chybami vypíšu hlášení, že příslušnou metodu nebo
						 * konstruktor nebude možné zavolat.
						 * 
						 * Konkrétně jde o to, že jsem našel nějaký list, ke kterému nelze prostě zadat
						 * hodnotu "ručně" od uživatele v dialogu, takže jej tento parametr nebude možné
						 * předat do parametru konstruktoru nebo metody.
						 */
						addInfoUnsupportedParameter(isInstance, p, null);

						continue;
					}

					// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
					// zadání hodnoty:
					createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

					/*
					 * Textové pole pro zadání hodnoty od uživatele:
					 */
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, defaultValue, pnlValues);

					addKeyListenerToTxtField(txtField, txtRegEx, txtErrorInfoText, kindOfField, x);
				}

				else {
					if (pValue != null)
						pValue.setTextValue(defaultValue);

					/*
					 * Pokud se jedná o případě, kdy se nepodařilo nalézt ani proměnnou ve třídách
					 * stejného datového typu jako příslušný list, pak jej nelze zavolat, takže
					 * nezbývá než vytvořit cmb pouze s null hodnotou.
					 */
					if (clearCmbModel) {
						// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
						// zadání hodnoty:
						createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

						// Cmb pro zadání hodnoty:
						createAndAddCmbForObjectValue(gbc, x, y2, pnlValues, new ArrayList<>(), new ArrayList<>(),
								true);

						continue;
					}

					// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
					// zadání hodnoty:
					createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, txtRegEx, txtErrorInfoText, kindOfField, x,
							true);
				}
			}
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			// Následuje testování pro jednorozměrné pole:
			
			// Otestuji, zda se jedná o parametry typu jednorozměrného pole a pole je byte int nebo Byte
			else if (p.getType().equals(byte[].class) || p.getType().equals(Byte[].class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);
				
				final String defaultValue = "[0, 1, -2]";
				final String txtErrorInfo = txtArrayFormatErrorForAll1 + " " + p.getType().getSimpleName() + " "
						+ txtArrayFormatErrorAll2 + txtByteInterval;
				
				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadání hodnot:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, defaultValue, pnlValues);

					// Přidání na textové pole reakci na stisknutí klávesy, na otestování hodnot pro
					addKeyListenerToTxtField(txtField, REG_EX_FOR_LIST_AND_ARRAY_BYTE, txtErrorInfo,
							DataTypeOfJTextField.LIST_AND_ARRAY_BYTE, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();

					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, defaultValue));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu ("úplně stejné pole"):
						fieldValues.addAll(getModelForCmbArray(availableFieldsList, p.getType()));
					}
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_LIST_AND_ARRAY_BYTE, txtErrorInfo,
							DataTypeOfJTextField.LIST_AND_ARRAY_BYTE, x, true);
				}
			}
			
			
			// Otestuji, zda se jedná o parametry typu jednorozměrného pole a pole je typu short nebo Short
			else if (p.getType().equals(short[].class) || p.getType().equals(Short[].class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);
				
				final String txtDefaultValue = "[0, 1, -2]";
				final String txtErrorInfo = txtArrayFormatErrorForAll1 + " " + p.getType().getSimpleName() + " "
						+ txtArrayFormatErrorAll2 + txtShortInterval;
				
				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadání hodnot:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, txtDefaultValue, pnlValues);

					// Přidání na textové pole reakci na stisknutí klávesy, na otestování hodnot pro
					// jednorozměrné pole typu short nebo Short:
					addKeyListenerToTxtField(txtField, REG_EX_FOR_LIST_AND_ARRAY_SHORT, txtErrorInfo,
							DataTypeOfJTextField.LIST_AND_ARRAY_SHORT, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();

					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, txtDefaultValue));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu ("úplně stejné
						// pole"):
						fieldValues.addAll(getModelForCmbArray(availableFieldsList, p.getType()));						
					}

					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						
					

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_LIST_AND_ARRAY_SHORT, txtErrorInfo,
							DataTypeOfJTextField.LIST_AND_ARRAY_SHORT, x, true);
				}
			}
			
			
			
			
			// Otestuji, zda se jedná o parametry typu jednorozměrného pole a pole je typu int nebo Integer
			else if (p.getType().equals(int[].class) || p.getType().equals(Integer[].class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);
				
				final String txtDefaultValue = "[0, 1, -2]";
				final String txtErrorInfo = txtArrayFormatErrorForAll1 + " " + p.getType().getSimpleName() + " "
						+ txtArrayFormatErrorAll2 + txtIntegerInterval;
				
				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadání hodnot:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, txtDefaultValue, pnlValues);

					// Přidání na textové pole reakci na stisknutí klávesy, na otestování hodnot pro
					// jednorozměrné pole typu int nebo Integer:
					addKeyListenerToTxtField(txtField, REG_EX_FOR_LIST_AND_ARRAY_INTEGER, txtErrorInfo,
							DataTypeOfJTextField.LIST_AND_ARRAY_INTEGER, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();

					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, txtDefaultValue));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu ("úplně stejné
						// pole"):
						fieldValues.addAll(getModelForCmbArray(availableFieldsList, p.getType()));						
					}
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						


					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_LIST_AND_ARRAY_INTEGER, txtErrorInfo,
							DataTypeOfJTextField.LIST_AND_ARRAY_INTEGER, x, true);
				}
			}
			
			
			
			
			
			
			// Otestuji, zda se jedná o parametry typu jednorozměrného pole a pole je typu long nebo Long
			else if (p.getType().equals(long[].class) || p.getType().equals(Long[].class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);
				
				final String txtDefaultValue = "[0, 1, -2]";
				final String txtErrorInfo = txtArrayFormatErrorForAll1 + " " + p.getType().getSimpleName() + " "
						+ txtArrayFormatErrorAll2 + txtLongInterval;
				
				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadání hodnot:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, txtDefaultValue, pnlValues);

					// Přidání na textové pole reakci na stisknutí klávesy, na otestování hodnot pro
					// jednorozměrné pole typu long nebo Long:
					addKeyListenerToTxtField(txtField, REG_EX_FOR_LIST_AND_ARRAY_LONG, txtErrorInfo,
							DataTypeOfJTextField.LIST_AND_ARRAY_LONG, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();

					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, txtDefaultValue));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu ("úplně stejné
						// pole"):
						fieldValues.addAll(getModelForCmbArray(availableFieldsList, p.getType()));						
					}
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_LIST_AND_ARRAY_LONG, txtErrorInfo,
							DataTypeOfJTextField.LIST_AND_ARRAY_LONG, x, true);
				}
			}
			
			
			
			
			// Otestuji, zda se jedná o parametry typu jednorozměrného pole a pole je typu float nebo Float
			else if (p.getType().equals(float[].class) || p.getType().equals(Float[].class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);
				
				final String txtDefaultValue = "[0.1, 2.3f, -4.5F]";
				final String txtErrorInfo = txtArrayFormatErrorForAll1 + " " + p.getType().getSimpleName() + " "
						+ txtArrayFormatErrorAll2 + txtFloatInterval;
				
				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadání hodnot:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, txtDefaultValue, pnlValues);

					// Přidání na textové pole reakci na stisknutí klávesy, na otestování hodnot pro
					// jednorozměrné pole typu float nebo Float:
					addKeyListenerToTxtField(txtField, REG_EX_FOR_LIST_AND_ARRAY_FLOAT, txtErrorInfo,
							DataTypeOfJTextField.LIST_AND_ARRAY_FLOAT, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();
					
					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, txtDefaultValue));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu ("úplně stejné
						// pole"):
						fieldValues.addAll(getModelForCmbArray(availableFieldsList, p.getType()));						
					}

					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						
					

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_LIST_AND_ARRAY_FLOAT, txtErrorInfo,
							DataTypeOfJTextField.LIST_AND_ARRAY_FLOAT, x, true);
				}
			}
			
			
						
			
			// Otestuji, zda se jedná o parametry typu jednorozměrného pole a pole je typu double nebo Double
			else if (p.getType().equals(double[].class) || p.getType().equals(Double[].class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);
				
				final String txtDefaultValue = "[0.1, 2.3d, -4.5D]";
				final String txtErrorInfo = txtArrayFormatErrorForAll1 + " " + p.getType().getSimpleName() + " "
						+ txtArrayFormatErrorAll2 + txtDoubleInterval;
				
				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadání hodnot:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, txtDefaultValue, pnlValues);

					// Přidání na textové pole reakci na stisknutí klávesy, na otestování hodnot pro
					// jednorozměrné pole typu double nebo Double:
					addKeyListenerToTxtField(txtField, REG_EX_FOR_LIST_AND_ARRAY_DOUBLE, txtErrorInfo,
							DataTypeOfJTextField.LIST_AND_ARRAY_DOUBLE, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();
					
					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, txtDefaultValue));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu ("úplně stejné
						// pole"):
						fieldValues.addAll(getModelForCmbArray(availableFieldsList, p.getType()));						
					}
					

					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						


					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_LIST_AND_ARRAY_DOUBLE, txtErrorInfo,
							DataTypeOfJTextField.LIST_AND_ARRAY_DOUBLE, x, true);
				}
			}
			
			
			
			
			// Otestuji, zda se jedná o parametry typu jednorozměrného pole a pole je typu char nebo Character
			else if (p.getType().equals(char[].class) || p.getType().equals(Character[].class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);
				
				final String txtDefaultValue = "[';', 'X', '-']";
				final String txtErrorInfo = txtArrayFormatErrorForAll1 + " " + p.getType().getSimpleName() + " "
						+ txtArrayCharacterFormatError2;
				
				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadání hodnot:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, txtDefaultValue, pnlValues);

					// Přidání na textové pole reakci na stisknutí klávesy, na otestování hodnot pro
					// jednorozměrné pole typu char nebo Character:
					addKeyListenerToTxtField(txtField, REG_EX_FOR_LIST_AND_ARRAY_CHARACTER, txtErrorInfo,
							DataTypeOfJTextField.LIST_AND_ARRAY_CHARACTER, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();

					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, txtDefaultValue));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu ("úplně stejné
						// pole"):
						fieldValues.addAll(getModelForCmbArray(availableFieldsList, p.getType()));						
					}
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_LIST_AND_ARRAY_CHARACTER,
							txtErrorInfo, DataTypeOfJTextField.LIST_AND_ARRAY_CHARACTER, x, true);
				}
			}
			
			
			
			
			// Otestuji, zda se jedná o parametry typu jednorozměrného pole a pole je typu boolean nebo Boolean
			else if (p.getType().equals(boolean[].class) || p.getType().equals(Boolean[].class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);
				
				final String txtDefaultValue = "[true, false, true]";
				final String txtErrorInfo = txtArrayFormatErrorForAll1 + " "
						+ p.getType().getSimpleName() + " " + txtArrayBooleanFormatError2;
				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadání hodnot:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, txtDefaultValue, pnlValues);

					// Přidání na textové pole reakci na stisknutí klávesy, na otestování hodnot pro
					// jednorozměrné pole typu boolean nebo Boolean:
					addKeyListenerToTxtField(txtField, REG_EX_FOR_LIST_AND_ARRAY_BOOLEAN, txtErrorInfo,
							DataTypeOfJTextField.LIST_AND_ARRAY_BOOLEAN, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();
					
					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, txtDefaultValue));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu ("úplně stejné
						// pole"):
						fieldValues.addAll(getModelForCmbArray(availableFieldsList, p.getType()));						
					}
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_LIST_AND_ARRAY_BOOLEAN, txtErrorInfo,
							DataTypeOfJTextField.LIST_AND_ARRAY_BOOLEAN, x, true);
				}
			}
			
			
			
			
			
			
			// Otestuji, zda se jedná o parametry typu jednorozměrného pole a pole je typu
			// int nebo Integer
			else if (p.getType().equals(String[].class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
				// zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

				final String txtDefaultValue = "[\"" + txtDefaultTextForStringListAndArray1 + " \", \""
						+ txtDefaultTextForStringListAndArray2 + "\"]";
				final String txtErrorInfo = txtArrayFormatErrorForAll1 + " " + p.getType().getSimpleName() + " "
						+ txtArrayStringFormatError2;

				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadání hodnot:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, txtDefaultValue, pnlValues);

					// Přidání na textové pole reakci na stisknutí klávesy, na otestování hodnot pro
					// jednorozměrné pole typu String:
					addKeyListenerToTxtField(txtField, REG_EX_FOR_LIST_AND_ARRAY_STRING, txtErrorInfo,
							DataTypeOfJTextField.LIST_AND_ARRAY_STRING, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();
					
					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, txtDefaultValue));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu ("úplně stejné
						// pole"):
						fieldValues.addAll(getModelForCmbArray(availableFieldsList, p.getType()));						
					}
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						


					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_LIST_AND_ARRAY_STRING, txtErrorInfo,
							DataTypeOfJTextField.LIST_AND_ARRAY_STRING, x, true);
				}
			}
			
			
			
			

			
			
			
			
			
			
			
			
			
			
			/*
			 * V následujících podmínkach testuji dvojrozměrné pole. Jelikož v aplikaci
			 * samotné není implementováno vytvoření dvojrozměrného pole například v editoru
			 * příkazů apod. Proto má uživatel možnost jej vytvořit pomocí zadaného textu v
			 * požadované syntaxi.
			 * 
			 * Proto je v následujícíh podmínkách implementováné pouze rozpoznávání více
			 * rozměrného pole pouze z proměnných načtených z nějaké třídy. není zde podpora
			 * pro rozpoznávání vícerozměrných polí jako výše u jednorozměrného.
			 * 
			 * Kdyby se tato aplikace v budoucnu rozšířila o podporu pro vícerozměrné pole,
			 * že by je mohl uživatel vytvořit apod. Tak v následujícíh podmínkách by bylo
			 * třeba nahradit metodu pro vyfiltrování položek tak, aby testovaly příslušnou
			 * proměnnou, jako tomu je výše u filtrování položek s jednorozměrným polem, tj.
			 * upravit metodu getModelForCmbArray, resp. napsat ji stejně, akorát by
			 * testovala jinouproměnnou pro dvojrozměrné pole, nebo pokud by se v objektu
			 * Parametervalue využili stejné proměnné, tak by se mohla využit metoda
			 * getModelForCmbArray místo getModelForCmb (současné) a bylo by to vyřešeno
			 * (například).
			 */
			
			
			// Zde následují podmínky pro otestování, zda se jedná o dvojrozměrné pole:
			
			else if (p.getType().equals(byte[][].class) || p.getType().equals(Byte[][].class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
				// zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

				final String txtDefaultValue = "[{0, -1}, {-2, 3}]";
				final String txtErrorInfo = txt2ArrayFormatErrorAll1 + " " + p.getType().getSimpleName() + " "
						+ txtArrayFormatErrorAll2 + txtByteInterval;
				
				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadání hodnot:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, txtDefaultValue, pnlValues);

					// Přidání na textové pole reakci na stisknutí klávesy, na otestování hodnot pro
					// jednorozměrné pole typu String:
					addKeyListenerToTxtField(txtField, REG_EX_FOR_TWO_DIM_ARRAY_BYTE, txtErrorInfo,
							DataTypeOfJTextField.TWO_DIMENSIONAL_ARRAY_BYTE, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();

					
					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, txtDefaultValue));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu:
						fieldValues.addAll(getModelForCmb(availableFieldsList, p.getType()));						
					}
					
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_TWO_DIM_ARRAY_BYTE, txtErrorInfo,
							DataTypeOfJTextField.TWO_DIMENSIONAL_ARRAY_BYTE, x, true);
				}
			}
						
			
			
			else if (p.getType().equals(short[][].class) || p.getType().equals(Short[][].class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
				// zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

				final String txtDefaultValue = "[{0, -1}, {-2, 3}]";
				final String txtErrorInfo = txt2ArrayFormatErrorAll1 + " " + p.getType().getSimpleName() + " "
						+ txtArrayFormatErrorAll2 + txtShortInterval;

				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadání hodnot:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, txtDefaultValue, pnlValues);

					// Přidání na textové pole reakci na stisknutí klávesy, na otestování hodnot pro
					// jednorozměrné pole typu String:
					addKeyListenerToTxtField(txtField, REG_EX_FOR_TWO_DIM_ARRAY_SHOWRT, txtErrorInfo,
							DataTypeOfJTextField.TWO_DIMENSIONAL_ARRAY_SHORT, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();

					
					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, txtDefaultValue));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu:
						fieldValues.addAll(getModelForCmb(availableFieldsList, p.getType()));						
					}
										
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_TWO_DIM_ARRAY_SHOWRT, txtErrorInfo,
							DataTypeOfJTextField.TWO_DIMENSIONAL_ARRAY_SHORT, x, true);
				}
			}
			
			
			
			else if (p.getType().equals(int[][].class) || p.getType().equals(Integer[][].class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
				// zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

				final String txtDefaultValue = "[{0, -1}, {-2, 3}]";
				final String txtErrorInfo = txt2ArrayFormatErrorAll1 + " " + p.getType().getSimpleName() + " "
						+ txtArrayFormatErrorAll2 + txtIntegerInterval;

				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadání hodnot:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, txtDefaultValue, pnlValues);

					// Přidání na textové pole reakci na stisknutí klávesy, na otestování hodnot pro
					// jednorozměrné pole typu String:
					addKeyListenerToTxtField(txtField, REG_EX_FOR_TWO_DIM_ARRAY_INTEGER, txtErrorInfo,
							DataTypeOfJTextField.TWO_DIMENSIONAL_ARRAY_INTEGER, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();
					
					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, txtDefaultValue));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu:
						fieldValues.addAll(getModelForCmb(availableFieldsList, p.getType()));						
					}
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_TWO_DIM_ARRAY_INTEGER, txtErrorInfo,
							DataTypeOfJTextField.TWO_DIMENSIONAL_ARRAY_INTEGER, x, true);
				}
			}
			
			
			
			else if (p.getType().equals(long[][].class) || p.getType().equals(Long[][].class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
				// zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

				final String txtDefaultValue = "[{0, -1}, {-2, 3}]";
				final String txtErrorInfo = txt2ArrayFormatErrorAll1 + " " + p.getType().getSimpleName() + " "
						+ txtArrayFormatErrorAll2 + txtLongInterval;

				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadání hodnot:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, txtDefaultValue, pnlValues);

					// Přidání na textové pole reakci na stisknutí klávesy, na otestování hodnot pro
					// jednorozměrné pole typu String:
					addKeyListenerToTxtField(txtField, REG_EX_FOR_TWO_DIM_ARRAY_LONG, txtErrorInfo,
							DataTypeOfJTextField.TWO_DIMENSIONAL_ARRAY_LONG, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();
					
					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, txtDefaultValue));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu:
						fieldValues.addAll(getModelForCmb(availableFieldsList, p.getType()));						
					}
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_TWO_DIM_ARRAY_LONG, txtErrorInfo,
							DataTypeOfJTextField.TWO_DIMENSIONAL_ARRAY_LONG, x, true);
				}
			}
			
			
			
			else if (p.getType().equals(float[][].class) || p.getType().equals(Float[][].class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
				// zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

				final String txtDefaultValue = "[{0f, -1F}, {-2.3f, 3.4F}]";
				final String txtErrorInfo = txt2ArrayFormatErrorAll1 + " " + p.getType().getSimpleName() + " "
						+ txtArrayFormatErrorAll2 + txtFloatInterval;

				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadání hodnot:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, txtDefaultValue, pnlValues);

					// Přidání na textové pole reakci na stisknutí klávesy, na otestování hodnot pro
					// jednorozměrné pole typu String:
					addKeyListenerToTxtField(txtField, REG_EX_FOR_TWO_DIM_ARRAY_FLOAT, txtErrorInfo,
							DataTypeOfJTextField.TWO_DIMENSIONAL_ARRAY_FLOAT, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();
					
					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, txtDefaultValue));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu:
						fieldValues.addAll(getModelForCmb(availableFieldsList, p.getType()));
					}

					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_TWO_DIM_ARRAY_FLOAT, txtErrorInfo,
							DataTypeOfJTextField.TWO_DIMENSIONAL_ARRAY_FLOAT, x, true);
				}
			}
			
			
			
			else if (p.getType().equals(double[][].class) || p.getType().equals(Double[][].class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
				// zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

				final String txtDefaultValue = "[{0d, -1D}, {-2.3d, 3.4D}]";
				final String txtErrorInfo = txt2ArrayFormatErrorAll1 + " " + p.getType().getSimpleName() + " "
						+ txtArrayFormatErrorAll2 + txtDoubleInterval;

				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadání hodnot:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, txtDefaultValue, pnlValues);

					// Přidání na textové pole reakci na stisknutí klávesy, na otestování hodnot pro
					// jednorozměrné pole typu String:
					addKeyListenerToTxtField(txtField, REG_EX_FOR_TWO_DIM_ARRAY_DOUBLE, txtErrorInfo,
							DataTypeOfJTextField.TWO_DIMENSIONAL_ARRAY_DOUBLE, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();

					
					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, txtDefaultValue));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu:
						fieldValues.addAll(getModelForCmb(availableFieldsList, p.getType()));						
					}

					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_TWO_DIM_ARRAY_DOUBLE, txtErrorInfo,
							DataTypeOfJTextField.TWO_DIMENSIONAL_ARRAY_DOUBLE, x, true);
				}
			}
			
			
			
			else if (p.getType().equals(char[][].class) || p.getType().equals(Character[][].class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
				// zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

				final String txtDefaultValue = "[{';', '-'}, {'X', '0'}]";
				final String txtErrorInfo = txt2ArrayFormatErrorAll1 + " " + p.getType().getSimpleName() + " "
						+ txtArrayCharacterFormatError2;

				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadání hodnot:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, txtDefaultValue, pnlValues);

					// Přidání na textové pole reakci na stisknutí klávesy, na otestování hodnot pro
					// jednorozměrné pole typu String:
					addKeyListenerToTxtField(txtField, REG_EX_FOR_TWO_DIM_ARRAY_CHARACTER, txtErrorInfo,
							DataTypeOfJTextField.TWO_DIMENSIONAL_ARRAY_CHARACTER, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();
					
					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, txtDefaultValue));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu:
						fieldValues.addAll(getModelForCmb(availableFieldsList, p.getType()));						
					}
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_TWO_DIM_ARRAY_CHARACTER,
							txtErrorInfo, DataTypeOfJTextField.TWO_DIMENSIONAL_ARRAY_CHARACTER, x, true);
				}
			}
			
			
			
			else if (p.getType().equals(boolean[][].class) || p.getType().equals(Boolean[][].class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
				// zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

				final String txtDefaultValue = "[{true, false}, {true, false}]";
				final String txtErrorInfo = txt2ArrayFormatErrorAll1 + " " + p.getType().getSimpleName() + " "
						+ txtArrayBooleanFormatError2;

				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadání hodnot:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, txtDefaultValue, pnlValues);

					// Přidání na textové pole reakci na stisknutí klávesy, na otestování hodnot pro
					// jednorozměrné pole typu String:
					addKeyListenerToTxtField(txtField, REG_EX_FOR_TWO_DIM_ARRAY_BOOLEAN, txtErrorInfo,
							DataTypeOfJTextField.TWO_DIMENSIONAL_ARRAY_BOOLEAN, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();
					
					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, txtDefaultValue));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu:
						fieldValues.addAll(getModelForCmb(availableFieldsList, p.getType()));						
					}
					
					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_TWO_DIM_ARRAY_BOOLEAN, txtErrorInfo,
							DataTypeOfJTextField.TWO_DIMENSIONAL_ARRAY_BOOLEAN, x, true);
				}
			}
			
			
			
			else if (p.getType().equals(String[][].class)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
				// zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

				final String txtDefaultValue = "[{\"" + txtDefaultTextForStringListAndArray1 + "\", \""
						+ txtDefaultTextForStringListAndArray2 + "\"}, {\"" + txtDefaultTextForStringListAndArray2
						+ "\", \"" + txtDefaultTextForStringListAndArray2 + "\"}]";
				final String txtErrorInfo = txt2ArrayFormatErrorAll1 + " " + p.getType().getSimpleName() + " "
						+ txtArrayStringFormatError2;

				
				if (!makeVariablesAvailable && !makeMethodsAvailable) {
					// Přidám textové pole pro zadání hodnot:
					final JTextField txtField = createAndAddTextFieldForValue(gbc, x, y2, txtDefaultValue, pnlValues);

					// Přidání na textové pole reakci na stisknutí klávesy, na otestování hodnot pro
					// jednorozměrné pole typu String:
					addKeyListenerToTxtField(txtField, REG_EX_FOR_TWO_DIM_ARRAY_STRING, txtErrorInfo,
							DataTypeOfJTextField.TWO_DIMENSIONAL_ARRAY_STRING, x);
				}

				else {
					/*
					 * Vytvořím si list, resp. model, který bude obsahovat na začátku první položku,
					 * kterou může zadat uživatel a pozdějí se do něj přidají veškeré nalezené
					 * proměnné, které jsou stejného datového typu.
					 */
					final List<ParameterValue> fieldValues = new ArrayList<>();

					/*
					 * List, do kterého budu vkládat veškeré nalezené metody - pokud je povoleno, že
					 * je možné zavolat i metody.
					 */
					final List<MethodValue> methodValues = new ArrayList<>();

                    // Přidání textového pole pro "ruční" zadání hodnoty:
                    fieldValues.add(new ParameterValue(KindOfVariable.TEXT_VARIABLE, txtDefaultValue));

					if (makeVariablesAvailable) {
						// Přidám do modelu nalezené položky stejného datového typu:
						fieldValues.addAll(getModelForCmb(availableFieldsList, p.getType()));						
					}

					/*
					 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
					 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
					 * předala do parametru metody.
					 */
					if (makeMethodsAvailable) {
						tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType());

						/*
						 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
						 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
						 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
						 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
						 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
						 * setr - akorát s get nebo is na začátku:
						 */
						if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                            methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));

						else
							methodValues.addAll(tempListForOthersMethods);
					}
						

					final JComboBox<CmbValueAncestor> cmb = createAndAddCmbForValue(gbc, x, y2, pnlValues, fieldValues,
							methodValues, true);

					addKeyListenerToCmb(cmb, fieldValues, methodValues, REG_EX_FOR_TWO_DIM_ARRAY_STRING, txtErrorInfo,
							DataTypeOfJTextField.TWO_DIMENSIONAL_ARRAY_STRING, x, true);
				}
			}
















            /*
             * Nyní otestuji, zda se jedná o parametr typu nějaké třídy z diagramu tříd
             *
             * Note:
             * K části podmínky, kde se testují, jestli nejsou listy tempListForOthersFields a
             * tempListForOthersMethods null, tak tam musí být "and / &&", aby se naplnily oba listy, resp.
             * zavolaly metody pro jejich naplnění,
             * protože, kdyby tam bylo "nebo / || ", tak by se mohla splnit vždy jen jedna část podmínky.
             * Tedy naplnit pouze jeden z listů, takže by se ve výsledku mohli zobrazit například pouze
             * proměnné a ne metody, protože by po naplnění listu s proměnnými byla podmínka splněna.
             *
             * Dále by bylo možné doplnit testování, zda nejsou tyto listy s proměnnými a metodami prázdné, ale tyto
             * hodnoty již nejsou nezbytné.
             */
			else if (isClassFromCd(p.getType())
					&& !(correspondingTypesList = getParameterClassInstance(p, classOrInstanceList)).isEmpty()
							&& (tempListForOthersFields = getModelForCmb(availableFieldsList, p.getType())) != null
							&& (tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference,
									p.getType())) != null) {
				/*
				 * V tomto případě jsou na výběr pouze možné třídy z diagramu tříd nebo
				 * vytvoření nové instance třídy z diagramu tříd, tak jako tak jsou veškeré
				 * možnost pokryty v komponentě JComboBox (viz níže), tak nebudu dávat tetové
				 * pole - proč?
				 */
				
				
				
				/*
				 * Proměnná, do které si vložím výsledný model pro komponentu JComboBox pro
				 * označení položek:
				 */
				final CmbValueAncestor[] model;
				
				/*
				 * Zde otestuji, zda se mají v příslušném comboBoxu zobrazit i možnosti pro
				 * vytvoření nové instance příslušné třídy, pokud ano, tak jen předám získaný
				 * list do modelu cmb, ale pokud se nemají zobrazovat možnosti pro vytvoření
				 * nové instance, tj. mají se zobrazit jen ty aktuální, tak musím vyfiltrovat ty
				 * položky, které slouží právě pro vytvoření té nové instance.
				 */
				if (showMakeNewInstanceOption)
					model = correspondingTypesList.toArray(new ClassOrInstanceForParameter[] {});

				else
					// Zde vynechám položky pro vytvoření nové instance.
					model = correspondingTypesList.stream().filter(c -> c.getClazz() == null)
							.collect(Collectors.toList()).toArray(new ClassOrInstanceForParameter[] {});
				
				
				
				
				
				/*
				 * Vytvořím si model hodnot pro přísulšný JComboBox, ale toto je takový "mezi
				 * krok", protože chci, aby uživatel měl možnost předal null hodnotu místo
				 * konkrétní instance, takže váše v jednorozměrné poli 'model' mám načtené
				 * veškeré možné hodnoty a na konec přidám instancí třídy NullValue, která
				 * značí, že se má předat null hodnota.
				 */
				final DefaultComboBoxModel<CmbValueAncestor> tempModel = new DefaultComboBoxModel<>(model);
				
				

				if (makeVariablesAvailable)
					// Přidám do modelu nalezené položky stejného datového typu:
					tempListForOthersFields.forEach(tempModel::addElement);

				
				
				
				
				
				/*
				 * V případě, že se mají vyhledávat metody zkusím vyhledat veškeré možné metody,
				 * které by mělo být možné zavolat nad příslušnou třídou, aby se její hodnota
				 * předala do parametru metody.
				 */
				
				/*
				 * Tato zakomentovaná podmínka by stačila, aby se přidali veškeré nalezené
				 * metody do výběru cmb, aby je uživatel mohl označit a jejich návratová hodnota
				 * se předala do příslušného parametru.
				 */
//				if (makeMethodsAvailable)
//					tempListForOthersMethods.forEach(m -> tempModel.addElement(m));
				
				
				
				/*
				 * Nově jsem zde doplnil (na požadavek vedoucího bakalářské práce), aby bylo
				 * možné v nastavení pro tuto aplikaci nastavit, zda se mají zobrazovat getry na
				 * instanci sebe sama, pokud se jedná o zavolání setru na instanci sebe sama.
				 * 
				 * Tzn., že pokud uživatel zvolil metodu coby setr na hodnotu proměnné, která
				 * značí referenci / atribut třídy na sebe sama tak se nebude zobrazovat getr té
				 * proměnné, která značí instanci třídy na sebe sama.
				 */				
				
				// Otestuji, zda se mají zobrazovat metody:
				if (makeMethodsAvailable) {
					/*
					 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
					 * příslušným názvem metody - mimo jiné). se jedná o setr na nějakou proměnnou v
					 * té instanci, pokud jsou tyto podmínky splněny, pak se vyfiltrují metody tak,
					 * aby nebyly v setru (který se aktuálně volá) nabídky pro getr, tj. metoda se
					 * stejným název jako setr - akorát s get nebo is na začátku:
					 */
					if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
						filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p)
								.forEach(tempModel::addElement);

					else
						tempListForOthersMethods.forEach(tempModel::addElement);
				}
					

				
				
				
				
				
				// Null hodnotu přidám pokaždé:
				tempModel.addElement(new NullValue());
				
				
				
				
				
				/*
				 * Zde v případě, že nebyla nalezena žádná položka a k tomu může dojít pouze v
				 * případě, že nebyla nalezena žádná instance příslušné třídy v diagramu
				 * instancí a zároveň není povolena možnost pro vytvoření nové instance, pak
				 * musí do dialogu vypsat hlášení, že příslušnou metodu nebo konstruktor nepůjde
				 * zavolat, protože není instance pro předání a zároeň nejsou povoleny null
				 * hodnota, takže příslušný konstruktor nebo metoda nepůjde zavolat.
				 */
				if (tempModel.getSize() == 0) {
					if (isInstance)
						addInfoUnsupportedParameter(isInstance, p, txtInstanceNotFoundConstructorCantCall);

					else
						addInfoUnsupportedParameter(isInstance, p, txtInstanceNotFoundMethodCantCall);
					
					continue;
				}
					
				
				
				
				/*
				 * Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
				 * zadání hodnoty.
				 * 
				 * Tento label je třeba přidat až zde, protože kdybych to udělel před podmínkou
				 * výše a tou by to neprošlo, pak by byl zobrazen pouze label, ale k němu by
				 * nebyl cmb.
				 */
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

				
				
				
				/*
				 * Zde si vytvořím instanci příslušné komponenty JComboBox a předám ji výše
				 * získaný model s položkami:
				 */
				final JComboBox<CmbValueAncestor> cmbClassOrInstances = new JComboBox<>(tempModel);
				
				/*
				 * Toto je tak trochu zbytečné, ale přidám událost na stisknutí klávesy Escape
				 * pro zavření dialogu (už to tak mám u komponent JTextField, tak to dám i sem).
				 * 
				 * Dále klávesa Enter pro potvrzení zavolání metody nebo konstruktoru.
				 */
				cmbClassOrInstances.addKeyListener(new KeyAdapter() {

					@Override
					public void keyPressed(KeyEvent e) {
						if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
							variable = false;
							dispose();
						}

						else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
							/*
							 * Pokud se jedná o zavolání metody, pak otestuji zadaná data a případně zkusím
							 * zavolat metodu:
							 */
							if (methodParametersForm != null)
								methodParametersForm.testData();

							/*
							 * Pokud se jedná o zavolání konstruktoru, tak otestuji zadaná data a případně
							 * zkusím zavolat konstruktor:
							 */
							else if (newInstanceForm != null)
								newInstanceForm.testData();
						}
					}
				});
				
				
				listOfParametersValues.add(cmbClassOrInstances);
				
				setGbc(gbc, x, y2, cmbClassOrInstances, pnlValues);
			}
			
			
			
			
			
			
			
			
			
			
			
			/*
			 * Pokud se jedná o parametr Object, pak budou na výběr pouze nalezené proměnné
			 * z tříd a editoru příkazů a nesmí být null. V cmb již nepůjde editovat první
			 * položka.
			 */
			else if (p.getType().equals(Object.class) && (makeVariablesAvailable || makeMethodsAvailable)) {
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
				// zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

				/*
				 * Vytvořím si model, který bude obsahovat pouze položky z tříd a editoru
				 * příkazů a ty položky / proměnné jsou naplněné, tj. nejsou null.
				 */
				final List<ParameterValue> fieldValues = new ArrayList<>();
				
				/*
				 * list s metodami, které je možné zavolat pro předání vrácené hodnoty do
				 * příslušného parametru.
				 * 
				 * jedná se o metody,které vracejí libolvolný object, tj. nejsou void.
				 */
				final List<MethodValue> methodValues = new ArrayList<>();
				
				if (makeVariablesAvailable)
					fieldValues.addAll(getModelWithFilledValues(availableFieldsList));
				
				
				if (makeMethodsAvailable) {
					tempListForOthersMethods = getAllAvailableMethodsForMethodWithSomeReturnType(clazz, reference);

					/*
					 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
					 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
					 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
					 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
					 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
					 * setr - akorát s get nebo is na začátku:
					 */
					if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
                        methodValues.addAll(filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p));
					else
						methodValues.addAll(tempListForOthersMethods);
				}

					
				
				// Vytvořím a přidám cmb do dialogu tak, aby uživatel mohl pouze označit položku
				// pro předání.
				createAndAddCmbForObjectValue(gbc, x, y2, pnlValues, fieldValues, methodValues, true);
			}


















            /*
             * Zde pouze otestuji, že pokud se jedná o mapu, tak do tohoto parametru lze
             * předat veškeré implementace mapy, jako je například HashMapa, LinkedHashMap,
             * HashTable, ...
             *
             * Ale pokud se jedná o konkrétní implementaci mapy (HashMap, HashTable, ...),
             * tak je to vyřešeno výše, kde se hlídají konkrétní datové typy proměnných.
             *
             *
             * Note:
             * Pokud bude vyžadovat parametr metody nebo konstruktoru mapu nějakého datového
             * typu, například <String, String>. Pak se najdou proměnné z tříd a instancí
             * příslušné mapy, ale už se netestují datové typy, takže pokud bude třeba
             * metoda vyžovat ten String, jak je uvedeno výše, ale v nějaké proměnné v
             * nějaké třídě bude třeba mapa typu Double, Integer, pak se do té metody i tak
             * předá.
             *
             * Note:
             * K části podmínky, kde se testují, jestli nejsou listy tempListForOthersFields a
             * tempListForOthersMethods null, tak tam musí být "and / &&", aby se naplnily oba listy, resp.
             * zavolaly metody pro jejich naplnění,
             * protože, kdyby tam bylo "nebo / || ", tak by se mohla splnit vždy jen jedna část podmínky.
             * Tedy naplnit pouze jeden z listů, takže by se ve výsledku mohli zobrazit například pouze
             * proměnné a ne metody, protože by po naplnění listu s proměnnými byla podmínka splněna.
             */
			else if (p.getType().equals(Map.class)
					&& ((tempListForOthersFields = getFieldsWithInstancesMap(availableFieldsList)) != null
							&& (tempListForOthersMethods = getMetodsWithMapReturnType(clazz, reference)) != null)
					&& (!tempListForOthersFields.isEmpty() || !tempListForOthersMethods.isEmpty())
					&& (makeVariablesAvailable || makeMethodsAvailable)) {

				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
				// zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

				/*
				 * V případě, že se nemají přidávat proměnné, pak vymažu list s proměnnými, aby se do JComboBoxu žádná proměná nepřidala.
				 */
				if (!makeVariablesAvailable)
					tempListForOthersFields.clear();

				/*
				 * Zde se mají přidat nabídky proměnných s mapou, tak vyfiltruji pouze ty typy
				 * mapy, kde se shodují datové typy pro klíč a hodnotu v mapě:
				 */
				else
					tempListForOthersFields = filterFieldsWithSpecificMap(p);
				

				/*
				 * V případě, že se nemají přidávat metody, pak vymažu list s metodami, aby se do JComboBoxu žádná metoda nepřidala.
				 */
				if (!makeMethodsAvailable)
					tempListForOthersMethods.clear();

				/*
				 * Zde se mají přidávat položky s metodami, tak vyfiltruji pouze ty položky /
				 * metody, kde se shodují datové typy map.
				 */
				else {
					tempListForOthersMethods = filterMethodsWithSpecificMap(p);

					/*
					 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
					 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
					 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
					 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
					 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
					 * setr - akorát s get nebo is na začátku:
					 */
					if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
						tempListForOthersMethods = filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p);
				}

					
				
				
				// Vytvořím a přidám cmb do dialogu tak, aby uživatel mohl pouze označit položku
				// pro předání.
				createAndAddCmbForObjectValue(gbc, x, y2, pnlValues, tempListForOthersFields, tempListForOthersMethods,
						true);
			}

















            /*
             * Tato verze dokaze rozpoznat treba List<JDialog>, nebo konkretní instance
             * mapy, jako je treba HashMap apod. Ale vždy hleda proměnné, které jsou
             * naprosto stejného datového typu, jako je parametr příslušné metody nebo
             * konstruktoru.
             *
             * Note:
             * K části podmínky, kde se testují, jestli nejsou listy tempListForOthersFields a
             * tempListForOthersMethods null, tak tam musí být "and / &&", aby se naplnily oba listy, resp.
             * zavolaly metody pro jejich naplnění,
             * protože, kdyby tam bylo "nebo / || ", tak by se mohla splnit vždy jen jedna část podmínky.
             * Tedy naplnit pouze jeden z listů, takže by se ve výsledku mohli zobrazit například pouze
             * proměnné a ne metody, protože by po naplnění listu s proměnnými byla podmínka splněna.
             */
			else if (((tempListForOthersFields = getFieldsWithSameDataType(availableFieldsList, p.getType())) != null
					&& (tempListForOthersMethods = getAvailableMethodsForMethod(clazz, reference, p.getType())) != null)
					&& (!tempListForOthersFields.isEmpty() || !tempListForOthersMethods.isEmpty())
					&& (makeVariablesAvailable || makeMethodsAvailable)) {
				
				// Přídám popisek pro hodnotu, která se má zadat - vlevo před políčkem pro
				// zadání hodnoty:
				createAndAddInfoValueLabel(gbc, ++x, y1, p, pnlValues);

				/*
				 * Výše v podmínce jsem si načetly proměnné a metody, tak zde otestuji, zda se
				 * mají proměnné / atributy přidávat, pokud ne, pak přísulšný list vymažu.
				 */
				if (!makeVariablesAvailable)
					tempListForOthersFields.clear();

				/*
				 * Výše v podmínce jsem si načetly proměnné a metody, tak zde otestuji, zda se
				 * mají metody přidávat, pokud ne, pak přísulšný list vymažu.
				 */
				if (!makeMethodsAvailable)
					tempListForOthersMethods.clear();

				/*
				 * Zde otestuji, zda se mají zobrazovat getry na příslušnou proměnnou (getr s
				 * příslušným názvem metody - mimo jiné). Pokud se volá setr na nějakou
				 * proměnnou v té instanci a nemají se zobrazovat getry - testováno dle
				 * příslušných názvů metod, pak se vyfiltrují metody tak, aby nebyly v setru
				 * (který se aktuálně volá) nabídky pro getr, tj. metoda se stejným název jako
				 * setr - akorát s get nebo is na začátku:
				 */
				else if (!GraphInstance.isShowGetrInSetrMethodToInstance() && methodNameOnly != null)
					tempListForOthersMethods = filterMethodWithoutGetrs(clazz, methodNameOnly, reference, p);
				
				
				// Vytvořím a přidám cmb do dialogu tak, aby uživatel mohl pouze označit položku
				// pro předání.
				createAndAddCmbForObjectValue(gbc, x, y2, pnlValues, tempListForOthersFields, tempListForOthersMethods,
						true);
			}
			
			
			
			
			
			
			
			/*
			 * Zde v části else se jedná o nepodporovaný parametr, resp. nepodporovaný
			 * datový typ parametr, tak do okna s chybami vypíšu hlášení, že příslušnou
			 * metodu nebo konstruktor nebude možné zavolat.
			 */
			else
				addInfoUnsupportedParameter(isInstance, p, null);
			

			
			
			/*
			 * - pozdeji rozsirit testovani i na to , aby se zjistito, zda se jedna o mapu,
			 * atd.
			 */
		}
		return pnlValues;
	}









	
	
	
	
	
	
	
	/**
	 * Metoda, která vyfiltruje pouze ty položky - proměnné, které jsou typu mapa a
	 * jsou konkrétních datových typů, jako mapa v parametru p, ale pokud se jedná o
	 * generické datové typy v mapě v p, pak se může jednat o libovolný datový typ
	 * (objektový).
	 * 
	 * @param parameter
	 *            - parametr metody nebo konstruktoru, který je typu Map - nebo
	 *            nějaké její implementace.
	 * 
	 * @return list, který bude obsahovat položky, kterě jsou stejné datového typu
	 *         jako v p, nebo libovolného datového typu, pokud sejedná o generické
	 *         datové typy.
	 */
	private List<ParameterValue> filterFieldsWithSpecificMap(final Parameter parameter) {
		/*
		 * Zde je třeba otestovat, zda je parametr vůbec typu mapa, ale ve smyslu, že ta
		 * mapa má parametry, takže pokud ten typ parametru není instance
		 * ParametrizedType, pak se jedná o mapu bez parametrů, a v listu
		 * 'tempListForOthersFields' již mám načtené pouze příslušné mapy, tak je mohu
		 * vrátit všechny.
		 */

        // Pokud se nejedná o mapu, je možné skončit:
		if (!MapHelper.isParameterTypeOfMap(parameter))
			return tempListForOthersFields;

		// Pokud se jedná o Mapu bez definovaných parametrů, mohou se vrátit veškeré nalezené Mapy:
		if (!ReflectionHelper.isParameterizedType(parameter.getParameterizedType()))
			return tempListForOthersFields;

		// Datový typ klíče Mapy:
		final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(parameter);
        // Datový typ hodnoty Mapy:
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(parameter);



        /*
		 * Vyfiltruji pouze ty mapy, u kterých se shodují datové typy klíče a hodnoty.
		 */
		return tempListForOthersFields.stream().filter(f -> {
			/*
			 * Tento případ může nastat například, když se zavolá metoda, která má parametr
			 * Map<X, Y>, a v této aktuálně iterované kolekci tempListForOthersFields se
			 * hledají položky s příslušným typem mapy, ale pokud tato položka / mapa není
			 * parametrizovaná, pak by nastala výjimka, proto musím testovat zda je i tato
			 * proměnná / mapa parametrizovaná, pokud ne, pak vrátím false, protože ta
			 * metoda chce mapu s parametry. Zde by ale šlo ještě doplnil do podmínky, zda
			 * jsou ty paramety v mapě u metody oba generické (otazníky) a pokud ano, pak by
			 * se dalo vrátit true, aby se ta proměnná vybrala.
			 */

            // Zda li je položka / proměnná typu Map (s nebo bez parametrů):
			if (!MapHelper.isFieldTypeOfMap(f.getField()))
				return false;

			/*
			 * Jestliže iterovaná proměnná je Mapa bez definovaných parametrů, tak se může tato proměnná přeskočit.
			 * Protože parameter je Mapa s definovanými parametry, které se musí shodovat.
			 */
			if (!ReflectionHelper.isParameterizedType(f.getField().getGenericType()))
				return false;

			final Object keyTypeOfMapField = MapHelper.getKeyTypeOfMap(f.getField());
            final Object valueTypeOfMapField = MapHelper.getValueTypeOfMap(f.getField());

            return ReflectionHelper.compareDataType(keyTypeOfMap, keyTypeOfMapField) && ReflectionHelper.compareDataType(valueTypeOfMap, valueTypeOfMapField);

        }).collect(Collectors.toList());
	}
	
	
	
	



	
	
	
	
	
	
	/**
	 * Metoda, která vyfiltruje pouze ty položky - metody, které jsou typu mapa a
	 * jsou konkrétních datových typů, jako mapa v parametru p, ale pokud se jedná o
	 * generické datové typy v mapě v p, pak se může jednat o libovolný datový typ
	 * (objektový).
	 * 
	 * @param parameter
	 *            - parametr metody nebo konstruktoru, který je typu Map - nebo
	 *            nějaké její implementace.
	 * 
	 * @return list, který bude obsahovat položky, kterě jsou stejné datového typu
	 *         jako v p, nebo libovolného datového typu, pokud sejedná o generické
	 *         datové typy.
	 */
	private List<MethodValue> filterMethodsWithSpecificMap(final Parameter parameter) {
		/*
		 * Zde je třeba otestovat, zda je parametr vůbec typu mapa, ale ve smyslu, že ta
		 * mapa má parametry, takže pokud ten typ parametru není instance
		 * ParametrizedType, pak se jedná o mapu bez parametrů, a v listu
		 * 'tempListForOthersMethods' již mám načtené pouze příslušné mapy (metody,
		 * které vracejí mapu), tak je mohu vrátit všechny.
		 */

        // Pokud se nejedná o mapu, je možné skončit:
        if (!MapHelper.isParameterTypeOfMap(parameter))
			return tempListForOthersMethods;

		// Pokud se jedná o Mapu bez definovaných parametrů, mohou se vrátit veškeré nalezené Mapy:
		if (!ReflectionHelper.isParameterizedType(parameter.getParameterizedType()))
			return tempListForOthersMethods;

        // Datový typ klíče Mapy:
        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(parameter);
        // Datový typ hodnoty Mapy:
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(parameter);


        /*
		 * Vyfiltruji pouze ty mapy, u kterých se shodují datové typy klíče a hodnoty.
		 */
		return tempListForOthersMethods.stream().filter(m -> {
			/*
			 * Tento případ může nastat například, když se zavolá metoda, která má parametr
			 * mapa<X, Y>, a v této aktuálně iterované kolekci tempListForOthersMethods se
			 * hledají metody, které vrací příslušný typ mapy, ale pokud tato metoda vrací
			 * mapu, která není parametrizovaná, pak by nastala výjimka, proto musím
			 * testovat zda i tato metoda vrací parametrizovanou mapu, pokud ne, pak vrátím
			 * false, protože ta metoda chce mapu s parametry. Zde by ale šlo ještě doplnil
			 * do podmínky, zda jsou ty paramety v mapě u metody oba generické (otazníky) a
			 * pokud ano, pak by se dalo vrátit true, aby se ta metoda vybrala.
			 */

            // Zda li metoda vrací datový typ Map:
            if (!MapHelper.methodReturnsMap(m.getMethod()))
                return false;

			/*
			 * Jestliže je návratový typ iterované metody Mapa bez definovaných parametrů, tak se může tato metoda
			 * přeskočit. Protože parameter je Mapa s definovanými parametry, které se musí shodovat.
			 */
			if (!ReflectionHelper.isParameterizedType(m.getMethod().getGenericReturnType()))
				return false;

            final Object keyTypeOfMapMethod = MapHelper.getKeyTypeOfMap(m.getMethod());
            final Object valueTypeOfMapMethod = MapHelper.getValueTypeOfMap(m.getMethod());

            return ReflectionHelper.compareDataType(keyTypeOfMap, keyTypeOfMapMethod) && ReflectionHelper.compareDataType(valueTypeOfMap, valueTypeOfMapMethod);

        }).collect(Collectors.toList());
	}









	/**
	 * Metoda, která přidá do listu pro hlášení s chybami hlášku, že nelze zavolat příslušný konstruktor nebo metodu,
	 * protože se jedná o parametr, který aplikace neumožňuje předat (například) nebo generický typ v listu či kolekci
	 * apod..
	 *
	 * @param isInstance
	 *         - logická proměnná, zda se jedná o zavolání konstruktoru nebo ne (nebo metody), true, pokud se jedná o
	 *         konstruktor, jinak false, pokud se jedná o zavolání metody.
	 * @param p
	 *         - parametr, který není možné "předat" (jedná se o nepodporovaný typ apod).
	 * @param replacementText-
	 *         text, který se má vložit do příslušné proměnné pro vložení do chybových výpisů s informací o tom, že
	 *         metoda nebo konstruktor nelze zavolat tato proměnná obsahuje text, který se má vložit do výsledného
	 *         výpisu bez jakých koli úprav.
	 */
	private void addInfoUnsupportedParameter(final boolean isInstance, final Parameter p,
											 final String replacementText) {
		// Zde se jedná o nepodporovaný typ, tak naplním proměnnou textem pro vložení do okna s chybovým upozorněmím:
		
		// V následující podmínce ještě není v testované proměnné žádná hodnota, takže se jedná o první parametr, jehož datový typ
		// není podporován, takže se jedná jen o jeden parametr (zatím):
		if (errorTextForBadTypeOfParameter == null) {
			if (isInstance) {
				if (replacementText != null)
					errorTextForBadTypeOfParameter = replacementText;

				else {
                    // Získání parametru v textu:
                    final String parameterInText = ParameterToText.getParameterInText(p, false);

                    errorTextForBadTypeOfParameter = txtDataTypeOfParametr + ": " + parameterInText + " " + txtIsNotSupportedForConstructor;
				}
			}

			
			else {
				if (replacementText != null)
					errorTextForBadTypeOfParameter = replacementText;

				else {
				    // Získání parametru v textu:
                    final String parameterInText = ParameterToText.getParameterInText(p, false);

                    errorTextForBadTypeOfParameter =
                            txtDataTypeOfParametr + ": " + parameterInText + " " + txtIsNotSupportedForMethod;
				}
			}
		}
		
		// Zde je daná proměnné již jednou naplněna a tato celá větev else se testuje minimálně podruhé (záleží na počtu parametrů, jejíž datové typy
		// nejsou podporovány), a když se testuje alespoň dvakrát a vícekrát, jedná se o více parametrů, které nejsou podporovány, tak upřesním iformaci:
		else {
			if (isInstance) {
				if (replacementText != null)
					errorTextForBadTypeOfParameter = replacementText;

				// Zde se jedná o paarmetry pro instanci třídy:
				else
					errorTextForBadTypeOfParameter = txtConstructorCannotCall;
			}

			// Zde se jedná o parametr pro metodu:
			else {
				if (replacementText != null)
					errorTextForBadTypeOfParameter = replacementText;

				else
					errorTextForBadTypeOfParameter = txtMethodCannotCall;
			}
		}
	}
	

	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro vyfiltrování proměnných, které "reprezentují" list a
	 * ten list je požadovaného datového typu, například List<Byte> apod. - v
	 * proměnné clazz.
	 * 
	 * @param availableValues
	 *            - získané položky z proměnných z tříd v diagramu tříd a instancí a
	 *            editoru příkazů.
	 * 
	 * @param clazz
	 *            - datový typ listu. Tento datový typ musí obsahovat položky v
	 *            availableValues. Pouze položky typu List typ clazz se vyfiltrují a
	 *            vrátí.
	 * 
	 * @return vyfiltrované položky v listu availableValues, které jsou typu
	 *         List<clazz>.
	 */
	private static List<ParameterValue> getVarsWithDataType(final List<ParameterValue> availableValues,
			final Class<?> clazz) {
		return availableValues.stream().filter(v -> {
			if (v.getKindOfVariable().equals(KindOfVariable.COMMAND_EDITOR_VARIABLE)) {
                return v.getObjEditorVariable() != null && v.getDataType().equals(clazz);
			}

			else if (v.getKindOfVariable().equals(KindOfVariable.FIELD_VARIABLE)) {
				/*
				 * Zde by se již mělo jednat o položky pouze typu List, ale pro případ, že by tomu tak nebylo, je zde
				 * doplněno testování, kdy se zjistí, zda se opravdu jedná o proměnnou typu List nebo ne.
				 */
				if (v.getDataTypeOfList() != null)
					/*
					 * clazz je datový typ Listu (například Byte, Short, ...) a má se zjistit, zda datový typ (v
					 * položce "v") výše iterovaného Listu je stejný typ jako clazz. Resp. lze hodnoty typu Listu v
					 * proměnné "v" vložit do Listu typu clazz.
					 */
					return ReflectionHelper.compareDataType(clazz, v.getDataTypeOfList().getDataType());


				/*
				 * (Tato část by nikdy neměla nastat.)
				 *
				 * V této části kódu se výše neuložila proměnné pro List, tak se zde zjistí datový typ Listu a
				 * porovnají se s clazz (jestli je možné do listu typu clazz vkládat položky typu listType).
				 */
				final Object listType = ReflectionHelper.getDataTypeOfList(v.getField()).getDataType();

				/*
				 * clazz je datový typ Listu (například Byte, Short, ...) a má se zjistit, zda datový typ (listType)
				 * výše iterovaného Listu je stejný typ jako clazz. Resp. lze hodnoty typu listType vložit do Listu
				 * typu clazz.
				 */
				return ReflectionHelper.compareDataType(clazz, listType);
			}
			return false;
		}).collect(Collectors.toList());
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží k vyfiltrování hodnot (proměnných / atributů), které
	 * byly získány z nějaké třídy a jedná se o proměnné typu clazz, ale ta proměnná
	 * clazz obsahuje jeden z typů kolekce, například arrayList, linkedList, Vector,
	 * Stack apod. A zároveň tato kolekce je datového typu dataTypeOfCollection, tj.
	 * jedná se například o kolekce typu String, Integer apod.
	 * 
	 * Tato metoda se použivá například, když je v parametru metody nebo
	 * konstruktoru, který chce uživatel zavolat datový typ kolekce ne list, ale
	 * konkrétní typ kolekce (například) Arraylist, LinkedList apod. A tato kolekce
	 * je datového typu String, nebo Integer apod. (v proměnné
	 * dataTypeOfCollection).
	 * 
	 * @param availableValues
	 *            - veškeré načtené proměnné z třídy a editoru příkazů
	 * 
	 * @param clazz
	 *            - jeden z výše uvedených typů kolekce.
	 * 
	 * @param dataTypeOfCollection
	 *            - datový typ hodnot, které lze vkládat do příslušné kolekce,
	 *            například String, Integer apod.
	 * 
	 * @return list, který bude obsahovat vyfiltrované položky, které jsou jedného z
	 *         typů kolekce. A zároven ty kolekce je typu dataTypeOfCollection.
	 */
	private static List<ParameterValue> getFieldsWithDataTypes(final List<ParameterValue> availableValues,
			final Class<?> clazz, final Object dataTypeOfCollection) {
        return availableValues.stream().filter(v -> {
            if (v.getKindOfVariable().equals(KindOfVariable.FIELD_VARIABLE) && v.getField().getType().equals(clazz)) {
                /*
                 * Získám si datový typ listu:
                 */
                final Object temp = ReflectionHelper.getDataTypeOfList(v.getField()).getDataType();

                /*
                 * dataTypeOfCollection je datový typ Listu v parametru metody, kterou uživatel chce zavolat a má se zjistit, zda do tohoto parametru lze vložit hodnotu, která je typu temp. Temp je datový typ Listu - List je aktuálně iterovaná proměnná.
                 */
                return ReflectionHelper.compareDataType(dataTypeOfCollection, temp);
            }

			return false;
		}).collect(Collectors.toList());
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda je parametr metody či konstruktoru ten typ
	 * parametru třídy z diagramu tříd pokud ano, tak se prohledá kolekci v
	 * parametru, kde jsou uloženy všechy uživatelem vytvořené instance a
	 * zkompilované třídy, a pokud bude parametr metody či konstruktoru odpovídat,
	 * tak danou hodnotu coby instance třídy ClassOrInstanceForParameter vloži do
	 * kolekce s názvem reference correspondingTypes, která se vrátí, budou v ní moci
	 * být tedy uložené vždy načtená třída po kompilaci a veškeré instance dané
	 * třídy - již vytvořené uživatelem
	 * 
	 * @param p
	 *            - parametr metody či konstruktoru, jehož datový typ testuje tato
	 *            metoda, zda se jedná o nějakou třídu z diagramu tříd
	 * 
	 * @param classOrInstanceList
	 *            - list, který obsahuje načtené třídy po kompilaci a všechny
	 *            vytvořené instance
	 * 
	 * @return list, který může obsahovat zkompilovanou třídu a všechny její
	 *         instance, které odpovídají parametru p
	 */
	private static List<ClassOrInstanceForParameter> getParameterClassInstance(final Parameter p,
			final List<ClassOrInstanceForParameter> classOrInstanceList) {
		final List<ClassOrInstanceForParameter> correspondingTypes = new ArrayList<>(); 
		
		for (final ClassOrInstanceForParameter c : classOrInstanceList) {
			if (c.getClazz() != null) {
				// Nejprve zde musím otestovat, zda se nejedná o abstraktní třídu nebo rozhraní - z těch se nevytváří instance:

				// TAKTO TO NEJDE OTESTOVAT ???, ALE JSOU NEJKTERE STEJNE ???
				//					if (p.getType().equals(c.getClazz()))
				if (!c.getClazz().isInterface() && !Modifier.isAbstract(c.getClazz().getModifiers()) && p.getType()
						.toString().equals(c.getClazz().toString()))
					correspondingTypes.add(c);
			}
			
			else {
				// TAKTO TO NEJDE OTESTOVAT ???
//				if (p.getType().equals(c.getObjInstance().getClass()))
				if (p.getType().toString().equals(c.getObjInstance().getClass().toString()))
					correspondingTypes.add(c);				
			}
		}
		
		return correspondingTypes;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Texty pro chybové hlášky do Jlabelů, které se zobrazí pri nedodežení formátu
	 * hodnoty, kterou je třeb zadat coby parametr je třeba zadat již při jejich
	 * vytvoření, tak jsem kvůli tomu napsat tuto metody, která je naplní a
	 * zbývající komponenty ,do kterých se má naplnit nějaký text, tak se naplní
	 * normálně v metodě setLanguage
	 * 
	 * @param properties
	 *            - reference na načtený soubor typu propeties s texty ve zvoleneém
	 *            jazyce aplikace
	 */
	protected void fillTextVariables(final Properties properties) {
		languageProperties = properties;
		
		if (properties != null) {
			txtByteFormatError = properties.getProperty("Fcop_FormatErrorByte", Constants.FCOP_BYTE_FORMAT_ERROR);
			txtShortFormatError = properties.getProperty("Fcop_FormatErrorShort", Constants.FCOP_SHORT_FORMAT_ERROR);
			txtIntFormatError = properties.getProperty("Fcop_FormatErrorInt", Constants.FCOP_INT_FORMAT_ERROR);
			txtLongFormatError = properties.getProperty("Fcop_FormatErrorLong", Constants.FCOP_LONG_FORMAT_ERROR);
			txtFloatFormatError = properties.getProperty("Fcop_FormatErrorFloat", Constants.FCOP_FLOAT_FORMAT_ERROR);
			txtDoubleFormatError = properties.getProperty("Fcop_FormatErrorDouble", Constants.FCOP_DOUBLE_FORMAT_ERROR);
			txtCharFormatError = properties.getProperty("Fcop_FormatErrorChar", Constants.FCOP_CHAR_FORMAT_ERROR);
			txtStringFormatError = properties.getProperty("Fcop_FormatErrorString", Constants.FCOP_STRING_FORMAT_ERROR);
			txtDefaultValueForStringField = properties.getProperty("Fcop_DefaultValueForStringJtextField", Constants.FCOP_DEFAULT_VALUE_FOR_STRING_FIELD);
			txtFieldContainError = properties.getProperty("Fcop_ErrorText_FieldContainsError", Constants.FCOP_ERROR_TEXT_FIELD_CONTAINS_ERRROR);
			txtErrorForErrorList = properties.getProperty("Fcop_ErrorText_ErrorForErrorList", Constants.FCOP_ERROR_TEXT_FOR_ERROR_LIST);
			txtPnlMistakesBorderTitle = properties.getProperty("Fcop_ErrorText_PnlMistakesBorderTitle", Constants.FCOP_PNL_MISTAKES_BORDER_TITLE);
			txtChcbWrapLyrics = properties.getProperty("Fcop_ErrorText_ChcbWrapLyrics", Constants.FCOP_CHCB_WRAP_LYRICS);
			
			badFormatSomeParameterText = properties.getProperty("Nif_BadFormatSomeParameterText", Constants.NIF_BAD_FORMAT_SOME_PARAMETER_TEXT);
			badFormatSomeParameterTitle = properties.getProperty("Nif_BadFormatSomeParameterTitle", Constants.NIF_BAD_FORMAT_SOME_PARAMETER_TITLE);	
			
			
			// Texty pro List:
			txtListStringFormatError = properties.getProperty("Fcop_TextListStringFormatError", Constants.FCOP_TXT_LIST_STRING_FORMAT_ERROR);
			txtListByteFormatError = properties.getProperty("Fcop_TextListByteFormatError", Constants.FCOP_TXT_LIST_BYTE_FORMAT_ERROR) + txtByteInterval;
			txtListShortFormatError = properties.getProperty("Fcop_TextListShortFormatError", Constants.FCOP_TXT_LIST_SHORT_FORMAT_ERROR) + txtShortInterval; 
			txtListIntegerFormatError = properties.getProperty("Fcop_TextListIntegerFormatError", Constants.FCOP_TXT_LIST_INTEGER_FORMAT_ERROR) + txtIntegerInterval;
			txtListLongFormatError = properties.getProperty("Fcop_TextListLongFormatError", Constants.FCOP_TXT_LIST_LONG_FORMAT_ERROR) + txtLongInterval;
			txtListCharacterFormatError = properties.getProperty("Fcop_TextListCharacterFormatError", Constants.FCOP_TXT_LIST_CHARACTER_FORMAT_ERROR);
			txtListBooleanFormatError = properties.getProperty("Fcop_TextListBooleanFormatError", Constants.FCOP_TXT_LIST_BOOLEAN_FORMAT_ERROR);
			txtListFloatFormatError = properties.getProperty("Fcop_TextListFloatFormatError", Constants.FCOP_TXT_LIST_FLOAT_FORMAT_ERROR) + txtFloatInterval;
			txtListDoubleFormatError = properties.getProperty("Fcop_TextListDoubleFormatError", Constants.FCOP_TXT_LIST_DOUBLE_FORMAT_ERROR) + txtDoubleInterval;
						
			
			// Texty pro kolekce:
			txtCollectionByteFormatError = properties.getProperty("Fcop_TextCollectionByteFormatError", Constants.FCOP_TXT__COLLECTION_BYTE_FORMAT_ERROR) + txtByteInterval;
			txtCollectionShortFormatError = properties.getProperty("Fcop_TextCollectionShortFormatError", Constants.FCOP_TXT__COLLECTION_SHORT_FORMAT_ERROR) + txtShortInterval;
			txtCollectionIntegerFormatError = properties.getProperty("Fcop_TextCollectionIntegerFormatError", Constants.FCOP_TXT__COLLECTION_INTEGER_FORMAT_ERROR) + txtIntegerInterval;
			txtCollectionLongFormatError = properties.getProperty("Fcop_TextCollectionLongFormatError", Constants.FCOP_TXT__COLLECTION_LONG_FORMAT_ERROR) + txtLongInterval;
			txtCollectionFloatFormatError = properties.getProperty("Fcop_TextCollectionFloatFormatError", Constants.FCOP_TXT__COLLECTION_FLOAT_FORMAT_ERROR) + txtFloatInterval;
			txtCollectionDoubleFormatError = properties.getProperty("Fcop_TextCollectionDoubleFormatError", Constants.FCOP_TXT__COLLECTION_DOUBLE_FORMAT_ERROR) + txtDoubleInterval;
			txtCollectionCharacterFormatError = properties.getProperty("Fcop_TextCollectionCharacterFormatError", Constants.FCOP_TXT__COLLECTION_CHARACTER_FORMAT_ERROR);
			txtCollectionBooleanFormatError = properties.getProperty("Fcop_TextCollectionBooleanFormatError", Constants.FCOP_TXT__COLLECTION_BOOLEAN_FORMAT_ERROR);
			txtCollectionStringFormatError = properties.getProperty("Fcop_TextCollectionStringFormatError", Constants.FCOP_TXT__COLLECTION_STRING_FORMAT_ERROR);
			
			// Texty pro jednorozměrné pole:
			txtArrayFormatErrorForAll1 = properties.getProperty("Fcop_TextArraryFormatErrorForAll_1", Constants.FCOP_TXT_ARRAY_FORMAT_ERROR_FOR_ALL_1);
			txtArrayFormatErrorAll2 = properties.getProperty("Fcop_TextArrayFormatErrorAll_2", Constants.FCOP_TXT_ARRAY_FORMAT_ERROR_ALL_2);
			txtArrayCharacterFormatError2 = properties.getProperty("Fcop_TextArrayCharacterFormatError_2", Constants.FCOP_TXT_ARRAY_CHARACTER_FORMAT_ERROR2);
			txtArrayBooleanFormatError2 = properties.getProperty("Fcop_TextArrayBooleanFormatError_2", Constants.FCOP_TXT_ARRAY_BOOLEAN_FORMAT_ERROR2);
			txtArrayStringFormatError2 = properties.getProperty("Fcop_TextArrayStringFormatError_2", Constants.FCOP_TXT_ARRAY_STRING_FORMAT_ERROR2);						
			
			
			// Výchozí text pro pole a list:
			txtDefaultTextForStringListAndArray1 = properties.getProperty("Fcop_DefaultTextForStringListAndArray_1", Constants.FCOP_DEFAULT_TEXT_FOR_STRING_LIST_AND_ARRAY_1);
			txtDefaultTextForStringListAndArray2 = properties.getProperty("Fcop_DefaultTextForStringListAndArray_2", Constants.FCOP_DEFAULT_TEXT_FOR_STRING_LIST_AND_ARRAY_2);
			
			// Hodnoty pro dvojrozměrné pole - chybové hlášky ohledně správného formátu syntaxe pro dvojrozměrné pole:
			txt2ArrayFormatErrorAll1 = properties.getProperty("Fcop_Text_2_ArrayFormatErrorAll_1", Constants.FCOP_TXT_2_ARRAY_FORMAT_ERROR_ALL_1);
			
			
			// Proměnné - resp. texty do chybového oznámení, pokud se jedná o nepodporovaný datový typ parametru:
			txtDataTypeOfParametr = properties.getProperty("Fcop_TextDataTypeOfParameter", Constants.FCOP_TXT_DATA_TYPE_OF_PARAMETER);
			txtIsNotSupportedForConstructor = properties.getProperty("Fcop_TextIsNotSupportedForConstructor", Constants.FCOP_TXT_IS_NOT_SUPPORTED_FOR_CONSTRUCTOR);
			txtIsNotSupportedForMethod = properties.getProperty("Fcop_TextIsNotSupportedForMethod", Constants.FCOP_TXT_IS_NOT_SUPPORTED_FOR_METHOD);
			txtConstructorCannotCall = properties.getProperty("Fcop_TetxtConstructorCannotCall", Constants.FCOP_TXT_CONSTRUCTOR_CANNOT_CALL);
			txtMethodCannotCall = properties.getProperty("Fcop_TextMethodCannotCall", Constants.FCOP_TXT_METHOD_CANNOT_CALL);
			
			
			// Chybové hlášky do editoru výstupů v případě, že se nepodaří vytvořit instanci třídy:
			txtFailedToCreateInstanceOfClass = properties.getProperty("Fcop_TextFailedToCreateInstanceOfClass", Constants.FCOP_TXT_FAILED_TO_CREATE_INSTANCE_OF_CLASS);
			txtPossibleErrors = properties.getProperty("Fcop_TextPossibleErrors", Constants.FCOP_TXT_POSSIBLE_ERRORS);
			txtPossibleErrorsWithPrivateType = properties.getProperty("Fcop_TextPossibleErrorsWithPrivateType", Constants.FCOP_TXT_POSSIBLE_ERRORS_WITH_PRIVATE_TYPE);
			
			
			// Texty do chybových výpisů, že nebyly nalezeny žádné instance pro předání do
			// metody nebo konstruktoru:
			txtInstanceNotFoundMethodCantCall = properties.getProperty("Fcop_TextInsanceNotFoundMethodCantBeCall", Constants.FCOP_TXT_INSTANCE_NOT_FOUND_METHOD_CANT_BE_CALL);
			txtInstanceNotFoundConstructorCantCall = properties.getProperty("Fcop_TextInsanceNotFoundConstructorCantBeCall", Constants.FCOP_TXT_INSTANCE_NOT_FOUND_CONSTRUCTOR_CANT_BE_CALL);
		}
		
		
		else {
			txtByteFormatError = Constants.FCOP_BYTE_FORMAT_ERROR;
			txtShortFormatError = Constants.FCOP_SHORT_FORMAT_ERROR;
			txtIntFormatError = Constants.FCOP_INT_FORMAT_ERROR;
			txtLongFormatError = Constants.FCOP_LONG_FORMAT_ERROR;
			txtFloatFormatError = Constants.FCOP_FLOAT_FORMAT_ERROR;
			txtDoubleFormatError = Constants.FCOP_DOUBLE_FORMAT_ERROR;
			txtCharFormatError = Constants.FCOP_CHAR_FORMAT_ERROR;
			txtStringFormatError = Constants.FCOP_STRING_FORMAT_ERROR;
			txtDefaultValueForStringField = Constants.FCOP_DEFAULT_VALUE_FOR_STRING_FIELD;
			txtFieldContainError = Constants.FCOP_ERROR_TEXT_FIELD_CONTAINS_ERRROR;
			txtErrorForErrorList = Constants.FCOP_ERROR_TEXT_FOR_ERROR_LIST;
			txtPnlMistakesBorderTitle = Constants.FCOP_PNL_MISTAKES_BORDER_TITLE;
			txtChcbWrapLyrics = Constants.FCOP_CHCB_WRAP_LYRICS;
			
			badFormatSomeParameterText = Constants.NIF_BAD_FORMAT_SOME_PARAMETER_TEXT;
			badFormatSomeParameterTitle = Constants.NIF_BAD_FORMAT_SOME_PARAMETER_TITLE;
			
			
			// Texty pro List:
			txtListStringFormatError = Constants.FCOP_TXT_LIST_STRING_FORMAT_ERROR;
			txtListByteFormatError = Constants.FCOP_TXT_LIST_BYTE_FORMAT_ERROR + txtByteInterval;
			txtListShortFormatError =Constants.FCOP_TXT_LIST_SHORT_FORMAT_ERROR + txtShortInterval; 
			txtListIntegerFormatError = Constants.FCOP_TXT_LIST_INTEGER_FORMAT_ERROR + txtIntegerInterval;
			txtListLongFormatError = Constants.FCOP_TXT_LIST_LONG_FORMAT_ERROR + txtLongInterval;
			txtListCharacterFormatError = Constants.FCOP_TXT_LIST_CHARACTER_FORMAT_ERROR;
			txtListBooleanFormatError = Constants.FCOP_TXT_LIST_BOOLEAN_FORMAT_ERROR;
			txtListFloatFormatError = Constants.FCOP_TXT_LIST_FLOAT_FORMAT_ERROR + txtFloatInterval;
			txtListDoubleFormatError = Constants.FCOP_TXT_LIST_DOUBLE_FORMAT_ERROR + txtDoubleInterval;
			
			
			// Texty pro kolekce:
			txtCollectionByteFormatError = Constants.FCOP_TXT__COLLECTION_BYTE_FORMAT_ERROR + txtByteInterval;
			txtCollectionShortFormatError = Constants.FCOP_TXT__COLLECTION_SHORT_FORMAT_ERROR + txtShortInterval;
			txtCollectionIntegerFormatError = Constants.FCOP_TXT__COLLECTION_INTEGER_FORMAT_ERROR + txtIntegerInterval;
			txtCollectionLongFormatError = Constants.FCOP_TXT__COLLECTION_LONG_FORMAT_ERROR + txtLongInterval;
			txtCollectionFloatFormatError = Constants.FCOP_TXT__COLLECTION_FLOAT_FORMAT_ERROR + txtFloatInterval;
			txtCollectionDoubleFormatError = Constants.FCOP_TXT__COLLECTION_DOUBLE_FORMAT_ERROR + txtDoubleInterval;
			txtCollectionCharacterFormatError = Constants.FCOP_TXT__COLLECTION_CHARACTER_FORMAT_ERROR;
			txtCollectionBooleanFormatError = Constants.FCOP_TXT__COLLECTION_BOOLEAN_FORMAT_ERROR;
			txtCollectionStringFormatError = Constants.FCOP_TXT__COLLECTION_STRING_FORMAT_ERROR;
			
			
			// Texty pro jednorozměrné pole:
			txtArrayFormatErrorForAll1 = Constants.FCOP_TXT_ARRAY_FORMAT_ERROR_FOR_ALL_1;
			txtArrayFormatErrorAll2 = Constants.FCOP_TXT_ARRAY_FORMAT_ERROR_ALL_2;
			txtArrayCharacterFormatError2 = Constants.FCOP_TXT_ARRAY_CHARACTER_FORMAT_ERROR2;
			txtArrayBooleanFormatError2 = Constants.FCOP_TXT_ARRAY_BOOLEAN_FORMAT_ERROR2;
			txtArrayStringFormatError2 = Constants.FCOP_TXT_ARRAY_STRING_FORMAT_ERROR2;
			
			
			// Výchozí text pro pole a list:
			txtDefaultTextForStringListAndArray1 = Constants.FCOP_DEFAULT_TEXT_FOR_STRING_LIST_AND_ARRAY_1;
			txtDefaultTextForStringListAndArray2 = Constants.FCOP_DEFAULT_TEXT_FOR_STRING_LIST_AND_ARRAY_2;
			
			// Hodnoty pro dvojrozměrné pole - chybové hlášky ohledně správného formátu syntaxe pro dvojrozměrné pole:
			txt2ArrayFormatErrorAll1 = Constants.FCOP_TXT_2_ARRAY_FORMAT_ERROR_ALL_1;
			
			
			// Proměnné - resp. texty do chybového oznámení, pokud se jedná o nepodporovaný datový typ parametru:
			txtDataTypeOfParametr = Constants.FCOP_TXT_DATA_TYPE_OF_PARAMETER;
			txtIsNotSupportedForConstructor = Constants.FCOP_TXT_IS_NOT_SUPPORTED_FOR_CONSTRUCTOR;
			txtIsNotSupportedForMethod = Constants.FCOP_TXT_IS_NOT_SUPPORTED_FOR_METHOD;
			txtConstructorCannotCall = Constants.FCOP_TXT_CONSTRUCTOR_CANNOT_CALL;
			txtMethodCannotCall = Constants.FCOP_TXT_METHOD_CANNOT_CALL;
			
			
			// Chybové hlášky do editoru výstupů v případě, že se nepodaří vytvořit instanci třídy:
			txtFailedToCreateInstanceOfClass = Constants.FCOP_TXT_FAILED_TO_CREATE_INSTANCE_OF_CLASS;
			txtPossibleErrors = Constants.FCOP_TXT_POSSIBLE_ERRORS;
			txtPossibleErrorsWithPrivateType = Constants.FCOP_TXT_POSSIBLE_ERRORS_WITH_PRIVATE_TYPE;
			
			
			// Texty do chybových výpisů, že nebyly nalezeny žádné instance pro předání do
			// metody nebo konstruktoru:
			txtInstanceNotFoundMethodCantCall = Constants.FCOP_TXT_INSTANCE_NOT_FOUND_METHOD_CANT_BE_CALL;
			txtInstanceNotFoundConstructorCantCall = Constants.FCOP_TXT_INSTANCE_NOT_FOUND_CONSTRUCTOR_CANT_BE_CALL;
		}
	}
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří instance Jlabelu a naplení ho textem v parametru Jedná
	 * se o label, který obsahuje informaci o tom jakou hodnotou se má naplnit
	 * JtextFiled - textové pole, před kterým je zobraze tento Jlabel
	 * 
	 * @param gbc
	 *            - objekt, kterému se nastaví index rádku a sloupce coby pozice pro
	 *            vložení komponenty do okna
	 * @param x
	 *            - index řádku
	 * @param y
	 *            - index sloupce
	 * @param p
	 *            - parametr metody nebo konstruktoru, do kterého se bude vkládat
	 *            nějaká hodnota, zde je potřeba aby se zjistilo, o jaký typ
	 *            parametru metody či konstruktoru se jedná a dle toho se vyplní
	 *            příslušný Jlabel s informací ohledně typu parametru
	 * @param panel
	 *            - jedná se o komponentu Jpanel, kam se pak vloží Jlabel s
	 *            informacemi o hodnotě, která se má vložit do parametru metody.
	 */
	private void createAndAddInfoValueLabel(final GridBagConstraints gbc, final int x, final int y,
			final Parameter p, final JPanel panel) {
		gbc.gridwidth = 1;

		// Nejprve zde otestuji, zda se jedná o List, pokud ano, tak si zjistím datový typ hodnot, které do něj lze vkládat,
		// abych to mohl oznámit uživateli v popisu hodnot,k které lze vkládat do pole pro zadání hodnot:
		// Pokud se nebude jednat o List, prostě tam vrátím datový typ té hodnoty, která se má vložit do pole pro parametr:

        lblTextInfo = new JLabel(ParameterToText.getParameterInText(p, false));

		labelsList.add(lblTextInfo);
		
		setGbc(gbc, x, y, lblTextInfo, panel);
	}








	/**
	 * Metoda, která vloží na zadanou pozici v dialogu textové pole - JtextField, do kterého uživatel zadá hodnoty coby
	 * parametry konstruktoru nebo metody
	 *
	 * @param gbc
	 *         - objekt, kterému se nastaví index rádku a sloupce coby pozice pro vložení komponenty do okna
	 * @param x
	 *         - index řádku
	 * @param y
	 *         - index sloupce
	 * @param text
	 *         - text pro vložení do textového pole jako výchozí hodnota pro parametr
	 * @param panel
	 *         - komponenty typu Jpanel, kam se má vložit komponenta Jtextfield, která se vytvoří pro zadání hodnoty
	 *         příslušného parametru metody či konstruktoru.
	 * @return vytvořené textové pole
	 */
	private JTextField createAndAddTextFieldForValue(final GridBagConstraints gbc, final int x, final int y, final
	String text, final JPanel panel) {
        final JTextField txtField = new JTextField(text, 20);

		listOfParametersValues.add(txtField);

		setGbc(gbc, x, y, txtField, panel);

		return txtField;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vloží na zadanou pozici v dialogu komponentu JComboBox, do
	 * kterého uživatel zadá hodnoty coby parametry konstruktoru nebo metody. Nebo
	 * si vybere hodnotu nějaké z načtených proměnných z instance třídy nebo
	 * statických proměnných z tříd nebo vytvořených proměnných v editoru příkazů.
	 * 
	 * @param gbc
	 *            - objekt, kterému se nastaví index rádku a sloupce coby pozice pro
	 *            vložení komponenty do okna
	 * @param x
	 *            - index řádku
	 * @param y
	 *            - index sloupce
	 * 
	 * @param panel
	 *            - komponenty typu Jpanel, kam se má vložit komponenta JComboBox,
	 *            která se vytvoří pro zadání hodnoty příslušného parametru metody
	 *            či konstruktoru.
	 * 
	 * @param fieldsList
	 *            - list, který slouží jako model hodnot s proměnnými pro cmb.
	 * 
	 * @param methodsList
	 *            - list, která obsahuje dostupné metody, které je možné zavolat pro
	 *            předání návratového hodnoty do parametru příslušné metody nebo
	 *            konstruktoru.
	 * 
	 * @param addNullValue
	 *            - logická hodnota která značí, zda se má přidat hodnota NullValue
	 *            do příslušného modelu JComboBoxu, který tato metoda vytvoří. Jde o
	 *            to, že u nekterých typů (objektých) parametru metod nebo
	 *            konstruktoru by bylo vhodné mít na výběr i hodnotu null.
	 * 
	 * @return vytvořený JComboBox pro zadání nebo "vybrání" do parametru pro metody
	 *         nebo konstruktoru.
	 */
	private JComboBox<CmbValueAncestor> createAndAddCmbForValue(final GridBagConstraints gbc, final int x, final int y,
			final JPanel panel, final List<ParameterValue> fieldsList, final List<MethodValue> methodsList,
			final boolean addNullValue) {
		
		/*
		 * JComboBox, jehož první položka slouží pro zadání hodnoty od uživatele a
		 * všechny ostatní jsou hodnoty, z proměnných ve třídách, instancích a editoru
		 * příkazů, jejichž hodnoty lze předat do parametru příslušné metody nebo
		 * konstruktoru. Pak následují metody a pak hodnota null.
		 */
		final JComboBox<CmbValueAncestor> cmb = new JComboBox<>();
		
		
		/*
		 * Získám si model pro cmb, abych do něj mohl přidat příslušné hodnoty -
		 * nalezené proměnné a metody, popřípadě i hodnotu null.
		 */
		final DefaultComboBoxModel<CmbValueAncestor> tempModel = (DefaultComboBoxModel<CmbValueAncestor>) cmb
				.getModel();
		
		// Přidám do modelu cmb veškeré nalezené atributy / proměnné:
		fieldsList.forEach(tempModel::addElement);

		// Přidám do modelu cmb veškeré nalezené metody:
		methodsList.forEach(tempModel::addElement);
		
		// Otestuji, zda se má přidat i hodnota null - případně ji přidám:
		if (addNullValue)
			tempModel.addElement(new NullValue());
		
		/*
		 * Výše potenciálně doplněný model pro cmb o atributy, metoda a případně null
		 * hodnotu nastavím příslušnému cmb, aby se změny projevily.
		 */
		cmb.setModel(tempModel);

		
		
		
		
		cmb.setPreferredSize(new Dimension(CMB_DEFAULT_WIDTH, CMB_DEFAULT_HEIGHT));

		/*
		 * Jako první označená položka (ve výchozím nastavení) je vždy první položka,
		 * takže ji na začátek nastavím, jako že půjde editovat / upravit její hodnota.
		 * 
		 * Ale první položka půjde editovat jen když je jako první zobrazena položka pro
		 * zadání hodnoty, tedy list s proměnnými má alespoň jednu položku, jako první e
		 * vždy zadává ta hodnota.
		 */
        if (!fieldsList.isEmpty() && fieldsList.get(0) != null
                && (fieldsList.get(0)).getKindOfVariable().equals(KindOfVariable.TEXT_VARIABLE)) {
            cmb.setEditable(true);

			cmb.addItemListener(event -> {
				/*
				 * Při změně označené položky nastavím příslušnou (označenou) položku na to, že
				 * buď půjde nebo nepůjde editovat.
				 * 
				 * Pouze první položka půjde editovat, protože pouze první položka v příslušném
				 * JComboBoxu je tam pouze pro to, aby ji zadal uživatel, veškeré ostatní
				 * položky jsou načtené proměnné z tříd a editoru příkazů.
				 */
				if (cmb.getSelectedIndex() == 0)
					cmb.setEditable(true);

				else
					cmb.setEditable(false);
			});
		}

		
		
		
		listOfParametersValues.add(cmb);

		setGbc(gbc, x, y, cmb, panel);

		return cmb;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vloží na zadanou pozici v dialogu komponentu JComboBox, který
	 * bude sloužít pouze pro označení hodnoty. Tento cmb již nepůjde editovat,
	 * resp. uživatel v něm již nebude moci upravit první položku, ale budemoci
	 * zadávat pouze položky, které se v tom cmbjiž nachází - v jeho modelu. Tento
	 * cmb se používá pro předání parametru Object a v takovém případě nebudu dávat
	 * uživateli možnost pro zadání hodnoty, ale puze pro označení hodnoty, kterou
	 * bude moci předat.
	 * 
	 * Bylo by také možné nechat uživatele zadat nějakou hodnotu, ale nevěděl bych
	 * na co tu hodnotu přetypovat, zda ji nechat v textu, nebo převést na číslo,
	 * pokud ano, tak jakého datového typu nbeo char, nebo boolean či pole apod.
	 * 
	 * Takže v tomto cmb si uživatel bude pouze moci označit proměnnou, kterou bue
	 * chtít předatdo parametru té metody nebo konstruktoru.
	 * 
	 * @param gbc
	 *            - objekt, kterému se nastaví index rádku a sloupce coby pozice pro
	 *            vložení komponenty do okna
	 * @param x
	 *            - index řádku
	 * @param y
	 *            - index sloupce
	 * 
	 * @param panel
	 *            - komponenty typu Jpanel, kam se má vložit komponenta JComboBox,
	 *            která se vytvoří pro zadání hodnoty příslušného parametru metody
	 *            či konstruktoru.
	 * 
	 * @param fieldValues
	 *            - list, který slouží jako model hodnot s proměnnými pro cmb.
	 * 
	 * @param addNullValue
	 *            - logická hodnota, která značí, zda se má nebo nemá přidat do
	 *            modelu s položkami v JComboBoxu i položka pro null hodnotu, tj.
	 *            když uživatel tuto položku označí, to znamená, že se při zavolání
	 *            příslušné metody nebo konstruktoru předá do příslušného parametru
	 *            hodnota null.
	 * 
	 * @return vytvořený JComboBox pro zadání nebo "vybrání" do parametru pro metody
	 *         nebo konstruktoru.
	 */
	private JComboBox<CmbValueAncestor> createAndAddCmbForObjectValue(final GridBagConstraints gbc, final int x,
			final int y, final JPanel panel, final List<ParameterValue> fieldValues,
			final List<MethodValue> methodValues, final boolean addNullValue) {

		/*
		 * JComboBox, jehož první položka slouží pro zadání hodnoty od uživatele a
		 * všechny ostatní jsou hodnoty, z proměnných ve třídách, instancích a editorup
		 * říkazů, jejichž hodnoty lze předat do parametru příslušné metody nebo
		 * konstruktoru.
		 */
		final JComboBox<CmbValueAncestor> cmb = new JComboBox<>();

		final DefaultComboBoxModel<CmbValueAncestor> tempModel = (DefaultComboBoxModel<CmbValueAncestor>) cmb
				.getModel();

		fieldValues.forEach(tempModel::addElement);
		
		methodValues.forEach(tempModel::addElement);
		
		
		/*
		 * Pokud se má do příslušného JComboBoxu přidat i na výběr položka pro null
		 * hodnotu, pak si vezmu model příslušného cmb, přidám do něj položku NullValue,
		 * která reprezentuje null hodnotu a pak tento doplněný model vložím zpět do
		 * cmb.
		 */
		if (addNullValue)
			tempModel.addElement(new NullValue());
		
		cmb.setModel(tempModel);
		
		
		
		
		
		cmb.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					variable = false;
					dispose();
				}

				else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					/*
					 * Pokud se jedná o zavolání metody, pak otestuji zadaná data a případně zkusím
					 * zavolat metodu:
					 */
					if (methodParametersForm != null)
						methodParametersForm.testData();

					/*
					 * Pokud se jedná o zavolání konstruktoru, tak otestuji zadaná data a případně
					 * zkusím zavolat konstruktor:
					 */
					else if (newInstanceForm != null)
						newInstanceForm.testData();
				}
			}
		});
		
		
		
		
		
		
		
		cmb.setPreferredSize(new Dimension(CMB_DEFAULT_WIDTH, CMB_DEFAULT_HEIGHT));
		
		// Nastavím autodoplňování pouze položek, které se nachází v modelu cmb
		// (cmbmodel):
		AutoCompleteDecorator.decorate(cmb);
		
		listOfParametersValues.add(cmb);

		setGbc(gbc, x, y, cmb, panel);

		return cmb;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá do okna tohoto dialogu - třídy komponentu v parametru na
	 * zadanou pozici v parametru
	 * 
	 * @param gbc
	 *            - objekt, kterému se nastaví index rádku a sloupce coby pozice pro
	 *            vložení komponenty do okna
	 * 
	 * @param rowIndex
	 *            - index řádku
	 * 
	 * @param columnIndex
	 *            - index sloupce
	 * 
	 * @param component
	 *            - komponenta pro přidání na danou pozici
	 */
	protected final void setGbc(final GridBagConstraints gbc, final int rowIndex, final int columnIndex,
			final JComponent component) {
		
		gbc.gridx = columnIndex;
		gbc.gridy = rowIndex;
		
		add(component, gbc);
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá do okna tohoto dialogu - třídy komponentu v parametru na
	 * zadanou pozici v parametru
	 * 
	 * @param gbc
	 *            - objekt, kterému se nastaví index rádku a sloupce coby pozice pro
	 *            vložení komponenty do okna
	 * 
	 * @param rowIndex
	 *            - index řádku
	 * 
	 * @param columnIndex
	 *            - index sloupce
	 * 
	 * @param component
	 *            - komponenta pro přidání na danou pozici
	 * 
	 * @param panel
	 *            - komponenta typu Jpanel, do kterého se vloží komponenta výše -
	 *            component
	 */
	protected static void setGbc(final GridBagConstraints gbc, final int rowIndex, final int columnIndex,
			final JComponent component, final JPanel panel) {
		
		gbc.gridx = columnIndex;
		gbc.gridy = rowIndex;
		
		panel.add(component, gbc);
	}
	
	
		
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá událost na stisknutí klávesy na jednotlivá pole v dialogu
	 * sloužící pro zadání parametru konstuktoru či metody Po stisknutí libovolného
	 * tlačítka se otestuje na příslušném textovém poli zadaná hodnoty, zda odpovídá
	 * typu parametru
	 * 
	 * @param txtField
	 *            - textové pole
	 * 
	 * @param regEx
	 *            - regulární výraz pro otestování zadané hodnoty v textovém poli
	 * 
	 * @param errorText
	 *            - chybová hláška, která se pridá do kolekce a ta do výpisu chyb -
	 *            v okně dialogu
	 * 
	 * @param dataTypeOfJTextfield
	 *            - index - výčtová hodnota, dle kterého poznám, na jaký typ hodnoty
	 *            mám zadanou hodnotu naparsovat - požadováný typ parametru
	 * 
	 * @param orderOfField
	 *            - pořadí textového pole v dialogu, které pomůže určit konkrétní
	 *            chybu - v jakém je textovém poli v dialogu
	 */
	private void addKeyListenerToTxtField(final JTextField txtField, final String regEx, final String errorText,
			final DataTypeOfJTextField dataTypeOfJTextfield, final int orderOfField) {
		txtField.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					variable = false;
					dispose();
				}

				else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					/*
					 * Pokud se jedná o zavolání metody, pak otestuji zadaná data a případně zkusím
					 * zavolat metodu:
					 */
					if (methodParametersForm != null)
						methodParametersForm.testData();

					/*
					 * Pokud se jedná o zavolání konstruktoru, tak otestuji zadaná data a případně
					 * zkusím zavolat konstruktor:
					 */
					else if (newInstanceForm != null)
						newInstanceForm.testData();
				}
			}
			
			
			@Override
			public void keyReleased(KeyEvent e) {
					/*
					 * V textu musím přičíst jedničku, protože položky jsou indexovány od nuly,
					 * takže v dialogu u uživatele by bylo zobrazeno, že třeba "0. text Chyby" ->
					 * což by znamenalo první pole, ale pro uživatele chci, aby viděl 1 jako první
					 * položku.
					 */
                final String textToErrorList = orderOfField + 1 + ". " + txtFieldContainError + " " + errorText;

				if (!txtField.getText().trim().isEmpty() && txtField.getText().trim().matches(regEx)) {
					try {
						switch (dataTypeOfJTextfield) {
						case BYTE:
							Byte.parseByte(txtField.getText().replaceAll("\\s", ""));

							removeErrorFromFieldOfDialog(txtField, textToErrorList);
							break;

						case SHORT:
							Short.parseShort(txtField.getText().replaceAll("\\s", ""));

							removeErrorFromFieldOfDialog(txtField, textToErrorList);
							break;

						case INT:
							Integer.parseInt(txtField.getText().replaceAll("\\s", ""));

							removeErrorFromFieldOfDialog(txtField, textToErrorList);
							break;

						case LONG:
							Long.parseLong(txtField.getText().replaceAll("\\s", ""));

							removeErrorFromFieldOfDialog(txtField, textToErrorList);
							break;

						case FLOAT:
							Float.parseFloat(txtField.getText().replaceAll("\\s", ""));

							removeErrorFromFieldOfDialog(txtField, textToErrorList);
							break;

						case DOUBLE:
							Double.parseDouble(txtField.getText().replaceAll("\\s", ""));

							removeErrorFromFieldOfDialog(txtField, textToErrorList);
							break;

						case CHAR:
							final String textInTexfield = txtField.getText().trim();

							final String textToParseChar = textInTexfield.substring(textInTexfield.indexOf('\'') + 1,
									textInTexfield.lastIndexOf('\''));

							Character.valueOf(textToParseChar.charAt(0));

							removeErrorFromFieldOfDialog(txtField, textToErrorList);
							break;

						case STRING:
							String.valueOf(txtField.getText().trim());

							removeErrorFromFieldOfDialog(txtField, textToErrorList);
							break;
								
								
							
							
							
							
						// Pro List a jednorozměrné pole:
						case LIST_AND_ARRAY_BYTE:
						case LIST_AND_ARRAY_SHORT:
						case LIST_AND_ARRAY_INTEGER:
						case LIST_AND_ARRAY_LONG:
						case LIST_AND_ARRAY_FLOAT:
						case LIST_AND_ARRAY_DOUBLE:
						case LIST_AND_ARRAY_CHARACTER:
						case LIST_AND_ARRAY_BOOLEAN:
						case LIST_AND_ARRAY_STRING:
							// Zkustím pretypovat zadané hodnoty v textovém poli na typ listu nebo
							// jednorozměrného pole:
							testValuesForListAnd1Array(txtField.getText(), dataTypeOfJTextfield);

							removeErrorFromFieldOfDialog(txtField, textToErrorList);
							break;

							
							
							
						// Pro dvojrozměrné pole:
						case TWO_DIMENSIONAL_ARRAY_BYTE:
						case TWO_DIMENSIONAL_ARRAY_SHORT:
						case TWO_DIMENSIONAL_ARRAY_INTEGER:
						case TWO_DIMENSIONAL_ARRAY_LONG:
						case TWO_DIMENSIONAL_ARRAY_FLOAT:
						case TWO_DIMENSIONAL_ARRAY_DOUBLE:
						case TWO_DIMENSIONAL_ARRAY_CHARACTER:
						case TWO_DIMENSIONAL_ARRAY_BOOLEAN:
						case TWO_DIMENSIONAL_ARRAY_STRING:
							testValuesTwoDimensionalArray(txtField.getText(), dataTypeOfJTextfield);

							removeErrorFromFieldOfDialog(txtField, textToErrorList);
							break;

						default:
							break;
						}

					} catch (NumberFormatException e2) {
						addErrorToFieldOfDialog(txtField, textToErrorList);

                        /*
                         * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                         * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                         * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                         * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                         */
                        final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                        // Otestuji, zda se podařilo načíst logger:
                        if (logger != null) {
                            // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                            // Nejprve mou informaci o chybě:
                            logger.log(Level.INFO,
                                    "Zachycena výjimka při parsování hodnoty datového typu String z textového pole na" +
                                            " datový typ typ jazyka Java (index typu v metodě: "
                                            + dataTypeOfJTextfield
                                            + "), metoda: addKeyListenerToTxtField, třída: FlexibleCoundOfParameters" +
                                            ".java, zadana hodnota: "
                                            + txtField.getText());

                            /*
                             * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                             * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                             * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                             */
                            logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e2));
                        }
                        /*
                         * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                         */
                        ExceptionLogger.closeFileHandler();

                    } catch (StringIndexOutOfBoundsException e2) {
                        /*
                         * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                         * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                         * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                         * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                         */
                        final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                        // Otestuji, zda se podařilo načíst logger:
                        if (logger != null) {
                            // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                            // Nejprve mou informaci o chybě:
                            logger.log(Level.INFO,
                                    "Zachycena výjimka při parsování hodnoty datového typu String z textového pole na" +
                                            " datový typ typ jazyka Java (index typu v metodě: "
                                            + dataTypeOfJTextfield
                                            + "), metoda: addKeyListenerToTxtField, třída: FlexibleCoundOfParameters" +
                                            ".java, zadana hodnota: "
                                            + txtField.getText()
                                            + ". Tato chyba může nastat například při získávání nějakého znaku ze " +
                                            "zadané syntaxe (metoda charAt), ale vrátí se záporná hodnota nebo " +
                                            "hodnota větší či stejná, jako je velikost textu.");

                            /*
                             * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                             * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                             * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                             */
                            logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e2));
                        }
                        /*
                         * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                         */
                        ExceptionLogger.closeFileHandler();
					}
				} else
					addErrorToFieldOfDialog(txtField, textToErrorList);
			}
		});
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá událost na stisknutí klávesy na jednotlivá pole v dialogu
	 * sloužící pro zadání parametru konstuktoru či metody Po stisknutí libovolného
	 * tlačítka se otestuje na příslušném textovém poli zadaná hodnoty, zda odpovídá
	 * typu parametru
	 * 
	 * @param cmb
	 *            = JComboBox, ke kterému se má přidat událost na stisknutí
	 *            tlačítek.
	 * 
	 * @param fieldsList
	 *            - list, který slouží jako model hodnot s atributy / proměnnými pro
	 *            cmb
	 * 
	 * @param methodsList
	 *            - list, který obsahuje nalezené metody, které je možné zavolat pro
	 *            naplnění hodnoty příslušného parametru metody nebo konstruktoru.
	 * 
	 * @param regEx
	 *            - regulární výraz pro otestování zadané hodnoty na první pozici v
	 *            cmb.
	 * 
	 * @param errorText
	 *            - chybová hláška, která se pridá do kolekce a ta do výpisu chyb -
	 *            v okně dialogu
	 * 
	 * @param dataTypeOfJTextfield
	 *            - index - výčtová hodnota, dle které poznám, na jaký typ hodnoty
	 *            mám zadanou hodnotu naparsovat - požadováný typ parametru
	 * 
	 * @param orderOfField
	 *            - pořadí cmb v dialogu, které pomůže určit konkrétní chybu - v
	 *            jakém je textovém poli nebo cmb v dialogu.
	 * 
	 * @param addNullValue
	 *            - logická hodnota, která značí, zda se má přidat i NullValue
	 *            (hodnota null) pro označení v příslušné JComboBox, aby se do
	 *            příslušné metody nebo konstruktoru při zavolání předala null
	 *            hodnota.
	 */	
	private void addKeyListenerToCmb(final JComboBox<CmbValueAncestor> cmb, final List<ParameterValue> fieldsList,
			final List<MethodValue> methodsList, final String regEx, final String errorText,
			final DataTypeOfJTextField dataTypeOfJTextfield, final int orderOfField, final boolean addNullValue) {
		
		cmb.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					variable = false;
					dispose();
				}
				
				
				else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					/*
					 * Pokud se jedná o zavolání metody, pak otestuji zadaná data a případně zkusím
					 * zavolat metodu:
					 */
					if (methodParametersForm != null)
						methodParametersForm.testData();

					/*
					 * Pokud se jedná o zavolání konstruktoru, tak otestuji zadaná data a případně
					 * zkusím zavolat konstruktor:
					 */
					else if (newInstanceForm != null)
						newInstanceForm.testData();
				}
			}
			
			
			@Override
			public void keyReleased(KeyEvent e) {
				/*
				 * Získám si označenou položku v tomto případě to bude ta editovatelná položka v
				 * příslušném cmb na prvním místě, protože jinou uživatel editovat nemůže.
				 * 
				 * (Do cmb jsou vkládány objekty ParameterValue, ale pouze v tomto případě má
				 * být první položka editovatelná, tak jej převedu na text čímž dostanu zadanou
				 * hodnotu od uživatele.)
				 */				
				final String newItem = cmb.getEditor().getItem().toString();

				/*
				 * Získám si položku z cmb modelu, která je na přvním místě a nastavím jí novou
				 * hodnotu (tu upravenou v dialogu od uživatele).
				 */
				final ParameterValue p = fieldsList.get(0);
				p.setTextValue(newItem);

				// Na první pozici v listu nastavím tu upravenou položku:
				fieldsList.set(0, p);

				// Nastavím model do cmb.
				final DefaultComboBoxModel<CmbValueAncestor> newModel = new DefaultComboBoxModel<>(
						fieldsList.toArray(new ParameterValue[] {}));
				
				/*
				 * Výše se do nového modelu vložil proměnné, nyní vložím metody, níže možná null
				 * hodnotu a pak ten model nastavím zpět do komponenty JComboBox.
				 */
				methodsList.forEach(newModel::addElement);
				
				/*
				 * Null hodnota půjde přidat pouze v případě, že se jedná o parametr metody nebo
				 * konstruktoru objektového datového typu.
				 * 
				 * Navíc je ten parametr zde potřeba přidat, protože se nenachází v listu s
				 * modelem (jeho položkami) pro ten JComboBox.
				 */
				if (addNullValue)
					newModel.addElement(new NullValue());
				
				cmb.setModel(newModel);
				
				
				
				
				
				
				
				/*
				 * V textu musím přičíst jedničku, protože položky jsou indexovány od nuly,
				 * takže v dialogu u uživatele by bylo zobrazeno, že třeba "0. text Chyby" ->
				 * což by znamenalo první pole, ale pro uživatele chci, aby viděl 1 jako první
				 * položku.
				 */
                final String textToErrorList = orderOfField + 1 + ". " + txtFieldContainError + " " + errorText;

				if (!newItem.trim().isEmpty() && newItem.trim().matches(regEx)) {
					try {
						switch (dataTypeOfJTextfield) {
						case BYTE:
							Byte.parseByte(newItem.trim());

							removeErrorFromFieldOfDialog(cmb, textToErrorList);
							break;

						case SHORT:
							Short.parseShort(newItem.trim());

							removeErrorFromFieldOfDialog(cmb, textToErrorList);
							break;

						case INT:
							Integer.parseInt(newItem.trim());

							removeErrorFromFieldOfDialog(cmb, textToErrorList);
							break;

						case LONG:
							Long.parseLong(newItem.trim());

							removeErrorFromFieldOfDialog(cmb, textToErrorList);
							break;

						case FLOAT:
							Float.parseFloat(newItem.trim());

							removeErrorFromFieldOfDialog(cmb, textToErrorList);
							break;

						case DOUBLE:
							Double.parseDouble(newItem.trim());

							removeErrorFromFieldOfDialog(cmb, textToErrorList);
							break;

						case CHAR:
							final String textInTexfield = newItem.trim();

							final String textToParseChar = textInTexfield.substring(textInTexfield.indexOf('\'') + 1,
									textInTexfield.lastIndexOf('\''));

							Character.valueOf(textToParseChar.charAt(0));

							removeErrorFromFieldOfDialog(cmb, textToErrorList);
							break;

						case STRING:
							String.valueOf(newItem.trim());

							removeErrorFromFieldOfDialog(cmb, textToErrorList);
							break;
								
								
							
							
						// Pro List a jednorozměrné pole:
						case LIST_AND_ARRAY_BYTE:
						case LIST_AND_ARRAY_SHORT:
						case LIST_AND_ARRAY_INTEGER:
						case LIST_AND_ARRAY_LONG:
						case LIST_AND_ARRAY_FLOAT:
						case LIST_AND_ARRAY_DOUBLE:
						case LIST_AND_ARRAY_CHARACTER:
						case LIST_AND_ARRAY_BOOLEAN:
						case LIST_AND_ARRAY_STRING:
							// Zkustím pretypovat zadané hodnoty v textovém poli na typ listu nebo
							// jednorozměrného pole:
							testValuesForListAnd1Array(newItem, dataTypeOfJTextfield);

							removeErrorFromFieldOfDialog(cmb, textToErrorList);
							break;
								
								
								
						// Pro dvojrozměrné pole:
						case TWO_DIMENSIONAL_ARRAY_BYTE:
						case TWO_DIMENSIONAL_ARRAY_SHORT:
						case TWO_DIMENSIONAL_ARRAY_INTEGER:
						case TWO_DIMENSIONAL_ARRAY_LONG:
						case TWO_DIMENSIONAL_ARRAY_FLOAT:
						case TWO_DIMENSIONAL_ARRAY_DOUBLE:
						case TWO_DIMENSIONAL_ARRAY_CHARACTER:
						case TWO_DIMENSIONAL_ARRAY_BOOLEAN:
						case TWO_DIMENSIONAL_ARRAY_STRING:
							testValuesTwoDimensionalArray(newItem, dataTypeOfJTextfield);

							removeErrorFromFieldOfDialog(cmb, textToErrorList);
							break;

							
							
						default:
							break;
						}
						
					} catch (NumberFormatException e2) {
						addErrorToFieldOfDialog(cmb, p, textToErrorList);

                        /*
                         * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                         * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                         * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                         * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                         */
                        final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                        // Otestuji, zda se podařilo načíst logger:
                        if (logger != null) {
                            // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                            // Nejprve mou informaci o chybě:
                            logger.log(Level.INFO,
                                    "Zachycena výjimka při parsování hodnoty datového typu String z textového pole¨(v" +
                                            " cmb - pouze první editovatelná položka) na datový typ typ jazyka Java (index " +
                                            "typu v metodě: "
                                            + dataTypeOfJTextfield
                                            + "), metoda: addKeyListenerToCmb, třída: FlexibleCoundOfParameters.java," +
                                            " zadana hodnota: "
                                            + newItem);

                            /*
                             * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                             * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                             * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                             */
                            logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e2));
                        }
                        /*
                         * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                         */
                        ExceptionLogger.closeFileHandler();

					} catch (StringIndexOutOfBoundsException e2) {
                        /*
                         * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                         * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                         * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                         * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                         */
                        final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                        // Otestuji, zda se podařilo načíst logger:
                        if (logger != null) {
                            // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                            // Nejprve mou informaci o chybě:
                            logger.log(Level.INFO,
                                    "Zachycena výjimka při parsování hodnoty datového typu String z textového pole na" +
                                            " datový typ typ jazyka Java (index typu v metodě: "
                                            + dataTypeOfJTextfield
                                            + "), metoda: addKeyListenerToCmb, třída: FlexibleCoundOfParameters.java," +
                                            " zadana hodnota: "
                                            + newItem
                                            + ". Tato chyba může nastat například při získávání nějakého znaku ze " +
                                            "zadané syntaxe (metoda charAt), ale vrátí se záporná hodnota nebo " +
                                            "hodnota větší či stejná, jako je velikost textu.");

                            /*
                             * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                             * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                             * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                             */
                            logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e2));
                        }
                        /*
                         * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                         */
                        ExceptionLogger.closeFileHandler();
					}
				} 
				else
					addErrorToFieldOfDialog(cmb, p, textToErrorList);
			}
		});
	}
	
	
	
	
	

	
	
	
	
	
	
	
	/**
	 * Metoda, kter8 do okna dialogu "přidá chybu", konkrétně přidá do Jlistu pro
	 * výpis chyb chybu, která uživateli oznámí, že má nějakou hodnotu špatně
	 * zadanou, dále příslušné pole, kde je špatně zadaná hodnota zvýrazní
	 * 
	 * Pokud bude textové pole null, tak se přidá pouze text do chybového listu, to
	 * je v případě, že metoda či konstruktor obsahují parametr, který není
	 * pozdporován, takže jeho hodnota nelze z dialogu zadat, proto nepůjde zavolat
	 * 
	 * @param txtField
	 *            - textové pole se špatně zadanou hodnotou pro zvýraznění
	 * 
	 * @param textToErrorList
	 *            - text s hybovým hlášením, který se má přidat do dialogu pro výpis
	 *            chyb
	 */
	protected final void addErrorToFieldOfDialog(final JTextField txtField, final String textToErrorList) {
		if (txtField != null) // Nemělo by nastat
			txtField.setBackground(Color.RED);

		if (!errorTextList.contains(textToErrorList))
			errorTextList.add(textToErrorList);

		errorList.setListData(errorTextList.toArray());
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, kter8 do okna dialogu "přidá chybu", konkrétně přidá do Jlistu pro
	 * výpis chyb chybu, která uživateli oznámí, že má nějakou hodnotu špatně
	 * zadanou, dále příslušné pole, kde je špatně zadaná hodnota zvýrazní
	 * 
	 * Pokud bude textové pole null, tak se přidá pouze text do chybového listu, to
	 * je v případě, že metoda či konstruktor obsahují parametr, který není
	 * pozdporován, takže jeho hodnota nelze z dialogu zadat, proto nepůjde zavolat
	 * 
	 * @param cmb
	 *            - JComboBox s editovatelným polem se špatně zadanou hodnotou pro
	 *            zvýraznění
	 * 
	 * @param parameterValue
	 *            - Položka, která obsahuje chybu, kterou uživatel udělal, tj. zadal
	 *            nějakou chybnou hodnotu, která například neprošla přetypováním na
	 *            hodnotu pro parametr nebo nepřošla regulárním výrazem apod. Tak
	 *            bude zpřístupněna pouze tato položak, dokud jej uživatel neopraví.
	 * 
	 * @param textToErrorList
	 *            - text s hybovým hlášením, který se má přidat do dialogu pro výpis
	 *            chyb
	 */
	private void addErrorToFieldOfDialog(final JComboBox<CmbValueAncestor> cmb,
			final ParameterValue parameterValue, final String textToErrorList) {
		if (cmb != null) {// Nemělo by nastat.
			/*
			 * Vytvořím si model pro příslušný cmb, který bude obsahovat puze jednu položku
			 * a sice tu, kterou uživatel právě edituje, konkrétně v tomto případě zadal
			 * hodnotu, která není validní pro příslušný parametr, tak uživateli zpřístupním
			 * pouze tu jednu položku, která mu tak zůstane, dokud jej neopraví. Až zadá
			 * uživatel validní hodnotu, pak se mu zpřístupní i všechny ostatní dostupné
			 * položky.
			 */
			final ParameterValue[] tempModel = { parameterValue };
			final DefaultComboBoxModel<CmbValueAncestor> cmbModel = new DefaultComboBoxModel<>(tempModel);
			
			// Nastavím model do cmb s tou jednou položkou:
			cmb.setModel(cmbModel);

			// Obarvím pozadí na červeno:
			changeBgOfCmb(cmb, Color.RED);
		}

		if (!errorTextList.contains(textToErrorList))
			errorTextList.add(textToErrorList);

		errorList.setListData(errorTextList.toArray());
	}
	
	
	
	
	
	
	
	

	
	
	
	
	/**
	 * Metoda, která z okna dialogu "odebere chybu" konkrétně odebere z Jlistu pro
	 * výpis chyb, chybu, která uživateli oznamuje, že má nějakou hodnotu špatně
	 * zadanou, dále příslušné pole, kde je špatně zadaná hodnota nastaví normální
	 * bílou barvu pozadí - zruší se zvýraznění chyby.
	 * 
	 * @param txtField
	 *            - textové pole se špatně zadanou hodnotou pro zrušení zvýraznění
	 * 
	 * @param textToErrorList
	 *            - text s hybovým hlášením, který se má odebraz z dialogu pro výpis
	 *            chyb
	 */
	protected final void removeErrorFromFieldOfDialog(final JTextField txtField, final String textToErrorList) {
		if (errorTextList.contains(textToErrorList)) {
			errorTextList.remove(textToErrorList);
			errorList.setListData(errorTextList.toArray());
		}

		txtField.setBackground(Color.WHITE);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která z okna dialogu "odebere chybu" konkrétně odebere z Jlistu pro
	 * výpis chyb, chybu, která uživateli oznamuje, že má nějakou hodnotu špatně
	 * zadanou, dále příslušné pole v cmb, kde je špatně zadaná hodnota nastaví
	 * normální bílou barvu pozadí - zruší se zvýraznění chyby.
	 * 
	 * @param cmb
	 *            - JComboBox se špatně zadanou hodnotou pro zrušení zvýraznění
	 * 
	 * @param textToErrorList
	 *            - text s hybovým hlášením, který se má odebraz z dialogu pro výpis
	 *            chyb
	 */
	private void removeErrorFromFieldOfDialog(final JComboBox<?> cmb, final String textToErrorList) {
		if (errorTextList.contains(textToErrorList)) {
			errorTextList.remove(textToErrorList);
			errorList.setListData(errorTextList.toArray());
		}

		changeBgOfCmb(cmb, Color.WHITE);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která obarví pozadí všech položek v komponěntě JComboBox (parametr
	 * této metody cmb) na barvu v color (parametr této metody). A zároveň ponechá
	 * tlačítko - šipku pro rozevírání popupMenu nedotčenou, resp. výchozí barvu
	 * apod.
	 * 
	 * Zdroj:
	 * https://stackoverflow.com/questions/10258224/change-background-color-editable-jcombobox
	 * 
	 * @param cmb
	 *            - Komponenta JComboBox, ve které se má obarvit pozadí položek v
	 *            popupmenu na barvu color.
	 * 
	 * @param color
	 *            - barva, na kterou se má obarvit pozadí položek v cmb.
	 */
	private static void changeBgOfCmb(final JComboBox<?> cmb, final Color color) {
		// Obarví se pozdí "textového pole", resp. položka:
		final JTextField text = ((JTextField) cmb.getEditor().getEditorComponent());
		text.setBackground(color);

		
		// Následuje cyklus, který vrátí tlačítko pro rozevírání popupmeu do původního
		// stavu:
		final Component[] comp = cmb.getComponents();

		for (final Component aComp : comp) {// hack valid only for Metal L&F
			if (aComp instanceof MetalComboBoxButton) {
				final MetalComboBoxButton coloredArrowsButton = (MetalComboBoxButton) aComp;
				coloredArrowsButton.setBackground(null);

				break;
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která si zjistí, o jako se jedná komponentu pro zadání hodnoty
	 * parametru - textové pole nebo Jcombobox a dle toho si z něj vezme příslušným
	 * způsobem zadanou či označenou hodnotu uživatelem jako hodnotu parametru
	 * konstuktoru nebo metody a tu vrátí
	 * 
	 * @param c
	 *            - komponenta ze které si zjistí hodnotu
	 * 
	 * @param varType
	 *            - typ parametru, dle čeho poznám, na jaký datový typ se má hodnota
	 *            "naprasovat"
	 * 
	 * @return vrátí získanou hodnotu v příslušném datovém typu
	 */
	protected final Object getWrittenParameter(final JComponent c, final Parameter varType) {
		// Pokud se jedná o Jtextové pole, tak si vezmu jeho hodnotu zadanou uživatelem
		// a zjistím na jaky typ ji mam nparsovat
		if (c instanceof JTextField) {
			// Název parametru, dle kterého poznám, na jaký typ mám hodnotu z textFieldu
			// naparsovat
			final String enteredParameter = ((JTextField) c).getText().trim();

			// Zkusím přetypovat zadanou hodnotu v textovém poli na příslušný datový typ:
			return getParsedParameterFromDialog(enteredParameter, varType);
		}
		
		
		
		
		
		
		
		
		
			
		
		// Otestuji, zda se jedná o komponentu typu JComboBox, která může obsahovat jak hodnoty typu Boolean (pro true nebo false)
		// a nebo pro vybrání třídy - typ ClassOrInstanceForParameter
		else if (c instanceof JComboBox<?>) {
			// Získám si označenou položku v ComboBoxu:
			final Object selectedItem = ((JComboBox<?>) c).getSelectedItem();
			
			// otestuji, zda se jedná o položku typu Boolean (v BooleanValue):
			if (selectedItem instanceof BooleanValue)
				return ((BooleanValue) selectedItem).isValue();
				
			
			
			
			// nebo, zde se jedná o položku typu ClassOrInstanceforParameter:
            else if (selectedItem instanceof ClassOrInstanceForParameter) {
                // Zde vím, že se jedná o parametr typu nějako třídy z diagramu tříd:
                // pokud se má vytvořit nová instance dané třídy a před do parametru, tak proměnná clazz nebude null,
                // pokud bude null, pak mám pouze vrátit referenci na instanci nějaké třídy -> parametr objInstance

                // Note: je jedno v jakém pořadí to testuji, mohl bych i testovat nejprve objInstance a výsledek by
                // byl stejný:
                if (((ClassOrInstanceForParameter) selectedItem).getClazz() != null) {
                    /*
                     * Zde se jedná o vytvoření nové instance nějaké třídy z diagramu tříd do parametru
                     *
                     *  Note: jedná se pouze o vytvoření instance do parametru, po vykonání dané metody či
                     *  konstruktoru instance zanikne, a nebude nikde uložena GC ji "uklidí" když na ní nebudou
                     *  ukazovat žádné reference, a ani ji nebudu ukládat, resp. reprezentovat v diagramu instancí:
                     */
                    return createInstance(((ClassOrInstanceForParameter) selectedItem).getClazz());
                }

                // Zde je proměná clazz null, takže je naplněna proměnné objInstance, tak poouze
                // vrátím příslušnou instanci v dané proměnné:
                else
                    return ((ClassOrInstanceForParameter) selectedItem).getObjInstance();
            }
			
			
			
			
			
			
			else if (selectedItem instanceof ParameterValue) {
				/*
				 * Zde uživatel zvolil jednu z proměnných, které měl na výběr, konkrétně v této
				 * části kódu to může být například proměnná z editoru příkazů, nebo proměnná z
				 * nějaké třídy, popřípadě instance nebo i samotná uživatelem zadaná hodnota viz
				 * následující podmínky, tak si získám a vrátím příslušnou hodnotu.
				 */

				/*
				 * Přetypuji si označenou hodnotu z komponenty JComboBox na objekt (/ třídu)
				 * typu ParameterValue.
				 */
				final ParameterValue pValue = (ParameterValue) selectedItem;

				/*
				 * Nyní si dle typu hodnoty, kterou reprezentuje hodnota pValue zjistím, z jaké
				 * proměnné si mám vzít hodnotu pro předání do parametru metody nebo
				 * konstruktoru a zda ji mám vrátit nebo provést ještě nějakou operaci:
				 */
				switch (pValue.getKindOfVariable()) {
				case COMMAND_EDITOR_VARIABLE:					
					/*
					 * Tuto podmínku jsem doplnil pouze pro to, že uživatel má možnost v editoru
					 * příkazů vytvořit jednorozměrné pole datového typu Javy, ale vše se pro
					 * aplikaci ukládá jako objekty (generické), proto zde nelze předat
					 * jednorozměrné pole "normálně" jen předáním hodnoty z metody
					 * getObjEditorVariable, ale je teba to pole po jednotlivých položkách převést
					 * na text, a pak přetypovat na konkrétní datový typ a vrátit -> resp. vrátit
					 * jednorozměrné pole stejného datového typu.
					 */
					if (pValue.getArrayType() != null) {
						/*
						 * Zde vím, že se jedná o datové typy Javy, ať už to jsou Byte, byte, Float,
						 * float, String, ...
						 * 
						 * Jelikož jsou v objektech VariableArray vždy objektové typy pro příslušnou
						 * hodnotu, a tak se i nachází v ParameterValue jako hodnota - resp. to pole
						 * samotné, tak si všechny položky potřebuji převést na test a předat je do
						 * metody: getParsedParameterFromDialog s parametrem datový typ toho parametru
						 * metody nebo konstruktoru, díky tomu se mi v uvedené metodě dojde k podmínce,
						 * že se jedná o datové pole a celý text se rozdělí dle příslušných hodnot a
						 * jednotlivé hodnoty se převedou na příslušné jednorozměrné pole konkrétního
						 * požadovaného (správného) datového typu. Díky tomu si zařídím, že se povede to
						 * předání to parametru metody uživatelem vytvořeného pole v editoru příkazů,
						 * jinak by to vždy hlásilo chybu. že se jedná o chybné typy parametrů.
						 * 
						 * V případě rozšíření pro vícerozměrné pole by bylo potřeba provést stejný
						 * postup.
						 */
						
						/*
						 * Zde ještě potřebuji přidat podmínku, pokud se jedná o pole znaků
						 * (char[].class nebo Character[].class), pak se příslušné pole převede na text
						 * tak, že budou znaky v syntaxi: [X, X, X], tj. nebudou v uvozovkách, jak si je
						 * následující metoda rozebere, tak je musím "přidat".
						 */
						if (varType.getType().equals(char[].class) || varType.getType().equals(Character[].class)) {
							final String arrayInText = MakeOperation.fromArrayToString(pValue.getObjEditorVariable());

							final String[] parts = arrayInText.substring(1, arrayInText.length() - 1).split(",");

							String result = "";

							if (parts.length > 0) {
								for (final String p : parts)
									result += "'" + p.trim() + "' ";

								result = result.substring(0, result.length() - 1);
							}

							final String finalResult = "[" + result + "]";

							return getParsedParameterFromDialog(finalResult, varType);
						}

						// Zde mohu "normálně" převést hodnotu na příslušný typ pole:
						return getParsedParameterFromDialog(
								MakeOperation.fromArrayToString(pValue.getObjEditorVariable()), varType);
					}
					

					// Zde vrátím označenou hodnotu z proměnné, kterou uživatel vytvořil v editoru
					// příkazů.
					return pValue.getObjEditorVariable();
					

				case FIELD_VARIABLE:
					// zde vrátím hodnotu, kterou uživatel označil z nějaké proměnné / třídy.
					return pValue.getObjValueFromField();
					

				case TEXT_VARIABLE:
					// Zde se jedná o zadání textu - vlastní proměnné, tak nejrpve přetypuji zadaný
					// text v dialogu na příslušný datový typ a poté vrátím:
					return getParsedParameterFromDialog(pValue.getTextValue(), varType);

				default:
					break;
				}
			}

			
			
			/*
			 * Otestuji, zda se jedná o zavolání metody, pokud ano, pak ji zavolám a vrátím
			 * hodnotu, kterou vrátí ta zavolaná metoda.
			 */
			else if (selectedItem instanceof MethodValue)
				return ((MethodValue) selectedItem).callMethod(instanceDiagram, makeVariablesAvailable,
						showMakeNewInstanceOption, makeMethodsAvailable, languageProperties);
			

			
			/*
			 * V případě, že se z JComboBoxu načetla označená hodnota coby instance
			 * NullValue, pak vrátím hodnotu null, protože se jedná o právě o to, že
			 * uživatel chtěl předat do parametru metody null hodnotu.
			 */
			else if (selectedItem instanceof NullValue)
				return null;
		}

		return null;
	}


    /**
     * Vytovření instance třídy clazz.
     *
     * @param clazz
     *         - třída, ze které se má vytvořit instance.
     *
     * @return vytvořená instance třídy clazz, jinak null v případě, že se nepodaří instanci vytvořit.
     */
    private Object createInstance(final Class<?> clazz) {
        try {

            return clazz.newInstance();

        } catch (final InstantiationException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při vytvřáření instance třídy z diagramu tříd do parametru " +
                        "konstruktoru nebo metody! Třída FlexibleCountOfParameters.java metoda: getWrittenParameter(),  " +
                        "poslední podmínka pro otestovani dat z ComboBoxu. Tato chyba může nastat například v případě, že " +
                        "třída, ze které se má vytvořit instance představuje abstraktní třídu, rozhraní, primitivní datový" +
                        " typ nebo void, nebo pokud se v dané třídě nenachází výchozí nullary konstruktor, nebo pokud " +
                        "instanciace selžez nějakého jiného důvodu.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();


            outputEditor.addResult(txtFailedToCreateInstanceOfClass + ": " + clazz.getName() + ", " + txtPossibleErrors);

        } catch (final IllegalAccessException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při vytvřáření instance třídy z diagramu tříd do parametru " +
                        "konstruktoru nebo metody! Třída FlexibleCountOfParameters.java metoda: getWrittenParameter(),  " +
                        "poslední podmínka pro otestovani dat z ComboBoxu. Tato chyba může nastat například v případě, že " +
                        "třída, ze které se má vytvořit instance není přístupná, nebo její výchozí nullary konstruktor " +
                        "není přístupný nebo obsažen v dané třídě.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();

            outputEditor.addResult(txtFailedToCreateInstanceOfClass
                    + clazz.getName() + ", "
                    + txtPossibleErrorsWithPrivateType);
        }

        return null;
    }
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která přetypuje zadanou hodnotu v dialogu: enteredParameter na datový
	 * typ varType, který vyžaduje parametr metody nebo konstruktoru, na konec se
	 * vrátí například přetypovaná hodnota apod. nebo null, pokud nebude podporován
	 * datový typ, popřípadě dojde k chybě apod.
	 * 
	 * @param enteredParameter
	 *            - Textová hodnota, kterou zadal uživatel jako nějaký parametr pro
	 *            příslušnou metodu, v této fází vím, že minimálně splňuje podmínky
	 *            dle regulárních výrazů, tak se může příslušná příslušná hodnota v
	 *            této proměnné pokusit přetypovat na příslušný datový typ -
	 *            varType.
	 * 
	 * @param varType
	 *            - typ parametru, dle čeho poznám, na jaký datový typ se má hodnota
	 *            "naprasovat"
	 * 
	 * @return výslednou hodnotu pro parametr metody nebo konstruktoru (například
	 *         přetypovanou hodnotu apod.). Nebo null, pokud nebude podporován
	 *         datový typ parametru.
	 */
	private static Object getParsedParameterFromDialog(final String enteredParameter, final Parameter varType) {

		if (varType.getType().equals(byte.class) || varType.getType().equals(Byte.class))
			return new ByteTypeParser().parse(enteredParameter);
		
		
		else if (varType.getType().equals(short.class) || varType.getType().equals(Short.class))
			return new ShortTypeParser().parse(enteredParameter);
		
		
		else if (varType.getType().equals(int.class) || varType.getType().equals(Integer.class))
			return new IntegerTypeParser().parse(enteredParameter);
		
		
		else if (varType.getType().equals(long.class) || varType.getType().equals(Long.class))
			return new LongTypeParser().parse(enteredParameter);
		
		
		else if (varType.getType().equals(float.class) || varType.getType().equals(Float.class))
			return new FloatTypeParser().parse(enteredParameter);
		
		
		else if (varType.getType().equals(double.class) || varType.getType().equals(Double.class))
			return new DoubleTypeParser().parse(enteredParameter);
		
		
		else if (varType.getType().equals(char.class) || varType.getType().equals(Character.class))
			return new CharacterTypeParser().parse(enteredParameter);
		
		
		else if (varType.getType().equals(String.class)) {
			/*
			 * Zde, v případě, že je příslušný text v syntaxi: "xxx", tedy ten text je
			 * obalen uvozovkami pro text, tak jej odeberu, protože, například, když bych
			 * tímto nastavil nějaku proměnnou, byly by v ní u ty uvozovky, což nechci.
			 * 
			 * Proto je třeba posílat pouze ten text bez uvozovek.
			 */

			// Regulárním výrazem otestuji, zda je text obalem uvozovkami:
			if (enteredParameter.trim().matches(REG_EX_TEXT_IN_QUOTATION_MARKS))
				/*
				 * Odeberu bílé znaky na začátku a na konci textu, pak odeberu, resp. příslušný
				 * text zkrátním o první a poslední znak, což jsou ty nežádoucí uvozovky.
				 */
				return enteredParameter.trim().substring(1, enteredParameter.length() - 1);

			/*
			 * V tomto případě by tato možnost nastat neměla, pokud se jedná o získání textu
			 * z příslušného textového pole, pak tam ty uvzovky budou vždy.
			 */
			return enteredParameter;
		}
			
		
		
		
		
		
		
		
		
		
		// Testování pro List:
		else if (varType.getType().equals(List.class)) {
			// Note: bude zde vytvářet instanci Listu typu ArrayList

			/*
			 * Zjistím si datový typ hodnot, které lze vkládat do Listu:
			 */
			final DataTypeOfList dataTypeOfListValues = ReflectionHelper.getDataTypeOfList(varType);

			/*
			 * Pokud se nejedná o konkrétní datový typ Listu, pak není jasné, na jaký typ by se měly hodnoty
			 * přetypovat. Proto je možné skončit.
			 */
			if (dataTypeOfListValues.getDataTypeOfListEnum() != DataTypeOfListEnum.SPECIFIC_DATA_TYPE)
				return null;

			/*
			 * (V této podmínce je třeba vyloučit možnosti, kdy je například List typu: "List<ArrayList<String>>" apod
			 * . Ale to by nastat nemělo. Pouze "pojistka".)
			 *
			 * Výše je zjištěno, že se jedná o specifický datový typ Listu, ale přetypování hodnot, které následuje je
			 * povoleno / podporováno pouze pro vybrané základní datové typy jazyka Java. Proto je třeba zjistit, zda
			 * je hodnota Listu instance třídy "Class<?>". Pokud je to null, pak se jedná o generický datový typ
			 * nebo nedefinovaný typ Listu a pokud to není instance třídy Class<?>, pak se jedná o nepodporovaný typ
			 * na přetypování hodnot. Proto je možné skončit.
			 */
			if (!(dataTypeOfListValues.getDataType() instanceof Class<?>))
				return null;

			// Převedou se hodnoty na příslušný list a vrátí se:
			return getListFromParameter((Class<?>) dataTypeOfListValues.getDataType(), enteredParameter);
		}
		
		
		
		
		
		
		
		
		
		
		
		/*
		 * Zde otestuji, zda se jedná o jeden z typů pro kolekci:
		 */
		else if (varType.getType().equals(ArrayList.class) || varType.getType().equals(CopyOnWriteArrayList.class)
				|| varType.getType().equals(LinkedList.class) || varType.getType().equals(Stack.class)
				|| varType.getType().equals(Vector.class))
			return getCollectionFromEnteredParameter(varType, enteredParameter);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		// testování pro jednorozměrné pole:
		
		// Note, zde musím testovat a vytváře zvlášt každé pole, protože je rozdíl mezi jednorozměrným polem
		// primitivního a objektového typu
		
		// Zatímco metoda: testValuesForListAndArray slouží pouze pro testování hodnot, tak musím použít podobný princip na
		// parsování a získání hodnot z pole, avšek už na tomot mástě potřebuji vytvořit a vrátit potřebné pole, proto nelze spojit
		// tuto a zmínenou metodu, například na vracení hodnot, apod.
		else if (varType.getType().equals(byte[].class)) {
			
			// Vezmu si text mezi hranatými závorkami a rozdělím je dle desetinné čárky = jednotlivé hodnoty:
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);

			// vytvořím si potřebné pole:
			final byte[] array = new byte[dividedByComma.length];

			// naplním výše vytvořené pole parsovanými hodnotami na potřebný datový typ:
			for (int i = 0; i < dividedByComma.length; i++)
				// Potřebuji zde otestovat, zda se nejedná o "prázdný text", pak by se jednalo o prázdné pole,
				// takže by se vytvořilo výše pole, v něm výchozí hodnota a v podstatě je to vše - stejně tak pro následující pole:
				if (!dividedByComma[i].isEmpty())
					array[i] = new ByteTypeParser().parse(dividedByComma[i]);
			
			// vrátím potřebné pole:
			return array;
		}
		
		
		
		else if (varType.getType().equals(Byte[].class)) {
			// Vezmu si text mezi hranatými závorkami a rozdělím je dle desetinné čárky = jednotlivé hodnoty:
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);

			// vytvořím si potřebné pole:
			final Byte[] array = new Byte[dividedByComma.length];
			
			// naplním výše vytvořené pole parsovanými hodnotami na potřebný datový typ:
			for (int i = 0; i < dividedByComma.length; i++)
				if (!dividedByComma[i].isEmpty())
					array[i] = new ByteTypeParser().parse(dividedByComma[i]);
			
			// vrátím potřebné pole:
			return array;
		}
		
		
		
		else if (varType.getType().equals(short[].class)) {
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);

			final short[] array = new short[dividedByComma.length];

			for (int i = 0; i < dividedByComma.length; i++)
				if (!dividedByComma[i].isEmpty())
					array[i] = new ShortTypeParser().parse(dividedByComma[i]);
			
			return array;
		}
		
		
		
		else if (varType.getType().equals(Short[].class)) {
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);

			final Short[] array = new Short[dividedByComma.length];

			for (int i = 0; i < dividedByComma.length; i++)
				if (!dividedByComma[i].isEmpty())
					array[i] = new ShortTypeParser().parse(dividedByComma[i]);
			
			return array;
		}
		
		
		
		else if (varType.getType().equals(int[].class)) {
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);
			
			final int[] array = new int[dividedByComma.length];

			for (int i = 0; i < dividedByComma.length; i++)
				if (!dividedByComma[i].isEmpty())
					array[i] = new IntegerTypeParser().parse(dividedByComma[i]);
			
			return array;
		}
		
		
		
		else if (varType.getType().equals(Integer[].class)) {
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);

			final Integer[] array = new Integer[dividedByComma.length];

			for (int i = 0; i < dividedByComma.length; i++)
				if (!dividedByComma[i].isEmpty())
					array[i] = new IntegerTypeParser().parse(dividedByComma[i]);
			
			return array;
		}
		
		
		
		else if (varType.getType().equals(long[].class)) {
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);

			final long[] array = new long[dividedByComma.length];

			for (int i = 0; i < dividedByComma.length; i++)
				if (!dividedByComma[i].isEmpty())
					array[i] = new LongTypeParser().parse(dividedByComma[i]);
			
			return array;
		}
		
		
		
		else if (varType.getType().equals(Long[].class)) {
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);

			final Long[] array = new Long[dividedByComma.length];

			for (int i = 0; i < dividedByComma.length; i++)
				if (!dividedByComma[i].isEmpty())
					array[i] = new LongTypeParser().parse(dividedByComma[i]);
			
			return array;
		}
		
		
		
		else if (varType.getType().equals(float[].class)) {
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);

			final float[] array = new float[dividedByComma.length];

			for (int i = 0; i < dividedByComma.length; i++)
				if (!dividedByComma[i].isEmpty())
					array[i] = new FloatTypeParser().parse(dividedByComma[i]);
			
			return array;
		}
		
		
		
		else if (varType.getType().equals(Float[].class)) {
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);

			final Float[] array = new Float[dividedByComma.length];

			for (int i = 0; i < dividedByComma.length; i++)
				if (!dividedByComma[i].isEmpty())
					array[i] = new FloatTypeParser().parse(dividedByComma[i]);
			
			return array;
		}
		
		
		
		else if (varType.getType().equals(double[].class)) {
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);

			final double[] array = new double[dividedByComma.length];

			for (int i = 0; i < dividedByComma.length; i++)
				if (!dividedByComma[i].isEmpty())
					array[i] = new DoubleTypeParser().parse(dividedByComma[i]);
			
			return array;
		}
		
		
		
		else if (varType.getType().equals(Double[].class)) {
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);

			final Double[] array = new Double[dividedByComma.length];

			for (int i = 0; i < dividedByComma.length; i++)
				if (!dividedByComma[i].isEmpty())
					array[i] = new DoubleTypeParser().parse(dividedByComma[i]);
			
			return array;
		}
		
		
		else if (varType.getType().equals(char[].class)) {
			// Zde již nemohu použít postup jako výše, pro začátek si vezmu text mezi hranatými závorkami:
			final String textWithoutBracket = getTextWithoutBracket(enteredParameter);

			// rozdělím jej dle jednoduchých uvozovek:
			final String[] dividedByQuotes = textWithoutBracket.split("'");
			
			// vytvořím si potřebné pole:
			final char[] array = new char[dividedByQuotes.length / 2];
			
			// a naplním hod hodnotami vždy u liché uvozovky - resp. znakem před ní:
			// Proměnná pro definování pozice v poli pro vložení hodnoty - array
			int index = 0;
			
			for (int i = 1; i < dividedByQuotes.length; i += 2)
				if (!dividedByQuotes[i].isEmpty())
					array[index++] = new CharacterTypeParser().parse(dividedByQuotes[i]);				
				
			
			// vrátím potřebné pole:
			return array;
		}
		
		
		else if (varType.getType().equals(Character[].class)) {
			// Zde je v podstatě stejný postup jako výše, akorát s jiným polem:
			final String textWithoutBracket = getTextWithoutBracket(enteredParameter);

			final String[] dividedByQuotes = textWithoutBracket.split("'");
			
			final Character[] array = new Character[dividedByQuotes.length / 2];
							
			int index = 0;
			for (int i = 1; i < dividedByQuotes.length; i += 2)
				if (!dividedByQuotes[i].isEmpty())
					array[index++] = new CharacterTypeParser().parse(dividedByQuotes[i]);
							
			return array;
		}
		
		
		
		else if (varType.getType().equals(boolean[].class)) {
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);

			final boolean[] array = new boolean[dividedByComma.length];

			for (int i = 0; i < dividedByComma.length; i++)
				if (!dividedByComma[i].isEmpty())
					array[i] = new BooleanTypeParser().parse(dividedByComma[i]);
			
			return array;
		}
		
		
		
		else if (varType.getType().equals(Boolean[].class)) {
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);

			final Boolean[] array = new Boolean[dividedByComma.length];

			for (int i = 0; i < dividedByComma.length; i++)
				if (!dividedByComma[i].isEmpty())
					array[i] = new BooleanTypeParser().parse(dividedByComma[i]);
			
			return array;
		}
		
		
		
		else if (varType.getType().equals(String[].class)) {
			// Zde si opět vezmu text bez hrenatých závorek:
			final String textWithoutBracket = getTextWithoutBracket(enteredParameter);

			// a oddělím ho dle uvozovek:
			final String[] divided = textWithoutBracket.split("\"");
			
			// vytvořím si potřebnýá pole typu String
			final String[] array = new String[divided.length / 2];
			
			// naplním ho hodnotami vždy u liché uvozovky, resp. textem přd ní:
			int index = 0;
			
			for (int i = 1; i < divided.length; i += 2)
				if (!divided[i].isEmpty())
					array[index++] = String.valueOf(divided[i]);
			
			// a vrátím vytvořené pole:
			return array;
		}
		
		
		
		
		
		
		
		
		
		
		
		
		// Testování dvojrozměrného pole:
		
		else if (varType.getType().equals(byte[][].class)) {		
			// Vezmu si oddělené hodnoty dle desetinné čárky, a ještě před tím odeberu otevírací závorku pro pole:
			final String[] arraySplit = getSplittedValues(enteredParameter);
			
			// Deklaruji si zde pole příslušného datového typu a definovanou velikostí:
			final byte[][] finalArray = new byte[arraySplit.length][];
			
			// Postup:
			// Prohledám pole arraySplit, kde jsou jednotlivá pole rozdělená dle špičaté uzavírací závorky: '}'
			// Odstraním otevírací závorku pro každé pole a rozdělíám dle desetinné čárky, čímž oddělím / zjistím jednotlivé hodnoty
			for (int i = 0; i < arraySplit.length; i++) {
				// Získám si konkrétní hodnoty v poli:
				final String[] partsOfValues = getSeparatedValues(arraySplit[i]);

				final byte[] newArray = new byte[partsOfValues.length];
				
				// Otestuji jednotlivé hodnoty, zda to není pouze prázdný text (v případě prázdného pole), a zkusím je
				// přetypovat na konkrétní datový typ:
				for (int j = 0; j < partsOfValues.length; j++)
					if (!partsOfValues[j].isEmpty())
						newArray[j] = new ByteTypeParser().parse(partsOfValues[j]);
				
				// naplnění jednorozměrné pole vložím do finálního pole, které vrátím jako parametr metody či kontruktoru:
				finalArray[i] = newArray; 
			}
			
			// vrátím pole vytvořené / zadané uživatelem
			return finalArray;
		}
		
		
		// Note: 
		// Níže následuje pro číselné a logické hodnoty stejný postup, tak už tento kó komentovat nebudu,
		// budou se lišit akorát datové typy polí a hodnot, které se parsují:
		else if (varType.getType().equals(Byte[][].class)) {
			final String[] arraySplit = getSplittedValues(enteredParameter);
			
			final Byte[][] finalArray = new Byte[arraySplit.length][];
			
			for (int i = 0; i < arraySplit.length; i++) {
				// Získám si konkrétní hodnoty v poli:
				final String[] partsOfValues = getSeparatedValues(arraySplit[i]);

				final Byte[] newArray = new Byte[partsOfValues.length];
				
				for (int j = 0; j < partsOfValues.length; j++)
					if (!partsOfValues[j].isEmpty())
						newArray[j] = new ByteTypeParser().parse(partsOfValues[j]);
				
				finalArray[i] = newArray; 
			}
			
			return finalArray;
		}
		
		
		else if (varType.getType().equals(short[][].class)) {
			final String[] arraySplit = getSplittedValues(enteredParameter);
			
			final short[][] finalArray = new short[arraySplit.length][];
			
			for (int i = 0; i < arraySplit.length; i++) {
				// Získám si konkrétní hodnoty v poli:
				final String[] partsOfValues = getSeparatedValues(arraySplit[i]);

				final short[] newArray = new short[partsOfValues.length];
				
				for (int j = 0; j < partsOfValues.length; j++)
					if (!partsOfValues[j].isEmpty())
						newArray[j] = new ShortTypeParser().parse(partsOfValues[j]);
				
				finalArray[i] = newArray; 
			}
			
			return finalArray;
		}
		
		
		else if (varType.getType().equals(Short[][].class)) {
			final String[] arraySplit = getSplittedValues(enteredParameter);
			
			final Short[][] finalArray = new Short[arraySplit.length][];
			
			for (int i = 0; i < arraySplit.length; i++) {
				// Získám si konkrétní hodnoty v poli:
				final String[] partsOfValues = getSeparatedValues(arraySplit[i]);

				final Short[] newArray = new Short[partsOfValues.length];
				
				for (int j = 0; j < partsOfValues.length; j++)
					if (!partsOfValues[j].isEmpty())
						newArray[j] = new ShortTypeParser().parse(partsOfValues[j]);
				
				finalArray[i] = newArray; 
			}
			
			return finalArray;
		}
		
					
		else if (varType.getType().equals(int[][].class)) {
			final String[] arraySplit = getSplittedValues(enteredParameter);
			
			final int[][] finalArray = new int[arraySplit.length][];
			
			for (int i = 0; i < arraySplit.length; i++) {
				// Získám si konkrétní hodnoty v poli:
				final String[] partsOfValues = getSeparatedValues(arraySplit[i]);

				final int[] newArray = new int[partsOfValues.length];
				
				for (int j = 0; j < partsOfValues.length; j++)
					if (!partsOfValues[j].isEmpty())
						newArray[j] = new IntegerTypeParser().parse(partsOfValues[j]);
				
				finalArray[i] = newArray; 
			}
			
			return finalArray;
		}
		
		
		else if (varType.getType().equals(Integer[][].class)) {
			final String[] arraySplit = getSplittedValues(enteredParameter);
			
			final Integer[][] finalArray = new Integer[arraySplit.length][];
			
			for (int i = 0; i < arraySplit.length; i++) {
				// Získám si konkrétní hodnoty v poli:
				final String[] partsOfValues = getSeparatedValues(arraySplit[i]);

				final Integer[] newArray = new Integer[partsOfValues.length];
				
				for (int j = 0; j < partsOfValues.length; j++)
					if (!partsOfValues[j].isEmpty())
						newArray[j] = new IntegerTypeParser().parse(partsOfValues[j]);
				
				finalArray[i] = newArray; 
			}
			
			return finalArray;
		}
		
					
		else if (varType.getType().equals(long[][].class)) {
			final String[] arraySplit = getSplittedValues(enteredParameter);
			
			final long[][] finalArray = new long[arraySplit.length][];
			
			for (int i = 0; i < arraySplit.length; i++) {
				// Získám si konkrétní hodnoty v poli:
				final String[] partsOfValues = getSeparatedValues(arraySplit[i]);

				final long[] newArray = new long[partsOfValues.length];
				
				for (int j = 0; j < partsOfValues.length; j++)
					if (!partsOfValues[j].isEmpty())
						newArray[j] = new LongTypeParser().parse(partsOfValues[j]);
				
				finalArray[i] = newArray; 
			}
			
			return finalArray;
		}
		
		
		else if (varType.getType().equals(Long[][].class)) {
			final String[] arraySplit = getSplittedValues(enteredParameter);
			
			final Long[][] finalArray = new Long[arraySplit.length][];
			
			for (int i = 0; i < arraySplit.length; i++) {
				// Získám si konkrétní hodnoty v poli:
				final String[] partsOfValues = getSeparatedValues(arraySplit[i]);

				final Long[] newArray = new Long[partsOfValues.length];
				
				for (int j = 0; j < partsOfValues.length; j++)
					if (!partsOfValues[j].isEmpty())
						newArray[j] = new LongTypeParser().parse(partsOfValues[j]);
				
				finalArray[i] = newArray; 
			}
			
			return finalArray;
		}
		
		
		else if (varType.getType().equals(float[][].class)) {
			final String[] arraySplit = getSplittedValues(enteredParameter);
			
			final float[][] finalArray = new float[arraySplit.length][];
			
			for (int i = 0; i < arraySplit.length; i++) {
				// Získám si konkrétní hodnoty v poli:
				final String[] partsOfValues = getSeparatedValues(arraySplit[i]);

				final float[] newArray = new float[partsOfValues.length];
				
				for (int j = 0; j < partsOfValues.length; j++)
					if (!partsOfValues[j].isEmpty())
						newArray[j] = new FloatTypeParser().parse(partsOfValues[j]);
				
				finalArray[i] = newArray; 
			}
			
			return finalArray;
		}
		
		
		else if (varType.getType().equals(Float[][].class)) {
			final String[] arraySplit = getSplittedValues(enteredParameter);
			
			final Float[][] finalArray = new Float[arraySplit.length][];
			
			for (int i = 0; i < arraySplit.length; i++) {
				// Získám si konkrétní hodnoty v poli:
				final String[] partsOfValues = getSeparatedValues(arraySplit[i]);

				final Float[] newArray = new Float[partsOfValues.length];
				
				for (int j = 0; j < partsOfValues.length; j++)
					if (!partsOfValues[j].isEmpty())
						newArray[j] = new FloatTypeParser().parse(partsOfValues[j]);
				
				finalArray[i] = newArray; 
			}
			
			return finalArray;
		}
					
		
		else if (varType.getType().equals(double[][].class)) {
			final String[] arraySplit = getSplittedValues(enteredParameter);
			
			final double[][] finalArray = new double[arraySplit.length][];
			
			for (int i = 0; i < arraySplit.length; i++) {
				// Získám si konkrétní hodnoty v poli:
				final String[] partsOfValues = getSeparatedValues(arraySplit[i]);

				final double[] newArray = new double[partsOfValues.length];
				
				for (int j = 0; j < partsOfValues.length; j++)
					if (!partsOfValues[j].isEmpty())
						newArray[j] = new DoubleTypeParser().parse(partsOfValues[j]);
				
				finalArray[i] = newArray; 
			}
			
			return finalArray;
		}
		
		
		else if (varType.getType().equals(Double[][].class)) {
			final String[] arraySplit = getSplittedValues(enteredParameter);
			
			final Double[][] finalArray = new Double[arraySplit.length][];
			
			for (int i = 0; i < arraySplit.length; i++) {
				// Získám si konkrétní hodnoty v poli:
				final String[] partsOfValues = getSeparatedValues(arraySplit[i]);

				final Double[] newArray = new Double[partsOfValues.length];
				
				for (int j = 0; j < partsOfValues.length; j++)
					if (!partsOfValues[j].isEmpty())
						newArray[j] = new DoubleTypeParser().parse(partsOfValues[j]);
				
				finalArray[i] = newArray; 
			}
			
			return finalArray;
		}
		
		
		
		
		else if (varType.getType().equals(char[][].class)) {
			// Oddělím si pole od hranatých závorek:
			final String textWithoutBracket = getTextWithoutBracket(enteredParameter);
			
			// jednotlivá pole si rozdělím dle zavírací špičaté závorky - krom poslední to řeším zvlášt, viz kód níže:
			final String[] splittedTextByRegEx = textWithoutBracket.split(REG_EX_FOR_SPLIT_OF_TEXT_AND_CHAR_ARRAY);
			
			// finální pole k vrácení po naplnění ho hodnotami:
			final char[][] finalArray = new char[splittedTextByRegEx.length][];
			
			
			for (int i = 0; i < splittedTextByRegEx.length; i++) {
				final String[] partsOfValues = getSeparatedValues(splittedTextByRegEx[i]);
				
				// pole, do kterého vložím naparsované hodnoty a toto pole vložím do výsledného pole:
				final char[] newArray = new char[partsOfValues.length];
				
				
				for (int j = 0; j < partsOfValues.length; j++) {
					if (!partsOfValues[j].isEmpty()) {
						// Zde se nejedná o prázdný znak, musím si vzít text mezi jednoduchými uvozovkami:
						final String textBetweenQuotes;
						
						// Pokud se jedná o poslední zadané pole, tak ho musím rozdělit "zvlášť" - dle regulárního výrazu se nerozdělí poslední
						// uzavírací špičatá závoka:
						if (i == splittedTextByRegEx.length - 1 && j == partsOfValues.length - 1) {
							// Zde se jedná o poslední prvek v poli a dle regulárního výrazu pro rozdělení se tam bude nacházet
							// následující hodnota:  ] - Poslední pole nebude rozdělené dle uzavřené špičaté závorky:
							final String textWithoutAngleBrackets = partsOfValues[j].substring(0, partsOfValues[j].indexOf('}'));
							
							// Zde potřebuji znovu otestovat, zda se v tom psledním poli nějaký znak nachází nebo ne,
							// pokud ne, tak mohu pokračovat na další iteraci - pole je prázdné není třeba nic testovat, "jde se dál":
							if (!textWithoutAngleBrackets.isEmpty())
								textBetweenQuotes = textWithoutAngleBrackets.substring(textWithoutAngleBrackets.indexOf('\'') + 1, textWithoutAngleBrackets.lastIndexOf('\''));
							
							else continue;
						}
										
						// Zde se nejedná o poslední pole, tak ho mohu normálně rozebrat z uvozovek:
						else textBetweenQuotes = partsOfValues[j].substring(partsOfValues[j].indexOf('\'') + 1, partsOfValues[j].lastIndexOf('\''));
						
						// Zde se pokudím přetypovat získanou hodnotu na datový typ znak:
						newArray[j] = textBetweenQuotes.charAt(0);
					}
				}	
				
				// přidám pole naplnění hodnotami do výsledného pole:
				finalArray[i] = newArray;
			}	
			
			// vrátím výsledné / zadané pole uživatelem:
			return finalArray;
		}
		
		
		else if (varType.getType().equals(Character[][].class)) {
			// Oddělím si pole od hranatých závorek:
			final String textWithoutBracket = getTextWithoutBracket(enteredParameter);
			
			// jednotlivá pole si rozdělím dle zavírací špičaté závorky - krom poslední to řeším zvlášt, viz kód níže:
			final String[] splittedTextByRegEx = textWithoutBracket.split(REG_EX_FOR_SPLIT_OF_TEXT_AND_CHAR_ARRAY);
			
			// finální pole k vrácení po naplnění ho hodnotami:
			final Character[][] finalArray = new Character[splittedTextByRegEx.length][];
			
			
			for (int i = 0; i < splittedTextByRegEx.length; i++) {
				final String[] partsOfValues = getSeparatedValues(splittedTextByRegEx[i]);
				
				// pole, do kterého vložím naparsované hodnoty a toto pole vložím do výsledného pole:
				final Character[] newArray = new Character[partsOfValues.length];
				
				
				for (int j = 0; j < partsOfValues.length; j++) {
					if (!partsOfValues[j].isEmpty()) {
						// Zde se nejedná o prázdný znak, musím si vzít text mezi jednoduchými uvozovkami:
						final String textBetweenQuotes;
						
						// Pokud se jedná o poslední zadané pole, tak ho musím rozdělit "zvlášť" - dle regulárního výrazu se nerozdělí poslední
						// uzavírací špičatá závoka:
						if (i == splittedTextByRegEx.length - 1 && j == partsOfValues.length - 1) {
							// Zde se jedná o poslední prvek v poli a dle regulárního výrazu pro rozdělení se tam bude nacházet
							// následující hodnota:  ] - Poslední pole nebude rozdělené dle uzavřené špičaté závorky:
							final String textWithoutAngleBrackets = partsOfValues[j].substring(0, partsOfValues[j].indexOf('}'));
							
							// Zde potřebuji znovu otestovat, zda se v tom psledním poli nějaký znak nachází nebo ne,
							// pokud ne, tak mohu pokračovat na další iteraci - pole je prázdné není třeba nic testovat, "jde se dál":
							if (!textWithoutAngleBrackets.isEmpty())
								textBetweenQuotes = textWithoutAngleBrackets.substring(textWithoutAngleBrackets.indexOf('\'') + 1, textWithoutAngleBrackets.lastIndexOf('\''));
							
							else continue;
						}
										
						// Zde se nejedná o poslední pole, tak ho mohu normálně rozebrat z uvozovek:
						else textBetweenQuotes = partsOfValues[j].substring(partsOfValues[j].indexOf('\'') + 1, partsOfValues[j].lastIndexOf('\''));
						
						// Zde se pokudím přetypovat získanou hodnotu na datový typ znak:
						newArray[j] = textBetweenQuotes.charAt(0);
					}
				}	
				
				// přidám pole naplnění hodnotami do výsledného pole:
				finalArray[i] = newArray;
			}	
			
			// vrátím výsledné / zadané pole uživatelem:
			return finalArray;
		}
		
		
		else if (varType.getType().equals(boolean[][].class)) {
			final String[] arraySplit = getSplittedValues(enteredParameter);
			
			final boolean[][] finalArray = new boolean[arraySplit.length][];
			
			for (int i = 0; i < arraySplit.length; i++) {
				// Získám si konkrétní hodnoty v poli:
				final String[] partsOfValues = getSeparatedValues(arraySplit[i]);

				final boolean[] newArray = new boolean[partsOfValues.length];
				
				for (int j = 0; j < partsOfValues.length; j++)
					if (!partsOfValues[j].isEmpty())
						newArray[j] = new BooleanTypeParser().parse(partsOfValues[j]);
				
				finalArray[i] = newArray; 
			}
			
			return finalArray;
		}
		
		
		else if (varType.getType().equals(Boolean[][].class)) {
			final String[] arraySplit = getSplittedValues(enteredParameter);
			
			final Boolean[][] finalArray = new Boolean[arraySplit.length][];
			
			for (int i = 0; i < arraySplit.length; i++) {
				// Získám si konkrétní hodnoty v poli:
				final String[] partsOfValues = getSeparatedValues(arraySplit[i]);

				final Boolean[] newArray = new Boolean[partsOfValues.length];
				
				for (int j = 0; j < partsOfValues.length; j++)
					if (!partsOfValues[j].isEmpty())
						newArray[j] = new BooleanTypeParser().parse(partsOfValues[j]);
				
				finalArray[i] = newArray; 
			}
			
			return finalArray;
		}
		
		
		else if (varType.getType().equals(String[][].class)) {
			// Oddělím si pole od hranatých závorek:
			final String textWithoutBracket = getTextWithoutBracket(enteredParameter);
			
			// jednotlivá pole si rozdělím dle zavírací špičaté závorky - krom poslední to řeším zvlášt, viz kód níže:
			final String[] splittedTextByRegEx = textWithoutBracket.split(REG_EX_FOR_SPLIT_OF_TEXT_AND_CHAR_ARRAY);
			
			// finální pole k vrácení po naplnění ho hodnotami:
			final String[][] finalArray = new String[splittedTextByRegEx.length][];
			
			
			for (int i = 0; i < splittedTextByRegEx.length; i++) {
				final String[] partsOfValues = getSeparatedValues(splittedTextByRegEx[i]);
				
				// pole, do kterého vložím naparsované hodnoty a toto pole vložím do výsledného pole:
				final String[] newArray = new String[partsOfValues.length];
				
				
				for (int j = 0; j < partsOfValues.length; j++) {
					if (!partsOfValues[j].isEmpty()) {
						// Zde se nejedná o prázdný text, musím si vzít text mezi dvojitymi uvozovkami:
						final String textBetweenQuotes;

						// Pokud se jedná o poslední zadané pole, tak ho musím rozdělit "zvlášť" - dle regulárního výrazu se nerozdělí poslední
						// uzavírací špičatá závoka:
						if (i == splittedTextByRegEx.length - 1 && j == partsOfValues.length - 1) {
							// Zde se jedná o poslední prvek v poli a dle regulárního výrazu pro rozdělení se tam bude nacházet
							// následující hodnota:  } - Poslední pole nebude rozdělené dle uzavřené špičaté závorky:
							final String textWithoutAngleBrackets = partsOfValues[j].substring(0, partsOfValues[j].indexOf('}'));

							// Zde potřebuji znovu otestovat, zda se v tom psledním poli nějaký text nachází nebo ne,
							// pokud ne, tak mohu pokračovat na další iteraci - pole je prázdné není třeba nic testovat, "jde se dál":
							if (!textWithoutAngleBrackets.isEmpty())
								textBetweenQuotes = textWithoutAngleBrackets.substring(textWithoutAngleBrackets.indexOf('"') + 1, textWithoutAngleBrackets.lastIndexOf('"'));

							else continue;
						}

						// Zde se nejedná o poslední pole, tak ho mohu normálně rozebrat z uvozovek:
						else textBetweenQuotes = partsOfValues[j].substring(partsOfValues[j].indexOf('"') + 1, partsOfValues[j].lastIndexOf('"'));

						// Zde se pokudím přetypovat získanou hodnotu na datový typ String:
						newArray[j] = String.valueOf(textBetweenQuotes);
					}
				}
				
				// přidám pole naplnění hodnotami do výsledného pole:
				finalArray[i] = newArray;
			}	
			
			// vrátím výsledné / zadané pole uživatelem:
			return finalArray;
		}
		
		
		return null;
	} 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která převede text zadaný od uživatele (enteredParameter) na kolekci
	 * příslušného typu (například ArrayList, LinkedList apod. -> pro podporované
	 * typy).
	 * 
	 * @param varType
	 *            - Parametr metody, ze kterého se pozná datový typ kolekce a
	 *            zároveň i jaký typ kolekce se má vrátit - ArrayList, Linked list
	 *            atd. pro podporované typy.
	 * 
	 * @param enteredParameter
	 *            - zadaný text od uživatele v příslušném dialogu pro zavolání
	 *            metody nebo konstruktoru.
	 * 
	 * @return null pokud nebude rozpoznána kolekce nebo datový typ, jinak příslušná
	 *         kolekce s naparsovanými hodnotami z textu enteredParameter na
	 *         přslušný datový typ kolekce.
	 */
	private static Object getCollectionFromEnteredParameter(final Parameter varType, final String enteredParameter) {
		/*
		 * Zjistím si datový typ hodnot, které lze vkládat do Listu - kolekce.
		 */
		final DataTypeOfList dataTypeOfListValues = ReflectionHelper.getDataTypeOfList(varType);

		/*
		 * Pokud se nejedná o specifický datový typ, pak není jasné, jaká kolekce by se měla vytvořit, proto je možné
		 * skončit.
		 */
		if (dataTypeOfListValues.getDataTypeOfListEnum() != DataTypeOfListEnum.SPECIFIC_DATA_TYPE)
			return null;

		/*
		 * Výše je zjištěno, že se jedná o specifický datový typ Listu, ale přetypování hodnot, které následuje je
		 * povoleno / podporováno pouze pro vybrané základní datové typy jazyka Java. Proto je třeba zjistit, zda je
		 * hodnota Listu instance třídy "Class<?>". Pokud je to null, pak se jedná o generický datový typ nebo
		 * nedefinovaný typ Listu a pokud to není instance třídy Class<?>, pak se jedná o nepodporovaný typ na
		 * přetypování hodnot. Proto je možné skončit.
		 */
		if (!(dataTypeOfListValues.getDataType() instanceof Class<?>))
			return null;

		/*
		 * List, do kterého se mi vloží přetypované zadané hodnoty na list hodnot typu
		 * varType - jeden z datových typů Javy.
		 */
		final List<?> list = getListFromParameter((Class<?>) dataTypeOfListValues.getDataType(), enteredParameter);

		
		/*
		 * Pokud se vrátí null, pak ani nemá smysl vytvářet potřebnou kolekci a rovnou
		 * vrátím null, nic jiného snad ani nejde, když nejsou potřebná data do kolekce.
		 *
		 * (Ale null zde testovat nemusím, nemělo by nastat.)
		 */

		
		
		
		/*
		 * Nyní pouze otestuji o jaký typ kolekce se jedná a dle toho jej vytvořím,
		 * vložím do té nové kolekce výše získané přetypované hodnoty z textového pole
		 * od uživatele a vrátím jej v příslušném typu kolekce.
		 */
		if (varType.getType().equals(ArrayList.class))
			return new ArrayList<>(list);

		else if (varType.getType().equals(LinkedList.class))
			return new LinkedList<>(list);

		else if (varType.getType().equals(Vector.class))
			return new Vector<>(list);

		else if (varType.getType().equals(CopyOnWriteArrayList.class))
		    return new CopyOnWriteArrayList<>(list);

		else if (varType.getType().equals(Stack.class))
			/*
			 * V listu list jsou již přetypované hodnoty získané v textu od uživatele, tak
			 * tento list pouze předám do instance třídy MyStack, kde se v příslušném
			 * konstruktoru naplní příslušný Stack daty získanými od uživatele (data v listu
			 * list.) a instance toho Stacku (třída MyStack) se vrátí aby se předala do
			 * parametru metody nebo konstruktoru pro zavolání.
			 */
			return new GenericStack<>(list);

		return null;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zadaný text od uživatele v dialogu - enterfedParameter převede
	 * na jeden z listů a ten vrátí. Pokud se nerozpozná datový typ - nemělo by
	 * nastat, pak se vrátí prázdný list, nechtěl jsem dávat null hodnotu. Aby někde
	 * nedošlo k výjimce (kdyby náhodou) apod.
	 * 
	 * @param dataTypeOfListValues
	 *            - měl by to být jeden z datových typů Javy -> datový typ kolekce,
	 *            resp. datový typ hodnot, které se mají vkládat do kolekce. Na
	 *            tento datový typ se přetypují zadané hodnoty.
	 * 
	 * @param enteredParameter
	 *            - Zadaný text od uživatele v dialogu.
	 * 
	 * @return list s přetypovanými hodnotami získanými od uživatele nebo prázdný
	 *         list typu ArrayList.
	 */
	private static List<?> getListFromParameter(final Class<?> dataTypeOfListValues, final String enteredParameter) {
		// Dle výše uvedené proměnné vím, jaký typ Listu mám vytvořit

		if (dataTypeOfListValues.equals(Byte.class)) {
			// Zde mám vytvořit List s hodnotami typu Byte:
			final List<Byte> list = new ArrayList<>();
			
			
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);
			
			// naparsuji hodnoty a vložím jej do listu, který na konec vrátím:
			for (final String s : dividedByComma)
				// Musím zde otestovat, zda se nejedná o prázdný text, v takovém případě ho nechci formátovat,
				// tato možnost by znamela, že se jedná o prazdný list: (stejně tak pro následující listy:)
				if (!s.isEmpty())
					list.add(new ByteTypeParser().parse(removeAllSpace(s)));
			
			return list;
		}
		
		
		else if (dataTypeOfListValues.equals(Short.class)) {
			// Zde mám vytvořit List s hodnotami typu Integer:
			final List<Short> list = new ArrayList<>();
			
			
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);
			
			// naparsuji hodnoty a vložím jej do listu, který na konec vrátím:
			for (final String s : dividedByComma)
				if (!s.isEmpty())
					list.add(new ShortTypeParser().parse(removeAllSpace(s)));
			
			return list;
		}
		
		
		else if (dataTypeOfListValues.equals(Integer.class)) {
			// Zde mám vytvořit List s hodnotami typu Integer:
			final List<Integer> list = new ArrayList<>();
			
			
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);
			
			// naparsuji hodnoty a vložím jej do listu, který na konec vrátím:
			for (final String s : dividedByComma)
				if (!s.isEmpty())
					list.add(new IntegerTypeParser().parse(removeAllSpace(s)));
			
			return list;
		}
		
		
		else if (dataTypeOfListValues.equals(Long.class)) {
			// Zde mám vytvořit List s hodnotami typu Long:
			final List<Long> list = new ArrayList<>();
			
			
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);
			
			// naparsuji hodnoty a vložím jej do listu, který na konec vrátím:
			for (final String s : dividedByComma)
				if (!s.isEmpty())
					list.add(new LongTypeParser().parse(removeAllSpace(s)));
			
			return list;
		}
		
		
		else if (dataTypeOfListValues.equals(Float.class)) {
			// Zde mám vytvořit List s hodnotami typu Float:
			final List<Float> list = new ArrayList<>();
			
			
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);
			
			// naparsuji hodnoty a vložím jej do listu, který na konec vrátím:
			for (final String s : dividedByComma)
				if (!s.isEmpty())
					list.add(new FloatTypeParser().parse(removeAllSpace(s)));
			
			return list;
		}
		
		
		else if (dataTypeOfListValues.equals(Double.class)) {
			// Zde mám vytvořit List s hodnotami typu Double:
			final List<Double> list = new ArrayList<>();
			
			
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);
			
			// naparsuji hodnoty a vložím jej do listu, který na konec vrátím:
			for (final String s : dividedByComma)
				if (!s.isEmpty())
					list.add(new DoubleTypeParser().parse(removeAllSpace(s)));
			
			return list;
		}
		
		
		else if (dataTypeOfListValues.equals(Character.class)) {
			// Zde mám vytvořit List s hodnotami typu Character:
			final List<Character> list = new ArrayList<>();
			
			// Vezmu si text v závorkách zadaný v poli od uživatelele:
			final String textWithoutBracket = getTextWithoutBracket(enteredParameter);
			
			// naparsuji hodnoty a vložím jej do listu, který na konec vrátím:
			final String[] dividedByQuotes = textWithoutBracket.split("'");
			
			for (int i = 1; i < dividedByQuotes.length; i += 2)
				if (!dividedByQuotes[i].isEmpty())
					list.add(new CharacterTypeParser().parse(dividedByQuotes[i]));
			
			return list;
		}
		
		
		else if (dataTypeOfListValues.equals(Boolean.class)) {
			// Zde mám vytvořit List s hodnotami typu Boolean:
			final List<Boolean> list = new ArrayList<>();
			
			
			final String[] dividedByComma = dividedTextByComaForListOrArray(enteredParameter);
			
			// naparsuji hodnoty a vložím jej do listu, který na konec vrátím:
			for (final String s : dividedByComma)
				if (!s.isEmpty())
					list.add(new BooleanTypeParser().parse(removeAllSpace(s)));
			
			return list;
		}
		
		
		else if (dataTypeOfListValues.equals(String.class)) {
			// Zde mám vytvořit List s hodnotami typu String:
			final List<String> list = new ArrayList<>();
			
			// Vezmu si text v závorkách zadaný v poli od uživatelele:
			final String textWithoutBracket = getTextWithoutBracket(enteredParameter);
			
			// naparsuji hodnoty a vložím jej do listu, který na konec vrátím:
			final String[] divided = textWithoutBracket.split("\"");
			
			for (int i = 1; i < divided.length; i += 2)
				if (!divided[i].isEmpty())
					list.add(String.valueOf(divided[i]));
			
			return list;
		}
		
		return new ArrayList<>();
	}
	
	
	
	
	
	
	
	
	
	
	
	



	
	
	


	
	/**
	 * Metoda, která odebere hranaté závorky, a rozdělí zbylý text dle uzavírací
	 * spičaté závorky, pro zjištění počtu polí a tuto pole vrátí
	 * 
	 * @param valueFromTxtField
	 *            - text z textového pole z dialogu zadaným uživatelem
	 * 
	 * @return jednorozměrné pole s oddělenými poly dle uzavírací špičaté závorky:
	 */
	private static String[] getSplittedValues(final String valueFromTxtField) {
		// Vezmu si text bez hranatých závorek:
		final String textWithoutBracket = getTextWithoutBracket(valueFromTxtField);

		// Vezmu si jednotlivá "pole", která jsou určena spičatými závorkami:
		// Note: Velikost následujícího pole je i počet polí ve výsledném poli:
		return textWithoutBracket.split("}");
	}







	
	


	
	/**
	 * Metoda, která vrát text uvnitř hranatých závorek - ze syntaxe pro List a pole
	 * Syntaxe: [ X, X, X, ... ] - tak vrátí text uvnitř závorek, resp. text bez
	 * těch závorek
	 * 
	 * @param valueFromTxtField
	 *            - hodnota z textového pole z dialogu zadaná uživatelem
	 * 
	 * @return text bez závorek, resp. text uvnitř závorek
	 */
	private static String getTextWithoutBracket(final String valueFromTxtField) {
		return valueFromTxtField
				.substring(valueFromTxtField.indexOf('[') + 1, valueFromTxtField.lastIndexOf(']')).trim();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda z textu, který se vezme z textového pole z dialogu rozdělí text bez
	 * závorek, tj. pro syntaxi pro zadání hodnot do jednorozměrného pole nebo
	 * Listu, tak z toho si vezme text mezi hranatými zývorkami a rodělí je dle
	 * desetinné čárky, pro oddělení jednotlivých hodnot
	 * 
	 * @param valueFromTxtField
	 *            - hodnota z textového pole z dialogu pro zadání hodnoty Listu nebo
	 *            jednorozměrného pole
	 * 
	 * @return jednorozměrné pole hodnoty rozdělených dle desetinné čárky a bez
	 *         hranatých závorek - již rozdělené hodnoty
	 */
	private static String[] dividedTextByComaForListOrArray(final String valueFromTxtField) {
		final String textWithoutBracket = getTextWithoutBracket(valueFromTxtField);

		return textWithoutBracket.split(",");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zkusí přetypovat hodnoty na datový typ parametru
	 * metody či konstruktoru, pokud nastave vyjímka je zachytávána v místě, kde je
	 * tato metoda volána a jedná se o vyjímku, že nejde zadaná hodnota přetypovat
	 * na potřebný datový typ parametru, proto se následně zobrazí chybový zpráva v
	 * dialogu o tom, že byly zadány chybně hodnoty
	 * 
	 * @param text
	 *            - text zadaný v poli pro hodnotu parametru
	 * 
	 * @param dataTypeOfList
	 *            - Výčtový typ, dle kterého metoda pozná, na jaký datový typ
	 *            ohodnoty se má pokusit data přetypovat
	 * 
	 * @throws NumberFormatException
	 *             - vyjímka, která nastane, pokud nepůjde nějaký zadaná hodnota v
	 *             poli přetypovat na zadanou hodnotu coby datový typ parametru
	 *             metody nebo konstruktoru, následně se zobrazí hláška v dialogu
	 * 
	 * @throws StringIndexOutOfBoundsException
	 *             - vyjímka, která nastane pokud uživatel zadává uvozovky coby znak
	 *             a ještě mezi nimi neni žádná hodnota, pak se na indexu nula coby
	 *             charAt(0) v daném textu ještě není, pak nastane daná vyjímka,
	 *             kterou je třeba zachytit
	 */
	private static void testValuesForListAnd1Array(final String text, final DataTypeOfJTextField dataTypeOfList)
			throws NumberFormatException, StringIndexOutOfBoundsException {
		final String textWithoutBracket = getTextWithoutBracket(text);
		
		// Proměnná, do které vložím hodnoty rozdělené dle desetinné čárky, je to rozdělené zde, protože je následující pole
		// využíváné u číselných hodnot a logických hodnot - true a false (boolean):
		final String[] dividedByComma = textWithoutBracket.split(",");
		
		// Note: musím zde parsovat hodnoty "takto", protože chci aby zde nastala vyjímka a dle toho poznám, zda zadaná hodnota
		// odpovídá potřenému parametru, kdybych hodnoty parsoval někde jinde, napríklad v ByteTypeParser, apod. tak by nastala vyjímka
		// "tam" - v té třídě a něco by vrátla ať už tu hodnotu nebo ne, ale to nechci:
		
		switch (dataTypeOfList) {
		case LIST_AND_ARRAY_BYTE:
			// Zde nemusím řešit uvozovky jako u textu, ani desetinné tečky, takže si rozdělím text dle desetinných
			// čárek, odeberu všechny mezery a pokusím se zbytek textu přetypovat na datový typ Byte:				
			
			// "Projedu" celé pole odeberu mezry a zkusím to přetypovat, pokud se to povede, pak je to OK, jinak se zobrazí 
			// chybová hláška uživateli, že zadal špatný formát hodnot:
			for (final String s : dividedByComma)
				// Zde potřebuji otestovat, zda se nejedná o "prázdnou hodnotu", pak se nemá nic konstrolovat, pouze mezery:
				if (!s.isEmpty())
					Byte.parseByte(removeAllSpace(s));
				
			break;
			
			
		case LIST_AND_ARRAY_SHORT:
			// Podobně jako výše se pokusím přetypovat hodnoty na datový typ Short:
			for (final String s : dividedByComma)
				if (!s.isEmpty())
					Short.parseShort(removeAllSpace(s));
			break;
			
			
		case LIST_AND_ARRAY_INTEGER:
			// Zde se pokusím přetypovat hodnoty na typ Integer:
			for (final String s : dividedByComma)
				if (!s.isEmpty())
					Integer.parseInt(removeAllSpace(s));
			break;
			
			
		case LIST_AND_ARRAY_LONG:
			for (final String s : dividedByComma)
				if (!s.isEmpty())
					Long.parseLong(removeAllSpace(s));
			break;
			
			
		case LIST_AND_ARRAY_FLOAT:
			for (final String s : dividedByComma)
				if (!s.isEmpty())
					Float.parseFloat(removeAllSpace(s));
			break;
			
			
		case LIST_AND_ARRAY_DOUBLE:
			for (final String s : dividedByComma)
				if (!s.isEmpty())
					Double.parseDouble(removeAllSpace(s));
			break;
			
			
		case LIST_AND_ARRAY_CHARACTER:
			// Zde se jedná o podobný princip jako u textu, akorát místo dvojitých uvozovek jsou zde jednoduché ('X')
			// takže text rozeberu dle jednoduchých uvozovek a získané hodnoty se pokusím naparsovat na datový typ Character:
			
			final String[] dividedByQuotes = textWithoutBracket.split("'");
			
			// Zde může při parsování nastat vyjímka java.lang.StringIndexOutOfBoundsException, proto ji musím dále také zachytávat
			// jedná se o vyjímku, když uživatel teprve zadává uvozovky, a zadá pouze dvě a mezi nimi není žádná hodnota, tak nastane
			// zmíněná vyjímka:
			for (int i = 1; i < dividedByQuotes.length; i += 2)
				if (!dividedByQuotes[i].isEmpty())
					Character.valueOf(dividedByQuotes[i].charAt(0));
			break;
			
			
		case LIST_AND_ARRAY_BOOLEAN:
			for (final String s : dividedByComma)
				if (!s.isEmpty())
					Boolean.parseBoolean(removeAllSpace(s));
			break;
			
			
		case LIST_AND_ARRAY_STRING:
			// Zde se jedná o zjistění, zda jdou zadané hodnoty přetypovat na String
			// takže si vezmu všechny hodnoty v uvozovkách a zkusím je přetypovat na String, pokud dojde k chybě (němělo by), tak 
			// vyskočí vyjímka, která je zachytávána v místě, kde se tato metoda volá
			
			// "some text", "some text2" - beru vždy hodnoty před lichou uvozovkou - dle splitu:
			final String[] divided = textWithoutBracket.split("\"");
			
			for (int i = 1; i < divided.length; i += 2)
				if (!divided[i].isEmpty())
					String.valueOf(divided[i]);
			break;

		default:
			break;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která "otestuje", zda jsou hodnoty v příslušném textovém poli v
	 * dialogu zadány ve "správném" rozrahu a syntaxi.
	 * 
	 * Metoda se pokudsi hodnoty z textového pole přetypovat na příslušné datové
	 * typy, pokud se vše povede, není co řešit je to OK, ale pokud se nějaká
	 * hodnota nepovede přetypovat na příslušný datový typ, nastane vyjímka
	 * NumberFormatException - nejčastěji, a do dialogu se do okna pro chybové
	 * hlášky zobrazí informace o tom, že je v nějakém poli zadána špatně hodnota
	 * 
	 * @param text
	 *            - text z textového pole z dialogu zadaným uživatelem
	 * 
	 * @param dataTypeOfArray
	 *            - Výčtový typ, který značí datový typ, na který se má pokusit
	 *            přetypovat hodnoty z pole - v proměnné výše (text)
	 * 
	 * @throws NumberFormatException
	 *             - vyjímka, která může nastat při parsování textu na nějaký datový
	 *             typ, dle toho poznám, že je nějaká hodnota ve špatné syntaxi či
	 *             rozsahu, apod.
	 */
	private static void testValuesTwoDimensionalArray(final String text, final DataTypeOfJTextField dataTypeOfArray)
			throws NumberFormatException {
		final String[] arraySplit = getSplittedValues(text);
		
		
		
		switch (dataTypeOfArray) {
		case TWO_DIMENSIONAL_ARRAY_BYTE:
			// Postup:
			// Prohledám pole arraySplit, kde jsou jednotlivá pole rozdělená dle špičaté uzavírací závorky: '}'
			// Odstraním otevírací závorku pro každé pole a rozdělíám dle desetinné čárky, čímž oddělím / zjistím jednotlivé hodnoty

            for (final String anArraySplit : arraySplit) {
                // Získám si konkrétní hodnoty v poli:
                final String[] partsOfValues = getSeparatedValues(anArraySplit);

                // Otestuji jednotlivé hodnoty, zda to není pouze prázdný text (v případě prázdného pole), a zkusím je
                // přetypovat na konkrétní datový typ:
                for (final String partsOfValue : partsOfValues)
                    if (!partsOfValue.isEmpty())
                        Byte.parseByte(removeAllSpace(partsOfValue));
            }
			break;
			
			
			
			// Note: pro číselné a logické hodnoty opakuji stejný postup - cyklus jako výše, akorát se liší datové typy,
			// na který se přetypují logické hodnoty, proto u nich nebudou komentáře:
			
		case TWO_DIMENSIONAL_ARRAY_SHORT:
            for (final String anArraySplit : arraySplit) {
                final String[] partsOfValues = getSeparatedValues(anArraySplit);

                for (final String partsOfValue : partsOfValues)
                    if (!partsOfValue.isEmpty())
                        Short.parseShort(removeAllSpace(partsOfValue));
            }
			break;
			
			
			
		case TWO_DIMENSIONAL_ARRAY_INTEGER:
            for (final String anArraySplit : arraySplit) {
                final String[] partsOfValues = getSeparatedValues(anArraySplit);

                for (final String partsOfValue : partsOfValues)
                    if (!partsOfValue.isEmpty())
                        Integer.parseInt(removeAllSpace(partsOfValue));
            }
			break;
			
			
			
		case TWO_DIMENSIONAL_ARRAY_LONG:
            for (final String anArraySplit : arraySplit) {
                final String[] partsOfValues = getSeparatedValues(anArraySplit);

                for (final String partsOfValue : partsOfValues)
                    if (!partsOfValue.isEmpty())
                        Long.parseLong(removeAllSpace(partsOfValue));
            }
			break;
			
			
			
		case TWO_DIMENSIONAL_ARRAY_FLOAT:
            for (final String anArraySplit : arraySplit) {
                final String[] partsOfValues = getSeparatedValues(anArraySplit);

                for (final String partsOfValue : partsOfValues)
                    if (!partsOfValue.isEmpty())
                        Float.parseFloat(removeAllSpace(partsOfValue));
            }
			break;
			
			
			
		case TWO_DIMENSIONAL_ARRAY_DOUBLE:
            for (final String anArraySplit : arraySplit) {
                final String[] partsOfValues = getSeparatedValues(anArraySplit);

                for (final String partsOfValue : partsOfValues)
                    if (!partsOfValue.isEmpty())
                        Double.parseDouble(removeAllSpace(partsOfValue));
            }
			break;
			
			
			
		case TWO_DIMENSIONAL_ARRAY_CHARACTER:
			// Oddělím si pole od hranatých závorek:
			final String textWithoutBracket = getTextWithoutBracket(text);
			
			// jednotlivá pole si rozdělím dle zavírací špičaté závorky - krom poslední to řeším zvlášt, viz kód níže:
			final String[] splittedTextByRegEx = textWithoutBracket.split(REG_EX_FOR_SPLIT_OF_TEXT_AND_CHAR_ARRAY);
			
			
			for (int i = 0; i < splittedTextByRegEx.length; i++) {
				final String[] partsOfValues = getSeparatedValues(splittedTextByRegEx[i]);
				
				for (int j = 0; j < partsOfValues.length; j++) {
					if (!partsOfValues[j].isEmpty()) {
						// Zde se nejedná o prázdný znak, musím si vzít text mezi jednoduchými uvozovkami:
						final String textBetweenQuotes;
						
						// Pokud se jedná o poslední zadané pole, tak ho musím rozdělit "zvlášť" - dle regulárního výrazu se nerozdělí poslední
						// uzavírací špičatá závoka:
						if (i == splittedTextByRegEx.length - 1 && j == partsOfValues.length - 1) {
							// Zde se jedná o poslední prvek v poli a dle regulárního výrazu pro rozdělení se tam bude nacházet
							// následující hodnota:  } - Poslední pole nebude rozdělené dle uzavřené špičaté závorky,
							// tak si musím vzít text od zašátku až op tu závorku:
							final String textWithoutAngleBrackets = partsOfValues[j].substring(0, partsOfValues[j].indexOf('}'));
							
							// Zde potřebuji znovu otestovat, zda se v tom psledním poli nějaký znak nachází nebo ne,
							// pokud ne, tak mohu pokračovat na další iteraci - pole je prázdné není třeba nic testovat, "jde se dál":
							if (!textWithoutAngleBrackets.isEmpty())
								textBetweenQuotes = textWithoutAngleBrackets.substring(
										textWithoutAngleBrackets.indexOf('\'') + 1,
										textWithoutAngleBrackets.lastIndexOf('\''));

							else
								continue;
						}
												
						else
							textBetweenQuotes = partsOfValues[j].substring(partsOfValues[j].indexOf('\'') + 1,
									partsOfValues[j].lastIndexOf('\''));
						
						// Zde se pokudím přetypovat získanou hodnotu na datový typ znak:
						Character.valueOf(textBetweenQuotes.charAt(0));
					}
				}											
			}			
			break;
			
			
			
		case TWO_DIMENSIONAL_ARRAY_BOOLEAN:
            for (final String anArraySplit : arraySplit) {
                final String[] partsOfValues = getSeparatedValues(anArraySplit);

                for (final String partsOfValue : partsOfValues)
                    if (!partsOfValue.isEmpty())
                        Boolean.parseBoolean(removeAllSpace(partsOfValue));
            }
			break;
			
			
			
		case TWO_DIMENSIONAL_ARRAY_STRING:
			// Oddělím si pole od hranatých závorek:
			final String textWithoutBracketString = getTextWithoutBracket(text);
			
			// jednotlivá pole si rozdělím dle zavírací špičaté závorky - krom poslední to řeším zvlášt, viz kód níže:
			final String[] splittedTextByRegExString = textWithoutBracketString.split(REG_EX_FOR_SPLIT_OF_TEXT_AND_CHAR_ARRAY);
			
			
			for (int i = 0; i < splittedTextByRegExString.length; i++) {
				final String[] partsOfValues = getSeparatedValues(splittedTextByRegExString[i]);
				
				for (int j = 0; j < partsOfValues.length; j++) {
					if (!partsOfValues[j].isEmpty()) {
						// Zde se nejedná o prázdný text, musím si vzít text mezi dvojitými uvozovkami:
						final String textBetweenQuotes;
						
						// Pokud se jedná o poslední zadané pole, tak ho musím rozdělit "zvlášť" - dle regulárního výrazu se nerozdělí poslední
						// uzavírací špičatá závoka:
						if (i == splittedTextByRegExString.length - 1 && j == partsOfValues.length - 1) {
							// Zde se jedná o poslední prvek v poli a dle regulárního výrazu pro rozdělení se tam bude nacházet
							// následující hodnota:  } - Poslední pole nebude rozdělené dle uzavřené špičaté závorky,
							// tak si musím vzít text od zašátku až op tu závorku:
							final String textWithoutAngleBrackets = partsOfValues[j].substring(0, partsOfValues[j].indexOf('}'));
							
							// Zde potřebuji znovu otestovat, zda se v tom psledním poli nějaký znak nachází nebo ne,
							// pokud ne, tak mohu pokračovat na další iteraci - pole je prázdné není třeba nic testovat, "jde se dál":
							if (!textWithoutAngleBrackets.isEmpty())
								textBetweenQuotes = textWithoutAngleBrackets.substring(
										textWithoutAngleBrackets.indexOf('"') + 1,
										textWithoutAngleBrackets.lastIndexOf('"'));
							
							else continue;
						}
												
						else
							textBetweenQuotes = partsOfValues[j].substring(partsOfValues[j].indexOf('"') + 1,
									partsOfValues[j].lastIndexOf('"'));
						
						// Zde se pokudím přetypovat získanou hodnotu na datový typ znak:
						String.valueOf(textBetweenQuotes);
					}
				}
			}
			break;

		default:
			break;
		}
	}









	
	
	
	
	/**
	 * Metoda, která oddělí, resp. odebere první otevírací špičatou závorku, která
	 * značí začátek pole a vrátí zbytek - tj. hodnoty pole a ty dále oddělí dle
	 * desetinné čárky, abych tak získal jednotlivé hodnoty pole, které se mají
	 * přetypovat na nějaký datový typ, to je ale jiná část
	 * 
	 * @param arrayValue
	 *            - hodnota pole, resp text, jednoho jednorozměrného pole, ze
	 *            kterého se má odebrat závorka a rozdělít hodnoty dle desetinné
	 *            čártky
	 * 
	 * @return vrátí jednorozměrné pole s hodnotami oddělenými dle desetinné čárky,
	 *         tj. jednotlivé hodnoty, které mají být ve výsledném poli
	 */
	private static String[] getSeparatedValues(final String arrayValue) {
		// Odeberu si otevírací špičatou závorku abych mohl oddělit hodnoty, kde nejsou závorky:
		final String textWithoutAngleBracket = getTextWithoutFirstAngleBracket(arrayValue).trim();
		
		// Rozdělím si hodnoty dle desetinné čárky - na konkrétní hodnoty:
		return textWithoutAngleBracket.split(",");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí text bez první otevírací špičaté závorky pro dvojrozměrné
	 * pole:
	 * 
	 * Syntaxe: { value1, value2, ... tak z toho odebere první závorku, tato syntaxe
	 * nastane po rozdělení zadaného textu z dialogu pomocí metody split( } ) -
	 * abych určit jednotlivá pole a jejich cpočet
	 * 
	 * @param text
	 *            - text s první otevírací závorkou, kterou chci odebrat
	 * 
	 * @return text bez otevírací závorky, pouze hodnoty oddělené desetinnou čárkou
	 */
	private static String getTextWithoutFirstAngleBracket(final String text) {
		return text.substring(text.indexOf('{') + 1);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která odebere veškeré mezery z textu v parametru.
	 * 
	 * @param text
	 *            - text, ze kterého se mají odebrat všechny mezery a bíle znaky
	 * 
	 * @return text v parametru akorát bez bílých znaků
	 */
	private static String removeAllSpace(final String text) {
		return text.replaceAll("\\s", "");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří instanci komponenty JList, který slouží pro zobrazování
	 * uživateli chybovou hlášku s tím, které z polí pro zadání hodnoty parametru
	 * obsahuje chybu a informaci o tom, co má v daném poli být za hodnoty.
	 * 
	 * @return vrátí instancí komponenty Jpanel, který bude obsahovat výše zmíněný
	 *         JList s chybami, aby když bude metody či konstruktor obsahovat mnoho
	 *         parametrů, aby se nemusel přizpůsobit dialog, v některých přípdaěch
	 *         by to možná ani nešlo. Dále ten panel bude obsahovat komponentu
	 *         JCheckBox, která slouží pro zalamování textů, aby uživatel nemusel
	 *         scrolovat do stran (když budou dlouhé, resp. široké texty).
	 */
	protected final JPanel getCreatedErrorList() {
		/*
		 * Vytvořím si komponentu JCheckBox, dle které se pozná, zda se mají zalamovat
		 * texty nebo ne.
		 */
		final JCheckBox chcbWrapLyrics = new JCheckBox(txtChcbWrapLyrics);
		// Událost, aby se zalomili nebo nezalomily texty v JListu:
		chcbWrapLyrics.addActionListener(event -> setCellRendererToJList(chcbWrapLyrics.isSelected()));
		// Zarovnání chcb na střed dialogu:
		chcbWrapLyrics.setHorizontalAlignment(JCheckBox.CENTER);

		
		
		
		/*
		 * Vytvořím si list, který bude obsahovat veškeré chybové hlášky, které se
		 * aktuálně v dialogu "nachází", tj. veškeré informace o chybně zadaných
		 * hodnotách, nebo o tom, že metodu nebo konstruktor nelze kvůli nepodporovanému
		 * parametru zavolat apod.
		 */
		errorList = new JList<>(errorTextList.toArray());

		/*
		 * Aby šel vždy vybrat pouze jedna hodnota - resp. řádek, i když v tomto případě
		 * je to úplně jedno, účelem tohoto listu je pouze zobrazit data:
		 */
		errorList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		// Hodnoty se budou radit do sloupců:
//		errorList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		// Hodnoty se budou řadit pod sebe a pak vedle sebe, zhruby vždy na dva sloupce:
//		errorList.setLayoutOrientation(JList.VERTICAL_WRAP);
		
		errorList.setLayoutOrientation(JList.VERTICAL);

		/*
		 * Přidám posluchač na změnu velikosti, tj., když uživatel například změní
		 * velikosti dialogu, tak aby se tomu přizpůsobilo i zalomení textů (pokud je
		 * tak nastaveno).
		 */
		errorList.addComponentListener(new ComponentAdapter() {

			@Override
			public void componentResized(ComponentEvent e) {
				setCellRendererToJList(chcbWrapLyrics.isSelected());
			}
		});

		
		
		
		/*
		 * Komponenta, která umožní scrolování, pokud se v listu bude nacházet velké
		 * množství položek.
		 */
		final JScrollPane jspErrorList = new JScrollPane(errorList);

		jspErrorList.setPreferredSize(new Dimension(0, 125));
		jspErrorList.setBorder(BorderFactory.createTitledBorder(txtErrorForErrorList));

		
		
		
		/*
		 * Vytvořím si panel, který bude obsahovat chcb a ten list (errorList) s
		 * chybami.
		 */
		final JPanel pnlMistakes = new JPanel(new GridBagLayout());
		final GridBagConstraints gbcMistakes = createGbc();

		// Titulek ohraničení:
		pnlMistakes.setBorder(BorderFactory.createTitledBorder(txtPnlMistakesBorderTitle));

		// Přidání komponent do panelu
		setGbc(gbcMistakes, 0, 0, chcbWrapLyrics, pnlMistakes);

		setGbc(gbcMistakes, 1, 0, jspErrorList, pnlMistakes);

		// Vrátím panel s chcb pro zalamování textů a listem s chybami:
		return pnlMistakes;
	}
	
	
	


	
	
	
	
	
	
	/**
	 * Metoda, která nastaví komponentě errorList (typu JList) příslušný renderer
	 * pro to, aby se buď zalamovaly nebo nezalamovaly šíroké řádky / texy - položky
	 * v řádcích.
	 * 
	 * Jde o to, že když je text příliš dlouhý, resp. šírkoky pro zobrazení v
	 * dialogu, tak má uživatel možnost jej zalomit, aby ten text byl zobrazen na
	 * více řádků a uživatel tak nemusel posouvat posuvník.
	 * 
	 * @param wrap
	 *            - logická hodnota, dle které se značí, zda se budou nebo nebudou
	 *            zalamovat řádky v komponentě JList. Hodnota true značí, že se
	 *            budou zalamovat řádky a hodnota false, že se nebudou zalamovat
	 *            řádky v Jlistu list.
	 */
	protected final void setCellRendererToJList(final boolean wrap) {
		if (wrap)
			errorList.setCellRenderer(new ListCellRenderer(((errorList.getWidth() / 4) * 3)));

		else
			errorList.setCellRenderer(new DefaultListCellRenderer());
	}
	
	
	
	
	
	
	
	
	
	






	/**
	 * Metoda, která zobrazí chybové hlášení s parametry, které jsem zadány ve
	 * špatném formátu, tak se zobrazí informace o poli, ve kterém se nachází a
	 * informace o hodnotě, která se tam má zadat
	 */
	protected final void viewIncorrectParameters() {
		// Zde vypíšu chybovou hlášku s informacemi o chybně zadaných hodnotách
		String errorText = badFormatSomeParameterText + "\n";
		
		for (final String s : errorTextList)
			errorText += s + "\n";
		
		JOptionPane.showMessageDialog(this, errorText, badFormatSomeParameterTitle, JOptionPane.ERROR_MESSAGE);
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří "zalamovací" text (String), nastavím mu text v
	 * parametru a velikost u jaké se má zalamovat
	 * 
	 * @param text
	 *            - text, jaký má obsahovat
	 * 
	 * @param width
	 *            - šířka u jaké s má zalamovat
	 * 
	 * @return text s html syntaxí pro zalamování s definovaným textem
	 */
	protected static String getWrappedText(final String text, final int width) {
		return String.format("<html><div WIDTH=%d>%s</div><html>", width - 27, text);
	}
	

	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří syntaxi pro název ukázku parametrů, jaké se mají
	 * zadávat do metody či konstruktoru Syntaxe: název metody či konstruktoru
	 * (parameters)
	 * 
	 * 
	 * 
	 * Note:
	 * Musel jsem nahradil znaky: '<dataType>' za níže uvedené znaky (v metodě),
	 * protože se v html textu nezobrazují tagy "takto", je třeba je nahradit za
	 * příslušné symboly.
	 * 
	 * Zdroj pro speciální znaky v HTML:
	 * https://stackoverflow.com/questions/6817262/how-to-display-html-tags-as-plain-text
	 * 
	 * @param name
	 *            - název metody nebo konstruktoru
	 * 
	 * @param parameters
	 *            parametr, ze kterých si vytáhnu jejích datové typy
	 * 
	 * @return vytvořím výše uvedenou syntaxi a vrátím jí
	 */
	protected static String getParametersOfMethod(final String name, final Parameter[] parameters) {
		String text = name + "(";

        String parametersInText = ParameterToText.getParametersInText(parameters, true);

        parametersInText =
         COMPILE.matcher(parametersInText).replaceAll(Matcher.quoteReplacement(Constants.HTML_OPEN_TAG));

        parametersInText = PATTERN.matcher(parametersInText).replaceAll(Matcher.quoteReplacement(Constants.HTML_CLOSE_TAG));

        text += parametersInText;

		// vrátím získaný text a přidám poslední závorku:		
		return text + ")";
	}

	
	
	
	
	
	
	/**
	 * Metoda, která zkompiluje všechny třídy v ClassDiagramu a pokud se tak povede,
	 * načte si vzniklý soubor s příponou .class a vloži jej do instance třídy
	 * ClassOrInstanceForParameter a tu do kolekce, dále si vezme celou hashMapu se
	 * všemi vytvořenými uživatelem instancemi a vloží jej podobně jako výše do
	 * kolekce s tím, že do instance třídy ClassOrInstanceForParameter vloží
	 * samotnou instnaci třídy a její referenci pro zobrazení v dialogu
	 * 
	 * @param instanceMap
	 *            - veškeré instance tříd z diagramu tříd vytvořené uživatelem
	 *            uložené v HashMapě
	 * 
	 * @return List typu ClassOrInstanceForParameter s naplněnými hodnotami - viz
	 *         výše
	 */
	protected final List<ClassOrInstanceForParameter> getClassOrInstanceList(final Map<String, Object> instanceMap) {
		// Vytvořím si kolekci, do které vložím převzaté hodnoty z mapy a mohl je následně zobrazit v dialogu,
		// aby si uživatel mohl vybrat jakou instanci, třídy nebo vytvoření nové instance:
		final List<ClassOrInstanceForParameter> classOrInstanceList = new ArrayList<>();
		
		// pro začátek si vytvořímiterátor, abych mohl "projít" celou mapu s instancemi a vložit j do kolekce:
		final Iterator<?> iterator = instanceMap.entrySet().iterator();

		// Projedu celou mapu:
		while (iterator.hasNext()) {
			@SuppressWarnings("unchecked")
			// načtu si proměnnou coby vždy hodnotu v mapě, pomocí které si získám konkrétní hodnotu:
			final Map.Entry<String, Object> pair = (Map.Entry<String, Object>) iterator.next();

			// vložím do kolekce hodnoty získané z mapy ještě pomocí instance třídy ClassOrInstanceForParameter,
			// díky které půjde proměnno zobrazit přehledně v dialogu:
			classOrInstanceList.add(new ClassOrInstanceForParameter(pair.getKey(), pair.getValue()));
		}
		
		
		
		
		
		// Dále si zkompiluji a načtu třídy z diagramu tříd, pokud by chtěl uživatel vytvořit jejich novou instanci:				
				
		
		
		// Získám si cestu k adresáři src:

		final String pathToSrc = App.READ_FILE.getPathToSrc();
		
		
		// Otestuji, zda byla načtena:
		if (pathToSrc != null) {
			/*
			 * Toto načtení listu filesList a testování, zda není null jsem zrušil, protože
			 * jsem aplikaci upravil tak, že dialog pro zavolání metody nebo konstruktoru
			 * lze zavolat pouze v případě, že jsou veškeré třídy zkompilované a lze je
			 * načíst. To lze pouze přes kontextové menu, a po kliknutí na na třídu se
			 * všechny zkompilují, pokud se jedná o zavolání metody nad instancí třídy, tak
			 * je to vlastně jedno, protože všechny potřebné "věci" si mohu načíst z
			 * příslušné instance, která se drží v mapě, takže nepotřebuji testovat
			 * existenci, i když by to pouze v tomto případě bylo vhodně, předpokládám, že
			 * uživatel nevytvoří instanci nějaké třídy a tu následně smaže z disku a i
			 * kdyby při aktulizaci by se to zjistilo. Takže opět to zde není potřeba
			 * protože se při aktualizaci tříd (opětovné kompilaci) přijde na to, že by
			 * příslušná třída chyběla a nesšlo by nad ní zavolat metody.
			 */
			
			
			// Otestuji, zda byla načtena, aby nedošlo vyjímce:
			// Zde byly zkompilovány bez chyby, tak mohu načist jednotlivé zkompilované -
			// přeložené soubory .class:
			// Zde se již znovu kompilovat třídy nemusí, všechny se zkompilovaly již po
			// kliknutí pravým tlačítkem myši na třídu
			// v diagramu tříd, tak je stačí pouze načíst
			for (final DefaultGraphCell d : GraphClass.getCellsList()) {
				// Jedná se pouze o buňku reprezentující tridu a ne komentar
				// ci hranu:
				// Note: stačial by pouze druhá podmínka pro testování bunky coby tridy:
				if (!(d instanceof DefaultEdge) && GraphClass.KIND_OF_EDGE.isClassCell(d)) {
					final Class<?> loadedClass = App.READ_FILE.loadCompiledClass(d.toString(), outputEditor);

					// Otestuji, zda se podařilo načíst zkompilovanou třídu a pokud ano, přidám ji
					// do kolekce:
					if (loadedClass != null)
						classOrInstanceList.add(new ClassOrInstanceForParameter(loadedClass));
				}
			}
		}

		// vrátím získaný list:
		return classOrInstanceList;
	}
	

	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro získání proměnných, jejichž hodnoty je možné předat
	 * jako parametr do nějaké metody. Tato metoda slouží právě pro získání
	 * proměnných, jejichž hodnoty je možné předat do parametru příslušné metody.
	 * 
	 * Metoda, která do vloží do listu veškeré proměnné, které jsou veřejné nebo
	 * chráněné a tento list s proměnnými vrátí. Vždy se ze třídy clazz načte i
	 * třída, ze které dědí a z ní si také načtou veškeré proměnné, které deklaruje
	 * a jsou opět věřejné nebo chráněné, protože pouze tyto proměnné lze "využít",
	 * resp. předat do parametru metody i z potomka.
	 * 
	 * Jedná se o to, že nad diagramem tříd nebo diagramem instancí lze zavolat
	 * metodu pomocí kontextového menu a zavolení metody, a do parametru té metody
	 * (v příslušném otevřeném dialogu) chci možnost, aby měl uživatel možnost
	 * zvolit proměnnou, kterou je možné předat jako parametr do té metody. Ve
	 * třídě, kde se ta metoda volá je možné načíst instanční neboli globální
	 * proměnné označeé modifikátorem public nebo protected a ve všech třídách, ze
	 * které tato "hlavní" třída, kde se volá metoda dědí, je možné načíst také
	 * pouze promenné označené modifikátorem public a protected.
	 * 
	 * Pokud se jedná o zavolání statické metody, která se volá nad třídou v
	 * diagramu tříd, ne nad instancí třídy , pak musí být všechny proměnné statické
	 * (pokud se načítají z tříd v diagramu tříd). Jinak když jsou k dispozici
	 * nějaké instance třídy, tak se mohou vzít i z nich nestatické proměnné (když
	 * je přístup k příslušné instanci).
	 * 
	 * Note:
	 * Tato metoda je trochu podobná metodě getAllMethods ve třídě PopupMenuAbstract
	 * v baličku classDiagram, ale je trochu upravena pro proměnné apod.
	 * 
	 * @param clazz
	 *            - Třída, ze které se mají vzít proměnné, až se všechny její
	 *            proměnné projdou, tak se zjistí, zda z něčeho dědí a pak se
	 *            rekurzí pokračuje až dokud existuje nějaká dědičnost, nebo se
	 *            nejedná o třídu Object, protože v potomkovi lze "využít" všechny
	 *            veřejné a chráněné proměnné z předka, resp. ze všech předků.
	 * 
	 * @param fieldsInfoList
	 *            - list, který obsahuje již dříve získané položky (proměnné /
	 *            atributy). Je zde potřeba, aby se zjistilo zda byla již nějaká
	 *            proěmnná načtena nebo ne. Občas tak může nastat, při načítání
	 *            proměnných z předků nějaké třídy. Například nějaké třeba 2 třídy
	 *            mohou dědit z jedné třídy v diagramu tříd. Pak se tato třída bude
	 *            testovat dvakrát.
	 * 
	 * @param fieldsList
	 *            - Jedná se o list typu Field, do kterého se vkládají všechny
	 *            veřejné a chráněné proměnné (případně statické - dle hodnoty v
	 *            mustBeStatic) ze všech testovaných tříd (v proěmnné clazz).
	 * 
	 * @param mustBeStatic
	 *            - logická proměnná, dle které se pozná, zda se mají "získávat" ->
	 *            tj. vkládat do výše uvedeného listu (fieldsList) pouze proměnné,
	 *            které jsou statické (hodnota true). Toto je potřeba, pokud se
	 *            jedná o zavolání metody nad třídou, ne nad instancí třídy, pak
	 *            proměnné, které je možné do parametru té metody předat musí být
	 *            statické (nebo ne, ale musí být získány pouze nad instancí nějaké
	 *            třídy).
	 * 
	 * @return v podstatě se vrátí list fieldsList, který je popsán výše, ale bude
	 *         již obsahovat všechny získané (výše uvedené) proměnné ze všech
	 *         testovaných tříd.
	 */
	private static List<Field> getAvailableFields(final Class<?> clazz, final List<FieldsInfo> fieldsInfoList,
			final List<Field> fieldsList, final boolean mustBeStatic) {
		/*
		 * Zde netestovat, zda je trida v diagramu trid !!! i kdyby byla nebo ne, stejne
		 * by meli jit zavolat ty metody, protože je to předek / rodič té třídy !!!
		 */
		if (clazz == null)
			return fieldsList;
		

		// Načtu si a projdu všechny proměnné třídy clazz:
		if (clazz.getDeclaredFields() != null)
			Arrays.stream(clazz.getDeclaredFields()).forEach(f -> {
				/*
				 * Otestji, zda se mají získat pouze statické proměnné, pokud se nejedná o
				 * statickouk proměnnou, pak pokračuji další iterací:
				 */
				if (mustBeStatic && !Modifier.isStatic(f.getModifiers()))
					return;

				
				/*
				 * Zde si otestuji, zda je příslušná (právě iterovaná proměnná) statická, pokud
				 * ano, tak si tímto zajistím, že v listu nebudou žádné duplicity statických
				 * proměnných, protože chci, aby se statické proměnné nacházely v listu pro
				 * nabídku hodnoty pouze jednou. Protože statická hodnota je pro všechny
				 * instance příslušné třídy stejná, tak proč ji načítat vícekrát. Později si
				 * akorát zjistím, zda je proměnná statická a pokud ano, tak k ní nebudu
				 * přidávat referenci.
				 */
				if (Modifier.isStatic(f.getModifiers()) && isFieldInListLoaded(f, fieldsInfoList))
					return;

				
				/*
				 * Načtu si pouze veřejné nebo chráněné atributy (zde už nemusím testovat
				 * duplicity, když to není statická proměnná, pak tam může býtvícekrát,
				 * například jedna proměnná v několika instancích stejné třídy apod.). pokud je
				 * to statická proměnná, pak se již v předhoczí podmínce otestovaly duplicity
				 * příslušné proměnné.
				 */
				if (Modifier.isPublic(f.getModifiers()) || Modifier.isProtected(f.getModifiers()))
					fieldsList.add(f);
			});

		
		// rekurzi pokracuji:
		if (clazz.getSuperclass() != null || clazz.getSuperclass() != Object.class)
			getAvailableFields(clazz.getSuperclass(), fieldsInfoList, fieldsList, mustBeStatic);

		return fieldsList;
	}
	
	
	

	
	
	
	
	/**
	 * Metoda, která zjistí, zda se již proměnná field nachází v nějakém listu dříve
	 * získaných položek v listu fieldsList.
	 * 
	 * @param field
	 *            - proměnná - položka nějaké třídy (atribut), o kterém se má
	 *            zjistit, zda byl již dříve načten.
	 * 
	 * @param fieldsList
	 *            - list obshující všechny dříve načtené proměnné ze všech
	 *            testovaných tříd.
	 * 
	 * @return true, pokud se field již nachází v nějakém listu dříve načtených
	 *         hodnot, jinak false.
	 */
	private static boolean isFieldInListLoaded(final Field field, final List<FieldsInfo> fieldsList) {
		return fieldsList.stream().anyMatch(f -> f.containsFieldsListField(field));
	}
	
	
	
	

	
	

	

	/**
	 * Metoda, která projde list fieldsList a všechny položky vloží do instance
	 * třídy ParameterValue, instance této třídy vloží do listu, který se vrátí.
	 * 
	 * (V podstatě převezme položky z listu fieldsList a vloží jej do instance třídy
	 * ParameterValue i s příslušnými hodnotami jako je třeba instance a reference
	 * na tu instanci apod.)
	 * 
	 * @param fieldsList
	 *            - list, který obsahuje načtené proměnné z třídy objInstance a
	 *            všechny veřejné a chráněné proměnné z předků této třídy
	 *            objInstance.
	 * 
	 * @param referenceName
	 *            - reference na instanci objInstance, kterou uživatel zvolil při
	 *            jejím vytváření.
	 * 
	 * @param objInstance
	 *            - instance třídy, ze které se vzali proměnné fieldsList.
	 * 
	 * @return list, který obsahuje instance třídy ParameterValue, kterou jemožné
	 *         vkládat do komponenty JComboBox pro výběr konkrétní hodnoty pro
	 *         parametr nějaké metody.
	 */
	protected static List<ParameterValue> getParameterValuesFromFields(final List<Field> fieldsList,
			final String referenceName, final Object objInstance) {
		final List<ParameterValue> list = new ArrayList<>(fieldsList.size());

		// Toto bylo původní, ale nehlídá, že statické proměnné se přidají pouze jednou
		// a bez reference na instanci třídy.
//		fieldsList.forEach(f -> list.add(new ParameterValue(KindOfVariable.FIELD_VARIABLE, f, objInstance, referenceName)));
		
		// Cyklus s hlídáním aby statické proměnné byly přidány pouze jednou bez
		// reference:
		fieldsList.forEach(f -> {
			if (Modifier.isStatic(f.getModifiers()))
				list.add(new ParameterValue(KindOfVariable.FIELD_VARIABLE, f, null, null));
			else
				list.add(new ParameterValue(KindOfVariable.FIELD_VARIABLE, f, objInstance, referenceName));
		});

		return list;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda,která získá veškeré veřejné a chráněné proměnné z uživatelem
	 * vytvořených instancí, které se nachází v diagramu instancí a dále získá
	 * všechny veřejné a chráněné statické proměnné ze všech tříd, které se nachází
	 * v diagramu tříd.
	 * 
	 * (Stručně: "Získajá se veškeré dostupné veřejné a chráněné proměnné z tříd v
	 * daigramu tříd a intancí v diagramu ínstancí.")
	 * 
	 * @param outputEditor
	 *            - jedná se o referenci na editor výstupů, do kterého se budou
	 *            vypisovat chyby v případě, že se nepodaří načíst nějakou třídu
	 *            (zkompilovanou).
	 * 
	 * @return - výše popsaný list, který bude obsahovat veškeré získané (výše
	 *         popsané) proměnné z tříd a instancí spolu s informací z jaké třídy se
	 *         tyto proměnné získaly.
	 */
	protected static List<FieldsInfo> getAvailableFieldsFromClassesAndInstances(
			final OutputEditorInterface outputEditor) {
		/*
		 * List, do kterého si níže vložím veškeré dostupné proměnné z instancí tříd,
		 * které se nachází v diagramu instancí a všechny statické proměnné (veřejné a
		 * chráněné z diagramu tříd).
		 */
		final List<FieldsInfo> availableFieldsList = new ArrayList<>();
		
		/*
		 * Zde se jedná o zavolání nějaké metody s alespoň jedním parametrem nad
		 * instancí třídy. Ta metoda method může a nemusí být statická v tuto chvíli na
		 * tom nezáleží, tak si načtu veškeré instance tříd, které jsou reprezentovány v
		 * diagramu instancí, protože z každé instance lze využít veřejnou a chráněnou
		 * proměnnou.
		 */
			
		/*
		 * Načtu si mapu, která obsahuje všechny instance tříd z diagramu tříd, které
		 * uživatel vytvořil a aktuálně se nachází v diagramu instancí, resp. jsou v něm
		 * reprezentovány.
		 */
		final Map<String, Object> instances = Instances.getAllInstancesOfClassInCd();

		instances.forEach((key, value) -> {
            /*
             * Do listu si načtu všechny veřejné a chráněné proměnné příslušné instance,
             * která se právě iteruje.
             */
            final List<Field> tempFieldsList = getAvailableFields(value.getClass(), availableFieldsList,
                    new ArrayList<>(), false);

            // Na konec přidám informace o získaných proměnných do kolekce, aby bylo později
            // možnmožné příslušné proměnné vložit do modelu pro komponentu JComboBox.
            availableFieldsList.add(new FieldsInfo(tempFieldsList, key, value));
        });
		
		
		
		

		/*
		 * Nyní mám v listu availableFieldsList načteny všechny možné veřejné a chráněné
		 * proměnné z každé instance tříd (včetně předků), která je reprezentována v
		 * diagramu instancí. Tak si teď načtu všechny třídy v diagramu tříd a načtu si
		 * úplně všechny veřejné a chráněné statické proměnné, protože veřejné a
		 * chráněné statické proměnné si můžeme bzít v podstatě "kdykoliv" nad třídou,
		 * resp. si je zpřístupnit.
		 */


		// Zde si nacist vsechny tridy z diagramu trid a ty projit!!
		GraphClass.getCellsList().forEach(c -> {
			if (!(c instanceof DefaultEdge) && GraphClass.KIND_OF_EDGE.isClassCell(c)) {
				final Class<?> loadedClass = App.READ_FILE.loadCompiledClass(c.toString(), outputEditor);

                if (loadedClass != null)
                    availableFieldsList.add(new FieldsInfo(
                            getAvailableFields(loadedClass, availableFieldsList, new ArrayList<>(), true), null, null));
            }
		});

		// Vrátím list se získanými hodnotami:
		return availableFieldsList;
	}
	

	
	
	
	
	
	
	
	
	/**
	 * Metoda, která do list v parametru této metody (valuesForCmbList) vloží
	 * veškeré proěmnné, které uživatel vytvořil v editoru příkazů.
	 * 
	 * @param valuesForCmbList
	 *            - list, kam se mají vložit proměnné, které uživatel vytvořil
	 *            pomocí editoru příkazů.
	 */
	protected static void getCommandsEditorVariables(final List<ParameterValue> valuesForCmbList) {
		/*
		 * Mapa, do které si načtu veškeré uživatelem vytvořené proměnné v editoru
		 * příkazů.
		 */
		final Map<String, Object> editorVariablesMap = Instances.getAllVariablesFromCommandEditor();

		editorVariablesMap.forEach((key, value) -> {
            if (value instanceof Variable) {
                final Variable variable = (Variable) value;
                valuesForCmbList.add(new ParameterValue(KindOfVariable.COMMAND_EDITOR_VARIABLE,
                        variable.getObjValue(), key, null, null));
            }

            else if (value instanceof VariableArray<?>) {
                final VariableArray<?> variable = (VariableArray<?>) value;
                valuesForCmbList.add(new ParameterValue(KindOfVariable.COMMAND_EDITOR_VARIABLE,
                        variable.getArray(), key, variable.getTypeArray(), variable.getClassTypeOfArrayForMethod()));
            }

            else if (value instanceof VariableList<?>) {
                final VariableList<?> variable = (VariableList<?>) value;
                valuesForCmbList.add(new ParameterValue(KindOfVariable.COMMAND_EDITOR_VARIABLE,
                        variable.getList(), key, variable.getClassType(), null));
            }
        });
	}
	
	
	
	
	

	
	
	
	
	
	
	
	/**
	 * Metoda, která seřadí veškeré proměnné v listu v parametru této metody
	 * (availableFields) tak, že nejprve budou proměnné, které uživatel vytovřil v
	 * editoru příkazů, pak budou staticé proměnné z tříd v diagramu tříd a pak
	 * budou proměnné, které se nachází v nějaké instanci třídy v diagramu instancí.
	 * (každá skupina výše uvedencýh proměnných bude seřazena vzestupně dle
	 * abecedy).
	 * 
	 * @param availableFields
	 *            - list, který obsahuje veškeré získané proměnné z editoru příkazů,
	 *            staticé proměnné z tříd a prměnné z instancí tříd (vše veřejné a
	 *            chráněné proměnné).
	 * 
	 * @return list, který bude obsahovat hodnoty v listu availableFields, akorát
	 *         budou seřazené dle výše uvedeného "postupu / syntaxe".
	 */
	protected static List<ParameterValue> getSortedAvailableFields(final List<ParameterValue> availableFields) {
		/*
		 * Zde si vytvořím nový list, do kterého budu postupně vkládat hodnoty, resp.
		 * výše získané proměnné, ale tak, aby bylo vše seřazení.
		 * 
		 * Řadit budu hodnoty dle následující ho pořadí vždy vzestupně dle abecedy:
		 * 
		 * 1) proměnné vytvořené uživatelem v editoru příkazů
		 * 
		 * 2) Statické proměnné z tříd v diagramu tříd.
		 * 
		 * 3) Proměnné, které nejsou statické a jsou získány z instancí tříd, které jsou
		 * reprezentovány v diagramu tříd.
		 */
		final List<ParameterValue> sortedValues = new ArrayList<>();

		// Seřadím si hodnoty z editoru příkazů:
		sortedValues.addAll(availableFields.stream()
				.filter(p -> p.getKindOfVariable().equals(KindOfVariable.COMMAND_EDITOR_VARIABLE))
				.sorted((o1, o2) -> o1.toString().compareToIgnoreCase(o2.toString())).collect(Collectors.toList()));

		// Seřadím si statické hodnoty (tj. hodnoty z získané z tříd, ale nemají
		// referenci.):
		sortedValues.addAll(availableFields.stream().filter(
				p -> p.getReferenceName() == null && p.getKindOfVariable().equals(KindOfVariable.FIELD_VARIABLE))
				.sorted((o1, o2) -> o1.toString().compareToIgnoreCase(o2.toString())).collect(Collectors.toList()));

		// Seřadím si hodnoty, které jsou v nějaké instanci třídy:
		sortedValues.addAll(availableFields.stream().filter(
				p -> p.getReferenceName() != null && p.getKindOfVariable().equals(KindOfVariable.FIELD_VARIABLE))
				.sorted((o1, o2) -> o1.toString().compareToIgnoreCase(o2.toString())).collect(Collectors.toList()));

		// Vrátím seřazené hodnoty (v syntaxi výše))
		return sortedValues;
	}


	/**
	 * Metoda, která slouží pro získání / vyfiltrování hodnot pro konkrétní komponentu typu JComboBox pro "označení"
	 * konkrétního parametru do metody.
	 *
	 * @param availableFields
	 *         - list, který obsahuje veškeré získané proměnné. Jedná se o proměnné z editoru příkazů, statické
	 *         proměnné
	 *         z diagramu tříd a proměnné z instancí v diagramu instancí.
	 * @param dataTypes
	 *         - datové typy hodnot, které se mají vyfiltrovat / vyhledat a vrátit.
	 *
	 * @return list, který obsahuje hodnoty, které jsou jednoho z datových typů, které se nachází v dataTypes, resp.
	 * datový typ parametru metody nebo konstruktoru.
	 */
	private static List<ParameterValue> getModelForCmb(final List<ParameterValue> availableFields,
													   final Class<?>... dataTypes) {
		return availableFields.stream()
				.filter(f -> (f.getKindOfVariable().equals(KindOfVariable.COMMAND_EDITOR_VARIABLE)
						&& f.getObjEditorVariable() != null
						&& isSameDataType(f.getObjEditorVariable().getClass(), dataTypes))
						|| (f.getKindOfVariable().equals(KindOfVariable.FIELD_VARIABLE)
						&& f.getObjValueFromField() != null && compareIncludeListTypes(f.getObjValueFromField(),
						dataTypes)))
				.collect(Collectors.toList());
	}


	/**
	 * Zjištění, jestli je objValue jednoho z typů, které se nachází v dataTypes.
	 *
	 * <i>Jedná se o zjištění, jestli lze objValue vložit do parametru, který je jednoho z typů v dataTypes.</i>
	 *
	 * <i>V případě, že je objValue kolekce, zjistí se, jestli je v dataTypes typ List, pokud ano, pak se vrátí true.
	 * Protože se jedná o zjištění, jestli lze do parametru typu List vložit typ objvalue a do parametru typu List lze
	 * vložit libovolnou implementaci Listu.</i>
	 *
	 * @param objValue
	 *         - hodnota proměnné, o které se zjistí, jestli je typu, který se nachází v dataTypes. Případně, pokud je
	 *         objValue kolekce, pak se zjistí, jestli je v dataTypes List a pokud jsou tyto dvě podmínky splněny,
	 *         vrátí
	 *         se true.
	 * @param dataTypes
	 *         - datové typy, které značí typ parametru metody, kterého musí být objValue, aby bylo možné ji předat do
	 *         parametru.
	 *
	 * @return true v případě, že je objValue datového typu, který se nachází v dataTypes, jinak false.
	 */
	private static boolean compareIncludeListTypes(final Object objValue, final Class<?>[] dataTypes) {
		/*
		 * V případě, že se hledá položka typu List, pak se zjistí, jestli je objValue typu "nějaké" kolekce - jedna z
		  * implementací Listu. Protože v případě, že uživatel zavolal metodu s parametrem List, tak do něj lze vložit
		   * libovolnou implementaci typu List.
		 *
		 * Resp. jestliže je objValue (hodnota proměnné) typu List, pak se zjistí, zda tato proměnná (její hodnota)
		 * může být typu List, aby se vybrala do modelu, pokud ano, tak se může vrátit true.
		 */
		if (ReflectionHelper.isDataTypeOfList(objValue.getClass()) && isSameDataType(List.class, dataTypes)) {
			return true;
		}

		// Zde se nejedná o hodnotu typu List, tak se "pouze" porovnají typy objektů:
		return isSameDataType(objValue.getClass(), dataTypes);
	}
	


	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vyfiltruje hodnoty v listu availableFields a vrátí pouze ty,
	 * které obsahují alespoň nějakou hodnotu.
	 * 
	 * Konkrétně se projde celý list availableFields a vyfiltrují se všechny položky
	 * typu proměnná z třídy nebo proměnná vytvořená v editoru příkazů, pak se u
	 * každého typu proměnné zjistí, zda není příslušná proměnné prázdná, tj. zda se
	 * nejedná o null hodnotu, pokud ne, přidá se do listu, který se vrátí, jinak se
	 * pokračuje dál.
	 * 
	 * (Tato metoda se používá pouze v případě, že se jedná o zadání proměnné tyu
	 * Object -> v parametru metody nebo konstruktoru. Do parametru toohoto typu lze
	 * vložit jakýkoliv parametr,)
	 * 
	 * @param availableFields
	 *            - list, který obsahuje veškeré nalezené proměnné. Veřejné a
	 *            chráněné proměnné z tříd a instancí tříd a proměnné z editoru
	 *            příkazů.
	 * 
	 * @return list, který bude obsahovat proměnné, které jsou veřejné nebo chráněné
	 *         proměnné z nějaké třídy nebo z editoru příkazů a jsou "naplněné", tj.
	 *         nejsou null.
	 */
	private static List<ParameterValue> getModelWithFilledValues(final List<ParameterValue> availableFields) {
		return availableFields.stream().filter(p -> {
			if (p.getKindOfVariable().equals(KindOfVariable.COMMAND_EDITOR_VARIABLE)
					&& p.getObjEditorVariable() != null)
				return true;

			else return p.getKindOfVariable().equals(KindOfVariable.FIELD_VARIABLE) && p.getObjValueFromField() !=
                    null;
        }).collect(Collectors.toList());
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vyfiltruje veškeré implementace objektu Map (mapa) z listu
	 * availableValues. Z tohoto listu availableValues se vyfiltrují pouze položky,
	 * které byly načtené z třídy nebo instancí tříd, protože jsem nedoplnil možnost
	 * v editoru příkazů pro její vytvoření a pro to, aby jej zadal uživatel také už
	 * nemám čas na vymyslení syntaxe a postupu pro předání do metod.
	 * 
	 * Proto se filtrují pouze proměnné z tříd a instancí tříd, které jsou datového
	 * typu jednoho z instnací map nebo map samotná.
	 * 
	 * @param availableValues
	 *            - list, který obsahuje veškeré načtené proměnné z tříd, instancí
	 *            tříd, a editoru příkazů.
	 * 
	 * @return vyfiltrovaný list, který proměnné, které jsou jednoho z datového typu
	 *         mapy.
	 */
	private static List<ParameterValue> getFieldsWithInstancesMap(final List<ParameterValue> availableValues) {
		/*
		 * Prohledám veškeré nalezené proměnné ve třídách a instancích tříd a zjistím, zda
		 * se jedná o příslušné implementace mapy.
		 */
		return availableValues.stream().filter(p ->
				p.getKindOfVariable().equals(KindOfVariable.FIELD_VARIABLE) && p.getObjValueFromField() != null &&
						isSameDataType(p.getObjValueFromField().getClass(), MAP_DATA_TYPES)
		).collect(Collectors.toList());
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vyfiltruje z listu availableFields položky, které jsou načtené
	 * z třídy nebo instance třídy, nejsou null a jsou datového typu dataType.
	 * 
	 * @param availableFields
	 *            - list, který obsahuje veškeré nalezené proměnné z tříd, instnací
	 *            tříd a editoru příkazů.
	 * 
	 * @param dataType
	 *            - datový typ proměnné v parametru metody nebo konstruktoru, kterou
	 *            je třeba naplnit, aby se mohla metoda zavolat.
	 * 
	 * @return list, který bude obsahovat vyfiltrované položky z listu
	 *         availableFields, které jsou datového typu dataType, a jedná se o
	 *         proměné, které nejsou null a jsou to proměnné načtené z třídy nebo
	 *         instance třídy.
	 */
	private static List<ParameterValue> getFieldsWithSameDataType(final List<ParameterValue> availableFields,
			final Class<?> dataType) {
		return availableFields.stream().filter(p -> p.getKindOfVariable().equals(KindOfVariable.FIELD_VARIABLE) && p.getObjValueFromField() != null
                && p.getObjValueFromField().getClass().equals(dataType)).collect(Collectors.toList());
	}


	/**
	 * Metoda, která vyfiltruje z listu availableFields položky, které jsou načtené z třídy nebo instance třídy, nejsou
	 * null a jsou to položky typu List datového typu dataType.
	 *
	 * @param availableFields
	 *         - list, který obsahuje veškeré nalezené proměnné z tříd, instancí tříd a editoru příkazů.
	 * @param dataType
	 *         - datový typ listu, které by se měli načíst.
	 *
	 * @return list, který bude obsahovat vyfiltrované položky z listu availableFields, které jsou datového typu
	 * dataType, a jedná se o proměné, které nejsou null a jsou to proměnné načtené z třídy nebo instance třídy a jsou
	 * to listy typu dataType.
	 */
	private static List<ParameterValue> getListFieldsWithSpecificDataType(
			final List<ParameterValue> availableFields, final Object dataType) {
		return availableFields.stream().filter(p -> p.getKindOfVariable().equals(KindOfVariable.FIELD_VARIABLE) && p.getObjValueFromField() != null
				&& p.getDataTypeOfList() != null && ReflectionHelper.compareDataType(dataType,
				p.getDataTypeOfList().getDataType()) && p.getArrayType() == null).collect(Collectors.toList());
	}
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro získání / vyfiltrování získaných proměnných, které
	 * jsou datového typu jednorozměrné pole konkrétního typu, napřílkad
	 * byte[].class nebo Byte[].class nebo int[].class apod. pro ostatní datové typy
	 * Javy.
	 * 
	 * @param availableFields
	 *            - list získaných proměnných z tříd a editoru příkazů.
	 * 
	 * @param dataTypeOfArray
	 *            - datový typ - pole. Pouze položky s tímto typem se vyfiltrují.
	 * 
	 * @return vyfiltrované položky konkrétního typu jednorozměrného pole.
	 */
    private static List<ParameterValue> getModelForCmbArray(final List<ParameterValue> availableFields,
                                                            final Class<?> dataTypeOfArray) {
        return availableFields.stream().filter(v -> {
            if (v.getKindOfVariable().equals(KindOfVariable.COMMAND_EDITOR_VARIABLE))
                return v.getObjEditorVariable() != null && v.getArrayType() != null
                        && v.getArrayType().equals(dataTypeOfArray);

            else if (v.getKindOfVariable().equals(KindOfVariable.FIELD_VARIABLE))
                return v.getObjValueFromField() != null && v.getObjValueFromField().getClass().equals(dataTypeOfArray);
            return false;
        }).collect(Collectors.toList());
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro získání / vyfiltrování získanách proměnných, které
	 * jsou datového typu List a ten list je libovlného datového typu.
	 * 
	 * Tato metoda slouží pro to, že uživatel chce zavolat metodu nebo konstruktor,
	 * který má parametr List, který je generického datového typu, takže do tohoto
	 * listu lze předat veškeré Listy libolného typu.
	 * 
	 * @param availableFields
	 *            - list získaných proměnných z tříd a editoru příkazů.
	 * 
	 * @return vyfiltrované položky typu List, který je libolvného datového typu.
	 */
	private static List<ParameterValue> getListFieldsWithAnyType(final List<ParameterValue> availableFields) {
		return availableFields.stream().filter(v -> {
			if (v.getKindOfVariable().equals(KindOfVariable.COMMAND_EDITOR_VARIABLE)) {
                return v.getObjEditorVariable() != null && v.getArrayType() == null
                        && ReflectionHelper.isDataTypeOfList(v.getObjEditorVariable().getClass());
			}

			/*
			 * Zde se jedná o proměnnou z nějaké třídy (popr. intance), tak stačí pouze
			 * otestovat, zda se hodnota té proměnné není null a zda ta proměnná je typu
			 * List, ale konkrétní typ listu už mě nezajímá, protože potřebuji zjistit
			 * generické typy, takže libovolný typ.
			 */
			else if (v.getKindOfVariable().equals(KindOfVariable.FIELD_VARIABLE)) {
                return v.getObjValueFromField() != null && ReflectionHelper.isDataTypeOfList(v.getField().getType())
                        && v.getArrayType() == null;
			}
			return false;
		}).collect(Collectors.toList());
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro získání / vyfiltrování získaných proměnných, které
	 * jsou datového typu jednoho z implementací Listu a ten list je libovolného
	 * datového typu.
	 * 
	 * Tato metoda slouží pro to, že uživatel chce zavolat metodu nebo konstruktor,
	 * který má parametr jednu z implementací Listu, který je generického datového
	 * typu, takže do tohoto listu lze předat veškeré implemenace Listu libolného
	 * datového typu.
	 * 
	 * @param availableFields
	 *            - list získaných proměnných z tříd a editoru příkazů.
	 * 
	 * @param kindOfList
	 *            - konkrétní implemntace listu, který musí ta proměnná být.
	 * 
	 * @return vyfiltrované položky typu implementace Lstu, který je libolvného
	 *         datového typu.
	 */
	private static List<ParameterValue> getListFieldsWithAnyType(final List<ParameterValue> availableFields,
			final Class<?> kindOfList) {
		return availableFields.stream().filter(v -> {
			if (v.getKindOfVariable().equals(KindOfVariable.COMMAND_EDITOR_VARIABLE)) {
                return v.getObjEditorVariable() != null && v.getArrayType() == null
                        && kindOfList.equals(v.getObjEditorVariable().getClass());
			}

			/*
			 * Zde se jedná o proměnnou z nějaké třídy (popr. intance), tak stačí pouze
			 * otestovat, zda se hodnota té proměnné není null a zda ta proměnná je typu
			 * List, ale konkrétní typ listu už mě nezajímá, protože potřebuji zjistit
			 * generické typy, takže libovolný typ.
			 */
			else if (v.getKindOfVariable().equals(KindOfVariable.FIELD_VARIABLE)) {
                return v.getObjValueFromField() != null && kindOfList.equals(v.getField().getType())
                        && v.getArrayType() == null;
			}
			return false;
		}).collect(Collectors.toList());
	}
	
	
	
	
	
	

	
	

	/**
	 * Metoda,která otestuje datový typ clazz, zda je to jeden z datových typů,
	 * které se nachází v array.
	 * 
	 * @param clazz
	 *            - datový typ, o kterém se má zjistit, zda se nachází v array.
	 * 
	 * @param array
	 *            - datové typy parametru nějaké metody nebo konstruktoru, jedná se
	 *            o objektový i "normální" datový typ, například byte a Byte apod.
	 * 
	 * @return true, pokud se clazz nachází v array, jinak false.
	 */
	private static boolean isSameDataType(final Class<?> clazz, final Class<?>[] array) {
		return Arrays.asList(array).contains(clazz);
	}
	

	
	
	
	
	
	/**
	 * Metoda, která vyfiltruje veškeré metody, které nejsou metody, které značí
	 * getr na nějakou hodnotu a ten getr je ve stejné instanci.
	 * 
	 * Getr rozpoznávám tak, že je příslušná metoda veřejná, není statická, vrací
	 * stejný datový typ jako je parametr p (parametr setru) a nemá žádný parametr a
	 * název metody / getru je v syntaxi: 'get' nebo 'is' 'SomeName' - SomeName je i
	 * název setr od slova set.
	 * 
	 * @param clazz
	 *            - třída, kde se volá setr a zároveň je to i třída, kde se musí
	 *            nacházet getr, aby NEbyl vybrán.
	 * 
	 * @param methodName
	 *            - název metody - setru, ale od slovíčka set, takže když je třeba
	 *            název metody setSomeVar, tak zde bude jen ta část 'SomeVar', tento
	 *            tex pak přidám ke get nebo is, abych zjistil název getru.
	 * 
	 * @param reference
	 *            - reference na instanci příslušné třídy, kde se ta metoda nachází,
	 *            díky tomu mohu zjistit, zda se ten getr nachází ve stejné instanci
	 *            příslušné třídy nebo ne.
	 * 
	 * @param p
	 *            - parametr zavolaného setru pro otestování, zda getr vrací stejný
	 *            datový typ.
	 * 
	 * @return veškeré metody, které nejsou getr na třídu clazz ve stejné instanci.
	 */
	private List<MethodValue> filterMethodWithoutGetrs(final Class<?> clazz, final String methodName,
			final String reference, final Parameter p) {
		
		/*
		 * Název metody pro getr, je složen buď z 'get' nebo 'is' a název té metody,
		 * toto je potřeba pro odlišení logických proměnných.
		 */
		final String testMethodName;
		if (p.getType().equals(Boolean.class) || p.getType().equals(boolean.class))
			testMethodName = "is" + methodName;
		else
			testMethodName = "get" + methodName;
		
		
		return tempListForOthersMethods.stream().filter(m -> {
			final Method method = m.getMethod();

			/*
			 * Otestuji, zda se jedná o getr, tj. metoda je veřejná, není statická, nemá
			 * parametr, název je get nebo is a methodName, má stejnou referenci a třída, ve
			 * které se nachází je také stejná, jak otřída, nad kterou se zavolala nějaká
			 * metoda.
			 * 
			 * Pokud je toto splněno, pak se jedná o getr, který nechci dát uživateli na
			 * výběr.
			 *
			 * Pro vrácení true -> když se nejedná o syntaxi pro getr, tak jej přidám do výběru pro uživatele:
			 */
            return !Modifier.isPublic(method.getModifiers()) || Modifier.isStatic(method.getModifiers())
                    || !method.getReturnType().equals(p.getType()) || !method.getName().equals(testMethodName)
                    || method.getParameterCount() != 0 || !method.getDeclaringClass().equals(clazz)
                    || m.getReference() == null || !m.getReference().equals(reference);
        }).collect(Collectors.toList());
	}
	
	
	
	
	
	
	

	
	
	
	
	
	
	/**
	 * Metoda, která získá veškeré metody (dle konvencí Javy), které vracejí jedenu
	 * z implemetnací objektu Map.
	 * 
	 * Tj. projdou se cyklem veškeré implementace objektu Map a pro každý z těch
	 * objektů se získají metody v příslušných třídách v diagramu tříd a v
	 * instancích v diagramu instancí.
	 * 
	 * @param clazz
	 *            - třída, ve které se mohou získávat i chráněné metody.
	 * 
	 * @param reference
	 *            - reference na instanci třídy, která senachází v diagramu
	 *            instancí. Tato proměnná bude naplněna pouze v případě, že se jedná
	 *            o zavolání nějaké metody nad instancí v diagramu instancí, pokud
	 *            se jedná o zavolání konstruktoru nebo statické metody nad třídou v
	 *            diagramu tříd, pak bude tato proměnná null.
	 * 
	 * @return list, který bude obsahovat veškeré načtené metody, které vracejí
	 *         jednu z implementací mapy.
	 */
	private static List<MethodValue> getMetodsWithMapReturnType(final Class<?> clazz, final String reference) {
		final List<MethodValue> methodsList = new ArrayList<>();

		Arrays.asList(MAP_DATA_TYPES)
				.forEach(m -> methodsList.addAll(getAvailableMethodsForMethod(clazz, reference, m)));

		return methodsList;
	}
	
	
	
	

	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro získání metod, které je možné zavolat z nějaké
	 * metody, tj. je otevřen dialog pro zavolání metody a zde se vyhledávají
	 * metody, které vracejí datový typ (returnType) parametru té metody.
	 * 
	 * Tj. metoda získá všechny veřejné statické metody ze všech tříd v diagramu
	 * tříd. Dále získá všechny veřejné a chráněné statické metody z třídy clazz a
	 * všech jejích předků. A dále získá všechny veřejné a chráněné nestatické
	 * metody z třídy clazz a všech jejích předků.
	 * 
	 * A pak získá všechny veřejné metody, které nejsou statické ze všech instancí v
	 * diagramu instancí.
	 * 
	 * @param clazz
	 *            - třídy, ve které se má zavolat nějaká metoda, z ní se mohou brát
	 *            i chráněné metody.
	 * 
	 * @param reference
	 *            - reference na příslušnou instanci, tato hodnota bude naplněna
	 *            pouze v případě, že se jedná o zavolání metody nad nějakou
	 *            instanci třídy v diagramu tříd, která se nachází v diagramu
	 *            instancí. Jinak, pokud se jedná o zavolání statické metody nad
	 *            třídou v diagramu tříd, pak bude tato proměnná null.
	 * 
	 * @param returnType
	 *            - datový typ, který má metoda vracet.
	 * 
	 * @return list, který bude obsahovat veškeré výše popsané nalezené metody.
	 */
	private static List<MethodValue> getAvailableMethodsForMethod(final Class<?> clazz, final String reference,
			final Class<?> returnType) {

		/*
		 * Vymažu si dříve získané metody, aby se vůbec nějaké našly.
		 */
		USED_METHODS.clear();

		
		/*
		 * List, do kterého postupně vložím veškeré nalezené metody.
		 */
		final List<MethodValue> availableMethods = new ArrayList<>();

		
		
		/*
		 * Veškeré instance, které se aktuálně nachází v diagramu instancí.
		 */
		final Map<String, Object> instancesInId = Instances.getAllInstancesOfClassInCd();

		
		
		/*
		 * Zde potřebuji rozlišit, zda se jedná o zavolání statické metody nad třídou v
		 * diagramu tříd nebo "nějaká" metoda nad instancí v diagramu instancí. Protože
		 * pokud se jedná o zavolání statické metody nad třídou v diagramu tříd pak se
		 * musí brát pouze veřejné metody z instancí v diagramu instancí.
		 * 
		 * Jinak, pokud se jedná o zavolání metody nad instancí, tak si i z té instance
		 * a všech jejich předků mohu vzít veřejné a níže i chráněné nestatické metody.
		 */
		if (reference == null)
			instancesInId
                    .forEach((key, value) -> Arrays.stream(value.getClass().getDeclaredMethods()).filter(m ->
                            Modifier.isPublic(m.getModifiers())
                                    && !Modifier.isStatic(m.getModifiers()) && m.getReturnType().equals(returnType))
                            .forEach(m -> {
                                /*
                                 * Zde se prochází veškeré instance, které uživatel vytvořil a nachází se v
                                 * diagramu instancí, ale nemohu zde testovat, zda se příslušná metoda již
                                 * nachází v listu USED_METHODS, protože může být X instancí jedné třídy ta
                                 * metoda bude vždy stejná, takže by tam pak byla ta metoda pouze jednou, což je
                                 * špatně, ta metoda se musí vzít z každé instance.
                                 *
                                 * Naví je již "vyřešeno", že to nesmí být statická metoda, takže to opravdu
                                 * bude metoda z konkrétní instance a ne z nějaké třídy, která by se tam mohla
                                 * vyskytovat vícekrát (statická metoda).
                                 */
                                availableMethods.add(new MethodValue(m, value, key));

                                if (!USED_METHODS.contains(m))
                                    USED_METHODS.add(m);
                            }));

		else
			/*
			 * Zde potřebuji vyfiltrovat ještě intance, které nejsou pod tou konkrétní
			 * referencí, kdybych zde testoval třídu, tak to může být více instancí
			 * příslušné třídy, ale já potřebuji jen tu jednu konkrétní.
			 */
			instancesInId.entrySet().stream().filter(i -> !i.getKey().equals(reference))
					.forEach(
							i -> Arrays.stream(i.getValue().getClass().getDeclaredMethods())
									.filter(m -> Modifier.isPublic(m.getModifiers())
											&& !Modifier.isStatic(m.getModifiers())
											&& m.getReturnType().equals(returnType))
									.forEach(m -> {
										/*
										 * Zde se prochází veškeré instance, které uživatel vytvořil a nachází se v
										 * diagramu instancí, ale nemohu zde testovat, zda se příslušná metoda již
										 * nachází v listu USED_METHODS, protože může být X instancí jedné třídy ta
										 * metoda bude vždy stejná, takže by tam pak byla ta metoda pouze jednou, což je
										 * špatně, ta metoda se musí vzít z každé instance.
										 * 
										 * Naví je již "vyřešeno", že to nesmí být statická metoda, takže to opravdu
										 * bude metoda z konkrétní instance a ne z nějaké třídy, která by se tam mohla
										 * vyskytovat vícekrát (statická metoda).
										 */
										availableMethods.add(new MethodValue(m, i.getValue(), i.getKey()));

										if (!USED_METHODS.contains(m))
											USED_METHODS.add(m);
									}));
		
		
		
		/*
		 * Zde projde třídu clazz a veškeré její předky až po třídu Object (bez ní) a
		 * vyhledám v ní veškeré veřejné a chráněné metody, kterou nejsou statické a
		 * vracejí datový typu returnType.
		 * 
		 * Toto je potřeba, protože v tomto případě se jedná o zavolání nějaké metody
		 * nad instancí v diagramu instancí, tak musím tu instanci prohledat i její
		 * předky a vzít si možné metody i z nich.
		 * 
		 * Pokud se ale jedná o zavolání statické metody nad třídou v diagramu tříd, pak
		 * se nesmí vyhledávat v té instanci! jen veřejné metody z instancí a jinak
		 * statické.
		 * 
		 * 
		 * Note:
		 * Tento postup s prochazením mapy je jen, že si potřebuji vzít metody z té
		 * instance a s ní i referenci a intanci samotné třídy.
		 */
		if (reference != null)
			instancesInId.entrySet().stream().filter(i -> i.getKey().equals(reference))
					.forEach(i -> availableMethods.addAll(getPublicProtectedNonStaticMethods(i.getValue().getClass(),
							i.getValue(), i.getKey(), returnType, new ArrayList<>())));	



		
		
		/*
		 * Získám si všechny veřejné a chráněné statické metody z třídy clazz a všech
		 * jejích předků.
		 */
		availableMethods.addAll(getPublicProtectedStaticMethod(clazz, new ArrayList<>(), returnType));

		
		/*
		 * Zde si načtu všechny veřejné statické metody, které vracejí returnType a
		 * nemusím zde testovat metody, které se nachází například v předcích třídy
		 * clazz, protože pokud se již využily, pak budou v listu usedMethods, takže
		 * jsem si tímto zařídil, že nevznikou duplicity metod.
		 * 
		 * Zde testuji veškeré třídy v diagramu tříd, a žádná z těch metod, které se zde
		 * najdou by se v listu ještě neměla vyskytovat, protože výše se hledají pouze
		 * veřejné, popřípadě chráněné nestatické metody ze všech předků nebo z tříd
		 * samotných, ale v instancích, zde se prohledávají statické metody z tříd v
		 * diagramu tříd.
		 */
		availableMethods.addAll(getAllPublicStaticMethods(returnType));

		
		return availableMethods;
	}
	
	
	
	
	
	

	
	
	
	
	/**
	 * Metoda, která slouží pro získání metod, které je možné zavolat z nějaké
	 * metody, tj. je otevřen dialog pro zavolání metody a zde se vyhledávají
	 * metody, které vracejí datový typ (returnType) parametru té metody.
	 * 
	 * Tj. metoda získá všechny veřejné statické metody ze všech tříd v diagramu
	 * tříd. Dále získá všechny veřejné a chráněné statické metody z třídy clazz a
	 * všech jejích předků. A dále získá všechny veřejné a chráněné nestatické
	 * metody z třídy clazz a všech jejích předků.
	 * 
	 * A pak získá všechny veřejné metody, které nejsou statické ze všech instancí v
	 * diagramu instancí.
	 * 
	 * Všechny metoda musí vracet "nějaký" / libovolný objekt. Tato metoda je
	 * využita, když se jedná o parametru typu Object, pak je do tohoto typu
	 * parametru možné předat libovolnou hodnotu, proto, když se se jedná o parametr
	 * typu Object, tak vyhledám všechny možné metody (z hlediska Javy, které by
	 * mělo být možné zavolat), které vracejí nějakou hodnotu, tj. nejsou void.
	 * 
	 * @param clazz
	 *            - třídy, ve které se má zavolat nějaká metoda, z ní se mohou brát
	 *            i chráněné metody.
	 * 
	 * @param reference
	 *            - reference na příslušnou instanci, tato hodnota bude naplněna
	 *            pouze v případě, že se jedná o zavolání metody nad nějakou
	 *            instanci třídy v diagramu tříd, která se nachází v diagramu
	 *            instancí. Jinak, pokud se jedná o zavolání statické metody nad
	 *            třídou v diagramu tříd, pak bude tato proměnná null.
	 * 
	 * @return list, který bude obsahovat veškeré výše popsané nalezené metody.
	 */
	private static List<MethodValue> getAllAvailableMethodsForMethodWithSomeReturnType(final Class<?> clazz,
			final String reference) {

		/*
		 * Vymažu si dříve získané metody, aby se vůbec nějaké našly.
		 */
		USED_METHODS.clear();

		
		/*
		 * List, do kterého postupně vložím veškeré nalezené metody.
		 */
		final List<MethodValue> availableMethods = new ArrayList<>();

		
		
		/*
		 * Veškeré instance, které se aktuálně nachází v diagramu instancí.
		 */
		final Map<String, Object> instancesInId = Instances.getAllInstancesOfClassInCd();

		
		
		/*
		 * Zde potřebuji rozlišit, zda se jedná o zavolání statické metody nad třídou v
		 * diagramu tříd nebo "nějaká" metoda nad instancí v diagramu instancí. Protože
		 * pokud se jedná o zavolání statické metody nad třídou v diagramu tříd pak se
		 * musí brát pouze veřejné metody z instancí v diagramu instancí.
		 * 
		 * Jinak, pokud se jedná o zavolání metody nad instancí, tak si i z té instance
		 * a všech jejich předků mohu vzít veřejné a níže i chráněné nestatické metody.
		 */
        if (reference == null)
            instancesInId
                    .forEach((key, value) -> Arrays.stream(value.getClass().getDeclaredMethods()).filter(m ->
                            Modifier.isPublic(m.getModifiers())
                                    && !Modifier.isStatic(m.getModifiers()) && !m.getReturnType().equals(Void.TYPE))
                            .forEach(m -> {
                                /*
                                 * Zde se prochází veškeré instance, které uživatel vytvořil a nachází se v
                                 * diagramu instancí, ale nemohu zde testovat, zda se příslušná metoda již
                                 * nachází v listu USED_METHODS, protože může být X instancí jedné třídy ta
                                 * metoda bude vždy stejná, takže by tam pak byla ta metoda pouze jednou, což je
                                 * špatně, ta metoda se musí vzít z každé instance.
                                 *
                                 * Naví je již "vyřešeno", že to nesmí být statická metoda, takže to opravdu
                                 * bude metoda z konkrétní instance a ne z nějaké třídy, která by se tam mohla
                                 * vyskytovat vícekrát (statická metoda).
                                 */
                                availableMethods.add(new MethodValue(m, value, key));

                                if (!USED_METHODS.contains(m))
                                    USED_METHODS.add(m);
                            }));

		else
			/*
			 * Zde potřebuji vyfiltrovat ještě intance, které nejsou pod tou konkrétní
			 * referencí, kdybych zde testoval třídu, tak to může být více instancí
			 * příslušné třídy, ale já potřebuji jen tu jednu konkrétní.
			 */
			instancesInId.entrySet().stream().filter(i -> !i.getKey().equals(reference))
					.forEach(
							i -> Arrays.stream(i.getValue().getClass().getDeclaredMethods())
									.filter(m -> Modifier.isPublic(m.getModifiers())
											&& !Modifier.isStatic(m.getModifiers())
											&& !m.getReturnType().equals(Void.TYPE))
									.forEach(m -> {
										/*
										 * Zde se prochází veškeré instance, které uživatel vytvořil a nachází se v
										 * diagramu instancí, ale nemohu zde testovat, zda se příslušná metoda již
										 * nachází v listu USED_METHODS, protože může být X instancí jedné třídy ta
										 * metoda bude vždy stejná, takže by tam pak byla ta metoda pouze jednou, což je
										 * špatně, ta metoda se musí vzít z každé instance.
										 * 
										 * Naví je již "vyřešeno", že to nesmí být statická metoda, takže to opravdu
										 * bude metoda z konkrétní instance a ne z nějaké třídy, která by se tam mohla
										 * vyskytovat vícekrát (statická metoda).
										 */
										availableMethods.add(new MethodValue(m, i.getValue(), i.getKey()));

										if (!USED_METHODS.contains(m))
											USED_METHODS.add(m);
									}));
		
		
		
		/*
		 * Zde projde třídu clazz a veškeré její předky až po třídu Object (bez ní) a
		 * vyhledám v ní veškeré veřejné a chráněné metody, kterou nejsou statické a
		 * vracejí datový typu returnType.
		 * 
		 * Toto je potřeba, protože v tomto případě se jedná o zavolání nějaké metody
		 * nad instancí v diagramu instancí, tak musím tu instanci prohledat i její
		 * předky a vzít si možné metody i z nich.
		 * 
		 * Pokud se ale jedná o zavolání statické metody nad třídou v diagramu tříd, pak
		 * se nesmí vyhledávat v té instanci! jen veřejné metody z instancí a jinak
		 * statické.
		 * 
		 * 
		 * Note:
		 * Tento postup s prochazením mapy je jen, že si potřebuji vzít metody z té
		 * instance a s ní i referenci a intanci samotné třídy.
		 */
		if (reference != null)
			instancesInId.entrySet().stream().filter(i -> i.getKey().equals(reference)).forEach(
					i -> availableMethods.addAll(getPublicProtectedNonStaticMethods(i.getValue().getClass(),
							i.getValue(), i.getKey(), new ArrayList<>())));	



		
		
		/*
		 * Získám si všechny veřejné a chráněné statické metody z třídy clazz a všech
		 * jejích předků.
		 */
		availableMethods.addAll(getPublicProtectedStaticMethod(clazz, new ArrayList<>()));

		
		/*
		 * Zde si načtu všechny veřejné statické metody, které vracejí libolný objekt a
		 * nemusím zde testovat metody, které se nachází například v předcích třídy
		 * clazz, protože pokud se již využily, pak budou v listu usedMethods, takže
		 * jsem si tímto zařídil, že nevznikou duplicity metod.
		 * 
		 * Zde testuji veškeré třídy v diagramu tříd, a žádná z těch metod, které se zde
		 * najdou by se v listu ještě neměla vyskytovat, protože výše se hledají pouze
		 * veřejné, popřípadě chráněné nestatické metody ze všech předků nebo z tříd
		 * samotných, ale v instancích, zde se prohledávají statické metody z tříd v
		 * diagramu tříd.
		 */
		availableMethods.addAll(getAllPublicStaticMethodsNonVoid());

		
		return availableMethods;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda slouží pro vyhledání metody, které vracejí List typu returnType.
	 * 
	 * Metoda, která slouží pro získání metod, které je možné zavolat z nějaké
	 * metody, tj. je otevřen dialog pro zavolání metody a zde se vyhledávají
	 * metody, které vracejí datový typ parametru té metody a ten datový typ musí
	 * být List typu returnType.
	 * 
	 * Tj. metoda získá všechny veřejné statické metody ze všech tříd v diagramu
	 * tříd. Dále získá všechny veřejné a chráněné statické metody z třídy clazz a
	 * všech jejích předků. A dále získá všechny veřejné a chráněné nestatické
	 * metody z třídy clazz a všech jejích předků.
	 * 
	 * A pak získá všechny veřejné metody, které nejsou statické ze všech instancí v
	 * diagramu instancí.
	 * 
	 * @param clazz
	 *            - třídy, ve které se má zavolat nějaká metoda, z ní se mohou brát
	 *            i chráněné metody.
	 * 
	 * @param reference
	 *            - reference na příslušnou instanci, tato hodnota bude naplněna
	 *            pouze v případě, že se jedná o zavolání metody nad nějakou
	 *            instanci třídy v diagramu tříd, která se nachází v diagramu
	 *            instancí. Jinak, pokud se jedná o zavolání statické metody nad
	 *            třídou v diagramu tříd, pak bude tato proměnná null.
	 * 
	 * @param returnType
	 *            - datový typ listu, který má metoda vracet. Např:
	 *            List<returnType>.
	 * 
	 * @return list, který bude obsahovat veškeré výše popsané nalezené metody.
	 */
	private static List<MethodValue> getAvailableMethodsForMethod_List(final Class<?> clazz, final String reference,
																	   final Class<?> kindOfList, final Object returnType) {
		/*
		 * Vymažu si dříve získané metody, aby se vůbec nějaké našly.
		 */
		USED_METHODS.clear();

		
		/*
		 * List, do kterého postupně vložím veškeré nalezené metody.
		 */
		final List<MethodValue> availableMethods = new ArrayList<>();

		
		
		/*
		 * Veškeré instance, které se aktuálně nachází v diagramu instancí.
		 */
		final Map<String, Object> instancesInId = Instances.getAllInstancesOfClassInCd();

		
		
		/*
		 * Zde potřebuji rozlišit, zda se jedná o zavolání statické metody nad třídou v
		 * diagramu tříd nebo "nějaká" metoda nad instancí v diagramu instancí. Protože
		 * pokud se jedná o zavolání statické metody nad třídou v diagramu tříd pak se
		 * musí brát pouze veřejné metody z instancí v diagramu instancí.
		 * 
		 * Jinak, pokud se jedná o zavolání metody nad instancí, tak si i z té instance
		 * a všech jejich předků mohu vzít veřejné a níže i chráněné nestatické metody.
		 */
		if (reference == null)
			instancesInId.forEach((key, value) -> Arrays.stream(value.getClass().getDeclaredMethods()).filter(m
					-> Modifier.isPublic(m.getModifiers()) && !Modifier.isStatic(m.getModifiers())
					&& areDataTypesSame(m, kindOfList, returnType))
					.forEach(m -> {
						/*
						 * Zde se prochází veškeré instance, které uživatel vytvořil a nachází se v
						 * diagramu instancí, ale nemohu zde testovat, zda se příslušná metoda již
						 * nachází v listu USED_METHODS, protože může být X instancí jedné třídy ta
						 * metoda bude vždy stejná, takže by tam pak byla ta metoda pouze jednou, což je
						 * špatně, ta metoda se musí vzít z každé instance.
						 *
						 * Naví je již "vyřešeno", že to nesmí být statická metoda, takže to opravdu
						 * bude metoda z konkrétní instance a ne z nějaké třídy, která by se tam mohla
						 * vyskytovat vícekrát (statická metoda).
						 */
						availableMethods.add(new MethodValue(m, value, key));

						if (!USED_METHODS.contains(m))
							USED_METHODS.add(m);
					}));

		else
			/*
			 * Zde potřebuji vyfiltrovat ještě intance, které nejsou pod tou konkrétní
			 * referencí, kdybych zde testoval třídu, tak to může být více instancí
			 * příslušné třídy, ale já potřebuji jen tu jednu konkrétní.
			 */
			instancesInId.entrySet().stream().filter(i -> !i.getKey().equals(reference))
					.forEach(
							i -> Arrays.stream(i.getValue().getClass().getDeclaredMethods())
									.filter(m -> Modifier.isPublic(m.getModifiers())
											&& !Modifier.isStatic(m.getModifiers())
											&& areDataTypesSame(m, kindOfList, returnType))
									.forEach(m -> {
										/*
										 * Zde se prochází veškeré instance, které uživatel vytvořil a nachází se v
										 * diagramu instancí, ale nemohu zde testovat, zda se příslušná metoda již
										 * nachází v listu USED_METHODS, protože může být X instancí jedné třídy ta
										 * metoda bude vždy stejná, takže by tam pak byla ta metoda pouze jednou, což je
										 * špatně, ta metoda se musí vzít z každé instance.
										 * 
										 * Naví je již "vyřešeno", že to nesmí být statická metoda, takže to opravdu
										 * bude metoda z konkrétní instance a ne z nějaké třídy, která by se tam mohla
										 * vyskytovat vícekrát (statická metoda).
										 */
										availableMethods.add(new MethodValue(m, i.getValue(), i.getKey()));

										if (!USED_METHODS.contains(m))
											USED_METHODS.add(m);
									}));
		
		
		
		/*
		 * Zde projde třídu clazz a veškeré její předky až po třídu Object (bez ní) a
		 * vyhledám v ní veškeré veřejné a chráněné metody, kterou nejsou statické a
		 * vracejí datový typu returnType.
		 * 
		 * Toto je potřeba, protože v tomto případě se jedná o zavolání nějaké metody
		 * nad instancí v diagramu instancí, tak musím tu instanci prohledat i její
		 * předky a vzít si možné metody i z nich.
		 * 
		 * Pokud se ale jedná o zavolání statické metody nad třídou v diagramu tříd, pak
		 * se nesmí vyhledávat v té instanci! jen veřejné metody z instancí a jinak
		 * statické.
		 * 
		 * 
		 * Note:
		 * Tento postup s prochazením mapy je jen, že si potřebuji vzít metody z té
		 * instance a s ní i referenci a intanci samotné třídy.
		 */
		if (reference != null)
			instancesInId.entrySet().stream().filter(i -> i.getKey().equals(reference))
					.forEach(i -> availableMethods
							.addAll(getPublicProtectedNonStaticMethodsWithReturnList(i.getValue().getClass(),
									i.getValue(), i.getKey(), kindOfList, returnType, new ArrayList<>())));



		
		
		/*
		 * Získám si všechny veřejné a chráněné statické metody z třídy clazz a všech
		 * jejích předků.
		 */
		availableMethods
				.addAll(getPublicProtectedStaticMethodWithReturnList(clazz, new ArrayList<>(), kindOfList, returnType));

		
		/*
		 * Zde si načtu všechny veřejné statické metody, které vracejí returnType a
		 * nemusím zde testovat metody, které se nachází například v předcích třídy
		 * clazz, protože pokud se již využily, pak budou v listu usedMethods, takže
		 * jsem si tímto zařídil, že nevznikou duplicity metod.
		 * 
		 * Zde testuji veškeré třídy v diagramu tříd, a žádná z těch metod, které se zde
		 * najdou by se v listu ještě neměla vyskytovat, protože výše se hledají pouze
		 * veřejné, popřípadě chráněné nestatické metody ze všech předků nebo z tříd
		 * samotných, ale v instancích, zde se prohledávají statické metody z tříd v
		 * diagramu tříd.
		 */
		availableMethods.addAll(getAllPublicStaticMethodsWithReturnList(kindOfList, returnType));

		return availableMethods;
	}
	
	
	
	
	

	
	
	
	
	
	/**
	 * Metoda slouží pro vyhledání metody, které vracejí List generického typu, tj.
	 * že ten List může být libolvného datového typu.
	 * 
	 * Metoda, která slouží pro získání metod, které je možné zavolat z nějaké
	 * metody, tj. je otevřen dialog pro zavolání metody a zde se vyhledávají
	 * metody, které vracejí datový typ parametru té metody a ten datový typ musí
	 * být List libovolného datového typu.
	 * 
	 * Tj. metoda získá všechny veřejné statické metody ze všech tříd v diagramu
	 * tříd. Dále získá všechny veřejné a chráněné statické metody z třídy clazz a
	 * všech jejích předků. A dále získá všechny veřejné a chráněné nestatické
	 * metody z třídy clazz a všech jejích předků.
	 * 
	 * A pak získá všechny veřejné metody, které nejsou statické ze všech instancí v
	 * diagramu instancí.
	 * 
	 * @param clazz
	 *            - třídy, ve které se má zavolat nějaká metoda, z ní se mohou brát
	 *            i chráněné metody.
	 * 
	 * @param reference
	 *            - reference na příslušnou instanci, tato hodnota bude naplněna
	 *            pouze v případě, že se jedná o zavolání metody nad nějakou
	 *            instanci třídy v diagramu tříd, která se nachází v diagramu
	 *            instancí. Jinak, pokud se jedná o zavolání statické metody nad
	 *            třídou v diagramu tříd, pak bude tato proměnná null.
	 * 
	 * @param kindOfList
	 *            - konkrétní implemetance Listu, který se má hledat.
	 * 
	 * @return list, který bude obsahovat veškeré výše popsané nalezené metody,
	 *         které vrací datový typ List libolného datového typu.
	 */
	private static List<MethodValue> getAvailableMethodsForMethod_ListGeneric(final Class<?> clazz,
			final String reference, final Class<?> kindOfList) {

		/*
		 * Vymažu si dříve získané metody, aby se vůbec nějaké našly.
		 */
		USED_METHODS.clear();

		
		/*
		 * List, do kterého postupně vložím veškeré nalezené metody.
		 */
		final List<MethodValue> availableMethods = new ArrayList<>();

		
		
		/*
		 * Veškeré instance, které se aktuálně nachází v diagramu instancí.
		 */
		final Map<String, Object> instancesInId = Instances.getAllInstancesOfClassInCd();

		
		
		/*
		 * Zde potřebuji rozlišit, zda se jedná o zavolání statické metody nad třídou v
		 * diagramu tříd nebo "nějaká" metoda nad instancí v diagramu instancí. Protože
		 * pokud se jedná o zavolání statické metody nad třídou v diagramu tříd pak se
		 * musí brát pouze veřejné metody z instancí v diagramu instancí.
		 * 
		 * Jinak, pokud se jedná o zavolání metody nad instancí, tak si i z té instance
		 * a všech jejich předků mohu vzít veřejné a níže i chráněné nestatické metody.
		 */
		if (reference == null)
			instancesInId.forEach((key, value) -> Arrays.stream(value.getClass().getDeclaredMethods()).filter(m
					-> Modifier.isPublic(m.getModifiers()) && !Modifier.isStatic(m.getModifiers())
					&& m.getReturnType().equals(kindOfList))
					.forEach(m -> {
						/*
						 * Zde se prochází veškeré instance, které uživatel vytvořil a nachází se v
						 * diagramu instancí, ale nemohu zde testovat, zda se příslušná metoda již
						 * nachází v listu USED_METHODS, protože může být X instancí jedné třídy ta
						 * metoda bude vždy stejná, takže by tam pak byla ta metoda pouze jednou, což je
						 * špatně, ta metoda se musí vzít z každé instance.
						 *
						 * Naví je již "vyřešeno", že to nesmí být statická metoda, takže to opravdu
						 * bude metoda z konkrétní instance a ne z nějaké třídy, která by se tam mohla
						 * vyskytovat vícekrát (statická metoda).
						 */
						availableMethods.add(new MethodValue(m, value, key));

						if (!USED_METHODS.contains(m))
							USED_METHODS.add(m);
					}));

		else
			/*
			 * Zde potřebuji vyfiltrovat ještě intance, které nejsou pod tou konkrétní
			 * referencí, kdybych zde testoval třídu, tak to může být více instancí
			 * příslušné třídy, ale já potřebuji jen tu jednu konkrétní.
			 */
			instancesInId.entrySet().stream().filter(i -> !i.getKey().equals(reference))
					.forEach(
							i -> Arrays.stream(i.getValue().getClass().getDeclaredMethods())
									.filter(m -> Modifier.isPublic(m.getModifiers())
											&& !Modifier.isStatic(m.getModifiers())
											&& m.getReturnType().equals(kindOfList))
									.forEach(m -> {
										/*
										 * Zde se prochází veškeré instance, které uživatel vytvořil a nachází se v
										 * diagramu instancí, ale nemohu zde testovat, zda se příslušná metoda již
										 * nachází v listu USED_METHODS, protože může být X instancí jedné třídy ta
										 * metoda bude vždy stejná, takže by tam pak byla ta metoda pouze jednou, což je
										 * špatně, ta metoda se musí vzít z každé instance.
										 * 
										 * Naví je již "vyřešeno", že to nesmí být statická metoda, takže to opravdu
										 * bude metoda z konkrétní instance a ne z nějaké třídy, která by se tam mohla
										 * vyskytovat vícekrát (statická metoda).
										 */
										availableMethods.add(new MethodValue(m, i.getValue(), i.getKey()));

										if (!USED_METHODS.contains(m))
											USED_METHODS.add(m);
									}));
		
		
		
		/*
		 * Zde projde třídu clazz a veškeré její předky až po třídu Object (bez ní) a
		 * vyhledám v ní veškeré veřejné a chráněné metody, kterou nejsou statické a
		 * vracejí datový typu returnType.
		 * 
		 * Toto je potřeba, protože v tomto případě se jedná o zavolání nějaké metody
		 * nad instancí v diagramu instancí, tak musím tu instanci prohledat i její
		 * předky a vzít si možné metody i z nich.
		 * 
		 * Pokud se ale jedná o zavolání statické metody nad třídou v diagramu tříd, pak
		 * se nesmí vyhledávat v té instanci! jen veřejné metody z instancí a jinak
		 * statické.
		 * 
		 * 
		 * Note:
		 * Tento postup s prochazením mapy je jen, že si potřebuji vzít metody z té
		 * instance a s ní i referenci a intanci samotné třídy.
		 */
		if (reference != null)
			instancesInId.entrySet().stream().filter(i -> i.getKey().equals(reference))
					.forEach(i -> availableMethods.addAll(getPublicProtectedNonStaticMethodsWithReturnListGeneric(
							i.getValue().getClass(), i.getValue(), i.getKey(), kindOfList, new ArrayList<>())));



		
		
		/*
		 * Získám si všechny veřejné a chráněné statické metody z třídy clazz a všech
		 * jejích předků.
		 */
		availableMethods
				.addAll(getPublicProtectedStaticMethodWithReturnListGeneric(clazz, new ArrayList<>(), kindOfList));

		
		/*
		 * Zde si načtu všechny veřejné statické metody, které vracejí returnType a
		 * nemusím zde testovat metody, které se nachází například v předcích třídy
		 * clazz, protože pokud se již využily, pak budou v listu usedMethods, takže
		 * jsem si tímto zařídil, že nevznikou duplicity metod.
		 * 
		 * Zde testuji veškeré třídy v diagramu tříd, a žádná z těch metod, které se zde
		 * najdou by se v listu ještě neměla vyskytovat, protože výše se hledají pouze
		 * veřejné, popřípadě chráněné nestatické metody ze všech předků nebo z tříd
		 * samotných, ale v instancích, zde se prohledávají statické metody z tříd v
		 * diagramu tříd.
		 */
		availableMethods.addAll(getAllPublicStaticMethodsWithReturnListGeneric(kindOfList));

		
		return availableMethods;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda slouží pro vyhledání metody, které vracejí List generického typu, tj.
	 * že ten List může být libolvného datového typu a ten List může být libolná implementace Listu.
	 * 
	 * Metoda, která slouží pro získání metod, které je možné zavolat z nějaké
	 * metody, tj. je otevřen dialog pro zavolání metody a zde se vyhledávají
	 * metody, které vracejí datový typ parametru té metody a ten datový typ musí
	 * být List (libolné implementace Listu) libovolného datového typu.
	 * 
	 * Tj. metoda získá všechny veřejné statické metody ze všech tříd v diagramu
	 * tříd. Dále získá všechny veřejné a chráněné statické metody z třídy clazz a
	 * všech jejích předků. A dále získá všechny veřejné a chráněné nestatické
	 * metody z třídy clazz a všech jejích předků.
	 * 
	 * A pak získá všechny veřejné metody, které nejsou statické ze všech instancí v
	 * diagramu instancí.
	 * 
	 * @param clazz
	 *            - třídy, ve které se má zavolat nějaká metoda, z ní se mohou brát
	 *            i chráněné metody.
	 * 
	 * @param reference
	 *            - reference na příslušnou instanci, tato hodnota bude naplněna
	 *            pouze v případě, že se jedná o zavolání metody nad nějakou
	 *            instanci třídy v diagramu tříd, která se nachází v diagramu
	 *            instancí. Jinak, pokud se jedná o zavolání statické metody nad
	 *            třídou v diagramu tříd, pak bude tato proměnná null.
	 * 
	 * @return list, který bude obsahovat veškeré výše popsané nalezené metody,
	 *         které vrací datový typ List libolného datového typu.
	 */
	private static List<MethodValue> getAvailableMethodsForMethod_ListGenericCollection(final Class<?> clazz,
			final String reference) {

		/*
		 * Vymažu si dříve získané metody, aby se vůbec nějaké našly.
		 */
		USED_METHODS.clear();

		
		/*
		 * List, do kterého postupně vložím veškeré nalezené metody.
		 */
		final List<MethodValue> availableMethods = new ArrayList<>();

		
		
		/*
		 * Veškeré instance, které se aktuálně nachází v diagramu instancí.
		 */
		final Map<String, Object> instancesInId = Instances.getAllInstancesOfClassInCd();

		
		
		/*
		 * Zde potřebuji rozlišit, zda se jedná o zavolání statické metody nad třídou v
		 * diagramu tříd nebo "nějaká" metoda nad instancí v diagramu instancí. Protože
		 * pokud se jedná o zavolání statické metody nad třídou v diagramu tříd pak se
		 * musí brát pouze veřejné metody z instancí v diagramu instancí.
		 * 
		 * Jinak, pokud se jedná o zavolání metody nad instancí, tak si i z té instance
		 * a všech jejich předků mohu vzít veřejné a níže i chráněné nestatické metody.
		 */
        if (reference == null)
            instancesInId.forEach((key, value) -> Arrays.stream(value.getClass().getDeclaredMethods()).filter(m
                    -> Modifier.isPublic(m.getModifiers()) && !Modifier.isStatic(m.getModifiers())
                    && ReflectionHelper.isDataTypeOfList(m.getReturnType()))
                    .forEach(m -> {
                        /*
                         * Zde se prochází veškeré instance, které uživatel vytvořil a nachází se v
                         * diagramu instancí, ale nemohu zde testovat, zda se příslušná metoda již
                         * nachází v listu USED_METHODS, protože může být X instancí jedné třídy ta
                         * metoda bude vždy stejná, takže by tam pak byla ta metoda pouze jednou, což je
                         * špatně, ta metoda se musí vzít z každé instance.
                         *
                         * Naví je již "vyřešeno", že to nesmí být statická metoda, takže to opravdu
                         * bude metoda z konkrétní instance a ne z nějaké třídy, která by se tam mohla
                         * vyskytovat vícekrát (statická metoda).
                         */
                        availableMethods.add(new MethodValue(m, value, key));

                        if (!USED_METHODS.contains(m))
                            USED_METHODS.add(m);
                    }));

		else
			/*
			 * Zde potřebuji vyfiltrovat ještě intance, které nejsou pod tou konkrétní
			 * referencí, kdybych zde testoval třídu, tak to může být více instancí
			 * příslušné třídy, ale já potřebuji jen tu jednu konkrétní.
			 */
			instancesInId.entrySet().stream().filter(i -> !i.getKey().equals(reference))
					.forEach(
							i -> Arrays.stream(i.getValue().getClass().getDeclaredMethods())
									.filter(m -> Modifier.isPublic(m.getModifiers())
											&& !Modifier.isStatic(m.getModifiers())
											&& ReflectionHelper.isDataTypeOfList(m.getReturnType()))
									.forEach(m -> {
										/*
										 * Zde se prochází veškeré instance, které uživatel vytvořil a nachází se v
										 * diagramu instancí, ale nemohu zde testovat, zda se příslušná metoda již
										 * nachází v listu USED_METHODS, protože může být X instancí jedné třídy ta
										 * metoda bude vždy stejná, takže by tam pak byla ta metoda pouze jednou, což je
										 * špatně, ta metoda se musí vzít z každé instance.
										 * 
										 * Naví je již "vyřešeno", že to nesmí být statická metoda, takže to opravdu
										 * bude metoda z konkrétní instance a ne z nějaké třídy, která by se tam mohla
										 * vyskytovat vícekrát (statická metoda).
										 */
										availableMethods.add(new MethodValue(m, i.getValue(), i.getKey()));

										if (!USED_METHODS.contains(m))
											USED_METHODS.add(m);
									}));
		
		
		
		/*
		 * Zde projde třídu clazz a veškeré její předky až po třídu Object (bez ní) a
		 * vyhledám v ní veškeré veřejné a chráněné metody, kterou nejsou statické a
		 * vracejí datový typu returnType.
		 * 
		 * Toto je potřeba, protože v tomto případě se jedná o zavolání nějaké metody
		 * nad instancí v diagramu instancí, tak musím tu instanci prohledat i její
		 * předky a vzít si možné metody i z nich.
		 * 
		 * Pokud se ale jedná o zavolání statické metody nad třídou v diagramu tříd, pak
		 * se nesmí vyhledávat v té instanci! jen veřejné metody z instancí a jinak
		 * statické.
		 * 
		 * 
		 * Note:
		 * Tento postup s prochazením mapy je jen, že si potřebuji vzít metody z té
		 * instance a s ní i referenci a intanci samotné třídy.
		 */
		if (reference != null)
			instancesInId.entrySet().stream().filter(i -> i.getKey().equals(reference))
					.forEach(i -> availableMethods.addAll(
							getPublicProtectedNonStaticMethodsWithReturnListGenericCollection(i.getValue().getClass(),
									i.getValue(), i.getKey(), new ArrayList<>())));



		
		
		/*
		 * Získám si všechny veřejné a chráněné statické metody z třídy clazz a všech
		 * jejích předků.
		 */
		availableMethods
				.addAll(getPublicProtectedStaticMethodWithReturnListGenericCollection(clazz, new ArrayList<>()));

		
		/*
		 * Zde si načtu všechny veřejné statické metody, které vracejí returnType a
		 * nemusím zde testovat metody, které se nachází například v předcích třídy
		 * clazz, protože pokud se již využily, pak budou v listu usedMethods, takže
		 * jsem si tímto zařídil, že nevznikou duplicity metod.
		 * 
		 * Zde testuji veškeré třídy v diagramu tříd, a žádná z těch metod, které se zde
		 * najdou by se v listu ještě neměla vyskytovat, protože výše se hledají pouze
		 * veřejné, popřípadě chráněné nestatické metody ze všech předků nebo z tříd
		 * samotných, ale v instancích, zde se prohledávají statické metody z tříd v
		 * diagramu tříd.
		 */
		availableMethods.addAll(getAllPublicStaticMethodsWithReturnListGenericCollection());

		
		return availableMethods;
	}


	/**
	 * Metoda, která zjistí, zda metoda method vrací List, a pak se zjistí datový typ toho listu a o něm se zjistí, zda
	 * je to stejný datový typ, jako v dataType.
	 * <p>
	 * Zdroj: https://stackoverflow.com/questions/31957173/getting-type-of-element-in-a-list-in-which-list-is-the
	 * -return-type-of-method-in
	 *
	 * @param method
	 *         - metoda, o které se zjistí, zda vrací datový typ kindOfList a ten List (některá z jeho implementací) je
	 *         datového typu dataType.
	 * @param kindOfList
	 *         - datový typ některé z implementací listu nebo list samotný, například List, ArrayList, LinkedList, ...
	 *         Jedná se o proměnnou, kterou musí metoda vracet a tato proměnné musá být typu dataType.
	 * @param dataType
	 *         - datový typ, který má být v tom listu.
	 *
	 * @return true, pokud je dataType stejný jako datový typ v listu methodReturnType, jinak false.
	 */
	private static boolean areDataTypesSame(final Method method, final Class<?> kindOfList, final Object dataType) {
		if (kindOfList.equals(List.class)) {
			if (!ReflectionHelper.isDataTypeOfList(method.getReturnType()))
				return false;
		} else if (!method.getReturnType().equals(kindOfList))
			return false;

		final DataTypeOfList listDataType = ReflectionHelper.getDataTypeOfList(method);

		/*
		 * V případě, že je listDataType null, pak se jedná o generický datový typ
		 * listu, takže lze do této hodnoty předat v podstatě libolvolný datový typ.
		 * Jednak bych zde mohl vrátit true, ale v této metodě nedokážu říci, co s čím
		 * testuji, zda například zavolanou metodu s parametrem nebo obráceně, tak
		 * vrátím false, jako že se nerovnají a v příslušné metodě, resp. někde jinde na
		 * příslušném místě si to již dle potřeby "rozeberu" jak potřebuji.
		 */

		if (listDataType.getDataTypeOfListEnum() == DataTypeOfListEnum.SPECIFIC_DATA_TYPE)
			// Metoda method musí vracet typ, který lze vložit do parametru (dataType)
			return ReflectionHelper.compareDataType(dataType, listDataType.getDataType());

		return false;
	}
	
	
	
	

	
	
	
	
	/**
	 * Metoda, která slouží pro získání veřejných a chráněných metod, které nejsou
	 * statické. Metoda se získají z třídy clazz a všech jejich předků.
	 * 
	 * @param clazz
	 *            - třída, ze které se mají vzít veřejné a chráněné nestatické
	 *            metody, dále pak z předků této třídy (až po třídu Object - bez
	 *            ní).
	 * 
	 * @param objInstance
	 *            - instance třídy, ze které se berou hodnoty.
	 * 
	 * @param reference
	 *            - reference na příslušnou instanci.
	 * 
	 * @param returnType
	 *            - datový typ, který má metoda vracet.
	 * 
	 * @param availableMethods
	 *            - list, kam se budou vkládat nalezené metody splňující výše
	 *            uvedený kritéria, tento list na konec metoda vrátí.
	 * 
	 * @return list availableMethods, který bude obsahovat veškeré nalezené metody,
	 *         které splňují výše uvedená kritéria, tj. nejsou statické a jsou
	 *         veřejné nebo chráněné a tyto metody jsou ve třídě clazz a všech
	 *         jejich předcích.
	 */
	private static List<MethodValue> getPublicProtectedNonStaticMethods(final Class<?> clazz, final Object objInstance,
			final String reference, final Class<?> returnType, final List<MethodValue> availableMethods) {

		Arrays.stream(clazz.getDeclaredMethods())
                .filter(m -> (Modifier.isPublic(m.getModifiers()) || Modifier.isProtected(m.getModifiers()))
                        && !Modifier.isStatic(m.getModifiers()) && m.getReturnType().equals(returnType))
                .forEach(m -> {
                    availableMethods.add(new MethodValue(m, objInstance, reference));

                    if (!USED_METHODS.contains(m))
                        USED_METHODS.add(m);
                });

		if (clazz.getSuperclass() != null && !clazz.getSuperclass().equals(Object.class))
			getPublicProtectedNonStaticMethods(clazz.getSuperclass(), objInstance, reference, returnType,
					availableMethods);

		return availableMethods;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro získání veřejných a chráněných metod, které nejsou
	 * statické. Metoda se získají z třídy clazz a všech jejich předků.
	 * 
	 * Jedná se o nalezení metod, které vracejí libovolný datový typ. resp. jsou to
	 * metody, které nejsou void.
	 * 
	 * @param clazz
	 *            - třída, ze které se mají vzít veřejné a chráněné nestatické
	 *            metody, dále pak z předků této třídy (až po třídu Object - bez
	 *            ní).
	 * 
	 * @param objInstance
	 *            - instance třídy, ze které se berou hodnoty.
	 * 
	 * @param reference
	 *            - reference na příslušnou instanci.
	 * 
	 * @param availableMethods
	 *            - list, kam se budou vkládat nalezené metody splňující výše
	 *            uvedený kritéria, tento list na konec metoda vrátí.
	 * 
	 * @return list availableMethods, který bude obsahovat veškeré nalezené metody,
	 *         které splňují výše uvedená kritéria, tj. nejsou statické a jsou
	 *         veřejné nebo chráněné a tyto metody jsou ve třídě clazz a všech
	 *         jejich předcích.
	 */
	private static List<MethodValue> getPublicProtectedNonStaticMethods(final Class<?> clazz, final Object objInstance,
			final String reference, final List<MethodValue> availableMethods) {

        Arrays.stream(clazz.getDeclaredMethods())
                .filter(m -> (Modifier.isPublic(m.getModifiers()) || Modifier.isProtected(m.getModifiers()))
                        && !Modifier.isStatic(m.getModifiers()) && !m.getReturnType().equals(Void.TYPE))
                .forEach(m -> {
                    availableMethods.add(new MethodValue(m, objInstance, reference));

                    if (!USED_METHODS.contains(m))
                        USED_METHODS.add(m);
                });

		if (clazz.getSuperclass() != null && !clazz.getSuperclass().equals(Object.class))
			getPublicProtectedNonStaticMethods(clazz.getSuperclass(), objInstance, reference, availableMethods);

		return availableMethods;
	}
	
	
	
	

	
	
	/**
	 * Metoda, která slouží pro získání veřejných a chráněných metod, které nejsou
	 * statické. Metoda se získají z třídy clazz a všech jejich předků.
	 * 
	 * Metody musejí vracet datový typ List (nebo jednu z jedho implementací) a ten
	 * musí být datového typu returnType.
	 * 
	 * @param clazz
	 *            - třída, ze které se mají vzít veřejné a chráněné nestatické
	 *            metody, dále pak z předků této třídy (až po třídu Object - bez
	 *            ní).
	 * 
	 * @param objInstance
	 *            - instance třídy, ze které se berou hodnoty.
	 * 
	 * @param reference
	 *            - reference na příslušnou instanci.
	 * 
	 * @param kindOfList
	 *            - typ některé z implementace listu, která má být datového typu
	 *            returnType. Metody, které vrací kindOfList<returnType> se vyberou.
	 * 
	 * @param returnType
	 *            - datový typ, list kindOfList, který mají vracet příslušné metody.
	 * 
	 * @param availableMethods
	 *            - list, kam se budou vkládat nalezené metody splňující výše
	 *            uvedený kritéria, tento list na konec metoda vrátí.
	 * 
	 * @return list availableMethods, který bude obsahovat veškeré nalezené metody,
	 *         které splňují výše uvedená kritéria, tj. nejsou statické a jsou
	 *         veřejné nebo chráněné a tyto metody jsou ve třídě clazz a všech
	 *         jejich předcích.
	 */
	private static List<MethodValue> getPublicProtectedNonStaticMethodsWithReturnList(final Class<?> clazz,
			final Object objInstance, final String reference, final Class<?> kindOfList, final Object returnType,
			final List<MethodValue> availableMethods) {
		Arrays.stream(clazz.getDeclaredMethods())
				.filter(m -> (Modifier.isPublic(m.getModifiers()) || Modifier.isProtected(m.getModifiers()))
						&& !Modifier.isStatic(m.getModifiers()) && areDataTypesSame(m, kindOfList, returnType))
				.forEach(m -> {
					availableMethods.add(new MethodValue(m, objInstance, reference));

					if (!USED_METHODS.contains(m))
						USED_METHODS.add(m);
				});

		if (clazz.getSuperclass() != null && !clazz.getSuperclass().equals(Object.class))
			getPublicProtectedNonStaticMethodsWithReturnList(clazz.getSuperclass(), objInstance, reference, kindOfList,
					returnType, availableMethods);

		return availableMethods;
	}
	
	
	
	

	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro získání veřejných a chráněných metod, které nejsou
	 * statické. Metoda se získají z třídy clazz a všech jejich předků.
	 * 
	 * Metody musejí vracet datový typ List a ten může být libolného datového typu,
	 * tj. jedná se o zavolání metody nebo konstruktoru, který má parametr typu
	 * List, které je generického datového typu, tak do něj mohu předat libovolnou
	 * hodnotu.
	 * 
	 * @param clazz
	 *            - třída, ze které se mají vzít veřejné a chráněné nestatické
	 *            metody, dále pak z předků této třídy (až po třídu Object - bez
	 *            ní).
	 * 
	 * @param objInstance
	 *            - instance třídy, ze které se berou hodnoty.
	 * 
	 * @param reference
	 *            - reference na příslušnou instanci.
	 * 
	 * @param kindOfList
	 *            - typ některé z implementace listu, který může být libolného
	 *            datového typu.
	 * 
	 * @param availableMethods
	 *            - list, kam se budou vkládat nalezené metody splňující výše
	 *            uvedený kritéria, tento list na konec metoda vrátí.
	 * 
	 * @return list availableMethods, který bude obsahovat veškeré nalezené metody,
	 *         které splňují výše uvedená kritéria, tj. nejsou statické a jsou
	 *         veřejné nebo chráněné a tyto metody jsou ve třídě clazz a všech
	 *         jejich předcích.
	 */
	private static List<MethodValue> getPublicProtectedNonStaticMethodsWithReturnListGeneric(final Class<?> clazz,
			final Object objInstance, final String reference, final Class<?> kindOfList,
			final List<MethodValue> availableMethods) {

		Arrays.stream(clazz.getDeclaredMethods())
                .filter(m -> (Modifier.isPublic(m.getModifiers()) || Modifier.isProtected(m.getModifiers()))
                        && !Modifier.isStatic(m.getModifiers()) && m.getReturnType().equals(kindOfList))
                .forEach(m -> {
                    availableMethods.add(new MethodValue(m, objInstance, reference));

                    if (!USED_METHODS.contains(m))
                        USED_METHODS.add(m);
                });

		if (clazz.getSuperclass() != null && !clazz.getSuperclass().equals(Object.class))
			getPublicProtectedNonStaticMethodsWithReturnListGeneric(clazz.getSuperclass(), objInstance, reference,
					kindOfList, availableMethods);

		return availableMethods;
	}
	
	
	


	
	

	
	
	/**
	 * Metoda, která slouží pro získání veřejných a chráněných metod, které nejsou
	 * statické. Metoda se získají z třídy clazz a všech jejich předků.
	 * 
	 * Metody musejí vracet datový typ List (nebo jednu z jeho implementací) a ten
	 * může být libolného datového typu, tj. jedná se o zavolání metody nebo
	 * konstruktoru, který má parametr typu List, které je generického datového
	 * typu, tak do něj mohu předat libovolnou hodnotu.
	 * 
	 * @param clazz
	 *            - třída, ze které se mají vzít veřejné a chráněné nestatické
	 *            metody, dále pak z předků této třídy (až po třídu Object - bez
	 *            ní).
	 * 
	 * @param objInstance
	 *            - instance třídy, ze které se berou hodnoty.
	 * 
	 * @param reference
	 *            - reference na příslušnou instanci.
	 * 
	 * @param availableMethods
	 *            - list, kam se budou vkládat nalezené metody splňující výše
	 *            uvedený kritéria, tento list na konec metoda vrátí.
	 * 
	 * @return list availableMethods, který bude obsahovat veškeré nalezené metody,
	 *         které splňují výše uvedená kritéria, tj. nejsou statické a jsou
	 *         veřejné nebo chráněné a tyto metody jsou ve třídě clazz a všech
	 *         jejich předcích.
	 */
	private static List<MethodValue> getPublicProtectedNonStaticMethodsWithReturnListGenericCollection(
			final Class<?> clazz, final Object objInstance, final String reference,
			final List<MethodValue> availableMethods) {

        Arrays.stream(clazz.getDeclaredMethods())
                .filter(m -> (Modifier.isPublic(m.getModifiers()) || Modifier.isProtected(m.getModifiers()))
                        && !Modifier.isStatic(m.getModifiers()) && ReflectionHelper.isDataTypeOfList(m.getReturnType()))
                .forEach(m -> {
                    availableMethods.add(new MethodValue(m, objInstance, reference));

                    if (!USED_METHODS.contains(m))
                        USED_METHODS.add(m);
                });

		if (clazz.getSuperclass() != null && !clazz.getSuperclass().equals(Object.class))
			getPublicProtectedNonStaticMethodsWithReturnListGenericCollection(clazz.getSuperclass(), objInstance,
					reference, availableMethods);

		return availableMethods;
	}
	
	
	
	
	

	
	
	
	
	/**
	 * Metoda, která zjistí všechny veřejné statické metody, které vracejí
	 * returnType.
	 * 
	 * @param returnType
	 *            - datový typ, která má metoda vracet.
	 * 
	 * @return list, který bude obsahovat metody, které jsou veřejné statické a
	 *         vracejí returnType.
	 */
	private static List<MethodValue> getAllPublicStaticMethods(final Class<?> returnType) {
		/*
		 * Všechny načtené třídy, které se nacházejí v diagramu tříd, jsou zde načteny
		 * jejich zkompiilované / přeložené verze.
		 */
		final List<Class<?>> allClassesInCd = getAllClassesFromCd();

		/*
		 * List, kam vložím veškeré nalezené veřejné statické metody, které vracejí
		 * returnType.
		 */
		final List<MethodValue> availableMethodsList = new ArrayList<>();

		/*
		 * Zjistím si metody, které jsou veřejné statické a vracejí returnType a ještě
		 * nebyly testovány, tímto jsem si pořešil, že nemusím hlídat, zda jsem již
		 * přísulšnou třídu testoval nebo ne, pokud ano, pak tak metoda již bude v
		 * kolekci.
		 */
        allClassesInCd
                .forEach(
                        c -> Arrays.stream(c.getDeclaredMethods())
                                .filter(m -> Modifier.isPublic(m.getModifiers()) && Modifier.isStatic(m.getModifiers())
                                        && m.getReturnType().equals(returnType) && !USED_METHODS.contains(m))
                                .forEach(m -> {
                                    availableMethodsList.add(new MethodValue(m, c));
                                    USED_METHODS.add(m);
                                }));

		return availableMethodsList;
	}
	
	
	
	

	
	
	
	
	/**
	 * Metoda, která zjistí všechny veřejné statické metody, které vracejí
	 * libovolnou hodnotu, tj. nejsou void.
	 * 
	 * @return list, který bude obsahovat metody, které jsou veřejné statické a
	 *         vracejí returnType.
	 */
	private static List<MethodValue> getAllPublicStaticMethodsNonVoid() {
		/*
		 * Všechny načtené třídy, které se nacházejí v diagramu tříd, jsou zde načteny
		 * jejich zkompiilované / přeložené verze.
		 */
		final List<Class<?>> allClassesInCd = getAllClassesFromCd();

		/*
		 * List, kam vložím veškeré nalezené veřejné statické metody, které vracejí
		 * returnType.
		 */
		final List<MethodValue> availableMethodsList = new ArrayList<>();

		/*
		 * Zjistím si metody, které jsou veřejné statické a vracejí returnType a ještě
		 * nebyly testovány, tímto jsem si pořešil, že nemusím hlídat, zda jsem již
		 * přísulšnou třídu testoval nebo ne, pokud ano, pak tak metoda již bude v
		 * kolekci.
		 */
		allClassesInCd
				.forEach(
						c -> Arrays.stream(c.getDeclaredMethods())
								.filter(m -> Modifier.isPublic(m.getModifiers()) && Modifier.isStatic(m.getModifiers())
										&& !m.getReturnType().equals(Void.TYPE) && !USED_METHODS.contains(m))
								.forEach(m -> {
									availableMethodsList.add(new MethodValue(m, c));
									USED_METHODS.add(m);
								}));

		return availableMethodsList;
	}
	
	
	

	
	
	
	
	
	/**
	 * Metoda, která zjistí všechny veřejné statické metody, které vracejí List
	 * (jednu z jeho implementací), který je typu returnType - List<returnType>.
	 * 
	 * @param kindOfList
	 *            - jedna z implementací listu, který je typu returnType
	 *            (kindOfList<returnType>) a toto musí metoda vrace, aby byla
	 *            vybrána (mimo jiné).
	 * 
	 * @param returnType
	 *            - datový typ Listu, který má metoda vracet.
	 * 
	 * @return list, který bude obsahovat metody, které jsou veřejné statické a
	 *         vracejí returnType.
	 */
	private static List<MethodValue> getAllPublicStaticMethodsWithReturnList(final Class<?> kindOfList,
			final Object returnType) {
		/*
		 * Všechny načtené třídy, které se nacházejí v diagramu tříd, jsou zde načteny
		 * jejich zkompiilované / přeložené verze.
		 */
		final List<Class<?>> allClassesInCd = getAllClassesFromCd();

		/*
		 * List, kam vložím veškeré nalezené veřejné statické metody, které vracejí
		 * returnType.
		 */
		final List<MethodValue> availableMethodsList = new ArrayList<>();

		/*
		 * Zjistím si metody, které jsou veřejné statické a vracejí returnType a ještě
		 * nebyly testovány, tímto jsem si pořešil, že nemusím hlídat, zda jsem již
		 * přísulšnou třídu testoval nebo ne, pokud ano, pak tak metoda již bude v
		 * kolekci.
		 */
		allClassesInCd
				.forEach(c -> Arrays.stream(c.getDeclaredMethods())
						.filter(m -> Modifier.isPublic(m.getModifiers()) && Modifier.isStatic(m.getModifiers())
								&& areDataTypesSame(m, kindOfList, returnType) && !USED_METHODS.contains(m))
						.forEach(m -> {
							availableMethodsList.add(new MethodValue(m, c));
							USED_METHODS.add(m);
						}));

		return availableMethodsList;
	}
	
	

	
	
	
	
	
	/**
	 * Metoda, která zjistí všechny veřejné statické metody, které vracejí List,
	 * který je libovolného datového typu.
	 * 
	 * Jde o to, že uživatel zavolal konstruktor nebo metodu, která má parametr typu
	 * List (nebo jedna z jeho implementací ) a je generického datového typu, takže
	 * mohu najít / vyfiltrovat listy, které vrací List (nebo jednu z jeho
	 * implementaci) a nemusím testovat jakého je ten List datového typu.
	 * 
	 * @param kindOfList
	 *            - jedna z implementací listu, který je libolného datového typu.
	 * 
	 * @return list, který bude obsahovat metody, které jsou veřejné statické a
	 *         vracejí List (nebo jednu z jeho implementací), který je libovolného
	 *         datového typu.
	 */
	private static List<MethodValue> getAllPublicStaticMethodsWithReturnListGeneric(final Class<?> kindOfList) {
		/*
		 * Všechny načtené třídy, které se nacházejí v diagramu tříd, jsou zde načteny
		 * jejich zkompiilované / přeložené verze.
		 */
		final List<Class<?>> allClassesInCd = getAllClassesFromCd();

		/*
		 * List, kam vložím veškeré nalezené veřejné statické metody, které vracejí
		 * returnType.
		 */
		final List<MethodValue> availableMethodsList = new ArrayList<>();

		/*
		 * Zjistím si metody, které jsou veřejné statické a vracejí returnType a ještě
		 * nebyly testovány, tímto jsem si pořešil, že nemusím hlídat, zda jsem již
		 * přísulšnou třídu testoval nebo ne, pokud ano, pak tak metoda již bude v
		 * kolekci.
		 */
		allClassesInCd
				.forEach(
						c -> Arrays.stream(c.getDeclaredMethods())
								.filter(m -> Modifier.isPublic(m.getModifiers()) && Modifier.isStatic(m.getModifiers())
										&& m.getReturnType().equals(kindOfList) && !USED_METHODS.contains(m))
								.forEach(m -> {
									availableMethodsList.add(new MethodValue(m, c));
									USED_METHODS.add(m);
								}));

		return availableMethodsList;
	}
	
	
	
	
	

	
	/**
	 * Metoda, která zjistí všechny veřejné statické metody, které vracejí List
	 * (jednu z jeho implementací), který je libovolného datového typu.
	 * 
	 * Jde o to, že uživatel zavolal konstruktor nebo metodu, která má parametr typu
	 * List (nebo jedna z jeho implementací) a je generického datového typu, takže
	 * mohu najít / vyfiltrovat listy, které vrací List (nebo jednu z jeho
	 * implementaci) a nemusím testovat jakého je ten List datového typu.
	 * 
	 * @return list, který bude obsahovat metody, které jsou veřejné statické a
	 *         vracejí List (nebo jednu z jeho implementací), který je libovolného
	 *         datového typu.
	 */
	private static List<MethodValue> getAllPublicStaticMethodsWithReturnListGenericCollection() {
		/*
		 * Všechny načtené třídy, které se nacházejí v diagramu tříd, jsou zde načteny
		 * jejich zkompiilované / přeložené verze.
		 */
		final List<Class<?>> allClassesInCd = getAllClassesFromCd();

		/*
		 * List, kam vložím veškeré nalezené veřejné statické metody, které vracejí
		 * returnType.
		 */
		final List<MethodValue> availableMethodsList = new ArrayList<>();

		/*
		 * Zjistím si metody, které jsou veřejné statické a vracejí returnType a ještě
		 * nebyly testovány, tímto jsem si pořešil, že nemusím hlídat, zda jsem již
		 * přísulšnou třídu testoval nebo ne, pokud ano, pak tak metoda již bude v
		 * kolekci.
		 */
        allClassesInCd.forEach(c -> Arrays.stream(c.getDeclaredMethods())
                .filter(m -> Modifier.isPublic(m.getModifiers()) && Modifier.isStatic(m.getModifiers())
                        && ReflectionHelper.isDataTypeOfList(m.getReturnType()) && !USED_METHODS.contains(m))
                .forEach(m -> {
                    availableMethodsList.add(new MethodValue(m, c));
                    USED_METHODS.add(m);
                }));

		return availableMethodsList;
	}
	
	
	
	
	

	
	
	/**
	 * Metoda, která slouží pro získání všech veřejných a chráněných metod, které
	 * jsou statické. Tyto metody se získají z třídy clazz a všeh jejích předků.
	 * 
	 * @param clazz
	 *            - třída, ze které se mají získat veřejné a chráněné statické
	 *            metody a všech předků této třídy.
	 * 
	 * @param availableMethods
	 *            - list, kam se budou vkládat nalezené metody.
	 * 
	 * @param returnType
	 *            - datový typ, který by měla metoda vracet.
	 * 
	 * @return list, který bude obsahovat veřejné a chráněné statické metody z třídy
	 *         clazz a všech jejích předků.
	 */	
	private static List<MethodValue> getPublicProtectedStaticMethod(final Class<?> clazz,
			final List<MethodValue> availableMethods, final Class<?> returnType) {
		/*
		 * Načtu si z třídy clazz a všech jejich předků veřejné a chráněné metody, které
		 * jsou statické a vracení returntType a ještě nejsou v listu usedMethods.
		 */
		Arrays.stream(clazz.getDeclaredMethods())
				.filter(m -> (Modifier.isPublic(m.getModifiers()) || Modifier.isProtected(m.getModifiers()))
						&& Modifier.isStatic(m.getModifiers()) && m.getReturnType().equals(returnType))
				.forEach(m -> {
					if (!USED_METHODS.contains(m)) {
						availableMethods.add(new MethodValue(m, clazz));
						USED_METHODS.add(m);
					}
				});

		// Rekurzí pokračuji s předkem třídy clazz:
		if (clazz.getSuperclass() != null && !clazz.getSuperclass().equals(Object.class))
			getPublicProtectedStaticMethod(clazz.getSuperclass(), availableMethods, returnType);

		return availableMethods;
	}
	
	
	
	
	

	
	
	
	/**
	 * Metoda, která slouží pro získání všech veřejných a chráněných metod, které
	 * jsou statické. Tyto metody se získají z třídy clazz a všeh jejích předků.
	 * 
	 * Vyhledávají se pouze metody, které vracení nějakou hodnotu, tj. nejsou void.
	 * 
	 * @param clazz
	 *            - třída, ze které se mají získat veřejné a chráněné statické
	 *            metody a všech předků této třídy.
	 * 
	 * @param availableMethods
	 *            - list pro vkládání nalezených výše popsaných metod.
	 * 
	 * @return list, který bude obsahovat veřejné a chráněné statické metody z třídy
	 *         clazz a všech jejích předků.
	 */
	private static List<MethodValue> getPublicProtectedStaticMethod(final Class<?> clazz,
			final List<MethodValue> availableMethods) {
		/*
		 * Načtu si z třídy clazz a všech jejich předků veřejné a chráněné metody, které
		 * jsou statické a vracení returntType a ještě nejsou v listu usedMethods.
		 */
		Arrays.stream(clazz.getDeclaredMethods())
				.filter(m -> (Modifier.isPublic(m.getModifiers()) || Modifier.isProtected(m.getModifiers()))
						&& Modifier.isStatic(m.getModifiers()) && !m.getReturnType().equals(Void.TYPE))
				.forEach(m -> {
					if (!USED_METHODS.contains(m)) {
						availableMethods.add(new MethodValue(m, clazz));
						USED_METHODS.add(m);
					}
				});

		// Rekurzí pokračuji s předkem třídy clazz:
		if (clazz.getSuperclass() != null && !clazz.getSuperclass().equals(Object.class))
			getPublicProtectedStaticMethod(clazz.getSuperclass(), availableMethods);

		return availableMethods;
	}
	
	
	
	

	
	
	/**
	 * Metoda, která slouží pro získání všech veřejných a chráněných metod, které
	 * jsou statické. Tyto metody se získají z třídy clazz a všeh jejích předků.
	 * 
	 * Metody musejí vracet datový typ List (implementace listu kindOfList), který
	 * je typu returnType.
	 * 
	 * @param clazz
	 *            - třída, ze které se mají získat veřejné a chráněné statické
	 *            metody a všech předků této třídy.
	 * 
	 * @param availableMethods
	 *            - list, kam se budou vkládat nalezené metody.
	 * 
	 * @param kindOfList
	 *            - jedna z implementací listu, který je typu returnType, což musí
	 *            metoda vracet, aby mohla být vybrána (mimo jiné).
	 * 
	 * @param returnType
	 *            - datový typ kindOfList (listu).
	 * 
	 * @return list, který bude obsahovat veřejné a chráněné statické metody z třídy
	 *         clazz a všech jejích předků.
	 */	
	private static List<MethodValue> getPublicProtectedStaticMethodWithReturnList(final Class<?> clazz,
			final List<MethodValue> availableMethods, final Class<?> kindOfList, final Object returnType) {
		/*
		 * Načtu si z třídy clazz a všech jejich předků veřejné a chráněné metody, které
		 * jsou statické a vracení returntType a ještě nejsou v listu usedMethods.
		 */
		Arrays.stream(clazz.getDeclaredMethods())
				.filter(m -> (Modifier.isPublic(m.getModifiers()) || Modifier.isProtected(m.getModifiers()))
						&& Modifier.isStatic(m.getModifiers()) && areDataTypesSame(m, kindOfList, returnType))
				.forEach(m -> {
					if (!USED_METHODS.contains(m)) {
						availableMethods.add(new MethodValue(m, clazz));
						USED_METHODS.add(m);
					}
				});

		// Rekurzí pokračuji s předkem třídy clazz:
		if (clazz.getSuperclass() != null && !clazz.getSuperclass().equals(Object.class))
			getPublicProtectedStaticMethodWithReturnList(clazz.getSuperclass(), availableMethods, kindOfList,
					returnType);

		return availableMethods;
	}
	
	
	
	
	

	
	
	
	/**
	 * Metoda, která slouží pro získání všech veřejných a chráněných metod, které
	 * jsou statické. Tyto metody se získají z třídy clazz a všeh jejích předků.
	 * 
	 * Metody musejí vracet datový typ List (implementace listu kindOfList), který
	 * může být libolvného datového typu. Jde o to, že uživatel zavolal metodu nebo
	 * konstruktor, který má parametr typu List, který je generického datévého typu,
	 * tak mohu vyfiltrovat veškeré metody, který vracejí List (kindOfList), který
	 * je libolvného datového typu, který není třeba testovat.
	 * 
	 * @param clazz
	 *            - třída, ze které se mají získat veřejné a chráněné statické
	 *            metody a všech předků této třídy.
	 * 
	 * @param availableMethods
	 *            - list, kam se budou vkládat nalezené metody.
	 * 
	 * @param kindOfList
	 *            - jedna z implementací listu, který může být libolného datového
	 *            typu, který musí metoda vracet, aby mohla být vybrána (mimo jiné).
	 * 
	 * @return list, který bude obsahovat veřejné a chráněné statické metody z třídy
	 *         clazz a všech jejích předků.
	 */
	private static List<MethodValue> getPublicProtectedStaticMethodWithReturnListGeneric(final Class<?> clazz,
			final List<MethodValue> availableMethods, final Class<?> kindOfList) {
		/*
		 * Načtu si z třídy clazz a všech jejich předků veřejné a chráněné metody, které
		 * jsou statické a vracení returntType a ještě nejsou v listu usedMethods.
		 */
        Arrays.stream(clazz.getDeclaredMethods())
                .filter(m -> (Modifier.isPublic(m.getModifiers()) || Modifier.isProtected(m.getModifiers()))
                        && Modifier.isStatic(m.getModifiers()) && m.getReturnType().equals(kindOfList))
                .forEach(m -> {
                    if (!USED_METHODS.contains(m)) {
                        availableMethods.add(new MethodValue(m, clazz));
                        USED_METHODS.add(m);
                    }
                });

		// Rekurzí pokračuji s předkem třídy clazz:
		if (clazz.getSuperclass() != null && !clazz.getSuperclass().equals(Object.class))
			getPublicProtectedStaticMethodWithReturnListGeneric(clazz.getSuperclass(), availableMethods, kindOfList);

		return availableMethods;
	}
	
	
	

	
	
	
	/**
	 * Metoda, která slouží pro získání všech veřejných a chráněných metod, které
	 * jsou statické. Tyto metody se získají z třídy clazz a všeh jejích předků.
	 * 
	 * Metody musejí vracet datový typ List nebo jednu z jeho implementací, který
	 * může být libolvného datového typu. Jde o to, že uživatel zavolal metodu nebo
	 * konstruktor, který má parametr typu List, který je generického datévého typu,
	 * tak mohu vyfiltrovat veškeré metody, který vracejí List (nebo jeho
	 * implementaci), který je libolvného datového typu, který není třeba testovat.
	 * 
	 * @param clazz
	 *            - třída, ze které se mají získat veřejné a chráněné statické
	 *            metody a všech předků této třídy.
	 * 
	 * @param availableMethods
	 *            - list, kam se budou vkládat nalezené metody.
	 * 
	 * @return list, který bude obsahovat veřejné a chráněné statické metody z třídy
	 *         clazz a všech jejích předků.
	 */
	private static List<MethodValue> getPublicProtectedStaticMethodWithReturnListGenericCollection(
			final Class<?> clazz, final List<MethodValue> availableMethods) {
		/*
		 * Načtu si z třídy clazz a všech jejich předků veřejné a chráněné metody, které
		 * jsou statické a vracení returntType a ještě nejsou v listu usedMethods.
		 */
		Arrays.stream(clazz.getDeclaredMethods())
				.filter(m -> (Modifier.isPublic(m.getModifiers()) || Modifier.isProtected(m.getModifiers()))
						&& Modifier.isStatic(m.getModifiers()) && ReflectionHelper.isDataTypeOfList(m.getReturnType()))
				.forEach(m -> {
					if (!USED_METHODS.contains(m)) {
						availableMethods.add(new MethodValue(m, clazz));
						USED_METHODS.add(m);
					}
				});

		// Rekurzí pokračuji s předkem třídy clazz:
		if (clazz.getSuperclass() != null && !clazz.getSuperclass().equals(Object.class))
			getPublicProtectedStaticMethodWithReturnListGenericCollection(clazz.getSuperclass(), availableMethods);

		return availableMethods;
	}
	
	
	

	
	
	
	/**
	 * Metoda, která načte veškeré třídy z diagramu tříd.
	 * 
	 * @return list, který bude obsahovat veškeré načtené třídy zdiagramu tříd, tedy
	 *         jejich přeložené / zkompilované verze, tj. soubory .class.
	 */
	private static List<Class<?>> getAllClassesFromCd() {
		/*
		 * List, do kterého vložím veškeré načtené třídy v diagramu tříd.
		 */
		final List<Class<?>> classesList = new ArrayList<>();

		GraphClass.getCellsList().stream()
				/*
				 * Vyfiltruji ze všech objektů v diagramu tříd pouze třídy, tj. ne hrany nebo
				 * komentáře:
				 */
				.filter(c -> !(c instanceof DefaultEdge) && GraphClass.KIND_OF_EDGE.isClassCell(c)).forEach(clazz -> {
					/*
					 * Získám si text buňky, dle toho si načtu požadovanou třídu z příslušného
					 * balíčku.
					 */
					final String cellTextWithDot = clazz.toString();

					/*
					 * Načtu si příslušnou třídu, která se přívě iteruje.
					 */
					final Class<?> loadedClazz = App.READ_FILE.loadCompiledClass(cellTextWithDot, null);

					/*
					 * Zjistím, zda se podařilo načíst příslušnou třídu, pokud ano, pak pro jistotu
					 * otestuji, zda se v listu ještě ta načtená třída nenachází, ale toto je
					 * zbytečná část, protože v diagramu tříd je každá třída jednou, takže by ani
					 * nemohly být duplicity a i kdyby byly, ničemu by to nevadilo.
					 */
					if (loadedClazz != null && !classesList.contains(loadedClazz))
						classesList.add(loadedClazz);
				});

		// Vrátím načtené třídy z diagramu tříd:
		return classesList;
	}
	
	
	
	
	

	
	
	
	/**
	 * Netoda, která zjistí, zda je třída clazz jedna z tříd, která se nachází v
	 * diagramu tříd.
	 * 
	 * @param clazz
	 *            - nějaká třída, o které se má zjistit, zda se jedná o třídu, která
	 *            se nachází v diagramu tříd.
	 * 
	 * @return true, pokud je clazz nějaká třída, která senachází v diagramu tříd,
	 *         jinak false.
	 */
	private static boolean isClassFromCd(final Class<?> clazz) {
		/*
		 * Načtu si všechn třídy z diagramu tříd.
		 */
		final List<Class<?>> classesFromCd = getAllClassesFromCd();

		/*
		 * Projdu veškeré načtené třídy z diagramu tříd a otestuji, zda se v nich
		 * nachází třída clazz, pokud ano, vrátí se true, jinak false.
		 */
		return classesFromCd.stream().anyMatch(c -> c.equals(clazz));
	}
}