package cz.uhk.fim.fimj.parameters_for_method;

/**
 * Tato třída slouží pouze k tomu, aby bylo v dialogu pro zavolání metody nebo konstruktoru možnost předat do parametru
 * příslušné metody nebo konstruktoru hodnotu null, ale to se bude týkat pouze objektových daotových typů.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class NullValue extends CmbValueAncestor {

    @Override
    public String toString() {
        return "null";
    }
}
