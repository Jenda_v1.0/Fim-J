package cz.uhk.fim.fimj.parameters_for_method;

import java.lang.reflect.Field;

import cz.uhk.fim.fimj.commands_editor.MakeOperation;
import cz.uhk.fim.fimj.file.DataTypeOfList;
import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;

/**
 * Tato třída slouží pro vkládání do komponenty JComboBox, resp. instance této třídy slouží pro model komponenty
 * JComboBox, ve které si uživatel bude moci vybrat jako hodnotu chce předat jako parametr do příslušné metody (popř.
 * zadat vlastní).
 * <p>
 * <p>
 * Note:
 * Ještě by zde mohla být proměnná Class<?>, která by definovala "datový typ" příslušné hodnoty, ale vždy při
 * potřebě se vždy zjišťuje tento typ z konkrétní hodnoty pokud není null, protože null hodnoty v aplikaci nevyužívám -
 * nelze je předat do parametru, naplnit proměnnou apod. Tak zde tuto proměnnou nemám.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ParameterValue extends CmbValueAncestor {
	
	/**
	 * Proměnná, která slouží jako hodnota, kterou uživatel "přímo / vlastnoručně"
	 * zadal, tj. Nejedná se o načtenou hodnotu z nějaké proměnné apod. Jedná se o
	 * hodnotu, kterou zadal uživatel.
	 * 
	 * Tato proměnná bude zadána pouze v případě, že tato aktuální instance této
	 * třídy má reprezentovat zadanou hodnotu od uživatele a ne nějakou hodnotu
	 * načtenou z nějaké třídy (proměnnou) nebo vytvořenou proměnnou v editoru
	 * příkazů.
	 */
	private String textValue;
	
	

	
	
	
	/**
	 * Proměnná, která slouží jako proměnná (atribut) z nějaké třídy, která může
	 * obsahovat nějakou hodnotu, kterou je možné předat do parametru metody pro
	 * zavolání.
	 * 
	 * Tato proměnná bude naplněna pouze v případě, že se jedná o to, že tento
	 * objekt (tato třída) reprezentuje aktuálně hodnotu nějaké proměnné a ne zadaný
	 * text od uživatele, resp. hodnota pro parametr nějaké metody pro zavolání nebo
	 * uživatelem vytvořenou proměnnou v editoru příkazů.
	 */
	private final Field field;
	
	/**
	 * Tato proměnná se může využít pouze v případě, že instance této třídy
	 * repezentuje nějakou hodnotu načtenou z proměnné field z nějaké třídy. Tato
	 * hodnota může být null i tak, že se tato null hodnota načte z té proměnné, ale
	 * taky bude null, pokud instance této třídy repezentuje pouze zadaný texty od
	 * uživatele a né načtenou proměnnou, popř. uživatelem vytvořenouu proměnnou z
	 * editoru příkazů.
	 */
	private final Object objValueFromField;
	
	/**
	 * Tato proměnná bude naplněna pouze v případech, kdy tato konkrétní instance
	 * této třídy reprezentuje hodnotu proměnné field a tato proměnná field není
	 * statická, pokud je to stická proměnná, pak není třeba udávat referenci.
	 * 
	 * Dále bude naplněna v případě, kdy se jedná o proměnnou vytvořenou uživatelem
	 * v editoru příkazů.
	 */
	private final String referenceName;
	
	
	
	

	
	
	/**
	 * Proměnná, která "určuje" o "jaký typ" se jedná, resp. jaký "typ" hodnoty
	 * reprezentuje konkrétní instance této třídy. Například, zda instance této
	 * třídy reprezentuje uživatelem zadanou hodnotu, nebo proměnnou získanou z
	 * nějaké třídy nebo proměnnou, kterou uživatel vytvořil v editoru příkazů apod.
	 * Dle toho se volí hodnoty v metodě toString, aby uživatel věděl, "co vybírá" a
	 * taky jaká hodnota se pak po označení má vybrat, resp. hodnota z jaké proměnné
	 * pro předání do příslušné metody.
	 */
	private final KindOfVariable kindOfVariable;
	

	
	
	
	/**
	 * Hodnota proměnné, kterou uživatel vytvořil v editoru příkazů.
	 */
	private final Object objEditorVariable;
	
	
	
	/**
	 * Tato proměnná se naplní pouze v případě, že se jedná o proměnnou načtenou z
	 * editoru příkazů nebo z nějaké třídy a jedná se o proměnnou typu pole. Tato proměnná bude značit datový typ hodnot, které lze do příslušného
	 *  pole vkládat. Je zde pro "snadnější / lepší" testování u získávání
	 * potřebných hodnot.
	 */
	private final Class<?> dataType;
	
	
	
	/**
	 * Tato proměnná slouží pro určení "konkrétního jednorozměrného pole". Tj. v
	 * této proměnné bude konkrétně uloženo například byte[].class nebo Byte[].class
	 * nebo Integer[].class apod. Díky tomu se pozná o "jaké" jednorozměrné pole
	 * jde.
	 * 
	 * Tat proměnná se naplní pouze v případě, že se jedná o proměnnou vytvořenou
	 * editoru příkazů a je to proměnná typu jednorozměrné pole. Zde je tato
	 * proměnná, protože se z proměnné objEditorVariable nepozná tato hodnota,
	 * alespoň nevím jak, tak si zde pomohu a když má nějaká metoda nebo konstruktor
	 * parametr jednorozměrné pole, tak se dle této proměnné může nalézt i hodnota
	 * pro příslušné jednorozměrné pole.
	 */
	private final Class<?> arrayType;


	/**
	 * Datový typ Listu. Bude naplněn pouze v případě, že se zavolá konstruktor s předáním proměnné získané z nějaké
	 * třídy / instance. A ta proměnná je typu List.
	 *
	 * <i>Tato proměnná obsahuje datový typ Listu a informaci o tom, zda se jedná o List s definovaným datovým typem,
	 * generickým typem nebo bez datového typu (pouze "List").</i>
	 */
	private final DataTypeOfList dataTypeOfList;
	
	
	
	
	

	
	/**
	 * Konstruktor této třídy.
	 * 
	 * Tento konstruktor slouží pro vytvoření instance této třídy, který bude
	 * reprezetnovat hodnotu, kterou zadá uživatel.
	 * 
	 * @param kindOfVariable
	 *            - Výčtový typ, který "určuje" jaký "typ" hodnoty reprezentuje
	 *            konkrétní instance této třídy.
	 * 
	 * @param defaultValue
	 *            - výchozí hodnota, která slouží jako ukázka "hodnoty", kterou může
	 *            uživatel zadat, aby jej bylo možné předat do parametru metody.
	 */
	public ParameterValue(final KindOfVariable kindOfVariable, final String defaultValue) {
		this.kindOfVariable = kindOfVariable;
		
		textValue = defaultValue;
		
		field = null;
		objValueFromField = null;
		referenceName = null;
		objEditorVariable = null;
		dataType = null;
		arrayType = null;
		dataTypeOfList = null;
	}
	
	
	
	
	
	
	/**
	 * Konstruktor slouží pro vytvoření instance této třídy, který bude
	 * reprezentovat nějakou proměnnou (atribut) nějaké třídy. Ať už je ta proměnná
	 * statická (bez reference na instanci) nebo je to proměnná, která není statická
	 * a je z nějaké instance třídy, která se nachází v diagramu instancí.
	 * 
	 * @param kindOfVariable
	 *            - Výčtový typ, který "určuje" jaký "typ" hodnoty reprezentuje
	 *            konkrétní instance této třídy.
	 * 
	 * @param field
	 *            - Proměnná, ze které se má vzít její hodnota, kterou je možné
	 *            předat do parametru nějaké metody.
	 * 
	 * @param objInstance
	 *            - instance nějaké třídy, ze které se vzala proměnná field, tato
	 *            proměnná je zde potřeba pouze pro to, aby se z proměnné field
	 *            mohla vzít její hodnota, ale taky tato proměnná objInstance může
	 *            být null v případě, že prměnná field je staická proměnná, pak není
	 *            potřeba instance, aby se vzala její hodnota, protože ve všech
	 *            instancích příslušné třídy bude hodnota té statické proměnné
	 *            stejná.
	 * 
	 * @param referenceName
	 *            - Název reference, kterou uživatel zvolil v diagramu instancí,
	 *            která ukazuje na nějakou instanci nějaké třídy Tato hodnotu může
	 *            být null v případě, že se jedná o statickou proměnnou.
	 */
	public ParameterValue(final KindOfVariable kindOfVariable, final Field field, final Object objInstance, final String referenceName) {

		this.kindOfVariable = kindOfVariable;
		textValue = null;
		objEditorVariable = null;
		
		this.field = field;
		this.referenceName = referenceName;
		objValueFromField = ReflectionHelper.getValueFromField(field, objInstance);
        dataType = null;
        arrayType = null;
		
		/*
		 * V případě, že se jedná o proměnnou typu list, pak mohu získat jeho datový typ pro pozdější testování.
		 */
		if (objValueFromField != null && !objValueFromField.getClass().isPrimitive()
				&& ReflectionHelper.isDataTypeOfList(field.getType()))
			dataTypeOfList = ReflectionHelper.getDataTypeOfList(field);
		else dataTypeOfList = null;
    }
	
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * Konstruktor této třídy slouží pro vytvoření instance této třídy, která bude
	 * reprezentovat proměnnou, kterou uživatel vytvořil v / pomocí editoru příkazů.
	 * 
	 * 
	 * @param kindOfVariable
	 *            - Výčtový typ, který "určuje" jaký "typ" hodnoty reprezentuje
	 *            konkrétní instance této třídy.
	 * 
	 * @param objEditorVariable
	 *            - Hodnota z proměnné, kterou uživatel vytvořil v editoru příkazů.
	 * 
	 * @param referenceName
	 *            - Název reference, kterou uživatel zvolil pro referenci na nějakou
	 *            proměnnou, kterou uživatel vytvoři pomocí editoru příkazů.
	 * 
	 * @param dataType
	 *            - Proměnná, který se uvede pouze v případě, že se jedná o
	 *            proměnnou typu List nebo pole. Tato proměnná značí datový typ
	 *            položek, které lze do pole nebo listu vkládat.
	 * 
	 * @param arrayDataType
	 *            - Tato proměnná se vyplní pouze v případě, že se jedná o proměnnou
	 *            vytvořenou v editoru příkazů a je to proměnná typu jednorozměrné
	 *            pole. Konkrétně tato proměnná značí konkrétní datový typ pro
	 *            jednorozměrné pole - například: byte[].class, nebo Byte[].class
	 *            apod. Je zde potřeba kvůli testování těch polí, protože z hodnoty
	 *            objeditorVariable to nelze zjistit, alespoň tedy nevím jak, tak si
	 *            pomohu touto proměnnou.
	 */
	public ParameterValue(final KindOfVariable kindOfVariable, final Object objEditorVariable,
			final String referenceName, final Class<?> dataType, final Class<?> arrayDataType) {

		this.objEditorVariable = objEditorVariable;
		this.referenceName = referenceName;
		this.dataType = dataType;
		arrayType = arrayDataType;
		
		this.kindOfVariable = kindOfVariable;
		textValue = null;
		
		field = null;
		objValueFromField = null;

		dataTypeOfList = null;
	}
	
	
	
	
	

	
	
	
	@Override
	public String toString() {
		/*
		 * Zde otestuji, zda není hodnota pro zadání textu ("hodnoty") od uživatele
		 * null, pokud není, pak jej vrátím, protože tato instance této třídy má
		 * reprezentovat hodnotu zadanou od uživatele. Pokud je ale tato proměnné
		 * (textValue) null, pak musím vrátit text, který bude obsahovat informace o
		 * tom, z jaké instance třídy je příslušná proměnná a její hodnota, která se po
		 * zvolení předá do parametru metody nebo konstruktoru pro zavolání.
		 * 
		 * Syntaxe:
		 * Pokud je to jen text od uživatele, pak se vypáše jen jeho hodnota.
		 * 
		 * v případě reference (proměnná v instanci třídy):
		 * referenceName -> ClassName: variableName = value
		 * 
		 * v případě statické proměnné:
		 * ClassName: variableName = value
		 * 
		 * v případě proměnné z editoru příkazů:
		 * referenceName = value
		 */
		
		/*
		 * Otestuji, zda se jedná o textovou proměnnou, kterou má uživatel zadat sám.
		 * 
		 * (Dále by bylo možné testovat, zda není ta hodnota null nebo prázdná apod. Ale
		 * to by nastat nemělo)
		 */
		if (kindOfVariable.equals(KindOfVariable.TEXT_VARIABLE))
			return textValue;
		
		
		else if (kindOfVariable.equals(KindOfVariable.FIELD_VARIABLE)) {
			// Zde si načtu hodnoty ze získané proměnné:
			String resultText = "";
			
			
			if (referenceName != null && !referenceName.isEmpty())
				resultText += referenceName + " -> ";
			
			
			resultText += field.getDeclaringClass().getSimpleName();
			
			resultText += ": ";
			
			resultText += field.getName();

			resultText += " = ";

			// Podmínky odkomentovat u mě v aplikaci!
			if (objValueFromField != null && objValueFromField.getClass().isArray())
				/*
				 * Poukud je to více jak jedna dimenze, pak mohu využít metodu deepToString,
				 * abych převedel multi - rozměrné pole do textu, jinak už mám před připravenou
				 * metodu fromArrayToString, ale tato metoda již je upraená tak, že si poradí i
				 * s primitivními datovými typy polí jak v 1 tak i více dimezí - poli.
				 */
				resultText += MakeOperation.fromArrayToString(objValueFromField);

			else
				resultText += objValueFromField;

			return resultText;
		}
		

			/*
			 * Zde se jedná o proměnnou vytvořenou uživatelem v editoru příkazů, bylo by
			 * možné ještě testovat konkrétní výčtový typ, ale v případě, že by byl doplněn
			 * by se zde také musel přidat, tak zde jen nechám else pro "výchozí" hodnotu.
			 */
			
			// Proměnné pro výchozí text, který bude vrácen:
			String resultText = "";

			if (referenceName != null && !referenceName.isEmpty())
				resultText += referenceName + " = ";

			// Podmínky odkomentovat u mě v aplikaci!			
			if (objEditorVariable != null && objEditorVariable.getClass().isArray())
				resultText += MakeOperation.fromArrayToString(objEditorVariable);

			else
				resultText += objEditorVariable;

			return resultText;
	}
		
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží jako setr na proměnnou textValue. Tj. jedná se o
	 * nastavení proměnné textValue, která obsahuje hodnotu, kterou zadal uživatel,
	 * popřípadě výchozí nastavenou hodnotu, pokud uživatel žádnou nezadal.
	 * 
	 * @param textValue
	 *            - hodnota, která se má nastavit, resp. hodnota, kterou zadal
	 *            uživatel nebo výchozí (přednastavená) hodnota
	 */
	void setTextValue(final String textValue) {
		this.textValue = textValue;
	}
	
	
	
	
	/**
	 * Metoda, která slouží jako getr na proměnnou textValue. Jedná se o proměnnou,
	 * která obsahuje hodnotu,kterou zadal přímo uživatel do příslušného dialogu pro
	 * získání parametru pro metodu nebo konstruktor.
	 * 
	 * @return hodnotu, kterou uživatel zadal v dialogu.
	 */
	public String getTextValue() {
		return textValue;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí výčtový "typ", který "určuje", jaký "typ" hodnoty
	 * reprezentuje konkrétní instance této třídy. Tj. zda se jedná o proměnnou
	 * vytvořenou v editoru příkazů nebo proměnnou získanou z instance třídy apod.
	 * 
	 * @return "typ" proměnné, kterou reprezentuje konkrétní instance této třídy
	 *         (popis výše).
	 */
	public KindOfVariable getKindOfVariable() {
		return kindOfVariable;
	}
	
	
	
	
	
	
	/**
	 * Metoda, která vráí referenci na proměnnou, která obsahuje referenci na buď
	 * proměnnou vytvořenou uživatelem v editoru příkazů nebo insance nějkaé třídy,
	 * ze které byla získána proměnná.
	 * 
	 * @return výše popsanou referenci na proměnnou z editoru příkazů nebo instanci
	 *         třídy.
	 */
	public String getReferenceName() {
		return referenceName;
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí referenci na hodnotu, kterou uživatel vložil do nějaké
	 * proměnné v editoru příkazů.
	 * 
	 * @return hodnota proměnné vytvořené v editoru příkazů.
	 */
	Object getObjEditorVariable() {
		return objEditorVariable;
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí referenci na hodnotu nějaké proměnné (atributu) z nějaké
	 * třídy.
	 * 
	 * @return hodnota z nějaké proměnné z nějaké třídy.
	 */
	Object getObjValueFromField() {
		return objValueFromField;
	}
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí referenci na Field, což je proměnná získaná z nějaké
	 * třídy.
	 * 
	 * @return reference na proměnnou třídy.
	 */
	public Field getField() {
		return field;
	}
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí proměnnou dataType, která obsahuje datový typ položek,
	 * které lze vkládat do pole nebo listu. Dle toho, jaký typ proměnné tato třída
	 * reprezentuje.
	 * 
	 * @return datový typ položky, které lze vkládat do pole nebo listu.
	 */
	public Class<?> getDataType() {
		return dataType;
	}



    /**
     * Getr na datový typ Listu. Získání příslušné hodnoty budou možné pouze v případě, že instance této třídy reprezentuje položku typu List.
     *
     * @return referenci na objekt s informacemi o Listu.
     */
    public DataTypeOfList getDataTypeOfList() {
        return dataTypeOfList;
    }



    /**
	 * Metoda, která vrátí typ pole nebo null (pokud nebude příslušná proměnná
	 * uvedena nebo se nejedná o proměnnou vytvořenou v editoru příkazů typu
	 * jednorozměrné pole).
	 * 
	 * Dle této hodnoty se pozná, o jaké pole se jedná, například byte[].class nebo
	 * Byte[].class nebo int[].class atd.
	 * 
	 * @return výše popsanou proměnnou pro typ jednorozměrného pole.
	 */
	Class<?> getArrayType() {
		return arrayType;
	}
}
