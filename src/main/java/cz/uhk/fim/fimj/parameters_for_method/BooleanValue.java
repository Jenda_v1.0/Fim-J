package cz.uhk.fim.fimj.parameters_for_method;

/**
 * Tato třída slouží jako položka do komponenty JComboBox, ve které si užvatel může zvolit, jakou hodnotu chce předat do
 * příslušnho parametru metody nebo konstruktoru.
 * <p>
 * Jedná se o komponentu JComboBox, která se může nacházet v dialogu pro zavolání konstruktoru nebo metody spříslušným
 * parametrem.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class BooleanValue extends CmbValueAncestor {

    /**
     * Logická hodnota, kterou reprezentuje konkrétní instance této třídy.
     * <p>
     * Tato hodnota bude nebo může být předána do parametru metody nebo konstruktoru pří jejím zavolání a zvolení této
     * hodnoty.
     */
    private final boolean value;


    /**
     * Konstruktor této třídy.
     *
     * @param value
     *         - logická hodnota, kterou bude konkrétní instance této třídy reprezentovat.
     */
    BooleanValue(final boolean value) {
        super();

        this.value = value;
    }


    /**
     * Getr na proměnnou vylue, která značí jednu z logických hodnot, kterou reprezentuje tato třída, resp. její
     * konkrétní instance.
     *
     * @return true nebo false.
     */
    public boolean isValue() {
        return value;
    }


    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
