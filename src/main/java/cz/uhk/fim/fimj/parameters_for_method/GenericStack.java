package cz.uhk.fim.fimj.parameters_for_method;

import java.util.List;
import java.util.Stack;

/**
 * Tato třída slouží pouze jako implementace kolekce Stack z toho důvodu, že se mi nechce vytvářet pro každý objektový
 * typ jazyka Java (String, Integer, Double, ...) nový Stack.
 * <p>
 * Tak je zde tato třída, která dostane konktérní datový typ a v konstruktoru se rovnou naplní potřebná data.
 * <p>
 * Tato třída je využita pouze pro to, že když je v parametru nějaké metody nebo konstruktoru, který chce uživatel
 * zavolat parametr typu Stack, tak užival má možnost zadat vlastní data, tak když je zadá, tak se naplní tato třída a
 * předá do parametru příslušné metody nebo konstruktoru pro zavolání.
 *
 * @param <T>
 *         Konkrétní datový typ jazyka java (String, Double, Integer, Byte, Float, Short, Long, Boolean, Character).
 *         Jedná se o datový typ položek, které budou vkládany do Stacku (po úspěšném přetypování).
 * @author Jan Krunčík
 * @version 1.0
 */

class GenericStack<T> extends Stack<T> {

    private static final long serialVersionUID = 1L;

    /**
     * Konstruktor této třídy.
     *
     * @param data
     *         - list, který by měl obsahovat puze data stejného datového typu, jako je T (generický typ "tohoto" Stacku
     *         -> konkrétní instance). Hodnotami v tomto listu se naplní příslušná instance Stacku.
     */
    @SuppressWarnings("unchecked")
    GenericStack(final List<?> data) {
        super();

        data.forEach(v -> push((T) v));
    }
}
