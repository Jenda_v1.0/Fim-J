package cz.uhk.fim.fimj.parameters_for_method;

/**
 * Tato třída slouží pro uchování informací o načteném souboru .class coby zkompilovaná třída z diagramu tříd nebo
 * konkrétní vytvořená instance nějaké třídy z diagramu tříd - záleží jaká z těch dvou hodnot bude uvedena.
 * <p>
 * Tyto hodnoty budou dále testovány ve třídě FlexibleCountOfParameters, v metodě: addFieldForValuesToDialog(p,...) pro
 * uvedení parametru pokud by se měla vytvořit nová instance nějaké třídy z diagramu tříd jako parametr nějaké metody či
 * konstruktoru.
 * <p>
 * Proto budou v této třídě uvedeny bud konkrétní instance, které se do tohoto parametru buď předají nebo se vytvoří
 * nová instance potřebné třídy.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ClassOrInstanceForParameter extends CmbValueAncestor {

    /**
     * Proměnná pro uchování načteného souboru .class pro potenciální vytvoření nové instance dané třídy (pokud tak
     * uživatel zvolí)
     */
    private final Class<?> clazz;


    /**
     * Proměnná pro uchování konkrétní již vytvořené instance třídy z diagramu tříd
     */
    private final Object objInstance;


    /*
     * Proměnná pro uchování názvu reference na instanci třídy - pokud se zadá
     * instance třídy, tak se v následující proměnné uchová i její název reference:
     */
    private final String referenceToInstance;


    /**
     * Konstruktor této třídy, který slouží pro "uložení" a následné testování hodnot pro načtenou zkompilovanou třídu
     * .class.
     * <p>
     * Tento konstruktor naplní výše uvedenou proměnnou pro vytvoření nové instance do parametru - pokud tak uživatel
     * zvolí
     *
     * @param clazz
     *         - zkompilovaná a načtená třída z diagramu instnací - konkrétně načteny soubor .class
     */
    public ClassOrInstanceForParameter(final Class<?> clazz) {
        super();

        this.clazz = clazz;

        objInstance = null;
        referenceToInstance = null;
    }


    /**
     * Konstruktor této třídy, který naplněí proměnnou typu Object a referenceToInstance nějakými hodnotami, v proměnné
     * refere.. bude název na danou referenci a v druhé proměnné objInstance bude konkrétní již vytvořená instance
     * nějaké třídy z diagramu tříd, kterou uživatel již dříve vytvořil
     *
     * @param referenceToInstance
     *         - název reference na již vytvořenou instanci třídy z diagramu tříd
     * @param objInstance
     *         - konkrétní instance třídy z diagramu tříd
     */
    public ClassOrInstanceForParameter(final String referenceToInstance, final Object objInstance) {
        super();

        this.objInstance = objInstance;

        this.referenceToInstance = referenceToInstance;

        clazz = null;
    }


    /**
     * Klasický getr na proměnnou clazz coby proměnná, která reprezentuje načtenou zkompilovanou třídu z diagramu tříd.
     *
     * @return načtená a zkompilovaná třída - soubor .class nějaké třídy z diagramu tříd
     */
    public final Class<?> getClazz() {
        return clazz;
    }


    /**
     * Klasický getr na proměnnou objInstance, která reprezentuje nějakou instancu třídy z diagramu tříd, načtenou z
     * hashMapy, kte jsou uchovány veškeré instance vytvořené uživatelem
     *
     * @return objInstance coby nějaká vytvořená instance nějakéí třídy z diagramu tříd
     */
    public final Object getObjInstance() {
        return objInstance;
    }


    @Override
    public String toString() {
        if (clazz != null)
            return "new " + clazz.getTypeName() + "()";

        return referenceToInstance + " (" + objInstance.getClass().getTypeName() + ")";
    }
}