package cz.uhk.fim.fimj.parameters_for_method;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Tato třída slouží pouze pro "předání" hodnot.
 * <p>
 * Jedná se o to, že potřebuji nějak předat získané hodnoty (proměnné) z nějaké instnace třídy a k této instanci
 * příslušnou referenci. I když by bylo možné vytvořit například jednorozměrné pole o dvou hodnotách apod. Ale přece jen
 * píšeme objektově.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class FieldsInfo {

    /**
     * List, který obsahuje veškeré proměnné (atributy), které byly dle příslušných pravidel získány z nějaké instance
     * třídy.
     */
    private final List<Field> fieldsList;

    /**
     * Reference na nějakou instanci třídy, kterou uživatel vytvořil a je aktuálně reprezentována v diagramu instancí.
     */
    private final String referenceName;


    private final Object objInstance;


    /**
     * Konstruktor této třídy.
     *
     * @param fieldsList
     *         - List obsahují získané proměnné (atributy) z nějaké třídy, popř. instance.
     * @param referenceName
     *         - reference na nějakou instanci třídy z diagramu tříd nebo null, pokud byl list s hodnotami fieldsList
     *         získán napřílkad pouze ze třídy (ne z instance). Pak se jedná puze o statické proměnné.
     * @param objInstance
     *         - instance nějaké třídy nebo null, pokud byly získány statické hodnoty z třídy (ne z instance třídy).
     */
    FieldsInfo(final List<Field> fieldsList, final String referenceName, final Object objInstance) {
        super();

        this.fieldsList = fieldsList;
        this.referenceName = referenceName;
        this.objInstance = objInstance;
    }


    /**
     * metoda, která zjistí, zda se proměnné (atribut) field již nachází v listu fieldsList nebo ne.
     *
     * @param field
     *         - proměnná nějaké třídy, o kterém se má zjistit, zda se nachází v listu fieldsList.
     *
     * @return true, pokud se field nachází v listu fieldsList, jinak false.
     */
    final boolean containsFieldsListField(final Field field) {
        return fieldsList.contains(field);
    }


    /**
     * Metoda, která slouží jako getr na list s proměnnými získanými z nějaké třídy.
     *
     * @return list získaných proměnných z nějaké třídy.
     */
    public List<Field> getFieldsList() {
        return fieldsList;
    }


    /**
     * Metoda, která slouží jako getr na proměnnou, která obsahuje referencena nějakou instanci třídy.
     *
     * @return referenci na instanci třídy (může být null).
     */
    public String getReferenceName() {
        return referenceName;
    }


    /**
     * Metoda, která slouží jako getr na objekt, který obsahuje instanci nějaké třídy, ze které se získali hodnoty v
     * list fieldsList.
     *
     * @return instanci nějaké třídy (může být null).
     */
    public Object getObjInstance() {
        return objInstance;
    }
}
