package cz.uhk.fim.fimj.parameters_for_method;

/**
 * Tato třída slouží pouze jako "předek", aby bylo možné do komponenty typu JComboBox v dialogu pro zavolání metody nebo
 * konstruktoru vkládat více "růžných" objektů, ale ve smyslu typu hodnoty pro parametr, například jeden potomek této
 * třídy slouží pro předání instance nějaké třídy nebo vytvoření nové instance nějaké třídy z diagramu tříd a následné
 * předání do parametru zmáněné metody nebo konstruktoru apod.
 * <p>
 * Jelikož každý z potomků obsahuje v podstatě jiné hodnoty a metody, tak tato třída je potřeba pouze pro to, aby šlo do
 * výše zmíněné komponenty JComboBox vložit instance "jiných" tříd (potomktů této třídy) s jinými hodnotami. Jinak tato
 * třída není pořeba, proto nemusí mít žádné hodnoty apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

abstract class CmbValueAncestor {

}
