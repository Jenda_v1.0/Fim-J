package cz.uhk.fim.fimj.check_configuration_files;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;

/**
 * Tato třída slouží pouze jako abstraktní třída pro splečné metody, abych je nemusel psát pořád dokola pro každou
 * třídu.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class CheckConfigPropAbstract {

	/**
	 * List, který slouží pro "odkládáná" hodnot, které obsahují informace o
	 * chybách, které byly nalezeny.
	 */
	static final List<ErrorInfo> ERRORS_LIST = new ArrayList<>();
	
	
	/**
	 * Regulární výraz pro rozpoznání textu 'true'.
	 */
	private static final String REG_EX_TRUE_PATTERN = "^\\s*[tT][rR][uU][eE]\\s*$";

	/**
	 * Regulární výraz pro rozpoznání textu 'false'.
	 */
	private static final String REG_EX_FALSE_PATTERN = "^\\s*[fF][aA][lL][sS][eE]\\s*$";

	/**
	 * Povolené hodnoty pro přiblížení diagramu.
	 */
	static final String DIAGRAM_SCALE_ALLOWED_VALUES = "<0.5 ; 20.0>";

	/**
	 * Povolené hodnoty pro rgb hodnotu nějaké barvy v textu.
	 */
	static final String COLOR_ALLOWED_VALUES = "<-1 ; -16777216>";

	/**
	 * Logické hodnoty.
	 */
	static final String BOOLEAN_ALLOWED_VALUES = "true, false";

	/**
	 * Povolené hodnoty pro styl písma.
	 */
	static final String FONT_STYLE_VALUES = "'0', '1', '2'";

	/**
	 * Povolené hodnoty pro velikost písma.
	 */
	static final String FONT_SIZE_VALUES = "<8 ; 150>";

	/**
	 * Povolené hodnoty pro vertikální zarovnání textu v buňce.
	 */
	static final String VERTICAL_ALIGNMENT_VALUES = "'0', '1', '3'";

	/**
	 * Povolené hodnoty pro horizontální zarování textu v buňce.
	 */
	static final String HORIZONTAL_ALIGNMENT_VALUES = "'0', '2', '4'";

	/**
	 * Povolené hodnoty pro styl začátku a konce hrany - nějaký znak na začátku nebo
	 * na konci hrany.
	 */
	static final String LINE_BEGIN_END_VALUES = "'0', '1', '2', '4', '5', '7', '8', '9'";

	/**
	 * Povolené hodnoty pro šířku hrany.
	 */
	static final String LINE_WIDTH_VALUES = "<0.1 ; 20.0>";
	
	/**
	 * Interval velikosti písma pro editory.
	 */
	private static final String EDITORS_FONT_SIZE_VALUES = "<8 ; 100>";
	
	
	/**
	 * Mutex pro blokování přístupu k příslušným souborů pro ostatní části aplikace.
	 */
	private static final Semaphore MUTEX = new Semaphore(1);
	
	
	
	
	

	/**
	 * Metoda, která slouží pro přidání textové hodnoty key do listu s chybami spolu
	 * s textem, že se příslušný text nenachází v příslušném objektu Properties.
	 * 
	 * @param key
	 *            - klíč, který se nenachází v objektu Properties.
	 */
	static void addErrorToList(final String key) {
		ERRORS_LIST.add(new ErrorInfo(key));
	}
	
	
	
	

	/**
	 * Metoda, která přidá do mapy s chybovými texty informaci o tom, že klíč key
	 * obsahuje hodnotu actualValue, ale může obsahovat pouze allowedValues.
	 * 
	 * @param key
	 *            - klíč nějaké hodnoty v objektu Properties.
	 * 
	 * @param actualValue
	 *            - aktuální chybná hodnota v objektu Properties.
	 * 
	 * @param allowedValues
	 *            - povolené hodnoty, které je možné do příslušného klíče zadat.
	 */
	static void addErrorToList(final String key, final String actualValue, final String allowedValues) {
		ERRORS_LIST.add(new ErrorInfo(key, actualValue, allowedValues));
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro získání vlastnosti, resp. hodnoty vlastnosti pod
	 * klíčem key.
	 * 
	 * Metoda vrátí hodnotu u klíče key nebo null, pokud se příslušný klíč v mapě (v
	 * objektu properties) nebude vyskytovat.
	 * 
	 * @param key
	 *            - klíč, který se má v mapě vyhledat.
	 * 
	 * @param properties
	 *            - objekt Properties, ve kterém se má vyhledat klíč hodnoty key.
	 * 
	 * @return hodnotu příslušného klíče key v objektu properties, jinak null, pokud
	 *         se v mapě klíč key nenachází.
	 */
	static String getValueFromProperties(final String key, final Properties properties) {
		return properties.getProperty(key, null);
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro otestování, zda je hodnota colorInText nějaká barva,
	 * tj. příslušný text se přetypuje na Integer, pomocí kterého se zkusí vytvořit
	 * instance třídy Color, pokud dojde k nějaké výjimce, pak se vrátí false, že
	 * colorInText je nějaká chybná hodnota, kterou nelze brát jako rgb hodnoty
	 * barvy, jinak pokud se toto přetypování hodnoty colorInText povede přetypovat
	 * na barvu, pak se vrátí true.
	 * 
	 * @param colorInText
	 *            - hodnota ve stringu, o které se má zjistit, zda se jedná o rgb
	 *            hodnotu nějaké barvy.
	 * 
	 * @return true, pokud je colorInText nějaká barva, jinak false.
	 */
	static boolean testColor(final String colorInText) {
		try {
			new Color(Integer.parseInt(colorInText));
			return true;
		} catch (NumberFormatException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informac text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informaci o chybě:
				logger.log(Level.INFO, "Zachycena výjimka při pokusu o přetypování hodnoty: '" + colorInText
						+ "' na datový typ Integer a pak Color.");

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();
		}
		return false;
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro otestování, zda je hodnota doubleInText hodnota typu
	 * double, pokud ano, vrátí se true, jinak false.
	 * 
	 * @param doubleInText
	 *            - nějaká textová hodnota, o které se má zjistit, zda se jedná o
	 *            double nebo ne.
	 * 
	 * @return - true, pokud je doubleInText hodnota typu double, jinak false.
	 */
	static boolean testDouble(final String doubleInText) {
		try {
			Double.parseDouble(doubleInText);
			return true;
		} catch (NumberFormatException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informaci o chybě:
				logger.log(Level.INFO, "Zachycena výjimka při pokusu o přetypování hodnoty: '" + doubleInText
						+ "' na datový typ Double.");

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();
		}

		return false;
	}
	
	
	
	
	/**
	 * Metoda, která zjistí, zda je hodnota integerInText hodnota typu Integer,
	 * pokud ano, vrátí se true, jinak false.
	 * 
	 * @param integerInText
	 *            - nějaká textové hodnota, o které se má zjistit, zda se jedná o
	 *            hodnotu typu Integer.
	 * 
	 * @return - true, pokud je hodnota integerInText hodnota typu Integer, jinak
	 *         false.
	 */
	static boolean testInteger(final String integerInText) {
		try {
			Integer.parseInt(integerInText);
			return true;
		} catch (NumberFormatException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informac o chybě:
				logger.log(Level.INFO, "Zachycena výjimka při pokusu o přetypování hodnoty: '" + integerInText
						+ "' na datový typ Integer.");

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();
		}

		return false;
	}
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro otestování, zda se je booleanInText hodnota, resp.
	 * text "true" nebo "false", zde nebudu přetypovávat hodnotu, protože pokud by
	 * se to nepovedlo, vrátilo by se false.
	 * 
	 * @param booleanInText
	 *            - hodnota, o které se má zjistit, zda se jedná o text true nebo
	 *            false.
	 * 
	 * @return true, pokud je booleanInText true, nebo false, jinak, pokud
	 *         booleanInText není v syntaxi: "true" nebo "false", pak se vrátí
	 *         false.
	 */
	static boolean testBoolean(final String booleanInText) {
		return booleanInText.matches(REG_EX_TRUE_PATTERN) || booleanInText.matches(REG_EX_FALSE_PATTERN);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro otestování logických hodnot u klíčů v objektu
	 * properties (prop).
	 * 
	 * @param prop
	 *            - objekt Properties, ze kterého se má vzít hodnota u klíče key a
	 *            otestovat, zda se jedná o logickou hodnotu.
	 * 
	 * @param key
	 *            - klíč, u kterého se má vzít hodnota z objektu prop a zjistit, zda
	 *            se jedná o logickou hodnotu.
	 */
	static void testBooleanValue(final Properties prop, final String key) {
		/*
		 * Získám si hodnotu z objektu properties:
		 */
		final String value = getValueFromProperties(key, prop);

		if (value == null)
			addErrorToList(key);

		else if (!testBoolean(value))
			addErrorToList(key, value, BOOLEAN_ALLOWED_VALUES);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro otestování některých hodnot, které mají společné
	 * některý editoru (editory zdrojového kódu, výstupní editory apod.).
	 * 
	 * Jedná se o otestování velikosti písma, stylu písma, barvy písma a barvy
	 * pozadí příslušného editoru.
	 * 
	 * @param prop
	 *            - objekt properties, ve kterém se nachází příslušné hodnoty, které
	 *            se mají otestovat.
	 * 
	 * @param keyFontStyle
	 *            - klíč pro index hodnoty pro styl písma.
	 * 
	 * @param keyFontSize
	 *            - klíč pro index hodnoty pro velikost písma.
	 * 
	 * @param keyBackgroundColor
	 *            - klíš pro index hodnoty pro barvu pozadí.
	 * 
	 * @param keyForegroundColor
	 *            - klíč pro index hodnoty pro barvu písma.
	 */
	static void checkSimilarValuesInEditors(final Properties prop, final String keyFontStyle,
			final String keyFontSize, final String keyBackgroundColor, final String keyForegroundColor) {

		final String fontStyle = getValueFromProperties(keyFontStyle, prop);

		if (fontStyle == null)
			addErrorToList(keyFontStyle);

		else if (!testInteger(fontStyle))
			addErrorToList(keyFontStyle, fontStyle, FONT_STYLE_VALUES);

		else {
			// Test pro rozsah:
			final int style = Integer.parseInt(fontStyle);

			// Hodnoty mohou být pouze 0, 1 nebo 2
			if (style < 0 || style > 2)
				addErrorToList(keyFontStyle, fontStyle, FONT_STYLE_VALUES);
		}
		
		
		
		
		
		
		
		
		final String fontSize = getValueFromProperties(keyFontSize, prop);

		if (fontSize == null)
			addErrorToList(keyFontSize);

		else if (!testInteger(fontSize))
			addErrorToList(keyFontSize, fontSize, EDITORS_FONT_SIZE_VALUES);

		else {
			// Test pro rozsah:
			final int size = Integer.parseInt(fontSize);

			// Hodnoty mohou být pouze v intervalu: <8 ; 100>
			if (size < 8 || size > 100)
				addErrorToList(keyFontSize, fontSize, EDITORS_FONT_SIZE_VALUES);
		}
		
		
		
		
		
		
		
		
		
		final String backgroundColor = getValueFromProperties(keyBackgroundColor, prop);

		if (backgroundColor == null)
			addErrorToList(keyBackgroundColor);

		else if (!testColor(backgroundColor))
			addErrorToList(keyBackgroundColor, backgroundColor, COLOR_ALLOWED_VALUES);
		
		
		
		
		
		
		
		
		
		final String foregroundColor = getValueFromProperties(keyForegroundColor, prop);

		if (foregroundColor == null)
			addErrorToList(keyForegroundColor);

		else if (!testColor(foregroundColor))
			addErrorToList(keyForegroundColor, foregroundColor, COLOR_ALLOWED_VALUES);
	}


    /**
     * Otestování správných / povolených hodnot v objektu properties.
     * <p>
     * Jedná se o otestování toho, zda se v objektu properties nachází povolené hodnoty pro vlastnosti formátování
     * zdrojového kódu.
     *
     * @param properties
     *         - objekt, o kterém se má zjistit, zda obsahuje hodnoty pro nastavení formátování zdrojového kódu a zda ty
     *         hodnoty jsou v povolené syntaxi.
     */
    static void checkFormattingProperties(final Properties properties) {
        testBooleanValue(properties, "DeleteEmptyLines");
        testBooleanValue(properties, "TabSpaceConversion");
        testBooleanValue(properties, "BreakElseIf");
        testBooleanValue(properties, "SingleStatement");
        testBooleanValue(properties, "BreakOneLineBlock");
        testBooleanValue(properties, "BreakClosingHeaderBrackets");
        testBooleanValue(properties, "BreakBlocks");
        testBooleanValue(properties, "OperatorPadding");
    }
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která spustí mutex pro blokování přístupu k souboru ostatnímí částmi
	 * aplikace.
	 */
	protected static void acquireMutex() {
		try {
			MUTEX.acquire();
		} catch (InterruptedException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informaci o chybě:
				logger.log(Level.INFO,
						"Zachycena výjimka při pokusu spuštění mutexu pro blokování přístupu k souboru. Tato výjimka " +
								"může "
								+ "nastat například v případě, že dojde k přerušení běhu vlákna.");

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Uvolnění mutextu - zrušení blokování ostatních "částí" aplikace pro přístup k
	 * příslušnému souboru.
	 */
	protected static void releaseMutex() {
		MUTEX.release();
	}
}
