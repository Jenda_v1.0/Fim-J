package cz.uhk.fim.fimj.check_configuration_files;

/**
 * Tento výčet obsahuje hodnoty, které značí, o jaký typ chybové hlášky se jedná. Je to potřeba pouze pro to, abych si
 * nějak "uspořádal" chybová hlášení, protože k tomu mám jednu třídu, abyc abych věděl jaké hodnoty se vždy mají
 * vypsat.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public enum KindOfErrorInfo {

	/**
	 * Tato hodnota značí, že v objektu Properties nebyl nalezen nějaký klíč - klíč
	 * v mapě.
	 */
	MISSING_KEY,

	/**
	 * Tato hodnota značí, že hodnota u příslušného klíče obsahuje "chybná data",
	 * resp. chybou hodnotu, například ta hodnota u příslušného klíče může obsahovat
	 * pouze čísla v nějakém intervalu, ale byly zadány čísla apod.
	 */
	ERROR_IN_KEY_VALUE,

	/**
	 * Tato hodnota značí, že v souboru s vlastnostmi pro diagram tříd byly nalezeny
	 * duplicitní nastavení pro buňky, které reprezetnují v daigramu tříd třídu a
	 * komentář.
	 */
	CELLS_DUPLICATE_ERROR,

	/**
	 * Tato hodnota značí, že v souboru s vlastnostmi pro diagram tříd byly nalezeny
	 * duplicitní hodnoty pro nějaké hrany, které v diagramu tříd reprezentují
	 * nějaké vztahy mezi třídami nebo spojení třídy s komentářem.
	 */
	CD_EDGES_DUPLICATE_ERROR,

	/**
	 * Tato hodnota značí, že byly nalezeny v souboru s vlastnostmi pro diagram
	 * instancí duplicitní hodnoty pro hrany.
	 */
	ID_EDGES_DUPLICATE_ERROR
}
