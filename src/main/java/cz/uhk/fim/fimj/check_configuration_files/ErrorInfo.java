package cz.uhk.fim.fimj.check_configuration_files;

import java.util.Properties;

import cz.uhk.fim.fimj.file.Constants;

/**
 * Tato třída slouží pouze "objekt", který bude obsahovat informace o příslušné chybě a tyto jednotlivé objekty
 * (instance této třídy) budou zobrazeny v příslušném dialogu uživateli.
 * <p>
 * Tento objekt vlastně ani není nějak extra potřeba, je možné vždy "složit" příslušný text, ale kdyby se někdy v
 * budoucnu nějak tyto texty rozšiřovaly nebo upravovali nějaké možnosti pro další chyby apod. Stači předělat tento
 * jeden objekt a přidat metodu pro vkládání a zbytek už bude zařízen - co se týká zobrazení v dialogu apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ErrorInfo {

	/**
	 * Tato výčtová hodnota slouží pouze pro určení, jaká hláška / syntaxe textu se
	 * má vypsat v metodě toString. Dle této hodnoty se pozná, o jakou chybu jde,
	 * jestli o chybějící klíč, nebo duplicitní hodnoty pro diagramy tříd nebo
	 * instancí, nebo chybně zadaná hodnota apod.
	 */
	private final KindOfErrorInfo kindOfError;

	/**
	 * Jedná se o hodnotu, která značí nějaký klíč v mapě - v objektu Properties, ke
	 * kterému se vztahu nějaká chyba nebo informace o chybě.
	 * 
	 * Tato hodnota bude zadána vždy.
	 */
	private final String key;
	
	/**
	 * Jedná se o hodnotu, která obsahuje aktuální hodnotu v objektu Properties u
	 * klíče key.
	 * 
	 * Tato hodnota bude zadána jen v případě, že se jedná o chybnou hodnotu u klíče
	 * key, například místo čísla je zadán písmeno apod.
	 */
	private final String value;
	
	
	/**
	 * Tato hodnota značí povolené hodnoty, které lze zadat ke klíči key.
	 * 
	 * Tato hodnota bude zobrazeny v případě, kdy bude naplněna i proměnná value.
	 * Tato proměnná značí tedy hodnoty, které mohou být zadány u klíče key, ale
	 * byla zadána chybná hodnota, která se aktuálně nachází v proměnné value.
	 */
	private final String allowedValue;
	
	
	
	// Proměnné pro texty v příslušném jazyce:
	private static String txtAllowedValues, txtMissingKey, txtCellsDuplicateError, txtCdEdgesError, txtIdEdgesError,
			txtInvalidValue;
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * Tento konstruktor se zavolá pouze jednou a slouží pro naplnění textových
	 * proměnných, které obsahuje text v příslušném jazyce. Jedná se o naplnění
	 * textových proměnných, aby se zobrazovali v metodě toString.
	 * 
	 * @param languageProperties
	 *            - objekt Properties, který obsahuje texty pro tuto aplikaci v
	 *            uživatelem zvoleném jazyce.
	 */
	public ErrorInfo(final Properties languageProperties) {
		super();

		if (languageProperties != null) {
			txtAllowedValues = languageProperties.getProperty("EI_Txt_AllowedValues", Constants.EI_TXT_ALLOWED_VALUES);
			txtMissingKey = languageProperties.getProperty("EI_Txt_MissingKey", Constants.EI_TXT_MISSING_KEY);
			txtCellsDuplicateError = languageProperties.getProperty("EI_Txt_CellsDuplicateError",
					Constants.EI_TXT_CELLS_DUPLICATE_ERROR);
			txtCdEdgesError = languageProperties.getProperty("EI_Txt_CdEdgesError", Constants.EI_TXT_CD_EDGES_ERROR);
			txtIdEdgesError = languageProperties.getProperty("EI_Txt_IdEdgesError", Constants.EI_TXT_ID_EDGES_ERROR);
			txtInvalidValue = languageProperties.getProperty("EI_Txt_InvalidValues", Constants.EI_TXT_INVALID_VALUE);
		}

		else {
			txtAllowedValues = Constants.EI_TXT_ALLOWED_VALUES;
			txtMissingKey = Constants.EI_TXT_MISSING_KEY;
			txtCellsDuplicateError = Constants.EI_TXT_CELLS_DUPLICATE_ERROR;
			txtCdEdgesError = Constants.EI_TXT_CD_EDGES_ERROR;
			txtIdEdgesError = Constants.EI_TXT_ID_EDGES_ERROR;
			txtInvalidValue = Constants.EI_TXT_INVALID_VALUE;
		}
		
		
		kindOfError = null;
		key = null;
		value = null;
		allowedValue = null;
	}
	
	
	
	
	/**
	 * Konstruktor této třídy, který slouží pro naplnění souboru, vekterém byla
	 * nalezena duplicita, jedná se pouze o soubory pro vlastnosti pro diagram tříd
	 * a diagram instancí. Takže v jednom z těchto souborů byla nalezena duplicita.
	 * 
	 * @param kindOfError
	 *            - výčtová hodnota, která blíže značí, o jakou duplicitu se jedná,
	 *            napřílkad o stené vlastnosti pro třídu a komentář nebo nějaké z
	 *            hran apod.
	 */
	public ErrorInfo(final KindOfErrorInfo kindOfError) {
		super();

		this.kindOfError = kindOfError;

		key = null;
		value = null;
		allowedValue = null;
	}
	
	
	
	
	/**
	 * Konstruktor této třidy slouží pro naplnění informacemi o tom, že v příslušném
	 * objektu properites (soubor fileName), tak, že v něm nebyl nalezen klíč key.
	 * 
	 * @param key
	 *            - klíč, který nebyl nalezen v soubor fileName.
	 */
	public ErrorInfo(final String key) {
		super();

		kindOfError = KindOfErrorInfo.MISSING_KEY;
		this.key = key;
		
		value = null;
		allowedValue = null;
	}
	
	
	
	
	/**
	 * Konstruktor této třídy, který slouží pro naplnění proměnných informacemi o
	 * tom, že u klíče key byla nalezena chybná hodnota value, ale může tento klíč
	 * key může obsahovat pouze hodnoty uvedené v allowedValue.
	 * 
	 * @param key
	 *            - klíč, u kterého byla nalezena chybná hodnota.
	 * 
	 * @param value
	 *            - hodnota, která obsahuje nějakou chybu, resp. hodnota, která se
	 *            aktuálně nachází u klíče key, ale je to chybná hodnota.
	 * 
	 * @param allowedValue
	 *            - text, která obsahuje povolené hodnoty, kterou se mohou nacházet
	 *            u klíče key.
	 */
	public ErrorInfo(final String key, final String value, final String allowedValue) {
		super();

		kindOfError = KindOfErrorInfo.ERROR_IN_KEY_VALUE;
		this.key = key;
		this.value = value;
		this.allowedValue = allowedValue;
	}
	
	
	
	
	
	
	
	@Override
	public String toString() {
		/*
		 * Info:
		 * 
		 * pro duplicity:
		 * Soubor: FileName zde info o chybě / duplicitě
		 * 
		 * pro chybějící klíč:
		 * Chybějící klíč: 'keyName'
		 * 
		 * pro chybnou hodnotu:
		 * Nevylidní hodnota: key = 'value' (povolene hodnoty: values)
		 */
		
		
		/*
		 * proměnná pro vložení výsledného textu.
		 */
		String text = "";

		if (kindOfError == KindOfErrorInfo.CELLS_DUPLICATE_ERROR)
			return text + txtCellsDuplicateError;

		else if (kindOfError == KindOfErrorInfo.CD_EDGES_DUPLICATE_ERROR)
			return text + txtCdEdgesError;

		else if (kindOfError == KindOfErrorInfo.ID_EDGES_DUPLICATE_ERROR)
			return text + txtIdEdgesError;

		else if (kindOfError == KindOfErrorInfo.MISSING_KEY)
			text += txtMissingKey + ": '" + key + "'";

		else if (kindOfError == KindOfErrorInfo.ERROR_IN_KEY_VALUE)
			text += txtInvalidValue + ": " + key + " = '" + value + "'\t(" + txtAllowedValues + ": " + allowedValue
					+ ")";

		return text;
	}
}
