package cz.uhk.fim.fimj.check_configuration_files;

import java.io.File;
import java.util.List;
import java.util.Properties;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.ConfigurationFiles;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.ReadFile;

/**
 * Tato třída slouží pro kontrolu soubor CommandEditor.properties, zda obsahuje veškeré požadované klíče a k nim validní
 * hodnoty.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

class CheckCommandEditorProp extends CheckConfigPropAbstract {

    /**
     * Metoda, která slouží pro kontrolu, zda soubor CommandEditor.properties obsahuje příslušné klíče a k němu povolené
     * hodnoty.
     *
     * @return list, který může obsahovat chybová hlášení, tedy položky, které značí nalezené chyby, nebo bude prázdný,
     * pokud se žádná chyba nenajde.
     */
    static List<ErrorInfo> checkCommandEditorProp() {
        ERRORS_LIST.clear();

        final Properties commandEditorProp = App.READ_FILE.getCommandEditorPropertiesFromWorkspaceOnly();

        if (commandEditorProp == null) {
            /*
             * K této části podmínky bych se nikdy neměl dostat, protože tato metoda se volá
             * z ControlClass, ale až po tom, co se již otestovatlo, že adresář
             * configuration ve workspace existuje, případně byl vytvořen, ale pouze pro
             * případ, že by i tak došlo z mě neznámého důvodu k tomu, že by se ani tak ty
             * soubory nenacházely ve workspace, tak jej zde zkusím vytvořit.
             */

            /*
             * Načtu si cestu k adresáři workspace.
             */
            final String pathToWorkspace = ReadFile.getPathToWorkspace();

            acquireMutex();

            /*
             * Pokud byla nalezena cesta k adresář workspace, pak pro jitotu znovu otestuji,
             * zda existuje adresář configuration, ale už zde netestuji verzi, kdy
             * neexistuje aby se vytvořil, protože by to nikdy nastat nemělo, a pokud
             * tenadresář existuje, tak vytvořím pčříslušný soubor.
             */
            if (pathToWorkspace != null
                    && ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
                ConfigurationFiles.writeCommandEditorProperties(
                        pathToWorkspace + File.separator + Constants.PATH_TO_COMMAND_EDITOR_PROPERTIES);

            releaseMutex();
        }

        else
            checkValues(commandEditorProp);

        return ERRORS_LIST;
    }


    /**
     * Metoda, která slouží pro otestování všech klíčů a jejich hodnot v souboru CommandEditor.properties (parametr
     * prop).
     *
     * @param prop
     *         - objekt Properties (načtený soubor CommandEditor.properties), ve kterém se mají zkontrolovat, zda ten
     *         objekt obsahuje veškeré klíče s hodnotami, které by měl a ty hodnoty obsahuje "validní data".
     */
    private static void checkValues(final Properties prop) {
        checkSimilarValuesInEditors(prop, "FontStyle", "FontSize", "BackgroundColor", "ForegroundColor");

        testBooleanValue(prop, "HighlightingJavaCode");

        // Test, zda jsou hodnoty pro okno pro doplňování příkazů do editoru příkazů validní:
        testBooleanValue(prop, "ShowAutoCompleteWindow");
        testBooleanValue(prop, "ShowReferenceVariablesInCompleteWindow");
        testBooleanValue(prop, "AddSyntaxForFillingTheVariable");
        testBooleanValue(prop, "AddFinalWordForFillingTheVariable");
        testBooleanValue(prop, "ShowExamplesOfSyntax");
        testBooleanValue(prop, "ShowDefaultMethods");
        // Historie příkazů v rámci otevřeného projektu:
        testBooleanValue(prop, "ShowProjectHistoryWindow");
        testBooleanValue(prop, "IndexCommandsInProjectHistoryWindow");
        // Historie příkazů v od spuštění aplikace:
        testBooleanValue(prop, "ShowCompleteHistoryWindow");
        testBooleanValue(prop, "IndexCommandsInCompleteHistoryWindow");
    }
}