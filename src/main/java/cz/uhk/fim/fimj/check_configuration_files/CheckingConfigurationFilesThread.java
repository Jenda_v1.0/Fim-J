package cz.uhk.fim.fimj.check_configuration_files;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;

/**
 * Tato třída slouží jako vlákno, které slouží pro monitorování změn souborů v adresáři configuration.
 * <p>
 * Jde o to, že pokud uživatel změní nějakou hodnotu v jednom z konfiguračních souborů, pak může například úmyslně zadat
 * nějakou hodnotu, která "tam nepatří", tj. jedná se o nepovolenou hodnotu, například místo čísel písmena, nebo hodnoty
 * mimo intervaly apod. Pak je třeba tyto hodnoty "opravitů, aby se nenačetli nějaké chybné hodnoty a v aplikaci
 * nedocházelo tak k nějakým chybám.
 * <p>
 * <p>
 * Zdroj: https://docs.oracle.com/javase/tutorial/essential/io/notification.html
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class CheckingConfigurationFilesThread extends Thread {

	private WatchService watcher;
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param pathToWorkspaceConfiguration
	 *            - cesta k adresáři, kde se mají sledovat změny, jedná se o adresář
	 *            configuration.
	 * 
	 * @param languageProperties
	 *            - objekt Properties, který obsahuje texty pro tuto aplikaci v
	 *            uživatelem zvoleném jazyce.
	 */
	public CheckingConfigurationFilesThread(final File pathToWorkspaceConfiguration,
			final Properties languageProperties) {

		super();

		/*
		 * Zde potřebuji zavolat konstruktor pro předání objektu s texty, aby se
		 * případně mohli vypsat hlášky o chybách, ale toto by nemělo být potřeba,
		 * protože nechci, aby se ty hlášky vypisovaly, takže je to jen jako taková
		 * "nouzová" situace, aby se v případě opravdu nějaké nečekané chyby vypsali
		 * hlášky ve zvoleném jazyce, kdybych zde ten jazyk nepředal, tak by se vzali
		 * výchozí texty v češtině, takže by se tak moc nestalo, ale i když to jsou "jen
		 * texty", tak chci aby to bylo "ucelené", proto je zde předám.
		 */
		new ControlClass(languageProperties);

		/*
		 * Cesta k adresáři, který se má monitorovat.
		 */
		final Path workspaceDir = Paths.get(pathToWorkspaceConfiguration.getPath());

		try {
			watcher = workspaceDir.getFileSystem().newWatchService();

			workspaceDir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE,
					StandardWatchEventKinds.ENTRY_MODIFY);

		} catch (IOException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informaci o chybě:
				logger.log(Level.INFO,
						"Zachycena výjimka při pousu o spuštění monitorovacího vlákna pro kongigurační soubory v " +
								"adresář "
								+ "workspace/configuration. Jedná se o nějaku I/O výjimku.");

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();
		}
	}
	
	
	
	
	
	
	
	
	@Override
	public void run() {
		try {
			if (watcher == null)
				return;

			final WatchKey watckKey = watcher.take();

			while (true) {
				/*
				 * Zde je potřeba uspat na malou chvilku toto vlákno, protože, když to neudělám
				 * a dojde k vytvoření nějakého souboru, například vytvoření
				 * ClassDiagram.properties, tak se zaznaměná změna, že je vytvořen, ale ten
				 * soubor bude nejprve třeba prázdný, pak se zaznamená modifikace / editace
				 * souboru, a zde už se třeba pozná, že je naplněn, tak to bude OK, ale tato
				 * modifikace se pozná dvakrát. Což už je zbytečné,
				 * 
				 * Takže proto toto zpoždění, když to vlákno uspím na "dostatečně" dlouhou dobu,
				 * aby se ten soubor stihl vytvořit a zapsat změny, pak dojde ke každému hlášení
				 * ohledně modifikace a vytvoření pouze jednou a nebudou zde tak duplicitní
				 * volání.
				 * 
				 * Dále by ještě bylo možné hlídat časy, kdy k těm změnám došlo (pokud se budou
				 * jednat o stejné soubory, které obsahují nějakou změnu), ale to je trochu více
				 * kódu.
				 * 
				 * Zdroj:
				 * https://stackoverflow.com/questions/16777869/java-7-watchservice-ignoring-multiple-occurrences-of-the-same-event/29442734
				 */
				sleep(600);

				final List<WatchEvent<?>> events = watckKey.pollEvents();

				for (final WatchEvent<?> event : events) {
					WatchEvent.Kind<?> kind = event.kind();
					if (kind == StandardWatchEventKinds.OVERFLOW)
						continue;

					else if (event.kind() == StandardWatchEventKinds.ENTRY_CREATE)
						/*
						 * Zde příslušný soubor otestovat, zda obsahuje povolene hodnoty!
						 */
						ControlClass.createdModifiedFile(event.context().toString(), true);

					else if (event.kind() == StandardWatchEventKinds.ENTRY_DELETE)
						/*
						 * Zde ten soubor vytvorit!
						 */
						ControlClass.deletedFile(event.context().toString());

					else if (event.kind() == StandardWatchEventKinds.ENTRY_MODIFY)
						/*
						 * Zde otestovat ten soubor, zda obsahuje povolene hodnoty!
						 */
						ControlClass.createdModifiedFile(event.context().toString(), true);
				}
			}
		} catch (InterruptedException e) {
			/*
			 * Tato výjimka nastane v podstatě pokaždé, když uživatel změní adresář
			 * workspace protože uspávám výše vlákno (Thread.sleep), aby se zbytečně
			 * neopakovali nějaké změny v souborech.
			 * 
			 * Takže při změně adresáře workspace se toto aktuální vlákno, které běží pokusí
			 * ukončit a spustit nové vlákno, v novém adresáři workspace/configuration, tak
			 * zde tu chybu pouze nebudu vypisovat do kozole užvateli.
			 */
			
			
			// Zde musím opravdu přrušit aktuální instanci toho vlákna:
			currentThread().interrupt();


            // Zápis do logu:
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informace i chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při pousu o ukončení vlákna. Jedná se o vlákno pro monitorování " +
                                "konfiguračních"
                                + " souborů pro změny. Vlákno nejspíše spalo (metoda sleep), když došlo k jeho " +
                                "přerušení.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
	}
}
