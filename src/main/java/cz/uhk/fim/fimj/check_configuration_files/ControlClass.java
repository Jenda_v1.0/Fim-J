package cz.uhk.fim.fimj.check_configuration_files;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.ConfigurationFiles;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.file.WriteToFile;
import cz.uhk.fim.fimj.font_size_form.CheckDuplicatesInDiagrams;
import cz.uhk.fim.fimj.forms.ErrorInConfigFilesForm;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;

/**
 * Tato třída slouží pro jakési "kontrolování požadavků" na zkontrolování jednotlivých konfiguračních souborů v adresáři
 * confugration ve workspace.
 * <p>
 * Jde o to, že když dojde k nějaké změně nějakého konfiguračního souboru pro tuto aplikaci, pak je třeba otestovat
 * zadané hodnoty, kdyby uživatel například úmyslně zadal nějaké chybné hodnoty, tak aby nedošlo v aplikaci k nějaké
 * chybě.
 * <p>
 * Takže tato třída zjistí o jaký konfigurační soubor se jedná, tj. v jakém z konfiguračních souborů došlo ke změně a
 * dle toho se rozhodne co dál, jestli se má příslušný konfigurační soubor vytvořit znovu nebo jen zkontrolovat hodnoty
 * apod.
 * <p>
 * <p>
 * Note: Zde pouze poznamenám, že by byla také možnost u každé chyby pouze vložit výchozím povolenou hodnotu. Tzn. Když
 * se najde nějaká chybně zadaná hodnota nebo chybí klíč, tak by bylo možné vytvořit jen ten klíč, nebo zadat jen tu
 * výchozí hodnotu k příslušnému klíči, ale zde nevím, zda například uživatel ještě třeba nesmazal komentáře, že třeba
 * neví, jaké tam patří povolené hodnoty, nebo jen úmyslěn zkouší "shodit" aplikaci úmyslním zadáním nevalidních hodnot
 * apod. Tak proto vždy vytvořím úplně nový soubor s výchozím nastavením, abych předešil tomu, že se k těm klíčům v
 * příslušném souboru nebudou nacházet komentáře s povolenými hodnotami, případně nějaké to uspořádání apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ControlClass {

    private static CheckDuplicatesInDiagrams checkDuplicatesInDiagrams;


    private static final Semaphore MUTEX = new Semaphore(1);



    /**
     * Objekt Properties obsahující texty pro tuto aplikaci v uživatelem zvoleném
     * jazyce, tato rpoěmné je zde pouze pro to, že ji potřebuji předat do dialogu
     * pro zobrazení informací o chybách.
     */
    private static Properties languageProperties;






    /**
     * Konstruktor této třídy.
     *
     * Tento konstruktor by neměl být potřeba, stačí do objektu
     * CheckDuplicatesInDiagrams předat null a ne ten objekt s texty pro aplikaci,
     * protože nechci, aby se nějaké hlášky vypisovali, ale když už by se nějaké
     * měla v nouzovém případě vypsat, tak ať je alespoň ve zvoleném jazyce a ne v
     * tom výchozím.
     *
     * @param languageProperties
     *            - objekt s texty pro tuto aplikaci.
     */
    public ControlClass(final Properties languageProperties) {
        super();

        ControlClass.languageProperties = languageProperties;

        checkDuplicatesInDiagrams = new CheckDuplicatesInDiagrams(languageProperties);

        /*
         * Zde zavolám konstruktor, který naplní statické proměnné, které obshují texty
         * pro metodu toString v uživatelem zvoleném jazyce, je to zde pouze pro to,
         * abych nemusel pořád do kola předávat ty texty.
         */
        new ErrorInfo(languageProperties);
    }









    /**
     * Metoda, která slouží pro kontrolu toho, zda jsou konfigurační soubory (vždy
     * soubor fileName) v adresáři configuration ve workspace validní, tj. obsahuje
     * povolené hodnoty a žádné duplicity a že existují, pokud něco z toho neplatí,
     * pak se vytvoří s výchozími (validními) hodotami.
     *
     * @param fileName
     *            - název souboru v adresáři workspace/configuration, ve kterém
     *            došlo k nějaké změně, nebo se má otestovat, zda obsahuje validní
     *            hodnoty.
     *
     * @param showErrorInfoDialog
     *            - logická proměnná, která značí, zda se má v případě, že nějaký
     *            soubor obsahuje nevalidní hodnoty, tak zda se informace o těchto
     *            chybách v souborech mají zobrazit v příslušném dialogu nebo ne.
     *            Toto je takový doplněk, protože se toto testování může provést při
     *            spuštění aplikace, tak, v takovém případě nebudu uživateli
     *            vypisovat informace o nalezených chybách.
     */
    public static void createdModifiedFile(final String fileName, final boolean showErrorInfoDialog) {
        /*
         * Zkusím si získat cestu k adresáři configuration ve workspace.
         *
         * Stačí jej zde získat pouze jednou, sice se vždy otestuje, zda existuje
         * adresář confugration a případě se vytvoří, ale z mého pohledu je to lepší zde
         * jendou "naplnit" / získat si tu cestu, než abych tuto operaci prováděl v
         * každé z podmínek níže.
         *
         * Navíc, pokud ten adresář neexistuje, tak se zde vytvoří i s příslušnými
         * soubory a ty už budou obsahovat výchozí nastavení, takže by měli být veškerá
         * data v pořádku.
         */
        final String pathToConfigurationDir = getPathToConfigurationDir();



        /*
         * List, který slouží pro zobrazení chyb, které byly nalezeny, například u
         * jakých klíčů v jakém souboru byla nalezan chybná hodnota apod.
         */
        final List<ErrorInfo> errorList = new ArrayList<>();

        /*
         * List, do kterého se budou vkládat informace o nalezených duplicitách,
         * například že v tomhle diagramu byly nalezeny stejné hrany pro tuto a tuto
         * hranu apod.
         */
        final List<ErrorInfo> duplicatesList = new ArrayList<>();










        if (fileName.equals(Constants.DEFAULT_PROPERTIES_NAME)) {
            /*
             * Otestuji zadané hodnoty v souboru Default.properties a získám si list, který
             * bude obsahovat přehled chyb. Pokud je tenlist prázdný, je to OK, pokud není,
             * pak byla nalezena nějaká chyba v příslušném souboru.
             */
            final List<ErrorInfo> tempErrorList = CheckDefaultProp.checkDefaultPropFile();

            /*
             * V případě, že byly nalezeny nějaké chyby, tak znovu vytvořím příslušný soubor v configuration ve workspace
             * s výchozími validními hodnotami a přidám chyby do listu pro celkové oznámení nalezených chyb.
             */
            if (!tempErrorList.isEmpty()) {
                /*
                 * Do "celkového" listu s chybami, který bude zobrazen přidám hlášení, která
                 * byla získána výše (veškeré nalezené chyby v příslušném souboru).
                 */
                errorList.addAll(tempErrorList);

                acquireMutex();

                /*
                 * Pokud se mi podařilo získat cestu k adresáři configuration ve workspace, pak
                 * zapíšu příslušný soubor s výchozím nastavením.
                 */
                if (pathToConfigurationDir != null)
                    ConfigurationFiles.writeDefaultPropertiesAllNew(
                            pathToConfigurationDir + File.separator + Constants.DEFAULT_PROPERTIES_NAME);

                releaseMutex();
            }
        }









        else if (fileName.equals(Constants.CLASS_DIAGRAM_NAME)) {
            /*
             * Načtu si objekt Properties, který obsahuje vlastnosti pro diagram tříd.
             */
            final Properties classDiagramProp = App.READ_FILE.getClassDiagramFromWorkspaceOnly();

            if (classDiagramProp != null) {
                /*
                 * List, do kterého si vložím nalezené chyby, pokud tento list nebude prázdný,
                 * pak byla nalezena nějaká chyba.
                 */
                final List<ErrorInfo> tempErrorList = CheckClassDiagramProp.checkClassDiagramPropFile(classDiagramProp);


                /*
                 * Pouze v případě, že nebyla nalezna nějaká chyba mohu / můžu otestovat, zda se
                 * v příslušném souboru nacházejí nějaké duplicity, protože pokud se v souboru
                 * nachází nějaká chyba, pak se automaticky vytvoří nový soubor s výchozím
                 * nastavením a ten neobsahuje duplicity, takže to ani není třeba testovat.
                 */
                if (!tempErrorList.isEmpty()) {
                    /*
                     * Do "celkového" listu s chybami, který bude zobrazen přidám hlášení, která
                     * byla získána výše (veškeré nalezené chyby v příslušném souboru).
                     */
                    errorList.addAll(tempErrorList);

                    /*
                     * Pokud se mi podařilo získat cestu k adresáři configuration ve workspace, pak
                     * zapíšu příslušný soubor s výchozím nastavením.
                     */
                    acquireMutex();

                    if (pathToConfigurationDir != null)
                        ConfigurationFiles.writeClassDiagramProperties(
                                pathToConfigurationDir + File.separator + Constants.CLASS_DIAGRAM_NAME);

                    releaseMutex();
                }

                else {
                    /*
                     * Zde jsou veškeré hodnoty zadány správně, tak otestuji, zda uživatel nezadal
                     * úmyslně stejné hodnoty pro třídy a komentáře nebo pro některé hrany, pokud
                     * ano, pak příslušný konfigurační soubor také zapíšu znovu s výchozím
                     * nastavením.
                     */
                    boolean sameSettings = false;

                    if (checkDuplicatesInDiagrams.areClassesAndCommentsSameInCd(classDiagramProp)) {
                        duplicatesList.add(new ErrorInfo(KindOfErrorInfo.CELLS_DUPLICATE_ERROR));

                        sameSettings = true;
                    }

                    if (checkDuplicatesInDiagrams.areEdgesInCdSame(classDiagramProp, false)) {
                        duplicatesList.add(new ErrorInfo(KindOfErrorInfo.CD_EDGES_DUPLICATE_ERROR));

                        sameSettings = true;
                    }

                    acquireMutex();

                    /*
                     * Zde otestji, zda se pro buňky komentáře a třídy nebo některé hrany nachází
                     * stejné nastavení, pokud ano, pak příslušný soubor zapíšu s výchozím
                     * nastavením.
                     */
                    if (sameSettings && pathToConfigurationDir != null)
                        ConfigurationFiles.writeClassDiagramProperties(
                                pathToConfigurationDir + File.separator + Constants.CLASS_DIAGRAM_NAME);

                    releaseMutex();
                }
            }



            else {
                /*
                 * Zde poznamenám, že tato část by měla být zbytečná a nikdy by neměla nastat,
                 * protože pokud běží aplikace a ten soubor se smaže, tak se to zjistí a znovu
                 * se vytvoří s výchozím nastavením, a když se upraví tak se také případně
                 * vytvoří s výchozím nastavením nebo ponechají změny a když se aplikace spustí,
                 * tak se také otstují potřebné soubory, takže tato část podmínky by se nikdy
                 * neměla vykonat, ale kdy už, tak aby se alespoň to výchozím nastavení
                 * vytvořilo.
                 */

                /*
                 * Zde se nepodařilo načíst soubor ClassDiagram.properties, resp. nepodařilo se
                 * načíst soubor .properties, který obsahuje nastavení pro diagram tříd, takže
                 * nejpravzděpodobnější důvod je, že ten soubor neexistuje, tak jej vytvořím s
                 * výchozím validním nastavením.
                 */
                final String pathToWorkspace = ReadFile.getPathToWorkspace();

                acquireMutex();

                /*
                 * Pokud byla nalezena cesta k adresář workspace, pak pro jitotu znovu otestuji,
                 * zda existuje adresář configuration, ale už zde netestuji verzi, kdy
                 * neexistuje aby se vytvořil, protože by to nikdy nastat nemělo, a pokud
                 * tenadresář existuje, tak vytvořím pčříslušný soubor.
                 */
                if (pathToWorkspace != null
                        && ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
                    ConfigurationFiles.writeClassDiagramProperties(
                            pathToWorkspace + File.separator + Constants.PATH_TO_CLASS_DIAGRAM_PROPERTIES);

                releaseMutex();
            }
        }








        else if (fileName.equals(Constants.INSTANCE_DIAGRAM_NAME)) {
            /*
             * Otestuji zadané hodnoty v souboru InstanceDiagram.properties a získám si
             * list, který bude obsahovat přehled chyb. Pokud je tenlist prázdný, je to OK,
             * pokud není, pak byla nalezena nějaká chyba v příslušném souboru.
             */
            final Properties instanceDiagramProp = App.READ_FILE.getInstanceDiagramFromWorkspaceOnly();

            if (instanceDiagramProp != null) {
                /*
                 * List, do kterého si vložím nalezené chyby, pokud tento list nebude prázdný,
                 * pak byla nalezena nějaká chyba.
                 */
                final List<ErrorInfo> tempErrorList = CheckInstanceDiagramProp
                        .checkInstanceDiagramPropFile(instanceDiagramProp);

                if (!tempErrorList.isEmpty()) {
                    /*
                     * Do "celkového" listu s chybami, který bude zobrazen přidám hlášení, která
                     * byla získána výše (veškeré nalezené chyby v příslušném souboru).
                     */
                    errorList.addAll(tempErrorList);

                    acquireMutex();

                    /*
                     * Pokud se mi podařilo získat cestu k adresáři configuration ve workspace, pak
                     * zapíšu příslušný soubor s výchozím nastavením.
                     */
                    if (pathToConfigurationDir != null)
                        ConfigurationFiles.writeInstanceDiagramProperties(
                                pathToConfigurationDir + File.separator + Constants.INSTANCE_DIAGRAM_NAME);

                    releaseMutex();
                }

                /*
                 * Zde nebyla nalezena žádná chyba v konfiguračním souboru, pak mohu otestovat,
                 * zda se v něm nachází hrany se stejným nastvením, resp. jestli je pro hrany
                 * pro agregaci a asociace nastaven stejný design nebo ne, pokud ano, pak také
                 * vytvořím znovu konfigurační soubor s výchozím nastavením.
                 */
                else if (checkDuplicatesInDiagrams.areAssociationAndAggregationSameInId(instanceDiagramProp)) {
                    duplicatesList.add(new ErrorInfo(KindOfErrorInfo.ID_EDGES_DUPLICATE_ERROR));

                    acquireMutex();

                    // Zkusím vytvoři příslušný soubor znovu s výchozím nastavením:
                    if (pathToConfigurationDir != null)
                        ConfigurationFiles.writeInstanceDiagramProperties(
                                pathToConfigurationDir + File.separator + Constants.INSTANCE_DIAGRAM_NAME);

                    releaseMutex();
                }
            }


            else {
                /*
                 * Zde poznamenám, že tato část by měla být zbytečná a nikdy by neměla nastat,
                 * protože pokud běží aplikace a ten soubor se smaže, tak se to zjistí a znovu
                 * se vytvoří s výchozím nastavením, a když se upraví tak se také případně
                 * vytvoří s výchozím nastavením nebo ponechají změny a když se aplikace spustí,
                 * tak se také otstují potřebné soubory, takže tato část podmínky by se nikdy
                 * neměla vykonat, ale kdy už, tak aby se alespoň to výchozím nastavení
                 * vytvořilo.
                 */

                /*
                 * Zde se nepodařilo načíst soubor InstanceDiagram.properties, resp. nepodařilo
                 * se načíst soubor .properties, který obsahuje nastavení pro diagram instancí,
                 * takže nejpravzděpodobnější důvod je, že ten soubor neexistuje, tak jej
                 * vytvořím s výchozím validním nastavením.
                 */
                final String pathToWorkspace = ReadFile.getPathToWorkspace();

                acquireMutex();

                /*
                 * Pokud byla nalezena cesta k adresář workspace, pak pro jitotu znovu otestuji,
                 * zda existuje adresář configuration, ale už zde netestuji verzi, kdy
                 * neexistuje aby se vytvořil, protože by to nikdy nastat nemělo, a pokud
                 * tenadresář existuje, tak vytvořím pčříslušný soubor.
                 */
                if (pathToWorkspace != null
                        && ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
                    ConfigurationFiles.writeInstanceDiagramProperties(
                            pathToWorkspace + File.separator + Constants.PATH_TO_INSTANCE_DIAGRAM_PROPERTIES);

                releaseMutex();
            }
        }









        else if (fileName.equals(Constants.COMMAND_EDITOR_NAME)) {
            /*
             * Otestuji zadané hodnoty v souboru CommandEditor.properties a získám si list,
             * který bude obsahovat přehled chyb. Pokud je tenlist prázdný, je to OK, pokud
             * není, pak byla nalezena nějaká chyba v příslušném souboru.
             */
            final List<ErrorInfo> tempErrorList = CheckCommandEditorProp.checkCommandEditorProp();

            /*
             * V případě, že byly nalezeny nějaké chyby, tak znovu vytvořím příslušný soubor v configuration ve workspace
             * s výchozími validními hodnotami a přidám chyby do listu pro celkové oznámení nalezených chyb.
             */
            if (!tempErrorList.isEmpty()) {
                /*
                 * Do "celkového" listu s chybami, který bude zobrazen přidám hlášení, která
                 * byla získána výše (veškeré nalezené chyby v příslušném souboru).
                 */
                errorList.addAll(tempErrorList);

                acquireMutex();

                /*
                 * Pokud se mi podařilo získat cestu k adresáři configuration ve workspace, pak
                 * zapíšu příslušný soubor s výchozím nastavením.
                 */
                if (pathToConfigurationDir != null)
                    ConfigurationFiles.writeCommandEditorProperties(
                            pathToConfigurationDir + File.separator + Constants.COMMAND_EDITOR_NAME);

                releaseMutex();
            }
        }









        else if (fileName.equals(Constants.CODE_EDITOR_NAME)) {
            /*
             * Otestuji zadané hodnoty v souboru CodeEditor.properties a získám si list,
             * který bude obsahovat přehled chyb. Pokud je tenlist prázdný, je to OK, pokud
             * není, pak byla nalezena nějaká chyba v příslušném souboru.
             */
            final List<ErrorInfo> tempErrorList = CheckCodeEditorProp.checkCodeEditorProp();

            /*
             * V případě, že byly nalezeny nějaké chyby, tak znovu vytvořím příslušný soubor v configuration ve workspace
             * s výchozími validními hodnotami a přidám chyby do listu pro celkové oznámení nalezených chyb.
             */
            if (!tempErrorList.isEmpty()) {
                /*
                 * Do "celkového" listu s chybami, který bude zobrazen přidám hlášení, která
                 * byla získána výše (veškeré nalezené chyby v příslušném souboru).
                 */
                errorList.addAll(tempErrorList);

                acquireMutex();

                /*
                 * Pokud se mi podařilo získat cestu k adresáři configuration ve workspace, pak
                 * zapíšu příslušný soubor s výchozím nastavením.
                 */
                if (pathToConfigurationDir != null)
                    ConfigurationFiles.writeCodeEditorProperties(
                            pathToConfigurationDir + File.separator + Constants.CODE_EDITOR_NAME);

                releaseMutex();
            }
        }







        else if (fileName.equals(Constants.OUTPUT_EDITOR_NAME)) {
            /*
             * Otestuji zadané hodnoty v souboru OutputEditor.properties a získám si list,
             * který bude obsahovat přehled chyb. Pokud je tenlist prázdný, je to OK, pokud
             * není, pak byla nalezena nějaká chyba v příslušném souboru.
             */
            final List<ErrorInfo> tempErrorList = CheckOutputEditorProp.checkOutputEditorProp();

            /*
             * V případě, že byly nalezeny nějaké chyby, tak znovu vytvořím příslušný soubor v configuration ve workspace
             * s výchozími validními hodnotami a přidám chyby do listu pro celkové oznámení nalezených chyb.
             */
            if (!tempErrorList.isEmpty()) {
                /*
                 * Do "celkového" listu s chybami, který bude zobrazen přidám hlášení, která
                 * byla získána výše (veškeré nalezené chyby v příslušném souboru).
                 */
                errorList.addAll(tempErrorList);

                acquireMutex();

                /*
                 * Pokud se mi podařilo získat cestu k adresáři configuration ve workspace, pak
                 * zapíšu příslušný soubor s výchozím nastavením.
                 */
                if (pathToConfigurationDir != null)
                    ConfigurationFiles.writeOutputEditorProperties(
                            pathToConfigurationDir + File.separator + Constants.OUTPUT_EDITOR_NAME);

                releaseMutex();
            }
        }







        else if (fileName.equals(Constants.OUTPUT_FRAME_NAME)) {
            /*
             * Otestuji zadané hodnoty v souboru OutputFrame.properties a získám si list,
             * který bude obsahovat přehled chyb. Pokud je tenlist prázdný, je to OK, pokud
             * není, pak byla nalezena nějaká chyba v příslušném souboru.
             */
            final List<ErrorInfo> tempErrorList = CheckOutputFrameProp.checkOutputFrameProp();

            /*
             * V případě, že byly nalezeny nějaké chyby, tak znovu vytvořím příslušný soubor v configuration ve workspace
             * s výchozími validními hodnotami a přidám chyby do listu pro celkové oznámení nalezených chyb.
             */
            if (!tempErrorList.isEmpty()) {
                /*
                 * Do "celkového" listu s chybami, který bude zobrazen přidám hlášení, která
                 * byla získána výše (veškeré nalezené chyby v příslušném souboru).
                 */
                errorList.addAll(tempErrorList);

                acquireMutex();

                /*
                 * Pokud se mi podařilo získat cestu k adresáři configuration ve workspace, pak
                 * zapíšu příslušný soubor s výchozím nastavením.
                 */
                if (pathToConfigurationDir != null)
                    ConfigurationFiles.writeOutputFrameProperties(
                            pathToConfigurationDir + File.separator + Constants.OUTPUT_FRAME_NAME);

                releaseMutex();
            }
        }






        else if (fileName.equals(Constants.CODE_EDITOR_INTERNAL_FRAMES_NAME)) {
            /*
             * Otestuji zadané hodnoty v souboru CodeEditorFinternalFrame.properties a
             * získám si list, který bude obsahovat přehled chyb. Pokud je tenlist prázdný,
             * je to OK, pokud není, pak byla nalezena nějaká chyba v příslušném souboru.
             */
            final List<ErrorInfo> tempErrorList = CheckCodeEditorInternalFramesProp
                    .checkProjectCodeEditorInternalFrameProp();

            /*
             * V případě, že byly nalezeny nějaké chyby, tak znovu vytvořím příslušný soubor v configuration ve workspace
             * s výchozími validními hodnotami a přidám chyby do listu pro celkové oznámení nalezených chyb.
             */
            if (!tempErrorList.isEmpty()) {
                /*
                 * Do "celkového" listu s chybami, který bude zobrazen přidám hlášení, která
                 * byla získána výše (veškeré nalezené chyby v příslušném souboru).
                 */
                errorList.addAll(tempErrorList);

                acquireMutex();

                /*
                 * Pokud se mi podařilo získat cestu k adresáři configuration ve workspace, pak
                 * zapíšu příslušný soubor s výchozím nastavením.
                 */
                if (pathToConfigurationDir != null)
                    ConfigurationFiles.writeCodeEditorPropertiesForInternalFrames(
                            pathToConfigurationDir + File.separator + Constants.CODE_EDITOR_INTERNAL_FRAMES_NAME);

                releaseMutex();
            }
        }







        /*
         * V případě, že se má zobrazit dialog a byly nalezeny v nějakém soubor nějaké
         * chyby (duplicity, nevalidní či chybějící hodnoty apod.) tak, zda se má ten
         * diaog zobrazit uživateli nebo ne.
         */
        if (showErrorInfoDialog && (!errorList.isEmpty() || !duplicatesList.isEmpty())) {
            final ErrorInConfigFilesForm ecff = new ErrorInConfigFilesForm(fileName, errorList, duplicatesList);

            ecff.setLanguage(languageProperties);

            ecff.setVisible(true);
        }
    }












    /**
     * Tato metoda se zavolá pouze v případě, že se zaznaměnala změna souboru s
     * název (a příponou) fileName ve sledovaném adresáři, pokud se jedná o jeden z
     * konfiguračních souborů, pak se znovu vytvoří s výchozím nastavením.
     *
     * @param fileName
     *            - název souboru, který byl smazán.
     */
    static void deletedFile(final String fileName) {
        /*
         * Zkusím si získat cestu k adresáři configuration ve workspace.
         *
         * Stačí jej zde získat pouze jednou, sice se vždy otestuje, zda existuje
         * adresář confugration a případě se vytvoří, ale z mého pohledu je to lepší zde
         * jendou "naplnit" / získat si tu cestu, než abych tuto operaci prováděl v
         * každé z podmínek níže.
         *
         * Navíc, pokud ten adresář neexistuje, tak se zde vytvoří i s příslušnými
         * soubory a ty už budou obsahovat výchozí nastavení, takže by měli být veškerá
         * data v pořádku.
         */
        final String pathToConfigurationDir = getPathToConfigurationDir();



        /*
         * Note:
         * Mutex bych mohl jednou spustit zde a na konc metody ukončit, ale ne vždy se
         * musí jednat o příslušný soubor, proto jej spouštím pouze, když mám jistotu,
         * že se má manipulovat s těmi soubory.
         */



        if (fileName.equals(Constants.DEFAULT_PROPERTIES_NAME) && pathToConfigurationDir != null) {
            acquireMutex();

            /*
             * Pokud se mi podařilo získat cestu k adresáři configuration ve workspace, pak
             * zapíšu příslušný soubor s výchozím nastavením.
             */
            ConfigurationFiles.writeDefaultPropertiesAllNew(
                    pathToConfigurationDir + File.separator + Constants.DEFAULT_PROPERTIES_NAME);

            releaseMutex();
        }



        else if (fileName.equals(Constants.CLASS_DIAGRAM_NAME) && pathToConfigurationDir != null) {
            acquireMutex();

            /*
             * Pokud se mi podařilo získat cestu k adresáři configuration ve workspace, pak
             * zapíšu příslušný soubor s výchozím nastavením.
             */
            ConfigurationFiles.writeClassDiagramProperties(
                    pathToConfigurationDir + File.separator + Constants.CLASS_DIAGRAM_NAME);

            releaseMutex();
        }



        else if (fileName.equals(Constants.INSTANCE_DIAGRAM_NAME) && pathToConfigurationDir != null) {
            acquireMutex();

            /*
             * Pokud se mi podařilo získat cestu k adresáři configuration ve workspace, pak
             * zapíšu příslušný soubor s výchozím nastavením.
             */
            ConfigurationFiles.writeInstanceDiagramProperties(
                    pathToConfigurationDir + File.separator + Constants.INSTANCE_DIAGRAM_NAME);

            releaseMutex();
        }



        else if (fileName.equals(Constants.COMMAND_EDITOR_NAME) && pathToConfigurationDir != null) {
            acquireMutex();

            /*
             * Pokud se mi podařilo získat cestu k adresáři configuration ve workspace, pak
             * zapíšu příslušný soubor s výchozím nastavením.
             */
            ConfigurationFiles.writeCommandEditorProperties(
                    pathToConfigurationDir + File.separator + Constants.COMMAND_EDITOR_NAME);

            releaseMutex();
        }



        else if (fileName.equals(Constants.CODE_EDITOR_NAME) && pathToConfigurationDir != null) {
            acquireMutex();

            /*
             * Pokud se mi podařilo získat cestu k adresáři configuration ve workspace, pak
             * zapíšu příslušný soubor s výchozím nastavením.
             */
            ConfigurationFiles
                    .writeCodeEditorProperties(pathToConfigurationDir + File.separator + Constants.CODE_EDITOR_NAME);

            releaseMutex();
        }



        else if (fileName.equals(Constants.OUTPUT_EDITOR_NAME) && pathToConfigurationDir != null) {
            acquireMutex();

            /*
             * Pokud se mi podařilo získat cestu k adresáři configuration ve workspace, pak
             * zapíšu příslušný soubor s výchozím nastavením.
             */
            ConfigurationFiles.writeOutputEditorProperties(
                    pathToConfigurationDir + File.separator + Constants.OUTPUT_EDITOR_NAME);

            releaseMutex();
        }



        else if (fileName.equals(Constants.OUTPUT_FRAME_NAME) && pathToConfigurationDir != null) {
            acquireMutex();

            /*
             * Pokud se mi podařilo získat cestu k adresáři configuration ve workspace, pak
             * zapíšu příslušný soubor s výchozím nastavením.
             */
            ConfigurationFiles
                    .writeOutputFrameProperties(pathToConfigurationDir + File.separator + Constants.OUTPUT_FRAME_NAME);

            releaseMutex();
        }



        else if (fileName.equals(Constants.CODE_EDITOR_INTERNAL_FRAMES_NAME) && pathToConfigurationDir != null) {
            acquireMutex();

            /*
             * Pokud se mi podařilo získat cestu k adresáři configuration ve workspace, pak
             * zapíšu příslušný soubor s výchozím nastavením.
             */
            ConfigurationFiles.writeCodeEditorPropertiesForInternalFrames(
                    pathToConfigurationDir + File.separator + Constants.CODE_EDITOR_INTERNAL_FRAMES_NAME);

            releaseMutex();
        }
    }












    /**
     * Metoda, která spustí mutex pro blokování přístupu k souboru ostatnímí částmi
     * aplikace.
     */
    private static void acquireMutex() {
        try {

            MUTEX.acquire();

        } catch (InterruptedException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při pokusu spuštění mutexu pro blokování přístupu k souboru. Tato výjimka " +
                                "může "
                                + "nastat například v případě, že dojde k přerušení běhu vlákna.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
    }






    /**
     * Uvolnění mutextu - zrušení blokování ostatních "částí" aplikace pro přístup k
     * příslušnému souboru.
     */
    private static void releaseMutex() {
        MUTEX.release();
    }







    /**
     * Metoda, která si zkusí získat cestu k adresáři configuration ve workspace.
     * Pokud se najde cesta k adresáři workspace, pak se ještě otestuje, zda v něm
     * existuje adresář configuration, pokud neexistuje, pak se vytvoří, pokud
     * existuje, pak se k němu vrátí cesta.
     *
     * @return cesta k adresář configuration nebo null, pokud nebude nalezen adresář
     *         workspace nebo se v něm nepodaři vytvořít adresář configuration.
     */
    private static String getPathToConfigurationDir() {
        final String pathToWorkspace = ReadFile.getPathToWorkspace();

        if (pathToWorkspace != null) {
            final String pathToConfigDir = pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME;
            if (!ReadFile.existsDirectory(pathToConfigDir))
                // Zde neexistuje configuration, tak ho nakopíruji:
                WriteToFile.createConfigureFilesInPath(pathToWorkspace);

            /*
             * Zde pro jistotu otestuji, zda se podařilo vytvořit adresář configuration ve
             * workspace, a pokud ano, pak k němu mohu vrátit cestu.
             */
            if (ReadFile.existsDirectory(pathToConfigDir))
                return pathToConfigDir;
        }

        return null;
    }
}
