package cz.uhk.fim.fimj.check_configuration_files;

import java.util.List;
import java.util.Properties;

/**
 * Tato třída slouží pro kontrolu hodnot, které je možné zadat do konfiguračního souboru (.properties), který obsahuje
 * nastavení pro diagram instancí.
 * <p>
 * V této třídě se otestuji zadané hodnoty, zda jsou zadány správné hodnoty, ve správné syntaxi, rozsahu apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

class CheckInstanceDiagramProp extends CheckConfigPropAbstract {

	/**
	 * Metoda, která slouží pro zkontrolování dat v načteném souboru
	 * InstancesDiagram.properties.
	 * 
	 * @param instanceDiagramProp
	 *            - objekt Properties, který je načteným souborem
	 *            InstanceDaigram.properties
	 * 
	 * @return list,který bude obsahovat veškeré nalezené chyby, nebo bude prázdný,
	 *         pokud žádná chyba nebude nalezena.
	 */
	static List<ErrorInfo> checkInstanceDiagramPropFile(final Properties instanceDiagramProp) {
		ERRORS_LIST.clear();

		// Podmínka by měla být zbytečná, ale kdyby náhodou:
		if (instanceDiagramProp != null)
			checkValues(instanceDiagramProp);
		
		return ERRORS_LIST;
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro otestování veškerých zadanách hodnot v konfiguračním
	 * souboru InstanceDiagram.properties.
	 * 
	 * @param prop
	 *            - načtený soubor instanceDiagram.properties, ve kterém se mají
	 *            otestovat zadané hodnoty.
	 */
	private static void checkValues(final Properties prop) {
		/*
		 * Následují testování pro vlastnosti samotného diagramu:
		 */
		
		// Úroveň přiblížení diagramu:
		final String cdScale = getValueFromProperties("Scale", prop);

		if (cdScale == null)
			addErrorToList("Scale");

		else if (!testDouble(cdScale))
			addErrorToList("Scale", cdScale, DIAGRAM_SCALE_ALLOWED_VALUES);

		else {
			// Test pro rozsah:
			final double dScale = Double.parseDouble(cdScale);

			if (dScale < 0.5d || dScale > 20.0d)
				addErrorToList("Scale", cdScale, DIAGRAM_SCALE_ALLOWED_VALUES);
		}
		
		
		
		
		
		
		// Barva pozadí diagramu:
		final String bgColor = getValueFromProperties("BackgroundColor", prop);

		if (bgColor == null)
			addErrorToList("BackgroundColor");

		else if (!testColor(bgColor))
			addErrorToList("BackgroundColor", bgColor, COLOR_ALLOWED_VALUES);
		
		
		
		
		
		
		
		
		final String edgeLabelMovable = getValueFromProperties("EdgeLabelMovable", prop);

		if (edgeLabelMovable == null)
			addErrorToList("EdgeLabelMovable");

		else if (!testBoolean(edgeLabelMovable))
			addErrorToList("EdgeLabelMovable", edgeLabelMovable, BOOLEAN_ALLOWED_VALUES);




		final String showInheritedClassNameWithPackages = getValueFromProperties("ShowInheritedClassNameWithPackages", prop);

		if (showInheritedClassNameWithPackages == null)
			addErrorToList("ShowInheritedClassNameWithPackages");

		else if (!testBoolean(showInheritedClassNameWithPackages))
			addErrorToList("ShowInheritedClassNameWithPackages", showInheritedClassNameWithPackages, BOOLEAN_ALLOWED_VALUES);






		final String includePrivateMethods = getValueFromProperties("IncludePrivateMethods", prop);

		if (includePrivateMethods == null)
			addErrorToList("IncludePrivateMethods");

		else if (!testBoolean(includePrivateMethods))
			addErrorToList("IncludePrivateMethods", includePrivateMethods, BOOLEAN_ALLOWED_VALUES);


		final String includeProtectedMethods = getValueFromProperties("IncludeProtectedMethods", prop);

		if (includeProtectedMethods == null)
			addErrorToList("IncludeProtectedMethods");

		else if (!testBoolean(includeProtectedMethods))
			addErrorToList("IncludeProtectedMethods", includeProtectedMethods, BOOLEAN_ALLOWED_VALUES);


		final String includePackagePrivateMethods = getValueFromProperties("IncludePackagePrivateMethods", prop);

		if (includePackagePrivateMethods == null)
			addErrorToList("IncludePackagePrivateMethods");

		else if (!testBoolean(includePackagePrivateMethods))
			addErrorToList("IncludePackagePrivateMethods", includePackagePrivateMethods, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		final String makeFiledsAvailableForMethod = getValueFromProperties("MakeFieldsAvailableForMethod", prop);

		if (makeFiledsAvailableForMethod == null)
			addErrorToList("MakeFieldsAvailableForMethod");

		else if (!testBoolean(makeFiledsAvailableForMethod))
			addErrorToList("MakeFieldsAvailableForMethod", makeFiledsAvailableForMethod, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		
		final String showRelationShipsToInstanceItself = getValueFromProperties("ShowRelationsShipsToInstanceItself",
				prop);

		if (showRelationShipsToInstanceItself == null)
			addErrorToList("ShowRelationsShipsToInstanceItself");

		else if (!testBoolean(showRelationShipsToInstanceItself))
			addErrorToList("ShowRelationsShipsToInstanceItself", showRelationShipsToInstanceItself,
					BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		
		final String showCreateNewInstanceOptionInCallMethod = getValueFromProperties(
				"ShowCreateNewInstanceOptionInCallMethod", prop);

		if (showCreateNewInstanceOptionInCallMethod == null)
			addErrorToList("ShowCreateNewInstanceOptionInCallMethod");

		else if (!testBoolean(showCreateNewInstanceOptionInCallMethod))
			addErrorToList("ShowCreateNewInstanceOptionInCallMethod", showCreateNewInstanceOptionInCallMethod,
					BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		
		final String makeMethodsAvailableForCallMethod = getValueFromProperties("MakeMethodsAvailableForCallMethod",
				prop);

		if (makeMethodsAvailableForCallMethod == null)
			addErrorToList("MakeMethodsAvailableForCallMethod");

		else if (!testBoolean(makeMethodsAvailableForCallMethod))
			addErrorToList("MakeMethodsAvailableForCallMethod", makeMethodsAvailableForCallMethod,
					BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		
		final String useFastWayToRefreshRelationshipsBetweeninstance = getValueFromProperties(
				"UseFastWayToRefreshRelationshipsBetweenInstances", prop);

		if (useFastWayToRefreshRelationshipsBetweeninstance == null)
			addErrorToList("UseFastWayToRefreshRelationshipsBetweenInstances");

		else if (!testBoolean(useFastWayToRefreshRelationshipsBetweeninstance))
			addErrorToList("UseFastWayToRefreshRelationshipsBetweenInstances",
					useFastWayToRefreshRelationshipsBetweeninstance, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		
		
		final String showRelationshipsInInheritedClasses = getValueFromProperties("ShowRelationshipsInInheritedClasses",
				prop);

		if (showRelationshipsInInheritedClasses == null)
			addErrorToList("ShowRelationshipsInInheritedClasses");

		else if (!testBoolean(showRelationshipsInInheritedClasses))
			addErrorToList("ShowRelationshipsInInheritedClasses", showRelationshipsInInheritedClasses,
					BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		final String showAssociationThroughAggregation = getValueFromProperties("ShowAssociationThroughAggregation",
				prop);

		if (showAssociationThroughAggregation == null)
			addErrorToList("ShowAssociationThroughAggregation");

		else if (!testBoolean(showAssociationThroughAggregation))
			addErrorToList("ShowAssociationThroughAggregation", showAssociationThroughAggregation,
					BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		
		final String showReferenceVariables = getValueFromProperties("ShowReferenceVariables", prop);

		if (showReferenceVariables == null)
			addErrorToList("ShowReferenceVariables");

		else if (!testBoolean(showReferenceVariables))
			addErrorToList("ShowReferenceVariables", showReferenceVariables, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		final String showGetterInSetterMethodInInstance = getValueFromProperties("ShowGetterInSetterMethodInInstance", prop);

		if (showGetterInSetterMethodInInstance == null)
			addErrorToList("ShowGetterInSetterMethodInInstance");

		else if (!testBoolean(showGetterInSetterMethodInInstance))
			addErrorToList("ShowGetterInSetterMethodInInstance", showGetterInSetterMethodInInstance, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		
		
		
		
		/*
		 * Následují hodnoty pro zvýraznění instance:
		 */
		
		final String highlightInstances = getValueFromProperties("HighlightInstances", prop);

		if (highlightInstances == null)
			addErrorToList("HighlightInstances");

		else if (!testBoolean(highlightInstances))
			addErrorToList("HighlightInstances", highlightInstances, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		final String highlightingTime = getValueFromProperties("HighlightingTime", prop);

		if (highlightingTime == null)
			addErrorToList("HighlightingTime");

		else if (!testDouble(highlightingTime))
			addErrorToList("HighlightingTime", highlightingTime, "<0.1 ; 5.0>");

		else {
			// Test pro rozsah:
			final double time = Double.parseDouble(highlightingTime);

			if (time < 0.5d || time > 20.0d)
				addErrorToList("HighlightingTime", highlightingTime, "<0.1 ; 5.0>");
		}
		
		
		
		
		
		
		
		final String textColorOfHighlightedinstance = getValueFromProperties("TextColorOfHighlightedInstance", prop);

		if (textColorOfHighlightedinstance == null)
			addErrorToList("TextColorOfHighlightedInstance");

		else if (!testColor(textColorOfHighlightedinstance))
			addErrorToList("TextColorOfHighlightedInstance", textColorOfHighlightedinstance, COLOR_ALLOWED_VALUES);
		
		
		
		
		
		
		final String attributeColorOfHighlightedinstance = getValueFromProperties("AttributeColorOfHighlightedInstance",
				prop);

		if (attributeColorOfHighlightedinstance == null)
			addErrorToList("AttributeColorOfHighlightedInstance");

		else if (!testColor(attributeColorOfHighlightedinstance))
			addErrorToList("AttributeColorOfHighlightedInstance", attributeColorOfHighlightedinstance,
					COLOR_ALLOWED_VALUES);
		
		
		
		
		
		
		
		final String methodColorOfhighlightedInstance = getValueFromProperties("MethodColorOfHighlightedInstance",
				prop);

		if (methodColorOfhighlightedInstance == null)
			addErrorToList("MethodColorOfHighlightedInstance");

		else if (!testColor(methodColorOfhighlightedInstance))
			addErrorToList("MethodColorOfHighlightedInstance", methodColorOfhighlightedInstance, COLOR_ALLOWED_VALUES);
		
		
		
		
		
		
		final String borderColorOfHighlightedInstance = getValueFromProperties("BorderColorOfHighlightedInstance",
				prop);

		if (borderColorOfHighlightedInstance == null)
			addErrorToList("BorderColorOfHighlightedInstance");

		else if (!testColor(borderColorOfHighlightedInstance))
			addErrorToList("BorderColorOfHighlightedInstance", borderColorOfHighlightedInstance, COLOR_ALLOWED_VALUES);
		
		
		
		
		
		
		final String backgroundColorOfHighlightedInstance = getValueFromProperties(
				"BackgroundColorOfHighlightedInstance", prop);

		if (backgroundColorOfHighlightedInstance == null)
			addErrorToList("BackgroundColorOfHighlightedInstance");

		else if (!testColor(backgroundColorOfHighlightedInstance))
			addErrorToList("BackgroundColorOfHighlightedInstance", backgroundColorOfHighlightedInstance,
					COLOR_ALLOWED_VALUES);
		
		
		
		
		
		
		
		
		
		
		
		/*
		 * Následují hodnoty pro zobrazení atributů a metod buňce reprezentující
		 * instanci v diagramu instancí:
		 */
		
		// Pro atributy v buňce:
		checkAttributesAndMethod(prop, "ShowAttributes", "ShowAllAttributes", "ShowSpecificCountOfAttributes",
				"UseForAttributesOneFontAndFontColorInInstanceCell", "AttributesFontSize", "AttributesFontStyle",
				"AttributesTextColor");
		
		
		// Pro metody v buňce:
		checkAttributesAndMethod(prop, "ShowMethods", "ShowAllMethods", "ShowSpecificCountOfMethods",
				"UseForMethodsOneFontAndFontColorInInstanceCell", "MethodsFontSize", "MethodsFontStyle",
				"MethodsTextColor");
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		/*
		 * Vlastnosti pro buňku, která v daigramu instancí reprezentuje instancí.
		 */
		
		
		final String roundedBorderMargin = getValueFromProperties("RoundedBorderMargin", prop);

		if (roundedBorderMargin == null)
			addErrorToList("RoundedBorderMargin");

		else if (!testInteger(roundedBorderMargin))
			addErrorToList("RoundedBorderMargin", roundedBorderMargin, "<1 ; 60>");

		else {
			// Test pro rozsah:
			final int margin = Integer.parseInt(roundedBorderMargin);

			if (margin < 1 || margin > 60)
				addErrorToList("RoundedBorderMargin", roundedBorderMargin, "<1 ; 60>");
		}
		
		
		
		
		
		final String showPackagesInCell = getValueFromProperties("ShowPackagesInCell", prop);

		if (showPackagesInCell == null)
			addErrorToList("ShowPackagesInCell");

		else if (!testBoolean(showPackagesInCell))
			addErrorToList("ShowPackagesInCell", showPackagesInCell, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		final String instanceFontStyle = getValueFromProperties("FontStyle", prop);

		if (instanceFontStyle == null)
			addErrorToList("FontStyle");

		else if (!testInteger(instanceFontStyle))
			addErrorToList("FontStyle", instanceFontStyle, FONT_STYLE_VALUES);

		else {
			// Test pro rozsah:
			final int style = Integer.parseInt(instanceFontStyle);

			// Hodnoty mohou být pouze 0, 1 nebo 2
			if (style < 0 || style > 2)
				addErrorToList("FontStyle", instanceFontStyle, FONT_STYLE_VALUES);
		}
		
		
		
		
		
		
		final String instanceFontSize = getValueFromProperties("FontSize", prop);

		if (instanceFontSize == null)
			addErrorToList("FontSize");

		else if (!testInteger(instanceFontSize))
			addErrorToList("FontSize", instanceFontSize, FONT_SIZE_VALUES);

		else {
			// Test pro rozsah:
			final int size = Integer.parseInt(instanceFontSize);

			// Hodnoty mohou být pouze v intervalu: <8 ; 150>
			if (size < 8 || size > 150)
				addErrorToList("FontSize", instanceFontSize, FONT_SIZE_VALUES);
		}
		
		
		
		
		
		
		final String instanceFontColor = getValueFromProperties("TextColor", prop);

		if (instanceFontColor == null)
			addErrorToList("TextColor");

		else if (!testColor(instanceFontColor))
			addErrorToList("TextColor", instanceFontColor, COLOR_ALLOWED_VALUES);
		
		
		
		
		
		
		
		final String borderColor = getValueFromProperties("BorderColor", prop);

		if (borderColor == null)
			addErrorToList("BorderColor");

		else if (!testColor(borderColor))
			addErrorToList("BorderColor", borderColor, COLOR_ALLOWED_VALUES);
		
		
		
		
		
		
		
		final String cellColor = getValueFromProperties("CellColor", prop);

		if (cellColor == null)
			addErrorToList("CellColor");

		else if (!testColor(cellColor))
			addErrorToList("CellColor", borderColor, COLOR_ALLOWED_VALUES);
		
		
		
		
		
		
		
		
		final String horizontalAlignment = getValueFromProperties("TextHorizontalAlignment", prop);

		if (horizontalAlignment == null)
			addErrorToList("TextHorizontalAlignment");

		else if (!testInteger(horizontalAlignment))
			addErrorToList("TextHorizontalAlignment", horizontalAlignment, HORIZONTAL_ALIGNMENT_VALUES);

		else {
			final int alignment = Integer.parseInt(horizontalAlignment);

			if (alignment != 0 && alignment != 2 && alignment != 4)
				addErrorToList("TextHorizontalAlignment", horizontalAlignment, HORIZONTAL_ALIGNMENT_VALUES);
		}
		
		
		
		
		
		final String verticalAlignment = getValueFromProperties("TextVerticalAlignment", prop);

		if (verticalAlignment == null)
			addErrorToList("TextVerticalAlignment");

		else if (!testInteger(verticalAlignment))
			addErrorToList("TextVerticalAlignment", verticalAlignment, VERTICAL_ALIGNMENT_VALUES);

		else {
			// Test pro rozsah:
			final int alignment = Integer.parseInt(verticalAlignment);

			if (alignment != 0 && alignment != 1 && alignment != 3)
				addErrorToList("TextVerticalAlignment", verticalAlignment, VERTICAL_ALIGNMENT_VALUES);
		}
		
		
		
		
		
		
		
		
		// Hrana asociace:
		checkEdgesProperties(prop, "AssociationEdgeLineColor", "AssociationEdgeLabelsAlongEdge",
				"AssociationEdgeLineEnd", "AssociationEdgeLineBegin", "AssociationEdgeLineWidth",
				"AssociationEdgeLineEndFill", "AssociationEdgeLineBeginFill", "AssociationEdgeLineTextColor",
				"AssociationEdgeLineFontSize", "AssociationEdgeLineFontStyle");
		
		
		// Hrana agregace:
		checkEdgesProperties(prop, "AggregationEdgeLineColor", "AggregationEdgeLabelsAlongEdge",
				"AggregationEdgeLineEnd", "AggregationEdgeLineBegin", "AggregationEdgeLineWidth",
				"AggregationEdgeLineEndFill", "AggregationEdgeLineBeginFill", "AggregationEdgeLineTextColor",
				"AggregationEdgeLineFontSize", "AggregationEdgeLineFontStyle");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro otestování toho, zda jsou správně zadané hodnoty v
	 * objektu / souboru Properties, který obsahuje vlastnosti pro diagram instancí.
	 * 
	 * Tato metoda konkrétně otestuji, zda jsou správně nastaveny vlastnosti pro
	 * hrany (asociace a agregace).
	 * 
	 * @param prop
	 *            - objekt Properties, který obsahuje příslušné vlasnosti - hodnoty
	 *            pro otestování.
	 * 
	 * @param keyLineColor
	 *            - klíč hodnoty pro barvu příslušné hran
	 * 
	 * @param keyLabelsAlongEdge
	 *            - klíč hodnoty pro to, zda mají být popisky podél hrany.
	 * 
	 * @param keyLineEnd
	 *            - klíč pro hodnotu pro styl zakončení hrany.
	 * 
	 * @param keyLineBegin
	 *            - klíč pro hodnotu pro styl začátku hrany.
	 * 
	 * @param keyLineWidth
	 *            - klíč pro hodnotu pro šířku hrany.
	 * 
	 * @param keyLineEndFill
	 *            - klíč pro hodnotu pro to, zda má být konec hrany vyplněn.
	 * 
	 * @param keyLineBeginFill
	 *            - klíč pro hodnotu pro to, zda má být začátek hrany vyplněn.
	 * 
	 * @param keyLineTextColor
	 *            - klíč pro hodnotu pro barvu písma na hraně.
	 * 
	 * @param keyLineFontSize
	 *            - klíč pro hodnotu pro velikost písma na příslušné hraně.
	 * 
	 * @param keyLineFontStyle
	 *            - klíč pro hdnotu pro styl písma na příslušné hraně.
	 */
	private static void checkEdgesProperties(final Properties prop, final String keyLineColor,
			final String keyLabelsAlongEdge, final String keyLineEnd, final String keyLineBegin,
			final String keyLineWidth, final String keyLineEndFill, final String keyLineBeginFill,
			final String keyLineTextColor, final String keyLineFontSize, final String keyLineFontStyle) {

		final String lineColor = getValueFromProperties(keyLineColor, prop);

		if (lineColor == null)
			addErrorToList(keyLineColor);

		else if (!testColor(lineColor))
			addErrorToList(keyLineColor, lineColor, COLOR_ALLOWED_VALUES);
		
		
		
		
		
		
		final String labelsAlongEdge = getValueFromProperties(keyLabelsAlongEdge, prop);

		if (labelsAlongEdge == null)
			addErrorToList(keyLabelsAlongEdge);

		else if (!testBoolean(labelsAlongEdge))
			addErrorToList(keyLabelsAlongEdge, labelsAlongEdge, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		
		
		final String lineEnd = getValueFromProperties(keyLineEnd, prop);

		if (lineEnd == null)
			addErrorToList(keyLineEnd);

		else if (!testInteger(lineEnd))
			addErrorToList(keyLineEnd, lineEnd, LINE_BEGIN_END_VALUES);

		else {
			// Test pro rozsah:
			final int end = Integer.parseInt(lineEnd);

			if (end != 0 && end != 1 && end != 2 && end != 4 && end != 5 && end != 7 && end != 8 && end != 9)
				addErrorToList(keyLineEnd, lineEnd, LINE_BEGIN_END_VALUES);
		}
		
		
		
		
		
		
		
		final String lineBegin = getValueFromProperties(keyLineBegin, prop);

		if (lineBegin == null)
			addErrorToList(keyLineBegin);

		else if (!testInteger(lineBegin))
			addErrorToList(keyLineBegin, lineBegin, LINE_BEGIN_END_VALUES);

		else {
			// Test pro rozsah:
			final int begin = Integer.parseInt(lineBegin);

			if (begin != 0 && begin != 1 && begin != 2 && begin != 4 && begin != 5 && begin != 7 && begin != 8
					&& begin != 9)
				addErrorToList(keyLineBegin, lineBegin, LINE_BEGIN_END_VALUES);
		}
		
		
		
		
		
		final String lineWidth = getValueFromProperties(keyLineWidth, prop);

		if (lineWidth == null)
			addErrorToList(keyLineWidth);

		else if (!testDouble(lineWidth))
			addErrorToList(keyLineWidth, lineWidth, LINE_WIDTH_VALUES);

		else {
			// Test pro rozsah:
			final double width = Double.parseDouble(lineWidth);

			if (width < 0.1d || width > 20.0d)
				addErrorToList(keyLineWidth, lineWidth, LINE_WIDTH_VALUES);
		}
		
		
		
		
		
		
		
		
		
		final String lineEndFill = getValueFromProperties(keyLineEndFill, prop);

		if (lineEndFill == null)
			addErrorToList(keyLineEndFill);

		else if (!testBoolean(lineEndFill))
			addErrorToList(keyLineEndFill, lineEndFill, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		final String lineBeginFill = getValueFromProperties(keyLineBeginFill, prop);

		if (lineBeginFill == null)
			addErrorToList(keyLineBeginFill);

		else if (!testBoolean(lineBeginFill))
			addErrorToList(keyLineBeginFill, lineBeginFill, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		final String lineTextColor = getValueFromProperties(keyLineTextColor, prop);

		if (lineTextColor == null)
			addErrorToList(keyLineTextColor);

		else if (!testColor(lineTextColor))
			addErrorToList(keyLineTextColor, lineTextColor, COLOR_ALLOWED_VALUES);
		
		
		
		
		
		
		
		
		final String fontSize = getValueFromProperties(keyLineFontSize, prop);

		if (fontSize == null)
			addErrorToList(keyLineFontSize);

		else if (!testInteger(fontSize))
			addErrorToList(keyLineFontSize, fontSize, FONT_SIZE_VALUES);

		else {
			// Test pro rozsah:
			final int size = Integer.parseInt(fontSize);

			// Hodnoty mohou být pouze v intervalu: <8 ; 150>
			if (size < 8 || size > 150)
				addErrorToList(keyLineFontSize, fontSize, FONT_SIZE_VALUES);
		}
		
		
		
		
		
		
		
		
		final String fontStyle = getValueFromProperties(keyLineFontStyle, prop);

		if (fontStyle == null)
			addErrorToList(keyLineFontStyle);

		else if (!testInteger(fontStyle))
			addErrorToList(keyLineFontStyle, fontStyle, FONT_STYLE_VALUES);

		else {
			// Test pro rozsah:
			final int style = Integer.parseInt(fontStyle);

			// Hodnoty mohou být pouze 0, 1 nebo 2
			if (style < 0 || style > 2)
				addErrorToList(keyLineFontStyle, fontStyle, FONT_STYLE_VALUES);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro otestování, zda jsou pro atributy a metody, které se
	 * mohou zobrazit v buňce reprezentující instanci nastavenysprávné hodnoty,
	 * resp. validní hodnoty.
	 * 
	 * @param prop
	 *            - objekt Properties obsahují příslušné hodnoty, které se mají
	 *            zkontrolovat.
	 * 
	 * @param keyShow
	 *            - zda se mají příslušné hodnoty zobrazit.
	 * 
	 * @param keyShowAll
	 *            - zda se mají zobrazit všechny atributy nebo metody.
	 * 
	 * @param keyShowSpecificCount
	 *            - zda se má zobrazit určitý počet atributů nebo metod.
	 * 
	 * @param keyUseOneFontAndOneColor
	 *            - zda se má využít jeden font a barva písma pro atributy nebo
	 *            metody.
	 * 
	 * @param keyFontSize
	 *            - velikost písma pro atributy nebo metody.
	 * 
	 * @param keyFontStyle
	 *            - styl písma pro atributy nebo metody.
	 * 
	 * @param keyfontColor
	 *            - barva písma pro atributy nebo metody.
	 */
	private static void checkAttributesAndMethod(final Properties prop, final String keyShow,
			final String keyShowAll, final String keyShowSpecificCount, final String keyUseOneFontAndOneColor,
			final String keyFontSize, final String keyFontStyle, final String keyfontColor) {

		final String show = getValueFromProperties(keyShow, prop);

		if (show == null)
			addErrorToList(keyShow);

		else if (!testBoolean(show))
			addErrorToList(keyShow, show, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		final String showAll = getValueFromProperties(keyShowAll, prop);

		if (showAll == null)
			addErrorToList(keyShowAll);

		else if (!testBoolean(showAll))
			addErrorToList(keyShowAll, showAll, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		
		final String specificCount = getValueFromProperties(keyShowSpecificCount, prop);

		if (specificCount == null)
			addErrorToList(keyShowSpecificCount);

		else if (!testInteger(specificCount))
			addErrorToList(keyShowSpecificCount, specificCount, "<1 ; 20>");

		else {
			// Test pro rozsah:
			final int count = Integer.parseInt(specificCount);

			// Hodnoty mohou být pouze v intervalu: <1 ; 20>
			if (count < 1 || count > 20)
				addErrorToList(keyShowSpecificCount, specificCount, "<1 ; 20>");
		}
		
		
		
		
		
		
		final String useOneFontAndFontColor = getValueFromProperties(keyUseOneFontAndOneColor, prop);

		if (useOneFontAndFontColor == null)
			addErrorToList(keyUseOneFontAndOneColor);

		else if (!testBoolean(useOneFontAndFontColor))
			addErrorToList(keyUseOneFontAndOneColor, useOneFontAndFontColor, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		
		
		
		final String fontSize = getValueFromProperties(keyFontSize, prop);

		if (fontSize == null)
			addErrorToList(keyFontSize);

		else if (!testInteger(fontSize))
			addErrorToList(keyFontSize, fontSize, FONT_SIZE_VALUES);

		else {
			// Test pro rozsah:
			final int size = Integer.parseInt(fontSize);

			// Hodnoty mohou být pouze v intervalu: <8 ; 150>
			if (size < 8 || size > 150)
				addErrorToList(keyFontSize, fontSize, FONT_SIZE_VALUES);
		}
		
		
		
		
		
		
		
		
		final String fontStyle = getValueFromProperties(keyFontStyle, prop);

		if (fontStyle == null)
			addErrorToList(keyFontStyle);

		else if (!testInteger(fontStyle))
			addErrorToList(keyFontStyle, fontStyle, FONT_STYLE_VALUES);

		else {
			// Test pro rozsah:
			final int style = Integer.parseInt(fontStyle);

			// Hodnoty mohou být pouze 0, 1 nebo 2
			if (style < 0 || style > 2)
				addErrorToList(keyFontStyle, fontStyle, FONT_STYLE_VALUES);
		}
		
		
		
		
		
		
		
		
		final String fontColor = getValueFromProperties(keyfontColor, prop);

		if (fontColor == null)
			addErrorToList(keyfontColor);

		else if (!testColor(fontColor))
			addErrorToList(keyfontColor, fontColor, COLOR_ALLOWED_VALUES);
	}
}
