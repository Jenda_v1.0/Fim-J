package cz.uhk.fim.fimj.check_configuration_files;

import java.io.File;
import java.util.List;
import java.util.Properties;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.ConfigurationFiles;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.ReadFile;

/**
 * Tato třída slouží pouze pro kontrolu toho, zda jsou veškeré hodnoty v soubor OutputFrame.properites v pořádku. Tj.
 * zda tento soubor obsahuje veškeré požadované klíče a každý klíč obsahuje validní hodnoty.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

class CheckOutputFrameProp extends CheckConfigPropAbstract {

	/**
	 * Metoda, která slouží pro kontrolu, zda soubor OutputFrame.properties obsahuje
	 * příslušné klíče a k němu povolené hodnoty.
	 * 
	 * @return list, který může obsahovat chybová hlášení, tedy položky, které značí
	 *         nalezené chyby, nebo bude prázdný, pokud se žádná chyba nenajde.
	 */
	static List<ErrorInfo> checkOutputFrameProp() {
		ERRORS_LIST.clear();

		final Properties outputFrameProp = App.READ_FILE.getOutputFramePropertiesFromWorkspaceOnly();

		if (outputFrameProp == null) {
			/*
			 * K této části podmínky bych se nikdy neměl dostat, protože tato metoda se volá
			 * z ControlClass, ale až po tom, co se již otestovatlo, že adresář
			 * configuration ve workspace existuje, případně byl vytvořen, ale pouze pro
			 * případ, že by i tak došlo z mě neznámého důvodu k tomu, že by se ani tak ty
			 * soubory nenacházely ve workspace, tak jej zde zkusím vytvořit.
			 */

			/*
			 * Načtu si cestu k adresáři workspace.
			 */
			final String pathToWorkspace = ReadFile.getPathToWorkspace();

			acquireMutex();
			
			/*
			 * Pokud byla nalezena cesta k adresář workspace, pak pro jitotu znovu otestuji,
			 * zda existuje adresář configuration, ale už zde netestuji verzi, kdy
			 * neexistuje aby se vytvořil, protože by to nikdy nastat nemělo, a pokud
			 * tenadresář existuje, tak vytvořím pčříslušný soubor.
			 */
			if (pathToWorkspace != null
					&& ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
				ConfigurationFiles.writeOutputFrameProperties(
						pathToWorkspace + File.separator + Constants.PATH_TO_OUTPUT_FRAME_PROPERTIES);
			
			releaseMutex();
		}

		else
			checkValues(outputFrameProp);

		return ERRORS_LIST;
	}
	
	
	
	
	/**
	 * Metoda, která slouží pro otestování všech klíčů a jejich hodnot v souboru
	 * OutputFrame.properties (parametr prop).
	 * 
	 * @param prop
	 *            - objekt Properties (načtený soubor OutputFrame.properties), ve
	 *            kterém se mají zkontrolovat, zda ten objekt obsahuje veškeré klíče
	 *            s hodnotami, které by měl a ty hodnoty obsahuje "validní data".
	 */
	private static void checkValues(final Properties prop) {
	    // Text hodnot pro výpisy do editoru pro System.out výpisy:
		checkSimilarValuesInEditors(prop, "SO_FontStyle", "SO_FontSize", "SO_BackgroundColor", "SO_ForegroundColor");

        // Text hodnot pro výpisy do editoru pro System.err výpisy:
		checkSimilarValuesInEditors(prop, "SE_FontStyle", "SE_FontSize", "SE_BackgroundColor", "SE_ForegroundColor");







		final String clearTextInSoBeforeCallMethod = getValueFromProperties("ClearTextInSoBeforeCallMethod", prop);

		if (clearTextInSoBeforeCallMethod == null)
			addErrorToList("ClearTextInSoBeforeCallMethod");

		else if (!testBoolean(clearTextInSoBeforeCallMethod))
			addErrorToList("ClearTextInSoBeforeCallMethod", clearTextInSoBeforeCallMethod, BOOLEAN_ALLOWED_VALUES);






        final String clearTextInSoBeforeCallConstructor = getValueFromProperties("ClearTextInSoBeforeCallConstructor", prop);

        if (clearTextInSoBeforeCallConstructor == null)
            addErrorToList("ClearTextInSoBeforeCallConstructor");

        else if (!testBoolean(clearTextInSoBeforeCallConstructor))
            addErrorToList("ClearTextInSoBeforeCallConstructor", clearTextInSoBeforeCallConstructor, BOOLEAN_ALLOWED_VALUES);






        final String recordCallMethodsInSo = getValueFromProperties("RecordCallMethodsInSo", prop);

        if (recordCallMethodsInSo == null)
            addErrorToList("RecordCallMethodsInSo");

        else if (!testBoolean(recordCallMethodsInSo))
            addErrorToList("RecordCallMethodsInSo", recordCallMethodsInSo, BOOLEAN_ALLOWED_VALUES);






        final String recordCallConstructorsInSo = getValueFromProperties("RecordCallConstructorsInSo", prop);

        if (recordCallConstructorsInSo == null)
            addErrorToList("RecordCallConstructorsInSo");

        else if (!testBoolean(recordCallConstructorsInSo))
            addErrorToList("RecordCallConstructorsInSo", recordCallConstructorsInSo, BOOLEAN_ALLOWED_VALUES);
	}
}
