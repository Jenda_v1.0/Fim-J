package cz.uhk.fim.fimj.check_configuration_files;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import cz.uhk.fim.fimj.class_diagram.KindOfCodeEditor;

/**
 * Tato třída slouží pro kontrolou hodnot, které je možné zadat do konfiguračního souboru (.properties), který obsahuje
 * nastavení pro diagramu tříd, tedy soubor ClassDiagram.properties
 *
 * @author Jan Krunčík
 * @version 1.0
 */

class CheckClassDiagramProp extends CheckConfigPropAbstract {

	/**
	 * Povolené hodnoty styl hrany.
	 */
	private static final String LINE_STYLE_VALUES = "'-1', '11', '12', '13'";

	/**
	 * Regulární výraz pro rozpoznání syntaxe pro nastavení vzoru čerchované čáry
	 * pro implementaci rozhraní.
	 */
	private static final String REG_EX_IMPLEMENTATION_EDGE_PATTERN = "^\\s*\\[\\s*\\d+\\s*(,\\s*\\d+\\s*){0,3}\\s*\\]\\s*$";

	/**
	 * Povolené hodnoty pro styl čerchované hrany pro implementaci rozhraní.
	 */
	private static final String IMPLEMENTATION_LINE_PATTERN_VALUES = "[10, 2, 2, 2], [10, 10], [5], [12]";


	
	
	// Modely pro čerchovanou čáru u implementace rozhraní:
	private static final float[] IMPLEMENTS_PATTERN_1 = { 10f, 2f, 2f, 2f };
	private static final float[] IMPLEMENTS_PATTERN_2 = { 10f, 10f };
	private static final float[] IMPLEMENTS_PATTERN_3 = { 5f };
	private static final float[] IMPLEMENTS_PATTERN_4 = { 12f };
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro kontrolu, zda jsou veškeré hodnoty v souboru
	 * ClassDiagram.properties v pořádku.
	 * 
	 * Tato metoda načte soubor ClassDiagramProperties a zavolá metoda, která
	 * otestuje konkrétní hodnoty.
	 * 
	 * @param classDiagramProp
	 *            - objekt Properties obsahující načtený soubor
	 *            ClassDiagram.properties
	 * 
	 * @return list, který slouží pro zobrazení nalezených chyb, dle jeho velikosti
	 *         se pozná, zda byla nějaká chyba nalezena nebo ne.
	 */
	static List<ErrorInfo> checkClassDiagramPropFile(final Properties classDiagramProp) {
		ERRORS_LIST.clear();

		// Podmínka by měla být zbytečná, ale kdyby náhodou:
		if (classDiagramProp != null)
			checkValues(classDiagramProp);

		return ERRORS_LIST;
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro otestování veškerých hodnot v souboru
	 * ClassDiagram.properties.
	 * 
	 * @param prop
	 *            objekt Properties, což je načtený soubor ClassDiagram.properties,
	 *            jehož hodnoty se mají zkontrolovat.
	 */
	private static void checkValues(final Properties prop) {
		/*
		 * Následují základní vlastnosti pro diagram tříd:
		 */

		// Barva pozadí diagramu:
		final String bgColor = getValueFromProperties("BackgroundColor", prop);

		if (bgColor == null)
			addErrorToList("BackgroundColor");

		else if (!testColor(bgColor))
			addErrorToList("BackgroundColor", bgColor, COLOR_ALLOWED_VALUES);
		
		
		
		
		
		
		// Úroveň přiblížení diagramu:
		final String cdScale = getValueFromProperties("ClassDiagramScale", prop);

		if (cdScale == null)
			addErrorToList("ClassDiagramScale");

		else if (!testDouble(cdScale))
			addErrorToList("ClassDiagramScale", cdScale, DIAGRAM_SCALE_ALLOWED_VALUES);

		else {
			// Test pro rozsah:
			final double dScale = Double.parseDouble(cdScale);

			if (dScale < 0.5d || dScale > 20.0d)
				addErrorToList("ClassDiagramScale", cdScale, DIAGRAM_SCALE_ALLOWED_VALUES);
		}





		final String showStaticInheritedClassNameWithPackages = getValueFromProperties("ShowStaticInheritedClassNameWithPackages", prop);

		if (showStaticInheritedClassNameWithPackages == null)
			addErrorToList("ShowStaticInheritedClassNameWithPackages");

		else if (!testBoolean(showStaticInheritedClassNameWithPackages))
			addErrorToList("ShowStaticInheritedClassNameWithPackages", showStaticInheritedClassNameWithPackages, BOOLEAN_ALLOWED_VALUES);







		final String includePrivateConstructors = getValueFromProperties("IncludePrivateConstructors", prop);

		if (includePrivateConstructors == null)
			addErrorToList("IncludePrivateConstructors");

		else if (!testBoolean(includePrivateConstructors))
			addErrorToList("IncludePrivateConstructors", includePrivateConstructors, BOOLEAN_ALLOWED_VALUES);



		final String includeProtectedConstructors = getValueFromProperties("IncludeProtectedConstructors", prop);

		if (includeProtectedConstructors == null)
			addErrorToList("IncludeProtectedConstructors");

		else if (!testBoolean(includeProtectedConstructors))
			addErrorToList("IncludeProtectedConstructors", includeProtectedConstructors, BOOLEAN_ALLOWED_VALUES);



		final String includePackagePrivateConstructors = getValueFromProperties("IncludePackagePrivateConstructors", prop);

		if (includePackagePrivateConstructors == null)
			addErrorToList("IncludePackagePrivateConstructors");

		else if (!testBoolean(includePackagePrivateConstructors))
			addErrorToList("IncludePackagePrivateConstructors", includePackagePrivateConstructors, BOOLEAN_ALLOWED_VALUES);






		final String includePrivateInnerStaticClasses = getValueFromProperties("IncludePrivateInnerStaticClasses", prop);

		if (includePrivateInnerStaticClasses == null)
			addErrorToList("IncludePrivateInnerStaticClasses");

		else if (!testBoolean(includePrivateInnerStaticClasses))
			addErrorToList("IncludePrivateInnerStaticClasses", includePrivateInnerStaticClasses, BOOLEAN_ALLOWED_VALUES);


		final String includeProtectedInnerStaticClasses = getValueFromProperties("IncludeProtectedInnerStaticClasses", prop);

		if (includeProtectedInnerStaticClasses == null)
			addErrorToList("IncludeProtectedInnerStaticClasses");

		else if (!testBoolean(includeProtectedInnerStaticClasses))
			addErrorToList("IncludeProtectedInnerStaticClasses", includeProtectedInnerStaticClasses, BOOLEAN_ALLOWED_VALUES);


		final String includePackagePrivateInnerStaticClasses = getValueFromProperties("IncludePackagePrivateInnerStaticClasses", prop);

		if (includePackagePrivateInnerStaticClasses == null)
			addErrorToList("IncludePackagePrivateInnerStaticClasses");

		else if (!testBoolean(includePackagePrivateInnerStaticClasses))
			addErrorToList("IncludePackagePrivateInnerStaticClasses", includePackagePrivateInnerStaticClasses, BOOLEAN_ALLOWED_VALUES);






		final String innerStaticClassesIncludePrivateStaticMethods = getValueFromProperties("InnerStaticClassesIncludePrivateStaticMethods", prop);

		if (innerStaticClassesIncludePrivateStaticMethods == null)
			addErrorToList("InnerStaticClassesIncludePrivateStaticMethods");

		else if (!testBoolean(innerStaticClassesIncludePrivateStaticMethods))
			addErrorToList("InnerStaticClassesIncludePrivateStaticMethods", innerStaticClassesIncludePrivateStaticMethods, BOOLEAN_ALLOWED_VALUES);


		final String innerStaticClassesIncludeProtectedStaticMethods = getValueFromProperties("InnerStaticClassesIncludeProtectedStaticMethods", prop);

		if (innerStaticClassesIncludeProtectedStaticMethods == null)
			addErrorToList("InnerStaticClassesIncludeProtectedStaticMethods");

		else if (!testBoolean(innerStaticClassesIncludeProtectedStaticMethods))
			addErrorToList("InnerStaticClassesIncludeProtectedStaticMethods", innerStaticClassesIncludeProtectedStaticMethods, BOOLEAN_ALLOWED_VALUES);


		final String innerStaticClassesIncludePackagePrivateStaticMethods = getValueFromProperties("InnerStaticClassesIncludePackagePrivateStaticMethods", prop);

		if (innerStaticClassesIncludePackagePrivateStaticMethods == null)
			addErrorToList("InnerStaticClassesIncludePackagePrivateStaticMethods");

		else if (!testBoolean(innerStaticClassesIncludePackagePrivateStaticMethods))
			addErrorToList("InnerStaticClassesIncludePackagePrivateStaticMethods", innerStaticClassesIncludePackagePrivateStaticMethods, BOOLEAN_ALLOWED_VALUES);





		
		final String includePrivateMethods = getValueFromProperties("IncludePrivateMethods", prop);

		if (includePrivateMethods == null)
			addErrorToList("IncludePrivateMethods");

		else if (!testBoolean(includePrivateMethods))
			addErrorToList("IncludePrivateMethods", includePrivateMethods, BOOLEAN_ALLOWED_VALUES);


		final String includeProtectedMethods = getValueFromProperties("IncludeProtectedMethods", prop);

		if (includeProtectedMethods == null)
			addErrorToList("IncludeProtectedMethods");

		else if (!testBoolean(includeProtectedMethods))
			addErrorToList("IncludeProtectedMethods", includeProtectedMethods, BOOLEAN_ALLOWED_VALUES);


		final String includePackagePrivateMethods = getValueFromProperties("IncludePackagePrivateMethods", prop);

		if (includePackagePrivateMethods == null)
			addErrorToList("IncludePackagePrivateMethods");

		else if (!testBoolean(includePackagePrivateMethods))
			addErrorToList("IncludePackagePrivateMethods", includePackagePrivateMethods, BOOLEAN_ALLOWED_VALUES);



		
		
		
		final String makeFiledsAvailableForConstructor = getValueFromProperties("MakeFieldsAvailableForConstructor",
				prop);

		if (makeFiledsAvailableForConstructor == null)
			addErrorToList("MakeFieldsAvailableForConstructor");

		else if (!testBoolean(makeFiledsAvailableForConstructor))
			addErrorToList("MakeFieldsAvailableForConstructor", makeFiledsAvailableForConstructor,
					BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		final String makeFieldsAvailableForMethod = getValueFromProperties("MakeFieldsAvailableForMethod", prop);

		if (makeFieldsAvailableForMethod == null)
			addErrorToList("MakeFieldsAvailableForMethod");

		else if (!testBoolean(makeFieldsAvailableForMethod))
			addErrorToList("MakeFieldsAvailableForMethod", makeFieldsAvailableForMethod, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		final String edgeLabelMovable = getValueFromProperties("EdgeLabelMovable", prop);

		if (edgeLabelMovable == null)
			addErrorToList("EdgeLabelMovable");

		else if (!testBoolean(edgeLabelMovable))
			addErrorToList("EdgeLabelMovable", edgeLabelMovable, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		final String showCreateNewInstanceOptionInCallConstructor = getValueFromProperties(
				"ShowCreateNewInstanceOptionInCallConstructor", prop);

		if (showCreateNewInstanceOptionInCallConstructor == null)
			addErrorToList("ShowCreateNewInstanceOptionInCallConstructor");

		else if (!testBoolean(showCreateNewInstanceOptionInCallConstructor))
			addErrorToList("ShowCreateNewInstanceOptionInCallConstructor", showCreateNewInstanceOptionInCallConstructor,
					BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		final String showCreateNewInstanceOptionInCallMethod = getValueFromProperties(
				"ShowCreateNewInstanceOptionInCallMethod", prop);

		if (showCreateNewInstanceOptionInCallMethod == null)
			addErrorToList("ShowCreateNewInstanceOptionInCallMethod");

		else if (!testBoolean(showCreateNewInstanceOptionInCallMethod))
			addErrorToList("ShowCreateNewInstanceOptionInCallMethod", showCreateNewInstanceOptionInCallMethod,
					BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		final String makeMethodsAvailableForCallMethod = getValueFromProperties("MakeMethodsAvailableForCallMethod",
				prop);

		if (makeMethodsAvailableForCallMethod == null)
			addErrorToList("MakeMethodsAvailableForCallMethod");

		else if (!testBoolean(makeMethodsAvailableForCallMethod))
			addErrorToList("MakeMethodsAvailableForCallMethod", makeMethodsAvailableForCallMethod,
					BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		final String MakeMethodsAvailableForCallConstructor = getValueFromProperties(
				"MakeMethodsAvailableForCallConstructor", prop);

		if (MakeMethodsAvailableForCallConstructor == null)
			addErrorToList("MakeMethodsAvailableForCallConstructor");

		else if (!testBoolean(MakeMethodsAvailableForCallConstructor))
			addErrorToList("MakeMethodsAvailableForCallConstructor", MakeMethodsAvailableForCallConstructor,
					BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		final String showRelationshipsToClassItself = getValueFromProperties("ShowRelationsShipsToClassItself", prop);

		if (showRelationshipsToClassItself == null)
			addErrorToList("ShowRelationsShipsToClassItself");

		else if (!testBoolean(showRelationshipsToClassItself))
			addErrorToList("ShowRelationsShipsToClassItself", showRelationshipsToClassItself, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		final String showAssociationThroughAggregation = getValueFromProperties("ShowAssociationThroughAggregation",
				prop);

		if (showAssociationThroughAggregation == null)
			addErrorToList("ShowAssociationThroughAggregation");

		else if (!testBoolean(showAssociationThroughAggregation))
			addErrorToList("ShowAssociationThroughAggregation", showAssociationThroughAggregation,
					BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		final String defaultSourceCodeEditor = getValueFromProperties("DefaultSourceCodeEditor", prop);

		if (defaultSourceCodeEditor == null)
			addErrorToList("DefaultSourceCodeEditor");

		else if (getKindOfEditorByTextValue(defaultSourceCodeEditor) == null)
			addErrorToList("DefaultSourceCodeEditor", defaultSourceCodeEditor,
					KindOfCodeEditor.SOURCE_CODE_EDITOR.getValue() + ", "
							+ KindOfCodeEditor.PROJECT_EXPLORER.getValue());
		
		
		
		
		final String addInterfaceMethodsToClass = getValueFromProperties("AddInterfaceMethodsToClass", prop);

		if (addInterfaceMethodsToClass == null)
			addErrorToList("AddInterfaceMethodsToClass");

		else if (!testBoolean(addInterfaceMethodsToClass))
			addErrorToList("AddInterfaceMethodsToClass", addInterfaceMethodsToClass, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		
		
		
		
		
		/*
		 * Následují hodnoty pro buňku, která v diagramu tříd reprezentuje třídu.
		 */

		final String classFontStyle = getValueFromProperties("ClassFontType", prop);

		if (classFontStyle == null)
			addErrorToList("ClassFontType");

		else if (!testInteger(classFontStyle))
			addErrorToList("ClassFontType", classFontStyle, FONT_STYLE_VALUES);

		else {
			// Test pro rozsah:
			final int classStyle = Integer.parseInt(classFontStyle);

			// Hodnoty mohou být pouze 0, 1 nebo 2
			if (classStyle < 0 || classStyle > 2)
				addErrorToList("ClassFontType", classFontStyle, FONT_STYLE_VALUES);
		}
		
		
		
		
		
		final String classFontSize = getValueFromProperties("ClassFontSize", prop);

		if (classFontSize == null)
			addErrorToList("ClassFontSize");

		else if (!testInteger(classFontSize))
			addErrorToList("ClassFontSize", classFontSize, FONT_SIZE_VALUES);

		else {
			// Test pro rozsah:
			final int classSize = Integer.parseInt(classFontSize);

			// Hodnoty mohou být pouze v intervalu: <8 ; 150>
			if (classSize < 8 || classSize > 150)
				addErrorToList("ClassFontSize", classFontSize, FONT_SIZE_VALUES);
		}
		
		
		
		
		
		// vertikální zarovnání:
		final String classVerticalAlignment = getValueFromProperties("VerticalAlignment", prop);

		if (classVerticalAlignment == null)
			addErrorToList("VerticalAlignment");

		else if (!testInteger(classVerticalAlignment))
			addErrorToList("VerticalAlignment", classVerticalAlignment, VERTICAL_ALIGNMENT_VALUES);

		else {
			// Test pro rozsah:
			final int verticalAlignment = Integer.parseInt(classVerticalAlignment);

			if (verticalAlignment != 0 && verticalAlignment != 1 && verticalAlignment != 3)
				addErrorToList("VerticalAlignment", classVerticalAlignment, VERTICAL_ALIGNMENT_VALUES);
		}
		
		
		
		
		
		
		// Horizontální zarovnání:
		final String classHorizontalAlignment = getValueFromProperties("HorizontalAlignment", prop);

		if (classHorizontalAlignment == null)
			addErrorToList("HorizontalAlignment");

		else if (!testInteger(classHorizontalAlignment))
			addErrorToList("HorizontalAlignment", classHorizontalAlignment, HORIZONTAL_ALIGNMENT_VALUES);

		else {
			final int horizontalAlignment = Integer.parseInt(classHorizontalAlignment);

			if (horizontalAlignment != 0 && horizontalAlignment != 2 && horizontalAlignment != 4)
				addErrorToList("HorizontalAlignment", classHorizontalAlignment, HORIZONTAL_ALIGNMENT_VALUES);
		}
		
		
		
		
		
		// Barva textu:
		final String foregroundColor = getValueFromProperties("ForegroundColor", prop);

		if (foregroundColor == null)
			addErrorToList("ForegroundColor");

		else if (!testColor(foregroundColor))
			addErrorToList("ForegroundColor", foregroundColor, COLOR_ALLOWED_VALUES);
		
		
		
		
		// Barva bunky od praveho dolniho rohu:
		final String gradiendColor = getValueFromProperties("GradientColor", prop);

		if (gradiendColor == null)
			addErrorToList("GradientColor");

		else if (!testColor(gradiendColor))
			addErrorToList("GradientColor", gradiendColor, COLOR_ALLOWED_VALUES);
		
		
		
		
		// Barva bunky od leveho horního rohu:
		final String classBackgroundColor = getValueFromProperties("ClassBackgroundColor", prop);

		if (classBackgroundColor == null)
			addErrorToList("ClassBackgroundColor");

		else if (!testColor(classBackgroundColor))
			addErrorToList("ClassBackgroundColor", classBackgroundColor, COLOR_ALLOWED_VALUES);
		
		
		
		
		// Zda má být buňka průhledná:
		final String classCellOpaque = getValueFromProperties("ClassCellOpaque", prop);

		if (classCellOpaque == null)
			addErrorToList("ClassCellOpaque");

		else if (!testBoolean(classCellOpaque))
			addErrorToList("ClassCellOpaque", classCellOpaque, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		// Barva pozadi tridy, kdyz je jedna barva
		final String classOneBgColor = getValueFromProperties("ClassOneBackgroundColor", prop);

		if (classOneBgColor == null)
			addErrorToList("ClassOneBackgroundColor");

		else if (!testColor(classOneBgColor))
			addErrorToList("ClassOneBackgroundColor", classOneBgColor, COLOR_ALLOWED_VALUES);
		
		
		
		
		
		// Zda má mít buňka reprezentující třídu v diagramu tříd jen jednu barvu nebo
		// ne:
		final String classOneBgColorBoolean = getValueFromProperties("ClassOneBgColorBoolean", prop);

		if (classOneBgColorBoolean == null)
			addErrorToList("ClassOneBgColorBoolean");

		else if (!testBoolean(classOneBgColorBoolean))
			addErrorToList("ClassOneBgColorBoolean", classOneBgColorBoolean, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		/*
		 * hodnoty pro buňku, která v diagramu tříd reprezentuje komentář.
		 */
		
		final String commentFontStyle = getValueFromProperties("CommentFontType", prop);

		if (commentFontStyle == null)
			addErrorToList("CommentFontType");

		else if (!testInteger(commentFontStyle))
			addErrorToList("CommentFontType", commentFontStyle, FONT_STYLE_VALUES);

		else {
			// Test pro rozsah:
			final int commentStyle = Integer.parseInt(commentFontStyle);

			// Hodnoty mohou být pouze 0, 1 nebo 2
			if (commentStyle < 0 || commentStyle > 2)
				addErrorToList("CommentFontType", commentFontStyle, FONT_STYLE_VALUES);
		}
		
		
		
		
		// Velikost písma pro komentář:
		final String commentFontSize = getValueFromProperties("CommentFontSize", prop);

		if (commentFontSize == null)
			addErrorToList("CommentFontSize");

		else if (!testInteger(commentFontSize))
			addErrorToList("CommentFontSize", commentFontSize, FONT_SIZE_VALUES);

		else {
			// Test pro rozsah:
			final int commentSize = Integer.parseInt(commentFontSize);

			// Hodnoty mohou být pouze v intervalu: <8 ; 150>
			if (commentSize < 8 || commentSize > 150)
				addErrorToList("CommentFontSize", commentFontSize, FONT_SIZE_VALUES);
		}
		
		
		
		
		
		// vertikální zarovnání:
		final String commentVerticalAlignment = getValueFromProperties("CommentVerticalAlignment", prop);

		if (commentVerticalAlignment == null)
			addErrorToList("CommentVerticalAlignment");

		else if (!testInteger(commentVerticalAlignment))
			addErrorToList("CommentVerticalAlignment", commentVerticalAlignment, VERTICAL_ALIGNMENT_VALUES);

		else {
			// Test pro rozsah:
			final int verticalAlignment = Integer.parseInt(commentVerticalAlignment);

			if (verticalAlignment != 0 && verticalAlignment != 1 && verticalAlignment != 3)
				addErrorToList("CommentVerticalAlignment", commentVerticalAlignment, VERTICAL_ALIGNMENT_VALUES);
		}
		
		
		
		
		
		
		// Horizontální zarovnání:
		final String commentHorizontalAlignment = getValueFromProperties("CommentHorizontalAlignment", prop);

		if (commentHorizontalAlignment == null)
			addErrorToList("CommentHorizontalAlignment");

		else if (!testInteger(commentHorizontalAlignment))
			addErrorToList("CommentHorizontalAlignment", commentHorizontalAlignment, HORIZONTAL_ALIGNMENT_VALUES);

		else {
			final int horizontalAlignment = Integer.parseInt(commentHorizontalAlignment);

			if (horizontalAlignment != 0 && horizontalAlignment != 2 && horizontalAlignment != 4)
				addErrorToList("CommentHorizontalAlignment", commentHorizontalAlignment, HORIZONTAL_ALIGNMENT_VALUES);
		}
		
		
		
		
		
		// Barva písma:
		final String commentForegroundColor = getValueFromProperties("CommentForegroundColor", prop);

		if (commentForegroundColor == null)
			addErrorToList("CommentForegroundColor");

		else if (!testColor(commentForegroundColor))
			addErrorToList("CommentForegroundColor", commentForegroundColor, COLOR_ALLOWED_VALUES);
		
		
		
		
		// Barva pozadí:
		final String commentBackgroundColor = getValueFromProperties("CommentBackgroundColor", prop);

		if (commentBackgroundColor == null)
			addErrorToList("CommentBackgroundColor");

		else if (!testColor(commentBackgroundColor))
			addErrorToList("CommentBackgroundColor", commentBackgroundColor, COLOR_ALLOWED_VALUES);
		
		
		
		
		// Barva od pravého spodního rohu:
		final String commentGradientColor = getValueFromProperties("CommentGradientColor", prop);

		if (commentGradientColor == null)
			addErrorToList("CommentGradientColor");

		else if (!testColor(commentGradientColor))
			addErrorToList("CommentGradientColor", commentGradientColor, COLOR_ALLOWED_VALUES);
		
		
		
		
		// Zda bude buňka průhledná:
		final String commentCellOpaque = getValueFromProperties("CommentCellOpaque", prop);

		if (commentCellOpaque == null)
			addErrorToList("CommentCellOpaque");

		else if (!testBoolean(commentCellOpaque))
			addErrorToList("CommentCellOpaque", commentCellOpaque, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		// Barva pro pozadí komentáře, pokud je jen jedna barva pro buňku komentáře:
		final String commentOneBgColor = getValueFromProperties("CommentOneBackgroundColor", prop);

		if (commentOneBgColor == null)
			addErrorToList("CommentOneBackgroundColor");

		else if (!testColor(commentOneBgColor))
			addErrorToList("CommentOneBackgroundColor", commentOneBgColor, COLOR_ALLOWED_VALUES);
		
		
		
		
		
		// Zda se má zobrazit jedna nebo dvě barvy pro pozadí buňky komentáře:
		final String commentOneBgColorBoolean = getValueFromProperties("CommentOneBgColorBoolean", prop);

		if (commentOneBgColorBoolean == null)
			addErrorToList("CommentOneBgColorBoolean");

		else if (!testBoolean(commentOneBgColorBoolean))
			addErrorToList("CommentOneBgColorBoolean", commentOneBgColorBoolean, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		
		// vlastnosti pro hranu komentare:
		checkEdgesProperties(prop, "CommentEdgeLineFontStyle", "CommentEdgeLineFontSize", "CommentEdgeColor",
				"CommentEdgeTextColor", "CommentEdgeLabelAlongEdge", "CommentEdgeLineStyle", "CommentEdgeLineEnd",
				"CommentEdgeLineBegin", "CommentEdgeLineWidth", "CommentEdgeEndFill", "CommentEdgeBeginFill");

		
		
		
		// vlastnosti pro hranu reprezentující vztah asociaci (symetrickou):
		checkEdgesProperties(prop, "AscEdgeLineFontStyle", "AscEdgeLineFontSize", "AscEdgeLineColor",
				"AscEdgeLineTextColor", "AscEdgeLabelAlongEdge", "AscEdgeLineStyle", "AscEdgeLineEnd",
				"AscEdgeLineBegin", "AscEdgeLineWidth", "AscEdgeEndFill", "AscEdgeBeginFill");

		
		
		
		// Hrana pro dědičnost:
		checkEdgesProperties(prop, "ExtEdgeLineFontStyle", "ExtEdgeLineFontSize", "ExtEdgeLineColor",
				"ExtEdgeLineTextColor", "ExtEdgeLabelAlongEdge", "ExtEdgeLineStyle", "ExtEdgeLineEnd",
				"ExtEdgeLineBegin", "ExtEdgeLineWidth", "ExtEdgeEndLineFill", "ExtEdgeBeginLineFill");

		
		
		
		// agregace 1 : 1 neboli asymetrická asociace:
		checkEdgesProperties(prop, "AgrEdgeLineFontStyle", "AgrEdgeLineFontSize", "AgrEdgeLineColor",
				"AgrEdgeLineTextColor", "AgrEdgeLabelAlongEdge", "AgrEdgeLineStyle", "AgrEdgeLineEnd",
				"AgrEdgeLineBegin", "AgrEdgeLineWidth", "AgrEdgeEndLineFill", "AgrEdgeBeginLineFill");

		
		
		// agregace 1 : N
		checkEdgesProperties(prop, "Agr_1_N_EdgeLineFontStyle", "Agr_1_N_EdgeLineFontSize", "Agr_1_N_EdgeLineColor",
				"Agr_1_N_EdgeLineTextColor", "Agr_1_N_EdgeLabelAlongEdge", "Agr_1_N_EdgeLineStyle",
				"Agr_1_N_EdgeLineEnd", "Agr_1_N_EdgeLineBegin", "Agr_1_N_EdgeLineWidth", "Agr_1_N_EdgeEndLineFill",
				"Agr_1_N_EdgeBeginLineFill");
		
		
		
		
		
		
		// Hrana pro implementaci rozhraní:
		checkEdgesProperties(prop, "ImpEdgeLineFontStyle", "ImpEdgeLineFontSize", "ImpEdgeLineColor",
				"ImpEdgeLineTextColor", "ImpEdgeLabelAlongEdge", "ImpEdgeLineStyle", "ImpEdgeLineEnd",
				"ImpEdgeLineBegin", "ImpEdgeLineWidth", "ImpEdgeEndLineFill", "ImpEdgeBeginLineFill");
		
		
		/*
		 * Zadaný text v konfiguračním souboru:
		 */
		final String pattern = getValueFromProperties("ImpEdgeDashPattern", prop);

		// Zdabyl načten:
		if (pattern == null)
			addErrorToList("ImpEdgeDashPattern");
		
		
		// Zda splňuje regulární výraz pro čísla:
		else if (!pattern.trim().matches(REG_EX_IMPLEMENTATION_EDGE_PATTERN))
			addErrorToList("ImpEdgeDashPattern", pattern, IMPLEMENTATION_LINE_PATTERN_VALUES);

		else {
			/*
			 * Zadaný text, který by již měl odpovídat regulárnímu vrazu pro syntaxi
			 * několika málo čísel v hranatých závorkách, zde odeberu ty hranaté závorky na
			 * konci.
			 */
			final String patternText = pattern.trim().substring(1, pattern.length() - 1);

			// pole čísel typu string:
			final String[] partsOfPattern = patternText.trim().replace("\\s", "").split(",");

			/*
			 * Pole pro vložení čísel typu float.
			 */
			final float[] patternArray = new float[partsOfPattern.length];

			for (int i = 0; i < partsOfPattern.length; i++)
				patternArray[i] = Float.parseFloat(partsOfPattern[i]);

			/*
			 * Test, zda jsou ty pole stejná.
			 */
			if (!Arrays.equals(patternArray, IMPLEMENTS_PATTERN_1) && !Arrays.equals(patternArray, IMPLEMENTS_PATTERN_2)
					&& !Arrays.equals(patternArray, IMPLEMENTS_PATTERN_3)
					&& !Arrays.equals(patternArray, IMPLEMENTS_PATTERN_4))
				addErrorToList("ImpEdgeDashPattern", pattern, IMPLEMENTATION_LINE_PATTERN_VALUES);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro otestování vlastností pro hrany, které se mohou
	 * vyskytovat v diagramu tříd, tj. hrany, které repreztnují vztyh typu
	 * implementace rozhraní, dědičnost, obě asociace a agregace 1 : N a hranu pro
	 * spojení třídy s komentářem.
	 * 
	 * @param prop
	 *            - objekt Properties, který obsahuje načtený konfigurační soubor
	 *            ClassDiagram.properties, aby se otestovali jeho nastavené hodnoty,
	 *            zda jsou v "povolených rozmězích".
	 * 
	 * @param keyEdgeLineFontStyle
	 *            - klíč pro vlastnost stylu písma příslušné hrany.
	 * 
	 * @param keyEdgeLineFontSize
	 *            - klíč pro vlastnost velikosti písma příslušné hrany.
	 * 
	 * @param keyEdgeColor
	 *            - klíč pro vlastnost barvu příslušné hrany
	 * 
	 * @param keyFontColor
	 *            - klíč pro vlastnost barvy písma příslušné hrany.
	 * 
	 * @param keyEdgeLabelAlongEdge
	 *            - klíč pro vlastnost, zda mají být popisky na hraně umístěny podél
	 *            hrany.
	 * 
	 * @param keyEdgeLineStyle
	 *            - klíč pro vlasnost stylu příslušné hrany.
	 * 
	 * @param keyEdgeLineEnd
	 *            - klíč pro index stylu konce hrany - resp. jaký znak má být na
	 *            konci hrany.
	 * 
	 * @param keyEdgeLineBegin
	 *            - klíč pro index stylu začátku hrany - resp. jaký znak má být na
	 *            začátku hrany.
	 * 
	 * @param keyEdgeLineWidth
	 *            - klíč pro šířku příslušné hrany.
	 * 
	 * @param keyEdgeEndFill
	 *            - klíč pro styl konce přísušné hrany - symbol na konci hrany.
	 * 
	 * @param keyEdgeBeginFill-
	 *            klíč pro styl začátku přísušné hrany - symbol na začátku hrany.
	 */
	private static void checkEdgesProperties(final Properties prop, final String keyEdgeLineFontStyle,
			final String keyEdgeLineFontSize, final String keyEdgeColor, final String keyFontColor,
			final String keyEdgeLabelAlongEdge, final String keyEdgeLineStyle, final String keyEdgeLineEnd,
			final String keyEdgeLineBegin, final String keyEdgeLineWidth, final String keyEdgeEndFill,
			final String keyEdgeBeginFill) {
		
		// styl písma
		final String fontStyle = getValueFromProperties(keyEdgeLineFontStyle, prop);

		if (fontStyle == null)
			addErrorToList(keyEdgeLineFontStyle);

		else if (!testInteger(fontStyle))
			addErrorToList(keyEdgeLineFontStyle, fontStyle, FONT_STYLE_VALUES);

		else {
			// Test pro rozsah:
			final int style = Integer.parseInt(fontStyle);

			// Hodnoty mohou být pouze 0, 1 nebo 2
			if (style < 0 || style > 2)
				addErrorToList(keyEdgeLineFontStyle, fontStyle, FONT_STYLE_VALUES);
		}
		
		
		
		
		// velikost písma:
		final String fontSize = getValueFromProperties(keyEdgeLineFontSize, prop);

		if (fontSize == null)
			addErrorToList(keyEdgeLineFontSize);

		else if (!testInteger(fontSize))
			addErrorToList(keyEdgeLineFontSize, fontSize, FONT_SIZE_VALUES);

		else {
			// Test pro rozsah:
			final int size = Integer.parseInt(fontSize);

			// Hodnoty mohou být pouze v intervalu: <8 ; 150>
			if (size < 8 || size > 150)
				addErrorToList(keyEdgeLineFontSize, fontSize, FONT_SIZE_VALUES);
		}
		
		
		
		
		// Barva hrany:
		final String edgeClr = getValueFromProperties(keyEdgeColor, prop);

		if (edgeClr == null)
			addErrorToList(keyEdgeColor);

		else if (!testColor(edgeClr))
			addErrorToList(keyEdgeColor, edgeClr, COLOR_ALLOWED_VALUES);
		
		
		
		
		
		// Barva písma:
		final String fontColor = getValueFromProperties(keyFontColor, prop);

		if (fontColor == null)
			addErrorToList(keyFontColor);

		else if (!testColor(fontColor))
			addErrorToList(keyFontColor, fontColor, COLOR_ALLOWED_VALUES);
		
		
		
		
		// Popisky hran podél hrany:
		final String edgeLabelAlongEdge = getValueFromProperties(keyEdgeLabelAlongEdge, prop);

		if (edgeLabelAlongEdge == null)
			addErrorToList(keyEdgeLabelAlongEdge);

		else if (!testBoolean(edgeLabelAlongEdge))
			addErrorToList(keyEdgeLabelAlongEdge, edgeLabelAlongEdge, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		
		// Styl hrany:
		final String edgeLineStyle = getValueFromProperties(keyEdgeLineStyle, prop);

		if (edgeLineStyle == null)
			addErrorToList(keyEdgeLineStyle);

		else if (!testInteger(edgeLineStyle))
			addErrorToList(keyEdgeLineStyle, edgeLineStyle, LINE_STYLE_VALUES);

		else {
			// Test pro rozsah:
			final int lineStyle = Integer.parseInt(edgeLineStyle);

			if (lineStyle != -1 && (lineStyle < 11 || lineStyle > 13))
				addErrorToList(keyEdgeLineStyle, edgeLineStyle, LINE_STYLE_VALUES);
		}
		
		
		
		
			
		
		
		// Konec hrany:
		final String edgeLineEnd = getValueFromProperties(keyEdgeLineEnd, prop);

		if (edgeLineEnd == null)
			addErrorToList(keyEdgeLineEnd);

		else if (!testInteger(edgeLineEnd))
			addErrorToList(keyEdgeLineEnd, edgeLineEnd, LINE_BEGIN_END_VALUES);

		else {
			// Test pro rozsah:
			final int lineEnd = Integer.parseInt(edgeLineEnd);

			if (lineEnd != 0 && lineEnd != 1 && lineEnd != 2 && lineEnd != 4 && lineEnd != 5 && lineEnd != 7
					&& lineEnd != 8 && lineEnd != 9)
				addErrorToList(keyEdgeLineEnd, edgeLineEnd, LINE_BEGIN_END_VALUES);
		}
		
		
		
		
		
		
		// Začátek hrany:
		final String edgeLineBegin = getValueFromProperties(keyEdgeLineBegin, prop);

		if (edgeLineBegin == null)
			addErrorToList(keyEdgeLineBegin);

		else if (!testInteger(edgeLineBegin))
			addErrorToList(keyEdgeLineBegin, edgeLineBegin, LINE_BEGIN_END_VALUES);

		else {
			// Test pro rozsah:
			final int lineBegin = Integer.parseInt(edgeLineBegin);

			if (lineBegin != 0 && lineBegin != 1 && lineBegin != 2 && lineBegin != 4 && lineBegin != 5 && lineBegin != 7
					&& lineBegin != 8 && lineBegin != 9)
				addErrorToList(keyEdgeLineBegin, edgeLineBegin, LINE_BEGIN_END_VALUES);
		}
		
		
		
		
		
		
		// Šířka hrany:
		final String edgeLineWidth = getValueFromProperties(keyEdgeLineWidth, prop);

		if (edgeLineWidth == null)
			addErrorToList(keyEdgeLineWidth);

		else if (!testDouble(edgeLineWidth))
			addErrorToList(keyEdgeLineWidth, edgeLineWidth, LINE_WIDTH_VALUES);

		else {
			// Test pro rozsah:
			final double width = Double.parseDouble(edgeLineWidth);

			if (width < 0.1d || width > 20.0d)
				addErrorToList(keyEdgeLineWidth, edgeLineWidth, LINE_WIDTH_VALUES);
		}
		
		
		
		
		
		// konec hrany vyplněn:
		final String edgeEndFill = getValueFromProperties(keyEdgeEndFill, prop);

		if (edgeEndFill == null)
			addErrorToList(keyEdgeEndFill);

		else if (!testBoolean(edgeEndFill))
			addErrorToList(keyEdgeEndFill, edgeEndFill, BOOLEAN_ALLOWED_VALUES);
		
		
		
		
		
		// Začátek hrany vyplněn:
		final String edgeBeginFill = getValueFromProperties(keyEdgeBeginFill, prop);

		if (edgeBeginFill == null)
			addErrorToList(keyEdgeBeginFill);

		else if (!testBoolean(edgeBeginFill))
			addErrorToList(keyEdgeBeginFill, edgeBeginFill, BOOLEAN_ALLOWED_VALUES);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která ze zadané "textové" proměnné, která značí "identifikátor"
	 * příslušného editoru zdrojového kódu vrátí výčtovou hodnotu, která značí
	 * editor zdrojového kódu, ve kterém se mají primárně otevírat třídy.
	 * 
	 * @param value
	 *            - hodnota načtená z příslušného souboru ClassDiagram.properties,
	 *            která značí index příslušného editoru kódu.
	 * 
	 * @return výčtovou hodnotu, která značí konkrétní editor zdrojového kódu, který
	 *         se má primárně využít pro otevření tříd v diagramu tříd nebo null,
	 *         pokud budezadána nějaká hodnoty mimo příslušné editory..
	 */
	private static KindOfCodeEditor getKindOfEditorByTextValue(final String value) {
		if (value.equalsIgnoreCase(KindOfCodeEditor.SOURCE_CODE_EDITOR.getValue()))
			return KindOfCodeEditor.SOURCE_CODE_EDITOR;

		else if (value.equalsIgnoreCase(KindOfCodeEditor.PROJECT_EXPLORER.getValue()))
			return KindOfCodeEditor.PROJECT_EXPLORER;

		return null;
	}
}