package cz.uhk.fim.fimj.check_configuration_files;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import cz.uhk.fim.fimj.app.LookAndFeelSupport;
import cz.uhk.fim.fimj.file.ConfigurationFiles;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.file.UserNameSupport;
import cz.uhk.fim.fimj.font_size_form.FontSizeFormType;
import cz.uhk.fim.fimj.language.KeysOrder;
import cz.uhk.fim.fimj.menu.SortRecentlyOpenedProjects;

/**
 * Tato třída slouží pro otestování toho, zda jsou hodnoty v souboru Default.properties zadány v pořádku, tj. zda se
 * veškeré hodnoty nachází například v povolených intervalech, zda jsou na místěch, kde mají být čísla opravdu čísla a
 * ne třeba písmena apod.
 * <p>
 * Prostě kontrola souboru Default.properties, zda obsahuje veškeré klíče a k němu povolené hodnoty.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

class CheckDefaultProp extends CheckConfigPropAbstract {

    /**
     * List, který obsahuje názvy obrázků, které lze využít jako animaci při načítání - tj. když aplikace provádí nějaké
     * operace, a je zobrazen příslušný dialog, tak ten obrázek, resp. jeho název.
     */
    private static final List<String> LOADING_IMAGES_MODEL = Collections.unmodifiableList(Arrays.asList("balls",
            "battery", "box", "clock", "dashinfinity", "default", "ellipsis", "flickr", "gear", "gears", "hourglass",
            "infinity", "pacman", "reload", "ring", "ripple", "squares", "sunny", "triangle", "wave", "wheel"));

    /**
     * Přípona souboru '.gif'
     */
    private static final String GIF_EXTENSION = ".gif";

    /**
     * Metoda, která slouží pro kontrolu, zda soubor Default.properties obsahuje příslušné klíče a k němu povolené
     * hodnoty.
     *
     * @return list, který může obsahovat chybová hlášení, tedy položky, které značí nalezené chyby, nebo bude prázdný,
     * pokud se žádná chyba nenajde.
     */
    static List<ErrorInfo> checkDefaultPropFile() {
        ERRORS_LIST.clear();

		final Properties defaultProp = ReadFile.getDefaultPropertiesInWorkspace();

		if (defaultProp == null) {
			/*
			 * K této části podmínky bych se nikdy neměl dostat, protože tato metoda se volá
			 * z ControlClass, ale až po tom, co se již otestovatlo, že adresář
			 * configuration ve workspace existuje, případně byl vytvořen, ale pouze pro
			 * případ, že by i tak došlo z mě neznámého důvodu k tomu, že by se ani tak ty
			 * soubory nenacházely ve workspace, tak jej zde zkusím vytvořit.
			 */

			/*
			 * Načtu si cestu k adresáři workspace.
			 */
			final String pathToWorkspace = ReadFile.getPathToWorkspace();

			acquireMutex();
			
			/*
			 * Pokud byla nalezena cesta k adresář workspace, pak pro jitotu znovu otestuji,
			 * zda existuje adresář configuration, ale už zde netestuji verzi, kdy
			 * neexistuje aby se vytvořil, protože by to nikdy nastat nemělo, a pokud
			 * tenadresář existuje, tak vytvořím pčříslušný soubor.
			 */
			if (pathToWorkspace != null
					&& ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
				ConfigurationFiles.writeDefaultPropertiesAllNew(
						pathToWorkspace + File.separator + Constants.PATH_TO_DEFAULT_PROPERTIES);
			
			releaseMutex();
		}

		else
			checkValues(defaultProp);

		return ERRORS_LIST;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro otestování všech klíčů a jejich hodnot v souboru
	 * Default.properties (parametr prop).
	 * 
	 * @param prop
	 *            - objekt Properties (načtený soubor Default.properties), ve kterém
	 *            se mají zkontrolovat, zda ten objekt obsahuje veškeré klíče s
	 *            hodnotami, které by měl a ty hodnoty obsahuje "validní data".
	 */
	private static void checkValues(final Properties prop) {
		// Zda se má zobrazit workspace:
		testBooleanValue(prop, "Display WorkspaceChooser");




		// Kontrola uživatelského jména:
		final String userName = getValueFromProperties("UserName", prop);

		if (userName == null)
			addErrorToList("UserName");

		else if (!UserNameSupport.isUserNameValid(userName))
			addErrorToList("UserName", userName, UserNameSupport.USER_NAME_REG_EX);


		
		
		
		// Jazyk pro aplikaci:
		final String language = getValueFromProperties("Language", prop);

		if (language == null)
			addErrorToList("Language");

		else {
			// Zde není jazyk null, tak potřebuji otestovat, zda obsahuje povolené hodnoty:
			if (!language.equals("CZ") && !language.equals("EN"))
				addErrorToList("Language", language, "'CZ', 'EN'");
		}
		
		
		
		
		
		
		
		// Obrázek pro loading dialog:
		final String loadingImage = getValueFromProperties("Loading Image", prop);

		if (loadingImage == null)
			addErrorToList("Loading Image");

		else {
			boolean foundMatch = false;

			for (final String s : LOADING_IMAGES_MODEL) {
				if ((s + GIF_EXTENSION).equals(loadingImage)) {
					foundMatch = true;
					break;
				}
			}
			
			
			
			if (!foundMatch) {
				final List<String> temp = new ArrayList<>();

				LOADING_IMAGES_MODEL.forEach(i -> temp.add("'" + i + GIF_EXTENSION + "'"));

				addErrorToList("Loading Image", loadingImage, Arrays.toString(temp.toArray()));
			}
		}
		
		
		
		
		
		
		// Způsob řazení textů v souborech .properties:
		final String keysOrder = getValueFromProperties("Default keys order", prop);

		if (keysOrder == null)
			addErrorToList("Default keys order");

		else if (getKeysOrderByFileValue(keysOrder) == null)
			addErrorToList("Default keys order", keysOrder,
					"'Without_Sorting', 'Ascending_By_Adding', 'Descending_By_Adding', 'Ascending_By_Alphabet', 'Descending_By_Alphabet'");
		
		
		
		
		// Zda se mají vkládat komentáře nebo ne ke klíčům s souboech .properties:
		testBooleanValue(prop, "Add comments");
		
		
		
		
		
		// Počet volných řádků/ mezer před komentřáem:
		final String countOfFreeLines = getValueFromProperties("Count of free lines before comment", prop);

		if (countOfFreeLines == null)
			addErrorToList("Count of free lines before comment");

		else if (!testInteger(countOfFreeLines))
			addErrorToList("Count of free lines before comment", countOfFreeLines, "<0 ; 100>");

		else {
			// Test pro rozsah:
			final int count = Integer.parseInt(countOfFreeLines);

			if (count < 0 || count > 100)
				addErrorToList("Count of free lines before comment", countOfFreeLines, "<0 ; 100>");
		}








		// Způsob řazení položek (/ dříve otevřených souborů) v rozevíracím menu v menu Projekt v okně aplikace:
		final String sorting_of_recently_opened_projects = getValueFromProperties("Sorting of recently opened " +
				"projects", prop);

		if (sorting_of_recently_opened_projects == null)
			addErrorToList("Sorting of recently opened projects");

		else if (getSortRecentlyOpenedProjects(sorting_of_recently_opened_projects) == null)
			addErrorToList("Sorting of recently opened projects", sorting_of_recently_opened_projects,
					"'ASC', 'DES'");








		// Locale:
		final String textLocle = getValueFromProperties("Properties locale", prop);

		if (textLocle == null)
			addErrorToList("Properties locale");

		else {
			boolean foundLocale = false;

			/*
			 * Projdu všechny dostupné jazyky a jestli se v nich nachází ten zadaný je to
			 * OK, jinak přidám chybové oznámení.
			 */
			for (final Locale l : DateFormat.getAvailableLocales()) {
				if (l.toLanguageTag().equals(textLocle)) {
					foundLocale = true;
					break;
				}
			}

			if (!foundLocale)
				addErrorToList("Properties locale", textLocle, Arrays.toString(ConfigurationFiles.getLocalesModel()));
		}
		
		
		
		
		
		
		
		
		// Zda se po spuštění aplikace mají vyhledat soubory potřebné pro kompilaci:
		testBooleanValue(prop, "Search compile files");
		
		
		
		
		
		
		
		
		
		// Zda se má prohledat celý disk, aby se našli potřebné soubory pro kompilování:
		testBooleanValue(prop, "Search whole disk for compile files");
		
		
		
		
		
		
		
		
		// Zda se mají vypisovat nastalé výjimky do výstupního terminálu:
		testBooleanValue(prop, "Write not captured exceptions");







        final String lookAndFeel = getValueFromProperties("LookAndFeel", prop);

        final String allowedValuesForLookAndFeel =
                "[" + Constants.WITHOUT_SPECIFIC_LOOK_AND_FEEL + ", " + LookAndFeelSupport.getSupportedLookAndFeelsInText() + "]";

        if (lookAndFeel == null)
            addErrorToList("LookAndFeel");

        else if (!LookAndFeelSupport.checkExistLookAndFeel(lookAndFeel) && !lookAndFeel.trim().equalsIgnoreCase(Constants.WITHOUT_SPECIFIC_LOOK_AND_FEEL))
            addErrorToList("LookAndFeel", lookAndFeel, allowedValuesForLookAndFeel);
		
		
		
		
		
		
		
		
		
		
		
		/*
		 * Následuje ukládání označených komponent pro dialog pro "rychlé" nastavení
		 * velikosti písma:
		 */
		
		// Aktualizace velikosti písma během posouvání posuvníku nebo ažpo finálním umístění posuvníku:
		testBooleanValue(prop, "FontSizeForm_UpdateDiagramsWhileMovingWithSlider");
		
		
		
		
		
		
		
		// Označený panel v dialogu pro "rychlé" nastavení velikosti písma:
		checkFontFormSelectedPanel(prop);
		
		
		
		
		// Zda má být chcb pro nastavení velikosti písma pro třídu označen:
		testBooleanValue(prop, "FontSizeForm_CD_ClassCell");

		// Zda má být chcb pro nastavení velikosti písma pro třídu označen:
		testBooleanValue(prop, "FontSizeForm_CD_CommentCell");

		// Zda má být chcb pro nastavení velikosti písma pro hranu komentáře označen:
		testBooleanValue(prop, "FontSizeForm_CD_CommentEdge");

		// Zda má být chcb pro nastavení velikosti písma pro hranu dědičnost označen:
		testBooleanValue(prop, "FontSizeForm_CD_ExtendsEdge");

		// Zda má být chcb pro nastavení velikosti písma pro hranu implementace rozhraní
		// označen:
		testBooleanValue(prop, "FontSizeForm_CD_ImplementsEdge");

		// Zda má být chcb pro nastavení velikosti písma pro hranu asociace (symetrická)
		// označen:
		testBooleanValue(prop, "FontSizeForm_CD_AssociationEdge");

		// Zda má být chcb pro nastavení velikosti písma pro hranu agregace 1 : 1
		// (asymetrická asociace) označen:
		testBooleanValue(prop, "FontSizeForm_CD_Aggregation_1_1_Edge");

		// Zda má být chcb pro nastavení velikosti písma pro hranu agregace 1 : N
		// označen:
		testBooleanValue(prop, "FontSizeForm_CD_Aggregation_1_N_Edge");

		// Zda má být chcb pro nastavení velikosti písma pro instanci označen:
		testBooleanValue(prop, "FontSizeForm_ID_InstanceCell");

		// Zda má být chcb pro nastavení velikosti písma pro atributy v instanci
		// označen:
		testBooleanValue(prop, "FontSizeForm_ID_InstanceCell_Attributes");

		// Zda má být chcb pro nastavení velikosti písma pro metody v instanci označen:
		testBooleanValue(prop, "FontSizeForm_ID_InstanceCell_Methods");

		// Zda má být chcb pro nastavení velikosti písma pro hranu asociace v diagramu
		// instancí označen:
		testBooleanValue(prop, "FontSizeForm_ID_AssociationEdge");

		// Zda má být chcb pro nastavení velikosti písma pro hranu agregace v daigramu
		// instancí označen:
		testBooleanValue(prop, "FontSizeForm_ID_AggregationEdge");
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Zkusím si načíst zadanou hodnotu pro zvolený panel v dialogu pro nastavení
	 * velikosti písma, pokud se najde shoda se zadaným indexem panelu, pak je to
	 * OK, ale pokud ne, pak se přidá se do listu příslušná chyba.
	 * 
	 * @param prop
	 *            - objekt Properties, který obsahuje načtený soubor
	 *            Default.properties, který obsahuje příslušné hodnoty, které se
	 *            mají otestovat.
	 */
	private static void checkFontFormSelectedPanel(final Properties prop) {
		final String tempFormType = prop.getProperty("FontSizeForm_SelectedPanel", null);

		if (tempFormType == null)
			addErrorToList("FontSizeForm_SelectedPanel");

		else if (!tempFormType.equals(FontSizeFormType.ONE_SAME_SIZE.getValue())
				&& !tempFormType.equals(FontSizeFormType.ONE_SIZE_FOR_CHOOSED_OBJECTS.getValue())
				&& !tempFormType.equals(FontSizeFormType.ONE_SIZE_FOR_EACH_OBJECT.getValue()))
			addErrorToList("FontSizeForm_SelectedPanel", tempFormType,
					"'One_Font_Size_For_All_Objects', 'Font_Size_For_Selected_Objects', 'One_Font_Size_For_Each_Object'");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která "převede" text v proměnné fileValue na výčtový typ tak, že dle
	 * hodnoty v fileValue se testuje, zda se daná hodnota shoduje s jednou z hodnot
	 * u výčtových typů, pokud ano, vrátí se daný výčtový typ, u kterého se shoduje
	 * příslušná hodnota, pokud ne (nemělo by nastat pokud uživatel schválně nezadá
	 * "nevalidní" hodnotu), pak se vrátí výchozí hodnota null, abych zjistitl, zda
	 * uživatel zadal jednu z validních hodnot nebo ne.
	 * 
	 * @param fileValue
	 *            - text načtený ze souboru a dle této hodnoty se má najít k ní
	 *            patřící výčtový typ.
	 * 
	 * @return výše popsaný výčtový typ dle hodnoty v fileValue, nebo výchozí
	 *         hodnotu null, protože potřebuji zjistit, zda je příslušná hodnota
	 *         zadaná správně nebo ne.
	 */
	private static KeysOrder getKeysOrderByFileValue(final String fileValue) {
		/*
		 * Pro případ, že uživatel nezadá úplně validní název nebudu testovat rovnosti
		 * velikosti písmen.
		 */
		if (fileValue.equalsIgnoreCase(KeysOrder.WITHOUT_SORTING.getValueForFile()))
			return KeysOrder.WITHOUT_SORTING;

		
		else if (fileValue.equalsIgnoreCase(KeysOrder.ASCENDING_BY_ADDING.getValueForFile()))
			return KeysOrder.ASCENDING_BY_ADDING;

		
		else if (fileValue.equalsIgnoreCase(KeysOrder.DESCENDING_BY_ADDING.getValueForFile()))
			return KeysOrder.DESCENDING_BY_ADDING;

		
		else if (fileValue.equalsIgnoreCase(KeysOrder.ASCENDING_BY_ALPHABET.getValueForFile()))
			return KeysOrder.ASCENDING_BY_ALPHABET;

		
		else if (fileValue.equalsIgnoreCase(KeysOrder.DESCENDING_BY_ALPHABET.getValueForFile()))
			return KeysOrder.DESCENDING_BY_ALPHABET;

		
		// Vrátím null hodnotu:
		return null;
	}







    /**
     * Metoda, která zjistí, zda je hodnota value index nějaké hodnoty ve výčtu {@link SortRecentlyOpenedProjects}.
     * Pokud ano, pak se vrátí ta hodnota, pokud ne, pak se vrátí null.
     * <p>
     * (Note: zde by bylo možné využít i zjišťování hodnoty dle textu ve výčtu, ale potřebuji, aby se vrátilo null, nebo
     * nějaký podobná hodnota, která značí, že se nejedná o hodnotu z výčtu.)
     *
     * @param value
     *         - hodnota, o které se má zjistit, zda se jedná o index výše zmíněné výčtové hodnoty.
     *
     * @return null, pokud value není index výše zmíněné výčtové hodnoty, jinak příslušnou výčtovou hodnotu.
     */
    private static SortRecentlyOpenedProjects getSortRecentlyOpenedProjects(final String value) {
        for (final SortRecentlyOpenedProjects sortRecentlyOpenedProjects : SortRecentlyOpenedProjects.values())
            if (sortRecentlyOpenedProjects.getValue().equals(value))
                return sortRecentlyOpenedProjects;

        return null;
    }
}
