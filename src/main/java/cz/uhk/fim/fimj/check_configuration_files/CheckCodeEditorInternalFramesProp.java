package cz.uhk.fim.fimj.check_configuration_files;

import java.io.File;
import java.util.List;
import java.util.Properties;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.ConfigurationFiles;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.ReadFile;

/**
 * Tato třída slouží pouze pro otestování hodnot v souboru CodeEditorInternalFrame.properties. Jde pouze o to, že se
 * otestuje, zda tento soubor obsahuje veškeré požadované klíče a ke každému klíči obsahuje validní hodnotu.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

class CheckCodeEditorInternalFramesProp extends CheckConfigPropAbstract {

    /**
     * Metoda, která slouží pro kontrolu, zda soubor CodeEditorInternalFrame.properties obsahuje příslušné klíče a k
     * němu povolené hodnoty.
     *
     * @return list, který může obsahovat chybová hlášení, tedy položky, které značí nalezené chyby, nebo bude prázdný,
     * pokud se žádná chyba nenajde.
     */
    static List<ErrorInfo> checkProjectCodeEditorInternalFrameProp() {
        ERRORS_LIST.clear();

        final Properties projectCodeEditProp = App.READ_FILE
                .getCodeEditorPropertiesForInternalFramesFromWorkspaceOnly();

        if (projectCodeEditProp == null) {
            /*
             * K této části podmínky bych se nikdy neměl dostat, protože tato metoda se volá
             * z ControlClass, ale až po tom, co se již otestovalo, že adresář
             * configuration ve workspace existuje, případně byl vytvořen, ale pouze pro
             * případ, že by i tak došlo z mě neznámého důvodu k tomu, že by se ani tak ty
             * soubory nenacházely ve workspace, tak jej zde zkusím vytvořit.
             */

            /*
             * Načtu si cestu k adresáři workspace.
             */
            final String pathToWorkspace = ReadFile.getPathToWorkspace();

            acquireMutex();

            /*
             * Pokud byla nalezena cesta k adresář workspace, pak pro jitotu znovu otestuji,
             * zda existuje adresář configuration, ale už zde netestuji verzi, kdy
             * neexistuje aby se vytvořil, protože by to nikdy nastat nemělo, a pokud
             * tenadresář existuje, tak vytvořím pčříslušný soubor.
             */
            if (pathToWorkspace != null
                    && ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
                ConfigurationFiles.writeCodeEditorPropertiesForInternalFrames(
                        pathToWorkspace + File.separator + Constants.PATH_TO_CODE_EDITOR_INTERNAL_FRAMES_PROPERTIES);

            releaseMutex();
        }
        else
            checkValues(projectCodeEditProp);

        return ERRORS_LIST;
    }


    /**
     * Metoda, která slouží pro otestování všech klíčů a jejich hodnot v souboru CodeEditorInternalFrame.properties
     * (parametr prop).
     *
     * @param prop
     *         - objekt Properties (načtený soubor CodeEditorInternalFrame.properties), ve kterém se mají zkontrolovat,
     *         zda ten objekt obsahuje veškeré klíče s hodnotami, které by měl a ty hodnoty obsahuje "validní data".
     */
    private static void checkValues(final Properties prop) {
        checkSimilarValuesInEditors(prop, "FontStyle", "FontSize", "BackgroundColor", "ForegroundColor");

        testBooleanValue(prop, "HighlightingCodeByKindOfFile");

        testBooleanValue(prop, "HighlightingCode");

        testBooleanValue(prop, "CompileClassInBackground");

        testBooleanValue(prop, "WrapTextInAllFrames");

        testBooleanValue(prop, "ShowWhiteSpaceInAllFrames");

        testBooleanValue(prop, "ClearWhiteSpaceLineInAllFrames");

        // Kontrola vlastností pro formátování zdrojového kódu:
        checkFormattingProperties(prop);
    }
}
