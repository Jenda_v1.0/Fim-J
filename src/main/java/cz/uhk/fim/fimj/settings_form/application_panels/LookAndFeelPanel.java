package cz.uhk.fim.fimj.settings_form.application_panels;

import cz.uhk.fim.fimj.app.LookAndFeelSupport;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.settings_form.cmb_items_with_tooltip.CmbItemAbstract;
import cz.uhk.fim.fimj.settings_form.cmb_items_with_tooltip.LookAndFeelItem;
import cz.uhk.fim.fimj.settings_form.cmb_items_with_tooltip.ToolTipsComboBoxRenderer;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.IntStream;

/**
 * Třída slouží jako panel pro nastavení Look and Feel aplikace.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 28.08.2018 22:37
 */

public class LookAndFeelPanel extends JPanel implements LanguageInterface {

    /**
     * Label pro zobrazení textu s informací, že se jedná o nastavení Look and Feel pro aplikaci.
     */
    private final JLabel lblLookAndFeel;

    /**
     * Komponenta, ve které si uživatel vybere požadovaný "vzhled" aplikace (Look and Feel).
     */
    private final JComboBox<CmbItemAbstract> cmbLookAndFeel;

    /**
     * Text do tooltipu pro položku, která značí, že se má využít výchozí Look and Feel společné pro všechny platformy.
     * Tj. nemá se vybrat specifiké nastavení Look and Feel.
     */
    private String txtWithoutLookAndFeelTt;


    /**
     * Konstruktor třídy.
     */
    @SuppressWarnings("unchecked")
    public LookAndFeelPanel() {
        setLayout(new FlowLayout(FlowLayout.CENTER));

        lblLookAndFeel = new JLabel();
        add(lblLookAndFeel);

        cmbLookAndFeel = new JComboBox<>();
        cmbLookAndFeel.setRenderer(new ToolTipsComboBoxRenderer(cmbLookAndFeel));
        add(cmbLookAndFeel);
    }


    /**
     * Nastavení hodnot do komponenty JComboBox, která zobrazuje možnosti nastavení Look and Feel.
     */
    public void setCmbModel() {
        final UIManager.LookAndFeelInfo[] installedLookAndFeels = LookAndFeelSupport.getInstalledLookAndFeels();

        final List<LookAndFeelItem> lookAndFeelItemList = new ArrayList<>();

        // Přidání výchozí hodnoty:
        lookAndFeelItemList.add(new LookAndFeelItem("Default", txtWithoutLookAndFeelTt,
                Constants.WITHOUT_SPECIFIC_LOOK_AND_FEEL));

        Arrays.stream(installedLookAndFeels).forEach(l -> lookAndFeelItemList.add(new LookAndFeelItem(l.getName(),
                l.getClassName(), l.getClassName())));

        cmbLookAndFeel.setModel(new DefaultComboBoxModel<>(lookAndFeelItemList.toArray(new LookAndFeelItem[]{})));
    }


    /**
     * Nastavení označené položky v komponentě JComboBox.
     *
     * @param indexOfLookAndFeel
     *         - index (/ identifikátor Look and Feel) položky, která se má označit.
     */
    public final void setSelectedItemByIndex(final String indexOfLookAndFeel) {
        final ComboBoxModel<CmbItemAbstract> model = cmbLookAndFeel.getModel();

        /*
         * Získání indexu položky v modelu, která se má nastavit jako označená. Pokud se položka nenajde (nemělo by
         * nastat), bude index 0, což je nastavená jako výchozí položka bez Look and Feel.
         */
        final int index =
                IntStream.range(0, model.getSize()).filter(i -> ((LookAndFeelItem) cmbLookAndFeel.getModel().getElementAt(i)).getIndexOfLookAndFeel().equalsIgnoreCase(indexOfLookAndFeel)).findFirst().orElse(0);

        cmbLookAndFeel.setSelectedIndex(index);
    }


    /**
     * Nastavení označené výchozí hodnoty (Výchozí Look and Feel).
     */
    public final void restoreDefaultSettings() {
        setSelectedItemByIndex(Constants.DEFAULT_LOOK_AND_FEEL);
    }


    /**
     * Získání označené položky v komponentě JComboBox.
     *
     * @return označenou položku (/ označený Look and Feel).
     */
    public final LookAndFeelItem getSelectedItem() {
        return (LookAndFeelItem) cmbLookAndFeel.getSelectedItem();
    }


    @Override
    public void setLanguage(final Properties properties) {
        final String borderTitle;

        if (properties != null) {
            borderTitle = properties.getProperty("Lafp_BorderTitle", Constants.LAFP_BORDER_TITLE);

            lblLookAndFeel.setText(properties.getProperty("Lafp_Lbl_LookAndFeel", Constants.LAFP_LBL_LOOK_AND_FEEL));

            txtWithoutLookAndFeelTt = properties.getProperty("Lafp_Txt_WithoutLookAndFeelItem_TT",
                    Constants.LAFP_TXT_WITHOUT_LOOK_AND_FEEL_ITEM_TT);
        }

        else {
            borderTitle = Constants.LAFP_BORDER_TITLE;

            lblLookAndFeel.setText(Constants.LAFP_LBL_LOOK_AND_FEEL);

            txtWithoutLookAndFeelTt = Constants.LAFP_TXT_WITHOUT_LOOK_AND_FEEL_ITEM_TT;
        }

        setBorder(BorderFactory.createTitledBorder(borderTitle + " (Look and Feel)"));

        lblLookAndFeel.setText(lblLookAndFeel.getText() + ": ");
    }
}