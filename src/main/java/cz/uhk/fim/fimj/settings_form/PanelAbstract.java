package cz.uhk.fim.fimj.settings_form;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JComponent;
import javax.swing.JPanel;

import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;

/**
 * Tato abstraktní třída slouží akorát pro definici základních metod, které se používají nejvíce v panelech v dialogu
 * coby nastavení aplikace, pro definici GridBagConstraints, aby ji nemusel pořád vytvářet, ...
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class PanelAbstract extends JPanel {

	private static final long serialVersionUID = 1L;

	
	/**
	 * Výchozí text pro labely, které slouží jako ukázka pro nějaké barvy, například
	 * barvy písma apod.
	 */
	protected static final String COLOR_EXAMPLE_LABEL_TEXT = "Col";
	
	
	
	/**
	 * Tento mutex by zde ani nemusel být, protože se jedná o vykonávání příslušných
	 * operací puze v "hlavním" swingovém vlákně, ne v externím apod. Ale bokem běží
	 * například vlákno pro monitorování změn konfiguračních souborů v adresáři
	 * workspace, tak když se zde mají zpasat změny, pak nechci, aby se v tom vlákně
	 * pro monitorování nebo náhodou někde jinde nějak s těmi daty manipulovalo,
	 * protože toto by mělo "nejvyšší" pritoritu, když uživatel nastaví v dialogu
	 * nastavení nějaké změny, pak chci, aby se ty změny uložily.
	 */
	private final Semaphore mutex = new Semaphore(1);
	
	
	

	
	/**
	 * Proměnná, která slouží pro indexování komponent při přidání, tj. slouží pro
	 * indexování řádků, kam se mají přidat komponenty v nějakém panelu.
	 */
	protected int index;
	
	
	


	/**
	 * Metoda, která vytvoří instanci objktu GridBagConstraint pro rozvržení
	 * komponent a doplní mu výchozí nastavení, které potřebui
	 * 
	 * @return vrátí instanci výše zmíněného objektu se základním nastavením
	 */
	protected static GridBagConstraints getGbc() {
		final GridBagConstraints gbc = new GridBagConstraints();

		gbc.fill = GridBagConstraints.HORIZONTAL;

		gbc.insets = new Insets(5, 5, 5, 5);

		gbc.weightx = 0.2;
		gbc.weighty = 0.2;

		gbc.gridwidth = 1;
		gbc.gridheight = 1;

		return gbc;
	}
	
	

	
	
	/**
	 * Metoda, která přidá do daného panelu danou komponentu na danou pozici v
	 * GridbagConstraint
	 * 
	 * @param gbc
	 *            - komponenta, do které uložím pozici - kam chci komponentu vložit
	 * 
	 * @param rowIndex
	 *            - index řádku
	 * 
	 * @param columnIndex
	 *            - index sloupce
	 * 
	 * @param component
	 *            - komponenta pro vložení
	 * 
	 * @param panel
	 *            - panel, kam chci komponentu vložit
	 */
	protected static void setGbc(GridBagConstraints gbc, int rowIndex, int columnIndex, JComponent component,
			JPanel panel) {
		gbc.gridx = columnIndex;
		gbc.gridy = rowIndex;
		panel.add(component, gbc);
	}
	
	
	

	
	
	
	/**
	 * Metoda, která spustí blokování vůči přístupu editování příslušného souboru /
	 * souborů jinými vlákny.
	 */
	protected final void acquireMutex() {
		try {
			mutex.acquire();
		} catch (InterruptedException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informaci o chybě:
				logger.log(Level.INFO,
						"Zachycena výjimka při pokusu spuštění mutexu pro blokování přístupu k souboru. Tato výjimka " +
								"může "
								+ "nastat například v případě, že dojde k přerušení běhu vlákna. V tomto případě by to" +
								" ale nastat nemělo.");

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();
		}
	}
	
	
	

	
	/**
	 * Metoda, která slouží pro uvolnění mutexu, tj. uvolnění blokování proti
	 * editaci příslušných souborů / souboru.
	 */
	protected final void releaseMutex() {
		mutex.release();
	}
}