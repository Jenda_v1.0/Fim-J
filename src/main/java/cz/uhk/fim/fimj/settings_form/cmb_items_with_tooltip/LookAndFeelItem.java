package cz.uhk.fim.fimj.settings_form.cmb_items_with_tooltip;

/**
 * Třída, která slouží jako položka do komponenty JComboBox pro zobrazování informací o Look and Feel. Jedná se o
 * možnosti, které lze nastavit pro tuto aplikaci.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 28.08.2018 21:05
 */

public class LookAndFeelItem extends CmbItemAbstract {

    /**
     * Hodnota, která se bude zapisovat do souboru. Jedná se o index, dle kterého se pozná specifický Look and Feel.
     */
    private final String indexOfLookAndFeel;

    /**
     * Konstruktor třídy (pro naplnění hodnot).
     *
     * @param textToShow
     *         - text, který se má zobrazit v komponentě JComboBox.
     * @param textToTooltip
     *         - text, který slouží jako tooltip (popisek), který se zobrazí v případě, že uživatel najede kurzorem myši
     *         na položku ve zmíněné komponentě JComboBox.
     * @param indexOfLookAndFeel
     *         - index konkrétního Look and Feel. Jedná se o hodnotu pro zápis do souboru.
     */
    public LookAndFeelItem(final String textToShow, final String textToTooltip, final String indexOfLookAndFeel) {
        super(textToShow, textToTooltip);

        this.indexOfLookAndFeel = indexOfLookAndFeel;
    }


    public String getIndexOfLookAndFeel() {
        return indexOfLookAndFeel;
    }
}
