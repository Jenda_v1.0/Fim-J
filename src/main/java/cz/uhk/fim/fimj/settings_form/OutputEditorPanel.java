package cz.uhk.fim.fimj.settings_form;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.ConfigurationFiles;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.file.WriteToFile;
import cz.uhk.fim.fimj.language.EditProperties;
import cz.uhk.fim.fimj.output_editor.OutputEditor;

/**
 * Tento panel slouží pro nastavení mnou zvolených základních vlastností pro editor výstupů
 * <p>
 * nastavení vlastnotí jako je font, veiksot a barva písma a pozadí
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class OutputEditorPanel extends EditorsPanelAbstract
		implements LanguageInterface, SaveSettingsInterface, ActionListener {

	private static final long serialVersionUID = 1L;


	
	/**
	 * Reference na editor výstupů.
	 */
	private final OutputEditor outputEditor;
	
	
	private final JButton btnRestoreDefaultSettings;
	
	
	
	
	/**
	 * Komponenta, která slouží pro nastavení toho, zda se mají do editoru výstupů
	 * vypisovat reference (referenční proměnné) na instance.
	 * 
	 * Tzn. Pokud se do editoru výstupů vypíše instance nebo pole nebo list, která
	 * bude obsahovat instance tříd z diagramu tříd a tyto instance se aktuálně
	 * nachází v diagramu instancí, tak se každou hodnotu (instanci) vypíše její
	 * reference, ktero uuživatel zadal při jejím vytváření.
	 */
	private final JCheckBox chcbShowReferenceVariables;
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param outputEditor
	 *            - reference na editoru výstupů, která je potřeba kvůli zavolání
	 *            meotdy pro změnu nastavení.
	 */
	public OutputEditorPanel(final OutputEditor outputEditor) {
		super();
		
		this.outputEditor = outputEditor;
	
		
		setLayout(new GridBagLayout());
		
		
		final GridBagConstraints gbc = getGbc();
		
		index = 0;
		
		
		
		// Komponenty do okna resp panelu:
		
		chcbShowReferenceVariables = new JCheckBox();
		gbc.gridwidth = 3;
		setGbc(gbc, index, 0, chcbShowReferenceVariables, this);
		gbc.gridwidth = 1;
		
		
		
		lblBgColor = new JLabel();
		setGbc(gbc, ++index, 0, lblBgColor, this);
		
		btnChooseBgColor = new JButton();
		btnChooseBgColor.addActionListener(this);
		setGbc(gbc, index, 1, btnChooseBgColor, this);
		
		lblChoosedBgColor = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChoosedBgColor.setOpaque(true);
		setGbc(gbc, index, 2, lblChoosedBgColor, this);
		
		
		
		lblTextColor = new JLabel();
		setGbc(gbc, ++index, 0, lblTextColor, this);
		
		btnChooseTextColor = new JButton();
		btnChooseTextColor.addActionListener(this);
		setGbc(gbc, index, 1, btnChooseTextColor, this);
		
		lblChoosedTextColor = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChoosedTextColor.setOpaque(true);
		setGbc(gbc, index, 2, lblChoosedTextColor, this);
		
		
		
		
		
		lblFontSize = new JLabel();
		setGbc(gbc, ++index, 0, lblFontSize, this);
		
		cmbFontSize = new JComboBox<>(getFontSizes());
		setGbc(gbc, index, 1, cmbFontSize, this);
		
		
		
		
		lblFontStyle = new JLabel();
		setGbc(gbc, ++index, 0, lblFontStyle, this);
		
		cmbFontStyle = new JComboBox<>(ARRAY_OF_FONT_STYLE.toArray(new String[] {}));
		setGbc(gbc, index, 1, cmbFontStyle, this);
		
		
		
		gbc.gridwidth = 3;
		final JPanel pnlButtonDefaultSettins = new JPanel(new FlowLayout(FlowLayout.CENTER));
		btnRestoreDefaultSettings = new JButton();
		btnRestoreDefaultSettings.addActionListener(this);
		pnlButtonDefaultSettins.add(btnRestoreDefaultSettings);
		
		setGbc(gbc, ++index, 0, pnlButtonDefaultSettins, this);
		
		
		openSettings();
	}
	
	
	
	

	

	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			setBorder(BorderFactory.createTitledBorder(properties.getProperty("Sf_Oep_BorderTitle", Constants.SF_OEP_BORDER_TITLE)));
			
			chcbShowReferenceVariables.setText(properties.getProperty("Sf_Oep_Chcb_ShowReferenceVariablesText", Constants.SF_OEP_CHCB_SHOW_REFERENCE_VARIABLES_TEXT));
			chcbShowReferenceVariables.setToolTipText(properties.getProperty("Sf_Oep_Chcb_ShowReferenceVariableTT", Constants.SF_OEP_CHCB_SHOW_REFERENCE_VARIABLES_TT));
			
			lblBgColor.setText(properties.getProperty("Sf_Ce_LabelBackgroundColor", Constants.SF_CE_LBL_BACKGROUND_COLOR) + ": ");
			lblTextColor.setText(properties.getProperty("Sf_Cd_LabelTextColorClassCell", Constants.SF_LBL_TEXT_COLOR_CELL) + ": ");
			lblFontSize.setText(properties.getProperty("Sf_Cd_LabelFontSizeClassCell", Constants.SF_LBL_FONT_SIZE_CELL) + ": ");
			lblFontStyle.setText(properties.getProperty("Sf_Cd_LabelFontStyleClassCell", Constants.SF_LBL_FONT_STYLE_CELL) + ": ");
			
			
			txtChooseTextColorDialog = properties.getProperty("Sf_Cd_ChooseTextColorDialog", Constants.SF_TXT_CHOOSE_TEXT_COLOR);
			txtChooseBgColorDialog = properties.getProperty("Sf_Cd_ChooseBackgroundColorDialog", Constants.SF_TXT_CHOOSE_BG_COLOR);
			
			
			btnChooseBgColor.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));
			btnChooseTextColor.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));
			
			btnRestoreDefaultSettings.setText(properties.getProperty("Sf_Cd_ButtonRestoreDefaultSettings", Constants.SF_BUTTONS_RESTORE_DEFAULT_SETTINGS));
		}
		
		
		else {
			setBorder(BorderFactory.createTitledBorder(Constants.SF_OEP_BORDER_TITLE));
			
			chcbShowReferenceVariables.setText(Constants.SF_OEP_CHCB_SHOW_REFERENCE_VARIABLES_TEXT);
			chcbShowReferenceVariables.setToolTipText(Constants.SF_OEP_CHCB_SHOW_REFERENCE_VARIABLES_TT);
			
			lblBgColor.setText(Constants.SF_CE_LBL_BACKGROUND_COLOR + ": ");
			lblTextColor.setText(Constants.SF_LBL_TEXT_COLOR_CELL + ": ");
			lblFontSize.setText(Constants.SF_LBL_FONT_SIZE_CELL + ": ");
			lblFontStyle.setText(Constants.SF_LBL_FONT_STYLE_CELL+ ": ");
			
			
			txtChooseTextColorDialog = Constants.SF_TXT_CHOOSE_TEXT_COLOR;
			txtChooseBgColorDialog =  Constants.SF_TXT_CHOOSE_BG_COLOR;
			
			
			btnChooseBgColor.setText(Constants.SF_CD_BTN_CHOOSE_COLOR);
			btnChooseTextColor.setText(Constants.SF_CD_BTN_CHOOSE_COLOR);
			
			btnRestoreDefaultSettings.setText(Constants.SF_BUTTONS_RESTORE_DEFAULT_SETTINGS);
		}
		
		chcbShowReferenceVariables.setText("? " + chcbShowReferenceVariables.getText());
	}
	
	
	
	
	
	

	@Override
	public void saveSetting() {
		EditProperties outputEditorProp = App.READ_FILE
				.getPropertiesInWorkspacePersistComments(Constants.OUTPUT_EDITOR_NAME);

		if (outputEditorProp == null) {
			final String pathToWorkspace = ReadFile.getPathToWorkspace();

			/*
			 * Zde pouze zjisšťuji, zda existují veškeré adresáře k cestě pro soubor
			 * OutputEditor.properties v adresáři configuration ve workspace. Pokud ne, pak v
			 * rámci možností nekteré adresáře mohu vytvořit, jinak není kam uložit soubory,
			 * pokud například byl smazan workspace apod. Pak už zbývá jedině znovu vytvořit
			 * adresář workspace s projekty apod. Ale tuto část zde vynechávám, protože o
			 * tom uživatel musí vědět - že si smazal projekty případně workspace apod. Tak
			 * snad musí vědět, že nelze pracovat s projekty / soubory, které byly smazány.
			 * 
			 * Otestuji, zda existuje workspace, pak configuration, pak příslušný soubor,
			 * případně některé z nich mohu vytvořit:
			 */
			if (pathToWorkspace != null) {
				acquireMutex();
				
				/*
				 * Pokud neexsistuje adresář configuration ve workspace, bude vytvořen i s
				 * potřebnými soubory s výchozím nastavením.
				 * 
				 * Pokud adresář configuration ve workspace existuje, pak stačí vytvořit jen
				 * soubor OutputEditor.properties, protože vím, že neexistuje, když se jej
				 * nepodařilo načíst.
				 */
				if (ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
					ConfigurationFiles.writeOutputEditorProperties(
							pathToWorkspace + File.separator + Constants.PATH_TO_OUTPUT_EDITOR_PROPERTIES);

				else
					WriteToFile.createConfigureFilesInPath(pathToWorkspace);

				// Znovu načtu soubor Default.properties z workspace z configuration, protože
				// výše byl znovu vytvořn:
				outputEditorProp = App.READ_FILE.getPropertiesInWorkspacePersistComments(Constants.OUTPUT_EDITOR_NAME);
				
				releaseMutex();
			}
			/*
			 * část else:
			 * 
			 * Zde by bylo možné vytvořit dříve zadaný adresář workspace - pokud byl již
			 * zadán, ale byl smazán, ale to by v rámci aplikace nemělo nastat, proto tuto
			 * část vynechám, uživatel jej musí smazat sám, pak o tom musí vědět.
			 */
		}
		
		
		
		
		
		
		if (outputEditorProp != null) {
			// Zde se podařilo načíst soubour, tak do něj vložím upravená data a uložím ho zpět:			
			
			// Výpis referenčních proměnných:
			outputEditorProp.setProperty("ShowReferenceVariables", String.valueOf(chcbShowReferenceVariables.isSelected()));
			
			// Barva pozadí:
			outputEditorProp.setProperty("BackgroundColor", Integer.toString(bgColor.getRGB()));
			
			// Barva písma:
			outputEditorProp.setProperty("ForegroundColor", Integer.toString(textColor.getRGB()));
			
			// Velikost písma:
			outputEditorProp.setProperty("FontSize", Integer.toString((Integer) cmbFontSize.getSelectedItem()));
			
			// Typ písma:
			final int fontStyle = cmbFontStyle.getSelectedIndex();
			
			if (fontStyle == 0)
				outputEditorProp.setProperty("FontStyle", Integer.toString(1));
			else if (fontStyle == 1)
				outputEditorProp.setProperty("FontStyle", Integer.toString(2));
			else if (fontStyle == 2)
				outputEditorProp.setProperty("FontStyle", Integer.toString(0));
			
			
			
			
			
			// Zápis zpět do soubour:
			outputEditorProp.save();
			
			// zavolat metodu v editoru pro přenačtení editoru !!
			// Vlastně je to v podstatě aktualizace editoru - změna nastavení:
			outputEditor.setSettingsEditor();
		}
	}

	

	
	
	@Override
	public void openSettings() {
		Properties outputEditorProp = App.READ_FILE.getOutputEditorPropertiesFromWorkspaceOnly();
		
		if (outputEditorProp == null) {
			final String pathToWorkspace = ReadFile.getPathToWorkspace();

			/*
			 * Zde pouze zjisšťuji, zda existují veškeré adresáře k cestě pro soubor
			 * OutputEditor.properties v adresáři configuration ve workspace. Pokud ne, pak v
			 * rámci možností nekteré adresáře mohu vytvořit, jinak není kam uložit soubory,
			 * pokud například byl smazan workspace apod. Pak už zbývá jedině znovu vytvořit
			 * adresář workspace s projekty apod. Ale tuto část zde vynechávám, protože o
			 * tom uživatel musí vědět - že si smazal projekty případně workspace apod. Tak
			 * snad musí vědět, že nelze pracovat s projekty / soubory, které byly smazány.
			 * 
			 * Otestuji, zda existuje workspace, pak configuration, pak příslušný soubor,
			 * případně některé z nich mohu vytvořit:
			 */
			if (pathToWorkspace != null) {
				acquireMutex();
				
				/*
				 * Pokud neexsistuje adresář configuration ve workspace, bude vytvořen i s
				 * potřebnými soubory s výchozím nastavením.
				 * 
				 * Pokud adresář configuration ve workspace existuje, pak stačí vytvořit jen
				 * soubor OutputEditor.properties, protože vím, že neexistuje, když se jej nepodařilo
				 * načíst.
				 */
				if (ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
					ConfigurationFiles.writeOutputEditorProperties(
							pathToWorkspace + File.separator + Constants.PATH_TO_OUTPUT_EDITOR_PROPERTIES);

				else
					WriteToFile.createConfigureFilesInPath(pathToWorkspace);

				// Znovu načtu soubor Default.properties z workspace z configuration, protože
				// výše byl znovu vytvořn:
				outputEditorProp = App.READ_FILE.getOutputEditorPropertiesFromWorkspaceOnly();
				
				releaseMutex();
			}
			/*
			 * část else:
			 * 
			 * Zde by bylo možné vytvořit dříve zadaný adresář workspace - pokud byl již
			 * zadán, ale byl smazán, ale to by v rámci aplikace nemělo nastat, proto tuto
			 * část vynechám, uživatel jej musí smazat sám, pak o tom musí vědět.
			 */
		}
		
		
		
		
		
		if (outputEditorProp != null) {
			// Zde se podařilo soubor načíst, do vyplním komponenty v okně příslušnými daty:
			
			chcbShowReferenceVariables.setSelected(Boolean.parseBoolean(outputEditorProp.getProperty("ShowReferenceVariables", String.valueOf(Constants.OUTPUT_EDITOR_SHOW_REFERENCE_VARIABLES))));
			
			final String backgroundColor = outputEditorProp.getProperty("BackgroundColor", Integer.toString(Constants.DEFAULT_BACKGROUND_COLOR_FOR_OUTPUT_EDITOR.getRGB()));
			bgColor = new Color(Integer.parseInt(backgroundColor));
			lblChoosedBgColor.setForeground(bgColor);
			lblChoosedBgColor.setBackground(bgColor);
			
			
			final String textColorText = outputEditorProp.getProperty("ForegroundColor", Integer.toString(Constants.DEFAULT_TEXT_COLOR_FOR_OUTPUT_EDITOR.getRGB()));
			textColor = new Color(Integer.parseInt(textColorText));
			lblChoosedTextColor.setForeground(textColor);
			lblChoosedTextColor.setBackground(textColor);
			
			
			
			final int fontSizeFromFile = Integer.parseInt(outputEditorProp.getProperty("FontSize", String.valueOf(Constants.DEFAULT_FONT_FOR_OUTPUT_EDITOR.getSize())));
			if (fontSizeFromFile >= 8 && fontSizeFromFile <= 100)
				cmbFontSize.setSelectedItem(fontSizeFromFile);
			
			
			
			final int fontStyle = Integer.parseInt(outputEditorProp.getProperty("FontStyle", Integer.toString(Constants.DEFAULT_FONT_FOR_OUTPUT_EDITOR.getStyle())));
			if (fontStyle == 0)		// Plain
				cmbFontStyle.setSelectedIndex(2);
			else if (fontStyle == 1)	// Bold
				cmbFontStyle.setSelectedIndex(0);
			else if (fontStyle == 2)	// Italic
				cmbFontStyle.setSelectedIndex(1);
		}
	}


	


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnChooseBgColor) {
				final Color tempVariable = JColorChooser.showDialog(this, txtChooseBgColorDialog,
						Constants.DEFAULT_BACKGROUND_COLOR_FOR_OUTPUT_EDITOR);
				
				if (tempVariable != null) {
					bgColor = tempVariable;
					lblChoosedBgColor.setForeground(bgColor);
					lblChoosedBgColor.setBackground(bgColor);
				}
			}
			
			
			else if (e.getSource() == btnChooseTextColor) {
				final Color tempVariable = JColorChooser.showDialog(this, txtChooseTextColorDialog,
						Constants.DEFAULT_TEXT_COLOR_FOR_OUTPUT_EDITOR);
				
				if (tempVariable != null) {
					textColor = tempVariable;
					lblChoosedTextColor.setForeground(textColor);
					lblChoosedTextColor.setBackground(textColor);
				}
			}
			
			
			else if (e.getSource() == btnRestoreDefaultSettings) {
				chcbShowReferenceVariables.setSelected(Constants.OUTPUT_EDITOR_SHOW_REFERENCE_VARIABLES);
				
				bgColor = Constants.DEFAULT_BACKGROUND_COLOR_FOR_OUTPUT_EDITOR;
				lblChoosedBgColor.setForeground(bgColor);
				lblChoosedBgColor.setBackground(bgColor);
				
				textColor = Constants.DEFAULT_TEXT_COLOR_FOR_OUTPUT_EDITOR;
				lblChoosedTextColor.setBackground(textColor);
				lblChoosedTextColor.setForeground(textColor);
				
				
				cmbFontSize.setSelectedItem(Constants.DEFAULT_FONT_FOR_OUTPUT_EDITOR.getSize());
				
				final int fontStyle = Constants.DEFAULT_FONT_FOR_OUTPUT_EDITOR.getStyle();
				if (fontStyle == 0)		// Plain
					cmbFontStyle.setSelectedIndex(2);
				else if (fontStyle == 1)	// Bold
					cmbFontStyle.setSelectedIndex(0);
				else if (fontStyle == 2)	// Italic
					cmbFontStyle.setSelectedIndex(1);
			}
		}
	}
}