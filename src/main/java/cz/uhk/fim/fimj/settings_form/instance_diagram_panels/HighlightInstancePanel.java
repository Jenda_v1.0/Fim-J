package cz.uhk.fim.fimj.settings_form.instance_diagram_panels;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.language.EditProperties;
import cz.uhk.fim.fimj.settings_form.PanelAbstract;
import cz.uhk.fim.fimj.settings_form.ReadWriteDiagramProperties;

/**
 * Tato třída slouží pro nastavení vlastností pro buňku, která v diagramu instancí reprezentuje instance, ale v případě,
 * že se jedná o zvýraznění příslušné buňky / instance v diagramu instancí.
 * <p>
 * Tzn. Například, když uživatel zavolá metodu, která vrátí nějakou instanci, která se aktuálně nachází v digramu
 * instancí, tak aby se ta instance v diagramu instancí zvýraznila, tak parametry té zvýrazněné buňky / instance se
 * nastavují v "této třídě".
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class HighlightInstancePanel extends PanelAbstract
		implements ActionListener, LanguageInterface, ReadWriteDiagramProperties {

	private static final long serialVersionUID = 1L;
	
	
	/**
	 * Komponenta, která slouží pro nastavení toho, zda se vůbec mají zvýrazňovat
	 * instance v diagramu instancí. (Například po zavolání metody apod.)
	 */
	private final JCheckBox chcbHighlightInstances;


	

	/**
	 * Komponenta, která slouží pro zobrazení textu s informací, že u té komponenty
	 * má nastavit čas, jak dlouho bude instance zvýrazněna.
	 */
	private final JLabel lblHighlightingTime;

	/**
	 * Komponenta, která slouží pro nastavení doby, po kterou má být buňka
	 * reprezentující instanci v diagramu instancí zvýrazněna.
	 */
	private final JComboBox<Double> cmbHighlightingTime;
	
	
	
	
	
	
	/**
	 * Komponenta, která slouží pro zobrazení příslušného textu, který "říká"
	 * uživateli, že "zde" si může nastavit barvu písma ve zvýrazněné instanci /
	 * buňce v diagramu instancí.
	 */
	private final JLabel lblTextColor;

	/**
	 * Tlačítko, které slouží pro nastavení barvy písma ve zvýrazněné instanci.
	 */
	private final JButton btnTextColor;

	/**
	 * Label, který bude sloužit jako ukázka nastavené barvy písma ve zvýrazněné
	 * instanci.
	 */
	private final JLabel lblTextColorExample;
	
	/**
	 * Proměná pro dočasné uložení zvolené barvy pro barvu písma v buňce / instanci,
	 * která bude zvýrazněná.
	 */
	private Color tmpTextColor;
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Komponenta pro zobrazení textu, který "říká" uživateli, že si na příslušném
	 * místě může nastavit barvu písma pro atributy v buňce / instanci v diagramu
	 * instancí když bude ta instance zvýrazněna.
	 */
	private final JLabel lblAttributeColor;

	/**
	 * Tlačítko, které slouží pro nastavení barvy písma pro atributy v instanci
	 * třídy.
	 */
	private final JButton btnAttributeColor;

	/**
	 * Label, který bude slouží pro ukázku nastavené barvy písma pro atributy ve
	 * zvýrazněné instanci.
	 */
	private final JLabel lblAttributeColorExample;

	/**
	 * Proměnná pro dočasné uložení zvolené barvy písma pro atributy v buňce /
	 * instanci, která bude zvýrazněna.
	 */
	private Color tmpAttributeColor;
	
	
	
	
	
	
	
	
	
	
	/**
	 * Komponenta pro zobrazení textu, který "říkaá", že na příslušném místě si může
	 * uživatel nastavit barvu písma pro metody v buňce / instanci v diagramu
	 * instancí, která se využije, když bude příslušná instance zvýrazněna.
	 */
	private final JLabel lblMetodColor;

	/**
	 * Tlačítko, které slouží pro zvolení barvy písma pro metody, které se mohou
	 * nacházet v buňce / instanci, když bude zvýrazněna.
	 */
	private final JButton btnMethodColor;

	/**
	 * Label, který slouží pro ukázku nastavené barvy písma metod v buňce / instanci
	 * když bude zvýrazněna.
	 */
	private final JLabel lblMethodColorExample;

	/**
	 * Proměnná pro "dočasné" uložení zvolené barvy písma pro metody v buňce /
	 * instanci, která bude zvýrazněna.
	 */
	private Color tmpMethodColor;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Komponenta, která slouží pro zobrazení příslušného textu, který "říká"
	 * uživateli, že "zde" si může nastavit barvu ohraníčení zvýrazněné instance /
	 * buňky v diagramu instancí.
	 */
	private final JLabel lblBorderColor;
	
	/**
	 * Tlačítko, které slouží pro nastavení barvy ohraničení zvýrazněné instance.
	 */
	private final JButton btnBorderColor;
	
	/**
	 * Label, který bude sloužit jako ukázka nastavené barvy ohraníčení zvýrazněné
	 * instance.
	 */
	private final JLabel lblBorderColorExample;
	
	/**
	 * Proměná pro dočasné uložení zvolené barvy pro barvu ohraničení buňky /
	 * instance, která bude zvýrazněná.
	 */
	private Color tmpBorderColor;
	
	
	
	
	
	
	
	
	
	/**
	 * Komponenta, která slouží pro zobrazení příslušného textu, který "říká"
	 * uživateli, že "zde" si může nastavit barvu pozadí zvýrazněné instance / buňky
	 * v diagramu instancí.
	 */
	private final JLabel lblBackgroundColor;
	
	/**
	 * Tlačítko, které slouží pro nastavení barvy pozadí zvýrazněné instance.
	 */
	private final JButton btnBackgroundColor;
		
	/**
	 * Label, který bude sloužit jako ukázka nastavené barvy pozadí zvýrazněné
	 * instance.
	 */
	private final JLabel lblBackgroundColorExample;
	
	/**
	 * Proměná pro dočasné uložení zvolené barvy pro barvu pozadí buňky / instance,
	 * která bude zvýrazněná.
	 */
	private Color tmpBackgroundColor;
	

	
	
	
	
	

	// Proměnné pro texty v uživatelem zvoleném jazyce do dialogu pro výběr barev:
	private static String txtChooseTextColor, txtChooseAttributeColor, txtChooseMethodColor, txtChooseBorderColor,
			txtChooseBackgroundColor;
	
	
	
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 */
	public HighlightInstancePanel() {
		super();

		setLayout(new GridBagLayout());
		
		index = 0;
		
		final GridBagConstraints gbc = getGbc();
		
		
		// Zda se mají zvýrazňovat instance:
		chcbHighlightInstances = new JCheckBox();
		chcbHighlightInstances.addActionListener(this);
		gbc.gridwidth = 3;
		setGbc(gbc, index, 0, chcbHighlightInstances, this);
		
		
		gbc.gridwidth = 1;
		
		
		
		// Čas zvýraznění:
		lblHighlightingTime = new JLabel();
		setGbc(gbc, ++index, 0, lblHighlightingTime, this);
			
		cmbHighlightingTime = new JComboBox<>(getHighlightingTimeCmbModel());
		gbc.gridwidth = 2;
		setGbc(gbc, index, 1, cmbHighlightingTime, this);
		
		gbc.gridwidth = 1;
		
		
		
		
		// Barva písma:
		lblTextColor = new JLabel();
		setGbc(gbc, ++index, 0, lblTextColor, this);
		
		btnTextColor = new JButton();
		btnTextColor.addActionListener(this);
		setGbc(gbc, index, 1, btnTextColor, this);
		
		lblTextColorExample = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblTextColorExample.setOpaque(true);
		setGbc(gbc, index, 2, lblTextColorExample, this);
		
		
		
		
		
		
		// Barva atributů:
		lblAttributeColor = new JLabel();
		setGbc(gbc, ++index, 0, lblAttributeColor, this);
		
		btnAttributeColor = new JButton();
		btnAttributeColor.addActionListener(this);
		setGbc(gbc, index, 1, btnAttributeColor, this);
		
		lblAttributeColorExample = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblAttributeColorExample.setOpaque(true);
		setGbc(gbc, index, 2, lblAttributeColorExample, this);
		
		
		
		
		
		
		
		// Barva metod:
		lblMetodColor = new JLabel();
		setGbc(gbc, ++index, 0, lblMetodColor, this);
		
		btnMethodColor = new JButton();
		btnMethodColor.addActionListener(this);
		setGbc(gbc, index, 1, btnMethodColor, this);
		
		lblMethodColorExample = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblMethodColorExample.setOpaque(true);
		setGbc(gbc, index, 2, lblMethodColorExample, this);
		
		
		
		
		
		
		
		
		// Barva ohraničení:
		lblBorderColor = new JLabel();
		setGbc(gbc, ++index, 0, lblBorderColor, this);
		
		btnBorderColor = new JButton();
		btnBorderColor.addActionListener(this);
		setGbc(gbc, index, 1, btnBorderColor, this);
		
		lblBorderColorExample = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblBorderColorExample.setOpaque(true);
		setGbc(gbc, index, 2, lblBorderColorExample, this);
		
		
		
		
		
		
		
		// Barva pozadí:
		lblBackgroundColor = new JLabel();
		setGbc(gbc, ++index, 0, lblBackgroundColor, this);
		
		btnBackgroundColor = new JButton();
		btnBackgroundColor.addActionListener(this);
		setGbc(gbc, index, 1, btnBackgroundColor, this);
		
		lblBackgroundColorExample = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblBackgroundColorExample.setOpaque(true);
		setGbc(gbc, index, 2, lblBackgroundColorExample, this);
	}

	
	
	
	
	
	/**
	 * Metoda, která vytvoří model pro komponentu JComboBox, který slouží pro
	 * nastavení doby pro zvýraznění instance.
	 * 
	 * Model bude jednorozměrné pole typu double, které bude obsahovat hodnotu od
	 * 0.1 do 5.0 s tím, že každá hodnota značí počet vteřin - jak dlouho bude
	 * zvýrazněna instance.
	 * 
	 * @return jednorozměrné pole typu Double, které bude obsahovat hodnoty od 0.1
	 *         do 5.0.
	 */
	private static Double[] getHighlightingTimeCmbModel() {
		final List<Double> model = new ArrayList<>();

		for (int i = 0; i < 5; i++) {
			for (int q = 0; q < 10; q++) {
				/*
				 * Pokud to bude úplné první iterace, tj .hodnoty: 0.0, tak budu pokračovat
				 * další iterací, proto tuto hodnotu zde nechci.
				 */
				if (i == 0 && q == 0)
					continue;

				// Zde přidám získanou hodnotu:
				model.add(Double.parseDouble(i + "." + q));
			}
		}

		model.add(5.5d);

		// Vrátím získané pole:
		return model.toArray(new Double[] {});
	}
	
	
	
	
	
	
	
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JCheckBox) {
			if (e.getSource() == chcbHighlightInstances)
				setEnabledComponents(chcbHighlightInstances.isSelected());
		}
		
		
		
		else if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnTextColor) {
				final Color temp = JColorChooser.showDialog(this, txtChooseTextColor,
						Constants.ID_TEXT_COLOR_OF_HIGHLIGHTED_INSTANCE);

				if (temp != null) {
					tmpTextColor = temp;
					setSelectedTextColor(tmpTextColor);
				}
			}
			
			
			else if (e.getSource() == btnAttributeColor) {
				final Color temp = JColorChooser.showDialog(this, txtChooseAttributeColor,
						Constants.ID_ATTRIBUTE_COLOR_OF_HIGHLIGHTED_INSTANCE);

				if (temp != null) {
					tmpAttributeColor = temp;
					setSelectedAttributeColor(tmpAttributeColor);
				}
			}
			
			
			else if (e.getSource() == btnMethodColor) {
				final Color temp = JColorChooser.showDialog(this, txtChooseMethodColor,
						Constants.ID_METHOD_COLOR_OF_HIGHLIGHTED_INSTANCE);

				if (temp != null) {
					tmpMethodColor = temp;
					setSelectedMethodColor(tmpMethodColor);
				}
			}

			
			else if (e.getSource() == btnBorderColor) {
				final Color temp = JColorChooser.showDialog(this, txtChooseBorderColor,
						Constants.ID_BORDER_COLOR_OF_HIGHLIGHTED_INSTANCE);

				if (temp != null) {
					tmpBorderColor = temp;
					setSelectedBorderColor(tmpBorderColor);
				}
			}
			

			else if (e.getSource() == btnBackgroundColor) {
				final Color temp = JColorChooser.showDialog(this, txtChooseBackgroundColor,
						Constants.ID_BACKGROUND_COLOR_OF_HIGHLIGHTED_INSTANCE);

				if (temp != null) {
					tmpBackgroundColor = temp;
					setSelectedBackgroundColor(tmpBackgroundColor);
				}
			}
		}
	}

	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro zpřístupnění / zněpřístupněné některých komponent v
	 * tomto panelu. Zpřístupní / zněpřístupní se zde komponenty, které slouží pro
	 * nastavení nějakého vzhledu nebo času zvýraznění zvýrazněné buňky instance.
	 * 
	 * @param enabled
	 *            - true - maji se zpřístupnit výše uvedené kompoenty, false - mají
	 *            se znepřístupnit výše uvedené komponenty.
	 */
	private void setEnabledComponents(final boolean enabled) {
		// Čas:
		lblHighlightingTime.setEnabled(enabled);
		cmbHighlightingTime.setEnabled(enabled);

		// Barva písma:
		lblTextColor.setEnabled(enabled);
		btnTextColor.setEnabled(enabled);

		// Barva atributů:
		lblAttributeColor.setEnabled(enabled);
		btnAttributeColor.setEnabled(enabled);
		
		// Barva metod:
		lblMetodColor.setEnabled(enabled);
		btnMethodColor.setEnabled(enabled);
		
		// Barva ohraničení:
		lblBorderColor.setEnabled(enabled);
		btnBorderColor.setEnabled(enabled);

		// Barva pozadí:
		lblBackgroundColor.setEnabled(enabled);
		btnBackgroundColor.setEnabled(enabled);
		
		/*
		 * Note:
		 * Label, který reprezentuje nastavenou barvu nebudu znepřístupňovat, jinak by
		 * byl vidět ten text.
		 */
	}
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public EditProperties writeDataToDiagramProp(final EditProperties instanceDiagramProp) {
		instanceDiagramProp.setProperty("HighlightInstances", String.valueOf(chcbHighlightInstances.isSelected()));

		instanceDiagramProp.setProperty("HighlightingTime", cmbHighlightingTime.getSelectedItem().toString());

		
		if (tmpTextColor != null)
			instanceDiagramProp.setProperty("TextColorOfHighlightedInstance", Integer.toString(tmpTextColor.getRGB()));

		if (tmpAttributeColor != null)
			instanceDiagramProp.setProperty("AttributeColorOfHighlightedInstance",
					Integer.toString(tmpAttributeColor.getRGB()));

		if (tmpMethodColor != null)
			instanceDiagramProp.setProperty("MethodColorOfHighlightedInstance",
					Integer.toString(tmpMethodColor.getRGB()));

		if (tmpBorderColor != null)
			instanceDiagramProp.setProperty("BorderColorOfHighlightedInstance",
					Integer.toString(tmpBorderColor.getRGB()));

		
		if (tmpBackgroundColor != null)
			instanceDiagramProp.setProperty("BackgroundColorOfHighlightedInstance",
					Integer.toString(tmpBackgroundColor.getRGB()));

		
		return instanceDiagramProp;
	}
	
	
	
	
	
	

	
	
	
	
	@Override
	public void readDataFromDiagramProp(final Properties instanceDiagramPro) {
		chcbHighlightInstances.setSelected(Boolean.parseBoolean(instanceDiagramPro.getProperty("HighlightInstances", String.valueOf(Constants.ID_HIGHLIGHT_INSTANCES))));
		// Nastavím zpřístupněné komponenty:
		setEnabledComponents(chcbHighlightInstances.isSelected());
		
		
		final double tempTime = Double.parseDouble(instanceDiagramPro.getProperty("HighlightingTime", String.valueOf(Constants.ID_DEFAULT_HIGHLIGHTING_TIME)));
		setCmbTimeSelectedItem(tempTime);
		

		// Barva písma:
		final String textColorTemp = instanceDiagramPro.getProperty("TextColorOfHighlightedInstance", Integer.toString(Constants.ID_TEXT_COLOR_OF_HIGHLIGHTED_INSTANCE.getRGB()));
		final Color tempTextColor = new Color(Integer.parseInt(textColorTemp));
		setSelectedTextColor(tempTextColor);
		
		
		// Barva atribuů:
		final String attributeColorTemp = instanceDiagramPro.getProperty("AttributeColorOfHighlightedInstance", Integer.toString(Constants.ID_ATTRIBUTE_COLOR_OF_HIGHLIGHTED_INSTANCE.getRGB()));
		final Color tempAttributeColor = new Color(Integer.parseInt(attributeColorTemp));
		setSelectedAttributeColor(tempAttributeColor);
		
		// Barva metod:
		final String methodColorTemp = instanceDiagramPro.getProperty("MethodColorOfHighlightedInstance", Integer.toString(Constants.ID_METHOD_COLOR_OF_HIGHLIGHTED_INSTANCE.getRGB()));
		final Color tempMethodColor = new Color(Integer.parseInt(methodColorTemp));
		setSelectedMethodColor(tempMethodColor);
		
		
		// Barva ohraničení:
		final String borderColorTemp = instanceDiagramPro.getProperty("BorderColorOfHighlightedInstance", Integer.toString(Constants.ID_BORDER_COLOR_OF_HIGHLIGHTED_INSTANCE.getRGB()));
		final Color tempBorderColor = new Color(Integer.parseInt(borderColorTemp));
		setSelectedBorderColor(tempBorderColor);

		
		// Barva pozadí:
		final String backgroundColorTemp = instanceDiagramPro.getProperty("BackgroundColorOfHighlightedInstance", Integer.toString(Constants.ID_BACKGROUND_COLOR_OF_HIGHLIGHTED_INSTANCE.getRGB()));
		final Color tempBackgroundColor = new Color(Integer.parseInt(backgroundColorTemp));
		setSelectedBackgroundColor(tempBackgroundColor);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která nstaví výchozí hodnotu pro komponentu JComboBox,k terá značí
	 * čas, jak dlouho bude instance zvýrazněna.
	 * 
	 * @param time
	 *            - čas, který se má nastavit jako označený.
	 */
	public final void setCmbTimeSelectedItem(final double time) {
		final int length = cmbHighlightingTime.getModel().getSize();

		for (int i = 0; i < length; i++) {
			final double value = cmbHighlightingTime.getModel().getElementAt(i);

			if (value == time) {
				cmbHighlightingTime.setSelectedIndex(i);
				break;
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			// Titulek panelu:
			setBorder(BorderFactory.createTitledBorder(properties.getProperty("Hip_BorderTitle", Constants.HIP_BODER_TITLE)));
			
			// Zda se mají zvýrazňovat instance:
			chcbHighlightInstances.setText(properties.getProperty("Hip_Chcb_HighlightInstancesText", Constants.HIP_CHCB_HIGHLIGHT_INSTANCES_TEXT));
			chcbHighlightInstances.setToolTipText(properties.getProperty("Hip_Chcb_HighlightInstancesTT", Constants.HIP_CHCB_HIGHLIGHT_INSTANCES_TT));
			
			// Čas, jak dlouho mají být zvýrazněné instance:
			lblHighlightingTime.setText(properties.getProperty("Hip_Lbl_HighlightingTimeText", Constants.HIP_LBL_HIGHLIGHTING_TIME_TEXT));
			lblHighlightingTime.setToolTipText(properties.getProperty("Hip_Lbl_HighlightingTimeTT", Constants.HIP_LBL_HIGHLIGHTING_TIME_TT));
			
			
			// Label pro výběr barvy písma ve zvýrazněné instanci:
			lblTextColor.setText(properties.getProperty("Hip_Lbl_TextColorText", Constants.HIP_LBL_TEXT_COLOR_TEXT));
			lblTextColor.setToolTipText(properties.getProperty("Hip_Lbl_TextColorTT", Constants.HIP_LBL_TEXT_COLOR_TT));

			lblAttributeColor.setText(properties.getProperty("Hip_Lbl_AttributeColorText", Constants.HIP_LBL_ATTRIBUTE_COLOR_TEXT));
			lblAttributeColor.setToolTipText(properties.getProperty("Hip_Lbl_AttributeColorTT", Constants.HIP_LBL_ATTRIBUTE_COLOR_TT));
			
			lblMetodColor.setText(properties.getProperty("Hip_Lbl_MethodColorText", Constants.HIP_LBL_METHOD_COLOR_TEXT));
			lblMetodColor.setToolTipText(properties.getProperty("Hip_Lbl_MethodColorTT", Constants.HIP_LBL_METHOD_COLOR_TT));
			
			// Label pro výběr barvy ohnraničení zvýrazněné instance:
			lblBorderColor.setText(properties.getProperty("Hip_Lbl_BorderColorText", Constants.HIP_LBL_BORDER_COLOR_TEXT));
			lblBorderColor.setToolTipText(properties.getProperty("Hip_Lbl_BorderColorTT", Constants.HIP_LBL_BORDER_COLOR_TT));

			// Label pro výběr barvy pozadí zvýrazněné instance:
			lblBackgroundColor.setText(properties.getProperty("Hip_Lbl_BackgroundColorText", Constants.HIP_LBL_BACKGROUND_COLOR_TEXT));
			lblBackgroundColor.setToolTipText(properties.getProperty("Hip_Lbl_BackgroundColorTT", Constants.HIP_LBL_BACKGROUND_COLOR_TT));
			
			
			// Tlčítka pro označení barev písma, ohraničení a pozadí instance:
			final String btnChooseColor = properties.getProperty("Hip_Btn_ChooseColor", Constants.HIP_BTN_CHOOSE_COLOR);
			
			btnTextColor.setText(btnChooseColor);
			btnAttributeColor.setText(btnChooseColor);
			btnMethodColor.setText(btnChooseColor);
			btnBorderColor.setText(btnChooseColor);
			btnBackgroundColor.setText(btnChooseColor);
			
			
			// Texty pro dialogy pro výběry barev:
			txtChooseTextColor = properties.getProperty("Hip_Txt_ChooseTextColorTitle", Constants.HIP_TXT_CHOOSE_TEXT_COLOR_TITLE);
			txtChooseAttributeColor = properties.getProperty("Hip_Txt_ChooseAttributeColorTitle", Constants.HIP_TXT_CHOOSE_ATTRIBUTE_COLOR_TITLE);
			txtChooseMethodColor = properties.getProperty("Hip_Txt_ChooseMethodColorTitle", Constants.HIP_TXT_CHOOSE_METHOD_COLOR_TITLE);
			txtChooseBorderColor = properties.getProperty("Hip_Txt_ChooseBorderColorTitle", Constants.HIP_TXT_CHOOSE_BORDER_COLOR_TITLE);
			txtChooseBackgroundColor = properties.getProperty("Hip_Txt_ChooseBackgroundColor", Constants.HIP_TXT_CHOOSE_BACKGROUND_COLOR);
		}
		
		
		else {
			// Titulek panelu:
			setBorder(BorderFactory.createTitledBorder(Constants.HIP_BODER_TITLE));

			// Zda se mají zvýrazňovat instance:
			chcbHighlightInstances.setText(Constants.HIP_CHCB_HIGHLIGHT_INSTANCES_TEXT);
			chcbHighlightInstances.setToolTipText(Constants.HIP_CHCB_HIGHLIGHT_INSTANCES_TT);

			// Čas, jak dlouho mají být zvýrazněné instance:
			lblHighlightingTime.setText(Constants.HIP_LBL_HIGHLIGHTING_TIME_TEXT);
			lblHighlightingTime.setToolTipText(Constants.HIP_LBL_HIGHLIGHTING_TIME_TT);

			// Label pro výběr barvy písma ve zvýrazněné instanci:
			lblTextColor.setText(Constants.HIP_LBL_TEXT_COLOR_TEXT);
			lblTextColor.setToolTipText(Constants.HIP_LBL_TEXT_COLOR_TT);
			
			// Label pro výběr barvy písma pro atributy ve zvýrazněné instanci:
			lblAttributeColor.setText(Constants.HIP_LBL_ATTRIBUTE_COLOR_TEXT);
			lblAttributeColor.setToolTipText(Constants.HIP_LBL_ATTRIBUTE_COLOR_TT);
			
			// Label pro výběr barvy písma pro metody ve zvýrazněné instanci:
			lblMetodColor.setText(Constants.HIP_LBL_METHOD_COLOR_TEXT);
			lblMetodColor.setToolTipText(Constants.HIP_LBL_METHOD_COLOR_TT);

			// Label pro výběr barvy ohnraničení zvýrazněné instance:
			lblBorderColor.setText(Constants.HIP_LBL_BORDER_COLOR_TEXT);
			lblBorderColor.setToolTipText(Constants.HIP_LBL_BORDER_COLOR_TT);

			// Label pro výběr barvy pozadí zvýrazněné instance:
			lblBackgroundColor.setText(Constants.HIP_LBL_BACKGROUND_COLOR_TEXT);
			lblBackgroundColor.setToolTipText(Constants.HIP_LBL_BACKGROUND_COLOR_TT);

			// Tlčítka pro označení barev písma, ohraničení a pozadí instance:
			final String btnChooseColor = Constants.HIP_BTN_CHOOSE_COLOR;

			btnTextColor.setText(btnChooseColor);
			btnBorderColor.setText(btnChooseColor);
			btnBackgroundColor.setText(btnChooseColor);

			// Texty pro dialogy pro výběry barev:
			txtChooseTextColor = Constants.HIP_TXT_CHOOSE_TEXT_COLOR_TITLE;
			txtChooseAttributeColor = Constants.HIP_TXT_CHOOSE_ATTRIBUTE_COLOR_TITLE;
			txtChooseMethodColor = Constants.HIP_TXT_CHOOSE_METHOD_COLOR_TITLE;
			txtChooseBorderColor = Constants.HIP_TXT_CHOOSE_BORDER_COLOR_TITLE;
			txtChooseBackgroundColor = Constants.HIP_TXT_CHOOSE_BACKGROUND_COLOR;
		}
		
		
		
		chcbHighlightInstances.setText("? " + chcbHighlightInstances.getText());
		lblHighlightingTime.setText("? " + lblHighlightingTime.getText() + ": ");

		lblTextColor.setText("? " + lblTextColor.getText() + ": ");
		lblAttributeColor.setText("? " + lblAttributeColor.getText() + ": ");
		lblMetodColor.setText("? " + lblMetodColor.getText() + ": ");
		lblBorderColor.setText("? " + lblBorderColor.getText() + ": ");
		lblBackgroundColor.setText("? " + lblBackgroundColor.getText() + ": ");
	}
	
	
	
	
	
	
	
	
	
	
	public final void setSelectedChcbHighlightInstances(final boolean selected) {
		chcbHighlightInstances.setSelected(selected);
		setEnabledComponents(selected);
	}

	public final void setSelectedTextColor(final Color color) {
		tmpTextColor = color;
		lblTextColorExample.setBackground(color);
		lblTextColorExample.setForeground(color);
	}
	
	public final void setSelectedAttributeColor(final Color color) {
		tmpAttributeColor = color;
		lblAttributeColorExample.setBackground(color);
		lblAttributeColorExample.setForeground(color);
	}
	
	public final void setSelectedMethodColor(final Color color) {
		tmpMethodColor = color;
		lblMethodColorExample.setBackground(color);
		lblMethodColorExample.setForeground(color);
	}

	public final void setSelectedBorderColor(final Color color) {
		tmpBorderColor = color;
		lblBorderColorExample.setBackground(color);
		lblBorderColorExample.setForeground(color);
	}

	public final void setSelectedBackgroundColor(final Color color) {
		tmpBackgroundColor = color;
		lblBackgroundColorExample.setBackground(color);
		lblBackgroundColorExample.setForeground(color);
	}
}
