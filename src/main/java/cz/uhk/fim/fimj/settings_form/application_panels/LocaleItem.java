package cz.uhk.fim.fimj.settings_form.application_panels;

import java.text.DateFormat;
import java.util.Arrays;
import java.util.Locale;

/**
 * Tato třída slouží jako "objekt" pro Locale (globální umístění). Tento objekt se bude vkládat do komponenty JComboBox
 * v dialogu Nastavení v panelu Aplikace v části nastavení jazyka a komentářů.
 * <p>
 * Tento "objekt" obsahuje požadovaný Locale a další metody pro získání potřebných hodnot.
 * <p>
 * Tato třída slouží pouze pro to, aby jej bylo možné dát uživateli na výběr v uvedené komponěntě JComboBox, s
 * potřebnými informacemi pro zobrazení a samotný objekt Locale slouží pro vytvoření datumu vytvoření souboru v zadaném
 * formátu právě dle Locale tyto datumy budou zapsány do soubouů .properties obsahující texty pro tuto aplikaci.
 * <p>
 * Zdroje:
 * <p>
 * https://docs.oracle.com/javase/tutorial/i18n/locale/matching.html
 * <p>
 * https://docs.oracle.com/javase/7/docs/api/java/util/Locale.html#toLanguageTag%28%29
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class LocaleItem {

    /**
     * "Globální umístění", které si může uživatel zvolit, toto umístění je využito pro sestavení komentářů při
     * vytvoření souborů .properties obsahující texty pro aplikaci v nějakém jazyce.
     */
    private final Locale locale;


    /**
     * Konstruktor této třídy.
     *
     * @param locale
     *         - "Globální umístění", které tato třída reprezentuje.
     */
    LocaleItem(final Locale locale) {
        super();

        this.locale = locale;
    }


    /**
     * Konstruktor této třídy.
     * <p>
     * Tento konstruktor je využit pri získávání názvu pro Locale ze souboru.
     *
     * @param locale
     *         - text, který značí daný Locale -> jedna se o tzv. LanguageTag neboli index pro získání příslušného
     *         Locale.
     * @throws WrongLocaleException
     *         - výjimka může nastat v případě, že nebude nalzen objekt Locale pro využívaný JVM, ale toto by nastat
     *         nemělo, když už, tak spíše v případě, že uživatel sám zadá do logu, nevalidní index pro locale.
     */
    public LocaleItem(final String locale) throws WrongLocaleException {
        super();

        // Pokud je parametr null nebo je prázdný, nebo příslušný tag není v seznamu
        // dostupných Locales pro JVM, pak se vyhodí výjimka:
        if (locale == null || locale.replaceAll("\\s", "").isEmpty() || !containsLocalesLanguageTag(locale))
            throw new WrongLocaleException();

            // Zde je zadaný / načtený locale ze souboru v pořádku, tak jej zkusím najít:
        else
            this.locale = Locale.forLanguageTag(locale);
    }


    /**
     * Metoda, která zjistí, zda se languageTag v parametru metody vyskytuje v Locales dostupné pro daný JVM. Pokud ano,
     * vrátí se true, jinak false.
     *
     * @param languageTag
     *         - LanguageTag, o kterém se má zjistit, zda je v seznamu dostupných Locales v JVM.
     * @return True, pokud se languageTag nachází v seznamu dostupných Locales pro JVM, jinal false.
     */
    static boolean containsLocalesLanguageTag(final String languageTag) {
        return Arrays.stream(DateFormat.getAvailableLocales()).anyMatch(l -> l.toLanguageTag().equals(languageTag));
    }


    /**
     * Tato metoda vrátí reprezentaci daného Locale do textu, nejčastěji v podobně xx_YY nebo něco velice podobného.
     * <p>
     * Tento text lze využit například při porovnávání Locale nebo při získání "indexu" pro uložení daného Locale do
     * textového souboru, protože je možné dle tohoto "indexu" opět zpetně vytvořit daný Locale.
     *
     * @return výše popsaný locale ("index") v textové podobě.
     */
    public final String getTagToFile() {
        return locale.toLanguageTag();
    }


    /**
     * Metoda, která vrátí Locale, který reprezentuje tato třída.
     *
     * @return Locale v této třídě.
     */
    public final Locale getLocale() {
        return locale;
    }


    @Override
    public String toString() {
        // Tento název se využije při zobrazení v nastavení:
        return locale.getDisplayName();
    }
}
