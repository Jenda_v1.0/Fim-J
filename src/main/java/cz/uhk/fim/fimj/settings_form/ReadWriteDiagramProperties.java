package cz.uhk.fim.fimj.settings_form;

import java.util.Properties;

import cz.uhk.fim.fimj.language.EditProperties;

/**
 * Toto rozhraní obsahuje deklaraci hlaviček metod pro načtení a nastaven / uložení objektu Properties, který obsahuje
 * nastavení pro jeden z diagramů (diagram tříd nebo diagram instancí). Toto rozhraní a metody zde nejsou nějak významné
 * a nějak "extrémě" potřeba to není, v tomto případě se jedná spíše o takové "zpřehlednění" metod pro načtení objektu
 * Properties, která obsahuje nastavení pro diagram tříd a instancí tak, že se vždy do příslušného panelu načte nějaká
 * část toho diagramu, npaříkad do nějakého panelu zde načte část z třídami do dalšího část s komentáři, pak s hranami
 * apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface ReadWriteDiagramProperties {

    /**
     * Metoda, která do objektu properties (v parametru metody) zapíše veškerá data z příslušných komponent v dialogu
     * nastavení.
     * <p>
     * Jedná se o zapsání potenciálně upravených dat v dialogu nastavení zpět do objektu properties, aby se poznalo, že
     * uživatel změnil nastavení a má se někam uložit.
     *
     * @param properties
     *         - objekt Properties do kterého se zapíšou potenciálně upravná data / nastavení některých vlastností pro
     *         diagram tříd nebo diagram instancí, například změna barvy buňky reprezentující instanci nebo třídu apod.
     * @return objekt Properties (z této metody), ale bude obsahovat upravená data, resp. ty aktuálně nastavená
     * uživatelem, nebo ty výchozí.
     */
    EditProperties writeDataToDiagramProp(final EditProperties properties);


    /**
     * Metoda, která slouží pro čtení / získání dat z objektu properties do příslušných komponent v příslušném panelu,
     * aby jej uživatel mohl nějak upravit.
     *
     * @param properties
     *         - objekt Properties, který obsahuje načtený nějaký soubor .properties obsahující nastavení pro diagram
     *         tříd nebo diagram instancí a z tohoto objekt se budou brát potřebná data.
     */
    void readDataFromDiagramProp(final Properties properties);
}
