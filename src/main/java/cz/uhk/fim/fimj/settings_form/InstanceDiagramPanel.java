package cz.uhk.fim.fimj.settings_form;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.ConfigurationFiles;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.file.WriteToFile;
import cz.uhk.fim.fimj.language.EditProperties;
import cz.uhk.fim.fimj.settings_form.instance_diagram_panels.AggregationEdgePanel;
import cz.uhk.fim.fimj.settings_form.instance_diagram_panels.ClassCellPanel;
import cz.uhk.fim.fimj.settings_form.instance_diagram_panels.HighlightInstancePanel;
import cz.uhk.fim.fimj.settings_form.instance_diagram_panels.InstanceCellAttributesPanel;
import cz.uhk.fim.fimj.settings_form.instance_diagram_panels.InstanceCellMethodsPanel;
import cz.uhk.fim.fimj.settings_form.instance_diagram_panels.AssociationEdgePanel;
import cz.uhk.fim.fimj.settings_form.instance_diagram_panels.InstanceDiagramGraphPanel;

/**
 * Tato třída slouží jako panel, který obsahuje možnosti pro nastavení diagramu instancí - design buňěk, apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class InstanceDiagramPanel extends PanelAbstract
		implements SaveSettingsInterface, LanguageInterface, ActionListener {

	private static final long serialVersionUID = 1L;

	
	
	private final JLabel lblResetApp;
	
	
	
	// Panely:
	private final InstanceDiagramGraphPanel pnlInstanceDiagramGraph;
	private final HighlightInstancePanel pnlHighlightInstance;
	private final ClassCellPanel pnlClassCell;
	private final AssociationEdgePanel pnlAssociationEdge;
	
	
	// Panely pro atributy a metody buňce pro instanci třídy v ID:
	private final InstanceCellAttributesPanel instanceCellAttributesPanel;
	private final InstanceCellMethodsPanel instanceCellMethodsPanel;
	
	/**
	 * Panel, do kterého vložím panely pro nastavení atributů a metod v buňce pro
	 * reprezentaci instancí. Tento panel je jako globální neboli instanční proměnná
	 * pouze pro to, abych mu mohl nastavit ohraničení s textem, aby "informovatl"
	 * uživatele nebo mu dal lépe na jevo, že to patří k sobě (ty panely).
	 */
	private final JPanel pnlInstanceAttributesMethods;
	
	
	
	
	
	/**
	 * panel pro hranu typu agregace:
	 */
	private final AggregationEdgePanel pnlAggregationEdge;
	
	private final JButton btnRestoreDefaultSettings;
	
	
	
	// Texty do chyových hlášek pro duplicitu nastav, ení parametrů hran:
    private String txtEdgesAreIdenticalText;
    private String txtEdgesAreIdenticalTitle;
	
	
	

	/**
	 * Konstruktor této třídy.
	 */
	public InstanceDiagramPanel() {
		super();
		
		setLayout(new GridBagLayout());

		
		final GridBagConstraints gbc = getGbc();
					
		index = 0;
		
		
		gbc.gridwidth = 2;
		
		
		
		final JPanel pnlResetApp = new JPanel(new FlowLayout(FlowLayout.CENTER));
		lblResetApp = new JLabel();
		pnlResetApp.add(lblResetApp);
		setGbc(gbc, index, 0, pnlResetApp, this);
		
		
		
		
		
		// Panel s vlastnostmi grafu:
		final JPanel pnlInstanceGraphPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		pnlInstanceDiagramGraph = new InstanceDiagramGraphPanel();
		pnlInstanceGraphPanel.add(pnlInstanceDiagramGraph);		
		setGbc(gbc, ++index, 0, pnlInstanceGraphPanel, this);
		
		
		
		
		
		// Panel pro zvýraznění instance:
		final JPanel pnlHidhlightInstancePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		pnlHighlightInstance = new HighlightInstancePanel();
		pnlHidhlightInstancePanel.add(pnlHighlightInstance);		
		setGbc(gbc, ++index, 0, pnlHidhlightInstancePanel, this);
		
		
		
		
		
		
		
		gbc.gridwidth = 2;
		
		
		final JPanel pnlClassCellToDialog = new JPanel(new FlowLayout(FlowLayout.CENTER));
		pnlClassCell = new ClassCellPanel();
		pnlClassCellToDialog.add(pnlClassCell);
		setGbc(gbc, ++index, 0, pnlClassCellToDialog, this);
		
		
		
		
		
		
		
		/*
		 * Panel, který bude obsahovat panely pro nastavení atributů a metod v buňce pro
		 * instanci třídy v ID.
		 */
		pnlInstanceAttributesMethods = new JPanel(new GridBagLayout());
		
		final GridBagConstraints gbcInstancesAttrMeths = getGbc();
		
		instanceCellAttributesPanel = new InstanceCellAttributesPanel();
		instanceCellMethodsPanel = new InstanceCellMethodsPanel();
		
		setGbc(gbcInstancesAttrMeths, 0, 0, instanceCellAttributesPanel, pnlInstanceAttributesMethods);
		setGbc(gbcInstancesAttrMeths, 0, 1, instanceCellMethodsPanel, pnlInstanceAttributesMethods);
		
		// Zde by bylo možné využít pouze přidání panelu pnlInstanceAttributesMethods
		// následujícím způsobem, ale pak by se ten panel "roztahoval" na celou šířku
		// dialogu, já ho chci jen vycentrovat:
//		setGbc(gbc, ++index, 0, pnlInstanceAttributesMethods, this);
		
		/*
		 * Tento panel je zde pouze pro "vycentrování panlu s komponentami pro natavení
		 * atributů a metod v buňce reprezentující instanci v diagramu instancí.
		 * 
		 * Pokud bych to neudělal takto, že bych ten panel nevložil do panelu, kteý je
		 * vycentrován, tak by se rozztahoval na celou šířku okna, což nechci, takto
		 * bude vycentrován.
		 */
		final JPanel pnlTestForCenterAlignment = new JPanel(new FlowLayout(FlowLayout.CENTER));
		pnlTestForCenterAlignment.add(pnlInstanceAttributesMethods);
		setGbc(gbc, ++index, 0, pnlTestForCenterAlignment, this);
		
		
		
		
		
		
		
		
		
		
		gbc.gridwidth = 1;
		
		
		final JPanel pnlAssociationEdgeToFrame = new JPanel(new FlowLayout(FlowLayout.CENTER));
		pnlAssociationEdge = new AssociationEdgePanel();		
		pnlAssociationEdgeToFrame.add(pnlAssociationEdge);
		setGbc(gbc, ++index, 0, pnlAssociationEdgeToFrame, this);
		
		
		
		
		
		
		final JPanel pnlAggregationEdgeToFrame = new JPanel(new FlowLayout(FlowLayout.CENTER));
		pnlAggregationEdge = new AggregationEdgePanel();
		pnlAggregationEdgeToFrame.add(pnlAggregationEdge);
		setGbc(gbc, index, 1, pnlAggregationEdgeToFrame, this);
		
		
		
		
		
		
		final JPanel pnlButtonDefaultSettings = new JPanel(new FlowLayout(FlowLayout.CENTER));
		btnRestoreDefaultSettings = new JButton();
		btnRestoreDefaultSettings.addActionListener(this);
		pnlButtonDefaultSettings.add(btnRestoreDefaultSettings);
		
		
		gbc.gridwidth = 2;
		setGbc(gbc, ++index, 0, pnlButtonDefaultSettings, this);
		
				
		
		openSettings();
	}
	
	
	
	
	
	
	
	
	@Override
	public void saveSetting() {
		EditProperties instanceDiagramProp = App.READ_FILE
				.getPropertiesInWorkspacePersistComments(Constants.INSTANCE_DIAGRAM_NAME);

		if (instanceDiagramProp == null) {
			final String pathToWorkspace = ReadFile.getPathToWorkspace();

			/*
			 * Zde pouze zjisšťuji, zda existují veškeré adresáře k cestě pro soubor
			 * InstanceDiagram.properties v adresáři configuration ve workspace. Pokud ne,
			 * pak v rámci možností nekteré adresáře mohu vytvořit, jinak není kam uložit
			 * soubory, pokud například byl smazan workspace apod. Pak už zbývá jedině znovu
			 * vytvořit adresář workspace s projekty apod. Ale tuto část zde vynechávám,
			 * protože o tom uživatel musí vědět - že si smazal projekty případně workspace
			 * apod. Tak snad musí vědět, že nelze pracovat s projekty / soubory, které byly
			 * smazány.
			 * 
			 * Otestuji, zda existuje workspace, pak configuration, pak příslušný soubor,
			 * případně některé z nich mohu vytvořit:
			 */
			if (pathToWorkspace != null) {
				acquireMutex();
				
				/*
				 * Pokud neexsistuje adresář configuration ve workspace, bude vytvořen i s
				 * potřebnými soubory s výchozím nastavením.
				 * 
				 * Pokud adresář configuration ve workspace existuje, pak stačí vytvořit jen
				 * soubor InstanceDiagram.properties, protože vím, že neexistuje, když se jej nepodařilo
				 * načíst.
				 */
				if (ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
					ConfigurationFiles.writeInstanceDiagramProperties(
							pathToWorkspace + File.separator + Constants.PATH_TO_INSTANCE_DIAGRAM_PROPERTIES);

				
				else
					WriteToFile.createConfigureFilesInPath(pathToWorkspace);

				
				// Znovu načtu soubor Default.properties z workspace z configuration, protože
				// výše byl znovu vytvořn:
				instanceDiagramProp = App.READ_FILE
						.getPropertiesInWorkspacePersistComments(Constants.INSTANCE_DIAGRAM_NAME);
				
				releaseMutex();
			}
			/*
			 * část else:
			 * 
			 * Zde by bylo možné vytvořit dříve zadaný adresář workspace - pokud byl již
			 * zadán, ale byl smazán, ale to by v rámci aplikace nemělo nastat, proto tuto
			 * část vynechám, uživatel jej musí smazat sám, pak o tom musí vědět.
			 */
		}
		
		
		
		
		
		
		
		// Zde soubor existuje - pokud existuje Workspace - otesuji, zda se načetl, a
		// pokud ano, tak ho pošlu dál, aby se do něj
		// uložili data z dialogu a uložím ho zpět:
		if (instanceDiagramProp != null) {
			instanceDiagramProp = pnlInstanceDiagramGraph.writeDataToDiagramProp(instanceDiagramProp);
			instanceDiagramProp = pnlHighlightInstance.writeDataToDiagramProp(instanceDiagramProp);
			instanceDiagramProp = pnlClassCell.writeDataToDiagramProp(instanceDiagramProp);
			instanceDiagramProp = pnlAssociationEdge.writeDataToDiagramProp(instanceDiagramProp);
			instanceDiagramProp = pnlAggregationEdge.writeDataToDiagramProp(instanceDiagramProp);

			instanceDiagramProp = instanceCellAttributesPanel.writeDataToDiagramProp(instanceDiagramProp);
			instanceDiagramProp = instanceCellMethodsPanel.writeDataToDiagramProp(instanceDiagramProp);

			// Zapíšu ho zpět:
			instanceDiagramProp.save();
		}
	}

	
	
	
	
	
	
	@Override
	public void openSettings() {
		// Pokusím se načíst Instance diagram

		// Zkusím ho načist z Workspace, pokud se nepodaří půjdu po cestě k němu, pokud
		// zjistím že neexistuje workspace, v podstatě je to konec.
		// pokud existuje, tak buď nakopíruji celá adresář configration nebo jen
		// příslušný subor InstanceDiagram.properties

		Properties instanceDiagramProperties = App.READ_FILE.getInstanceDiagramFromWorkspaceOnly();
		
		if (instanceDiagramProperties == null) {
			final String pathToWorkspace = ReadFile.getPathToWorkspace();

			/*
			 * Zde pouze zjisšťuji, zda existují veškeré adresáře k cestě pro soubor
			 * InstanceDiagram.properties v adresáři configuration ve workspace. Pokud ne,
			 * pak v rámci možností nekteré adresáře mohu vytvořit, jinak není kam uložit
			 * soubory, pokud například byl smazan workspace apod. Pak už zbývá jedině znovu
			 * vytvořit adresář workspace s projekty apod. Ale tuto část zde vynechávám,
			 * protože o tom uživatel musí vědět - že si smazal projekty případně workspace
			 * apod. Tak snad musí vědět, že nelze pracovat s projekty / soubory, které byly
			 * smazány.
			 * 
			 * Otestuji, zda existuje workspace, pak configuration, pak příslušný soubor,
			 * případně některé z nich mohu vytvořit:
			 */
			if (pathToWorkspace != null) {
				acquireMutex();
				
				/*
				 * Pokud neexsistuje adresář configuration ve workspace, bude vytvořen i s
				 * potřebnými soubory s výchozím nastavením.
				 * 
				 * Pokud adresář configuration ve workspace existuje, pak stačí vytvořit jen
				 * soubor InstanceDiagram.properties, protože vím, že neexistuje, když se jej
				 * nepodařilo načíst.
				 */
				if (ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
					ConfigurationFiles.writeInstanceDiagramProperties(
							pathToWorkspace + File.separator + Constants.PATH_TO_INSTANCE_DIAGRAM_PROPERTIES);

				else
					WriteToFile.createConfigureFilesInPath(pathToWorkspace);

				// Znovu načtu soubor Default.properties z workspace z configuration, protože
				// výše byl znovu vytvořn:
				instanceDiagramProperties = App.READ_FILE.getInstanceDiagramFromWorkspaceOnly();
				
				releaseMutex();
			}
			/*
			 * část else:
			 * 
			 * Zde by bylo možné vytvořit dříve zadaný adresář workspace - pokud byl již
			 * zadán, ale byl smazán, ale to by v rámci aplikace nemělo nastat, proto tuto
			 * část vynechám, uživatel jej musí smazat sám, pak o tom musí vědět.
			 */
		}
		
		
		
		
		
		
		// Otestuji, zda opravdu  existuje workspace a načetl se soubor, pokud ano, načtu si hodnoty v souboru do dialogu:
		if (instanceDiagramProperties != null) {
			pnlInstanceDiagramGraph.readDataFromDiagramProp(instanceDiagramProperties);
			pnlHighlightInstance.readDataFromDiagramProp(instanceDiagramProperties);
			pnlAssociationEdge.readDataFromDiagramProp(instanceDiagramProperties);
			pnlAggregationEdge.readDataFromDiagramProp(instanceDiagramProperties);
			pnlClassCell.readDataFromDiagramProp(instanceDiagramProperties);

			instanceCellAttributesPanel.readDataFromDiagramProp(instanceDiagramProperties);
			instanceCellMethodsPanel.readDataFromDiagramProp(instanceDiagramProperties);
		}
	}


	
	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			lblResetApp.setText(properties.getProperty("Sf_App_LabelResetApplication", Constants.SF_LBL_RESET_APP));
			
			pnlInstanceAttributesMethods.setBorder(BorderFactory
					.createTitledBorder(properties.getProperty("Sf_App_ID_Lbl_AttributesAndMethodsPanelTitle",
							Constants.SF_ID_LBL_ATTRIBUTES_AND_METHODS_PANEL_TITLE)));
			
			btnRestoreDefaultSettings.setText(properties.getProperty("Sf_Cd_ButtonRestoreDefaultSettings", Constants.SF_BUTTONS_RESTORE_DEFAULT_SETTINGS));
			
			txtEdgesAreIdenticalText = properties.getProperty("Sf_Cd_TxtEdgesAreIdenticalText", Constants.SF_CD_TXT_EDGES_ARE_IDENTICAL_TEXT);
			txtEdgesAreIdenticalTitle = properties.getProperty("Sf_Cd_TxtEdgesAreIdenticalTitle", Constants.SF_CD_TXT_EDGES_ARE_IDENTICAL_TITLE);
		}			
		
		
		else {		
			lblResetApp.setText(Constants.SF_LBL_RESET_APP);
			
			pnlInstanceAttributesMethods.setBorder(BorderFactory.createTitledBorder(Constants.SF_ID_LBL_ATTRIBUTES_AND_METHODS_PANEL_TITLE));
			
			btnRestoreDefaultSettings.setText(Constants.SF_BUTTONS_RESTORE_DEFAULT_SETTINGS);
			
			txtEdgesAreIdenticalText = Constants.SF_CD_TXT_EDGES_ARE_IDENTICAL_TEXT;
			txtEdgesAreIdenticalTitle = Constants.SF_CD_TXT_EDGES_ARE_IDENTICAL_TITLE;
		}
		
		
		pnlInstanceDiagramGraph.setLanguage(properties);
		pnlHighlightInstance.setLanguage(properties);
		pnlClassCell.setLanguage(properties);
		pnlAssociationEdge.setLanguage(properties);
		pnlAggregationEdge.setLanguage(properties);

		instanceCellAttributesPanel.setLanguage(properties);
		instanceCellMethodsPanel.setLanguage(properties);
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se nastavení pro hrany pro reprezentaci vztahu
	 * asociace a agregace shodují v dialogu nastavení nebo ne, pokud ano, vrátí se
	 * true, pokud ne, vrátí se false.
	 * 
	 * Metoda porovná jednotlivé nastavené vlastností pro obě hrany (barva hrany,
	 * barva písma, velikost písma, ...)
	 * 
	 * @return true, pokud jsou všechny nastavené vlastnosti pro obě hrany
	 *         identické, jinak pokud se alespoň jedna liší, tak false
	 */
	final boolean areEdgesAndCellsIdentical() {
		if (pnlAssociationEdge.isChcbTextAlongEdgeSelected() == pnlAggregationEdge.isChcbTextAlongEdgeSelected() && 
				pnlAssociationEdge.isChcbLineBeginFillSelected() == pnlAggregationEdge.isChcbLineBeginFillSelected()
				&& pnlAssociationEdge.isChcbLineEndFillSelected() == pnlAggregationEdge.isChcbLineEndFillSelected()
				&& pnlAssociationEdge.getRgbOfLineColor() == pnlAggregationEdge.getRgbOfLineColor()
				&& pnlAssociationEdge.getCmbLineStartSelectedItem().equals(pnlAggregationEdge
				.getCmbLineStartSelectedItem())
				&& pnlAssociationEdge.getCmbLineEndSelectedItem().equals(pnlAggregationEdge
				.getCmbLineEndSelectedItem())
				&& pnlAssociationEdge.getCmbLineWidthSelectedItem() == pnlAggregationEdge.getCmbLineWidthSelectedItem()
				&& pnlAssociationEdge.getRgbOfTextColor() == pnlAggregationEdge.getRgbOfTextColor()
				&& pnlAssociationEdge.getFontSizeSelectedItem() == pnlAggregationEdge.getFontSizeSelectedItem()
				&& pnlAssociationEdge.getFontStyleSelectedItem().equals(pnlAggregationEdge.getFontStyleSelectedItem())) {
			
			JOptionPane.showMessageDialog(null,
					txtEdgesAreIdenticalText + "\n" + pnlAssociationEdge.getTxtTextBorderTitleToJop() + "\n"
							+ pnlAggregationEdge.getTxtTextBorderTitleToJop(),
					txtEdgesAreIdenticalTitle, JOptionPane.ERROR_MESSAGE);
			
			return true;
		}
		
		return false;
	}
	
	
	
	
	
	
	


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton && e.getSource() == btnRestoreDefaultSettings) {
			pnlInstanceDiagramGraph.setSelectedChcbConDisConLabel(Constants.CD_COM_LABELS_MOVABLE);
			pnlInstanceDiagramGraph.setSelectedChcbShowRelationShipsToInstanceItself(Constants
					.ID_SHOW_RELATION_SHIPS_TO_INSTANCE_ITSELF);
			pnlInstanceDiagramGraph.setSelectedChcbShowInheritedClassNameWithPackages(Constants
					.ID_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES);
			pnlInstanceDiagramGraph.setSelectedChcbIncludePrivateMethods(Constants.ID_INCLUDE_PRIVATE_METHODS);
			pnlInstanceDiagramGraph.setSelectedChcbIncludeProtectedMethods(Constants.ID_INCLUDE_PROTECTED_METHODS);
			pnlInstanceDiagramGraph.setSelectedChcbIncludePackagePrivateMethods(Constants
					.ID_INCLUDE_PACKAGE_PRIVATE_METHODS);
			pnlInstanceDiagramGraph.setSelectedChcbMakeAvailableFieldsForCallMethod(Constants
					.ID_MAKE_FIELDS_AVAILABLE_FOR_CALL_METHOD);
			pnlInstanceDiagramGraph.setSelectedChcbMakeAvailableMethodsForCallMethod(Constants
					.ID_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD);
			pnlInstanceDiagramGraph.setSelectedChcbWayToRefreshRelationshipsBetweenInstances(Constants
					.ID_USE_FAST_WAY_TO_REFRESH_RELATIONSHIPS_BETWEEN_CLASSES);
			pnlInstanceDiagramGraph.setSelectedChcbShowRelationshipsInInheritedClasses(Constants
					.ID_SHOW_RELATIONSHIPS_IN_INHERITED_CLASSES);
			pnlInstanceDiagramGraph.setSelectedChcbShowCreateNewInstanceOptionInCallMethod(Constants
					.ID_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD);
			pnlInstanceDiagramGraph.setSelectedChcbShowGetterInSetterMethodInInstance(Constants
					.ID_SHOW_GETTER_IN_SETTER_METHOD_IN_INSTANCE);
			pnlInstanceDiagramGraph.setSelectedChcbShowAssociationThroughAggregation(Constants
					.ID_SHOW_ASOCIATION_THROUGH_AGREGATION);
			pnlInstanceDiagramGraph.setSelectedChcbShowReferenceVariables(Constants.ID_SHOW_REFERENCES_VARIABLES);
			pnlInstanceDiagramGraph.setBgColor(Constants.CD_BACKGROUND_COLOR.getRGB());
			pnlInstanceDiagramGraph.setScaleOfGraph(Constants.CLASS_DIAGRAM_SCALE);


			// Zvýraznění:
			pnlHighlightInstance.setSelectedChcbHighlightInstances(Constants.ID_HIGHLIGHT_INSTANCES);
			pnlHighlightInstance.setCmbTimeSelectedItem(Constants.ID_DEFAULT_HIGHLIGHTING_TIME);
			pnlHighlightInstance.setSelectedTextColor(Constants.ID_TEXT_COLOR_OF_HIGHLIGHTED_INSTANCE);
			pnlHighlightInstance.setSelectedAttributeColor(Constants.ID_ATTRIBUTE_COLOR_OF_HIGHLIGHTED_INSTANCE);
			pnlHighlightInstance.setSelectedMethodColor(Constants.ID_METHOD_COLOR_OF_HIGHLIGHTED_INSTANCE);
			pnlHighlightInstance.setSelectedBorderColor(Constants.ID_BORDER_COLOR_OF_HIGHLIGHTED_INSTANCE);
			pnlHighlightInstance.setSelectedBackgroundColor(Constants.ID_BACKGROUND_COLOR_OF_HIGHLIGHTED_INSTANCE);


			// Buňka - pro instance
			pnlClassCell.setSelectedChcbShowPackages(Constants.ID_SHOW_PACKAGES_IN_CELL);
			pnlClassCell.setFontSize(Constants.ID_FONT.getSize());
			pnlClassCell.setFontStyle(Constants.ID_FONT.getStyle());
			pnlClassCell.setFontAlignmentX(Constants.ID_HORIZONTAL_ALIGNMENT);
			pnlClassCell.setFontAlignmentY(Constants.ID_VERTICAL_ALIGNMENT);
			pnlClassCell.setSelectedTextColor(Constants.ID_TEXT_COLOR);
			// Barva ohraničení:
			pnlClassCell.setColorBorder(Constants.ID_COLOR_BORDER);
			// Barva buňky
			pnlClassCell.setColorCell(Constants.ID_COLOR_CELL);
			// dodělat zaobleni hran:
			pnlClassCell.setRoundedBorder(Constants.ID_ROUNDED_BORDER_MARGIN);


			// Atributy v buňce pro instanci třídy:
			instanceCellAttributesPanel.setChcbShowAllSelected(Constants.ID_SHOW_ALL_ATTRIBUTES);
			instanceCellAttributesPanel.setChcbUseSameFontAndColorAsReferenceSelected(
					Constants.ID_USE_FOR_ATTRIBUTES_ONE_FONT_AND_FONT_COLOR_IN_INSTANCE_CELL);
			instanceCellAttributesPanel.setCmbCountSelected(Constants.ID_SPECIFIC_COUNT_OF_ATTRIBUTES);
			instanceCellAttributesPanel.setTextColor(Constants.ID_ATTRIBUTES_TEXT_COLOR);
			instanceCellAttributesPanel.setTextFontSize(Constants.ID_ATTRIBUTES_FONT.getSize());
			instanceCellAttributesPanel.setTextFontStyle(Constants.ID_ATTRIBUTES_FONT.getStyle());
			// Tato metoda musí být jako poslední v rámci panelu atributů, protože obsahuje
			// metody pro zpřístupnění / znepřístupnění nekterých položek:
			instanceCellAttributesPanel.setChcbShowSelected(Constants.ID_SHOW_ATTRIBUTES);


			// Metody v buňce pro instanci třídy:
			instanceCellMethodsPanel.setChcbShowAllSelected(Constants.ID_SHOW_ALL_METHODS);
			instanceCellMethodsPanel.setChcbUseSameFontAndColorAsReferenceSelected(
					Constants.ID_USE_FOR_METHODS_ONE_FONT_AND_FONT_COLOR_IN_INSTANCE_CELL);
			instanceCellMethodsPanel.setCmbCountSelected(Constants.ID_SPECIFIC_COUNT_OF_METHODS);
			instanceCellMethodsPanel.setTextColor(Constants.ID_METHODS_TEXT_COLOR);
			instanceCellMethodsPanel.setTextFontSize(Constants.ID_METHODS_FONT.getSize());
			instanceCellMethodsPanel.setTextFontStyle(Constants.ID_METHODS_FONT.getStyle());
			// Tato metoda musí být jako poslední v rámci panelu metod, protože obsahuje
			// metody pro zpřístupnění / znepřístupnění nekterých položek:
			instanceCellMethodsPanel.setChcbShowSelected(Constants.ID_SHOW_METHODS);


			// Pro hranu typu Asociace:
			pnlAssociationEdge.setChcbLableAlongEdge(Constants.ID_ASSOCIATION_LABELS_ALONG_EDGE);
			pnlAssociationEdge.setChcbLineBeginFillSelected(Constants.ID_ASSOCIATION_EDGE_LINE_BEGIN_FILL);
			pnlAssociationEdge.setChcbLIneEndFillSelected(Constants.ID_ASSOCIATION_EDGE_LINE_END_FILL);
			pnlAssociationEdge.setLineColor(Constants.ID_ASSOCIATION_EDGE_COLOR);
			pnlAssociationEdge.setLineBegin(Constants.ID_ASSOCIATION_EDGE_LINE_BEGIN);
			pnlAssociationEdge.setLineEnd(Constants.ID_ASSOCIATION_EDGE_LINE_END);
			pnlAssociationEdge.setLineWidth(Constants.ID_ASSOCIATION_EDGE_LINE_WIDTH);
			pnlAssociationEdge.setFontColor(Constants.ID_ASSOCIATION_EDGE_FONT_COLOR);
			pnlAssociationEdge.setFontSize(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getSize());
			pnlAssociationEdge.setFontStyle(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getStyle());


			// Pro hranu typu Agregace:
			pnlAggregationEdge.setChcbLableAlongEdge(Constants.ID_AGGREGATION_LABELS_ALONG_EDGE);
			pnlAggregationEdge.setChcbLineBeginFillSelected(Constants.ID_AGGREGATION_EDGE_LINE_BEGIN_FILL);
			pnlAggregationEdge.setChcbLIneEndFillSelected(Constants.ID_AGGREGATION_EDGE_LINE_END_FILL);
			pnlAggregationEdge.setLineColor(Constants.ID_AGGREGATION_EDGE_COLOR);
			pnlAggregationEdge.setLineBegin(Constants.ID_AGGREGATION_EDGE_LINE_BEGIN);
			pnlAggregationEdge.setLineEnd(Constants.ID_AGGREGATION_EDGE_LINE_END);
			pnlAggregationEdge.setLineWidth(Constants.ID_AGGREGATION_EDGE_LINE_WIDTH);
			pnlAggregationEdge.setFontColor(Constants.ID_AGGREGATION_EDGE_FONT_COLOR);
			pnlAggregationEdge.setFontSize(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getSize());
			pnlAggregationEdge.setFontStyle(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getStyle());
		}
	}
}