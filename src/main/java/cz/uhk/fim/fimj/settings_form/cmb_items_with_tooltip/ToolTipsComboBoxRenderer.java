package cz.uhk.fim.fimj.settings_form.cmb_items_with_tooltip;

import java.awt.Component;

import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

/**
 * Tato třída slouží jako renderer, který zobrazuje popisky - ToolTips nad označenou položkou v komponentě JComboBox.
 * <p>
 * Zdroj:
 * <p>
 * http://www.java2s.com/Code/Java/Swing-Components/ToolTipComboBoxExample.htm
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ToolTipsComboBoxRenderer extends BasicComboBoxRenderer {

    private static final long serialVersionUID = -3643610194985265117L;


    /**
     * Reference na instanci komponenty JComboBox, u které má fungovat zobrazování tooltipů nad jednotlivými položkami.
     */
    private final JComboBox<CmbItemAbstract> cmb;


    /**
     * Konstruktor třídy.
     *
     * @param cmb
     *         - Referencena instanci komponenty JComboBox, ve kterém se má zobrazit popisek.
     */
    public ToolTipsComboBoxRenderer(final JComboBox<CmbItemAbstract> cmb) {
        this.cmb = cmb;
    }


    @Override
    public Component getListCellRendererComponent(final JList list, final Object value, final int index,
                                                  final boolean isSelected, final boolean cellHasFocus) {
        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());

            if (-1 < index)
                list.setToolTipText(cmb.getModel().getElementAt(index).getTextToTooltip());

        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }

        setFont(list.getFont());
        setText((value == null) ? "" : value.toString());

        return this;
    }
}
