package cz.uhk.fim.fimj.settings_form;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.commands_editor.Editor;
import cz.uhk.fim.fimj.file.ConfigurationFiles;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.file.WriteToFile;
import cz.uhk.fim.fimj.language.EditProperties;
import cz.uhk.fim.fimj.settings_form.command_editor_panels.CommandCompleteWindowPanel;
import cz.uhk.fim.fimj.settings_form.command_editor_panels.CompleteHistoryPanel;
import cz.uhk.fim.fimj.settings_form.command_editor_panels.ProjectHistoryPanel;

/**
 * Tato třída slouží jako panel pro nastavení některých parametrů pro editor příkazů (v hlavním okně aplikace vlevo
 * dole). Parametrÿ jako jsou například fonr písma, barva písma, barva pozadí, apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class CommandEditorPanel extends EditorsPanelAbstract
        implements LanguageInterface, ActionListener, SaveSettingsInterface {

    private static final long serialVersionUID = -3720378401461637587L;


    private final Editor commandEditor;

    private final JButton btnRestoreDefaultSettings;


    /**
     * Panel pro nastavení vlastností pro okno, které obsahuje dostupné příkazy pro editor příkazů.
     */
    private final CommandCompleteWindowPanel commandCompleteWindowPanel;


    /**
     * Panel pro nastavení vlastností pro okno s historií příkazů v rámci otevřeného projektu v editoru příkazů.
     */
    private final ProjectHistoryPanel pnlProjectHistory;

    /**
     * Panel pro nastavení vlastností pro okno s historií příkazů, obsahující příkazy zadané v editoru příkazů od
     * spuštění aplikace.
     */
    private final CompleteHistoryPanel pnlCompleteHistory;





	public CommandEditorPanel(final Editor commandEditor) {
		this.commandEditor = commandEditor;
		
		
		setLayout(new GridBagLayout());
		
		
		final GridBagConstraints gbc = getGbc();
		index = 0;
		
		
		// Komponenty do okna resp. panelu:
		gbc.gridwidth = 3;
		
		chcbHighlightingCode = new JCheckBox();
		setGbc(gbc, index, 0, chcbHighlightingCode, this);
		
		
		gbc.gridwidth = 1;
		
		lblBgColor = new JLabel();
		setGbc(gbc, ++index, 0, lblBgColor, this);
		
		btnChooseBgColor = new JButton();
		btnChooseBgColor.addActionListener(this);
		setGbc(gbc, index, 1, btnChooseBgColor, this);
		
		lblChoosedBgColor = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChoosedBgColor.setOpaque(true);
		setGbc(gbc, index, 2, lblChoosedBgColor, this);
		
		
		
		lblTextColor = new JLabel();
		setGbc(gbc, ++index, 0, lblTextColor, this);
		
		btnChooseTextColor = new JButton();
		btnChooseTextColor.addActionListener(this);
		setGbc(gbc, index, 1, btnChooseTextColor, this);
		
		lblChoosedTextColor = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChoosedTextColor.setOpaque(true);
		setGbc(gbc, index, 2, lblChoosedTextColor, this);
		
		
		
		
		
		lblFontSize = new JLabel();
		setGbc(gbc, ++index, 0, lblFontSize, this);
		
		cmbFontSize = new JComboBox<>(getFontSizes());
		setGbc(gbc, index, 1, cmbFontSize, this);
		
		
		
		
		lblFontStyle = new JLabel();
		setGbc(gbc, ++index, 0, lblFontStyle, this);
		
		cmbFontStyle = new JComboBox<>(ARRAY_OF_FONT_STYLE.toArray(new String[] {}));
		setGbc(gbc, index, 1, cmbFontStyle, this);





        gbc.gridwidth = 3;

        // Panel pro nastavení okna s automatickým dokončováním / doplňováním příkazů v editoru příkazů:
        commandCompleteWindowPanel = new CommandCompleteWindowPanel();
        setGbc(gbc, ++index, 0, commandCompleteWindowPanel, this);





        // Okno s historií zadaných příkazů v otevřeném projektu:
        pnlProjectHistory = new ProjectHistoryPanel();
        setGbc(gbc, ++index, 0, pnlProjectHistory, this);

        // Okno s historií zadaných příkazů od spuštění aplikace:
        pnlCompleteHistory= new CompleteHistoryPanel();
        setGbc(gbc, ++index, 0, pnlCompleteHistory, this);








		// Tlačítko pro obnovení výchozího nastavení:
		final JPanel pnlButtonDefaultSettings = new JPanel(new FlowLayout(FlowLayout.CENTER));
		btnRestoreDefaultSettings = new JButton();
		btnRestoreDefaultSettings.addActionListener(this);
		pnlButtonDefaultSettings.add(btnRestoreDefaultSettings);
		
		setGbc(gbc, ++index, 0, pnlButtonDefaultSettings, this);
		

		
		
		openSettings();
	}
	
	
	
	


	@Override
	public void setLanguage(final Properties properties) {
	    pnlProjectHistory.setLanguage(properties);
	    pnlCompleteHistory.setLanguage(properties);
        commandCompleteWindowPanel.setLanguage(properties);

		if (properties != null) {
			setBorder(BorderFactory.createTitledBorder(properties.getProperty("Sf_Ce_PanelBorderTitle", Constants.SF_CE_BORDER_TITLE)));
			
			chcbHighlightingCode.setText(properties.getProperty("Sf_Ce_CheckBoxJavaHighlightingSyntax", Constants.SF_CE_CHCB_HIGHLIGHTING_CODE));
			
			
			lblBgColor.setText(properties.getProperty("Sf_Ce_LabelBackgroundColor", Constants.SF_CE_LBL_BACKGROUND_COLOR) + ": ");
			lblTextColor.setText(properties.getProperty("Sf_Cd_LabelTextColorClassCell", Constants.SF_LBL_TEXT_COLOR_CELL) + ": ");
			lblFontSize.setText(properties.getProperty("Sf_Cd_LabelFontSizeClassCell", Constants.SF_LBL_FONT_SIZE_CELL) + ": ");
			lblFontStyle.setText(properties.getProperty("Sf_Cd_LabelFontStyleClassCell", Constants.SF_LBL_FONT_STYLE_CELL) + ": ");
			
			
			txtChooseTextColorDialog = properties.getProperty("Sf_Cd_ChooseTextColorDialog", Constants.SF_TXT_CHOOSE_TEXT_COLOR);
			txtChooseBgColorDialog = properties.getProperty("Sf_Cd_ChooseBackgroundColorDialog", Constants.SF_TXT_CHOOSE_BG_COLOR);
			
			
			btnChooseBgColor.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));
			btnChooseTextColor.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));
			
			btnRestoreDefaultSettings.setText(properties.getProperty("Sf_Cd_ButtonRestoreDefaultSettings", Constants.SF_BUTTONS_RESTORE_DEFAULT_SETTINGS));
		}
		
		
		else {
			setBorder(BorderFactory.createTitledBorder(Constants.SF_CE_BORDER_TITLE));
			
			chcbHighlightingCode.setText(Constants.SF_CE_CHCB_HIGHLIGHTING_CODE);
			
			
			lblBgColor.setText(Constants.SF_CE_LBL_BACKGROUND_COLOR + ": ");
			lblTextColor.setText(Constants.SF_LBL_TEXT_COLOR_CELL + ": ");
			lblFontSize.setText(Constants.SF_LBL_FONT_SIZE_CELL + ": ");
			lblFontStyle.setText(Constants.SF_LBL_FONT_STYLE_CELL + ": ");
			
			
			txtChooseTextColorDialog = Constants.SF_TXT_CHOOSE_TEXT_COLOR;
			txtChooseBgColorDialog =  Constants.SF_TXT_CHOOSE_BG_COLOR;
			
			
			btnChooseBgColor.setText(Constants.SF_CD_BTN_CHOOSE_COLOR);
			btnChooseTextColor.setText(Constants.SF_CD_BTN_CHOOSE_COLOR);
			
			btnRestoreDefaultSettings.setText(Constants.SF_BUTTONS_RESTORE_DEFAULT_SETTINGS);
        }
    }








	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnChooseBgColor) {
				final Color tempVariable = JColorChooser.showDialog(this, txtChooseBgColorDialog,
						Constants.COMMAND_EDITOR_BACKGROUND_COLOR);
				
				if (tempVariable != null) {
					bgColor = tempVariable;
					lblChoosedBgColor.setForeground(bgColor);
					lblChoosedBgColor.setBackground(bgColor);
				}
			}
			
			
			else if (e.getSource() == btnChooseTextColor) {
				final Color tempVariable = JColorChooser.showDialog(this, txtChooseTextColorDialog,
						Constants.COMMAND_EDITOR_FOREGROUND_COLOR);
				
				if (tempVariable != null) {
					textColor = tempVariable;
					lblChoosedTextColor.setForeground(textColor);
					lblChoosedTextColor.setBackground(textColor);
				}
			}


			else if (e.getSource() == btnRestoreDefaultSettings) {
                chcbHighlightingCode.setSelected(Constants.COMMAND_EDITOR_HIGHLIGHTING_CODE);

                bgColor = Constants.COMMAND_EDITOR_BACKGROUND_COLOR;
                lblChoosedBgColor.setForeground(bgColor);
                lblChoosedBgColor.setBackground(bgColor);

                textColor = Constants.COMMAND_EDITOR_FOREGROUND_COLOR;
                lblChoosedTextColor.setBackground(textColor);
                lblChoosedTextColor.setForeground(textColor);


                cmbFontSize.setSelectedItem(Constants.DEFAULT_FONT_FOR_COMMAND_EDITOR.getSize());

                final int fontStyle = Constants.DEFAULT_FONT_FOR_COMMAND_EDITOR.getStyle();
                if (fontStyle == 0)        // Plain
                    cmbFontStyle.setSelectedIndex(2);
                else if (fontStyle == 1)    // Bold
                    cmbFontStyle.setSelectedIndex(0);
                else if (fontStyle == 2)    // Italic
                    cmbFontStyle.setSelectedIndex(1);


                // Výchozí hodnoty pro okno s příkazy pro doplnění do editoru:
                commandCompleteWindowPanel.setChcbShowAutoCompleteWindowSelectedSelected(Constants.CE_SHOW_AUTO_COMPLETE_WINDOW);
                commandCompleteWindowPanel.setChcbShowReferenceVariablesInCompleteWindowSelected(Constants.CE_SHOW_REFERENCE_VARIABLES_IN_COMPLETE_WINDOW);
                commandCompleteWindowPanel.setChcbAddSyntaxForFillingTheVariableSelected(Constants.CE_ADD_SYNTAX_FOR_FILLING_THE_VARIABLE);
                commandCompleteWindowPanel.setChcbAddFinalWordForFillingTheVariableSelected(Constants.CD_ADD_FINAL_WORD_FOR_FILLING_THE_VARIABLE);
                commandCompleteWindowPanel.setChcbShowExamplesOfSyntaxSelected(Constants.CE_SHOW_EXAMPLES_OF_SYNTAX);
                commandCompleteWindowPanel.setChcbShowDefaultMethodsSelected(Constants.CE_SHOW_DEFAULT_METHODS);
                // Kontrola zpřístupnění komponent pro nastavení vlastností:
                commandCompleteWindowPanel.chechSelectedChcbShowAutoCompleteWindow();


                // Výchozí hodnoty pro okna s historií příkazů v rámci otevřeného projektu:
                pnlProjectHistory.setSelectedChcbShowHistory(Constants.CE_SHOW_PROJECT_HISTORY_WINDOW);
                pnlProjectHistory.setSelectedChcbIndexCommands(Constants.CE_INDEX_COMMANDS_IN_PROJECT_HISTORY_WINDOW);
                // Zpřístupní / Znepřístupní se vybrané komponenty:
                pnlProjectHistory.checkSelectedChcbShowHistory();

                // Výchozí hodnoty pro okna s historií příkazů v od spuštění aplikace:
                pnlCompleteHistory.setSelectedChcbShowHistory(Constants.CD_SHOW_COMPLETE_HISTORY_WINDOW);
                pnlCompleteHistory.setSelectedChcbIndexCommands(Constants.CD_INDEX_COMMANDS_IN_COMPLETE_HISTORY_WINDOW);
                // Zpřístupní / Znepřístupní se vybrané komponenty:
                pnlCompleteHistory.checkSelectedChcbShowHistory();
            }
        }
    }






	@Override
	public void saveSetting() {
		// Zde se pokusím načíst soubor z Workspace, pokud se tak nepovede
		// pak "půjdu po cestě" do configration ve Workspace - pokud existuje
		// dle toho kde narazím na absenci souboru či adresáři tak začnu kopírovat nebo
		// vytvářet příslušné soubory
		EditProperties commandEditorProp = App.READ_FILE
				.getPropertiesInWorkspacePersistComments(Constants.COMMAND_EDITOR_NAME);
		
		if (commandEditorProp == null) {
			final String pathToWorkspace = ReadFile.getPathToWorkspace();

			/*
			 * Zde pouze zjisšťuji, zda existují veškeré adresáře k cestě pro soubor
			 * CommandEditor.properties v adresáři configuration ve workspace. Pokud ne, pak v
			 * rámci možností nekteré adresáře mohu vytvořit, jinak není kam uložit soubory,
			 * pokud například byl smazan workspace apod. Pak už zbývá jedině znovu vytvořit
			 * adresář workspace s projekty apod. Ale tuto část zde vynechávám, protože o
			 * tom uživatel musí vědět - že si smazal projekty případně workspace apod. Tak
			 * snad musí vědět, že nelze pracovat s projekty / soubory, které byly smazány.
			 * 
			 * Otestuji, zda existuje workspace, pak configuration, pak příslušný soubor,
			 * případně některé z nich mohu vytvořit:
			 */
			if (pathToWorkspace != null) {
				acquireMutex();
				
				/*
				 * Pokud neexsistuje adresář configuration ve workspace, bude vytvořen i s
				 * potřebnými soubory s výchozím nastavením.
				 * 
				 * Pokud adresář configuration ve workspace existuje, pak stačí vytvořit jen
				 * soubor CommandEditor.properties, protože vím, že neexistuje, když se jej nepodařilo
				 * načíst.
				 */
				if (ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
					ConfigurationFiles.writeCommandEditorProperties(
							pathToWorkspace + File.separator + Constants.PATH_TO_COMMAND_EDITOR_PROPERTIES);

				
				else
					WriteToFile.createConfigureFilesInPath(pathToWorkspace);

				
				// Znovu načtu soubor Default.properties z workspace z configuration, protože
				// výše byl znovu vytvořn:
				commandEditorProp = App.READ_FILE
						.getPropertiesInWorkspacePersistComments(Constants.COMMAND_EDITOR_NAME);
				
				releaseMutex();
			}
			/*
			 * část else:
			 * 
			 * Zde by bylo možné vytvořit dříve zadaný adresář workspace - pokud byl již
			 * zadán, ale byl smazán, ale to by v rámci aplikace nemělo nastat, proto tuto
			 * část vynechám, uživatel jej musí smazat sám, pak o tom musí vědět.
			 */
		}
		
		
		
		
		if (commandEditorProp != null) {
			// Zde se podařilo načíst soubour, tak do něj vložím upravená data a uložím ho zpět:
			
			commandEditorProp.setProperty("HighlightingJavaCode", String.valueOf(chcbHighlightingCode.isSelected()));
			
			// Barva pozadí:
			commandEditorProp.setProperty("BackgroundColor", Integer.toString(bgColor.getRGB()));
			
			// Barva písma:
			commandEditorProp.setProperty("ForegroundColor", Integer.toString(textColor.getRGB()));
			
			// Velikost písma:
			commandEditorProp.setProperty("FontSize", Integer.toString((Integer) cmbFontSize.getSelectedItem()));
			
			// Typ písma:
			final int fontStyle = cmbFontStyle.getSelectedIndex();
			
			if (fontStyle == 0)
				commandEditorProp.setProperty("FontStyle", Integer.toString(1));
			else if (fontStyle == 1)
				commandEditorProp.setProperty("FontStyle", Integer.toString(2));
			else if (fontStyle == 2)
				commandEditorProp.setProperty("FontStyle", Integer.toString(0));


            // Hodnoty pro okno s příkazy pro doplnění do editoru:
            commandEditorProp.setProperty("ShowAutoCompleteWindow",
                    String.valueOf(commandCompleteWindowPanel.isChcbShowAutoCompleteWindowSelectedSelected()));
            commandEditorProp.setProperty("ShowReferenceVariablesInCompleteWindow",
                    String.valueOf(commandCompleteWindowPanel.isChcbShowReferenceVariablesInCompleteWindowSelected()));
            commandEditorProp.setProperty("AddSyntaxForFillingTheVariable",
                    String.valueOf(commandCompleteWindowPanel.isChcbAddSyntaxForFillingTheVariableSelected()));
            commandEditorProp.setProperty("AddFinalWordForFillingTheVariable",
                    String.valueOf(commandCompleteWindowPanel.isChcbAddFinalWordForFillingTheVariableSelected()));
            commandEditorProp.setProperty("ShowExamplesOfSyntax",
                    String.valueOf(commandCompleteWindowPanel.isChcbShowExamplesOfSyntaxSelected()));
            commandEditorProp.setProperty("ShowDefaultMethods",
                    String.valueOf(commandCompleteWindowPanel.isChcbShowDefaultMethodsSelected()));


            // Uložení vlastností pro historii příkazů:
            commandEditorProp.setProperty("ShowProjectHistoryWindow",
                    String.valueOf(pnlProjectHistory.isChcbShowHistorySelected()));
            commandEditorProp.setProperty("IndexCommandsInProjectHistoryWindow",
                    String.valueOf(pnlProjectHistory.isChcbIndexCommandsSelected()));

            commandEditorProp.setProperty("ShowCompleteHistoryWindow",
                    String.valueOf(pnlCompleteHistory.isChcbShowHistorySelected()));
            commandEditorProp.setProperty("IndexCommandsInCompleteHistoryWindow",
                    String.valueOf(pnlCompleteHistory.isChcbIndexCommandsSelected()));

			
			
			// Zápis zpět do souboru:
			commandEditorProp.save();
			
			// zavolat metodu v editoru pro přenačtení editoru !!
			// Vlastně je to v podstatě aktualizace editoru - změna nastavení:
			commandEditor.setSettingsEditor();
		}
	}






	@Override
	public void openSettings() {
		// Pokusím se načíst CommandEditor.properties z workspace
		// pokud to nenactu, tak "pujdu po ceste" abych se to pokusil najit
		// a dle toho kde se "zaseknu", tak se pokusím soubory znovu vytvořit
		// (pokud existuje workspace)

		// Pokud existuje alespoň workspace, tak se soubor načte ať už výchozí nebo
		// upravený
		Properties commandEditorProp = App.READ_FILE.getCommandEditorPropertiesFromWorkspaceOnly();

		if (commandEditorProp == null) {
			final String pathToWorkspace = ReadFile.getPathToWorkspace();

			/*
			 * Zde pouze zjisšťuji, zda existují veškeré adresáře k cestě pro soubor
			 * CommandEditor.properties v adresáři configuration ve workspace. Pokud ne, pak v
			 * rámci možností nekteré adresáře mohu vytvořit, jinak není kam uložit soubory,
			 * pokud například byl smazan workspace apod. Pak už zbývá jedině znovu vytvořit
			 * adresář workspace s projekty apod. Ale tuto část zde vynechávám, protože o
			 * tom uživatel musí vědět - že si smazal projekty případně workspace apod. Tak
			 * snad musí vědět, že nelze pracovat s projekty / soubory, které byly smazány.
			 * 
			 * Otestuji, zda existuje workspace, pak configuration, pak příslušný soubor,
			 * případně některé z nich mohu vytvořit:
			 */
			if (pathToWorkspace != null) {
				acquireMutex();
				
				/*
				 * Pokud neexsistuje adresář configuration ve workspace, bude vytvořen i s
				 * potřebnými soubory s výchozím nastavením.
				 * 
				 * Pokud adresář configuration ve workspace existuje, pak stačí vytvořit jen
				 * soubor CommandEditor.properties, protože vím, že neexistuje, když se jej
				 * nepodařilo načíst.
				 */
				if (ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
					ConfigurationFiles.writeCommandEditorProperties(
							pathToWorkspace + File.separator + Constants.PATH_TO_COMMAND_EDITOR_PROPERTIES);

				
				else
					WriteToFile.createConfigureFilesInPath(pathToWorkspace);

				
				// Znovu načtu soubor Default.properties z workspace z configuration, protože
				// výše byl znovu vytvořn:
				commandEditorProp = App.READ_FILE.getCommandEditorPropertiesFromWorkspaceOnly();
				
				releaseMutex();
			}
			/*
			 * část else:
			 * 
			 * Zde by bylo možné vytvořit dříve zadaný adresář workspace - pokud byl již
			 * zadán, ale byl smazán, ale to by v rámci aplikace nemělo nastat, proto tuto
			 * část vynechám, uživatel jej musí smazat sám, pak o tom musí vědět.
			 */
		}
		
		
		
		
		if (commandEditorProp != null) {
			// Zde se podařilo soubor načíst, do vyplním komponenty v okně příslušnými daty:
			chcbHighlightingCode.setSelected(Boolean.parseBoolean(commandEditorProp.getProperty("HighlightingJavaCode", String.valueOf(Constants.COMMAND_EDITOR_HIGHLIGHTING_CODE))));
			
			final String backgroundColor = commandEditorProp.getProperty("BackgroundColor", Integer.toString(Constants.COMMAND_EDITOR_BACKGROUND_COLOR.getRGB()));
			bgColor = new Color(Integer.parseInt(backgroundColor));
			lblChoosedBgColor.setForeground(bgColor);
			lblChoosedBgColor.setBackground(bgColor);
			
			
			final String textColorText = commandEditorProp.getProperty("ForegroundColor", Integer.toString(Constants.COMMAND_EDITOR_FOREGROUND_COLOR.getRGB()));
			textColor = new Color(Integer.parseInt(textColorText));
			lblChoosedTextColor.setForeground(textColor);
			lblChoosedTextColor.setBackground(textColor);
			
			
			
			final int fontSizeFromFile = Integer.parseInt(commandEditorProp.getProperty("FontSize", String.valueOf(Constants.DEFAULT_FONT_FOR_COMMAND_EDITOR.getSize())));
			if (fontSizeFromFile >= 8 && fontSizeFromFile <= 100)
				cmbFontSize.setSelectedItem(fontSizeFromFile);
			
			
			
			final int fontStyle = Integer.parseInt(commandEditorProp.getProperty("FontStyle", Integer.toString(Constants.DEFAULT_FONT_FOR_COMMAND_EDITOR.getStyle())));
			if (fontStyle == 0)		// Plain
				cmbFontStyle.setSelectedIndex(2);
			else if (fontStyle == 1)	// Bold
				cmbFontStyle.setSelectedIndex(0);
			else if (fontStyle == 2)	// Italic
				cmbFontStyle.setSelectedIndex(1);




            // Hodnoty pro okno s příkazy pro doplnění do editoru:
            commandCompleteWindowPanel.setChcbShowAutoCompleteWindowSelectedSelected(Boolean.parseBoolean(commandEditorProp.getProperty("ShowAutoCompleteWindow", String.valueOf(Constants.CE_SHOW_AUTO_COMPLETE_WINDOW))));
            commandCompleteWindowPanel.setChcbShowReferenceVariablesInCompleteWindowSelected(Boolean.parseBoolean(commandEditorProp.getProperty("ShowReferenceVariablesInCompleteWindow", String.valueOf(Constants.CE_SHOW_REFERENCE_VARIABLES_IN_COMPLETE_WINDOW))));
            commandCompleteWindowPanel.setChcbAddSyntaxForFillingTheVariableSelected(Boolean.parseBoolean(commandEditorProp.getProperty("AddSyntaxForFillingTheVariable", String.valueOf(Constants.CE_ADD_SYNTAX_FOR_FILLING_THE_VARIABLE))));
            commandCompleteWindowPanel.setChcbAddFinalWordForFillingTheVariableSelected(Boolean.parseBoolean(commandEditorProp.getProperty("AddFinalWordForFillingTheVariable", String.valueOf(Constants.CD_ADD_FINAL_WORD_FOR_FILLING_THE_VARIABLE))));
            commandCompleteWindowPanel.setChcbShowExamplesOfSyntaxSelected(Boolean.parseBoolean(commandEditorProp.getProperty("ShowExamplesOfSyntax", String.valueOf(Constants.CE_SHOW_EXAMPLES_OF_SYNTAX))));
            commandCompleteWindowPanel.setChcbShowDefaultMethodsSelected(Boolean.parseBoolean(commandEditorProp.getProperty("ShowDefaultMethods", String.valueOf(Constants.CE_SHOW_DEFAULT_METHODS))));
            // Kontrola zpřístupnění komponent pro nastavení vlastností:
            commandCompleteWindowPanel.chechSelectedChcbShowAutoCompleteWindow();


            // Nastavení vlastností pro historii příkazů:
            pnlProjectHistory.setSelectedChcbShowHistory(Boolean.parseBoolean(commandEditorProp.getProperty(
                    "ShowProjectHistoryWindow", String.valueOf(Constants.CE_SHOW_PROJECT_HISTORY_WINDOW))));
            pnlProjectHistory.setSelectedChcbIndexCommands(Boolean.parseBoolean(commandEditorProp.getProperty(
                    "IndexCommandsInProjectHistoryWindow",
                    String.valueOf(Constants.CE_INDEX_COMMANDS_IN_PROJECT_HISTORY_WINDOW))));
            // Zpřístupní / Znepřístupní se vybrané komponenty:
            pnlProjectHistory.checkSelectedChcbShowHistory();

            pnlCompleteHistory.setSelectedChcbShowHistory(Boolean.parseBoolean(commandEditorProp.getProperty(
                    "ShowCompleteHistoryWindow", String.valueOf(Constants.CD_SHOW_COMPLETE_HISTORY_WINDOW))));
            pnlCompleteHistory.setSelectedChcbIndexCommands(Boolean.parseBoolean(commandEditorProp.getProperty(
                    "IndexCommandsInCompleteHistoryWindow",
                    String.valueOf(Constants.CD_INDEX_COMMANDS_IN_COMPLETE_HISTORY_WINDOW))));
            // Zpřístupní / Znepřístupní se vybrané komponenty:
            pnlCompleteHistory.checkSelectedChcbShowHistory();
        }
    }
}