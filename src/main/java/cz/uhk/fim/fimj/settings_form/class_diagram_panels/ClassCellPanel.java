package cz.uhk.fim.fimj.settings_form.class_diagram_panels;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.language.EditProperties;
import cz.uhk.fim.fimj.settings_form.ReadWriteDiagramProperties;

/**
 * Třída slouží jako panel v nastavení pro nastavení parametru buňky, reprezentující třídu v diagramu tříd
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ClassCellPanel extends CellPanelAbstract
		implements ActionListener, LanguageInterface, ReadWriteDiagramProperties {
	
	private static final long serialVersionUID = 1L;

	
	
	/**
	 * Konstruktor této třídy.
	 */
	public ClassCellPanel() {
		super();
		
		setLayout(new GridBagLayout());
		
		
		// Vytvořím si instanci:
		final GridBagConstraints gbc = getGbc();
		
		index = 0;
		
		gbc.gridwidth = 3;
		
		// Nastavím komponenty do tohoto panelu:
		chcbCellOpaque = new JCheckBox();
		setGbc(gbc, index, 0, chcbCellOpaque, this);
		
		
				
		
		
		
		gbc.gridwidth = 1;
		
		
		lblFontSize = new JLabel();
		setGbc(gbc, ++index, 0, lblFontSize, this);
		
		cmbFontSize = new JComboBox<>(getArrayFontSizes());
		setGbc(gbc, index, 1, cmbFontSize, this);
		
		
		
		
		lblFontStyle = new JLabel();
		setGbc(gbc, ++index, 0, lblFontStyle, this);
		
		cmbFontStyle = new JComboBox<>(FONT_STYLES.toArray(new String[] {}));
		setGbc(gbc, index, 1, cmbFontStyle, this);
		
		
		
		
		lblTextAlignmentX = new JLabel();
		setGbc(gbc, ++index, 0, lblTextAlignmentX, this);
		
		cmbTextAlignmentX = new JComboBox<>(ARRAY_ALIGNMENT_X.toArray(new String[] {}));
		setGbc(gbc, index, 1, cmbTextAlignmentX, this);
		
		
		
		
		lblTextAlignmentY = new JLabel();
		setGbc(gbc, ++index, 0, lblTextAlignmentY, this);
		
		cmbTextAlignmentY = new JComboBox<>(ARRAY_ALIGNMENT_Y.toArray(new String[] {}));
		setGbc(gbc, index, 1, cmbTextAlignmentY, this);
		
		
		
		
		lblTextColor = new JLabel();
		setGbc(gbc, ++index, 0, lblTextColor, this);
		
		btnChooseColor = new JButton();
		btnChooseColor.addActionListener(this);
		setGbc(gbc, index, 1, btnChooseColor, this);
		
		lblChooseColorText = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChooseColorText.setOpaque(true);
		setGbc(gbc, index, 2, lblChooseColorText, this);
		
		
		
		
		
		// inicializuji radiobutotny:
		initializeRadioButtons();
		
		// pridám jim události:
		rbTwoColor.addActionListener(this);
		rbOneColor.addActionListener(this);
		
		
		
		
		setGbc(gbc, ++index, 0, rbTwoColor, this);
		
		
		
		
		lblBgColorLeft = new JLabel();
		setGbc(gbc, ++index, 0, lblBgColorLeft, this);
		
		btnChooseBgColorLeft = new JButton();
		btnChooseBgColorLeft.addActionListener(this);		
		setGbc(gbc, index, 1, btnChooseBgColorLeft, this);
		
		lblChooseBgColorLeft = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChooseBgColorLeft.setOpaque(true);
		setGbc(gbc, index, 2, lblChooseBgColorLeft, this);
		
		
		
		
		
		lblBgColorRight = new JLabel();
		setGbc(gbc, ++index, 0, lblBgColorRight, this);
		
		btnChooseBgColorRight = new JButton();
		btnChooseBgColorRight.addActionListener(this);
		setGbc(gbc, index, 1, btnChooseBgColorRight, this);
		
		lblChooseColorBgRight = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChooseColorBgRight.setOpaque(true);
		setGbc(gbc, index, 2, lblChooseColorBgRight, this);
		
		
		
		
		
		
		
		setGbc(gbc, ++index, 0, rbOneColor, this);
		
		lblBgColor = new JLabel();
		setGbc(gbc, ++index, 0, lblBgColor, this);
		
		btnBgColor = new JButton();
		btnBgColor.addActionListener(this);
		setGbc(gbc, index, 1, btnBgColor, this);
		
		lblChoosedColor = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChoosedColor.setOpaque(true);
		setGbc(gbc, index, 2, lblChoosedColor, this);		
	}
	
	

	
	
	

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnChooseColor) {				
				// Nejprve si zvolenou barvu uložím do dočasné proměnné - pouze v metodě, prtože 
				// když uživatel dialog zruši barby by byla null a následně by se uložila do souboru, což je chyba
				final Color tempColor = JColorChooser.showDialog(this, txtChooseTextColor, Constants.CD_TEXT_COLOR); 
				
				if (tempColor != null) {
					clrSelectedTextColor = tempColor;
					lblChooseColorText.setForeground(clrSelectedTextColor);
					lblChooseColorText.setBackground(clrSelectedTextColor);	
				}
			}
			
			
			
			
			else if (e.getSource() == btnChooseBgColorLeft) {				
				final Color tempColor = JColorChooser.showDialog(this, txtBgColor, Constants.CD_CLASS_BACKGROUND_COLOR);
				
				if (tempColor != null) {
					clrSelectedColorLeft = tempColor;
					lblChooseBgColorLeft.setForeground(clrSelectedColorLeft);
					lblChooseBgColorLeft.setBackground(clrSelectedColorLeft);	
				}
			}
			
			
			
			else if (e.getSource() == btnChooseBgColorRight) {
				final Color tempColor = JColorChooser.showDialog(this, txtBgColor, Constants.CD_GRADIENT_COLOR);
				
				if (tempColor != null) {
					clrSelectedColorRight = tempColor;
					lblChooseColorBgRight.setForeground(clrSelectedColorRight);
					lblChooseColorBgRight.setBackground(clrSelectedColorRight);	
				}
			} 
			
			
			
			
			else if (e.getSource() == btnBgColor) {
				final Color tempColor = JColorChooser.showDialog(this, txtBgColor, Constants.CD_ONE_BG_COLOR);
				
				if (tempColor != null) {
					// Uložím si vybranou barvu pozadí:
					clrSelectedBgColor = tempColor;
					
					// a obarvím Label zvolenou barvou, aby o tom věděl uživatel:
					lblChoosedColor.setForeground(tempColor);
					lblChoosedColor.setBackground(tempColor);
				}
			}
		}
		
		
		
		
		
		// Nyní otestuji, zda se náhodou nekliklo na jeden z radioButtonů, pokud ano,
		// tak musím některé komponenty pro výběr barvy povolit a jiné zakázat
		else if (e.getSource() instanceof JRadioButton) {
			if (e.getSource() == rbOneColor)
				enableColorItems(false);
			
			
			else if (e.getSource() == rbTwoColor)
				enableColorItems(true);
		}
	}


	
	

	

	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			setBorder(BorderFactory.createTitledBorder(properties.getProperty("Sf_Cd_ClassCellBorderTitle", Constants.SF_CD_PNL_CLASS_CELL_BORDER_TITLE)));			
			
			chcbCellOpaque.setText(properties.getProperty("Sf_Cd_CheckBoxOpaqueClassCell", Constants.SF_CHCB_OPAQUE_CELL));
			
			lblFontSize.setText(properties.getProperty("Sf_Cd_LabelFontSizeClassCell", Constants.SF_LBL_FONT_SIZE_CELL) + ": ");
			lblFontStyle.setText(properties.getProperty("Sf_Cd_LabelFontStyleClassCell", Constants.SF_LBL_FONT_STYLE_CELL) + ": ");
			lblTextAlignmentX.setText(properties.getProperty("Sf_Cd_LabelTextAlignent_X_ClassCell", Constants.SF_LBL_TEXT_ALIGNMENT_X_CELL) + ": ");
			lblTextAlignmentY.setText(properties.getProperty("Sf_Cd_LabelTextAlignment_Y_ClassCell", Constants.SF_LBL_TEXT_ALIGNMENT_Y_CELL) + ": ");
			lblTextColor.setText(properties.getProperty("Sf_Cd_LabelTextColorClassCell", Constants.SF_LBL_TEXT_COLOR_CELL) + ": ");
			lblBgColorLeft.setText(properties.getProperty("Sf_Cd_LabelBackgroundColor_Left_ClassCell", Constants.SF_LBL_BG_COLOR_LEFT_CELL) + ": ");
			lblBgColorRight.setText(properties.getProperty("Sf_Cd_LabelBackgroundColor_Right_ClassCell", Constants.SF_LBL_BG_COLOR_RIGHT_CELL) + ": ");
			
			btnChooseColor.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));
			btnChooseBgColorLeft.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));
			btnChooseBgColorRight.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));
			
			
			txtChooseTextColor = properties.getProperty("Sf_Cd_ChooseTextColorDialog", Constants.SF_TXT_CHOOSE_TEXT_COLOR);
			txtBgColor = properties.getProperty("Sf_Cd_ChooseBackgroundColorDialog", Constants.SF_TXT_CHOOSE_BG_COLOR);
			
			
			rbOneColor.setText(properties.getProperty("Sf_Cd_TxtOneColorForCell", Constants.SF_TXT_ONE_COLOR_FOR_CELL));
			rbTwoColor.setText(properties.getProperty("Sf_Cd_TxtTwoColorForCell", Constants.SF_TXT_TWO_COLOR_FOR_CELL));
			lblBgColor.setText(properties.getProperty("Sf_Cd_TxtBgColorForCell", Constants.SF_TXT_BG_COLOR_FOR_CELL) + ": ");
			btnBgColor.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));
		}
		
		
		else {
			setBorder(BorderFactory.createTitledBorder(Constants.SF_CD_PNL_CLASS_CELL_BORDER_TITLE));
			
			chcbCellOpaque.setText(Constants.SF_CHCB_OPAQUE_CELL);
			
			lblFontSize.setText( Constants.SF_LBL_FONT_SIZE_CELL + ": ");
			lblFontStyle.setText(Constants.SF_LBL_FONT_STYLE_CELL + ": ");
			lblTextAlignmentX.setText(Constants.SF_LBL_TEXT_ALIGNMENT_X_CELL + ": ");
			lblTextAlignmentY.setText(Constants.SF_LBL_TEXT_ALIGNMENT_Y_CELL + ": ");
			lblTextColor.setText(Constants.SF_LBL_TEXT_COLOR_CELL + ": ");
			lblBgColorLeft.setText(Constants.SF_LBL_BG_COLOR_LEFT_CELL + ": ");
			lblBgColorRight.setText(Constants.SF_LBL_BG_COLOR_RIGHT_CELL + ": ");
			
			btnChooseColor.setText(Constants.SF_CD_BTN_CHOOSE_COLOR);
			btnChooseBgColorLeft.setText(Constants.SF_CD_BTN_CHOOSE_COLOR);
			btnChooseBgColorRight.setText(Constants.SF_CD_BTN_CHOOSE_COLOR);		
			
			// Budou v těchto panelech stejné:
			txtChooseTextColor = Constants.SF_TXT_CHOOSE_TEXT_COLOR;
			txtBgColor = Constants.SF_TXT_CHOOSE_BG_COLOR;
			
			rbOneColor.setText(Constants.SF_TXT_ONE_COLOR_FOR_CELL);
			rbTwoColor.setText(Constants.SF_TXT_TWO_COLOR_FOR_CELL);
			lblBgColor.setText(Constants.SF_TXT_BG_COLOR_FOR_CELL + ": ");
			btnBgColor.setText(Constants.SF_CD_BTN_CHOOSE_COLOR);
		}
	}
	

	

	
	
	
	@Override
	public void readDataFromDiagramProp(final Properties classDiagramProperties) {
		chcbCellOpaque.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("ClassCellOpaque", String.valueOf(Constants.CD_OPAQUE_CLASS_CELL))));
		
		final String textColor = classDiagramProperties.getProperty("ForegroundColor", Integer.toString(Constants.CD_TEXT_COLOR.getRGB()));
		lblChooseColorText.setBackground(new Color(Integer.parseInt(textColor)));
		lblChooseColorText.setForeground(new Color(Integer.parseInt(textColor)));
		clrSelectedTextColor = new Color(Integer.parseInt(textColor));
		
		final String gradientColor = classDiagramProperties.getProperty("GradientColor", Integer.toString(Constants.CD_GRADIENT_COLOR.getRGB()));
		lblChooseColorBgRight.setBackground(new Color(Integer.parseInt(gradientColor)));
		lblChooseColorBgRight.setForeground(new Color(Integer.parseInt(gradientColor)));
		clrSelectedColorRight = new Color(Integer.parseInt(gradientColor));
		
		final String backgroundColor = classDiagramProperties.getProperty("ClassBackgroundColor", Integer.toString(Constants.CD_CLASS_BACKGROUND_COLOR.getRGB()));
		lblChooseBgColorLeft.setBackground(new Color(Integer.parseInt(backgroundColor)));
		lblChooseBgColorLeft.setForeground(new Color(Integer.parseInt(backgroundColor)));
		clrSelectedColorLeft = new Color(Integer.parseInt(backgroundColor));
		
		
		cmbFontSize.setSelectedItem(Integer.parseInt(classDiagramProperties.getProperty("ClassFontSize", Integer.toString(Constants.CD_FONT_CLASS.getSize()))));
		
		
		
		
		final String bgColor = classDiagramProperties.getProperty("ClassOneBackgroundColor", Integer.toString(Constants.CD_ONE_BG_COLOR.getRGB()));
		lblChoosedColor.setBackground(new Color(Integer.parseInt(bgColor)));
		lblChoosedColor.setForeground(new Color(Integer.parseInt(bgColor)));
		clrSelectedBgColor = new Color(Integer.parseInt(bgColor));
		
		
		// načtu si logickouk proměnnou, dle které zjistím kolik barev se má nastavit pro pozadí buňky:
		final boolean oneBgColor = Boolean.parseBoolean(classDiagramProperties.getProperty("ClassOneBgColorBoolean", String.valueOf(Constants.CD_ONE_BG_COLOR_BOOLEAN)));
		
		// pokud je hodnota true, pak se jedná o jednu barvu pozadi pro bunku coby tridu
		if (oneBgColor) {			
			// nyní označím některé komponenty:
			rbOneColor.setSelected(true);
			enableColorItems(false);
		}
		
		else {
			// Zde je hodnota false, tak se jedná o dvě barvy pzadí pro buku coby tridu:
			rbTwoColor.setSelected(true);
			enableColorItems(true);
		}
		
		
		
		final int fontStyle = Integer.parseInt(classDiagramProperties.getProperty("ClassFontType", Integer.toString(Constants.CD_FONT_CLASS.getStyle())));
		
		if (fontStyle == 0)		// Plain
			cmbFontStyle.setSelectedIndex(2);
		else if (fontStyle == 1)	// Bold
			cmbFontStyle.setSelectedIndex(0);
		else if (fontStyle == 2)	// Italic
			cmbFontStyle.setSelectedIndex(1);
		
		
		// Zarovnání:
		/*
		 * center: 0
		 * left: 2
		 * right: 4
		 * top: 1
		 * bottom: 3
		 */
		final int verticalAlignment = Integer.parseInt(classDiagramProperties.getProperty("VerticalAlignment", String.valueOf(Constants.CD_VERTICAL_ALIGNMENT)));
		
		if (verticalAlignment == 1)	// Top			
			cmbTextAlignmentY.setSelectedIndex(0);
		else if (verticalAlignment == 0) 	// Center
			cmbTextAlignmentY.setSelectedIndex(1);
		else if (verticalAlignment == 3)	// Bottom
			cmbTextAlignmentY.setSelectedIndex(2);
			
		
		final int horizontalAlignment = Integer.parseInt(classDiagramProperties.getProperty("HorizontalAlignment", String.valueOf(Constants.CD_HORIZONTAL_ALIGNMENT)));
		
		if (horizontalAlignment == 2)	// Left
			cmbTextAlignmentX.setSelectedIndex(0);
		else if (horizontalAlignment == 0)	// Center
			cmbTextAlignmentX.setSelectedIndex(1);
		else if (horizontalAlignment == 4)	// Right
			cmbTextAlignmentX.setSelectedIndex(2);
	}
	
	
	

	
	
	@Override
	public EditProperties writeDataToDiagramProp(final EditProperties classDiagramProperties) {
		classDiagramProperties.setProperty("ClassCellOpaque", String.valueOf(chcbCellOpaque.isSelected()));
		
		// Barva textu:
		if (clrSelectedTextColor != null)
			classDiagramProperties.setProperty("ForegroundColor", Integer.toString(clrSelectedTextColor.getRGB()));
		
		if (clrSelectedColorRight != null)
			classDiagramProperties.setProperty("GradientColor", Integer.toString(clrSelectedColorRight.getRGB()));
		
		if (clrSelectedColorLeft != null)
			classDiagramProperties.setProperty("ClassBackgroundColor", Integer.toString(clrSelectedColorLeft.getRGB()));
		
		if (clrSelectedBgColor != null)
			classDiagramProperties.setProperty("ClassOneBackgroundColor", Integer.toString(clrSelectedBgColor.getRGB()));
		
		
		// Logckou proměnnou, zda je označena jedna barva pro buňku nebo dve:
		classDiagramProperties.setProperty("ClassOneBgColorBoolean", String.valueOf(rbOneColor.isSelected()));
		
		
				
		classDiagramProperties.setProperty("ClassFontSize", Integer.toString((Integer) cmbFontSize.getSelectedItem()));
		
		
		
		final int verticalAlignment = cmbTextAlignmentY.getSelectedIndex();
		
		if (verticalAlignment == 0)
			classDiagramProperties.setProperty("VerticalAlignment", Integer.toString(1));
		else if (verticalAlignment == 1)
			classDiagramProperties.setProperty("VerticalAlignment", Integer.toString(0));
		else if (verticalAlignment == 2)
			classDiagramProperties.setProperty("VerticalAlignment", Integer.toString(3));
		
		
		final int horizontalAlignment = cmbTextAlignmentX.getSelectedIndex();
		
		if (horizontalAlignment == 0)
			classDiagramProperties.setProperty("HorizontalAlignment", Integer.toString(2));
		else if (horizontalAlignment == 1)
			classDiagramProperties.setProperty("HorizontalAlignment", Integer.toString(0));
		else if (horizontalAlignment == 2)
			classDiagramProperties.setProperty("HorizontalAlignment", Integer.toString(4));
		
		
		final int fontStyle = cmbFontStyle.getSelectedIndex();
		
		if (fontStyle == 0)
			classDiagramProperties.setProperty("ClassFontType", Integer.toString(1));
		else if (fontStyle == 1)
			classDiagramProperties.setProperty("ClassFontType", Integer.toString(2));
		else if (fontStyle == 2)
			classDiagramProperties.setProperty("ClassFontType", Integer.toString(0));
		
		
		return classDiagramProperties;
	}
}