package cz.uhk.fim.fimj.settings_form.instance_diagram_panels;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.language.EditProperties;
import cz.uhk.fim.fimj.settings_form.ReadWriteDiagramProperties;

/**
 * Tato třída slouží jako panel, který obsahuje komponenty pro nastavení vlastností pro metody, které se mohou
 * vykreslovat v buňce, která v diagramu instancí reprezentuje instanci nějaké třídy z diagramu tříd.
 * <p>
 * Jedná se například o nastavení toho, zda se ty metody mají v buňce, která v diagramu instancí reprezentuje instanci
 * zobrazovat nebo ne, případě jako barvou a fontem apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class InstanceCellMethodsPanel extends InstanceAttributesMethodsAbstract
		implements LanguageInterface, ActionListener, ReadWriteDiagramProperties {

	private static final long serialVersionUID = 1L;

	
	/**
	 * Konstruktor této třídy.
	 */
	public InstanceCellMethodsPanel() {
		super();
		
		setLayout(new GridBagLayout());
		
		final GridBagConstraints gbc = getGbc();
		
		index = 0;
		
		
		
		// Zda se mají zobrazovat atributy:
		chcbShow = new JCheckBox();
		chcbShow.addActionListener(this);
		setGbc(gbc, index, 0, chcbShow, this);
		
		
		// Zda se mají zobrazit všechny atributy:
		chcbShowAll = new JCheckBox();
		chcbShowAll.addActionListener(this);
		setGbc(gbc, ++index, 0, chcbShowAll, this);
		
		
		
		gbc.gridwidth = 1;
		
		// Nastavení počtu zobrazených atributů:
		lblShowSpecificCount = new JLabel();
		setGbc(gbc, ++index, 0, lblShowSpecificCount, this);
		
		// cmb pro nastavení počtu:
		cmbCount = new JComboBox<>(getCmbCountModel());
		setGbc(gbc, index, 1, cmbCount, this);
		
		
		
		
		gbc.gridwidth = 3;
		
		
		
		
		// Zda se má využít stejný font a barva písma jako text s referencí:
		chcbUseSameFontAndColorAsReference = new JCheckBox();
		chcbUseSameFontAndColorAsReference.addActionListener(this);
		setGbc(gbc, ++index, 0, chcbUseSameFontAndColorAsReference, this);
		
		
		gbc.gridwidth = 1;
		
		
		// Velikost písma:
		lblFontSize = new JLabel();
		setGbc(gbc, ++index, 0, lblFontSize, this);
		
		cmbFontSize = new JComboBox<>(getArrayFontSizes());
		setGbc(gbc, index, 1, cmbFontSize, this);
		
		
		
		
		// Styl / typ písma:
		lblFontStyle = new JLabel();
		setGbc(gbc, ++index, 0, lblFontStyle, this);
		
		cmbFontStyle = new JComboBox<>(FONT_STYLES.toArray(new String[] {}));
		setGbc(gbc, index, 1, cmbFontStyle, this);
		
		
		
		// Barva písma:
		lblTextColor = new JLabel();
		setGbc(gbc, ++index, 0, lblTextColor, this);
		
		btnChooseColor = new JButton();
		btnChooseColor.addActionListener(this);
		setGbc(gbc, index, 1, btnChooseColor, this);
		
		lblChooseColorText = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChooseColorText.setOpaque(true);
		setGbc(gbc, index, 2, lblChooseColorText, this);
	}
	
	
	
	
	
	
	
	
	
	@Override
	public EditProperties writeDataToDiagramProp(final EditProperties properties) {
		properties.setProperty("ShowMethods", String.valueOf(chcbShow.isSelected()));
		
		properties.setProperty("ShowAllMethods", String.valueOf(chcbShowAll.isSelected()));
		
		properties.setProperty("UseForMethodsOneFontAndFontColorInInstanceCell", String.valueOf(chcbUseSameFontAndColorAsReference.isSelected()));

		properties.setProperty("ShowSpecificCountOfMethods", String.valueOf(cmbCount.getSelectedItem()));
		
		if (clrTempFontColor != null)
		properties.setProperty("MethodsTextColor", Integer.toString(clrTempFontColor.getRGB()));
		
		properties.setProperty("MethodsFontSize", String.valueOf(cmbFontSize.getSelectedItem()));
		
		final int fontStyle = cmbFontStyle.getSelectedIndex();
		
		if (fontStyle == 0)
			properties.setProperty("MethodsFontStyle", Integer.toString(1));
		else if (fontStyle == 1)
			properties.setProperty("MethodsFontStyle", Integer.toString(2));
		else if (fontStyle == 2)
			properties.setProperty("MethodsFontStyle", Integer.toString(0));
		
		return properties;
	}

	
	
	
	@Override
	public void readDataFromDiagramProp(final Properties properties) {
		chcbShow.setSelected(Boolean.parseBoolean(properties.getProperty("ShowMethods", String.valueOf(Constants.ID_SHOW_METHODS))));
		
		chcbShowAll.setSelected(Boolean.parseBoolean(properties.getProperty("ShowAllMethods", String.valueOf(Constants.ID_SHOW_ALL_METHODS))));
		
		chcbUseSameFontAndColorAsReference.setSelected(Boolean.parseBoolean(properties.getProperty("UseForMethodsOneFontAndFontColorInInstanceCell", String.valueOf(Constants.ID_USE_FOR_METHODS_ONE_FONT_AND_FONT_COLOR_IN_INSTANCE_CELL))));
		
		
		// Počet proměnných pro zobrazení v instanci:
		final String tempSpecificCountOfVariables = properties.getProperty("ShowSpecificCountOfMethods", Integer.toString(Constants.ID_SPECIFIC_COUNT_OF_METHODS));
		final int tempCount = getIntegerFromString(tempSpecificCountOfVariables, Constants.ID_SPECIFIC_COUNT_OF_METHODS);
		setSelectedCmbCount(tempCount);
		
		
		// Barva písma:
		final String textColor = properties.getProperty("MethodsTextColor", Integer.toString(Constants.ID_METHODS_TEXT_COLOR.getRGB()));
		clrTempFontColor = new Color(Integer.parseInt(textColor));
		lblChooseColorText.setBackground(clrTempFontColor);
		lblChooseColorText.setForeground(clrTempFontColor);
		
		
		
		
		final String tempFontSize = properties.getProperty("MethodsFontSize", Integer.toString(Constants.ID_METHODS_FONT.getSize()));
		final int fontSizeTemp = getIntegerFromString(tempFontSize, Constants.ID_METHODS_FONT.getSize());
		setSelectedCmbFontSize(fontSizeTemp);
		
		
		
		
		
		final int fontStyle = Integer.parseInt(properties.getProperty("MethodsFontStyle", Integer.toString(Constants.ID_METHODS_FONT.getStyle())));
		
		if (fontStyle == 0)		// Plain
			cmbFontStyle.setSelectedIndex(2);
		else if (fontStyle == 1)	// Bold
			cmbFontStyle.setSelectedIndex(0);
		else if (fontStyle == 2)	// Italic
			cmbFontStyle.setSelectedIndex(1);
		
		
		
		
		
		
		
		// Zde pouze otestuji, zda jsou některé komponennty označeny nebo ne a dle toho
		// se případně zpřístupní / znepřístupní nekteré komponenty pro nastavení
		// některých vlastností:
		enabledAllComponents(chcbShow.isSelected());

		if (chcbShow.isSelected()) {
			enableComponentsForCountOfAttributsOrMethods(!chcbShowAll.isSelected());

			enableComponentsForFontAndColorOfText(!chcbUseSameFontAndColorAsReference.isSelected());
		}
	}

	
	
	
	
	
	
	
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JCheckBox) {
			// Zpřístupním nebo znepřístupním položky v panelu, když se nemají zobrazovat
			// atributy, tak ani nepůjdou nastavit jejich vlastnosti:
			if (e.getSource() == chcbShow) {
				// Zpřístupním nebo znepřístupním všechny komponenty:
				enabledAllComponents(chcbShow.isSelected());

				/*
				 * Zde pouze v případě, že je nastaveno, že se mají zobrazovat atributy, takže
				 * jsou povoleny i ostatní komponenty v tomto panelu, tak mohu dále zjistit, zda
				 * se mají nebo neají zpřístupnit komponenty pro nastavení počtu atributů a
				 * fontu s barvou písma pro atributy.
				 */
				if (chcbShow.isSelected()) {
					enableComponentsForCountOfAttributsOrMethods(!chcbShowAll.isSelected());
					enableComponentsForFontAndColorOfText(!chcbUseSameFontAndColorAsReference.isSelected());
				}
			}
				

			
			// Pokud se mají zobrazit všechny atributy, pak znepřístupním komponenty pro
			// nastavení počtu atributů pro zobrazení:
			else if (e.getSource() == chcbShowAll)
				enableComponentsForCountOfAttributsOrMethods(!chcbShowAll.isSelected());
			
			
			// Pokud se má aplikovat stejný font a barva písma pro atributy, pak
			// znepřístupním komponenty pro nastavení jiného fontu písma nebo brvy písma:
			else if (e.getSource() == chcbUseSameFontAndColorAsReference)
				enableComponentsForFontAndColorOfText(!chcbUseSameFontAndColorAsReference.isSelected());
		}
		
		
		else if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnChooseColor) {
				final Color tempColor = JColorChooser.showDialog(this, txtClrDialogTitle,
						Constants.ID_ATTRIBUTES_TEXT_COLOR);

				if (tempColor != null) {
					clrTempFontColor = tempColor;
					lblChooseColorText.setForeground(clrTempFontColor);
					lblChooseColorText.setBackground(clrTempFontColor);
				}
			}
		}
	}

	
	
	
	
	
	
	
	
	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			setBorder(BorderFactory.createTitledBorder(properties.getProperty("ICMP_DialogTitle", Constants.ICMP_BORDER_TITLE)));

			chcbShow.setText(properties.getProperty("ICMP_Chcb_Show_Text", Constants.ICMP_CHCB_SHOW_TEXT));
			chcbShowAll.setText(properties.getProperty("ICMP_Chcb_ShowAll_Text", Constants.ICMP_CHCB_SHOW_ALL_TEXT));
			chcbUseSameFontAndColorAsReference.setText(properties.getProperty("ICMP_Chcb_UseSameFontAndColorAsReference_Text", Constants.ICMP_CHCB_USE_SAME_FONT_AND_COLOR_AS_REFERENCE_TEXT));

			chcbShow.setToolTipText(properties.getProperty("ICMP_Chcb_Show_TT", Constants.ICMP_CHCB_SHOW_TT));
			chcbShowAll.setToolTipText(properties.getProperty("ICMP_Chcb_ShowAll_TT", Constants.ICMP_CHCB_SHOW_ALL_TT));
			chcbUseSameFontAndColorAsReference.setToolTipText(properties.getProperty("ICMP_Chcb_UseSameFontAndColorAsReference_TT", Constants.ICMP_CHCB_USE_SAME_FONT_AND_COLOR_AS_REFERENCE_TT));

			lblShowSpecificCount.setText(properties.getProperty("ICMP_Lbl_ShowSpecificCount_Text", Constants.ICMP_LBL_SHOW_SPECIFIC_COUNT_TEXT));
			lblShowSpecificCount.setToolTipText(properties.getProperty("ICMP_Lbl_ShowSpecificCount_TT", Constants.ICMP_LBL_SHOW_SPECIFIC_COUNT_TT));

			lblFontSize.setText(properties.getProperty("ICMP_Lbl_FontSize", Constants.ICMP_LBL_FONT_SIZE));
			lblFontStyle.setText(properties.getProperty("ICMP_Lbl_FontStyle", Constants.ICMP_LBL_FONT_STYLE));
			lblTextColor.setText(properties.getProperty("ICMP_Lbl_TextColor", Constants.ICMP_LBL_TEXT_COLOR));

			btnChooseColor.setText(properties.getProperty("ICMP_BtnChooseColor", Constants.ICMP_BTN_CHOOSE_COLOR));

			txtClrDialogTitle = properties.getProperty("ICMP_Txt_Clr_DialogTitle", Constants.ICMP_TXT_CLR_DIALOG_TITLE);
		}
		
		
		else {
			setBorder(BorderFactory.createTitledBorder(Constants.ICMP_BORDER_TITLE));

			chcbShow.setText(Constants.ICMP_CHCB_SHOW_TEXT);
			chcbShowAll.setText(Constants.ICMP_CHCB_SHOW_ALL_TEXT);
			chcbUseSameFontAndColorAsReference.setText(Constants.ICMP_CHCB_USE_SAME_FONT_AND_COLOR_AS_REFERENCE_TEXT);

			chcbShow.setToolTipText(Constants.ICMP_CHCB_SHOW_TT);
			chcbShowAll.setToolTipText(Constants.ICMP_CHCB_SHOW_ALL_TT);
			chcbUseSameFontAndColorAsReference
					.setToolTipText(Constants.ICMP_CHCB_USE_SAME_FONT_AND_COLOR_AS_REFERENCE_TT);

			lblShowSpecificCount.setText(Constants.ICMP_LBL_SHOW_SPECIFIC_COUNT_TEXT);
			lblShowSpecificCount.setToolTipText(Constants.ICMP_LBL_SHOW_SPECIFIC_COUNT_TT);

			lblFontSize.setText(Constants.ICMP_LBL_FONT_SIZE);
			lblFontStyle.setText(Constants.ICMP_LBL_FONT_STYLE);
			lblTextColor.setText(Constants.ICMP_LBL_TEXT_COLOR);

			btnChooseColor.setText(Constants.ICMP_BTN_CHOOSE_COLOR);

			txtClrDialogTitle = Constants.ICMP_TXT_CLR_DIALOG_TITLE;
		}
		
		
		chcbShow.setText("? " + chcbShow.getText());
		chcbShowAll.setText("? " + chcbShowAll.getText());
		chcbUseSameFontAndColorAsReference.setText("? " + chcbUseSameFontAndColorAsReference.getText());
		
		lblShowSpecificCount.setText("? " + lblShowSpecificCount.getText() + ": ");
		lblFontSize.setText(lblFontSize.getText() + ": ");
		lblFontStyle.setText(lblFontStyle.getText() + ": ");
		lblTextColor.setText(lblTextColor.getText() + ": ");
	}
}
