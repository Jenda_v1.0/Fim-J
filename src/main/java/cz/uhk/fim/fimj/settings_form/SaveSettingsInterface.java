package cz.uhk.fim.fimj.settings_form;

/**
 * Tot rozhraní - třída slouží na to že když se klikne v settingsForm na tlačítko OK, tak se v příslušných panelech
 * zavolá následující metoda
 * <p>
 * <p>
 * metoda se v příslušné třídě - panelu, zavolá po kliknutí na tlačítko OK jako uložit nastavení, abych nemusel do
 * halvního panelu v diaglou SettingsForm dávat do reakce na kliknutí na tlačítko OK, tak místo toho v každeém panelu
 * napíšu tuto metodu, která uloží příslušná nastavení z každého panelu
 * <p>
 * <p>
 * Dále obsahuje metodu open..., která se po vytvoření instance příslušného panelu, kde se metoda nachází zavolá a načte
 * do příslušného dialogu nastavení z příslušného souboru např v panelu pro CLassDiagram - nastavení načte výchozí
 * hodnoty - zaškrtné výchozí hodnoty v Jcheckboxech, ...
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface SaveSettingsInterface {

    /**
     * Metoda, která uloží nastavení z příslušného panelu, kde se nachází a zapíše ho do příslušného souboru v adresáři
     * configuration v adresáři zvoleným coby Workspace uživatelem
     * <p>
     * pokud nebude soubor s nastavením nalezen zkopíruje se do workspace znovu do složky configuration, aby bylo
     * změněné nastavení kam ukládat
     */
    void saveSetting();


    /**
     * Metoda, která po zavolání otevře v příslušném panelu příslušný soubor s potřebným nastavením daných vlastností
     * které panel obsahuje - např v classDiagrampanelu se nastaví komponenty na výchozí vlastnost, které budou přečteny
     * ze soubour: Pokud bude daný souboru chybět, znovu se vytvoří aby bylo změněné nastavení kam ukládat
     */
    void openSettings();
}