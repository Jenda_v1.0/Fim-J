package cz.uhk.fim.fimj.settings_form.instance_diagram_panels;

import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.settings_form.class_diagram_panels.CellPanelAbstract;

import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;

/**
 * Tato třída slouží pouze jako předek pro deklaraci některých společných komponent pro panely, které slouží pro
 * nastavení atributů a metod v buňce reprezentující instanci instancí.
 * <p>
 * Jedná se o komponenty pro nastavení například toho, zda se budou ty atributy nebo metody příslušné instance
 * zobrazovat v té buňce, která reprezentuje instanci apod.
 * <p>
 * Třída je zde pouze pro to, že ty komponenty stačí deklarovat pouze jednou, protože budou stejné pro oba panely, tak
 * abych předešel duplicitám. Navíc, může být v budoucnu požadavek například na doplnění konstruktorů apod. Tak by pak
 * stačilo pouze doplnit třídu / panel pro konstruktory, který bude dědit z této třídy a kód je možné zkopírovat a
 * upravit pro potřeby těch konstruktorů.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class InstanceAttributesMethodsAbstract extends CellPanelAbstract {

    private static final long serialVersionUID = 6868010854860040113L;
    /**
     * Komponenta pro nastavení toho, zda se vůbec mají zobrazit / vykreslovat atributy nebo metody v buňce, která
     * reprezentuje instanci v diagramu instancí.
     */
    JCheckBox chcbShow;

    /**
     * Komponenta, která slouží pro nastavení toho, zda se mají zobrazit veškeré atributy nebo veškeré metody v
     * příslušné instanci nebo jen zvolený počet atributů nebo metod.
     */
    JCheckBox chcbShowAll;


    /**
     * Label, která obsahuje text, který informuje uživatele, že si může nastavit určitý počet atributů nebo metod,
     * který se může zobrazit v buňce pro instanci v diagramu instancí.
     */
    JLabel lblShowSpecificCount;

    /**
     * Komponenta, která slouží pro nastavení specifického počtu atributů nebo metod, která se může zobrazit v buňce,
     * která reprezentuje instanci v diagramu instancí.
     */
    JComboBox<Integer> cmbCount;


    /**
     * Komponenta, která slouží pro nastavení toho, zda se mají pro písmo, kterým se budou vykreslovat / zobrazovat
     * atributy nebo metody využít stejný font a barva písma, jako je pro text reference - tedy "obecně" pro buňku
     * instance třídy v diagramu instancí.
     */
    JCheckBox chcbUseSameFontAndColorAsReference;


    /**
     * Proměnná, která slouží pro uložení zvolené barvy pro písmo, kterým se budou vykreslovat atributy nebo metody v
     * buňce / instanci.
     */
    Color clrTempFontColor;


    /**
     * Proměnná, která slouží pro "uložení" textu, který se vloží do titulku dialogu pro výběr barvy písma pro atributy
     * nebo metody.
     */
    String txtClrDialogTitle;


    /**
     * Metoda, která slouží pro vytvoření modelu pro komponentu JComboBox. Jedná o model - jednorozměrné pole o dvaceti
     * položkách, které značí počet atributů nebo metod, které jemožné zobrazit v buňce reprezentující instanci.
     *
     * @return výše popsané jednorozměrné pole o dvaceti položkách.
     */
    static Integer[] getCmbCountModel() {
        Integer[] model = new Integer[20];

        for (int i = 1; i <= 20; i++)
            model[i - 1] = i;

        return model;
    }


    /**
     * Metoda, která slouží pro zpřístupnění / znepřístupnění položek téměř všech komponent v příslušném panelu pro
     * nastavení vlastností pro atrbuty a metody v buňce reprezentující instanci v diagramu instancí.
     * <p>
     * Tato metoda je potřeba, když se například označí chcb pro to, zda se mají zobrazovat atributy nebo metody v
     * příslušné buňce / instanci, tak pokud nemají, pak ani nebudou zpřístupněny komponenty pro nastavení těch
     * vlastností pro atributy, například pro barvu písma, velikost písma, ...
     *
     * @param enabled
     *         - logická proměnná, která značí, zda se mají komponenty v těle této metody zpřístupnit nebo ne. True =
     *         zpřístupnit, false = znepřístupnit.
     */
    final void enabledAllComponents(final boolean enabled) {
        chcbShowAll.setEnabled(enabled);
        lblShowSpecificCount.setEnabled(enabled);
        cmbCount.setEnabled(enabled);
        chcbUseSameFontAndColorAsReference.setEnabled(enabled);

        enableComponentsForFontAndColorOfText(enabled);
    }


    /**
     * Metoda, která slouží pro zpřístupnění / znepřístupnění komponent pro nastavení určitého počtu atributů / metod,
     * které se mají zobrazit.
     * <p>
     * Jde o to, že když uživatel zvolí, že se mají zobrazit všechny atributy nebo metody, pak se znepřístupní
     * komponenty pro nastavení určitého počtu atributů nebo metod pro zobrazení, to by se stejně nevyužilo, tak proč
     * jej nechávat přístupné?
     *
     * @param enabled
     *         - logická hodnota, která značí, zda se výše popsané komponenty mají zpřístupnít (true) nebo znepřístupnit
     *         (false).
     */
    final void enableComponentsForCountOfAttributsOrMethods(final boolean enabled) {
        lblShowSpecificCount.setEnabled(enabled);
        cmbCount.setEnabled(enabled);
    }


    /**
     * Metoda, která slouží pro zpřístupnšní / znepřístupnění komponent pro nastavení typu písma a velikosti písma
     * (obecně fontu písma) a barvy písma pro atributy nebo metody v buňce reprezentující instanci v diagramu instancí.
     *
     * @param enabled
     *         - logická proměnná, která značí, zda se mají zpřístupnit (true) nebo znepřístupnit (false) komponenty pro
     *         nastavení fontu a barvy písma pro atributy a metody v buňce pro instanci v diagramu instancí.
     */
    final void enableComponentsForFontAndColorOfText(final boolean enabled) {
        lblFontSize.setEnabled(enabled);
        cmbFontSize.setEnabled(enabled);
        lblFontStyle.setEnabled(enabled);
        cmbFontStyle.setEnabled(enabled);
        lblTextColor.setEnabled(enabled);
        btnChooseColor.setEnabled(enabled);
    }


    /**
     * Metoda, která slouží pro získání hodnoty typu Integer z textu number. Jelikož se nemusí podařit přetypovat tu
     * hodnotu na Integer, tak když nastane výjimka vrátí se výchozí hodnota defaultNumber.
     *
     * @param number
     *         - číslo v textu, které se má přetypovat na Integer.
     * @param defaultNumber
     *         - výchozí číslo, které se vrátí v případě, že se text number nepodaří přetypovat na Integer.
     *
     * @return number v typu int, jinak defaultNumber.
     */
    static int getIntegerFromString(final String number, final int defaultNumber) {
        try {

            return Integer.parseInt(number);

        } catch (NumberFormatException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci i chybě:
                logger.log(Level.INFO, "Zachycena výjimka při parsování hodnoty: '" + number + "' na datový typ " +
                        "Integer.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();

            // Vrácení výchozí hodnoty:
            return defaultNumber;
        }
    }


    /**
     * Metoda, která slouží pro označení položky v komponentě typu JComboBox, která slouží pro nastavení počtu atributů,
     * který se bude zobrazovat v buňce reprezentující instanci v diagramu instancí.
     * <p>
     * Tento postup pro označení položky je napsaán takto "zdlouhavě" pouze z toho důvodu, že když by se v hodnotě value
     * nacházela nějaká položka, která v tom modelu JComboBoxu ještě není, pak aby nedošlo k chybě, že by se nenastavila
     * / neoznačila příslušná položka.
     *
     * @param value
     *         - hodnota typu int, která se má označit v komponentě JComboBox.
     */
    final void setSelectedCmbCount(final int value) {
        final int length = cmbCount.getModel().getSize();

        for (int i = 0; i < length; i++)
            if (cmbCount.getModel().getElementAt(i) == value) {
                cmbCount.setSelectedIndex(i);
                return;
            }
    }


    /**
     * Metoda, která slouží pro označení položky v komponentě typu JComboBox, která slouží pro nastavení velikosti písma
     * pro atributy, ktere se mohou zobrazovat v buňce reprezentující instanci v diagramu instancí.
     * <p>
     * Tento postup pro označení položky je napsaán takto "zdlouhavě" pouze z toho důvodu, že když by se v hodnotě value
     * nacházela nějaká položka, která v tom modelu JComboBoxu ještě není, pak aby nedošlo k chybě, že by se nenastavila
     * / neoznačila příslušná položka.
     *
     * @param value
     *         - hodnota typu int, která se má označit v komponentě JComboBox.
     */
    final void setSelectedCmbFontSize(final int value) {
        final int length = cmbFontSize.getModel().getSize();

        for (int i = 0; i < length; i++)
            if (cmbFontSize.getModel().getElementAt(i) == value) {
                cmbFontSize.setSelectedIndex(i);
                return;
            }
    }


    // Setry:

    /**
     * Metoda, která slouží pro označení komponenty JCheckBox, která slouží pro nastavení toho, zda se mají v buňce
     * reprezentující instanci v diagramu instancí zobrazovat atributy nebo metody, ale dále se také nastaví hodnoty k
     * tomu, aby se zpřístupnily nebo znepřístupnily některé komponenty, například, když se nemají vypisovat atributy
     * nebo metody, tak se i znepřístupní položky v příslušném panelu, protože by stejně neměli jit nastavit, když se
     * nebudou používat.
     *
     * @param selected
     *         - true, má se chcb pro to, zda se budou vypisovat atributy nebo metody buňce / intanci označit, jinak
     *         false. A dle této hodnoty se také zpřístupní (true) nebo znepřístupní (false) výše zmíněné komponenty.
     */
    public final void setChcbShowSelected(final boolean selected) {
        chcbShow.setSelected(selected);

        enabledAllComponents(chcbShow.isSelected());

        if (chcbShow.isSelected()) {
            enableComponentsForCountOfAttributsOrMethods(!chcbShowAll.isSelected());

            enableComponentsForFontAndColorOfText(!chcbUseSameFontAndColorAsReference.isSelected());
        }
    }

    /**
     * Označení komponenty JCheckBox pro nastavení toho, zda se mají zobrazit všechny atributy nebo metod v příslušné
     * instanci / buňce reprezentující instanci v diagramu instancí.
     *
     * @param selected
     *         - true, má se výše popsané chcb označit, jinak false.
     */
    public final void setChcbShowAllSelected(final boolean selected) {
        chcbShowAll.setSelected(selected);
    }

    /**
     * Označení komponenty JCheckBox pro nastavení toho, zda se má pro atributy nebo metody využít stejný font a písmo
     * jako "obecně" pro písmo buňky / instance nebo ne.
     *
     * @param selected
     *         true, má se chcb označit, jinak false.
     */
    public final void setChcbUseSameFontAndColorAsReferenceSelected(final boolean selected) {
        chcbUseSameFontAndColorAsReference.setSelected(selected);
    }

    /**
     * Označení položky v komponentě JComboBox, která slouží pro nastavení počtu atributů nebo metod, které se mají
     * zobrazit v buňce reprezentující instanci v diagramu instancí.
     *
     * @param count
     *         - počet, který se má označít v výše zmíněné cmb.
     */
    public final void setCmbCountSelected(final int count) {
        setSelectedCmbCount(count);
    }


    /**
     * Označení nějaké barvy pro písmo pro atributy nebo metody v buňce / instanci.
     *
     * @param color
     *         - barva pro písmo pro atributy nebo metody v buňce reprezentující instanci v diagramu instancí.
     */
    public final void setTextColor(final Color color) {
        clrTempFontColor = color;
        lblChooseColorText.setBackground(clrTempFontColor);
        lblChooseColorText.setForeground(clrTempFontColor);
    }

    /**
     * Metoda pro nastavení velikosti písma pro atributy nebo metody v buňce / instanci.
     *
     * @param fontSize
     *         - velikost písma pro atributy nebo metody v buňce / instanci.
     */
    public final void setTextFontSize(final int fontSize) {
        setSelectedCmbFontSize(fontSize);
    }

    /**
     * Metoda pro nastavení stylu / typu písma pro atributy nebo metody v buňce / instanci v diagramu instancí.
     *
     * @param fontStyle
     *         - styl písma.
     */
    public final void setTextFontStyle(final int fontStyle) {
        if (fontStyle == 0)        // Plain
            cmbFontStyle.setSelectedIndex(2);
        else if (fontStyle == 1)    // Bold
            cmbFontStyle.setSelectedIndex(0);
        else if (fontStyle == 2)    // Italic
            cmbFontStyle.setSelectedIndex(1);
    }
}
