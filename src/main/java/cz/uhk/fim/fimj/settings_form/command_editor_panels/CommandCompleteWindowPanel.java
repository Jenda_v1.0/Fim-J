package cz.uhk.fim.fimj.settings_form.command_editor_panels;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.settings_form.PanelAbstract;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.util.Properties;

/**
 * Panel obsahující komponenty pro nastavení vlastností pro okno pro automatické doplňování dostupných příkazů pro
 * editor příkazů.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 23.09.2018 21:18
 */

public class CommandCompleteWindowPanel extends PanelAbstract implements LanguageInterface, ChangeListener {


    /**
     * Komponenta pro nastavení toho, zda se má zobrazit okno s dostupnými příkazy v editoru příkazů po stisknutí
     * klávesové zkratky Ctrl + Space.
     */
    private final JCheckBox chcbShowAutoCompleteWindow;

    /**
     * Zda se mají v okně pro doplňování příkazů v editoru příkazů zobrazovat referenční proměnné u zobrazených hodnot
     * proměnných v okně pro doplňování příkazů v editoru příkazů.
     */
    private final JCheckBox chcbShowReferenceVariablesInCompleteWindow;

    /**
     * Zda se mají v okně pro doplňování příkazů v editoru příkazů zobrazovat syntaxe pro naplnění / deklaraci proměnné
     * v editoru příkazů tak, že se ta nová nebo existující proměnná v editoru příkazů naplní hodnotou z proměnné v
     * nějaké instanci.
     */
    private final JCheckBox chcbAddSyntaxForFillingTheVariable;

    /**
     * Zda se v okně pro doplňování příkazů v editoru příkazů v syntaxi pro deklaraci "klasické" proměnné má uvádět
     * klíčové slovo "final". Resp. vytvořená proměnná bude implicitně "final".
     */
    private final JCheckBox chcbAddFinalWordForFillingTheVariable;

    /**
     * Zda se mají v okně pro doplňování příkazů v editoru příkazů zobrazovat definované ukázky syntaxí pro práci s
     * editorem příkazů. Například deklarace a naplnění proměnných apod.
     */
    private final JCheckBox chcbShowExamplesOfSyntax;

    /**
     * Zda se mají v okně pro doplňování příkazů v editoru příkazů zobrazovat výchozí definované metody specifické pro
     * editor příkazů. Například metody pro výpis proměnných, nápovědy apod.
     */
    private final JCheckBox chcbShowDefaultMethods;


    /**
     * Konstruktor třídy.
     */
    public CommandCompleteWindowPanel() {
        setLayout(new GridBagLayout());

        final GridBagConstraints gbc = getGbc();
        index = 0;

        chcbShowAutoCompleteWindow = new JCheckBox();
        chcbShowAutoCompleteWindow.addChangeListener(this);
        setGbc(gbc, index, 0, chcbShowAutoCompleteWindow, this);

        chcbShowReferenceVariablesInCompleteWindow = new JCheckBox();
        setGbc(gbc, ++index, 0, chcbShowReferenceVariablesInCompleteWindow, this);

        chcbAddSyntaxForFillingTheVariable = new JCheckBox();
        setGbc(gbc, ++index, 0, chcbAddSyntaxForFillingTheVariable, this);

        chcbAddFinalWordForFillingTheVariable = new JCheckBox();
        setGbc(gbc, ++index, 0, chcbAddFinalWordForFillingTheVariable, this);

        chcbShowExamplesOfSyntax = new JCheckBox();
        setGbc(gbc, ++index, 0, chcbShowExamplesOfSyntax, this);

        chcbShowDefaultMethods = new JCheckBox();
        setGbc(gbc, ++index, 0, chcbShowDefaultMethods, this);
    }


    @Override
    public void stateChanged(ChangeEvent e) {
        if (e.getSource() instanceof JCheckBox && e.getSource() == chcbShowAutoCompleteWindow) {
            /*
             * V případě, že není povoleno zpřístupnění okna s dostupnými příkazy, nebudou zpřístupněny ostatní
             * položky v panelu.
             */
            chechSelectedChcbShowAutoCompleteWindow();
        }
    }


    /**
     * Nastavení zpřístupnění komponent v panelu dle toho, zda je povoleno zpřístupnění okna s dostupnými příkazy.
     */
    public void chechSelectedChcbShowAutoCompleteWindow() {
        setEnabledChcbs(chcbShowAutoCompleteWindow.isSelected());
    }

    /**
     * Nastavení zpřístupnění komponent v panelu.
     *
     * @param enabled
     *         true v případě, že se mají vybrané komponenty zpřístupnit, jinak false.
     */
    private void setEnabledChcbs(final boolean enabled) {
        chcbShowReferenceVariablesInCompleteWindow.setEnabled(enabled);
        chcbAddSyntaxForFillingTheVariable.setEnabled(enabled);
        chcbAddFinalWordForFillingTheVariable.setEnabled(enabled);
        chcbShowExamplesOfSyntax.setEnabled(enabled);
        chcbShowDefaultMethods.setEnabled(enabled);
    }


    @Override
    public void setLanguage(final Properties properties) {
        if (properties != null) {
            setBorder(BorderFactory.createTitledBorder(properties.getProperty("CEP_Pnl_AutoCompleteWindow",
                    Constants.CEP_PNL_AUTO_COMPLETE_WINDOW)));

            chcbShowAutoCompleteWindow.setText(properties.getProperty("CEP_Chcb_ShowAutoCompleteWindow_Text",
                    Constants.CEP_CHCB_SHOW_AUTO_COMPLETE_WINDOW_TEXT));
            chcbShowAutoCompleteWindow.setToolTipText(properties.getProperty("CEP_Chcb_ShowAutoCompleteWindow_TT",
                    Constants.CEP_CHCB_SHOW_AUTO_COMPLETE_WINDOW_TT));

            chcbShowReferenceVariablesInCompleteWindow.setText(properties.getProperty(
                    "CEP_Chcb_ShowReferenceVariablesInCompleteWindow_Text",
                    Constants.CEP_CHCB_SHOW_REFERENCE_VARIABLES_IN_COMPLETE_WINDOW_TEXT));
            chcbShowReferenceVariablesInCompleteWindow.setToolTipText(properties.getProperty(
                    "CEP_Chcb_ShowReferenceVariablesInCompleteWindow_TT",
                    Constants.CEP_CHCB_SHOW_REFERENCE_VARIABLES_IN_COMPLETE_WINDOW_TT));

            chcbAddSyntaxForFillingTheVariable.setText(properties.getProperty(
                    "CEP_Chcb_AddSyntaxForFillingTheVariable_Text",
                    Constants.CEP_CHCB_ADD_SYNTAX_FOR_FILLING_THE_VARIABLE_TEXT));
            chcbAddSyntaxForFillingTheVariable.setToolTipText(properties.getProperty(
                    "CEP_Chcb_AddSyntaxForFillingTheVariable_TT",
                    Constants.CEP_CHCB_ADD_SYNTAX_FOR_FILLING_THE_VARIABLE_TT));

            chcbAddFinalWordForFillingTheVariable.setText(properties.getProperty(
                    "CEP_Chcb_AddFinalWordForFillingTheVariable_Text",
                    Constants.CEP_CHCB_ADD_FINAL_WORD_FOR_FILLING_THE_VARIABLE_TEXT));
            chcbAddFinalWordForFillingTheVariable.setToolTipText(properties.getProperty(
                    "CEP_Chcb_AddFinalWordForFillingTheVariable_TT",
                    Constants.CEP_CHCB_ADD_FINAL_WORD_FOR_FILLING_THE_VARIABLE_TT));

            chcbShowExamplesOfSyntax.setText(properties.getProperty("CEP_Chcb_ShowExamplesOfSyntax_Text",
                    Constants.CEP_CHCB_SHOW_EXAMPLES_OF_SYNTAX_TEXT));
            chcbShowExamplesOfSyntax.setToolTipText(properties.getProperty("CEP_Chcb_ShowExamplesOfSyntax_TT",
                    Constants.CEP_CHCB_SHOW_EXAMPLES_OF_SYNTAX_TT));

            chcbShowDefaultMethods.setText(properties.getProperty("CEP_Chcb_ShowDefaultMethods_Text",
                    Constants.CEP_CHCB_SHOW_DEFAULT_METHODS_TEXT));
            chcbShowDefaultMethods.setToolTipText(properties.getProperty("CEP_Chcb_ShowDefaultMethods_TT",
                    Constants.CEP_CHCB_SHOW_DEFAULT_METHODS_TT));
        } else {
            setBorder(BorderFactory.createTitledBorder(Constants.CEP_PNL_AUTO_COMPLETE_WINDOW));

            chcbShowAutoCompleteWindow.setText(Constants.CEP_CHCB_SHOW_AUTO_COMPLETE_WINDOW_TEXT);
            chcbShowAutoCompleteWindow.setToolTipText(Constants.CEP_CHCB_SHOW_AUTO_COMPLETE_WINDOW_TT);

            chcbShowReferenceVariablesInCompleteWindow.setText(Constants.CEP_CHCB_SHOW_REFERENCE_VARIABLES_IN_COMPLETE_WINDOW_TEXT);
            chcbShowReferenceVariablesInCompleteWindow.setToolTipText(Constants.CEP_CHCB_SHOW_REFERENCE_VARIABLES_IN_COMPLETE_WINDOW_TT);

            chcbAddSyntaxForFillingTheVariable.setText(Constants.CEP_CHCB_ADD_SYNTAX_FOR_FILLING_THE_VARIABLE_TEXT);
            chcbAddSyntaxForFillingTheVariable.setToolTipText(Constants.CEP_CHCB_ADD_SYNTAX_FOR_FILLING_THE_VARIABLE_TT);

            chcbAddFinalWordForFillingTheVariable.setText(Constants.CEP_CHCB_ADD_FINAL_WORD_FOR_FILLING_THE_VARIABLE_TEXT);
            chcbAddFinalWordForFillingTheVariable.setToolTipText(Constants.CEP_CHCB_ADD_FINAL_WORD_FOR_FILLING_THE_VARIABLE_TT);

            chcbShowExamplesOfSyntax.setText(Constants.CEP_CHCB_SHOW_EXAMPLES_OF_SYNTAX_TEXT);
            chcbShowExamplesOfSyntax.setToolTipText(Constants.CEP_CHCB_SHOW_EXAMPLES_OF_SYNTAX_TT);

            chcbShowDefaultMethods.setText(Constants.CEP_CHCB_SHOW_DEFAULT_METHODS_TEXT);
            chcbShowDefaultMethods.setToolTipText(Constants.CEP_CHCB_SHOW_DEFAULT_METHODS_TT);
        }

        chcbShowAutoCompleteWindow.setText("? " + chcbShowAutoCompleteWindow.getText());
        chcbShowReferenceVariablesInCompleteWindow.setText("? " + chcbShowReferenceVariablesInCompleteWindow.getText());
        chcbAddSyntaxForFillingTheVariable.setText("? " + chcbAddSyntaxForFillingTheVariable.getText());
        chcbAddFinalWordForFillingTheVariable.setText("? " + chcbAddFinalWordForFillingTheVariable.getText() + " " +
                "'final'.");
        chcbShowExamplesOfSyntax.setText("? " + chcbShowExamplesOfSyntax.getText());
        chcbShowDefaultMethods.setText("? " + chcbShowDefaultMethods.getText());
    }


    /**
     * Nastavení označení komponenty, která značí, zda se má zpřístupnit okno s dostupnými příkazy pro editor příkazů.
     *
     * @param selected
     *         - true, komponenta se má označit, okno s příkazy bude dostupné. Jinak false.
     */
    public void setChcbShowAutoCompleteWindowSelectedSelected(final boolean selected) {
        chcbShowAutoCompleteWindow.setSelected(selected);
    }


    /**
     * Nastavení označení komponenty, která značí, zda se mají zobrazovat referenční proměnné v okně s dostupnými
     * příkazy pro editor příkazů.
     *
     * @param selected
     *         - true, komponenta se má označit, referenční proměnné budou zobrazeny. Jinak false.
     */
    public void setChcbShowReferenceVariablesInCompleteWindowSelected(final boolean selected) {
        chcbShowReferenceVariablesInCompleteWindow.setSelected(selected);
    }


    /**
     * Nastavení označení komponenty, která značí, zda se mají zobrazovat / nabízet syntaxe pro naplnění / deklaraci
     * proměnných v editorup příkazů hodnotou z proměnné v instanci třídy.
     *
     * @param selected
     *         - true, komponenta se má označit, budou nabízeny syntaxe pro naplnění / deklaraci proměnné v editoru
     *         příkazů. Jinak false.
     */
    public void setChcbAddSyntaxForFillingTheVariableSelected(final boolean selected) {
        chcbAddSyntaxForFillingTheVariable.setSelected(selected);
    }


    /**
     * Nastavení označení komponenty, která značí, zda se má v syntaxi pro deklaraci proměnné v editoru příkazů
     * zobrazovat klíčové slovo "final".
     *
     * @param selected
     *         - true, komponenta se má označit, budou připraveny syntaxe pro deklaraci proměnných v editoru příkazů s
     *         klíčovým slovem "final". Jinak false.
     */
    public void setChcbAddFinalWordForFillingTheVariableSelected(final boolean selected) {
        chcbAddFinalWordForFillingTheVariable.setSelected(selected);
    }


    /**
     * Nastavení označení komponenty, která značí, zda se mají do okna s dostupnými příkazy doplnit ukázky deklarace
     * vybraných proměnných, které jsou podporovány v editoru příkazů.
     *
     * @param selected
     *         - true, komponenta se má označit, budou dostupné ukázky deklarace proměnných v editoru příkazů. Jinak
     *         false.
     */
    public void setChcbShowExamplesOfSyntaxSelected(final boolean selected) {
        chcbShowExamplesOfSyntax.setSelected(selected);
    }


    /**
     * Nastavení označení komponenty, která značí, zda se mají v okně s dostupnými příkazy pro editor příkazů zobrazovat
     * i výchozí definované metody pro tento editor. Například výpis proměnných apod.
     *
     * @param selected
     *         - true, komponenta se má označit, budou dostupné výchozí metody definované pro editor příkazů. Jinak
     *         false.
     */
    public void setChcbShowDefaultMethodsSelected(final boolean selected) {
        chcbShowDefaultMethods.setSelected(selected);
    }


    /**
     * Getr, zda li je označena komponenta pro nastavení zpřístupnění okna s dostupnými příkazy pro editor příkazů.
     *
     * @return true v případě, že je komponenta označena, jinak false.
     */
    public boolean isChcbShowAutoCompleteWindowSelectedSelected() {
        return chcbShowAutoCompleteWindow.isSelected();
    }


    /**
     * Getr, zda li je označena komponenta pro nastavení, zda se mají zobrazovat u hodnot proměnných zobrazených v okně
     * s dostupnými příkazy i referenční proměnné.
     *
     * @return true v případě, že je komponenta označena, jinak false.
     */
    public boolean isChcbShowReferenceVariablesInCompleteWindowSelected() {
        return chcbShowReferenceVariablesInCompleteWindow.isSelected();
    }


    /**
     * Getr, zda li je označena komponenta pro nastavení, že se mají do okna s dostupnými příkazy přidat i příkazy pro
     * naplnění / deklaraci proměnných v editoru příkazů tak, že se naplní hodnotou stejného datového typu z proměnné v
     * nějaké instanci třídy.
     *
     * @return true v případě, že je komponenta označena, jinak false.
     */
    public boolean isChcbAddSyntaxForFillingTheVariableSelected() {
        return chcbAddSyntaxForFillingTheVariable.isSelected();
    }


    /**
     * Getr, zda li je označena komponenta pro nastavení toho, zda se mají pro připravené syntaxe pro deklaraci
     * proměnných v editoru příkazů (v okně s dostupnými příkazy) deklarovat proměnné s "final".
     *
     * @return true v případě, že je komponenta označena, jinak false.
     */
    public boolean isChcbAddFinalWordForFillingTheVariableSelected() {
        return chcbAddFinalWordForFillingTheVariable.isSelected();
    }


    /**
     * Getr, zda li je označena komponenta pro nastavení toho, zda se mají do okna s dostupnými příkazy pro editor
     * příkazů přidat vybrané ukázky příkazů podporované v editoru příkazů. Například deklarace proměnných apod.
     *
     * @return true v případě, že je komponenta označena, jinak false.
     */
    public boolean isChcbShowExamplesOfSyntaxSelected() {
        return chcbShowExamplesOfSyntax.isSelected();
    }


    /**
     * Getr, zda li je označena komponenta pro nastavení toho, zda se mají v okně s dostupnými příkazy pro editor
     * příkazů zobrazovat metody specifické pro editor příkazů. Například výpis proměnných apod.
     *
     * @return true v případě, že je komponenta označena, jinak false.
     */
    public boolean isChcbShowDefaultMethodsSelected() {
        return chcbShowDefaultMethods.isSelected();
    }
}