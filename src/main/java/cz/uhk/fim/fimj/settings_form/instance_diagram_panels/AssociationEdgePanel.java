package cz.uhk.fim.fimj.settings_form.instance_diagram_panels;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import org.jgraph.graph.GraphConstants;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.language.EditProperties;
import cz.uhk.fim.fimj.settings_form.ReadWriteDiagramProperties;
import cz.uhk.fim.fimj.settings_form.class_diagram_panels.EdgePanelAbstract;

/**
 * Třída slouží jako panel pro nastavení některých parametrů pro hranu, která v diagramu incsatní reprezentuje hranu
 * typu asociace 1 : 1.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class AssociationEdgePanel extends EdgePanelAbstract
		implements ActionListener, LanguageInterface, ReadWriteDiagramProperties {

	private static final long serialVersionUID = 1L;


	/**
	 * Konstruktor této třídy.
	 */
	public AssociationEdgePanel() {
		super();
		
		setLayout(new GridBagLayout());
		
		final GridBagConstraints gbc = getGbc();
		
		index = 0;
		
		
		gbc.gridwidth = 3;
		
		chcbLabelAlongEdge = new JCheckBox();
		setGbc(gbc, index, 0, chcbLabelAlongEdge, this);
		
		
		chcbLineBeginFill = new JCheckBox();		
		setGbc(gbc, ++index, 0, chcbLineBeginFill, this);
		
		
		chcbLineEndFill = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbLineEndFill, this);
				
		
		gbc.gridwidth = 1;
		
		
		lblLineColor = new JLabel();
		setGbc(gbc, ++index, 0, lblLineColor, this);
		
		btnChooseLineColor = new JButton();
		btnChooseLineColor.addActionListener(this);
		setGbc(gbc, index, 1, btnChooseLineColor, this);
		
		lblChoosedLineColor = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChoosedLineColor.setOpaque(true);
		setGbc(gbc, index, 2, lblChoosedLineColor, this);
		
		
		
		
		lblTextColor = new JLabel();
		setGbc(gbc, ++index, 0, lblTextColor, this);
		
		btnChooseTextColor = new JButton();
		btnChooseTextColor.addActionListener(this);		
		setGbc(gbc, index, 1, btnChooseTextColor, this);
		
		lblChoosedTextColor = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChoosedTextColor.setOpaque(true);
		setGbc(gbc, index, 2, lblChoosedTextColor, this);





		lblFontSize = new JLabel();
		setGbc(gbc, ++index, 0, lblFontSize, this);
		
		cmbFontSize = new JComboBox<>(getArrayFontSizes());
		setGbc(gbc, index, 1, cmbFontSize, this);
		
		
		
		
		lblFontStyle = new JLabel();
		setGbc(gbc, ++index, 0, lblFontStyle, this);
		
		cmbFontStyle = new JComboBox<>(ARRAY_OF_FONT_STYLE.toArray(new String[] {}));
		setGbc(gbc, index, 1, cmbFontStyle, this);

		
		
		
		
		lblLineBeginStyle = new JLabel();
		setGbc(gbc, ++index, 0, lblLineBeginStyle, this);
		
		cmbLineStart = new JComboBox<>(ARRAY_OF_EDGE_LINE_END_START.toArray(new String[] {}));
		setGbc(gbc, index, 1, cmbLineStart, this);
		
		
		
		
		lblLineEndStyle = new JLabel();
		setGbc(gbc, ++index, 0, lblLineEndStyle, this);
		
		cmbLineEnd = new JComboBox<>(ARRAY_OF_EDGE_LINE_END_START.toArray(new String[] {}));
		setGbc(gbc, index, 1, cmbLineEnd, this);
		
		
		
		lblLineWidth = new JLabel();
		setGbc(gbc, ++index, 0, lblLineWidth, this);
		
		cmbLineWidth = new JComboBox<>(getWidthsOfLine());
		setGbc(gbc, index, 1, cmbLineWidth, this);
	}
	
	
	

	
	
	
	@Override
	public void readDataFromDiagramProp(final Properties instanceDiagramPro) {
		chcbLabelAlongEdge.setSelected(Boolean.parseBoolean(instanceDiagramPro.getProperty("AssociationEdgeLabelsAlongEdge", String.valueOf(Constants.ID_ASSOCIATION_LABELS_ALONG_EDGE))));
		
		chcbLineBeginFill.setSelected(Boolean.parseBoolean(instanceDiagramPro.getProperty("AssociationEdgeLineBeginFill", String.valueOf(Constants.ID_ASSOCIATION_EDGE_LINE_BEGIN_FILL))));
		
		chcbLineEndFill.setSelected(Boolean.parseBoolean(instanceDiagramPro.getProperty("AssociationEdgeLineEndFill", String.valueOf(Constants.ID_ASSOCIATION_EDGE_LINE_END_FILL))));
		
		
		final String colorLine = instanceDiagramPro.getProperty("AssociationEdgeLineColor", Integer.toString(Constants.ID_ASSOCIATION_EDGE_COLOR.getRGB()));
		lblChoosedLineColor.setBackground(new Color(Integer.parseInt(colorLine)));
		lblChoosedLineColor.setForeground(new Color(Integer.parseInt(colorLine)));
		clrLineColor = new Color(Integer.parseInt(colorLine));
		
		
		/*
		 * Konec a začátek hrany:
		 * none: 0
		 * Classic: 1
		 * Technical: 2
		 * Simple: 4
		 * circle: 5
		 * Line: 7
		 * Doubleline: 8
		 * Diamond: 9
		 */
		final int lineBegin = Integer.parseInt(instanceDiagramPro.getProperty("AssociationEdgeLineBegin", String.valueOf(Constants.ID_ASSOCIATION_EDGE_LINE_BEGIN)));
		
		if (lineBegin == 0)
			cmbLineStart.setSelectedIndex(5);
		else if (lineBegin == 1)
			cmbLineStart.setSelectedIndex(1);
		else if (lineBegin == 2)
			cmbLineStart.setSelectedIndex(7);
		else if (lineBegin == 4)
			cmbLineStart.setSelectedIndex(6);
		else if (lineBegin == 5)
			cmbLineStart.setSelectedIndex(0);
		else if (lineBegin == 7)
			cmbLineStart.setSelectedIndex(4);
		else if (lineBegin == 8)
			cmbLineStart.setSelectedIndex(3);
		else if (lineBegin == 9)
			cmbLineStart.setSelectedIndex(2);
		
		
		
		final int lineEnd = Integer.parseInt(instanceDiagramPro.getProperty("AssociationEdgeLineEnd", String.valueOf(Constants.ID_ASSOCIATION_EDGE_LINE_END)));
		
		if (lineEnd == 0)
			cmbLineEnd.setSelectedIndex(5);
		else if (lineEnd == 1)
			cmbLineEnd.setSelectedIndex(1);
		else if (lineEnd == 2)
			cmbLineEnd.setSelectedIndex(7);
		else if (lineEnd == 4)
			cmbLineEnd.setSelectedIndex(6);
		else if (lineEnd == 5)
			cmbLineEnd.setSelectedIndex(0);
		else if (lineEnd == 7)
			cmbLineEnd.setSelectedIndex(4);
		else if (lineEnd == 8)
			cmbLineEnd.setSelectedIndex(3);
		else if (lineEnd == 9)
			cmbLineEnd.setSelectedIndex(2);
		

		
		final float lineWidth = Float.parseFloat(instanceDiagramPro.getProperty("AssociationEdgeLineWidth", String.valueOf(Constants.ID_ASSOCIATION_EDGE_LINE_WIDTH)));
		cmbLineWidth.setSelectedItem(lineWidth);	
		
		
		
		// Barva písma na hrane:
		final String textColor = instanceDiagramPro.getProperty("AssociationEdgeLineTextColor", Integer.toString(Constants.ID_ASSOCIATION_EDGE_FONT_COLOR.getRGB()));
		
		clrTextColor = new Color(Integer.parseInt(textColor));
		lblChoosedTextColor.setForeground(clrTextColor);
		lblChoosedTextColor.setBackground(clrTextColor);
		
		
		
		// styl písma na hrane:
		final int fontStyle = Integer.parseInt(instanceDiagramPro.getProperty("AssociationEdgeLineFontStyle", Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getStyle())));
		
		if (fontStyle == 0)		// Plain
			cmbFontStyle.setSelectedIndex(2);
		else if (fontStyle == 1)	// Bold
			cmbFontStyle.setSelectedIndex(0);
		else if (fontStyle == 2)	// Italic
			cmbFontStyle.setSelectedIndex(1);
		
		
		
		
		// velikost písma na hrane
		cmbFontSize.setSelectedItem(Integer.parseInt(instanceDiagramPro.getProperty("AssociationEdgeLineFontSize", Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getSize()))));
	}


	
	
	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			setBorder(BorderFactory.createTitledBorder(properties.getProperty("Sf_Id_AssociationEdgePanelBorderTitle", Constants.SF_ID_ASSOCIATION_EDGE_PANEL_BORDER_TITLE)));
		
			txtTextBorderTitleToJop = properties.getProperty("Sf_Id_AssociationEdgePanelBorderTitle", Constants.SF_ID_ASSOCIATION_EDGE_PANEL_BORDER_TITLE);
			
			chcbLabelAlongEdge.setText(properties.getProperty("Sf_Cd_CheckBoxLabelAlongEdge", Constants.SF_CD_CHCB_EDGE_LABEL_ALONG_EDGE));
			chcbLineBeginFill.setText(properties.getProperty("Sf_Cd_CheckBoxLineBeginFill", Constants.SF_CD_CHCB_LINE_BEGIN_FILL));
			chcbLineEndFill.setText(properties.getProperty("Sf_Cd_CheckBoxLineEndFill", Constants.SF_CD_CHCB_LINE_EDN_FILL));
			
			
			lblLineColor.setText(properties.getProperty("Sf_Cd_LineColor", Constants.SF_CD_LBL_EDGE_COLOR) + ": ");
			lblLineEndStyle.setText(properties.getProperty("Sf_Cd_LineEnd", Constants.SF_CD_LBL_LINE_END) + ": ");
			lblLineBeginStyle.setText(properties.getProperty("Sf_Cd_LineBegin", Constants.SF_CD_LBL_LINE_BEGIN) + ": ");
			lblLineWidth.setText(properties.getProperty("Sf_Cd_LineWidth", Constants.SF_CD_LBL_LINE_WIDTH) + ": ");
			lblTextColor.setText(properties.getProperty("Sf_Cd_LabelTextColorClassCell", Constants.SF_LBL_TEXT_COLOR_CELL) + ": ");
			lblFontStyle.setText(properties.getProperty("Sf_Cd_LabelFontStyleClassCell", Constants.SF_LBL_FONT_STYLE_CELL) + ": ");
			lblFontSize.setText(properties.getProperty("Sf_Cd_LabelFontSizeClassCell", Constants.SF_LBL_FONT_SIZE_CELL) + ": ");
			
			btnChooseLineColor.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));
			btnChooseTextColor.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));
			
			txtLineColorChooser = properties.getProperty("Sf_Cd_ChooseEdgeLineColor", Constants.SF_TXT_CHOOSE_LINE_COLOR);
			txtTextColorChooser = properties.getProperty("Sf_Cd_ChooseTextColorDialog", Constants.SF_TXT_CHOOSE_TEXT_COLOR);
		}
		
		
		else {
			setBorder(BorderFactory.createTitledBorder(Constants.SF_ID_ASSOCIATION_EDGE_PANEL_BORDER_TITLE));
		
			txtTextBorderTitleToJop = Constants.SF_ID_ASSOCIATION_EDGE_PANEL_BORDER_TITLE;
			
			chcbLabelAlongEdge.setText(Constants.SF_CD_CHCB_EDGE_LABEL_ALONG_EDGE);
			chcbLineBeginFill.setText(Constants.SF_CD_CHCB_LINE_BEGIN_FILL);
			chcbLineEndFill.setText(Constants.SF_CD_CHCB_LINE_EDN_FILL);
			
			
			lblLineColor.setText(Constants.SF_CD_LBL_EDGE_COLOR + ": ");
			lblLineEndStyle.setText(Constants.SF_CD_LBL_LINE_END + ": ");
			lblLineBeginStyle.setText(Constants.SF_CD_LBL_LINE_BEGIN + ": ");
			lblLineWidth.setText(Constants.SF_CD_LBL_LINE_WIDTH + ": ");
			lblTextColor.setText(Constants.SF_LBL_TEXT_COLOR_CELL + ": ");
			lblFontStyle.setText(Constants.SF_LBL_FONT_STYLE_CELL + ": ");
			lblFontSize.setText(Constants.SF_LBL_FONT_SIZE_CELL + ": ");
			
			btnChooseLineColor.setText(Constants.SF_CD_BTN_CHOOSE_COLOR);
			btnChooseTextColor.setText(Constants.SF_CD_BTN_CHOOSE_COLOR);
			
			txtLineColorChooser = Constants.SF_TXT_CHOOSE_LINE_COLOR;
			txtTextColorChooser = Constants.SF_TXT_CHOOSE_TEXT_COLOR;
		}
	}





	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnChooseLineColor) {
				// Nejprve si zvolenou barvu uložím do dočasné proměnné - pouze v metodě, prtože 
				// když uživatel dialog zruši barby by byla null a následně by se uložila do souboru, což je chyba
				final Color tempColor = JColorChooser.showDialog(this, txtLineColorChooser,
						Constants.ID_ASSOCIATION_EDGE_COLOR); 
				
				if (tempColor != null) {
					clrLineColor = tempColor;
					lblChoosedLineColor.setForeground(clrLineColor);
					lblChoosedLineColor.setBackground(clrLineColor);	
				}
			}
			
			
			else if (e.getSource() == btnChooseTextColor) {
				final Color tempColor = JColorChooser.showDialog(this, txtTextColorChooser,
						Constants.ID_ASSOCIATION_EDGE_FONT_COLOR);
			
				if (tempColor != null) {
					clrTextColor = tempColor;
					lblChoosedTextColor.setForeground(clrTextColor);
					lblChoosedTextColor.setBackground(clrTextColor);	
				}
			}
		}
	}
	
	
	
	

	
	
	@Override
	public final EditProperties writeDataToDiagramProp(final EditProperties instanceDiagramProp) {
		instanceDiagramProp.setProperty("AssociationEdgeLabelsAlongEdge", String.valueOf(chcbLabelAlongEdge.isSelected()));
		
		instanceDiagramProp.setProperty("AssociationEdgeLineBeginFill", String.valueOf(chcbLineBeginFill.isSelected()));
		
		instanceDiagramProp.setProperty("AssociationEdgeLineEndFill", String.valueOf(chcbLineEndFill.isSelected()));
		
		if (clrLineColor != null)
			instanceDiagramProp.setProperty("AssociationEdgeLineColor", Integer.toString(clrLineColor.getRGB()));
		
		
		
		
		// Typ začátku hrany:
		final int lineBegin = cmbLineStart.getSelectedIndex();
		
		if (lineBegin == 0)
			instanceDiagramProp.setProperty("AssociationEdgeLineBegin", Integer.toString(GraphConstants.ARROW_CIRCLE));
		
		if (lineBegin == 1)
			instanceDiagramProp.setProperty("AssociationEdgeLineBegin", Integer.toString(GraphConstants.ARROW_CLASSIC));
		
		if (lineBegin == 2)
			instanceDiagramProp.setProperty("AssociationEdgeLineBegin", Integer.toString(GraphConstants.ARROW_DIAMOND));
		
		if (lineBegin == 3)
			instanceDiagramProp.setProperty("AssociationEdgeLineBegin", Integer.toString(GraphConstants.ARROW_DOUBLELINE));
		
		if (lineBegin == 4)
			instanceDiagramProp.setProperty("AssociationEdgeLineBegin", Integer.toString(GraphConstants.ARROW_LINE));
		
		if (lineBegin == 5)
			instanceDiagramProp.setProperty("AssociationEdgeLineBegin", Integer.toString(GraphConstants.ARROW_NONE));
		
		if (lineBegin == 6)
			instanceDiagramProp.setProperty("AssociationEdgeLineBegin", Integer.toString(GraphConstants.ARROW_SIMPLE));
		
		if (lineBegin == 7)
			instanceDiagramProp.setProperty("AssociationEdgeLineBegin", Integer.toString(GraphConstants.ARROW_TECHNICAL));
		
		
		// Typ konce hrany:
		final int lineEnd = cmbLineEnd.getSelectedIndex();
		
		if (lineEnd == 0)
			instanceDiagramProp.setProperty("AssociationEdgeLineEnd", Integer.toString(GraphConstants.ARROW_CIRCLE));
		
		if (lineEnd == 1)
			instanceDiagramProp.setProperty("AssociationEdgeLineEnd", Integer.toString(GraphConstants.ARROW_CLASSIC));
		
		if (lineEnd == 2)
			instanceDiagramProp.setProperty("AssociationEdgeLineEnd", Integer.toString(GraphConstants.ARROW_DIAMOND));
		
		if (lineEnd == 3)
			instanceDiagramProp.setProperty("AssociationEdgeLineEnd", Integer.toString(GraphConstants.ARROW_DOUBLELINE));
		
		if (lineEnd == 4)
			instanceDiagramProp.setProperty("AssociationEdgeLineEnd", Integer.toString(GraphConstants.ARROW_LINE));
		
		if (lineEnd == 5)
			instanceDiagramProp.setProperty("AssociationEdgeLineEnd", Integer.toString(GraphConstants.ARROW_NONE));
		
		if (lineEnd == 6)
			instanceDiagramProp.setProperty("AssociationEdgeLineEnd", Integer.toString(GraphConstants.ARROW_SIMPLE));
		
		if (lineEnd == 7)
			instanceDiagramProp.setProperty("AssociationEdgeLineEnd", Integer.toString(GraphConstants.ARROW_TECHNICAL));
		
		
		// Šířka hrany:
		final float lineWidth = (float) cmbLineWidth.getSelectedItem();
		instanceDiagramProp.setProperty("AssociationEdgeLineWidth", String.valueOf(lineWidth));
		
		
		final int fontStyle = cmbFontStyle.getSelectedIndex();
		
		if (fontStyle == 0)
			instanceDiagramProp.setProperty("AssociationEdgeLineFontStyle", Integer.toString(1));
		
		else if (fontStyle == 1)
			instanceDiagramProp.setProperty("AssociationEdgeLineFontStyle", Integer.toString(2));
		
		else if (fontStyle == 2)
			instanceDiagramProp.setProperty("AssociationEdgeLineFontStyle", Integer.toString(0));
		
		
		
		
		instanceDiagramProp.setProperty("AssociationEdgeLineFontSize", Integer.toString((Integer) cmbFontSize.getSelectedItem()));
		
		

		// Barva textu:
		if (clrTextColor != null)
			instanceDiagramProp.setProperty("AssociationEdgeLineTextColor", Integer.toString(clrTextColor.getRGB()));
		
		
		return instanceDiagramProp;
	}
}