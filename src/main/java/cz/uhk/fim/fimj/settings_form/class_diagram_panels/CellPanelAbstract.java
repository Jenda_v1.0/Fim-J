package cz.uhk.fim.fimj.settings_form.class_diagram_panels;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.settings_form.PanelAbstract;

/**
 * Třída slouží k tomu, abych si zde deklaroval nějaké proměnně, která dále použiji v panelu pro nastavení parametrů pro
 * design buňky v diagram tříd reprezentující komentář a třídu
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class CellPanelAbstract extends PanelAbstract {

	private static final long serialVersionUID = 1L;
	
	
	// Pole - konstanty pro  naplnění některých hodnot v komponentě JComboBox:
	protected static final List<String> ARRAY_ALIGNMENT_X = Collections
			.unmodifiableList(Arrays.asList("Left", "Center", "Right"));

	protected static final List<String> ARRAY_ALIGNMENT_Y = Collections
			.unmodifiableList(Arrays.asList("Top", "Center", "Bottom"));
	
	
	/**
	 * Promměnná coby model do JComboboxu, kde lze zvolit font, resp. styl písma pro
	 * objekty v diagramu tříd a nebo instancí
	 */
	protected static final List<String> FONT_STYLES = Collections
			.unmodifiableList(Arrays.asList("Bold", "Italic", "Plain"));
	
	
	// Pro komponentu pro výběr barvy:
	protected String txtChooseTextColor, txtBgColor;
	
	
	protected JCheckBox chcbCellOpaque, chcbShowPackages;
	
	
	// Labely pro posis - která vlastnost se nastavuje:
	protected JLabel lblFontSize, lblFontStyle, lblTextAlignmentX, lblTextAlignmentY, lblTextColor, lblBgColorLeft,
			lblBgColorRight, lblBgColor;
	
	
	// Labley, reprezentující zvolenou barvu:
	protected JLabel lblChooseColorText, lblChooseColorBgRight, lblChooseBgColorLeft, lblChoosedColor;
	protected Color clrSelectedTextColor, clrSelectedColorRight, clrSelectedColorLeft, clrSelectedBgColor;
	
	
	
	// tlačítka pro nastavení některých vlastností:
	protected JButton btnChooseColor, btnChooseBgColorLeft, btnChooseBgColorRight, btnBgColor;
	
	
	
	protected JComboBox<Integer> cmbFontSize;
	protected JComboBox<String> cmbFontStyle, cmbTextAlignmentX, cmbTextAlignmentY;
	
	
	
	protected GridBagConstraints gbc;
	
	
	
	
	
	
	
	/**
	 * RadioButton rbOneColor slouží pro to, že bude definovaná pro buňku
	 * reprezentující třídu nebo komentář použita pouze jedna barva pro celou buňkou
	 * (Jenda barva pro oba rohy), dále rbTwoColor slouží pro to, že si uživatel
	 * může vybrat celkem dvě barvy pro buňkou reprezentující třídu a nebo komentář,
	 * jedna barva půjde o levého horního rohu a druhá od pravého dolního rohu a
	 * uprosřed buňky je přechod mezi zadanými barvami
	 */
	JRadioButton rbOneColor, rbTwoColor;
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí jednorozměrné pole s hodnotami 8 - 150 = rozhsa čísel -
	 * velikostí písma, kterých může buňka nabývat
	 * 
	 * @return jednorozměrné pole čísel typu Integer s hodnotami 8 . 150
	 */
	protected static Integer[] getArrayFontSizes() {
		final Integer[] arrayFontSizes = new Integer[143];
		
		for (int i = Constants.FONT_SIZE_MIN; i <= Constants.FONT_SIZE_MAX; i++)
			arrayFontSizes[i - 8] = i;
		
		return arrayFontSizes;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zakáže nebo anopak povolí komponenty pro výběr barvy pozadí pro
	 * buňku reprezentující třídu nebo komentář.
	 * 
	 * Metoda obshauje jeden logický parametr, který vždy komponenty pro výběr jedné
	 * barvy pzadí povolí a ty komponenty pro výběr dvou barev pro jednu buňku zase
	 * povolí a naopak
	 * 
	 * @param enable
	 *            - logický parametr, dle kterého se některé komponenty povolí a
	 *            jiné naopak zakážou, pokud bude parametr true, tak se povolí
	 *            komponenty pro výběr dvou barev pro jednu buňku coby třída nebo
	 *            komentář, a pokud bude parametr false, tak se zase povolí
	 *            komponenty pro výběr jedné barvy pro pozadí buňky coby třídy nebo
	 *            komentáře a zakážou se komponenty pro výběr jedné barvy pozadí
	 *            zmíněných buňěk
	 */
	final void enableColorItems(final boolean enable) {
		// Komponenty pro dvé barvu pro jednu buňku:
		lblBgColorLeft.setEnabled(enable);
		btnChooseBgColorLeft.setEnabled(enable);
		
		lblBgColorRight.setEnabled(enable);
		btnChooseBgColorRight.setEnabled(enable);
		// Labely s barvou nechám povolené, jinak by nebyla vidět barva, ale popisek Col
//		lblChooseColorBgRight.setEnabled(enable);
		
		
		// Komponenty pro jednu barvu pro celou buňku:
		lblBgColor.setEnabled(!enable);
		btnBgColor.setEnabled(!enable);
		// Labely s barvou nechám povolené, jinak by nebyla vidět barva, ale popisek Col
//		lblChoosedColor.setEnabled(!enable);
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která inicializuje komponenty pro výběr počtu barev pro pozadí buňky
	 */
	final void initializeRadioButtons() {
		// inicializuji si Radio buttony pro označení kolik barev se má
		// použít pro buňku
		rbTwoColor = new JRadioButton();
		rbOneColor = new JRadioButton();
		
		
		rbTwoColor.setSelected(true);
		
		// Přidám je do skupiny, díky které bude možné označit jen jeden 
		// z radioButtonů:
		final ButtonGroup bg = new ButtonGroup();
		bg.add(rbOneColor);
		bg.add(rbTwoColor);
	}
	
	
	
	
	
	
	
	
	


	
	// Přístupové metody - getry pro získání zvolené hodnoty pro otestování, zda jsou hodnoty různé nebo idencické
	
	public final boolean isChcbCellOpaqueSelected() {
		return chcbCellOpaque.isSelected();
	}

	public final int getRgbOfClrSelectedTextColor() {
		return clrSelectedTextColor.getRGB();
	}

	
	public final int getRgbOfClrSelectedColorRight() {
		return clrSelectedColorRight.getRGB();
	}


	public final int getRgbOfSelectedColorLeft() {
		return clrSelectedColorLeft.getRGB();
	}
	
	public final int getRgbOfSelectedBgColor() {
		// Barva pro pozdi bunky - jedna barva pro pozadi.
		return clrSelectedBgColor.getRGB();
	}

	public final int getCmbFontSizeSelectedItem() {
		return (int) cmbFontSize.getSelectedItem();
	}

	public final String getCmbFontStyleSelectedItem() {
		return (String) cmbFontStyle.getSelectedItem();
	}

	public final String getCmbTextAlignmentXSelectedItem() {
		return (String) cmbTextAlignmentX.getSelectedItem();
	}

	public final String getCmbTextAlignmentYSelectedItem() {
		return (String) cmbTextAlignmentY.getSelectedItem();
	}	
	
	
	// Pro nastavení hodnot:
	public final void setSelectedChcbOpaque(final boolean selected) {
		chcbCellOpaque.setSelected(selected);
	}
	
	public final void setFontSize(final int fontSize) {
		cmbFontSize.setSelectedItem(fontSize);
	}
	
	public final void setFontStyle(final int fontStyle) {
		if (fontStyle == 0)		// Plain
			cmbFontStyle.setSelectedIndex(2);
		else if (fontStyle == 1)	// Bold
			cmbFontStyle.setSelectedIndex(0);
		else if (fontStyle == 2)	// Italic
			cmbFontStyle.setSelectedIndex(1);
	}
	
	public final void setFontAlignmentX(final int horizonalAlignment) {
		if (horizonalAlignment == 2)
			cmbTextAlignmentX.setSelectedIndex(0);
		else if (horizonalAlignment == 0)
			cmbTextAlignmentX.setSelectedIndex(1);
		else if (horizonalAlignment == 4)
			cmbTextAlignmentX.setSelectedIndex(2);
	}
	
	public final void setFontAlignmentY(final int verticalAlignment) {
		if (verticalAlignment == 1)
			cmbTextAlignmentY.setSelectedIndex(0);
		else if (verticalAlignment == 0)
			cmbTextAlignmentY.setSelectedIndex(1);
		else if (verticalAlignment == 3)
			cmbTextAlignmentY.setSelectedIndex(2);
	}
	
	public final void setSelectedTextColor(final Color color) {
		clrSelectedTextColor = color;
		lblChooseColorText.setForeground(clrSelectedTextColor);
		lblChooseColorText.setBackground(clrSelectedTextColor);
	}
	
	public final void setBgColorLeft(final Color color) {
		clrSelectedColorLeft = color;
		lblChooseBgColorLeft.setBackground(clrSelectedColorLeft);
		lblChooseBgColorLeft.setForeground(clrSelectedColorLeft);
	}
	
	public final void setBgColorRight(final Color color) {
		clrSelectedColorRight = color;
		lblChooseColorBgRight.setBackground(clrSelectedColorRight);
		lblChooseColorBgRight.setForeground(clrSelectedColorRight);
	}
	
	public final void setBgColorForCell(final Color color) {
		clrSelectedBgColor = color;
		lblChoosedColor.setBackground(color);
		lblChoosedColor.setForeground(color);
	}
	
	
	public final void setSelectedChcbShowPackages(final boolean selected) {
		chcbShowPackages.setSelected(selected);
	}
	
	
	
	public final boolean isRbOneColorSelected() {
		return rbOneColor.isSelected();
	} 
	
	public final boolean isRbTwoColorSelected() {
		return rbTwoColor.isSelected();
	}
	
	
	/**
	 * Tato metoda slouží pro oznčení kompnent pro výběr jedné nebo dvou barev,
	 * pokud bude parametr oneColor true, tak se označí komponenty pro výběr jedné
	 * barvy, pokud bude false, tak se označeí komponenty pro výběr dvou barev
	 * 
	 * @param oneColor
	 *            - logická proměnné pro výběr komponent, které se mají označit
	 */
	public final void setSelectedComponentsForColor(final boolean oneColor) {
		if (oneColor) {
			rbOneColor.setSelected(true);
			enableColorItems(false);
		}
		
		else {
			rbTwoColor.setSelected(true);
			enableColorItems(true);
		}
	}
}