package cz.uhk.fim.fimj.settings_form;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.ConfigurationFiles;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.file.WriteToFile;
import cz.uhk.fim.fimj.language.EditProperties;
import cz.uhk.fim.fimj.settings_form.class_diagram_panels.Aggregation_1_ku_1_EdgePanel;
import cz.uhk.fim.fimj.settings_form.class_diagram_panels.Aggregation_1_ku_N_EdgePanel;
import cz.uhk.fim.fimj.settings_form.class_diagram_panels.AssociationEdgePanel;
import cz.uhk.fim.fimj.settings_form.class_diagram_panels.ClassCellPanel;
import cz.uhk.fim.fimj.settings_form.class_diagram_panels.ClassDiagramGraphPanel;
import cz.uhk.fim.fimj.settings_form.class_diagram_panels.CommentCellPanel;
import cz.uhk.fim.fimj.settings_form.class_diagram_panels.CommentEdgePanel;
import cz.uhk.fim.fimj.settings_form.class_diagram_panels.EdgePanelAbstract;
import cz.uhk.fim.fimj.settings_form.class_diagram_panels.ExtendsEdgePanel;
import cz.uhk.fim.fimj.settings_form.class_diagram_panels.ImplementsEdgePanel;

/** 
 * Tato třída slouží jako panel pro nastavení  některých vlastností ohldně
 * objektů, které lze vložit do tohoto diagramu:
 * 
 * Pro bunky - tridy:
 * Nepůjdu hýbat s textem na hranách
 * Barva pozadí grafu
 * font bunjky:
 * barva pisma
 * zarovnani pisma v bunkce
 * barva pozadi bunky - vlevo nahore
 * barva pozadi bunky - vprao dole
 * Pruhlednost
 * 
 * 
 * Pro bunky - komentare: (to same jako vyse:)
 * 	 * Nepůjdu hýbat s textem na hranách
 * Barva pozadí grafu
 * font bunjky:
 * barva pisma
 * zarovnani pisma v bunkce
 * barva pozadi bunky - vlevo nahore
 * barva pozadi bunky - vprao dole
 * Pruhlednost
 * 
 * 
 * 
 * pro hrany: (Asociace, dedicnost, implementace)
 * Barva hrany
 * popis podel cary nebo ne
 * styl hrany
 * konec hrany
 * zacatek hrany
 * sirka hrany
 * vypln na konci hrany
 * vypln na zacatku hrany
 * 
 * @author Jan Krunčík
 * @version 1.0
 */


public class ClassDiagramPanel extends PanelAbstract
		implements SaveSettingsInterface, LanguageInterface, ActionListener {

	private static final long serialVersionUID = 1L;


	
	/**
	 * Proměnná pro text aby se restartovala apikace:
	 */
	private final JLabel lblResetApp;
	
	
		
	/**
	 * Panel, do kterého umístím panely pro jednotlivé hrany:
	 */
	private final JPanel pnlEdges;
	
	
	
	
	// Jednotlivé panely do tohoto panelu - třídy:
	private final ClassDiagramGraphPanel classDiagramGraphPanel;
	private final ClassCellPanel classPanel;
	private final CommentCellPanel commentPanel;
	private final AssociationEdgePanel associationPanel;
	private final ExtendsEdgePanel extendsPanel;
	private final ImplementsEdgePanel implementsPanel;
	private final Aggregation_1_ku_1_EdgePanel aggregation_1_Panel;
	private final Aggregation_1_ku_N_EdgePanel aggregation_N_Panel;
	private final CommentEdgePanel commentEdgePanel;
	
	
	
	/**
	 * Tlačítko pro obnovení výchozího nastavení komponent classdigramu - vlastnosti hran a buňek:
	 */
	private final JButton btnRestoreDefaultSettings;
	
	
	
	// Proměnné pro texty do chybových hlášek:
	private static String txtClassAndCommentCellsAreIdenticalText, txtClassAndCommentCellsAreIdenticalTitle,
			txtEdgesAreIdenticalText, txtEdgesAreIdenticalTitle;
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 */
	public ClassDiagramPanel() {
		super();
		
		setLayout(new GridBagLayout());
		
		final GridBagConstraints gbc = getGbc();
		
		index = 0;
		
		final JPanel pnlResetApp = new JPanel(new FlowLayout(FlowLayout.CENTER));
		
		lblResetApp = new JLabel();
		pnlResetApp.add(lblResetApp);
		gbc.gridwidth = 2;
		setGbc(gbc, index, 0, pnlResetApp, this);
		
		
		
		final JPanel pnlGraphPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		classDiagramGraphPanel = new ClassDiagramGraphPanel();
		
		pnlGraphPanel.add(classDiagramGraphPanel);
		classDiagramGraphPanel.setMaximumSize(new Dimension(classDiagramGraphPanel.getMinimumSize().width + 100,
				classDiagramGraphPanel.getMinimumSize().height));
		
		setGbc(gbc, ++index, 0, pnlGraphPanel, this);
		
		
		
		
		
		gbc.gridwidth = 1;
		
		// Panel pro nastavení parametrů buňky reprezentující třídu v class diagramu
		final JPanel pnlClassPanelToFrame = new JPanel(new FlowLayout(FlowLayout.CENTER));
		classPanel = new ClassCellPanel();
		pnlClassPanelToFrame.add(classPanel);
		
		setGbc(gbc, ++index, 0, pnlClassPanelToFrame, this);
		
		
		
		
		// Panel pro nastavení parametrů buňky reprezentující komentář v grafu - class diagramu
		final JPanel pnlCommentPanelToFrame = new JPanel(new FlowLayout(FlowLayout.CENTER));
		commentPanel = new CommentCellPanel();
		pnlCommentPanelToFrame.add(commentPanel);
		setGbc(gbc, index, 1, pnlCommentPanelToFrame, this);
		
		
		
		
		// Panel pro hrany:
		pnlEdges = new JPanel(new GridBagLayout());
		final GridBagConstraints gbcEdges = getGbc();
		
		index = 0;
		
		
		// Panel pro nastavení parametru hrany, která v class diagramu reprezentuje asociace:
		final JPanel pnlAssociationPanelToFrame = new JPanel(new FlowLayout(FlowLayout.CENTER));
		associationPanel = new AssociationEdgePanel();		
		pnlAssociationPanelToFrame.add(associationPanel);
		setGbc(gbcEdges, index, 0, pnlAssociationPanelToFrame, pnlEdges);
		
		
		
		
		// Panel pro nastavení parametru hrany, která v class diagramu reprezentuje dědičnost:
		final JPanel pnlExtendsPanelToFrame = new JPanel(new FlowLayout(FlowLayout.CENTER));
		extendsPanel = new ExtendsEdgePanel();
		pnlExtendsPanelToFrame.add(extendsPanel);
		setGbc(gbcEdges, index, 1, pnlExtendsPanelToFrame, pnlEdges);
		
		
		
		
		// Panel pro nastavení parametru hrany, která v class diagramu reprezentuje agregaci typu 1 : 1
		final JPanel pnlAggregation_1_PanelToFrame = new JPanel(new FlowLayout(FlowLayout.CENTER));
		aggregation_1_Panel = new Aggregation_1_ku_1_EdgePanel();
		pnlAggregation_1_PanelToFrame.add(aggregation_1_Panel);
		
		setGbc(gbcEdges, ++index, 0, pnlAggregation_1_PanelToFrame, pnlEdges);
		
		
		
		
		// Panel pro nastavení parametru hrany, která v class diagramu reprezentuje agregaci typu 1 : N
		final JPanel pnlAggregation_N_PanelToFrame = new JPanel(new FlowLayout(FlowLayout.CENTER));
		aggregation_N_Panel = new Aggregation_1_ku_N_EdgePanel();
		pnlAggregation_N_PanelToFrame.add(aggregation_N_Panel);
		setGbc(gbcEdges, index, 1, pnlAggregation_N_PanelToFrame, pnlEdges);
		
		
		
		
		// Panel pro nastavení parämetrů hrany, která v Class diagramu reprezentuje hranu komentáře:
		final JPanel pnlCommentEdgePanelToFrame = new JPanel(new FlowLayout(FlowLayout.CENTER));
		commentEdgePanel = new CommentEdgePanel();
		pnlCommentEdgePanelToFrame.add(commentEdgePanel);
		setGbc(gbcEdges, ++index, 0, pnlCommentEdgePanelToFrame, pnlEdges);
		
		
		
		// Panel pro nastavení parametru hrany, která v class diagramu reprezentuje Implementaci rozhraní:
		final JPanel pnlImplementsPanelToFrame = new JPanel(new FlowLayout(FlowLayout.CENTER));
		implementsPanel = new ImplementsEdgePanel();
		pnlImplementsPanelToFrame.add(implementsPanel);
		setGbc(gbcEdges, index, 1, pnlImplementsPanelToFrame, pnlEdges);
		
		
				
		
		gbc.gridwidth = 2;
		
		setGbc(gbc, ++index, 0, pnlEdges, this);
		
		
		
		final JPanel pnlButtonDefaultSettings = new JPanel(new FlowLayout(FlowLayout.CENTER));
		
		btnRestoreDefaultSettings = new JButton();
		btnRestoreDefaultSettings.addActionListener(this);
		pnlButtonDefaultSettings.add(btnRestoreDefaultSettings);
		
		setGbc(gbc, ++index, 0, pnlButtonDefaultSettings, this);
			
		
		openSettings();
	}
	
	
	

	
	
	
	

	
	
	@Override
	public void setLanguage(final Properties languageProperties) {
		if (languageProperties != null) {
			// Ohraničení panelu hrany: - ve kterém jsou panely pro parametry hrany umístěny:
			pnlEdges.setBorder(BorderFactory.createTitledBorder(languageProperties.getProperty("Sf_Cd_EdgesPanel", Constants.SF_CD_EDGES_PNL_BORDER_TITLE)));
			
			lblResetApp.setText(languageProperties.getProperty("Sf_App_LabelResetApplication", Constants.SF_LBL_RESET_APP));			
			
			btnRestoreDefaultSettings.setText(languageProperties.getProperty("Sf_Cd_ButtonRestoreDefaultSettings", Constants.SF_BUTTONS_RESTORE_DEFAULT_SETTINGS));			
			
			txtClassAndCommentCellsAreIdenticalText = languageProperties.getProperty("Sf_Cd_TxtClassAndCommentCellsAreIdenticalText", Constants.SF_CD_TXT_CLASS_AND_COMMENT_CELLS_ARE_IDENTICAL_TEXT);
			txtClassAndCommentCellsAreIdenticalTitle = languageProperties.getProperty("Sf_Cd_TxtClassAndCommentCellsAreIdenticalTitle", Constants.SF_CD_TXT_CLASS_AND_COMMENT_CELLS_ARE_IDENTICAL_TITLE);
			txtEdgesAreIdenticalText = languageProperties.getProperty("Sf_Cd_TxtEdgesAreIdenticalText", Constants.SF_CD_TXT_EDGES_ARE_IDENTICAL_TEXT);
			txtEdgesAreIdenticalTitle = languageProperties.getProperty("Sf_Cd_TxtEdgesAreIdenticalTitle", Constants.SF_CD_TXT_EDGES_ARE_IDENTICAL_TITLE);
		}
		
		
		else {
			pnlEdges.setBorder(BorderFactory.createTitledBorder(Constants.SF_CD_EDGES_PNL_BORDER_TITLE));
			
			lblResetApp.setText(Constants.SF_LBL_RESET_APP);
			
			btnRestoreDefaultSettings.setText(Constants.SF_BUTTONS_RESTORE_DEFAULT_SETTINGS);
			
			txtClassAndCommentCellsAreIdenticalText = Constants.SF_CD_TXT_CLASS_AND_COMMENT_CELLS_ARE_IDENTICAL_TEXT;
			txtClassAndCommentCellsAreIdenticalTitle = Constants.SF_CD_TXT_CLASS_AND_COMMENT_CELLS_ARE_IDENTICAL_TITLE;
			txtEdgesAreIdenticalText = Constants.SF_CD_TXT_EDGES_ARE_IDENTICAL_TEXT;
			txtEdgesAreIdenticalTitle = Constants.SF_CD_TXT_EDGES_ARE_IDENTICAL_TITLE;
		}
		
		
		// Do ostatních panelů to nastavím vždy zda to bude null nebo ne, si už jednotlivé panely přeberou
		classDiagramGraphPanel.setLanguage(languageProperties);
		classPanel.setLanguage(languageProperties);
		commentPanel.setLanguage(languageProperties);
		associationPanel.setLanguage(languageProperties);
		extendsPanel.setLanguage(languageProperties);
		aggregation_1_Panel.setLanguage(languageProperties);
		aggregation_N_Panel.setLanguage(languageProperties);
		commentEdgePanel.setLanguage(languageProperties);
		implementsPanel.setLanguage(languageProperties);
	}





	@Override
	public void saveSetting() {
		// Zde si načtu soubour pro uložení změn pouze z adresáře Workspace, jinak nic
		// ten z aplikace je pouze pro výchozí nastavení:
		EditProperties classDiagramProperties = App.READ_FILE
				.getPropertiesInWorkspacePersistComments(Constants.CLASS_DIAGRAM_NAME);
		
		if (classDiagramProperties == null) {
			final String pathToWorkspace = ReadFile.getPathToWorkspace();

			/*
			 * Zde pouze zjisšťuji, zda existují veškeré adresáře k cestě pro soubor
			 * ClassDiagram.properties v adresáři configuration ve workspace. Pokud ne, pak v
			 * rámci možností nekteré adresáře mohu vytvořit, jinak není kam uložit soubory,
			 * pokud například byl smazan workspace apod. Pak už zbývá jedině znovu vytvořit
			 * adresář workspace s projekty apod. Ale tuto část zde vynechávám, protože o
			 * tom uživatel musí vědět - že si smazal projekty případně workspace apod. Tak
			 * snad musí vědět, že nelze pracovat s projekty / soubory, které byly smazány.
			 * 
			 * Otestuji, zda existuje workspace, pak configuration, pak příslušný soubor,
			 * případně některé z nich mohu vytvořit:
			 */
			if (pathToWorkspace != null) {
				acquireMutex();

				/*
				 * Pokud neexsistuje adresář configuration ve workspace, bude vytvořen i s
				 * potřebnými soubory s výchozím nastavením.
				 * 
				 * Pokud adresář configuration ve workspace existuje, pak stačí vytvořit jen
				 * soubor ClassDiagram.properties, protože vím, že neexistuje, když se jej
				 * nepodařilo načíst.
				 */
				if (ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
					ConfigurationFiles.writeClassDiagramProperties(
							pathToWorkspace + File.separator + Constants.PATH_TO_CLASS_DIAGRAM_PROPERTIES);

				else
					WriteToFile.createConfigureFilesInPath(pathToWorkspace);

				// Znovu načtu soubor Default.properties z workspace z configuration, protože
				// výše byl znovu vytvořn:
				classDiagramProperties = App.READ_FILE
						.getPropertiesInWorkspacePersistComments(Constants.CLASS_DIAGRAM_NAME);

				releaseMutex();
			}
			/*
			 * část else:
			 * 
			 * Zde by bylo možné vytvořit dříve zadaný adresář workspace - pokud byl již
			 * zadán, ale byl smazán, ale to by v rámci aplikace nemělo nastat, proto tuto
			 * část vynechám, uživatel jej musí smazat sám, pak o tom musí vědět.
			 */
		}
		
		
		
		
		
		// ZDe se soubor načetl, uložím data:
		if (classDiagramProperties != null) {
			// Zde se mi podařilo příslušný soubor načíst, tak ho mohu naplnit změněnými hodnotami:
						
			classDiagramProperties = classDiagramGraphPanel.writeDataToDiagramProp(classDiagramProperties);
			classDiagramProperties = classPanel.writeDataToDiagramProp(classDiagramProperties);
			classDiagramProperties = commentPanel.writeDataToDiagramProp(classDiagramProperties);
			classDiagramProperties = associationPanel.writeDataToDiagramProp(classDiagramProperties);
			classDiagramProperties = extendsPanel.writeDataToDiagramProp(classDiagramProperties);
			classDiagramProperties = implementsPanel.writeDataToDiagramProp(classDiagramProperties);
			classDiagramProperties = aggregation_1_Panel.writeDataToDiagramProp(classDiagramProperties);
			classDiagramProperties = commentEdgePanel.writeDataToDiagramProp(classDiagramProperties);
			classDiagramProperties = aggregation_N_Panel.writeDataToDiagramProp(classDiagramProperties);
			
			
			// Zapíšu zpět do souboru
			
			classDiagramProperties.save();
		}
	}





	@Override
	public void openSettings() {
		// pokudsím se načíst soubor s nastavením pro Classidiagram, pkud se to nepovede, vytvořím ho do confuguration v Workspace
		// Dále každému panelu (pokud se soubor načte) předám na něj referenci a každý panel si z něj vezme hodnoty vlastností potřebné pro 
		// daný anel:

		// Hledám pouze ve workspace, pokud neexistuje, tak se vytvoří nová složka
		// configuration nebo daný soubor ClassDiagram.properites

		Properties classDiagramProperties = App.READ_FILE.getClassDiagramFromWorkspaceOnly();
		
		if (classDiagramProperties == null) {
			final String pathToWorkspace = ReadFile.getPathToWorkspace();

			/*
			 * Zde pouze zjišťuji, zda existují veškeré adresáře k cestě pro soubor
			 * ClassDiagram.properties v adresáři configuration ve workspace. Pokud ne, pak
			 * v rámci možností nekteré adresáře mohu vytvořit, jinak není kam uložit
			 * soubory, pokud například byl smazan workspace apod. Pak už zbývá jedině znovu
			 * vytvořit adresář workspace s projekty apod. Ale tuto část zde vynechávám,
			 * protože o tom uživatel musí vědět - že si smazal projekty případně workspace
			 * apod. Tak snad musí vědět, že nelze pracovat s projekty / soubory, které byly
			 * smazány.
			 * 
			 * Otestuji, zda existuje workspace, pak configuration, pak příslušný soubor,
			 * případně některé z nich mohu vytvořit:
			 */
			if (pathToWorkspace != null) {
				acquireMutex();

				if (ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
					ConfigurationFiles.writeClassDiagramProperties(
							pathToWorkspace + File.separator + Constants.PATH_TO_CLASS_DIAGRAM_PROPERTIES);

				
				
				// Zde neexistuje configuration, tak ho nakopíruji:
				else
					WriteToFile.createConfigureFilesInPath(pathToWorkspace);

				
				// Zde se nakopírovatl bud configuration - adresář nebo jen daný
				// soubour ClassDIagram.properties ale Workspace existuje
				// tak si načtu potřbný soubor znovu:
				classDiagramProperties = App.READ_FILE.getClassDiagramFromWorkspaceOnly();

				releaseMutex();
			}
		}
		
		
		
		
		
		
		
		// Otestuji, zda se soubor opravdu načetl - zda existuje Workspace
		// případně načtu jeho hodnoty do dialogu
		
		if (classDiagramProperties != null) {		
			classDiagramGraphPanel.readDataFromDiagramProp(classDiagramProperties);
			classPanel.readDataFromDiagramProp(classDiagramProperties);
			commentPanel.readDataFromDiagramProp(classDiagramProperties);
			associationPanel.readDataFromDiagramProp(classDiagramProperties);
			extendsPanel.readDataFromDiagramProp(classDiagramProperties);
			implementsPanel.readDataFromDiagramProp(classDiagramProperties);
			aggregation_1_Panel.readDataFromDiagramProp(classDiagramProperties);
			commentEdgePanel.readDataFromDiagramProp(classDiagramProperties);
			aggregation_N_Panel.readDataFromDiagramProp(classDiagramProperties);
		}
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, podle toho, zda jsou označené stejné barvy pozadí pro 
	 * bunku reprezentující třídu a komentář.
	 * 
	 * 
	 * Barvy, které se nesmí rovnat, resp. testuji, zda se rovanjí:
	 * Pokud jsou označena pro obe bunky dve barvy:
	 * - otestuji, zda se barva pro levy horni roh a pravy spodni roh rovnaji, ve tride a pro komentar,
	 * 		pokud ano, tak se vrati true, coz je spatne, pokud se alespon jedna z barev nerovna, tak se 
	 * 		vrati false, coz je spravne - budou pro kazdou bunku jine barvy (alespon jedna barva bude jina - lze je rozezenat)
	 * 
	 * 
	 * Pokud jsou označeny pro obe bunky jedna barva:
	 * - zde akorat otestuji, zda se nerovna ta jedna barva pro bunku coby komentare a tridu, pokud ne, vrati se false
	 * 		a to je spravne, jinak se vrati true, coz je spatne - barvy bunek se shoduji
	 * 
	 * 
	 * Pokud se pro obe bunky lisi oznaceni - jedna bunka ma dve barvy a druha jednu:
	 * - Zde jsou moznosti ze bud bunka pro tridu ma jednu barvu a komentar dve nebo naopak
	 * - takze napriklad: jestli ma trida jednu barvu a komantar dve (popisu tuto moznost, ale plati to i naopak):
	 * - barva pro tridu se nesmi rovnat barve pro levy horni roh komentare NEBO barva pro tridu se nesmi rovnat pro pravy dilni roh komentare (a naopak)
	 * nebo pokud je označení naopak - jedna barva pro komentar a dve pro tridu:
	 * barva pro komentar se nesmi rovnat barve leveho horniho rohu pro tridu NEBO barva pro komentar se nesmi rovnat pravemu dolnimu rohu pro tridu
	 * 
	 * 
	 * @return true, pokud se označené bunky rovnaji, tj. je to spatne, protoze jsou stejne barvy pro buky
	 * , nebo false, pokud se alespon nektere z testovanych kombinaci nerovnaji, tj. je to sprvna - jsou označene ruzne barvy pro bunky
	 */
	private boolean checkSelectedColors() {
		// pro obe bunky jsou označeny dve barvy:
		if (classPanel.isRbTwoColorSelected() == commentPanel.isRbTwoColorSelected() && classPanel
				.isRbTwoColorSelected())
			// Pokud jsou obe stejne, tak se vrati true, pokud se alespon jedna barva listi, tak se vrati false -
			// spravne!
			// true je spatne - nemohou byt stejne barvy
			return classPanel.getRgbOfSelectedColorLeft() == commentPanel.getRgbOfSelectedColorLeft()
					&& classPanel.getRgbOfClrSelectedColorRight() == commentPanel.getRgbOfClrSelectedColorRight();
		
		
		
		// pro obe nuky je označena jedna barva
		else if (classPanel.isRbOneColorSelected() == commentPanel.isRbOneColorSelected() && classPanel
				.isRbOneColorSelected())
			return classPanel.getRgbOfSelectedBgColor() == commentPanel.getRgbOfSelectedBgColor();
		
		
		
		// obe nunky maji jiny pocet barev pro pozadi - bud pro tridu je jedna barva a pro komentar dve nebo naopak:
		else if (classPanel.isRbOneColorSelected() != commentPanel.isRbOneColorSelected()) {
			if (classPanel.isRbOneColorSelected())
				// Zde je označen pro tridu jedna barva, takže pro komentar musi byt oznaceny dve barvy:
				// podminky v popisu:
				// resp. otestuji, zda se barvy pro tridu shoduje s barvou pro komentar pro levy horni a dolni pravy
				// roh,
				// pokud se rovnaji, tak je to spatne, takze se vrati true, protoze jsou barvy stejne, jinak,
				// kdyz se alespon jedna barva listi, tak je to spravne a podminka neprojde a vrati se false
				return classPanel.getRgbOfSelectedBgColor() == commentPanel.getRgbOfSelectedColorLeft()
						&& classPanel.getRgbOfSelectedBgColor() == commentPanel.getRgbOfClrSelectedColorRight();

			else if (classPanel.isRbTwoColorSelected())
				// Zde jsou označeny dve barvy pro tridu, takze pro komentar musi byt jedna barva, tak obratim
				// podminky vyse:
				// Jestliže se barva pro komentar rovna barve pro tridu pro levy horni a pravy dolni roh, pak se vrati
				// true,
				// jako ze jsou barvy stejne a to je spatne, musi se alespon jedna z tech barev lisit, jinak budou
				// stejne bunky, coz je spatne
				return commentPanel.getRgbOfSelectedBgColor() == classPanel.getRgbOfSelectedColorLeft()
						&& commentPanel.getRgbOfSelectedBgColor() == classPanel.getRgbOfClrSelectedColorRight();
		}
		
		
		
		return false;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se hrany a buňky v tomto panelu navzájem liší Pokud
	 * by byly nějaké hrany stejné, tak bych je nemohl rozeznat, která co
	 * reprezentuje
	 * 
	 * Postup:
	 * Nejprve otestuji vlastnosti buňěk reprezentující třídu a komentář, pokud se
	 * neshodují tak budu pokračovat tak, že si vytvořím kolekci do které vložím
	 * všechny hrany, kroměn hrany, reprezentující implementaci - ta bude díky
	 * čerchované čáře vždy jiná, a pak tuto kolekci prohledám a otestuji, zda se
	 * všechny vlastnosti dané hrany shoduje s nějakou jinou, pokud ano, vrátím true
	 * - našly se dvě identické hrany, jinak pokračuji dále a plkud se jiná shoda
	 * nenajde, na konci vrátím false - žádná shoda
	 * 
	 * @return false, pokud žádné dva objeky - hrany nebo buňky nejsou stejné, jinak
	 *         když se libovoln= dvě hrany nebo buňky shodují, vrátí true
	 */
	final boolean areEdgesAndCellsIdentical() {
		// Nejprve porovnám buňky reprezentující třídu a komentář:
		if (classPanel.isChcbCellOpaqueSelected() == commentPanel.isChcbCellOpaqueSelected()
				&& classPanel.getCmbFontSizeSelectedItem() == commentPanel.getCmbFontSizeSelectedItem()
				&& classPanel.getCmbFontStyleSelectedItem().equals(commentPanel.getCmbFontStyleSelectedItem())
				&& classPanel.getCmbTextAlignmentXSelectedItem().equals(commentPanel.getCmbTextAlignmentXSelectedItem())
				&& classPanel.getCmbTextAlignmentYSelectedItem().equals(commentPanel.getCmbTextAlignmentYSelectedItem())
				&& classPanel.getRgbOfClrSelectedTextColor() == commentPanel.getRgbOfClrSelectedTextColor()
				&& checkSelectedColors()) {
			
			JOptionPane.showMessageDialog(null, txtClassAndCommentCellsAreIdenticalText,
					txtClassAndCommentCellsAreIdenticalTitle, JOptionPane.ERROR_MESSAGE);
			
			return true;
		}
			
		
		// Pokud se dostanu v kódu sem, tak buňky nejsou identické, nyní musím otestovat hrany - kromě hrany, která reprezentuje implementaci
		// rozhraní, ta je vždy čerchovaná, - vždy bude jiná, není třeba ji testovat - i když může mít stejné všechny ostatní vlastnosti bude vždy přerušovaná
		
		
		// Nyní budu postupovat dle výše sepsaného postupu - vytvořím kolekci, dám do ní panely s hranami a dvěma ckli ji prohledám,
		// a otestuji jejich vlastnosti - nastavené parametry:
		
		final List<EdgePanelAbstract> edgesPanelList = new ArrayList<>();
		
		edgesPanelList.add(associationPanel);
		edgesPanelList.add(extendsPanel);
		edgesPanelList.add(aggregation_1_Panel);
		edgesPanelList.add(aggregation_N_Panel);
		edgesPanelList.add(commentEdgePanel);
		
		// Nyní porovnám jejich vlastnosti:
		for (final EdgePanelAbstract p1 : edgesPanelList) {
			for (final EdgePanelAbstract p2 : edgesPanelList) {
				// Nesmím testovat jeden stejný panel, vždy bych selhal - podmínky by byla vždy splněna:
				if (p1 != p2) {	
					// Zde potřebuji ještě hlídat, že hrany agregace 1 : 1 a 1 : N mohou mít stejné parametry, tak se píše ještě multiplicita, se kterou uživatel 
					// nic neudělá takže budou jednoznačně k rozeznání, i když jejich parametry budou identické
					if (!((p1 == aggregation_1_Panel && p2 == aggregation_N_Panel)
							|| (p1 == aggregation_N_Panel && p2 == aggregation_1_Panel)))
						
						if (p1.isChcbTextAlongEdgeSelected() == p2.isChcbTextAlongEdgeSelected()
								&& p1.isChcbLineBeginFillSelected() == p2.isChcbLineBeginFillSelected()
								&& p1.isChcbLineEndFillSelected() == p2.isChcbLineEndFillSelected()
								&& p1.getRgbOfLineColor() == p2.getRgbOfLineColor()
								&& p1.getCmbLineStylSelectedItem().equals(p2.getCmbLineStylSelectedItem())
								&& p1.getCmbLineStartSelectedItem().equals(p2.getCmbLineStartSelectedItem())
								&& p1.getCmbLineEndSelectedItem().equals(p2.getCmbLineEndSelectedItem())
								&& p1.getCmbLineWidthSelectedItem() == p2.getCmbLineWidthSelectedItem()
								&& p1.getRgbOfTextColor() == p2.getRgbOfTextColor()
								&& p1.getFontSizeSelectedItem() == p2.getFontSizeSelectedItem()
								&& p1.getFontStyleSelectedItem().equals(p2.getFontStyleSelectedItem())) {
							
							JOptionPane.showMessageDialog(null,
									txtEdgesAreIdenticalText + "\n" + p1.getTxtTextBorderTitleToJop() + "\n"
											+ p2.getTxtTextBorderTitleToJop(),
									txtEdgesAreIdenticalTitle, JOptionPane.ERROR_MESSAGE);
							
							return true;
						}
				}
			}
		}		
		
		return false;
	}





	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton && e.getSource() == btnRestoreDefaultSettings) {
			// Nastavím komponenty v panelu na výchozí hodnoty:

			// Výchozí hodnoty pro základní nastavení grafu - class diagramu:
			classDiagramGraphPanel.setSelectedChcbConDisConLabel(Constants.CD_COM_LABELS_MOVABLE);
			classDiagramGraphPanel.setSelectedChcbShowRelationShipsToClassItself(Constants
					.CD_SHOW_RELATION_SHIPS_TO_CLASS_ITSELF);
			classDiagramGraphPanel.setSelectedChcbShowInheritedClassNameWithPackages(Constants
					.CD_SHOW_STATIC_INHERITED_CLASS_NAME_WITH_PACKAGES);
			classDiagramGraphPanel.setSelectedChcbIncludePrivateConstructors(Constants
					.CD_INCLUDE_PRIVATE_CONSTRUCTORS);
			classDiagramGraphPanel.setSelectedChcbIncludeProtectedConstructors(Constants
					.CD_INCLUDE_PROTECTED_CONSTRUCTORS);
			classDiagramGraphPanel.setSelectedChcbIncludePackagePrivateConstructors(Constants
					.CD_INCLUDE_PACKAGE_PRIVATE_CONSTRUCTORS);
			classDiagramGraphPanel.setSelectedChcbIncludePrivateMethods(Constants.CD_INCLUDE_PRIVATE_METHODS);
			classDiagramGraphPanel.setSelectedChcbIncludeProtectedMethods(Constants.CD_INCLUDE_PROTECTED_METHODS);
			classDiagramGraphPanel.setSelectedChcbIncludePackagePrivateMethods(Constants
					.CD_INCLUDE_PACKAGE_PRIVATE_METHODS);
			classDiagramGraphPanel.setSelectedChcbIncludePrivateInnerStaticClasses(Constants
					.CD_INCLUDE_PRIVATE_INNER_STATIC_CLASSES);
			classDiagramGraphPanel.setSelectedChcbIncludeProtectedInnerStaticClasses(Constants
					.CD_INCLUDE_PROTECTED_INNER_STATIC_CLASSES);
			classDiagramGraphPanel.setSelectedChcbIncludePackagePrivateInnerStaticClasses(Constants
					.CD_INCLUDE_PACKAGE_PRIVATE_INNER_STATIC_CLASSES);
			classDiagramGraphPanel.setSelectedChcbInnerStaticClassesIncludePrivateStaticMethods(Constants
					.CD_INNER_STATIC_CLASSES_INCLUDE_PRIVATE_STATIC_METHODS);
			classDiagramGraphPanel.setSelectedChcbInnerStaticClassesIncludeProtectedStaticMethods(Constants
					.CD_INNER_STATIC_CLASSES_INCLUDE_PROTECTED_STATIC_METHODS);
			classDiagramGraphPanel.setSelectedChcbInnerStaticClassesIncludePackagePrivateStaticMethods(Constants
					.CD_INNER_STATIC_CLASSES_INCLUDE_PACKAGE_PRIVATE_STATIC_METHODS);
			classDiagramGraphPanel.setSelectedChcbMakeAvailableFieldsForCallConstructor(Constants
					.CD_MAKE_FIELDS_AVAILABLE_FOR_CALL_CONSTRUCTOR);
			classDiagramGraphPanel.setSelectedChcbMakeAvailableMethodsForCallConstructor(Constants
					.CD_MAKE_METHODS_AVAILABLE_FOR_CALL_CONSTRUCTOR);
			classDiagramGraphPanel.setSelectedChcbMakeAvailableFieldsForCallMethod(Constants
					.CD_MAKE_FIELDS_AVAILABLE_FOR_CALL_METHOD);
			classDiagramGraphPanel.setSelectedChcbMakeAvailableMethodsForCallMethod(Constants
					.CD_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD);
			classDiagramGraphPanel.setSelectedChcbShowCreateNewInstanceOptionInCallConstructor(Constants
					.CD_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_CONSTRUCTOR);
			classDiagramGraphPanel.setSelectedChcbShowCreateNewInstanceOptionInCallMethod(Constants
					.CD_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD);
			classDiagramGraphPanel.setSelectedChcbShowAssociationThroughAggregation(Constants
					.CD_SHOW_ASSOCIATION_THROUGH_AGGREGATION);
			classDiagramGraphPanel.setSelectedChcbAddInterfaceMethodsToClass(Constants.ADD_INTERFACE_METHODS_TO_CLASS);
			classDiagramGraphPanel.setBgColor(Constants.CD_BACKGROUND_COLOR.getRGB());
			classDiagramGraphPanel.setScaleOfGraph(Constants.CLASS_DIAGRAM_SCALE);
			classDiagramGraphPanel.setDefaultCodeEditor(Constants.CD_KIND_OF_DEFAULT_CODE_EDITOR);


			// Výchozí nastavení pro buňku reprezentující třídu:
			classPanel.setSelectedChcbOpaque(Constants.CD_OPAQUE_CLASS_CELL);
			classPanel.setFontSize(Constants.CD_FONT_CLASS.getSize());
			classPanel.setFontStyle(Constants.CD_FONT_CLASS.getStyle());
			classPanel.setFontAlignmentX(Constants.CD_HORIZONTAL_ALIGNMENT);
			classPanel.setFontAlignmentY(Constants.CD_VERTICAL_ALIGNMENT);
			classPanel.setSelectedTextColor(Constants.CD_TEXT_COLOR);
			classPanel.setBgColorLeft(Constants.CD_CLASS_BACKGROUND_COLOR);
			classPanel.setBgColorRight(Constants.CD_GRADIENT_COLOR);
			classPanel.setBgColorForCell(Constants.CD_ONE_BG_COLOR);
			classPanel.setSelectedComponentsForColor(Constants.CD_ONE_BG_COLOR_BOOLEAN);


			// Výchozí nastavení pro buňku reprezentující komentář:
			commentPanel.setSelectedChcbOpaque(Constants.CD_OPAQUE_COMMENT_CELL);
			commentPanel.setFontSize(Constants.CD_FONT_COMMENT.getSize());
			commentPanel.setFontStyle(Constants.CD_FONT_COMMENT.getStyle());
			commentPanel.setFontAlignmentX(Constants.CD_COM_HORIZONTAL_ALIGNMENT);
			commentPanel.setFontAlignmentY(Constants.CD_COM_VERTICAL_ALIGNMENT);
			commentPanel.setSelectedTextColor(Constants.CD_COM_FOREGROUND_COLOR);
			commentPanel.setBgColorLeft(Constants.CD_COM_BACKGROUND_COLOR);
			commentPanel.setBgColorRight(Constants.CD_COM_GRADIENT_COLOR);
			commentPanel.setBgColorForCell(Constants.CD_COM_ONE_BG_COLOR);
			commentPanel.setSelectedComponentsForColor(Constants.CD_COM_ONE_BG_COLOR_BOOLEAN);


			// Výchozí nastavení pro hranu, reprezentující asociace:
			associationPanel.setChcbLableAlongEdge(Constants.CD_ASC_LABEL_ALONG_EDGE);
			associationPanel.setChcbLineBeginFillSelected(Constants.CD_ASC_EDGE_BEGIN_FILL);
			associationPanel.setChcbLIneEndFillSelected(Constants.CD_ASC_EDGE_END_FILL);
			associationPanel.setLineColor(Constants.CD_ASC_EDGE_COLOR);
			associationPanel.setLineStyle(Constants.CD_ASC_EDGE_LINE_STYLE);
			associationPanel.setLineBegin(Constants.CD_ASC_EDGE_LINE_BEGIN);
			associationPanel.setLineEnd(Constants.CD_ASC_EDGE_LINE_END);
			associationPanel.setLineWidth(Constants.CD_ASC_EDGE_LINE_WIDTH);
			associationPanel.setFontColor(Constants.CD_ASC_EDGE_FONT_COLOR);
			associationPanel.setFontSize(Constants.CD_ASC_EDGE_LINE_FONT.getSize());
			associationPanel.setFontStyle(Constants.CD_ASC_EDGE_LINE_FONT.getStyle());


			// Výchozí hodnoty pro hranu, reprezentující dědičnost:
			extendsPanel.setChcbLableAlongEdge(Constants.CD_EXT_LABEL_ALONG_EDGE);
			extendsPanel.setChcbLineBeginFillSelected(Constants.CD_EXT_EDGE_BEGIN_FILL);
			extendsPanel.setChcbLIneEndFillSelected(Constants.CD_EXT_EDGE_END_FILL);
			extendsPanel.setLineColor(Constants.CD_EXT_EDGE_COLOR);
			extendsPanel.setLineStyle(Constants.CD_EXT_EDGE_LINE_STYLE);
			extendsPanel.setLineBegin(Constants.CD_EXT_EDGE_LINE_BEGIN);
			extendsPanel.setLineEnd(Constants.CD_EXT_EDGE_LINE_END);
			extendsPanel.setLineWidth(Constants.CD_EXT_EDGE_LINE_WIDTH);
			extendsPanel.setFontColor(Constants.CD_EXT_EDGE_FONT_COLOR);
			extendsPanel.setFontSize(Constants.CD_EXT_EDGE_LINE_FONT.getSize());
			extendsPanel.setFontStyle(Constants.CD_EXT_EDGE_LINE_FONT.getStyle());


			// Výchozí hodnoty pro hranu, reprezentující agregaci 1 : 1:
			aggregation_1_Panel.setChcbLableAlongEdge(Constants.CD_AGR_LABEL_ALONG_EDGE);
			aggregation_1_Panel.setChcbLineBeginFillSelected(Constants.CD_AGR_EDGE_BEGIN_FILL);
			aggregation_1_Panel.setChcbLIneEndFillSelected(Constants.CD_AGR_EDGE_END_FILL);
			aggregation_1_Panel.setLineColor(Constants.CD_AGR_EDGE_COLOR);
			aggregation_1_Panel.setLineStyle(Constants.CD_AGR_EDGE_LINE_STYLE);
			aggregation_1_Panel.setLineBegin(Constants.CD_AGR_EDGE_LINE_BEGIN);
			aggregation_1_Panel.setLineEnd(Constants.CD_AGR_EDGE_LINE_END);
			aggregation_1_Panel.setLineWidth(Constants.CD_AGR_EDGE_LINE_WIDTH);
			aggregation_1_Panel.setFontColor(Constants.CD_AGR_EDGE_FONT_COLOR);
			aggregation_1_Panel.setFontSize(Constants.CD_AGR_EDGE_LINE_FONT.getSize());
			aggregation_1_Panel.setFontStyle(Constants.CD_AGR_EDGE_LINE_FONT.getStyle());


			// Výchozí hodnoty pro hranu, reprezentující agregaci 1 : N:
			aggregation_N_Panel.setChcbLableAlongEdge(Constants.CD_AGR_N_LABEL_ALONG_EDGE);
			aggregation_N_Panel.setChcbLineBeginFillSelected(Constants.CD_AGR_N_EDGE_BEGIN_FILL);
			aggregation_N_Panel.setChcbLIneEndFillSelected(Constants.CD_AGR_N_EDGE_END_FILL);
			aggregation_N_Panel.setLineColor(Constants.CD_AGR_N_EDGE_COLOR);
			aggregation_N_Panel.setLineStyle(Constants.CD_AGR_EDGE_N_LINE_STYLE);
			aggregation_N_Panel.setLineBegin(Constants.CD_AGR_N_EDGE_LINE_BEGIN);
			aggregation_N_Panel.setLineEnd(Constants.CD_AGR_N_EDGE_LINE_END);
			aggregation_N_Panel.setLineWidth(Constants.CD_AGR_EDGE_N_LINE_WIDTH);
			aggregation_N_Panel.setFontColor(Constants.CD_AGR_N_EDGE_FONT_COLOR);
			aggregation_N_Panel.setFontSize(Constants.CD_AGR_EDGE_N_LINE_FONT.getSize());
			aggregation_N_Panel.setFontStyle(Constants.CD_AGR_EDGE_N_LINE_FONT.getStyle());


			// Výchozí hodnoty pro hranu, reprezentující kometnář:
			commentEdgePanel.setChcbLableAlongEdge(Constants.CD_COM_LABEL_ALONG_EDGE);
			commentEdgePanel.setChcbLineBeginFillSelected(Constants.CD_COM_LINE_BEGIN_FILL);
			commentEdgePanel.setChcbLIneEndFillSelected(Constants.CD_COM_LINE_END_FILL);
			commentEdgePanel.setLineColor(Constants.CD_COM_EDGE_COLOR);
			commentEdgePanel.setLineStyle(Constants.CD_COM_EDGE_LINE_STYLE);
			commentEdgePanel.setLineBegin(Constants.CD_COM_LINE_BEGIN);
			commentEdgePanel.setLineEnd(Constants.CD_COM_LINE_END);
			commentEdgePanel.setLineWidth(Constants.CD_COM_LINE_WIDTH);
			commentEdgePanel.setFontColor(Constants.CD_COM_EDGE_FONT_COLOR);
			commentEdgePanel.setFontSize(Constants.CD_COM_EDGE_FONT.getSize());
			commentEdgePanel.setFontStyle(Constants.CD_COM_EDGE_FONT.getStyle());


			// Výchozí hodnoty pro hranu, reprezentující implementaci rozhraní:
			implementsPanel.setChcbLableAlongEdge(Constants.CD_IMP_LABEL_ALONG_EDGE);
			implementsPanel.setChcbLineBeginFillSelected(Constants.CD_IMP_EDGE_BEGIN_FILL);
			implementsPanel.setChcbLIneEndFillSelected(Constants.CD_IMP_EDGE_END_FILL);
			implementsPanel.setLineColor(Constants.CD_IMP_EDGE_COLOR);
			implementsPanel.setLineStyle(Constants.CD_IMP_EDGE_LINE_STYLE);
			implementsPanel.setLineBegin(Constants.CD_IMP_EDGE_LINE_BEGIN);
			implementsPanel.setLineEnd(Constants.CD_IMP_EDGE_LINE_END);
			implementsPanel.setLineWidth(Constants.CD_IMP_EDGE_LINE_WIDTH);
			implementsPanel.setPattern(Arrays.toString(Constants.CD_IMP_PATTERN));
			implementsPanel.setFontColor(Constants.CD_IMP_EDGE_FONT_COLOR);
			implementsPanel.setFontSize(Constants.CD_IMP_EDGE_LINE_FONT.getSize());
			implementsPanel.setFontStyle(Constants.CD_IMP_EDGE_LINE_FONT.getStyle());
		}
	}
}