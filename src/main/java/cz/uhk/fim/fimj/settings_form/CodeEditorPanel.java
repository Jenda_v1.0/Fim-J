package cz.uhk.fim.fimj.settings_form;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.ConfigurationFiles;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.file.WriteToFile;
import cz.uhk.fim.fimj.language.EditProperties;

/**
 * Tato třída slouží jako panel pro nastavení parametrů pro editor kódu, parametry jako jsou například font, barva
 * pozadí, zda se má zvýrazňovat syntaxe jazyka Java, apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class CodeEditorPanel extends EditorsPanelAbstract
		implements ActionListener, LanguageInterface, SaveSettingsInterface {
	
	private static final long serialVersionUID = 1L;


	
	// Obnovit výchozí nasgavení
	private final JButton btnRestoreDefaultSettings;
	
	
	
	/**
	 * Následující JcheckBox pro to, aby si uživatel mohl nastavit, zda se má pokud
	 * je nějaká třída otevřená v editoru zdrojového kódu, tak zda se má na pozadí
	 * daná třída zkompilovat nebo ne, pokud ano (je zaškrtnutý), tak se bude na
	 * pozadí kompilovat popmocí vlákna, které daný kod vzdy uloži a zkompiluje bud
	 * jen tu tridu nebo vsechny, pak tuto třidu nacte (soubor .class) a pokud se
	 * nacte v pohodě nastavi se do okna s doplnovanim kodu nově načtené proměnné,
	 * pokud se tento soubor nenačte, nic se neděje, vlakno se zavola po stisknutí
	 * stredniku nebo zaviracich zavorek na anglické klavesnici (zaviraci ] a } )
	 */
	private final JCheckBox chcbCompileInBackground;


    /**
     * Panel obsahující komponentami pro nastavení vlastností formátování zdrojového kódu.
     */
	private final FormattingPropertiesPanel formattingPropertiesPanel;
	
	
	
	/**
	 * Konstruktor třídy.
	 */
	CodeEditorPanel() {
		setLayout(new GridBagLayout());
		
		index = 0;
		
		final GridBagConstraints gbc = getGbc();
		
		
		
		
		
		
		// Kompnenty:

		// Komponenty do okna resp. panelu:
		gbc.gridwidth = 3;
		
		chcbHighlightingCode = new JCheckBox();
		setGbc(gbc, index, 0, chcbHighlightingCode, this);
		
		
		
		chcbHighlightCodeByKindOfFile = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbHighlightCodeByKindOfFile, this);
		
		
		
		
		chcbCompileInBackground = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbCompileInBackground, this);
		
		
		
		
		
		
		gbc.gridwidth = 1;
		
		lblBgColor = new JLabel();
		setGbc(gbc, ++index, 0, lblBgColor, this);
		
		btnChooseBgColor = new JButton();
		btnChooseBgColor.addActionListener(this);
		setGbc(gbc, index, 1, btnChooseBgColor, this);
		
		lblChoosedBgColor = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChoosedBgColor.setOpaque(true);
		setGbc(gbc, index, 2, lblChoosedBgColor, this);
		
		
		
		lblTextColor = new JLabel();
		setGbc(gbc, ++index, 0, lblTextColor, this);
		
		btnChooseTextColor = new JButton();
		btnChooseTextColor.addActionListener(this);
		setGbc(gbc, index, 1, btnChooseTextColor, this);
		
		lblChoosedTextColor = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChoosedTextColor.setOpaque(true);
		setGbc(gbc, index, 2, lblChoosedTextColor, this);
		
		
		
		
		
		lblFontSize = new JLabel();
		setGbc(gbc, ++index, 0, lblFontSize, this);
		
		cmbFontSize = new JComboBox<>(getFontSizes());
		setGbc(gbc, index, 1, cmbFontSize, this);
		
		
		
		
		lblFontStyle = new JLabel();
		setGbc(gbc, ++index, 0, lblFontStyle, this);
		
		cmbFontStyle = new JComboBox<>(ARRAY_OF_FONT_STYLE.toArray(new String[] {}));
		setGbc(gbc, index, 1, cmbFontStyle, this);



        gbc.gridwidth = 3;

        // Panel pro nastavení formátování zdrojového kódu:
        formattingPropertiesPanel = new FormattingPropertiesPanel();
        setGbc(gbc, ++index, 0, formattingPropertiesPanel, this);


		
		final JPanel pnlButtonDefaultSettings = new JPanel(new FlowLayout(FlowLayout.CENTER));
		
		btnRestoreDefaultSettings = new JButton();
		btnRestoreDefaultSettings.addActionListener(this);
		pnlButtonDefaultSettings.add(btnRestoreDefaultSettings);

		setGbc(gbc, ++index, 0, pnlButtonDefaultSettings, this);
		
		
		
		openSettings();
	}
	
	

	
	
	
	
	
	@Override
	public void setLanguage(final Properties properties) {
        formattingPropertiesPanel.setLanguage(properties);

		if (properties != null) {
			setBorder(BorderFactory.createTitledBorder(properties.getProperty("Sf_Cep_PanelBorderTitle", Constants.SF_CODE_EP_BORDER_TITLE)));
			
			chcbHighlightingCode.setText(properties.getProperty("Sf_Cep_CheckBoxJavaHighlightingSyntax", Constants.SF_CODE_EP_CHCB_HIGHLIGHTING_CODE));
			chcbHighlightingCode.setToolTipText(properties.getProperty("Sf_Cep_CheckBoxJavaHighlightingSyntax_TT", Constants.SF_CODE_EP_CHCB_HIGHLIGHTING_CODE_TT));
			
			chcbHighlightCodeByKindOfFile.setText(properties.getProperty("Sf_Cep_CheckBoxHighlightingSyntaxTextByKindOfFile", Constants.SF_CODE_EP_CHCB_HIGHLIGHTING_CODE_BY_KIND_OF_FILE));
			chcbHighlightCodeByKindOfFile.setToolTipText(properties.getProperty("Sf_Cep_CheckBoxHighlightingSyntaxTextByKindOfFile_TT", Constants.SF_CODE_EP_CHCB_HIGHLIGHTING_CODE_BY_KIND_OF_FILE_TT));
			
			chcbCompileInBackground.setText("? " + properties.getProperty("Sf_Cep_CheckBoxCompileClassInBackground", Constants.SF_CODE_EP_CHCB_COMPILE_CLASS_IN_BACKGROUND));
			chcbCompileInBackground.setToolTipText(properties.getProperty("Sf_Cep_CheckBoxCompileClassInBackground_TT", Constants.SF_CODE_EP_CHCB_COMPILE_CLASS_IN_BACKGROUND_TT));			
			
			
			lblBgColor.setText(properties.getProperty("Sf_Ce_LabelBackgroundColor", Constants.SF_CE_LBL_BACKGROUND_COLOR) + ": ");
			lblTextColor.setText(properties.getProperty("Sf_Cd_LabelTextColorClassCell", Constants.SF_LBL_TEXT_COLOR_CELL) + ": ");
			lblFontSize.setText(properties.getProperty("Sf_Cd_LabelFontSizeClassCell", Constants.SF_LBL_FONT_SIZE_CELL) + ": ");
			lblFontStyle.setText(properties.getProperty("Sf_Cd_LabelFontStyleClassCell", Constants.SF_LBL_FONT_STYLE_CELL) + ": ");
			
			
			txtChooseTextColorDialog = properties.getProperty("Sf_Cd_ChooseTextColorDialog", Constants.SF_TXT_CHOOSE_TEXT_COLOR);
			txtChooseBgColorDialog = properties.getProperty("Sf_Cd_ChooseBackgroundColorDialog", Constants.SF_TXT_CHOOSE_BG_COLOR);
			
			
			btnChooseBgColor.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));
			btnChooseTextColor.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));
			
			btnRestoreDefaultSettings.setText(properties.getProperty("Sf_Cd_ButtonRestoreDefaultSettings", Constants.SF_BUTTONS_RESTORE_DEFAULT_SETTINGS));
		}
		
		
		else {
			setBorder(BorderFactory.createTitledBorder(Constants.SF_CODE_EP_BORDER_TITLE));
			
			chcbHighlightingCode.setText(Constants.SF_CODE_EP_CHCB_HIGHLIGHTING_CODE);
			chcbHighlightingCode.setToolTipText( Constants.SF_CODE_EP_CHCB_HIGHLIGHTING_CODE_TT);
			
			chcbHighlightCodeByKindOfFile.setText(Constants.SF_CODE_EP_CHCB_HIGHLIGHTING_CODE_BY_KIND_OF_FILE);
			chcbHighlightCodeByKindOfFile.setToolTipText(Constants.SF_CODE_EP_CHCB_HIGHLIGHTING_CODE_BY_KIND_OF_FILE_TT);
			
			chcbCompileInBackground.setText("? " + Constants.SF_CODE_EP_CHCB_COMPILE_CLASS_IN_BACKGROUND);
			chcbCompileInBackground.setToolTipText(Constants.SF_CODE_EP_CHCB_COMPILE_CLASS_IN_BACKGROUND_TT);
			
			lblBgColor.setText(Constants.SF_CE_LBL_BACKGROUND_COLOR + ": ");
			lblTextColor.setText(Constants.SF_LBL_TEXT_COLOR_CELL + ": ");
			lblFontSize.setText(Constants.SF_LBL_FONT_SIZE_CELL + ": ");
			lblFontStyle.setText(Constants.SF_LBL_FONT_STYLE_CELL + ": ");
			
			
			txtChooseTextColorDialog =  Constants.SF_TXT_CHOOSE_TEXT_COLOR;
			txtChooseBgColorDialog = Constants.SF_TXT_CHOOSE_BG_COLOR;
			
			
			btnChooseBgColor.setText( Constants.SF_CD_BTN_CHOOSE_COLOR);
			btnChooseTextColor.setText(Constants.SF_CD_BTN_CHOOSE_COLOR);
			
			btnRestoreDefaultSettings.setText(Constants.SF_BUTTONS_RESTORE_DEFAULT_SETTINGS);
		}
		
		// Doplním znaky, které slouží pro kompilaci:
		chcbCompileInBackground.setText(chcbCompileInBackground.getText() + Constants.KEYS_FOR_COMPILING + ".");
		
		chcbHighlightingCode.setText("? " + chcbHighlightingCode.getText());
		chcbHighlightCodeByKindOfFile.setText("? " + chcbHighlightCodeByKindOfFile.getText());
	}
	
	
	
	

	
	@Override
	public void saveSetting() {
        /*
         *  Pokusím si načíst soubor ClassEditor.properties z Workspace, když ho nenačtu a adresář Workspace
         *  existuje, tak ho postupně zkusím vytvořit a vložit do něj změněné hodnoty.
         */
		EditProperties codeEditorProp = App.READ_FILE
				.getPropertiesInWorkspacePersistComments(Constants.CODE_EDITOR_NAME);

		if (codeEditorProp == null) {			
			final String pathToWorkspace = ReadFile.getPathToWorkspace();

			/*
			 * Zde pouze zjisšťuji, zda existují veškeré adresáře k cestě pro soubor
			 * CodeEditor.properties v adresáři configuration ve workspace. Pokud ne, pak v
			 * rámci možností nekteré adresáře mohu vytvořit, jinak není kam uložit soubory,
			 * pokud například byl smazan workspace apod. Pak už zbývá jedině znovu vytvořit
			 * adresář workspace s projekty apod. Ale tuto část zde vynechávám, protože o
			 * tom uživatel musí vědět - že si smazal projekty případně workspace apod. Tak
			 * snad musí vědět, že nelze pracovat s projekty / soubory, které byly smazány.
			 * 
			 * Otestuji, zda existuje workspace, pak configuration, pak příslušný soubor,
			 * případně některé z nich mohu vytvořit:
			 */
			if (pathToWorkspace != null) {
				acquireMutex();
				
				/*
				 * Pokud neexsistuje adresář configuration ve workspace, bude vytvořen i s
				 * potřebnými soubory s výchozím nastavením.
				 * 
				 * Pokud adresář configuration ve workspace existuje, pak stačí vytvořit jen
				 * soubor CodeEditor.properties, protože vím, že neexistuje, když se jej nepodařilo
				 * načíst.
				 */
				if (ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
					ConfigurationFiles.writeCodeEditorProperties(
							pathToWorkspace + File.separator + Constants.PATH_TO_CODE_EDITOR_PROPERTIES);

				
				else
					WriteToFile.createConfigureFilesInPath(pathToWorkspace);

				// Znovu načtu soubor Default.properties z workspace z configuration, protože
				// výše byl znovu vytvořn:
				codeEditorProp = App.READ_FILE.getPropertiesInWorkspacePersistComments(Constants.CODE_EDITOR_NAME);
				
				releaseMutex();
			}
			/*
			 * část else:
			 * 
			 * Zde by bylo možné vytvořit dříve zadaný adresář workspace - pokud byl již
			 * zadán, ale byl smazán, ale to by v rámci aplikace nemělo nastat, proto tuto
			 * část vynechám, uživatel jej musí smazat sám, pak o tom musí vědět.
			 */
		}
		
		
		
		
		
		
		if (codeEditorProp != null) {
			// Zde naplním soubor hodnotami z dialogu - změny 
			
			codeEditorProp.setProperty("HighlightingCode", String.valueOf(chcbHighlightingCode.isSelected()));
			
			codeEditorProp.setProperty("HighlightingCodeByKindOfFile", String.valueOf(chcbHighlightCodeByKindOfFile.isSelected()));			
			
			// Zda se má třída kompilovat na pozadí a načtat jeji proměnné do nápovedy slovíček:
			codeEditorProp.setProperty("CompileClassInBackground", String.valueOf(chcbCompileInBackground.isSelected()));
			
			// Barva pozadí:
			codeEditorProp.setProperty("BackgroundColor", Integer.toString(bgColor.getRGB()));
			
			// Barva písma:
			codeEditorProp.setProperty("ForegroundColor", Integer.toString(textColor.getRGB()));
			
			// Velikost písma:
			codeEditorProp.setProperty("FontSize", Integer.toString((Integer) cmbFontSize.getSelectedItem()));
			
			// Typ písma:
			final int fontStyle = cmbFontStyle.getSelectedIndex();
			
			if (fontStyle == 0)
				codeEditorProp.setProperty("FontStyle", Integer.toString(1));
			else if (fontStyle == 1)
				codeEditorProp.setProperty("FontStyle", Integer.toString(2));
			else if (fontStyle == 2)
				codeEditorProp.setProperty("FontStyle", Integer.toString(0));


            // Vlastnosti pro formátování zdrojového kódu:
            codeEditorProp = formattingPropertiesPanel.saveSetting(codeEditorProp);
			
			
			// a uložím:
			codeEditorProp.save();
		}
	}
	
	
	
	
	
	

	@Override
	public void openSettings() {
        /*
         * Zkusím načíst soubor s nastavením pro ClassEditor z Workspace, pokud se to nepovede, tak ho do workspace
         * do adresáře configuration vytvořím - s výchozím nastavením a načtu znovu pkud workspace neexistuje, tak v
         * podstatě nic dělat nebudu nový musí vybrat uživatel.
         */
		Properties codeEditorProp = App.READ_FILE.getCodeEditorPropertiesFromWorkspaceOnly();

		if (codeEditorProp == null) {
			final String pathToWorkspace = ReadFile.getPathToWorkspace();

			/*
			 * Zde pouze zjisšťuji, zda existují veškeré adresáře k cestě pro soubor
			 * Default.properties v adresáři configuration ve workspace. Pokud ne, pak v
			 * rámci možností nekteré adresáře mohu vytvořit, jinak není kam uložit soubory,
			 * pokud například byl smazan workspace apod. Pak už zbývá jedině znovu vytvořit
			 * adresář workspace s projekty apod. Ale tuto část zde vynechávám, protože o
			 * tom uživatel musí vědět - že si smazal projekty případně workspace apod. Tak
			 * snad musí vědět, že nelze pracovat s projekty / soubory, které byly smazány.
			 * 
			 * Otestuji, zda existuje workspace, pak configuration, pak příslušný soubor,
			 * případně některé z nich mohu vytvořit:
			 */
			if (pathToWorkspace != null) {
				acquireMutex();

				/*
				 * Pokud neexsistuje adresář configuration ve workspace, bude vytvořen i s
				 * potřebnými soubory s výchozím nastavením.
				 * 
				 * Pokud adresář configuration ve workspace existuje, pak stačí vytvořit jen
				 * soubor Default.properties, protože vím, že neexistuje, když se jej nepodařilo
				 * načíst.
				 */
				if (ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
					ConfigurationFiles.writeCodeEditorProperties(
							pathToWorkspace + File.separator + Constants.PATH_TO_CODE_EDITOR_PROPERTIES);

				else
					WriteToFile.createConfigureFilesInPath(pathToWorkspace);


				// Znovu načtu soubor Default.properties z workspace z configuration, protože
				// výše byl znovu vytvořn:
				codeEditorProp = App.READ_FILE.getCodeEditorPropertiesFromWorkspaceOnly();

				releaseMutex();
			}
			/*
			 * část else:
			 * 
			 * Zde by bylo možné vytvořit dříve zadaný adresář workspace - pokud byl již
			 * zadán, ale byl smazán, ale to by v rámci aplikace nemělo nastat, proto tuto
			 * část vynechám, uživatel jej musí smazat sám, pak o tom musí vědět.
			 */
		}
		
		
		
		
		// Zde pokud existuje workspace, by měl být soubor CodeEditor.properties načten, tak vyplním hodnoty do dialogu:
		if (codeEditorProp != null) {
			
			chcbHighlightingCode.setSelected(Boolean.parseBoolean(codeEditorProp.getProperty("HighlightingCode", String.valueOf(Constants.CODE_EDITOR_HIGHLIGHTING_CODE))));
			
			chcbHighlightCodeByKindOfFile.setSelected(Boolean.parseBoolean(codeEditorProp.getProperty("HighlightingCodeByKindOfFile", String.valueOf(Constants.CODE_EDITOR_HIGHLIGHTING_CODE_BY_KIND_OF_FILE))));			
			
			chcbCompileInBackground.setSelected(Boolean.parseBoolean(codeEditorProp.getProperty("CompileClassInBackground", String.valueOf(Constants.CODE_EDITOR_COMPILE_CLASS_IN_BACKGROUND))));
			
			
			final String backgroundColor = codeEditorProp.getProperty("BackgroundColor", Integer.toString(Constants.CODE_EDITOR_BACKGROUND_COLOR.getRGB()));
			bgColor = new Color(Integer.parseInt(backgroundColor));
			lblChoosedBgColor.setForeground(bgColor);
			lblChoosedBgColor.setBackground(bgColor);
			
			
			final String textColorText = codeEditorProp.getProperty("ForegroundColor", Integer.toString(Constants.CODE_EDITOR_FOREGROUND_COLOR.getRGB()));
			textColor = new Color(Integer.parseInt(textColorText));
			lblChoosedTextColor.setForeground(textColor);
			lblChoosedTextColor.setBackground(textColor);
			
			
			
			final int fontSizeFromFile = Integer.parseInt(codeEditorProp.getProperty("FontSize", String.valueOf(Constants.DEFAULT_FONT_FOR_CODE_EDITOR.getSize())));
			if (fontSizeFromFile >= 8 && fontSizeFromFile <= 100)
				cmbFontSize.setSelectedItem(fontSizeFromFile);
			
			
			
			final int fontStyle = Integer.parseInt(codeEditorProp.getProperty("FontStyle", Integer.toString(Constants.DEFAULT_FONT_FOR_CODE_EDITOR.getStyle())));
			if (fontStyle == 0)		// Plain
				cmbFontStyle.setSelectedIndex(2);
			else if (fontStyle == 1)	// Bold
				cmbFontStyle.setSelectedIndex(0);
			else if (fontStyle == 2)	// Italic
				cmbFontStyle.setSelectedIndex(1);


            // Vlastnosti pro formátování zdrojového kódu:
            formattingPropertiesPanel.openSettings(codeEditorProp);
		}
	}








	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnChooseBgColor) {
				final Color tempVariable = JColorChooser.showDialog(this, txtChooseBgColorDialog,
						Constants.CODE_EDITOR_BACKGROUND_COLOR);
				
				if (tempVariable != null) {
					bgColor = tempVariable;
					lblChoosedBgColor.setForeground(bgColor);
					lblChoosedBgColor.setBackground(bgColor);
				}
			}
			
			
			else if (e.getSource() == btnChooseTextColor) {
				final Color tempVariable = JColorChooser.showDialog(this, txtChooseTextColorDialog,
						Constants.CODE_EDITOR_FOREGROUND_COLOR);
				
				if (tempVariable != null) {
					textColor = tempVariable;
					lblChoosedTextColor.setForeground(textColor);
					lblChoosedTextColor.setBackground(textColor);
				}
			}
			
			
			else if (e.getSource() == btnRestoreDefaultSettings) {
				chcbHighlightingCode.setSelected(Constants.CODE_EDITOR_HIGHLIGHTING_CODE);
				chcbHighlightCodeByKindOfFile.setSelected(Constants.CODE_EDITOR_HIGHLIGHTING_CODE_BY_KIND_OF_FILE);
				chcbCompileInBackground.setSelected(Constants.CODE_EDITOR_COMPILE_CLASS_IN_BACKGROUND);
				
				bgColor = Constants.CODE_EDITOR_BACKGROUND_COLOR;
				lblChoosedBgColor.setForeground(bgColor);
				lblChoosedBgColor.setBackground(bgColor);
				
				textColor = Constants.CODE_EDITOR_FOREGROUND_COLOR;
				lblChoosedTextColor.setBackground(textColor);
				lblChoosedTextColor.setForeground(textColor);
				
				
				cmbFontSize.setSelectedItem(Constants.DEFAULT_FONT_FOR_CODE_EDITOR.getSize());
				
				final int fontStyle = Constants.DEFAULT_FONT_FOR_CODE_EDITOR.getStyle();
				
				if (fontStyle == 0)		// Plain
					cmbFontStyle.setSelectedIndex(2);
				else if (fontStyle == 1)	// Bold
					cmbFontStyle.setSelectedIndex(0);
				else if (fontStyle == 2)	// Italic
					cmbFontStyle.setSelectedIndex(1);

                // Vlastnosti pro formátování zdrojového kódu:
                formattingPropertiesPanel.setDefaultValues();
			}
		}
	}
}