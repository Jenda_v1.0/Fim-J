package cz.uhk.fim.fimj.settings_form;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.commands_editor.Editor;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.GetIconInterface;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.output_editor.OutputEditor;

/**
 * Tato třída slouží jako dialog pro nastavení aplikace jako ne např nastavení jazyka aplikace, nastavení diagramů -
 * fonty, barva pozadí, písma, nějaké nastavení pro design buněk, apod. Dálě lze upravit nastavení command editru a
 * class diagramu
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class SettingsForm extends JDialog implements ActionListener, LanguageInterface {

	private static final long serialVersionUID = 1L;


	
	private static JButton btnOk, btnCancel;
	
	
	/**
	 * Komponenta, do které vložím ostatní panely
	 */
	private static JTabbedPane tabbedPane;
	
	
	
	// Panely pro editaci, pro přidání do Tabbed pane
	private static ApplicationPanel pnlApplication;
	private static ClassDiagramPanel pnlClassDiagram;
	private static InstanceDiagramPanel pnlInstanceDiagram;
	private static CommandEditorPanel pnlCommandEditor;
	private static CodeEditorPanel pnlCodeEditor;
	private static OutputEditorPanel pnlOutputEditor;
	

	
	/**
	 * Panel pro nastavení outputFramu - výstupního terminálu:
	 */
	private static OutputFramePanel pnoOutputFrame;
	
	
	/**
	 * panel, do kterého se vloži Jlabel a panel výše - OutputFramePanel
	 */
	private static OutputFramePanelForSettings pnlOutputFrameForSettings; 
	
	
	
	private static ProjectExplorerCodeEditorPanel pnlProjectExplorerEditor;
	
	
	
	// Text do popisků a text do textu záložek:
	private static String pnlAppTitle, pnlAppText, pnlClassDiagTitle, pnlClassDiagText, pnlInstancDiagTitle,
			pnlInstancDiagText, pnlCommaEditTitle, pnlCommaEditText, pnlCodeEditTitle, pnlCodeEditText,
			pnlOutputEditTitle, pnlOutputEditText, pnlProjectExpCodeEditTitle, pnlProjectExpCodeEditText,
			pnlOutputFrameTitle, pnlOutputFrameText,

			// Texty pro restart aplikace:
			txtRestartAppText, txtRestartAppTitle;


	/**
	 * Reference na okno aplikace. Je zde potřeba kvůli zavolání metody pro restartování aplikace.
	 */
	private final App app;

	
	
	

	public SettingsForm(final Editor commandEditor, final OutputEditor outputEditor, final App app) {
		super();

		this.app = app;

		initGui();		
		
		
		// Instance tabbedPane
		tabbedPane = new JTabbedPane();
		
		// vytvoření instance panelu a jejich přidání do tabbed Pane
		pnlApplication = new ApplicationPanel();
		
		tabbedPane.addTab(pnlAppTitle, GetIconInterface.getMyIcon(Constants.PATH_TO_APP_ICON, true),
				new JScrollPane(pnlApplication), pnlAppText);
		
		
		pnlClassDiagram = new ClassDiagramPanel();
		tabbedPane.addTab(pnlClassDiagTitle, GetIconInterface.getMyIcon("/icons/app/ClassDiagramForSettingsFormIcon.png", true),
				new JScrollPane(pnlClassDiagram), pnlClassDiagText);
		
		
		pnlInstanceDiagram = new InstanceDiagramPanel();
		tabbedPane.addTab(pnlInstancDiagTitle,
				GetIconInterface.getMyIcon("/icons/app/InstanceDiagramForSettingsFormIcon.png", true),
				new JScrollPane(pnlInstanceDiagram), pnlInstancDiagText);
		
		
		
		
		
		
		pnlCommandEditor = new CommandEditorPanel(commandEditor);
		final JPanel pnlCommandEditor2 = new JPanel(new FlowLayout(FlowLayout.CENTER, 20, 20));
		pnlCommandEditor2.add(pnlCommandEditor);
		pnlCommandEditor.setMaximumSize(
				new Dimension(pnlCommandEditor.getMinimumSize().width, pnlCommandEditor.getMinimumSize().height));
		
		tabbedPane.addTab(pnlCommaEditTitle, GetIconInterface.getMyIcon("/icons/app/CommandEditorIcon.png", true),
				new JScrollPane(pnlCommandEditor2), pnlCommaEditText);
		
		
		
		
		
		
		
		pnlOutputEditor = new OutputEditorPanel(outputEditor);
		final JPanel pnlOutputEditor2 = new JPanel(new FlowLayout(FlowLayout.CENTER, 20, 20));
		pnlOutputEditor2.add(pnlOutputEditor);
		pnlOutputEditor.setMaximumSize(
				new Dimension(pnlOutputEditor.getMinimumSize().width, pnlOutputEditor.getMinimumSize().height));
		
		tabbedPane.addTab(pnlOutputEditTitle,
				GetIconInterface.getMyIcon("/icons/app/OutputEditorForSettingsFormIcon.png", true),
				new JScrollPane(pnlOutputEditor2), pnlOutputEditText);
		
		
		
		
		
		pnoOutputFrame = new OutputFramePanel();
		pnlOutputFrameForSettings = new OutputFramePanelForSettings(pnoOutputFrame);
		
		final JPanel pnlOutputFrame2 = new JPanel(new FlowLayout(FlowLayout.CENTER, 20, 20));
		pnlOutputFrame2.add(pnlOutputFrameForSettings);
		
		pnlOutputFrame2.setMaximumSize(
				new Dimension(pnlOutputFrame2.getMinimumSize().width, pnlOutputFrame2.getMinimumSize().height));
		
		tabbedPane.addTab(pnlOutputFrameTitle,
				GetIconInterface.getMyIcon("/icons/outputFrameIcons/OutputFrameIcon.png", true),
				new JScrollPane(pnlOutputFrame2), pnlOutputFrameText);
		
		
		
		
		
		
		
		pnlCodeEditor = new CodeEditorPanel();
		final JPanel pnlClassEditor2 = new JPanel(new FlowLayout(FlowLayout.CENTER, 20, 20));
		pnlClassEditor2.add(pnlCodeEditor);
		tabbedPane.addTab(pnlCodeEditTitle, GetIconInterface.getMyIcon("/icons/app/ClassEditorIcon.png", true),
				new JScrollPane(pnlClassEditor2), pnlCodeEditText);
		
		
		
		
		pnlProjectExplorerEditor = new ProjectExplorerCodeEditorPanel();
		final JPanel pnlProjectExplorerEditor2 = new JPanel(new FlowLayout(FlowLayout.CENTER, 20, 20));
		pnlProjectExplorerEditor2.add(pnlProjectExplorerEditor);
		tabbedPane.addTab(pnlProjectExpCodeEditTitle,
				GetIconInterface.getMyIcon("/icons/projectExplorerIcons/ProjectExplorerDialogIcon.gif", true),
				new JScrollPane(pnlProjectExplorerEditor2), pnlProjectExpCodeEditText);
		
		
		
		
		// Přidání tabbedPane s ostatními panely do okna idalogu
		add(tabbedPane, BorderLayout.CENTER);
		
		
		
		// Pane s tlačítky:
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		btnCancel = new JButton();
		btnOk = new JButton();
		
		btnOk.addActionListener(this);
		btnCancel.addActionListener(this);
		
		pnlButtons.add(btnOk);
		pnlButtons.add(btnCancel);
		
		add(pnlButtons, BorderLayout.SOUTH);

		
		
		pack();
		setLocationRelativeTo(null);
	}
	
	
	


	/**
	 * Metoda, kter8 nastaví "základní" design okna tohoto dialogu po spuštění
	 */
	private void initGui() {
		setLayout(new BorderLayout());
		setIconImage(GetIconInterface.getMyIcon("/icons/app/SettingsIcon.png", true).getImage());
		setPreferredSize(new Dimension(925, 650));
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setModal(true);
	}

	
	

	
	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			setTitle(properties.getProperty("SfDialogTitle", Constants.SF_TITLE));
			
			// texty do záložek:
			pnlAppTitle = properties.getProperty("SfApplicationTitle", Constants.SF_APP_TITLE);
			pnlClassDiagTitle = properties.getProperty("SfClassDiagramTitle", Constants.SF_CLASS_DIAG_TITLE);
			pnlInstancDiagTitle = properties.getProperty("SfInstanceDiagramTitle", Constants.SF_INSTANC_DIAG_TITLE);
			pnlCommaEditTitle = properties.getProperty("SfCommandEditorTitle", Constants.SF_COMMAND_EDIT_TITLE);
			pnlCodeEditTitle = properties.getProperty("SfClassEditorTitle", Constants.SF_CODE_EDIT_TITLE);
			pnlOutputEditTitle = properties.getProperty("SfOutputEditorTitle", Constants.SF_OUTPUT_EDIT_TITLE);
			
			// texty k popiskům záložek:
			pnlAppText = properties.getProperty("SfApplicationToolTip", Constants.SF_APP_TOOL_TIP);
			pnlClassDiagText = properties.getProperty("SfClassDiagramToolTip", Constants.SF_CLASS_DIAG_TOOL_TIP);
			pnlInstancDiagText = properties.getProperty("SfInstanceDiagramToolTip", Constants.SF_INSTANC_DIAG_TOOL_TIP);
			pnlCommaEditText = properties.getProperty("SfCommandEditorToolTip", Constants.SF_COMMAND_EDIT_TOOL_TIP);
			pnlCodeEditText = properties.getProperty("SfClassEditorToolTip", Constants.SF_CODE_EDIT_TOOL_TIP);				
			pnlOutputEditText = properties.getProperty("SfOutputEditorToolTip", Constants.SF_OUTPUT_EDIT_TOOL_TIP);			
			pnlProjectExpCodeEditTitle = properties.getProperty("SfPnlProjectExpCodeEditTitle", Constants.SF_PNL_PROJECT_EXP_CODE_EDIT_TITLE);
			pnlProjectExpCodeEditText = properties.getProperty("SfPnlProjectExpCodeEditText", Constants.SF_PNL_PROJECT_EXP_CODE_EDIT_TEXT);
			pnlOutputFrameTitle = properties.getProperty("SfPnlOutputFrameTitle", Constants.SF_PNL_OUTPUT_FRAME_TITLE); 
			pnlOutputFrameText = properties.getProperty("SfPnlOutputFrameText", Constants.SF_PNL_OUTPUT_FRAME_TEXT);
			
			// texty pro tlačítka:
			btnOk.setText(properties.getProperty("SfButtonOk", Constants.SF_BTN_OK));
			btnCancel.setText(properties.getProperty("SfButtonCancel", Constants.SF_BTN_CANCEL));
						
			// Texty pro restart aplikace:
			txtRestartAppText = properties.getProperty("Sf_RestartAppText", Constants.SF_TXT_RESTART_APP_TEXT);
			txtRestartAppTitle = properties.getProperty("Sf_RestartAppTitle", Constants.SF_TXT_RESTART_APP_TITLE);
		}
		
		
		else {
			setTitle(Constants.SF_TITLE);						
			
			// texty do záložek:
			pnlAppTitle = Constants.SF_APP_TITLE;
			pnlClassDiagTitle = Constants.SF_CLASS_DIAG_TITLE;
			pnlInstancDiagTitle = Constants.SF_INSTANC_DIAG_TITLE;
			pnlCommaEditTitle = Constants.SF_COMMAND_EDIT_TITLE;
			pnlCodeEditTitle = Constants.SF_CODE_EDIT_TITLE;
			pnlOutputEditTitle = Constants.SF_OUTPUT_EDIT_TITLE;
			
			// texty k popiskům záložek:
			pnlAppText = Constants.SF_APP_TOOL_TIP;
			pnlClassDiagText = Constants.SF_CLASS_DIAG_TOOL_TIP;
			pnlInstancDiagText = Constants.SF_INSTANC_DIAG_TOOL_TIP;
			pnlCommaEditText = Constants.SF_COMMAND_EDIT_TOOL_TIP;
			pnlCodeEditText = Constants.SF_CODE_EDIT_TOOL_TIP;			
			pnlOutputEditText = Constants.SF_OUTPUT_EDIT_TOOL_TIP;
			pnlProjectExpCodeEditTitle = Constants.SF_PNL_PROJECT_EXP_CODE_EDIT_TITLE;
			pnlProjectExpCodeEditText = Constants.SF_PNL_PROJECT_EXP_CODE_EDIT_TEXT;
			pnlOutputFrameTitle = Constants.SF_PNL_OUTPUT_FRAME_TITLE; 
			pnlOutputFrameText = Constants.SF_PNL_OUTPUT_FRAME_TEXT;
			
			// texty pro tlačítka:
			btnOk.setText(Constants.SF_BTN_OK);
			btnCancel.setText(Constants.SF_BTN_CANCEL);
			
			// Texty pro restart aplikace:
			txtRestartAppText = Constants.SF_TXT_RESTART_APP_TEXT;
			txtRestartAppTitle = Constants.SF_TXT_RESTART_APP_TITLE;
		}
		
		
		// Zde potřebuji aktualizovat komponentu TabbedPane, 
		// metoda: tabbedPane.repaint(); nefunguje, tak aktualizuji 
		// jednotlivé záložky "ručně" :
		
		tabbedPane.setTitleAt(0, pnlAppTitle);
		tabbedPane.setTitleAt(1, pnlClassDiagTitle);
		tabbedPane.setTitleAt(2, pnlInstancDiagTitle);
		tabbedPane.setTitleAt(3, pnlCommaEditTitle);
		tabbedPane.setTitleAt(4, pnlOutputEditTitle);
		tabbedPane.setTitleAt(5, pnlOutputFrameTitle);
		tabbedPane.setTitleAt(6, pnlCodeEditTitle);
		tabbedPane.setTitleAt(7, pnlProjectExpCodeEditTitle);
		
		
		
		
		// Doplnění textu ve zvoleném jazyce doostatních panelů:
		pnlApplication.setLanguage(properties);
		pnlClassDiagram.setLanguage(properties);
		pnlInstanceDiagram.setLanguage(properties);
		pnlCommandEditor.setLanguage(properties);
		pnlOutputEditor.setLanguage(properties);
		pnoOutputFrame.setLanguage(properties);
		pnlOutputFrameForSettings.setLanguage(properties);
		pnlCodeEditor.setLanguage(properties);
		pnlProjectExplorerEditor.setLanguage(properties);
	}


	
	
	
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnOk) {				
				// V každém z panelů se uloží příslušné hodnoty do příslušných souborů
				
				
				// Zpracování dat z ApplicationPanel do Default.properties, do classDiagram.prop, ... - uložení změněných hodnot::
				pnlApplication.saveSetting();
				

				pnlCommandEditor.saveSetting();
				pnlOutputEditor.saveSetting();
				pnoOutputFrame.saveSetting();
				pnlCodeEditor.saveSetting();
				pnlProjectExplorerEditor.saveSetting();
				
				// Zde nejprve musím otestovat, zda nejsou hrany a bu�?ky identické,
				// Pak bych nerozeznal hrany, která co reprezentuje
				
				// Dále:
				// Zde musí také otestovat, zda se neshodují hrany pro reprezentaci agregace a asociace,
				// jinak bych je pak v diagramu instancí nerozeznal:
				if (!pnlInstanceDiagram.areEdgesAndCellsIdentical()
						&& !pnlClassDiagram.areEdgesAndCellsIdentical()) {
					// Zde se hrany liší, mohu uložit nastavení a zavřit dialog
					pnlClassDiagram.saveSetting();
					pnlInstanceDiagram.saveSetting();
					dispose();
					
					// Zpetám se uživatele, zda se má restartovat aplikace:
					restartApp();
				}
			}
			
			
			
			else if (e.getSource() == btnCancel)
				dispose();
		}
	}






    /**
     * Metoda, která se zeptá uživatele, zda se má restartovat aplikace. Pokud ano, rovnou se zavolá metoda, která
     * restartuje tuto aplikaci.
     */
    private void restartApp() {
        if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(SettingsForm.this, txtRestartAppText,
                txtRestartAppTitle,
                JOptionPane.YES_NO_OPTION))
            app.restartApp();
    }
	
	

	
	
	
	/**
	 * Tato třída slouží pouze jako další panel, do kterého se vloží label s
	 * popiskem že se musí restartovat aplikace pro projevení změn, a panel, který
	 * slouží pro nastavení OutputFrame - výstupního terminálu - editoru.
	 * 
	 * Tato třída je zde pouze pro to ze tym potrebuji pridat jeste ten Jlabel, tak
	 * aby se to shodovalo s ostatnimi panely v tomto nastaveni a aby sli
	 * zarovnat¨nastred jako ostatni
	 * 
	 * @author Jan Krunčík
	 * @version 1.0
	 */
	private static final class OutputFramePanelForSettings extends PanelAbstract implements LanguageInterface {

		private static final long serialVersionUID = 1L;
		
		
		/**
		 * Proměná, která bude sloužít pro to, aby v ní byl text, který upozorní
		 * uživatele, že aby se projevily změny, tak musí restartovat aplikaci Tento
		 * Jlabel bude umístěn akorát v panelu pro Výstupní terminál - OuputFramePanel
		 */
		private static JLabel lblResetApp;
		
		
		
		/**
		 * Konstruktor této třídy
		 * 
		 * @param pnlOutputFrame
		 *            - instance třídy OutputFramePanel, protože k němu potřebuji ještě
		 *            přidat proměnnou typu Jlabel, ve které bude popisek, že se musí
		 *            restartovat aplikace, aby se změny projevily
		 */
		OutputFramePanelForSettings(final JPanel pnlOutputFrame) {
			super();
			
			setLayout(new GridBagLayout());
			
			final GridBagConstraints gbc = getGbc();
			
			lblResetApp = new JLabel();
			setGbc(gbc, 0, 0, lblResetApp, this);
			
			
			setGbc(gbc, 1, 0, pnlOutputFrame, this);
		}

		
		
		@Override
		public void setLanguage(final Properties properties) {
			if (properties != null)
				lblResetApp.setText(properties.getProperty("Sf_App_LabelResetApplication", Constants.SF_LBL_RESET_APP));

			else
				lblResetApp.setText(Constants.SF_LBL_RESET_APP);
		}
	}
}