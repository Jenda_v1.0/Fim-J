package cz.uhk.fim.fimj.settings_form.application_panels;

/**
 * Třída slouží jako informace pro jazyky aplikace obsahuje text = jazyk a obsahuje indentifikátor - např CZ, EN, ... =
 * identifikator pro jazyk, který se má nastavit do aplikace - dle identifikátoru poznám, jaký souboru s jakým jazykem
 * mám otevřít: např CZ.properties, EN.properites, ... v adresáři configuration v složce coby Worksapce, pokud nebude
 * nalezen tak v adresáři aplikace, configuration, language, pokud ani ten nebude nalezen, tak výchozí jazyk = CZ, z
 * Constants - třídy
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class LanguageInfo {

    private final String languageText;

    private final String languageId;


    LanguageInfo(final String languageText, final String languageId) {
        this.languageText = languageText;
        this.languageId = languageId;
    }


    public final String getLanguageId() {
        return languageId;
    }


    @Override
    public String toString() {
        // Metoda slouží, aby se v JComboboxu zobrazil pouze text - název jazyka:
        return languageText;
    }
}