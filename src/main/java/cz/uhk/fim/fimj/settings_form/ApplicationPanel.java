package cz.uhk.fim.fimj.settings_form;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.*;
import cz.uhk.fim.fimj.language.EditProperties;
import cz.uhk.fim.fimj.language.Languages;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.settings_form.application_panels.*;

/**
 * Tato třída slouží jako panel pro základní možnosti nastavení aplikace
 * <p>
 * Obsahuje možnosti pro zkopirovaní adresare configuration, případně některých z jeho podadresářů, nastavení jazyka,
 * načítacího obrázku do Loading dialogu a mnoho dalšího, viz jednotlvé části
 * <p>
 * Note: V metodách pro otevření nastavení ze souboru a uložení nastavení by se to dalo udělat tak, že bych třeba do
 * ReadFile nebo WriteToFile nebo někam do abstraktních tříd napsal metody, které si zjistí vždy potřený soubor,
 * případně jej vytvoří ve workspace, toto platí pro všechny panely pro nastavení, ale jde o to, že bych potřeboval
 * navíc stejně vytvořit výčet nebo to vyřešit podobným způsobem, abych pokaždé věděl, jaký soubor mám hledat s jakým
 * názvem a dle toho případně vytvořit potřebné souboru, ale to jsem zavrhnul, protože jediné využití je v těchto
 * panelech - pouze jednou pro každý konfigurační soubor. Tak jsem to nechal trochu "přehlednější" - alespoň pro mě, a
 * vždy si načítám příslušný soubor pro příslušný panel.
 * <p>
 * <p>
 * Note: využití mutexu není potřeba u zaápisu souborů, ale doplnil jsem vlákno pro monitorování těch změn, tak nechci,
 * aby se nějak ovlivnil ten zápis souborů, i když by k tomu nemělo dojít.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ApplicationPanel extends PanelAbstract
        implements ActionListener, SaveSettingsInterface, LanguageInterface {

    private static final long serialVersionUID = 3076695626868492899L;


    /**
     * Zda se má ptát aplikace při jejím spuštění na Workspace:
     */
    private final JCheckBox chcbAskForWorkspace;


    // Labely pro popis - co se po kliknutí na tlačítko vedle zkopíruje
    private final JLabel lblCopyConfigurationDir, lblCopyLanguageDir, lblCopyDefaultProp, lblCopyCD_Prop,
            lblCopyID_Prop, lblCopyCommandEditorProp, lblCopyCodeEditorProp, lblResetApp, lblCopyOutputEditProp,
            lblCopyOutputFrameProp, lblCopyCodeEditorForInternalFrames, lblAnimationImage;


    private final JButton btnCopyConfigurationDir, btnCopyLanguageDir, btnCopyDefaultProp, btnCopyCDProp, btnCopyIDProp,
            btnCopyCommandEditorProp, btnCopyCodeEditorProp, btnCopyOutputEditProp, btnCopyOutputFrameProp,
            btnCopyCodeEditorForInternalFrames;


    /**
     * Toto tlačítko obnoví veškeré nastavení v tomto panelu do výchozího nastavení, tj. označí se výchozí položky, jako
     * je například jazyk, způsob řazení, výchozí obrázek v Loading dialogu apod.
     */
    private final JButton btnRestoreDefault;


    /**
     * JComboBox, ve kterém budou definovány možnosti pro obrázky, které budou reprezentovat načítání, když běží
     * například vlákno, apod. tak se zobrazí dialog s příslušným obrázkem typu .gif coby animace, která reprezentuje
     * načítání, aby si uživatel nemyslël, že došlo například k chybě, nebo něco v tom smyslu
     */
    private final JComboBox<ImageInfo> cmbImages;


    /**
     * Model pro výběr obrázků do načítacího dialogu
     */
    private static final List<ImageInfo> IMAGE_INFO_MODEL = Collections
            .unmodifiableList(Arrays.asList(new ImageInfo("balls"), new ImageInfo("battery"), new ImageInfo("box"),
                    new ImageInfo("clock"), new ImageInfo("dashinfinity"), new ImageInfo("default"),
                    new ImageInfo("ellipsis"), new ImageInfo("flickr"), new ImageInfo("gear"), new ImageInfo("gears"),
                    new ImageInfo("hourglass"), new ImageInfo("infinity"), new ImageInfo("pacman"),
                    new ImageInfo("reload"), new ImageInfo("ring"), new ImageInfo("ripple"), new ImageInfo("squares"),
                    new ImageInfo("sunny"), new ImageInfo("triangle"), new ImageInfo("wave"), new ImageInfo("wheel")));


    // Následující proměnné musí být globální neboli instanční, aby bylo možné jim nastavit titulek ohraničení:
    private final JPanel pnlLanguage;
    private final JPanel pnlCopyDir;
    private final JPanel pnlLoadingFramePanel;


    /**
     * Panel pro nastavení Look and Feel.
     */
    private final LookAndFeelPanel pnlLookAndFeel;


    /**
     * Panel pro nastavení uživatelského jména.
     */
    private final UserNamePanel userNamePanel;


    /**
     * Panel, který obsahuje komponenty pro nastavení toho, zda se má pří spuštění aplikace testovat existence adresáře,
     * která obsahuje soubory potřebné pro kompilaci tříd v aplikaci, a kdyby je neobshoval nebo samotný adresář
     * neexistoval, tak zda se při hledání těch souborů má prohledat celý disk nebo ne (na daném zařízení, kde běží tato
     * aplikace).
     * <p>
     * Tento panel je zde implementován jako globální neboli instanční proměnná, pouze pro to, aby bylo možné mu
     * nastavit popisek.
     */
    private final JPanel pnlSearchCompileFiles;


    /**
     * Komponenta, která slouží pro nastavení toho, zda se mají při spuštění aplikace vyhledat soubory potřebné pro to,
     * aby mohla aplikace kompilovat třídy jazyka Java v aplikaci.
     * <p>
     * (To se provede tak, že se spustí vlákno CheckCompileFiles před spuštěním samotného okna aplikace, více viz třída
     * Start.)
     */
    private final JCheckBox chcbSearchCompileFiles;

    /**
     * Komponenta, která slouží pro nastavení toho, zda se má za účelem nalezení potřebných souborů, které aplikace
     * potřebuje, aby mohla kompilovat třídy jazyka Java, tak aby prohledala celý disk na zařízení, na kterém běží tato
     * aplikace.
     * <p>
     * Tato možnost, ale není doporučena, protože se může stát, že se najde soubor se stejným názvem, ale nebude to
     * soubor, který potřebuje aplikace pro zmíněné kompilování tříd.
     */
    private final JCheckBox chcbSearchEntireDiskForCompileFiles;


    // Proměnné, do kterých vložím hodnoty pro text do dialogu pro uložení jednotlivých konfiguračních souborů
    private String txtPlaceConfigurationDir, txtPlaceLanguageDir, txtPlaceDefaultProp, txtPlaceClassDiagramProp,
            txtPlaceInstanceDiagramProp, txtPlaceCommandEditorProp, txtPlaceClassEditorProp, txtPlaceOutputEditProp,
            txtPlaceOutputFrameProp, txtPlaceCodeEditorForInternalFrames;


    /**
     * Panel, který obsahuje komponenty pro nastavení jazyka a způsob řazení (/ "a formátování") textů v souborech
     * .properties, které obsahuje texty pro tuto aplikaci v příslušných jazyích.
     */
    private final LanguagePanel languagePanel;


    /**
     * Panel, který bude obsahovat komponentu pro nastavení toho, zda se mají vypisovat výjimky do dialogu Výstupní
     * terminál.
     * <p>
     * V případě, že v aplikaci Fim-J dojde k nějaké výjimce, kterou se nepodařilo zachytit, tak zda se tato výjimka má
     * vypsat do dialogu Výstupní terminál. Při výpisech se totiž mohou filtrovat texty a pokud se ten text bude
     * nacházet v syntaxi nějaké výjimky, pak se prostě nevypíše.
     */
    private final JPanel pnlCaptureExceptions;

    /**
     * Komponenta, která slouží pro nastavení toho, zda se mají vypisovat nezachycené výjimky v aplikaci Fim-J do
     * dialogu Výstupní terminál. Nebo, zda se mají vyfiltrovat a případně nevypisovat.
     */
    private final JCheckBox chcbCaptureExceptions;


    /**
     * Uložení hodnoty načtené ze souboru, aby se mohla příslušná hodnota označit později v panelu pro nastavení Look
     * and Feel.
     */
    private String indexOfLookAndFeel;


    /**
     * Konstruktor třídy.
     */
    public ApplicationPanel() {
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));


        // Label s informací, že pro projevení změn je třeba restartovat aplikaci:
        lblResetApp = new JLabel();
        add(Box.createRigidArea(new Dimension(0, 15)));
        lblResetApp.setAlignmentX(CENTER_ALIGNMENT);
        add(lblResetApp);


        // Zda se má aplikace dotázat pří spuštění na adresář workspace:
        chcbAskForWorkspace = new JCheckBox();
        add(Box.createRigidArea(new Dimension(0, 15)));
        chcbAskForWorkspace.setAlignmentX(CENTER_ALIGNMENT);
        add(chcbAskForWorkspace);



        // Zda se mají při spuštění aplikace vyhledat soubory potřebné pro kompilací Javovských tříd:
        pnlSearchCompileFiles = new JPanel(new GridBagLayout());
        final GridBagConstraints gbcSearchCompileFiles = getGbc();

        chcbSearchCompileFiles = new JCheckBox();
        chcbSearchEntireDiskForCompileFiles = new JCheckBox();

        setGbc(gbcSearchCompileFiles, 0, 0, pnlSearchCompileFiles, this);
        pnlSearchCompileFiles.add(chcbSearchCompileFiles, gbcSearchCompileFiles);

        setGbc(gbcSearchCompileFiles, 1, 0, pnlSearchCompileFiles, this);
        pnlSearchCompileFiles.add(chcbSearchEntireDiskForCompileFiles, gbcSearchCompileFiles);

        addPanel(pnlSearchCompileFiles, 520, 150);



        pnlCaptureExceptions = new JPanel(new FlowLayout(FlowLayout.CENTER));

        chcbCaptureExceptions = new JCheckBox();
        pnlCaptureExceptions.add(chcbCaptureExceptions);

        addPanel(pnlCaptureExceptions, 350, 150);



        // Panel pro nastavení obrázku do "Loading dialogu":
        pnlLoadingFramePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

        lblAnimationImage = new JLabel();
        pnlLoadingFramePanel.add(lblAnimationImage);

        cmbImages = new JComboBox<>(IMAGE_INFO_MODEL.toArray(new ImageInfo[]{}));
        pnlLoadingFramePanel.add(cmbImages);

        addPanel(pnlLoadingFramePanel, 150, 200);



        // Panel pro nastavení Look And Feel:
        pnlLookAndFeel = new LookAndFeelPanel();
        addPanel(pnlLookAndFeel, 300, 200);



        // Panel pro uživatelské jméno:
        userNamePanel = new UserNamePanel();
        addPanel(userNamePanel, 320, 200);



        // Panel pro vyber jazyka a řazení textů v souborech .properties:
        pnlLanguage = new JPanel(new FlowLayout(FlowLayout.CENTER, 20, 10));
        languagePanel = new LanguagePanel();
        pnlLanguage.add(languagePanel);

        addPanel(pnlLanguage, 250, 100);



        // Tlačítko pro obnovení výchozích hodnot:
        btnRestoreDefault = new JButton();
        btnRestoreDefault.addActionListener(this);

        final JPanel pnlCenterBtnDefault = new JPanel(new FlowLayout(FlowLayout.CENTER));
        pnlCenterBtnDefault.add(btnRestoreDefault);

        add(Box.createRigidArea(new Dimension(0, 20)));
        add(pnlCenterBtnDefault);












        /*
         * Vím, že následující panel by mohl být v samostatné třídě včetně komponent,
         * ale přeci jen mi to přišlo trochu s prominutím všem "zbytečné", kdyby se s
         * tím manipulovalo ve smyslu přidání či odebrání příslušného tlačítka, bude to
         * stejné, a na přehlednosti to až tak moc nemění, zde by se pouze přidal jeden
         * panel, od "ostatního" kódu to je v celku také oddělené a přehledné to také
         * celkem je (alespoň tedy z mého pohledu, proto se mi nechtělo vytvořit pro to
         * další třídu).
         */

        // Panel pro kopírování:
        pnlCopyDir = new JPanel();
        // Layout + parametry layoutu
        pnlCopyDir.setLayout(new GridBagLayout());
        final GridBagConstraints gbc = getGbc();

        index = 0;

        // Popisek a tlačítko pro kopírování konfiguračního souboru
        lblCopyConfigurationDir = new JLabel();
        setGbc(gbc, index, 0, lblCopyConfigurationDir, pnlCopyDir);
        btnCopyConfigurationDir = new JButton();
        btnCopyConfigurationDir.addActionListener(this);
        setGbc(gbc, index, 1, btnCopyConfigurationDir, pnlCopyDir);


        // Popisek a tlačítko pro nakopírování adresáře s jazyky
        lblCopyLanguageDir = new JLabel();
        setGbc(gbc, ++index, 0, lblCopyLanguageDir, pnlCopyDir);
        btnCopyLanguageDir = new JButton();
        btnCopyLanguageDir.addActionListener(this);
        setGbc(gbc, index, 1, btnCopyLanguageDir, pnlCopyDir);


        // Popisek a tlačítko pro nakopírování adresáře Default.properties
        lblCopyDefaultProp = new JLabel();
        setGbc(gbc, ++index, 0, lblCopyDefaultProp, pnlCopyDir);
        btnCopyDefaultProp = new JButton();
        btnCopyDefaultProp.addActionListener(this);
        setGbc(gbc, index, 1, btnCopyDefaultProp, pnlCopyDir);


        //  popisek a tlačítko pro nakopírování  souboru ClassDiagram.propeties
        lblCopyCD_Prop = new JLabel();
        setGbc(gbc, ++index, 0, lblCopyCD_Prop, pnlCopyDir);
        btnCopyCDProp = new JButton();
        btnCopyCDProp.addActionListener(this);
        setGbc(gbc, index, 1, btnCopyCDProp, pnlCopyDir);


        // Popisek a tlačítko pro nakopírování souboru Instance diagram.properties
        lblCopyID_Prop = new JLabel();
        setGbc(gbc, ++index, 0, lblCopyID_Prop, pnlCopyDir);
        btnCopyIDProp = new JButton();
        btnCopyIDProp.addActionListener(this);
        setGbc(gbc, index, 1, btnCopyIDProp, pnlCopyDir);


        // Tlačítko a popisek pro nakopírování souboru CommandEditor.properties:
        lblCopyCommandEditorProp = new JLabel();
        setGbc(gbc, ++index, 0, lblCopyCommandEditorProp, pnlCopyDir);
        btnCopyCommandEditorProp = new JButton();
        btnCopyCommandEditorProp.addActionListener(this);
        setGbc(gbc, index, 1, btnCopyCommandEditorProp, pnlCopyDir);


        // Tlačítko a popisek pro nakopírování ClassEditor.properties
        lblCopyCodeEditorProp = new JLabel();
        setGbc(gbc, ++index, 0, lblCopyCodeEditorProp, pnlCopyDir);
        btnCopyCodeEditorProp = new JButton();
        btnCopyCodeEditorProp.addActionListener(this);
        setGbc(gbc, index, 1, btnCopyCodeEditorProp, pnlCopyDir);


        // Tlačítka a popisek pro nakopírování OutputEditor.properties:
        lblCopyOutputEditProp = new JLabel();
        setGbc(gbc, ++index, 0, lblCopyOutputEditProp, pnlCopyDir);
        btnCopyOutputEditProp = new JButton();
        btnCopyOutputEditProp.addActionListener(this);
        setGbc(gbc, index, 1, btnCopyOutputEditProp, pnlCopyDir);


        // Tlačítko a popisek pro zkopírování OutputFrame.properties
        lblCopyOutputFrameProp = new JLabel();
        setGbc(gbc, ++index, 0, lblCopyOutputFrameProp, pnlCopyDir);
        btnCopyOutputFrameProp = new JButton();
        btnCopyOutputFrameProp.addActionListener(this);
        setGbc(gbc, index, 1, btnCopyOutputFrameProp, pnlCopyDir);


        lblCopyCodeEditorForInternalFrames = new JLabel();
        setGbc(gbc, ++index, 0, lblCopyCodeEditorForInternalFrames, pnlCopyDir);
        btnCopyCodeEditorForInternalFrames = new JButton();
        btnCopyCodeEditorForInternalFrames.addActionListener(this);
        setGbc(gbc, index, 1, btnCopyCodeEditorForInternalFrames, pnlCopyDir);


        // Přidání panelu do dialogu:
        add(Box.createRigidArea(new Dimension(0, 20)));
        add(pnlCopyDir);


        openSettings();
    }


    /**
     * Přidání panelu panel do okna dialogu, ale tak, že se panelu nastaví hodrizontální pozice na střed a nastavení se
     * mu minimální a maximální šířka (kvůli použítému layout manageru, jinak by se panel roztáhl na šířku dialogu).
     *
     * @param panel
     *         - panel, kterému se má nastavit minimální, nastavit horizontální pozice na střed a maximální šířka a
     *         přidat do okna
     * @param additionalWidth
     *         - "přídavná" šířka ("o jak moc se má zvětšít šířka panelu")
     * @param additionalHeight
     *         - "přídavná" výška ("o jak moc se má zvětšít výška panelu")
     */
    private void addPanel(final JPanel panel, final int additionalWidth, final int additionalHeight) {
        panel.setAlignmentX(CENTER_ALIGNMENT);
        panel.setMaximumSize(new Dimension(panel.getMinimumSize().width + additionalWidth, panel.getMinimumSize().height + additionalHeight));

        add(Box.createRigidArea(new Dimension(0, 20)));
        add(panel);
    }






	@Override
    public void setLanguage(final Properties languageProperties) {
        /*
         * Proměnná pro text "Umístit" - pro tlačítka pro umístění konfiguračních souborů.
         */
        final String txtPlace;
        /*
         * Jednorozměrné pole pro texty do cmb cmbKindOfSortingTextInProp.
         */
        final String[] sortingTextArray;
        /*
         * Jednorozměrné pole pro popisky k položkám (způsobům řazení textů v properties).
         */
        final String[] sortingTtArray;


        if (languageProperties != null) {
			// Texty pro modely do komponenty cmb pro řazení klíčů v souboru .properties:
			sortingTextArray = Languages.createArrayFromText(languageProperties.getProperty("Sf_App_Sorting_Text_In_Prop_File_Model"));
			sortingTtArray = Languages.createArrayFromText(languageProperties.getProperty("Sf_App_Sorting_Text_In_Prop_File_TT_Model"));



			chcbAskForWorkspace.setText(languageProperties.getProperty("Sf_App_CheckBoxAskForWorkspace", Constants.SF_CHCB_ASK_FOR_WORKSPACE));

			pnlLanguage.setBorder(BorderFactory.createTitledBorder(languageProperties.getProperty("Sf_App_BorderLanguage", Constants.SF_BORDER_LANGUAGE)));

			pnlCaptureExceptions.setBorder(BorderFactory.createTitledBorder(languageProperties.getProperty("Sf_App_Pnl_CaptureExceptions", Constants.SF_PNL_CAPTURE_EXCEPTION)));

			pnlLoadingFramePanel.setBorder(BorderFactory.createTitledBorder(languageProperties.getProperty("Sf_App_PanelLanguageBorderTitle", Constants.SF_PNL_LOADING_FRAME_PANEL)));

			pnlCopyDir.setBorder(BorderFactory.createTitledBorder(languageProperties.getProperty("Sf_App_BorderCopyDir", Constants.SF_BORDER_COPY_DIR)));


			// Nastavení titulku:
			pnlSearchCompileFiles.setBorder(BorderFactory.createTitledBorder(languageProperties.getProperty("Sf_App_Ap_PnlSearchCompileFiles", Constants.SF_AP_PNL_SEARCH_COMPILE_FILES)));

			// Texty pro chcb pro testování adresáře se soubory, které jsou potřeba pro kompilaci tříd pomocí aplikace:
			chcbSearchCompileFiles.setText("? " + languageProperties.getProperty("Sf_App_Ap_ChcbSearchCompileFilesText", Constants.SF_AP_CHCB_SEARCH_COMPILE_FILES_TEXT));
			chcbSearchCompileFiles.setToolTipText(languageProperties.getProperty("Sf_App_Ap_ChcbSearchCompileFilesTt", Constants.SF_AP_CHCB_SEARCH_COMPILE_FILES_TT));

			chcbSearchEntireDiskForCompileFiles.setText("? " + languageProperties.getProperty("Sf_App_Ap_ChcbSearchEntireDiskForCompileFilesText", Constants.SF_AP_CHCB_SEARCH_ENTIRE_DISK_FOR_COMPILE_FILES_TEXT));
			chcbSearchEntireDiskForCompileFiles.setToolTipText(languageProperties.getProperty("Sf_App_Ap_ChcbSearchEntireDiskForCompileFilesTt", Constants.SF_AP_CHCB_SEARCH_ENTIRE_DISK_FOR_COMPILE_FILES_TT));


			chcbCaptureExceptions.setText("? " + languageProperties.getProperty("Sf_App_Chcb_CaptureExceptionsText", Constants.SF_CHCB_CAPTURE_EXCEPTIONS_TEXT));
			chcbCaptureExceptions.setToolTipText(languageProperties.getProperty("Sf_App_Chcb_CaptureExceptionsText_TT", Constants.SF_CHCB_CAPTURE_EXCEPTIONS_TEXT_TT));


			// Texty do labelu:
			lblCopyConfigurationDir.setText(languageProperties.getProperty("Sf_App_LabelCopyConfigurationDir", Constants.SF_LBL_COPY_CONFIGURATION_DIR) + ": ");
			lblCopyLanguageDir.setText(languageProperties.getProperty("Sf_App_LabelCopyLanguageDir", Constants.SF_LBL_COPY_LANGUAGE_DIR) + ": ");
			lblCopyDefaultProp.setText(languageProperties.getProperty("Sf_App_LabelCopyDefaultProperties", Constants.SF_LBL_COPY_DEFAULT_PROP) + ": ");
			lblCopyCD_Prop.setText(languageProperties.getProperty("Sf_App_LabelCopyClassDiagramProperties", Constants.SF_LBL_COPY_CD_PROP) + ": ");
			lblCopyID_Prop.setText(languageProperties.getProperty("Sf_App_LabelCopyInstanceDiagramProperties", Constants.SF_LBL_COPY_ID_PROP) + ": ");
			lblCopyCommandEditorProp.setText(languageProperties.getProperty("Sf_App_LabelCopyCommandEditorProperties", Constants.SF_LBL_CPY_COMMAND_EDITOR_PROP) + ": ");
			lblCopyCodeEditorProp.setText(languageProperties.getProperty("Sf_App_LabelCopyClassEditorProperties", Constants.SF_LBL_COPY_CLASS_EDITOR_PROP) + ": ");
			lblCopyOutputEditProp.setText(languageProperties.getProperty("Sf_App_LabelCopyOutputEditorProperties", Constants.SF_LBL_COPY_OUTPUT_EDITOR_PROP) + ": ");
			lblCopyOutputFrameProp.setText(languageProperties.getProperty("Sf_App_LabelCopyOutputFrameProperties", Constants.SF_LBL_COPY_OUTPUT_FRAME_PROP) + ": ");
			lblCopyCodeEditorForInternalFrames.setText(languageProperties.getProperty("Sf_App_LabelCopyCodeEditorForInternalFrames", Constants.SF_LBL_COPY_CODE_EDITOR_FOR_INTERNAL_FAMES) + ": ");
			lblAnimationImage.setText(languageProperties.getProperty("Sf_App_LabelAnimationImage", Constants.SF_LBL_ANIMATION_IMAGE) + ": ");

			// Text do tlačítek:
            txtPlace = languageProperties.getProperty("Sf_App_ButtonsCopyDirectories", Constants.SF_BUTTONS_FOR_PLACES);

			lblResetApp.setText(languageProperties.getProperty("Sf_App_LabelResetApplication", Constants.SF_LBL_RESET_APP));

			btnRestoreDefault.setText(languageProperties.getProperty("Sf_App_BtnRestoreDefault", Constants.SF_BTN_RESTORE_DEFAULT));


			// Texty do dialogu pro umístění ckonfiguračních souborů:
			txtPlaceConfigurationDir = languageProperties.getProperty("Sf_App_Fc_PlaceConfigurationDirText", Constants.SF_APP_FC_CONFIGURATION_DIR);
			txtPlaceLanguageDir = languageProperties.getProperty("Sf_App_Fc_PlaceLanguageDirText", Constants.SF_APP_FC_LANGUAGE_DIR);
			txtPlaceDefaultProp = languageProperties.getProperty("Sf_App_Fc_PlaceDefaultPropertiesFileText", Constants.SF_APP_FC_DEFAULT_PROP);
			txtPlaceClassDiagramProp = languageProperties.getProperty("Sf_App_Fc_PlaceClassDiagramPropertiesFileText", Constants.SF_APP_FC_CLASS_DIAGRAM_PROP);
			txtPlaceInstanceDiagramProp = languageProperties.getProperty("Sf_App_Fc_PlaceInstanceDiagramPropertiesFileText", Constants.SF_APP_FC_INSTANCE_DIAGRAM_PROP);
			txtPlaceCommandEditorProp = languageProperties.getProperty("Sf_App_Fc_PlaceCommandEditorPropertiesFileText", Constants.SF_APP_FC_COMMAND_EDITOR_PROP);
			txtPlaceClassEditorProp = languageProperties.getProperty("Sf_App_Fc_PlaceClassEditorPropertiesFileText", Constants.SF_APP_FC_CLASS_EDITOR_PROP);
			txtPlaceOutputEditProp = languageProperties.getProperty("Sf_App_Fc_PlaceOutputEditorPropertiesFileText", Constants.SF_APP_FC_OUTPUT_EDITOR_PROP);
			txtPlaceOutputFrameProp = languageProperties.getProperty("Sf_App_Fc_PlaceOutputFramePropertiesFileText", Constants.SF_APP_FC_OUTPUT_FRAME_PROP);
			txtPlaceCodeEditorForInternalFrames = languageProperties.getProperty("Sf_App_Fc_PlaceCodeEditorForInternalFramesFileText", Constants.SF_APP_FC_CODE_EDITOR_FOR_INTERNAL_FRAMES_PROP);
		}


		else {
			// Texty pro modely do komponenty cmb pro řazení klíčů v souboru .properties:
			sortingTextArray = Constants.SORTING_TEXT_IN_PROP_FILE_MODEL;
			sortingTtArray = Constants.SORTING_TEXT_IN_PROP_FILE_TT_MODEL;


			chcbAskForWorkspace.setText(Constants.SF_CHCB_ASK_FOR_WORKSPACE);

			pnlLanguage.setBorder(BorderFactory.createTitledBorder(Constants.SF_BORDER_LANGUAGE));

			pnlLoadingFramePanel.setBorder(BorderFactory.createTitledBorder(Constants.SF_PNL_LOADING_FRAME_PANEL));

			pnlCopyDir.setBorder(BorderFactory.createTitledBorder(Constants.SF_BORDER_COPY_DIR));


			// Nastavení titulku:
			pnlSearchCompileFiles.setBorder(BorderFactory.createTitledBorder(Constants.SF_AP_PNL_SEARCH_COMPILE_FILES));

			// Texty pro chcb pro testování adresáře se soubory, které jsou potřeba pro kompilaci tříd pomocí aplikace:
			chcbSearchCompileFiles.setText("? " + Constants.SF_AP_CHCB_SEARCH_COMPILE_FILES_TEXT);
			chcbSearchCompileFiles.setToolTipText(Constants.SF_AP_CHCB_SEARCH_COMPILE_FILES_TT);

			chcbSearchEntireDiskForCompileFiles.setText("? " + Constants.SF_AP_CHCB_SEARCH_ENTIRE_DISK_FOR_COMPILE_FILES_TEXT);
			chcbSearchEntireDiskForCompileFiles.setToolTipText(Constants.SF_AP_CHCB_SEARCH_ENTIRE_DISK_FOR_COMPILE_FILES_TT);

			chcbCaptureExceptions.setText("? " + Constants.SF_CHCB_CAPTURE_EXCEPTIONS_TEXT);
			chcbCaptureExceptions.setToolTipText(Constants.SF_CHCB_CAPTURE_EXCEPTIONS_TEXT_TT);

			// Texty do labelu:
			lblCopyConfigurationDir.setText(Constants.SF_LBL_COPY_CONFIGURATION_DIR + ": ");
			lblCopyLanguageDir.setText(Constants.SF_LBL_COPY_LANGUAGE_DIR + ": ");
			lblCopyDefaultProp.setText(Constants.SF_LBL_COPY_DEFAULT_PROP + ": ");
			lblCopyCD_Prop.setText(Constants.SF_LBL_COPY_CD_PROP + ": ");
			lblCopyID_Prop.setText(Constants.SF_LBL_COPY_ID_PROP + ": ");
			lblCopyCommandEditorProp.setText(Constants.SF_LBL_CPY_COMMAND_EDITOR_PROP + ": ");
			lblCopyCodeEditorProp.setText(Constants.SF_LBL_COPY_CLASS_EDITOR_PROP + ": ");
			lblCopyOutputEditProp.setText(Constants.SF_LBL_COPY_OUTPUT_EDITOR_PROP + ": ");
			lblCopyOutputFrameProp.setText(Constants.SF_LBL_COPY_OUTPUT_FRAME_PROP + ": ");
			lblCopyCodeEditorForInternalFrames.setText(Constants.SF_LBL_COPY_CODE_EDITOR_FOR_INTERNAL_FAMES + ": ");
			lblAnimationImage.setText(Constants.SF_LBL_ANIMATION_IMAGE + ": ");

			// Text do tlačítek:
            txtPlace = Constants.SF_BUTTONS_FOR_PLACES;

			lblResetApp.setText(Constants.SF_LBL_RESET_APP);

			btnRestoreDefault.setText(Constants.SF_BTN_RESTORE_DEFAULT);

			// Texty do dialogu pro umístění ckonfiguračních souborů:
			txtPlaceConfigurationDir = Constants.SF_APP_FC_CONFIGURATION_DIR;
			txtPlaceLanguageDir = Constants.SF_APP_FC_LANGUAGE_DIR;
			txtPlaceDefaultProp = Constants.SF_APP_FC_DEFAULT_PROP;
			txtPlaceClassDiagramProp = Constants.SF_APP_FC_CLASS_DIAGRAM_PROP;
			txtPlaceInstanceDiagramProp = Constants.SF_APP_FC_INSTANCE_DIAGRAM_PROP;
			txtPlaceCommandEditorProp = Constants.SF_APP_FC_COMMAND_EDITOR_PROP;
			txtPlaceClassEditorProp = Constants.SF_APP_FC_CLASS_EDITOR_PROP;
			txtPlaceOutputEditProp = Constants.SF_APP_FC_OUTPUT_EDITOR_PROP;
			txtPlaceOutputFrameProp = Constants.SF_APP_FC_OUTPUT_FRAME_PROP;
			txtPlaceCodeEditorForInternalFrames = Constants.SF_APP_FC_CODE_EDITOR_FOR_INTERNAL_FRAMES_PROP;
		}


        btnCopyConfigurationDir.setText(txtPlace);
        btnCopyLanguageDir.setText(txtPlace);
        btnCopyDefaultProp.setText(txtPlace);
        btnCopyCDProp.setText(txtPlace);
        btnCopyIDProp.setText(txtPlace);
        btnCopyCommandEditorProp.setText(txtPlace);
        btnCopyCodeEditorProp.setText(txtPlace);
        btnCopyOutputEditProp.setText(txtPlace);
        btnCopyOutputFrameProp.setText(txtPlace);
        btnCopyCodeEditorForInternalFrames.setText(txtPlace);





        pnlLookAndFeel.setLanguage(languageProperties);
		pnlLookAndFeel.setCmbModel();
		pnlLookAndFeel.setSelectedItemByIndex(indexOfLookAndFeel);



        userNamePanel.setLanguage(languageProperties);






        /*
         * Získám si model pro komponentu cmbKindOfSortingTextInProp.
         *
         * Toto získání a nastavení modelu pro označení způsobu řazení je třeba provést
         * zde, ne v metodě openSettings, protože si potřebuji načíst texty ve zvoleném
         * jazyce, tak když mám výžše získané texty, tak nyní mohu vložiz do komponenty
         * JComboBox dané texty ve zvoleném jazyce a označit položku:
         */
        final SortingInfo[] cmbKindOfSortingModel = LanguagePanel.getCmbKindOfSortingModel(sortingTextArray,
                sortingTtArray);

        languagePanel.setLanguage(languageProperties);

        // Vložím vytvořený model do cmb, aby si jej mohl uživatel vybrat sám:
        languagePanel.setModelForCmbKindOfSorting(new DefaultComboBoxModel<>(cmbKindOfSortingModel));

        // Nastavím označenou položku:
        languagePanel.setSelectedItemForSortingText(cmbKindOfSortingModel);
    }






	@Override
    public void actionPerformed(final ActionEvent e) {
        if (e.getSource() instanceof JButton) {
            if (e.getSource() == btnCopyConfigurationDir) {
                final String pathToDir = new FileChooser().getPathToPlaceSomeConfigurationDir(txtPlaceConfigurationDir);

                if (pathToDir != null)
                    // Zapíšu adresář configuration na zadané místu ouživatelem:
                    // a do tohoto adresáře dále vložím adresář language s jazyky a konfigurační soubory
                    // potřeb é pro aplikaci:
                    WriteToFile.createConfigureFilesInPath(pathToDir);
            }






			else if (e.getSource() == btnCopyLanguageDir) {
				final String pathToDir = new FileChooser().getPathToPlaceSomeConfigurationDir(txtPlaceLanguageDir);

				if (pathToDir != null)
					// vytvořím adresář language a zapíšu do nej soubory s texty v jazycích pro aplikaci:
					WriteToFile.writeLanguages(new File(pathToDir));
			}






			else if (e.getSource() == btnCopyDefaultProp) {
				final String pathToFile = new FileChooser().getPathToPlaceSomeConfigurationFile(txtPlaceDefaultProp);

				if (pathToFile != null) {
					acquireMutex();
					ConfigurationFiles.writeDefaultPropertiesAllNew(pathToFile);
					releaseMutex();
				}
			}






			else if (e.getSource() == btnCopyCDProp) {
				final String pathToFile = new FileChooser()
						.getPathToPlaceSomeConfigurationFile(txtPlaceClassDiagramProp);

				if (pathToFile != null) {
					acquireMutex();
					ConfigurationFiles.writeClassDiagramProperties(pathToFile);
					releaseMutex();
				}
			}





			else if (e.getSource() == btnCopyIDProp) {
				final String pathToFile = new FileChooser()
						.getPathToPlaceSomeConfigurationFile(txtPlaceInstanceDiagramProp);

				if (pathToFile != null) {
					acquireMutex();
					ConfigurationFiles.writeInstanceDiagramProperties(pathToFile);
					releaseMutex();
				}
			}





			else if (e.getSource() == btnCopyCommandEditorProp) {
				final String pathToFile = new FileChooser()
						.getPathToPlaceSomeConfigurationFile(txtPlaceCommandEditorProp);

				if (pathToFile != null) {
					acquireMutex();
					ConfigurationFiles.writeInstanceDiagramProperties(pathToFile);
					releaseMutex();
				}
			}





			else if (e.getSource() == btnCopyCodeEditorProp) {
				final String pathToFile = new FileChooser()
						.getPathToPlaceSomeConfigurationFile(txtPlaceClassEditorProp);

				if (pathToFile != null) {
					acquireMutex();
					ConfigurationFiles.writeCodeEditorProperties(pathToFile);
					releaseMutex();
				}
			}




			else if (e.getSource() == btnCopyOutputEditProp) {
				// Načtu si cestu od uživatele, kam se má zapsat příslušný soubor:
				final String pathToFile = new FileChooser().getPathToPlaceSomeConfigurationFile(txtPlaceOutputEditProp);

				// Otestuji, zda byla zadána cesta:
				if (pathToFile != null) {
					acquireMutex();
					// zapíšu příslušný soubor na zadané místo:
					ConfigurationFiles.writeOutputEditorProperties(pathToFile);
					releaseMutex();
				}
			}






			else if (e.getSource() == btnCopyOutputFrameProp) {
				final String pathToFile = new FileChooser()
						.getPathToPlaceSomeConfigurationFile(txtPlaceOutputFrameProp);

				if (pathToFile != null) {
					acquireMutex();
					ConfigurationFiles.writeOutputFrameProperties(pathToFile);

					releaseMutex();
				}
			}









			else if (e.getSource() == btnCopyCodeEditorForInternalFrames) {
				final String pathToFile = new FileChooser()
						.getPathToPlaceSomeConfigurationFile(txtPlaceCodeEditorForInternalFrames);

				if (pathToFile != null) {
					acquireMutex();
					ConfigurationFiles.writeCodeEditorPropertiesForInternalFrames(pathToFile);
					releaseMutex();
				}
			}







			else if (e.getSource() == btnRestoreDefault) {
				// Nastavím výchozí hodnoty z Consants:
				chcbAskForWorkspace.setSelected(Constants.DEFAULT_DISPLAY_WORKSPACE);

				// Zda se má po spuštění aplikace vyhledat adresář se soubory potřebnými pro
				// kompilaci tříd v aplikaci:
				chcbSearchCompileFiles.setSelected(Constants.SEARCH_COMPILE_FILES_AFTER_START_UP_APP);


				// Zda se má za účelem nalezení potřebných souborů pro kompilaci tříd v aplikaci
				// prohledat celý disk.
				chcbSearchEntireDiskForCompileFiles.setSelected(Constants.SEARCH_ENTIRE_DISK_FOR_COMPILE_FILES);


				chcbCaptureExceptions.setSelected(Constants.WRITE_NOT_CAPTURED_EXCEPTIONS_TO_OUTPUT_TERMINAL);


				/*
				 * Zjistím si index položky v modelu pro daný cmb cmbImages, která obsahuje
				 * výchozí obrázek a tento index nastavím jako označený.
				 *
				 * Note:
				 * Kdyby se nanašla shoda - nemělo by nastat, tak jako výchozí hodnotu vrátím
				 * hodnotu 5, což je hodnota, kde se nachází v modelu výchozí obrázek.
				 *
				 */
				final int index = IntStream.range(0, IMAGE_INFO_MODEL.size()).filter(
						i -> IMAGE_INFO_MODEL.get(i).getImageName().equals(Constants.DEFAULT_IMAGE_FOR_LOADING_PANEL))
						.findFirst().orElse(5);
				cmbImages.setSelectedIndex(index);

				// Označení výchozí hodnoty - Look and Feel:
				pnlLookAndFeel.restoreDefaultSettings();

				// Zavolám metodu, která nastaví výchozí hodnoty v panelu pro jazyky:
				languagePanel.restoreDefaultSettings();
			}
		}
	}






	@Override
	public void saveSetting() {
		EditProperties defaultPropertiesInWorkspace = App.READ_FILE
				.getPropertiesInWorkspacePersistComments(Constants.DEFAULT_PROPERTIES_NAME);

		if (defaultPropertiesInWorkspace == null) {
			final String pathToWorkspace = ReadFile.getPathToWorkspace();

			/*
			 * Zde pouze zjisšťuji, zda existují veškeré adresáře k cestě pro soubor
			 * Default.properties v adresáři configuration ve workspace. Pokud ne, pak v
			 * rámci možností nekteré adresáře mohu vytvořit, jinak není kam uložit soubory,
			 * pokud například byl smazan workspace apod. Pak už zbývá jedině znovu vytvořit
			 * adresář workspace s projekty apod. Ale tuto část zde vynechávám, protože o
			 * tom uživatel musí vědět - že si smazal projekty případně workspace apod. Tak
			 * snad musí vědět, že nelze pracovat s projekty / soubory, které byly smazány.
			 *
			 * Otestuji, zda existuje workspace, pak configuration, pak příslušný soubor,
			 * případně některé z nich mohu vytvořit:
			 */
			if (pathToWorkspace != null) {
				acquireMutex();

				/*
				 * Pokud neexsistuje adresář configuration ve workspace, bude vytvořen i s
				 * potřebnými soubory s výchozím nastavením.
				 *
				 * Pokud adresář configuration ve workspace existuje, pak stačí vytvořit jen
				 * soubor Default.properties, protože vím, že neexistuje, když se jej nepodařilo
				 * načíst.
				 */
				if (ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
					ConfigurationFiles.writeDefaultPropertiesAllNew(
							pathToWorkspace + File.separator + Constants.PATH_TO_DEFAULT_PROPERTIES);

				else
					WriteToFile.createConfigureFilesInPath(pathToWorkspace);

				// Znovu načtu soubor Default.properties z workspace z configuration, protože
				// výše byl znovu vytvořn:
				defaultPropertiesInWorkspace = App.READ_FILE
						.getPropertiesInWorkspacePersistComments(Constants.DEFAULT_PROPERTIES_NAME);

				releaseMutex();
			}
			/*
			 * část else:
			 *
			 * Zde by bylo možné vytvořit dříve zadaný adresář workspace - pokud byl již
			 * zadán, ale byl smazán, ale to by v rámci aplikace nemělo nastat, proto tuto
			 * část vynechám, uživatel jej musí smazat sám, pak o tom musí vědět.
			 */
		}






		if (defaultPropertiesInWorkspace != null) {
			defaultPropertiesInWorkspace.setProperty("Display WorkspaceChooser",
					String.valueOf(chcbAskForWorkspace.isSelected()));


			// Nastavení, zda se má při spuštění aplikace testovat existence adresáře s
			// potřebnými soubory pro kompilaci tříd pomocí aplikace:
			defaultPropertiesInWorkspace.setProperty("Search compile files",
					String.valueOf(chcbSearchCompileFiles.isSelected()));

			// Nastavení, zda se má prohledávat celý disk (pomocí rekurze), aby senašly
			// ptřebné soubory pro kompilaci tříd pomocí aplikace:
			defaultPropertiesInWorkspace.setProperty("Search whole disk for compile files",
					String.valueOf(chcbSearchEntireDiskForCompileFiles.isSelected()));


			defaultPropertiesInWorkspace.setProperty("Write not captured exceptions",
					String.valueOf(chcbCaptureExceptions.isSelected()));


			defaultPropertiesInWorkspace.setProperty("Language",
					languagePanel.getSelectedLanguageInfo().getLanguageId());


			defaultPropertiesInWorkspace.setProperty("Loading Image",
					((ImageInfo) cmbImages.getSelectedItem()).getImageName());


            // Nastavení Look and Feel:
            defaultPropertiesInWorkspace.setProperty("LookAndFeel",
                    pnlLookAndFeel.getSelectedItem().getIndexOfLookAndFeel());


			// Výchozí hodnota pro řazení textů v souborech .properties
			defaultPropertiesInWorkspace.setProperty("Default keys order",
					languagePanel.getSelectedKindOfSorting().getKeysOrder().getValueForFile());

			// Výchozí hodnota prot to, zda se mají vkládat komentář do souboru properties s texty pro aplikaci:
			defaultPropertiesInWorkspace.setProperty("Add comments",
					String.valueOf(languagePanel.isSelectedChcbAddComments()));

			// Výchozí počet volných řádků před komentářem:
			defaultPropertiesInWorkspace.setProperty("Count of free lines before comment",
					String.valueOf(languagePanel.getSelectedCountOfFreeLinesBeforeComment()));

			defaultPropertiesInWorkspace.setProperty("Properties locale",
					languagePanel.getSelectedLocale().toLanguageTag());


			// Zapíšu ho zpět:
			defaultPropertiesInWorkspace.save();

		}
		// Zde ho ani vytvářet nebudu, prostě nic neuložím - žádné nastavení - když není kam
	}


    @Override
    public void openSettings() {
        Properties defaultPropertiesInWorkspace = ReadFile.getDefaultPropertiesInWorkspace();

        /*
         * Zjistím si cestuk adresáři označeným uživatelem jako workspace.
         */
        final String pathToWorkspace = ReadFile.getPathToWorkspace();

        if (defaultPropertiesInWorkspace == null) {
            /*
             * Zde pouze zjisšťuji, zda existují veškeré adresáře k cestě pro soubor
             * Default.properties v adresáři configuration ve workspace. Pokud ne, pak v
             * rámci možností nekteré adresáře mohu vytvořit, jinak není kam uložit soubory,
             * pokud například byl smazan workspace apod. Pak už zbývá jedině znovu vytvořit
             * adresář workspace s projekty apod. Ale tuto část zde vynechávám, protože o
             * tom uživatel musí vědět - že si smazal projekty případně workspace apod. Tak
             * snad musí vědět, že nelze pracovat s projekty / soubory, které byly smazány.
             *
             * Otestuji, zda existuje workspace, pak configuration, pak příslušný soubor,
             * případně některé z nich mohu vytvořit:
             */
            if (pathToWorkspace != null) {
                acquireMutex();

                /*
                 * Pokud neexsistuje adresář configuration ve workspace, bude vytvořen i s
                 * potřebnými soubory s výchozím nastavením.
                 *
                 * Pokud adresář configuration ve workspace existuje, pak stačí vytvořit jen
                 * soubor Default.properties, protože vím, že neexistuje, když se jej nepodařilo
                 * načíst.
                 */
                if (ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
                    ConfigurationFiles.writeDefaultPropertiesAllNew(
                            pathToWorkspace + File.separator + Constants.PATH_TO_DEFAULT_PROPERTIES);

                else
                    WriteToFile.createConfigureFilesInPath(pathToWorkspace);

                // Znovu načtu soubor Default.properties z workspace z configuration, protože
                // výše byl znovu vytvořn:
                defaultPropertiesInWorkspace = ReadFile.getDefaultPropertiesInWorkspace();

                releaseMutex();
            }
            /*
             * část else:
             *
             * Zde by bylo možné vytvořit dříve zadaný adresář workspace - pokud byl již
             * zadán, ale byl smazán, ale to by v rámci aplikace nemělo nastat, proto tuto
             * část vynechám, uživatel jej musí smazat sám, pak o tom musí vědět.
             */
        }






        /*
         * Zde jeětě otestuji, zda existuje adresář language (s jazyky), případně jej
         * vytvořím.
         *
         * Note:
         * Zde bych ještě mohl testovat například, zda existují všechny aplkací
         * podporované jazyky apod. Ale to už mi přijde trochu zbytečné, pokud bude
         * uživatel například chtít vytvořit všechny soubory znovu apod. Tak je stačí
         * smazat z adresáře configuration a při otevření dialogu Nastavení se znovu
         * vytvoří s výchozžím nastavením, i kdyby se smazal jen jeden soubor nebo
         * adresář language, tak se znovu vytvoří.
         */
        if (pathToWorkspace != null && !ReadFile.existsDirectory(pathToWorkspace + File.separator
                + Constants.CONFIGURATION_NAME + File.separator + Constants.LANGUAGE_NAME))
            WriteToFile.writeLanguages(new File(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME));


        if (defaultPropertiesInWorkspace != null) {
            final boolean askForWorkspace = Boolean
                    .parseBoolean(defaultPropertiesInWorkspace.getProperty("Display WorkspaceChooser",
                            String.valueOf(Constants.DEFAULT_DISPLAY_WORKSPACE)));

            chcbAskForWorkspace.setSelected(askForWorkspace);


            // Nastavení, zda se má při spuštění aplikace testovat existence adresáře s
            // potřebnými soubory pro kompilaci tříd pomocí aplikace:
            chcbSearchCompileFiles
                    .setSelected(Boolean.parseBoolean(defaultPropertiesInWorkspace.getProperty("Search compile files",
                            String.valueOf(Constants.SEARCH_COMPILE_FILES_AFTER_START_UP_APP))));


            // Nastavení, zda se má prohledávat celý disk (pomocí rekurze), aby senašly
            // ptřebné soubory pro kompilaci tříd pomocí aplikace:
            chcbSearchEntireDiskForCompileFiles.setSelected(
                    Boolean.parseBoolean(defaultPropertiesInWorkspace.getProperty("Search whole disk for compile files",
                            String.valueOf(Constants.SEARCH_ENTIRE_DISK_FOR_COMPILE_FILES))));


            chcbCaptureExceptions.setSelected(
                    Boolean.parseBoolean(defaultPropertiesInWorkspace.getProperty("Write not captured exceptions",
                            String.valueOf(Constants.WRITE_NOT_CAPTURED_EXCEPTIONS_TO_OUTPUT_TERMINAL))));


            // Obrázek do loagingDialogu:
            final String image = defaultPropertiesInWorkspace.getProperty("Loading Image",
                    Constants.DEFAULT_IMAGE_FOR_LOADING_PANEL);

            for (final ImageInfo i : IMAGE_INFO_MODEL)
                if (i.getImageName().equals(image)) {
                    cmbImages.setSelectedItem(i);
                    break;
                }


            // Načtení Look and Feel:
            indexOfLookAndFeel = defaultPropertiesInWorkspace.getProperty("LookAndFeel",
                    Constants.DEFAULT_LOOK_AND_FEEL);


            // Nastavení jazyka:
            final String language = defaultPropertiesInWorkspace.getProperty("Language",
                    Constants.DEFAULT_LANGUAGE_NAME);

            languagePanel.setLanguageById(language);





            /*
             * Nyní si načtu logickou hodnotu pro to, zda se má nastavit jako označený
             * JCeckBox pro přidávání komentářů do souborů .properties s texty pro aplikaci
             * nebo ne. Dle této hodnoty se dále zpřístupní nebo znepřístupní panel s
             * komponentami pro nastavení počtu volných řádků.
             */
            final boolean addComments = Boolean.parseBoolean(defaultPropertiesInWorkspace.getProperty("Add comments",
                    String.valueOf(Constants.DEFAULT_VALUE_ADD_COMMENTS)));
            languagePanel.setSelectedItemForAddComment(addComments);


            /*
             * Nyní se označí položky pro cmb pro počet volných řádku kolem komentářů:
             */
            // Nastavím modely pro cmbs:
            languagePanel.setModelToCmbForFreeLines();

            // Nastavím označené zvolené položky v cmbs pro volné řádky:
            languagePanel.setSelectedItemToCmbForFreeLines(defaultPropertiesInWorkspace);





            /*
             * Získám si hodnotu pro Locale pro formátování datumu vytvoření souborů
             * .properties s texty pro tuto apliakci.
             *
             * Pokud nastane při vytváření instance třídy MyLocale vyjímka, znamená to, že
             * načtený Locale ze souboru - zadaný index pro Locale se nenachází v seznamu
             * dostupných Locale pro aktuální JVM. Proto není v cmbLocale v LanguagePanelu
             * co označit, ta se zkusí najít výchozí hodnota pro Locale dle hodnoty v
             * Constants, pokud se ani to nenajde, žádná položka se v cmbLocale nastavovat
             * nebude.
             *
             * Note:
             * Bylo by dobré vypsat uživateli nějaké oznámení, pokud nebude nalezen zadaný
             * Locale, ale v souborech budou zadány možnosti, a uživatel může vlastí
             * požadavek nastavit v Nastavení aplikace, pokud to nějak přepíše s chybou, měl
             * by o tom vědět, ale tuto část psát nebudu, prostě se jen nic nenastaví a
             * pokud uživatel klikne na OK -> pro opětovné uložení, pak se nastaví validní
             * hodnota.
             */
            try {
                /*
                 * Načtu si hodnotu / index Locale z objektu Properties a zkusím vytvořit
                 * instanci MyLocale, pokud dojde k výjimce, pak je zadán buď chyný index Locale
                 * nebo ten, který není k dispozici v aktuálním JVM a provede se obsahluha
                 * výjimky, kde se zkusí nastavit výchozí Locale pro dané JVM, to by nemělo
                 * skončit chybou (když už k tomu dojde).
                 */
                final LocaleItem locale = new LocaleItem(defaultPropertiesInWorkspace.getProperty("Properties locale",
                        Constants.DEFAULT_LOCALE_FOR_PROPERTIES.toLanguageTag()));

                languagePanel.setSelectedCmbLocale(locale.getLocale());

            } catch (final WrongLocaleException e) {
                // Zde není k dispozici zadané globální umístění v souboru, tak zkusím nastavit výchozí pro JVM:
                languagePanel.setSelectedCmbLocale(Constants.DEFAULT_LOCALE_FOR_PROPERTIES);

                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při načítání / vytváření Locale nastaveného v souboru .properties. " +
                                    "Bude" +
                                    " nastaveno výchozí " +
                                    "Locale pro JVM.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
            }
        }
    }
}