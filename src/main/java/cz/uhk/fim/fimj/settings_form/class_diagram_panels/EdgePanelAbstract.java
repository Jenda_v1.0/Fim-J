package cz.uhk.fim.fimj.settings_form.class_diagram_panels;

import java.awt.Color;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.settings_form.PanelAbstract;

/**
 * Tato třída slouží k tomu, abych si zde deklaroval nějaké proměnné, které budou dále použity v panelech, v dialogu
 * nastavení v panelu pro class diagram a v tomto panelu budou panely pro nastravení paramaetrů - vlastnosti - designu
 * hran reprezentujících v grafu různé vlastnosti - dědíčnost, implementaci, asociace, komentář, ... tak abych si
 * nemusel proměnné pořd deklarovat v každé třídě, udělám to zde, a zmíněné panely od tud budou dědit
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class EdgePanelAbstract extends PanelAbstract {
	
	private static final long serialVersionUID = 1L;


	
	// Výchozí hodnoty pro výběr style a konce hrany:
	static final List<String> ARRAY_OF_EDGE_LINE_STYLE = Collections
			.unmodifiableList(Arrays.asList("Bezier", "Orthogonal", "Spline", "Normal"));

	protected static final List<String> ARRAY_OF_EDGE_LINE_END_START = Collections.unmodifiableList(
			Arrays.asList("Circle", "Classic", "Diamond", "DoubleLine", "Line", "None", "Simple", "Technical"));

	protected static final List<String> ARRAY_OF_FONT_STYLE = Collections
			.unmodifiableList(Arrays.asList("Bold", "Italic", "Plain"));
	
	
	// Proměnná pro barvu hrany
	protected Color clrLineColor, clrTextColor;
	
	
	// Promenná, do které vložím text k popisku dialogu pro výběr barvy hrany a titulek ohraničení pro vypis do chybové hlasky v classDiagramPanelu:
	protected String txtLineColorChooser, txtTextBorderTitleToJop, txtTextColorChooser;
	
	
	// Komponenty pro výběr parametrů hrany, dále popisky a tlačítka pro různé vlastnosti
	protected JCheckBox chcbLabelAlongEdge, chcbLineBeginFill, chcbLineEndFill;
	
	
	protected JLabel lblLineColor, lblLineStyle, lblLineEndStyle, lblLineBeginStyle, lblLineWidth, lblChoosedLineColor,
			lblFontSize, lblTextColor, lblChoosedTextColor, lblFontStyle;
	
	
	
	protected JComboBox<String> cmbLineEnd, cmbLineStart, cmbLineStyle, cmbFontStyle;
	protected JComboBox<Float> cmbLineWidth;
	protected JComboBox<Integer> cmbFontSize;
	
	
	protected JButton btnChooseLineColor, btnChooseTextColor;
	
	
	
	

	
	
	/**
	 * Metoda, která vrátí jednorozměrné pole s hodnotami 8 - 150 = rozhsa čísel -
	 * velikostí písma, kterých může buňka nabývat
	 * 
	 * U nastavování velikostí
	 * 
	 * @return jednorozměrné pole čísel typu Integer s hodnotami 8 . 150
	 */
	protected static Integer[] getArrayFontSizes() {
		final Integer[] arrayFontSizes = new Integer[143];
		
		for (int i = Constants.FONT_SIZE_MIN; i <= Constants.FONT_SIZE_MAX; i++)
			arrayFontSizes[i - 8] = i;
		
		return arrayFontSizes;
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří jednorozměrné pole s hodnotami typu float v rozsahu 0.1
	 * - 20.0
	 * 
	 * @return výše zmíněné pole
	 */
	protected static Float[] getWidthsOfLine() {
		final Float[] arrayOfWidthsOfLine = new Float[200];
		
		int index = 0;
		
		for (int i = 0; i <= 20; i++) {
			for (int j = 0; j <= 9; j++) {
				if (!(i == 0 && j == 0)) {
					arrayOfWidthsOfLine[index] = Float.parseFloat(i + "." + j);		
					
					index++;
				}
				
				// Zde jsem dosáh hodnoty, kterou chci - 20.0, dále 20.1 .. už nechci pokračovat:
				if (i == 20 && j == 0)
					break;
			}
		}
		
		return arrayOfWidthsOfLine;
	}
	
	
	
	
	

	
	
	
	
	// Přístupové metody pro získání hodnot jednotlivých komponent pro nastavení 
	// vlastností - parametrů hran, abych mohl otestovat, zda se libovolné dvě hrany
	// - jejich parametry neshodují s jinou hranou:
	public final boolean isChcbTextAlongEdgeSelected() {
		return chcbLabelAlongEdge.isSelected();
	}
	
	public final boolean isChcbLineBeginFillSelected() {
		return chcbLineBeginFill.isSelected();
	}
	
	public final boolean isChcbLineEndFillSelected() {
		return chcbLineEndFill.isSelected();
	}
	
	public final int getRgbOfLineColor() {
		return clrLineColor.getRGB();
	}
	
	public final String getCmbLineStylSelectedItem() {
		return (String) cmbLineStyle.getSelectedItem();
	}
	
	public final String getCmbLineStartSelectedItem() {
		return (String) cmbLineStart.getSelectedItem();
	}
	
	public final String getCmbLineEndSelectedItem() {
		return (String) cmbLineEnd.getSelectedItem();
	}
	
	public final float getCmbLineWidthSelectedItem() {
		return (Float) cmbLineWidth.getSelectedItem();
	}
	
	public final String getTxtTextBorderTitleToJop() {
		return txtTextBorderTitleToJop;
	}
	
	public final int getRgbOfTextColor() {
		return clrTextColor.getRGB();
	}
	
	public final int getFontSizeSelectedItem() {
		return (int) cmbFontSize.getSelectedItem();
	}
	
	public final String getFontStyleSelectedItem() {
		return (String) cmbFontStyle.getSelectedItem();
	}
	
	
	
	
	
	
	// Metody pro nastaveni hodnot komponent:
	public final void setChcbLableAlongEdge(final boolean selected) {
		chcbLabelAlongEdge.setSelected(selected);
	}
	
	public final void setChcbLineBeginFillSelected(final boolean selected) {
		chcbLineBeginFill.setSelected(selected);
	}
	
	public final void setChcbLIneEndFillSelected(final boolean selected) {
		chcbLineEndFill.setSelected(selected);
	}
	
	public final void setLineColor(final Color color) {
		clrLineColor = color;
		lblChoosedLineColor.setForeground(clrLineColor);
		lblChoosedLineColor.setBackground(clrLineColor);
	}
	
	public final void setLineStyle(final int lineStyle) {
		if (lineStyle == -1)
			cmbLineStyle.setSelectedIndex(3);
		else if (lineStyle == 11)
			cmbLineStyle.setSelectedIndex(1);
		else if (lineStyle == 12)
			cmbLineStyle.setSelectedIndex(0);
		else if (lineStyle == 13)
			cmbLineStyle.setSelectedIndex(2);
	}
	
	public final void setLineBegin(final int lineBegin) {
		if (lineBegin == 0)
			cmbLineStart.setSelectedIndex(5);
		else if (lineBegin == 1)
			cmbLineStart.setSelectedIndex(1);
		else if (lineBegin == 2)
			cmbLineStart.setSelectedIndex(7);
		else if (lineBegin == 4)
			cmbLineStart.setSelectedIndex(6);
		else if (lineBegin == 5)
			cmbLineStart.setSelectedIndex(0);
		else if (lineBegin == 7)
			cmbLineStart.setSelectedIndex(4);
		else if (lineBegin == 8)
			cmbLineStart.setSelectedIndex(3);
		else if (lineBegin == 9)
			cmbLineStart.setSelectedIndex(2);
	}
	
	public final void setLineEnd(final int lineEnd) {
		if (lineEnd == 0)
			cmbLineEnd.setSelectedIndex(5);
		else if (lineEnd == 1)
			cmbLineEnd.setSelectedIndex(1);
		else if (lineEnd == 2)
			cmbLineEnd.setSelectedIndex(7);
		else if (lineEnd == 4)
			cmbLineEnd.setSelectedIndex(6);
		else if (lineEnd == 5)
			cmbLineEnd.setSelectedIndex(0);
		else if (lineEnd == 7)
			cmbLineEnd.setSelectedIndex(4);
		else if (lineEnd == 8)
			cmbLineEnd.setSelectedIndex(3);
		else if (lineEnd == 9)
			cmbLineEnd.setSelectedIndex(2);
	}
	
	public final void setLineWidth(final float lineWidth) {
		cmbLineWidth.setSelectedItem(lineWidth);
	}
	
	public final void setFontColor(final Color color) {
		clrTextColor = color;
		lblChoosedTextColor.setForeground(clrTextColor);
		lblChoosedTextColor.setBackground(clrTextColor);
	}
	
	public final void setFontSize(final int fontSize) {
		cmbFontSize.setSelectedItem(fontSize);
	}
	
	public final void setFontStyle(final int fontStyle) {
		if (fontStyle == 0)		// Plain
			cmbFontStyle.setSelectedIndex(2);
		else if (fontStyle == 1)	// Bold
			cmbFontStyle.setSelectedIndex(0);
		else if (fontStyle == 2)	// Italic
			cmbFontStyle.setSelectedIndex(1);
	}
}