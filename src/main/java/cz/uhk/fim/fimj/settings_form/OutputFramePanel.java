package cz.uhk.fim.fimj.settings_form;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.ConfigurationFiles;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.file.WriteToFile;
import cz.uhk.fim.fimj.language.EditProperties;

/**
 * Třída slouží jako panel, který reprezentuje nastavení pro tzv. komponentu Výstupní terminál, resp. okno
 * OutputFrame.java, kde se zobrazuje text jako do výstupní konzole - výstupy z aplikace pomocí Sysout.out nebo
 * System.err, ...
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class OutputFramePanel extends EditorsPanelAbstract
		implements ActionListener, LanguageInterface, SaveSettingsInterface {

	private static final long serialVersionUID = 1L;
	
	
	
	/**
	 * Tlačítko, které slouží pro obnovení výchozího nastavení pro komponentu coby
	 * Výstupní terminál
	 */
	private final JButton btnRestoreDefaultSettings;
	


	// Zde se nachází hodnoty pro nastavení vlastností textového pole pro výpisy System.err:
	private final JLabel lblBgColorSe, lblChoosedBgColorSe, lblTextColorSe, lblChoosedTextColorSe, lblFontSizeSe, lblFontStyleSe;


	private final JButton btnChooseBgColorSe, btnChooseTextColorSe;


	private final JComboBox<Integer> cmbFontSizeSe;
	private final JComboBox<String> cmbFontStyleSe;


    private Color bgColorSe, textColorSe;


	
	/**
	 * Konstruktor této třídy.
	 */
	public OutputFramePanel() {
		super();
		
		setLayout(new GridBagLayout());
		
		// Rozložení pro celý panel:
		final GridBagConstraints gbc = getGbc();

		// Rozložení pro panel pro výpisy z System.out:
        final GridBagConstraints gbcSysOut = getGbc();

        // Rozložení pro panel pro výpisy z System.err:
        final GridBagConstraints gbcSysErr = getGbc();


        // Rozložení pro celý panel:
		index = 0;

		int indexSysOut = 0;
		int indexSysErr = 0;


		// Panel pro komponenty pro nastavení textového pole pro výpisy z System.out:
		final JPanel pnlSysOut = new JPanel(new GridBagLayout());
		pnlSysOut.setBorder(BorderFactory.createTitledBorder("System.out"));

        // Panel pro komponenty pro nastavení textového pole pro výpisy z System.err:
		final JPanel pnlSysErr = new JPanel(new GridBagLayout());
        pnlSysErr.setBorder(BorderFactory.createTitledBorder("System.err"));

		
		
		
		// Komponenty do okna resp panelu:

        // System.out:
		lblBgColor = new JLabel();
		setGbc(gbcSysOut, indexSysOut, 0, lblBgColor, pnlSysOut);
		
		btnChooseBgColor = new JButton();
		btnChooseBgColor.addActionListener(this);		
		setGbc(gbcSysOut, indexSysOut, 1, btnChooseBgColor, pnlSysOut);
		
		lblChoosedBgColor = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChoosedBgColor.setOpaque(true);
		setGbc(gbcSysOut, indexSysOut, 2, lblChoosedBgColor, pnlSysOut);
		
		
		
		lblTextColor = new JLabel();
		setGbc(gbcSysOut, ++indexSysOut, 0, lblTextColor, pnlSysOut);
		
		btnChooseTextColor = new JButton();
		btnChooseTextColor.addActionListener(this);
		setGbc(gbcSysOut, indexSysOut, 1, btnChooseTextColor, pnlSysOut);
		
		lblChoosedTextColor = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChoosedTextColor.setOpaque(true);
		setGbc(gbcSysOut, indexSysOut, 2, lblChoosedTextColor, pnlSysOut);
		
		
		
		
		
		lblFontSize = new JLabel();
		setGbc(gbcSysOut, ++indexSysOut, 0, lblFontSize, pnlSysOut);
		
		cmbFontSize = new JComboBox<>(getFontSizes());
		setGbc(gbcSysOut, indexSysOut, 1, cmbFontSize, pnlSysOut);
		
		
		
		
		lblFontStyle = new JLabel();
		setGbc(gbcSysOut, ++indexSysOut, 0, lblFontStyle, pnlSysOut);
		
		cmbFontStyle = new JComboBox<>(ARRAY_OF_FONT_STYLE.toArray(new String[] {}));
		setGbc(gbcSysOut, indexSysOut, 1, cmbFontStyle, pnlSysOut);
		






		// System.err:
        lblBgColorSe = new JLabel();
        setGbc(gbcSysErr, ++indexSysErr, 0, lblBgColorSe, pnlSysErr);

        btnChooseBgColorSe = new JButton();
        btnChooseBgColorSe.addActionListener(this);
        setGbc(gbcSysErr, indexSysErr, 1, btnChooseBgColorSe, pnlSysErr);

        lblChoosedBgColorSe = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
        lblChoosedBgColorSe.setOpaque(true);
        setGbc(gbcSysErr, indexSysErr, 2, lblChoosedBgColorSe, pnlSysErr);



        lblTextColorSe = new JLabel();
        setGbc(gbcSysErr, ++indexSysErr, 0, lblTextColorSe, pnlSysErr);

        btnChooseTextColorSe = new JButton();
        btnChooseTextColorSe.addActionListener(this);
        setGbc(gbcSysErr, indexSysErr, 1, btnChooseTextColorSe, pnlSysErr);

        lblChoosedTextColorSe = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
        lblChoosedTextColorSe.setOpaque(true);
        setGbc(gbcSysErr, indexSysErr, 2, lblChoosedTextColorSe, pnlSysErr);





        lblFontSizeSe = new JLabel();
        setGbc(gbcSysErr, ++indexSysErr, 0, lblFontSizeSe, pnlSysErr);

        cmbFontSizeSe = new JComboBox<>(getFontSizes());
        setGbc(gbcSysErr, indexSysErr, 1, cmbFontSizeSe, pnlSysErr);




        lblFontStyleSe = new JLabel();
        setGbc(gbcSysErr, ++indexSysErr, 0, lblFontStyleSe, pnlSysErr);

        cmbFontStyleSe = new JComboBox<>(ARRAY_OF_FONT_STYLE.toArray(new String[] {}));
        setGbc(gbcSysErr, indexSysErr, 1, cmbFontStyleSe, pnlSysErr);







        // Přidání panelů s komponentami pro nastavení textových polí v okně výstupního terminálu pro výpisy:
        setGbc(gbc, index, 0, pnlSysOut, this);
        setGbc(gbc, ++index, 0, pnlSysErr, this);






		
		gbc.gridwidth = 3;
		
		final JPanel pnlButtonDefaultSettins = new JPanel(new FlowLayout(FlowLayout.CENTER));
		btnRestoreDefaultSettings = new JButton();
		btnRestoreDefaultSettings.addActionListener(this);
		pnlButtonDefaultSettins.add(btnRestoreDefaultSettings);
		
		setGbc(gbc, ++index, 0, pnlButtonDefaultSettins, this);
		
		
		
		openSettings();
	}

	
	
	
	
	
	
	@Override
	public void saveSetting() {
		EditProperties outputFrameProp = App.READ_FILE
				.getPropertiesInWorkspacePersistComments(Constants.OUTPUT_FRAME_NAME);

		if (outputFrameProp == null) {
			final String pathToWorkspace = ReadFile.getPathToWorkspace();

			/*
			 * Zde pouze zjisšťuji, zda existují veškeré adresáře k cestě pro soubor
			 * OutputFrame.properties v adresáři configuration ve workspace. Pokud ne, pak v
			 * rámci možností nekteré adresáře mohu vytvořit, jinak není kam uložit soubory,
			 * pokud například byl smazan workspace apod. Pak už zbývá jedině znovu vytvořit
			 * adresář workspace s projekty apod. Ale tuto část zde vynechávám, protože o
			 * tom uživatel musí vědět - že si smazal projekty případně workspace apod. Tak
			 * snad musí vědět, že nelze pracovat s projekty / soubory, které byly smazány.
			 * 
			 * Otestuji, zda existuje workspace, pak configuration, pak příslušný soubor,
			 * případně některé z nich mohu vytvořit:
			 */
			if (pathToWorkspace != null) {
				acquireMutex();
				
				/*
				 * Pokud neexsistuje adresář configuration ve workspace, bude vytvořen i s
				 * potřebnými soubory s výchozím nastavením.
				 * 
				 * Pokud adresář configuration ve workspace existuje, pak stačí vytvořit jen
				 * soubor OutputFrame.properties, protože vím, že neexistuje, když se jej
				 * nepodařilo načíst.
				 */
				if (ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
					ConfigurationFiles.writeOutputFrameProperties(
							pathToWorkspace + File.separator + Constants.PATH_TO_OUTPUT_FRAME_PROPERTIES);

				
				else
					WriteToFile.createConfigureFilesInPath(pathToWorkspace);

				
				// Znovu načtu soubor Default.properties z workspace z configuration, protože
				// výše byl znovu vytvořn:
				outputFrameProp = App.READ_FILE.getPropertiesInWorkspacePersistComments(Constants.OUTPUT_FRAME_NAME);
				
				releaseMutex();
			}
			/*
			 * část else:
			 * 
			 * Zde by bylo možné vytvořit dříve zadaný adresář workspace - pokud byl již
			 * zadán, ale byl smazán, ale to by v rámci aplikace nemělo nastat, proto tuto
			 * část vynechám, uživatel jej musí smazat sám, pak o tom musí vědět.
			 */
		}
		
		
		
		
		
		
		
		if (outputFrameProp != null) {
			// Zde se podařilo načíst soubour, tak do něj vložím upravená data a uložím ho zpět:			

            // System.out:

			// Barva pozadí:
			outputFrameProp.setProperty("SO_BackgroundColor", Integer.toString(bgColor.getRGB()));
			
			// Barva písma:
			outputFrameProp.setProperty("SO_ForegroundColor", Integer.toString(textColor.getRGB()));
			
			// Velikost písma:
			outputFrameProp.setProperty("SO_FontSize", Integer.toString((Integer) cmbFontSize.getSelectedItem()));
			
			// Typ písma:
			final int fontStyle = cmbFontStyle.getSelectedIndex();
			
			if (fontStyle == 0)
				outputFrameProp.setProperty("SO_FontStyle", Integer.toString(1));
			else if (fontStyle == 1)
				outputFrameProp.setProperty("SO_FontStyle", Integer.toString(2));
			else if (fontStyle == 2)
				outputFrameProp.setProperty("SO_FontStyle", Integer.toString(0));



			//System.err:

            // Barva pozadí:
            outputFrameProp.setProperty("SE_BackgroundColor", Integer.toString(bgColorSe.getRGB()));

            // Barva písma:
            outputFrameProp.setProperty("SE_ForegroundColor", Integer.toString(textColorSe.getRGB()));

            // Velikost písma:
            outputFrameProp.setProperty("SE_FontSize", Integer.toString((Integer) cmbFontSizeSe.getSelectedItem()));

            // Typ písma:
            final int fontStyleSe = cmbFontStyleSe.getSelectedIndex();

            if (fontStyleSe == 0)
                outputFrameProp.setProperty("SE_FontStyle", Integer.toString(1));
            else if (fontStyleSe == 1)
                outputFrameProp.setProperty("SE_FontStyle", Integer.toString(2));
            else if (fontStyleSe == 2)
                outputFrameProp.setProperty("SE_FontStyle", Integer.toString(0));


            // Zápis zpět do soubour:
			outputFrameProp.save();
		}
	}
	
	

	
	

	@Override
	public void openSettings() {
		Properties outputFrameProp = App.READ_FILE.getOutputFramePropertiesFromWorkspaceOnly();

		if (outputFrameProp == null) {
			final String pathToWorkspace = ReadFile.getPathToWorkspace();

			/*
			 * Zde pouze zjisšťuji, zda existují veškeré adresáře k cestě pro soubor
			 * OutputFrame.properties v adresáři configuration ve workspace. Pokud ne, pak v
			 * rámci možností nekteré adresáře mohu vytvořit, jinak není kam uložit soubory,
			 * pokud například byl smazan workspace apod. Pak už zbývá jedině znovu vytvořit
			 * adresář workspace s projekty apod. Ale tuto část zde vynechávám, protože o
			 * tom uživatel musí vědět - že si smazal projekty případně workspace apod. Tak
			 * snad musí vědět, že nelze pracovat s projekty / soubory, které byly smazány.
			 * 
			 * Otestuji, zda existuje workspace, pak configuration, pak příslušný soubor,
			 * případně některé z nich mohu vytvořit:
			 */
			if (pathToWorkspace != null) {
				acquireMutex();
				
				/*
				 * Pokud neexsistuje adresář configuration ve workspace, bude vytvořen i s
				 * potřebnými soubory s výchozím nastavením.
				 * 
				 * Pokud adresář configuration ve workspace existuje, pak stačí vytvořit jen
				 * soubor OutputFrame.properties, protože vím, že neexistuje, když se jej nepodařilo
				 * načíst.
				 */
				if (ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
					ConfigurationFiles.writeOutputFrameProperties(
							pathToWorkspace + File.separator + Constants.PATH_TO_OUTPUT_FRAME_PROPERTIES);

				else
					WriteToFile.createConfigureFilesInPath(pathToWorkspace);

				// Znovu načtu soubor Default.properties z workspace z configuration, protože
				// výše byl znovu vytvořn:
				outputFrameProp = App.READ_FILE.getOutputFramePropertiesFromWorkspaceOnly();
				
				releaseMutex();
			}
			/*
			 * část else:
			 * 
			 * Zde by bylo možné vytvořit dříve zadaný adresář workspace - pokud byl již
			 * zadán, ale byl smazán, ale to by v rámci aplikace nemělo nastat, proto tuto
			 * část vynechám, uživatel jej musí smazat sám, pak o tom musí vědět.
			 */
		}
		
		
		
		
		
		
		
		if (outputFrameProp != null) {
			// Zde se podařilo soubor načíst, do vyplním komponenty v okně příslušnými daty:

            // System.out:
			final String backgroundColor = outputFrameProp.getProperty("SO_BackgroundColor", Integer.toString(Constants.DEFAULT_BACKGROUND_COLOR_FOR_SO_OUTPUT_FRAME.getRGB()));
			bgColor = new Color(Integer.parseInt(backgroundColor));
			lblChoosedBgColor.setForeground(bgColor);
			lblChoosedBgColor.setBackground(bgColor);
			
			
			final String textColorText = outputFrameProp.getProperty("SO_ForegroundColor", Integer.toString(Constants.DEFAULT_TEXT_COLOR_FOR_SO_OUTPUT_FRAME.getRGB()));
			textColor = new Color(Integer.parseInt(textColorText));
			lblChoosedTextColor.setForeground(textColor);
			lblChoosedTextColor.setBackground(textColor);
			
			
			
			final int fontSizeFromFile = Integer.parseInt(outputFrameProp.getProperty("SO_FontSize", String.valueOf(Constants.DEFAULT_FONT_FOR_SO_OUTPUT_FRAME.getSize())));
			if (fontSizeFromFile >= 8 && fontSizeFromFile <= 100)
				cmbFontSize.setSelectedItem(fontSizeFromFile);
			
			
			
			final int fontStyle = Integer.parseInt(outputFrameProp.getProperty("SO_FontStyle", Integer.toString(Constants.DEFAULT_FONT_FOR_SO_OUTPUT_FRAME.getStyle())));
			if (fontStyle == 0)		// Plain
				cmbFontStyle.setSelectedIndex(2);
			else if (fontStyle == 1)	// Bold
				cmbFontStyle.setSelectedIndex(0);
			else if (fontStyle == 2)	// Italic
				cmbFontStyle.setSelectedIndex(1);



            // System.err:
            final String backgroundColorSe = outputFrameProp.getProperty("SE_BackgroundColor", Integer.toString(Constants.DEFAULT_BACKGROUND_COLOR_FOR_SE_OUTPUT_FRAME.getRGB()));
            bgColorSe = new Color(Integer.parseInt(backgroundColorSe));
            lblChoosedBgColorSe.setForeground(bgColorSe);
            lblChoosedBgColorSe.setBackground(bgColorSe);


            final String textColorTextSe = outputFrameProp.getProperty("SE_ForegroundColor", Integer.toString(Constants.DEFAULT_TEXT_COLOR_FOR_SE_OUTPUT_FRAME.getRGB()));
            textColorSe = new Color(Integer.parseInt(textColorTextSe));
            lblChoosedTextColorSe.setForeground(textColorSe);
            lblChoosedTextColorSe.setBackground(textColorSe);



            final int fontSizeFromFileSe = Integer.parseInt(outputFrameProp.getProperty("SE_FontSize", String.valueOf(Constants.DEFAULT_FONT_FOR_SE_OUTPUT_FRAME.getSize())));
            if (fontSizeFromFileSe >= 8 && fontSizeFromFileSe <= 100)
                cmbFontSizeSe.setSelectedItem(fontSizeFromFileSe);



            final int fontStyleSe = Integer.parseInt(outputFrameProp.getProperty("SE_FontStyle", Integer.toString(Constants.DEFAULT_FONT_FOR_SE_OUTPUT_FRAME.getStyle())));
            if (fontStyleSe == 0)		// Plain
                cmbFontStyleSe.setSelectedIndex(2);
            else if (fontStyleSe == 1)	// Bold
                cmbFontStyleSe.setSelectedIndex(0);
            else if (fontStyleSe == 2)	// Italic
                cmbFontStyleSe.setSelectedIndex(1);
		}
	}

	
	
	

	

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
		    //System.out:
			if (e.getSource() == btnChooseBgColor) {
				final Color tempVariable = JColorChooser.showDialog(this, txtChooseBgColorDialog,
						Constants.DEFAULT_BACKGROUND_COLOR_FOR_SO_OUTPUT_FRAME);
				
				if (tempVariable != null) {
					bgColor = tempVariable;
					lblChoosedBgColor.setForeground(bgColor);
					lblChoosedBgColor.setBackground(bgColor);
				}
			}
			
			
			else if (e.getSource() == btnChooseTextColor) {
				final Color tempVariable = JColorChooser.showDialog(this, txtChooseTextColorDialog,
						Constants.DEFAULT_TEXT_COLOR_FOR_SO_OUTPUT_FRAME);
				
				if (tempVariable != null) {
					textColor = tempVariable;
					lblChoosedTextColor.setForeground(textColor);
					lblChoosedTextColor.setBackground(textColor);
				}
			}
			






			// System.err:
            if (e.getSource() == btnChooseBgColorSe) {
                final Color tempVariable = JColorChooser.showDialog(this, txtChooseBgColorDialog,
                        Constants.DEFAULT_BACKGROUND_COLOR_FOR_SE_OUTPUT_FRAME);

                if (tempVariable != null) {
                    bgColorSe = tempVariable;
                    lblChoosedBgColorSe.setForeground(bgColorSe);
                    lblChoosedBgColorSe.setBackground(bgColorSe);
                }
            }


            else if (e.getSource() == btnChooseTextColorSe) {
                final Color tempVariable = JColorChooser.showDialog(this, txtChooseTextColorDialog,
                        Constants.DEFAULT_TEXT_COLOR_FOR_SE_OUTPUT_FRAME);

                if (tempVariable != null) {
                    textColorSe = tempVariable;
                    lblChoosedTextColorSe.setForeground(textColorSe);
                    lblChoosedTextColorSe.setBackground(textColorSe);
                }
            }







            else if (e.getSource() == btnRestoreDefaultSettings) {
                // System.out:
                bgColor = Constants.DEFAULT_BACKGROUND_COLOR_FOR_SO_OUTPUT_FRAME;
                lblChoosedBgColor.setForeground(bgColor);
                lblChoosedBgColor.setBackground(bgColor);

                textColor = Constants.DEFAULT_TEXT_COLOR_FOR_SO_OUTPUT_FRAME;
                lblChoosedTextColor.setBackground(textColor);
                lblChoosedTextColor.setForeground(textColor);


                cmbFontSize.setSelectedItem(Constants.DEFAULT_FONT_FOR_SO_OUTPUT_FRAME.getSize());

                final int fontStyle = Constants.DEFAULT_FONT_FOR_SO_OUTPUT_FRAME.getStyle();
                if (fontStyle == 0)		// Plain
                    cmbFontStyle.setSelectedIndex(2);
                else if (fontStyle == 1)	// Bold
                    cmbFontStyle.setSelectedIndex(0);
                else if (fontStyle == 2)	// Italic
                    cmbFontStyle.setSelectedIndex(1);


			    // System.err:
                bgColorSe = Constants.DEFAULT_BACKGROUND_COLOR_FOR_SE_OUTPUT_FRAME;
                lblChoosedBgColorSe.setForeground(bgColorSe);
                lblChoosedBgColorSe.setBackground(bgColorSe);

                textColorSe = Constants.DEFAULT_TEXT_COLOR_FOR_SE_OUTPUT_FRAME;
                lblChoosedTextColorSe.setBackground(textColorSe);
                lblChoosedTextColorSe.setForeground(textColorSe);


                cmbFontSizeSe.setSelectedItem(Constants.DEFAULT_FONT_FOR_SE_OUTPUT_FRAME.getSize());

                final int fontStyleSe = Constants.DEFAULT_FONT_FOR_SE_OUTPUT_FRAME.getStyle();
                if (fontStyleSe == 0)		// Plain
                    cmbFontStyleSe.setSelectedIndex(2);
                else if (fontStyleSe == 1)	// Bold
                    cmbFontStyleSe.setSelectedIndex(0);
                else if (fontStyleSe == 2)	// Italic
                    cmbFontStyleSe.setSelectedIndex(1);
            }
		}
	}

	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			setBorder(BorderFactory.createTitledBorder(properties.getProperty("Sf_Ofp_BorderTitle", Constants.SF_OFP_BORDER_TITLE)));

			// System.out:
			lblBgColor.setText(properties.getProperty("Sf_Ce_LabelBackgroundColor", Constants.SF_CE_LBL_BACKGROUND_COLOR) + ": ");
			lblTextColor.setText(properties.getProperty("Sf_Cd_LabelTextColorClassCell", Constants.SF_LBL_TEXT_COLOR_CELL) + ": ");
			lblFontSize.setText(properties.getProperty("Sf_Cd_LabelFontSizeClassCell", Constants.SF_LBL_FONT_SIZE_CELL) + ": ");
			lblFontStyle.setText(properties.getProperty("Sf_Cd_LabelFontStyleClassCell", Constants.SF_LBL_FONT_STYLE_CELL) + ": ");

			txtChooseTextColorDialog = properties.getProperty("Sf_Cd_ChooseTextColorDialog", Constants.SF_TXT_CHOOSE_TEXT_COLOR);
			txtChooseBgColorDialog = properties.getProperty("Sf_Cd_ChooseBackgroundColorDialog", Constants.SF_TXT_CHOOSE_BG_COLOR);

			btnChooseBgColor.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));
			btnChooseTextColor.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));



            // System.err:
            lblBgColorSe.setText(properties.getProperty("Sf_Ce_LabelBackgroundColor", Constants.SF_CE_LBL_BACKGROUND_COLOR) + ": ");
            lblTextColorSe.setText(properties.getProperty("Sf_Cd_LabelTextColorClassCell", Constants.SF_LBL_TEXT_COLOR_CELL) + ": ");
            lblFontSizeSe.setText(properties.getProperty("Sf_Cd_LabelFontSizeClassCell", Constants.SF_LBL_FONT_SIZE_CELL) + ": ");
            lblFontStyleSe.setText(properties.getProperty("Sf_Cd_LabelFontStyleClassCell", Constants.SF_LBL_FONT_STYLE_CELL) + ": ");

            btnChooseBgColorSe.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));
            btnChooseTextColorSe.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));

            btnRestoreDefaultSettings.setText(properties.getProperty("Sf_Cd_ButtonRestoreDefaultSettings", Constants.SF_BUTTONS_RESTORE_DEFAULT_SETTINGS));
		}
		
		
		else {
			setBorder(BorderFactory.createTitledBorder(Constants.SF_OFP_BORDER_TITLE));

			// System.out:
			lblBgColor.setText(Constants.SF_CE_LBL_BACKGROUND_COLOR + ": ");
			lblTextColor.setText(Constants.SF_LBL_TEXT_COLOR_CELL + ": ");
			lblFontSize.setText(Constants.SF_LBL_FONT_SIZE_CELL + ": ");
			lblFontStyle.setText(Constants.SF_LBL_FONT_STYLE_CELL + ": ");

			txtChooseTextColorDialog = Constants.SF_TXT_CHOOSE_TEXT_COLOR;
			txtChooseBgColorDialog =  Constants.SF_TXT_CHOOSE_BG_COLOR;

			btnChooseBgColor.setText(Constants.SF_CD_BTN_CHOOSE_COLOR);
			btnChooseTextColor.setText(Constants.SF_CD_BTN_CHOOSE_COLOR);

            // System.err:
            lblBgColorSe.setText(Constants.SF_CE_LBL_BACKGROUND_COLOR + ": ");
            lblTextColorSe.setText(Constants.SF_LBL_TEXT_COLOR_CELL + ": ");
            lblFontSizeSe.setText(Constants.SF_LBL_FONT_SIZE_CELL + ": ");
            lblFontStyleSe.setText(Constants.SF_LBL_FONT_STYLE_CELL + ": ");

            btnChooseBgColorSe.setText(Constants.SF_CD_BTN_CHOOSE_COLOR);
            btnChooseTextColorSe.setText(Constants.SF_CD_BTN_CHOOSE_COLOR);


            btnRestoreDefaultSettings.setText(Constants.SF_BUTTONS_RESTORE_DEFAULT_SETTINGS);
		}
	}
}