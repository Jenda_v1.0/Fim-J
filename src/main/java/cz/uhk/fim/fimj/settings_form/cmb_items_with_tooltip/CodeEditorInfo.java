package cz.uhk.fim.fimj.settings_form.cmb_items_with_tooltip;

import cz.uhk.fim.fimj.class_diagram.KindOfCodeEditor;

/**
 * Tato třída slouží jako položky v modlu komponenty JComboBox, která slouží pro výběr výchozího editoru zdrojového
 * kódu.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class CodeEditorInfo extends CmbItemAbstract {

    /**
     * Výčtová hodnota, která značí typ editoru zdrojového kódu, která rerezentuje tato položka.
     */
    private final KindOfCodeEditor kindOfCodeEditor;


    /**
     * Konstruktor této třídy.
     *
     * @param kindOfCodeEditor
     *         - výčtová hodnota, která reprezentuje konkrétní editor zdrojového kódu.
     * @param text
     *         - text, který bude zobrazen v komponentě typu JComboBox u této položky
     * @param toolTip
     *         - popisek této položky.
     */
    public CodeEditorInfo(final KindOfCodeEditor kindOfCodeEditor, final String text, final String toolTip) {
        super(text, toolTip);

        this.kindOfCodeEditor = kindOfCodeEditor;
    }


    /**
     * Getr na výčtovou hodnotu, která reprezentuj konkrétní editor kódu.
     *
     * @return výčtovou hodnotu, která značí konkrétní editor zdrojového kódu.
     */
    public KindOfCodeEditor getKindOfCodeEditor() {
        return kindOfCodeEditor;
    }
}
