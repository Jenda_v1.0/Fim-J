package cz.uhk.fim.fimj.settings_form.application_panels;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.UserNameSupport;
import cz.uhk.fim.fimj.settings_form.PanelAbstract;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Properties;

/**
 * Třída slouží jako panel, který obsahuje komponenty pro nastavení uživatelského jména. Jméno bude využito například
 * jako autor při vytváření nových tříd nebo nového projektu apod.
 *
 * <i>Zdroj pro textové pole s posuvníkem: http://www.java2s
 * .com/Tutorial/Java/0240__Swing/JTextFieldwithaJScrollBarforScrolling.htm</i>
 *
 * <i>Tento panel pro nastavní uživatelského jména má zvlášť možnost pro uložení jméně do konfiguračního souboru, tj.
 * uložení není vázáno na "celý" dialog. Kdyby tomu tak bylo, musely by se zvlášť řešit když jméno obsahuje chybu apod.
 * V takovém případě by se mohly například zapsat jen nějaké nastavení z jiných panelů nebo žádné. V takovém případě by
 * bylo třeba upřednostnit testování tohoto jména. Podobně pro případy, kdy by se doplnila další možnost nastavení
 * nějaké vlastnosti. Muselo by se zvážit, co se upředností s testováním. Proto bylo rozhodnuto, že se "tyto" části
 * nastavení budou řešit takto zvlášť pro uložení. Ne jako "celý" dialog nastavení.</i>
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 30.08.2018 10:23
 */

public class UserNamePanel extends PanelAbstract implements ActionListener, LanguageInterface, KeyListener {

    /**
     * Barva pozadí textového pole v případě, že obsahuje chybu.
     */
    private static final Color ERROR_TEXT_FIELD_COLOR = Color.RED;
    /**
     * Barva pozadí textového pole v případě, že je text validní.
     */
    private static final Color VALID_TEXT_FIELD_COLOR = Color.WHITE;

    /**
     * Label pro text, že se má zadat uživatelské jméno.
     */
    private final JLabel lblUserName;


    /**
     * Textové pole pro zadání uživatelského jména.
     */
    private final JTextField txtUserNameField;


    /**
     * Tlačítko, které bude zobrazeno při otevření dialogu nastavení. Kliknutím na toto tlačítko bude zpřístupněné
     * textové pole (v něm uživatelské jméno) editaci.
     */
    private final JButton btnEdit;
    /**
     * Tlačítko pro obnovení výchozího uživatelského jména, které je nastavenév OS (přihlášený uživatel).
     */
    private final JButton btnRestoreDefault;
    /**
     * Tlačítko, které se zpřístupní v případě, že se klikne na tlačítko btnEdit. Tím se zpřístupní pole editace, pak
     * bude zpřístupněno tlačítko btnSave pro uložení nového uživatelského jména.
     */
    private final JButton btnSave;
    /**
     * Tlačítko, které slouží pro zrušení editace uživatelského jména. Tlačítko bude zpřístupněno po kliknutí na
     * btnEdit.
     */
    private final JButton btnCancel;


    /**
     * Panel, do kterého se budou vkládat tlačítka, které budou dostupná pro uložení nebo editaci či zrušení editace
     * uživatelského jména. Buď bude dostupné tlačítko btnEdit nebo btnSave a btnCalcel.
     */
    private final JPanel pnlButtons;


    /**
     * Proměnná, dle které se pozná, zda je povolena editace uživatelského jména v textovém pole nebo ne. Dle této
     * proměnné se budou přidávat tlačítka do panelu, která budou přístupná. Buď uložení změny nebo zrušení editace.
     * Popř. že se má zpřístupnit editace uživatelského jména.
     */
    private boolean userNameEditAllowed;


    // Texty pro chybové hlášky:
    private String txtFailedSaveUserNameToFileText;
    private String txtFailedSaveUserNameToFileTitle;

    private String txtInvalidNameText;
    private String txtInvalidNameTitle;


    /**
     * Konstruktor třídy.
     */
    public UserNamePanel() {
        setLayout(new GridBagLayout());

        final GridBagConstraints gbc = getGbc();

        index = 0;


        // Inicializace labelu pro text:
        lblUserName = new JLabel();
        setGbc(gbc, index, 0, lblUserName, this);


        // Inicializace textového pole pro uživatelské jméno:
        txtUserNameField = new JTextField();
        txtUserNameField.addKeyListener(this);
        final JPanel pnlWithUserNameTextField = setAttributesToUserNameTextField();
        setGbc(gbc, index, 1, pnlWithUserNameTextField, this);


        // Panel pro tlačítka:
        pnlButtons = new JPanel(new FlowLayout(FlowLayout.CENTER));

        btnEdit = new JButton();
        btnRestoreDefault = new JButton();
        btnSave = new JButton();
        btnCancel = new JButton();

        btnEdit.addActionListener(this);
        btnRestoreDefault.addActionListener(this);
        btnSave.addActionListener(this);
        btnCancel.addActionListener(this);

        gbc.gridwidth = 2;
        setGbc(gbc, ++index, 0, pnlButtons, this);

        // Nastavení tlačítek do panelu:
        setButtonsToPanel();
    }


    /**
     * Nastavení atributů / vlastností pro textové pole, do kterého se zadává uživatelské jméno.
     *
     * <i>Jedná se o nastavení vlastností jako jsou například, že textové pole bude mít posuvník kvůli dlouhému textu,
     * rozměry textového pole, nebude editovatelné, nastaví se mu výchozí text (výchozí uživatelské jméno) atd.
     * Konkrétní vlastnosti viz tělo metody.</i>
     *
     * @return panel, který bude obsahovat výše popsané textové pole pro zadání uživatelského jména s nastavenými
     * vlastnostmi. Panel stačí vložit do okna dialogu.
     */
    private JPanel setAttributesToUserNameTextField() {
        txtUserNameField.setPreferredSize(new Dimension(250, 27));

        txtUserNameField.setText(UserNameSupport.getUsedUserName());
        txtUserNameField.setEditable(false);

        final JScrollBar scrollBar = new JScrollBar(JScrollBar.HORIZONTAL);

        final JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        final BoundedRangeModel brm = txtUserNameField.getHorizontalVisibility();

        scrollBar.setModel(brm);
        panel.add(txtUserNameField);
        panel.add(scrollBar);

        return panel;
    }


    /**
     * Nastavení komponent do panelu s tlačítky.
     *
     * <i>V případě, že je povolena editace, vloži se do panelu s tlačítky tlačítka pro uložení a zrušení změn. V
     * opačném případě se do panelu vloží tlačítko pro změnu uživatelského jména.</i>
     */
    private void setButtonsToPanel() {
        pnlButtons.removeAll();

        if (userNameEditAllowed) {
            pnlButtons.add(btnSave);
            pnlButtons.add(btnCancel);
        } else {
            pnlButtons.add(btnEdit);
            pnlButtons.add(btnRestoreDefault);
        }

        /*
         * Překreslení panelu, aby se projevily změny komponent v obsahu panelu.
         */
        pnlButtons.repaint();
    }


    @Override
    public void actionPerformed(final ActionEvent e) {
        if (e.getSource() instanceof JButton) {// Není nezbytná podmínka
            if (e.getSource() == btnEdit) {
                txtUserNameField.setEditable(true);
                userNameEditAllowed = true;
            }

            else if (e.getSource() == btnRestoreDefault) {
                /*
                 * Uloží se jméno přihlášeného uživatele v OS dokonfiguračního souboru, nastaví se proměnná, která se
                  * využívá pro získání uživatelského jména pro potřeby aplikace na příslušnou hodnotu a nastaví se
                  * text v textovém poli.
                 */
                final String userNameInOs = UserNameSupport.getUserNameWithTest(UserNameSupport.getUserNameInOs());

                if (!UserNameSupport.saveUserNameToConfigFile(userNameInOs))
                    JOptionPane.showMessageDialog(this, txtFailedSaveUserNameToFileText,
                            txtFailedSaveUserNameToFileTitle, JOptionPane.ERROR_MESSAGE);

                UserNameSupport.setUsedUserName(userNameInOs);
                txtUserNameField.setText(userNameInOs);
            }

            else if (e.getSource() == btnSave) {
                final String userName = txtUserNameField.getText().trim();

                /*
                 * Test, zda je jméno validní.
                 *
                 * Note:
                 *
                 * Tato část by neměly být potřeba, protože tlačítko pro uložení by nemělo být přístupné v případě,
                 * že text není validní, ale kdyby náhodou někde něco...
                 */
                if (!UserNameSupport.isUserNameValid(userName)) {
                    JOptionPane.showMessageDialog(this, txtInvalidNameText, txtInvalidNameTitle,
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }

                // Uložení jména do konfiguračního souboru:
                if (!UserNameSupport.saveUserNameToConfigFile(userName))
                    JOptionPane.showMessageDialog(this, txtFailedSaveUserNameToFileText,
                            txtFailedSaveUserNameToFileTitle, JOptionPane.ERROR_MESSAGE);

                // Nastavení nového uživatelského jména pro aplikaci.
                UserNameSupport.setUsedUserName(userName);

                txtUserNameField.setEditable(false);
                userNameEditAllowed = false;
            }

            else if (e.getSource() == btnCancel) {
                txtUserNameField.setText(UserNameSupport.getUsedUserName());
                txtUserNameField.setEditable(false);

                userNameEditAllowed = false;
            }

            /*
             * Nastavení komponent do panelu se zavolá po každém kliknutí na libovolné z výše uvedených tlačítek.
             * Pouze se v panelu nastaví různá tlačítko dle proměnné userNameEditAllowed.
             */
            setButtonsToPanel();
        }
    }


    /**
     * Nastavení barvy pozadí textového pole. Zvlášť v případě, že je text v něm validní (uživatelské jméno) nebo
     * obsahuje chybu.
     *
     * @param error
     *         true v případě, že se má nastavit barva pozadí textového pole na barvu, která značí chybu. Jinak true v
     *         případě, že se má nastavit výchozí barva pozadí textového pole.
     */
    private void highlightTextField(final boolean error) {
        if (error)
            txtUserNameField.setBackground(ERROR_TEXT_FIELD_COLOR);

        else txtUserNameField.setBackground(VALID_TEXT_FIELD_COLOR);
    }


    @Override
    public void setLanguage(final Properties properties) {
        final String txtUserNameField_1;
        final String txtUserNameField_2;
        final String txtInvalidNameText_1;
        final String txtInvalidNameText_2;

        if (properties != null) {
            setBorder(BorderFactory.createTitledBorder(properties.getProperty("Unp_Pnl_BorderTitle",
                    Constants.UNP_PNL_BORDER_TITLE)));

            lblUserName.setText(properties.getProperty("Unp_Lbl_UserName_Text", Constants.UNP_LBL_USER_NAME_TEXT));
            lblUserName.setToolTipText(properties.getProperty("Unp_Lbl_UserName_Title",
                    Constants.UNP_LBL_USER_NAME_TITLE));

            txtUserNameField_1 = properties.getProperty("Unp_Txt_UserNameField_1", Constants.UNP_TXT_USER_NAME_FIELD_1);
            txtUserNameField_2 = properties.getProperty("Unp_Txt_UserNameField_2", Constants.UNP_TXT_USER_NAME_FIELD_2);

            btnEdit.setText(properties.getProperty("Unp_Btn_Edit", Constants.UNP_BTN_EDIT));
            btnSave.setText(properties.getProperty("Unp_Btn_Save", Constants.UNP_BTN_SAVE));
            btnCancel.setText(properties.getProperty("Unp_Btn_Cancel", Constants.UNP_BTN_CANCEL));

            btnRestoreDefault.setText(properties.getProperty("Unp_Btn_RestoreDefault_Text",
                    Constants.UNP_BTN_RESTORE_DEFAULT_TEXT));
            btnRestoreDefault.setToolTipText(properties.getProperty("Unp_Btn_RestoreDefault_Title",
                    Constants.UNP_BTN_RESTORE_DEFAULT_TITLE));

            // Texty do chybových hlášek:
            txtFailedSaveUserNameToFileText = properties.getProperty("Unp_Txt_FailedSaveUserNameToFile_Text",
                    Constants.UNP_TXT_FAILED_SAVE_USER_NAME_TO_FILE_TEXT);
            txtFailedSaveUserNameToFileTitle = properties.getProperty("Unp_Txt_FailedSaveUserNameToFile_Title",
                    Constants.UNP_TXT_FAILED_SAVE_USER_NAME_TO_FILE_TITLE);

            txtInvalidNameText_1 = properties.getProperty("Unp_Txt_InvalidName_Text_1",
                    Constants.UNP_TXT_INVALID_NAME_TEXT_1);
            txtInvalidNameText_2 = properties.getProperty("Unp_Txt_InvalidName_Text_2",
                    Constants.UNP_TXT_INVALID_NAME_TEXT_2);
            txtInvalidNameTitle = properties.getProperty("Unp_Txt_InvalidName_Title",
                    Constants.UNP_TXT_INVALID_NAME_TITLE);
        }

        else {
            setBorder(BorderFactory.createTitledBorder(Constants.UNP_PNL_BORDER_TITLE));

            lblUserName.setText(Constants.UNP_LBL_USER_NAME_TEXT);
            lblUserName.setToolTipText(Constants.UNP_LBL_USER_NAME_TITLE);

            txtUserNameField_1 = Constants.UNP_TXT_USER_NAME_FIELD_1;
            txtUserNameField_2 = Constants.UNP_TXT_USER_NAME_FIELD_2;

            btnEdit.setText(Constants.UNP_BTN_EDIT);
            btnSave.setText(Constants.UNP_BTN_SAVE);
            btnCancel.setText(Constants.UNP_BTN_CANCEL);

            btnRestoreDefault.setText(Constants.UNP_BTN_RESTORE_DEFAULT_TEXT);
            btnRestoreDefault.setToolTipText(Constants.UNP_BTN_RESTORE_DEFAULT_TITLE);

            // Texty do chybových hlášek:
            txtFailedSaveUserNameToFileText = Constants.UNP_TXT_FAILED_SAVE_USER_NAME_TO_FILE_TEXT;
            txtFailedSaveUserNameToFileTitle = Constants.UNP_TXT_FAILED_SAVE_USER_NAME_TO_FILE_TITLE;

            txtInvalidNameText_1 = Constants.UNP_TXT_INVALID_NAME_TEXT_1;
            txtInvalidNameText_2 = Constants.UNP_TXT_INVALID_NAME_TEXT_2;
            txtInvalidNameTitle = Constants.UNP_TXT_INVALID_NAME_TITLE;
        }

        txtInvalidNameText =
                txtInvalidNameText_1 + " " + UserNameSupport.MIN_USER_NAME_LENGTH + " - " + UserNameSupport.MAX_USER_NAME_LENGTH + " " + txtInvalidNameText_2;

        final String txtUserNameFieldText =
                txtUserNameField_1 + " " + UserNameSupport.MIN_USER_NAME_LENGTH + " - " + UserNameSupport.MAX_USER_NAME_LENGTH + " " + txtUserNameField_2;
        txtUserNameField.setToolTipText(txtUserNameFieldText);

        lblUserName.setText("? " + lblUserName.getText() + ": ");
    }


    @Override
    public void keyTyped(final KeyEvent e) {
        // Není potřeba
    }

    @Override
    public void keyPressed(final KeyEvent e) {
        // Není potřeba
    }

    @Override
    public void keyReleased(final KeyEvent e) {
        if (e.getSource() instanceof JTextField) {// Podmínka není nezbytná
            if (e.getSource() == txtUserNameField) {
                // Zjištění, zda je jméno validní (odpovídá regulárnímu výrazu):
                final boolean validName = UserNameSupport.isUserNameValid(txtUserNameField.getText());

                highlightTextField(!validName);
                btnSave.setEnabled(validName);
            }
        }
    }
}
