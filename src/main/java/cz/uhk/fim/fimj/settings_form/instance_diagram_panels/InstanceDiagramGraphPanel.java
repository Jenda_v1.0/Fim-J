package cz.uhk.fim.fimj.settings_form.instance_diagram_panels;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.language.EditProperties;
import cz.uhk.fim.fimj.settings_form.PanelAbstract;
import cz.uhk.fim.fimj.settings_form.ReadWriteDiagramProperties;

/**
 * Třída obsahuje komponenty pro základní nastavení grafu - diagramu instancí vlastnosti jako barva pozadi, přiblížení,
 * odpojitelnost popisku
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class InstanceDiagramGraphPanel extends PanelAbstract
		implements ActionListener, LanguageInterface, ReadWriteDiagramProperties {

	private static final long serialVersionUID = 1L;

	
	
	private final JButton btnChooseBgColor;
	
	private final JLabel lblBgColor, lblChoosedBgColor, lblScale, lblResetApp;
	
	private final JCheckBox chcbConDisConLabelsFromEdge;
	
	private final JComboBox<Double> cmbScale;
	
	private Color bgColor;
	
	private String txtChooseBgColorTitle;
	
	
	/**
	 * Komponenta, která slouží pro nastavení toho, zda se mají v diagramu instancí
	 * zobrazovat vztahy instance na sebe sama.
	 */
	private final JCheckBox chcbShowRelationShipsToInstanceItself;
	
	
	
	/**
	 * Komponenta, která slouží pro nastavení toho, zda se mají v kontextovém menu
	 * nad označenou instancí v diagramu instancí zpřístupnit i soukromé metody pro
	 * zavolání.
	 */
	private final JCheckBox chcbIncludePrivateMethods;
	/**
	 * Komponenta, která slouží pro nastavení toho, zda se mají v kontextovém menu nad označenou instancí v diagramu
	 * instancí zpřístupnit chráněné metody pro zavolání.
	 */
	private final JCheckBox chcbIncludeProtectedMethods;

	/**
	 * Komponenta, která slouží pro nastavení toho, zda se mají v kontextovém menu nad označenou instancí v diagramu
	 * instancí zpřístupnit package-private metody pro zavolání.
	 */
	private final JCheckBox chcbIncludePackagePrivateMethods;

	
	
	/**
	 * Komponenta, kterou slouží pro nastavení toho, zda se mají v dialogu pro
	 * zavolání metody zpřístupnit veřejné a chráněné proměnné tříd z diagramu tříd,
	 * instnací v diagramu instancí a proměnné vytvořené uživatelem v editoru
	 * příkazů. Tyto proměnné vždy stejného datového typu jako je parametr metody
	 * budou zpřístupněné a bude možné je předat do parametru příslušé metody místo
	 * zadání vlastní hodnoty.
	 */
	private final JCheckBox chcbMakeAvailableFieldsForCallMethod;
	
	
	
	
	
	/**
	 * Komponenta, která slouží pro nastavení toho, zda se pro aktualizaci vztahů
	 * mezi instancemi v diagramu instancí má využít "rychlý" nebo "pomalý" způsob.
	 */
	private final JCheckBox chcbWayToRefreshRelationshipsBetweenInstances;
	
	
	
	
	/**
	 * Komponenta pro nastavení toho, zda se maji v diagramu instancí zobrazit i
	 * vztahy, které jsou naplněny v proměnných v předcích příslušné instance v
	 * diagramu instnací.
	 */
	private final JCheckBox chcbShowRelationshipsInInheritedClasses;
	
	
	
	
	
	
	/**
	 * Tato komponenta slouží pro nastavení toho, zda se má při zavolání metody
	 * pomocí kontextového menu nad instancí v diagramu instancí zpřístupnit možnost
	 * pro vytvoření nové instance v případě, že uživatelem vybraná metoda pro
	 * zavolání obsahuje parametr typu nějaké třídy, která se nachází v diagramu
	 * tříd. Pak lze do parametru této metody předat veškeré instance příslušné
	 * třídy, které se aktuálně nachází v diagramu instancí a pokud bude tato
	 * možnost true, pak bude na výběr i možnost pro vytvoření nové instance
	 * příslušné třídy, která se předá do parametru příslušné metody. Jinak, pokud
	 * bude tato možnost false, pak nebude možnost pro vytvoření nové instance ale
	 * pokud se v diagramu instancí nebude nacházet žádná instace typu příslušné
	 * třídy, pak meotoda nepůjde zavolat, protože nebude parametr, který by bylo
	 * možné předat a null hodnoty v aplikaci nejsou povoleny.
	 */
	private final JCheckBox chcbShowCreateNewInstanceOptionInCallMethod;
	
	
	
	
	
	
	
	/**
	 * Komponenta, která slouží pro nastavení toho, zda se má vztah asociace v
	 * diagramu instancí zobrazit jako jedna hrana s šipkami na obou koncích nebo jako
	 * dvě agregace.
	 */
	private final JCheckBox chcbShowAssociationThroughAggregation;
	
	
	
	
	
	/**
	 * Komponenta, která slouží pro nastavení toho, zda se mají v dialogu s
	 * přehledem informací o instanci v diagramu instancí zobrazit u naplněných
	 * proměnných ty reference na příslušné instance v diagramu instnací, které
	 * uživatel zadal při jejich vytváření.
	 */
	private final JCheckBox chcbShowReferenceVariables;
	
	
	
	
	/**
	 * Komponenta, která slouží pro nastavení toho, zda se mají do dialogu pro
	 * zavolání metody nad instanci v diagramu instnací zobrazit i veřejné a
	 * popřípadě chráněné metody, které je možné zavolat a jejich návratovou hodnotu
	 * vložit do příslušného parametru příslušné označené metody pro zavolání.
	 */
	private final JCheckBox chcbMakeMethodsAvailableForCallMethod;
	
	
	
	
	/**
	 * Komponenta, která slouží pro nastavení toho, zda se mají v dialogu pro
	 * zavolání setru (metody) zobrzaovat i getr (metoda) na proměnnou, která je
	 * typu té třídy, kde se ten setr a getr nachází a setr má parametru typu té
	 * třídy, kde senachází.
	 */
	private final JCheckBox chcbShowGetterInSetterMethodInInstance;


	/**
	 * Komponenta, která slouží pro nastavení toho, zda se má v kontextovém menu nad instancí v diagramu instancí v
	 * položce "Zděděné metody" zobrazovat jednotlivé třídy (předci) jako názvy tříd s balíčky, ve kterých / kterém se
	 * nachází nebo jako pouzehý název třídy (bez balíčků).
	 */
	private final JCheckBox chcbShowInheritedClassNameWithPackages;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 */
	public InstanceDiagramGraphPanel() {
		super();
		
		setLayout(new GridBagLayout());
		
		
		
		final GridBagConstraints gbc = getGbc();
		
		index = 0;
		
		
		gbc.gridwidth = 3;
		
		lblResetApp = new JLabel();
		setGbc(gbc, index, 0, lblResetApp, this);
		
		
		
		chcbConDisConLabelsFromEdge = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbConDisConLabelsFromEdge, this);
		
		
		chcbShowRelationShipsToInstanceItself = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbShowRelationShipsToInstanceItself, this);
		
		
		chcbShowAssociationThroughAggregation = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbShowAssociationThroughAggregation, this);


		chcbShowInheritedClassNameWithPackages = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbShowInheritedClassNameWithPackages, this);

		
		chcbIncludePrivateMethods = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbIncludePrivateMethods, this);

		chcbIncludeProtectedMethods = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbIncludeProtectedMethods, this);

		chcbIncludePackagePrivateMethods = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbIncludePackagePrivateMethods, this);
		
		
		chcbMakeAvailableFieldsForCallMethod = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbMakeAvailableFieldsForCallMethod, this);
		
		
		chcbMakeMethodsAvailableForCallMethod = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbMakeMethodsAvailableForCallMethod, this);
		
		
		chcbShowCreateNewInstanceOptionInCallMethod = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbShowCreateNewInstanceOptionInCallMethod, this);
		
		
		chcbShowGetterInSetterMethodInInstance = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbShowGetterInSetterMethodInInstance, this);
		
		
		chcbWayToRefreshRelationshipsBetweenInstances = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbWayToRefreshRelationshipsBetweenInstances, this);
		
				
		chcbShowRelationshipsInInheritedClasses = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbShowRelationshipsInInheritedClasses, this);		
		
		chcbShowReferenceVariables = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbShowReferenceVariables, this);
		
		gbc.gridwidth = 1;
		
		
		lblBgColor = new JLabel();
		setGbc(gbc, ++index, 0, lblBgColor, this);
		
		btnChooseBgColor = new JButton();
		btnChooseBgColor.addActionListener(this);
		setGbc(gbc, index, 1, btnChooseBgColor, this);
		
		lblChoosedBgColor = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChoosedBgColor.setOpaque(true);
		setGbc(gbc, index, 2, lblChoosedBgColor, this);
		
		
		
		
		
		lblScale = new JLabel();
		setGbc(gbc, ++index, 0, lblScale, this);
		
		cmbScale = new JComboBox<>(getArrayOfScale());
		setGbc(gbc, index, 1, cmbScale, this);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří instanci jednorozměrného pole s hodnotami typu double v
	 * rozsahu: 0.1 - 20.0
	 * 
	 * @return vrátí uvedené pole
	 */
	private static Double[] getArrayOfScale() {
		final Double[] arrayOfDouble = new Double[196];
		
		int index = 0;

		
		for (int i = 0; i <= 20; i++) {
			for (int j = 0; j <= 9; j++) {
				if (!(i == 0 && (j == 0 || j == 1 || j == 2 || j == 3 || j == 4))) {
					arrayOfDouble[index] = Double.parseDouble(i + "." + j); 	
					index++;
				}
						
				// Mám požadované hodnoty, mohu ukončít cyklus:
				if (i == 20 && j == 0)
					break;
			}
		}
		
		return arrayOfDouble;
	}


	
	
	
	

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton && e.getSource() == btnChooseBgColor) {
			final Color tempVariable = JColorChooser.showDialog(this, txtChooseBgColorTitle,
					Constants.ID_BACKGROUND_COLOR);

			if (tempVariable != null) {
				bgColor = tempVariable;
				lblChoosedBgColor.setForeground(bgColor);
				lblChoosedBgColor.setBackground(bgColor);
			}
		}
	}



	
	
	
	
	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			setBorder(BorderFactory.createTitledBorder(properties.getProperty("Sf_Cd_Cdgp_BorderTitle", Constants.SF_CD_PNL_CDGP_BORDER_TITLE)));
			
			txtChooseBgColorTitle = properties.getProperty("Sf_Cd_ChooseBackgroundColorDialog", Constants.SF_TXT_CHOOSE_BG_COLOR);
			
			chcbConDisConLabelsFromEdge.setText(properties.getProperty("Sf_Cd_Cdgp_LabelDisconectableFromEdge", Constants.SF_CD_PNL_CDGP_CHCB_CON_DIS_CON_LABEL_FROM_EDGE));
			
			chcbShowRelationShipsToInstanceItself.setText(properties.getProperty("Sf_Cd_Cdgp_ShowRelationsShipsToClassItself", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_RELATION_SHIPS_TO_CLASS_ITSELF));
								
			chcbShowAssociationThroughAggregation.setText(properties.getProperty("Sf_Cd_Cdgp_ChcbShowAsociationThroughAgregation", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_ASOCIATION_THROUGH_AGREGATION));
			chcbShowAssociationThroughAggregation.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_ChcbShowAsociationThroughAgregation_TT", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_ASOCIATION_THROUGH_AGREGATION_TT));

			chcbShowInheritedClassNameWithPackages.setText(properties.getProperty("Sf_Cd_Cdgp_ShowInheritedClassNameWithPackages", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES));
			chcbShowInheritedClassNameWithPackages.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_Id_ShowInheritedClassNameWithPackages_TT", Constants.SF_ID_PNL_CDGP_CHCB_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES_TT));

			chcbIncludePrivateMethods.setText(properties.getProperty("Sf_Cd_Cdgp_IncludePrivateMethods", Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PRIVATE_METHODS));			
			chcbIncludePrivateMethods.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_Id_IncludePrivateMethods_TT", Constants.SF_CD_PNL_CDGP_CHCB_ID_INCLUDE_PRIVATE_METHODS_TT));

			chcbIncludeProtectedMethods.setText(properties.getProperty("Sf_Cd_Cdgp_IncludeProtectedMethods", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PROTECTED_METHODS));
			chcbIncludeProtectedMethods.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_Id_IncludeProtectedMethods_TT", Constants.SF_CD_PNL_CDGP_CHCB_ID_INCLUDE_PROTECTED_METHODS_TT));

			chcbIncludePackagePrivateMethods.setText(properties.getProperty("Sf_Cd_Cdgp_IncludePackagePrivateMethods", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PACKAGE_PRIVATE_METHODS));
			chcbIncludePackagePrivateMethods.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_Id_IncludePackagePrivateMethods_TT", Constants.SF_CD_PNL_CDGP_CHCB_ID_INCLUDE_PACKAGE_PRIVATE_METHODS_TT));

			chcbMakeAvailableFieldsForCallMethod.setText(properties.getProperty("Sf_Cd_Cdgp_MakeAvailableFieldsForCallMethod", Constants.SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_METHOD));
			chcbMakeAvailableFieldsForCallMethod.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_MakeAvailableFieldsForCallMethod_TT", Constants.SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_METHOD_TT));

			chcbMakeMethodsAvailableForCallMethod.setText(properties.getProperty("Sf_Id_Cdgp_MakeMethodsAvailableForCallMethod", Constants.SF_ID_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD));
			chcbMakeMethodsAvailableForCallMethod.setToolTipText(properties.getProperty("Sf_Id_Cdgp_MakeMethodsAvailableForCallMethod_TT", Constants.SF_ID_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD_TT));
			
			chcbShowCreateNewInstanceOptionInCallMethod.setText(properties.getProperty("Sf_Cd_Cdgp_ShowCreateNewInstanceOptionInCallMethod", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD));
			chcbShowCreateNewInstanceOptionInCallMethod.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_ShowCreateNewInstanceOptionInCallMethod_TT", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD_TT));			
			
			chcbShowGetterInSetterMethodInInstance.setText(properties.getProperty("Sf_Cd_Cdgp_ShowGetterInSetterMethodInInstance", Constants.SF_ID_PNL_CDGP_CHCB_SHOW_GETTER_IN_SETTER_METHOD_IN_INSTANCE));
			chcbShowGetterInSetterMethodInInstance.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_ShowGetterInSetterMethodInInstance_TT", Constants.SF_ID_PNL_CDGP_CHCB_SHOW_GETTER_IN_SETTER_METHOD_IN_INSTANCE_TT));
			
			chcbWayToRefreshRelationshipsBetweenInstances.setText(properties.getProperty("Sf_Cd_Cdgp_WayToRefreshRelationshipsBetweenInstances_Text", Constants.SF_CD_PNL_CDGP_CHCB_WAY_TO_REFRESH_RELATIONSHIPS_BETWEEN_INSTANCES));
			chcbWayToRefreshRelationshipsBetweenInstances.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_WayToRefreshRelationshipsBetweenInstances_TT", Constants.SF_CD_PNL_CDGP_CHCB_ID_WAY_TO_REFRESH_RELATIONSHIPS_BETWEEN_INSTANCES_TT));
						
			chcbShowRelationshipsInInheritedClasses.setText(properties.getProperty("Sf_Cd_Cdgp_ShowRelationshipsInInheritedClasses", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_RELATIONSHIPS_IN_INHERITED_CLASSES));
			chcbShowRelationshipsInInheritedClasses.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_ShowRelationshipsInInheritedClasses_TT", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_RELATIONSHIPS_IN_INHERITED_CLASSES_TT));						
					
			chcbShowReferenceVariables.setText(properties.getProperty("Sf_Cd_Cdgp_Chcb_ShowReferenceVariablesText", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_REFERENCE_VARIABLES_TEXT));
			chcbShowReferenceVariables.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_Chcb_ShowReferenceVariablesTT", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_REFERENCE_VARIABLES_TT));
			
			lblBgColor.setText(properties.getProperty("Sf_Cd_Cdgp_LabelBackgroundColor", Constants.SF_CD_PNL_CDGP_LBL_BG_COLOR) + ": ");
			
			lblScale.setText(properties.getProperty("Sf_Cd_Cdgp_LabelScaleOfGraph", Constants.SF_CD_PNL_CDGP_LBL_SCALE) + ": ");
			
			btnChooseBgColor.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));
		}
		
		
		else {
			setBorder(BorderFactory.createTitledBorder(Constants.SF_CD_PNL_CDGP_BORDER_TITLE));
			
			txtChooseBgColorTitle = Constants.SF_TXT_CHOOSE_BG_COLOR;
			
			chcbConDisConLabelsFromEdge.setText(Constants.SF_CD_PNL_CDGP_CHCB_CON_DIS_CON_LABEL_FROM_EDGE);
			
			chcbShowRelationShipsToInstanceItself.setText(Constants.SF_CD_PNL_CDGP_CHCB_SHOW_RELATION_SHIPS_TO_CLASS_ITSELF);
						
			chcbShowAssociationThroughAggregation.setText(Constants.SF_CD_PNL_CDGP_CHCB_SHOW_ASOCIATION_THROUGH_AGREGATION);
			chcbShowAssociationThroughAggregation.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_SHOW_ASOCIATION_THROUGH_AGREGATION_TT);

			chcbShowInheritedClassNameWithPackages.setText(Constants.SF_CD_PNL_CDGP_CHCB_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES);
			chcbShowInheritedClassNameWithPackages.setToolTipText(Constants.SF_ID_PNL_CDGP_CHCB_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES_TT);

			chcbIncludePrivateMethods.setText(Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PRIVATE_METHODS);			
			chcbIncludePrivateMethods.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_ID_INCLUDE_PRIVATE_METHODS_TT);

			chcbIncludeProtectedMethods.setText(Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PROTECTED_METHODS);
			chcbIncludeProtectedMethods.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_ID_INCLUDE_PROTECTED_METHODS_TT);

			chcbIncludePackagePrivateMethods.setText(Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PACKAGE_PRIVATE_METHODS);
			chcbIncludePackagePrivateMethods.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_ID_INCLUDE_PACKAGE_PRIVATE_METHODS_TT);

			chcbMakeAvailableFieldsForCallMethod.setText(Constants.SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_METHOD);
			chcbMakeAvailableFieldsForCallMethod.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_METHOD_TT);
			
			chcbMakeMethodsAvailableForCallMethod.setText(Constants.SF_ID_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD);
			chcbMakeMethodsAvailableForCallMethod.setToolTipText(Constants.SF_ID_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD_TT);
			
			chcbShowCreateNewInstanceOptionInCallMethod.setText(Constants.SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD);
			chcbShowCreateNewInstanceOptionInCallMethod.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD_TT);
			
			chcbShowGetterInSetterMethodInInstance.setText(Constants.SF_ID_PNL_CDGP_CHCB_SHOW_GETTER_IN_SETTER_METHOD_IN_INSTANCE);
			chcbShowGetterInSetterMethodInInstance.setToolTipText(Constants.SF_ID_PNL_CDGP_CHCB_SHOW_GETTER_IN_SETTER_METHOD_IN_INSTANCE_TT);
			
			chcbWayToRefreshRelationshipsBetweenInstances.setText(Constants.SF_CD_PNL_CDGP_CHCB_WAY_TO_REFRESH_RELATIONSHIPS_BETWEEN_INSTANCES);
			chcbWayToRefreshRelationshipsBetweenInstances.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_ID_WAY_TO_REFRESH_RELATIONSHIPS_BETWEEN_INSTANCES_TT);
			
			chcbShowRelationshipsInInheritedClasses.setText(Constants.SF_CD_PNL_CDGP_CHCB_SHOW_RELATIONSHIPS_IN_INHERITED_CLASSES);
			chcbShowRelationshipsInInheritedClasses.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_SHOW_RELATIONSHIPS_IN_INHERITED_CLASSES_TT);
			
			chcbShowReferenceVariables.setText(Constants.SF_CD_PNL_CDGP_CHCB_SHOW_REFERENCE_VARIABLES_TEXT);
			chcbShowReferenceVariables.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_SHOW_REFERENCE_VARIABLES_TT);
			
			lblBgColor.setText(Constants.SF_CD_PNL_CDGP_LBL_BG_COLOR + ": ");
			
			lblScale.setText(Constants.SF_CD_PNL_CDGP_LBL_SCALE + ": ");
			
			btnChooseBgColor.setText(Constants.SF_CD_BTN_CHOOSE_COLOR);
		}
		
		// Natavím otazník na začátek, aby uživatel věděl, že tam má nápovědu:
		chcbShowInheritedClassNameWithPackages.setText("? " + chcbShowInheritedClassNameWithPackages.getText());
		chcbIncludePrivateMethods.setText("? " + chcbIncludePrivateMethods.getText());
		chcbIncludeProtectedMethods.setText("? " + chcbIncludeProtectedMethods.getText());
		chcbIncludePackagePrivateMethods.setText("? " + chcbIncludePackagePrivateMethods.getText());
		chcbMakeAvailableFieldsForCallMethod.setText("? " + chcbMakeAvailableFieldsForCallMethod.getText());
		chcbMakeMethodsAvailableForCallMethod.setText("? " + chcbMakeMethodsAvailableForCallMethod.getText());
		chcbWayToRefreshRelationshipsBetweenInstances.setText("? " + chcbWayToRefreshRelationshipsBetweenInstances.getText());
		chcbShowRelationshipsInInheritedClasses.setText("? " + chcbShowRelationshipsInInheritedClasses.getText());		
		chcbShowCreateNewInstanceOptionInCallMethod.setText("? " + chcbShowCreateNewInstanceOptionInCallMethod.getText());
		chcbShowGetterInSetterMethodInInstance.setText("? " + chcbShowGetterInSetterMethodInInstance.getText());
		chcbShowAssociationThroughAggregation.setText("? " + chcbShowAssociationThroughAggregation.getText());
		chcbShowReferenceVariables.setText("? " + chcbShowReferenceVariables.getText());
	}
	
	
	
	
	
	
	

	
	
	@Override
	public void readDataFromDiagramProp(final Properties instanceDiagramPro) {
		chcbConDisConLabelsFromEdge.setSelected(Boolean.parseBoolean(instanceDiagramPro.getProperty("EdgeLabelMovable", String.valueOf(Constants.ID_EDGE_LABELS_MOVABLE))));
		
		chcbShowRelationShipsToInstanceItself.setSelected(Boolean.parseBoolean(instanceDiagramPro.getProperty("ShowRelationsShipsToInstanceItself", String.valueOf(Constants.ID_SHOW_RELATION_SHIPS_TO_INSTANCE_ITSELF))));
		
		chcbShowAssociationThroughAggregation.setSelected(Boolean.parseBoolean(instanceDiagramPro.getProperty("ShowAssociationThroughAggregation", String.valueOf(Constants.ID_SHOW_ASOCIATION_THROUGH_AGREGATION))));

		chcbShowInheritedClassNameWithPackages.setSelected(Boolean.parseBoolean(instanceDiagramPro.getProperty("ShowInheritedClassNameWithPackages", String.valueOf(Constants.ID_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES))));

		chcbIncludePrivateMethods.setSelected(Boolean.parseBoolean(instanceDiagramPro.getProperty("IncludePrivateMethods", String.valueOf(Constants.ID_INCLUDE_PRIVATE_METHODS))));

		chcbIncludeProtectedMethods.setSelected(Boolean.parseBoolean(instanceDiagramPro.getProperty("IncludeProtectedMethods", String.valueOf(Constants.ID_INCLUDE_PROTECTED_METHODS))));

		chcbIncludePackagePrivateMethods.setSelected(Boolean.parseBoolean(instanceDiagramPro.getProperty("IncludePackagePrivateMethods", String.valueOf(Constants.ID_INCLUDE_PACKAGE_PRIVATE_METHODS))));

		chcbMakeAvailableFieldsForCallMethod.setSelected(Boolean.parseBoolean(instanceDiagramPro.getProperty("MakeFieldsAvailableForMethod", String.valueOf(Constants.ID_MAKE_FIELDS_AVAILABLE_FOR_CALL_METHOD))));
		
		chcbMakeMethodsAvailableForCallMethod.setSelected(Boolean.parseBoolean(instanceDiagramPro.getProperty("MakeMethodsAvailableForCallMethod", String.valueOf(Constants.ID_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD))));
		
		chcbShowCreateNewInstanceOptionInCallMethod.setSelected(Boolean.parseBoolean(instanceDiagramPro.getProperty("ShowCreateNewInstanceOptionInCallMethod", String.valueOf(Constants.ID_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD))));		
		
		chcbShowGetterInSetterMethodInInstance.setSelected(Boolean.parseBoolean(instanceDiagramPro.getProperty("ShowGetterInSetterMethodInInstance", String.valueOf(Constants.ID_SHOW_GETTER_IN_SETTER_METHOD_IN_INSTANCE))));
		
		chcbWayToRefreshRelationshipsBetweenInstances.setSelected(Boolean.parseBoolean(instanceDiagramPro.getProperty("UseFastWayToRefreshRelationshipsBetweenInstances", String.valueOf(Constants.ID_USE_FAST_WAY_TO_REFRESH_RELATIONSHIPS_BETWEEN_CLASSES))));		
				
		chcbShowRelationshipsInInheritedClasses.setSelected(Boolean.parseBoolean(instanceDiagramPro.getProperty("ShowRelationshipsInInheritedClasses", String.valueOf(Constants.ID_SHOW_RELATIONSHIPS_IN_INHERITED_CLASSES))));		
		
		chcbShowReferenceVariables.setSelected(Boolean.parseBoolean(instanceDiagramPro.getProperty("ShowReferenceVariables", String.valueOf(Constants.ID_SHOW_REFERENCES_VARIABLES))));
			
		final String backgroundColor = instanceDiagramPro.getProperty("BackgroundColor", Integer.toString(Constants.ID_BACKGROUND_COLOR.getRGB()));
		bgColor = new Color(Integer.parseInt(backgroundColor));
		lblChoosedBgColor.setBackground(bgColor);
		lblChoosedBgColor.setForeground(bgColor);
		
		
		final double scale = Double.parseDouble(instanceDiagramPro.getProperty("Scale", String.valueOf(Constants.INSTANCE_DIAGRAM_SCALE)));
		
		if (scale > 0 && scale <= 60)
			cmbScale.setSelectedItem(scale);
	}
	
	
	
	
	
	
	
	
	
	@Override
	public EditProperties writeDataToDiagramProp(final EditProperties instanceDiagramProp) {
		instanceDiagramProp.setProperty("EdgeLabelMovable", String.valueOf(chcbConDisConLabelsFromEdge.isSelected()));
		
		instanceDiagramProp.setProperty("ShowRelationsShipsToInstanceItself", String.valueOf(chcbShowRelationShipsToInstanceItself.isSelected()));
		
		instanceDiagramProp.setProperty("ShowAssociationThroughAggregation", String.valueOf(chcbShowAssociationThroughAggregation.isSelected()));
		
		instanceDiagramProp.setProperty("ShowInheritedClassNameWithPackages", String.valueOf(chcbShowInheritedClassNameWithPackages.isSelected()));

		instanceDiagramProp.setProperty("IncludePrivateMethods", String.valueOf(chcbIncludePrivateMethods.isSelected()));

		instanceDiagramProp.setProperty("IncludeProtectedMethods", String.valueOf(chcbIncludeProtectedMethods.isSelected()));

		instanceDiagramProp.setProperty("IncludePackagePrivateMethods", String.valueOf(chcbIncludePackagePrivateMethods.isSelected()));

		instanceDiagramProp.setProperty("MakeFieldsAvailableForMethod", String.valueOf(chcbMakeAvailableFieldsForCallMethod.isSelected()));		
		
		instanceDiagramProp.setProperty("MakeMethodsAvailableForCallMethod", String.valueOf(chcbMakeMethodsAvailableForCallMethod.isSelected()));
		
		instanceDiagramProp.setProperty("ShowCreateNewInstanceOptionInCallMethod", String.valueOf(chcbShowCreateNewInstanceOptionInCallMethod.isSelected()));	
		
		instanceDiagramProp.setProperty("ShowGetterInSetterMethodInInstance", String.valueOf(chcbShowGetterInSetterMethodInInstance.isSelected()));
		
		instanceDiagramProp.setProperty("UseFastWayToRefreshRelationshipsBetweenInstances", String.valueOf(chcbWayToRefreshRelationshipsBetweenInstances.isSelected()));
				
		instanceDiagramProp.setProperty("ShowRelationshipsInInheritedClasses", String.valueOf(chcbShowRelationshipsInInheritedClasses.isSelected()));
				
		instanceDiagramProp.setProperty("ShowReferenceVariables", String.valueOf(chcbShowReferenceVariables.isSelected()));
		
		instanceDiagramProp.setProperty("Scale", String.valueOf(cmbScale.getSelectedItem()));
		
		if (bgColor != null)
			instanceDiagramProp.setProperty("BackgroundColor", Integer.toString(bgColor.getRGB()));				
		
		
		return instanceDiagramProp;
	}
	
	
	
	
	// Přístupové metody - settry na nastavení hodnot komponent v panelu:
	public final void setSelectedChcbConDisConLabel(final boolean selected) {
		chcbConDisConLabelsFromEdge.setSelected(selected);
	}

	public final void setSelectedChcbShowRelationShipsToInstanceItself(final boolean selected) {
		chcbShowRelationShipsToInstanceItself.setSelected(selected);
	}

	public final void setSelectedChcbShowInheritedClassNameWithPackages(final boolean selected) {
		chcbShowInheritedClassNameWithPackages.setSelected(selected);
	}

	public final void setSelectedChcbIncludePrivateMethods(final boolean selected) {
		chcbIncludePrivateMethods.setSelected(selected);
	}

	public final void setSelectedChcbIncludeProtectedMethods(final boolean selected) {
		chcbIncludeProtectedMethods.setSelected(selected);
	}
	public final void setSelectedChcbIncludePackagePrivateMethods(final boolean selected) {
		chcbIncludePackagePrivateMethods.setSelected(selected);
	}

	public final void setSelectedChcbMakeAvailableFieldsForCallMethod(final boolean selected) {
		chcbMakeAvailableFieldsForCallMethod.setSelected(selected);
	}

	public final void setSelectedChcbMakeAvailableMethodsForCallMethod(final boolean selected) {
		chcbMakeMethodsAvailableForCallMethod.setSelected(selected);
	}

	public final void setSelectedChcbWayToRefreshRelationshipsBetweenInstances(final boolean selected) {
		chcbWayToRefreshRelationshipsBetweenInstances.setSelected(selected);
	}

	public final void setSelectedChcbShowRelationshipsInInheritedClasses(final boolean selected) {
		chcbShowRelationshipsInInheritedClasses.setSelected(selected);
	}

	public final void setSelectedChcbShowCreateNewInstanceOptionInCallMethod(final boolean selected) {
		chcbShowCreateNewInstanceOptionInCallMethod.setSelected(selected);
	}

	public final void setSelectedChcbShowGetterInSetterMethodInInstance(final boolean selected) {
		chcbShowGetterInSetterMethodInInstance.setSelected(selected);
	}

	public final void setSelectedChcbShowAssociationThroughAggregation(final boolean selected) {
		chcbShowAssociationThroughAggregation.setSelected(selected);
	}

	public final void setSelectedChcbShowReferenceVariables(final boolean selected) {
		chcbShowReferenceVariables.setSelected(selected);
	}		
		
		

	public final void setBgColor(final int rgbOfBgColor) {
		bgColor = new Color(rgbOfBgColor);
		lblChoosedBgColor.setForeground(bgColor);
		lblChoosedBgColor.setBackground(bgColor);
	}
	
	public final void setScaleOfGraph(final double scale) {
		cmbScale.setSelectedItem(scale);
	}
}