package cz.uhk.fim.fimj.settings_form.application_panels;

import cz.uhk.fim.fimj.language.KeysOrder;
import cz.uhk.fim.fimj.settings_form.cmb_items_with_tooltip.CmbItemAbstract;

/**
 * Tato třída slouží pouze jako objekt do komponenty JcomboBox - cmbKindOfSortingTextInProp ve třídě LanguagePanel v
 * balíčku applicationPanels.
 * <p>
 * Jedná se o třídu, která obsahuje výčet, který definuje příslušné řazení textů v souborech .properties, které obsahují
 * texty pro aplikaci v příslušném jazyce.
 * <p>
 * Dále obsahuje proměnnou pro text, který se zobrazí v uvedené komponentě cmbKindOfSortingTextInProp a další proměnnou,
 * která slouží jako ToolTip, tj. když se najede kurzorem myši na příslušnou položku v uvedeném
 * cmbKindOfSortingTextInProp, tak se u kurzoru myší zobrazí popisek k danému způsobu řazení textů v souboru
 * .properties.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class SortingInfo extends CmbItemAbstract {

    /**
     * Výčtový typ, který definuje způsob řazení textů v souborech .properties.
     */
    private final KeysOrder keysOrder;


    /**
     * Konstruktor třídy.
     *
     * @param keysOrder
     *         - Výčtový typ, který definuje způsob řazení textů v souborech .properties s texty v příslušném jazyce pro
     *         tuto aplikaci.
     * @param kindOfSortingText
     *         - Text, který bude zobrazen v cmbKindOfSortingTextInProp v panelu LanguagePanel, dle tohoto textu
     *         uživatel pozná, jaký způsob řazení si vybral.
     * @param kindOfSortingTt
     *         - ToolTip neboli popisek pro způsob řazení.
     */
    SortingInfo(final KeysOrder keysOrder, final String kindOfSortingText, final String kindOfSortingTt) {
        super(kindOfSortingText, kindOfSortingTt);

        this.keysOrder = keysOrder;
    }


    public KeysOrder getKeysOrder() {
        return keysOrder;
    }
}
