package cz.uhk.fim.fimj.settings_form.cmb_items_with_tooltip;

/**
 * Třída, která obsahuje hodnoty, které jsou společené pro třídy, které slouží jako položky do komponenty JComboBox.
 * Takové položky mají text, které je v uvedené komponentě zobrazen a navíc mají nebo moou mít tooltip, který se zobrazí
 * po najetí myši na položku v uvedené komponentě.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 28.08.2018 20:49
 */

public abstract class CmbItemAbstract {

    /**
     * Text, který se má zobrazit v komponentě JComboBox - konkrétní zobrazená položka.
     */
    private final String textToShow;

    /**
     * Text, který slouží jako tooltip položky zobrazené v komponentě JComboBox. Jedná se o "doplňující" informace k
     * příslušné položce.
     */
    private final String textToTooltip;


    /**
     * Konstruktor třídy pro naplnění hodnot.
     *
     * @param textToShow
     *         - text, který se má zobrazit v komponentě JComboBox.
     * @param textToTooltip
     *         - text, který slouží jako tooltip položky zobrazené v komponentě JComboBox. Tento text se zobrazí po
     *         najetí myši na příslušnou položku ve zmíněné komponentě.
     */
    public CmbItemAbstract(final String textToShow, final String textToTooltip) {
        this.textToShow = textToShow;
        this.textToTooltip = textToTooltip;
    }


    String getTextToTooltip() {
        return textToTooltip;
    }


    @Override
    public String toString() {
        return textToShow;
    }
}
