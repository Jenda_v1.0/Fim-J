package cz.uhk.fim.fimj.settings_form;

/**
 * Tato třída slouží pro vytvoření a reprezentaci dat coby volba obrázku typu .gif, který reprezentuje animace coby
 * načítání, například když běží vlákno, apod. tak aby si uživatel nemyslel, že došlo třeba k chybě apod., ale aby
 * věděl, že aplikace zpracovává data a nejedná se o chybu.
 * <p>
 * V této třídě budou akorát dvě proměnné, jedna pouze pro název obrázku, který zárověň "říká" o jaký typ obrázku jde a
 * druhá proměnná, která určituje typ obrázku, ale to bude vždy .gif akorát se mi dle této přípony bude lépe zapisovat
 * příslušné obrázky
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ImageInfo {

    private static final String EXTENSION = ".gif";


    /**
     * Proměnná, do které se vloží název obrázku, který se pak použije při načítání v tzv. loading dialogu, který se
     * zobrazí, když aplikace načítá nějaká data
     */
    private final String imageName;


    /**
     * Konstruktor této třídy - slouží akorát pro naplnění proměnné
     *
     * @param imageName
     *         - text coby název obrázku, který se má použít pro reprezentaci načítání v tzv. loading dialogu
     */
    public ImageInfo(final String imageName) {
        super();

        this.imageName = imageName;
    }


    /**
     * Metoda, která vrátí text, v podobně názvu obrázku a přípony .gif
     *
     * @return název obrázku.gif
     */
    final String getImageName() {
        return imageName + EXTENSION;
    }


    @Override
    public String toString() {
        return imageName;
    }
}