package cz.uhk.fim.fimj.settings_form.application_panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.stream.IntStream;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.language.KeysOrder;
import cz.uhk.fim.fimj.settings_form.PanelAbstract;
import cz.uhk.fim.fimj.settings_form.cmb_items_with_tooltip.CmbItemAbstract;
import cz.uhk.fim.fimj.settings_form.cmb_items_with_tooltip.ToolTipsComboBoxRenderer;

/**
 * Tato třída slouží pro nastavení jazyka a dále pro nastavení řazení textů a komentářů v souboru .properties, který
 * obsahuje texty pro aplikaci v příslušném jazyce.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class LanguagePanel extends PanelAbstract implements LanguageInterface {

    private static final long serialVersionUID = 7513903402311500415L;


    /**
     * Jazyky, které budou na výběr (model pro cmb cmbLanguage).
     */
    private static final List<LanguageInfo> LANGUAGES = Collections
            .unmodifiableList(Arrays.asList(new LanguageInfo("Czech", "CZ"), new LanguageInfo("English", "EN")));


    /**
     * Komponenta pro výběr jazyka aplikace
     */
    private final JComboBox<LanguageInfo> cmbLanguage;


    // Labely do okna panelu:
    private final JLabel lblLanguage;
    private final JLabel lblSortingTextInPropFile;
    private final JLabel lblCountOfFreeLinesBeforeComment;
    private final JLabel lblLocale;


    /**
     * Komponenta, ve které si uživatel může zvolit, jaký typ "řazení" textů v souboru .properties se aplikuje.
     */
    private final JComboBox<CmbItemAbstract> cmbKindOfSortingTextInProp;


    /**
     * Komponenta, dle které se pozná, zda se mají při vytvoření souboru .properties s texty pro tuto aplikaci přidat do
     * tohoto souboru .properties komentáře k textům, resp. klíčům s texty.
     */
    private final JCheckBox chcbAddComments;


    /**
     * Komponenty, které slouží pro výběr počtu volných řádků před komentářem.
     */
    private final JComboBox<Integer> cmbCountOfFreeLinesBeforeComment;


    /**
     * Panel, který obsahuje komponenty pro nastavení komentářů v souborech .properties, které obsahují texty pro
     * aplikaci.
     */
    private final JPanel pnlComments;


    /**
     * Panel, který obsahuje komponenty pro nastavení volných řádků kolem komentářů.
     */
    private final JPanel pnlFreeLinesAroundComments;


    /**
     * Komponenta pro zvolení objektu Locale neboli globálního umístění pro datumy v souborech .properties obsahující
     * texty pro tuto aplikaci.
     */
    private final JComboBox<LocaleItem> cmbLocale;


    /**
     * Konstruktor této třídy.
     */
    @SuppressWarnings("unchecked")
    public LanguagePanel() {
        setLayout(new GridBagLayout());

        final GridBagConstraints gbc = getGbc();

        index = 0;


        // Label - Jazyk
        lblLanguage = new JLabel();
        add(lblLanguage, gbc);


        // Výběr jazyka:
        cmbLanguage = new JComboBox<>(LANGUAGES.toArray(new LanguageInfo[]{}));
        setGbc(gbc, index, 1, cmbLanguage, this);


        // Label pro text způsob razení:
        lblSortingTextInPropFile = new JLabel();
        setGbc(gbc, ++index, 0, lblSortingTextInPropFile, this);


        // Výběr způsobu řazení textů v souborech .properties:
        cmbKindOfSortingTextInProp = new JComboBox<>();
        cmbKindOfSortingTextInProp.setRenderer(new ToolTipsComboBoxRenderer(cmbKindOfSortingTextInProp));
        // Přidám událost pro označení položky, kdy se zpřístupní / zněpřístupní panel s
        // komponentami pro vkládání komentářů:
        cmbKindOfSortingTextInProp.addActionListener(event -> checkSelectedKindOfSorting());
        setGbc(gbc, index, 1, cmbKindOfSortingTextInProp, this);


        // Tento panel je třeba inicializovat zde kvůli předání reference:
        pnlFreeLinesAroundComments = new JPanel(new GridBagLayout());
        final GridBagConstraints gbcLines = getGbc();

        // Chcb pro to, zda se mají přidat komentáře do souborů .properties s jazyky pro apliaci:
        chcbAddComments = new JCheckBox();
        chcbAddComments.addActionListener(
                event -> enableAllComponentsInPanel(pnlFreeLinesAroundComments, chcbAddComments.isSelected()));

        // Label pro text k cmb pro pořet volných řádků nad komentářem:
        lblCountOfFreeLinesBeforeComment = new JLabel();
        // Popisek pro Globálí umístění - Locale
        lblLocale = new JLabel();

        // Komponenta pro zvolení počtu volných řádku před komentářem:
        cmbCountOfFreeLinesBeforeComment = new JComboBox<>();

        // Komponenta pro zvolení globálního umístění - Locale:
        cmbLocale = new JComboBox<>(getLocalesModel());


        /*
         * Panel, který bude obsahovat komponenty pro nastavení toho, zda se mají
         * přidávat do souborů .properties komentáře a nastavení mezer (počet volných
         * řádků) před a za každým komentářem.
         */
        pnlComments = new JPanel(new GridBagLayout());
        final GridBagConstraints gbcComments = getGbc();

        // Přidání chcb pro přidat komentáře:
        setGbc(gbcComments, 0, 0, chcbAddComments, pnlComments);

        // Label a cmb pro počet volných řádků před komentářem:
        setGbc(gbcLines, 0, 0, lblCountOfFreeLinesBeforeComment, pnlFreeLinesAroundComments);
        setGbc(gbcLines, 0, 1, cmbCountOfFreeLinesBeforeComment, pnlFreeLinesAroundComments);

        // Zvolení Locale:
        setGbc(gbcLines, 1, 0, lblLocale, pnlFreeLinesAroundComments);
        setGbc(gbcLines, 1, 1, cmbLocale, pnlFreeLinesAroundComments);


        // Přidám panel s komponentami pro nastavení volných řádků kolem kmentářů do
        // panelu s komponentami pro nastavení komentářů
        setGbc(gbcComments, 1, 0, pnlFreeLinesAroundComments, pnlComments);


        // Přidám panel s komponentami pro vkládání komentářů do okna panelu
        gbc.gridwidth = 2;
        setGbc(gbc, ++index, 0, pnlComments, this);
    }


    /**
     * Metoda, která vytvoří model pro cmbLocale. Jedná se pouze o to, že se načtou z aktuálního JVM veškeré dostupné
     * Locales a pokud "jsou využitelné", tj. mají potřebné značky - indentifikátory pro uložení do souboru a název
     * apod. Pak se vloží do listu, coby model pro zmíněný cmb.
     *
     * @return Jednorozměré pole typu MyLocale, které slouží jako model pro cmb cmbLocale, ve kterém si uživatel může
     * zvolit Locale pro formát datumu v souborech .properties obsahující texty pro aplikaci v příslušných jazycích.
     */
    public static LocaleItem[] getLocalesModel() {
        final List<LocaleItem> localesList = new ArrayList<>();

        /*
         * Vyfiltrují se veškeré Locales, které jsou null, nejsou obsaženy pro konkrétní / využívaný JVM nebo jsou
         * nevalidní - prázdné. Pro zbytek se vytvoří možnosti zadání / nastavení v aplikaci.
         */
        Arrays.stream(DateFormat.getAvailableLocales()).filter(l -> l != null && !l.getDisplayName().replaceAll
                ("\\s", "").isEmpty()
                && LocaleItem.containsLocalesLanguageTag(l.toLanguageTag())).forEach(l -> localesList.add(new
                LocaleItem(l)));

        return localesList.toArray(new LocaleItem[]{});
    }


    /**
     * Metoda, která vrátí označenou položku pro Locale.
     *
     * @return Vrátí se označená položka v cmb cmbLocale, ze které se vezme přímo hodnota Locale, kterou daná položka v
     * daném cmb reprezentuje.
     */
    public final Locale getSelectedLocale() {
        return ((LocaleItem) cmbLocale.getSelectedItem()).getLocale();
    }


    /**
     * Metoda, která označí v komponentě JComboBox položku, která obsahuje jako Locale locale v parametru metody.
     * <p>
     * Pokud se v modelu JComboBoxu cmbLocale žádná taková položka, která by obsahovala daný locale nenašla, pak se nic
     * nenastaví (nemělo by nastat).
     *
     * @param locale
     *         - objekt Locale, který má obsahovat nějaká položka v modelu cmbLocale a ta se má označit.
     */
    public final void setSelectedCmbLocale(final Locale locale) {
        /*
         * Získám si index položky v modelu pro cmbLocale, která obsahuje locale v
         * parametru metody, které se má nastavit.
         *
         * Pokud nebude nalezeno, vrátí se jako index -1, ale k tomu by němělo dojít,
         * jedině v případě, že se ze souboru načte zadaný index, ale ten nebude
         * nalezen, pak by ale mělo dojít k vyjímce a ta by se měla obsloužit a vzít se
         * výchozí hodnoty, takže by se nikdy -1 v tomto indexu nacházet nemělo, ale
         * kdyby náhodou.
         */
        final int index = IntStream.range(0, cmbLocale.getModel().getSize())
                .filter(i -> cmbLocale.getModel().getElementAt(i).getLocale().equals(locale)).findFirst().orElse(-1);

        /*
         * Otestuji, zda byla nalezena položka pro dané Locale, jinak nic nenastavuji -
         * není co a výchozí hodnoty se řaší jinde.
         */
        if (index > -1)
            cmbLocale.setSelectedIndex(index);
    }


    /**
     * Metoda, která nastaví v komponentách cmbCountOfFreeLinesBeforeComment a cmbCountOfFreeLinesUnderComment označené
     * položky dle hodnot v Default.properties buď ve workspace nebo v adresáři aplikace, případně výchozí hodnoty.
     *
     * @param defaultProp
     *         - načtený soubor Default.properties z workspace nebo jako výchozí ale pořád nově vytvořený ve workspace.
     *         Z něj se vezmou potřebné hodnoty pro nastavení počtu volných řádků kolem komentářů.
     */
    public final void setSelectedItemToCmbForFreeLines(final Properties defaultProp) {
        final int countBeforeComment = Integer.parseInt(defaultProp.getProperty("Count of free lines before comment",
                String.valueOf(Constants.DEFAULT_COUNT_OF_FREE_LINES_BEFORE_COMMENT)));

        cmbCountOfFreeLinesBeforeComment.setSelectedIndex(countBeforeComment);
    }


    /**
     * Metoda, která vytvořeí jednorozměrné pole coby model pro komponentu cmbCountOfFreeLinesBeforeComment a nastaví
     * jednorozměrné pole jako model hodnot, který si může uživatel zvolit.
     */
    public final void setModelToCmbForFreeLines() {
        /*
         * Jednorozměrné pole coby model pro výše uvedené komponenty:
         */
        final Integer[] model = new Integer[101];

        for (int i = 0; i < 101; i++)
            model[i] = i;

        // Vložení modelu do komponenty:
        cmbCountOfFreeLinesBeforeComment.setModel(new DefaultComboBoxModel<>(model));
    }


    /**
     * Metoda, která vrátí označený počet volných řádků před komentářem v souborech .properties.
     *
     * @return - Počet volných řádků, kolik má být před komentářem v souborech .properties, které obsahují texty pro
     * tuto apliakci v nějakém jazyce.
     */
    public final int getSelectedCountOfFreeLinesBeforeComment() {
        return (int) cmbCountOfFreeLinesBeforeComment.getSelectedItem();
    }


    /**
     * Metoda, která vrátí objekt LanguageInfo, který je označen v komponentě cmbLanguage.
     *
     * @return vrátí označenou položku LanguageInfo v komponentě cmbLanguage.
     */
    public final LanguageInfo getSelectedLanguageInfo() {
        return (LanguageInfo) cmbLanguage.getSelectedItem();
    }


    /**
     * Metoda, která nastaví model pro komponentu cmbKindOfSortingTextInProp. Tento model obsahuje položky, které lze
     * zvolit v uvedené komponentě a obsahuje položky pro zvolení způsobu řazení textů - klíčů v souborech .properties,
     * které obsahují texty pro aplikaci.
     *
     * @param model
     *         - Jedná se o model po uvedenou komponentu cmbKindOfSortingTextinProp popsanou výše.
     */
    public final void setModelForCmbKindOfSorting(final ComboBoxModel<CmbItemAbstract> model) {
        cmbKindOfSortingTextInProp.setModel(model);
    }


    /**
     * Metoda, která vrátí označenou položku v komponentě JComboBox cmbKindOfSortingTextInProp, která slouží pro
     * označení způsobu řazení klíčů - textů v souborech .properties, které obsahují texty pro aplikaci.
     *
     * @return výše popsanou označenou položku v cmb pro označení způsobu řazení textů v souborech .properties s texty
     * pro aplikaci.
     */
    public final SortingInfo getSelectedKindOfSorting() {
        return (SortingInfo) cmbKindOfSortingTextInProp.getSelectedItem();
    }


    /**
     * Metoda, která označí / odznačí komponentu chcb, která značí, zda se mají vkládat komentáře do souborů
     * .properties, které obsahují texty pro aplikaci. A dle této hodnoty (selected) se zpřístupní nebo znepřístupní
     * panel, který obsahuje komponenty pro nastavení volných řádků kolem komentáře v souboru .properties.
     *
     * @param selected
     *         - logická hodnota o tom, zda se má nastavit chcb jakooznačený nebo odznačený, stejně tak komponenty pro
     *         nastavení volných řádků kolem komentáře. True -> chcb bude označen a panel s komponentami pro volné řádky
     *         kolem komentářů bude zpřístupněn. False -> chcb nebude označen a panel s komponentami pro volné řádky
     *         bude zněpřístupněn.
     */
    public final void setSelectedItemForAddComment(final boolean selected) {
        chcbAddComments.setSelected(selected);

        enableAllComponentsInPanel(pnlFreeLinesAroundComments, selected);
    }


    /**
     * Metoda, která vrátí logickou hodnotu o tom, zda je komponenta JCheckBox chcbAddComments označena nebo ne.
     * <p>
     * Jedná se o komponentu, dle které se pozná, zda se mají přidávat komentáře do souborů .properties s texty pro tuto
     * aplikace nebo ne.
     *
     * @return true, pokud je výše popsaný chcb označen, jinak false.
     */
    public final boolean isSelectedChcbAddComments() {
        return chcbAddComments.isSelected();
    }


    @Override
    public void setLanguage(final Properties properties) {
        if (properties != null) {
            lblLanguage.setText(properties.getProperty("Sf_App_LabelLanguage", Constants.SF_LANGUAGE) + ": ");

            lblSortingTextInPropFile.setText(properties.getProperty("Sf_LP_Lbl_Sorting_Text_In_Prop_File",
                    Constants.SF_LP_LBL_SORTING_TEXT_IN_PROP_FILE) + " '.properties': ");

            chcbAddComments.setText(properties.getProperty("Sf_LP_Chcb_Add_Comments",
                    Constants.SF_LP_CHCB_ADD_COMMENTS));

            lblCountOfFreeLinesBeforeComment.setText(properties.getProperty(
                    "Sf_LP_Lbl_Count_Of_Free_Lines_Before_Comment",
                    Constants.SF_LP_LBL_COUNT_OF_FREE_LINES_BEFORE_COMMENT) + ": ");

            lblLocale.setText("? " + properties.getProperty("Sf_LP_Lbl_Locale", Constants.SF_LP_LBL_LOCALE) + ": ");

            lblLocale.setToolTipText(properties.getProperty("Sf_LP_Lbl_Locale_Tt", Constants.SF_LP_LBL_LOCALE_TT));
        }

        else {
            lblLanguage.setText(Constants.SF_LANGUAGE + ": ");

            lblSortingTextInPropFile.setText(Constants.SF_LP_LBL_SORTING_TEXT_IN_PROP_FILE + " '.properties': ");

            chcbAddComments.setText(Constants.SF_LP_CHCB_ADD_COMMENTS);

            lblCountOfFreeLinesBeforeComment.setText(Constants.SF_LP_LBL_COUNT_OF_FREE_LINES_BEFORE_COMMENT + ": ");

            lblLocale.setText("? " + Constants.SF_LP_LBL_LOCALE + ": ");

            lblLocale.setToolTipText(Constants.SF_LP_LBL_LOCALE_TT);
        }
    }


    /**
     * Metoda, která nastaví komponenty v tomto panelu do výchozích hodnot, tzn. že se například označí výchozí jazyk
     * dle hodnoty ve třídě Constants, dále se nastaví výchozí způsob řazení, volné řádky apod.
     */
    public final void restoreDefaultSettings() {
        // Nastavím jako označený výchozí jazyk dle hodnoty v Constants:
        setLanguageById(Constants.DEFAULT_LANGUAGE_NAME);

        /*
         * Zjistím si index položky v modelu daného cmb pro způsob řazení textů v
         * souborech .properties. Najdu si hodnotu, která obsahuje výčet, který je
         * zvolen jako výchozí hodnota, a jeho index nastavím jako označený, zde neberu
         * případy, kdy se nenajde položka v modelu, jedině, že by se nenastavil samotný
         * model, což by nastat nemělo, akorát se může stát, že by uživatel mohl přepsat
         * texty, pak ale nebudou sedět akorát texty k položkám, jinak samotné (výčtové)
         * hodnoty do modelu se načítání v "kódu", neb ze souboru, takže tato možnost
         * by nikdy nastat neměla, ale kdyby z nějakého důvodu nastala, tak jako výchozí
         * hodnotu volím hodnotu 1, protože na tom indexu je ve výčtu KeysOrder způsob
         * řazení, který je v kódu zvolen jako výchozí. Tak v případě nějaké nouze by
         * byl nastaven tento způsob, ale nevím, jak by k tomu mohlo dojít, pouze jako
         * taková snad zbytečná záloha.
         */
        final int index =
                IntStream.range(0, cmbKindOfSortingTextInProp.getModel().getSize()).filter(k -> ((SortingInfo) cmbKindOfSortingTextInProp.getModel().getElementAt(k)).getKeysOrder() == Constants.DEFAULT_KEYS_ORDER).findFirst().orElse(1);
        cmbKindOfSortingTextInProp.setSelectedIndex(index);


        /*
         * Zde pouze nastavím označení chcb pro to, zda se mají nebo nemají přidávat
         * komentáře na výchozí hodnotu, která je v Constants:
         */
        chcbAddComments.setSelected(Constants.DEFAULT_VALUE_ADD_COMMENTS);
        /*
         * Zde potřebuji otestovat, zda jsou zpřístupněné položky pro nastavení volných
         * řádků kolem komentářů, pokud ne, pak je třeba je zpřístupnit (dle označené
         * položky v cmb pro způsob řazení textů a označeného chcb pro přidání komentářů
         * -> pro případ, že by někdo měnil hodnoty v Constatnts)
         */
        checkSelectedKindOfSorting();


        // Nastavím označenou hodnotu, která je jako výchozí v Constants:
        cmbCountOfFreeLinesBeforeComment.setSelectedItem(Constants.DEFAULT_COUNT_OF_FREE_LINES_BEFORE_COMMENT);


        // Nastavím jako označenou hodnotu pro globální umístění (Locale) tu, která je jako výchozí v Constants:
        setSelectedCmbLocale(Constants.DEFAULT_LOCALE_FOR_PROPERTIES);
    }


    /**
     * Metoda, která nastaví jako označený jazyk v příslušném cmb (cmbLanguage) jazyk, jehož název je v parametru této
     * metody, tedy v proměnné languageId, pokud takový jazyk nebude nalezen v poli s možnými jazyky, pak se využije ten
     * na prvním indexu v poli LANGUGE_ARRAY, což je výchozí jazyk - čeština.
     *
     * @param languageId
     *         - ID, resp. název jazyka, který se má nastavit jako označený v výše uvedeném cmb.
     */
    public final void setLanguageById(final String languageId) {
        /*
         * Zjistím si index položky, která slouží jako potřebný jazyk (languageId),
         * pokud se takový nenajde - nemělo by nastat, pak se využije výchozí jazyk,
         * který je na indexu nula (první položka) v poli LANGUAGE_ARRAY - čeština.
         *
         * Možnost, že by se nenašel v textu ten jazyk je jedině ten, že by uživatel sám
         * zadal nějaký "nesmyslný" text pro jazyk pak se využije ten první jazyk v
         * poli, který je již v kódu definován jako čeština, proto pokud uživatel nebude
         * manipulovat i s kódem aplikace, pak se využije ta četina, nebo zvolený jazyk.
         */
        final int index = IntStream.range(0, LANGUAGES.size())
                .filter(i -> LANGUAGES.get(i).getLanguageId().equalsIgnoreCase(languageId)).findFirst().orElse(0);

        cmbLanguage.setSelectedIndex(index);
    }


    /**
     * Metoda, která v komponentě cmb cmbKindOfSortingTextInProp nastaví označenou položku dle nastaveného způsobu
     * řazení, který se načte ze souboru Default.properties (buď z workspace nebo z aplikace, popřípdě jako výchozí
     * hodnota).
     *
     * @param cmbKindOfSortingModel
     *         - jednorozměrné pole coby model pro komponentu cmbKindOfSortingTextInProp, ve které se označí položka na
     *         nalezeném indexu.
     */
    public final void setSelectedItemForSortingText(final SortingInfo[] cmbKindOfSortingModel) {
        /*
         * Nyní, si načtu Default.properties buď z workspace nebo z aplikace, dle toho,
         * zda se načtení z workspace povede nebo ne. A zjistím si označený způsob
         * řazení pro texty v souborech .properties s texty pro aplikaci v příslušných
         * jazycích a nastavím jej jako označnou položku.
         */
        final KeysOrder choosedKeysOrder = ReadFile.getKeysOrder();

        /*
         * Zjistím si index zvoleného způsobu řazení položek v souborech .properties,
         * který uživatel zvolil a nachází se tak v výše uvedeném modelu
         * cmbKindOfSortignModel.
         *
         * Projde výše uvedený model a zjistím si index položky, která obsahuje výše
         * získaná způsob řazení.
         *
         * Pokud se nenajde položky ze souboru Default.properties, využije se výchozí,
         * ale pokud by se ani z nějakého důvodu ani to nestalo, což mě absolutně
         * nenapadá důvod, jak by to mohlo nastat (bez zásahu do zdrojového kódu), tak
         * se v následujícím cyklu vrátí jako výchozí hodnota na index 1, což je způsob
         * indexování vzestupně dle přidání.
         */
        final int index = IntStream.range(0, cmbKindOfSortingModel.length)
                .filter(i -> choosedKeysOrder == cmbKindOfSortingModel[i].getKeysOrder()).findFirst().orElse(1);

        // Nastavím označenou položku na výše získaném indexu:
        cmbKindOfSortingTextInProp.setSelectedIndex(index);


        // Zjistím si označenou položku a dle toho zpřístupním / znepřístupním panel pro
        // vkládání komentářů:
        checkSelectedKindOfSorting();
    }


    /**
     * Metoda, která zjistí označenou položku v komponentě cmbKindOfSortingTextInProp, resp. zjistí, jaký je zvolen
     * způsob řazení a pokud se jedná o způsob řazení dle pořadí přidání (vzestupně nebo sestupně), pak se zpřístupní
     * panel pro nastavení komentářů, jinak se tento panel pro nastavení komentřů znepřístupní.
     * <p>
     * Komentáře lze přidat pouze v případě, že se jedná o řazení textů - klíčů dle indexu / pořadí přidání sestupně
     * nebo vzestupně. V ostatních případěch by se komentář přidal pouze k jednomu zvoleném klíči v souboru, ale zbytek
     * textů - klíčů, které spolu souvisí, tj. jedná se o texty například do stejného menu, akorát pro zbytek tlačítek
     * apod.
     */
    private void checkSelectedKindOfSorting() {
        /*
         * Zjistím si označenou položku v komponentě pro výběr způsobu řazení klíčů v
         * souborech .properties s texty pro aplikaci v příslušném jazyce.
         */
        final SortingInfo si = (SortingInfo) cmbKindOfSortingTextInProp.getSelectedItem();

        /*
         * Pokud je zvolena položka pro sestupně nebo vzestupné řazení klíčů dle pořadí
         * přidání, pak se panel s komponentami pro přidání komentářů zpřístupní, pokud
         * se jedná o jiný způsob řazení, panel s komponentami pro vkládání komentářů
         * bude zněpřístupněn.
         */
        if (si.getKeysOrder() == KeysOrder.ASCENDING_BY_ADDING || si.getKeysOrder() == KeysOrder.DESCENDING_BY_ADDING) {
            enableAllComponentsInPanel(pnlComments, true);

            /*
             * V tomto případě je zpřístupněn panel pro nastavení toho, zda se mají přidávat
             * komentáře do souborů .properties s texty pro aplikaci nebo ne.
             *
             * Ale když tyto komponenty zpřístupním, pak musím dále otestovat, zda uživatel
             * zvolil, zda se mají zapisovat nebo ne. Pokud ne, pak je chcb chcbAddComments
             * neoznačen a musím tak znepřístupnit komponenty v panelu
             * pnlFreeLinesAroundComments pro nastavení počtu volných řádků kolem komentářů.
             */
            if (!chcbAddComments.isSelected())
                enableAllComponentsInPanel(pnlFreeLinesAroundComments, false);
        }

        else
            enableAllComponentsInPanel(pnlComments, false);
    }


    /**
     * Metoda, která zpřístupní / zněpřístupní (dle hodnoty enable) veškeré komponenty, které se nachází v komponentě
     * Jpanel panel.
     *
     * @param panel
     *         - Jpanel, jehož komponenty se mají zpřístupnit / zněpřístupnit (dle hodnoty enable)
     * @param enable
     *         - logická hodnota o tom, zda se mají veškeré hodnoty v komponentě panel zpřístupnit nebo znepřístupnit.
     *         True -> veškeré komponenty v panel se zpřístupní, false -> veškeré komponenty v panel se znepřístupní.
     */
    private static void enableAllComponentsInPanel(final JPanel panel, final boolean enable) {
        Arrays.stream(panel.getComponents()).forEach(c -> {
            if (c instanceof JPanel)
                enableAllComponentsInPanel((JPanel) c, enable);

            else
                c.setEnabled(enable);
        });
    }


    /**
     * Metoda, která vytvoří model pro komponentu cmb cmbKindOfSortingTextInProp a vrátí jej.
     *
     * @param sortingTextArray
     *         - Pole obsahující položky pro texty do výše uvedené cmb komponenty.
     * @param sortingTtArray
     *         - Pole obsahující položky pro popisky způsobu řazení (popisky pro položky v poli sortingTextArray).
     *
     * @return model pro komponentu cmbKindOfSortingTextInProp.
     */
    public static SortingInfo[] getCmbKindOfSortingModel(String[] sortingTextArray, String[] sortingTtArray) {
        /*
         * Pro jistotu otestuji, zda se shodují velikost obou polí (sortingTextArray a
         * sortingTtArray) a tyto velikosti jsou stejné, jako výchozí délka polí ve
         * třídě Constants. Pokud tomu tak není, pak uživatel zadal chybný počet
         * položek, proto se využije výchozí.
         */
        if (sortingTextArray.length != sortingTtArray.length
                || sortingTextArray.length != Constants.SORTING_TEXT_IN_PROP_FILE_MODEL.length) {
            sortingTextArray = Constants.SORTING_TEXT_IN_PROP_FILE_MODEL;
            sortingTtArray = Constants.SORTING_TEXT_IN_PROP_FILE_TT_MODEL;
        }



        /*
         * Nyní cyklem projdu obě pole a vytvořím tak model objektů pro komponentu
         * cmbKindOfSortingTextInProp, ze které si uživatel bude moci vybírat.
         *
         * Note: Je nezbytné, aby texty byly ve stejném pořadí, jako v Constants, resp.
         * jako ve výčtu KeysOrder, protože se tam cyklem načítají ty položky i z
         * uvedeného výčtu v pořadí, v jakém se v tom výčtu nacházejí.
         */
        final SortingInfo[] sortingInfoModel = new SortingInfo[sortingTextArray.length];

        for (int i = 0; i < sortingInfoModel.length; i++)
            sortingInfoModel[i] = new SortingInfo(KeysOrder.values()[i], sortingTextArray[i], sortingTtArray[i]);


        // Vrátím výše vytvořený model:
        return sortingInfoModel;
    }
}
