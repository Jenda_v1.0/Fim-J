package cz.uhk.fim.fimj.settings_form.instance_diagram_panels;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.language.EditProperties;
import cz.uhk.fim.fimj.settings_form.ReadWriteDiagramProperties;
import cz.uhk.fim.fimj.settings_form.class_diagram_panels.CellPanelAbstract;

/**
 * Třída coby panel, který obsahuje komponenty, pro nastavení některých parametrů pro buňku, která v diagramu instancí
 * (v hlavním okně aplikace vpravo) reprezentuje instanci nějaké třídy.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ClassCellPanel extends CellPanelAbstract
		implements LanguageInterface, ActionListener, ReadWriteDiagramProperties {

	private static final long serialVersionUID = 1L;
	

	
	private static Color clrBorderColor, bgCellColor;
	private final JButton btnChooseBorderColor, btnChooseBgCellColor;
	private final JLabel lblChoosedBorderColor, lblChoosedBgCellColor, lblBorderColor, lblRoundedBorder;
	private static String txtChhoseBorderColor, txtChoosedBgCellColor;
	
	private final JComboBox<Integer> cmbRoundedBorder;
			
	
	
	
	/**
	 * Konstruktor této třídy.
	 */
	public ClassCellPanel() {
		super();
		
		setLayout(new GridBagLayout());
		
		
		// Vytvořím si instanci:
		final GridBagConstraints gbc = getGbc();
		
		index = 0;
		
		
		gbc.gridwidth = 3;
		
		// JCheckBox pro označení, zda se mají zobrazit balíčky v buňce coby
		// instance třídy.
		chcbShowPackages = new JCheckBox();
		setGbc(gbc, index, 0, chcbShowPackages, this);
		
		
		
		
		gbc.gridwidth = 1;
		
		
		lblFontSize = new JLabel();
		setGbc(gbc, ++index, 0, lblFontSize, this);
		
		cmbFontSize = new JComboBox<>(getArrayFontSizes());
		setGbc(gbc, index, 1, cmbFontSize, this);
		
		
		
		
		lblFontStyle = new JLabel();
		setGbc(gbc, ++index, 0, lblFontStyle, this);
		
		cmbFontStyle = new JComboBox<>(FONT_STYLES.toArray(new String[] {}));
		setGbc(gbc, index, 1, cmbFontStyle, this);
		
		
		
		
		lblTextAlignmentX = new JLabel();
		setGbc(gbc, ++index, 0, lblTextAlignmentX, this);
		
		cmbTextAlignmentX = new JComboBox<>(ARRAY_ALIGNMENT_X.toArray(new String[] {}));
		setGbc(gbc, index, 1, cmbTextAlignmentX, this);
		
		
		
		
		lblTextAlignmentY = new JLabel();
		setGbc(gbc, ++index, 0, lblTextAlignmentY, this);
		
		cmbTextAlignmentY = new JComboBox<>(ARRAY_ALIGNMENT_Y.toArray(new String[] {}));
		setGbc(gbc, index, 1, cmbTextAlignmentY, this);
		
		
		
		
		lblTextColor = new JLabel();
		setGbc(gbc, ++index, 0, lblTextColor, this);
		
		btnChooseColor = new JButton();
		btnChooseColor.addActionListener(this);
		setGbc(gbc, index, 1, btnChooseColor, this);
		
		lblChooseColorText = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChooseColorText.setOpaque(true);
		setGbc(gbc, index, 2, lblChooseColorText, this);
		
		
		
		
		
		
		lblBgColor = new JLabel();
		setGbc(gbc, ++index, 0, lblBgColor, this);
		
		btnChooseBgCellColor = new JButton();
		btnChooseBgCellColor.addActionListener(this);
		setGbc(gbc, index, 1, btnChooseBgCellColor, this);
		
		lblChoosedBgCellColor = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChoosedBgCellColor.setOpaque(true);
		setGbc(gbc, index, 2, lblChoosedBgCellColor, this);
		
		
		
		
		
		lblBorderColor = new JLabel();
		setGbc(gbc, ++index, 0, lblBorderColor, this);
		
		btnChooseBorderColor = new JButton();
		btnChooseBorderColor.addActionListener(this);
		setGbc(gbc, index, 1, btnChooseBorderColor, this);
		
		lblChoosedBorderColor = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChoosedBorderColor.setOpaque(true);
		setGbc(gbc, index, 2, lblChoosedBorderColor, this);
		
		
		
		
		lblRoundedBorder = new JLabel();
		setGbc(gbc, ++index, 0, lblRoundedBorder, this);
		
		cmbRoundedBorder = new JComboBox<>(getAngles());
		setGbc(gbc, index, 1, cmbRoundedBorder, this);
	}
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí pole tyu Integer o hodnotách v rozsahu 1 - 60 slouží jako
	 * výběr úhlu zaoblení hran pro buňku v grafu, která reprezentuje instanci třídy
	 * 
	 * @return jednorozměrné ple typu Integer s hodnotami v rozsahu 1 - 60
	 */
	private static Integer[] getAngles() {
		final Integer[] arrayOfAngles = new Integer[60];
		
		for (int i = 1; i <= 60; i++)
			arrayOfAngles[i - 1] = i;
		
		return arrayOfAngles;
	}
	
	
	


	
	
	@Override
	public void readDataFromDiagramProp(final Properties instanceDiagramPro) {		
		final boolean showPackagesInCell = Boolean.parseBoolean(instanceDiagramPro.getProperty("ShowPackagesInCell", String.valueOf(Constants.ID_SHOW_PACKAGES_IN_CELL)));
		chcbShowPackages.setSelected(showPackagesInCell);
		
		
		final String textColor = instanceDiagramPro.getProperty("TextColor", Integer.toString(Constants.ID_TEXT_COLOR.getRGB()));
		clrSelectedTextColor = new Color(Integer.parseInt(textColor));
		lblChooseColorText.setBackground(clrSelectedTextColor);
		lblChooseColorText.setForeground(clrSelectedTextColor);
		
					
		// Barva ohraničení a barvy buňky:
		final String borderColor = instanceDiagramPro.getProperty("BorderColor", Integer.toString(Constants.ID_COLOR_BORDER.getRGB()));
		clrBorderColor = new Color(Integer.parseInt(borderColor));
		lblChoosedBorderColor.setBackground(clrBorderColor);
		lblChoosedBorderColor.setForeground(clrBorderColor);
		
		final String bgColor = instanceDiagramPro.getProperty("CellColor", Integer.toString(Constants.ID_COLOR_CELL.getRGB()));
		bgCellColor = new Color(Integer.parseInt(bgColor));
		lblChoosedBgCellColor.setBackground(bgCellColor);
		lblChoosedBgCellColor.setForeground(bgCellColor);
		
		
		
		cmbFontSize.setSelectedItem(Integer.parseInt(instanceDiagramPro.getProperty("FontSize", Integer.toString(Constants.ID_FONT.getSize()))));
		
		
		final int fontStyle = Integer.parseInt(instanceDiagramPro.getProperty("FontStyle", Integer.toString(Constants.ID_FONT.getStyle())));
		
		if (fontStyle == 0)		// Plain
			cmbFontStyle.setSelectedIndex(2);
		else if (fontStyle == 1)	// Bold
			cmbFontStyle.setSelectedIndex(0);
		else if (fontStyle == 2)	// Italic
			cmbFontStyle.setSelectedIndex(1);
		
		
		// Zarovnání:
		/*
		 * center: 0
		 * left: 2
		 * right: 4
		 * top: 1
		 * bottom: 3
		 */
		final int verticalAlignment = Integer.parseInt(instanceDiagramPro.getProperty("TextVerticalAlignment", String.valueOf(Constants.ID_VERTICAL_ALIGNMENT)));
		
		if (verticalAlignment == 1)	// Top			
			cmbTextAlignmentY.setSelectedIndex(0);
		else if (verticalAlignment == 0) 	// Center
			cmbTextAlignmentY.setSelectedIndex(1);
		else if (verticalAlignment == 3)	// Bottom
			cmbTextAlignmentY.setSelectedIndex(2);
			
		
		final int horizontalAlignment = Integer.parseInt(instanceDiagramPro.getProperty("TextHorizontalAlignment", String.valueOf(Constants.ID_HORIZONTAL_ALIGNMENT)));
		
		if (horizontalAlignment == 2)	// Left
			cmbTextAlignmentX.setSelectedIndex(0);
		else if (horizontalAlignment == 0)	// Center
			cmbTextAlignmentX.setSelectedIndex(1);
		else if (horizontalAlignment == 4)	// Right
			cmbTextAlignmentX.setSelectedIndex(2);
		
		
		// Zaoblení hran:
		final int roundedBorder = Integer.parseInt(instanceDiagramPro.getProperty("RoundedBorderMargin", Integer.toString(Constants.ID_ROUNDED_BORDER_MARGIN)));
		
		if (roundedBorder >= 1 && roundedBorder <= 60)
			cmbRoundedBorder.setSelectedItem(roundedBorder);
	}

	
	
	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			setBorder(BorderFactory.createTitledBorder(properties.getProperty("Sf_Id_ClassCellPanelBorderTitle", Constants.SF_ID_PNL_CLASS_CELL_BORDER_TITLE)));
			
			chcbShowPackages.setText(properties.getProperty("Sf_Id_ShowPackagesInCell", Constants.SF_ID_SHOW_PACKAGES_IN_CELL));
			
			
			// Texty do labelů pro barvu pozadí buňky a její ohraničení
			lblBorderColor.setText(properties.getProperty("Sf_Id_LabelChooseBorderColor", Constants.SF_ID_LBL_BORDER_COLOR) + ": ");
			lblBgColor.setText(properties.getProperty("Sf_Id_LabelChooseCellBackgroundColor", Constants.SF_ID_LBL_BG_COLOR) + ": ");
			
			// Text do tlačítka pro výběr barvy pozadí a ohraničení
			btnChooseBorderColor.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));
			btnChooseBgCellColor.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));
			
			
			// Texty do dialogu pro výběrbarvy pozadí a ohraničení
			txtChhoseBorderColor = properties.getProperty("Sf_Id_TextToDialogChooseBorderColor", Constants.SF_ID_DIALOG_CHOOSE_BORDER_COLOR);
			txtChoosedBgCellColor = properties.getProperty("Sf_Id_TextToDialogChooseBackgroundColor", Constants.SF_ID_DIALOG_CHOOSE_BG_CELL_COLOR);
			
			
			lblFontSize.setText(properties.getProperty("Sf_Cd_LabelFontSizeClassCell", Constants.SF_LBL_FONT_SIZE_CELL) + ": ");
			lblFontStyle.setText(properties.getProperty("Sf_Cd_LabelFontStyleClassCell", Constants.SF_LBL_FONT_STYLE_CELL) + ": ");
			lblTextAlignmentX.setText(properties.getProperty("Sf_Cd_LabelTextAlignent_X_ClassCell", Constants.SF_LBL_TEXT_ALIGNMENT_X_CELL) + ": ");
			lblTextAlignmentY.setText(properties.getProperty("Sf_Cd_LabelTextAlignment_Y_ClassCell", Constants.SF_LBL_TEXT_ALIGNMENT_Y_CELL) + ": ");
			lblTextColor.setText(properties.getProperty("Sf_Cd_LabelTextColorClassCell", Constants.SF_LBL_TEXT_COLOR_CELL) + ": ");
			
			lblRoundedBorder.setText(properties.getProperty("Sf_Id_LabelRoundedBorder", Constants.SF_ID_LBL_RONDED_BORDER) + ": ");
			
			btnChooseColor.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));
			
			txtChooseTextColor = properties.getProperty("Sf_Cd_ChooseTextColorDialog", Constants.SF_TXT_CHOOSE_TEXT_COLOR);
		}
		
		
		else {
			setBorder(BorderFactory.createTitledBorder(Constants.SF_ID_PNL_CLASS_CELL_BORDER_TITLE));
			
			chcbShowPackages.setText(Constants.SF_ID_SHOW_PACKAGES_IN_CELL);
			
			lblFontSize.setText( Constants.SF_LBL_FONT_SIZE_CELL + ": ");
			lblFontStyle.setText(Constants.SF_LBL_FONT_STYLE_CELL + ": ");
			lblTextAlignmentX.setText(Constants.SF_LBL_TEXT_ALIGNMENT_X_CELL + ": ");
			lblTextAlignmentY.setText(Constants.SF_LBL_TEXT_ALIGNMENT_Y_CELL + ": ");
			lblTextColor.setText(Constants.SF_LBL_TEXT_COLOR_CELL + ": ");
			
			lblRoundedBorder.setText(Constants.SF_ID_LBL_RONDED_BORDER + ": ");
			
			btnChooseColor.setText(Constants.SF_CD_BTN_CHOOSE_COLOR);	
			
			// Budou v těchto panelech stejné:
			txtChooseTextColor = Constants.SF_TXT_CHOOSE_TEXT_COLOR;
			
			// Texty do labelů pro barvu pozadí buňky a její ohraničení
			lblBorderColor.setText(Constants.SF_ID_LBL_BORDER_COLOR + ": ");
			lblBgColor.setText(Constants.SF_ID_LBL_BG_COLOR + ": ");
			
			// Text do tlačítka pro výběr barvy pozadí a ohraničení
			btnChooseBorderColor.setText(Constants.SF_CD_BTN_CHOOSE_COLOR);
			btnChooseBgCellColor.setText(Constants.SF_CD_BTN_CHOOSE_COLOR);
			
			
			// Texty do dialogu pro výběrbarvy pozadí a ohraničení
			txtChhoseBorderColor = Constants.SF_ID_DIALOG_CHOOSE_BORDER_COLOR;
			txtChoosedBgCellColor = Constants.SF_ID_DIALOG_CHOOSE_BG_CELL_COLOR;
		}
	}




	
	
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnChooseColor) {				
				// Nejprve si zvolenou barvu uložím do dočasné proměnné - pouze v metodě, prtože 
				// když uživatel dialog zruši barby by byla null a následně by se uložila do souboru, což je chyba
				final Color tempColor = JColorChooser.showDialog(this, txtChooseTextColor, Constants.ID_TEXT_COLOR);
				
				if (tempColor != null) {
					clrSelectedTextColor = tempColor;
					lblChooseColorText.setForeground(clrSelectedTextColor);
					lblChooseColorText.setBackground(clrSelectedTextColor);	
				}
			}
			
			
			
			else if (e.getSource() == btnChooseBgCellColor) {
				final Color tempColor = JColorChooser.showDialog(this, txtChoosedBgCellColor, Constants.ID_COLOR_CELL);
				
				if (tempColor != null) {
					bgCellColor = tempColor;
					lblChoosedBgCellColor.setForeground(bgCellColor);
					lblChoosedBgCellColor.setBackground(bgCellColor);
				}
			}
			
			
			
			else if (e.getSource() == btnChooseBorderColor) {
				final Color tempColor = JColorChooser.showDialog(this, txtChhoseBorderColor, Constants.ID_COLOR_BORDER);
				
				if (tempColor != null) {
					clrBorderColor = tempColor;
					lblChoosedBorderColor.setForeground(clrBorderColor);
					lblChoosedBorderColor.setBackground(clrBorderColor);
				}
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	@Override
	public EditProperties writeDataToDiagramProp(final EditProperties instanceDiagramProp) {
		instanceDiagramProp.setProperty("ShowPackagesInCell", String.valueOf(chcbShowPackages.isSelected()));
		
		
		// Barva textu:
		if (clrSelectedTextColor != null)
			instanceDiagramProp.setProperty("TextColor", Integer.toString(clrSelectedTextColor.getRGB()));
		
		// Barva ohraniceni:
		if (clrBorderColor != null)
			instanceDiagramProp.setProperty("BorderColor", Integer.toString(clrBorderColor.getRGB()));
		
		// Barva bunky:
		if (bgCellColor != null)
			instanceDiagramProp.setProperty("CellColor", Integer.toString(bgCellColor.getRGB()));
		
		instanceDiagramProp.setProperty("FontSize", Integer.toString((Integer) cmbFontSize.getSelectedItem()));
		
		
		final int verticalAlignment = cmbTextAlignmentY.getSelectedIndex();
		
		if (verticalAlignment == 0)
			instanceDiagramProp.setProperty("TextVerticalAlignment", Integer.toString(1));
		else if (verticalAlignment == 1)
			instanceDiagramProp.setProperty("TextVerticalAlignment", Integer.toString(0));
		else if (verticalAlignment == 2)
			instanceDiagramProp.setProperty("TextVerticalAlignment", Integer.toString(3));
		
		
		
		
		final int horizontalAlignment = cmbTextAlignmentX.getSelectedIndex();
		
		if (horizontalAlignment == 0)
			instanceDiagramProp.setProperty("TextHorizontalAlignment", Integer.toString(2));
		else if (horizontalAlignment == 1)
			instanceDiagramProp.setProperty("TextHorizontalAlignment", Integer.toString(0));
		else if (horizontalAlignment == 2)
			instanceDiagramProp.setProperty("TextHorizontalAlignment", Integer.toString(4));
		
		
		final int fontStyle = cmbFontStyle.getSelectedIndex();
		
		if (fontStyle == 0)
			instanceDiagramProp.setProperty("FontStyle", Integer.toString(1));
		else if (fontStyle == 1)
			instanceDiagramProp.setProperty("FontStyle", Integer.toString(2));
		else if (fontStyle == 2)
			instanceDiagramProp.setProperty("FontStyle", Integer.toString(0));
		
		
		// Zaoblení hran:
		final int roundedBorder = (Integer) cmbRoundedBorder.getSelectedItem();
		instanceDiagramProp.setProperty("RoundedBorderMargin", Integer.toString(roundedBorder));
		
		return instanceDiagramProp;
	}
	
	
	
	public final void setRoundedBorder(final int margin) {
		cmbRoundedBorder.setSelectedItem(margin);
	}
	
	
	public final void setColorCell(final Color color) {
		bgCellColor = color;
		lblChoosedBgCellColor.setForeground(bgCellColor);
		lblChoosedBgCellColor.setBackground(bgCellColor);
	}
	
	public final void setColorBorder(final Color color) {
		clrBorderColor = color;
		lblChoosedBorderColor.setForeground(clrBorderColor);
		lblChoosedBorderColor.setBackground(clrBorderColor);
	}
}