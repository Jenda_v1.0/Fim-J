package cz.uhk.fim.fimj.settings_form.class_diagram_panels;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.class_diagram.KindOfCodeEditor;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.language.EditProperties;
import cz.uhk.fim.fimj.settings_form.PanelAbstract;
import cz.uhk.fim.fimj.settings_form.ReadWriteDiagramProperties;
import cz.uhk.fim.fimj.settings_form.cmb_items_with_tooltip.CmbItemAbstract;
import cz.uhk.fim.fimj.settings_form.cmb_items_with_tooltip.CodeEditorInfo;
import cz.uhk.fim.fimj.settings_form.cmb_items_with_tooltip.ToolTipsComboBoxRenderer;

/**
 * Třída slouží jako panel pro nastavení vlastnosti grafů - class diagramu
 * <p>
 * vlastnosti jako: barva pozadi grafu, zda lze popisky od hran odpojit a přiblížení - Scale
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ClassDiagramGraphPanel extends PanelAbstract
		implements LanguageInterface, ActionListener, ReadWriteDiagramProperties {

	private static final long serialVersionUID = 2950617551940565538L;


	
	private final JCheckBox chcbConDisConLabelFromEdge;
	
	private final JLabel lblBgColor;
    private final JLabel lblScale;
    private final JLabel lblChoosedBgColor;
	
	
	/**
	 * Prměnná do titulku dialogu pro výběr barvy pozadí:
	 */
	private String txtBgColor;
	
	
	/**
	 * Promenná pro uložení zvolené barvy pozadí:
	 */
	private Color choosedBgColor;
	
	
	/**
	 * Tlačítko pro výběr barvy pozadí
	 */
	private final JButton btnChooseBgColor;
	
	
	/**
	 * Combobox pro výběr přiblížení grafu:
	 */
	private final JComboBox<Double> cmbScale;
	
	
	
	/**
	 * Komponenta, která slouží pro nastavení toho, zda se budou zobrazovat vztahy
	 * třídy na sebe sama v diagramu tříd.
	 */
	private final JCheckBox chcbShowRelationShipsToClassItself;


	/**
	 * Komponenta, která slouží pro nastavení toho, zda se v kontextovém menu nad třídou v diagramu tříd mají
	 * zpřístupnit i soukromé konstruktory v označené třídě.
	 */
	private final JCheckBox chcbIncludePrivateConstructors;

	/**
	 * Komponenta, která slouží pro nastavení toho, zda se v kontextovém menu nad třídou v diagramu tříd mají
	 * zpřístupnit i chráněné konstruktory v označené třídě.
	 */
	private final JCheckBox chcbIncludeProtectedConstructors;

	/**
	 * Komponenta, která slouží pro nastavení toho, zda se v kontextovém menu nad třídou v diagramu tříd mají
	 * zpřístupnit i package-private konstruktory v označené třídě.
	 */
	private final JCheckBox chcbIncludePackagePrivateConstructors;



	/**
	 * Komponenta, která slouží pro nastavení toho, zda se v kontextovém menu nad třídou v diagramu tříd mají
	 * zpřístupnit i soukromé metody označené třídy.
	 */
	private final JCheckBox chcbIncludePrivateMethods;

	/**
	 * Komponenta, která slouží pro nastavení toho, zda se v kontextovém menu nad třídou v diagramu tříd mají
	 * zpřístupnit i chráněné metody označené třídy.
	 */
	private final JCheckBox chcbIncludeProtectedMethods;

	/**
	 * Komponenta, která slouží pro nastavení toho, zda se v kontextovém menu nad třídou v diagramu tříd mají
	 * zpřístupnit i chráněné metody označené třídy.
	 */
	private final JCheckBox chcbIncludePackagePrivateMethods;


	/**
	 * Komponenta, která slouží pro nastavení toho, zda se v kontextovém menu nad třídou v diagramu tříd mají
	 * zpřístupnit i statické soukromé vnitřní třídy.
	 */
	private final JCheckBox chcbIncludePrivateInnerStaticClasses;
	/**
	 * Komponenta, která slouží pro nastavení toho, zda se v kontextovém menu nad třídou v diagramu tříd mají
	 * zpřístupnit i statické chráněné vnitřní třídy.
	 */
	private final JCheckBox chcbIncludeProtectedInnerStaticClasses;
	/**
	 * Komponenta, která slouží pro nastavení toho, zda se v kontextovém menu nad třídou v diagramu tříd mají
	 * zpřístupnit i statické package-private vnitřní třídy.
	 */
	private final JCheckBox chcbIncludePackagePrivateInnerStaticClasses;





	/**
	 * Komponenta, která slouží pro nastavení toho, zda se v kontextovém menu nad třídou v diagramu tříd v nabídce
	 * "Vnitřní statické třídy" mají zpřístupnit pro zavolání i soukromé statické metody.
	 */
	private final JCheckBox chcbInnerStaticClassesIncludePrivateStaticMethods;

	/**
	 * Komponenta, která slouží pro nastavení toho, zda se v kontextovém menu nad třídou v diagramu tříd v nabídce
	 * "Vnitřní statické třídy" mají zpřístupnit pro zavolání i chráněné statické metody.
	 */
	private final JCheckBox chcbInnerStaticClassesIncludeProtectedStaticMethods;

	/**
	 * Komponenta, která slouží pro nastavení toho, zda se v kontextovém menu nad třídou v diagramu tříd v nabídce
	 * "Vnitřní statické třídy" mají zpřístupnit pro zavolání i package-private statické metody.
	 */
	private final JCheckBox chcbInnerStaticClassesIncludePackagePrivateStaticMethods;



	
	
	
	
	/**
	 * Proměnná, která slouží pro nastavení toho, zda se má při zavolání
	 * konstruktoru pomocí grafického rozhraní, tj. pomocí kontextového menu nad
	 * třídou v diagramutříd a zvolit požadovaný konstruktor. V diaogu pro zavolání
	 * konstruktoru se pro zadání parametru nastaví komponenta JComboBox, která
	 * umožňuje vždy na první pozici zadat text od uživatele a veškeré ostaní
	 * položky v příslušném cmb jsou získané proměnné stejného datového typu jako
	 * příslušný parametr konstruktoru. proměnné jsou získány staticé z tříd v
	 * diagramu tříd, ostatní z vytvořených instancí v diagramu instancí a proměnné,
	 * které uživatel vytvořil pomocí editoru příkazů ("dočasné proměnné").
	 */
	private final JCheckBox chcbMakeAvailableFieldsForCallConstructor;
	
	
	
	/**
	 * Komponenta, kterou slouží pro nastavení toho, zda se mají v dialogu pro
	 * zavolání metody zpřístupnit veřejné a chráněné proměnné tříd z diagramu tříd,
	 * instnací v diagramu instancí a proměnné vytvořené uživatelem v editoru
	 * příkazů. Tyto proměnné vždy stejného datového typu jako je parametr metody
	 * budou zpřístupněné a bude možné je předat do parametru příslušé metody místo
	 * zadání vlastní hodnoty.
	 */
	private final JCheckBox chcbMakeAvailableFieldsForCallMethod;
	
	
	
	
	
	
	/**
	 * Komponenta, která slouží pro nastavení toho, zda se má při volání
	 * konstruktoru pomocí kontextového menu nad třídou v diagramu tříd zpřístupnit
	 * možnost pro vytvoření nové instance. V případě, že uživatelem zvolaneý
	 * konstruktor pro zavolání obsahuje parametr, který je typu nějaké třídy, která
	 * se nachází v diagramu tříd, tak bude na výběr navíc možnost vytvořit novou
	 * instanci té třídy, nebo předat jen instance, které se aktuálně nachází v
	 * diagramu instancí, ale pokud se v diagramu instancí, žádná instance příslušné
	 * třídy nebude nacházet, konstruktor nepůjde zavolat.
	 */
	private final JCheckBox chcbShowCreateNewInstanceOptionInCallConstructor;
	
	
	/**
	 * Tato komponenta slouží pro nastavení toho, zda se má při zavolání metody
	 * pomocí kontextového menu nad třídou v diagramu tříd zpřístupnit možnost pro
	 * vytvoření nové instance v případě, že uživatelem vybraná metoda pro zavolání
	 * obsahuje parametr typu nějaké třídy, která se nachází v diagramu tříd. Pak
	 * lze do parametru této metody předat veškeré instance příslušné třídy, které
	 * se aktuálně nachází v diagramu instancí a pokud bude tato možnost true, pak
	 * bude na výběr i možnost pro vytvoření nové instance příslušné třídy, která se
	 * předá do parametru příslušné metody. Jinak, pokud bude tato možnost false,
	 * pak nebude možnost pro vytvoření nové instance ale pokud se v diagramu
	 * instancí nebude nacházet žádná instace typu příslušné třídy, pak meotoda
	 * nepůjde zavolat, protože nebude parametr, který by bylo možné předat a null
	 * hodnoty v aplikaci nejsou povoleny.
	 */
	private final JCheckBox chcbShowCreateNewInstanceOptionInCallMethod;
	
	
	
	
	
	
	
	/**
	 * Komponenta, která slouží pro nastavení toho, zda se má vztah asociace v
	 * diagramu tříd zobrazit jako jedna hrana s šipkami na obou koncích nebo jako
	 * dvě agregace.
	 */
	private final JCheckBox chcbShowAssociationThroughAggregation;
	
	
	
	
	
	/**
	 * Komponenta, která slouží pro nastavení toho, zda se mají v dialogu pro
	 * zavolání konstruktoru nad třídou v daigramu tříd zobrazovat nabídky pro
	 * zavolání metody, aby se její návratová hodnota předala do příslušného
	 * parametru konstruktoru.
	 */
	private final JCheckBox chcbMakeMethodsAvailableForCallConstructor;
	
	
	/**
	 * Komponenta, která slouží pro nastavení toho, zda se mají v dialogu pro
	 * zavolání statické metody nad třídou v diagramu tříd zobrazovat nabídky pro
	 * zavolání metody, aby se její návratová hodnota předala do příslušného
	 * parametru statické metody.
	 */
	private final JCheckBox chcbMakeMethodsAvailableForCallMethod;
	
	
	
	
	
	
	
	
	
	/**
	 * Label, který bude obsahovat popisek,jako informace pro uživatele, jaký si
	 * chce zvolit výchozí editor zdrojového kódu tříd v diagramu tříd.
	 */
	private final JLabel lblCodeEditorInfo;
	
	
	/**
	 * Komponenta, která slouží pro výběr konkrétního editoru zdrojového kódu.
	 */
	private final JComboBox<CmbItemAbstract> cmbCodeEditor;
	
	
	
	
	
	
	/**
	 * Tato proměnná slouží pouze jako dočasná proměnná pro výchozí hodnotu, která
	 * se má označit v cmbCodeEditor.
	 * 
	 * Protože při načtení se nejprve otevře nastavení a pak se načte jazyk, jenže
	 * bych neměl načtený model v cmbCodeEditor při načtení položky pro označení,
	 * tak by nebylo co označit, proto si zde uložím tuto proměnnou při načtení
	 * vhodnoty ze souboru a při načtení textu ji označním (po naplnění modelu
	 * cmbCodeEditor).
	 */
	private KindOfCodeEditor kindOfCodeEditorTemp;
	
	
	
	
	/**
	 * Komponenta, která slouží pro nastavení toho, zda semají při vytvoření vztahu
	 * typu implementace roznraní v diagramu tříd vygenerovat do příslušné / oznčené
	 * třídy metody které nacházejí v označeném rozhraní.
	 */
	private final JCheckBox chcbAddInterfaceMethodsToClass;


	/**
	 * Komponenta, která slouží pro nastavení toho, zda se má v kontextovém menu nad třídou v diagramu tříd v položce
	 * "Zděděné statické metody" zobrazovat jednotlivé třídy (předci) jako názvy tříd s balíčky, ve kterých / kterém se
	 * nachází nebo jako pouzehý název třídy (bez balíčků).
	 */
	private final JCheckBox chcbShowInheritedClassNameWithPackages;
	
	
	
	

	
	
	
	
	/**
	 * Konstruktor této třídy.
	 */
	@SuppressWarnings("unchecked")
	public ClassDiagramGraphPanel() {
		setLayout(new GridBagLayout());

		
		final GridBagConstraints gbc = getGbc();
		
		index = 0;
		
		gbc.gridwidth = 3;
						
		chcbConDisConLabelFromEdge = new JCheckBox();
		setGbc(gbc, index, 0, chcbConDisConLabelFromEdge, this);
		
		
		
		chcbShowRelationShipsToClassItself = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbShowRelationShipsToClassItself, this);
		
		
		chcbShowAssociationThroughAggregation = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbShowAssociationThroughAggregation, this);
		

		
		chcbAddInterfaceMethodsToClass = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbAddInterfaceMethodsToClass, this);



		chcbShowInheritedClassNameWithPackages = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbShowInheritedClassNameWithPackages, this);



		chcbIncludePrivateConstructors = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbIncludePrivateConstructors, this);

		chcbIncludeProtectedConstructors = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbIncludeProtectedConstructors, this);

		chcbIncludePackagePrivateConstructors = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbIncludePackagePrivateConstructors, this);


		
		
		chcbIncludePrivateMethods = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbIncludePrivateMethods, this);

		chcbIncludeProtectedMethods = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbIncludeProtectedMethods, this);

		chcbIncludePackagePrivateMethods = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbIncludePackagePrivateMethods, this);




		chcbIncludePrivateInnerStaticClasses = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbIncludePrivateInnerStaticClasses, this);

		chcbIncludeProtectedInnerStaticClasses = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbIncludeProtectedInnerStaticClasses, this);

		chcbIncludePackagePrivateInnerStaticClasses = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbIncludePackagePrivateInnerStaticClasses, this);




		chcbInnerStaticClassesIncludePrivateStaticMethods = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbInnerStaticClassesIncludePrivateStaticMethods, this);

		chcbInnerStaticClassesIncludeProtectedStaticMethods = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbInnerStaticClassesIncludeProtectedStaticMethods, this);

		chcbInnerStaticClassesIncludePackagePrivateStaticMethods = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbInnerStaticClassesIncludePackagePrivateStaticMethods, this);


		
		
		
		chcbMakeAvailableFieldsForCallConstructor = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbMakeAvailableFieldsForCallConstructor, this);
				
		
		chcbMakeMethodsAvailableForCallConstructor = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbMakeMethodsAvailableForCallConstructor, this);
		
		
		chcbShowCreateNewInstanceOptionInCallConstructor = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbShowCreateNewInstanceOptionInCallConstructor, this);		
		
		
		
		
		
		chcbMakeAvailableFieldsForCallMethod = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbMakeAvailableFieldsForCallMethod, this);
		
		
		chcbMakeMethodsAvailableForCallMethod = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbMakeMethodsAvailableForCallMethod, this);
		
		
		chcbShowCreateNewInstanceOptionInCallMethod = new JCheckBox();
		setGbc(gbc, ++index, 0, chcbShowCreateNewInstanceOptionInCallMethod, this);
				
		
		
		
		gbc.gridwidth = 1;
		
		
		lblBgColor = new JLabel();
		setGbc(gbc, ++index, 0, lblBgColor, this);
		
		btnChooseBgColor = new JButton();
		btnChooseBgColor.addActionListener(this);		
		setGbc(gbc, index, 1, btnChooseBgColor, this);
		
		lblChoosedBgColor = new JLabel(COLOR_EXAMPLE_LABEL_TEXT);
		lblChoosedBgColor.setOpaque(true);
		setGbc(gbc, index, 2, lblChoosedBgColor, this);
		
		
		
		
		
		gbc.gridwidth = 1;

		lblScale = new JLabel();
		setGbc(gbc, ++index, 0, lblScale, this);

		cmbScale = new JComboBox<>(getArrayOfScale());
		gbc.gridwidth = 2;
		setGbc(gbc, index, 1, cmbScale, this);
		
		
		
		
		
		
		
		lblCodeEditorInfo = new JLabel();
		gbc.gridwidth = 1;
		setGbc(gbc, ++index, 0, lblCodeEditorInfo, this);

		cmbCodeEditor = new JComboBox<>();
		cmbCodeEditor.setRenderer(new ToolTipsComboBoxRenderer(cmbCodeEditor));
		gbc.gridwidth = 2;
		setGbc(gbc, index, 1, cmbCodeEditor, this);
	}
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří instanci jednorozměrného pole s hodnotami typu double v
	 * rozsahu: 0.1 - 20.0
	 * 
	 * @return vrátí uvedené pole
	 */
	private static Double[] getArrayOfScale() {
		final Double[] arrayOfDouble = new Double[196];
		
		int index = 0;
		
		for (int i = 0; i <= 20; i++) {
			for (int j = 0; j <= 9; j++) {
				if (!(i == 0 && (j == 0 || j == 1 || j == 2 || j == 3 || j == 4))) {
					arrayOfDouble[index] = Double.parseDouble(i + "." + j); 	
					index++;
				}
						
				// Mám požadované hodnoty, mohu ukončít cyklus:
				if (i == 20 && j == 0)
					break;
			}
		}
		
		return arrayOfDouble;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public void readDataFromDiagramProp(final Properties classDiagramProperties) {
		chcbConDisConLabelFromEdge.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("EdgeLabelMovable", String.valueOf(Constants.CD_COM_LABELS_MOVABLE))));
		
		chcbShowRelationShipsToClassItself.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("ShowRelationsShipsToClassItself", String.valueOf(Constants.CD_SHOW_RELATION_SHIPS_TO_CLASS_ITSELF))));
				
		chcbShowAssociationThroughAggregation.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("ShowAssociationThroughAggregation", String.valueOf(Constants.CD_SHOW_ASSOCIATION_THROUGH_AGGREGATION))));
		
		chcbAddInterfaceMethodsToClass.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("AddInterfaceMethodsToClass", String.valueOf(Constants.ADD_INTERFACE_METHODS_TO_CLASS))));

		chcbShowInheritedClassNameWithPackages.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("ShowStaticInheritedClassNameWithPackages", String.valueOf(Constants.CD_SHOW_STATIC_INHERITED_CLASS_NAME_WITH_PACKAGES))));

		chcbIncludePrivateConstructors.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("IncludePrivateConstructors", String.valueOf(Constants.CD_INCLUDE_PRIVATE_CONSTRUCTORS))));

		chcbIncludeProtectedConstructors.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("IncludeProtectedConstructors", String.valueOf(Constants.CD_INCLUDE_PROTECTED_CONSTRUCTORS))));

		chcbIncludePackagePrivateConstructors.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("IncludePackagePrivateConstructors", String.valueOf(Constants.CD_INCLUDE_PACKAGE_PRIVATE_CONSTRUCTORS))));

		chcbIncludePrivateMethods.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("IncludePrivateMethods", String.valueOf(Constants.CD_INCLUDE_PRIVATE_METHODS))));

		chcbIncludeProtectedMethods .setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("IncludeProtectedMethods", String.valueOf(Constants.CD_INCLUDE_PROTECTED_METHODS))));

		chcbIncludePackagePrivateMethods.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("IncludePackagePrivateMethods", String.valueOf(Constants.CD_INCLUDE_PACKAGE_PRIVATE_METHODS))));

		chcbIncludePrivateInnerStaticClasses.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("IncludePrivateInnerStaticClasses", String.valueOf(Constants.CD_INCLUDE_PRIVATE_INNER_STATIC_CLASSES))));

		chcbIncludeProtectedInnerStaticClasses.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("IncludeProtectedInnerStaticClasses", String.valueOf(Constants.CD_INCLUDE_PROTECTED_INNER_STATIC_CLASSES))));

		chcbIncludePackagePrivateInnerStaticClasses.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("IncludePackagePrivateInnerStaticClasses", String.valueOf(Constants.CD_INCLUDE_PACKAGE_PRIVATE_INNER_STATIC_CLASSES))));

		chcbInnerStaticClassesIncludePrivateStaticMethods.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("InnerStaticClassesIncludePrivateStaticMethods", String.valueOf(Constants.CD_INNER_STATIC_CLASSES_INCLUDE_PRIVATE_STATIC_METHODS))));

		chcbInnerStaticClassesIncludeProtectedStaticMethods.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("InnerStaticClassesIncludeProtectedStaticMethods", String.valueOf(Constants.CD_INNER_STATIC_CLASSES_INCLUDE_PROTECTED_STATIC_METHODS))));

		chcbInnerStaticClassesIncludePackagePrivateStaticMethods.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("InnerStaticClassesIncludePackagePrivateStaticMethods", String.valueOf(Constants.CD_INNER_STATIC_CLASSES_INCLUDE_PACKAGE_PRIVATE_STATIC_METHODS))));

		chcbMakeAvailableFieldsForCallConstructor.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("MakeFieldsAvailableForConstructor", String.valueOf(Constants.CD_MAKE_FIELDS_AVAILABLE_FOR_CALL_CONSTRUCTOR))));
		
		chcbMakeMethodsAvailableForCallConstructor.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("MakeMethodsAvailableForCallConstructor", String.valueOf(Constants.CD_MAKE_METHODS_AVAILABLE_FOR_CALL_CONSTRUCTOR))));
		
		chcbMakeAvailableFieldsForCallMethod.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("MakeFieldsAvailableForMethod", String.valueOf(Constants.CD_MAKE_FIELDS_AVAILABLE_FOR_CALL_METHOD))));		
		
		chcbMakeMethodsAvailableForCallMethod.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("MakeMethodsAvailableForCallMethod", String.valueOf(Constants.CD_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD))));
		
		chcbShowCreateNewInstanceOptionInCallConstructor.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("ShowCreateNewInstanceOptionInCallConstructor", String.valueOf(Constants.CD_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_CONSTRUCTOR))));
		
		chcbShowCreateNewInstanceOptionInCallMethod.setSelected(Boolean.parseBoolean(classDiagramProperties.getProperty("ShowCreateNewInstanceOptionInCallMethod", String.valueOf(Constants.CD_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD))));
		
		
		kindOfCodeEditorTemp = GraphClass.getKindOfEditorByTextValue(classDiagramProperties.getProperty("DefaultSourceCodeEditor", Constants.CD_KIND_OF_DEFAULT_CODE_EDITOR.getValue()));
		setSelectedCodeEditor(kindOfCodeEditorTemp);
		
		final String textColor = classDiagramProperties.getProperty("BackgroundColor", Integer.toString(Constants.CD_BACKGROUND_COLOR.getRGB()));
		choosedBgColor = new Color(Integer.parseInt(textColor));
		lblChoosedBgColor.setForeground(new Color(Integer.parseInt(textColor)));
		lblChoosedBgColor.setBackground(new Color(Integer.parseInt(textColor)));
		
		// Pro případ, že by užávatel zadal hodnotu mimo rozsah do soubour:
		final double scale = Double.parseDouble(classDiagramProperties.getProperty("ClassDiagramScale", String.valueOf(Constants.CLASS_DIAGRAM_SCALE)));
		
		if (scale > 0 && scale <= 20)
			cmbScale.setSelectedItem(scale);
	}
	
	
	
	
	
	
	@Override
	public void setLanguage(final Properties properties) {
        // Texty do modelu pro cmb pro nastavení výchozí editoru zdrojového kodu:
        final String txtSourceCodeEditorText;
        final String txtSourceCodeEditorTt;
        final String txtProjectExplorerText;
        final String txtProjectExplorerTt;
		
		
		if (properties != null) {
			setBorder(BorderFactory.createTitledBorder(properties.getProperty("Sf_Cd_Cdgp_BorderTitle", Constants.SF_CD_PNL_CDGP_BORDER_TITLE)));
			
			chcbConDisConLabelFromEdge.setText(properties.getProperty("Sf_Cd_Cdgp_LabelDisconectableFromEdge", Constants.SF_CD_PNL_CDGP_CHCB_CON_DIS_CON_LABEL_FROM_EDGE));
			
			chcbShowRelationShipsToClassItself.setText(properties.getProperty("Sf_Cd_Cdgp_ShowRelationsShipsToClassItself", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_RELATION_SHIPS_TO_CLASS_ITSELF));
						
			chcbShowAssociationThroughAggregation.setText(properties.getProperty("Sf_Cd_Cdgp_ChcbShowAsociationThroughAgregation", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_ASOCIATION_THROUGH_AGREGATION));
			chcbShowAssociationThroughAggregation.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_ChcbShowAsociationThroughAgregation_TT", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_ASOCIATION_THROUGH_AGREGATION_TT));
			
			chcbAddInterfaceMethodsToClass.setText(properties.getProperty("Sf_Cd_Cdgp_AddInterfaceMethodsToClassText", Constants.SF_CD_PNL_CDGP_CHCB_ADD_INTERFACE_METHODS_TO_CLASS_TEXT));				
			chcbAddInterfaceMethodsToClass.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_AddInterfaceMethodsToClassTT", Constants.SF_CD_PNL_CDGP_CHCB_ADD_INTERFACE_METHODS_TO_CLASS_TT));

			chcbShowInheritedClassNameWithPackages.setText(properties.getProperty("Sf_Cd_Cdgp_ShowInheritedClassNameWithPackages", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES));
			chcbShowInheritedClassNameWithPackages.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_ShowInheritedClassNameWithPackages_TT", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES_TT));

			chcbIncludePrivateConstructors.setText(properties.getProperty("Sf_Cd_Cdgp_IncludePrivateConstructors", Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PRIVATE_CONSTRUCTORS));
			chcbIncludePrivateConstructors.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_IncludePrivateConstructors_TT", Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PRIVATE_CONSTRUCTORS_TT));

			chcbIncludeProtectedConstructors.setText(properties.getProperty("Sf_Cd_Cdgp_IncludeProtectedConstructors", Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PROTECTED_CONSTRUCTORS));
			chcbIncludeProtectedConstructors.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_IncludeProtectedConstructors_TT", Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PROTECTED_CONSTRUCTORS_TT));

			chcbIncludePackagePrivateConstructors.setText(properties.getProperty("Sf_Cd_Cdgp_IncludePackagePrivateConstructors", Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PACKAGE_PRIVATE_CONSTRUCTORS));
			chcbIncludePackagePrivateConstructors.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_IncludePackagePrivateConstructors_TT", Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PACKAGE_PRIVATE_CONSTRUCTORS_TT));

			chcbIncludePrivateMethods.setText(properties.getProperty("Sf_Cd_Cdgp_IncludePrivateMethods", Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PRIVATE_METHODS));						
			chcbIncludePrivateMethods.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_Cd_IncludePrivateMethods_TT", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PRIVATE_METHODS_TT));

			chcbIncludeProtectedMethods.setText(properties.getProperty("Sf_Cd_Cdgp_IncludeProtectedMethods", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PROTECTED_METHODS));
			chcbIncludeProtectedMethods.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_IncludeProtectedMethods_TT", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PROTECTED_METHODS_TT));

			chcbIncludePackagePrivateMethods.setText(properties.getProperty("Sf_Cd_Cdgp_IncludePackagePrivateMethods", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PACKAGE_PRIVATE_METHODS));
			chcbIncludePackagePrivateMethods.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_IncludePackagePrivateMethods_TT", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PACKAGE_PRIVATE_METHODS_TT));

			chcbIncludePrivateInnerStaticClasses.setText(properties.getProperty("Sf_Cd_Cdgp_IncludePrivateInnerStaticClasses", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PRIVATE_INNER_STATIC_CLASSES));
			chcbIncludePrivateInnerStaticClasses.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_IncludePrivateInnerStaticClasses_TT", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PRIVATE_INNER_STATIC_CLASSES_TT));

			chcbIncludeProtectedInnerStaticClasses.setText(properties.getProperty("Sf_Cd_Cdgp_IncludeProtectedInnerStaticClasses", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PROTECTED_INNER_STATIC_CLASSES));
			chcbIncludeProtectedInnerStaticClasses.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_IncludeProtectedInnerStaticClasses_TT", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PROTECTED_INNER_STATIC_CLASSES_TT));

			chcbIncludePackagePrivateInnerStaticClasses.setText(properties.getProperty("Sf_Cd_Cdgp_IncludePackagePrivateInnerStaticClasses", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PACKAGE_PRIVATE_INNER_STATIC_CLASSES));
			chcbIncludePackagePrivateInnerStaticClasses.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_IncludePackagePrivateInnerStaticClasses_TT", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PACKAGE_PRIVATE_INNER_STATIC_CLASSES_TT));

			chcbInnerStaticClassesIncludePrivateStaticMethods.setText(properties.getProperty("Sf_Cd_Cdgp_InnerStaticClassesIncludePrivateStaticMethods", Constants.SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PRIVATE_STATIC_METHODS));
			chcbInnerStaticClassesIncludePrivateStaticMethods.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_InnerStaticClassesIncludePrivateStaticMethods_TT", Constants.SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PRIVATE_STATIC_METHODS_TT));

			chcbInnerStaticClassesIncludeProtectedStaticMethods.setText(properties.getProperty("Sf_Cd_Cdgp_InnerStaticClassesIncludeProtectedStaticMethods", Constants.SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PROTECTED_STATIC_METHODS));
			chcbInnerStaticClassesIncludeProtectedStaticMethods.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_InnerStaticClassesIncludeProtectedStaticMethods_TT", Constants.SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PROTECTED_STATIC_METHODS_TT));

			chcbInnerStaticClassesIncludePackagePrivateStaticMethods.setText(properties.getProperty("Sf_Cd_Cdgp_InnerStaticClassesIncludePackagePrivateStaticMethods", Constants.SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PACKAGE_PRIVATE_STATIC_METHODS));
			chcbInnerStaticClassesIncludePackagePrivateStaticMethods.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_InnerStaticClassesIncludePackagePrivateStaticMethods_TT", Constants.SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PACKAGE_PRIVATE_STATIC_METHODS_TT));

			chcbMakeAvailableFieldsForCallConstructor.setText(properties.getProperty("Sf_Cd_Cdgp_MakeAvailableFieldsForCallConstructor", Constants.SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_CONSTRUCTOR));
			chcbMakeAvailableFieldsForCallConstructor.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_MakeAvailableFieldsForCallConstructor_TT", Constants.SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_CONSTRUCTOR_TT));

			chcbMakeMethodsAvailableForCallConstructor.setText(properties.getProperty("Sf_Cd_Cdgp_MakeMethodsAvailableForCallConstructor", Constants.SF_CD_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_CONSTRUCTOR));
			chcbMakeMethodsAvailableForCallConstructor.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_MakeMethodsAvailableForCallConstructor_TT", Constants.SF_CD_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_CONSTRUCTOR_TT));
			
			chcbMakeAvailableFieldsForCallMethod.setText(properties.getProperty("Sf_Cd_Cdgp_MakeAvailableFieldsForCallMethod", Constants.SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_METHOD));
			chcbMakeAvailableFieldsForCallMethod.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_MakeAvailableFieldsForCallMethod_TT", Constants.SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_METHOD_TT));
						
			chcbMakeMethodsAvailableForCallMethod.setText(properties.getProperty("Sf_Cd_Cdgp_MakeMethodsAvailableForCallMethod", Constants.SF_CD_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD));
			chcbMakeMethodsAvailableForCallMethod.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_MakeMethodsAvailableForCallMethod_TT", Constants.SF_CD_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD_TT));
			
			chcbShowCreateNewInstanceOptionInCallConstructor.setText(properties.getProperty("Sf_Cd_Cdgp_ShowCreateNewInstanceOptionInCallConstructor", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_CONSTRUCTOR));
			chcbShowCreateNewInstanceOptionInCallConstructor.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_ShowCreateNewInstanceOptionInCallConstructor_TT", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_CONSTRUCTOR_TT));
			
			chcbShowCreateNewInstanceOptionInCallMethod.setText(properties.getProperty("Sf_Cd_Cdgp_ShowCreateNewInstanceOptionInCallMethod", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD));
			chcbShowCreateNewInstanceOptionInCallMethod.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_ShowCreateNewInstanceOptionInCallMethod_TT", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD_TT));			
			
			lblBgColor.setText(properties.getProperty("Sf_Cd_Cdgp_LabelBackgroundColor", Constants.SF_CD_PNL_CDGP_LBL_BG_COLOR) + ": ");
			
			btnChooseBgColor.setText(properties.getProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR));
						
			lblScale.setText(properties.getProperty("Sf_Cd_Cdgp_LabelScaleOfGraph", Constants.SF_CD_PNL_CDGP_LBL_SCALE) + ": ");
			
			txtBgColor = properties.getProperty("Sf_Cd_ChooseBackgroundColorDialog", Constants.SF_TXT_CHOOSE_BG_COLOR);
			
			// Label pro editor zdrojového kodu:
			lblCodeEditorInfo.setText(properties.getProperty("Sf_Cd_Cdgp_LabelCodeEditorInfoText", Constants.SF_CD_PNL_CDGP_LBL_CODE_EDITOR_INFO_TEXT));
			lblCodeEditorInfo.setToolTipText(properties.getProperty("Sf_Cd_Cdgp_LabelCodeEditorInfoTT", Constants.SF_CD_PNL_CDGP_LBL_CODE_EDITOR_INFO_TT));

			// Texty do cmb pro výběr výchozího editoru zdrojového kódu:
			txtSourceCodeEditorText = properties.getProperty("Sf_Cd_Cdgp_TxtSourceCodeEditorText", Constants.SF_CD_PNL_CDGP_TXT_SOURCE_CODE_EDITOR_TEXT);
			txtSourceCodeEditorTt = properties.getProperty("Sf_Cd_Cdgp_TxtSourceCodeEditorTT", Constants.SF_CD_PNL_CDGP_TXT_SOURCE_CODE_EDITOR_TT);
			txtProjectExplorerText = properties.getProperty("Sf_Cd_Cdgp_TxtProjectExplorerText", Constants.SF_CD_PNL_CDGP_TXT_PROJECT_EXPLORER_TEXT);
			txtProjectExplorerTt = properties.getProperty("Sf_Cd_Cdgp_TxtProjectExplorerTT", Constants.SF_CD_PNL_CDGP_TXT_PROJECT_EXPLORER_TT);
		}
		
		
		else {
			setBorder(BorderFactory.createTitledBorder(Constants.SF_CD_PNL_CDGP_BORDER_TITLE));
			
			chcbConDisConLabelFromEdge.setText(Constants.SF_CD_PNL_CDGP_CHCB_CON_DIS_CON_LABEL_FROM_EDGE);
			
			chcbShowRelationShipsToClassItself.setText(Constants.SF_CD_PNL_CDGP_CHCB_SHOW_RELATION_SHIPS_TO_CLASS_ITSELF);
			
			chcbShowAssociationThroughAggregation.setText(Constants.SF_CD_PNL_CDGP_CHCB_SHOW_ASOCIATION_THROUGH_AGREGATION);
			chcbShowAssociationThroughAggregation.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_SHOW_ASOCIATION_THROUGH_AGREGATION_TT);
			
			chcbAddInterfaceMethodsToClass.setText(Constants.SF_CD_PNL_CDGP_CHCB_ADD_INTERFACE_METHODS_TO_CLASS_TEXT);				
			chcbAddInterfaceMethodsToClass.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_ADD_INTERFACE_METHODS_TO_CLASS_TT);

			chcbShowInheritedClassNameWithPackages.setText(Constants.SF_CD_PNL_CDGP_CHCB_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES);
			chcbShowInheritedClassNameWithPackages.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES_TT);

			chcbIncludePrivateConstructors.setText(Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PRIVATE_CONSTRUCTORS);
			chcbIncludePrivateConstructors.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PRIVATE_CONSTRUCTORS_TT);

			chcbIncludeProtectedConstructors.setText(Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PROTECTED_CONSTRUCTORS);
			chcbIncludeProtectedConstructors.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PROTECTED_CONSTRUCTORS_TT);

			chcbIncludePackagePrivateConstructors.setText(Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PACKAGE_PRIVATE_CONSTRUCTORS);
			chcbIncludePackagePrivateConstructors.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PACKAGE_PRIVATE_CONSTRUCTORS_TT);

			chcbIncludePrivateMethods.setText(Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PRIVATE_METHODS);			
			chcbIncludePrivateMethods.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PRIVATE_METHODS_TT);

			chcbIncludeProtectedMethods.setText(Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PROTECTED_METHODS);
			chcbIncludeProtectedMethods.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PROTECTED_METHODS_TT);

			chcbIncludePackagePrivateMethods.setText(Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PACKAGE_PRIVATE_METHODS);
			chcbIncludePackagePrivateMethods.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PACKAGE_PRIVATE_METHODS_TT);

			chcbIncludePrivateInnerStaticClasses.setText(Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PRIVATE_INNER_STATIC_CLASSES);
			chcbIncludePrivateInnerStaticClasses.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PRIVATE_INNER_STATIC_CLASSES_TT);

			chcbIncludeProtectedInnerStaticClasses.setText(Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PROTECTED_INNER_STATIC_CLASSES);
			chcbIncludeProtectedInnerStaticClasses.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PROTECTED_INNER_STATIC_CLASSES_TT);

			chcbIncludePackagePrivateInnerStaticClasses.setText(Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PACKAGE_PRIVATE_INNER_STATIC_CLASSES);
			chcbIncludePackagePrivateInnerStaticClasses.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PACKAGE_PRIVATE_INNER_STATIC_CLASSES_TT);

			chcbInnerStaticClassesIncludePrivateStaticMethods.setText(Constants.SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PRIVATE_STATIC_METHODS);
			chcbInnerStaticClassesIncludePrivateStaticMethods.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PRIVATE_STATIC_METHODS_TT);

			chcbInnerStaticClassesIncludeProtectedStaticMethods.setText(Constants.SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PROTECTED_STATIC_METHODS);
			chcbInnerStaticClassesIncludeProtectedStaticMethods.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PROTECTED_STATIC_METHODS_TT);

			chcbInnerStaticClassesIncludePackagePrivateStaticMethods.setText(Constants.SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PACKAGE_PRIVATE_STATIC_METHODS);
			chcbInnerStaticClassesIncludePackagePrivateStaticMethods.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PACKAGE_PRIVATE_STATIC_METHODS_TT);

			chcbMakeAvailableFieldsForCallConstructor.setText(Constants.SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_CONSTRUCTOR);
			chcbMakeAvailableFieldsForCallConstructor.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_CONSTRUCTOR_TT);
			
			chcbMakeMethodsAvailableForCallConstructor.setText(Constants.SF_CD_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_CONSTRUCTOR);
			chcbMakeMethodsAvailableForCallConstructor.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_CONSTRUCTOR_TT);
			
			chcbMakeAvailableFieldsForCallMethod.setText(Constants.SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_METHOD);
			chcbMakeAvailableFieldsForCallMethod.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_METHOD_TT);
			
			chcbMakeMethodsAvailableForCallMethod.setText(Constants.SF_CD_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD);
			chcbMakeMethodsAvailableForCallMethod.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD_TT);
			
			chcbShowCreateNewInstanceOptionInCallConstructor.setText(Constants.SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_CONSTRUCTOR);
			chcbShowCreateNewInstanceOptionInCallConstructor.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_CONSTRUCTOR_TT);
			
			chcbShowCreateNewInstanceOptionInCallMethod.setText(Constants.SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD);
			chcbShowCreateNewInstanceOptionInCallMethod.setToolTipText(Constants.SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD_TT);			
			
			lblBgColor.setText(Constants.SF_CD_PNL_CDGP_LBL_BG_COLOR + ": ");
			
			btnChooseBgColor.setText( Constants.SF_CD_BTN_CHOOSE_COLOR);
						
			lblScale.setText(Constants.SF_CD_PNL_CDGP_LBL_SCALE + ": ");
			
			txtBgColor = Constants.SF_TXT_CHOOSE_BG_COLOR;
			
			// Label pro editor zdrojového kodu:
			lblCodeEditorInfo.setText(Constants.SF_CD_PNL_CDGP_LBL_CODE_EDITOR_INFO_TEXT);
			lblCodeEditorInfo.setToolTipText(Constants.SF_CD_PNL_CDGP_LBL_CODE_EDITOR_INFO_TT);
			
			// Texty do cmb pro výběr výchozího editoru zdrojového kódu:
			txtSourceCodeEditorText = Constants.SF_CD_PNL_CDGP_TXT_SOURCE_CODE_EDITOR_TEXT;
			txtSourceCodeEditorTt = Constants.SF_CD_PNL_CDGP_TXT_SOURCE_CODE_EDITOR_TT;
			txtProjectExplorerText = Constants.SF_CD_PNL_CDGP_TXT_PROJECT_EXPLORER_TEXT; 
			txtProjectExplorerTt = Constants.SF_CD_PNL_CDGP_TXT_PROJECT_EXPLORER_TT;
		}
		
		// Nastavím otazník na začátek, aby uživatel věděl, že tam má nápovědu po
		// najetí.
		chcbShowInheritedClassNameWithPackages.setText("? " + chcbShowInheritedClassNameWithPackages.getText());
		chcbIncludePrivateConstructors.setText("? " + chcbIncludePrivateConstructors.getText());
		chcbIncludeProtectedConstructors.setText("? " + chcbIncludeProtectedConstructors.getText());
		chcbIncludePackagePrivateConstructors.setText("? " + chcbIncludePackagePrivateConstructors.getText());
		chcbIncludePrivateMethods.setText("? " + chcbIncludePrivateMethods.getText());
		chcbIncludeProtectedMethods.setText("? " + chcbIncludeProtectedMethods.getText());
		chcbIncludePackagePrivateMethods.setText("? " + chcbIncludePackagePrivateMethods.getText());
		chcbIncludePrivateInnerStaticClasses.setText("? " + chcbIncludePrivateInnerStaticClasses.getText());
		chcbIncludeProtectedInnerStaticClasses.setText("? " + chcbIncludeProtectedInnerStaticClasses.getText());
		chcbIncludePackagePrivateInnerStaticClasses.setText("? " + chcbIncludePackagePrivateInnerStaticClasses.getText());
		chcbInnerStaticClassesIncludePrivateStaticMethods.setText("? " + chcbInnerStaticClassesIncludePrivateStaticMethods.getText());
		chcbInnerStaticClassesIncludeProtectedStaticMethods.setText("? " + chcbInnerStaticClassesIncludeProtectedStaticMethods.getText());
		chcbInnerStaticClassesIncludePackagePrivateStaticMethods.setText("? " + chcbInnerStaticClassesIncludePackagePrivateStaticMethods.getText());
		chcbMakeAvailableFieldsForCallConstructor.setText("? " + chcbMakeAvailableFieldsForCallConstructor.getText());
		chcbMakeMethodsAvailableForCallConstructor.setText("? " + chcbMakeMethodsAvailableForCallConstructor.getText());
		chcbMakeAvailableFieldsForCallMethod.setText("? " + chcbMakeAvailableFieldsForCallMethod.getText());
		chcbMakeMethodsAvailableForCallMethod.setText("? " + chcbMakeMethodsAvailableForCallMethod.getText());
		chcbShowCreateNewInstanceOptionInCallConstructor.setText("? " + chcbShowCreateNewInstanceOptionInCallConstructor.getText());
		chcbShowCreateNewInstanceOptionInCallMethod.setText("? " + chcbShowCreateNewInstanceOptionInCallMethod.getText());
		chcbShowAssociationThroughAggregation.setText("? " + chcbShowAssociationThroughAggregation.getText());
		chcbAddInterfaceMethodsToClass.setText("? " + chcbAddInterfaceMethodsToClass.getText());
		
		
		lblCodeEditorInfo.setText("? " + lblCodeEditorInfo.getText() + ": ");
		
		
		
		
		// Model pro JComboBox s výchozím editorem zdrojového kódu:
		
		/*
		 * List, do kterého budu vkládat jednotlivé položky modelu (mohlo by to být
		 * rovnou pole, ale jsem líný řešit indexy).
		 */
		final List<CodeEditorInfo> codeEditorInfoList = new ArrayList<>(4);
		
		codeEditorInfoList.add(new CodeEditorInfo(KindOfCodeEditor.SOURCE_CODE_EDITOR, txtSourceCodeEditorText, txtSourceCodeEditorTt));
		codeEditorInfoList.add(new CodeEditorInfo(KindOfCodeEditor.PROJECT_EXPLORER, txtProjectExplorerText, txtProjectExplorerTt));
		
		// Nastavím model:
		cmbCodeEditor.setModel(new DefaultComboBoxModel<>(codeEditorInfoList.toArray(new CodeEditorInfo[] {})));
		
		// Označím položku:
		setSelectedCodeEditor(kindOfCodeEditorTemp);
	}



	
	
	
	
	
	


	@Override
	public void actionPerformed(final ActionEvent e) {
		if (e.getSource() instanceof JButton && e.getSource() == btnChooseBgColor) {
			// Nejprve si zvolenou barvu uložím do dočasné proměnné - pouze v metodě, prtože
			// když uživatel dialog zruši barby by byla null a následně by se uložila do souboru, což je chyba
			final Color tempColor = JColorChooser.showDialog(this, txtBgColor, Constants.CD_BACKGROUND_COLOR);

			if (tempColor != null) {
				choosedBgColor = tempColor;
				lblChoosedBgColor.setForeground(choosedBgColor);
				lblChoosedBgColor.setBackground(choosedBgColor);
			}
		}
	}
	
	
	
	
	
	

	
	
	
	@Override
	public EditProperties writeDataToDiagramProp(final EditProperties classDiagramProperties) {
		classDiagramProperties.setProperty("EdgeLabelMovable", String.valueOf(chcbConDisConLabelFromEdge.isSelected()));
		
		classDiagramProperties.setProperty("ShowRelationsShipsToClassItself", String.valueOf(chcbShowRelationShipsToClassItself.isSelected()));
				
		classDiagramProperties.setProperty("ShowAssociationThroughAggregation", String.valueOf(chcbShowAssociationThroughAggregation.isSelected()));
		
		classDiagramProperties.setProperty("AddInterfaceMethodsToClass", String.valueOf(chcbAddInterfaceMethodsToClass.isSelected()));

		classDiagramProperties.setProperty("ShowStaticInheritedClassNameWithPackages", String.valueOf(chcbShowInheritedClassNameWithPackages.isSelected()));

		classDiagramProperties.setProperty("IncludePrivateConstructors", String.valueOf(chcbIncludePrivateConstructors.isSelected()));

		classDiagramProperties.setProperty("IncludeProtectedConstructors", String.valueOf(chcbIncludeProtectedConstructors.isSelected()));

		classDiagramProperties.setProperty("IncludePackagePrivateConstructors", String.valueOf(chcbIncludePackagePrivateConstructors.isSelected()));

		classDiagramProperties.setProperty("IncludePrivateMethods", String.valueOf(chcbIncludePrivateMethods.isSelected()));

		classDiagramProperties.setProperty("IncludeProtectedMethods", String.valueOf(chcbIncludeProtectedMethods.isSelected()));

		classDiagramProperties.setProperty("IncludePackagePrivateMethods", String.valueOf(chcbIncludePackagePrivateMethods.isSelected()));

		classDiagramProperties.setProperty("IncludePrivateInnerStaticClasses", String.valueOf(chcbIncludePrivateInnerStaticClasses.isSelected()));

		classDiagramProperties.setProperty("IncludeProtectedInnerStaticClasses", String.valueOf(chcbIncludeProtectedInnerStaticClasses.isSelected()));

		classDiagramProperties.setProperty("IncludePackagePrivateInnerStaticClasses", String.valueOf(chcbIncludePackagePrivateInnerStaticClasses.isSelected()));

		classDiagramProperties.setProperty("InnerStaticClassesIncludePrivateStaticMethods", String.valueOf(chcbInnerStaticClassesIncludePrivateStaticMethods.isSelected()));

		classDiagramProperties.setProperty("InnerStaticClassesIncludeProtectedStaticMethods", String.valueOf(chcbInnerStaticClassesIncludeProtectedStaticMethods.isSelected()));

		classDiagramProperties.setProperty("InnerStaticClassesIncludePackagePrivateStaticMethods", String.valueOf(chcbInnerStaticClassesIncludePackagePrivateStaticMethods.isSelected()));

		classDiagramProperties.setProperty("MakeFieldsAvailableForConstructor", String.valueOf(chcbMakeAvailableFieldsForCallConstructor.isSelected()));
		
		classDiagramProperties.setProperty("MakeMethodsAvailableForCallConstructor", String.valueOf(chcbMakeMethodsAvailableForCallConstructor.isSelected()));		
		
		classDiagramProperties.setProperty("MakeFieldsAvailableForMethod", String.valueOf(chcbMakeAvailableFieldsForCallMethod.isSelected()));
		
		classDiagramProperties.setProperty("MakeMethodsAvailableForCallMethod", String.valueOf(chcbMakeMethodsAvailableForCallMethod.isSelected()));	
		
		classDiagramProperties.setProperty("ShowCreateNewInstanceOptionInCallConstructor", String.valueOf(chcbShowCreateNewInstanceOptionInCallConstructor.isSelected()));
				
		classDiagramProperties.setProperty("ShowCreateNewInstanceOptionInCallMethod", String.valueOf(chcbShowCreateNewInstanceOptionInCallMethod.isSelected()));		
		
		classDiagramProperties.setProperty("ClassDiagramScale", String.valueOf(cmbScale.getSelectedItem()));
		
		classDiagramProperties.setProperty("DefaultSourceCodeEditor", ((CodeEditorInfo)cmbCodeEditor.getSelectedItem()).getKindOfCodeEditor().getValue());
		
		if (choosedBgColor != null)
			classDiagramProperties.setProperty("BackgroundColor", Integer.toString(choosedBgColor.getRGB()));
		
		return classDiagramProperties;
	}







    /**
     * Metoda, která označí položku v komponentě JComboBox, který obshauje položky pro označení výchozího editoru
     * zdrojového kódu.
     *
     * @param kindOfCodeEditor
     *         - výčtový typ, který značí jeden z editoru zdrojových kódů, který se má označit v komponentě JComboBox.
     */
    private void setSelectedCodeEditor(final KindOfCodeEditor kindOfCodeEditor) {
        if (kindOfCodeEditor == null)
            return;

        final int length = cmbCodeEditor.getModel().getSize();

        for (int i = 0; i < length; i++) {
            if (((CodeEditorInfo) cmbCodeEditor.getModel().getElementAt(i)).getKindOfCodeEditor().equals(kindOfCodeEditor)) {
                cmbCodeEditor.setSelectedIndex(i);
                break;
            }
        }
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Přístupové metody - settry na nastavení hodnot komponent v panelu:
	public final void setSelectedChcbConDisConLabel(final boolean selected) {
		chcbConDisConLabelFromEdge.setSelected(selected);
	}

	public final void setSelectedChcbShowRelationShipsToClassItself(final boolean selected) {
		chcbShowRelationShipsToClassItself.setSelected(selected);
	}

	public final void setSelectedChcbShowInheritedClassNameWithPackages(final boolean selected) {
		chcbShowInheritedClassNameWithPackages.setSelected(selected);
	}

	public final void setSelectedChcbIncludePrivateConstructors(final boolean selected) {
		chcbIncludePrivateConstructors.setSelected(selected);
	}

	public final void setSelectedChcbIncludeProtectedConstructors(final boolean selected) {
		chcbIncludeProtectedConstructors.setSelected(selected);
	}

	public final void setSelectedChcbIncludePackagePrivateConstructors(final boolean selected) {
		chcbIncludePackagePrivateConstructors.setSelected(selected);
	}

	public final void setSelectedChcbIncludePrivateMethods(final boolean selected) {
		chcbIncludePrivateMethods.setSelected(selected);
	}

	public final void setSelectedChcbIncludeProtectedMethods(final boolean selected) {
		chcbIncludeProtectedMethods.setSelected(selected);
	}
	public final void setSelectedChcbIncludePackagePrivateMethods(final boolean selected) {
		chcbIncludePackagePrivateMethods.setSelected(selected);
	}

	public final void setSelectedChcbIncludePrivateInnerStaticClasses(final boolean selected) {
		chcbIncludePrivateInnerStaticClasses.setSelected(selected);
	}

	public final void setSelectedChcbIncludeProtectedInnerStaticClasses(final boolean selected) {
		chcbIncludeProtectedInnerStaticClasses.setSelected(selected);
	}

	public final void setSelectedChcbIncludePackagePrivateInnerStaticClasses(final boolean selected) {
		chcbIncludePackagePrivateInnerStaticClasses.setSelected(selected);
	}

	public final void setSelectedChcbInnerStaticClassesIncludePrivateStaticMethods(final boolean selected) {
		chcbInnerStaticClassesIncludePrivateStaticMethods.setSelected(selected);
	}

	public final void setSelectedChcbInnerStaticClassesIncludeProtectedStaticMethods(final boolean selected) {
		chcbInnerStaticClassesIncludeProtectedStaticMethods.setSelected(selected);
	}

	public final void setSelectedChcbInnerStaticClassesIncludePackagePrivateStaticMethods(final boolean selected) {
		chcbInnerStaticClassesIncludePackagePrivateStaticMethods.setSelected(selected);
	}

	public final void setSelectedChcbMakeAvailableFieldsForCallConstructor(final boolean selected) {
		chcbMakeAvailableFieldsForCallConstructor.setSelected(selected);
	}

	public final void setSelectedChcbMakeAvailableMethodsForCallConstructor(final boolean selected) {
		chcbMakeMethodsAvailableForCallConstructor.setSelected(selected);
	}

	public final void setSelectedChcbMakeAvailableFieldsForCallMethod(final boolean selected) {
		chcbMakeAvailableFieldsForCallMethod.setSelected(selected);
	}

	public final void setSelectedChcbMakeAvailableMethodsForCallMethod(final boolean selected) {
		chcbMakeMethodsAvailableForCallMethod.setSelected(selected);
	}

	public final void setSelectedChcbShowCreateNewInstanceOptionInCallConstructor(final boolean selected) {
		chcbShowCreateNewInstanceOptionInCallConstructor.setSelected(selected);
	}

	public final void setSelectedChcbShowCreateNewInstanceOptionInCallMethod(final boolean selected) {
		chcbShowCreateNewInstanceOptionInCallMethod.setSelected(selected);
	}

	public final void setSelectedChcbShowAssociationThroughAggregation(final boolean selected) {
		chcbShowAssociationThroughAggregation.setSelected(selected);
	}

	public final void setSelectedChcbAddInterfaceMethodsToClass(final boolean selected) {
		chcbAddInterfaceMethodsToClass.setSelected(selected);
	}

	public final void setDefaultCodeEditor(final KindOfCodeEditor kindOfCodeEditor) {
		setSelectedCodeEditor(kindOfCodeEditor);
	}

	public final void setBgColor(final int rgbOfBgColor) {
		choosedBgColor = new Color(rgbOfBgColor);
		lblChoosedBgColor.setForeground(choosedBgColor);
		lblChoosedBgColor.setBackground(choosedBgColor);
	}

	public final void setScaleOfGraph(final double scale) {
		cmbScale.setSelectedItem(scale);
	}
}