package cz.uhk.fim.fimj.settings_form.application_panels;

/**
 * Tato třída slouží puze jako výjimka, který se vyhodí v případě, že je zadán chybný Locale, například když jej
 * uživatel změní v souboru a nebude nalezen v seznamu dostupných Locale pro JVM, tak se vyhodí tato vyjímka, dle které
 * se pozná, že se daný Locale nemá využít v aplikaci.
 * <p>
 * U obsluhy této vyjímka se "nic" dít nebude, protože se využívá při načítání locale v cyklu, tak se pouze přeskočí
 * příslušná iterace. A přesně tak to chci udělat -> pouze se vynechá příslušný Locale, který je "chybný".
 * <p>
 * Note: Je možné napsat alespoň výchozí konstruktor, ale to je již "předpřipravené" pro Javu obecně.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class WrongLocaleException extends Exception {

    private static final long serialVersionUID = 1L;
}
