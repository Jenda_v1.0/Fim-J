package cz.uhk.fim.fimj.settings_form.command_editor_panels;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.settings_form.PanelAbstract;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.util.Properties;

/**
 * Nastavení vlastností pro okno s historií příkazů zadaných v editoru příkazů v rámci otevřeného projektu.
 *
 * <i>Jedná se o vlastnosti pro okno s veškerými příkazy, které uživatel zadal do editoru příkazů v rámci otevřeného
 * projektu. Viz třída: {@link cz.uhk.fim.fimj.commands_editor.history.project_history.ProjectHistoryCompletion}</i>
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 23.09.2018 15:47
 */

public class ProjectHistoryPanel extends PanelAbstract implements LanguageInterface, ChangeListener {


    /**
     * Zda se má zobrazovat okno s historií.
     */
    private final JCheckBox chcbShowHistory;


    /**
     * Zda se mají číslovat příkazy v historii.
     *
     * <i>Pokud se budou číslovat, nelze je filtrovat. Pokud se nebudou číslovat, bude možné mezi nimi filtrovat, ale
     * nebudou seřazeny dle pořadí zadání / potvrzení v editoru.</i>
     */
    private final JCheckBox chcbIndexCommands;


    /**
     * Konstruktor třídy.
     */
    public ProjectHistoryPanel() {
        setLayout(new GridBagLayout());

        final GridBagConstraints gbc = getGbc();
        index = 0;


        // Komponenty do panelu:
        chcbShowHistory = new JCheckBox();
        chcbShowHistory.addChangeListener(this);
        setGbc(gbc, index, 0, chcbShowHistory, this);


        chcbIndexCommands = new JCheckBox();
        setGbc(gbc, ++index, 0, chcbIndexCommands, this);
    }


    @Override
    public void setLanguage(final Properties properties) {
        final String txtBorderTitle;
        final String txtTooltipText;
        final String txtIndexCommandsText;

        if (properties != null) {
            txtBorderTitle = properties.getProperty("Php_Txt_Pnl_BorderTitle", Constants.PHP_TXT_PNL_BORDER_TITLE);
            txtTooltipText = properties.getProperty("Php_Txt_PnlTooltip_Text", Constants.PHP_TXT_PNL_TOOLTIP_TEXT);

            chcbShowHistory.setText(properties.getProperty("Php_Chcb_ShowHistory_Text",
                    Constants.PHP_CHCB_SHOW_HISTORY_TEXT));

            txtIndexCommandsText = properties.getProperty("Php_Chcb_IndexCommands_Text",
                    Constants.PHP_CHCB_INDEX_COMMANDS_TEXT);
            chcbIndexCommands.setToolTipText(properties.getProperty("Php_Chcb_IndexCommands_TT",
                    Constants.PHP_CHCB_INDEX_COMMANDS_TT));
        } else {
            txtBorderTitle = Constants.PHP_TXT_PNL_BORDER_TITLE;
            txtTooltipText = Constants.PHP_TXT_PNL_TOOLTIP_TEXT;

            chcbShowHistory.setText(Constants.PHP_CHCB_SHOW_HISTORY_TEXT);

            txtIndexCommandsText = Constants.PHP_CHCB_INDEX_COMMANDS_TEXT;
            chcbIndexCommands.setToolTipText(Constants.PHP_CHCB_INDEX_COMMANDS_TT);
        }


        setBorder(BorderFactory.createTitledBorder("? " + txtBorderTitle));
        setToolTipText(txtTooltipText);

        chcbIndexCommands.setText("? " + txtIndexCommandsText);
    }


    /**
     * Nastavení označení komponenty pro nastavení, zda se má zpřístupnit okno s historií v editoru příkazů.
     *
     * @param selected
     *         - true v případě, že se má komponenta označít, jinak false.
     */
    public void setSelectedChcbShowHistory(final boolean selected) {
        chcbShowHistory.setSelected(selected);
    }


    /**
     * Zjištění, zda li je komponenta pro nastavní toho, zda se má zobrazovat historie příkazů označena nebo ne.
     *
     * @return true v případě, že je komponenta označena, tj. má se zpřístupnit okno s historií v editoru příkazů. Jinak
     * false.
     */
    public boolean isChcbShowHistorySelected() {
        return chcbShowHistory.isSelected();
    }


    /**
     * Nastavení označení komponenty pro nastavení, zda se mají indexovat příkazy v okně s historíí příkazů (aby byly v
     * pořadí zadání).
     *
     * @param selected
     *         - true v případě, že se má komponenta označít (příkazy se budou číslovat), jinak false.
     */
    public void setSelectedChcbIndexCommands(final boolean selected) {
        chcbIndexCommands.setSelected(selected);
    }


    /**
     * Zjištění, zda li je komponenta pro nastavní toho, zda se mají indexovat zadané příkay v editoru příkazů (tj. v
     * okně s historií příkazů budou očíslované příkazy dle pořadí, v jakém byly zadány) označena nebo ne.
     *
     * @return true v případě, že je komponenta označena, tj. mají se číslovat zadané příkazy v okně s historií příkazů.
     * Jinak false.
     */
    public boolean isChcbIndexCommandsSelected() {
        return chcbIndexCommands.isSelected();
    }


    @Override
    public void stateChanged(ChangeEvent e) {
        if (e.getSource() instanceof JCheckBox && e.getSource() == chcbShowHistory) {
            /*
             * V případě, že se nemá zpřístupnit historie příkazů, není třeba zpřístupnit nastavení ostatních komponent.
             */
            checkSelectedChcbShowHistory();
        }
    }


    /**
     * Nastavení zpřístupnění komponent v panelu dle toho, zda je povoleno zpřístupnění okna s historií.
     */
    public void checkSelectedChcbShowHistory() {
        setEnabledChcbs(chcbShowHistory.isSelected());
    }

    /**
     * Nastavení zpřístupnění komponent v panelu.
     *
     * @param enabled
     *         true v případě, že se mají vybrané komponenty zpřístupnit, jinak false.
     */
    private void setEnabledChcbs(final boolean enabled) {
        chcbIndexCommands.setEnabled(enabled);
    }
}