package cz.uhk.fim.fimj.settings_form;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.language.EditProperties;

import javax.swing.*;
import java.awt.*;
import java.util.Properties;

/**
 * Třída slouží jako panel s komponentami pro nastavení vlastností formátování zdrojového kódu.
 *
 * <i>Uživatel může nastavit zvlášť vlastnosti formátování pro dialog editor zdrojového kódu a pro dialog průzkumník
 * projektů. Ale v obou případech se jedná o nastavení identických vlastností pro formátování zdrojového kódu.</i>
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 28.09.2018 12:00
 */

public class FormattingPropertiesPanel extends PanelAbstract implements LanguageInterface {


    /**
     * Nastavení, zda se mají mazat prázdné řádky v blocích kódu, ale ne mezi mezi metodami. Tj. budou se mazat prázdné
     * řádky v metodách, ne okolo nich.
     */
    private final JCheckBox chcbDeleteEmptyLines;


    /**
     * Zda se mají převádět tabulátory na mezery.
     */
    private final JCheckBox chcbTabSpaceConversion;


    /**
     * Zda se má zalamovat syntaxe "else if ()".
     *
     * <i>
     * True značí, že se výše uvedená syntaxe zalomí na syntaxi: else if () ...
     * </i>
     */
    private final JCheckBox chcbBreakElseIf;


    /**
     * Zda se mají zalamovat / rozdělit řádky, které obsahujíc více výrazů / příkazů.
     *
     * <i>
     * Například když na jednom řádku bude System.out.println("adf");System.out.println("adf");
     * <p>
     * Pak se zalomí na:
     * <p>
     * System.out.println("adf"); System.out.println("adf");
     * </i>
     */
    private final JCheckBox chcbSetSingleStatement;


    /**
     * Zda se má zalomit jednořádkový blok kódu na více řádků.
     *
     * <i>
     * Například metoda: public Object test() {return null;}
     * <p>
     * V případě nastavené hodnoty true bude metoda zalomena na tři řádky (hlavička, tělo metody a zavírací závorka).
     * Hodnota false ponechá metodu v syntaxi, v jaké je uvedena.
     * </i>
     */
    private final JCheckBox chcbBreakOneLineBlock;


    /**
     * Zda se má zalamovat kód za zavíracímí závorkami.
     *
     * <i>
     * Například když bude bude syntaxe: if (true) {
     * <p>
     * } else { ... } V případě, že se nastaví true, bude část else (za zavírací závorkou) vždy na novém řádku.
     * </i>
     */
    private final JCheckBox chcbBreakClosingHeaderBracket;


    /**
     * Přerušení nesouvisejících bloků kódu prázdnými řádky.
     *
     * <i>
     * Například public ClassName() {
     * <p>
     * } if (true) {
     * <p>
     * } System.out.println();
     * <p>
     * Tak v případě, že je tato vlastnost true, tak se nad příkaz System.out.println(); vloží prázdný řádek.
     * </i>
     */
    private final JCheckBox chcbBreakBlock;


    /**
     * Nastavení mezer mezi operátory.
     *
     * <i>
     * Například syntaxe: double i = 5+5+11+0.002;
     * <p>
     * V případě, že se nastaví true, tak vedle operátorů budou mezery. Resp. mezi číslem a operátorem bude mezera. V
     * případě hodnoty false zůstane výše uvedený příklad mez mezer.
     * </i>
     */
    private final JCheckBox chcbOperatorPaddingMode;


    /**
     * Konstruktor třídy.
     */
    FormattingPropertiesPanel() {
        setLayout(new GridBagLayout());
        index = 0;

        final GridBagConstraints gbc = getGbc();


        chcbDeleteEmptyLines = new JCheckBox();
        setGbc(gbc, index, 0, chcbDeleteEmptyLines, this);

        chcbTabSpaceConversion = new JCheckBox();
        setGbc(gbc, ++index, 0, chcbTabSpaceConversion, this);

        chcbBreakElseIf = new JCheckBox();
        setGbc(gbc, ++index, 0, chcbBreakElseIf, this);

        chcbSetSingleStatement = new JCheckBox();
        setGbc(gbc, ++index, 0, chcbSetSingleStatement, this);

        chcbBreakOneLineBlock = new JCheckBox();
        setGbc(gbc, ++index, 0, chcbBreakOneLineBlock, this);

        chcbBreakClosingHeaderBracket = new JCheckBox();
        setGbc(gbc, ++index, 0, chcbBreakClosingHeaderBracket, this);

        chcbBreakBlock = new JCheckBox();
        setGbc(gbc, ++index, 0, chcbBreakBlock, this);

        chcbOperatorPaddingMode = new JCheckBox();
        setGbc(gbc, ++index, 0, chcbOperatorPaddingMode, this);
    }


    @Override
    public void setLanguage(final Properties properties) {
        final String txtBorderTitle;
        final String txtDeleteEmptyLineText;
        final String txtDeleteEmptyLineTT;
        final String txtTabSpaceConversionText;
        final String txtTabSpaceConversionTT;
        final String txtBreakElseIfText;
        final String txtBreakElseIfTT;
        final String txtSetSingleStatementText;
        final String txtSetSingleStatementTT;
        final String txtBreakOneLineBlockText;
        final String txtBreakOneLineBlockTT;
        final String txtBreakClosingHeaderBracketText;
        final String txtBreakClosingHeaderBracketTT;
        final String txtBreakBlockText;
        final String txtBreakBlockTT;
        final String txtOperatorPaddingModeText;
        final String txtOperatorPaddingModeTT;


        if (properties != null) {
            txtBorderTitle = properties.getProperty("Fpp_BorderTitle", Constants.FPP_BORDER_TITLE);

            txtDeleteEmptyLineText = properties.getProperty("Fpp_DeleteEmptyLine_Text",
                    Constants.FPP_DELETE_EMPTY_LINE_TEXT);
            txtDeleteEmptyLineTT = properties.getProperty("Fpp_DeleteEmptyLine_TT", Constants.FPP_DELETE_EMPTY_LINE_TT);

            txtTabSpaceConversionText = properties.getProperty("Fpp_TabSpaceConversion_Text",
                    Constants.FPP_TAB_SPACE_CONVERSION_TEXT);
            txtTabSpaceConversionTT = properties.getProperty("Fpp_TabSpaceConversion_TT",
                    Constants.FPP_TAB_SPACE_CONVERSION_TT);

            txtBreakElseIfText = properties.getProperty("Fpp_BreakElseIf_Text", Constants.FPP_BREAK_ELSE_IF_TEXT);
            txtBreakElseIfTT = properties.getProperty("Fpp_BreakElseIf_TT", Constants.FPP_BREAK_ELSE_IF_TT);

            txtSetSingleStatementText = properties.getProperty("Fpp_SetSingleStatement_Text",
                    Constants.FPP_SET_SINGLE_STATEMENT_TEXT);
            txtSetSingleStatementTT = properties.getProperty("Fpp_SetSingleStatement_TT",
                    Constants.FPP_SET_SINGLE_STATEMENT_TT);

            txtBreakOneLineBlockText = properties.getProperty("Fpp_BreakOneLineBlock_Text",
                    Constants.FPP_BREAK_ONE_LINE_BLOCK_TEXT);
            txtBreakOneLineBlockTT = properties.getProperty("Fpp_BreakOneLineBlock_TT",
                    Constants.FPP_BREAK_ONE_LINE_BLOCK_TT);

            txtBreakClosingHeaderBracketText = properties.getProperty("Fpp_BreakClosingHeaderBracket_Text",
                    Constants.FPP_BREAK_CLOSING_HEADER_BRACKET_TEXT);
            txtBreakClosingHeaderBracketTT = properties.getProperty("Fpp_BreakClosingHeaderBracket_TT",
                    Constants.FPP_BREAK_CLOSING_HEADER_BRACKET_TT);

            txtBreakBlockText = properties.getProperty("Fpp_BreakBlock_Text", Constants.FPP_BREAK_BLOCK_TEXT);
            txtBreakBlockTT = properties.getProperty("Fpp_BreakBlock_TT", Constants.FPP_BREAK_BLOCK_TT);

            txtOperatorPaddingModeText = properties.getProperty("Fpp_OperatorPaddingMode_Text",
                    Constants.FPP_OPERATOR_PADDING_MODE_TEXT);
            txtOperatorPaddingModeTT = properties.getProperty("Fpp_OperatorPaddingMode_TT",
                    Constants.FPP_OPERATOR_PADDING_MODE_TT);
        } else {
            txtBorderTitle = Constants.FPP_BORDER_TITLE;

            txtDeleteEmptyLineText = Constants.FPP_DELETE_EMPTY_LINE_TEXT;
            txtDeleteEmptyLineTT = Constants.FPP_DELETE_EMPTY_LINE_TT;

            txtTabSpaceConversionText = Constants.FPP_TAB_SPACE_CONVERSION_TEXT;
            txtTabSpaceConversionTT = Constants.FPP_TAB_SPACE_CONVERSION_TT;

            txtBreakElseIfText = Constants.FPP_BREAK_ELSE_IF_TEXT;
            txtBreakElseIfTT = Constants.FPP_BREAK_ELSE_IF_TT;

            txtSetSingleStatementText = Constants.FPP_SET_SINGLE_STATEMENT_TEXT;
            txtSetSingleStatementTT = Constants.FPP_SET_SINGLE_STATEMENT_TT;

            txtBreakOneLineBlockText = Constants.FPP_BREAK_ONE_LINE_BLOCK_TEXT;
            txtBreakOneLineBlockTT = Constants.FPP_BREAK_ONE_LINE_BLOCK_TT;

            txtBreakClosingHeaderBracketText = Constants.FPP_BREAK_CLOSING_HEADER_BRACKET_TEXT;
            txtBreakClosingHeaderBracketTT = Constants.FPP_BREAK_CLOSING_HEADER_BRACKET_TT;

            txtBreakBlockText = Constants.FPP_BREAK_BLOCK_TEXT;
            txtBreakBlockTT = Constants.FPP_BREAK_BLOCK_TT;

            txtOperatorPaddingModeText = Constants.FPP_OPERATOR_PADDING_MODE_TEXT;
            txtOperatorPaddingModeTT = Constants.FPP_OPERATOR_PADDING_MODE_TT;
        }


        setBorder(BorderFactory.createTitledBorder(txtBorderTitle));

        final String questionMark = "? ";
        final String txtElseIfSyntax = ": 'else if ()'";

        chcbDeleteEmptyLines.setText(questionMark + txtDeleteEmptyLineText);
        chcbDeleteEmptyLines.setToolTipText(txtDeleteEmptyLineTT);

        chcbTabSpaceConversion.setText(questionMark + txtTabSpaceConversionText);
        chcbTabSpaceConversion.setToolTipText(txtTabSpaceConversionTT);

        chcbBreakElseIf.setText(questionMark + txtBreakElseIfText + txtElseIfSyntax);
        chcbBreakElseIf.setToolTipText(txtBreakElseIfTT);

        chcbSetSingleStatement.setText(questionMark + txtSetSingleStatementText);
        chcbSetSingleStatement.setToolTipText(txtSetSingleStatementTT);
        chcbBreakOneLineBlock.setText(questionMark + txtBreakOneLineBlockText);
        chcbBreakOneLineBlock.setToolTipText(txtBreakOneLineBlockTT);

        chcbBreakClosingHeaderBracket.setText(questionMark + txtBreakClosingHeaderBracketText);
        chcbBreakClosingHeaderBracket.setToolTipText(txtBreakClosingHeaderBracketTT);

        chcbBreakBlock.setText(questionMark + txtBreakBlockText);
        chcbBreakBlock.setToolTipText(txtBreakBlockTT);

        chcbOperatorPaddingMode.setText(questionMark + txtOperatorPaddingModeText);
        chcbOperatorPaddingMode.setToolTipText(txtOperatorPaddingModeTT);
    }


    /**
     * Uložení vlastností do objektu properties.
     *
     * <i>Jedná se o uložení nastavených vlastností pro formátování zdrojového kódu do objektu properties.</i>
     *
     * @param properties
     *         - objekt, do kterého se mají zapsat nastavení vlastnosti.
     *
     * @return objekt properties, který bude obsahovat nastavené vlastnosti pro formátování zdrojového kódu.
     */
    EditProperties saveSetting(final EditProperties properties) {
        properties.setProperty("DeleteEmptyLines", String.valueOf(chcbDeleteEmptyLines.isSelected()));
        properties.setProperty("TabSpaceConversion", String.valueOf(chcbTabSpaceConversion.isSelected()));
        properties.setProperty("BreakElseIf", String.valueOf(chcbBreakElseIf.isSelected()));
        properties.setProperty("SingleStatement", String.valueOf(chcbSetSingleStatement.isSelected()));
        properties.setProperty("BreakOneLineBlock", String.valueOf(chcbBreakOneLineBlock.isSelected()));
        properties.setProperty("BreakClosingHeaderBrackets",
                String.valueOf(chcbBreakClosingHeaderBracket.isSelected()));
        properties.setProperty("BreakBlocks", String.valueOf(chcbBreakBlock.isSelected()));
        properties.setProperty("OperatorPadding", String.valueOf(chcbOperatorPaddingMode.isSelected()));

        return properties;
    }


    /**
     * Označení komponent dle nastavených vlastností v objektu properties.
     *
     * @param properties
     *         - objekt, ze kterého se má přešít nastavení vlastností pro formátování zdrojového kódu a dle toho se
     *         označit komponenty v panelu.
     */
    void openSettings(final Properties properties) {
        chcbDeleteEmptyLines.setSelected(Boolean.parseBoolean(properties.getProperty("DeleteEmptyLines",
                String.valueOf(Constants.SCF_DELETE_EMPTY_LINES))));
        chcbTabSpaceConversion.setSelected(Boolean.parseBoolean(properties.getProperty("TabSpaceConversion",
                String.valueOf(Constants.SCF_TAB_SPACE_CONVERSION))));
        chcbBreakElseIf.setSelected(Boolean.parseBoolean(properties.getProperty("BreakElseIf",
                String.valueOf(Constants.SCF_BREAK_ELSE_IF))));
        chcbSetSingleStatement.setSelected(Boolean.parseBoolean(properties.getProperty("SingleStatement",
                String.valueOf(Constants.SCF_SINGLE_STATEMENT))));
        chcbBreakOneLineBlock.setSelected(Boolean.parseBoolean(properties.getProperty("BreakOneLineBlock",
                String.valueOf(Constants.SCF_BREAK_ONE_LINE_BLOCK))));
        chcbBreakClosingHeaderBracket.setSelected(Boolean.parseBoolean(properties.getProperty(
                "BreakClosingHeaderBrackets", String.valueOf(Constants.SCF_BREAK_CLOSING_HEADER_BRACKETS))));
        chcbBreakBlock.setSelected(Boolean.parseBoolean(properties.getProperty("BreakBlocks",
                String.valueOf(Constants.SCF_BREAK_BLOCKS))));
        chcbOperatorPaddingMode.setSelected(Boolean.parseBoolean(properties.getProperty("OperatorPadding",
                String.valueOf(Constants.SCF_OPERATOR_PADDING_MODE))));
    }


    /**
     * Obnovení výchozího nastavení.
     *
     * <i>Komponenty v panelu se označí dle výchozího nastavení.</i>
     */
    void setDefaultValues() {
        chcbDeleteEmptyLines.setSelected(Constants.SCF_DELETE_EMPTY_LINES);
        chcbTabSpaceConversion.setSelected(Constants.SCF_TAB_SPACE_CONVERSION);
        chcbBreakElseIf.setSelected(Constants.SCF_BREAK_ELSE_IF);
        chcbSetSingleStatement.setSelected(Constants.SCF_SINGLE_STATEMENT);
        chcbBreakOneLineBlock.setSelected(Constants.SCF_BREAK_ONE_LINE_BLOCK);
        chcbBreakClosingHeaderBracket.setSelected(Constants.SCF_BREAK_CLOSING_HEADER_BRACKETS);
        chcbBreakBlock.setSelected(Constants.SCF_BREAK_BLOCKS);
        chcbOperatorPaddingMode.setSelected(Constants.SCF_OPERATOR_PADDING_MODE);
    }
}