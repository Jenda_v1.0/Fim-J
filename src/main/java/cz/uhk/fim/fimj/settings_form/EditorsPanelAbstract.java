package cz.uhk.fim.fimj.settings_form;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;

/**
 * Tato třída slouží pouze jako abstraktní třída pro některé metody a atributy pro ostatní panely pro dialog coby
 * nastavení pro aplikaci, více v jednotlivých proměnných a metodách
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class EditorsPanelAbstract extends PanelAbstract {

    private static final long serialVersionUID = 1L;


    protected static final List<String> ARRAY_OF_FONT_STYLE = Collections
            .unmodifiableList(Arrays.asList("Bold", "Italic", "Plain"));


    protected GridBagConstraints gbc;

    /**
     * Komponenta pro označení toho, zda se má zvýraznit kod otevřeného souboru - ano ne, buď se bude zvýrazňovat
     * syntaxe nebo ne. Nic jiného
     */
    JCheckBox chcbHighlightingCode;

    /**
     * Komponenta, která slouží pro nastavení toho, zda se má využít zvýrazňování pro každý typ souboru, tj. zda se má
     * zvýrazňovat soubor s příponou .java jinak než třeba soubor s příponou .properties apod.
     * <p>
     * Jde o to, že využitá knihovna umožňuje nastavení zvýraznění kódu dle různého tpyu otevřeného souboru, tak zda to
     * tak uživatel chce nebo ne.
     */
    JCheckBox chcbHighlightCodeByKindOfFile;


    protected Color bgColor, textColor;
    JLabel lblBgColor, lblTextColor, lblChoosedBgColor, lblChoosedTextColor, lblFontSize, lblFontStyle;
    String txtChooseBgColorDialog, txtChooseTextColorDialog;

    JButton btnChooseBgColor, btnChooseTextColor;

    protected JComboBox<Integer> cmbFontSize;
    protected JComboBox<String> cmbFontStyle;


    /**
     * Metoda, ktery vytvoří jednorozměrné pole s hodnotami typu Integer v rozsahu 8 - 100 tento rozsah lze zvlit jako
     * velikost písma v příkazovém editoru - Command editoru
     *
     * @return vrátí výše zmíněné pole
     */
    static Integer[] getFontSizes() {
        final Integer[] arrayOfFontSizes = new Integer[93];

        for (int i = 8; i <= 100; i++)
            arrayOfFontSizes[i - 8] = i;

        return arrayOfFontSizes;
    }
}