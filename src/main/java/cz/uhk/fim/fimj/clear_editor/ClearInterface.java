package cz.uhk.fim.fimj.clear_editor;

/**
 * Toto rozhraní obsahuje pouze jedinou metodu, kterou musí implementovat příslušný editor, vekterém má být k dispozici
 * možnost pro vymazání příslušného editoru.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface ClearInterface {

    /**
     * Metoda, k terá vymaže veškerý obsah příslušného editoru.
     */
    void clearEditor();
}
