package cz.uhk.fim.fimj.clear_editor;

import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.Properties;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.border.BevelBorder;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;

/**
 * Tato třída slouží jako kontextové menu, které zavolá metodu v příslušné komponentě, která vyčistí příslušná editor.
 * <p>
 * Toto kontextové menu obsahuje jedinou položku 'Vymazat' a po kliknutí na tuto položku se zavolá výše zmíněná metoda,
 * která vymaže text z příslušného editoru, například editor výstupů nebo editor příkazů nebo editoru pro výstupy
 * ohledně kompilace jako je třeba v editoru zdrojového kódu nebo v dialogu průzkumník projektů.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class PopupMenu extends JPopupMenu implements LanguageInterface {

    private static final long serialVersionUID = -5418111987961242755L;


    /**
     * Položka, která slouží pro vymazání veškerého obsahu z příslušného editoru.
     */
    private final JMenuItem itemClear;


    /**
     * Konstruktor této třídy.
     *
     * @param clearInterface
     *         - rozhraní, které implementuje příslušný editor, ve kterém se má vymazat příslušný text, resp. zavolat
     *         metoda pro vymazání příslušného textu.
     */
    public PopupMenu(final ClearInterface clearInterface) {
        super();

        // Aby to menu vypadalo "navýšeně" - trochu připomíná dlaždici.
        setBorder(new BevelBorder(BevelBorder.RAISED));

        // Inicializace:
        itemClear = new JMenuItem();

        // Přidání posluchače na kliknutí:
        itemClear.addActionListener(even -> clearInterface.clearEditor());

        // Přidání do okna:
        add(itemClear);
    }


    @Override
    public void setLanguage(final Properties properties) {
        if (properties != null) {
            itemClear.setText(properties.getProperty("CE_PM_ItemClearText", Constants.CE_PM_ITEM_CLEAR_TEXT));
            itemClear.setToolTipText(properties.getProperty("CE_PM_ItemClearTT", Constants.CE_PM_ITEM_CLEAR_TT));
        } else {
            itemClear.setText(Constants.CE_PM_ITEM_CLEAR_TEXT);
            itemClear.setToolTipText(Constants.CE_PM_ITEM_CLEAR_TT);
        }
    }


    /**
     * Metoda, která přidá událost na stisknutí kombinace kláves: Ctrl + L. Po stisknutí tété kombinace se také vymaže
     * příslušný editor (jeho text) stejně jako po kliknutí na položku 'Vymazat' v tomto kontextovém menu.
     * <p>
     * Tato metoda by zde možná ani nemusela být, ale pak bych ji musel pro každý editor psát znovu, ale jelikož má
     * úplně stejný účel, jako toto kontextové menu, tak ji nechám zde a jen zavolám u příslušného editoru.
     *
     * @param clearInterface
     *         - rozhraní, které obsahuje metodu, která slouží pro vymazání textu z příslušného editoru.
     *
     * @return KeyAdapter s upravenou metodou KeyReleased, která reaguje na stisknutí kombince kláves Ctrl + L, která
     * vymaže text v příslušném editoru.
     */
    public static KeyAdapter getKeyAdapterForClearEditor(final ClearInterface clearInterface) {
        return new KeyAdapter() {

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_L && e.isControlDown())
                    clearInterface.clearEditor();
            }
        };
    }


    /**
     * Metoda, která slouží pro vytvoření instance této třídy, resp. kontextového menu pro příslušnou kompnentu, ve
     * které se má vymazat text z příslušného editoru.
     *
     * @param languageProperties
     *         - objekt obsahující text pro tuto aplikaci v uživatelem zvoleném jazyce.
     * @param component
     *         - komponenta, která zavolala tuto metodu pro vytvoření kontextového menu, tento parametr je zde potřeba
     *         kvůli vytvoření kontextového menu na "správném" umístění.
     * @param clearInterface
     *         - rozhraní, které implementuje příslušná komponenta / editor, ve kterém se má zavolat metoda pro
     *         vymazání. V tomto případě je to i parametr component.
     * @param e
     *         - událost myši pro získání pozice pro otevření kontextového menu (mohl bych si i předat místo tohoto
     *         parametru dva parametr pro umístění na x a y).
     */
    public static void createPopupMenu(final Properties languageProperties, final Component component,
                                       final ClearInterface clearInterface, MouseEvent e) {
        /*
         * Vytvořím si instanci kontextového menu a předám mu odkaz na tento editor.
         */
        final PopupMenu popupMenu = new PopupMenu(clearInterface);

        popupMenu.setLanguage(languageProperties);

        popupMenu.show(component, e.getX(), e.getY());
    }
}
