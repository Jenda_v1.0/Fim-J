package cz.uhk.fim.fimj.output_editor;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Properties;

import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.text.AbstractDocument;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.clear_editor.ClearInterface;
import cz.uhk.fim.fimj.clear_editor.PopupMenu;
import cz.uhk.fim.fimj.commands_editor.Editor;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.OutputEditorInterface;

/**
 * Tato třída slouží jako Jtextarea pro výstupy z editoru kódu, tj. chybové hlaášky, apod. dále výpisy z kompilace -
 * její výsledek, případně chyby, apod.
 * <p>
 * Půjde sem zadávat v posstatě libovolný text, ale ten co se sem přidá z aplikace nepůjde editovat.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class OutputEditor extends JTextArea implements OutputEditorInterface, ClearInterface, LanguageInterface {

	private static final long serialVersionUID = -1598393087583268128L;


	/**
	 * Promenná, která obsahuje pouze dvě mezery (dva bílé znaky).
	 *
	 * Tato proměnná je zde tak trochu potřeba jen pro to, pokud nechci zadávat
	 * všude dvě mezery ručně, tak mohu využít tuto proměnnou a jedna se o to, že
	 * když se vypisují nějaké informace do tohoto editoru výstupů, tak pokud se
	 * jedná o více jak jeden řádek, pak na dalších řádcích již nebudou data
	 * zarovnána.
	 */
	public static final String TWO_GAPS = "  ";


	/**
	 * Promenná, která obsahuje pouze značku pro nový řádek a za ním dvě mezery
	 * (nový řádek a dva bílé znaky).
	 *
	 * Tato proměnná je zde tak trochu potřeba jen pro to, pokud nechci zadávat
	 * všude dvě mezery ručně, tak mohu využít tuto proměnnou a jedna se o to, že
	 * když se vypisují nějaké informace do tohoto editoru výstupů, tak pokud se
	 * jedná o více jak jeden řádek, pak na dalších řádcích již nebudou data
	 * zarovnána.
	 */
	public static final String NEW_LINE_TWO_GAPS = "\n  ";




	/**
	 * Tato proměnná obsahuje výchozí text, která se bude vkládat při vytvoření
	 * editoru výstupů, včetně jeho čištění.
	 */
	private static final String DEFAULT_INFO_COMMAND = ": 'printCommands();'";


	/**
	 * Logická proměnná, která určuje, zda se mají do editoru výstupů vypisovat
	 * referenční proměnné (reference na instanci v daigramu instancí, kterou
	 * uživatel zadal při jejím vytváření).
	 *
	 * Tj. Když uživatel například zavolá metodu pomocí editoru příkazů nebo
	 * kontextového menu apod. Tak se v případě, že ta metoda vrátí instanci, která
	 * se aktuálně nachází v diagramu instancí, tak se za tu instanci ještě doplní
	 * ta reference na tu instancí v diagramu instancí, která ukazuje na tu instancí
	 * (hodnota true). jinak, když bude tato hodnota false, tak se za tu instaci již
	 * vypisovat ty hodnoty nebudou.
	 */
	private static boolean showReferenceVariables;







	/**
	 * Objekt, který obsahuje veškeré text pro tuto aplikaci v uživatelem zvoleném
	 * jazyce.
	 */
	private final Properties languageProperties;




    /**
     * Proměnná, která obsahuje ůvodní informace o zavolání metody s přehledem příkazů - pouze začátek věty, že má
     * uživatel zavolat tu metodu, tento text je ve zvoleném jazyce pro aplikaci.
     */
    private String textForHelp;
    /**
     * Informace pro klávesovou zkratku Ctrl + Enter - potvrzení příkazu.
     */
    private String textForCtrlEnter;
    /**
     * Informace pro klávesové zkratky Ctrl a šipka nahoru a dolu - listování historie příkazů.
     */
    private String textForCtrUpDown;
    /**
     * Informace pro klávesovou zkratku Shift + Enter - nový řádek bez potvrzení příkazu.
     */
    private String textForShiftEnter;
    /**
     * Informace pro klávesovou zkratku Ctrl + Space - okno s dostupnými příkazy.
     */
    private String textForCtrlSpace;
    /**
     * Informace pro klávesovou zkratku Ctrl + H - okno s historií zadaných příkazů v rámci otevřeného projektu.
     */
    private String textForCtrlH;
    /**
     * Informace pro klávesovou zkratku Ctrl + Shift + H - okno s historií zadaných příkazů, které uživatel zadal od
     * spuštění aplikace. Nehledě na změnu či zavření projektu.
     */
    private String textForCtrlShiftH;










	/**
	 * Konstruktor této třídy, potřebuje parametr coby texty, aby načetl výchozí
	 * text pro nápovědu příkazů
	 *
	 * @param languageProperties
	 *            - soubor typu Properties, který obsahuje texty pro aplikaci ve
	 *            zvoleném jazyce, konstruktor si z něj načte akorát text pro
	 *            nápovědu pro uživatele
	 */
	public OutputEditor(final Properties languageProperties) {
		this.languageProperties = languageProperties;

		setSettingsEditor();


		// Nastavení proměnných s texty ve zvoleném jazyce pro pozdější použití:
		setLanguage(languageProperties);


		// Nastavení, aby nešla vypsaná část editovat:
		((AbstractDocument) getDocument()).setDocumentFilter(new NoneEditableLineDocumentFilter());



		addMouseWheelListener();


		addMouseListener();


		addKeyListener(PopupMenu.getKeyAdapterForClearEditor(this));
	}


    /**
     * Nastavení výchozího textu do editoru výstupů.
     * <p>
     * Jedná se o informace pro zacházení s editorem příkazů - klávesové zkratky pro prácí s editorem příkazů, vypsání
     * dostupných možností apod.
     */
    private void setDefaultInfoText() {
        String infoText = TWO_GAPS + textForHelp + DEFAULT_INFO_COMMAND + NEW_LINE_TWO_GAPS +
                NEW_LINE_TWO_GAPS + "Ctrl + Enter" + " -> " + textForCtrlEnter +
                NEW_LINE_TWO_GAPS + "Ctrl + (Up | Down)" + " -> " + textForCtrUpDown +
                NEW_LINE_TWO_GAPS + "Shift + Enter" + " -> " + textForShiftEnter;

        if (Editor.isShowAutoCompleteWindow())
            infoText += NEW_LINE_TWO_GAPS + "Ctrl + Space" + " -> " + textForCtrlSpace;

        if (Editor.isShowProjectHistoryWindow())
            infoText += NEW_LINE_TWO_GAPS + "Ctrl + H" + " -> " + textForCtrlH;

        if (Editor.isShowCompleteHistoryWindow())
            infoText += NEW_LINE_TWO_GAPS + "Ctrl + Shift + H" + " -> " + textForCtrlShiftH;

        infoText += NEW_LINE_TWO_GAPS;

        setText(infoText);
    }







	@Override
	public void setSettingsEditor() {
		final Properties outputEditorProp = App.READ_FILE.getOutputEditorProperties();

		if (outputEditorProp != null) {
			// Zde se padařilo načíst soubor bud s výchozím nastavením nebo ve workspace,
			// Tak nastavím hodnoty do editoru v app:
			final int fontStyle = Integer.parseInt(outputEditorProp.getProperty("FontStyle", Integer.toString(Constants.DEFAULT_FONT_FOR_OUTPUT_EDITOR.getStyle())));
			final int fontSize = Integer.parseInt(outputEditorProp.getProperty("FontSize", Integer.toString(Constants.DEFAULT_FONT_FOR_OUTPUT_EDITOR.getSize())));

			setFont(new Font(Constants.DEFAULT_FONT_FOR_OUTPUT_EDITOR.getName(), fontStyle, fontSize));


			final String bgColor = outputEditorProp.getProperty("BackgroundColor", Integer.toString(Constants.DEFAULT_BACKGROUND_COLOR_FOR_OUTPUT_EDITOR.getRGB()));
			setBackground(new Color(Integer.parseInt(bgColor)));


			final String foregroundColor = outputEditorProp.getProperty("ForegroundColor", Integer.toString(Constants.DEFAULT_TEXT_COLOR_FOR_OUTPUT_EDITOR.getRGB()));
			setForeground(new Color(Integer.parseInt(foregroundColor)));


			// Zda se mají vypisovat referenční proměnné za instanci:
			showReferenceVariables = Boolean.parseBoolean(outputEditorProp.getProperty("ShowReferenceVariables", String.valueOf(Constants.OUTPUT_EDITOR_SHOW_REFERENCE_VARIABLES)));
		}


		else {
			// Zde použiji výchozí nastavení:
			setFont(Constants.DEFAULT_FONT_FOR_OUTPUT_EDITOR);

			setBackground(Constants.DEFAULT_BACKGROUND_COLOR_FOR_OUTPUT_EDITOR);

			setForeground(Constants.DEFAULT_TEXT_COLOR_FOR_OUTPUT_EDITOR);

			// Zda se mají vypisovat referenční proměnné za instanci:
			showReferenceVariables = Constants.OUTPUT_EDITOR_SHOW_REFERENCE_VARIABLES;
		}
	}






	/**
	 * Metoda, která této třídě - editoru přidá reakci na myš konkrétně na rolování
	 * koelčkem myši, pokud bude stisknuta klávesa CTRL pak se bude zvětšovat /
	 * změnšovat písmo editoru
	 */
	private void addMouseWheelListener() {
		addMouseWheelListener(e -> {

			if (e.getWheelRotation() < 0 && e.isControlDown()) { // Rotace kolečkem myši nahoru - zvětšování písma
				float fontSize = getFont().getSize();

				if (fontSize < 100)
					setFont(getFont().deriveFont(++fontSize));
			}

			else if (e.getWheelRotation() > 0 && e.isControlDown()) { // Rotování kolečkem myši dolů - zmenšování
																		// písma
				float fontSize = getFont().getSize();
				if (fontSize > 8)
					setFont(getFont().deriveFont(--fontSize));
			}
		});
	}













	/**
	 * Metod, která přidá událost na kliknutí pravým tlačítkem myši na tento editor
	 * výstupů, že se po kliknutí pravým tlačítkem myši na tento editoru výstupů
	 * zobrazí kontextové menu, které bude obsahovat jedinou položku vymazat, která
	 * slouží pro vymazání veškerého obsahu editoru výstupů a na začátek akorát
	 * nastaví úvodní informaci ohledně zavolání té metody s přehledem příkazů pro
	 * ovládíní editoru příkazů
	 */
	private void addMouseListener() {
		addMouseListener(new MouseAdapter() {

			@Override
			public void mouseReleased(MouseEvent e) {
				/*
				 * Pokud lze editor výstupů editovat, tj. je otevřen nějaký projekt a uvolní se
				 * pravé tlačítko myši, pak otevřu kontxtové menu s položkou pro vymazání textu.
				 */
				if (isEnabled() && SwingUtilities.isRightMouseButton(e))
					PopupMenu.createPopupMenu(languageProperties, OutputEditor.this, OutputEditor.this, e);
			}
		});
	}






	@Override
	public void clearEditor() {
		// Nejprve vymažu "filtr" proti úpravám.
		((AbstractDocument) getDocument()).setDocumentFilter(null);

		/*
		 * Vymažu veškerý text a nastavím pouze na začátek výchozí text s příkazem pro
		 * výpis přehledu příkazů a nějaké klávesové zkratky.
		 */
		setDefaultInfoText();

		// Znovu nastavím, aby nešlel editovat již odentrovaný text.
		((AbstractDocument) getDocument()).setDocumentFilter(new NoneEditableLineDocumentFilter());
	}





	@Override
	public void addResult(final String result) {
		append(NEW_LINE_TWO_GAPS + result + NEW_LINE_TWO_GAPS);

		// Nastavení kurzoru na konec textu v editoru:
		setCaretPosition(getDocument().getLength());
	}




	/**
	 * Getr na proměnnou, která značí, zda se mají vypisovat referenční proměnné za
	 * instanci do editoru výstupů.
	 *
	 * Tzn. když třeba uživatel zavolá metodu nebo si vezme hodnotu z pole apod. Buď
	 * pomocí editoru příkazů nebo zavoláním metody pomocí kontextového menu. Tak
	 * poud ta metoda nebo hodnota z pole bude instancí třídy z diagramu tříd, která
	 * se aktuálně nachází v diagramu instnací, tak seza tu instanci ještě vypíše
	 * referenční proměnná, resp. reference na tu instanci v diagramu instancí,
	 * kterou uživatel zadal při jejím vytváření.
	 *
	 * @return výše popsanou logickou hodnotu true - mají se vypisovat referenční
	 *         proměnné, false - nemají se vypisovat referencční proměnné za
	 *         instanci.
	 */
	public static boolean isShowReferenceVariables() {
		return showReferenceVariables;
	}


    @Override
    public void setLanguage(final Properties properties) {
        if (properties != null) {
            textForHelp = languageProperties.getProperty("Oe_InfoAboutCommands", Constants.OE_INFO_ABOUT_COMMANDS);

            // Informace pro zkratky:
            textForCtrlEnter = languageProperties.getProperty("Oe_InfoForCtrl_Enter", Constants.OE_INFO_FOR_CTRL_ENTER);
            textForCtrUpDown = languageProperties.getProperty("Oe_InfoForEnter_UpDown",
                    Constants.OE_INFO_FOR_ENTER_UP_DOWN);
            textForShiftEnter = languageProperties.getProperty("Oe_InfoForShift_Enter",
                    Constants.OE_INFO_FOR_SHIFT_ENTER);
            textForCtrlSpace = languageProperties.getProperty("Oe_InfoForCtrl_Space", Constants.OE_INFO_FOR_CTRL_SPACE);
            textForCtrlH = languageProperties.getProperty("Oe_InfoForCtrl_H", Constants.OE_INFO_FOR_CTRL_H);
            textForCtrlShiftH = languageProperties.getProperty("Oe_InfoForCtrl_Shift_H",
                    Constants.OE_INFO_FOR_CTRL_SHIFT_H);
        } else {
            textForHelp = Constants.OE_INFO_ABOUT_COMMANDS;

            // Informace pro zkratky:
            textForCtrlEnter = Constants.OE_INFO_FOR_CTRL_ENTER;
            textForCtrUpDown = Constants.OE_INFO_FOR_ENTER_UP_DOWN;
            textForShiftEnter = Constants.OE_INFO_FOR_SHIFT_ENTER;
            textForCtrlSpace = Constants.OE_INFO_FOR_CTRL_SPACE;
            textForCtrlH = Constants.OE_INFO_FOR_CTRL_H;
            textForCtrlShiftH = Constants.OE_INFO_FOR_CTRL_SHIFT_H;
        }
    }
}