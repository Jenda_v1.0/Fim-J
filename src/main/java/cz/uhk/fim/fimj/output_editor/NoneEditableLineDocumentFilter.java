package cz.uhk.fim.fimj.output_editor;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.Element;

/**
 * Tato třída slouží k tomu, aby nešla komponenta, kde se tato třída použije editovat (nebo její část), například je
 * použita pro editor výstupů (vpravo dole v hlavním okně aplikace), kde lze editovat vždy poslední řádek, ale veškeré
 * výpisy před ním resp. výpisy co se již vypsaly eitovat nelze, ale může si uživatel vždy něco napsat na poslední
 * řádek, k tomu tato třída a její metody slouží.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class NoneEditableLineDocumentFilter extends DocumentFilter {

    private static final String PROMPT = ">";


    // Metoda, která se zavolá před vložením textu:
    @Override
    public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr)
            throws BadLocationException {
        if (string != null)
            replace(fb, offset, 0, string, attr);
    }


    // Metoda, která se zavolá před odebráním:
    @Override
    public void remove(FilterBypass fb, int offset, int length) throws BadLocationException {
        replace(fb, offset, length, "", null);
    }


    // Metoda, která se zavolá před editací textu:
    @Override
    public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs)
            throws BadLocationException {

        final Document document = fb.getDocument();
        final Element root = document.getDefaultRootElement();
        final int count = root.getElementCount();
        final int index = root.getElementIndex(offset);

        final Element cur = root.getElement(index);
        final int promptPosition = cur.getStartOffset() + PROMPT.length();

        if (index == count - 1 && offset - promptPosition >= 0) {
            if (text.endsWith("\n"))
                text = "\n" + OutputEditor.TWO_GAPS;

            fb.replace(offset, length, text, attrs);
        }
    }
}