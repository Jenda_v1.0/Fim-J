package cz.uhk.fim.fimj.buttons_panel;

import java.io.File;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.swing.JOptionPane;

import cz.uhk.fim.fimj.file.*;
import cz.uhk.fim.fimj.reflection_support.ParameterToText;
import org.jgraph.graph.DefaultGraphCell;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.project_explorer.ReadWriteFile;
import cz.uhk.fim.fimj.project_explorer.ReadWriteFileInterface;

/**
 * Třída slouží k tomu, aby otestovala vždy dva označené objekt - třídy, resp. buňky v graf tak, že si zjistí, zda se
 * jedná o třídy a pokud ano, tak zda vůbec existuje, a když ano, tak se mezi nimi vytvoři požadované spojení -
 * asociace, dedičnost, ... jak v grafu, tak i ve třídě
 * <p>
 * <p>
 * <p>
 * Postup: Nejprve si do kolekce načtu veškerý text nalezené - označené třídy Bude to kolekce Stringů, jeden Strin =
 * jeden rádek ve tridě, pak si tuto kolekci cyklem "projdu", a zjistím, si na jakem radku, resp indexu koelce se
 * vyskytují klicova slova tyto klicova slova budou hlavicky tříd 'pubilc class name {' nebo 'public abstract class
 * anem' nebo public enum name... a ostatní třídy, které tyto aplikace může vytvořit
 * <p>
 * pokud takový rádek - text bude v kolekci nalezen, zjistím si jeho index a za daný index, vložím odřádková pak dany
 * atribut u asociace je to odkaz na druhou označenou třídu ve tvaru private DruhaTrida druhatrida; a tento způsob -
 * postup aplikuji na všechny ostatní hrany akorát někde bude taková to proměnná, nekde (u agregace) může být list
 * těchto referencí
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class TestAndCreateEdge implements TestAndCreateEdgeInterface, LanguageInterface {
	
	// Proměnné pro texty do chybových hlášek:
	private static String missingClassText, missingClassTitle, inheritErrorText, inheritErrorTitle,
			enumInheritErrorText, enumInheritErrorTitle, interfaceErrorText, interfaceErrorTitle, implementsErrorText_1,
			implementsErrorText_2, implementsErrorTitle, selectedClassErrorText, selectedClassErrorTitle,
			missingSrcDirectoryText, missingSrcDirectoryTitle, enumInheritErrorText_2, enumInheritErrorTitle_2,
			wrongSelectedObjectText, wrongSelectedObjectTitle, choosedCommentNotClassText, choosedCommentNotClassTitle;
	
	
	/**
	 * Pro manipulaci kódu - resp pro získání a zápis kodu do a z třídy:
	 */
	private static final ReadWriteFileInterface editCodeOfClass = new ReadWriteFile();

	
	
	
	
	/**
	 * Reference na diagram tříd.
	 */
	private final GraphClass classDiagram;
	

	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd.
	 */
	public TestAndCreateEdge(final GraphClass classDiagram) {
		this.classDiagram = classDiagram;		
		
		
		
		// Na poprvě si musím načíst jazyk samostatně, protože v classDiagramu, kde se
		// tato instance této třídy vytváři
		// je ještě hodnota null v language properties, až po konsturuktoru se nastaví
		// jazyk:

		setLanguage(App.READ_FILE.getSelectLanguage());
	}

	
	
	
	@Override
	public void addAssociationEdge(final DefaultGraphCell[] cells) {	
		if (!GraphClass.KIND_OF_EDGE.isCommentCell(cells[0]) && !GraphClass.KIND_OF_EDGE.isCommentCell(cells[1])) {
			final String cellValue1 = cells[0].toString();
			final String cellValue2 = cells[1].toString();
			
			
			// Zjistím si cestu k projektu, a otestuji, zda se jedná opravdu o třídy:
			// Cesta k adrešíři src v projektu:
			final String pathToSrc = App.READ_FILE.getPathToSrc();

			if (pathToSrc != null) {
				// pro konkrétní cestu nahradím tečky v názvu balíčků na zpětné limétko

				// Zde existuje adresář src, tak se kouknu na cestu konkrétní třídy i s balíčky
				// v src:
				final String pathToCell1 = pathToSrc + File.separator + cellValue1.replace(".", File.separator)
						+ ".java";
				final String pathToCell2 = pathToSrc + File.separator + cellValue2.replace(".", File.separator)
						+ ".java";

				
				if (ReadFile.existsFile(pathToCell1) && ReadFile.existsFile(pathToCell2)) {
					// Zde obě třídy existují, tak mezi nimi mohu vytvořit asociaci

					
					// Zde si načtu texty obou tříd do kolekci:
					List<String> textOfClass1 = editCodeOfClass.getTextOfFile(pathToCell1);
					List<String> textOfClass2 = editCodeOfClass.getTextOfFile(pathToCell2);

					
					// kvůli názvu jména třídy:
					final File fileClass1 = new File(pathToCell1);
					final File fileClass2 = new File(pathToCell2);

					
					// Vezmu si název tridy bez pripony:
					final String className1 = fileClass1.getName().substring(0, fileClass1.getName().lastIndexOf('.'));
					final String className2 = fileClass2.getName().substring(0, fileClass2.getName().lastIndexOf('.'));

					
					// Prohledám obě kolekce - kód tříd a zjistím, kde mají třídy hlavíčku:
					// Zjištění inexu hlavičky první třídy:
					final int headerOfClassIndex1 = getIndexHeaderOfClass(textOfClass1, className1);
					// Zjištění indexu hlavičky druhé třídy:
					final int headerOfClassIndex2 = getIndexHeaderOfClass(textOfClass2, className2);
										
					
					
					// Na zjištěné indexy vložím reference:
					if (headerOfClassIndex1 > -1 && headerOfClassIndex2 > -1) {
						// Nejprve ještě potřebuji naimportova balicky - pokud se tridy v nejakem
						// nachazeji:
						// "Do cilove tridy naimportuji balicky ve kterem je zdrojova trida a naopak -
						// podobně pak i s atributy"

						// Nad hlavičku vložím balíčky třídy - pokud se nějaká třída nachází v jiném
						// balíčku:
						// Pokud se první třída nachází vnějakém balíčku, potřebuji ho do třídy, která
						// na tuto třídu má mít referenci naiportovat:

						// Zde otestuji, zda se mají importovat baličky třídy, na kterou se bude vkládat
						// reference
						// Pokud ano, naimportují se balíčky a odřádkuje se o index navíc než bez něj
						// pokud ne, tak se jen pod hlavičku vloží příslušný atribut - reference
						if (importPackages(cellValue1, textOfClass2, headerOfClassIndex2)) {
							// Zde ještě otestuji, zda se již daný atribut v dané třídě nenachází:
							// Pokud ne, tak ho přidám - jdu do podmínky, pokud, se v třídě již nachází, nic
							// nedělám

							// Zde může nastat možnost, že v dané třídě atribut již existuje, ale má se
							// vytvořit další, tak k němu musím
							// přidat další index:

							// proměnná, dokud se má opakovat cyklus stestováním
							boolean repeat = true;

							// index, kteý se přidá k proměnné, pokud je jich více:
							int indexForClassName = 1;

							// Cyklus, který otestuji, zda se ve třídě již daná proměnná nachází, pokud ne,
							// tak ji vloží, a pokud ano,
							// tak se ptá jaký index se má vložit k proměnné, aby nedošlo ke shodě názvů:

							while (repeat) {
								if (!isAttributeInClass(textOfClass2, className1,
										Character.toLowerCase(className1.charAt(0)) + className1.substring(1))) {
									textOfClass2.add(headerOfClassIndex2 + 3,
											"\tprivate " + className1 + " "
													+ Character.toLowerCase(className1.charAt(0))
													+ className1.substring(1) + ";");
									textOfClass2.add(headerOfClassIndex2 + 4, "\n");

									repeat = false;
								}

								else if (!isAttributeInClass(textOfClass2, className1,
										Character.toLowerCase(className1.charAt(0)) + className1.substring(1) + "_"
												+ indexForClassName)) {
									textOfClass2.add(headerOfClassIndex2 + 3,
											"\tprivate " + className1 + " "
													+ Character.toLowerCase(className1.charAt(0))
													+ className1.substring(1) + "_" + indexForClassName + ";");
									textOfClass2.add(headerOfClassIndex2 + 4, "\n");

									repeat = false;
								}

								else
									indexForClassName++;
							}					
						} else {
							// proměnná, dokud se má opakovat cyklus stestováním
							boolean repeat = true;

							// index, kteý se přidá k proměnné, pokud je jich více:
							int indexForClassName = 1;

							// Cyklus, který otestuji, zda se ve třídě již daná proměnná nachází, pokud ne,
							// tak ji vloží, a pokud ano,
							// tak se ptá jaký index se má vložit k proměnné, aby nedošlo ke shodě názvů:

							while (repeat) {
								if (!isAttributeInClass(textOfClass2, className1,
										Character.toLowerCase(className1.charAt(0)) + className1.substring(1))) {
									textOfClass2.add(headerOfClassIndex2 + 2,
											"\tprivate " + className1 + " "
													+ Character.toLowerCase(className1.charAt(0))
													+ className1.substring(1) + ";");
									textOfClass2.add(headerOfClassIndex2 + 3, "\n");

									repeat = false;
								}

								else if (!isAttributeInClass(textOfClass2, className1,
										Character.toLowerCase(className1.charAt(0)) + className1.substring(1) + "_"
												+ indexForClassName)) {
									textOfClass2.add(headerOfClassIndex2 + 2,
											"\tprivate " + className1 + " "
													+ Character.toLowerCase(className1.charAt(0))
													+ className1.substring(1) + "_" + indexForClassName + ";");
									textOfClass2.add(headerOfClassIndex2 + 3, "\n");

									repeat = false;
								}

								else
									indexForClassName++;
							}
						}
						
						// Zde to samé ale naopak:
						// Otestují se, zda se maji vložit balíček, pokud ano, tak se vloží a pak o indx
						// navíc se vloží příslučný atribut - reerence
						// a nebo se balíčky nevloží a vloží se pouze atribut pod hlavičku:
						if (importPackages(cellValue2, textOfClass1, headerOfClassIndex1)) {
							// Zde opět otestuji, zda se v třídě již daný atribut nenachází:

							// proměnná, dokud se má opakovat cyklus stestováním
							boolean repeat = true;

							// index, kteý se přidá k proměnné, pokud je jich více:
							int indexForClassName = 1;

							// Cyklus, který otestuji, zda se ve třídě již daná proměnná nachází, pokud ne,
							// tak ji vloží, a pokud ano,
							// tak se ptá jaký index se má vložit k proměnné, aby nedošlo ke shodě názvů:

							while (repeat) {
								if (!isAttributeInClass(textOfClass1, className2,
										Character.toLowerCase(className2.charAt(0)) + className2.substring(1))) {
									textOfClass1.add(headerOfClassIndex1 + 3,
											"\tprivate " + className2 + " "
													+ Character.toLowerCase(className2.charAt(0))
													+ className2.substring(1) + ";");
									textOfClass1.add(headerOfClassIndex1 + 4, "\n");

									repeat = false;
								}

								else if (!isAttributeInClass(textOfClass1, className2,
										Character.toLowerCase(className2.charAt(0)) + className2.substring(1) + "_"
												+ indexForClassName)) {
									textOfClass1.add(headerOfClassIndex1 + 3,
											"\tprivate " + className2 + " "
													+ Character.toLowerCase(className2.charAt(0))
													+ className2.substring(1) + "_" + indexForClassName + ";");
									textOfClass1.add(headerOfClassIndex1 + 4, "\n");

									repeat = false;
								}

								else
									indexForClassName++;
							}
						}

						else {
							// proměnná, dokud se má opakovat cyklus stestováním
							boolean repeat = true;

							// index, kteý se přidá k proměnné, pokud je jich více:
							int indexForClassName = 1;

							// Cyklus, který otestuji, zda se ve třídě již daná proměnná nachází, pokud ne,
							// tak ji vloží, a pokud ano,
							// tak se ptá jaký index se má vložit k proměnné, aby nedošlo ke shodě názvů:

							while (repeat) {
								if (!isAttributeInClass(textOfClass1, className2,
										Character.toLowerCase(className2.charAt(0)) + className2.substring(1))) {
									textOfClass1.add(headerOfClassIndex1 + 2,
											"\tprivate " + className2 + " "
													+ Character.toLowerCase(className2.charAt(0))
													+ className2.substring(1) + ";");
									textOfClass1.add(headerOfClassIndex1 + 3, "\n");

									repeat = false;
								}

								else if (!isAttributeInClass(textOfClass1, className2,
										Character.toLowerCase(className2.charAt(0)) + className2.substring(1) + "_"
												+ indexForClassName)) {
									textOfClass1.add(headerOfClassIndex1 + 2,
											"\tprivate " + className2 + " "
													+ Character.toLowerCase(className2.charAt(0))
													+ className2.substring(1) + "_" + indexForClassName + ";");
									textOfClass1.add(headerOfClassIndex1 + 3, "\n");

									repeat = false;
								}

								else
									indexForClassName++;
							}
						}
						

						// Zápis tříd zpět do souboru - obě třídy:, pokud proběhl v pořádku přidá se
						// hrana do grafu:
						if (editCodeOfClass.setTextToFile(pathToCell1, textOfClass1)
								&& editCodeOfClass.setTextToFile(pathToCell2, textOfClass2))
							// Zde proběhlo vše v pořádku, tak mohu přidat hranu do grafu:
							classDiagram.addAssociationEdge(cells[0], cells[1], 1);
					}
				} 
				else
					JOptionPane.showMessageDialog(classDiagram, missingClassText, missingClassTitle,
							JOptionPane.ERROR_MESSAGE);
			}
		} 
		else
			JOptionPane.showMessageDialog(classDiagram, wrongSelectedObjectText, wrongSelectedObjectTitle,
					JOptionPane.ERROR_MESSAGE);
	}
	
	
	

	
	
	
	
	
	@Override
	public void addExtendsEdge(final DefaultGraphCell[] cells) {
		// Postup:
		// Zjistím, zda třída existuje, případně to oznámín, pokud existuje, tak
		// otestuji, zda z něčeho již dědí, případně to oznámín
		// pokud ne, tak naimportuji balíčky(pokud bude potřeba) a přidám dědičnost

		if (!GraphClass.KIND_OF_EDGE.isCommentCell(cells[0]) && !GraphClass.KIND_OF_EDGE.isCommentCell(cells[1])) {
			final String cellValue1 = cells[0].toString();
			final String cellValue2 = cells[1].toString();
			
			// Zjistím si cestu k projektu, a otestuji, zda se jedná opravdu o třídy:
			// Cesta k adrešíři src v projektu:
			final String pathToSrc = App.READ_FILE.getPathToSrc();
			
			if (pathToSrc != null) {
				// pro konkrétní cestu nahradím tečky v názvu balíčků na zpětné limétko
				
				
				// Zde existuje adresář src, tak se kouknu na cestu konkrétní třídy i s balíčky v src:
				final String pathToCell1 = pathToSrc + File.separator + cellValue1.replace(".", File.separator)
						+ ".java";
				final String pathToCell2 = pathToSrc + File.separator + cellValue2.replace(".", File.separator)
						+ ".java";
				
				if (ReadFile.existsFile(pathToCell1) && ReadFile.existsFile(pathToCell2)) {
					// Zde obě třídy existují, tak mezi nimi mohu vytvořit dědičnost
					
					
					// Zde si načtu texty třídy pro přidání dědičnosti do kolekce:
					List<String> textOfClass1 = editCodeOfClass.getTextOfFile(pathToCell1);
					

					
					// kvůli názvu jména třídy:
					final File fileClass1 = new File(pathToCell1);
					final File fileClass2 = new File(pathToCell2);

					// Bezmu si název tridy bez pripony:
					final String className1 = fileClass1.getName().substring(0, fileClass1.getName().lastIndexOf('.'));
					final String className2 = fileClass2.getName().substring(0, fileClass2.getName().lastIndexOf('.'));
					
									

					// Prohledám kolekci a zjistím si index řádku - kolekce hlavičky třídy:
					// Zjištění inexu hlavičky třídy:
					final int headerOfClassIndex1 = getIndexHeaderOfClass(textOfClass1, className1);
										
					
					
					
					// Stačí znát hlavičku první třídy - té která má dědit z druhé označené - cílové
					// Pokud jsem našel hlavičku, tak otestuji, zda třída ještě z niččeho nedědí.
					if (headerOfClassIndex1 > -1) {
						// Test jestli zdrojová třáda z něčeho dědí:
						if (!classInheritsfromSomething(textOfClass1, headerOfClassIndex1)) {
							// Zde potřebuji proměnnou, protože pokud třída z něčeho již dědí, metoda
							// addExtendToHeaderClass vrátí null, a na to
							// je třeba reagovat u přidání hrany nebo ne
							boolean variable = true;
							// Zde zdrojová třída z níčeho nedědí, tak doimportuji balíčky (pokud je to
							// potřeba:
							if (importPackages(cellValue2, textOfClass1, headerOfClassIndex1)) {
								// Zde byly potřeba doimportovat balíčky, tak se hlavička o řádek posunula:
								// Do hlavičky přidám dědičnost dané třídy:
								final String textHeaderOfClass = addExtendsToHeaderClass(
										textOfClass1.get(headerOfClassIndex1 + 1), className1, className2);

								if (textHeaderOfClass != null)
									textOfClass1.set(headerOfClassIndex1 + 1, textHeaderOfClass);
								// Třída z něčeho dědí nebo došlo k chybě (němělo by nastat), tak nesmím v grafu
								// vytvořit hranu:
								else
									variable = false;
							}
							
							else {
								// Zde nebylo potřeba doimportovat balíčky, tak něni se hlavička třídy
								// neposunula:
								// Do hlavičky přidám dědičnost dané třídy
								final String textHeaderOfClass = addExtendsToHeaderClass(
										textOfClass1.get(headerOfClassIndex1), className1, className2);

								if (textHeaderOfClass != null)
									textOfClass1.set(headerOfClassIndex1, textHeaderOfClass);
								else
									variable = false;
							}

							// Zápis třídy zpět do souboru - pokud proběhl v pořádku přidá se hrana do
							// grafu:
							// Přidal jsem dědičnost pouze do první označené třídy s druhou není třeba nic
							// dělt - v této metodě jsem její obsah nezměnil
							if (variable && editCodeOfClass.setTextToFile(pathToCell1, textOfClass1))
								// Zde proběhlo vše v pořádku, tak mohu přidat hranu do grafu:
								classDiagram.addExtendsEdge(cells[0], cells[1]);
						} 
						else
							JOptionPane.showMessageDialog(classDiagram, inheritErrorText, inheritErrorTitle,
									JOptionPane.ERROR_MESSAGE);
					}
				} 
				else
					JOptionPane.showMessageDialog(classDiagram, missingClassText, missingClassTitle,
							JOptionPane.ERROR_MESSAGE);
			}
		}
		else
			JOptionPane.showMessageDialog(classDiagram, wrongSelectedObjectText, wrongSelectedObjectTitle,
					JOptionPane.ERROR_MESSAGE);
	}
	
	
	
	
	
	


	@Override
	public void addImplementsEdge(final DefaultGraphCell[] cells) {
		// Postup:
		// Zda obě třídy existují - nejdená se o komentáře nebo o hrany,
		// Otestuji, zda je cílový objekt rozhraní a zdrojový obejkt není výčet
		// POkud jso updmínky splněny, tak zapíšu kódy do souborů - třídy a vytvořím v
		// grafu hranu:

		if (!GraphClass.KIND_OF_EDGE.isCommentCell(cells[0]) && !GraphClass.KIND_OF_EDGE.isCommentCell(cells[1])) {

			final String cellValue1 = cells[0].toString();
			final String cellValue2 = cells[1].toString();
			
			
			// Zjistím si cestu k projektu, a otestuji, zda se jedná opravdu o třídy:
			
			// Cesta k adrešíři src v projektu:
			final String pathToSrc = App.READ_FILE.getPathToSrc();
			
			if (pathToSrc != null) {
				// pro konkrétní cestu nahradím tečky v názvu balíčků na zpětné limétko
				
				
				// Zde existuje adresář src, tak se kouknu na cestu konkrétní třídy i s balíčky v src:
				final String pathToCell1 = pathToSrc + File.separator + cellValue1.replace(".", File.separator)
						+ ".java";
				final String pathToCell2 = pathToSrc + File.separator + cellValue2.replace(".", File.separator)
						+ ".java";
				
				if (ReadFile.existsFile(pathToCell1) && ReadFile.existsFile(pathToCell2)) {
					// Zde obě třídy existují, tak mezi nimi mohu vytvořit implementaci						
					
					
					// Zde si načtu texty třídy pro přidání dědičnosti do kolekce:
					List<String> textOfClass1 = editCodeOfClass.getTextOfFile(pathToCell1);
					// Zde si zjistím i kó druhé= třídy ale jen si ho přečtu - nic s ním nedělám,
					// pouze potřebuji zjistit, zda se jedná o rozhraní:
					List<String> textOfClass2 = editCodeOfClass.getTextOfFile(pathToCell2);

					
					// kvůli názvu jména třídy:
					final File fileClass1 = new File(pathToCell1);
					final File fileClass2 = new File(pathToCell2);
					
					// Bezmu si název tridy bez pripony:
					final String className1 = fileClass1.getName().substring(0, fileClass1.getName().lastIndexOf('.'));
					final String className2 = fileClass2.getName().substring(0, fileClass2.getName().lastIndexOf('.'));
					
									

					// Prohledám kolekci a zjistím si index řádku - kolekce hlavičky třídy:
					// Zjištění inexu hlavičky třídy:
					final int headerOfClassIndex1 = getIndexHeaderOfClass(textOfClass1, className1);
					// Zjistím si index hlavičky druhé třídy a pomocí něj otestuji, zda se jedná o
					// rozhraní:
					final int headerOfClassIndex2 = getIndexHeaderOfClass(textOfClass2, className2);

					// Note: Následujících pár podmínek by šlo zkrátit, např nemusel bych zvlášt
					// testovat a vypisovat
					// že rozhraní nemůže implementovat rohraní, zda nějaká třída je rozhraní apod.
					// šlo by to i napsat do jedné či dvou podmínek pro otestovaní jestli cílová
					// třída je rozhraní
					// zdrojojvá není a není ani výčet ale z hlediska účelu programu jsem to chtěl
					// trochu přizpůsobit uživateli tím,
					// že mu oznámím, o jakou chybu se jedná
					
					
					
					// Stačí znát hlavičku první třídy - té která má dědit z druhé označené - cílové
					// Pokud jsem našel hlavičku, tak otestuji, zda třída ještě z niččeho nedědí.
					if (headerOfClassIndex1 > -1 && headerOfClassIndex2 > -1) {
						
						// Mám hlavičky a kód obou tříd, tak zjistím, zda 'cílová' třída je rozhraní:
						if (isClassInterface(textOfClass2.get(headerOfClassIndex2))) {
							
							// Zde vím, že cílová třída je interface, tak otestuji, zda zdrojová třída není interface:
							if (!isClassInterface(textOfClass1.get(headerOfClassIndex1))) {
								
								// Zde otestuji, zda zdrojová třída není enum, ten nemůže implementovat rozhraní:
								if (!isClassEnum(textOfClass1.get(headerOfClassIndex1))) {
									
									// Zde by mělo být "vše" v pořádku, tak mohu naimportovat balíček (pokud se cílová třída nachází v jiném než zdrojová):
									if (importPackages(cellValue2, textOfClass1, headerOfClassIndex1)) {
										// Zde jsem musel naimportovat balíčky, tak se hlavička posunula
										// Přídám implementaci rozhraní do hlavičky třídy:
										
										final String newTextHeaderOfClass = addInterfaceToClass(textOfClass1.get(headerOfClassIndex1 + 1), className1, className2);
										
										if (!newTextHeaderOfClass.isEmpty())
											textOfClass1.set(headerOfClassIndex1 + 1, newTextHeaderOfClass);
									}
									
									else {
										// Zde jsem nemusel importovat balíčky, tak hlavička zůstala na stejném místě:
										final String newTextHeaderOfClass = addInterfaceToClass(textOfClass1.get(headerOfClassIndex1), className1, className2);
										
										if (!newTextHeaderOfClass.isEmpty())
											textOfClass1.set(headerOfClassIndex1, newTextHeaderOfClass);
									}
									
									// Zde proběhlo "vše v pořádku", tak stačí uložit změnenou 'Zdrojovou' třídu zpět do souboru:
									if (editCodeOfClass.setTextToFile(pathToCell1, textOfClass1))
										classDiagram.addImplementsEdge(cells[0], cells[1]);
								}

								else
									JOptionPane.showMessageDialog(classDiagram, enumInheritErrorText,
											enumInheritErrorTitle, JOptionPane.ERROR_MESSAGE);
							}

							else
								JOptionPane.showMessageDialog(classDiagram, interfaceErrorText, interfaceErrorTitle,
										JOptionPane.ERROR_MESSAGE);
						}

						else
							JOptionPane.showMessageDialog(classDiagram,
									implementsErrorText_1 + className2 + " " + implementsErrorText_2,
									implementsErrorTitle, JOptionPane.ERROR_MESSAGE);
					}
				}

				else
					JOptionPane.showMessageDialog(classDiagram, missingClassText, missingClassTitle,
							JOptionPane.ERROR_MESSAGE);
			}
		}

		else
			JOptionPane.showMessageDialog(classDiagram, wrongSelectedObjectText, wrongSelectedObjectTitle,
					JOptionPane.ERROR_MESSAGE);
	}


	
	
	

	@Override
	public void addAggregation_1_ku_1_Edge(final DefaultGraphCell[] cells) {
		// Postup:
		// Otestuji, zda obě třídy existují - nejedná se o komentáře či hrany,
		// Pokud obě třídy existují, tak naimportuji balíčky - pokud se nachází v různých balíččcích
		// Pokud budou podmínky splněny a balíček úspěšně naimportovách, vytvoří se odkaz na cílovou třídu a hrana v grafu
		
		if (!GraphClass.KIND_OF_EDGE.isCommentCell(cells[0]) && !GraphClass.KIND_OF_EDGE.isCommentCell(cells[1])) {
			
			final String cellValue1 = cells[0].toString();
			final String cellValue2 = cells[1].toString();
			
			
			// Zjistím si cestu k projektu, a otestuji, zda se jedná opravdu o třídy:
			// Cesta k adrešíři src v projektu:
			final String pathToSrc = App.READ_FILE.getPathToSrc();
			
			if (pathToSrc != null) {
				// pro konkrétní cestu nahradím tečky v názvu balíčků na zpětné limétko
				
				
				// Zde existuje adresář src, tak se kouknu na cestu konkrétní třídy i s balíčky v src:
				final String pathToCell1 = pathToSrc + File.separator + cellValue1.replace(".", File.separator)
						+ ".java";
				final String pathToCell2 = pathToSrc + File.separator + cellValue2.replace(".", File.separator)
						+ ".java";
				
				if (ReadFile.existsFile(pathToCell1) && ReadFile.existsFile(pathToCell2)) {
					// Zde obě třídy existují, tak mezi nimi mohu vytvořit agragaci						
					
					
					// Zde si načtu texty třídy pro přidání agregace do kolekce:
					final List<String> textOfClass1 = editCodeOfClass.getTextOfFile(pathToCell1);
					

					
					// kvůli názvu jména třídy:
					final File fileClass1 = new File(pathToCell1);
					final File fileClass2 = new File(pathToCell2);
				
					// Vezmu si název tridy bez pripony:
					final String className1 = fileClass1.getName().substring(0, fileClass1.getName().lastIndexOf('.'));
					final String className2 = fileClass2.getName().substring(0, fileClass2.getName().lastIndexOf('.'));
					
									

					// Prohledám kolekci a zjistím si index řádku - kolekce hlavičky třídy:
					// Zjištění inexu hlavičky třídy:
					final int headerOfClassIndex1 = getIndexHeaderOfClass(textOfClass1, className1);
										
					
					
					
					// Stačí znát hlavičku první třídy - té která má mít referenci na druhou označenou - cílovou třídu
					// Pokud jsem našel hlavičku, tak otestuji, zda třída ještě z niččeho nedědí.
					if (headerOfClassIndex1 > -1) {
						// Zde jsem naše hlavičku třídy, tak mohu otestovat baličky, připadně je přidat
						if (importPackages(cellValue2, textOfClass1, headerOfClassIndex1)) {
							// zde se naimportoval baliček, tak se hlavička třídy posunula:
							// Nyní mohu přidat referenci na cílovou třídu:
							// Ale nejdříve otestuji, zda se tam již reference nenachází, jinak byc ji přidal znovu:
						
							
							// proměnná, dokud se má opakovat cyklus stestováním
							boolean repeat = true;
							
							// index, kteý se přidá k proměnné, pokud je jich více:
							int indexForClassName = 1;
							
							// Cyklus, který otestuji, zda se ve třídě již daná proměnná nachází, pokud ne, tak ji vloží, a pokud ano,
							// tak se ptá jaký index se má vložit k proměnné, aby nedošlo ke shodě názvů:
							
							while (repeat) {
								if (!isAttributeInClass(textOfClass1, className2,
										Character.toLowerCase(className2.charAt(0)) + className2.substring(1))) {
									// Zde se atribut ve třídě ještě nenachází, tak ho musím přidat
									textOfClass1.add(headerOfClassIndex1 + 3,
											"\tprivate " + className2 + " "
													+ Character.toLowerCase(className2.charAt(0))
													+ className2.substring(1) + ";");
									textOfClass1.add(headerOfClassIndex1 + 4, "\n");

									repeat = false;
								}

								else if (!isAttributeInClass(textOfClass1, className2,
										Character.toLowerCase(className2.charAt(0)) + className2.substring(1) + "_"
												+ indexForClassName)) {
									// Zde se atribut ve třídě ještě nenachází, tak ho musím přidat
									textOfClass1.add(headerOfClassIndex1 + 3,
											"\tprivate " + className2 + " "
													+ Character.toLowerCase(className2.charAt(0))
													+ className2.substring(1) + "_" + indexForClassName + ";");
									textOfClass1.add(headerOfClassIndex1 + 4, "\n");

									repeat = false;
								}

								else
									indexForClassName++;
							}
						}

						else {
							// proměnná, dokud se má opakovat cyklus stestováním
							boolean repeat = true;

							// index, kteý se přidá k proměnné, pokud je jich více:
							int indexForClassName = 1;

							// Cyklus, který otestuje, zda se ve třídě již daná proměnné nachází, pokud ne,
							// tak ji vloží, a pokud ano,
							// tak se ptá jaký index se má vložit k proměnné, aby nedošlo ke shodě názvů:

							while (repeat) {
								if (!isAttributeInClass(textOfClass1, className2,
										Character.toLowerCase(className2.charAt(0)) + className2.substring(1))) {
									// textOfClass1.add(headerOfClassIndex1 + 2, " private " + className2 + " " +
									// Character.toLowerCase(className2.charAt(0)) + className2.substring(1) + ";");
									textOfClass1.add(headerOfClassIndex1 + 2,
											"\tprivate " + className2 + " "
													+ Character.toLowerCase(className2.charAt(0))
													+ className2.substring(1) + ";");
									textOfClass1.add(headerOfClassIndex1 + 3, "\n");

									repeat = false;
								}

								else if (!isAttributeInClass(textOfClass1, className2,
										Character.toLowerCase(className2.charAt(0)) + className2.substring(1) + "_"
												+ indexForClassName)) {
									// textOfClass1.add(headerOfClassIndex1 + 2, " private " + className2 + " " +
									// Character.toLowerCase(className2.charAt(0)) + className2.substring(1) + ";");
									textOfClass1.add(headerOfClassIndex1 + 2,
											"\tprivate " + className2 + " "
													+ Character.toLowerCase(className2.charAt(0))
													+ className2.substring(1) + "_" + indexForClassName + ";");
									textOfClass1.add(headerOfClassIndex1 + 3, "\n");

									repeat = false;
								}

								else
									indexForClassName++;
							}
						}

						// Zde proběhlo vše v pořádku, tak mohu zapsat změnenou třídu, a pokud to
						// proběhne v pořádku, přidám hranu do grafu:
						if (editCodeOfClass.setTextToFile(pathToCell1, textOfClass1))
							classDiagram.addAggregation_1_1ku_1_Edge(cells[0], cells[1], 1, 1);
					}
				} 
				else
					JOptionPane.showMessageDialog(classDiagram, missingClassText, missingClassTitle,
							JOptionPane.ERROR_MESSAGE);
			}
		} 
		else
			JOptionPane.showMessageDialog(classDiagram, wrongSelectedObjectText, wrongSelectedObjectTitle,
					JOptionPane.ERROR_MESSAGE);
	}


	
	
	

	@Override
	public void addAggregation_1_ku_N_Edge(final DefaultGraphCell[] cells) {
		// Postup:
		// Otestuje se, zda dané třídy existují, pokud ano, přidají se balíčky
		// pokud to dopadne dobře, uloži se změna třídy do souboru a pokud nedojde k chybě, vytvoří 
		// se vazba mezi označenými třídami v grafu
		
		
		if (!GraphClass.KIND_OF_EDGE.isCommentCell(cells[0]) && !GraphClass.KIND_OF_EDGE.isCommentCell(cells[1])) {
			
			final String cellValue1 = cells[0].toString();
			final String cellValue2 = cells[1].toString();
			
			
			// Zjistím si cestu k projektu, a otestuji, zda se jedná opravdu o třídy:
			// Cesta k adrešíři src v projektu:
			final String pathToSrc = App.READ_FILE.getPathToSrc();
			
			if (pathToSrc != null) {
				// pro konkrétní cestu nahradím tečky v názvu balíčků na zpětné limétko
				
				
				// Zde existuje adresář src, tak se kouknu na cestu konkrétní třídy i s balíčky v src:
				final String pathToCell1 = pathToSrc + File.separator + cellValue1.replace(".", File.separator)
						+ ".java";
				final String pathToCell2 = pathToSrc + File.separator + cellValue2.replace(".", File.separator)
						+ ".java";
				
				if (ReadFile.existsFile(pathToCell1) && ReadFile.existsFile(pathToCell2)) {
					// Zde obě třídy existují, tak mezi nimi mohu vytvořit agragaci						
					
					
					// Zde si načtu texty třídy pro přidání agregace do kolekce:
					List<String> textOfClass1 = editCodeOfClass.getTextOfFile(pathToCell1);
					

					
					// kvůli názvu jména třídy:
					final File fileClass1 = new File(pathToCell1);
					final File fileClass2 = new File(pathToCell2);
					
					// Bezmu si název tridy bez pripony:
					final String className1 = fileClass1.getName().substring(0, fileClass1.getName().lastIndexOf('.'));
					final String className2 = fileClass2.getName().substring(0, fileClass2.getName().lastIndexOf('.'));
					
									

					// Prohledám kolekci a zjistím si index řádku - kolekce hlavičky třídy:
					// Zjištění inexu hlavičky třídy:
					final int headerOfClassIndex1 = getIndexHeaderOfClass(textOfClass1, className1);
										
					
					
					
					// Stačí znát hlavičku první třídy - té která má mít referenci na druhou označenou - cílovou třídu
					// Pokud jsem našel hlavičku, tak otestuji, zda třída ještě z ničeho nedědí.
                    if (headerOfClassIndex1 > -1) {
                        // Zde jsem našel hlavičku třídy, tak mohu otestovat baličky, připadně je přidat
                        if (importPackages(cellValue2, textOfClass1, headerOfClassIndex1))
                            // V tomto případě se importoval balíček, takže se posunula hlavička třídy:
                            textOfClass1 = editSourceCode(textOfClass1, className2, headerOfClassIndex1 + 1);
                            // Zde se neimportoval balíček, nejspíše už byl importován, takže se hlavička neposunula:
                        else textOfClass1 = editSourceCode(textOfClass1, className2, headerOfClassIndex1);

                        /*
                         * V této fázi by již balíčky měly být naimportovány (pokud nebyly) a atribut přidaný, tak se
                         * může přidat reprezentace do diagramu třid. Pokud by někde výše selhal nějaký import
                         * (například), tak by tu chybu ohlásil kompilátor uživateli.
                         */
                        if (editCodeOfClass.setTextToFile(pathToCell1, textOfClass1))
                            classDiagram.addAggregation_1_ku_N_Edge(cells[0], cells[1]);
                    }
                }
				else
					JOptionPane.showMessageDialog(classDiagram, missingClassText, missingClassTitle,
							JOptionPane.ERROR_MESSAGE);
			}
		}
		else
			JOptionPane.showMessageDialog(classDiagram, wrongSelectedObjectText, wrongSelectedObjectTitle,
					JOptionPane.ERROR_MESSAGE);
	}


	
	
	
	
	

	/**
	 * Metoda, která "upraví" zdrojový kód třídy - přidá do něj referenci na List
	 * cílové třídy: (pokud již neexistuje)
	 * 
	 * @param textOfClass1
	 *            - zdrojový kód třídy k editaci
	 * 
	 * @param className2
	 *            - název cílové třídy
	 * 
	 * @param headerOfClassIndex1
	 *            - index hlavičky třídy
	 * 
	 * @return upravený zdrojový kód třídy
	 */
	private static List<String> editSourceCode(List<String> textOfClass1, final String className2,
			final int headerOfClassIndex1) {
		// Zde potřebuji otestovat, ještě zda se nachází balíčky pro List z java.util
		if (!isImportedPackageForList(textOfClass1)) {
			// Zde se balíčky z java.util.list nejsou naimportované, tak tak je musím
			// naimportovat
			textOfClass1 = addPackageForList(textOfClass1, headerOfClassIndex1);
			// Zde mám již balíčky naimportované, tak otestuji atribut - dostupnot List
			// <cilova trida>

			if (!existListInClass(textOfClass1, className2)) {
				// Zde atribut - List<Cílová třída> ještě neexistuje, tak ho přidám:
				textOfClass1.add(headerOfClassIndex1 + 3, "\n");
				
				textOfClass1.add(headerOfClassIndex1 + 4, "\tprivate List<" + className2 + "> "
						+ Character.toLowerCase(className2.charAt(0)) + className2.substring(1) + "List;");
				
				textOfClass1.add(headerOfClassIndex1 + 5, "\n");
			}
			// else - zde již atribut existuje, tak není co řešit, stačí přidat hranu
		}
		// else - zde jsou baličky naimportované, tak otestuji atribut:
		else {
			if (!existListInClass(textOfClass1, className2)) {
				// Zde atribut - List<Cílová třída> ještě neexistuje, tak ho přidám:
				textOfClass1.add(headerOfClassIndex1 + 2, "\n");
				
				textOfClass1.add(headerOfClassIndex1 + 3, "\tprivate List<" + className2 + "> "
						+ Character.toLowerCase(className2.charAt(0)) + className2.substring(1) + "List;");
				
				textOfClass1.add(headerOfClassIndex1 + 4, "\n");
			}
			// else - zde již atribut existuje, tak není co řešit, stačí přidat hranu
		}
		return textOfClass1;
	}


	
	
	
	
	

	@Override
	public void addComment(final DefaultGraphCell cell) {
		// Postup:
		// Otestuji, zda zvolená tříd vůbec existuje, pokud ano
		// tak k ni v grafu přidám komentář
		// Komentář je pouze v grafu, nikde jinde		
		// Pokud ne oznámím to uživateli jako chybu - nemělo by nastat (pokud uživatel nezoznačí komentář) !
		
		
		// Úplně na začátek otestuji, zda se nejedná o bunku, která reprezentuje komentář:
		if (!GraphClass.KIND_OF_EDGE.isCommentCell(cell)) {
			// Zde se nejedná o buňku, která reprezentuje komentář, takže zbývá pouze třída, tak otestuji, zda existuje
			// jetě v adresíři src - tam kde má - v příslušném balíčku:
			
			
			// vezmu si text buňky - název třídy i s balíčky:
			final String cellValue = cell.toString();
			
			
			// Zjistím si cestu k projektu, a otestuji, zda se jedná opravdu o třídu:
			
			// Cesta k adrešíři src v projektu:
			final String pathToSrc = App.READ_FILE.getPathToSrc();
			
			if (pathToSrc != null) {
				// pro konkrétní cestu nahradím tečky v názvu balíčků na zpětné limétko
							
				// Zde existuje adresář src, tak se kouknu na cestu konkrétní třídy i s balíčky v src:
				final String pathToCell = pathToSrc + File.separator + cellValue.replace(".", File.separator) + ".java";

				if (ReadFile.existsFile(pathToCell))
					// Zde třída existuje, tak k ní mohu připojit komenář:
					classDiagram.addComment(cell, null);

				else
					JOptionPane.showMessageDialog(classDiagram, selectedClassErrorText + pathToCell,
							selectedClassErrorTitle, JOptionPane.ERROR_MESSAGE);
			} else
				JOptionPane.showMessageDialog(classDiagram, missingSrcDirectoryText + pathToSrc,
						missingSrcDirectoryTitle, JOptionPane.ERROR_MESSAGE);
		} else
			JOptionPane.showMessageDialog(classDiagram, choosedCommentNotClassText, choosedCommentNotClassTitle,
					JOptionPane.ERROR_MESSAGE);
	}
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public boolean addImplementedMethods(final DefaultGraphCell cell, final Method[] methods) {
		if (!GraphClass.KIND_OF_EDGE.isClassCell(cell)) {
			JOptionPane.showMessageDialog(classDiagram, wrongSelectedObjectText, wrongSelectedObjectTitle,
					JOptionPane.ERROR_MESSAGE);

			return false;
		}

		final String cellValue = cell.toString();

		// Zjistím si cestu k projektu, a otestuji, zda se jedná opravdu o třídy:
		// Cesta k adrešíři src v projektu:
		final String pathToSrc = App.READ_FILE.getPathToSrc();

		if (pathToSrc == null)
			return false;


		// pro konkrétní cestu nahradím tečky v názvu balíčků na zpětné lomítko

		// Zde existuje adresář src, tak se kouknu na cestu konkrétní třídy i s balíčky
		// v src:
		final String pathToCell = pathToSrc + File.separator + cellValue.replace(".", File.separator) + ".java";


		if (!ReadFile.existsFile(pathToCell)) {
			JOptionPane.showMessageDialog(classDiagram, missingClassText, missingClassTitle,
					JOptionPane.ERROR_MESSAGE);
			return false;
		}

		// Zde si načtu texty třídy pro přidání implementace:
		final List<String> textOfClass = editCodeOfClass.getTextOfFile(pathToCell);

		/*
		 * Index pro určení řádku poslední závorky - pro uzavření třídy.
		 */
		int index = -1;

		/*
		 * Cyklus pro nalezení poslední složené závorky, která slouží pro "uzavření"
		 * třídy.
		 */
		for (int i = textOfClass.size() - 1; i >= 0; i--)
			if (textOfClass.get(i).matches(Constants.REG_EX_CLOSING_BRACKET)) {
				index = i;
				break;
			}


		if (index == -1)
			return false;

		// Zde mohu přidat do kolekce nad tu uzavírací závorku vygenerované metody:
		textOfClass.addAll(index, getImplementedMethods(methods));

		return editCodeOfClass.setTextToFile(pathToCell, textOfClass);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která převede veškeré metody do textové podoby tak, že je možné je
	 * implementovat do příslušné třídy.
	 * 
	 * Tj. konkrétně převede hlavičky metod z rozhraní do "implementační" podoby,
	 * například je v rozhraní deklarace mtody v syntaxi: 'void name(p p1);', pak se
	 * tato syntaxe převede do následující podoby a zapíše se do příslušné třídy.
	 * 
	 * nová podoba:
	 * 
	 * _@Override
	 * public void name(p p) {
	 * 	// generation comment
	 * }
	 * 
	 * @param methods
	 *            - jednorozměrné pole, které obsahuje metody, které se mají převést
	 *            do výše uvedené podoby.
	 * 
	 * @return list, jehož jednotlivé položky jsou jednotlivé řádky výše uvedených
	 *         metod.
	 */
	private static List<String> getImplementedMethods(final Method[] methods) {
		/*
		 * list, do kterého budu vkládat veškeré nové řádky příslušné metody.
		 */
		final List<String> methodsList = new ArrayList<>();
		
		// První odřádkování, resp. 2 řádky pro mezeru pro oddělení ostatního kódu::
		methodsList.add("\n");

		/*
		 * Vyfiltruji se veškeré metody z rozhraní, které nejsou statické, protože
		 * statické metody musí být implementované přímo v rozhraní.
		 */
		Arrays.stream(methods).filter(m -> !Modifier.isStatic(m.getModifiers())).forEach(m -> {
			methodsList.add("\t@Override");

			String headerOfMethod = "\tpublic ";

			
			// Název metody, tedy k public přidám ještě návratový datový typ metody a název metody:
			headerOfMethod += ParameterToText.getMethodReturnTypeInText(m, false) + " " + m.getName() + "(";

			// Přidám i parametry metody:
			headerOfMethod += getParametersOfMethod(m.getParameters());

			// Do těla přidám výchozí komentáře:
			headerOfMethod += ") {\n\t\t// TODO Auto-generated method stub";

			// Přidám do listu pro kód již vygenerovanou hlavičku: s komentářem
			methodsList.add(headerOfMethod);
			
			/*
			 * Zde otestuji návratový datový typ metody. Jedná se o to, že když metoda vrací
             * objekt, pak ve výchozím nastavení bude vracet null, a u číselných datový typů
             * bude vracet nulu, u booleanu false atd.
             */
            if (m.getReturnType().equals(Void.TYPE))
                methodsList.add("\n\t}\n");

            else if (!m.getReturnType().isPrimitive())
                methodsList.add("\t\treturn null;\n\t}\n");


            else if (m.getReturnType().equals(byte.class) || m.getReturnType().equals(short.class)
                    || m.getReturnType().equals(int.class) || m.getReturnType().equals(long.class)
                    || m.getReturnType().equals(double.class) || m.getReturnType().equals(float.class)
                    || m.getReturnType().equals(char.class))
                methodsList.add("\t\treturn 0;\n\t}\n");

            else if (m.getReturnType().equals(boolean.class))
                methodsList.add("\t\treturn false;\n\t}\n");
        });

		return methodsList;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metdoda, která slouží pro převod parametrů nějaké metody do textové podoby,
	 * tj. veškeré parametry v parametru paramters se odděli desetinnou čárkou v
	 * syntaxi: DataTypeOfParameters variableName, nextVaraibleDataType
	 * nextVariableName.
	 * 
	 * @param parameters
	 *            parametr, ze kterých si vytáhnu jejích datové typy
	 * 
	 * @return vytvořím výše uvedenou syntaxi a vrátím jí
	 */
    private static String getParametersOfMethod(final Parameter[] parameters) {
        final List<String> namesOfParameters = new ArrayList<>();

        final StringBuilder methodBuilder = new StringBuilder();

        for (final Parameter p : parameters) {
            /*
             * Vezmu si název parametru, resp. název příslušného typu paametru s prvním
             * písmenem malým.
             */
            String name = getFirstCharSmall(p.getType().getSimpleName());


            /*
             * Následující podmínka slouží pouze pro to, aby se v hlavičce té metody
             * nenacházely název parametrů se stejným název, protože pokud budou v té metodě
             * například dva parametry typu int (int, int), pak se by se vygeneroval název:
             * (int int, int int) ale to je špatně, proto potřebuji ty názvy indexovat, aby
             * se veškeré názvy parametrů nacházely v té metodě pouze jednou, proto
             * vpřípadě, že byl takový název již zadán, pak budu přidávat na konec názvu
             * index, kterým oddělím ty předchozí názvy parametrů.
             *
             * Dále potřebuji ještě testovat, zda se název parametru neshoduje s datový
             * typem parametru, protože například parametr typu int, tak jeho název se
             * vygeneruje také int, ale obojí je chybné, proto potřebuji i v těchto
             * případech vygenerovat indexy.
             *
             * Note:
             * Ty názvy proměnných musím testovat s ignorováním velikostí písmen, protože
             * například Long a long je oboje datový typ.
             */
            if (namesOfParameters.contains(name) || name.equalsIgnoreCase(p.getType().getSimpleName())) {
                int index = 1;

                name += index;

                while (namesOfParameters.contains(name) || name.equalsIgnoreCase(p.getType().getSimpleName())) {
                    name = name.substring(0, name.length() - 1);
                    name += ++index;
                }
            }

            namesOfParameters.add(name);

            final String parameterInText = ParameterToText.getParameterInText(p, false);
            methodBuilder.append(parameterInText).append(" ").append(name).append(", ");
        }

        // odeberu poslední čárku - tj, poslední dva znaky (pokud je třeba):
        if (methodBuilder.toString().endsWith(", "))
            methodBuilder.setLength(methodBuilder.length() - 2);

        // vrátím získaný text a přidám poslední závorku:
        return methodBuilder.toString();
    }

	
	
	
	
	
	
	
	/**
	 * Metoda, která převede první znak z textu text na malé písmeno a celý text
	 * vrátí.
	 * 
	 * @param text
	 *            - text, u kterého se má první znak převést na malé písmeno.
	 * 
	 * @return text v parametru této mtetody (text), ale s prvním písmenem malým.
	 */
	private static String getFirstCharSmall(final String text) {
		return text.substring(0, 1).toLowerCase() + text.substring(1);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se ve třídě již nachází importovaný baliček pro
	 * List Konkrétně balíček: import java.util.List;
	 * 
	 * @param textOfClass
	 *            - Text třídy - zdrojový kód
	 * 
	 * @return true, pokud se v třídě balíček nachází, jinak false
	 */
	private static boolean isImportedPackageForList(final List<String> textOfClass) {
        // jestlize obsahuje package nebo import java.util.list; - ignoracase
        for (final String s : textOfClass)
            if (s.equalsIgnoreCase("import java.util.List;") || s.equalsIgnoreCase("import java.util.*;"))
                return true;

        return false;
    }
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která do třídy přidá balíček pro List z java.util.List;
	 * 
	 * @param textOfClass
	 *            - zdrojový kód třídy
	 * 
	 * @return balíček si přidáným importem balíčku z java.util.Lilst;
	 */
	private static List<String> addPackageForList(final List<String> textOfClass, final int headerIndex) {
		// Pokud bude nalezena hlavička třídy, tak to přidám nad ní - pokud to půjde,
		// jinak
		// naimportuji baliček na začátek třídy - pod package (pokud existuje)

        final String packageForList = "import java.util.List;";

        if (headerIndex - 2 >= 0)
            textOfClass.add(headerIndex - 1, packageForList);

        else textOfClass.add(1, packageForList);

        return textOfClass;
    }
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se ve třídě již List s daným jménem nachází
	 * 
	 * @param textOfClass
	 *            - zdrojový kód třídy
	 * 
	 * @param className
	 *            - název třídy
	 * 
	 * @return true, pokud List již v dané třídě existuje, jinak false
	 */
	private static boolean existListInClass(final List<String> textOfClass, final String className) {
		final String classNameText = Character.toLowerCase(className.charAt(0)) + className.substring(1) + "List";
		
		
		final String regexPrivate = "^\\s*[pP][rR][iI][vV][aA][tT][eE]\\s+(([sS][tT][aA][tT][iI][cC]\\s+)?([fF][iI][nN][aA][lL]\\s+)?|([fF][iI][nN][aA][lL]\\s+)?([sS][tT][aA][tT][iI][cC]\\s+)?)?[lL][iI][sS][tT]\\s*[<]\\s*" + className + "\\s*[>]\\s*" + classNameText + "\\s*;\\s*$";
		
		final String regexPublic = "^\\s*[pP][uU][bB][lL][iI][cC]\\s+(([sS][tT][aA][tT][iI][cC]\\s+)?([fF][iI][nN][aA][lL]\\s+)?|([fF][iI][nN][aA][lL]\\s+)?([sS][tT][aA][tT][iI][cC]\\s+)?)?[lL][iI][sS][tT]\\s*[<]\\s*" + className + "\\s*[>]\\s*" + classNameText + "\\s*;\\s*$";
		

		final String regexProtected = "^\\s*[pP][rR][oO][tT][eE][cC][tT][eE][dD]\\s+(([sS][tT][aA][tT][iI][cC]\\s+)?([fF][iI][nN][aA][lL]\\s+)?|([fF][iI][nN][aA][lL]\\s+)?([sS][tT][aA][tT][iI][cC]\\s+)?)?[lL][iI][sS][tT]\\s*[<]\\s*" + className + "\\s*[>]\\s*" + classNameText + "\\s*;\\s*$";


        for (final String s : textOfClass)
            if (s.matches(regexPrivate) || s.matches(regexPublic) || s.matches(regexProtected))
                return true;

		return false;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda daná třída je výčet
	 * 
	 * @param headerText
	 *            - hlavička třídy - její text
	 * 
	 * @return true, pokud se jedná o enum, jinak false
	 */
	private static boolean isClassEnum(final String headerText) {
		final String[] partsOfHeader = headerText.split("\\s");

		for (final String s : partsOfHeader) {
			if (s.equalsIgnoreCase("enum"))
				return true;
		}
		
		return false;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda je danáhlavička třídy Interface
	 * 
	 * @param headerAtLine
	 *            - texst hlavičky třídy
	 * 
	 * @return true, pokud se jedná o interface, jinak false
	 */
	private static boolean isClassInterface(final String headerAtLine) {
		final String[] partsOfLine = headerAtLine.trim().split("\\s");

		for (final String s : partsOfLine) {
			if (s.equalsIgnoreCase("interface"))
				return true;
		}		
		
		return false;
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá implementaci interface do hlavičky třídy:
	 * 
	 * @param headerText
	 *            - text hlavičky třídy, kam se má přidat implementace
	 * @param className
	 *            - název třídy
	 * 
	 * @param desClassName_Interface
	 *            - název cílové třídy - interface, jehož implementace se má přidat
	 *            do halvičky třídy
	 * 
	 * @return novou hlavičku třídy nebo původní - dojde li k nečekané chybě
	 */
	private static String addInterfaceToClass(final String headerText, final String className, final String desClassName_Interface) {
		// Postup:
		// Vytvořím novou hlavičku, tak že zjistím, zda hlavička obsahuje dedičnost nebo ne
		// pokud ano, tak to nechám a přidám implementaci rozhraní
		// pokud obsahuje implementaci rozhraní, tak pridam desetinnu čárku aza ním rozhraní - název cílové třídy:
		
		
		// Možné typy hlaviček: (abych věděl, zda mam pridat implementets rozhrani nebo jen ,rozhrani, ...)
		// public (abstract) typ nazev 		pridam implements rozhrani
		// public (abstract) typ nazev EXTENDS NECO		pridam implement rozhrani	
		// public (abstract) typ nazev IMPLEMENTS NECO			pridam , rozhrani
		// public (abstract) typ nazev extends neco implements neco		pridam , rozhrani
		
		// U ka6d0ho m;6e a nemus9 b7t y8vorka stejn2 jako u d2di4nosti to 5e3it nebudu,
		// prost2 ji na konec hlavi4kz p5id8m, dle spr8vn0 szntaxe Javz>
		
		final String regexClass = "^\\s*[pP][uU][bB][lL][iI][cC]\\s+\\w+\\s+\\w+\\s+\\w*?\\s*[{]?\\s*$";
		final String regexClassExtends = "^\\s*[pP][uU][bB][lL][iI][cC]\\s+\\w+\\s+\\w+\\s+\\w*?\\s*[eE][xX][tT][eE][nN][dD][sS]\\s+\\w+\\s*[{]?\\s*$";
		final String regexClassImplements = "^\\s*[pP][uU][bB][lL][iI][cC]\\s+\\w+\\s+\\w+\\s+\\w*?\\s*[iI][mM][pP][lL][eE][mM][eE][nN][tT][sS]\\s+[\\w,? *]+\\s*[{]?\\s*$";
		final String regexClassExtendsImpl = "^\\s*[pP][uU][bB][lL][iI][cC]\\s+\\w+\\s+\\w+\\s+\\w*?\\s*[eE][xX][tT][eE][nN][dD][sS]\\s+\\w+\\s+[iI][mM][pP][lL][eE][mM][eE][nN][tT][sS]\\s+[\\w,? *]+\\s*[{]?\\s*$";
		
		
		
		
		
		
		// 1 - klasicka trida: public class nazev, 
		// 2 - public enum nazev, enum to také být v teto fazi kodu nemuze
		// 3 - Zde dědí třčída od Appletu
		// 4 - public interface name  - interface to v této fázi být nemůže
		// 5 - public abstract name ...
		
		// Stačí testovat class a abstract class enum ani rozhrani nemohou implementovat rozhrani:
		
		String newClassHeader = "";
		
		// Zde se jedná o třídu : public a dvě slova, takže je třebat pridat : implements rozhrani:
		if (headerText.matches(regexClass)) {
			// Zde vím, ze se jedná o hlavičku ve tvaru: public typ nazev, k tomu, abych postavil novou hlavicku, potřebuji vědět,
			// o jaký typ třídy - hlavičky se jedný, tak otestuji hlavičku:
			
			final int kindOfHeaderClass = regularExpressionForClassHead(headerText, className);
			
			if (kindOfHeaderClass == 1 || kindOfHeaderClass == 0)
				newClassHeader = "public class " + className + " implements " + desClassName_Interface;
			
			else if (kindOfHeaderClass == 5)
				newClassHeader = "public abstract class " + className + " implements " + desClassName_Interface;
		}
		
		
		
		else if (headerText.matches(regexClassExtends)) {
			// Zde daná třída z něčeho dědí:, tak to musím nechat, vezmu si slova po extends + 1 a pridam implements rozhrani
			// s tím, žepotřebuji otestovat, aby na posledním místě nebyla závorka:
			
			// Note: Pokud bude otevírací závorka za třídou, ze které tato třída dědí bude v hlaviččce třídy chyba se závorkou			
			
			final String[] partsOfHeader = headerText.split(" ");
			
			int extendsIndex = -1;
			
			for (int i = 0; i < partsOfHeader.length; i++) {
				if (partsOfHeader[i].equalsIgnoreCase("extends")) {
					extendsIndex = i;
					break;
				}
			}
			
			
			if (extendsIndex > -1) {
				for (int i = 0; i <= extendsIndex + 1; i++)
					newClassHeader += partsOfHeader[i] + " ";
				
				newClassHeader += "implements " + desClassName_Interface;
			}
		}
		else if (headerText.matches(regexClassImplements))
			// Zde stačí akorát přidat: , MyInterface, ale potřebuji veědět do jaké části:
			newClassHeader = getNewHeaderText(headerText, desClassName_Interface, newClassHeader);
			
		else if (headerText.matches(regexClassExtendsImpl))
			// Zde to samé jako předchozí:
			newClassHeader = getNewHeaderText(headerText, desClassName_Interface, newClassHeader);
		
		
		newClassHeader += " {";
		
		// Jestliže se nenajde hlavička nebo dojde k nějaké chybě - ať už v algoritmu či někde jinde, vrátilo by se jako nová hlavička
		// pouze výše zmíněné otevírací závorka, tak pokud by k tomu nechtěně došlo vrátím původní hlavičku:
		
		if (!newClassHeader.equals(" {"))
			return newClassHeader;
		
		return headerText; 
	}


	
	
	
	
	
	

	/**
	 * Metod,a která vrátí "novou" hlavičku třídy
	 * 
	 * @param headerText
	 *            - text hlavičk ytřídy
	 * 
	 * @param desClassName_Interface
	 *            - název cílové třídy, neboli interfacu, který se má přidat do
	 *            hlavičky třídy
	 * 
	 * @param newClassHeader
	 *            - proměnná, do které se má zapsat hlavička
	 * 
	 * @return nový text hlavičky třídy - newClassHeader
	 */
	private static String getNewHeaderText(final String headerText, final String desClassName_Interface,
			String newClassHeader) {
		// Jestliže najdu index závorky, tak před ní vložím nové rozhraní (parametr metody)
		// Když ho nenajdu, tak lze předpokládat, že je závorka na následujícím řádku, nebo je spojena s posledním rozhraním - slovem, nebo vůbec
		
		final String[] partsOfHeader = headerText.split("\\s");
		
		int indexOfBracket = -1;
		
		for (int i = 0; i < partsOfHeader.length; i++) {
			if (partsOfHeader[i].equalsIgnoreCase("{")) {
				indexOfBracket = i;
				break;
			}					
		}
		
		
		if (indexOfBracket > -1) {
			// Zde jsem našel závorku:
			// Tak si vezmu text před ní a přidám ho do nové hlavičky a na konec přidám: ,
			// nové rozhraní
			for (int i = 0; i < indexOfBracket; i++)
				newClassHeader += partsOfHeader[i] + " ";

			newClassHeader += ", " + desClassName_Interface;
		}
		
		
		
		else {
			// Zde jsem nenašel závorku, tak se kouknu, jestli není za posledním slovem - rozhraním:
			final String lastWord = partsOfHeader[partsOfHeader.length - 1];
			if (lastWord.endsWith("{")) {
				// Zde je závorka spojena s posledním slovem:
				
				
				// Tak si vezmu texti do předposledního slova a přidám ho do nové hlavičky, pak
				// tam přidám rozhraní, které chce nově pčidat, a pak přidám
				// to poslední rozhraní - ale bez závorky (substring)

				// Ve výsledku bude nové přidané rozhraní na předposledním místě:

				for (int i = 0; i < partsOfHeader.length - 2; i++)
					newClassHeader += partsOfHeader[i] + " ";

				newClassHeader += desClassName_Interface + ", ";
				
				final String lastInterface = partsOfHeader[partsOfHeader.length - 1];
				// Poslední interface bez posledního znaku - bez závorky:
				newClassHeader += lastInterface.substring(0, lastInterface.length() - 1);
			}
			
			
			else {
				// Zde je závorka nejspíše na následujícím řádku nebo bůbec:
				// tak stačí přidat akorát: , MyInterface
				for (final String s : partsOfHeader)
					newClassHeader += s + " ";
					
				newClassHeader += ", " + desClassName_Interface;
			}
		}
		
		return newClassHeader;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí text nové hlavičky třídy (navíc: extends
	 * destinationClassName) (pokud se nejedná o enum, ten nedědí, v takovém případě
	 * vrátí null)
	 * 
	 * @param classHeader
	 *            - text hlavičky
	 * 
	 * @param className
	 *            - název třídy
	 * 
	 * @param desClassName
	 *            - název třídy, ze které má ta 'zdrojová' předchozí dědit
	 * 
	 * @return nový text hlavičky s extends nebo null, pokud je to enum, jinak
	 *         chyba, která by neměla nastat
	 */
	private String addExtendsToHeaderClass(final String classHeader, final String className,
			final String desClassName) {
		final String[] partsOfHeader = classHeader.trim().split("\\s");
		
		// Zdrojová třída nesmí být enum, ten nededi
		for (final String s : partsOfHeader) {
			if (s.equalsIgnoreCase("enum")) {
				JOptionPane.showMessageDialog(classDiagram, enumInheritErrorText_2, enumInheritErrorTitle_2,
						JOptionPane.ERROR_MESSAGE);
				return null;
			}
		}
		
		// Sem se dostanu, pokud třída není výčet, pak lze doplnit dědičnost:
		// Následující možnosti mohou nastat v případě hlavičky - abych věděl kam mám přidat: extends trida
//		public ..... implements neco mezery(
//		public ..... implements neco(
//		public .....     mezery (
//		public ......(
//		public ......
		
		
		// 1 - klasicka trida: public class nazev, pridam extends nazevTridy
		// 2 - public enum nazev, pridam extends nazevTridy
		// 3 - Zde dědí třčída od Appletu, to tu ale testovat nemusím protože v této metodě vím, že třída z níčeho nědědí !!!
		// 4 - public interface name zde vic neni - dedit zde nededi a implementovat nemuze
		// 5 - public abstract name ...
		
		String newClassHeader = "";
		if (regularExpressionForClassHead(classHeader, className) == 1 || regularExpressionForClassHead(classHeader, className) == 0)
			newClassHeader = "public class " + className + " extends " + desClassName;
		
		else if (regularExpressionForClassHead(classHeader, className) == 2)
			newClassHeader = "public enum " + className + " extends " + desClassName;
		
		else if (regularExpressionForClassHead(classHeader, className) == 4)
			newClassHeader = "public interface " + className + " extends " + desClassName;
		
		else if (regularExpressionForClassHead(classHeader, className) == 5)
			newClassHeader = "public abstract class " + className + " extends " + desClassName;
		
		
		
		// V této fázi třída dědit nemůže, jelikož jsem "vytvořil novou hlavičku třídy", tak potřebuji zjistit, zda před tím třída implementovala 
		// nějaké rozhraní, pokud ano, tak je potřebuji zpět přidat, pokudne, tak přidám akorát závorku na konec řádku - hlavičky
		// Zde netestuji, zda se závorka nachází na konci řádku shlavičkou, nebo zda je na následující řádku (syntaxe např C#)
		// V Jave patří na konec řádku s hlavičkou, tak to tak udělám:
		
		int indexOfWordImplements = -1;
		
		// Zjistím si index klíčového slova implements v poli:
		for (int i = 0; i < partsOfHeader.length; i++) {
			if (partsOfHeader[i].equalsIgnoreCase("implements")) {
				indexOfWordImplements = i;
				break;
			}
		}
		
		// POkud se v poli klíčové slovo implements nachází (indexOfWordImplements > -1), pak přidám slova od tohoto indexu k hlavičce
		// POkud ne, tak na konec přidám akorát závorku:
		newClassHeader += " ";
		if (indexOfWordImplements > -1) {
			for (int i = indexOfWordImplements; i < partsOfHeader.length; i++) {
				newClassHeader += partsOfHeader[i] + " ";
			}
		}
		else newClassHeader += "{";
		
		// Vrátím novou hlavičku:
		return newClassHeader;
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda daná třída z něčeho už dědí nebo ne
	 * 
	 * @param textOfClass
	 *            - text třídy - zdrojový kód v kolekci
	 * 
	 * @param headerIndex
	 *            - index hlavičky třídy pro zjištění dědičnosti
	 * 
	 * @return true, pokud třída již z něčeho dědí jinak false
	 */
	private static boolean classInheritsfromSomething(final List<String> textOfClass, final int headerIndex) {
		final String patternsExtends = "^ *[pP][uU][bB][lL][iI][cC]\\s+[\\w+\\s*]+\\s+[eE][xX][tT][eE][nN][dD][sS]\\s+[\\w+,\\s*{]+\\s*[{]? *$";

		return textOfClass.get(headerIndex).matches(patternsExtends);
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda nebude fungovat, pokud budou 2 třídy se stejným názvem ale z jiných
	 * balíčků Balíčky se naimportují správně, ale třídy - private class name už ne,
	 * alespoň ne pokud budou stejné názvy tříd uživatel je už bude muset dopsat
	 * ručně
	 *
	 * Metoda slouží k tomu, aby otestovala, zda se v třídě již jednou daný atribut
	 * nenachází, původně jsem tuto metodu napsal proto, že když se mezi dvěma
	 * třídami vytvoří např asociace, obě třídy na sebe budou mít reference, kdyz se
	 * smazou nemusí se smazat příslušný atribut, a když se vazba znovu vytvoří byl
	 * by v dané třídě stejný atribut 2x
	 * 
	 * Metoda otestuje, zda se v dané třídě již nachází daný atribut nebo ne
	 * 
	 * @param textOfClass
	 *            - kód třídy
	 * 
	 * @param className
	 *            - název třídy
	 * 
	 * @return true, pokud se atribut v třídě již nachází, jinak false
	 */
	private static boolean isAttributeInClass(final List<String> textOfClass, final String className,
			final String classNameText) {
		
		// Note:
		// Jak jsem již zmínil, tato metoda otestuje, zda se v daném kódu již nachází daný atribut
		// v syntaxi níže, jenže už nemohu zjistit, zda k němu uživatel dopsal vytvoření instance třídy
		// nebo definici kolekce či její naplnění, apod.
		// Proto se uchýlím pouze k "základní" kontrole atributu, a sice, pouze v syntaxi: viditelnost (static final) název třídy a název proměnné;
		// Více již neznám, tak nemohu určit u čeho je deklarace, u čeho není, apod.
		// Dále také neznám konkrétní název proměnné - ten mohl uživatel také změnit a regulární výrazy selžou
		
		
		// Atribut se může nacházet v syntaxi:
		// (private | public | protected) static final Classname variable;
		

		final String regexPrivate = "^\\s*(private\\s+|public\\s+|protected\\s+)?((static\\s+)?(final\\s+)|final\\s+)?(static\\s+)?" + className + "\\s+" + classNameText + "\\s*;\\s*$";
						
				
		
		for (final String s : textOfClass) {
			if (s.matches(regexPrivate))
				return true;
		}

		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která do danoho textu - kódu třídy naimportuje baliček třídy, na
	 * kterou má mít referenci
	 * 
	 * @param cellValue
	 *            - celý název třídy - i s balíčky, které se zjísti
	 * @param textOfClass
	 *            - text - kód třídy, do které se mají naimportovat balíčky
	 * 
	 * @param headerIndex
	 *            - index řádku hlavičky třídy nad který se mají vložit balíček
	 * 
	 * @return true, pokud se balíček naimportuje, jinak false - nema se jaký
	 *         balíček importovat - uz tam je
	 */
	private static boolean importPackages(final String cellValue, List<String> textOfClass, final int headerIndex) {
		if (cellValue.contains(".")) {
			// Zjistím, zda třída již daný balíček nebsahuje:
			if (!containsClassPackage(textOfClass, (cellValue + ";"))) {
				// Zde třída balíček neobsahuje
				// Naimportuji balíčky do druhé třídy nad její hlavičku:
				if (headerIndex - 1 >= 0) {
					textOfClass.add(headerIndex - 1, "import " + (cellValue + ";"));
					return true;
				}
			}
			// zde třída balíček již obsahuje, nemusím ho znovu přidávat
			return false;
		}
		
		return false;
	}
	
	
	

	
	
	
	
	/**
	 * Metoda, která zjistí, zda daná třída již obsahuje balíček nebo ne - abych
	 * neimportoval balíček do třídy, když už tam jednou je
	 * 
	 * @param textOfClass
	 *            - text třídy - zdrojový kód v kolekci
	 * 
	 * @param packageText
	 *            - tesxt balíčku, který se má importovat
	 * 
	 * @return true pokud balíček třída již obsahuje, jinak false
	 */
	private static boolean containsClassPackage(final List<String> textOfClass, final String packageText) {
        for (final String s : textOfClass) {
            if (s.equalsIgnoreCase("import " + packageText) || s.equalsIgnoreCase("package " + packageText))
                return true;
        }
		
		return false;
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí index řádku hlavíčky třídy
	 * 
	 * @param textOfClass1
	 *            - kó - text třídy ve které chci zjistit index řádku hlavičky
	 * 
	 * @param className
	 *            - název třídy - je v hlavičce dané třídy
	 * 
	 * @return -1, pokud nebude hlavička nalezena, jinak vrát index řádku hleddané
	 *         hlavičky třídy
	 */
	private static int getIndexHeaderOfClass(final List<String> textOfClass1, final String className) {
		for (int i = 0; i < textOfClass1.size(); i++)
			if (regularExpressionForClassHead(textOfClass1.get(i), className) > -1)
				return i;

		return -1;
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se na aktuálně testovaném řádku nachází hlavička
	 * atřídy.
	 * 
	 * Note:
	 * Skrze výrazy projde napr i hlavicka: public static final abstract class
	 * ClassName exte Class Chtěl jsem je následující výrazy napsat co nejvíce
	 * flexibilní, aby kdyz napr uzivatel v editoru tridu upravi s chybou nekde v
	 * hlaviccte napr. u slova extends bude chybet slovo s uz by to hlavicku nenaslo
	 * podobne je o i s velkymy a malymi pismey meli by byýt Témer vsechna mala a
	 * pro pripad, ze by jedno s pismen bylo velke sice pro kompilator je to chyba
	 * ale nemusi tomu tak byt i pro uzivatele diky tomuto malemu / velkemu pismenu
	 * bych jiz nerozeznal hlavicku, apod. a v takovém případě by nešlo vytvořít
	 * příslušný vztah mezi třídami v diagramu tříd.
	 * 
	 * @param textAtLine
	 *            - řádek z třídy
	 * 
	 * @param className
	 *            - název třídy
	 * 
	 * @return číslo > 0, pokud se v proměnné textAtLine nachází hlavička nějaké z
	 *         tříd, jinak -1
	 * 
	 */
	public static int regularExpressionForClassHead(final String textAtLine, final String className) {

		final String regularExpressClassNormal = "^\\s*[pP][uU][bB][lL][iI][cC]\\s+(([sS][tT][aA][tT][iI][cC]\\s+)?([fF][iI][nN][aA][lL]\\s+)?|([fF][iI][nN][aA][lL]\\s+)?([sS][tT][aA][tT][iI][cC]\\s+)?)?([aA][bB][sS][tT][rR][aA][cC][tT]\\s+)?[cC][lL][aA][sS][sS]\\s+" + className + "\\s*\\{?\\s*";
		
		
		final String regularExpressClass = "^\\s*[pP][uU][bB][lL][iI][cC]\\s+(([sS][tT][aA][tT][iI][cC]\\s+)?([fF][iI][nN][aA][lL]\\s+)?|([fF][iI][nN][aA][lL]\\s+)?([sS][tT][aA][tT][iI][cC]\\s+)?)?([aA][bB][sS][tT][rR][aA][cC][tT]\\s+)?[cC][lL][aA][sS][sS]\\s+" + className + "\\s*[[eE][xX][tT][eE][nN][dD][sS]]?\\s*[[iI][mM][pP][lL][eE][mM][eE][nN][tT][sS]]?\\s*[\\w,\\s\\{\\?]*\\s*[{]?\\s*$";
		
		
		
		final String regularExpressEnumClass = "^\\s*[pP][uU][bB][lL][iI][cC]\\s+(([sS][tT][aA][tT][iI][cC]\\s+)?([fF][iI][nN][aA][lL]\\s+)?|([fF][iI][nN][aA][lL]\\s+)?([sS][tT][aA][tT][iI][cC]\\s+)?)?([aA][bB][sS][tT][rR][aA][cC][tT]\\s+)?[eE][nN][uU][mM]\\s" + className + "\\s*[[eE][xX][tT][eE][nN][dD][sS]]?\\s*[[iI][mM][pP][lL][eE][mM][eE][nN][tT][sS]]?\\s*[\\w,\\s\\{\\?]*\\s*[{]?\\s*$";
		
		final String regularExpressAppletClass = "^\\s*[pP][uU][bB][lL][iI][cC]\\s+(([sS][tT][aA][tT][iI][cC]\\s+)?([fF][iI][nN][aA][lL]\\s+)?|([fF][iI][nN][aA][lL]\\s+)?([sS][tT][aA][tT][iI][cC]\\s+)?)?([aA][bB][sS][tT][rR][aA][cC][tT]\\s+)?[cC][lL][aA][sS][sS]\\s+" + className + "\\s+[eE][xX][tT][eE][nN][dD][sS]\\s+[aA][pP][pP][lL][eE][tT]\\s*(\\s+[iI][mM][pP][lL][eE][mM][eE][nN][tT][sS]\\s+)?\\s*[\\w,\\s\\{\\?]*\\s*[{]?\\s*$";
		
		
		final String regularExpressInterfaceClass = "^\\s*[pP][uU][bB][lL][iI][cC]\\s+(([sS][tT][aA][tT][iI][cC]\\s+)?([fF][iI][nN][aA][lL]\\s+)?|([fF][iI][nN][aA][lL]\\s+)?([sS][tT][aA][tT][iI][cC]\\s+)?)?([aA][bB][sS][tT][rR][aA][cC][tT]\\s+)?[iI][nN][tT][eE][rR][fF][aA][cC][eE]\\s+" + className + "\\s*([[eE][xX][tT][eE][nN][dD][sS]]?\\s*[\\w\\s\\{\\?]+)?\\s*[{]?\\s*$";

		
		final String regularExpressAbstractClass = "^\\s*[pP][uU][bB][lL][iI][cC]\\s+(([sS][tT][aA][tT][iI][cC]\\s+)?([fF][iI][nN][aA][lL]\\s+)?|([fF][iI][nN][aA][lL]\\s+)?([sS][tT][aA][tT][iI][cC]\\s+)?)?[aA][bB][sS][tT][rR][aA][cC][tT]\\s+[cC][lL][aA][sS][sS]\\s+" + className + "\\s*[[eE][xX][tT][eE][nN][dD][sS]]?\\s*[[iI][mM][pP][lL][eE][mM][eE][nN][tT][sS]]?\\s*[\\w,\\s\\{\\?]*\\s*[{]?\\s*$";
		
		
		
		
		if (textAtLine.matches(regularExpressClassNormal))
			return 0;
		
		else if (textAtLine.matches(regularExpressClass))
			return 1;
		
		else if (textAtLine.matches(regularExpressEnumClass))
			return 2;
		
		else if (textAtLine.matches(regularExpressAppletClass))
			return 3;
		
		else if (textAtLine.matches(regularExpressInterfaceClass))
			return 4;
		
		else if (textAtLine.matches(regularExpressAbstractClass))
			return 5;
		
		
		return -1;
	}
	
	
	
	
	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			missingClassText = properties.getProperty("TacMissingClassText", Constants.TAC_MISSING_CLASS_TEXT);
			missingClassTitle = properties.getProperty("TacMissingClassTitle", Constants.TAC_MISSING_CLASS_TITLE);
			inheritErrorText = properties.getProperty("TacInheritErrorText", Constants.TAC_INHERIT_ERROR_TEXT);
			inheritErrorTitle = properties.getProperty("TacInheritErrorTitle", Constants.TAC_INHERIT_ERROR_TITLE);
			enumInheritErrorText = properties.getProperty("TacEnumInheritErrorText", Constants.TAC_ENUM_INHERIT_ERROR_TEXT);
			enumInheritErrorTitle = properties.getProperty("TacEnumInheritErrorTitle", Constants.TAC_ENUM_INHERIT_ERROR_TITLE);
			interfaceErrorText = properties.getProperty("TacInterfaceErrorText", Constants.TAC_INTERFACE_ERROR_TEXT);
			interfaceErrorTitle = properties.getProperty("TacInterfaceErrorTitle", Constants.TAC_INTERFACE_ERROR_TITLE);
			implementsErrorText_1 = properties.getProperty("TacImplementsErrorText_1", Constants.TAC_IMPLEMENTS_ERROR_TEXT_1) + ": ";
			implementsErrorText_2 = properties.getProperty("TacImplementsErrorText_2", Constants.TAC_IMPLEMENTS_ERROR_TEXT_2);
			implementsErrorTitle = properties.getProperty("TacImplementsErrorTitle", Constants.TAC_IMPLEMENTS_ERROR_TITLE);
			selectedClassErrorText = properties.getProperty("TacSelectedClassErrorText", Constants.TAC_SELECTED_CLASS_ERROR_TEXT) + ": ";
			selectedClassErrorTitle = properties.getProperty("TacSelectedClassErrorTitle", Constants.TAC_SELECTED_CLASS_ERROR_TITLE);
			missingSrcDirectoryText = properties.getProperty("TacMissingSrcDirectoryText", Constants.TAC_MISSING_SRC_DIRECTORY_TEXT);
			missingSrcDirectoryTitle = properties.getProperty("TacMissingSrcDirectoryTitle", Constants.TAC_MISSING_SRC_DIRECTORY_TITLE);
			enumInheritErrorText_2 = properties.getProperty("TacEnumInheritErrorText_2", Constants.TAC_ENUM_INHERIT_ERROR_TEXT_2);
			enumInheritErrorTitle_2 = properties.getProperty("TacEnumInheritErrorTitle_2", Constants.TAC_ENUM_INHERIT_ERROR_TITLE_2);
			
			wrongSelectedObjectText = properties.getProperty("Tac_WrongSelectedObjectText", Constants.TAC_WRONG_ELECTED_OBJECT_TEXT);
			wrongSelectedObjectTitle = properties.getProperty("Tac_WrongSelectedObjectTitle", Constants.TAC_WRONG_SELECTED_OBJECT_TITLE);
			choosedCommentNotClassText = properties.getProperty("Tac_ChoosedCommentNotClassText", Constants.TAC_CHOOSED_COMMENT_NOT_CLASS_TEXT);
			choosedCommentNotClassTitle = properties.getProperty("Tac_ChoosedCommentNotClassTitle", Constants.TAC_CHOOSED_COMMENT_NOT_CLASS_TITLE);
		}
		
		else {
			missingClassText = Constants.TAC_MISSING_CLASS_TEXT;
			missingClassTitle = Constants.TAC_MISSING_CLASS_TITLE;
			inheritErrorText = Constants.TAC_ENUM_INHERIT_ERROR_TEXT;
			inheritErrorTitle = Constants.TAC_ENUM_INHERIT_ERROR_TITLE;
			enumInheritErrorText = Constants.TAC_ENUM_INHERIT_ERROR_TEXT;
			enumInheritErrorTitle = Constants.TAC_ENUM_INHERIT_ERROR_TITLE;
			interfaceErrorText = Constants.TAC_INTERFACE_ERROR_TEXT;
			interfaceErrorTitle = Constants.TAC_INTERFACE_ERROR_TITLE;
			implementsErrorText_1 = Constants.TAC_IMPLEMENTS_ERROR_TEXT_1 + ": ";
			implementsErrorText_2 = Constants.TAC_IMPLEMENTS_ERROR_TEXT_2;
			implementsErrorTitle = Constants.TAC_IMPLEMENTS_ERROR_TITLE;
			selectedClassErrorText = Constants.TAC_SELECTED_CLASS_ERROR_TEXT + ": ";
			selectedClassErrorTitle = Constants.TAC_SELECTED_CLASS_ERROR_TITLE;
			missingSrcDirectoryText = Constants.TAC_MISSING_SRC_DIRECTORY_TEXT;
			missingSrcDirectoryTitle = Constants.TAC_MISSING_SRC_DIRECTORY_TITLE;
			enumInheritErrorText_2 = Constants.TAC_ENUM_INHERIT_ERROR_TEXT_2;
			enumInheritErrorTitle_2 = Constants.TAC_ENUM_INHERIT_ERROR_TITLE_2;
			
			wrongSelectedObjectText = Constants.TAC_WRONG_ELECTED_OBJECT_TEXT;
			wrongSelectedObjectTitle = Constants.TAC_WRONG_SELECTED_OBJECT_TITLE;
			choosedCommentNotClassText = Constants.TAC_CHOOSED_COMMENT_NOT_CLASS_TEXT;
			choosedCommentNotClassTitle = Constants.TAC_CHOOSED_COMMENT_NOT_CLASS_TITLE;
		}
	}
}