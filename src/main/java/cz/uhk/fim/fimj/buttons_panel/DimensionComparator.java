package cz.uhk.fim.fimj.buttons_panel;

import java.awt.Dimension;
import java.util.Comparator;
import java.util.List;

import javax.swing.JToggleButton;

/**
 * Tato třída slouží pro porovnání dimenzi - velikost tlačítek v Toolbaru (panelu s tlačítky) - kvuli nastavení jejich
 * velikosti
 * <p>
 * Zdroj: http://stackoverflow.com/questions/3692987/why-will-boxlayout-not-allow-me-to-change-the-width-of-a-jbutton
 * -but-let-me-chan
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class DimensionComparator implements Comparator<Dimension> {

    /**
     * Metoda, která vynásobí šířku a výšku objektu Dimension a vrátí výsledek tohoto násobení, aby šlo porovnat tyto
     * objekty - jejich velikosti mezi sebou
     *
     * @param d
     *         - proměnná typu Dimension, u které se vynásobí její šířka a výška a vrátí se výsledek
     * @return výška krát šířka proměnné d
     */
    private static double getDimensionArea(final Dimension d) {
        return d.getWidth() * d.getHeight();
    }


    /**
     * Metoda compare vrátí hodnoty:
     * 1 -> o1 > o2
     * 0 -> o1 == o2
     * -1 -> o1 < o2
     */


    @Override
    public int compare(Dimension o1, Dimension o2) {
        return Double.compare(getDimensionArea(o1), getDimensionArea(o2));
    }


    /**
     * Metoda, která projde celou kolekci s tlačítky a zjistí si velikost - dimenzi toho největšího a tuto vrátí.
     *
     * @param buttonsList
     *         - kolekce - list s tlačítky
     * @param firstDimension
     *         - dimenze tlačítka pro přidání třídy - to je typ tlačítka Jbutton a ne JtogleButton, proto není v
     *         kolekci, ale je třeba tuto velikost také započítat
     * @return velikost, reps. objekt typu Dimension největšího tlačítka
     */
    final Dimension getMaxDimension(final List<JToggleButton> buttonsList, final Dimension firstDimension) {
        // Do této proměnné si uložím tu největší velikost tlačítka, resp. velikost
        // nebo rozmery (Diemnsion) tlačítka, které je největší - s nejnejdelším textem
        // a pak tento rozměr nastavím všem tlačítkům, tak aby měly všechna tlačítka stejné
        // rozměry

        Dimension maxD = firstDimension;

        for (final JToggleButton b : buttonsList) {
            // Otestuji, zda je to testované tlačítke větší než to zvolené,
            // pokud ano, tak si uložím tuto novou velikost
            if (compare(maxD, b.getPreferredSize()) < 0)
                maxD = b.getPreferredSize();
        }

        return maxD;
    }
}