package cz.uhk.fim.fimj.buttons_panel;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.class_diagram.EdgeTypeEnum;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.forms.NewClassForm;

import javax.swing.*;
import java.util.Properties;

/**
 * ("Výpomocná") Třída, která obsahuje metody pro nastavení vztahů a označení tlačítek v toolbaru.
 *
 * <i>Třída hraje roli ve smyslu "rozhraní", ale tak, že obsahuje společné metody, které se využívají pro tlačítka v
 * toolbaru a v kontextovém menu na pozadí diagramu tříd. Více viz využití jednotlivých metod.</i>
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 29.08.2018 15:05
 */

final class RelationshipHelper {

    private RelationshipHelper() {
    }


    /**
     * Metoda, která slouží pro vytvoření nové třídy.
     * <p>
     * Nová třída se přidá jak do diagramu tříd, tak i do příslušného adresáře v otevřeném projektu.
     *
     * @param languageProperties
     *         - objekt Properties, která obsahuje texty pro tuto aplikace v uživatelem zvoleném jazyce.
     * @param classDiagram
     *         - reference na diagram tříd, aby bylo možné zavolat metodu pro přidání třídy do diagramu tříd.
     */
    static void addClass(final Properties languageProperties, final GraphClass classDiagram) {
        final NewClassForm ncf = new NewClassForm();
        ncf.setLanguage(languageProperties);

        // Získám si informace o nové třídě:
        final NewClassInfo classInfo = ncf.getNewClassInfo();

        if (classInfo != null && App.WRITE_TO_FILE.writeClassToProjectSrc(classInfo.getClassName(),
                classInfo.getClassType(), classInfo.isGenerateMainMethod())) {
            classDiagram.addClassCell(classInfo.getClassName());

            /*
             * Výše se vytvořila nová třída v diagramu tříd, tak je třeba aktualizovat okno s příkazy v editoru
             * příkazů, aby se v něm nabízela i nově vytvořená třída.
             */
            App.getCommandEditor().launchThreadForUpdateValueCompletion();
        }

        // else - došlo k chybě - nemělo by nastat, proto zde nic nedělám.
    }


    /**
     * Kontrola vytvoření vztahů.
     *
     * <i>Princip je takový, že se zjistí, zda je tlačítko toggleButton označené, pokud ano, pak se nastaví vztah
     * edgeTypeEnum pro vytvoření. Pokud tlačítko není označené, pak se zrušeí vytváření veškerých vztahů a veškerá
     * tlačítka budou odznačena.</i>
     *
     * @param edgeTypeEnum
     *         - výčtová hodnota, která se má nastavit pro vytvoření vztahů (/ vztah, který se má vytvořit mezi třídami
     *         v diagramu tříd).
     * @param toggleButton
     *         - tlačítko, které se má otestovat, zda je označené a dle něj se nastaví / nenastaví vztah pro vytvoření.
     * @param buttonsPanel
     *         - reference na instanci třídy, která obsahuje tlačítka pro označení. Reference je potřeba kvůli zavolání
     *         metody pro nastavení označení příslušných tlačítek.
     */
    static void checkRelationship(final EdgeTypeEnum edgeTypeEnum, final JToggleButton toggleButton,
                                  final ButtonsPanel buttonsPanel) {
        if (toggleButton.isSelected())
            setRelationship(edgeTypeEnum, toggleButton, buttonsPanel);

        else
            setRelationship(buttonsPanel);
    }


    /**
     * Konstrola vytvoření vztahů.
     *
     * <i>Metoda se volá z kontextového menu, proto je nejprve třeba znegovat označení tlačítka toggleButton. Jinak by
     * se v podstatě nic nestalo, protože by se nezměnila hodnota označení tlačítka, takže by se pokračovalo ve
     * vytváření vztahů nebo by se do kola nastavovalo zrušení vztahů.</i>
     *
     * @param edgeTypeEnum
     *         - výčtová hodnota, která se má nastavit pro vytvoření vztahů (/ vztah, který se má vytvořit mezi třídami
     *         v diagramu tříd).
     * @param toggleButton
     *         - tlačítko, které se má otestovat, zda je označené a dle něj se nastaví / nenastaví vztah pro vytvoření.
     * @param buttonsPanel
     *         - reference na instanci třídy, která obsahuje tlačítka pro označení. Reference je potřeba kvůli zavolání
     *         metody pro nastavení označení příslušných tlačítek.
     */
    static void checkRelationshipWithNegationButton(final EdgeTypeEnum edgeTypeEnum, final JToggleButton toggleButton
            , final ButtonsPanel buttonsPanel) {
        toggleButton.setSelected(!toggleButton.isSelected());

        checkRelationship(edgeTypeEnum, toggleButton, buttonsPanel);
    }


    /**
     * Nastavení vytvoření, popř. zrušení vztahu edgeTypeEnum a označení, popř. zrušení označení tlačítka toggleButton.
     *
     * @param edgeTypeEnum
     *         - pokud je tento parametr null, pak se zruší vytváření vztahů. Tj. nebude nastaven žádný vztah pro
     *         vytvoření. V případě, že je v tomto parametru konkrétní výčtová hodnota, jedná se o nastavení vytvoření
     *         konkrétního vztahu.
     * @param toggleButton
     *         - tlačítko, která se má nastavit jako označené. Pokud je tento parametr null, nebude označené žádné
     *         tlačítko.
     * @param buttonsPanel
     *         - reference na třídu obsahující výše zmíněná tlačítka, aby se v ní mohla zavolat metoda pro označení
     *         tlačítek.
     */
    private static void setRelationship(final EdgeTypeEnum edgeTypeEnum, final JToggleButton toggleButton,
                                        final ButtonsPanel buttonsPanel) {
        /*
         * Tato metoda (setRelationship) se zavolá po kliknutí na nějaké tlačítko v toolbaru. Ale nejprve se zde
         * nastaví veškeré proměnné (týkající se vytvoření vztahu mezi třídami) na null hodnnotu. Protože uživatel
         * mohl například změnit nějaký vztah. Tedy označil například vytvoření dědičnosti, pak si to ale rozmyslel a
         * chce vytvořil vztah typu dědičnost, tak se vymažou veškeré označené třídy, aby se nebrala již první
         * označená třída.
         */
        GraphClass.clearVariablesForChooseEdge();

        /*
         * V případě, že je proměnná pro definici vztahu pro vytvoření null. Pak se nemá vytvořil žádný vztah. V
         * takovém případě se zavolá metoda, která nastaví veškeré proměnné týkající se vytvoření vztahu v diagramu
         * na null. Protože mohl uživatel označit třeba i jednu třídu v diagramu, která by se aplikovala pro další
         * označený vztah. Což je špatně.
         */

        /*
         * Podmínka není nezbytná, v případě null hodnoty by se pouze nastavila příslušná proměnná na null hodnotu,
         * ale v té proměnné již je null hodnota (nastaveno v předchozím kroku).
         */
        if (edgeTypeEnum != null)
            GraphClass.setEdgeType(edgeTypeEnum);

        buttonsPanel.setSelectedMenuButtons(toggleButton);
    }


    /**
     * Zrušení vytváření vztahů a označený tlačítek.
     *
     * <i>Metoda zavolá metodu cz.uhk.fim.fimj.buttons_panel.ButtonsPanel#setRelationship(cz.uhk.fim.fimj
     * .class_diagram.EdgeTypeEnum, javax.swing.JToggleButton), s oběma parametry null. Tím se zruší vytváření vztahů a
     * nebude označené žádné tlačítko v toolbaru.</i>
     *
     * @param buttonsPanel
     *         - reference na třídu obsahující tlačítka, aby se zavolala metoda pro zrušení označení vztahů.
     */
    private static void setRelationship(final ButtonsPanel buttonsPanel) {
        setRelationship(null, null, buttonsPanel);
    }
}
