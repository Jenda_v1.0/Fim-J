package cz.uhk.fim.fimj.buttons_panel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.border.BevelBorder;

import cz.uhk.fim.fimj.class_diagram.EdgeTypeEnum;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;

/**
 * Tato třída slouží jako kontextové menu, které se otevře po kliknutí pravým tlačítkem myši na pozadí diagramu tříd,
 * resp. tak aby nebyl žádný objekt v diagramu tříd označen.
 * <p>
 * Toto menu slouží k tomu, aby se označl jeden se vztahů mezi třídami v diagramu tříd (nebo komentář), který se bude
 * přidávat do diagramu tříd mezi označené třídy.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class PopupMenuRelationships extends JPopupMenu implements ActionListener, LanguageInterface {

    private static final long serialVersionUID = 8129725262207120876L;


    /**
     * Třída, která obsahuje tlačítka pro označení vztahů, které se mají přidat mezi označené třídy v diagramu tříd.
     * <p>
     * Reference na tuto třídu je zde potřeba, aby se označela příslušná tlačítka, aby uživatel viděl, jaký vztah se
     * bude přidávat.
     */
    private final ButtonsPanel buttonsPanel;


    // Tlačítka do tohoto popupmenu, které slouží pro označení vztahu pro přidání
    // mezi třídy (nebo komentář):
    private final JMenuItem itemAddClass;
    private final JMenuItem itemAddAssociation;
    private final JMenuItem itemAddExtends;
    private final JMenuItem itemAddImplements;
    private final JMenuItem itemAddAggregation1Ku1;
    private final JMenuItem itemAddAggregation1KuN;
    private final JMenuItem itemAddComment;


    /**
     * Objekt, který bude obsahovat textu pro tuto aplikaci v uživatelem zvoleném jazyce, tento objekt je zde pouze pro
     * to, že jej potřebuji předat do dialogupro vytvoření nové třídy.
     */
    private Properties languageProperties;


    /**
     * Reference na diagram tříd, tato reference je zde potřeba pouze pro to, že jej potřebuji přdat do dialogu pro
     * vytvoření nové třídy, aby se pak přidala nová třída, resp. její reprezentace i do diagramu tříd.
     */
    private final GraphClass classDiagram;


    /**
     * Konstruktor třídy.
     *
     * @param buttonsPanel
     *         - reference na třídu, která slouží jako toolbar s tlačítky, která slouží pro označení vztahu, který se má
     *         přidat mezi třídy v diagramu tříd.
     * @param classDiagram
     *         - reference na diagram tříd, aby bylo možné zavolat metodu pro vytvoření nové třídy (kvůli vytvoření
     *         reprezentace příslušné - nové třídy v diagramu tříd).
     */
    public PopupMenuRelationships(final ButtonsPanel buttonsPanel, final GraphClass classDiagram) {
        this.buttonsPanel = buttonsPanel;
        this.classDiagram = classDiagram;

        setBorder(new BevelBorder(BevelBorder.RAISED));


        itemAddClass = new JMenuItem();
        itemAddAssociation = new JMenuItem();
        itemAddExtends = new JMenuItem();
        itemAddImplements = new JMenuItem();
        itemAddAggregation1Ku1 = new JMenuItem();
        itemAddAggregation1KuN = new JMenuItem();
        itemAddComment = new JMenuItem();

        itemAddClass.addActionListener(this);
        itemAddAssociation.addActionListener(this);
        itemAddExtends.addActionListener(this);
        itemAddImplements.addActionListener(this);
        itemAddAggregation1Ku1.addActionListener(this);
        itemAddAggregation1KuN.addActionListener(this);
        itemAddComment.addActionListener(this);


        add(itemAddClass);
        addSeparator();
        add(itemAddAssociation);
        addSeparator();
        add(itemAddAggregation1Ku1);
        addSeparator();
        add(itemAddExtends);
        addSeparator();
        add(itemAddImplements);
        addSeparator();
        add(itemAddAggregation1KuN);
        addSeparator();
        add(itemAddComment);
    }


    @Override
    public void actionPerformed(final ActionEvent e) {
        if (e.getSource() instanceof JMenuItem) {// Podmínka není nezbytná
            if (e.getSource() == itemAddClass)
                RelationshipHelper.addClass(languageProperties, classDiagram);

            else if (e.getSource() == itemAddAssociation)
                RelationshipHelper.checkRelationshipWithNegationButton(EdgeTypeEnum.ASSOCIATION,
                        buttonsPanel.getTbtnAddAssociation(), buttonsPanel);

            else if (e.getSource() == itemAddExtends)
                RelationshipHelper.checkRelationshipWithNegationButton(EdgeTypeEnum.EXTENDS,
                        buttonsPanel.getTbtnAddExtends(), buttonsPanel);

            else if (e.getSource() == itemAddImplements)
                RelationshipHelper.checkRelationshipWithNegationButton(EdgeTypeEnum.IMPLEMENTATION,
                        buttonsPanel.getTbtnAddImplements(), buttonsPanel);

            else if (e.getSource() == itemAddAggregation1Ku1)
                RelationshipHelper.checkRelationshipWithNegationButton(EdgeTypeEnum.AGGREGATION_1_1,
                        buttonsPanel.getTbtnAggregation_1_ku_1(), buttonsPanel);

            else if (e.getSource() == itemAddAggregation1KuN)
                RelationshipHelper.checkRelationshipWithNegationButton(EdgeTypeEnum.AGGREGATION_1_N,
                        buttonsPanel.getTbtnAggregation_1_ku_N(), buttonsPanel);

            else if (e.getSource() == itemAddComment)
                RelationshipHelper.checkRelationshipWithNegationButton(EdgeTypeEnum.COMMENT,
                        buttonsPanel.getTbtnComment(), buttonsPanel);
        }
    }


    @Override
    public void setLanguage(final Properties properties) {
        languageProperties = properties;

        if (properties != null) {
            itemAddClass.setText(properties.getProperty("PPR_Item_AddClass_Text", Constants.PMR_ITEM_ADD_CLASS_TEXT));
            itemAddAssociation.setText(properties.getProperty("PPR_Item_AddAssociation_Text", Constants
                    .PMR_ITEM_ADD_ASSOCIATION_TEXT));
            itemAddExtends.setText(properties.getProperty("PPR_Item_AddExtends_Text", Constants
                    .PMR_ITEM_ADD_EXTENDS_TEXT));
            itemAddImplements.setText(properties.getProperty("PPR_Item_AddImplements_Text", Constants
                    .PMR_ITEM_ADD_IMPLEMENTS_TEXT));
            itemAddAggregation1Ku1.setText(properties.getProperty("PPR_Item_AddAggregation_1_ku_1_Text", Constants
                    .PMR_ITEM_ADD_AGGREGATION_1_KU_1_TEXT));
            itemAddAggregation1KuN.setText(properties.getProperty("PPR_Item_AddAggregation_1_ku_N_Text", Constants
                    .PMR_ITEM_ADD_AGGREGATION_1_KU_N_TEXT));
            itemAddComment.setText(properties.getProperty("PPR_Item_AddComment_Text", Constants
                    .PMR_ITEM_ADD_COMMENT_TEXT));

            itemAddClass.setToolTipText(properties.getProperty("PPR_Item_AddClass_TT", Constants
                    .PMR_ITEM_ADD_CLASS_TT));
            itemAddAssociation.setToolTipText(properties.getProperty("PPR_Item_AddAssociation_TT", Constants
                    .PMR_ITEM_ADD_ASSOCIATION_TT));
            itemAddExtends.setToolTipText(properties.getProperty("PPR_Item_AddExtends_TT", Constants
                    .PMR_ITEM_ADD_EXTENDS_TT));
            itemAddImplements.setToolTipText(properties.getProperty("PPR_Item_AddImplements_TT", Constants
                    .PMR_ITEM_ADD_IMPLEMENTS_TT));
            itemAddAggregation1Ku1.setToolTipText(properties.getProperty("PPR_Item_AddAggregation_1_ku_1_TT",
                    Constants.PMR_ITEM_ADD_AGGREGATION_1_KU_1_TT));
            itemAddAggregation1KuN.setToolTipText(properties.getProperty("PPR_Item_AddAggregation_1_ku_N_TT",
                    Constants.PMR_ITEM_ADD_AGGREGATION_1_KU_N_TT));
            itemAddComment.setToolTipText(properties.getProperty("PPR_Item_AddComment_TT", Constants
                    .PMR_ITEM_ADD_COMMENT_TT));
        }

        else {
            itemAddClass.setText(Constants.PMR_ITEM_ADD_CLASS_TEXT);
            itemAddAssociation.setText(Constants.PMR_ITEM_ADD_ASSOCIATION_TEXT);
            itemAddExtends.setText(Constants.PMR_ITEM_ADD_EXTENDS_TEXT);
            itemAddImplements.setText(Constants.PMR_ITEM_ADD_IMPLEMENTS_TEXT);
            itemAddAggregation1Ku1.setText(Constants.PMR_ITEM_ADD_AGGREGATION_1_KU_1_TEXT);
            itemAddAggregation1KuN.setText(Constants.PMR_ITEM_ADD_AGGREGATION_1_KU_N_TEXT);
            itemAddComment.setText(Constants.PMR_ITEM_ADD_COMMENT_TEXT);

            itemAddClass.setToolTipText(Constants.PMR_ITEM_ADD_CLASS_TT);
            itemAddAssociation.setToolTipText(Constants.PMR_ITEM_ADD_ASSOCIATION_TT);
            itemAddExtends.setToolTipText(Constants.PMR_ITEM_ADD_EXTENDS_TT);
            itemAddImplements.setToolTipText(Constants.PMR_ITEM_ADD_IMPLEMENTS_TT);
            itemAddAggregation1Ku1.setToolTipText(Constants.PMR_ITEM_ADD_AGGREGATION_1_KU_1_TT);
            itemAddAggregation1KuN.setToolTipText(Constants.PMR_ITEM_ADD_AGGREGATION_1_KU_N_TT);
            itemAddComment.setToolTipText(Constants.PMR_ITEM_ADD_COMMENT_TT);
        }
    }
}
