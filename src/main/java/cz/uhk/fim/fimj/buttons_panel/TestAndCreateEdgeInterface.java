package cz.uhk.fim.fimj.buttons_panel;

import java.lang.reflect.Method;

import org.jgraph.graph.DefaultGraphCell;

/**
 * Tato třída coby rozhraní slouží pro deklaraci signatur a kontraktů metod pro manipulaci se vztahy v diagramu tříd
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface TestAndCreateEdgeInterface {

	/*
	 * ASOCIACE - ZDROJOVÁ TŘÍDA VÍ O CÍLOVÉ A NAOPAK
	 * 
	 * AGREGACE 1 : 1 ZDROJOVÁ TŘÍDA MÁ REFERENCI NA CÍLOVOU
	 * 
	 * AGREGACE 1 : N ZDROJOVÁ TŘÍDA MÁ REFERENCI NA LIST CÍLOVÝCH TŘÍD
	 */
	
	/**
	 * Metoda, která otestuje, zda vybrané objekty jsou třídy a ne hrany nebo
	 * komentáře Dále zda tyto třídy ještě vůbec existují Pokud budou tyto podmínky
	 * splněny: tak se mezi nimi vytvoří vztah v grafu i ve třídě Vztah asociace -
	 * Zdrojová třída ví o cílové a naopak
	 * 
	 * @param cells
	 *            - jednorozměrné pole o velikost 2, toto pole obsahuje označené
	 *            objekty, mezi kterými se má vytvořit vztah asociace
	 */
	void addAssociationEdge(final DefaultGraphCell[] cells);
	
	
	
	
	/**
	 * Metoda, která otestuje, zda vybrané objekty jsou třídy (pokud existují) Dále
	 * pokud obě třídy existuje a nejedná se o komentáře či hrany se otestuje, zda
	 * 'Zdrojová' třída z něčeho dědí nebo ne, pokud dědí, tak se oznámí, že v Javě
	 * vícenásobná dědičnost nelze zrealizovat (proto jsou rozhraní a abstraktní
	 * třídy) a když nedědí, tak se otestuje, zda se nejedná o Enum, ten nedědí a
	 * pokud jsou tyto podmínky splněny, tak se naimportují balíčky (pokud je třeba
	 * - když jsou třídy v různých balíčcích) a přidá se do hlavičky třídy dědíčnost
	 * - k tomu komentýře v poříslušné metodě - kdy to selže a kdy ne pokud toto
	 * dopadne dobře, tak se přidá i hrana do grafu "a jede se dál"
	 * 
	 * 
	 * @param cells
	 *            - Jednorozměrné pole o dvou oběktech - 'zdrojová a cílová' třída -
	 *            označené v grafu
	 */
	void addExtendsEdge(final DefaultGraphCell[] cells);
	
	
	
	
	/**
	 * CÍLOVÝ OBJEKT MUSÍ BÝT ROZHRANÍ ZDROJOVÝ OBJEKT NESMÁÍ BÝT ROZHRANÍ, ANI ENUM
	 * 
	 * Pokud budou podmínky splněny, třídy existují, a nenastane další problém,
	 * vytvoří se mezi označenými třídami vazva implementace: Zdorojová třída bude
	 * implementovat cílové rozhraní
	 * 
	 * Note:
	 * Pokud bude 'zdrojová' třída z něčeho dědit, a nebude mezera před poslední
	 * zvárkoku (bude li na řádku s hlavičkou) (public class ClassName extends
	 * Something{ ) - chyba, musí bát mezera: "public class className extends
	 * Something {" - OK
	 * 
	 * @param cells-=
	 *            Označené buňky, mezi kterými se mý vytvořit implementace
	 */
	void addImplementsEdge(final DefaultGraphCell[] cells);
	
	
	
	/**
	 * Zde asi neresit nic zdrojový objekt bude mít referenci na cilovy objekt a
	 * cilovy objektu muze byt asi cokoliv
	 * 
	 * Metoda, která přidá agregaci mezi dvěma označenými objekty: Oba objekty musí
	 * existovat - musí to být třídy v projektu pokud je toto splněno, vytvoří se ve
	 * 'Zdrojové' třídě reference - atribut na 'Cílovou' třídu : ve zdrojové třídě:
	 * private DestinationClass destinationClassName;
	 * 
	 * 
	 * @param cells
	 *            - Označené objekty - třídy
	 */
	void addAggregation_1_ku_1_Edge(final DefaultGraphCell[] cells);
	
	
	
	
	/**
	 * 
	 * Metoda přidá vztah agregace (1 : N) mezi označené třídy Otestuje se, zda oba
	 * objekty - třídy existují, pokud ano, tak ve 'Zdrojové' třídě vytvoří private
	 * List<DestinationClass> destinationClassName; (reference na cílovou třídu)
	 * 
	 * samozřejmě se naimportují i balíčky - pokud bude třeba
	 * 
	 * @param cells
	 *            - označené objekty - třídy
	 */
	void addAggregation_1_ku_N_Edge(final DefaultGraphCell[] cells);
	
	
	
	/**
	 * ZDROJOVÝ OBJEKTU MUSÍ BÝT TŘÍDA, NEBUDU PŘIDÁVAT KOMENTÁŘ KE KOMENTÁŘI, APOD.
	 * 
	 * Otestuje, se zda je označena třída, a pokud existuje, připojí se k ní
	 * komentář
	 * 
	 * Note:
	 * komentář je pouze buňka s textem v grafu, nikam se její obsah neukládá
	 * 
	 * @param cell
	 *            - označené objekty - třídy
	 */
	void addComment(final DefaultGraphCell cell);
	
	
	
	
	/**
	 * Metoda, která slouží pro přidání metod z příslušného rozhraní, tj. do třídy
	 * cell se přidají / vygenerují metody v methods.
	 * 
	 * @param cell
	 *            - buňka / třídy v diagramu tříd, do které se mají přidat metody z
	 *            rozhraní.
	 * 
	 * @param methods
	 *            - pole metod, které se mají vygenerovat do třídy cell, ale
	 *            vyfiltrují se statické metody, ty se do příslušné třídy nepřidají.
	 * 
	 * @return true, pokud se povedlo vygenerovat a zapsat metody v methods do třídy
	 *         cell, jinak false.
	 */
	boolean addImplementedMethods(final DefaultGraphCell cell, final Method[] methods);
}