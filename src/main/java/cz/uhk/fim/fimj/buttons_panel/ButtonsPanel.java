package cz.uhk.fim.fimj.buttons_panel;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import cz.uhk.fim.fimj.class_diagram.EdgeTypeEnum;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;

/**
 * Tato třída dědí z Jpanelu tento "panel" obsahuje základní tlačítka, které budou dostupné v okne na levé straně okna
 * aplikace - "levé menu" panel obsahuje základní tlačítka, jako je tlačítko pro vytvoření nové třídy, vytvoření vztahu
 * asociace mezi třídami, implementace rozhraní a dědičnosti mezi třídami
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ButtonsPanel extends JPanel implements ActionListener, LanguageInterface {

    private static final long serialVersionUID = 2927112754055280988L;


    /**
     * Tlačítko pro vytvoření nové třídy, resp. otevření dialogu pro zadání údajů pro novou třídu a následuné vytvoření
     * příslušné třídy.
     */
    private JButton btnNewClass;


    // JToogleButton vypadá po kliknutí "promáčknutě":
    private JToggleButton tbtnAddAssociation;
    private JToggleButton tbtnAddExtends;
    private JToggleButton tbtnAddImplements;
    private JToggleButton tbtnAggregation_1_ku_1;
    private JToggleButton tbtnAggregation_1_ku_N;
    private JToggleButton tbtnComment;


    /**
     * , Proměnná typu List - kolekce, do které vložím tlačítka, které reprezentují vztahy mezi třídami a komentář -
     * vlastně všechny, kromě tlačítka pro vytvořní nové třídy, budou vloženy v této kolekce, protože se bude lépe
     * nastavovat jejich označení - zda se na ně kliklo, apod:
     */
    private final List<JToggleButton> buttonsList;


    /**
     * Reference na diagram tříd pro přidávání vztahů k třídám a komentáře.
     */
    private final GraphClass classDiagram;

    /**
     * Proměnn, do které se vloži zvolený jazyk, ten se dále předává do dialogů které se otevřou po kliknutí na nějaké z
     * tlačítek:
     */
    private Properties languageProperties;


    /**
     * Konstruktor třídy.
     *
     * @param classDiagram
     *         - reference na diagram tříd.
     */
    public ButtonsPanel(final GraphClass classDiagram) {
        this.classDiagram = classDiagram;

        // nastavím mu velikost 6, protože tam bude pouze 6 tlačítek:
        buttonsList = new ArrayList<>(6);

        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        createAndInsertButtons();
    }


    /**
     * Metoda, která vytvoří instanci tlačítek a přidá je do panelu
     */
    private void createAndInsertButtons() {
        // Vytvoření instancí tlačítek:
        btnNewClass = new JButton();
        tbtnAddAssociation = new JToggleButton();
        tbtnAddExtends = new JToggleButton();
        tbtnAddImplements = new JToggleButton();
        tbtnAggregation_1_ku_1 = new JToggleButton();
        tbtnAggregation_1_ku_N = new JToggleButton();
        tbtnComment = new JToggleButton();


        // Nastavení událostí tlačítkům
        btnNewClass.addActionListener(this);
        tbtnAddAssociation.addActionListener(this);
        tbtnAddExtends.addActionListener(this);
        tbtnAddImplements.addActionListener(this);
        tbtnComment.addActionListener(this);
        tbtnAggregation_1_ku_1.addActionListener(this);
        tbtnAggregation_1_ku_N.addActionListener(this);


        // Přidám tlačítka do kolekce, abych mohl snadněji manipulovat s jejich odznačením / odzačením:
        buttonsList.add(tbtnAddAssociation);
        buttonsList.add(tbtnAddExtends);
        buttonsList.add(tbtnAddImplements);
        buttonsList.add(tbtnComment);
        buttonsList.add(tbtnAggregation_1_ku_1);
        buttonsList.add(tbtnAggregation_1_ku_N);


        // Přidání komponent do panelu:
        // rozestupy mezi tlačítky:
        add(Box.createRigidArea(new Dimension(0, 15)));

        // Zarovnání komponenty - v tomto případě tlačítka na střed
        btnNewClass.setAlignmentX(CENTER_ALIGNMENT);
        add(btnNewClass);


        add(Box.createRigidArea(new Dimension(0, 15)));
        tbtnAddAssociation.setAlignmentX(CENTER_ALIGNMENT);
        add(tbtnAddAssociation);


        add(Box.createRigidArea(new Dimension(0, 15)));
        tbtnAggregation_1_ku_1.setAlignmentX(CENTER_ALIGNMENT);
        add(tbtnAggregation_1_ku_1);


        add(Box.createRigidArea(new Dimension(0, 15)));
        tbtnAddExtends.setAlignmentX(CENTER_ALIGNMENT);
        add(tbtnAddExtends);


        add(Box.createRigidArea(new Dimension(0, 15)));
        tbtnAddImplements.setAlignmentX(CENTER_ALIGNMENT);
        add(tbtnAddImplements);


        add(Box.createRigidArea(new Dimension(0, 15)));
        tbtnAggregation_1_ku_N.setAlignmentX(CENTER_ALIGNMENT);
        add(tbtnAggregation_1_ku_N);


        add(Box.createRigidArea(new Dimension(0, 15)));
        tbtnComment.setAlignmentX(CENTER_ALIGNMENT);
        add(tbtnComment);
    }


    @Override
    public void actionPerformed(final ActionEvent e) {
        if (e.getSource() instanceof JButton) {
            if (e.getSource() == btnNewClass)
                RelationshipHelper.addClass(languageProperties, classDiagram);
        }

        else if (e.getSource() instanceof JToggleButton) {

            if (e.getSource() == tbtnAddAssociation)
                RelationshipHelper.checkRelationship(EdgeTypeEnum.ASSOCIATION, tbtnAddAssociation, this);


            else if (e.getSource() == tbtnAddExtends)
                RelationshipHelper.checkRelationship(EdgeTypeEnum.EXTENDS, tbtnAddExtends, this);


            else if (e.getSource() == tbtnAddImplements)
                RelationshipHelper.checkRelationship(EdgeTypeEnum.IMPLEMENTATION, tbtnAddImplements, this);


            else if (e.getSource() == tbtnAggregation_1_ku_1)
                RelationshipHelper.checkRelationship(EdgeTypeEnum.AGGREGATION_1_1, tbtnAggregation_1_ku_1, this);


            else if (e.getSource() == tbtnAggregation_1_ku_N)
                RelationshipHelper.checkRelationship(EdgeTypeEnum.AGGREGATION_1_N, tbtnAggregation_1_ku_N, this);


            else if (e.getSource() == tbtnComment)
                RelationshipHelper.checkRelationship(EdgeTypeEnum.COMMENT, tbtnComment, this);
        }
    }


    @Override
    public void setLanguage(final Properties properties) {
        languageProperties = properties;

        if (properties != null) {
            btnNewClass.setText(properties.getProperty("ButtonNewClass", Constants.BP_NEW_CLASS));
            tbtnAddAssociation.setText(properties.getProperty("ButtonAddAssociation", Constants.BP_ADD_ASSOCIATION));
            tbtnAddExtends.setText(properties.getProperty("ButtonAddExtends", Constants.BP_ADD_EXTENDS));
            tbtnAddImplements.setText(properties.getProperty("ButtonAddImplements", Constants.BP_ADD_IMPLEMENTS));
            tbtnComment.setText(properties.getProperty("ButtonComment", Constants.BP_ADD_COMMENT));
            tbtnAggregation_1_ku_1.setText(properties.getProperty("ButtonAggregation_1_ku_1",
                    Constants.BP_ADD_AGGREGATION_1_KU_1));
            tbtnAggregation_1_ku_N.setText(properties.getProperty("ButtonAggregation_1_ku_N",
                    Constants.BP_ADD_AGGREGATION_1_KU_N));


            // Popisky tlačítek:
            btnNewClass.setToolTipText(properties.getProperty("ButtonsNewClassToolTip",
                    Constants.BP_NEW_CLASS_TOOL_TIP));
            tbtnAddAssociation.setToolTipText(properties.getProperty("ButtonAddAssociationToolTip",
                    Constants.BP_ADD_ASSOCIATION_TOOL_TIP));
            tbtnAddExtends.setToolTipText(properties.getProperty("ButtonAddExtendsToolTip",
                    Constants.BP_ADD_EXTENDS_TOOL_TIP));
            tbtnAddImplements.setToolTipText(properties.getProperty("ButtonAddImplementsToolTip",
                    Constants.BP_ADD_IMPLEMENTS_TOOL_TIP));
            tbtnComment.setToolTipText(properties.getProperty("ButtonCommentToolTip",
                    Constants.BP_BTN_COMMENT_TOOL_TIP));
            tbtnAggregation_1_ku_1.setToolTipText(properties.getProperty("ButtonAggregation_1_ku_1ToolTip",
                    Constants.BP_BTN_AGGREGATION_1_KU_1_TOOL_TIP));
            tbtnAggregation_1_ku_N.setToolTipText(properties.getProperty("ButtonAggregation_1_ku_NToolTip",
                    Constants.BP_BTN_AGGREGATION_1_KU_N_TOOL_TIP));
        }

        else {
            btnNewClass.setText(Constants.BP_NEW_CLASS);
            tbtnAddAssociation.setText(Constants.BP_ADD_ASSOCIATION);
            tbtnAddExtends.setText(Constants.BP_ADD_EXTENDS);
            tbtnAddImplements.setText(Constants.BP_ADD_IMPLEMENTS);
            tbtnComment.setText(Constants.BP_ADD_COMMENT);
            tbtnAggregation_1_ku_1.setText(Constants.BP_ADD_AGGREGATION_1_KU_1);
            tbtnAggregation_1_ku_N.setText(Constants.BP_ADD_AGGREGATION_1_KU_N);


            // Popisky tlačítek:
            btnNewClass.setToolTipText(Constants.BP_NEW_CLASS_TOOL_TIP);
            tbtnAddAssociation.setToolTipText(Constants.BP_ADD_ASSOCIATION_TOOL_TIP);
            tbtnAddExtends.setToolTipText(Constants.BP_ADD_EXTENDS_TOOL_TIP);
            tbtnAddImplements.setToolTipText(Constants.BP_ADD_IMPLEMENTS_TOOL_TIP);
            tbtnComment.setToolTipText(Constants.BP_BTN_COMMENT_TOOL_TIP);
            tbtnAggregation_1_ku_1.setToolTipText(Constants.BP_BTN_AGGREGATION_1_KU_1_TOOL_TIP);
            tbtnAggregation_1_ku_N.setToolTipText(Constants.BP_BTN_AGGREGATION_1_KU_N_TOOL_TIP);
        }


        /*
         * Projdou se veškerá tlačítka a zjistí se jejich velikosti - dimension. Pak se otestuje, které je nejrvětší
         * (dle textu) a velikost tohoto "nejširšího" tlačítka se aplikuje i na ostatní - aby byla veškerá tlačítka v
         * toolbaru "stejná" na pohled.
         *
         * Note:
         *
         * Je to potřeba natavit protože se velikost tlačítek určuje dle textu, proto by bylo jinak každé tlačítko
         * různě široké.
         */


        // Istance třídy pro porovnávání objektů typu Dimension:
        final DimensionComparator myComparator = new DimensionComparator();


        final Dimension maxD = myComparator.getMaxDimension(buttonsList, btnNewClass.getPreferredSize());


        /*
         * Nyní je v proměnné maxD uloženoa hodnota největšího tlačítka, tak se na tuto hodnotu nastaví všechna
         * tlačítka:
         *
         * Note:
         *
         * Jelikož je jako layout pro tento panel s tlačítky použit BoxLayout, který v podstatě ignoruje nastaveni
         * velikosti pomoci metod setSize či PreferedSize, tak je třeba použít metodu setMaximumSize, která nastaví
         * požadovanou velikost i když je použit zmíněný BoxLayout
         *
         * Zdroj:
         *
         * http://stackoverflow.com/questions/3692987/why-will-boxlayout-not-allow-me-to-change-the-width-of-a
         * -jbutton-but-let-me-chan
         */

        buttonsList.forEach(b -> b.setMaximumSize(maxD));

        // Ještě se nastaví tlačítko pro vytvoření nové třídy na stejnou velikost:
        btnNewClass.setMaximumSize(maxD);
    }


    /**
     * Metoda, která slouží pro zdnačení tlačítka, na které se kliklo, rj. tlačítko, které reprezentuje nějaký vztah
     * mezi třídami v diagramu tříd.
     * <p>
     * Metoda vždy všechna tlačítka odznačí a pak dle proměnné button se dané tlačítko označí, tj. tlačítko, na které
     * uživatel klikl, tak bude vypadat jako stisknuté, aby užvatel věděl, jaký vztah se mezi označenými třídami
     * realizuje, pokud bude tato proměnná null tak se všechna tlačítka pouze odznačí, tj. nějaký vztah mezi třídami byl
     * již realizován
     *
     * @param button
     *         - tlačitko, které se má nastavit na opak proměnné selected
     */
    public final void setSelectedMenuButtons(final JToggleButton button) {
        buttonsList.forEach(b -> b.setSelected(false));

        if (button != null)
            button.setSelected(true);
    }


    /**
     * Metoda, která zakáže nebo povolí tlačítka v tomto panelu
     * <p>
     * Dle toho, zda je projekt otevřen nebo ne, jsou tyto tlačítka zpřístupněna nebo ne
     *
     * @param enable
     *         - Logická promnná, true nebo false, dle toho, zda se mají tlačítka povolit nebo zakázat
     */
    public void enableButtons(final boolean enable) {
        btnNewClass.setEnabled(enable);
        tbtnAddAssociation.setEnabled(enable);
        tbtnAddExtends.setEnabled(enable);
        tbtnAddImplements.setEnabled(enable);
        tbtnComment.setEnabled(enable);
        tbtnAggregation_1_ku_1.setEnabled(enable);
        tbtnAggregation_1_ku_N.setEnabled(enable);
    }


    /**
     * Getr na tlačítko, které slouží pro nastavení toho, že se do diagramu tříd bude přidávat vztah typu (symetrická)
     * asociace.
     *
     * @return - referenci na tlačítko, které slouží pro nastavení přidání vztahu typu (symetrická) asociace.
     */
    JToggleButton getTbtnAddAssociation() {
        return tbtnAddAssociation;
    }

    /**
     * Getr na tlačítko, které slouží pro nastavení toho, že se do diagramu tříd bude přidávat vztah typu dědičnost.
     *
     * @return - referenci na tlačítko, které slouží pro nastavení přidání vztahu typu dědičnost.
     */
    JToggleButton getTbtnAddExtends() {
        return tbtnAddExtends;
    }

    /**
     * Getr na tlačítko, které slouží pro nastavení toho, že se do diagramu tříd bude přidávat vztah typu implementace
     * rozhraní.
     *
     * @return - referenci na tlačítko, které slouží pro nastavení přidání vztahu typu implementace rozhraní.
     */
    JToggleButton getTbtnAddImplements() {
        return tbtnAddImplements;
    }

    /**
     * Getr na tlačítko, které slouží pro nastavení toho, že se do diagramu tříd bude přidávat vztah typu agregace 1 : 1
     * -> nové je to vztah asymetrická asociace.
     *
     * @return - referenci na tlačítko, které slouží pro nastavení přidání vztahu typu asymetrické asociace (agregace 1
     * : 1).
     */
    JToggleButton getTbtnAggregation_1_ku_1() {
        return tbtnAggregation_1_ku_1;
    }

    /**
     * Getr na tlačítko, které slouží pro nastavení toho, že se do diagramu tříd bude přidávat vztah typu agregace 1 :
     * N
     *
     * @return - referenci na tlačítko, které slouží pro nastavení přidání vztahu typu agregace 1 : N
     */
    JToggleButton getTbtnAggregation_1_ku_N() {
        return tbtnAggregation_1_ku_N;
    }

    /**
     * Getr na tlačítko, které slouží pro nastavení toho, že se do diagramu tříd bude přidávat komentář k označené
     * třídě.
     *
     * @return - referenci na tlačítko, které slouží pro nastavení přidání komentáře k označené třídě.
     */
    JToggleButton getTbtnComment() {
        return tbtnComment;
    }
}