package cz.uhk.fim.fimj.buttons_panel;

/**
 * Tato třídy coby Výčet slouží pro určení toho, jaká třída se má vytvořit. Resp. uživatel si označí nějaký typ třídy
 * (kalsická, výčet, rozhraní, ...) a dle toho, kterou třídu si označení, dle toho se daná třída vytvoří v adresáři
 * projěktu v src na příslušném umístění, a do diagramu tříd se přidá její reprezentace, k tomu abych, resp. aby
 * aplikace pozanala, jaký typ Javovské třídy se má vytvořit použiji tento výčet, kde definuji jednotlivé typy
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public enum ClassTypeEnum {

    CLASS, ABSTRACT_CLASS, INTERFACE, APPLET, ENUM, UNIT_TEST
}
