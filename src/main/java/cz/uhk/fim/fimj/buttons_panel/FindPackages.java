package cz.uhk.fim.fimj.buttons_panel;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import cz.uhk.fim.fimj.file.ReadFile;

/**
 * Tato třída obsahuje metody pouze pro získání balíčků z adresáře src v aktuálně otevřeném projektu.
 * <p>
 * Balíčky lze získat pomocí listu typu String, který bude obsahovat veškeré adresáře coby balíčky v projektu (od
 * adresáře src). Balíčky budou oddělené desetinnou tečkou a seřazeny dle abecedy vzestupně.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class FindPackages {

	/**
	 * List, který slouží pro "uložení" balíčků, které se najdou v adresáři src v
	 * otevřeném projektu.
	 */
	private static List<String> packageList;

	
	/**
	 * Proměnná, která obsahuje cestu k adresáři src v aktuálně otevřeném projektu.
	 * Je zde jako globální neboli instanční proměnná, protože nechci do metod
	 * předávat referenc na tuto cestu k adresáři.
	 */
	private static File srcFile;


	private FindPackages() {
	}

	/**
	 * metoda, která získá balíčky z adresáře scr v aktuálně otevřeném projektu.
	 * 
	 * @param pathToSrc
	 *            - cesta k adresáři src v aktuálně otevřeném projektu.
	 * 
	 * @return list typu String, který bude obsahovat veškeré balíčky (resp.
	 *         podadresáře) v adresáři src. Nebo tento list bude prázdný, pokud
	 *         adresář src neexistuje (nemělo by nastat), nebo pokud adresář src
	 *         žádné balíčky (podadresáře) neobsahuje.
	 */
	public static List<String> getPackages(final String pathToSrc) {
		/*
		 * Vytvořím si instanci listu pro vkládání balíčků - resp. podadresářů adresáře
		 * src v aktuálně otevřeném projektu.
		 */
		packageList = new ArrayList<>();
		
		/*
		 * Otestuji, zda existuje a je to adresář, ale tato podmínka by měla být
		 * zbytečná, stejně tak jako ty ostatní.
		 * 
		 * Pouze kdyby náhodou uživatel nějaký adresář omylem či úmyslně smazal.
		 */
		if (pathToSrc == null || !ReadFile.existsDirectory(pathToSrc))
			// Vrátím prázdný list.
			return packageList;
		
		/*
		 * Zde si již mohu vytvořit instanci proměnné srcFile a pokračovat v získávání
		 * balíčků.
		 */
		srcFile = new File(pathToSrc);			
		

		
		/*
		 * Najdu si veškeré potřebné balíčky v adresáři src v otevřeném projektu:
		 */
		findPackages(srcFile);
		
		
		/*
		 * Nahradím lomítka v adresářich za desetinné tečky pro oddělení balíčků.	
		 */
		packageList = replaceSlashByDot(packageList);
		
		/*
		 * Seřadím balíčky dle abecedy:
		 */
		Collections.sort(packageList);
		
		
		// Vrátím získané balíčky:
		return packageList;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o adresář, pokud ano, pak si načte všechny
	 * jeho podadresáře a rekurzí se volá tato metoda pořád do kola. Pokud se jedná
	 * o adresář, pak se ještě přidá do listu, který obsahuje veškeré podadresáře
	 * (pokud jej ještě neobsahuje).
	 * 
	 * @param file
	 *            cesta k nějakému souboru či adresáři (vše to jsou podadresáře src
	 *            v otevřeném projektu) u kterého se testuje, zda je to adresář a
	 *            případně se přidá z cesty k danému adresáři balíčky do výše
	 *            uvedeného listu.
	 */
	private static void findPackages(final File file) {
		// Nemělo by nastat, že by neexistoval nebo byl list souborů null:
		if (file.exists() && file.isDirectory() && file.listFiles() == null)
			return;
		
		/*
		 * Projdu všechny soubory v daném adresáři
		 */
		Arrays.stream(file.listFiles()).forEach(f -> {
			// Testování existence by mělo být zbytečné
			if (f.exists() && f.isDirectory()) {
				/*
				 * Načtu si balíčky daného adresáře:
				 */
				final String packageText = getTextOfPackage(f.getAbsolutePath());

				// Přidám jej do listu (pokud tam ještě není):
				if (packageText != null && !packageList.contains(packageText))
					packageList.add(packageText);

				// Pokračuji rekurzí:
				findPackages(f);
			}
		});
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která získá pouze text balíčku z abstolutní cesty k danému adresáři,
	 * resp. balíčku. Tento text balíčku se získá tak, že si vezmu pouze text z
	 * cesty k danému adresáři od délky adresáře src.
	 * 
	 * například: cesta k adresáři src: C:\\Users\\krunc\\Desktop\\BC_Workspace\\src
	 * cestak balíčku:
	 * C:\\Users\\krunc\\Desktop\\BC_Workspace\\src\\model\\data\\...
	 * 
	 * vrátí se adresáře (balíčky) od adresáře src - od jeho délky: model\\data\\...
	 * 
	 * @param path
	 *            - cesta k nějakému adresáři - balíčku, ze kterého se vezme pouze
	 *            text, který znyčí balíčky od adresáře src.
	 * 
	 * @return výše popsaný text z balíčku od adresáře src, popř. null, poud bude
	 *         cesty k balíčku kratší než cesta k adresáři src, ale to by nastat
	 *         němělo, protože vše jsou podadresáře src.
	 */
	private static String getTextOfPackage(final String path) {
		// nemělo by nastat -> path by vždy měl být v podadresáři srcFile:
		if (srcFile.getAbsolutePath().length() > path.length())
			return null;

		/*
		 * Získám si text balíčku, který je od délky adreáře src +1 (to +1 je lomítko) a
		 * ten zbytek jsou adresáře, které značí balíčky v adresáři src v otevřeném
		 * projektu.
		 */
		return path.substring(srcFile.getAbsolutePath().length() + 1);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která nahradí veškerá lomítka v jednotlivých položkách v listu
	 * (lomítko je dle OS) za desetinnou tečku a daný list vrátí.
	 * 
	 * @param list
	 *            - list, který obshuje položky coby balíčky nalezené v adresáři src
	 * 
	 * @return list s balíčky v otevřeném adresáři src, akorát bude jako
	 *         "oddělovače" obsahovat desetinné tečky a ne lomítka.
	 */
	private static List<String> replaceSlashByDot(final List<String> list) {
		return list.stream().map(s -> s.replace(File.separator, ".")).collect(Collectors.toList());
	}
}
