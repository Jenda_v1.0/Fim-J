package cz.uhk.fim.fimj.buttons_panel;

/**
 * Třída slouží pouze k tomu, když chce uživatel vytvořit novou třídy klikne na tlačítko New Class, otevře se dialogové
 * okno, kam se zadá název nové třídy a označí se typ třídy, pomocí do této třídy se tyto hodnoty "uloží" a předjí se
 * zpět do třídy s tlačítky - ButtonsPanel kde se dále vytvoří příslušná třída
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class NewClassInfo {

    /**
     * Pro název nové třídy:
     */
    private final String className;

    /**
     * Typ nové třídy:
     */
    private final ClassTypeEnum classType;


    private final boolean generateMainMethod;


    /**
     * Konstruktor této třídy, který naplní příslušné hodnoty potřbné pro vytvoření nové třídy
     *
     * @param className
     *         - název třídy
     * @param classType
     *         - typ Javovské třídy, který se má vytvořit (výšet, rozhraní, abstraktní třída, ...)
     * @param generateMainMethod
     *         - logická hodnota, dle které poznám, zda mám v nově vytvořené třídě definovat hlavní - spouštěcí metodu
     *         main
     */
    public NewClassInfo(final String className, final ClassTypeEnum classType, final boolean generateMainMethod) {
        super();

        this.className = className;
        this.classType = classType;
        this.generateMainMethod = generateMainMethod;
    }


    /**
     * Metoda, která vrátní název třídy
     *
     * @return nový - zadaná název třídy
     */
    public final String getClassName() {
        return className;
    }


    /**
     * Metoda, která vrátí typ Javovské třídy, který se má vytvořit
     *
     * @return typ třídy, který se má vytvořit
     */
    public final ClassTypeEnum getClassType() {
        return classType;
    }


    /**
     * Metoda, která vrátí logicku hodnotu, dle které poznám, zda se má v nově vytvořené třídě vy generovat hlavní -
     * spouštěcí metoda main
     *
     * @return true, pokud se má vytvořet metda main, jinak false
     */
    final boolean isGenerateMainMethod() {
        return generateMainMethod;
    }
}