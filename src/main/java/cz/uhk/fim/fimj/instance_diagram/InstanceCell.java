package cz.uhk.fim.fimj.instance_diagram;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Properties;

import cz.uhk.fim.fimj.reflection_support.ParameterToText;
import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;
import org.jgraph.graph.DefaultGraphCell;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.instances.Instances;

/**
 * Tato třída slouží jako buňka coby instance, v tomto případě se narozdíl od diagramu tříd jedná poiuze o proměnné,
 * coby název třídy, jejíž instanci tato buňak reprezentuje a pak dvojtečka, a pak název reference, pod kterou je tato
 * intance dostupná.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class InstanceCell extends DefaultGraphCell implements LanguageInterface {

	private static final long serialVersionUID = 1L;


    /**
     * Název třídy - classname = název třídy, z níž byla vytvořena konkrétní instance
     */
    private final String className;
    /**
     * referenceName = název reference, pod kterou je tato třída dostupná
     */
    private final String referenceName;
    /**
     * packageName = balíčky třídy model.data (a nzev třídy: .className)
     */
    private final String packageName;

	
	
	/**
	 * Logická proměnná, do které se vloží hodnota true, poukd se mají 
	 * v instancí, resp. v této buňce zobrazit u třídy její balíčky, jinak
	 * false, pokud se má zobrazit pouze název třídy jejích instance tato buňka 
	 * reprezentuje
	 * 
	 * Note:
	 * Třída by se vždy měla nacházet v nějakém balíčku, minimálně v defaultPackage,
	 * takže by tato proměnná neměla být null niky, pokud se jedná o třídu z diagramu tříd,
	 * a v takkovém případě, by se vždy měla buď pomocí vytvoření třídy v aplikaci nebo
	 * pomocí vlákna při otevření projektu přesunout, alespoň do toho výchozího balíčku,
	 * a i kdyby v této proměnné náhodou bylo null (nemělo by nastat), tak by nemělo dojít 
	 * k chybě, protože se hodnota této proměnné převede na text a pří nejhorším případě se před
	 * třídou zobrazí null.ClassName, apod.
	 */
	private final boolean showPackages;
	
	
	
	
	/**
	 * Proměnná, která značí počet proměnných, které se mají zobrazit v této buňce.
	 * Jedná se o počet proměnných z příslušné instance, kterou tato třída
	 * reprezentuje.
	 */
	private final int countOfVariables;
	
	
	
	
	/**
	 * Proměnná, která značí, zda se má zobrazit prvních několik proměnných z
	 * příslušné instance nebo ne.
	 */
	private final boolean showVariables;
	
	
	
	
	
	
	/**
	 * Proměnná pro text v uživatelem zvoleném jazyce: 'Referenční proměnná', tento
	 * text se využije v případě, že je hodnota příslušné proměnné instance nějaké
	 * třídy z diagramu tříd, která se aktuálně nachází v diagramu instancí.
	 */
	private static String txtReferenceVar;
	
	/**
	 * Proměnná pro text v uživatelem zvoleném jazyce. Tento text značí, že nebyly
	 * nalezeny žádné proměnné v příslušné instanci, které sby semohli zobrazit.
	 */
	private static String txtNoVariables;
	
	/**
	 * Proměnná pro text v uživatelem zvoleném jazyce. Tento text obsahuje informaci
	 * o tom, že v příslušné instanci nebyly nalezeny žádné metody, které by se
	 * mohli uživateli zobrazit.
	 */
	private static String txtNoMethods;
	
	
	
	
	
	
	/**
	 * Proměnná, která obsahuje text s proměnnými v příslušné instanci, buď může
	 * obsahovat přehled několik zvolených proměnných nebo text, že třída
	 * (instance) neobsahuje žádné proměnné.
	 */
	private String textWithVariables;
	
	
	
	
	
	
	
	/**
	 * Tato proměnná slouží pro "dočasné" uložení textu pro převod X - rozměrného
	 * pole do textu. Je to statická proměnná, aby vždy, když se iteruje nějakým
	 * polem, tak se do této proměnné "odkládal" text hodnot z toho iterovaného
	 * pole, pak se pouze ten text vloží do nějaké proměné pro vykreslení a
	 * pokračuje se dál.
	 */
	private static final StringBuilder tempForArray = new StringBuilder();
	
	
	
	
	
	/**
	 * Proměnná, která značí počet metod, který se má zobrazit. Jedná se o počet
	 * nějakolika prvních nalezených metod v příslušné instanci, který se má
	 * zobrazit.
	 */
	private final int countOfMethods;
	
	
	/**
	 * Logická proměnná, která značí, zda se mají zobrazovat metody nebo ne. Tj.
	 * pokud bude tato proměnná true, pak se zobrazí i prvních několik nalezených
	 * metod v příslušné instanci, jinak ne.
	 */
	private final boolean showMethods;
	
	
	/**
	 * Proměnná, do které se načtou nalezené metody, pokud se mají zobrazit. Tato
	 * proměnná bude obsahovat prvních několik nalezených metod, nebo příslušný
	 * počet prvních nalezených metod (countOfMethods) a ty se zobrazí pod
	 * proměnnými.
	 */
	private String textWithMethods;
	
	
	
	
	
	/**
	 * Proměnná, která značí počet proměnných (+ desetinné tečky pod proměnnými -
	 * jsou li zobrazeny) zobrazených v příslušné buňce / instanci. Tato proměnná je
	 * potřeba, abych věděl, kolik řádků mám obarvit jakou barvou a fontem, protože
	 * chci, abych uživatel měl možnost zvýraznit název reference, proměnné a metody
	 * jinou barvou.
	 * 
	 * Proto potřebuji tuto proměnnou, která mi vždy "řekne", kolik bylo načtených
	 * proměnných a dle toho mohu příslušný počet řádků obarvit příslušnou barvou a
	 * aplikovat na ně příslušný font a pro všechny ostatní řádky (po této proměnné
	 * tedy variablesCount + 1) budou metody a na prvním řádku - index 0 bude ten
	 * název reference.
	 * 
	 * Pro počet metod už si řádky nepotřebuji počítat, protože vím, že na prvním
	 * řádku bude vždy reference, pak jsou volitelné proměnné (atributy), jejich
	 * počet mám v této proměnné variablesCount a po těchto atributech následují
	 * metody, takže variablesCount + 1 jsou metody.
	 * 
	 * Note: Pokud počítám i mezeru mezi atributy a metody, tak variablesCount + 2
	 * budou metody pro obarvení textu.
	 */
	private int variablesCount;
	
	
	
	
	
	
	
	
	/**
	 * Proměnná, která značí, zda se mají zobrazit veškeré atributy / proměnné v
	 * příslušné instanci. Pokud ano, pak se vždy zobrazí veškeré atributy v
	 * příslušné třídě nehledě na počet atributů, kolik se má zobrazit, tj. proměnná
	 * countOfVariables bude ignorována, načtou se veškeré proměnné.
	 */
	private final boolean showAllVariables;
	
	
	/**
	 * Proměnná, která značí, zda se mají načíst veškeré metody v příslušné instanci
	 * nebo ne. Poukd bude tato proměnná true, pak se mají načíst veškeré metody v
	 * příslušné instanci nehledě na zadaný počet metod, který se má zobrazit, tj.
	 * proměnná countOfMethods bude igorována.
	 */
	private final boolean showAllMethods;
	
	
	
	
	
	
	
	
	
	/**
	 * Konstrukor této třídy. Slouží hlavně pro naplnění proměnných
	 * 
	 * @param className
	 *            - název třídy, ze které byla vytvořena instance
	 * 
	 * @param referenceName
	 *            - název reference na instanci nějaké třídy, kterou tato buňka
	 *            reprezentuje
	 * 
	 * @param showPackages
	 *            - logická hodnota, dle které se pozná, zda se má v této konkrétní
	 *            instanci coby buňky zobrazit název třídy s balíčkama nebo bez nich
	 * 
	 * @param countOfVariables
	 *            - proměnná, která značí počet atributů, který se má zobrazit.
	 * 
	 * @param showVariables
	 *            - logická prměnná, která značí, zda se má zobrazit prvních několik
	 *            proměnných nebo ne.
	 * 
	 * @param countOfMethods
	 *            - počet metod, která by se měl zobrazit v příslušné buňce /
	 *            instanci v diagramu instancí.
	 * 
	 * @param showMethods
	 *            - logická proměnná, která značí, zda se mají zobrazit i metody pod
	 *            proměnnými.
	 * 
	 * @param showAllVariables
	 *            - logická proměnná, která značí, zda se mají zobrazit veškeré
	 *            proměnné z příslušné třídy nebo ne. True značí, že se zobrazí
	 *            veškeré proměnné v příslušné třídě, takže se pak bude "ignorovat"
	 *            proměnná countOfVariables, zobrazí se v "této" buňce veškeré
	 *            proměnné, které se v příslušné instanci nachází.
	 * 
	 * @param showAllMethods
	 *            - logická proměnná, která značí, zda se mají zobrazit všechny
	 *            metody z příslušné třídy nebo ne. True značí, že se mají zobrazit
	 *            všechny metody v příslušné instanci, false značí, že se zobrazí
	 *            pouze countOfMethods.
	 */
	InstanceCell(final String className, final String referenceName, final String packageName,
                        final boolean showPackages, final int countOfVariables, final boolean showVariables,
                        final int countOfMethods, final boolean showMethods, final boolean showAllVariables,
                        final boolean showAllMethods) {

		this.className = className;
		this.referenceName = referenceName;
		this.packageName = packageName;
		this.showPackages = showPackages;
		this.countOfVariables = countOfVariables;
		this.showVariables = showVariables;
		this.countOfMethods = countOfMethods;
		this.showMethods = showMethods;
		this.showAllVariables = showAllVariables;
		this.showAllMethods = showAllMethods;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro sestavení textu s prvními několika proměnnými v
	 * příslušné instanci.
	 * 
	 * @param objInstance
	 *            - instance nějaké třídy, ze které se má vzít prvních několik
	 *            proměnných.
	 */
	final void makeTextWithVariables(final Object objInstance) {
		/*
		 * Získám si veškeré proměnné z příslušné instance.
		 */
		final Field[] fields = objInstance.getClass().getDeclaredFields();
		
		
		// Nastavím počet vypsaných proměnných na nulu:
		variablesCount = 0;
		
		
		/*
		 * Pokud žádné proměnné neobsahuje, pak není co řešit.
		 */
		if (fields.length == 0) {
			// Text, že nebyly nalezena žádné proměnné. Dále je třeba přidat mezeru, kdyby
			// se vypisovaly i metody, tak aby nebyly ihned pod sebou, ale byla mezi
			// proměnnými a metodami mezera:
			textWithVariables = txtNoVariables + "\n";
			
			/*
			 * Zde musím nastavit počet proměnných na (nebo inkrementovat), protože když to
			 * neudělám, tak se i ten nápis, že nebyly nalezeny proměnné bude zobrazovat
			 * barvou a fontem pro metody, což je špatně.
			 */
			variablesCount++;
			
			// Zde mohu skončit:
			return;
		}
			
		
		// "Vyprázdním" proměnnou pro nový text:
		textWithVariables = "";
		
		
		
		for (int i = 0; i < fields.length; i++) {
			/*
			 * Získám si položku (proměnnou) na příslušném indexu v poli fields.
			 */
			final Field field = fields[i];
			
			// Zjistím si viditelnost a dle toho přidělím proměnné znak:
			if (Modifier.isPublic(field.getModifiers()))
				textWithVariables += "+";
			else if (Modifier.isPrivate(field.getModifiers()))
				textWithVariables += "-";
			else if (Modifier.isProtected(field.getModifiers()))
				textWithVariables += "#";
			else if (ReflectionHelper.isPackagePrivateVisibility(field.getModifiers()))// (Stačí else)
				textWithVariables += "~";
			
			
			// Mezera mezi znaménkem viditelnosti a názvem proměnné:
			textWithVariables += " ";
			
			// Název proměnné:
			textWithVariables += field.getName();

			
			// Rovná se mezi názvem proměnné a její hodnotou:
			textWithVariables += " = ";
			
			
			// Hodnota proměnné:
			textWithVariables += getFieldValueInText(field, objInstance);
			
			
			// Zvýším počet proměnných:
			variablesCount++;
			
			
			// Nový řádek pro další proměnnou, budu jej přidávat v každém případě, protože
			// chci, aby byly případně metody odděleny:
			textWithVariables += "\n";

			
			
			// Pokud jsem již vypsal příslušný počet proměněnných tak skončím, ale jen v
			// případě, že se nemají vypisovat všechny proměnné, pokud ano, pak nemohu
			// skončit, prostě projdu veškeré proměnné:
			if (i == (countOfVariables - 1) && !showAllVariables)
				break;
		}
		
		
		// Pokud se v příslušné instanci (/ třídě) nachází více proměnných přidám na
		// konec ještě tři tečky. Ale to jen v případě, že se nevypsaly všechny proměnné
		// v takovém případě ty tečky již přidávat nebudu, protože se vypsali všechny
		// proměnné, a žádné další už tam nejsou:
		if (fields.length > countOfVariables && !showAllVariables) {
			// Zde je také potřeba zvýšit počet proměnných, jinak by se ty tečky mohli
			// obarvit jinou barvou:
			variablesCount++;

			// Nový řádek je pro potenciální oddělení metod:
			textWithVariables += "...\n";
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro sestavení textu, který bude obsahovat prvních
	 * několik metody z instanci objInstance.
	 * 
	 * Metoda převede požadované / načtené metody do textu tak, že bude znak
	 * viditelnosti metody, pak název metody a pak v kulatých závorkách parametry,
	 * pak dvojtečka a návratový typ metody.
	 * 
	 * @param objInstance
	 *            - instance třídy, ze které se mají vzít nějaké metody a zobrazit v
	 *            textu.
	 */
	final void makeTextWithMethods(final Object objInstance) {
		/*
		 * Získám si veškeré metody z příslušné instance.
		 */
		final Method[] methods = objInstance.getClass().getDeclaredMethods();
		
		
		/*
		 * Pokud žádné metody neobsahuje, pak není co řešit.
		 */
		if (methods.length == 0) {
			textWithMethods = txtNoMethods;
			return;
		}
			
		
		// "Vyprázdním" proměnnou pro nový text:
		textWithMethods = "";
		
		
		
		for (int i = 0; i < methods.length; i++) {
			/*
			 * Získám si položku (metodu) na příslušném indexu v poli methods.
			 */
			final Method method = methods[i];
			
			// Zjistím si viditelnost a dle toho přidělím metodě znak:
			if (Modifier.isPublic(method.getModifiers()))
				textWithMethods += "+";
			else if (Modifier.isPrivate(method.getModifiers()))
				textWithMethods += "-";
			else if (Modifier.isProtected(method.getModifiers()))
				textWithMethods += "#";
			else if (ReflectionHelper.isPackagePrivateVisibility(method.getModifiers()))// (Stačí else)
				textWithMethods += "~";
			
			
			// Mezera mezi znaménkem viditelnosti a názvem metody:
			textWithMethods += " ";

			// Parametry a název metody:
			textWithMethods += ReflectionHelper.getMethodNameAndParametersInText(method.getName(), method.getParameters());

			// Dvojtečka za parametry před návratovou hodnotou metody:
			textWithMethods += " : ";

			// Návratový typ metody:
			textWithMethods += ParameterToText.getMethodReturnTypeInText(method, false);
			
			
			
			/*
			 * Nový řádek přidám v případě, že se mají projít všechny metody a ještě není na
			 * řadě ta poslední, až se výše vypíše ta poslední, pak už symbol pro nový řádek
			 * přidávat nebudu.
			 */
			if (showAllMethods && i != (methods.length - 1))
				// Nový řádek pro další proměnnou:
				textWithMethods += "\n";
			
			/*
			 * Dále, když se nemají vypisovat veškeré metody, pak se nový řádek přidá pouze
			 * v případě, že se ještě nenačetla ta poslední metoda - do počtu metod, které
			 * se mají zobrazit, nebo pokud ještě bude následovat další iterace cyklem, tj.
			 * ještě se bude alespoň jedna iterace následovat.
			 * 
			 * Proto když už nebude další iterace, pak nebude načtení další metody, takže
			 * není potřeba přidávat nový řádek, a když už se načetl určitý / definovaný
			 * počet metod, tak se také nebude přidávat symbol pro nový řádek, protože další
			 * iterace také nebude.
			 */
			else if (i != (countOfMethods - 1) && i != (methods.length - 1))
				// Nový řádek pro další proměnnou:
				textWithMethods += "\n";

			
			
			/*
			 * Pokud jsem již vypsal příslušný počet metod tak skončím, ale jen v případě,
			 * že se nemají vypsat všechny metody, protože pokud ano, pak musím projít celé
			 * pole s metodami.
			 */
			if (i == (countOfMethods - 1) && !showAllMethods)
				break;
		}
		
		
		// Pokud se v příslušné instanci (/ třídě) nachází více proměnných přidám na
		// konec ještě tři tečky. Ale je v případě, že se nemají vypsat všechny metody,
		// pokud ano, pak nebudu přidávat tečky, protože to nemá smysl, žádné další
		// metody už tam nejsou, všechny jsou vypsané:
		if (methods.length > countOfMethods && !showAllMethods)
			textWithMethods += "\n...";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro získání hodnoty z proměnné field, která se nachází v
	 * instanci objInstance. A pokud je ta hodnota null, pak se vrátí null (v
	 * textu), protože není třeba dále pokračovat. Jinak pokud hodnota té proměnné
	 * není null, pak se pokračuje dál, kde se zjišťuje, zda je ta hodnota pole,
	 * list nebo "klasická" proměnná a pokaždé se zjistí, zda je ta hodnota instance
	 * nějaké třídy z diagramu tříd, která se nachází v diagramu instancí, a pokud
	 * ano, přidá se za příslušnou položku ještě referenční proměnná na tu instanci.
	 * JInak se jen všechny ty hodnoty převedou do takové podoby, aby je bylo možné
	 * zobrazit v instanci (buňce).
	 * 
	 * @param field
	 *            - proměnná, ze které se má vzít její hodnota a otestovat, zda se
	 *            jedná o instanci v diagramu instancí (pro přidání reference za
	 *            tuto instanci), dále se pak veškeré hodnoty / hodnota převedou do
	 *            podoby pro zobrazení v diagramu instnací v příslušné buňce
	 *            (instanci).
	 * 
	 * @param objInstance
	 *            - instance nějaké třídy, která obsahuje proměnnou field, tato
	 *            instance zde je potřeba, aby se mohla vzít hodnota z příslušné
	 *            proměnné.
	 * 
	 * @return text, který reprezentuje hodnotu proměnné field.
	 */
	private static String getFieldValueInText(final Field field, final Object objInstance) {
		/*
		 * Získám si hodnotu z příslušné proměnné field, která se nachází v instanci
		 * objInstance.
		 */
        final Object objValue = ReflectionHelper.getValueFromField(field, objInstance);
		
		// Pokud je hodnota null, pak vrátím null:
		if (objValue == null)
			return "null";
		
		
		/*
		 * Zde vím, že získaná hodnota z proměnné není null, tak mohu pokračovat tak, že
		 * projdu hodnotu té proměnné, a zjistím, o jakou hodnotu se jedná, zda
		 * například to není instanci v diagramu instancí, pak by se za tu hodnotu
		 * přidala i refernce na tu instanci a v případě pole nebo listu by se to pole
		 * nebo list prošel a také by se otestovaly jeho hodnoty, zda to nejsou instance
		 * v diagramu instancí pro potenciální přidávní referencí a také hlavně převod
		 * pole do textové podoby, aby bylo možné jej vypsat do té buňky / instance v
		 * diagramu instancí.
		 */
		return buildTextFromValue(objValue);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro sestavení textu z hodnoty objValue (získaná hodnota
	 * z nějaké proměnné), o této hodnotě objValue se už musí vědět, že není null.
	 * 
	 * Metoda, zjistí o jaký typu se jedná (testuje se pouze pole, list a "klasická"
	 * proměnná) a dle toho se projdou hodnoty / hodnota proměnné a převede se do
	 * textové podoby, případě, pokud se jedná o instanci z diagramu instanci, tak
	 * se k ní přidá ještě referenční proměnná na tu instanci v diagramu instancí.
	 * 
	 * @param objValue
	 *            - hodnota získaná z nějaké proměnné, která se má převést do
	 *            textové podoby (případně se za ní přidá ještě reference - pokud je
	 *            to instance v diagramu instancí).
	 * 
	 * @return objValue převedenou do textové podoby, případně ještě za hodnotou
	 *         bude reference na instanci v diagramu instancí.
	 */
	private static String buildTextFromValue(final Object objValue) {
        // Projdu celé pole a u každé hodnoty otestuji, zda je to instance v diagramu
        // instancí, případě doplním referenci a celé pole v podobně textu vrátím.
        if (objValue.getClass().isArray()) {
            tempForArray.setLength(0);
            tempForArray.append("[");

            buildTextFromArray(objValue);

            if (tempForArray.toString().endsWith(", "))
                tempForArray.setLength(tempForArray.length() - 2);

            tempForArray.append("]");

            return tempForArray.toString();
        }





        // Pokud je to list, tak jej projdu a otestuji jeho hodnoty, zda to není
        // instance v diagramu instancí pro potenciální přidání reference.
        else if (ReflectionHelper.isDataTypeOfList(objValue.getClass())) {
            /*
             * Proměnná, do které postupně vložím veškeré hodnoty, které se nachází v
             * příslušné kolekci, veškeré hodnoty v příslušné kolekci je třeba "takto"
             * vládat do textové proměnné, aby v případě, že se jedná o instanci třídy z
             * diagramu tříd, která se aktuálně nachází v diagramu instancí, tak aby se za
             * tu instanci v listu mohla přidat reference na tu instanci.
             */
            final StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("[");

            @SuppressWarnings("unchecked") final List<Object> valuesList = (List<Object>) objValue;

            for (final Object o : valuesList) {
                /*
                 * Pokud je to null hodnota, pak prostě přidám null do textu a pokračuji další
                 * iterací, instance to zcela určitě nebude, tak nemá smysl pokračovat zbytkem
                 * těla cyklu.
                 */
                if (o == null) {
                    stringBuilder.append("null, ");
                    continue;
                }


                if (GraphInstance.isShowReferenceVariables()) {
                    /*
                     * Zde se jedná o nějakou hodnotu, která nění null a mají se přidávat referenční proměnné, tak si
                     * zjistím, zda je ta hodnota nějaká instance třídy z diagramu tříd, která se nachází v diagramu
                     * instancí.
                     */
                    final String reference = Instances.getReferenceToInstanceFromCdInMap(o);

                    /*
                     * Pokud reference není null, pak je hodnota 'o' nějaká instance nějaké třídy z
                     * diagramu tříd a tato instance ('o') se nachází v diagramu instancí, tak za
                     * hodnotu (instanci) přidám referenci na tuto instancí, aby uživatel věděl, o
                     * jaký objekt (/ instancí) se jedná.
                     */
                    if (reference != null)
                        stringBuilder.append(o).append(" (").append(txtReferenceVar).append(": ").append(reference).append("), ");

                    else
                        /*
                         * Zde se nejedná o instanci, která se nachází v diagramu instancí, tak ten objekt prostě
                         * "přidám":
                         */
                        stringBuilder.append(o).append(", ");
                }

                /*
                 * Zde se nemají zobrazovat referenční proměnné, tak tu hodnotu prostě přidám do textové proměnné pro
                 * vypsání a pokračuji další iterací.
                 */
                else
                    stringBuilder.append(o).append(", ");
            }

            // Odeberu poslední čárku - pokud se tam nachází:
            if (stringBuilder.toString().endsWith(", "))
                stringBuilder.setLength(stringBuilder.length() - 2);

            stringBuilder.append("]");

            return stringBuilder.toString();
        }






        /*
         * V případě, že se jedná o hodnotu typu String, tak budu ten text zobrazovat v
         * uvozovkách.
         *
         * Toto by nemělo být, ale, když ta proměnná bude prázdná text, tak se zobrazí
         * pouze například: "'+' variableName = " a na konci za rovná se už nebude nic,
         * ani ty prázdné uvozovky. Takže, je to spíše pouze pro přehlednost.
         */
        else if (objValue.getClass().equals(String.class))
            return "\"" + objValue + "\"";

            /*
             * Zde se jedná o stejný případ, jako výše u textové proměnné. Tj. Pokud zde
             * bude proměnná typu char nebo Character, a ten znak bude nějaký bílý znak,
             * například mezera apod. Tak by to nebylo vidět, proto ten znak "obalím" ještě
             * jednoduchými uvozovkami.
             */
        else if (objValue.getClass().equals(Character.class) || objValue.getClass().equals(char.class))
            return "'" + objValue + "'";









        /*
         * Pokud se mají zobrazovat reference na instance, tak otestuji, zda je ta
         * hodnota objValue instance nějaké třídy z diagramu tříd, která se nachází v
         * diagramu instancí, pokud ano, pak se u příslušné hodnoty zobrazi i reference
         * na tu instanci v diagramu instancí.
         */
        if (GraphInstance.isShowReferenceVariables()) {
            /*
             * Zde si zjistím, zda je získaná hodnota z proměnné nějaká instance třídy z
             * diagramu tříd, která se nachází v diagramu instancí.
             */
            final String reference = Instances.getReferenceToInstanceFromCdInMap(objValue);

            /*
             * Pokud reference není null, pak je objValue nějaká instance nějaké třídy z
             * diagramu tříd a tato instance (objValue) se nachází v diagramu instancí, tak
             * za hodnotu (instanci) přidám referenci na tuto instancí, aby uživatel věděl,
             * o jaký objekt se jedná.
             */
            if (reference != null)
                return objValue + " (" + txtReferenceVar + ": " + reference + ")";
        }

        // Zde vím, že se nejedná o null hodnotu, tak mohu zavolat metodu toString:
        return objValue.toString();
    }






    /**
     * Metoda, která slouží pro sestavení textu z X - rozměrného pole objArray. Metoda projde celé pole objArray a u
     * každé položky otestji, zda je to instance nějaké třídy z diagramu tříd a zda se tato instance nachází v diagramu
     * instancí, pokud ano, pak se otestuji, zda se mají přidávat reference a případně za tu instanci v poli přidá
     * referenci na tu instancí.
     * <p>
     * Ale v každém případě převede N - rozměrné pole objArray do podoby textu, aby bylo možné jej vypsat.
     * <p>
     * (Neřeší se případy, kdy je pole datového typu třeba nějaké datové struktury, například listu apod.)
     *
     * @param objArray
     *         - objekt, o kterém se již musí vědět, že není null a je to X - rozměrné pole. Toto pole se má "přepsat"
     *         do podoby textu pro vypsání v buňce reprezentující instanci v diagramu instancí.
     */
    private static void buildTextFromArray(final Object objArray) {
        /*
         * Zjistím si velikost pole objArray, abych jej mohl celé priterovat.
         */
        final int length = Array.getLength(objArray);

        // Projdu pole:
        for (int i = 0; i < length; i++) {
            /*
             * Získám si hodnotu na aktuálně iterovaném indexu v poli objArray.
             */
            final Object objValue = Array.get(objArray, i);

            // Pokud je hodnota null, pak jej přidám do textu a pokračuji další iterací:
            if (objValue == null)
                tempForArray.append("null, ");



                /*
                 * Pokud se jedná o další pole / dimezi, pak do textu přidám otevárací hranatou
                 * závorku, projdu to pole a na konec otestuji, zda se na konci nachází
                 * desetinná čárka pro oddělení položek v poli, pokud ano, pak ji odeberu a na
                 * konec přidám hranatou uzavírací závorku.
                 */
            else if (objValue.getClass().isArray()) {
                tempForArray.append("[");

                buildTextFromArray(objValue);

                if (tempForArray.toString().endsWith(", "))
                    tempForArray.setLength(tempForArray.length() - 2);

                tempForArray.append("], ");
            }


            // Zde se jedná o konkrétní hodnotu v poli, tak zjistím, zda je to instance z
            // diagramu instancí:
            else {
                /*
                 * Pokud se mají zobrazovat reference na instance, tak otestuji, zda je ta
                 * hodnota objValue instance nějaké třídy z diagramu tříd, která se nachází v
                 * diagramu instancí, pokud ano, pak se u příslušné hodnoty zobrazi i reference
                 * na tu instanci v diagramu instancí.
                 */
                if (GraphInstance.isShowReferenceVariables()) {
                    /*
                     * Zde si zjistím, zda je získaná hodnota z proměnné nějaká instance třídy z
                     * diagramu tříd, která se nachází v diagramu instancí.
                     */
                    final String reference = Instances.getReferenceToInstanceFromCdInMap(objValue);

                    /*
                     * Pokud reference není null, pak je objValue nějaká instance nějaké třídy z
                     * diagramu tříd a tato instance (objValue) se nachází v diagramu instancí, tak
                     * za hodnotu (instanci) přidám referenci na tuto instancí, aby uživatel věděl,
                     * o jaký objekt se jedná.
                     */
                    if (reference != null)
                        tempForArray.append(objValue).append(" (").append(txtReferenceVar).append(": ").append(reference).append("), ");

                        /*
                         * Zde to není instance v diagramu instancí, tak je třeba pouze přidat ten
                         * objekt a desetinnou čárku pro oddělení položek.
                         */
                    else
                        tempForArray.append(objValue).append(", ");
                }

                /*
                 * Zde se nemají přidávat reference, tak stačí přidat pouze tu položku pro
                 * zobrazení.
                 */
                else
                    tempForArray.append(objValue).append(", ");
            }
        }
    }



    public final String getClassName() {
        return className;
    }


    public final String getReferenceName() {
        return referenceName;
    }


    /**
     * Getr na proměnnou, která obsahuje počet načtených proměnných. Dle této hodnoty se pozná, kolik bylo zobrazeno
     * atributů.
     *
     * @return výše popsaný zobrazený počet atributů v buňce / instanci.
     */
    int getVariablesCount() {
        return variablesCount;
    }


    @Override
    public String toString() {
        /*
         * Text, do kterého vložím celkový text pro vypsání, tj. zda se mají vypisovat i
         * balíčky třídy nebo ne. Pak zda se mají zobrazovat atributy třídy a nebo
         * metody třídy.
         */
        String text = "";

        // Zda se mají zobrazovat balíčky, ve kterých se třída nchází:
        if (showPackages)
            text += packageName + "." + className + " : " + referenceName;

            // Zde jen název třídy s referencí:
        else
            text += className + " : " + referenceName;

        // Zda se mají zobrazovat některé atributy ve třídě:
        if (showVariables)
            text += "\n" + textWithVariables;

        // Zda se mají zobrazovat některé metody ve třídě:
        if (showMethods)
            // Přidám text s metodami:
            text += "\n" + textWithMethods;

        return text;
    }
















	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			txtReferenceVar = properties.getProperty("MIS_Txt_ReferenceVar", Constants.MIS_TXT_REFERENCE_VAR);
			txtNoVariables = properties.getProperty("MIS_Txt_NoVariables", Constants.MIS_TXT_NO_VARIABLES);
			txtNoMethods = properties.getProperty("MIS_Txt_NoMethods", Constants.MIS_TXT_NO_METHODS);
		}

		else {
			txtReferenceVar = Constants.MIS_TXT_REFERENCE_VAR;
			txtNoVariables = Constants.MIS_TXT_NO_VARIABLES;
			txtNoMethods = Constants.MIS_TXT_NO_METHODS;
		}
	}
}