package cz.uhk.fim.fimj.instance_diagram.overview_of_instance;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;
import cz.uhk.fim.fimj.forms.NameForInstance;
import cz.uhk.fim.fimj.instance_diagram.CheckRelationShipsBetweenInstances;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;
import cz.uhk.fim.fimj.instances.Instances;
import cz.uhk.fim.fimj.swing_worker.ThreadSwingWorker;

/**
 * Tato třída slouží pouze jako objekt do komponenty JList v dialogu pro zobrazení přehledu ohledně příslušné instance v
 * diagramu instancí. Konkrétně do listu s přehledem proměnných a je potřeba pro to, že je doplněna možnost pro převzetí
 * hodnoty z proměnné, která je datového typu nějaké třídy, která se nachází v diagramu tříd.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class VariableInfo extends ValuesInfo {

    /**
     * Logická proměnná, která značí, zda je hodnota v proměnné, kterou konkrétní instance této třídy reprezentuje
     * instance nějaké třídy z diagramu tříd a ta instanci v proměnné jetě není reprezentována v diagramu instancí
     * (hodnota true), jinak, když je tato hodnota false, pak nelze vytvořit referenci na příslušnou instanci v
     * proměnné, kterou tato třída reprezetnuje, protože ta hodnota proměnné, kterou tato třída reprezentuje není
     * instance třídy, nebo ta třída není z diagramu tříd.
     */
    private boolean isPossibleToCreateReferenceToInstance;


    /**
     * Hodnota získaná z příslušné proměnné, kterou konkrétní instance této třídy reprezentuje.
     * <p>
     * Může to být například list, pole, nebo konkrétní instance apod. Z této hodnoty se následně zjistí, zda obsahuje
     * nějaké instance tříd z diagramu tříd a ty se ještě nenachází v diagramu instancí, aby bylo možné na ně teprve
     * vytvořit referenci.
     */
    private final Object objValue;


    /**
     * List, který bude obsahovat instance, na které je možné vytvořit reference. Tedy tento list bude obsahovat
     * instance tříd z diagramu tříd, které se ještě nenachází v diagramu instancí.
     */
    private final List<Object> instancesList;


    /**
     * Proměnná, kterou reprezentuje konkrétní instance této třídy v dialogu pro zobrazení hodnot instance.
     */
    private final Field field;


    /**
     * Konstruktor této třídy.
     *
     * @param text
     *         - text, který má "reprezentovat" instance této třídy, resp. text, který bude zobrazen v příslušném
     *         listu.
     * @param objValue
     *         - hodnota získaná z proměnné, kterou instance této konkrétní třídy reprezetnuje.
     * @param clazz
     *         - třída, ze které byla získána proměnná a sestaven text (text).
     * @param field
     *         - proměnná, ze které byl sestaven text "text" a získána hodnota objValue. Proměnná je reprezentována
     *         konkrétní instancí této třídy.
     */
    VariableInfo(final String text, final Object objValue, final Class<?> clazz, final Field field) {
        super(text, clazz);

        this.objValue = objValue;
        this.field = field;

        instancesList = new ArrayList<>();

        isPossibleToCreateReferenceToInstance = isPossibleToCreateReference(objValue);
    }


    /**
     * Metoda, která zjistí, zda je objValue instance nějaké třídy z diagramu tříd, která se ještě nenachází v diagramu
     * instancí nebo zda je to nějaká datová struktura typu X - rozměrné pole nebo list a obsahuje instanci nějaké třídy
     * z diagramu tříd, která ještě není reprezentována v diagramu instancí..
     *
     * @param objValue
     *         - nějaká získaná hodnota z nějaké proměnné, o které se má zjistit, zda se jedná o instanci nějaké třídy z
     *         diagramu tříd a tato konkrétní intance ještě není reprezetována v diagramu instancí, nebo zda je to pole
     *         nebo list obsahující nějakou instanci třídy z diagramu tříd a ještě ta instance není v diagramu
     *         instancí.
     *
     * @return true, pokud je objValue instance nějaké třídy z diagramu tříd a tato instance ještě není repreztnována v
     * diagramu instancí, takže na ní je možné vytvořit referenci, nebo zda je objValue pole nebo list obsahující
     * instanci třídy z diagramu tříd, a ta instance ještě není reprezentována v diagramu instancí. Jinak false.
     */
    private boolean isPossibleToCreateReference(final Object objValue) {
        /*
         * Dále otestuji, zda vrácená hodnota není null a pokud není null, tak zjistím,
         * zda se jedná o instanci nějaké třídy, která se nachází v diagramu tříd a a
         * konkrétní hodnota vrácena z nějaké metody není instance, která se aktuálně
         * nachází v diagramu instancí - tedy není reprezentována, aby bylo možné na ni
         * vytvořit novou referenci.
         */


        /*
         * Zde musím nejprve vymazat list, protože by v něm jinak zůstávali původní
         * hodnoty.
         */
        instancesList.clear();


        /*
         * V případě, že se jedná o null hodnotu, pak nemá smysl pokračovat, žádná
         * instance to není.
         */
        if (objValue == null)
            return false;


        if (objValue.getClass().isArray())
            checkInstancesInArray(objValue);


        else if (ReflectionHelper.isDataTypeOfList(objValue.getClass())) {
            /*
             * Přetypuji si získaný list na list objektů:
             */
            @SuppressWarnings("unchecked") final List<Object> valuesList = (List<Object>) objValue;

            for (final Object o : valuesList) {
                if (o == null)
                    continue;

                /*
                 * Zjistím, zda je tato konkrétní položky v listu instance třídy z diagramu tříd
                 * a ještě není reprezentována v diagramu instancí, pokud ano, pak jej přidám do
                 * listu instancesList, protože na ni je možné vytvořit referenci.
                 */
                if (checkObject(o))
                    instancesList.add(o);
            }
        }

        else {
            /*
             * Zde se jedná o "klasickou" proměnnou nebo nějakou z těch netestovaných, tak
             * pouze zjistím zda se jedná o instanci nějaké tříy z diagramu tříd, která se
             * ještě nenachází v diagramu instancí a pokud ano, přidám ji do listu pro
             * potenciální vytvoření reference.
             */
            if (checkObject(objValue))
                instancesList.add(objValue);
        }


        /*
         * Zde, pokud se do listu instancesList vložila alespoň jedna nalezená instance,
         * pak je možné vytvořit referenci, protože se našla alespoň jedna instance
         * nějaké třídy z diagramu tříd, která se ještě nenachází v diagramu instancí.
         */
        return !instancesList.isEmpty();
    }


    /**
     * Metoda, která prohledá celé X - rozměrné pole objArray a pro každou nalezenou položku zjistí, zda se jedná o
     * instanci třídy z diagramu tříd, která se ještě nenachází v diagramu instancí, pokud takovou položku najde, pak ji
     * přidá do listu instancesList, který bude obsahovat veškeré položky, resp. instance, ke kterým je možné vytvořit
     * nové reference a zobrazit je tak v diagramu instancí.
     *
     * @param objArray
     *         - X - rozměrné pole, které se má prohledat a případě, pokud se najde instance třídy z diagramu tříd,
     *         která ještě není v diagramu instancí, tak ji přidat do listu instancesList.
     */
    private void checkInstancesInArray(final Object objArray) {
        /*
         * Zjistím si velikost / délku pole objArray:
         */
        final int length = Array.getLength(objArray);

        // Projdu celé pole:
        for (int i = 0; i < length; i++) {
            /*
             * Získám si hodnotu z pole na právě iterovaném indexu.
             */
            final Object objTempValue = Array.get(objArray, i);

            // Pokud je to null hodnota, pak nemá smysl pokračovat.
            if (objTempValue == null)
                continue;


            // Pokud je to další pole, pak pokračuji rekurzí:
            if (objTempValue.getClass().isArray())
                checkInstancesInArray(objTempValue);


                /*
                 * Zde je to nějaká hodnota, která není null ani další pole / dimeze, tak
                 * zjistím, zda je to instance třídy z digramu tříd, která ještě není v diagramu
                 * instancí a pokud ano, pak ji přidám do listu instancesList.
                 */
            else if (checkObject(objTempValue))
                instancesList.add(objTempValue);
        }
    }


    /**
     * Metoda, která zjistí, zda je objekt objValue instance nějaké třídy z diagramu tříd, která ještě není v diagramu
     * instancí, tj. uživatel na ní ještě nemá referenci (v rámci této aplikace), takže ještě ta instance není
     * reprezentována v digramu instancí.
     *
     * @param objValue
     *         - objekt, o kterém se má zjistit, zda je to instance nějaké třídy z diagramu tříd, která se ještě
     *         nenachází v diagramu instancí.
     *
     * @return true, pokud je objValue instance nějaké třídy z diagramu tříd, který se ještě nenachází v diagramu
     * instancí, jinak false.
     */
    public static boolean checkObject(final Object objValue) {

        /*
         * Zjistím si, zda se vrátila nějaká hodnota, která je nějaká instance, která se
         * nachází v diagramu instancí.
         */
        final boolean existInstance = Instances.isInstanceInMap(objValue);

        /*
         * V případě, že instance již v diagramu instancí existuje, pak nemá smysl
         * pokračovat, protože už má nějakou referenci
         */
        if (existInstance)
            return false;



        /*
         * Složím si text, který obsahuje veškeré balíčky, ve kterých se třída nachází a
         * na konec přidám desetinnou tečku a název třídy. Toto je pro otestování, zda
         * se třída nachází v diagramu tříd nebo ne.
         */

        final String packages;

        if (objValue.getClass().getPackage() != null)
            packages = objValue.getClass().getPackage().getName();
        else
            packages = "";

        /*
         * Název třídy i s balíčky, abych mohl zjistit, zda se v diagramu tříd vyskytuje
         * taková třídy, resp. buňka s takovým textem, který reprezentuje příslušnou
         * třídu.
         */
        final String classNameWithPackages = packages + "." + objValue.getClass().getSimpleName();


        /*
         * Zjistím si, zda se v diagramu tříd nachází třída / buňka, která obsahuje
         * příslušný text (název třídy s balíčky).
         *
         * V případě, že se nejedná o třídu z diagramu tříd, tak mohu skončit, jiné
         * instance tříd než třídy z diagramu tříd se v diagramu instanci
         * nereprezentují.
         */
        return GraphClass.isClassWithTextInClassDiagram(classNameWithPackages);
    }


    /**
     * Metoda, která slouží pro vytvoření reference na instanci objValue.
     * <p>
     * Tato metoda otevře dialog pro zadání nové reference na instanci objValue, takové, která dosud ještě neexistuje. A
     * po zadání reference se objValue (instance) zobrazí v diagramu instancí pod příslušnou referencí.
     *
     * @param objValue
     *         - instance nějaké třídy z diagramu tříd, která se ještě nenachází v diagramu instancí, tj. v rácmi této
     *         aplikace na ni ještě uživatel nezadal referenci, aby ji bylo možné zobrazit v diagramu instancí.
     * @param instanceDiagram
     *         - reference na diagram instancí.
     * @param languageProperties
     *         - objekt, který obsahuje texty pro tuto aplikace ve zvoleném jazyce.
     */
    public static void createNewReferenceForInstance(final Object objValue, final GraphInstance instanceDiagram,
                                                     final Properties languageProperties) {

        /*
         * Složím si text, který obsahuje veškeré balíčky, ve kterých se třída nachází a
         * na konec přidám desetinnou tečku a název třídy. Toto je pro získání a předání
         * potřebných dat do následujícího dialogu pro vytvoření reference na příslušnou
         * instanci.
         *
         * Note:
         * Následující podínka pro to, zda se objValue v nějakém balíčku nachází by už
         * neměla být potřeba, protože při zavolání této metody už vím, že se jedná o
         * instanci nějaké třídy z diagramu tříd, takže se ta třída, resp. konkrétní
         * instance v objValue musí nacházet minimálně v jednom balíčku, ale kdyby
         * náhodou se někde něco pokazilo, tak aby zde nenastala výjimka.
         */


        final String packages;

        if (objValue.getClass().getPackage() != null)
            packages = objValue.getClass().getPackage().getName();
        else
            packages = "";

        /*
         * Název třídy i s balíčky pro vytvoření buňky v diagramu instancí, aby
         * reprezentovala instnanci příslušné třídy.
         */
        final String classNameWithPackages = packages + "." + objValue.getClass().getSimpleName();



        /*
         * Vytvořím si dialog pro zadání nového názvu - nové reference:
         */
        final NameForInstance nfi = new NameForInstance(Instances.getListOfVariablesOfClassInstances(),
                classNameWithPackages, objValue);
        nfi.setLanguage(languageProperties);


        /*
         * zobrazím dialog a získám si novou referenci:
         */
        final String newReference = nfi.getNewReference();


        if (newReference != null) {
            Instances.addInstanceToMap(newReference, objValue);

            instanceDiagram.addInstanceCell(objValue.getClass().getSimpleName(), packages, newReference);

            /*
             * Výše se přidala nová instance do digramu instancí, tak je třeba aktualizovat okno pro automatické
             * doplňování / dokončování příkazů v editoru příkazů, aby byla na výběr i výše vytvořená instance.
             */
            App.getCommandEditor().launchThreadForUpdateValueCompletion();

            // Zde je třeba spustit vlákno, které zkontroluj vztahy mezi instancemi v
            // diagramu instanci:
            ThreadSwingWorker.runMyThread(new CheckRelationShipsBetweenInstances(instanceDiagram,
                    instanceDiagram.isShowRelationShipsToItself(), GraphInstance.isUseFastWay(),
                    GraphInstance.isShowRelationshipsInInheritedClasses(),
                    instanceDiagram.isShowAssociationThroughAggregation()));
        }
    }


    /**
     * Metoda, která do proměnné isPossibleToCreateReferenceToInstance vloží logickou hodnotu o tom, zda je objValue
     * instance nějaké třídy, na kterou je možné vytvořit referenci a znázornit tu instanci v diagramu instancí (hodnota
     * true), jinak false.
     */
    final void checkCreationOfReference() {
        isPossibleToCreateReferenceToInstance = isPossibleToCreateReference(objValue);
    }


    /**
     * Getr na list, který obsahuje nebo slouží pro "uložení" instancí tříd z diagramu tříd, které se ještě nenachází v
     * diagramu instancí, tj. je možné na ně vytvořit reference.
     *
     * @return výše popsaný list s instancemi, na které je možné vytvořit reference.
     */
    final List<Object> getInstancesList() {
        return instancesList;
    }


    /**
     * Getr na proměnnou, která značí, žda je možné vytvořit referenci na instanci nějaké třídy z diagramu tříd
     * (objValue) - hodnota true, jinak false.
     * <p>
     * "getr na proměnnou isPossibleToCreateReferenceToInstance".
     *
     * @return popsáno výše.
     */
    boolean isPossibleToCreateReferenceToInstance() {
        return isPossibleToCreateReferenceToInstance;
    }


    @Override
    public String getReferenceTextToClipboard() {
        return getTextOfClassForReference(clazz) + field.getName();
    }


    @Override
    public int compareTo(final Object o) {
        return finalTextForShow.compareToIgnoreCase(o.toString());
    }


    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;

        if (!(obj instanceof VariableInfo))
            return false;

        /*
         * Tato podmínka by měla v podstatě stačit. Jedná se o zobrazený text v okně dialogu, který by měl být vždy
         * unikátní pro každou metodu. Tedy žádná metoda by neměla být zobrazena vícekrát.
         */
        if (!finalTextForShow.equals(((VariableInfo) obj).finalTextForShow))
            return false;

        if (clazz != ((VariableInfo) obj).clazz)
            return false;

        if (!objValue.equals(((VariableInfo) obj).objValue))
            return false;

        if (!field.equals((((VariableInfo) obj).field)))
            return false;

        if (!instancesList.equals((((VariableInfo) obj).instancesList)))
            return false;

        return isPossibleToCreateReferenceToInstance == ((VariableInfo) obj).isPossibleToCreateReferenceToInstance;
    }


    @Override
    public int hashCode() {
        // Zdroj: https://www.mkyong.com/java/java-how-to-overrides-equals-and-hashcode/

        final int prime = 31;

        int result = 17;

        result = prime * result + (finalTextForShow == null ? 0 : finalTextForShow.hashCode());
        result = prime * result + (clazz == null ? 0 : clazz.hashCode());
        result = prime * result + (objValue == null ? 0 : objValue.hashCode());
        result = prime * result + (field == null ? 0 : field.hashCode());
        result = prime * result + (instancesList == null ? 0 : instancesList.hashCode());
        result = prime * result + Boolean.valueOf(isPossibleToCreateReferenceToInstance).hashCode();

        return result;
    }
}
