package cz.uhk.fim.fimj.instance_diagram;

import java.awt.Color;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.SwingUtilities;

import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;

import cz.uhk.fim.fimj.instances.Instances;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.swing_worker.SwingWorkerInterface;

/**
 * Tato třída slouží jako vlákno, které na určitý čas zvýrazní příslušnou buňku reprezentující instanci v diagramu
 * instancí.
 * <p>
 * Jde o to, že když například uživatel zavolá metodu, která vrátí instancí nějaké třídy z diagramu tříd, která se
 * nachází v diagramu instancí, tak je vhodné ji "zvýraznit", aby uživatel věděl, o jakou instanci se jedná (pro
 * přehlednost).
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class HighlightAnInstanceThread extends Thread implements SwingWorkerInterface {

	/**
	 * Čas, po jakou dobu se má ponechat zvýrazněná buňka / instace v diagramu
	 * instancí.
	 */
	private final double time;
	
	
	/**
	 * Objekt, resp. hodnota vrácená například z metody nebo získaná z nějaké
	 * proměnné, o které se má zjistit, zda se jedná o instanci nějaké třídy z
	 * diagramu tříd, která se aktuálně nachází v diagramu instancí a pokud ano, pak
	 * se příslušná buňka / instance v diagramu instnací zvýrazní.
	 * 
	 * Dále pokud se jedná o pole nebo list, tak se celá struktura projde a pro
	 * každou položku se zjistí, zda se jedná o instanci v diagramu instancí, pokud
	 * ano, pak se zvýrazní.
	 * 
	 * 
	 * Note:
	 * Místo této položky bych si zde mohl nechat načíst přímo list, který by se
	 * mohl získat z metody 'getReturnValueWithReferenceVariableForPrint' ve třídě
	 * OperationsWithInstances, ale rozhodl jsem se sem předat také tento objekt, a
	 * toto vlákno už si zjistí, co potřebuji pro potenciální zvýraznění instance,
	 * protože jsem chtěl, aby to bylo trochu rychlejší, kdybych upravil výše
	 * uvedenou metodu, aby vracela například objekt, který bude obsahovat jednu
	 * proměnnou, kterou vrací teď (pro výpis), a druhou proměnnou by byl ten list,
	 * tak to je možnost, ale celkově by to bylo trochu pomalejší, takto si může
	 * toto vlákno běžet mimo to EDT - co aplikace a nebude tak "brzdit" uživatele.
	 */
	private final Object objReturnedValue;
	
	
	
	/**
	 * Reference na diagram instancí.
	 */
	private final GraphInstance instancesDiagram;
	
	
	
	/**
	 * Objekt Properties, který obsahuje zkopírované vlastnosti pro diagram
	 * instancí, akorát obsahuje změněné hodnoty pro barvu ohraničení a barvu pozadí
	 * (potenciálně - záleží natom, co uživatel vše nastavil, jako že se má změnit).
	 * 
	 * Tento objekt se předá příslušné buňce / buňkám pro nastavení designu.
	 */
	private final Properties instancesHighlightProp;
	
	
	/**
	 * Časovač, který bude "zvýrazňovat příslušnou / příslušné buňky - instance v
	 * diagramu instnací. Nejprve je zvýrazní, pak nějaký čas počká, pak je vrátí do
	 * "normální" / výchozí barvy.
	 */
	private final Timer timer;
	
	
	/**
	 * Logická proměnná, která značí, zda se příslušné buňky / instance již
	 * zvýraznily nebo ne, pokud ne, tak se zvýrazní, a až se zvýrazní, tak se při
	 * dalším průchodu metodu v timeru zjistí, že jsou zvýrazněné, tak se vrátí do
	 * "původního" designu a ukončí se timer a vlákno.
	 */
	private boolean isHighlighted = false;
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param time
	 *            - čas, která se má čekat, ale v sekundách, například 1.1 = 1
	 *            sekunda a 1 destina.
	 * 
	 * @param textColor
	 *            - barvá písma reference, které se má nastavit zvýrazněné buňce /
	 *            instanci.
	 * 
	 * @param attributeColor
	 *            - barva písma, které se má nastavit pro atributy ve zvýrazněné
	 *            buňce / instanci.
	 * 
	 * @param methodColor
	 *            - barva písma, které se má nastavit pro metody vezvýrazněné buňce
	 *            / instanci.
	 * 
	 * @param borderColor
	 *            - barva ohraničení, která se má nastavit zvýrazněné buňce /
	 *            instanci.
	 * 
	 * @param backgroundColor
	 *            - barva pozadí, která se má nastavit zvýrazněné buňce / instanci.
	 * 
	 * @param objReturnedValue
	 *            - vraceny objekt z metody a z nej se stejne jako u tech operaci
	 *            ziskaji instnace a z tech bunky v ID.
	 * 
	 * @param instancesDiagram
	 *            - referencena diagram instancí.
	 */
	HighlightAnInstanceThread(final double time, final Color textColor, final Color attributeColor,
			final Color methodColor, final Color borderColor, final Color backgroundColor,
			final Object objReturnedValue, final GraphInstance instancesDiagram) {

		super();

		this.time = time;
		this.objReturnedValue = objReturnedValue;

		this.instancesDiagram = instancesDiagram;

		/*
		 * Zde je třeba vytvořit nový objekt Properties, do kterého se zkopíruje veškeré
		 * nastavení pro diagram instancí, pouze se změní nastavení pro barvu pozadí a
		 * ohraničení buňky reprezentující instance v diagramu instancí a tyto upravené
		 * hodnoty budou značit zvýraznění.
		 * 
		 * Tuto kopii objektu Properties je třeba vytvořit, protože jinak bych si měnil
		 * přímo ty hodnoty v aktuálním nastavení, tak bych si musel pamatovat ty
		 * původní hodnoty, ale je to jedno, tento způsob mi přišel přehlednější.
		 * 
		 * 
		 * Note:
		 * Pro zvýraznění bych zde ještě mohl doplnit například velikost písma, úhel
		 * zaoblení hran nebo i styl písma nebo jeho zarovnání, ale řekl bych, že z mého
		 * pohledu by stačilo už jen brva pozadí a ohraničení. Tak u nechám pro některé
		 * uživatele třeba i tu barvu písma a to by mohlo stačit, doplnit se to dá
		 * vždycky.
		 */

		// Nová instance:
		instancesHighlightProp = new Properties();
		// Zkopírování vlastností:
		instancesHighlightProp.putAll(instancesDiagram.getDefaultProperties());

		// Nastavení hodnot pro zvýraznění:
		instancesHighlightProp.setProperty("TextColor", Integer.toString(textColor.getRGB()));
		instancesHighlightProp.setProperty("AttributesTextColor", Integer.toString(attributeColor.getRGB()));
		instancesHighlightProp.setProperty("MethodsTextColor", Integer.toString(methodColor.getRGB()));
		instancesHighlightProp.setProperty("BorderColor", Integer.toString(borderColor.getRGB()));
		instancesHighlightProp.setProperty("CellColor", Integer.toString(backgroundColor.getRGB()));

		
		// Inicializace timeru:
		timer = new Timer();
	}
	
	

	
	



	
	
	
	
	@Override
	public void run() {
		try {
			SwingUtilities.invokeAndWait(() -> {
				/*
				 * Získám si list, který obsahuje buňky (instance), které se mají zvýraznit.
				 */
				final List<DefaultGraphCell> instancesList = getCellsListByInstances(objReturnedValue,
						new ArrayList<>());

				/*
				 * Přiřadím timeru úlohu tak, aby se ihned spustila, aby nečekala ten stanovaný
				 * čas a rovnou ji spustím.
				 */
				timer.scheduleAtFixedRate(new TimerTask() {

					@Override
					public void run() {
						/*
						 * V případě, že jsou buňky / instance již zvýrazněné, tak je vrátím do
						 * původního nastavení:
						 */
						if (isHighlighted) {
							// Vrátím buňkám / instancím původní vzhled:
							changeInstanceCellsDesign(instancesList, instancesDiagram.getBackground(),
									instancesDiagram.getDefaultProperties());

							/*
							 * Zde stačí ukončit timer a (pro jistotu) i vlákno, ale to již není nezbytné.
							 */
							timer.cancel();
							interrupt();
						}

						else {
							/*
							 * Zde se jedná o úplně první průchod timer (této metody), tak je teprve potřeba
							 * zvýraznit příslušné buňky / instance a nastavit proměnnou isHighlighted na
							 * true, protože jsou buňky již zvýrazněné, tak aby se při dalším průchodu timer
							 * vrátili do původního nastavení / vzhledu.
							 */
							changeInstanceCellsDesign(instancesList, instancesDiagram.getBackground(),
									instancesHighlightProp);

							isHighlighted = true;
						}


						/*
						 * Následující aktualizaci diagramu instancí musí být také zabalena v bloku
						 * 'SwingUtilities.invokeAndWait(new Runnable()...', protože, když jsem tu
						 * aktualizaci diagramu instancí nechal uvedeného bloku, tak při opakovaném
						 * zvýrazňování natala občas nějaká divná chyba, u které ani nebylo možné
						 * dohledat přičinu, opět někde v knihovně JGraph, ale na aplikaci to opět
						 * nemělo žádný vliv, pouze vypadla výjimka ale aplikace běžela a fungovala
						 * dále, tedy dále se zvýrazńovaly instance tak, jak měly.
						 *
						 * K tomu aby ta výjimka vypadla jsem musel opakovaně například volat metodu,
						 * která vrací nějakou instanci, nebo si jí brát z pole, aby se vždy ta instance
						 * musela zvýraznit. Hlavně mi přišělo, že ty výjimka vypadla, když se mi
						 * podařilo třeba vzít tu instanci z pole, zrovna, když se to zvýraznění
						 * instance jakoby vracelo do původního stavu.
						 */
						try {
                            SwingUtilities.invokeAndWait(() ->
                                    // Aby se změny projevily, tak je třeba aktualizovat diagram instnací:
                                    instancesDiagram.updateDiagram(GraphInstance.getCellsList()));

						} catch (InvocationTargetException e) {
							/*
							 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
							 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
							 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
							 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
							 */
							final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

							// Otestuji, zda se podařilo načíst logger:
							if (logger != null) {
								// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

								// Nejprve mou informaci o chybě:
								logger.log(Level.INFO,
										"Zachycena výjimka při pokusu o zavolání SwingUtilities.invokeAndWait ve " +
												"vlákně pro zvýraznění buněk reprezentující instanci v diagramu " +
												"instancí"
												+ "za účelem aktualizace diagramu instanci po zvýraznění nebo vrácení " +
												"vzhledu instancí do původního stavu."
												+ "Tato chyba může nastat například v případě, že je vyhozena výjimka " +
												"pri pokusu o spuštění programu doRun.");

								/*
								 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
								 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
								 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
								 */
								logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
							}
							/*
							 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
							 */
							ExceptionLogger.closeFileHandler();

						} catch (InterruptedException e) {
                            /*
                             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                             */
                            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                            // Otestuji, zda se podařilo načíst logger:
                            if (logger != null) {
                                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                                // Nejprve mou informaci o chybě:
                                logger.log(Level.INFO,
                                        "Zachycena výjimka při pokusu o zavolání SwingUtilities.invokeAndWait ve " +
                                                "vlákně pro zvýraznění buněk reprezentující instanci v diagramu " +
                                                "instancí"
                                                + "za účelem aktualizace diagramu instanci po zvýraznění nebo vrácení" +
                                                " vzhledu instancí do původního stavu."
                                                + "Tato chyba může nastat například v případě, že pokud budeme " +
                                                "přerušeni při čekaní na událost dispečování vlákna k dokončení " +
                                                "spuštění příkazu doRun.run ().");

                                /*
                                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                                 */
                                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                            }
                            /*
                             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                             */
                            ExceptionLogger.closeFileHandler();
						}
					}
				}, 0, Math.round((time * 1000)));
			});

		} catch (InvocationTargetException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při pokusu o zavolání SwingUtilities.invokeAndWait ve vlákně pro " +
                                "zvýraznění buněk reprezentující instanci v diagramu instancí"
                                + "za účelem 'ukázání' uživateli o jakou instanci se jedná, toto se provede například" +
                                " po zavolání metody nebo získání instance z proměnné apod."
                                + "Tato chyba může nastat například v případě, že je vyhozena výjimka pri pokusu o " +
                                "spuštění programu doRun.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
			ExceptionLogger.closeFileHandler();

        } catch (InterruptedException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při pokusu o zavolání SwingUtilities.invokeAndWait ve vlákně pro " +
                                "zvýraznění buněk reprezentující instanci v diagramu instancí"
                                + "za účelem 'ukázání' uživateli o jakou instanci se jedná, toto se provede například" +
                                " po zavolání metody nebo získání instance z proměnné apod."
                                + "Tato chyba může nastat například v případě, že pokud budeme přerušeni při čekaní " +
                                "na událost dispečování vlákna k dokončení spuštění příkazu doRun.run ().");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
		}
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro nastavení buňkám / instancím v instancesList barvu
	 * pozadí a ohraničení, která se nachází v objektu Properties v properties
	 * parametru.
	 * 
	 * @param instancesList
	 *            - list, který obsahuje buňky / instance z diagramu instancí,
	 *            kterým se má nastavit barva pozadí a ohraničení v objektu
	 *            properties.
	 * 
	 * @param diagramBackgroundColor
	 *            - barva pozadí diagramu instancí.
	 * 
	 * @param properties
	 *            - objekt Properties, který obsahuje nastavení pro diagram
	 *            instancí, v tomto případě jsou nejdůležitější a potřebné pouze
	 *            nastavení pro buňku reprezentující instanci v daigramu instancí.
	 */
	private static void changeInstanceCellsDesign(final List<DefaultGraphCell> instancesList,
			final Color diagramBackgroundColor, final Properties properties) {
		instancesList.forEach(c -> {
			final Map<?, ?> attributes = c.getAttributes();

			GraphConstants.setBorder(attributes,
					BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(diagramBackgroundColor, 1, true),
							new RoundedBorderLine(c.toString(), ((InstanceCell) c).getVariablesCount(), properties)));
		});
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí pro objekt objValue, zda je to null, pole, list nebo
	 * "klasická" proměnná a dle toho vrátí list, který bude obsahovat buňky, které
	 * v diagramu instancí reprezentují instance, které se mají zvýraznit.
	 * 
	 * @param objValue
	 *            - hodnota, která se získala z proměnné nebo návratová hodnota z
	 *            metody apod. Pro tuto metodu je to hodnota, u které se má zjistit,
	 *            zda je to instance nebo datový struktura, která obsahuje instance,
	 *            které se nachází v diagramu instancí.
	 * 
	 * @param instancesList
	 *            - list, do kterého se vloží nalezené instance (buňky) v diagramu
	 *            instancí pro zvýraznění.
	 * 
	 * @return list, který bude obsahovat buňky / instance, které se vyskytují v
	 *         diagramu instnací a byly získány z objektu objValue.
	 */
	private static List<DefaultGraphCell> getCellsListByInstances(final Object objValue,
			final List<DefaultGraphCell> instancesList) {
		
		if (objValue ==  null)
			return instancesList;
		
		
		// Projdu celé pole, případně veškeré dimeze:
		else if (objValue.getClass().isArray())
			return checkInstances(objValue, instancesList);
		
		
		
		// Přetypu si list a projdu jej:
		else if (ReflectionHelper.isDataTypeOfList(objValue.getClass())) {
			/*
			 * Přetypuji si získaný list na list objektů:
			 */
			@SuppressWarnings("unchecked")
			final List<Object> valuesList = (List<Object>) objValue;

            for (final Object o : valuesList) {
                /*
                 * Získám si aktuálně iterovaný objekt:
                 */
                /*
                 * Pokud je to null, tak prostě přidám null hodnotu do výsledného listu a
                 * pokračuji další iterací, nemá smysl testovat reference.
                 */
                if (o == null)
                    continue;

                /*
                 * Zjistím, zda se jedná o instanci nějaké třídy z diagramu tříd a ta instance
                 * je reprezentována v diagramu instancí, takže pokud se mi naplní proměnná
                 * reference, pak ji přidám za ten objekt, resp. hodnotu v listu.
                 */
                final String reference = Instances.getReferenceToInstanceFromCdInMap(o);

                if (reference != null) {
                    /*
                     * Získám si buňku, která v diagramu tříd reprezentuje instanci příslušné třídy,
                     * na které ukazuje reference reference.
                     *
                     * Note:
                     * Ta podmínka by něměla být třeba, výše už vím, že ta instance existuje, tak by
                     * měla být i v diagramu instnací, ale kdyby náhodou, tak aby nedošlo k nějaké
                     * chybě.
                     */
                    final DefaultGraphCell cell = GraphInstance.getCellWithText(reference);

                    if (cell != null && !instancesList.contains(cell))// null - Nemělo by nastat
                        instancesList.add(cell);
                }

                // else - Zde se nejedná o instanci z diagramu instnací
            }
		}
		
		
		
		else {
			// Klasická hodnota
			final String reference = Instances.getReferenceToInstanceFromCdInMap(objValue);

			if (reference != null) {
				final DefaultGraphCell cell = GraphInstance.getCellWithText(reference);

				if (cell != null && !instancesList.contains(cell))// null by nemělo nastat
					instancesList.add(cell);
			}
		}
		
		
		
		return instancesList;
	}
	
	
	
	
	
	
	
	

	
	
	/**
	 * Metoda, která projde celé pole objArray, případě všechny jeho dimeze a pro
	 * každou položku bude zjšťovat, zda se jedná o instanci třídy z diagramu tříd,
	 * která se aktuálně nachází v diagramu instancí.
	 * 
	 * Pokud ano, pak tuto buňku vloži do listu instancesList, který bude na konec
	 * obsahovat veškeré buňky (instance) v diagramu instancí, které se mají
	 * zvýraznit.
	 * 
	 * @param objArray
	 *            - X - rozměrné pole.
	 * 
	 * @param instancesList
	 *            - list, do kterého se vloží veškeré nalezené instanc v objArray,
	 *            které se nachází i v diagramu instancí.
	 * 
	 * @return list instancesList, který bude obsahovat veškeré buňky (instance),
	 *         které byly nalezeny v poli objArray a vyskytují se i v diagramu
	 *         instancí.
	 */
	private static List<DefaultGraphCell> checkInstances(final Object objArray,
			final List<DefaultGraphCell> instancesList) {
		/*
		 * Získám si délku pole.
		 */
		final int length = Array.getLength(objArray);
		
		
		// proiteruji celé pole:
		for (int i = 0; i < length; i++) {
			/*
			 * Získám si objekt / položku v poli na aktuálně iterovaném indexu - pozici v
			 * poli.
			 */
			final Object objValue = Array.get(objArray, i);
			
			/*
			 * pokud je to null hodnota, pak nemá smysl pokračovat.
			 * 
			 * Zde by nemělo smysl dále pokračovat, protože je jasné, že to instance nebude.
			 */
			if (objValue == null)
				continue;
				
			
			
			/*
			 * Pokud se jedná o další pole, resp. další dimezi pole objArray, tak pokračuji
			 * rekurzí.
			 */
			else if (objValue.getClass().isArray())
				checkInstances(objValue, instancesList);
				
			
			
			else {
				/*
				 * Zde se jedná o "nějakou" položku v poli, tj. není to null hodnota ani další
				 * pole / dimeze, tak otestuji, zda se nejedná o instanci, která se nachází v
				 * diagramu instancí, pokud ano, tak si dle referenci najdu buňku v diagramu
				 * instancí, která tu instanci reprezentuje a přidám ji do listu pro zvýraznění.
				 */
				final String reference = Instances.getReferenceToInstanceFromCdInMap(objValue);

				/*
				 * Otestuji, zda byla získána reference, pokud ano, je to instnace z diagramu
				 * instnací, tak ji na konec přidám, pokud bude null, tak to není instnace,
				 * která se nachází v diagramu instnací.
				 */
				if (reference != null) {
					/*
					 * Získám si buňku, která v diagramu instancí reprezentuje instanci příslušné
					 * třídy, na které ukazuje reference reference.
					 * 
					 * Note:
					 * Ta podmínka by něměla být třeba, výše už vím, že ta instance existuje, tak by
					 * měla být i v diagramu instnací, ale kdyby náhodou, tak aby nedošlo k nějaké
					 * chybě.
					 */
					final DefaultGraphCell cell = GraphInstance.getCellWithText(reference);
					
					if (cell != null && !instancesList.contains(cell))// null - Nemělo by nastat
						instancesList.add(cell);
				}
			}
		}

		return instancesList;
	}
}
