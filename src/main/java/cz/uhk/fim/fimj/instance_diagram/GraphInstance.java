package cz.uhk.fim.fimj.instance_diagram;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;

import org.jgraph.graph.DefaultCellViewFactory;
import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.DefaultGraphModel;
import org.jgraph.graph.GraphConstants;
import org.jgraph.graph.GraphLayoutCache;
import org.jgraph.graph.GraphModel;
import org.jgraph.util.ParallelEdgeRouter;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.class_diagram.GraphAbstract;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.font_size_form.CheckDuplicatesInDiagrams;
import cz.uhk.fim.fimj.font_size_form.FontSizePanelAbstract;
import cz.uhk.fim.fimj.instance_diagram.overview_of_instance.VariablesInstanceOfClassForm;
import cz.uhk.fim.fimj.instances.Instances;
import cz.uhk.fim.fimj.instances.RefreshInstances;
import cz.uhk.fim.fimj.output_editor.OutputEditor;

/**
 * Tato třída slouží jako diagram instancí (v hlavním okně aplikace na "pravo"). V tomto diagramu instancí jsou, jak již
 * plyne z návu, zobrazovány - reprezentovány instance tříd z diagramu tříd, lze nad nimi zavolat jejich metody s
 * nějakými parametry, či prohlížet jejich stav proměmých, - přehled proměnných, metod, konstruktorů, ... Dále jsou v
 * tomto diagramu zobrazovány příslušné vztahy coby agregace a asociace mezi jednotlivými instancemi, jakou jsou
 * například naplněné proměnné v jední instanci třídy jinou instnací třídy, pak je to buď jeden z druhgů agregace (1:1,
 * 1:N) a nebo asociace.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class GraphInstance extends GraphAbstract implements GraphInstanceInterface, LanguageInterface {
	
	private static final long serialVersionUID = 1L;

	
	
	/**
	 * "Komponenta, která obsahuje metody, které slouží pro změnu velkosti písma
	 * objektům v diagramu instancí.
	 */
	private static final FontSizeOperation_ID_Interface FONT_SIZE_OPERATION = new FontSizeOperation_ID();
	
	
	
	
	
	

	/**
	 * Proměnná, která vždy obsahuje aktuální velikost písma / fontu pro buňku,
	 * která reprezentuje v instanci diagramu instancí.
	 * 
	 * Tato proměnná je zde potřeba, protože uživatel má možnost si pomocí
	 * klávesových zkratek Ctrl + nebo - nastavit velikost písma v objektech v
	 * diagramu instancí, tak v této proměné vždy bude velikost písma v instanci.
	 * 
	 * Je celkem jedno, která hodnota by zde byla, zda hodnota pro velikost písma
	 * pro text hrany apod. Velikost písma v instanci jsem vybral, protože je to
	 * taková nevíce vyskytující se část v diagramu instancí, taková, na kterou se
	 * každý nejvíce dívá.
	 */
	private static int instanceFontSize;
	
	
	
	/**
	 * Kolekce, do které budu vkládat buňky, které budou reprezentovat instanci
	 * třídy a hrany:
	 */
	private static final List<DefaultGraphCell> CELLS_LIST = new ArrayList<>();
	
	
	/**
	 * V podstatě model grafu - do něj se předávaí komponenty pro zobrazení v grafu:
	 */
	private static final GraphModel MODEL = new DefaultGraphModel();
	
	
	/**
	 * View, které zobrazí model objektu do grafu:
	 */
	private static final GraphLayoutCache VIEW = new GraphLayoutCache(MODEL, new DefaultCellViewFactory());	
	
	
	
	/**
	 * Třída, pomocí které zjistím o jakou hranu se jedná v diagramu instancí
	 */
	public static final KindOfInstanceEdgeInterface KIND_OF_EDGE = new KindOfInstanceEdge();
	

	
	
	/**
	 * Logická proměnné, která značí, zda se mají v diagramu instancí zobrazit i
	 * vztahy v proměnných, které se nachází v předcích příslušné instance v
	 * diagramu tříd.
	 * 
	 * Tzn. že pokud bude tato proměnná true a když v diagramu instancí bude nějaká
	 * instance, která má nějakého předka, který je nějaká třída v diagramu trid
	 * (znázorněna vdiagramu tříd), pak se načtou i proměnné z tohoto předka a
	 * zobrazí se vztahy z příslušné instance.
	 * 
	 * Pokud bude tato proměnná false, pak se budou brát pouze vztahy z té jedné
	 * instance v diagramu instancí.
	 */
	private static boolean showRelationshipsInInheritedClasses;
	
	
	
	
	
	/**
	 * Logická proměnná, která značí, zda se mají například v dialogu s přehledem
	 * hodnot o zvolené instanci zobrazit u naplněných proměnných instancemi v
	 * diagramu instnací i jejich reference. Dále pak u buňky reprezentující
	 * instanci, pokud je povolené zobrazení atributů.
	 */
	private static boolean showReferenceVariables;
	
	
	
	
	
	
	/**
	 * logikcá proměnná, která značí, zda se má využít "rychlý" způsob pro zjištění
	 * vztahů mezi mezi instancemi v diagramu instancí.
	 * 
	 * Hodnota true značí, že ano, v tomto případě, je postup takový, že se vymažou
	 * veškeré vztahy instancemi v diagramu instancí a pak se zjistí, jaké hrany
	 * vztahy existují dle hodnot proměnných v instanci a dle toho se vytvoří
	 * příslušné vztahy v diagramu instancí.
	 * 
	 * Hodnota false značí, že se nebudou mazat všechny hrany mezi instancemi, ale
	 * vždy se zjistí vztahy, které mají existovat mezi instancemi v diagramu
	 * instancí. Pak se zjistí, jaké vztahy opravdu existují v diagramu instancí
	 * mezi instancemi a dle toho, jaké tam být mají se ty aktuální vztahy buď
	 * ponechají, přidají nebo odeberou.
	 * 
	 * Mohl by zde být například výčet pro více typů, ale původně zde byla jen jedna
	 * možnost, tuto další jsem přidal pouze pro to, že mě to napadlo kvůli opravě
	 * jedné chyby v knihovně JGraph a další možnosti ani neplánuji, stejně je
	 * nejlepší využít tu nejrychlejší vyriantu (hodnota true), ostatní varianty mi
	 * nepřidají moc zajímavé, když jsou pomalejší, navíc je to jen proto, že se mi
	 * nechtělo smazat vše co jsem napsal, tak jsem dal na výběr mezi těmito
	 * možnostmi.
	 */
	private static boolean useFastWay;
	
	
	
	
	
	
	
	
	
	/**
	 * Logická proměnná, která značí, zda se budou zvýrazňovat instance v diagramu
	 * instancí v případě, uživatel například zvolá metodu nebo získá příslušnou
	 * instanci z proměnné apod. Tak pokud to bude instance z diagramu instancí, tak
	 * se zvýrazní (nebo ne). true - mají se zvýrazňovat instance v diagramu
	 * instancí, false - nemají se zvýrazňovat instance v diagramu instancí.
	 */
	private static boolean highlightInstances;
	
	/**
	 * Proměnná, která obsahuje čas ve vteřinách,který značí, jak dlouho bude
	 * příslušná buňka / instance v diagramu instancí zvýrazněna.
	 */
	private static double highlightingTime;
	
	/**
	 * Proměnná, která značí barvu písma, kterou bude mít buňka reprezentující
	 * instanci v diagramu instancí v případě, že bude zvýrazěná.
	 */
	private static Color textColorOfHighlightedInstance;
	
	/**
	 * Proměnná, která značí barvu písma pro atributy ve ve zvýrazněné instanci v
	 * diagramu instancí.
	 */
	private static Color attributeColorOfHighlightedInstance;

	/**
	 * Proměnná, která značí barvu písma pro metody ve zvýrazněné instanci v
	 * diagramu instancí.
	 */
	private static Color methodColorOfHighlightedInstance;

	/**
	 * Proměnná, která značí barvu ohnraničení, kterou bude mít buňka reprezentující
	 * instanci v diagramu instancí v případě, že bude zvýrazěná.
	 */
	private static Color borderColorOfHighlightedInstance;
	
	/**
	 * Proměnná, která značí barvu pozadí, které bude mít buňka reprezentující
	 * instanci v diagramu instancí v případě, že bude zvýrazěná.
	 */
	private static Color backgroundColorOfHighlightedInstance;
	
	
	
	
	
	
	
	
	
	/**
	 * Proměnná, která značí, zda se mají v jednotlivých buňkách reprezentujcí
	 * instance v diagramu instancí zobrazovat několik prvních nalezených atributů /
	 * proměnných v příslušné instanci.
	 */
	private static boolean showAttributesInInstanceCell;

	/**
	 * Proměnná, která značí, zda se mají v jednotlivých buňkách reprezentující
	 * instance v diagramu instancí zobrazovat několik prvních nalezených metod v
	 * příslušné instanci (třídě).
	 */
	private static boolean showMethodsInInstanceCell;

	/**
	 * Proměnná, která značí, zda se mají zobrazit všechny atributy / proměnné v
	 * buňce reprezentující instanci třídy v diagramu instancí nebo zda se má
	 * zobrazit pouze zvolený počet proměnných v příslušné instanci.
	 */
	private static boolean showAllVariables;

	/**
	 * Počet atributů / proměnných, který se má zobrazit v instanci v diagramu
	 * intancí, toto platí pouze v případě, že se nemají zobrazit všechny proměnné v
	 * příslušné instanci.
	 */
	private static int countOFVariablesForShowInInstance;

	/**
	 * Proměnná, která značí, zda se mají zobrazit v buňce reprezentující instanci v
	 * diagramu instancí všechny metody z příslušné instance, kterou ta buňka
	 * reprezentuje nebo jen zvolený počet.
	 */
	private static boolean showAllMethods;

	/**
	 * Proměnná, která značí počet metod, který se má zobrazit v buňce
	 * reprezentující instanci v diagramu instancí. Tento počet se využije pouze v
	 * případě, že se nemají zobrazit všechny metody v příslušné instanci.
	 */
	private static int countOfMethodsForShowInInstance;
	
	
	/**
	 * "Zda se mají v dialogu pro zavolání nestatického setru na proměnnou typu
	 * třídy sebe sama zobrazovat i getry, které vrací hodnotu typu třídy sebe
	 * sama".
	 * 
	 * Toto lze aplikovat pouze v případě, že bude povoleno zavoolání metody pro
	 * předání návratové hodnoty do příslušného parametru metody.
	 * 
	 * Logická proměnnná, která značí, zda se mají v dialogu pro zavolání metody nad
	 * instancí v diagramu instancí, a ta metoda není statická, tak pokud je ta
	 * zavolaná metoda setr na proměnnou typu té třídy, ve které se nachází, tak zda
	 * se mají zobrazit do parametru toho setru v nabídce i getry, které vrací typ
	 * té třídy, ve které se obě ty metody nachází.
	 */
	private static boolean showGetrInSetrMethodToInstance;
	
	
	
	
	
	

	/**
	 * Proměnná, která obsahuje logickou hodnotu, která značí, zda se mají v buňce,
	 * která v diagramu instancí reprezentuje instanci zobrazovat název třídy i s
	 * balíčky, ve kterých se ta třída nachází (od adresáře src - bez něj), nebo zda
	 * se v té buňce má zobrazit pouze název třídy (bez balíčků).
	 */
	private static boolean showPackages;
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param outputEditor
	 *            - reference na instanci editoru výstupů pro vypisování chybových
	 *            hlášení ohledně nějaké chyby.
	 */
	public GraphInstance(final OutputEditor outputEditor) {
		super(MODEL, VIEW);
		
		this.outputEditor = outputEditor;
		
		// Výchozí nastavení grafu:
		chartSettings();
		
		// Načtení výchozího nastavení pro graf:
		setInstanceDiagramProperties();
		
				
		addMouseListener();
		
		
		addMouseWheelListener();
		
		
		addMyKeyListener();
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá do tohoto diagramu instancí posluchač na stisknutí
	 * klávesy Del, která odebere příslušný objekt z diagramu instancí, resp.
	 * odebere příslušnou instanci (pokud bude označena), hranu nelze odebrat,
	 * prtože reprezentuje nějaký vztah mezi instancemi dle stavu těch instancí,
	 * pouze instanci lze odebrat.
	 */
	private void addMyKeyListener() {
		addKeyListener(new KeyAdapter() {			
			
			@Override
			public void keyPressed(KeyEvent e) {
				/*
				 * Proměnná, do které si uložím označenou instanci, resp. označenou buňku v
				 * diagramu instancí, pokud nebude označeno nic nebo nebude označena buňka
				 * (instance), ale nějaká hrana, pak bude tato proměnná null.
				 */
				final DefaultGraphCell selectedCell;
				
				// Bezmu si označený objekt v diagramu:
				final Object selectedObject = getSelectionCell();				
				
				if (selectedObject != null && !(selectedObject instanceof DefaultEdge))
					// Zde se jedná o označenou buňku, která reprezentuje instanci nějaké třídy
					selectedCell = (DefaultGraphCell) selectedObject;
				else
					selectedCell = null;
				
				
				
				/*
				 * Po stisknutí klávesy Enter by se měl zobrazit dialog s přehledemo dané
				 * instnaci - přehled proměnných, metod, ...
				 */
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					showInfoAboutInstance(selectedCell);
				
				
				
				/*
				 * Po stisknutí klávesy Delete se vymaže označená instance.
				 */
				else if (e.getKeyCode() == KeyEvent.VK_DELETE) {
					if (selectedCell != null)
						removeInstance(selectedCell);
				}
				
				
				
				// Kombinace Ctrl a + (plus) pro zvětšení písma.
				// Toto nefungovalo ???
//				else if (e.isControlDown() && e.getKeyChar() == KeyEvent.VK_PLUS)
//				else if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_PLUS)
				else if (e.isControlDown() && e.getKeyChar() == '+') {
					if (instanceFontSize < Constants.FONT_SIZE_MAX) {
						FONT_SIZE_OPERATION.quickSetupOfFontSize(++instanceFontSize, GraphInstance.this);

						/*
						 * Výše uživatel nastavil nějakou velikosti písma, ale zde ještě potřebuji
						 * otestovat, zda náhodou nemají hrany pro reprezentaci vztahu typ asociace a
						 * agregace v diagramu instnací identické nastavení, protože pokud ano, pak by
						 * nebylo možné je od sebe rozeznat, tak na to upozorním uživatele a nastavím
						 * těm hranám "nouzové" výchozí velikosti písma, aby se alespoň trochu
						 * rozeznali. Mohu jim nastavit například výchozí hodnoty, ale v případě, že
						 * došlo k této chybě, že jsou stejné, tak uživatel upravil jejich nastavení,
						 * tak mu to nechci "zkazit" ta velikosti písma na hranách není až tak důležitá.
						 */
						FontSizePanelAbstract.checkDuplicateEdgesInId(GraphInstance.this, checkDuplicatesInDiagrams);
					}
				}


				// Kombinace Ctrl a - (mínus) pro změnšení písma.
				else if (e.isControlDown() && e.getKeyChar() == KeyEvent.VK_MINUS && instanceFontSize > Constants
						.FONT_SIZE_MIN) {
					FONT_SIZE_OPERATION.quickSetupOfFontSize(--instanceFontSize, GraphInstance.this);

					/*
					 * Výše uživatel nastavil nějakou velikosti písma, ale zde ještě potřebuji
					 * otestovat, zda náhodou nemají hrany pro reprezentaci vztahu typ asociace a
					 * agregace v diagramu instnací identické nastavení, protože pokud ano, pak by
					 * nebylo možné je od sebe rozeznat, tak na to upozorním uživatele a nastavím
					 * těm hranám "nouzové" výchozí velikosti písma, aby se alespoň trochu
					 * rozeznali. Mohu jim nastavit například výchozí hodnoty, ale v případě, že
					 * došlo k této chybě, že jsou stejné, tak uživatel upravil jejich nastavení,
					 * tak mu to nechci "zkazit" ta velikosti písma na hranách není až tak důležitá.
					 */
					FontSizePanelAbstract.checkDuplicateEdgesInId(GraphInstance.this, checkDuplicatesInDiagrams);
				}
			}
		});
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zobrazí informace o oznčené instancí v diagramu instancí, tj.
	 * Zobrazí se dialog, ve kterém bude přehled metoda, konstruktorů a proměnných
	 * příslušné instance.
	 * 
	 * @param selectedCell
	 *            - označená buňka / instance v diagramu instancí, jejíž informace
	 *            se mají zobrazit v příslušném dialogu.
	 */
	final void showInfoAboutInstance(final DefaultGraphCell selectedCell) {
		if (selectedCell instanceof InstanceCell) {// Tato podmínka by měla být zbytečná - pouze pro
														// jistotu.
			// Získám si zadanou referenci na instanci:
			final String referenceName = ((InstanceCell) selectedCell).getReferenceName();
			// Získám si samotnou intanci:
			final Object objInstance = Instances.getInstanceFromMap(referenceName);

			// Zde ten objekt - instanci třídy předám dále do dialogu, který si z ní vezme
			// co potřebuje
			final VariablesInstanceOfClassForm viocf = new VariablesInstanceOfClassForm(objInstance, referenceName,
					GraphInstance.this, outputEditor, showReferenceVariables);
			
			viocf.setLanguage(languageProperties);
			viocf.setVisible(true);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která grafu přidá reakce na pravé tlačítko myši - zobrazí se "menu",
	 * které bude obsahovat přehled metod dané třídy jejíž instanci daná buňka
	 * reperzentuje a které lze zavolat poklepaním na položku s danou metodou dané
	 * třídy
	 */
	private void addMouseListener() {
		addMouseListener(new MouseAdapter() {
			
			// Reakce na puštění tlačítka myši, jinak by byla při kliknutí na druhou instnaci třídy - buňku označena pořád 
			// ta první, podobně i po kliknutí do "prázdného" místa v grafu, pokud před tím byla označena buňka - instnace třídy,
			// proto reakce na uvolnění / puštění tlačítka myši
			
			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON3) {
					final DefaultGraphCell cell = (DefaultGraphCell) getSelectionCell();
					
					if (cell instanceof InstanceCell) {
						final PopupMenu popupMenu = new PopupMenu((InstanceCell) cell, outputEditor,
								GraphInstance.this, languageProperties, includePrivateMethods,
								includeProtectedMethods, includePackagePrivateMethods,
								makeVariablesAvailableForCallMethod);
						
						popupMenu.show(GraphInstance.this, e.getX(), e.getY());	
					}
				}
			}
			
			
			
			
			// Následuje posluchač pro dvojnásobné kliknutí na nějký objekt v diagramu
			// instancí:
			// Pokud uživatel klikne na nějakou buňku reprezentující instancí v diagramu
			// instancí,
			// tak se zobrazí informace o příslušné instanci - proměnné, metody, ...
			@Override
			public void mouseClicked(MouseEvent e) {
				// Otestuji, zda uživatel klikl dvakrát levým tlačítkem myši:
				if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON1) {
					final Object selectedObject = getSelectionCell();

					if (selectedObject != null && !(selectedObject instanceof DefaultEdge))
						// Zde se jedná o označenou buňku, která reprezentuje instanci nějaké třídy
						showInfoAboutInstance((DefaultGraphCell) selectedObject);
				}
			}
		});
	}
	
	

	
	
	
	
	
	
	
	
	@Override
	public void addInstanceCell(final String className, final String packageName, final String name) {
		/*
		 * Instance pro buňku, která se má vytvořit a reprezentovat tak nějakou instanci
		 * nějaké třídy z diagramu tříd.
		 */
		final InstanceCell instanceCell = new InstanceCell(className, name, packageName, showPackages,
				countOFVariablesForShowInInstance, showAttributesInInstanceCell, countOfMethodsForShowInInstance,
				showMethodsInInstanceCell, showAllVariables, showAllMethods);

		// Nastavím příslušné buňce texty, kdyby se vypisovaly proměnné a metody:
		instanceCell.setLanguage(languageProperties);

		/*
		 * Získám si příslušnou instanci z mapy, kde se nachází veškeré vytvořené
		 * instance.
		 * 
		 * Note:
		 * Zde by byla i možnost předat tu instanci, ale chci je mít na jednom místě a
		 * pak se na ně jen dotazovat (pro přehlednost), tak si ji zde "vytáhnu".
		 */
		final Object objInstance = Instances.getInstanceFromMap(name);

		/*
		 * Tato podmínka by měla být zbytečná, ale kdyby náhodou.
		 * 
		 * Zde potřebuji hned na začátku naplnit příslušné textové hodnoty, které
		 * obsahují texty s atributy a metodami, aby se v případě potřeby mohli vypsat.
		 */
		if (objInstance != null) {
			instanceCell.makeTextWithVariables(objInstance);
			instanceCell.makeTextWithMethods(objInstance);
		}
		
		
		
		
		
		
		
		
		// atributy buňky, které budu dále modifikovat:
		final Map<?, ?> attributes = instanceCell.getAttributes();
		
		
		// Obdélník pro graf:
		GraphConstants.setBounds(attributes, new Rectangle2D.Double(10, 10, 120, 100));		
		
		// Nyní nastavím zaoblené hrany pomocí obdélníků z graphics:
		GraphConstants.setBorder(attributes,
				BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(getBackground(), 1, true),
						new RoundedBorderLine(instanceCell.toString(),
								instanceCell.getVariablesCount(), defaultProperties)));
		
		// Aby nebyly vidět hranaté okraje nastavím jim stejnou barvu, jako je barva pozadí:
		GraphConstants.setGradientColor(attributes, getBackground());
		GraphConstants.setBackground(attributes, getBackground());
		
		
		// Nastavím neprůhlednost, tyde s tím zatěžovat nemusím, prostě nebudou průhledné
		// Zde k tomu ani není důvod, v tomto gafu budou akorát buňky, které reprezentují instance
		// tříd, a k ním nějaké hrany, jinak žádné komentáře, apod.
		GraphConstants.setOpaque(attributes, true);
		
		// Buňka nepůjde editovat
		GraphConstants.setEditable(attributes, false);				
		
		
		// Přidání portu, aby se mohla připojit hrana a dopočítavalo se její 
		// umístění na hraně buňky
		instanceCell.addPort();
		
		// Přidání instance do kolekce pro vykreslení:
		CELLS_LIST.add(instanceCell);
		
		updateDiagram(CELLS_LIST);
	}
	
	
	
	
	
	
	
	
	
	@Override
	public void addAssociationEdge(final Object srcCell, final Object desCell, final String srcSide,
			final String desSide) {
		
		if (srcCell instanceof DefaultGraphCell && desCell instanceof DefaultGraphCell) {
			if (CELLS_LIST.contains(srcCell) && CELLS_LIST.contains(desCell)) {
				final DefaultEdge edge = new DefaultEdge();
				
				edge.setSource(((DefaultGraphCell) srcCell).getChildAt(0));
				edge.setTarget(((DefaultGraphCell) desCell).getChildAt(0));
				
				
				
				// Design:
				final Map<?, ?> attributes = edge.getAttributes();
				
				
				if (defaultProperties != null) {
					// Barva hrany:			
					final String colorText = defaultProperties.getProperty("AssociationEdgeLineColor", Integer.toString(Constants.ID_ASSOCIATION_EDGE_COLOR.getRGB()));
					GraphConstants.setLineColor(attributes, new Color(Integer.parseInt(colorText)));
					
					// Popis hrany bude poděl čáry:
					GraphConstants.setLabelAlongEdge(attributes, Boolean.parseBoolean(defaultProperties.getProperty("AssociationEdgeLabelsAlongEdge", String.valueOf(Constants.ID_ASSOCIATION_LABELS_ALONG_EDGE))));
					
					// konec hrany:
					GraphConstants.setLineEnd(attributes, Integer.parseInt(defaultProperties.getProperty("AssociationEdgeLineEnd", Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_END))));
								
					// začátek hrany:
					GraphConstants.setLineBegin(attributes, Integer.parseInt(defaultProperties.getProperty("AssociationEdgeLineBegin", Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_BEGIN))));
					
					// Šířka hrany:
					GraphConstants.setLineWidth(attributes, Float.parseFloat(defaultProperties.getProperty("AssociationEdgeLineWidth", String.valueOf(Constants.ID_ASSOCIATION_EDGE_LINE_WIDTH))));
					
					// Výplň na konci hrany:
					GraphConstants.setEndFill(attributes, Boolean.parseBoolean(defaultProperties.getProperty("AssociationEdgeLineEndFill", String.valueOf(Constants.ID_ASSOCIATION_EDGE_LINE_END_FILL))));
					
					// Výplň na začátku hrany:
					GraphConstants.setBeginFill(attributes, Boolean.parseBoolean(defaultProperties.getProperty("AssociationEdgeLineBeginFill", String.valueOf(Constants.ID_ASSOCIATION_EDGE_LINE_BEGIN_FILL))));
					
					// Barva písma pro poznámky na hraně:
					final String textColor = defaultProperties.getProperty("AssociationEdgeLineTextColor", Integer.toString(Constants.ID_ASSOCIATION_EDGE_FONT_COLOR.getRGB()));
					GraphConstants.setForeground(attributes, new Color(Integer.parseInt(textColor)));
					
					// Font písma pro poznámky - text na hrane:
					final int fontSize = Integer.parseInt(defaultProperties.getProperty("AssociationEdgeLineFontSize", Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getSize())));
					final int fontStyle = Integer.parseInt(defaultProperties.getProperty("AssociationEdgeLineFontStyle", Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getStyle())));
					GraphConstants.setFont(attributes, new Font(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getName(), fontStyle, fontSize));
				}
				
				
				else {	
					// Barva hrany:			
					GraphConstants.setLineColor(attributes, Constants.ID_ASSOCIATION_EDGE_COLOR);
					
					// Popis hrany bude poděl čáry:
					GraphConstants.setLabelAlongEdge(attributes, Constants.ID_ASSOCIATION_LABELS_ALONG_EDGE);				
					
					// konec hrany:
					GraphConstants.setLineEnd(attributes, Constants.ID_ASSOCIATION_EDGE_LINE_END);
								
					// začátek hrany:
					GraphConstants.setLineBegin(attributes, Constants.ID_ASSOCIATION_EDGE_LINE_BEGIN);
					
					// Šířka hrany:
					GraphConstants.setLineWidth(attributes, Constants.ID_ASSOCIATION_EDGE_LINE_WIDTH);
					
					// Výplň na konci hrany:
					GraphConstants.setEndFill(attributes, Constants.ID_ASSOCIATION_EDGE_LINE_END_FILL);
					
					// Výplň na začátku hrany:
					GraphConstants.setBeginFill(attributes, Constants.ID_ASSOCIATION_EDGE_LINE_BEGIN_FILL);
					
					// Barva písma pro poznámky na hraně:
					GraphConstants.setForeground(attributes, Constants.ID_ASSOCIATION_EDGE_FONT_COLOR);
					
					// Font písma pro poznámky - text na hrane:	
					GraphConstants.setFont(attributes, Constants.ID_ASSOCIATION_EDGE_LINE_FONT);
				}
				
				
				
				
				// Zda se má zapsat multiplicita 1 : 1 nebo 1 : N:
				// (Cíl a zdroj)
				final Object[] labels = {desSide, srcSide};
				
				final Point2D[] labelPositions = {new Point2D.Double(GraphConstants.PERMILLE * 7 / 8, -20), new Point2D.Double(GraphConstants.PERMILLE / 8, -20)};
				GraphConstants.setExtraLabelPositions(attributes, labelPositions);
				
				GraphConstants.setExtraLabels(attributes, labels);
				
				
				
				
				
				// Aby šlo k buňce připojit více hran:
				GraphConstants.setRouting(attributes, ParallelEdgeRouter.getSharedInstance());
				
				
				// Uživatel s hranami v podstatě nemůže nic dělat - odpojit je a připojit:
				GraphConstants.setConnectable(attributes, false);
				GraphConstants.setDisconnectable(attributes, false);
				
				// na Hranu půde vložit komentář od uživatele:
				GraphConstants.setEditable(attributes, true);
				
				// vložení hrany do kolekce a vykreslení:
				CELLS_LIST.add(edge);
						
				updateDiagram(CELLS_LIST);
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Metoda, který vytvoři hranu reprezentující agregaci, všechny agregace budou stejné, akorát s jinými popisky
	@Override
	public void addAggregationEdge(final Object srcCell, final Object desCell, final String desSide) {
		if (srcCell instanceof DefaultGraphCell && desCell instanceof DefaultGraphCell) {
			if (CELLS_LIST.contains(srcCell) && CELLS_LIST.contains(desCell)) {
				final DefaultEdge edge = new DefaultEdge();
				
				edge.setSource(((DefaultGraphCell) srcCell).getChildAt(0));
				edge.setTarget(((DefaultGraphCell) desCell).getChildAt(0));
				
				
				
				// Design:
				final Map<?, ?> attributes = edge.getAttributes();
				
				if (defaultProperties != null) {
					// Barva hrany:			
					final String colorText = defaultProperties.getProperty("AggregationEdgeLineColor", Integer.toString(Constants.ID_AGGREGATION_EDGE_COLOR.getRGB()));
					GraphConstants.setLineColor(attributes, new Color(Integer.parseInt(colorText)));
					
					// Popis hrany bude poděl čáry:
					GraphConstants.setLabelAlongEdge(attributes, Boolean.parseBoolean(defaultProperties.getProperty("AggregationEdgeLabelsAlongEdge", String.valueOf(Constants.ID_AGGREGATION_LABELS_ALONG_EDGE))));
					
					// konec hrany:
					GraphConstants.setLineEnd(attributes, Integer.parseInt(defaultProperties.getProperty("AggregationEdgeLineEnd", Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_END))));
								
					// začátek hrany:
					GraphConstants.setLineBegin(attributes, Integer.parseInt(defaultProperties.getProperty("AggregationEdgeLineBegin", Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_BEGIN))));
					
					// Šířka hrany:
					GraphConstants.setLineWidth(attributes, Float.parseFloat(defaultProperties.getProperty("AggregationEdgeLineWidth", String.valueOf(Constants.ID_AGGREGATION_EDGE_LINE_WIDTH))));
					
					// Výplň na konci hrany:
					GraphConstants.setEndFill(attributes, Boolean.parseBoolean(defaultProperties.getProperty("AggregationEdgeLineEndFill", String.valueOf(Constants.ID_AGGREGATION_EDGE_LINE_END_FILL))));
					
					// Výplň na začátku hrany:
					GraphConstants.setBeginFill(attributes, Boolean.parseBoolean(defaultProperties.getProperty("AggregationEdgeLineBeginFill", String.valueOf(Constants.ID_AGGREGATION_EDGE_LINE_BEGIN_FILL))));
					
					// Barva písma pro poznámky na hraně:
					final String textColor = defaultProperties.getProperty("AggregationEdgeLineTextColor", Integer.toString(Constants.ID_AGGREGATION_EDGE_FONT_COLOR.getRGB()));
					GraphConstants.setForeground(attributes, new Color(Integer.parseInt(textColor)));				
					
					// Font písma pro poznámky - text na hrane:				
					final int fontSize = Integer.parseInt(defaultProperties.getProperty("AggregationEdgeLineFontSize", Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getSize())));
					final int fontStyle = Integer.parseInt(defaultProperties.getProperty("AggregationEdgeLineFontStyle", Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getStyle())));
					GraphConstants.setFont(attributes, new Font(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getName(), fontStyle, fontSize));
				}
				
				
				else {	
					// Barva hrany:			
					GraphConstants.setLineColor(attributes, Constants.ID_AGGREGATION_EDGE_COLOR);
					
					// Popis hrany bude poděl čáry:
					GraphConstants.setLabelAlongEdge(attributes, Constants.ID_AGGREGATION_LABELS_ALONG_EDGE);				
					
					// konec hrany:
					GraphConstants.setLineEnd(attributes, Constants.ID_AGGREGATION_EDGE_LINE_END);
								
					// začátek hrany:
					GraphConstants.setLineBegin(attributes, Constants.ID_AGGREGATION_EDGE_LINE_BEGIN);
					
					// Šířka hrany:
					GraphConstants.setLineWidth(attributes, Constants.ID_AGGREGATION_EDGE_LINE_WIDTH);
					
					// Výplň na konci hrany:
					GraphConstants.setEndFill(attributes, Constants.ID_AGGREGATION_EDGE_LINE_END_FILL);
					
					// Výplň na začátku hrany:
					GraphConstants.setBeginFill(attributes, Constants.ID_AGGREGATION_EDGE_LINE_BEGIN_FILL);
					
					// Barva písma pro poznámky na hraně:
					GraphConstants.setForeground(attributes, Constants.ID_AGGREGATION_EDGE_FONT_COLOR);
					
					// Font písma pro poznámky - text na hrane:	
					GraphConstants.setFont(attributes, Constants.ID_AGGREGATION_EDGE_LINE_FONT);
				}
				
				
				
				
				// Zda se má zapsat multiplicita 1 : 1 nebo 1 : N:
				// (Cíl a zdroj)
				final Object[] labels = {desSide, ""};
				
				final Point2D[] labelPositions = { new Point2D.Double(GraphConstants.PERMILLE * 7 / 8, -20),
						new Point2D.Double(GraphConstants.PERMILLE / 8, -20) };
				GraphConstants.setExtraLabelPositions(attributes, labelPositions);
				
				GraphConstants.setExtraLabels(attributes, labels);
				
				
				
				
				
				// Aby šlo k buňce připojit více hran:
				GraphConstants.setRouting(attributes, ParallelEdgeRouter.getSharedInstance());
				
				
				// Uživatel s hranami v podstatě nemůže nic dělat - odpojit je a připojit:
				GraphConstants.setConnectable(attributes, false);
				GraphConstants.setDisconnectable(attributes, false);
				
				// Na hranu půjde vložit komentář od uživatele
				GraphConstants.setEditable(attributes, true);
				
				// vložení hrany do kolekce a vykreslení:
				CELLS_LIST.add(edge);
				
				updateDiagram(CELLS_LIST);				
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, pro "Výchozí" nastavení grafu metoda obsahuje prvky, díky kterým
	 * uživatel bude mít povolené jen "nějaké" operace třeba aby mohl přemísťovat
	 * buňky v grafu, nešlo hýbar s textem na hraně, apod
	 */
	private void chartSettings() {
		// Půjde hýbat s Buňkami:
		setMoveable(true);		
		
		// Aby se buňky nedostaly za okraj grafu
		// Půjdou posunout doprava a dolů - "zvětší" se okno grafu, ale nepůjdou posunout doleva a nahoru
		setMoveBelowZero(false);
		
		// Hrana se nesmí při pohybu odpojit:
		setDisconnectOnMove(false);
		
		// Povolení antialisingu:
		// zaoblené hrany - přechody, ...
		setAntiAliased(true);		
		
		// Zde půjdou editovat akorát popisky na hranách coby nějaý vztah mezi instancemi, takže to zde 
		// povolím a zakážu editaci akorát u bunky reprezentující instanci nějaké třídy z diagramu tříd
		setEditable(true);
		
		// Při změné názvu se velikost buňky nepřizpůsobní, to si uživatel musí "buňku roztáhnout" sám
		// Já nastavím pouze výchozí velikst buňky, zbytek - ohledně velikosti je na uživateli
		getGraphLayoutCache().setAutoSizeOnValueChange(false);			
		
		// Nepůjdu hýbat s textem na hranách
		setEdgeLabelsMovable(Constants.ID_EDGE_LABELS_MOVABLE);
		
		// Barva pozadí:
		setBackground(Constants.ID_BACKGROUND_COLOR);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která si ze souboru načte nastavení diagramu - fonty, barvy, písmo,
	 * ... Pokud soubor ve Workspace nebude nalazen, použije se výchozí nastavení z
	 * adresáře aplikace
	 */
	private void setInstanceDiagramProperties() {
		defaultProperties = App.READ_FILE.getInstanceDiagramProperties();

		/*
		 * Toto je potřeba například pokud se při spuštění aplikace nachází v příslušném
		 * konfiguračním soubour nějaká chyba, tak se ty soubory vytvoří s výchozím
		 * nastavením, ale zde je potřeba tensoubor přenačíst, když jsou již data v
		 * pořádku, což je v této části při souštění aplikace.
		 */
		KIND_OF_EDGE.reloadProperties();
		
		
		
		if (defaultProperties != null) {
			// Zda se má zobrazit v instanci název třídy s balíčky nebo bez nich:
			showPackages = Boolean.parseBoolean(defaultProperties.getProperty("ShowPackagesInCell",
					String.valueOf(Constants.ID_SHOW_PACKAGES_IN_CELL)));
			
			
			
			// Velikost písma v buňce reprezentující instanci:
			instanceFontSize = Integer.parseInt(defaultProperties.getProperty("FontSize", Integer.toString(Constants.ID_FONT.getSize())));
			
			// Zda půjdou popisky hran posouvat:
			setEdgeLabelsMovable(Boolean.parseBoolean(defaultProperties.getProperty("EdgeLabelMovable", String.valueOf(Constants.ID_EDGE_LABELS_MOVABLE))));
			
			// Barva pozadí grafu:
			final String backgroundColor = defaultProperties.getProperty("BackgroundColor", Integer.toString(Constants.ID_BACKGROUND_COLOR.getRGB()));
			setBackground(new Color(Integer.parseInt(backgroundColor)));			
			
			// Přiblížení grafu:
			setScale(Double.parseDouble(defaultProperties.getProperty("Scale", String.valueOf(Constants.INSTANCE_DIAGRAM_SCALE))));
			
			
			// Zda se mají zobrazovat vztahy instance na sebe sama:
			showRelationShipsToItself = Boolean.parseBoolean(defaultProperties.getProperty("ShowRelationsShipsToInstanceItself", String.valueOf(Constants.ID_SHOW_RELATION_SHIPS_TO_INSTANCE_ITSELF)));

			// Zda se mají zobrazovat názvy tříd s balíčky nebo bez nich (v položce "Zděděné metody" v kontextovém menu nad instancí)
			showInheritedClassNameWithPackages = Boolean.parseBoolean(defaultProperties.getProperty("ShowInheritedClassNameWithPackages", String.valueOf(Constants.ID_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES)));

			
			// Zda se mají zobrazit i privátní metody nadoznačenou instancí:
			includePrivateMethods = Boolean.parseBoolean(defaultProperties.getProperty("IncludePrivateMethods", String.valueOf(Constants.ID_INCLUDE_PRIVATE_METHODS)));

			// Zda se mají v kontextovém menu zobrazovat i chráněné metody:
			includeProtectedMethods = Boolean.parseBoolean(defaultProperties.getProperty("IncludeProtectedMethods", String.valueOf(Constants.ID_INCLUDE_PROTECTED_METHODS)));

			// Zda se mají v kontextovém menu zobrazovat i package-private metody:
			includePackagePrivateMethods = Boolean.parseBoolean(defaultProperties.getProperty("IncludePackagePrivateMethods", String.valueOf(Constants.ID_INCLUDE_PACKAGE_PRIVATE_METHODS)));


			// Zda se mají zpřístupnit proměnné pro předání do parametru metody.
			makeVariablesAvailableForCallMethod  = Boolean.parseBoolean(defaultProperties.getProperty("MakeFieldsAvailableForMethod", String.valueOf(Constants.ID_MAKE_FIELDS_AVAILABLE_FOR_CALL_METHOD)));
			
			// Zda se má využít "rychlý" způsob pro aktualizaci vztahů mezi instancemi nebo "pomalý":
			useFastWay = Boolean.parseBoolean(defaultProperties.getProperty("UseFastWayToRefreshRelationshipsBetweenInstances", String.valueOf(Constants.ID_USE_FAST_WAY_TO_REFRESH_RELATIONSHIPS_BETWEEN_CLASSES)));
			
			// Zda se mají zobrazit vztahy v proměnných, které se nachází v předcích:
			showRelationshipsInInheritedClasses = Boolean.parseBoolean(defaultProperties.getProperty("ShowRelationshipsInInheritedClasses", String.valueOf(Constants.ID_SHOW_RELATIONSHIPS_IN_INHERITED_CLASSES)));
			
			
			// Zda se má zpřístupnit možnost pro vytvoření nové instance, pokud se v parametru zavolané metody bude nacházet třída z diagramu tříd:
			showCreateNewInstanceOptionInCallMethod = Boolean.parseBoolean(defaultProperties.getProperty("ShowCreateNewInstanceOptionInCallMethod", String.valueOf(Constants.CD_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD)));
			
			
			// Zda se má vztah typu asociace znázornit jako jedna hrana s šipkami na obou koncích nebo pomocí dvou agregací:
			showAssociationThroughAggregation = Boolean.parseBoolean(defaultProperties.getProperty("ShowAssociationThroughAggregation", String.valueOf(Constants.ID_SHOW_ASOCIATION_THROUGH_AGREGATION)));
			
			
			// Zda se mají zobrazit v dialogu s přehldem hodnot o instanci i referenční proměnné u instancí v proměnných:
			showReferenceVariables = Boolean.parseBoolean(defaultProperties.getProperty("ShowReferenceVariables", String.valueOf(Constants.ID_SHOW_REFERENCES_VARIABLES)));
			
			
			
			// Následují hodnoty pro zvýrazněnou instanci:
			
			// Zda se vůbec mají instance zvýrazňovat:
			highlightInstances = Boolean.parseBoolean(defaultProperties.getProperty("HighlightInstances", String.valueOf(Constants.ID_HIGHLIGHT_INSTANCES)));
				
			// Doba / interval, který značí, jak dlouho bude buňka / instance zvýrazněná:
			highlightingTime = Double.parseDouble(defaultProperties.getProperty("HighlightingTime", Double.toString(Constants.ID_DEFAULT_HIGHLIGHTING_TIME)));
			
			// Barva písma, kterou bude mít buňka / instance v případě, že bude zvýrazněná:
			final String textColor = defaultProperties.getProperty("TextColorOfHighlightedInstance", Integer.toString(Constants.ID_TEXT_COLOR_OF_HIGHLIGHTED_INSTANCE.getRGB()));
			textColorOfHighlightedInstance = new Color(Integer.parseInt(textColor));

			// Barva písma pro atributy, které budou mít ve zvýrazněné instanci:
			final String attributeColor = defaultProperties.getProperty("AttributeColorOfHighlightedInstance", Integer.toString(Constants.ID_ATTRIBUTE_COLOR_OF_HIGHLIGHTED_INSTANCE.getRGB()));
			attributeColorOfHighlightedInstance = new Color(Integer.parseInt(attributeColor));

			// Barva písma pro metody, které budou mít ve zvýrazněné instanci:
			final String methodColor = defaultProperties.getProperty("MethodColorOfHighlightedInstance", Integer.toString(Constants.ID_METHOD_COLOR_OF_HIGHLIGHTED_INSTANCE.getRGB()));
			methodColorOfHighlightedInstance = new Color(Integer.parseInt(methodColor));
			
			// Barva ohraničení, kterou bude mít buňka / instance v případě, že bude zvýrazněná:
			final String borderColor = defaultProperties.getProperty("BorderColorOfHighlightedInstance", Integer.toString(Constants.ID_BORDER_COLOR_OF_HIGHLIGHTED_INSTANCE.getRGB()));
			borderColorOfHighlightedInstance = new Color(Integer.parseInt(borderColor));
			
			// Barva pozadí, které bude mít buňka / instance v případě, že bude zvýrazněná:
			final String instanceBackgroundColor = defaultProperties.getProperty("BackgroundColorOfHighlightedInstance", Integer.toString(Constants.ID_BACKGROUND_COLOR_OF_HIGHLIGHTED_INSTANCE.getRGB()));
			backgroundColorOfHighlightedInstance = new Color(Integer.parseInt(instanceBackgroundColor));
			
			
			// Zda se mají nabízet metody pro předání návratovhé hodnoty do parametru metody v dialogu pro zavolání metody:
			makeMethodsAvailableForCallMethod = Boolean.parseBoolean(defaultProperties.getProperty("MakeMethodsAvailableForCallMethod", String.valueOf(Constants.ID_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD)));
			
			// Zda se mají v dialogu pro zavolání metody / setru na proměnnou typu třídy, kde se ten setr nachází zobrazovat v nabídce i zavolání getru na třídu sebe sama, tj. kde se ten getr a setr nachází: 
			showGetrInSetrMethodToInstance = Boolean.parseBoolean(defaultProperties.getProperty("ShowGetterInSetterMethodInInstance", String.valueOf(Constants.ID_SHOW_GETTER_IN_SETTER_METHOD_IN_INSTANCE)));
			
			
			
			// Následují hodnoty pro to, zda se mají zobrazovat atributy a metody v buňce reprezentující instanci:
			
			// Atributy:
			
			// Zda se vůbec mají zobrazovat atributy v buňce / instanci:
			showAttributesInInstanceCell = Boolean.parseBoolean(defaultProperties.getProperty("ShowAttributes", String.valueOf(Constants.ID_SHOW_ATTRIBUTES)));

			// Zda se mají zobrazit v buňce / instanci všechny atributy (v příslušné
			// instanci / třídě) nebo jen zvolený počet:
			showAllVariables = Boolean.parseBoolean(defaultProperties.getProperty("ShowAllAttributes",String.valueOf(Constants.ID_SHOW_ALL_ATTRIBUTES)));

			// Počet atributů, který se má v buňce / instanci zobrazit:
			countOFVariablesForShowInInstance = Integer.parseInt(defaultProperties.getProperty("ShowSpecificCountOfAttributes", String.valueOf(Constants.ID_SPECIFIC_COUNT_OF_ATTRIBUTES)));
			
			
			// Metody:

			// Zda se vůbec mají zobrazovat metody v buňce / instanci:
			showMethodsInInstanceCell = Boolean.parseBoolean(defaultProperties.getProperty("ShowMethods", String.valueOf(Constants.ID_SHOW_METHODS)));

			// Zda se mají zobrazit v buňce / instanci všechny metody (v příslušné instanci
			// / třídě) nebo jen zvolený počet:
			showAllMethods = Boolean.parseBoolean(defaultProperties.getProperty("ShowAllMethods", String.valueOf(Constants.ID_SHOW_ALL_METHODS)));

			// Ppočet metod, který se má v buňce / instanci zobrazit:
			countOfMethodsForShowInInstance = Integer.parseInt(defaultProperties.getProperty("ShowSpecificCountOfMethods", String.valueOf(Constants.ID_SPECIFIC_COUNT_OF_METHODS)));
		}
		
		
		else {
			// Zda se má zobrazit v instanci název třídy s balíčky nebo bez nich:
			showPackages = Constants.ID_SHOW_PACKAGES_IN_CELL;
			
			
			// Velikost písma v buňce reprezentující instanci:
			instanceFontSize = Constants.ID_FONT.getSize();
			
			setEdgeLabelsMovable(Constants.ID_EDGE_LABELS_MOVABLE);
			
			// Barva pozadi:
			setBackground(Constants.ID_BACKGROUND_COLOR);
			
			setScale(Constants.INSTANCE_DIAGRAM_SCALE);
			
			// Zda se mají zobrazovat vztahy instance na sebe sama:
			showRelationShipsToItself = Constants.ID_SHOW_RELATION_SHIPS_TO_INSTANCE_ITSELF;

			// Zda se mají zobrazovat názvy tříd s balíčky nebo bez nich (v položce "Zděděné metody" v kontextovém menu nad instancí)
			showInheritedClassNameWithPackages = Constants.ID_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES;

			// Zda se mají zobrazit i privátní metody nadoznačenou instancí:
			includePrivateMethods = Constants.ID_INCLUDE_PRIVATE_METHODS;

			// Zda se mají v kontextovém menu zobrazovat i chráněné metody:
			includeProtectedMethods = Constants.ID_INCLUDE_PROTECTED_METHODS;

			// Zda se mají v kontextovém menu zobrazovat i package-private metody:
			includePackagePrivateMethods = Constants.ID_INCLUDE_PACKAGE_PRIVATE_METHODS;
			
			// Zda se mají zpřístupnit proměnné pro předání do parametru metody.
			makeVariablesAvailableForCallMethod  = Constants.ID_MAKE_FIELDS_AVAILABLE_FOR_CALL_METHOD;
			
			// Zda se má využít "rychlý" způsob pro aktualizaci vztahů mezi instancemi nebo "pomalý":
			useFastWay = Constants.ID_USE_FAST_WAY_TO_REFRESH_RELATIONSHIPS_BETWEEN_CLASSES;
			
			// Zda se mají zobrazit vztahy v proměnných, které se nachází v předcích:
			showRelationshipsInInheritedClasses = Constants.ID_SHOW_RELATIONSHIPS_IN_INHERITED_CLASSES;
			
			// Zda se má vztah typu asociace znázornit jako jedna hrana s šipkami na obou koncích nebo pomocí dvou agregací:
			showAssociationThroughAggregation = Constants.ID_SHOW_ASOCIATION_THROUGH_AGREGATION;
			
			// Zda se mají zobrazit v dialogu s přehldem hodnot o instanci i referenční proměnné u instancí v proměnných:
			showReferenceVariables = Constants.ID_SHOW_REFERENCES_VARIABLES;
			
			
			
			// Následují hodnoty pro zvýrazněnou instanci:
			
			// Zda se vůbec mají instance zvýrazňovat:
			highlightInstances = Constants.ID_HIGHLIGHT_INSTANCES;
				
			// Doba / interval, který značí, jak dlouho bude buňka / instance zvýrazněná:
			highlightingTime = Constants.ID_DEFAULT_HIGHLIGHTING_TIME;
			
			// Barva písma, kterou bude mít buňka / instance v případě, že bude zvýrazněná:
			textColorOfHighlightedInstance = Constants.ID_TEXT_COLOR_OF_HIGHLIGHTED_INSTANCE;

			// Barva písma pro atributy, které budou mít ve zvýrazněné instanci:
			attributeColorOfHighlightedInstance = Constants.ID_ATTRIBUTE_COLOR_OF_HIGHLIGHTED_INSTANCE;

			// Barva písma pro metody, které budou mít ve zvýrazněné instanci:
			methodColorOfHighlightedInstance = Constants.ID_METHOD_COLOR_OF_HIGHLIGHTED_INSTANCE;

			// Barva ohraničení, kterou bude mít buňka / instance v případě, že bude
			// zvýrazněná:
			borderColorOfHighlightedInstance = Constants.ID_BORDER_COLOR_OF_HIGHLIGHTED_INSTANCE;
			
			// Barva pozadí, které bude mít buňka / instance v případě, že bude zvýrazněná:
			backgroundColorOfHighlightedInstance = Constants.ID_BACKGROUND_COLOR_OF_HIGHLIGHTED_INSTANCE;
			
			
			// Zda se mají nabízet metody pro předání návratovhé hodnoty do parametru metody v dialogu pro zavolání metody:
			makeMethodsAvailableForCallMethod = Constants.ID_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD;
			
			
			// Zda se mají v dialogu pro zavolání metody / setru na proměnnou typu třídy,
			// kde se ten setr nachází zobrazovat v nabídce i zavolání getru na třídu sebe
			// sama, tj. kde se ten getr a setr nachází:
			showGetrInSetrMethodToInstance = Constants.ID_SHOW_GETTER_IN_SETTER_METHOD_IN_INSTANCE;
			
			
			
			// Následují hodnoty pro to, zda se mají zobrazovat atributy a metody v buňce reprezentující instanci:

			// Atributy:

			// Zda se vůbec mají zobrazovat atributy v buňce / instanci:
			showAttributesInInstanceCell = Constants.ID_SHOW_ATTRIBUTES;

			// Zda se mají zobrazit v buňce / instanci všechny atributy (v příslušné
			// instanci / třídě) nebo jen zvolený počet:
			showAllVariables = Constants.ID_SHOW_ALL_ATTRIBUTES;

			// Počet atributů, který se má v buňce / instanci zobrazit:
			countOFVariablesForShowInInstance = Constants.ID_SPECIFIC_COUNT_OF_ATTRIBUTES;
			
			
			// Metody:

			// Zda se vůbec mají zobrazovat metody v buňce / instanci:
			showMethodsInInstanceCell = Constants.ID_SHOW_METHODS;

			// Zda se mají zobrazit v buňce / instanci všechny metody (v příslušné instanci
			// / třídě) nebo jen zvolený počet:
			showAllMethods = Constants.ID_SHOW_ALL_METHODS;

			// Ppočet metod, který se má v buňce / instanci zobrazit:
			countOfMethodsForShowInInstance = Constants.ID_SPECIFIC_COUNT_OF_METHODS;
		}
		
		updateDiagram(CELLS_LIST);
	}
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zkusí v diagramu instancí, resp. v příslušné kolekci najít
	 * buňku, která obsahuje text v parametru metody.
	 * 
	 * @param text
	 *            - text, který by měla buňka obsahovat, resp. jedná se pro
	 *            uživatele o referenci na danou instanci
	 * 
	 * @return null, pokud nebude taková buňka nalezena, jinak vrátí tu buňku
	 */
	static DefaultGraphCell getCellWithText(final String text) {
		for (final DefaultGraphCell c : CELLS_LIST) {
			// jestliže to není hrana, tak je to buňka, jiné objekty v diagramu nejsou, 
			// a zároveň obsahuje příslušný text, tak ji vrátím:
			
			if (c instanceof InstanceCell && ((InstanceCell) c).getReferenceName().equals(text))
				return c;
		}
		
		return null;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	/**
	 * Metoda, která určí počet hran daného typu mezi příslušnými buňkami.
	 * 
	 * @param srcCell
	 *            - "zrojová bunka", měla by se nacházet na začátku hrany
	 * 
	 * @param desCell
	 *            - cílová bunka, mela by se nacházet na konci hrany
	 * 
	 * @param srcSide
	 *            - popisek coby název proměnné, měla by se nacházet na začátku
	 *            hrany
	 * 
	 * @param desSide
	 *            - popisek coby název proměnné, měl by se nacházet na konci hrany
	 * 
	 * @param kindOfEdge
	 *            - výčotý typ o hraně, která se má počítat
	 * 
	 * @return počet hran daného typu a s daným popiskem, který by se měl nacházet
	 *         mezi příslušnými bunkami
	 */
	static int getCountOfEdgesBetweenCells(final DefaultGraphCell srcCell, final DefaultGraphCell desCell,
			final String srcSide, final String desSide, final KindOfInstanceEdgeEnum kindOfEdge) {
		
		int count = 0;
		
		
		for (final DefaultGraphCell c : CELLS_LIST) {
		
			// Otestuji, zda se jedná o potřebný typ hrany a její zdroj a cíl jsou bunkÿ v paramtru metody
//			if (c instanceof DefaultEdge /*&& isKindOfEdge((DefaultEdge) c, srcSide, desSide, kindOfEdge)*/) {
            if (!(c instanceof DefaultEdge) /*&& isKindOfEdge((DefaultEdge) c, srcSide, desSide, kindOfEdge)*/)
                continue;

            // Nejprve otestuji, zda se jedná o agregaci nebo Asociaci, protože u Asociace mohou být zdroj a cíl hrany
            // prohozené a to samé i pro její popisky na koncích hrany
            if (kindOfEdge.equals(KindOfInstanceEdgeEnum.ASSOCIATION)) {
                // Zde se jedná o hranu typu Asociace, tak musím otestovat, je to hrana se správnými popisky,
                // ale můžou být ty popisky prohozené
                if (!isKindOfEdge((DefaultEdge) c, srcSide, desSide, kindOfEdge) && !isKindOfEdge((DefaultEdge) c,
                        desSide, srcSide, kindOfEdge))
                    continue;

                // Načtu si zdroj a cíl hrany:
                final DefaultGraphCell srcCellOfEdge = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) c).getSource()).getParent();
                final DefaultGraphCell desCellOfEdge = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) c).getTarget()).getParent();

                // pokud se shoduje zdroj a cíl hrany, tak zvýším počet:

                // Zde si akorát musím dát pozor, protože u hrany typu Asociace nezáleží na zdroji nebo kooci hrany,
                // v případě asociace je to to samé když je srcCell zdroj nebo cíl hrany, ať tak či tak, je to to samé pro
                // reprezentaci daného vztahu:
                if ((srcCell == srcCellOfEdge && desCell == desCellOfEdge) || (srcCell == desCellOfEdge && desCell == srcCellOfEdge))
                    count++;
            }


            // Zde se jedná o agregaci, kde musí být zdroj a cíl hrany, včetně popisku na jeho konci identické pro
            // určení hrany:
            else if (isKindOfEdge((DefaultEdge) c, srcSide, desSide, kindOfEdge)) {
                // Načtu si zdroj a cíl hrany:
                final DefaultGraphCell srcCellOfEdge = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) c).getSource()).getParent();
                final DefaultGraphCell desCellOfEdge = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) c).getTarget()).getParent();

                // pokud se shoduje zdroj a cíl hrany, tak zvýším počet:

                // zde už může být pouze agragace, a u té se musí zdroj a cíl hrany shodovat
                if (srcCell == srcCellOfEdge && desCell == desCellOfEdge)
                    count++;
            }
        }
		
		
		return count;
	}




    /**
     * Metoda, která otestuje, zda se jedná o příslušný typ hrany.
     *
     * @param edge
     *         - hrana, o které chci zjistit, zda je nějakého typu
     * @param srcSide
     *         - popisek, který by se měl nacházet na začátku hrany
     * @param desSide
     *         - popisek, který by se měl nacházet na konci hrany
     * @param kindOfEdgeEnum
     *         - typ hrany, který by měla edge být, resp. chci vědět, zda je hrana edge typu kindOfEdgeEnum
     *
     * @return true, pokud je hrana edge typu kindOfEdgeEnum, jinak false
     */
    private static boolean isKindOfEdge(final DefaultEdge edge, final String srcSide, final String desSide,
                                        final KindOfInstanceEdgeEnum kindOfEdgeEnum) {

        switch (kindOfEdgeEnum) {
            case ASSOCIATION:
                return KIND_OF_EDGE.isAssociation(edge, srcSide, desSide);

            case AGGREGATION_1_1:
            case AGGREGATION_1_N_ARRAY:
            case AGGREGATION_1_N_LIST:
                return KIND_OF_EDGE.isAggregation(edge, desSide);

            default:
                break;
        }

        return false;
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která odebere příslušnou hranu z diagramu instancí - odebre se hrana,
	 * která odpovídá hodnotám v parametru metody
	 * 
	 * @param info
	 *            - proměná typu InfoAbouEdgesForCreate, která obsahuje informace o
	 *            hraně, která se má smazat
	 */
	final void removeSpecificEdgeFromDiagram(final InfoAboutEdgesForCreate info) {
		DefaultEdge edgeForDelete = null;
		
		for (final DefaultGraphCell c : CELLS_LIST) {
			// Podmínka otestuje, zda se jedná o hranu správného typu a se správnými popisky
			if (c instanceof DefaultEdge && isKindOfEdge((DefaultEdge) c, info.getSrcSide(), info.getDesSide(), info.getKindOfEdge())) {
				// Nyí je ještě třeba otestovat, zda odpovídá i zdroj a cíl hrany:
				final DefaultGraphCell srcCellOfEdge = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) c).getSource()).getParent();
				final DefaultGraphCell desCellOfEdge = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) c).getTarget()).getParent();
				
				if (srcCellOfEdge == info.getSrcCell() && desCellOfEdge == info.getDesCell()) {
					// Zde odpovídá i zdroj a cíl hrany a jelikož tato metoda má za úkol smazat pouze první výskyt 
					// hrany s odpovídajícím vlastnostmi, tak si uložím tuto nalezenou hrqnu a ukončím cyklus, a metoda bude
					// dále pokračovat pro smazání hrany:
					edgeForDelete = (DefaultEdge) c;
					break;
				}					
			}
		}
		
		
		
		// Otestuji, zda byla nějaká hrana nalezena a smažu ji:
		if (edgeForDelete != null)
			deleteObjectFromGraph(edgeForDelete, CELLS_LIST);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Úkolem metody je smazat "přebytečné" hrany v tomto diagramu, resp. smazat hrany, které nejsou v
	 * kolekci v parametru této metdy - infoList
	 * 
	 * Metoda, která projde všechny objekty v diagramu instancí, zjistí si, zda se jedná o hranu a pokud ano,
	 * tak zda zdroj této hrany je bunka v parametru této metody, pokud tomu tak je, tak ta testovana bunka v 
	 * parametru metody ma vztah ne nejakou jinou instanci, tak otestuji, zda to ještě platí, pokud ne, tj. pokud
	 * tato nalezená hrana není v kolekci infoList, tak se musí smazat, protože tento vztah již neplatí.
	 * 
	 * 
	 * @param srcCell = "zdrojová" buňka, ze které vychází nějaký vztah na jiné bunky coby instance
	 * 
	 * @param infoList = kolekce - List hran, které by měly existovat mezi bunkou v parametru metody - srcCell
	 * a zbylyma ostatníma bunkama v diagramu instancí, ty které v diagramu instancí existují mezi bunkou srcCell
	 * a ostatníma a zároveň nejsou v inoList - Listu, tak se musí smazaat, protože tento vztah již mezi instancema 
	 * neexistuje
	 */
	final void deleteSpecificEdges(final DefaultGraphCell srcCell, final List<InfoAboutEdgesForCreate> infoList) {
		// kolekce, do které vložím "přebytečné" hrany, které se mají smazat:
		final List<DefaultEdge> edgesForDeleteList = new ArrayList<>();
		
		
		for (final DefaultGraphCell c : CELLS_LIST) {			
			
			// Otestuji, zda se jedná o hranu a typ té hrany je v kolekci infoList:
			
			if (c instanceof DefaultEdge) {
				// Načtu si zdroj a cíl hrany:
				final DefaultGraphCell srcCellOfEdge = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) c).getSource()).getParent();
				final DefaultGraphCell desCellOfEdge = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) c).getTarget()).getParent();
				
				
				// Otestuji, zda bunka coby zdroj hrany je testovaná instance:
				if (srcCellOfEdge == srcCell) {
					// Zde jsem našel hranu, která vychází z té aktuálně testované bunky - instance,
					// tak musím zjistit, zda se nachází v kolekci infoList, pokud ne, je třeba jí smazat,
					// pokud ano, není třeba nic dělat:
					
					boolean isEdgeInList = false;
					
					
					for (final InfoAboutEdgesForCreate i : infoList) {
						if (i.getSrcCell() == srcCellOfEdge && i.getDesCell() == desCellOfEdge
								&& isKindOfEdge((DefaultEdge) c, i.getSrcSide(), i.getDesSide(), i.getKindOfEdge())) {
							// Zde jsem našel hranu, která je v kolekci s hranami, které by měly existovat, takže mohu vložit do proměnné
							// isEdgeInList hodnotu true - tato hrana se mazat nemá a mohu ukončit cyklus:
							isEdgeInList = true;
							break;
						}
					}
					
					
					// Otestuji, zda se v kolekci infoList nachází právě testovaná hrana, pokud ne, tak ji musím vložit do kolekce
					// pro smazání
					if (!isEdgeInList)
						// Zde nebyla hrana v kolekci nalezena, tak ji přidám do kolekce pro smazání:
						edgesForDeleteList.add((DefaultEdge) c);
				}
			}
		}
		
		
		
		for (final DefaultEdge e : edgesForDeleteList)
			deleteObjectFromGraph(e, CELLS_LIST);

		updateDiagram(CELLS_LIST);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která z diagramu odebere buňku, která v grafu reprezentuje instanci
	 * nějaké třídy Odebere buňku, která nese daná název (označená buňka) v
	 * parametru metody
	 * 
	 * @param cell
	 *            - označená buňka v diagramu instancí určená pro smazání
	 */
	private void removeInstanceFromGraph(final DefaultGraphCell cell) {
		
		final List<DefaultGraphCell> cellsForDelete = new ArrayList<>();
		
		// Nejprve otestuji, zda se nejedná o hranu:
		
		if (!(cell instanceof DefaultEdge)) {
			// Zde se nejedná o hranu, tak mou zjistit, zda je označená buňka v kolekci - pro jistotu
			
			
			if (CELLS_LIST.contains(cell)) {
				// Zde kolekce objektů v diagramu instancí obsahuje danou buňku
				
				// Nejprve musím projit všechny objekty v grafu a zjistit, zda s označenou buňkou je spojena 
				// nějaká hrana, pokud ano, tak je všechny nejprve odeberu a na konec smažu označenou buňku:
				
				
				for (final DefaultGraphCell c : CELLS_LIST) {
					if (c instanceof DefaultEdge) {
						//Zde se jedná o hranu, potřebuji zjistit, zda je jeden  z konců spojen s označenou buňku 
						// coby instance nějaké třídy, pokud ano, tak ji vložím do kolekce pro smazání:
					
						final DefaultGraphCell srcCell = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) c).getSource()).getParent();
						final DefaultGraphCell desCell = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) c).getTarget()).getParent();
						
						// zde otestuji, zda je konec nebo začátek hrany spojet s označenou buňkou - instance třídy
						if (srcCell == cell || desCell == cell)
							cellsForDelete.add(c);
					}
				}
				
				// na konec přidám do kolekce pro smazání označenou buňku - instanci třídy:
				cellsForDelete.add(cell);
			}
			// Toto by nemělo nastat, když je buňka v grafu, tak by měla byt i v této kolekci
			else
				JOptionPane.showMessageDialog(this, missingObjectInGraphText, missingObjectInGraphTitle,
						JOptionPane.ERROR_MESSAGE);	
		}		
		
		
		// Zde pohledám kolekci s objekty pro smazání z grafu, a smažu ji:
		cellsForDelete.forEach(cellForDelete -> deleteObjectFromGraph(cellForDelete, CELLS_LIST));
						
		updateDiagram(CELLS_LIST);
	}



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
			
	
	/**
	 * Metoda, která odebere příslušnou instanci z mapy - ze všech instancí a z
	 * tohoto diagramu a vymaže veškeré hodnoty / referena na příslušnou instanci,
	 * které se nachází v nějakých proměnných v diagramu instancí.
	 * 
	 * @param selectedCell
	 *            - označená buňka v tomto diagramu, resp. buňka, krerá reprezentuje
	 *            nějakou instanci a má se smazat
	 */
	final void removeInstance(final DefaultGraphCell selectedCell) {
		// Odeberu instanci z mapy instancí tříd:

		if (selectedCell instanceof InstanceCell) {
			/*
			 * Získám si příslušnou buňku / instanci a z ní referenci na příslušnou instanci
			 * nějaké třídy.
			 */
			final String textOfCell_Instance = ((InstanceCell) selectedCell).getReferenceName();

			/*
			 * Nejprve si zde dočasně uložím / získám objekt, který se má smazat, protože
			 * jej bude třeba smazat ze všech proměnných ve všech instancích v diagramu
			 * instancí.
			 */
			final Object instanceForDelete = Instances.getInstanceFromMap(textOfCell_Instance);

			// Vymažu instanci z mapy, která obsahuje veškeré vytvořené instance:
			Instances.deleteInstanceFromMap(textOfCell_Instance);

			// zde odeberu označenou instanci třídy z diagramu instancí - včetně jejich
			// vazeb - včech
			removeInstanceFromGraph(selectedCell);

			// Zavolám metodu, která projde veškeré proměnné ve všech instancích v diagramu
			// tříd a ze všech proměnných vymaže veškeré reference na smazanou instanci:
			RefreshInstances.deleteAllReferencesFromAllFields(instanceForDelete);

            /*
             * Aktualizuje se okno pro automatické doplňování / dokončování hodnot v editoru příkazů, protože se výše
             * smazala instance, tak aby se v tom okně uživateli nenabízely hodnoty z této instance - proměnné,
             * metody, ani instance samotná pro předání do parametru metody apod.
             */
            App.getCommandEditor().launchThreadForUpdateValueCompletion();
        }
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro zvýraznění instancí v diagramu instancí, které se
	 * nacházejí v objValue (pokud se tam nějaké instance z diagramu instancí
	 * nacházejí).
	 * 
	 * Metoda vytvoří a spustí nové vlákno a předá mu hodnoty potřebné pro
	 * zvýraznění příslušných instancí v diagramu instancí.
	 * 
	 * @param objValue
	 *            - objekt,který vrátila nějaká metoda nebo byl získán z nějaké
	 *            proměnné apod. Tento objekt může obsahovat nějaké instance,které
	 *            se nachází v diagramu tříd, tak se zjistí, zda je objValue list
	 *            nebo X - rozměrné pole nebo "klasická" proměnná a pokud ano, tak
	 *            se příslušným způsobem získají instance a zvýrazní v diagramu
	 *            instancí.
	 */
	public final void checkHighlightOfInstances(final Object objValue) {
		/*
		 * Note:
		 * Zde bych mohl využít následující zakomentovaný kód, který by hlídal, že by
		 * mohlo běžet vždy pouze jedno vlákno, ale kdybych to udělal, tak by mohla být
		 * vždy zvýrazněna pouze jedna "sada" instancí, například těch pár instancí,
		 * které by se třeba nacházeli v poli nebo listu, který by vrátila nějaká
		 * zavolaná metoda, ale když by uživatel hned na to zavolal další metodu, která
		 * také vrátí nějaké instanci z diagramu instancí, a ty původní - ta "první
		 * sada" by byla ještě zvýrazněná, tak by se ta aktuální nezvýraznila.
		 * 
		 * Proměnná: 'highlightInstanceThread' by byla například statická proměnné někde
		 * v této třídě.
		 */
		
		
//		if (highlightInstanceThread == null || !highlightInstanceThread.isAlive()) {
//			highlightInstanceThread = new HighlightAnInstanceThread(2.5, Color.YELLOW, Color.GREEN, objValue, this);
//
//			highlightInstanceThread.start();
//		}

		
		/*
		 * Zde pouze vytvořím a spustím nové vlákno (pokud se mají zvýrazňovat
		 * instance), které zvýrazní vždy "aktuální sadu" instancí - ty v objValue.
		 * Takže uživatel může volat metody třeba ihned po sobě a vždy se zvýrazní ty
		 * potřebné v objValue.
		 */
		if (highlightInstances)
			new HighlightAnInstanceThread(highlightingTime, textColorOfHighlightedInstance,
					attributeColorOfHighlightedInstance, methodColorOfHighlightedInstance,
					borderColorOfHighlightedInstance, backgroundColorOfHighlightedInstance, objValue, this).start();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Klasícký getr na list - kolekci, ve které jsou ukládány veškeré objekty v
	 * tomto diagramu instancí
	 * 
	 * @return výše zmíněnou kolekci - list cellsList s objekty v diagramu instancí
	 */
	public static List<DefaultGraphCell> getCellsList() {
		return CELLS_LIST;
	}
	
	
	
	
	
	





	@Override
	public void setLanguage(final Properties properties) {
		checkDuplicatesInDiagrams = new CheckDuplicatesInDiagrams(properties);
		
		// do této třídy žádný jazyk na texty není třba, pouze pro předání pro texty do menu
		// po kliknutí pravým tlačítkem myši na intanci třídy:
		languageProperties = properties;
		
		if (properties != null) {
			missingObjectInGraphTitle = properties.getProperty("Id_Jop_MissingObjectInGraphTitle", Constants.ID_MISSING_OBJECT_IN_GRAPH_TITLE);
			missingObjectInGraphText = properties.getProperty("Id_Jop_MissingObjectInGraphText", Constants.ID_MISSING_OBJECT_IN_GRAPH_TEXT);
		}
		
		
		else {
			missingObjectInGraphTitle = Constants.ID_MISSING_OBJECT_IN_GRAPH_TITLE;
			missingObjectInGraphText = Constants.ID_MISSING_OBJECT_IN_GRAPH_TEXT;
		}
	}
	
	
	
		
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Getr na logickou promennou, která značí, zda se v diagramu instnací mají mezi
	 * příslušnými instancemi zobrazit i naplněné vztahy, které jsou naplněny v
	 * proměnných v předcích příslušné instance ne v samotné instanci jakožto třídě.
	 * 
	 * @return true, pokud se mají zobrazit i ty vztahy v proměnných, které
	 *         senacházejí v předcich, jinak false (budou se zobrazovat pouze vztahy
	 *         naplněné v příslušných proměnných pouze ve třídě / instancí samotné).
	 */
	public static boolean isShowRelationshipsInInheritedClasses() {
		return showRelationshipsInInheritedClasses;
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí logickou proměnnou o tom, da se má při zavolání metody
	 * zpřístupnit veřejné a chráněné proměnné v třídách v diagramu tříd.
	 * 
	 * @return popsáno výše
	 */
	public final boolean isMakeVariablesAvailableForCallMethod() {
		return makeVariablesAvailableForCallMethod;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vymaže veškeré vztahy / hrany v diagramu instancí.
	 */
	final void deleteAllEdges() {
		for (final Iterator<DefaultGraphCell> it = CELLS_LIST.iterator(); it.hasNext();) {
			final DefaultGraphCell cell = it.next();

			if (cell instanceof DefaultEdge) {
				it.remove();

				if (getModel().contains(cell))
					getModel().remove(new Object[] { cell });
			}
		}
		updateDiagram(CELLS_LIST);
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro aktualizaci hodnot proměnných v jednotlivých
	 * instancích / buňkách v diagramu instancí. Jedna se o aktualizaci hodnoty v
	 * proměnných, které jsou zobrazeny v příslušné buňce (pokud jsou v ní
	 * zobrazeny).
	 * 
	 * Toto je potřeba protože byla například zavolána nějaká metoda, tak se
	 * aktualizovaly vztahy v diagramu instancí, ale dále je potřeba aktualizovat i
	 * hodnoty těch instancí, mohla se například nějaká změnit apod.
	 */
	private void updateVariablesInCells() {
		// Pokud se ty atributy a metdy nemají zobrazovat, pak mohu skončit:
		if (!showAttributesInInstanceCell && !showMethodsInInstanceCell)
			return;

		
		
		// Projdu všechny instance a aktualizuji v nich proměnné:
		CELLS_LIST.forEach(c -> {
			if (c instanceof InstanceCell) {
				/*
				 * Získám si příslušnou instanci, abych mohl zavolat metodu, která aktualizuje
				 * hodnoty v příslušných proměnných.
				 */
				final InstanceCell myCell = (InstanceCell) c;

				/*
				 * Získám si příslušnou instanci z mapy, kde se nachází veškeré vytvořené
				 * instance.
				 */
				final Object objInstance = Instances.getInstanceFromMap(myCell.getReferenceName());

				// Tato podmínka by měla být zbytečná, ale kdyby náhodou:
				if (objInstance != null) {
					myCell.makeTextWithVariables(objInstance);
					myCell.makeTextWithMethods(objInstance);
				}
				

				/*
				 * Zde musím příslušné buňce / instance nastavit znou "ohraničení", protože
				 * kdybych to neudělal, tak by se nepřekreslily ty atributy, byly by pořád
				 * stejné, i když by v té buňce byly jiné hodnoty, nevím proč, se v tom graphics
				 * neaktualizuji hodnoty.
				 */
				GraphConstants.setBorder(c.getAttributes(),
						BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(getBackground(), 1, true),
								new RoundedBorderLine(c.toString(), myCell.getVariablesCount(), defaultProperties)));
			}
		});
	}







    /**
     * Aktualiace pouze hodnot, které jsou zobrazeny v instanci / buňce v diagramu instancí, například při změně nějaké
     * hodnoty, třeba inkremenace položky v poli, které se nachází v nějaké instanci v diagramu instancí.
     * <p>
     * Pokud tu položku inkrementujeme, tak je třeba aktualizovat následujícím způsob hodnoty zobrazené v té instanci,
     * jinak by se pořád zobrazovali v té instanci původní hodnoty, protože v tomto případě není třeba aktualizovat
     * vztahy mezi instancemi a v tom aktualizovat i hodnoty zobrazené v instancích, zde stačí aktualizovat pouze ty
     * hodnoty.
     */
    public void updateInstanceCellValues() {
        updateVariablesInCells();
        updateDiagram(GraphInstance.getCellsList());
    }






    /**
     * Setr na proměnnou, která obsahuje vždy aktuální velikost buňky reprezentující instanci třídy v diagramu
     * instancí.
     *
     * @param instanceFontSize
     *         - velikost písma / fontu v buňce reprezentující instanci třídy.
     */
    public static void setInstanceFontSize(int instanceFontSize) {
        GraphInstance.instanceFontSize = instanceFontSize;
    }


    /**
     * Getr na logickou proměnnou, která značí, zda se má využít 'rychlý' nebo 'pomalejší' způsob pro zobrazení vztahů
     * mezi instancemi v diagramu instancí. (popis u proměnné).
     *
     * @return hodnotu v proměnné useFastWay neboli true, pokud se má využít 'rychlý' způsob pro zobrazení vztahů mezi
     * instancemi, jinak false.
     */
    public static boolean isUseFastWay() {
        return useFastWay;
    }


    /**
     * Getr na proměnnou, která značí, zda se mají u některých hodnot zobrazovat reference na instanci v diagramu
     * instancí. Pokud je ta hodnota v proměnné instance (reference na instanci), která se nachází v diagramu instancí.
     *
     * @return logickou proměnnou, která značí, zda se mají zobrazovat u hodnot proměnných reference na instanci v
     * diagramu instnací - pokud ta hodnota je nějaká instance, která se nachází v diagramu instancí.
     */
    static boolean isShowReferenceVariables() {
        return showReferenceVariables;
    }


    /**
     * Getr na proměnnou, která značí, zda se mají v buňce / instanci zobrazovat atributy.
     *
     * @return true, pokud se v buňce reprezentující instanci mají zobrazovat atributy, jinak false.
     */
    public static boolean isShowAttributesInInstanceCell() {
        return showAttributesInInstanceCell;
    }


    /**
     * Getr na proměnnou, která značí, zda se mají v buňce reprezentující instanci zobrazovat metody.
     *
     * @return true, pokud se v buňce reprezentující instanci mají zobrazovat metody, jinak false.
     */
    static boolean isShowMethodsInInstanceCell() {
        return showMethodsInInstanceCell;
    }


    /**
     * Getr na proměnnou, která značí, zda se mají zobrazovat u zavolání setru na třídu sebe sama i getr na třídu sebe
     * sama (true), jinal fakse.
     *
     * @return true, pokud se mají zobrazovat getr u zavolání setru na proměnnou typu třídy sebe sama, jinak false.
     */
    public static boolean isShowGetrInSetrMethodToInstance() {
        return showGetrInSetrMethodToInstance;
    }
}