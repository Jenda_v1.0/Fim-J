package cz.uhk.fim.fimj.instance_diagram;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

import cz.uhk.fim.fimj.file.*;
import cz.uhk.fim.fimj.reflection_support.ParameterToText;
import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;
import org.jgraph.graph.DefaultGraphCell;

import cz.uhk.fim.fimj.instances.Instances;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.swing_worker.SwingWorkerInterface;

/**
 * Tato třída slouží jako vlákno, které otestuje vztahy mezi instancemi v diagramu instancí.
 * <p>
 * Toto vlákno se zavolá po vytvoření instance nebo zavolání metody nad instancí třídy z diagramu tříd v diagramu
 * instancí.
 * <p>
 * Po spuštění vlákna - pomocí metody start se provede metoda run, která otestuje vztahy mezi instancemi. Resp. vztahy
 * na ostatní instance pomocí proměnných coby referencí. Které jsou znázorněny v diagramu tříd.
 * <p>
 * <p>
 * Pricipy:
 * <p>
 * u Agregace 1 : N pro list a pole je to celkem jasne, vzdy si nactu promennou testovane tridy, pokud je to pole nebo
 * list typu jine instnce v diagramu tak si zjistim bunku, ktera tu instance reprezentuje a pripojim k ni hranu,
 * konkretne v jednolivych algoritmech
 * <p>
 * U Asociace a Agregace 1 : 1 je to trochu složitejsi, zde je třeba si nacist promenne te testovane tridy, proiterovat
 * je, a vzdy si zjistit zda je to typ instance a tato promenna obsahuje tu instanci, ktera je v diagramu instanci pokud
 * ano, tak zjistit kolik promennych je v teto tride naplnene touto hodnotou coby instancí a kolik je promennych, v te
 * druhe instance, ktere ukazuji na tuto, pokud se od sebe odectou pocty promennych v teto instanci, a pocty promennych
 * v te instanci, z testovane promenne, tak se otestuje, zda je vysledek kladny ( > 0) nebo <= 0 pokud je <= 0, pak se
 * prida tolik asociaci, jako je v teto tride, protoze prevazuje pocet proemnych v te druhe tride, tam se ale dostane
 * cyklus pri dalsi iteraci instancemi v diagramu instanci pokud je vysledek kladny > 0, pak je tento vysledek roven
 * poctu agregaci 1 : 1, ktere se maji pridat a pocet promennych v teto testovane tride B je roven poctu asociace, ktere
 * se maji pridat, protoze pkud se od sebe tyto pocty promennych odectou, a vysledek je > 0, tak prevazuje pocet
 * promennych v teto testovane instanci tridy, takze je to pocet agregaci- vysledek, ale ten zbytek coby pocet
 * promennych , ktere se odecetly, tj. pocet promennych v tride B je pocet asociaci, tj. pocet promennych, ktere jsou
 * symetricke ukazuje na sebe stejny pocet promennych mezi tridami
 * <p>
 * <p>
 * Note: Pro otestovani jednotlicych vztahu by bylo dobre jeste testovat HashMapu, pripadne jeji jednotlive "potomky"
 * TreeMap, dále HashSet, TreeSet, apod. ale tato aplikace ma slouzit halvne pro zacatecniky, tak se touto casti nebudu
 * omentalne zdrzovat a odlozim to na dalsi verzi - pro otestovani dalsich kolekci.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class CheckRelationShipsBetweenInstances extends AbstractForRelationShipThreads
		implements SwingWorkerInterface {
	
	/**
	 * Reference na diagram instancí, ve kterém je třeba vytvářet a editorvat
	 * příslušné vztahy mezi instnacemi tříd z diagramu tříd dle naplnění jeijch
	 * vztahů, které jsou reprezentované v diagramu tříd
	 */
	private static GraphInstance instanceDiagram;
	
	
	
	
	
	
	
	
	/**
	 * Tento List slouží pro "dočasné" uložení instancí, které se najdou v X -
	 * rozměrném poli instancí z této kolekce se dále testuji, zda jsou instance v
	 * této kolekci v diagramu tříd pro editaci potenciálních hran coby vztahů mezi
	 * jednotlivými instancemi v diagramu instancí
	 */
	private final List<Object> tempListForInstancesFromArray;
	
	
	
	
	
	/**
	 * Logická proměnná, která značí, zda se mají v diagramu instnací znázornit i
	 * vztahy, které jsou naplněný pomocí přílušných proměnných, ale tyto proměnné
	 * se nachází v předcích příslušné instance v diagramu instancí, ne ve třídě /
	 * instanci samotné.
	 */
	private final boolean showRelationshipsInInheritedClasses;
	
	
	
	
	
	
	/**
	 * Logická proměnná, která značí, zda se má využít pro aktualizaci vztahů v
	 * diagramu instancí 'rychlá' nebo 'pomalejší' verze.
	 * 
	 * Note:
	 * Tuto proměnnou je možné si získavat pomocí getru z přslušného diagramu, ale
	 * abych kvůli tomu nemusel opakované volat getr tak ji zde jednou naplním a pak
	 * pouze využívám.
	 */
	private final boolean useFastWay;







    /**
     * Konstruktor této třídy
     *
     * @param instanceDiagram
     *         - Reference na diagram instancí, ve kterém je třba vytvářet a editorvat vztahy mezi instancemi, které
     *         jsou reprezentovány v diagramu tříd
     * @param showRelationShipsToClassItself
     *         - logická hodnota, která značí, zda se mají v diagramu zobrazovat vztahy instance na sebe sama.
     * @param useFastWay
     *         - logická proměnná, která značí, zda se mý využít pro aktualizaci vztahů mezi instancemi v diagramu
     *         instancí rychlá nebo pomalejší verze. true pro rychlou, false pro pomalejší.
     * @param showRelationshipsInInheritedClasses
     *         - logická proměnná, která značí, zda se mají v diagramu instancí zobrazit i vztahy mezi instancemi, které
     *         reprezentují naplněné proměnné, které senacházejí v předích příslušné intance ne v instanci samotné.
     * @param showAssociationThroughAggregation
     *         - logická proměnná, která značí, zda se má vztah typu asociace znázornit klasicky jako asiciace, tj.
     *         jedna hrana mezi instancemi s šipkami na obou koncích (hodnota false). Nebo, zda se má vztah typu
     *         asociace zobrazit jako dvě agregacemezi instancemi (hodnota true).
     */
    public CheckRelationShipsBetweenInstances(final GraphInstance instanceDiagram,
                                              final boolean showRelationShipsToClassItself, final boolean useFastWay,
                                              final boolean showRelationshipsInInheritedClasses,
                                              final boolean showAssociationThroughAggregation) {

        super(showRelationShipsToClassItself, showAssociationThroughAggregation);

        CheckRelationShipsBetweenInstances.instanceDiagram = instanceDiagram;

        this.useFastWay = useFastWay;

        this.showRelationshipsInInheritedClasses = showRelationshipsInInheritedClasses;

        tempListForInstancesFromArray = new ArrayList<>();
    }
	
	
	
	
	
	
	
	
	@Override
	public void run() {
		// Načtu si všechny instance tříd, které se nachází v diagramu tříd:
		final Map<String, Object> allInstancesFromCd = Instances.getAllInstancesOfClassInCd();

		try {
			SwingUtilities.invokeAndWait(() -> {
                /*
                 * V případě, že se má využít "rychlý" způsob vytváření vztahů, tak zde nejprve
                 * vymažu veškeré hrany / vztahy mezi instancemi v diagramu instancí a níže
                 * vytvořím puze ty, které se vytvořit mají, tj. ty aktuální.
                 */
                if (useFastWay)
                    instanceDiagram.deleteAllEdges();






                /*
                 * Cyklus, který projde veškeré vytvořené instance a zjistí naplněné vztahy mezi
                 * nimi a v rámci možností je zobrazí v diagramu instancí:
                 */
                for (final Map.Entry<String, Object> map : Instances.getAllInstancesOfClassInCd().entrySet()) {

                    final Object objInstance = map.getValue();

                    /*
                     * Právě testovaná buňka, resp. instance, která byla buď vytvořena, nebo nad ní
                     * byla zavolána metoda:
                     */
                    final DefaultGraphCell selectedSrcCell = GraphInstance.getCellWithText(map.getKey());




                    /*
                     * Vytvořím si kolekci, do které budu vkládat všechny informace o hranách, které
                     * se mají vytvořit, nebo které by meli existovat mezi příslušnými bunkami v
                     * diagramu instancí, vždy "touto" buňkou, a nějakou další coby nějaká instance,
                     * na kterou drží "tato" třída, resp. instance referenci
                     */
                    final List<InfoAboutEdgesForCreate> infoAboutEdgesList = new ArrayList<>();




                    /*
                     * Tento List slouží pro uložení instancí, které již byly testovány, do této
                     * kolekce se vloží objekt pokud se jedná o proměnnou coby reference na jinou
                     * instanci, coby vztah asociace nebo agregace 1 : 1
                     */
                    final List<Object> testedInstancesList = new ArrayList<>();



                    /*
                     * Zjistím si typ Class instance, nad kterou byla zavolána metoda nebo byla
                     * vytvořena:
                     */
                    final Class<?> clazz = objInstance.getClass();




                    /*
                     * List, do kterého si vložím vekškeré proměnné (veřejné, soukromé i charáněné)
                     * z třídy clazz a pokud bude proměnná pro zobrazení vztahů i z proměnných v
                     * předcích třídy clazz, pak se načtou do tohoto listu i veškeré proměnné ze
                     * všech předků třídy clazz až po třídu Object, z té už se proměnné brát
                     * nebudou.
                     */
                    final List<Field> fieldsList = getFieldsFromClasses(clazz, new ArrayList<>(),
                            showRelationshipsInInheritedClasses);




                    // Projedu všechny položky v dané třídě:
                    for (final Field f : fieldsList) {
                        // Zjistím si hodnotu té položky:
                        final Object objValueOfField = ReflectionHelper.getValueFromField(f, objInstance);


                        // Zde potřebuji otestovat, zda není hodnota null, pokud je hodnota té položky null,
                        // pak není co řeit protože bych nezjistil, ke které instanci třídy bych měl připojit hranu,
                        // Dále zde potřebuji zjistit, zda se typ té položky nachází v mapě coby instance nějaké třídy,
                        // a nejedná se o tuto instanci (pro případ, že by třída měla referenci sama na sebe)

                        if (objValueOfField != null && isKindOfClassInMap(f, allInstancesFromCd)) {
                            // Zde je proměnná naplněná - není null a je to typ třídy, která se nachází v
                            // diagramu instancí a
                            // její instance v mapě - je již uživatelem vytvořena:

                            // Nyní vím, že se v diagramu instancí nachází příslušná intance typu té položky
                            // - aktuálně testované
                            // tak mohu zjistit, jaké vazby se mez instancemi mají vytvořit - pokud
                            // neexistují:

                            // Agregace 1 : N v podobě listu:
                            if (ReflectionHelper.isDataTypeOfList(f.getType())) {
                                // Vezmu si kolekci referencí, abych mohl otestovat,
                                // hodnoty v ní ukazují na jiné instance v diagramu instancí:
                                @SuppressWarnings("unchecked")
                                final List<Object> listOfReferences = (List<Object>) objValueOfField;

                                // Nyní si celou kolekci projdu a otestuji, zda neni hodnota null, a pokud není
                                // jedná se o konkrétní
                                // referenci na instanci nějaké třídy z diagramu instancí, tak otestuji, zda
                                // ještě v idagramu instancí existuje
                                // a zda se daná reference nachází v mapě se všemi instancemi, pokud ano, tak ji
                                // přidám do kolekce, protože
                                // na ní má ukazovat hrana:

                                for (final Object o : listOfReferences) {
                                    // tak si zjistím příslušnou buňku v diagramu instancí, která obsahuje text té
                                    // reference:
                                    final DefaultGraphCell desCell = getSpecificCell(o, allInstancesFromCd);

                                    // Jestliže byla příslušná buňka nalezena, tak ji přidám do kolekce, která
                                    // osabuje
                                    // všechny informace o hranách, které by v diagramu instancí měly existovat:

                                    /*
                                     * Nyní, abych nějak nastavil, zda se mají zobrazovat vztahy instancí na sebe
                                     * sama. Takže pokud má instance (třída) vztah na sebe sama a ty vztahy se v
                                     * diagramu instancí nemají zobrazit, pak tu proměnnou vynechám, v ostaních
                                     * případech vztah dané proměnné přidám k testování pro zobrazení v diagramu
                                     * instancí.
                                     *
                                     * V této části kódu otestuji, zda se jedná o instanci, jejíž reprezentace je
                                     * také reprezentována v diagramu instancí a pokud ano, tj. proměnná desCell
                                     * není null, pak zjistím, zda se nejedná o tu samou buňku (instanci), pokud
                                     * ano, tak pokud se mají zobrazovat vztahy instancí (tříd) na sebe sama, pak
                                     * budu pokračovat, jinak přeskočím danou instanci a budu pokračovat další
                                     * iterací
                                     */
                                    if (desCell != null) {
                                        // Testování, zda se mají zobrazovat vztahy instance sama na sebe:
                                        if (o.equals(objInstance) && !showRelationShipsToClassItself)
                                            continue;

                                        infoAboutEdgesList.add(new InfoAboutEdgesForCreate(selectedSrcCell,
                                                desCell, f.getName(), KindOfInstanceEdgeEnum.AGGREGATION_1_N_LIST));
                                    }
                                }
                            }


                            // Agregace 1 : N v podobně X rozměrného pole:
                            else if (f.getType().isArray()) {
                                // Projdu cele pole a pokud neni hodnota null, tak se mi vrati kolekce s
                                // hodnotami z pole,
                                // kterou projdu a otestuji tyto instnace na reference v diagrmau instanci

                                // vymažu kolekci, abych tam nebyly již pro tuto konkrétní iteraci neplatné
                                // znaky
                                tempListForInstancesFromArray.clear();

                                // vložím si hodnoty z pole do kolekce výše:
                                addInstancesFromArrayToList(objValueOfField);

                                // "Projedu" kolekci výše -  tempListForInstancesFromArray a zjistím, zda se v ní
                                // vyskytují instance, jejichž reprezentace jsou v diagramu instancí, pak je
                                // podobně jako v podmínce výše u List přidám do kolekce s informacemi o hraně:

                                for (final Object o : tempListForInstancesFromArray) {
                                    // Zjistím si, zda nějaká instance, coby hodnota z
                                    // pole je nějaká instance v idagramu tříd, resp. má v tomto diagramu nějakou
                                    // bunku, která ji
                                    // reprezentuje:
                                    final DefaultGraphCell desCell = getSpecificCell(o, allInstancesFromCd);

                                    /*
                                     * Nyní, abych nějak nastavil, zda se mají zobrazovat vztahy instancí na sebe
                                     * sama. Takže pokud má instance (třída) vztah na sebe sama a ty vztahy se v
                                     * diagramu instancí nemají zobrazit, pak tu proměnnou vynechám, v ostaních
                                     * případech vztah dané proměnné přidám k testování pro zobrazení v diagramu
                                     * instancí.
                                     *
                                     * V této části kódu otestuji, zda se jedná o instanci, jejíž reprezentace je
                                     * také reprezentována v diagramu instancí a pokud ano, tj. proměnná desCell
                                     * není null, pak zjistím, zda se nejedná o tu samou buňku (instanci), pokud
                                     * ano, tak pokud se mají zobrazovat vztahy instancí (tříd) na sebe sama, pak
                                     * budu pokračovat, jinak přeskočím danou instanci a budu pokračovat další
                                     * iterací
                                     */
                                    if (desCell != null) {
                                        // Testování, zda se mají zobrazovat vztahy instnace sama na sebe:
                                        if (o.equals(objInstance) && !showRelationShipsToClassItself)
                                            continue;


                                        // Zde je v diagramu instancí bunka, která reprezentuje právě procházenou instanci, tak
                                        // do kolekce infoAboutEdgesList přidám novou informaci, že
                                        // by měla existovat příslušná hrana mezi příslušnými bunkami
                                        // v diagramu instancí:
										infoAboutEdgesList.add(new InfoAboutEdgesForCreate(selectedSrcCell,
												desCell, f.getName(), ReflectionHelper.getCountOfDimensionArray
												(objValueOfField)));
                                    }
                                }
                            }


                            // Zde už zbývají pouze "klasické" proměnné - nemůže to být
                            // list ani pole (může to být například mapa apod. Ale to se zde již neřeší):






                            // Pokaždé se dotázat, zda ta instance, načtená z proměnné
                            // není v kolekci, kam si odkládaám
                            // již testované objekty, abych si nespletl počet hran pro
                            // vytvoření



                            // Nejprve se dotážu, zda se již načtená instance z proměnné testovala nebo ne, pokud ano, tak již bude
                            // v kolekcei: testedInstancesList, pokud ne, tak tam ještě není, vysvětlení v průběhu podmínky:
                            else if (!testedInstancesList.contains(objValueOfField)) {
                                // Sem se dostanu, pokud se instance ještě netestovala, takže si pro začátek
                                // zjistím počet proměnných
                                // v této aktuálně testované instanci, nad kterou byla zvolána metoda nebo byla
                                // práve vytvořena,
                                // zjistím počet proměnných, které obsahují hodnotu objValueOfField = instance
                                // nějaké třídy, na kterou má
                                // tato testovaná třída referenci zjistím si počet proměnných, v této třídě a
                                // počet
                                // proměnných v instanci objValueOfField, kolik obsahuje proměnných, které
                                // obsahují hodnotu této instance, tj. které obsahují hodnotu: objInstance:

                                // do kolekce přidám hodnotu, protože se zde poprvé a naposled otestuji pro
                                // případně vztahy a je ten
                                // algoritmus napsaný tak, aby se zde vypočíl potřebný ppočet hran,
                                // a nemuselo se sem již znovu vrace při další iteraci:
                                testedInstancesList.add(objValueOfField);


                                /*
                                 * Nyní, abych nějak nastavil, zda se mají zobrazovat vztahy instancí na sebe
                                 * sama. Takže pokud má instance (třída) vztah na sebe sama a ty vztahy se v
                                 * diagramu instancí nemají zobrazit, pak tu proměnnou vynechám, v ostaních
                                 * případech vztah dané proměnné přidám k testování pro zobrazení v diagramu
                                 * instancí.
                                 *
                                 * V této části kódu otestuji, zda se jedná o instanci, jejíž reprezentace je
                                 * také reprezentována v diagramu instancí a pokud ano, tj. proměnná desCell
                                 * není null, pak zjistím, zda se nejedná o tu samou buňku (instanci), pokud
                                 * ano, tak pokud se mají zobrazovat vztahy instancí (tříd) na sebe sama, pak
                                 * budu pokračovat, jinak přeskočím danou instanci a budu pokračovat další
                                 * iterací
                                 */

                                // Testování, zda se mají zobrazovat vztahy instnace sama na sebe:
                                if (objValueOfField.equals(objInstance) && !showRelationShipsToClassItself)
                                    continue;


                                else if (objValueOfField.equals(objInstance) && showRelationShipsToClassItself) {
                                    /*
                                     * Vztah sám na sebe v tomto typu proměnné je vždy agregace 1 : 1. Pro agragaci
                                     * 1 : N by to musel být například list nebo pole, ale to je testováno výše.
                                     */
                                    infoAboutEdgesList
                                            .add(new InfoAboutEdgesForCreate(selectedSrcCell, selectedSrcCell,
                                                    f.getName(), KindOfInstanceEdgeEnum.AGGREGATION_1_1));
                                    continue;
                                }







                                // Pro začátek si najdu cílovou buňku, ke které by se
                                // následně mohli připojit instance Zjistím si, zda nějaká je opravu v diagramu
                                // instancí
                                // bunka, která tuto instanci reprezentuje:
                                final DefaultGraphCell desCell = getSpecificCell(objValueOfField,
                                        allInstancesFromCd);

                                // Zde musím otestovat, zda v digramu instancí opravu ta buňka je, jinak by
                                // němělo smysl určovat hrany,
                                // když by je nebylo s kterou bunkou propojit:
                                if (desCell != null) {
                                    // Zde byla nalezena bunka v diagramu instanci, tak mohu pokracovat pro určení hran:

                                    // Počet proměnných v této třídě, které obsahují
                                    // instancí: objValueOfField:
                                    final List<String> countA = getCountOfVariablesFilledInstance(fieldsList,
                                            objValueOfField, objInstance);


                                    // Počet proměnných, které obsahuje reference
                                    // objValueOfField a zárověň, tato proměnná má referenci
                                    // na tuto testovanou třídu - objInstance
									final List<String> countB = getCountOfVariablesFilledInstance(
											getFieldsFromClasses(objValueOfField.getClass(), new ArrayList<>(),
													showRelationshipsInInheritedClasses),
											objInstance, objValueOfField);



                                    // Zjistím si rozdíl proměnných pro určení počtu agregací a počtu asociací mezi
                                    // instancemi:
                                    final int result = countA.size() - countB.size();








                                    /*
                                     * Zde zjistím, zda se má vztah typu asociace zobrazovat jako dva vztahy typu
                                     * agregace, pokud ano, tak stačí vatvořit pouze vztahy na tu druhou třídu, až
                                     * se bude iterovat ta druhá třída, tak se v ní vytvoří vztahy na tuto třídu a
                                     * bude to OK, a dále již není třeba řešit vztah asociace, tak zde budu
									 * pokracovat dalším cyklem (po přidání proměnných pro agragaci).
									 */
									if (showAssociationThroughAggregation) {
										for (final String aCountA : countA)
											infoAboutEdgesList.add(new InfoAboutEdgesForCreate(selectedSrcCell,
													desCell, aCountA,
													KindOfInstanceEdgeEnum.AGGREGATION_1_1));

										continue;
									}











                                    // Princip výpočtu:
                                    // jsou třída A a B, kde třída A je právě testovaná třída a třída B je hodnota práve testované proměnné (instance
                                    // jiné třídy z diagramu instancí)

                                    // - Zjistím si počet proměnných třídy A, které obsahují právě načtenou instanci z práve testované proměnné z třídy A

                                    // - Odečtu od sebe počet proměnných třídy A - B

                                    // - pokud je výsledek záporný nebo roven nule, pak se musí vytvřit počet asociací, kolik je počet proměnných
                                    // třídy A, protože jedna proměnná z třídy A ukazuje na třídu B a naopak (asociace), ale zbylě proměnné - v záporu
                                    // ukazují z třídy B na třídu A, ale to teď netestuji, až se dostane řada na třídu B

                                    // Dále pokud výsledek kladný ( > 0), pak se jedná o počet agregací 1 : 1, které se mají vytvořit z instance třídy A na instanci
                                    // třídu B
                                    // a zárovenˇpočet proměnných z B je počet asociací pro přidání ve stejném směru
                                    // protože Když je třeba A = 3, B = 1 (ve třídě A jsou 3 proměnné ukazjící na B, a ve tříd B je jedna proměnná ukazující na A)
                                    // pak 3 - 1 = 2, takže se mají vytvořit 2 agregace ve směru A -> B, a jedna asociace mezi A a B, protože je tam jedna proměnná,
                                    // která je symterická mezi třídami - proměnná, díky které o sobě třídy, resp. instance navzájem ví.

                                    /*
                                     * Note:
                                     * Bylo by možné zde využít stejný postup jako u diagramu tříd asice, že by zde
                                     * byla podmínka, která by otestovala, zda se jendá o "rychlý" nebo "pomalější"
                                     * postup pro testování vztahů mezi instancemi, ale v tomto konkrétním případě
                                     * to lze vyřešit jednodušení, protože v diagramu tříd se dávají pro vztah
                                     * asociace na popsiky hran vždy 1 : 1, tak tam nelze určit to, že se může vždy
                                     * vyskytovat jedna asociace s příslušnými proměnnými.
                                     *
                                     * Proto zde tu podmínku dávat nebudu (pro přehlednost a trochu i jednoduchost).
                                     * V tomto případě stačí pouze u vytváření vztahů typu asociacemezi třídami
                                     * otestovat, zda se mezi příslušnými instancemi již nachází hrana, která nese
                                     * příslušné názvy proměnných, protože každá proměnná může být využita pouze
                                     * jednou, takže pokud se v diagramu ještě příslušná hrana mezi příslušnými
                                     * instancemi s příslušnými proměnnými nenachází je možné ji přidat, v opačném
                                     * případě již byla přidána, tak není co řešit, prostě se nepřidám, už tam je.
                                     */

                                    if (result <= 0) {
                                        // Zde je třeba přidat tolik asociací, jako je v proměnné countA:
                                        for (int i = 0; i < countA.size(); i++)
                                            infoAboutEdgesList.add(new InfoAboutEdgesForCreate(selectedSrcCell,
                                                    desCell, countB.get(i), countA.get(i)));
                                    }


                                    // else if (result > 0) {
                                    else {
                                        // Zde výsledet - result značí počet hran typu agregace 1 : 1, které se mají
                                        // přidat
                                        // a počet proměnných v instanci, na kterou tato intance ukazuje, tj. počet
                                        // proměnných s danou
                                        // instací countB ta značí počet asociací pro přidání

                                        // Note:
                                        // pro popisek na konec hrany akorát zvýším index, protože by se jinak mohli
                                        // vyskytnou stejné popisky na
                                        // nálsedující hraně pro agregaci i níže pro asociace, protože by se pokaždé
                                        // iterovalo kolekcí counA
                                        // stejnými indexy od nuly, a v této části výpočtu by stačilo přidat index
                                        // coby počet proměnných v druhé straně
                                        // a od té iterovat, protože do této hodnoty od nuly bude iterovat cyklus
                                        // pro asociace, tak agregaci udělám
                                        // od nějaké hodnoty coby počet asociací do konce:

                                        for (int i = 0; i < result; i++)
                                            infoAboutEdgesList.add(new InfoAboutEdgesForCreate(selectedSrcCell,
                                                    desCell, countA.get(countB.size() + i),
                                                    KindOfInstanceEdgeEnum.AGGREGATION_1_1));

                                        for (int i = 0; i < countB.size(); i++)
                                            infoAboutEdgesList.add(new InfoAboutEdgesForCreate(selectedSrcCell,
                                                    desCell, countB.get(i), countA.get(i)));
                                    }
                                }
                            }
                        }
                    }





                    /*
                     * Zde otestuji, zda se má využít "rychlá" verze pro vytvoření vztahů, pokud
                     * ano, pak byly již veškeré vztahy mezi instancmi smazány před cyklem pro
                     * procházení vytvořených instancí, tak zde stačí pouze přidat aktuálně nalezené
                     * vztahy.
                     *
                     * Pak v části else se zjistí, jaké vztahy aktuálně v diagramu instancí existují
                     * a dle toho, zda mají nebo nemají existovat se přidají, odeberou nebo
                     * ponechají.
                     */
                    if (useFastWay) {
                        infoAboutEdgesList.forEach(i -> {
                            /*
                             * Nejprve otestuji, zda se jedná o vztah typu asociace, pokud ano, pak musím
                             * zjistit, kolik vztahů typu asociace již se již nachází mezi konkrétními
                             * buňkami (instancemi) v diagramu instancí, protože v tomto případě, kdy se
                             * využívá "rychlejší" cesta pro zjišťování vztahů může docházet k tomu, že se
                             * přidá vícekrát vztah asociace. Ale vztah asociace mezi dvěma třídami může být
                             * pouze jednou se stejnými proměnnými. Takže pokud se mezi příslušnými buňkami
                             * v diagramu instancí ještě nenachází vztah typu asociace s popisky s
                             * příslušnými proměnnými, pak jej mohu vložit a vždy tam může být maximláně
                             * jednou.
                             *
                             * Note:
                             * Druhá verze ("ta pomalejší") si již zjišťuje počty vztahů mezi instancemi,
                             * tak k tomu dojít ani nemůže.
                             */
                            if (i.getKindOfEdge().equals(KindOfInstanceEdgeEnum.ASSOCIATION)) {
                                final int count = GraphInstance.getCountOfEdgesBetweenCells(i.getSrcCell(),
                                        i.getDesCell(), i.getSrcSide(), i.getDesSide(), i.getKindOfEdge());

                                if (count < 1)
                                    instanceDiagram.addAssociationEdge(i.getSrcCell(), i.getDesCell(),
                                            i.getSrcSide(), i.getDesSide());
                            }

                            // Zde se již musí jednat o jeden z typů agregace:
                            else
                                instanceDiagram.addAggregationEdge(i.getSrcCell(), i.getDesCell(), i.getDesSide());
                        });
                    }




                    else {
                        // Zde napsat cyklus, ktery projde celou kolekci s objekty a vykresli je - pokud
                        // nejsou, případně smaže, pokud tam nemají být

                        // Cyklus, který projde kolekci, která obsahuje všechny hrany, které by měly být
                        // mezi právě testovanou buňkou
                        // a všemi ostatními, resp. na které jiné instance mí tato intance vztahy, resp.
                        // reference
                        for (final InfoAboutEdgesForCreate i : infoAboutEdgesList) {
                            // Zde si nejak hlídat počet hran mezi danými bunkami

                            // Postup:
                            // Zjistím si počet hran v kolekci infoAboutEdges - kolik hran s danými paraetry i by melo byt v grafu
                            // pak si zjistím počet hran s danými parametry, kolik jich je v diagramu instancí mezi bunkami

                            // jestliže se rozdíl: POČET HRAN V KOLEKCI (infoAboutEdgesList) minus POČET HRAN V GRAFU:
                            // jestli je nula, neni co řešit,
                            // jestli je zaporny, pak se má ten zápor převede do kladu a ten počet hran se má smazat
                            // jesltiže je to v kladu
                            // jestliže je vysledek kladny, tak tolik hran se tam ma pridat

                            // Note:
                            // Zde budu muset do diagramu zrovna hrany přidávat nebo odebírat a ne je dát někam bokem a pak přidat, protože:
                            // například v situaci, kdy je v kolekci infoAboutEdges dve hodnoty pro dvě stejné hrany, například pro kolekci či pole,
                            // při první iteraci se zjistí že v diagramu je jedna ale mají tam být dvě, takže by se jedna přidala do potenciální kolekce,
                            // při druhé iteraci by se zjistilo to samé a vložila by se do kolekce ta samá hrana už po druhé, ale měla by tam být pouze jednou!
                            // to by bylo špatně, a další podobné případy


                            // Note:
                            // Další řešení je vytvořit nějakou například statickou globální kolekce, která se vymaže před zašátku
                            // cyklu procházení všech instancí (na začátku této metody) a do této kolekce se budou postupně vkládat
                            // všechny hrany, které mají v diagramu existovat, pak po skončení cyklu, který prochází veškeré instance
                            // by se tato získaná statická kolekce prošla a obsahovala by pouze ty hrany, které by měly být v diagramu,
                            // takže by se při procházení této statické kolekce postpně všechny hrany vytvořily



                            // Proměnná, do které si zjistím počet hran daného typu mezi příslušnými
                            // buňkami, která se právě testuje
                            final int countOfEdgesInInstanceDiagram = GraphInstance.getCountOfEdgesBetweenCells(
                                    i.getSrcCell(), i.getDesCell(), i.getSrcSide(), i.getDesSide(),
                                    i.getKindOfEdge());


                            // počet hran se stejnými hodnotami jako je 'i' - cyklus, v kolekci
                            final int countOfEdgesInCollection = getCountOfEdgesInCollection(infoAboutEdgesList, i);


                            // rozdíl počtu hran v kolekci a počtu hran v diagramu instancí,
                            // abych věděl, kolik hran mám vytvořit, smazat, nebo nic nedělat:
                            final int resultInNumbers = countOfEdgesInCollection - countOfEdgesInInstanceDiagram;



                            // Jestliže je nula, není co řešit:

                            // Jinak jestliže je počet kladný ( > 0), tak se jedná o počet hran, který se má přidat
                            if (resultInNumbers > 0)
                                for (int q = 0; q < resultInNumbers; q++) {

                                    // Zde akorát otestuji jaký typ hrany se má přidat, zda asociace nebo agregace,
                                    // na konkrétních typech agregaci v tuto chvíli nezáleží to je v informacich v
                                    // kolekci infoAboutEdgesList a
                                    // konkrétní hrana je v i,

                                    // Podmínka, bud se jedná o asociace, a kdyz ne, tak to musí
                                    // být jeden z typů agregace:
                                    if (i.getKindOfEdge().equals(KindOfInstanceEdgeEnum.ASSOCIATION))
                                        instanceDiagram.addAssociationEdge(i.getSrcCell(), i.getDesCell(),
                                                i.getSrcSide(), i.getDesSide());

                                    // Zde se již musí jednat o jeden z typů agregace:
                                    else
                                        instanceDiagram.addAggregationEdge(i.getSrcCell(), i.getDesCell(),
                                                i.getDesSide());
                                }



                            // Note:
                            // Následující podmínka by zde již nemusel být, ale ušetřím si tím nějaké
                            // testování podmínek a urychlím tak procházení
                            // objektů v diagramu instancí kvůli mazání hran v následujícím cyklu, který
                            // smaže všechny přebytečné hrany:

                            // Jinak jestliže je počet záporný, tak se zneguje (ze záporného na kladné
                            // číslo) a toto číslo po negaci
                            // je počet hran s informacemi z 'i', které se mají z diagramu instancí smazat,
                            // tak je smažu:
                            else if (resultInNumbers < 0) {
                                for (int q = 0; q < -resultInNumbers; q++)
                                    instanceDiagram.removeSpecificEdgeFromDiagram(i);
                            }
                        }







                        // Na konec musím projít všechny hrany v diagramu instancí a zjistit, zda je v
                        // tomto diagramu nějaká hrana,
                        // která není v kolekci: infoAboutEdgesList, pokud takovou najdu, tak je třeba
                        // jí smazat.

                        // Toto může nastat v případě, že je kolekce naplněna referencemi na instance a
                        // pak se vymaže například pomocí metody
                        // clear, pak se ani do této kolekce ta hrana nedostane, tak se nesmaže jako
                        // "přebytečná hrana" v cyklu výše,

                        // Postup:
                        // Projdu všechny objekty v diagramu instancí, zjistím si, zda se jedná o hranu,
                        // pokud ano, tak si zjistím, zda její
                        // zdroj je "tato" buňka - instance: selected_src_Cell, pokud ano, tak jsem
                        // našel hranu, která reprezentuje, že má "tato"
                        // právě testovaná instance vztah na instanci, která je ca druhém konci hrany,
                        // tak musím otestovat, zda se tato hrana nachází
                        // ve zmíněné kolekci infoAboutEdgesList, otestuji, zdroj a cil te hrany, její
                        // typ a popisky, pokud tato hrana
                        // neodpovídá žádné hraně z kolekce infoAboutEdgesList, pak je třeba jí smazat,
                        // jinak se nic neřeší:

                        // Zavolám výše popsanou metodu, která smaže všchny hrany mezi aktuálně
                        // testovanou bunkou a všemi ostaními, a
                        // tyto hrany nejsou v kolekci: infoAboutEdgesList:
                        instanceDiagram.deleteSpecificEdges(selectedSrcCell, infoAboutEdgesList);
                    }
                }


                // Zde byly aktualizovány veškeré vztahy, tak mohu aktualizovat proměnné, které
                // jsou načtené v buňkách v diagramu instancí (v každé instancí):
                instanceDiagram.updateInstanceCellValues();
            });
			
		} catch (InvocationTargetException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informaci o chybě:
				logger.log(Level.INFO,
						"Zachycena výjimka při pokusu o zavolání SwingUtilities.invokeAndWait pro volání metod pro " +
								"znázornění vztahů mezi instancemi v diagramu instancí. "
								+ "Tato chyba může nastat například v případě, že je vyhozena výjimka pri pokusu o " +
								"spuštění programu doRun.");

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();

        } catch (InterruptedException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při pokusu o zavolání SwingUtilities.invokeAndWait pro volání metod pro " +
                                "znázornění vztahů mezi instancemi v diagramu instancí. "
                                + "Tato chyba může nastat například v případě, že pokud budeme přerušeni při čekaní " +
                                "na událost dispečování vlákna k dokončení spuštění příkazu doRun.run ().");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, kolik "klasických" proměnných z pole fieldsArray
	 * obsahuje hodnotu objInstance.
	 * 
	 * Pokud na tuto proměnnou narazí, tak vloží do kolekce název té proměnné, aby
	 * se mohllo na příslušné hraně zobrazit, která proměnná na co ukazuje.
	 * 
	 * Jelikož se jedná o "klasickou" proměnnou, tj. například: private SomeType
	 * someName; tj. není to List - kolekce, ani pole, ajiná datová struktura
	 * 
	 * @param fieldsList
	 *            - list typu Field coby všechny proměnné z nějaké třídy, které
	 *            projdu a pokud neni hodnota null, tak otestuji, zda je daná
	 *            proměnná normální, tj. není pole ani kolekce, a zda obsahuje
	 *            hodnotu objInstance pokud tomu tak je, tak se zvýši počet
	 *            proměnných
	 * 
	 * @param objInstance
	 *            - instance nějaké třídy, o které chcí vědět, kolik proměnných na
	 *            ní ukazuje
	 * 
	 * @param ownerOfField
	 *            - instance, neboli vlastník pole fieldsArray, ze kterého se mají
	 *            brát položky
	 * 
	 * @return Vrátí list typu String, který obsahuje názvy proměnných, které
	 *         obsahuje reference na instanci objInstance a velikost kolekce je
	 *         zároveň počet proměnných.
	 */
	private static List<String> getCountOfVariablesFilledInstance(final List<Field> fieldsList, final Object objInstance,
			final Object ownerOfField) {

		final List<String> list = new ArrayList<>();

		for (final Field f : fieldsList) {
			final Object objValue = ReflectionHelper.getValueFromField(f, ownerOfField);

			if (objValue != null && !f.getType().isArray() && !ReflectionHelper.isDataTypeOfList(f.getType()) && objValue.equals(objInstance))
				list.add(f.getName());
		}

		return list;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se instance v parametru metody 'o' nachází v mapě s
	 * instancemi vytvořenými uživatelem a tato instance je reprezentována v
	 * diagramu instancí nějaoku buňkou, pokud ano, tak bude tato buňka nalezena a
	 * vrácena, pokud ne, vrátí se null.
	 * 
	 * @param o
	 *            - Reference na nějakou instancí třídy nebo null
	 * 
	 * @param allInstancesFromCd
	 *            - reference na hashMapu, která obsahuje všechny instance tříd z
	 *            diagramu tříd, které uživatel vytvořil
	 * 
	 * @return null, pokud neexistuje reference 'o' v diagramuinstancí nebo v mapě s
	 *         instancemi, jinak konkrétní buku z diagramu instancí, která danou
	 *         instanci reprezentuje v tomto diagramu.
	 */
	private static DefaultGraphCell getSpecificCell(final Object o, final Map<String, Object> allInstancesFromCd) {
		final String referenceToInstance;

		if (o != null && (referenceToInstance = getReferenceInMapWithAllInstances(o, allInstancesFromCd)) != null)
			// Zde se jedná o referenci z instnce třídy, která má referenci i na nějakou z
			// diagramu
			// instancí, resp. aktuálně procházený objekt o je nějaká instance v diagramu
			// instancí a v mapě allInstancesFromCD

			// tak si zjistím příslušnou buňku v diagramu instancí, která obsahuje text té
			// reference:
			return GraphInstance.getCellWithText(referenceToInstance);

		return null;
	}
	

	
	
	
	
	
	

	
	/**
	 * Metoda, která projde celé pole - nezáleží na počtu dimení a každou získanou
	 * hodnotu z pole otestuje, zda není null, pokud ne, tak se tento nalezený
	 * objekt, resp. instanci v poli vloží do kolekce - List:
	 * tempListForInstancesFromArray, od kud si je po dokončení metody zase
	 * "vyzvedne" pokračování metody run aby se otestovaly zda jsou nějaké hodnoty v
	 * této kolekci (získané z pole) v diagramu tříd a mohli se mezi nimi nějak
	 * "natahat" hrany
	 * 
	 * @param array
	 *            - objekt, který představuje X - rozměrné pole z instance třídy
	 */
	private void addInstancesFromArrayToList(final Object array) {
		// zjistím si velikost pole:
		final int length = Array.getLength(array);
		
		// celé ho "projedu" - proiteruji a otestuji, zda je hodnota v poli další pole nebo
		// již konkrétní hodnota, pokud je to další pole, rekurzivně pokračuji zavoláním téže metody,
		// jinak se jedná o konkrétní hodnotu a sice nějakou instanci nějaké třídy, tak ji vložím do kolekce
		// kde později otestuji, zda se jedná o instanci nějaké třídy z diagramu tříd, pro potenciální vytvoření či editaci
		// nějakých vztahů mezi danými instancemi v diagramu instancí:
		for (int i = 0; i < length; i++) {
			// Získám si hodnotu z pole na konkrétním indexu:
			final Object objValue = Array.get(array, i);
			
			// Otestuji, zda není získaná hodnota z pole null:
			if (objValue != null) {
				// Pokud je to další pole, rekurzí pokračuji
				if (objValue.getClass().isArray())
					addInstancesFromArrayToList(objValue);
				
				// zde je to konkrétní hodnota, tak ji přidám do kolekce:
				else tempListForInstancesFromArray.add(objValue);
			}
		}
	}
	
	
	
	
	

	
	
	
	/**
	 * Metoda, která zjistí kolik hran příslušného typu se má vytvořit, resp. kolik
	 * hran - stejných - se stejnými daty (bunkami, popisky, ....) se nachází v
	 * jedné kolekci
	 * 
	 * @param infoList
	 *            - kolekce typu InfoAboutEdgesForCreate, ve které mám zjistit kolik
	 *            obsahuje hran se stejnými informacemi jako je v proměnné info
	 * 
	 * @param info
	 *            - proměnná typu InfoAboutEdgesForCreate, která obsahuje informace
	 *            o hraně, o které chci zjistit, kolikrát se tato hrana nachází v
	 *            kolekci, - kolikrát má mezi příslušnými buňkami existovat
	 * 
	 * @return počet hran, které se nachází v kolekci infoList a obsahují stené
	 *         informace jako v proměnné info
	 * 
	 *         Note: budou se vracet počet hran včetně té v parametru info
	 */
	private static int getCountOfEdgesInCollection(final List<InfoAboutEdgesForCreate> infoList,
			final InfoAboutEdgesForCreate info) {
		int count = 0;

		for (final InfoAboutEdgesForCreate i : infoList) {
			if (i.getSrcCell() == info.getSrcCell() && i.getDesCell() == info.getDesCell()
					&& i.getDesSide().equals(info.getDesSide()) && i.getKindOfEdge().equals(info.getKindOfEdge()))
				count++;
		}

		return count;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se v mapě se všemi instancemi, resp. instancemi v
	 * diagramu tříd vyskytuje instance v parametru této metody, pokud ano, vráti se
	 * text, který ukazuje na příslušnou instanci v diagramu instancí, pokud ne,
	 * vrátí se null.
	 * 
	 * 
	 * @param objReference
	 *            - refenrece získaná z instnace nějaké třídy, a chci o ní vědět,
	 *            zda se nachází v diagramu tříd, resp. v mapě s instancemi v
	 *            diagramu tříd
	 * 
	 * @param instancesMap
	 *            - mapa, která obsahuje všechny instance, vytvořené uživatelem
	 * 
	 * @return true, pokud je instance - objReference v mapě instancesMap, jinak
	 *         false
	 */
	private static String getReferenceInMapWithAllInstances(final Object objReference,
			final Map<String, Object> instancesMap) {
		for (final Map.Entry<String, Object> map : instancesMap.entrySet()) {
			if (map.getValue().equals(objReference))
				return map.getKey();
		}

		return null;
	}


    /**
     * Metoda, která zjistí, zda je typ položky nějaká třída, jejíž instance se nachází v mapě s instancemi tříd, resp.
     * jedná se o typ položky, ten typ je třída v diagramu tříd, a instancí této třídy již uživatel vytvořil a její
     * reprezentace se nachází v diagramu instancí.
     * <p>
     * Metoda to zjistí porovnáváním textů, složí si balíčky třídy a její název do podoby textu balíčků, tj.
     * model.data.ClassName a to samé si zjistí i u příslušné instance v mapě a tyto texty porovná, pokud narazí na
     * první instanci, které se shodují, pak metoda vrátí true, jako že se v diagramu instancí nachází alespoň jedna
     * instance, která by se mohla nacházet jako reference v dané proměnné.
     * <p>
     * Jinak pokud se projde celá mapa a nenajde se shoda, pak metoda vrátí false.
     *
     * @param field
     *         - položka o jejímž datovém typu chcí vědět, zda je to typ třídy v diagramu tříd a instance této třídy již
     *         existuje v diagramu instancí
     * @param instancesMap
     *         - mapa s instancemi tříd z diagramu tříd, instance, které vytvořil uživatel
     *
     * @return true, pokud se nachází instance třídy typu té položky field v mapě, resp. v diagramu instancí je jeí
     * reprezentace, jinak false
     */
    private static boolean isKindOfClassInMap(final Field field, final Map<String, Object> instancesMap) {
        final String fieldType;

        /*
         * Zde musím otestovat, zda se jedná o pole, list, nebo "klasickou" proměnnou, jinak, kdyby to byl například
         * list typu nějaké třídy, tak by se vrátil například: java.util.variableName, ale ja potřebuji typ toho
         * listu, ne typ list!
         */

        // Nejprve otestuji, zda se jedná o proměnnou typu kolekce, v takovém případě si vezmu typ té kolekce:
        if (ReflectionHelper.isDataTypeOfList(field.getType())) {
            // zde se jedná o List, tak si zjistím jeho typ:
            final DataTypeOfList tempListType = ReflectionHelper.getDataTypeOfList(field);

            if (tempListType.getDataTypeOfListEnum() == DataTypeOfListEnum.SPECIFIC_DATA_TYPE)
                fieldType = ParameterToText.getDataTypeInText(tempListType.getDataType(), true);
            else fieldType = null;
        }


        // Zde otestuji zda se jedná o pole, nezajímá mě kolik má dimezí, ale jeho typ:
        else if (field.getType().isArray()) {
            /*
             * Zde se jedná o pole, tak je třeba zjistit jeho typ (poslední dimenzi) a o tento typ převést do textu a
             * o něm se dále zjistí, zda se jedná o syntaxi pro název třídy v diagramu tříd - resp. instanci z třídy
             * v diagramu tříd.
             */
            final Class<?> lastDimensionOfArray = ReflectionHelper.getDataTypeOfArray(field.getType());
            fieldType = ParameterToText.getDataTypeInText(lastDimensionOfArray, true);
        }


        /*
         * Zde se jedná o klasickou proměnnou, tak se převede její typ do textu (Mapa se nebere pro zjišťování, zda
         * je to třída z diagramu tříd):
         */
        else
            fieldType = ParameterToText.getFieldInText(field, true);


        if (fieldType == null)
            return false;

        // Projdu všechny instance:
        for (final Map.Entry<String, Object> map : instancesMap.entrySet()) {
            // zjistím si konkrétní instanci:
            final Object mapValue = map.getValue();

            // Název třídy (instance) s balíčky:
            final String instanceText = mapValue.getClass().getName();

            if (fieldType.equals(instanceText))
                return true;
        }

        return false;
    }
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která prohledá si načte veškeré proměnné z třídy clazz a pokud je
	 * proměnná loadFieldFromInheritedClasses true, pak se načtou i veškeré proměnné
	 * i ze všech předků třídy clazz až po třidu Object. Z té už se proměnné brát
	 * nebudou.
	 * 
	 * V tomto případě se berou veškeré proměnné, jak statické, tak i soukromé,
	 * chráněné a veřejné.
	 * 
	 * @param clazz
	 *            - třída, ze které se mají vzít veškeré proěmnné.
	 * 
	 * @param fieldsList
	 *            - list, do kterého se budou vkládat veškeré nalezené proěmnné.
	 * 
	 * @param loadFieldFromInheritedClasses
	 *            - logická hodnota o tom, zda se mají brát i veškeré proměnné ze
	 *            všech předků třídy clazz (hodnota true). Pokud zde bude hodnota
	 *            false, pak se načtou pouze proměnné z třídy clazz a dále, tedy
	 *            její předkové (třídy clazz) se již prohledávat nebudou.
	 * 
	 * @return list, který bude obsahovat veškeré nalezené proměnné z třídy clazz a
	 *         pokud je proměnné loadFieldFromInheritedClasses true, pak i ze všech
	 *         jejích předků až po třídu Object.
	 */
	private static List<Field> getFieldsFromClasses(final Class<?> clazz, final List<Field> fieldsList,
			final boolean loadFieldFromInheritedClasses) {
		if (clazz == null || clazz.equals(Object.class))
			return fieldsList;

		if (clazz.getDeclaredFields() != null)
            fieldsList.addAll(Arrays.asList(clazz.getDeclaredFields()));

		if (loadFieldFromInheritedClasses && clazz.getSuperclass() != null
				&& !clazz.getSuperclass().equals(Object.class))
			getFieldsFromClasses(clazz.getSuperclass(), fieldsList, loadFieldFromInheritedClasses);

		return fieldsList;
	}
}