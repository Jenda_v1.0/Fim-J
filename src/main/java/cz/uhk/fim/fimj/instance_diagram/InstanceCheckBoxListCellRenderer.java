package cz.uhk.fim.fimj.instance_diagram;

import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * Tato třída slouží jako Renderer pro vykreslování komponent JCheckBox v komponentě JList.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class InstanceCheckBoxListCellRenderer extends JCheckBox implements ListCellRenderer<InstanceAndCheckBox> {

    private static final long serialVersionUID = 1L;

    @Override
    public Component getListCellRendererComponent(JList<? extends InstanceAndCheckBox> list, InstanceAndCheckBox value,
                                                  int index, boolean isSelected, boolean cellHasFocus) {
        setComponentOrientation(list.getComponentOrientation());
        setFont(list.getFont());
        setBackground(list.getBackground());
        setForeground(list.getForeground());
        setSelected(value.isChcbSelected());
        setEnabled(list.isEnabled());

        setText(value.getText());

        return this;
    }
}
