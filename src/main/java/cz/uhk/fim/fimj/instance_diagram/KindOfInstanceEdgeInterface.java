package cz.uhk.fim.fimj.instance_diagram;

import java.util.Properties;

import org.jgraph.graph.DefaultEdge;

/**
 * Tato třída coby rozhraní slouží pro deklaraci a popis metod, které slouží pro zjištění typů hran v diagramu instancí
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface KindOfInstanceEdgeInterface {

    /**
     * Metoda, která otestuje, zda se jedná o hranu, která v diagramu instancí reprezentuje vztah asociace, tato hrana
     * má šipky na obou stranách a na obou stranách má také texty s proměnnými.
     *
     * @param edge
     *         - hrana o které chci zjistit, zda reprezentuje vztah typu asociace mezi instancemi v diagramu instancí
     * @param srcSide
     *         - text, která by se měl nacházet na začátku hrany
     * @param desSide
     *         - text, který by se měl acházet na konci hrany
     * @return true, pokud hrana edge reprezentuje vztah typu asociace v diagramu instancí, jinak false
     */
    boolean isAssociation(final DefaultEdge edge, final String srcSide, final String desSide);


    /**
     * Metoda, která otestuje, zda e jedná o hranu, která v diagramu instancí reprezentuje vztah typu agregace. taté
     * hrana má šipku pouze na jedné straně - na straně cíle a na této straně má i text s názvem proměnné.
     * <p>
     * Metoda porovná všechny vlastnosti této hrany a otestuji, zda jim hrana edge odpovídá
     * <p>
     * Metoda nerozeznává agregaci 1:1 nebo 1:N, vypadají stejně a liší se akorát jejich text na konci hrany
     *
     * @param edge
     *         - hrana, o které má metoda zjistit, zda je typu agregace
     * @param desSide
     *         - text, který by se měl nacházet na konci hrany
     * @return true, pokud hrana odpovídá potřeným parametrům, jinak false
     */
    boolean isAggregation(final DefaultEdge edge, final String desSide);


    /**
     * Metoda, která slouží pro vložení hodnoty value ke klíči key v objektu Properties, který obsahuje veškeré
     * nastavení pro diagram instacní, toto je potřeba, hlavně, když uživatel změní pomocí "rychlého" způsobu změny
     * velikost písma, tak aby se pak při práci s diagramem instancí rozpoznávaly příslušné objekty v diagramu
     * instancí,
     * tak veškeré provedené změny musím změnit i v tomto objektu, jinak by se nerozeznaly objekty v diagramu instnací.
     *
     * @param key
     *         - klíč, který se má vložit do mapy Properties, která obsahuje informace ohledně nastavení diagramu
     *         instancí.
     * @param value
     *         - hodnota, která se má nastavit na klíč key v mapě Properties s informacemi ohledně nastavení diagramu
     *         instancí.
     */
    void editValueInProperties(final String key, final String value);


    /**
     * Metoda, která vrátí objekt Properties, který obsahuje informace ohledně nastavení diagramu instancí, tj. jaké
     * obsahuje obejty, resp. ty objekty, jaké mají nastavení apod. Aby se mohli rozeznávat.
     *
     * @return objekt Properties obsahující informace ohledně nastavení diagramu instancí.
     */
    Properties getProperties();


    /**
     * Metoda, která slouží pro přenačtení aktuálně načteného objektu Properties obsahující nastavení pro diagram
     * instancí.
     * <p>
     * Toto je potřeba například v případě, že se při spuštění aplikace nachází nějaká chyba nebo duplicita v
     * příslušném
     * konfiguračním souboru, tak aby se ty data nenačetla chabně, tak je potřeba jej přenačíst, až když je jistot, že
     * ty data jsou správně vytvořena.
     */
    void reloadProperties();
}