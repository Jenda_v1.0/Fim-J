package cz.uhk.fim.fimj.instance_diagram.overview_of_instance;

/**
 * Tato abstraktní třída slouží pro deklaraci některých společných proměnných a aby se do příslušné komponenty JList
 * mohl nastavit jen tento objekt a v tom listu mohli být veškeré potomci této třídy.
 * <p>
 * Jedná se o to, že v dialogu pro zobrazení přehledu hodnot o nějaké instanci v diagramu instancí je možné například
 * zavolat metodu nebo vytvořit referenci na instanci třídy z diagramu tříd, která se ještě nenachází v diagramu instací
 * apod..
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class ValuesInfo implements Comparable {

    /**
     * Hodnota / Znak, který bude oddělovat název třídy od konkrétní metody, proměnné, konstruktoru apod.
     */
    private static final String SEPARATOR = "#";


    /**
     * Proměnná, která obsahuje pouze text, která se má zobrazit v příslušném JListu, tato proměnná obsahuje text
     * příslušné proměnné / metody, resp. příslušná proměnná / metoda v příslušné syntaxi, aby uživatel viděl tu
     * proměnnou / metodu a její hodnotu / parametry.
     */
    final String finalTextForShow;

    /**
     * Třída, ze které byla získána proměnná / metoda / konstruktor apod.
     * <p>
     * Je potřeba například pro sestavení reference na proměnnou pro zkopírování do schránky apod.
     */
    protected final Class<?> clazz;


    /**
     * Konstruktor této třídy (pouze pro naplnění proměnných).
     *
     * @param finalTextForShow
     *         - texty, který bude příslušná položka zobrazovat v komponentě typu JList v dialogu pro přehled hodnot o
     *         příslušné instanci.
     * @param clazz
     *         - třída, ze které byla získána proměnná / metoda / konstruktor apod. Je potřeba například pro sestavení
     *         reference na zmíněný objekt pro vložení reference do schránky apod.
     */
    ValuesInfo(final String finalTextForShow, final Class<?> clazz) {
        this.finalTextForShow = finalTextForShow;
        this.clazz = clazz;
    }


    /**
     * Sestavení textu, který se bude vkládat do schránky (Ctrl + C).
     * <p>
     * Jedná se o text, který značí reference na například konstruktor, metodu, proměnnou apod. Dle každé třídy se
     * sestaví text, který bude značit například referenci na proměnnou apod.
     *
     * @return text, který bude značit referenci na píslušný objekt (metoda, proměnná, ...). Jedná se o text, pro
     * zkopírování do schránky. Bude se jednat o syntaxi: název třídy s balíčky, pak hashtag, pak název proměnné nebo
     * konsturktoru nebo metody.
     * <p>
     * Pokud je metoda přetížená v příslušné třídě, budou doplněny i její parametry, stejně tak pro konstruktor.
     */
    abstract String getReferenceTextToClipboard();


    /**
     * Získání názvu třídy s balíčky kde se nachází a na konci hashtag, za který se připojí například název proměnné,
     * metody apod..
     * <p>
     * V případě, že se jedná o vnitřní třídu, budou nahrazeny dolary za desetinné tečky.
     *
     * @param clazz
     *         - třída, která se má převést do textové podoby tak, že budou nejprve balíčky, kde se třída nachází
     *         oddělené desetinnými tečkami, pak název třídy, pak případně vnitřní třídy také oddělené desetinnou tečkou
     *         a pak hashtag.
     *
     * @return výše popsanou syntaxi obsahující název třídy s balíčky, případně i název / názvy vnitřních tříd oddělené
     * desetinnou tečkou a hashtag na konci.
     */
    static String getTextOfClassForReference(final Class<?> clazz) {
        final String classNameWithDots = clazz.getName().replace("$", ".");

        return classNameWithDots + SEPARATOR;
    }

    @Override
    public String toString() {
        return finalTextForShow;
    }
}
