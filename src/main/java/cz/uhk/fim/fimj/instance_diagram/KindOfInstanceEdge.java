package cz.uhk.fim.fimj.instance_diagram;

import java.awt.Font;
import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.GraphConstants;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.Constants;

/**
 * Tato třída obsahuje metody pro zjisštění typu hran, v daigramu instancí, typy hran jsou popsány u jednotlivých metod
 * <p>
 * Každá metoda si zjistí, jaké parametry by měla daná hrana mít, například barvu, šířku, popisek, ... a na základě
 * těchto vlastností se rozhodne o typu hrany.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class KindOfInstanceEdge implements KindOfInstanceEdgeInterface {

	/**
	 * Soubor typu Properties, který by se měl načist, abych mohl kontrolovat hrany
	 */
	private static Properties instanceDiagramProp;
	
	

	/**
	 * Konstruktor této třídy.
	 */
	public KindOfInstanceEdge() {
		super();
		
		instanceDiagramProp = App.READ_FILE.getInstanceDiagramProperties();
	}


	

	@Override
	public boolean isAssociation(final DefaultEdge edge, final String srcSide, final String desSide) {		
		final Map<?, ?> attributes = edge.getAttributes();
		
		if (instanceDiagramProp != null) {
			final String lineColor = instanceDiagramProp.getProperty("AssociationEdgeLineColor", Integer.toString(Constants.ID_ASSOCIATION_EDGE_COLOR.getRGB()));			
			if (GraphConstants.getLineColor(attributes).getRGB() != Integer.parseInt(lineColor))
				return false;
					
			
			final boolean labelsAlongEdge = Boolean.parseBoolean(instanceDiagramProp.getProperty("AssociationEdgeLabelsAlongEdge", String.valueOf(Constants.ID_ASSOCIATION_LABELS_ALONG_EDGE)));
			if (GraphConstants.isLabelAlongEdge(attributes) != labelsAlongEdge)
				return false;
			
			
			final int lineEnd = Integer.parseInt(instanceDiagramProp.getProperty("AssociationEdgeLineEnd", Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_END)));
			if (GraphConstants.getLineEnd(attributes) != lineEnd)
				return false;
			
			
			final int lineBegin = Integer.parseInt(instanceDiagramProp.getProperty("AssociationEdgeLineBegin", Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_BEGIN)));
			if (GraphConstants.getLineBegin(attributes) != lineBegin)
				return false;
			
			
			final float lineWidth = Float.parseFloat(instanceDiagramProp.getProperty("AssociationEdgeLineWidth", String.valueOf(Constants.ID_ASSOCIATION_EDGE_LINE_WIDTH)));
			if (GraphConstants.getLineWidth(attributes) != lineWidth)
				return false;
			
			
			final boolean endFill = Boolean.parseBoolean(instanceDiagramProp.getProperty("AssociationEdgeLineEndFill", String.valueOf(Constants.ID_ASSOCIATION_EDGE_LINE_END_FILL)));
			if (GraphConstants.isEndFill(attributes) != endFill)
				return false;
			
			
			final boolean beginFill = Boolean.parseBoolean(instanceDiagramProp.getProperty("AssociationEdgeLineBeginFill", String.valueOf(Constants.ID_ASSOCIATION_EDGE_LINE_BEGIN_FILL)));
			if (GraphConstants.isBeginFill(attributes) != beginFill)
				return false;
			
			
			final String fontColor = instanceDiagramProp.getProperty("AssociationEdgeLineTextColor", Integer.toString(Constants.ID_ASSOCIATION_EDGE_FONT_COLOR.getRGB()));
			if (GraphConstants.getForeground(attributes).getRGB() != Integer.parseInt(fontColor))
				return false;
			
			
			// Font poznámky na hraně:
			final int fontSize = Integer.parseInt(instanceDiagramProp.getProperty("AssociationEdgeLineFontSize", Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getSize())));
			final int fontStyle = Integer.parseInt(instanceDiagramProp.getProperty("AssociationEdgeLineFontStyle", Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getStyle())));
			
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getName(), fontStyle, fontSize)))
				return false;
		}
		
		
		else {
			if (GraphConstants.getLineColor(attributes).getRGB() != Constants.ID_ASSOCIATION_EDGE_COLOR.getRGB())
				return false;
			
			if (GraphConstants.isLabelAlongEdge(attributes) != Constants.ID_ASSOCIATION_LABELS_ALONG_EDGE)
				return false;
					
						
			if (GraphConstants.getLineEnd(attributes) != Constants.ID_ASSOCIATION_EDGE_LINE_END)
				return false;
			
			if (GraphConstants.getLineBegin(attributes) != Constants.ID_ASSOCIATION_EDGE_LINE_BEGIN)
				return false;
			
			if (GraphConstants.getLineWidth(attributes) != Constants.ID_ASSOCIATION_EDGE_LINE_WIDTH)
				return false;
			
			if (GraphConstants.isEndFill(attributes) != Constants.ID_ASSOCIATION_EDGE_LINE_END_FILL)
				return false;
						
			if (GraphConstants.isBeginFill(attributes) != Constants.ID_ASSOCIATION_EDGE_LINE_BEGIN_FILL)
				return false;
			
			if (GraphConstants.getForeground(attributes).getRGB() != Constants.ID_ASSOCIATION_EDGE_FONT_COLOR.getRGB())
				return false;
			
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getName(),
					Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getStyle(), Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getSize())))
				return false;
		}
		
		
		final Object[] labels = {desSide, srcSide};
		final Object[] labels2 = GraphConstants.getExtraLabels(attributes);
		
		

		// Porovnám pole:
		if (!Arrays.equals(labels, labels2))
			return false;
		
		final Point2D[] points = { new Point2D.Double(GraphConstants.PERMILLE * 7 / 8, -20),
				new Point2D.Double(GraphConstants.PERMILLE / 8, -20) };
		final Point2D[] points2 = GraphConstants.getExtraLabelPositions(attributes);
		
		if (!Arrays.equals(points, points2))
			return false;
		
		
		// Kromě hrany reprezentující implementaci rozhraní nemá žádný jiná hrana čerchovaný styl čáry - bude null:
		float[] pattern = GraphConstants.getDashPattern(attributes);

		return Arrays.equals(pattern, null);
	}


	
	
	
	
	
	

	@Override
	public boolean isAggregation(final DefaultEdge edge, final String desSide) {		
		final Map<?, ?> attributes = edge.getAttributes();
		
		if (instanceDiagramProp != null) {
			final String lineColor = instanceDiagramProp.getProperty("AggregationEdgeLineColor", Integer.toString(Constants.ID_AGGREGATION_EDGE_COLOR.getRGB()));			
			if (GraphConstants.getLineColor(attributes).getRGB() != Integer.parseInt(lineColor))
				return false;
					
			
			final boolean labelsAlongEdge = Boolean.parseBoolean(instanceDiagramProp.getProperty("AggregationEdgeLabelsAlongEdge", String.valueOf(Constants.ID_AGGREGATION_LABELS_ALONG_EDGE)));
			if (GraphConstants.isLabelAlongEdge(attributes) != labelsAlongEdge)
				return false;
			
			
			final int lineEnd = Integer.parseInt(instanceDiagramProp.getProperty("AggregationEdgeLineEnd", Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_END)));
			if (GraphConstants.getLineEnd(attributes) != lineEnd)
				return false;
			
			
			final int lineBegin = Integer.parseInt(instanceDiagramProp.getProperty("AggregationEdgeLineBegin", Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_BEGIN)));
			if (GraphConstants.getLineBegin(attributes) != lineBegin)
				return false;
			
			
			final float lineWidth = Float.parseFloat(instanceDiagramProp.getProperty("AggregationEdgeLineWidth", String.valueOf(Constants.ID_AGGREGATION_EDGE_LINE_WIDTH)));
			if (GraphConstants.getLineWidth(attributes) != lineWidth)
				return false;
			
			
			final boolean endFill = Boolean.parseBoolean(instanceDiagramProp.getProperty("AggregationEdgeLineEndFill", String.valueOf(Constants.ID_AGGREGATION_EDGE_LINE_END_FILL)));
			if (GraphConstants.isEndFill(attributes) != endFill)
				return false;
			
			
			final boolean beginFill = Boolean.parseBoolean(instanceDiagramProp.getProperty("AggregationEdgeLineBeginFill", String.valueOf(Constants.ID_AGGREGATION_EDGE_LINE_BEGIN_FILL)));
			if (GraphConstants.isBeginFill(attributes) != beginFill)
				return false;
			
			
			final String fontColor = instanceDiagramProp.getProperty("AggregationEdgeLineTextColor", Integer.toString(Constants.ID_AGGREGATION_EDGE_FONT_COLOR.getRGB()));
			if (GraphConstants.getForeground(attributes).getRGB() != Integer.parseInt(fontColor))
				return false;
			
			
			// Font poznámky na hraně:
			final int fontSize = Integer.parseInt(instanceDiagramProp.getProperty("AggregationEdgeLineFontSize", Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getSize())));
			final int fontStyle = Integer.parseInt(instanceDiagramProp.getProperty("AggregationEdgeLineFontStyle", Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getStyle())));
			
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getName(), fontStyle, fontSize)))
				return false;
		}
		
		
		else {
			if (GraphConstants.getLineColor(attributes).getRGB() != Constants.ID_AGGREGATION_EDGE_COLOR.getRGB())
				return false;
			
			if (GraphConstants.isLabelAlongEdge(attributes) != Constants.ID_AGGREGATION_LABELS_ALONG_EDGE)
				return false;
			
			if (GraphConstants.getLineEnd(attributes) != Constants.ID_AGGREGATION_EDGE_LINE_END)
				return false;
			
			if (GraphConstants.getLineBegin(attributes) != Constants.ID_AGGREGATION_EDGE_LINE_BEGIN)
				return false;
			
			if (GraphConstants.getLineWidth(attributes) != Constants.ID_AGGREGATION_EDGE_LINE_WIDTH)
				return false;
			
			if (GraphConstants.isEndFill(attributes) != Constants.ID_AGGREGATION_EDGE_LINE_END_FILL)
				return false;
						
			if (GraphConstants.isBeginFill(attributes) != Constants.ID_AGGREGATION_EDGE_LINE_BEGIN_FILL)
				return false;
			
			if (GraphConstants.getForeground(attributes).getRGB() != Constants.ID_AGGREGATION_EDGE_FONT_COLOR.getRGB())
				return false;
			
			
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getName(),
					Constants.ID_AGGREGATION_EDGE_LINE_FONT.getStyle(), Constants.ID_AGGREGATION_EDGE_LINE_FONT.getSize())))
				return false;
		}
		
		
		final Object[] labels = {desSide, ""};
		final Object[] labels2 = GraphConstants.getExtraLabels(attributes);

		
		
		// Porovnám pole:
		if (!Arrays.equals(labels, labels2))
			return false;
		
		
		final Point2D[] points = { new Point2D.Double(GraphConstants.PERMILLE * 7 / 8, -20),
				new Point2D.Double(GraphConstants.PERMILLE / 8, -20) };
		final Point2D[] points2 = GraphConstants.getExtraLabelPositions(attributes);
		
		if (!Arrays.equals(points, points2))
			return false;
		
		
		// Kromě hrany reprezentující implementaci rozhraní nemá žádný jiná hrana čerchovaný styl čáry - bude null:
		float[] pattern = GraphConstants.getDashPattern(attributes);

		return Arrays.equals(pattern, null);
	}




	@Override
	public void editValueInProperties(String key, String value) {
		instanceDiagramProp.setProperty(key, value);
	}


	@Override
	public Properties getProperties() {
		return instanceDiagramProp;
	}


	@Override
	public void reloadProperties() {
		instanceDiagramProp = App.READ_FILE.getInstanceDiagramProperties();
	}
}