package cz.uhk.fim.fimj.instance_diagram;

import java.awt.Font;
import java.io.File;
import java.util.Map;
import java.util.Properties;

import javax.swing.BorderFactory;

import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.GraphConstants;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.ConfigurationFiles;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.file.WriteToFile;
import cz.uhk.fim.fimj.language.EditProperties;

/**
 * Tato třída obsahuje pouze metody, které slouží pro "rychlé" nastavení velikosti písma pro objekty, které se nachází v
 * diagramu instancí.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class FontSizeOperation_ID implements FontSizeOperation_ID_Interface {

	@Override
	public void quickSetupOfFontSize(final int size, final GraphInstance instanceDiagram) {
		/*
		 * Získám si aktuálně využívaný objekt Properties s hodnotami pro diagram
		 * instancí.
		 */
		final Properties prop = GraphInstance.KIND_OF_EDGE.getProperties();
		
		
		
		/*
		 * Tato podmínka by neměla nikdy nastat, pouze jako potenciální prevence.
		 */
		if (prop == null)
			return;
		
		
		
		/*
		 * Získám si typ fontu / písma pro zvětšení velikosti pro příslušné objekty v
		 * diagramu instancí.
		 */
		final int fontStyleAgregation = Integer.parseInt(prop.getProperty("AggregationEdgeLineFontStyle",
				Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getStyle())));
		final int fontStyleAssociation = Integer.parseInt(prop.getProperty("AssociationEdgeLineFontStyle",
				Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getStyle())));
		
		
		
		
		/*
		 * Velikost písma pro buňku reprezentující třídu musím nastavit již zde, protože
		 * se pouze předává objekt Properties do instance RoundedBorderLine, kde se
		 * berou potřebné hodnoty, kdybych tuto velikost písma nastavil až po cyklu, tak
		 * by se nastavila vždy ta "předchozí" velikost při posouvání posuvníku, ne ta
		 * aktuální nastavená velikost pomocí posuvníku.
		 */
		GraphInstance.KIND_OF_EDGE.editValueInProperties("FontSize", String.valueOf(size));

		// Velikost písma pro atributy v buňce / instanci:
		GraphInstance.KIND_OF_EDGE.editValueInProperties("AttributesFontSize", String.valueOf(size));

		// Velikost písma pro metody v buňce / instanci:
		GraphInstance.KIND_OF_EDGE.editValueInProperties("MethodsFontSize", String.valueOf(size));
		
		
		
		
		/*
		 * Nyní projdu veškeré objekty v diagramu instancí a každému nastavím
		 * požadovanou / upravenou velikost písma.
		 */
		GraphInstance.getCellsList().forEach(c -> {
			/*
			 * Zde si získám vlastnosti příslušného / právě iterovaného objektu a těmto
			 * vlastnostem nastavím novou velikost.
			 */
			final Map<?, ?> attributes = c.getAttributes();

			// Zde otestuji, zda je to hrana / vztah nebo instnace:
			if (c instanceof DefaultEdge) {
				/*
				 * Zde si načtu text na obou koncích hran.
				 */
				final Object[] labels = GraphConstants.getExtraLabels(attributes);

				if (GraphInstance.KIND_OF_EDGE.isAggregation((DefaultEdge) c, labels[0].toString()))
					GraphConstants.setFont(attributes,
							new Font(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getName(), fontStyleAgregation, size));

				else if (GraphInstance.KIND_OF_EDGE.isAssociation((DefaultEdge) c, labels[1].toString(), labels[0].toString()))
					GraphConstants.setFont(attributes,
							new Font(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getName(), fontStyleAssociation, size));
			}
			
			
			/*
			 * Zde se edná o instanci - buňku reprezentující instanci třídy.
			 * 
			 * Zde je třeba nastavit zaoblené hrany pomocí obdélníků z graphics, takže musím
			 * znovu vytvořit graphics, pro "renderování" celé instance.
			 */
			else
				GraphConstants.setBorder(attributes,
						BorderFactory.createCompoundBorder(
								BorderFactory.createLineBorder(instanceDiagram.getBackground(), 1, true),
								new RoundedBorderLine(c.toString(), ((InstanceCell) c).getVariablesCount(),
										GraphInstance.KIND_OF_EDGE.getProperties())));
		});
		
		
		
		
		
		/*
		 * Je třeba aktualizovat diagram instancí, aby se změny velikosti písmen / fontů
		 * projevily v okně aplikace.
		 */
		instanceDiagram.updateDiagram(GraphInstance.getCellsList());
		
		
		/*
		 * Zde nastavím hodnoty do příslušných vlastnotí pro objekt Properties
		 * obsahující aktuální nastavení pro diagram instancí. Kdybych tyto vlastnosti
		 * nastavil ještě před cyklem, pak se se v cyklu v podmínách pro zjištění
		 * jednotlivých hran nerozeznaly ty hray, protože v digramu by ty objekty byly s
		 * původním nastavením -> původní velikost textů / fontu, ale už by se
		 * vyhledával v těch podmínách ten s tou aktuální velikostí písma.
		 */
		GraphInstance.KIND_OF_EDGE.editValueInProperties("AggregationEdgeLineFontSize", String.valueOf(size));
		GraphInstance.KIND_OF_EDGE.editValueInProperties("AssociationEdgeLineFontSize", String.valueOf(size));
		

		
		
		
		/*
		 * Zde nastavím do příslušné proměnné aktuálí nastavní diagramu instací, to je
		 * potřeba například kvůli vytváření nových objektů,resp. přidávání nových
		 * instancí a vzthů vztahů mezi nimi do diagramu instancí, kdybych toto
		 * neudělal, tak by se vždy vytvářely nové objekty s původním nastavením, v
		 * tomto konkrétním případě s původní velikostí písma a při práci s nimi v
		 * diagramu instancí by se ty objekty již nerozpozanyl, protože by měli jinou
		 * velikost písma.
		 */
		 instanceDiagram.setDefaultProperties(GraphInstance.KIND_OF_EDGE.getProperties());
			
		
		
		
		
		
		
		
		
		// Uložení do souboru:
		
		/*
		 * Note:
		 * 
		 * Zde pouze zmíním, že to načítání s komentáři zde dělám proto, že když bych to
		 * takto načítal již při "prvotní" fází a držel ten soubor v aplikaci a uživatel
		 * za běhu aplikace ten soubor upravil, resp. upravil nějaký komentář, tak v
		 * této fází by se ten komentář přepsal na ty co byly načteny a drženy v
		 * aplikaci, takže by to musel uživatle přepsat znovu, nebo by si toho ani
		 * nevšiml, proto si výše načítám a v aplikaci držím pouze soubor s nastavín a
		 * ne ten pro ukládání.
		 */
		
		
		/*
		 * Načtu si soubor z configuration ve workspace, zde si dovolím ještě otestovat,
		 * zda příslušný soubor i adresář configuration existuje a kdyby ne, tak
		 * jejznovu vytvořím.
		 * 
		 * Je to vhodné, aby se pracovalo s aktuálním nastavením adpo.
		 */
		 final EditProperties instanceDiagramProp = getInstanceDiagramPropWithComments();
		
		
		
		/*
		 * Zde pouze změním velikost písma v načteném objektu a uložím zpět do souboru.
		 */
		if (instanceDiagramProp != null) {
			instanceDiagramProp.setProperty("FontSize", String.valueOf(size));
			instanceDiagramProp.setProperty("AttributesFontSize", String.valueOf(size));
			instanceDiagramProp.setProperty("MethodsFontSize", String.valueOf(size));

			instanceDiagramProp.setProperty("AggregationEdgeLineFontSize", String.valueOf(size));
			instanceDiagramProp.setProperty("AssociationEdgeLineFontSize", String.valueOf(size));

			instanceDiagramProp.save();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	

	
	@Override
	public void quickSetupDefaultFontSizes(final GraphInstance instanceDiagram) {
		/*
		 * Získám si aktuálně využívaný objekt Properties s hodnotami pro diagram
		 * instancí.
		 */
		final Properties prop = GraphInstance.KIND_OF_EDGE.getProperties();
		
		
		
		/*
		 * Tato podmínka by neměla nikdy nastat, pouze jako potenciální prevence.
		 */
		if (prop == null)
			return;
		
		
		
		/*
		 * Získám si typ fontu / písma pro zvětšení velikosti pro příslušné objekty v
		 * diagramu instancí.
		 */
		final int fontStyleAgregation = Integer.parseInt(prop.getProperty("AggregationEdgeLineFontStyle",
				Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getStyle())));
		
		final int fontStyleAssociation = Integer.parseInt(prop.getProperty("AssociationEdgeLineFontStyle",
				Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getStyle())));
		
		
		
		
		/*
		 * Velikost písma pro buňku reprezentující třídu musím nastavit již zde, protože
		 * se pouze předává objekt Properties do instance RoundedBorderLine, kde se
		 * berou potřebné hodnoty, kdybych tuto velikost písma nastavil až po cyklu, tak
		 * by se nastavila vždy ta "předchozí" velikost při posouvání posuvníku, ne ta
		 * aktuální nastavená velikost pomocí posuvníku.
		 */
		GraphInstance.KIND_OF_EDGE.editValueInProperties("FontSize", String.valueOf(Constants.ID_FONT.getSize()));

		// Velikost písma pro atributy v buňce / instanci:
		GraphInstance.KIND_OF_EDGE.editValueInProperties("AttributesFontSize",
				String.valueOf(Constants.ID_ATTRIBUTES_FONT.getSize()));

		// Velikost písma pro metody v buňce / instanci:
		GraphInstance.KIND_OF_EDGE.editValueInProperties("MethodsFontSize",
				String.valueOf(Constants.ID_METHODS_FONT.getSize()));
		
		
		
		/*
		 * Nyní projdu veškeré objekty v diagramu instancí a každému nastavím
		 * požadovanou / upravenou velikost písma.
		 */
		GraphInstance.getCellsList().forEach(c -> {
			/*
			 * Zde si získám vlastnosti příslušného / právě iterovaného objektu a těmto
			 * vlastnostem nastavím novou velikost.
			 */
			final Map<?, ?> attributes = c.getAttributes();

			// Zde otestuji, zda je to hrana / vztah nebo instnace:
			if (c instanceof DefaultEdge) {
				/*
				 * Zde si načtu text na obou koncích hran.
				 */
				final Object[] labels = GraphConstants.getExtraLabels(attributes);

				if (GraphInstance.KIND_OF_EDGE.isAggregation((DefaultEdge) c, labels[0].toString()))
					GraphConstants.setFont(attributes, new Font(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getName(),
							fontStyleAgregation, Constants.ID_AGGREGATION_EDGE_LINE_FONT.getSize()));

				else if (GraphInstance.KIND_OF_EDGE.isAssociation((DefaultEdge) c, labels[1].toString(),
						labels[0].toString()))
					GraphConstants.setFont(attributes, new Font(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getName(),
							fontStyleAssociation, Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getSize()));
			}
			
			
			/*
			 * Zde se edná o instanci - buňku reprezentující instanci třídy.
			 * 
			 * Zde je třeba nastavit zaoblené hrany pomocí obdélníků z graphics, takže musím
			 * znovu vytvořit graphics, pro "renderování" celé instance.
			 */
			else
				GraphConstants.setBorder(attributes,
						BorderFactory.createCompoundBorder(
								BorderFactory.createLineBorder(instanceDiagram.getBackground(), 1, true),
								new RoundedBorderLine(c.toString(), ((InstanceCell) c).getVariablesCount(),
										GraphInstance.KIND_OF_EDGE.getProperties())));
		});
		
		
		
		
		
		/*
		 * Je třeba aktualizovat diagram instancí, aby se změny velikosti písmen / fontů
		 * projevily v okně aplikace.
		 */
		instanceDiagram.updateDiagram(GraphInstance.getCellsList());
		
		
		/*
		 * Zde nastavím hodnoty do příslušných vlastností pro objekt Properties
		 * obsahující aktuální nastavení pro diagram instancí. Kdybych tyto vlastnosti
		 * nastavil ještě před cyklem, pak se se v cyklu v podmínách pro zjištění
		 * jednotlivých hra nerozeznaly ty hray, protože v digramu by ty objekty byly s
		 * původním nastavením -> původní velikost textů / fontu, ale už by se
		 * vyhledával v těch podmínách ten s tou aktuální velikostí písma.
		 */
		GraphInstance.KIND_OF_EDGE.editValueInProperties("AggregationEdgeLineFontSize",
				String.valueOf(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getSize()));
		GraphInstance.KIND_OF_EDGE.editValueInProperties("AssociationEdgeLineFontSize",
				String.valueOf(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getSize()));
		

		
		
		
		/*
		 * Zde nastavím do příslušné proměnné aktuálí nastavní diagramu instací, to je
		 * potřeba například kvůli vytváření nových objektů,resp. přidávání nových
		 * instancí a vzthů vztahů mezi nimi do diagramu instancí, kdybych toto
		 * neudělal, tak by se vždy vytvářely nové objekty s původním nastavením, v
		 * tomto konkrétním případě s původní velikostí písma a při práci s nimi v
		 * diagramu instancí by se ty objekty již nerozpozanyl, protože by měli jinou
		 * velikost písma.
		 */
		 instanceDiagram.setDefaultProperties(GraphInstance.KIND_OF_EDGE.getProperties());
			
		
		
		
		
		
		
		
		
		// Uložení do souboru:
		
		/*
		 * Note:
		 * 
		 * Zde pouze zmíním, že to načítání s komentáři zde dělám proto, že když bych to
		 * takto načítal již při "prvotní" fází a držel ten soubor v aplikaci a uživatel
		 * za běhu aplikace ten soubor upravil, resp. upravil nějaký komentář, tak v
		 * této fází by se ten komentář přepsal na ty co byly načteny a drženy v
		 * aplikaci, takže by to musel uživatle přepsat znovu, nebo by si toho ani
		 * nevšiml, proto si výše načítám a v aplikaci držím pouze soubor s nastavín a
		 * ne ten pro ukládání.
		 */
		
		
		/*
		 * Načtu si soubor z configuration ve workspace, zde si dovolím ještě otestovat,
		 * zda příslušný soubor i adresář configuration existuje a kdyby ne, tak
		 * jejznovu vytvořím.
		 * 
		 * Je to vhodné, aby se pracovalo s aktuálním nastavením adpo.
		 */
		 final EditProperties instanceDiagramProp = getInstanceDiagramPropWithComments();
		
		
		
		/*
		 * Zde pouze změním velikost písma v načteném objektu a uložím zpět do souboru.
		 */
		if (instanceDiagramProp != null) {
			instanceDiagramProp.setProperty("FontSize", String.valueOf(Constants.ID_FONT.getSize()));

			instanceDiagramProp.setProperty("AttributesFontSize",
					String.valueOf(Constants.ID_ATTRIBUTES_FONT.getSize()));
			instanceDiagramProp.setProperty("MethodsFontSize", String.valueOf(Constants.ID_METHODS_FONT.getSize()));

			instanceDiagramProp.setProperty("AggregationEdgeLineFontSize",
					String.valueOf(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getSize()));
			instanceDiagramProp.setProperty("AssociationEdgeLineFontSize",
					String.valueOf(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getSize()));

			instanceDiagramProp.save();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	

	@Override
	public void quickSetupOfFontSize(final int size, final boolean isInstance, final boolean isInstanceAttributes,
			final boolean isInstanceMethods, final boolean isAssociation, final boolean isAggregation,
			final GraphInstance instanceDiagram) {
		/*
		 * Získám si aktuálně využívaný objekt Properties s hodnotami pro diagram
		 * instancí.
		 */
		final Properties prop = GraphInstance.KIND_OF_EDGE.getProperties();
		
		
		
		/*
		 * Tato podmínka by neměla nikdy nastat, pouze jako potenciální prevence.
		 */
		if (prop == null)
			return;
		
		
		
		/*
		 * Získám si typ fontu / písma pro zvětšení velikosti pro příslušné objekty v
		 * diagramu instancí.
		 */
		final int fontStyleAgregation = Integer.parseInt(prop.getProperty("AggregationEdgeLineFontStyle",
				Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getStyle())));
		
		final int fontStyleAssociation = Integer.parseInt(prop.getProperty("AssociationEdgeLineFontStyle",
				Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getStyle())));
		
		
		
		/*
		 * Velikost písma pro buňku reprezentující třídu musím nastavit již zde, protože
		 * se pouze předává objekt Properties do instance RoundedBorderLine, kde se
		 * berou potřebné hodnoty, kdybych tuto velikost písma nastavil až po cyklu, tak
		 * by se nastavila vždy ta "předchozí" velikost při posouvání posuvníku, ne ta
		 * aktuální nastavená velikost pomocí posuvníku.
		 */
		if (isInstance)
			GraphInstance.KIND_OF_EDGE.editValueInProperties("FontSize", String.valueOf(size));

		if (isInstanceAttributes)
			// Velikost písma pro atributy v buňce / instanci:
			GraphInstance.KIND_OF_EDGE.editValueInProperties("AttributesFontSize", String.valueOf(size));

		if (isInstanceMethods)
			// Velikost písma pro metody v buňce / instanci:
			GraphInstance.KIND_OF_EDGE.editValueInProperties("MethodsFontSize", String.valueOf(size));
		
		
		
		
		/*
		 * Nyní projdu veškeré objekty v diagramu instancí a každému nastavím
		 * požadovanou / upravenou velikost písma.
		 */
		GraphInstance.getCellsList().forEach(c -> {
			/*
			 * Zde si získám vlastnosti příslušného / právě iterovaného objektu a těmto
			 * vlastnostem nastavím novou velikost.
			 */
			final Map<?, ?> attributes = c.getAttributes();

			// Zde otestuji, zda je to hrana / vztah nebo instnace:
			if (c instanceof DefaultEdge) {
				/*
				 * Zde si načtu text na obou koncích hran.
				 */
				final Object[] labels = GraphConstants.getExtraLabels(attributes);

				if (isAggregation && GraphInstance.KIND_OF_EDGE.isAggregation((DefaultEdge) c, labels[0].toString()))
					GraphConstants.setFont(attributes,
							new Font(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getName(), fontStyleAgregation, size));

				else if (isAssociation && GraphInstance.KIND_OF_EDGE.isAssociation((DefaultEdge) c,
						labels[1].toString(), labels[0].toString()))
					GraphConstants.setFont(attributes,
							new Font(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getName(), fontStyleAssociation, size));
			}

			/*
			 * Zde se jedná o instanci - buňku reprezentující instanci třídy nebo písmo pro
			 * atributy nebo metody (v té buňce).
			 * 
			 * Zde je třeba nastavit zaoblené hrany pomocí obdélníků z graphics, takže musím
			 * znovu vytvořit graphics, pro "renderování" celé instance.
			 */
			else if (isInstance || isInstanceAttributes || isInstanceMethods)
				GraphConstants.setBorder(attributes,
						BorderFactory.createCompoundBorder(
								BorderFactory.createLineBorder(instanceDiagram.getBackground(), 1, true),
								new RoundedBorderLine(c.toString(), ((InstanceCell) c).getVariablesCount(),
										GraphInstance.KIND_OF_EDGE.getProperties())));
		});
		
		
		
		
		
		/*
		 * Je třeba aktualizovat diagram instancí, aby se změny velikosti písmen / fontů
		 * projevily v okně aplikace.
		 */
		instanceDiagram.updateDiagram(GraphInstance.getCellsList());
		
		
		/*
		 * Zde nastavím hodnoty do příslušných vlastnotí pro objekt Properties
		 * obsahující aktuální nastavení pro diagram instancí. Kdybych tyto vlastnosti
		 * nastavil ještě před cyklem, pak se se v cyklu v podmínách pro zjištění
		 * jednotlivých hra nerozeznaly ty hray, protože v digramu by ty objekty byly s
		 * původním nastavením -> původní velikost textů / fontu, ale už by se
		 * vyhledával v těch podmínách ten s tou aktuální velikostí písma.
		 */
		if (isAggregation)
			GraphInstance.KIND_OF_EDGE.editValueInProperties("AggregationEdgeLineFontSize", String.valueOf(size));

		if (isAssociation)
			GraphInstance.KIND_OF_EDGE.editValueInProperties("AssociationEdgeLineFontSize", String.valueOf(size));
		

		
		
		
		/*
		 * Zde nastavím do příslušné proměnné aktuálí nastavní diagramu instací, to je
		 * potřeba například kvůli vytváření nových objektů,resp. přidávání nových
		 * instancí a vzthů vztahů mezi nimi do diagramu instancí, kdybych toto
		 * neudělal, tak by se vždy vytvářely nové objekty s původním nastavením, v
		 * tomto konkrétním případě s původní velikostí písma a při práci s nimi v
		 * diagramu instancí by se ty objekty již nerozpozanyl, protože by měli jinou
		 * velikost písma.
		 */
		 instanceDiagram.setDefaultProperties(GraphInstance.KIND_OF_EDGE.getProperties());
			
		
		
		
		
		
		
		
		
		// Uložení do souboru:
		
		/*
		 * Note:
		 * 
		 * Zde pouze zmíním, že to načítání s komentáři zde dělám proto, že když bych to
		 * takto načítal již při "prvotní" fází a držel ten soubor v aplikaci a uživatel
		 * za běhu aplikace ten soubor upravil, resp. upravil nějaký komentář, tak v
		 * této fází by se ten komentář přepsal na ty co byly načteny a drženy v
		 * aplikaci, takže by to musel uživatle přepsat znovu, nebo by si toho ani
		 * nevšiml, proto si výše načítám a v aplikaci držím pouze soubor s nastavín a
		 * ne ten pro ukládání.
		 */
		
		
		/*
		 * Načtu si soubor z configuration ve workspace, zde si dovolím ještě otestovat,
		 * zda příslušný soubor i adresář configuration existuje a kdyby ne, tak
		 * jejznovu vytvořím.
		 * 
		 * Je to vhodné, aby se pracovalo s aktuálním nastavením adpo.
		 */
		 final EditProperties instanceDiagramProp = getInstanceDiagramPropWithComments();
		
		
		
		/*
		 * Zde pouze změním velikost písma v načteném objektu a uložím zpět do souboru.
		 */
		if (instanceDiagramProp != null) {
			if (isInstance)
				instanceDiagramProp.setProperty("FontSize", String.valueOf(size));

			if (isInstanceAttributes)
				instanceDiagramProp.setProperty("AttributesFontSize", String.valueOf(size));

			if (isInstanceMethods)
				instanceDiagramProp.setProperty("MethodsFontSize", String.valueOf(size));

			if (isAggregation)
				instanceDiagramProp.setProperty("AggregationEdgeLineFontSize", String.valueOf(size));

			if (isAssociation)
				instanceDiagramProp.setProperty("AssociationEdgeLineFontSize", String.valueOf(size));
						
			
			instanceDiagramProp.save();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	@Override
	public void quickSetupOfInstanceFontSize(final int size, final GraphInstance instanceDiagram) {
		/*
		 * Získám si aktuálně využívaný objekt Properties s hodnotami pro diagram
		 * instancí.
		 */
		final Properties prop = GraphInstance.KIND_OF_EDGE.getProperties();
		
		
		
		/*
		 * Tato podmínka by neměla nikdy nastat, pouze jako potenciální prevence.
		 */
		if (prop == null)
			return;
		
		
		
		
		
		/*
		 * Velikost písma pro buňku reprezentující třídu musím nastavit již zde, protože
		 * se pouze předává objekt Properties do instance RoundedBorderLine, kde se
		 * berou potřebné hodnoty, kdybych tuto velikost písma nastavil až po cyklu, tak
		 * by se nastavila vždy ta "předchozí" velikost při posouvání posuvníku, ne ta
		 * aktuální nastavená velikost pomocí posuvníku.
		 */
		GraphInstance.KIND_OF_EDGE.editValueInProperties("FontSize", String.valueOf(size));
		
		
		
		
		
		/*
		 * Nyní projdu veškeré objekty v diagramu instancí a každému nastavím
		 * požadovanou / upravenou velikost písma.
		 */
		GraphInstance.getCellsList().forEach(c -> {
			/*
			 * Zde si získám vlastnosti příslušného / právě iterovaného objektu a těmto
			 * vlastnostem nastavím novou velikost.
			 */
			final Map<?, ?> attributes = c.getAttributes();

			// Zde otestuji, zda je to hrana / vztah nebo instnace:
			if (!(c instanceof DefaultEdge)) {
				/*
				 * Zde se jedná o instanci - buňku reprezentující instanci třídy.
				 * 
				 * Zde je třeba nastavit zaoblené hrany pomocí obdélníků z graphics, takže musím
				 * znovu vytvořit graphics, pro "renderování" celé instance.
				 */
				GraphConstants.setBorder(attributes,
						BorderFactory.createCompoundBorder(
								BorderFactory.createLineBorder(instanceDiagram.getBackground(), 1, true),
								new RoundedBorderLine(c.toString(), ((InstanceCell) c).getVariablesCount(),
										GraphInstance.KIND_OF_EDGE.getProperties())));
			}
		});
		
		
		
		
		
		/*
		 * Je třeba aktualizovat diagram instancí, aby se změny velikosti písmen / fontů
		 * projevily v okně aplikace.
		 */
		instanceDiagram.updateDiagram(GraphInstance.getCellsList());
		
		
		
		
		/*
		 * Zde nastavím do příslušné proměnné aktuálí nastavní diagramu instací, to je
		 * potřeba například kvůli vytváření nových objektů,resp. přidávání nových
		 * instancí a vzthů vztahů mezi nimi do diagramu instancí, kdybych toto
		 * neudělal, tak by se vždy vytvářely nové objekty s původním nastavením, v
		 * tomto konkrétním případě s původní velikostí písma a při práci s nimi v
		 * diagramu instancí by se ty objekty již nerozpozanyl, protože by měli jinou
		 * velikost písma.
		 */
		 instanceDiagram.setDefaultProperties(GraphInstance.KIND_OF_EDGE.getProperties());
			
		
		
		
		
		
		
		
		
		// Uložení do souboru:
		
		/*
		 * Note:
		 * 
		 * Zde pouze zmíním, že to načítání s komentáři zde dělám proto, že když bych to
		 * takto načítal již při "prvotní" fází a držel ten soubor v aplikaci a uživatel
		 * za běhu aplikace ten soubor upravil, resp. upravil nějaký komentář, tak v
		 * této fází by se ten komentář přepsal na ty co byly načteny a drženy v
		 * aplikaci, takže by to musel uživatle přepsat znovu, nebo by si toho ani
		 * nevšiml, proto si výše načítám a v aplikaci držím pouze soubor s nastavín a
		 * ne ten pro ukládání.
		 */
		
		
		/*
		 * Načtu si soubor z configuration ve workspace, zde si dovolím ještě otestovat,
		 * zda příslušný soubor i adresář configuration existuje a kdyby ne, tak
		 * jejznovu vytvořím.
		 * 
		 * Je to vhodné, aby se pracovalo s aktuálním nastavením adpo.
		 */
		 final EditProperties instanceDiagramProp = getInstanceDiagramPropWithComments();
		
		
		
		/*
		 * Zde pouze změním velikost písma v načteném objektu a uložím zpět do souboru.
		 */
		if (instanceDiagramProp != null) {		
			instanceDiagramProp.setProperty("FontSize", String.valueOf(size));
						
			instanceDiagramProp.save();
		}
	}
	
	
	
	
	
	
	
	
	
	@Override
	public void quickSetupOfAttributesFontSizeInInstance(final int size, final GraphInstance instanceDiagram) {
		/*
		 * Získám si aktuálně využívaný objekt Properties s hodnotami pro diagram
		 * instancí.
		 */
		final Properties prop = GraphInstance.KIND_OF_EDGE.getProperties();
		
		/*
		 * Tato podmínka by neměla nikdy nastat, pouze jako potenciální prevence.
		 */
		if (prop == null)
			return;
		

		/*
		 * Velikost písma pro atributy v buňce reprezentující třídu musím nastavit již
		 * zde, protože se pouze předává objekt Properties do instance
		 * RoundedBorderLine, kde se berou potřebné hodnoty, kdybych tuto velikost písma
		 * nastavil až po cyklu, tak by se nastavila vždy ta "předchozí" velikost při
		 * posouvání posuvníku, ne ta aktuální nastavená velikost pomocí posuvníku.
		 */
		GraphInstance.KIND_OF_EDGE.editValueInProperties("AttributesFontSize", String.valueOf(size));

		
		/*
		 * Nyní projdu veškeré objekty v diagramu instancí a každému nastavím
		 * požadovanou / upravenou velikost písma.
		 */
		GraphInstance.getCellsList().forEach(c -> {
			/*
			 * Zde si získám vlastnosti příslušného / právě iterovaného objektu a těmto
			 * vlastnostem nastavím novou velikost.
			 */
			final Map<?, ?> attributes = c.getAttributes();

			// Zde otestuji, zda je to hrana / vztah nebo instnace:
			if (!(c instanceof DefaultEdge)) {
				/*
				 * Zde se jedná o instanci - buňku reprezentující instanci třídy.
				 * 
				 * Zde je třeba nastavit zaoblené hrany pomocí obdélníků z graphics, takže musím
				 * znovu vytvořit graphics, pro "renderování" celé instance.
				 */
				GraphConstants.setBorder(attributes,
						BorderFactory.createCompoundBorder(
								BorderFactory.createLineBorder(instanceDiagram.getBackground(), 1, true),
								new RoundedBorderLine(c.toString(), ((InstanceCell) c).getVariablesCount(),
										GraphInstance.KIND_OF_EDGE.getProperties())));
			}
		});
		
		
		
		
		
		/*
		 * Je třeba aktualizovat diagram instancí, aby se změny velikosti písmen / fontů
		 * projevily v okně aplikace.
		 */
		instanceDiagram.updateDiagram(GraphInstance.getCellsList());
		
		
		
		
		/*
		 * Zde nastavím do příslušné proměnné aktuálí nastavní diagramu instací, to je
		 * potřeba například kvůli vytváření nových objektů,resp. přidávání nových
		 * instancí a vzthů vztahů mezi nimi do diagramu instancí, kdybych toto
		 * neudělal, tak by se vždy vytvářely nové objekty s původním nastavením, v
		 * tomto konkrétním případě s původní velikostí písma a při práci s nimi v
		 * diagramu instancí by se ty objekty již nerozpozanyl, protože by měli jinou
		 * velikost písma.
		 */
		 instanceDiagram.setDefaultProperties(GraphInstance.KIND_OF_EDGE.getProperties());
			

		
		
		// Uložení do souboru:
		
		/*
		 * Note:
		 * 
		 * Zde pouze zmíním, že to načítání s komentáři zde dělám proto, že když bych to
		 * takto načítal již při "prvotní" fází a držel ten soubor v aplikaci a uživatel
		 * za běhu aplikace ten soubor upravil, resp. upravil nějaký komentář, tak v
		 * této fází by se ten komentář přepsal na ty co byly načteny a drženy v
		 * aplikaci, takže by to musel uživatle přepsat znovu, nebo by si toho ani
		 * nevšiml, proto si výše načítám a v aplikaci držím pouze soubor s nastavín a
		 * ne ten pro ukládání.
		 */
		
		
		/*
		 * Načtu si soubor z configuration ve workspace, zde si dovolím ještě otestovat,
		 * zda příslušný soubor i adresář configuration existuje a kdyby ne, tak
		 * jejznovu vytvořím.
		 * 
		 * Je to vhodné, aby se pracovalo s aktuálním nastavením adpo.
		 */
		 final EditProperties instanceDiagramProp = getInstanceDiagramPropWithComments();
		

		/*
		 * Zde pouze změním velikost písma v načteném objektu a uložím zpět do souboru.
		 */
		if (instanceDiagramProp != null) {		
			instanceDiagramProp.setProperty("AttributesFontSize", String.valueOf(size));
						
			instanceDiagramProp.save();
		}
	}
	
	
	
	
	
	
	
	
	@Override
	public void quickSetupOfMethodsFontSizeInInstance(final int size, final GraphInstance instanceDiagram) {
		/*
		 * Získám si aktuálně využívaný objekt Properties s hodnotami pro diagram
		 * instancí.
		 */
		final Properties prop = GraphInstance.KIND_OF_EDGE.getProperties();
		
		
		
		/*
		 * Tato podmínka by neměla nikdy nastat, pouze jako potenciální prevence.
		 */
		if (prop == null)
			return;
		
		
		
		
		
		/*
		 * Velikost písma pro metody v buňce reprezentující třídu musím nastavit již
		 * zde, protože se pouze předává objekt Properties do instance
		 * RoundedBorderLine, kde se berou potřebné hodnoty, kdybych tuto velikost písma
		 * nastavil až po cyklu, tak by se nastavila vždy ta "předchozí" velikost při
		 * posouvání posuvníku, ne ta aktuální nastavená velikost pomocí posuvníku.
		 */
		GraphInstance.KIND_OF_EDGE.editValueInProperties("MethodsFontSize", String.valueOf(size));
		
		
		
		
		
		/*
		 * Nyní projdu veškeré objekty v diagramu instancí a každému nastavím
		 * požadovanou / upravenou velikost písma.
		 */
		GraphInstance.getCellsList().forEach(c -> {
			/*
			 * Zde si získám vlastnosti příslušného / právě iterovaného objektu a těmto
			 * vlastnostem nastavím novou velikost.
			 */
			final Map<?, ?> attributes = c.getAttributes();

			// Zde otestuji, zda je to hrana / vztah nebo instnace:
			if (!(c instanceof DefaultEdge)) {
				/*
				 * Zde se jedná o instanci - buňku reprezentující instanci třídy.
				 * 
				 * Zde je třeba nastavit zaoblené hrany pomocí obdélníků z graphics, takže musím
				 * znovu vytvořit graphics, pro "renderování" celé instance.
				 */
				GraphConstants.setBorder(attributes,
						BorderFactory.createCompoundBorder(
								BorderFactory.createLineBorder(instanceDiagram.getBackground(), 1, true),
								new RoundedBorderLine(c.toString(), ((InstanceCell) c).getVariablesCount(),
										GraphInstance.KIND_OF_EDGE.getProperties())));
			}
		});
		
		
		
		
		
		/*
		 * Je třeba aktualizovat diagram instancí, aby se změny velikosti písmen / fontů
		 * projevily v okně aplikace.
		 */
		instanceDiagram.updateDiagram(GraphInstance.getCellsList());
		
		
		
		
		/*
		 * Zde nastavím do příslušné proměnné aktuálí nastavní diagramu instací, to je
		 * potřeba například kvůli vytváření nových objektů,resp. přidávání nových
		 * instancí a vzthů vztahů mezi nimi do diagramu instancí, kdybych toto
		 * neudělal, tak by se vždy vytvářely nové objekty s původním nastavením, v
		 * tomto konkrétním případě s původní velikostí písma a při práci s nimi v
		 * diagramu instancí by se ty objekty již nerozpozanyl, protože by měli jinou
		 * velikost písma.
		 */
		 instanceDiagram.setDefaultProperties(GraphInstance.KIND_OF_EDGE.getProperties());
			
		
		

		
		
		// Uložení do souboru:
		
		/*
		 * Note:
		 * 
		 * Zde pouze zmíním, že to načítání s komentáři zde dělám proto, že když bych to
		 * takto načítal již při "prvotní" fází a držel ten soubor v aplikaci a uživatel
		 * za běhu aplikace ten soubor upravil, resp. upravil nějaký komentář, tak v
		 * této fází by se ten komentář přepsal na ty co byly načteny a drženy v
		 * aplikaci, takže by to musel uživatle přepsat znovu, nebo by si toho ani
		 * nevšiml, proto si výše načítám a v aplikaci držím pouze soubor s nastavín a
		 * ne ten pro ukládání.
		 */
		
		
		/*
		 * Načtu si soubor z configuration ve workspace, zde si dovolím ještě otestovat,
		 * zda příslušný soubor i adresář configuration existuje a kdyby ne, tak
		 * jejznovu vytvořím.
		 * 
		 * Je to vhodné, aby se pracovalo s aktuálním nastavením adpo.
		 */
		 final EditProperties instanceDiagramProp = getInstanceDiagramPropWithComments();
		
		
		
		/*
		 * Zde pouze změním velikost písma v načteném objektu a uložím zpět do souboru.
		 */
		if (instanceDiagramProp != null) {		
			instanceDiagramProp.setProperty("MethodsFontSize", String.valueOf(size));
						
			instanceDiagramProp.save();
		}
	}
	
	
	
	
	
	
	
	
	

	@Override
	public void quickSetupOfAssociationFontSize(final int size, final GraphInstance instanceDiagram) {
		/*
		 * Získám si aktuálně využívaný objekt Properties s hodnotami pro diagram
		 * instancí.
		 */
		final Properties prop = GraphInstance.KIND_OF_EDGE.getProperties();
		
		
		
		/*
		 * Tato podmínka by neměla nikdy nastat, pouze jako potenciální prevence.
		 */
		if (prop == null)
			return;
		
		
		
		/*
		 * Získám si typ fontu hrany, která reprezentuje asociace v diagramu instnací.
		 */
		final int fontStyleAssociation = Integer.parseInt(prop.getProperty("AssociationEdgeLineFontStyle",
				Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getStyle())));
		
		
		
		
		
		/*
		 * Nyní projdu veškeré objekty v diagramu instancí a každému nastavím
		 * požadovanou / upravenou velikost písma.
		 */
		GraphInstance.getCellsList().forEach(c -> {
			/*
			 * Zde si získám vlastnosti příslušného / právě iterovaného objektu a těmto
			 * vlastnostem nastavím novou velikost.
			 */
			final Map<?, ?> attributes = c.getAttributes();

			// Zde otestuji, zda je to hrana / vztah nebo instnace:
			if (c instanceof DefaultEdge) {
				/*
				 * Zde si načtu text na obou koncích hran.
				 */
				final Object[] labels = GraphConstants.getExtraLabels(attributes);

				if (GraphInstance.KIND_OF_EDGE.isAssociation((DefaultEdge) c, labels[1].toString(),
						labels[0].toString()))
					GraphConstants.setFont(attributes,
							new Font(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getName(), fontStyleAssociation, size));
			}
		});
		
		
		
		
		
		/*
		 * Je třeba aktualizovat diagram instancí, aby se změny velikosti písmen / fontů
		 * projevily v okně aplikace.
		 */
		instanceDiagram.updateDiagram(GraphInstance.getCellsList());
		
		
		/*
		 * Zde nastavím hodnoty do příslušných vlastnotí pro objekt Properties
		 * obsahující aktuální nastavení pro diagram instancí. Kdybych tyto vlastnosti
		 * nastavil ještě před cyklem, pak se se v cyklu v podmínách pro zjištění
		 * jednotlivých hra nerozeznaly ty hray, protože v digramu by ty objekty byly s
		 * původním nastavením -> původní velikost textů / fontu, ale už by se
		 * vyhledával v těch podmínách ten s tou aktuální velikostí písma.
		 */
		GraphInstance.KIND_OF_EDGE.editValueInProperties("AssociationEdgeLineFontSize", String.valueOf(size));
		

		
		
		
		/*
		 * Zde nastavím do příslušné proměnné aktuálí nastavní diagramu instací, to je
		 * potřeba například kvůli vytváření nových objektů,resp. přidávání nových
		 * instancí a vzthů vztahů mezi nimi do diagramu instancí, kdybych toto
		 * neudělal, tak by se vždy vytvářely nové objekty s původním nastavením, v
		 * tomto konkrétním případě s původní velikostí písma a při práci s nimi v
		 * diagramu instancí by se ty objekty již nerozpozanyl, protože by měli jinou
		 * velikost písma.
		 */
		 instanceDiagram.setDefaultProperties(GraphInstance.KIND_OF_EDGE.getProperties());
			
		
		
		
		
		
		
		
		
		// Uložení do souboru:
		
		/*
		 * Note:
		 * 
		 * Zde pouze zmíním, že to načítání s komentáři zde dělám proto, že když bych to
		 * takto načítal již při "prvotní" fází a držel ten soubor v aplikaci a uživatel
		 * za běhu aplikace ten soubor upravil, resp. upravil nějaký komentář, tak v
		 * této fází by se ten komentář přepsal na ty co byly načteny a drženy v
		 * aplikaci, takže by to musel uživatle přepsat znovu, nebo by si toho ani
		 * nevšiml, proto si výše načítám a v aplikaci držím pouze soubor s nastavín a
		 * ne ten pro ukládání.
		 */
		
		
		/*
		 * Načtu si soubor z configuration ve workspace, zde si dovolím ještě otestovat,
		 * zda příslušný soubor i adresář configuration existuje a kdyby ne, tak
		 * jejznovu vytvořím.
		 * 
		 * Je to vhodné, aby se pracovalo s aktuálním nastavením adpo.
		 */
		 final EditProperties instanceDiagramProp = getInstanceDiagramPropWithComments();
		
		
		
		/*
		 * Zde pouze změním velikost písma v načteném objektu a uložím zpět do souboru.
		 */
		if (instanceDiagramProp != null) {
			instanceDiagramProp.setProperty("AssociationEdgeLineFontSize", String.valueOf(size));

			instanceDiagramProp.save();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public void quickSetupOfAgregationFontSize(final int size, final GraphInstance instanceDiagram) {
		/*
		 * Získám si aktuálně využívaný objekt Properties s hodnotami pro diagram
		 * instancí.
		 */
		final Properties prop = GraphInstance.KIND_OF_EDGE.getProperties();
		
		
		
		/*
		 * Tato podmínka by neměla nikdy nastat, pouze jako potenciální prevence.
		 */
		if (prop == null)
			return;
		
		
		
		
		/*
		 * Získám si typu fontu / písma, pro typ hrany, která v diagramu instancí
		 * reprezentuje vztah typu agregace.
		 */
		final int fontStyleAgregation = Integer.parseInt(prop.getProperty("AggregationEdgeLineFontStyle",
				Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getStyle())));
		
		
		
		
		
		/*
		 * Nyní projdu veškeré objekty v diagramu instancí a každému nastavím
		 * požadovanou / upravenou velikost písma.
		 */
		GraphInstance.getCellsList().forEach(c -> {
			/*
			 * Zde si získám vlastnosti příslušného / právě iterovaného objektu a těmto
			 * vlastnostem nastavím novou velikost.
			 */
			final Map<?, ?> attributes = c.getAttributes();

			// Zde otestuji, zda je to hrana / vztah nebo instnace:
			if (c instanceof DefaultEdge) {
				/*
				 * Zde si načtu text na obou koncích hran.
				 */
				final Object[] labels = GraphConstants.getExtraLabels(attributes);

				if (GraphInstance.KIND_OF_EDGE.isAggregation((DefaultEdge) c, labels[0].toString()))
					GraphConstants.setFont(attributes,
							new Font(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getName(), fontStyleAgregation, size));
			}
		});
		
		
		
		
		
		/*
		 * Je třeba aktualizovat diagram instancí, aby se změny velikosti písmen / fontů
		 * projevily v okně aplikace.
		 */
		instanceDiagram.updateDiagram(GraphInstance.getCellsList());
		
		
		/*
		 * Zde nastavím hodnoty do příslušných vlastnotí pro objekt Properties
		 * obsahující aktuální nastavení pro diagram instancí. Kdybych tyto vlastnosti
		 * nastavil ještě před cyklem, pak se se v cyklu v podmínách pro zjištění
		 * jednotlivých hra nerozeznaly ty hray, protože v digramu by ty objekty byly s
		 * původním nastavením -> původní velikost textů / fontu, ale už by se
		 * vyhledával v těch podmínách ten s tou aktuální velikostí písma.
		 */
		GraphInstance.KIND_OF_EDGE.editValueInProperties("AggregationEdgeLineFontSize", String.valueOf(size));
		

		
		
		
		/*
		 * Zde nastavím do příslušné proměnné aktuálí nastavní diagramu instací, to je
		 * potřeba například kvůli vytváření nových objektů,resp. přidávání nových
		 * instancí a vzthů vztahů mezi nimi do diagramu instancí, kdybych toto
		 * neudělal, tak by se vždy vytvářely nové objekty s původním nastavením, v
		 * tomto konkrétním případě s původní velikostí písma a při práci s nimi v
		 * diagramu instancí by se ty objekty již nerozpozanyl, protože by měli jinou
		 * velikost písma.
		 */
		 instanceDiagram.setDefaultProperties(GraphInstance.KIND_OF_EDGE.getProperties());
			
		
		
		
		
		
		
		
		
		// Uložení do souboru:
		
		/*
		 * Note:
		 * 
		 * Zde pouze zmíním, že to načítání s komentáři zde dělám proto, že když bych to
		 * takto načítal již při "prvotní" fází a držel ten soubor v aplikaci a uživatel
		 * za běhu aplikace ten soubor upravil, resp. upravil nějaký komentář, tak v
		 * této fází by se ten komentář přepsal na ty co byly načteny a drženy v
		 * aplikaci, takže by to musel uživatle přepsat znovu, nebo by si toho ani
		 * nevšiml, proto si výše načítám a v aplikaci držím pouze soubor s nastavín a
		 * ne ten pro ukládání.
		 */
		
		
		/*
		 * Načtu si soubor z configuration ve workspace, zde si dovolím ještě otestovat,
		 * zda příslušný soubor i adresář configuration existuje a kdyby ne, tak
		 * jejznovu vytvořím.
		 * 
		 * Je to vhodné, aby se pracovalo s aktuálním nastavením adpo.
		 */
		 final EditProperties instanceDiagramProp = getInstanceDiagramPropWithComments();
		
		
		
		/*
		 * Zde pouze změním velikost písma v načteném objektu a uložím zpět do souboru.
		 */
		if (instanceDiagramProp != null) {
			instanceDiagramProp.setProperty("AggregationEdgeLineFontSize", String.valueOf(size));

			instanceDiagramProp.save();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro načtení objektu Properties, který obsahuje veškeré
	 * nastavení pro diagram instancí. Ale tento objekt Properties se načte tak, že
	 * se zachovají komentáře.
	 * 
	 * @return objekt Properties načtený ze soubory se zachovanými komentáři
	 *         obsahující nastavení pro diagram instancí..
	 */
	private static EditProperties getInstanceDiagramPropWithComments() {
		EditProperties instanceDiagramProp = App.READ_FILE
				.getPropertiesInWorkspacePersistComments(Constants.INSTANCE_DIAGRAM_NAME);

		if (instanceDiagramProp == null) {
			final String pathToWorkspace = ReadFile.getPathToWorkspace();

			/*
			 * Zde pouze zjisšťuji, zda existují veškeré adresáře k cestě pro soubor
			 * InstanceDiagram.properties v adresáři configuration ve workspace. Pokud ne,
			 * pak v rámci možností nekteré adresáře mohu vytvořit, jinak není kam uložit
			 * soubory, pokud například byl smazan workspace apod. Pak už zbývá jedině znovu
			 * vytvořit adresář workspace s projekty apod. Ale tuto část zde vynechávám,
			 * protože o tom uživatel musí vědět - že si smazal projekty případně workspace
			 * apod. Tak snad musí vědět, že nelze pracovat s projekty / soubory, které byly
			 * smazány.
			 * 
			 * Otestuji, zda existuje workspace, pak configuration, pak příslušný soubor,
			 * případně některé z nich mohu vytvořit:
			 */
			if (pathToWorkspace != null) {
				/*
				 * Pokud neexsistuje adresář configuration ve workspace, bude vytvořen i s
				 * potřebnými soubory s výchozím nastavením.
				 * 
				 * Pokud adresář configuration ve workspace existuje, pak stačí vytvořit jen
				 * soubor InstanceDiagram.properties, protože vím, že neexistuje, když se jej nepodařilo
				 * načíst.
				 */
				if (ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
					ConfigurationFiles.writeInstanceDiagramProperties(
							pathToWorkspace + File.separator + Constants.PATH_TO_INSTANCE_DIAGRAM_PROPERTIES);

				
				else
					WriteToFile.createConfigureFilesInPath(pathToWorkspace);

				
				// Znovu načtu soubor Default.properties z workspace z configuration, protože
				// výše byl znovu vytvořn:
				instanceDiagramProp = App.READ_FILE
						.getPropertiesInWorkspacePersistComments(Constants.INSTANCE_DIAGRAM_NAME);
			}
			/*
			 * část else:
			 * 
			 * Zde by bylo možné vytvořit dříve zadaný adresář workspace - pokud byl již
			 * zadán, ale byl smazán, ale to by v rámci aplikace nemělo nastat, proto tuto
			 * část vynechám, uživatel jej musí smazat sám, pak o tom musí vědět.
			 */
		}
		
		return instanceDiagramProp;
	}
}
