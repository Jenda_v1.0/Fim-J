package cz.uhk.fim.fimj.instance_diagram.overview_of_instance;

import java.lang.reflect.Method;
import java.util.*;

import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;
import cz.uhk.fim.fimj.forms.GetParametersMethodForm;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;
import cz.uhk.fim.fimj.instances.Called;
import cz.uhk.fim.fimj.output_editor.OutputEditor;
import cz.uhk.fim.fimj.popup_menu.PopupMenuAbstract;

/**
 * Tato třída slouží jako položky do komponenty typu JList v dialogu pro přehled hodnot v instanci třídy. Každá tato
 * položka slouží pro reprezentaci jedné z metod v nějaké instanci třídy. Tato proměnná je zde doplněna protože jsem
 * chtěl doplnit možnost pro zavolání příslušné metody i z toho dialogu, kdyby si to náhodou uživatel "rozmyslel", nebo
 * se z ničeho nic rozhodl, že tuto metodu prostě zavolá apod. Takže spíše je to taková "drobnost" navíc.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class MethodInfo extends ValuesInfo {

    /**
     * Metoda, kterou konkrétní instance této třídy reprezentuje v dialogu pro přehled hodnot o příslušné instanci v
     * diagramu instancí. Tato metoda se zavolá, resp. jedná o se nějakou metodu z označené instance, kterou je možné
     * zavolat, resp. která se zavolá v případě, že uivatel označí "tuto" položku v komponentě JList.
     */
    private final Method method;


    /**
     * Instance nějaké třídy z diagramu tříd, jejích hodnoty byly načteny do dialogu pro zobrazení hodnot nějaké
     * instance.
     * <p>
     * Tato instance obsahuje metodu method a nad touto instnací se bude metoda method volat.
     */
    private final Object objInstance;


    private final String reference;


    /**
     * Konstruktor této třídy.
     *
     * @param text
     *         - text, který obsahuje syntaxi metody v příslušném formátu, který bude zobrazen v komponentě JList, v
     *         dialogu
     * @param method
     *         - metoda, která je reprezentována konkrétní instnací této třídy, případně, metoda, která se zavolá.
     * @param objInstance
     *         - instance nějaké třídy, nad kterou se bude případně volatmetoda method, resp. je to instance, která
     *         obsahuje metodu method.
     * @param clazz
     *         - třída, ze které byla získána metoda method.
     */
    MethodInfo(final String text, final Method method, final Object objInstance, final String reference, final Class<
            ?> clazz) {
        super(text, clazz);

        this.method = method;

        this.objInstance = objInstance;

        this.reference = reference;
    }


    /**
     * Metoda, která slouží pro zavolání metody v konkrétní instanci této třídy, resp. jedná se o nějakou specifickou
     * položku v komponentě JList v dialogu s přehledem hodnot o zvolené instanci v diagramu instancí.
     * <p>
     * Tato metroda slouží pro zavolání příslušné metody. Otestuje se, zda se jedná o zavolání metody s nebo bez
     * parametru, pak se zavolá (popř. vyplní parametry) a na konec se zjistí, zda se vrátila nějaká hodnota z metody,
     * resp. zda metoda vrací nějakou hodnotu a pokud ano, tak se otestuji, zda to náhodou není nějaká instance třídy z
     * diagramu tříd, která ještě není v diagramu instancí a pokud ano, bude na výběr možnost pro vytvoření nové
     * reference na příslušnou proměnnou.
     *
     * @param instanceDiagram
     *         - reference na diagram instancí.
     * @param outputEditor
     *         - reference na editoru výstupů, tj. kam se mají vypisovat hlášení ohledně zavolání metody.
     * @param languageProperties
     *         - objekt s texty pro tuto aplikaci ve zvoleném jazyce.
     * @param txtSuccess
     *         - text "Úspěch" pro výpis do editoru výstupů.
     */
    public final void callMethod(final GraphInstance instanceDiagram, final OutputEditor outputEditor,
                                 final Properties languageProperties, final String txtSuccess) {

        if (method.getParameterCount() == 0) {
            // Metoda neobsahuje žádný parametr, tak ji mohu zavolat:
            final Called returnValue = VariablesInstanceOfClassForm.operationWithInstances
                    .callMethodWithoutParameters(method, objInstance, true);

            if (returnValue == null || !returnValue.isWasCalled())
                /*
                 * Zde se metodu nepodařilo zavolat, chyby byly již vypsány, tak bych
                 * mohl vypsat třeba neúspěch apod. Ale už nic dělat nebudu. Uživatel už o chybě
                 * ví.
                 *
                 *  Zde by se do editoru výstupů vypsali informace o nastalé chybě pri zavolání metody:
                 *  callMethodWithoutParameters
                 */
                return;


            // Zde byla metoda zavolána, tak zjistím, zda metoda má něco vracet:
            if (method.getReturnType().equals(Void.TYPE))
                // zde metoda nemá nic vracet:
                outputEditor.addResult(txtSuccess);

            else {
                // Zde metoda má něco vrátit, tak to vypíšu:
                PopupMenuAbstract.printReturnedValueToEditor(txtSuccess + ": ", returnValue.getObjReturnValue(),
                        outputEditor);

                // Otestuji, zda se vrátila instance, případně se zeptám, zda se má vytvořit
                // reprezentace v diagramu instancí.
                PopupMenuAbstract.checkReturnedValue(returnValue.getObjReturnValue(), instanceDiagram,
                        languageProperties);
            }
        }

        else {
            // Zde metoda obsahuje alespoň jeden parametr, tak ho musím načíst od uživatele:
            // Původní:
            final GetParametersMethodForm gpmf = new GetParametersMethodForm(method, languageProperties, outputEditor,
                    instanceDiagram.isMakeVariablesAvailableForCallMethod(),
                    instanceDiagram.isShowCreateNewInstanceOptionInCallMethod(), instanceDiagram,
                    objInstance.getClass(), reference, instanceDiagram.isMakeMethodsAvailable());

            final List<Object> parametersOfMethod = gpmf.getParameters(method.getName());

            if (parametersOfMethod == null || parametersOfMethod.isEmpty())
                return;

            // Zjistím si návratovou hodnotu a případně ji vypíšu do editoru
            final Called returnValue = VariablesInstanceOfClassForm.operationWithInstances
                    .callMethodWithParameters(method, objInstance, true, parametersOfMethod.toArray());


            // Otestuji, zda se něco vrátilo a pokud ano, vypíši to do editoru výstupů

            if (returnValue == null || !returnValue.isWasCalled())
                /*
                 * else - Zde se metodu nepodařilo zavolat, chyby byly již vypsány, tak bych
                 * mohl vypsat třeba neúspěch apod. Ale už nic dělat nebudu. Uživatel už o chybě
                 * ví.
                 *
                 * else - Zde by se do editoru výstupů vypsali informace o nastalé chybě pri zavolání metody:
                 * callMethodWithoutParameters
                 */
                return;

            // Zde byla metoda zavolána, tak zjistím, zda metoda má něco vracet:
            if (method.getReturnType().equals(Void.TYPE))
                // zde metoda nemá nic vracet:
                outputEditor.addResult(txtSuccess);

            else {
                // Zde metoda má něco vrátit, tak to vypíšu:
                PopupMenuAbstract.printReturnedValueToEditor(txtSuccess + ": ",
                        returnValue.getObjReturnValue(), outputEditor);

                // Otestuji, zda se vrátila instance, případně se zeptám, zda se má vytvořit
                // reprezentace v diagramu instancí.
                PopupMenuAbstract.checkReturnedValue(returnValue.getObjReturnValue(), instanceDiagram,
                        languageProperties);
            }
        }
    }


    @Override
    public String getReferenceTextToClipboard() {
        final StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(getTextOfClassForReference(clazz)).append(method.getName());

        if (!isMethodOverloaded(clazz, method.getName()))
            return stringBuilder.toString();

        /*
         * Zde je metoda přetížená, tak je třeba do výsledného textu přidat i parametry metody, aby bylo jasné, o
         * jakou "konkrétní" metodu se jedná, tedy s jakými parametry.
         */
        stringBuilder.append("(");

        if (method.getParameterCount() > 0)
            stringBuilder.append(ReflectionHelper.getMethodNameAndParametersInText(method.getParameters()));

        stringBuilder.append(")");

        return stringBuilder.toString();
    }


    /**
     * Zjištění, zda třída clazz obsahuje přetíženou metodu methodName.
     * <p>
     * To se zjistí tak, že se zjistí, zda se ve třídě clazz nachází více metod, se stejným názvem.
     *
     * @param clazz
     *         - třída, o které se zjistí, zda obsahuje přetíženou metodu methodName.
     * @param methodName
     *         - název metody, o které se má zjistit, zda je ve třídě clazz přetížená.
     *
     * @return true v případě, že je metodName přetížená ve třídě clazz, jinak false.
     */
    private static boolean isMethodOverloaded(final Class<?> clazz, final String methodName) {
        /*
         * Kolekce pro veškeré názvy metod, které se nachází ve třídě clazz.
         *
         * Je to potřeba, aby bylo možné zjistit, zda je metoda methodName přetížená nebo ne.
         *
         * Pokud je metoda přetížená v příslušné třídě, pak se bude název metody nacházet vícekrát.
         */
        final List<String> allMethodNameListInClass = new ArrayList<>();

        Arrays.stream(clazz.getDeclaredMethods()).forEach(m -> allMethodNameListInClass.add(m.getName()));

        return Collections.frequency(allMethodNameListInClass, methodName) > 1;
    }


    @Override
    public int compareTo(final Object o) {
        return finalTextForShow.compareToIgnoreCase(o.toString());
    }


    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;

        if (!(obj instanceof MethodInfo))
            return false;

        /*
         * Tato podmínka by měla v podstatě stačit. Jedná se o zobrazený text v okně dialogu, který by měl být vždy
         * unikátní pro každou metodu. Tedy žádná metoda by neměla být zobrazena vícekrát.
         */
        if (!finalTextForShow.equals(((MethodInfo) obj).finalTextForShow))
            return false;

        if (clazz != ((MethodInfo) obj).clazz)
            return false;

        if (!method.equals(((MethodInfo) obj).method))
            return false;

        if (!objInstance.equals(((MethodInfo) obj).objInstance))
            return false;

        return reference.equals(((MethodInfo) obj).reference);
    }


    @Override
    public int hashCode() {
        // Zdroj: https://www.mkyong.com/java/java-how-to-overrides-equals-and-hashcode/

        final int prime = 31;

        int result = 17;

        result = prime * result + (finalTextForShow == null ? 0 : finalTextForShow.hashCode());
        result = prime * result + (clazz == null ? 0 : clazz.hashCode());
        result = prime * result + (method == null ? 0 : method.hashCode());
        result = prime * result + (objInstance == null ? 0 : objInstance.hashCode());
        result = prime * result + (reference == null ? 0 : reference.hashCode());

        return result;
    }
}
