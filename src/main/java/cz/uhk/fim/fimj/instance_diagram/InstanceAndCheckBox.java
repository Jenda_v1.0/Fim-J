package cz.uhk.fim.fimj.instance_diagram;

import javax.swing.JCheckBox;

/**
 * Tato třída slouží jako položky do dialogu SelectedInstancesForCreateReference, kdy instance této třídy slouží jako
 * položky do listu v uvedeném dialogu a každá tato položka slouží jako komponenta JCheckBox pro označení všech
 * instancí, na které se mají vytvořit reference.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class InstanceAndCheckBox {

    /**
     * Objekt, který obsahuje příslušnou instanci, na kterou by se vytvořila reference.
     */
    private final Object objInstance;


    /**
     * Komponenta JCheckBox, kterou může uživatel označit, a která značí, zda se pak má vytvořit reference na
     * instanci v
     * objInstance (pokud bude tento chcb označen).
     */
    private final JCheckBox chcb;


    /**
     * Proměnná, která obsahuje text, který se bude vypisovat u příslušné chcb. Bude obsahovat tu instanci - její
     * toString metodu a za ní bude v závorce název třídy (příslušné instance) a balíčky ve kterých se nachází.
     */
    private final String text;


    /**
     * Konstruktor této třídy.
     *
     * @param objInstance
     *         - instance nějaké třídy z diagramu tříd, která se ještě nenachází v diagramu instancí a může se na ní
     *         vytvořit reference a zobrazit se tak v diagramu instancí.
     */
    public InstanceAndCheckBox(final Object objInstance) {
        super();

        this.objInstance = objInstance;


        /*
         * Balíčky, ve kterých / kterém se třída objInstance nachází.
         *
         * už by níže nebylo třeba testovat, zda jsou ty balíčky null, v tuto chvíli
         * vím, že je to instance nějaké třídy z diagramu tříd, takže musí být v nějakém
         * balíčku, ale kdyby se někde náhodou něco pokazilo, tak abych zde předešel
         * výjimce.
         */
        final String packages;

        if (objInstance.getClass().getPackage() != null)
            packages = objInstance.getClass().getPackage().getName();

        else packages = "";

        final String packagesWithClassName = packages + "." + objInstance.getClass().getSimpleName();


        text = objInstance + " (" + packagesWithClassName + ")";

        chcb = new JCheckBox(text);
    }


    public Object getObjInstance() {
        return objInstance;
    }


    public final void setSelected(final boolean selected) {
        chcb.setSelected(selected);
    }

    public final boolean isChcbSelected() {
        return chcb.isSelected();
    }

    public String getText() {
        return text;
    }
}
