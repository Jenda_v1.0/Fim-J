package cz.uhk.fim.fimj.instance_diagram;

/**
 * Tato třída coyb výčet slouží pro definici výčtových hodnot ohledně typu hrany, která se má vytvořit mezi buňakmi,
 * které repezentují instanci v diagramu instancí
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public enum KindOfInstanceEdgeEnum {

    /**
     * Klasická hrana pro reprezentaci asociace, kde je špka na obou stranách a na obou stranách názvy proměnných - nic
     * není null
     */
    ASSOCIATION,

    /**
     * Klasická hrana pro reprezentaci agregaci 1 : 1, tj. na jedné straně nic a na cílové je šipka s názvem proměnné
     */
    AGGREGATION_1_1,

    /**
     * Klasická hrana pro agregaci, kde na zdrojové straně není nic a na cílové je šipka s názvem proměnné a : List
     */
    AGGREGATION_1_N_LIST,

    /**
     * Stejně jako výše, akorát na konci hrany je šipka a název proměnné a : Array [] - tolik hranatých závorek kolik
     * dimenzí má pole
     */
    AGGREGATION_1_N_ARRAY
}