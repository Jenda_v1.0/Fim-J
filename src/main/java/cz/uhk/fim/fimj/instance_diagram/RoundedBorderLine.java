package cz.uhk.fim.fimj.instance_diagram;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.border.AbstractBorder;

import cz.uhk.fim.fimj.file.Constants;

/**
 * Třída slouží k tomu, aby v grafu instance diagram byly buňky, které reprezentují instanci třídy, tak aby měli
 * zaoblené hrany dále využívám Graphics, k designu této buňky, barva pozadí, písmo,...
 * <p>
 * Zdroj (jeden z posledních komentářů): https://stackoverflow
 * .com/questions/239537/how-to-output-a-string-on-multiple-lines-using-graphics
 * <p>
 * Zdroj (oprava zarovnání textu na více řádků - FontMetrics) (druhý komentář od shora):
 * https://stackoverflow.com/questions/2168963/use-java-drawstring-to-achieve-the-following-text-alignment
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class RoundedBorderLine extends AbstractBorder {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Proměnná, která značí mezeru mezi okrajem a příslušným textem - pro
	 * horizontální polohu, takže hodnota této proměnné značí velikost mezery mezi
	 * levým krajem a začátkem příslušnho textu nebo mezi pravým krajem a koncem
	 * příslušného textu.
	 */
	private static final int HORIZONTAL_SPACE = 10;
	
	/**
	 * Proměnná, která značí mezeru mezi okrajem a příslušným textem - pro
	 * vertikální polohu. Takže hodnota této proměnné značí velikost mezery mezi
	 * horním nebo dolním okrajem a příslušným textem.
	 */
	private static final int VERTICAL_SPACE = 5;
	
	
	
	private Color colorBorder, colorCell, textColor, attributeColor, methodColor;
	
	
	/**
	 * Název buňky = název instance třídy, reference, popřípadě atributy a balíčky.
	 */
	private String name;
	
	
	/**
	 * Určuje zaoblení hran:
	 */
	private static int margin;
	
	
	/**
	 * Font pro písmo - název buňky
	 */
	private Font font;
	
	/**
	 * Font, který se aplikuje pro atributy, tedy pro veškeré texty od druhého řádku
	 * (na prvním řádku je pouze info o třídě a instanci - reference).
	 */
	private Font attributeFont;

	/**
	 * Font, který se aplikuje na příslušné řádky, kde senachází seznam metod.
	 */
	private Font methodFont;
	
	
	/**
	 * Načtený soubor s vlastnostmi pro instanceDiagram:
	 */
	private static Properties propInstanceDiagram;
	
	
	/**
	 * vertikální a horizontální zarovnání textu v buňce:
	 */
	private int horizontal, vertical;
	
	
	/**
	 * Proměnná, která značí počet řádků, na kterých senachází atributy příslušné
	 * třídy. První řádek je s referencí, pak další počet countOfVariables jsou
	 * atributy a další metody, pokud bude tato proměnná countOfVariables nula, pak
	 * už následujíc pouze metody, případně ani ty ne.
	 */
	private final int countOfVariables;
	
	
	
	/**
	 * Proměnná, která značí, zda se mají pro písmo, kterým se vypisují atributy
	 * využít stejná barva písma a font písma jako pro vypsání reference - tj.
	 * výchozí text pro instanci jako buňku.
	 */
	private boolean useForAttributesOneFontAndFontColor;

	/**
	 * Proměnná, která značí, zda se mají pro písmo, kterým se vypisují metody
	 * využít stejná barva písma a font písma jako pro vypsání reference - tj.
	 * výchozí text pro instanci jako buňku.
	 */
	private boolean useForMethodsOneFontAndFontColor;
	
	
	
	
	
	/**
	 * Konstuktor třídy
	 *
	 * @param name
	 *            - název buňky = nzev instance třídy, kterou daná buňka
	 *            reprezentuje
	 * 
	 * @param countOfVariables
	 *            - počet proměnných, který byl zobrazen, zde je tato hodnota
	 *            potřeba, aby se vědělo, kolik řádků se obarvit jakou barvou, tj. X
	 *            řádků se má obarvit barvou a aplikovat na ně font pro atributy,
	 *            dalších X řádků se má obarvit další barvou a aplikovat na ně další
	 *            font pro metody. A první řádek je řádek s referencí, tj. je také
	 *            jiný font a barva písma.
	 * 
	 * @param propInstanceDiagram
	 *            - objekt Properties obsahující nastavení pro diagram instnací.
	 */
	public RoundedBorderLine(final String name, final int countOfVariables, final Properties propInstanceDiagram) {
		super();

		this.name = name;
		this.countOfVariables = countOfVariables;
		RoundedBorderLine.propInstanceDiagram = propInstanceDiagram;

		// Naplním proměnné:
		fillVariables();
	}
	
	
	
	
	
	@Override
	public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
		((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);	
		
		
		// Nejprve nastavím černou barvu - aktuálně barva pozadí:
		g.setColor(colorBorder);

		// Vytvořím buňku se zaoblenými hranami - resp vyplněný obdélník:
		g.fillRoundRect(x, y, width, height, margin, margin);

		// Teď nastavím barvu, kterou se má vyplnit jakoby vnitřek buňky,
		// což bude vlastně barva obdélníku uvnitř:
		g.setColor(colorCell);

		// 2. Obdélník uvnitř
		g.fillRoundRect(x + 5, y + 5, width - 10, height - 10, margin, margin);
		
		
		// nastavím barvu písma:
		g.setColor(textColor);
		
		
		/*
		 * Nejprve si vytvořím "obdélník", který bude obsahovat veškerá text pro buňku.
		 * Toto není nějak extra potřeba, ale kdyby se načetly nějaká chybná data ze
		 * souboru apod. Tak by se pak níže nenastavilo správné zarovnání textu apod.
		 * Tak toto je takové výchozí nastavení pro střed buňky, které se níže změní dle
		 * nastavených hodnot ze souboru.
		 */
        final Rectangle2D r2d = font.getStringBounds(name, new FontRenderContext(null, true, true));

		// Šířka a výška textu:
		final int rWidth = (int) Math.round(r2d.getWidth());
		final int rHeight = (int) Math.round(r2d.getHeight());

		// Pozide text - levý horní okraj
		final int rx = (int) Math.round(r2d.getX());
		final int ry = (int) Math.round(r2d.getY());

		// Vypočítám si umístění:
		final int a = (width / 2) - (rWidth / 2) - rx;
		final int b = (height / 2) - (rHeight / 2) - ry;
        
        
		// Nastavím font (nastavený pro text s třídou a referencí) a vykreslím text na
		// vypočítanou pozici:
		g.setFont(font);

		
        // Pozice:
        // Vertikální pozice: 0 = nahoře, 1 = střed, 2 = dolě
        // Horizontální pozice: 0 = vlevo, 1 = střed, 2 = vpravo
        
        // Ve výchozím nastavení bude text na středu
        // Pokud se podaří načíst data ze souboru, nastavení se změní, jinak
        // zůstane text na středu:
		
		// Výchozí hodnoty pro zarovnání písma na střed:
        int textY = y + b, textX = x + a;
        
        
        
        
        /*
		 * Proměnná pro vypočítání výsledné hodnoty pro vertikální zarovnání. Toto je
		 * potřeba, když celkově text pro vykreslení obsahuje více řádků, tak potřebuji
		 * zjistit velikost celkové pro všechny řádky a s tím počítat, jinak bych
		 * nevěděl, kam přesně na vertikální pozici vykreslit příslušný řádek z
		 * celkového textu.
		 */
        int tempY = 0;
        
        /*
		 * Rozdělím se celý text pro vykreslení na řádky.
		 */
        final String[] outputs = name.split("\n");
        
        /*
		 * Zde projdu veškeré řádky a posšítám si velikost všech řádků, zde by byla
		 * možnost jen vynásobit počet řádků nějakou konstantou, ale když se nastaví
		 * jiná velikost písma, pak ta velikost bude také jiná, proto jsem tu konstantu
		 * zavrhl.
		 */
		for (int i = 0; i < outputs.length; i++) {
			/*
			 * Níže si vezmu vždy výšku řádku, resp. textu dle aktuálního fontu. Toto je
			 * potřeba, protože potřebuji zjistit celkovou velikost textu, jak bde ten text
			 * se všemi řádky vysoký, tak si sečku výšku všech řádků dle příslušného fontu,
			 * prootže každý font může mít různou velikost písma. Tak si sečtku velikost
			 * všech řádků dle příslušného fontu a z té vypočítám střed apod. - Dle potřeby
			 * vertikálního zarování textu v buňce reprezentující instanci v diagramu
			 * instancí.
			 */

			// Texty pro první řádek s referencí:
			if (i == 0)
				// Stačílo by (ale kdybych náhodou někde něco změnil a zde na to zapoměl):
//				tempY += g.getFontMetrics().getHeight();
				tempY += g.getFontMetrics(font).getHeight();

			
			// Text pro atributy:
			else if (i <= countOfVariables && GraphInstance.isShowAttributesInInstanceCell()) {
				/*
				 * Jestliže se má využít stejný font pro písmo pro atributy, tak využiji font
				 * 'font' (již je aplikován), ale pokud ne, pak využiji ten font, který je
				 * nastaven pro atributy.
				 */
				if (!useForAttributesOneFontAndFontColor)
					tempY += g.getFontMetrics(attributeFont).getHeight();
				else
					tempY += g.getFontMetrics(font).getHeight();
			}
				

			// Text pro metody (podmínka pro to, zda se zobrazují metody již v této části
			// není třeba):
			else if (GraphInstance.isShowMethodsInInstanceCell()) {
				/*
				 * Jestliže se má využít stejný font pro písmo pro metody, tak využiji font
				 * 'font' (již je aplikován), ale pokud ne, pak využiji ten font, který je
				 * nastaven pro atributy.
				 */
				if (!useForMethodsOneFontAndFontColor)
					tempY += g.getFontMetrics(methodFont).getHeight();
				else
					tempY += g.getFontMetrics(font).getHeight();
			}
		}

		
		
		/*
		 * Zde se vypočítám nové umístění s novou velikostí textu (pro všechny řádky).
		 */
		final int b_2 = (height / 2) - (tempY / 2) - ry;

		// Umístění na vertikální polohu textu:
		if (vertical == 1) // 1 = nahoře
			textY = y + rHeight + VERTICAL_SPACE;
		else if (vertical == 0) // 0 = center
			textY = y + b_2;
		else if (vertical == 3) // 3 = dole
			textY = height - tempY + VERTICAL_SPACE;
        
        
		
        
		
		/*
		 * Vytvořím si list, do kterého si budu vkládat veškeré využité hodnoty Y -
		 * souřadnice pro text na příslušném řádku.
		 * 
		 * Hodnoty v tomto listu potřebuji protože se mi z nějakého důvodu občas
		 * překryly řádky, ale divné bylo, že se to stalo jen občas u nějakých instancí.
		 * Ve většíně případů se texty vykreslovaly jak měly, ale občas se z ničeho nic
		 * překryli, tak mě napadlo toto řešení.
		 * 
		 * Každá použití souřadnice Y pro zobrazení textu na příslušném řádku se přidá
		 * do tohoto listu a při každé iteraci (druhé a více) se otestuje, zda je ta
		 * aktuální souřadnice Y větší než ta předchozí (vždy ta poslední v listu),
		 * pokud ano, pak je to OK, ale pokud ne, pak si vypočítám vždy dvojnásoebk
		 * aktuální velikosti řádku - dle aktuálně využitého fontu. Ten dvojnásobek jsem
		 * zhruba odvodil, že mi pak vyjdou zhruba stejné mezery, protože dopředu nevím,
		 * kdy to nastane (jestli vůbec) a pokud ano, tak o kolik to bude menší, zde by
		 * se ještě hodil cyklus while, který bude násobit tu velikost řádku, dokud
		 * nebude ta soubřadnice Y větší než ta předchozí, ale to je težké dopoředu
		 * říci, zatím mi vždy vystačilo toto, a počítám s tím, že se v tom okně nebude
		 * zobrazovat několik desítek hodnot.
		 * 
		 * 
		 * Important:
		 * Bohužel, pokud taková situace nastane, tak se trochu "rozhodí" vertikální
		 * zarovnání, protože když se najednou přiště index k Y (souřadnici), tak bude
		 * celkové zarovnání trochu někde jinde, protože se budou texty vypisovat níže,
		 * než se počítalo váše, když se zjišťovala celková výška textu se všemi řádky.
		 * 
		 * Bohužel, ale nevím, jak je to možné ani jak to opravit, tak jsem to zarovnání
		 * oželel, navíc to nastane až když jsem měl cca 16 proměnných a cca 18 metod a
		 * jednalo se o jeden řádek.
		 */
		final List<Integer> tempYList = new ArrayList<>();
		
                
		// Zde vykreslím celý text (s více řádky).
		for (int i = 0; i < outputs.length; i++) {
			/*
			 * Zde potřebuji nastavit správnou barvu písma a font pro příslušné texty, pro
			 * první řádek, kde je reference to bude "výchozí" font 'font' (je již
			 * aplikován), pak pro řádky, kde jsou atributy to bude jiný font a barva písma
			 * a pro řádky, kde jsou metody to bude další font a barva písma.
			 * 
			 * K těm podmínkám ještě je vhodné dodat, že se například pro font a bavu písma
			 * pro atributy aplikuje pokud je tedy aktuální řádek větší než 1 (tedy druhý
			 * řádek a výše) a zároveň je řádek menší nebo roven počtu atributů pro
			 * zobrazení, ale toto vše se aplikuji pouze v případě, že se nemá využít stejný
			 * font pro atributy, jako pro referenci. A to samé platí i pro metody, také se
			 * na ně aplikuje příslušný font a barva písma pouze v případě, že je příslušný
			 * řádek větší než index 0 (tedy řádek druhý a výše) a je to již po atributech
			 * (pokud byly zobrazeny) a samozřejmě, jestli se pro metody taky nemá využít
			 * stejný font a barva písma jako pro referenci.
			 * 
			 * Reference je pouze na prvním řádku (index 0). Na ostatních řádcích v
			 * intervalu (indexy řádků) 1 - countOfVariables) jsou atributy, ale to pouze v
			 * případě, že se mají zobrazit a pak jsou metody.
			 */
			if (i > 0 && i <= countOfVariables && GraphInstance.isShowAttributesInInstanceCell()) {
				/*
				 * Zde vím, že je index řádku větší než jedna a zároveň v intervalu počtu
				 * atributů, takže musím ještě otestovat, zda se má aplikovat stejný font a
				 * barva písma jako pro text s referencí, jestli ne, pak aplikuji font a barvu
				 * písma, která má být pro atributy, jinak tu pro referenci.
				 */
				if (!useForAttributesOneFontAndFontColor) {
					g.setColor(attributeColor);
					g.setFont(attributeFont);
				}

				else {
					g.setFont(font);
					g.setColor(textColor);
				}
			}

			// Podmínka, zda semají zobrazovat metody není třeba, jen ta, že je i větší než
			// nula: 
			else if (i > 0 && GraphInstance.isShowMethodsInInstanceCell()) {
				/*
				 * Zde vím, že jsem již mimo rozsah atributů a nejsem na prvním řádku, tj.
				 * nejedná se o text pro referenci, tak ještě musím otestovat, zda se má
				 * aplikovat font a barva písma pro metody nebo se má aplikovat stejný font jako
				 * pro text s referencí.
				 */
				if (!useForMethodsOneFontAndFontColor) {
					g.setColor(methodColor);
					g.setFont(methodFont);
				}

				else {
					g.setFont(font);
					g.setColor(textColor);
				}
			}
			
			
			
			
			/*
			 * Zde si potřebuji znovu vytvořit "obdélník", ale tentokrát pro určení pozice
			 * levého horního rohu, se kterou se bude dále preacovat pro horizontální
			 * zarovnání textu.
			 */
			final Rectangle2D r2d2 = g.getFont().getStringBounds(outputs[i], new FontRenderContext(null, true, true));

			// Šířka a výška textu:
			final int rWidth_2 = (int) Math.round(r2d2.getWidth());

			// Pozice text - levý horní okraj
			final int rx_2 = (int) Math.round(r2d2.getX());

			// Vypočítám si umístění:
			final int a_2 = (width / 2) - (rWidth_2 / 2) - rx_2;
			
			
			

			// Přetypování na Graphics2D.
			final Graphics2D g2 = (Graphics2D) g;
			/*
			 * Toto je potřeba, kvůli správnému zarovnání, když jsem to nepoužil, tak se mi
			 * nepodařilo správně zarovnat některé řádky.
			 */
			final FontMetrics fontMetrics = g2.getFontMetrics();

			// horizontální zarovnání textu:
			if (horizontal == 2) // 2 = vlevo
				textX = x + HORIZONTAL_SPACE;
			else if (horizontal == 0) // 0 = center
				textX = x + a_2;
			else if (horizontal == 4) // 4 = vpravo
				textX = width - fontMetrics.stringWidth(outputs[i]) - HORIZONTAL_SPACE;
			
			
			// Vykreslení textu (S tímto se občas překreslovaly texty, nevím proč, oprava níže):
//			g2.drawString(outputs[i], textX, (textY + i * g.getFontMetrics().getHeight() + 1));
			
			
			/*
			 * Vypočítám si aktuální pozici pro Y:
			 */
			int currentY = (textY + i * g.getFontMetrics().getHeight() + 1);
			
			/*
			 * Zde přichází na řadu testování (popsáno výše u listu tempYList). Zde je potřeba otestovat, zda náhodou
			 * aktuální souřadnice Y není menší než ta předchozí, pokud ano, pak zdvojnásobím
			 */
			if (i > 0 && currentY < tempYList.get(i - 1)) {
				/*
				 * Toto bylo původní, vždy dvojnásobek řádku, protože v této části vím, že by se
				 * vykresloval aktuální text na ten předchozí, tak jen zdvojnásobím řádek:
				 */
//				textY += 2 * g.getFontMetrics().getHeight();
				
				
				/*
				 * Toto je nové (oprava předchozího zakomentovaného řádku.
				 * 
				 * V podstatě zde pouze testuji, že jestli je ta aktuální souřadnice Y menší než
				 * ta předchozí, nebo je velikost mezery - řádku menší než tak, která má být dle
				 * aktuálního fontu (druhá část podmínky), pak inkrementuji souřadnici Y, dokud
				 * nedosáhne takové hodnoty, aby byla správná velikost řádku (případně volného
				 * řádku) dle aktuální velikosti zvoleného fontu a nepřekrývali se texty s tím
				 * předchozím.
				 * 
				 * (Když už tento případ nastal, tak pouze když se jednalo o volný řádek (\n),
				 * takže pak u toho vykreslení je část 'else', nevykresluje se zrovna - jen
				 * psunu souřadnici na příslušnou pozici a vykreslím až další text.)
				 */
				while (currentY < tempYList.get(i - 1)
						|| (currentY - tempYList.get(i - 1) < g.getFontMetrics().getHeight())) {
					textY++;

					// Znovu přepočítám aktuální hodnotu.
					currentY = (textY + i * g.getFontMetrics().getHeight() + 1);
				}
			}

			
			// Zde mohu vykreslit příslušný text (else, protože v podmínce výše by měl být
			// pouze volný řádek, zde již ne):
			else
				g2.drawString(outputs[i], textX, currentY);

			
			// Přidám výsledek výpočtu pro další testování - v další iteraci:
			tempYList.add(currentY);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda,k terá slouží pro naplněná proměnných, které obsahují nastavení pro
	 * buňku - například barvu písma, barvu ohraničení atd. Veškeré potřebné hodnoty
	 * vloží do příslušných proměnných a využijí se při vykreslování příslušné buňky
	 * (instance nějaké třídy v diagramu instancí).
	 */
	private void fillVariables() {
		
		// Zjistím si zaoblení hran buňky:
		if (propInstanceDiagram != null) {
			// Zaoblené hrany:
			margin = Integer.parseInt(propInstanceDiagram.getProperty("RoundedBorderMargin", String.valueOf(Constants.ID_ROUNDED_BORDER_MARGIN)));
			
			// Barva písma:
			final String colorForText = propInstanceDiagram.getProperty("TextColor", Integer.toString(Constants.ID_TEXT_COLOR.getRGB()));
			textColor = new Color(Integer.parseInt(colorForText));
			
						
			// Barva ohraničení:
			final String borderColorText = propInstanceDiagram.getProperty("BorderColor", Integer.toString(Constants.ID_COLOR_BORDER.getRGB()));
			colorBorder = new Color(Integer.parseInt(borderColorText));
					
			// Barva buňky:
			final String cellColorText = propInstanceDiagram.getProperty("CellColor", Integer.toString(Constants.ID_COLOR_CELL.getRGB()));
			colorCell = new Color(Integer.parseInt(cellColorText));
			
			// Font:
			final int fontStyle = Integer.parseInt(propInstanceDiagram.getProperty("FontStyle", Integer.toString(Constants.ID_FONT.getStyle())));
			final int fontSize = Integer.parseInt(propInstanceDiagram.getProperty("FontSize", Integer.toString(Constants.ID_FONT.getSize())));
			font = new Font(Constants.ID_FONT.getName(), fontStyle, fontSize);
			
			horizontal = Integer.parseInt(propInstanceDiagram.getProperty("TextHorizontalAlignment", Integer.toString(Constants.ID_HORIZONTAL_ALIGNMENT)));
			vertical = Integer.parseInt(propInstanceDiagram.getProperty("TextVerticalAlignment", Integer.toString(Constants.ID_VERTICAL_ALIGNMENT)));
			
			
			
			// Zda se má využít pro atributy stejná barva a font jako pro referenci:
			useForAttributesOneFontAndFontColor = Boolean.parseBoolean(propInstanceDiagram.getProperty("UseForAttributesOneFontAndFontColorInInstanceCell", String.valueOf(Constants.ID_USE_FOR_ATTRIBUTES_ONE_FONT_AND_FONT_COLOR_IN_INSTANCE_CELL)));

			
			// Barvy písma pro atributy:
			final String attributeColorText = propInstanceDiagram.getProperty("AttributesTextColor", Integer.toString(Constants.ID_ATTRIBUTES_TEXT_COLOR.getRGB()));
			attributeColor = new Color(Integer.parseInt(attributeColorText));
			
			
			// Font pro atributy:
			final int attributeFontStyle = Integer.parseInt(propInstanceDiagram.getProperty("AttributesFontStyle", Integer.toString(Constants.ID_ATTRIBUTES_FONT.getStyle())));
			final int attributeFontSize = Integer.parseInt(propInstanceDiagram.getProperty("AttributesFontSize", Integer.toString(Constants.ID_ATTRIBUTES_FONT.getSize())));
			attributeFont = new Font(Constants.ID_ATTRIBUTES_FONT.getName(), attributeFontStyle, attributeFontSize);


			// Zda se má využít pro metody stejná barva a font jako pro referenci:
			useForMethodsOneFontAndFontColor = Boolean.parseBoolean(propInstanceDiagram.getProperty("UseForMethodsOneFontAndFontColorInInstanceCell", String.valueOf(Constants.ID_USE_FOR_METHODS_ONE_FONT_AND_FONT_COLOR_IN_INSTANCE_CELL)));
			
			// Barva písma pro metody:
			final String methodColorText = propInstanceDiagram.getProperty("MethodsTextColor", Integer.toString(Constants.ID_METHODS_TEXT_COLOR.getRGB()));
			methodColor = new Color(Integer.parseInt(methodColorText));
			
			
			// Font pro metody:
			final int methodFontStyle = Integer.parseInt(propInstanceDiagram.getProperty("MethodsFontStyle", Integer.toString(Constants.ID_METHODS_FONT.getStyle())));
			final int methodFontSize = Integer.parseInt(propInstanceDiagram.getProperty("MethodsFontSize", Integer.toString(Constants.ID_METHODS_FONT.getSize())));
			methodFont = new Font(Constants.ID_METHODS_FONT.getName(), methodFontStyle, methodFontSize);
		}		

		
		
		else {
			margin = Constants.ID_ROUNDED_BORDER_MARGIN;

			textColor = Constants.ID_TEXT_COLOR;

			colorBorder = Constants.ID_COLOR_BORDER;

			colorCell = Constants.ID_COLOR_CELL;

			font = Constants.ID_FONT;

			horizontal = Constants.ID_HORIZONTAL_ALIGNMENT;

			vertical = Constants.ID_VERTICAL_ALIGNMENT;

			// Zda se má využít pro atributy stejná barva a font jako pro referenci:
			useForAttributesOneFontAndFontColor = Constants.ID_USE_FOR_ATTRIBUTES_ONE_FONT_AND_FONT_COLOR_IN_INSTANCE_CELL;

			// Barvy písma pro atributy:
			attributeColor = Constants.ID_ATTRIBUTES_TEXT_COLOR;

			// Font pro atributy:
			attributeFont = Constants.ID_ATTRIBUTES_FONT;

			// Zda se má využít pro metody stejná barva a font jako pro referenci:
			useForMethodsOneFontAndFontColor = Constants.ID_USE_FOR_METHODS_ONE_FONT_AND_FONT_COLOR_IN_INSTANCE_CELL;

			// Barva písma pro metody:
			methodColor = Constants.ID_METHODS_TEXT_COLOR;

			// Font pro metody:
			methodFont = Constants.ID_METHODS_FONT;
		}
	}
}