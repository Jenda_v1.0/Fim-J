package cz.uhk.fim.fimj.instance_diagram.overview_of_instance;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Properties;

import javax.swing.JOptionPane;

import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;
import cz.uhk.fim.fimj.forms.NewInstanceForm;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;
import cz.uhk.fim.fimj.output_editor.OutputEditor;

/**
 * Tato třída slouží jako položky do komponenty JList v dialogu pro přehled hodnot v instancí třídy. Každá položka
 * (instance této třídy) slouží jako informace ohledně konstruktoru v příslušné třídě.
 * <p>
 * Tato třída je zde doplněna, protože jsem uživateli chtěl "zpřístupní" i zavolání konstruktoru, když o něm má zrovna
 * přehled v dialogu a rozmyslel se, že ho zavolá, tak aby nemusel zbytečně zavírat dialog, najít třídu v diagramu tříd,
 * vyhledat konstruktor a zavolat jej.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ConstructorInfo extends ValuesInfo {

    /**
     * Konstruktor, který konkrétní instance této třídy představuje v dialogu s přehledem hodnot o instance. Tento
     * konstruktor je případně možné zavolat.
     */
    private final Constructor<?> constructor;


    /**
     * Konstruktor této třídy.
     *
     * @param finalTextForShow
     *         - text, který se má zobrazit v komponentě JList, jedná se o text konstruktoru constructor, který
     *         představuje konkrétní instance této třídy.
     * @param constructor
     *         - konstruktor, který představuje konkrétní instance této třídy. Jedná se o konstruktor načtený z
     *         příslušné instance, kterou si aktuálně v diagramu instancí uživatel prohlíží.
     * @param clazz
     *         - načtená přeložená / zkompilovaná třída, kterou si uživatel prohlíží - její hodnoty. Z této třídy byl
     *         získán konstruktor.
     */
    ConstructorInfo(final String finalTextForShow, final Constructor<?> constructor, final Class<?> clazz) {
        super(finalTextForShow, clazz);

        this.constructor = constructor;
    }


    /**
     * Metoda, která slouží pro zavolání konstruktoru, který konkrétní instance této třídy reprezentuje.
     *
     * @param languageProperties
     *         - objekt Properties obsahující text pro tuto aplikace v uživatelem zvoleném jazyce.
     * @param outputEditor
     *         - nějaký editor výstupů, kam se mají přesměrovat texty, které se vypíšou například v konstruktoru nebo
     *         pro chybové hlášení apod.
     * @param txtFailedToCreateInstanceText
     *         - text do chybové hlášky v případě, že dojde k nějaké chybě a příslušný konstruktor se nepodaří zavolat.
     * @param txtFailedToCreateInstanceTitle
     *         - titulek do chybové hlášky, která může nastat v případě, že dojde k nějaké chybě a konstruktor se
     *         nepodaří zavolat.
     * @param instanceDiagram
     *         - reference na diagram instancí pro předání do dialogu pro zavolání konstruktoru.
     */
    public final void callConstructor(final Properties languageProperties, final OutputEditor outputEditor,
                                      final String txtFailedToCreateInstanceText,
                                      final String txtFailedToCreateInstanceTitle,
                                      final GraphInstance instanceDiagram) {

        // Získám hodnoty z dialogu
        final NewInstanceForm nif = new NewInstanceForm(constructor, languageProperties, outputEditor,
                GraphClass.isMakeVariablesAvailable(), GraphClass.isShowCreateNewInstanceOptionInCallConstructor(),
                instanceDiagram, clazz, GraphClass.isMakeMethodsAvailableForCallConstructor());

        final List<Object> valuesForInstance = nif.getValuesForNewInstance(clazz.getName());



        /*
         * Prměnná, do které si uložím balíčky, ve kterých / kterém se třída nachází.
         */
        final String packageName;

        /*
         * Získám si balíček, vekterém se třída nachází, níže bych pak nemusel testovat,
         * že je to null, protože už v diagramu tříd při vytvoření třídy si hlídám, že
         * se třída musí nacházet alespoň v nějakém (jednom) balíčku, jinak ji ani nelze
         * vytvořit, proto zde dále neřeším null hodnoty.
         */
        final Package pcgName = clazz.getPackage();

        // Toto by nikdy nemělo nastat:
        if (pcgName != null)
            packageName = pcgName.getName();
        else
            packageName = null;


        if (valuesForInstance != null && !valuesForInstance.isEmpty()) {
            // Zde uživatel klikl na OK - nezavřel dialog

            // Otestuji, jakou mám zavolat metodu pro vytvoření instance - s parametry nebo
            // bez
            if (valuesForInstance.size() == 1) {
                // Zde se zavolat konstruktor bez parametrů:
                final boolean isNewInstanceCreated = VariablesInstanceOfClassForm.operationWithInstances.createInstance(
                        clazz.getSimpleName(), packageName, valuesForInstance.get(0).toString(), constructor);

                if (!isNewInstanceCreated)
                    JOptionPane.showMessageDialog(null,
                            txtFailedToCreateInstanceText + ": " + clazz.getSimpleName() + "!",
                            txtFailedToCreateInstanceTitle, JOptionPane.ERROR_MESSAGE);
            }

            else {
                // Zde se jedná i o parametry:

                // Zjistím si název reference
                final String referenceName = valuesForInstance.get(0).toString();

                // Odeberu z kolekce název reference, aby zuůstaly pouze parametry, referenci
                // mám uloženou výše
                valuesForInstance.remove(0);

                final boolean isNewInstanceCreated = VariablesInstanceOfClassForm.operationWithInstances
                        .createInstanceWithParameters(clazz.getSimpleName(), packageName, referenceName, constructor,
                                true, valuesForInstance.toArray());

                if (!isNewInstanceCreated)
                    JOptionPane.showMessageDialog(null,
                            txtFailedToCreateInstanceText + ": " + clazz.getSimpleName() + "!",
                            txtFailedToCreateInstanceTitle, JOptionPane.ERROR_MESSAGE);
            }
        }
    }


    @Override
    public String getReferenceTextToClipboard() {
        final StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(getTextOfClassForReference(clazz)).append(clazz.getSimpleName());

        // Pokud je ve třídě pouze jeden konstruktor, pak může vrátit text sestavený výše:
        if (clazz.getDeclaredConstructors().length == 1)
            return stringBuilder.toString();

        /*
         * Zde má třída více konstruktorů, tak je třeba do závorky přidat parametry zvoleného konstruktoru - jsou li
         * nějaké. Minimálně bude alespoň prázdná závorka.
         */
        stringBuilder.append("(");
        if (constructor.getParameterCount() > 0)
            stringBuilder.append(ReflectionHelper.getMethodNameAndParametersInText(constructor.getParameters()));

        stringBuilder.append(")");

        return stringBuilder.toString();
    }


    @Override
    public int compareTo(final Object o) {
        return finalTextForShow.compareToIgnoreCase(o.toString());
    }


    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;

        if (!(obj instanceof ConstructorInfo))
            return false;

        /*
         * Tato podmínka by měla v podstatě stačit. Jedná se o zobrazený text v okně dialogu, který by měl být vždy
         * unikátní pro každou metodu. Tedy žádná metoda by neměla být zobrazena vícekrát.
         */
        if (!finalTextForShow.equals(((ConstructorInfo) obj).finalTextForShow))
            return false;

        if (clazz != ((ConstructorInfo) obj).clazz)
            return false;

        return constructor.equals(((ConstructorInfo) obj).constructor);
    }


    @Override
    public int hashCode() {
        // Zdroj: https://www.mkyong.com/java/java-how-to-overrides-equals-and-hashcode/

        final int prime = 31;

        int result = 17;

        result = prime * result + (finalTextForShow == null ? 0 : finalTextForShow.hashCode());
        result = prime * result + (clazz == null ? 0 : clazz.hashCode());
        result = prime * result + (constructor == null ? 0 : constructor.hashCode());

        return result;
    }
}
