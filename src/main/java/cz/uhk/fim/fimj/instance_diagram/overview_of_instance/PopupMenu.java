package cz.uhk.fim.fimj.instance_diagram.overview_of_instance;

import cz.uhk.fim.fimj.file.ClipboardHelper;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;

import javax.swing.*;
import java.util.Properties;

/**
 * Kontextové menu, které obsahuje možnosti pro zkopírování reference do schránky (Ctrl + C).
 * <p>
 * Toto menu se otevře po kliknutí pravým tlačítkem myši na položku v dilogu s přehledem hodnot o instanci v diagramu
 * instancí.
 * <p>
 * Položkou se myslí konstruktor, proměnná nebo metoda, které jsou zobrazeny ve zmíněném dialogu.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 12.08.2018 23:21
 */

public class PopupMenu extends JPopupMenu implements LanguageInterface {

    private static final long serialVersionUID = 8742467631557707729L;


    /**
     * Položka pro zkopírování textu do schránky v používané platformě.
     */
    private final JMenuItem itemCopy;


    /**
     * Konstruktor třídy.
     *
     * @param textForClipboard
     *         - text, který se má vložit do schránky.
     */
    public PopupMenu(final String textForClipboard) {
        super();

        itemCopy = new JMenuItem();
        itemCopy.setToolTipText(textForClipboard);
        add(itemCopy);

        /*
         * Přidání události na kliknutí na položku pro zkopírování / vložení textu do schránky.
         *
         *
         * Note:
         *
         * Pokud se do této třídy bude přidávat další položka, bylo by vhodné implementovat ActionListener, do
         * kterého vkládat akce pro jednotlivé položky.
         */
        itemCopy.addActionListener(event -> ClipboardHelper.copyToClipboard(textForClipboard));
    }


    @Override
    public void setLanguage(final Properties properties) {
        if (properties != null)
            itemCopy.setText(properties.getProperty("Ooi_Pm_ItemCopy", Constants.OOI_PM_ITEM_COPY));

        else itemCopy.setText(Constants.OOI_PM_ITEM_COPY);
    }
}
