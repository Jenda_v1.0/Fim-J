package cz.uhk.fim.fimj.instance_diagram;

import org.jgraph.graph.DefaultGraphCell;

/**
 * Tato třída slouží pro uchování informací o hraně, která by se měla buď vytvořit nebo by měla existovat mezi
 * příslušnými buňkami v diagramu instancí.
 * <p>
 * Informace jako jsou zdrojová a cílová buňka, mezi kterými by měla existovat hrana. Dále typ té hrany jako je třeba
 * asociace a agregace 1:1 nebo 1:N, dále jestli jsou jejich koneci null hodnota (pro asociaci) nebo ne a texty, které
 * by měla hrana obsahovat, buď na obou koncích pro asociaci nebo na jednom konci pro agregaci
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class InfoAboutEdgesForCreate {

    /**
     * Zdrojová a cílová buňka
     */
    private final DefaultGraphCell srcCell;
    private final DefaultGraphCell desCell;


    // texty, které by měly být na straně začátku nebo konce hrany
    private final String srcSide;
    private final String desSide;


    /**
     * Proměnná pro typ hrany, která se má vytvořit mezi buňkami:
     */
    private final KindOfInstanceEdgeEnum kindOfEdge;


    // Naplnění asociace s obema naplněnými hodnotami, na obou stranach jsou sipky i texty

    /**
     * Koknstruktor této třídy, který slouží pro naplnění potřebných dat pro vytvoření hrany reprezentující asociaci
     * mezi dvěma příslušnými buňkami v diagramu instancí
     * <p>
     * Tato hrana typu asociace bude mít na obou straných šipky i texty.
     *
     * @param srcCell
     *         - zdrojová buňka
     * @param desCell
     *         - cílová buňka
     * @param srcSide
     *         - text na zdrojové straně hrany
     * @param desSide
     *         - text na cílové straně hrany
     */
    public InfoAboutEdgesForCreate(final DefaultGraphCell srcCell, final DefaultGraphCell desCell, final String
            srcSide, final String desSide) {

        super();

        this.srcCell = srcCell;
        this.desCell = desCell;
        this.srcSide = srcSide;
        this.desSide = desSide;

        // Jedná se o asociaci:
        kindOfEdge = KindOfInstanceEdgeEnum.ASSOCIATION;
    }


    // Konstruktor pro agregace 1 : N s Listem , text a sipka je akorat na cilove strane, ale na cilove strane se
    // prida : List
    // Nebo: 	Konstruktor pro agregaci 1 : 1, kde sipka a text promenne na cilove strane

    /**
     * Tento konstruktor slouží pro naplnění informací o dvou typech hran:
     * <p>
     * Pro Agregaci 1 : N s Listem nebo Pro Agregaci 1 : 1.
     * <p>
     * Konstruktor pro Agregaci 1 : N bude mít šipku pouze na cílové straně hrany a popisek taky na cílové straně hrany,
     * ale přidá se za název proměnné ještě : List
     * <p>
     * Konstruktor pro Agregace 1 : 1, bude mít také šipku pouze na cílové straně hreny a tam bude mít i text s názvem
     * proměnné.
     *
     * @param srcCell
     *         - zdrojová buňka pro hranu
     * @param desCell
     *         - cílová buňka pro hranu
     * @param desSide
     *         - text na cílové straně hrany
     * @param kindOfEdge
     *         - výštový typ hrany, dle které poznám, o kterou hranu jde
     */
    public InfoAboutEdgesForCreate(final DefaultGraphCell srcCell, final DefaultGraphCell desCell, final String
            desSide, final KindOfInstanceEdgeEnum kindOfEdge) {

        super();

        this.srcCell = srcCell;
        this.desCell = desCell;
        this.kindOfEdge = kindOfEdge;

        if (kindOfEdge.equals(KindOfInstanceEdgeEnum.AGGREGATION_1_N_LIST))
            this.desSide = desSide + " : List";

        else this.desSide = desSide;

        // zbytek proměnných nebude potřeba:
        srcSide = null;
    }


    // Konstruktor pro agregace 1 : N s Polem, text a sipka je akorat na cilove strane, ale na cilove strane se prida
    // : Array [] - zavorky dle poctu dimenzi

    /**
     * Konstruktor pro naplnění informací o hraně, která v diagramu instancí reprezentuje Agregaci 1 : N ale s polem,
     * takže šipka bude pouze na cílové straně hrany, tam bude i text s proměnnou, ale za proměnnou se ještě přidá tolik
     * hranatých závorek, kolik má pole dimenzí.
     *
     * @param srcCell
     *         - zdrojová buňka pro hranu
     * @param desCell
     *         - cílová buňka pro hranu
     * @param desSide
     *         - text na cílové straně hrany
     * @param countOfDimension
     *         - počet dimenzí pole = počet hranatých závorek za textem proměnné na hraně
     */
    public InfoAboutEdgesForCreate(final DefaultGraphCell srcCell, final DefaultGraphCell desCell, final String
            desSide, final int countOfDimension) {

        super();

        this.srcCell = srcCell;
        this.desCell = desCell;
        kindOfEdge = KindOfInstanceEdgeEnum.AGGREGATION_1_N_ARRAY;


        // cyklus, který do násleudjící promenně dimensionIntext vloží tolik hranatých závorek coby pole, kolik má
        // pole dimenzí:
        String dimensionIntext = "";

        for (int i = 0; i < countOfDimension; i++)
            dimensionIntext += "[]";

        // uložím danou proměnou pro text na hranu:
        this.desSide = desSide + " : Array" + dimensionIntext;


        // zbytek nepotřebuji:
        srcSide = null;
    }


    /**
     * Klasický getr na proměnnou srcCell coby zdrojovou buňku pro hranu
     *
     * @return vrátí referenci na zdrojovou buňku - srcCell
     */
    final DefaultGraphCell getSrcCell() {
        return srcCell;
    }


    /**
     * Klasický getr na proměnnou desCell coby cílovou buňku pro hranu
     *
     * @return vrátí referenci na proměnnou desCell
     */
    final DefaultGraphCell getDesCell() {
        return desCell;
    }


    /**
     * Metoda - klasický getr na proměnnou srcSide
     *
     * @return referenci na proměnou srcSide coby popisek na zdrojové straně hrany
     */
    final String getSrcSide() {
        return srcSide;
    }


    /**
     * Metoda - klasický getr na proměnnou desSide coby popisek na cílové straně hrany
     *
     * @return reference na proměnnou desSide
     */
    final String getDesSide() {
        return desSide;
    }


    /**
     * Klasický getr na proměnou kindOfEdge, který drží informaci o typu hrany
     *
     * @return referenci na kindOfEdge
     */
    final KindOfInstanceEdgeEnum getKindOfEdge() {
        return kindOfEdge;
    }
}