package cz.uhk.fim.fimj.instance_diagram;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.reflection_support.ParameterToText;
import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;
import cz.uhk.fim.fimj.forms.GetParametersMethodForm;
import cz.uhk.fim.fimj.instances.Called;
import cz.uhk.fim.fimj.instances.Instances;
import cz.uhk.fim.fimj.instances.OperationsWithInstances;
import cz.uhk.fim.fimj.output_editor.OutputEditor;
import cz.uhk.fim.fimj.popup_menu.MethodInfo;
import cz.uhk.fim.fimj.popup_menu.PopupMenuAbstract;
import cz.uhk.fim.fimj.popup_menu.PopupMenuScroller;

/**
 * Tato třída slouží jako "menu", které se otevře po kliknutí pravým tlačítkem na buňku, která reprezentuje v diagramu
 * instancí instanci nějaké třídy z diagramu tříd
 * <p>
 * v tomoto menu bude přehled metod, které daná třída jejiži nstanci buňka reprezentuje obsahuje a které lze zavolat
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class PopupMenu extends PopupMenuAbstract implements LanguageInterface {

	private static final long serialVersionUID = -1458587711393391710L;


    private final JMenuItem itemRemove;
    private final JMenuItem itemViewVariables;
	
	/**
	 * Komponenta, která slouží jako menu, které obsahuje další položky. Tyto
	 * položky značí metody, které jsou zděděné z třídy Object.
	 */
	private final JMenu menuInheritedMethodsFromObject;
	
	/**
	 * Komponenta, která slouží jako menu, které obsahuje veškeré zděděné metody z
	 * předka, resp. ze všech předků, aby šli zavolat.
	 * 
	 * Vím, že by šlo zavolat i privátní metody, ale budou zde zobrazeny vždy jen
	 * veřejné a chráněné metody, abych zde dodržoval alespoň trochu ty pravidla
	 * jazyka Java. I když nad instancí samotnou jdou zavolat veškeré metody, včetně
	 * těch privátních. Ale taky bych mohl udělat, že půjdou zavolat všechny, ale
	 * nebudu to přehánět.
	 */
	private final JMenu menuInheritedMethods;
	
	
	
	
	/**
	 * Proměnná, do které se vloží text 'Úspěch' v příslušném jazyce, jedná se o
	 * text, který se bude vypisovat do editoru výstupů v případě úspěšného zavolání
	 * metody.
	 */
	private String txtSuccess;











	/**
	 * Konstruktor této třídy.
	 *
	 * @param cell
	 *         - označená buňka - instance.
	 * @param outputEditor
	 *         - editor pro výpis hlášení.
	 * @param instanceDiagram
	 *         - reference na diagram instancí.
	 * @param languageProperties
	 *         - objekt s texty pro tuto aplikaci ve zvoleném jazyce.
	 * @param includePrivateMethod
	 *         - zda se mají načíst i privátní metody - toto je potřeba, když se mají načíst i do menu i privátní
	 *         metody, například, když se označí buňka - instnace a je tak nastaveno, ale jinak nad instancí nejde
	 *         zavolat privátní metoda, ta je přístupná jen "uvnitř" té třídy / instance.
	 * @param includeProtectedMethods
	 *         - zda se mají načíst do kontextového menu i chráněné metody.
	 * @param includePackagePrivateMethods
	 *         - zda se mají načíst do kontextového menu i metody s viditelností package-private.
	 * @param makeVariablesAvailableForCallMethod
	 *         - logická proměnné, dle které se pozná, zda se mají zpřístupnit veřejné a chráněné proměnné v třídách v
	 *         diagramu tříd, v instancích v diagramu instancí a vytvořených proměnných pomocí editorupříkazů -> tyto
	 *         hodnoty ze zpřístupní pro předání do parametru pro zavolání metody. Bude se jednat o hodnoty puze
	 *         stejného datového typu,které bude možné předat do příslušného parametru metody.
	 */
	public PopupMenu(final InstanceCell cell, final OutputEditor outputEditor, final GraphInstance instanceDiagram,
                     final Properties languageProperties, final boolean includePrivateMethod, final boolean
							  includeProtectedMethods, final boolean includePackagePrivateMethods,
                     final boolean makeVariablesAvailableForCallMethod) {
		
		this.instanceDiagram = instanceDiagram;
		
		
		this.outputEditor = outputEditor;
		this.makeVariablesAvailableForCallMethod = makeVariablesAvailableForCallMethod;
		
		operationWithInstances = new OperationsWithInstances(outputEditor, languageProperties, instanceDiagram);
		
		
		
		/*
		 * Vytvořím si "menu", které bude obsahovat položky s metodami zděděné z třídy
		 * Object a tyto metody budou volány nad příslušnou instancí nějaké třídy,
		 * nadkterou bylo otevřeno toto kontextové menu v diagramu instancí.
		 */
		menuInheritedMethodsFromObject = new JMenu();
		// Zde bych to scrolování použít nemusel nad třídou Object je vždy definovaný
		// počet metod
		new PopupMenuScroller(menuInheritedMethodsFromObject, SCROLL_COUNT, SCROLL_INTERVAL, TOP_FIXED_COUNT,
				BOTTOM_FIXED_COUNT);
		add(menuInheritedMethodsFromObject);
		addSeparator();



		/*
		 * Vytvořím si "menu", do kterého vložím metody ze všech předků, které jsou
		 * veřejné nebo chráněné.
		 */
		menuInheritedMethods = new JMenu();
		new PopupMenuScroller(menuInheritedMethods, SCROLL_COUNT, SCROLL_INTERVAL, TOP_FIXED_COUNT, BOTTOM_FIXED_COUNT);


		
		// Zde přidám metody zděděné ze všech předků označené instance:
		addInheritedMethods(cell.getReferenceName());

		/*
		 * Pouze v případě, že byly výše nalazeny a do nabídky menuInheritedMethods přidány nějaké zděděné metody, pak
		  * je zobrazím v tomto kontextovém
		 * menu, jinak tam být nemusí. Pro uživatele to bude jen "prázdná nabídka", tak proč ji tam zobrazovat.
		 */
        if (menuInheritedMethods.getItemCount() > 0) {
            add(menuInheritedMethods);
            addSeparator();
            // V tomto případě je třeba ukotvit horní 4 položky (Zděděno z Object, zděděné metody a 2 separátory.
            new PopupMenuScroller(this, SCROLL_COUNT_METHODS, SCROLL_INTERVAL, 4, 4);
        }
        /*
         * V tomto případě se nepřidá položka se zděděnými metodami z jiných tříd než li z Object. Proto je třeba
         * ukotvit horní dvě položky pro rolování metod (Zděděno z Object a separátor). Jinak by byly ukotvené dvě
         * metody, které by se měly rolovat šipkami.
         */
        else new PopupMenuScroller(this, SCROLL_COUNT_METHODS, SCROLL_INTERVAL, 2, 4);




		// Zde přidám metody zděděné z třídy Object:
		addMethodsInheritedFromObject(cell.getReferenceName());



		// Zde přidám metody z příslušné instance:
		final boolean addedMethod = addAllMethod(cell.getReferenceName(), includePrivateMethod,
				includeProtectedMethods, includePackagePrivateMethods);
		
		/*
		 * Pouze v případě, že byla přidána alespoň jedna metoda, tak se přidá separátor
		 * pro oddělení metod a "položek" pro manipulaci s instancí - její odebrání a
		 * prohlížení hodnot.
		 * 
		 * Je to takto udělané, protože jsem usoudil, že je trochu přehlednější, když
		 * nebudu dávat separátor pod každou položku pro zavolání metody - resp. metodu.
		 */
		if (addedMethod)
			addSeparator();
		
		
		
		// Výchozí položka pro odebrání instance
		itemViewVariables = new JMenuItem();		
		add(itemViewVariables);

		addSeparator();
		
		itemRemove = new JMenuItem();
		// Červená barva písma:
		itemRemove.setForeground(REMOVE_ITEM_FOREGROUND_COLOR);
		add(itemRemove);
		
		
		
		
		
		// Události na poslední dvě tlačítka:
		itemViewVariables.addActionListener(event -> instanceDiagram.showInfoAboutInstance(cell));
		
			
		
		
		
		itemRemove.addActionListener(event -> {
			// Odeberu buňku z diagramu intanci, včetně vztahů a instance v mape s intancemi:
			instanceDiagram.removeInstance(cell);
			
			// je třeba aktualizovat jiz vytvořené instance - aby se prenacetly reference
			// v naplnenych promennych, protoze jiz odebrana instance neexistuje, tak ji musim
			// smazat i ze vsech promenncych v intancich, protoze dana instnace tedy neexistuje:
			
			// Nejprve je třeba zjistit, zda byla nalezena cesta k adresáři bin v otevřeném projektu://	
			final String pathToBinDir = App.READ_FILE.getPathToBin();
						
			if (pathToBinDir != null)
				// Zde byla nalezena cesta k adresáři bin, popř byl znovu vytvořen,
				// je tedy třeba přenačíst veškeré instance a jejich proměnné, protože se
				// zde jedna odebrala, tak ji pomocí metody výše - removeInstance smažu z diagramu
				// a s mapy, kde jsou ukládány, ale dále je potřebna přenačíst i hodnoty instancí,
				// protože na tu smazanou mohla nějaká ukazovat, tak je třeba ji nastavit na null:
				Instances.refreshClassInstances(pathToBinDir, outputEditor, languageProperties, true);
		});
		
		
		
		
		
		
		setLanguage(languageProperties);
	}









	/**
	 * Metoda, která si získá veškeré metody z označené třídy (/ instance) a všechny je projde. Pro každou metodu se
	 * zjistí, zda se mají načíst soukromé, chráněné, případně package-private metody, pokud ne a taková metoda se bude
	 * iterovat, tak se nepřidá. Jinak ano.
	 *
	 * @param classNameVariable
	 *         - název reference na instanci třídy, nad kterou bylo otevřeno toto kontextové menu a nad touto instancí
	 *         se budou volat příslušné metody.
	 * @param includePrivateMethods
	 *         - logická proměnná, která značí, zda se mají do kontextového menu přidat i soukromé metody.
	 * @param includeProtectedMethods
	 *         - logická proměnná, která značí, zda se mají do kontextového menu přidat i chráněné metody.
	 * @param includePackagePrivateMethods
	 *         - logická proměnná, která značí, zda se mají do kontextového menu přidat i package-private metody (tj.
	 *         metody s viditelností pouze pro třídy ve stejném balíčku).
	 * @return logická proměnná, která značí, zda se přidala alespoň jedna metoda nebo ne. True v případě, že se
	 * přidala
	 * alespoň jedna metoda, jinak false.
	 */
	private boolean addAllMethod(final String classNameVariable, final boolean includePrivateMethods, final boolean
			includeProtectedMethods, final boolean includePackagePrivateMethods) {
		// Vezmu si odkaz na Class z mapy:
		final Object myObject = operationWithInstances.getClassFromMap(classNameVariable);

		if (myObject == null)
			return false;

		boolean addedMethod = false;

		// Zde byl nalezen požadovaný objekt, mohu si získat veškeré metody:
		final Method[] methodsOfClass = myObject.getClass().getDeclaredMethods();

		for (final Method m : methodsOfClass) {
			/*
			 * Pokud je metoda privátní a nemají se načítat privátní metody, pak se tato
			 * aktuální metoda v příslušné iterace přeskočí.
			 */
			if (!modifiersCorrespond(m.getModifiers(), includePrivateMethods, includeProtectedMethods,
					includePackagePrivateMethods))
				continue;


			final JMenuItem itemMethod = getMenuItemForMethod(m);

			addListenerToMethodItem(itemMethod, m, myObject, classNameVariable);

			add(itemMethod);

			if (!addedMethod)
				addedMethod = true;
		}


		return addedMethod;
	}







	
	/**
	 * Metoda, která vytvoří instanci třídy JMenuItem, která bude sloužit jako
	 * položka, na kterou když uživatel klikne, zavolá se metoda, kterou příslušná
	 * položka reprezentuje.
	 * 
	 * @param m
	 *            - Metoda, kterou bude reprezentovat vytvořená položka (JMenuItem)
	 * 
	 * @return výše popsanou instanci třídy JMenuItem, která umožňuje zavolání
	 *         příslušné metody m.
	 */
	private static JMenuItem getMenuItemForMethod(final Method m) {
		// Návratový typ metody:
		String itemText = ParameterToText.getMethodReturnTypeInText(m, false) + " ";

		// Název metody:
		itemText += m.getName();

		final String parametersText = ReflectionHelper.getMethodNameAndParametersInText(itemText, m.getParameters()) + ";";

		return new JMenuItem(parametersText);
	}
	
	
	
	
	
	
	

	
	
	/**
	 * Metoda, která přidá do tohoto kontextového menu položky coby metody, které
	 * jsou zděděné z třídy Object a budou volány (po kliknutí na příslušnou
	 * položku) na příslušnou instancí, nad kterou se otevřelo toto kontextové menu.
	 * 
	 * @param classNameVariable
	 *            - reference (proměnná) na instanci nějaké třídy z diagramu tříd (v
	 *            diagramu instancí), nad kterou bylo otevřeno toho kontextové menu
	 *            a nad kterou se budou volat zděděné metody.
	 */
	private void addMethodsInheritedFromObject(final String classNameVariable) {
		/*
		 * Získám si všechny veřejné metody z třídy Object, aby bylo možné zavolat ty
		 * metody nad instancí příslušné třídy.
		 */
		final Method[] methods = Object.class.getMethods();
		
		// Nemělo by nastat:
		if (methods.length == 0)
			return;
		
		// Vezmu si odkaz na Class z mapy:
		final Object myObject = operationWithInstances.getClassFromMap(classNameVariable);
		
		if (myObject == null)
			return;
		
		/*
		 * Vytvořím si veškeré položky do JMenu menu, na kterou když se klikne, tak se
		 * zavolá metoda, kterou repezentuje a která byla zděděna z třídy Object.
		 */
		Arrays.stream(methods).forEach(m -> {
			final JMenuItem itemMethod = getMenuItemForMethod(m);
			
			addListenerToMethodItem(itemMethod, m, myObject, classNameVariable);
			
			menuInheritedMethodsFromObject.add(itemMethod);
		});
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá do tohoto kontextového menu, konkrétně do menu
	 * menuInheritedMethods přidá položky, které budou reprezentovat veškeré zděděné
	 * metody ze všech předků (až po třídu Object). (zděděné metody jsou veřejné nebo chráněné)
	 * 
	 * @param classNameVariable
	 *            - reference (proměnná) na instanci nějaké třídy z diagramu tříd (v
	 *            diagramu instancí), nad kterou bylo otevřeno toho kontextové menu
	 *            a nad kterou se budou volat zděděné metody.
	 */
	private void addInheritedMethods(final String classNameVariable) {
		// Vezmu si odkaz na Class z mapy:
		final Object myObject = operationWithInstances.getClassFromMap(classNameVariable);

		// (nemělo by nastat)
		if (myObject == null)
			return;


		/*
		 * Zavolám metodu, která proiteruje veškeré předky dané třídy až dokud se
		 * nenarazí na třídu Object a vloží do listu inheritedMethodsList všechny veřejné a
		 * chráněné metody ze všech předků.
		 */
		final List<MethodInfo> inheritedMethodsListTEST = getInheritedMethods(myObject.getClass().getSuperclass(),
				new ArrayList<>(), false, myObject.getClass().getSimpleName());


		/*
		 * Projdu získaný list s informacemi o zděděných metodách. Pro každou získanou třídu (předka) vytvořím
		 * rozevírací
		 * menu, které bude reprezentovat toho předka. Toto rozevícací menu bude obsahovat tooltip, který "ukazuje"
		 * postup dědičnosti
		 * od označené třídy v diagramu tříd. To menu dále bude obshovat jako jednotlivé položky statické veřejné a
		 * chráněné metody, které
		 * se v té třídě (reprezentující rozevírací menu - předka) nachází.
		 */
		inheritedMethodsListTEST.forEach(info -> {
			/*
			 * Vytvořím si rozevírací položku (menu), do kterého níže vložím příslušné metody.
			 */
			final JMenu menu = getMenuForClass(info.getClazz(), instanceDiagram.isShowInheritedClassNameWithPackages()
					, info.getTooltip());

			info.getMethods().forEach(m -> {
				/*
				 * Položka, která bude reprezentovat konkrétní (aktuálně ierovanou) metodu. Po kliknutí na toto
				 * tlačítko se metoda zkusí zavolat.
				 */
				final JMenuItem itemMethod = getMethodMenuItem(m);

				// Přidám událost na kliknutí na tu položku, aby se zavolala příslušná metoda:
				addListenerToMethodItem(itemMethod, m, myObject, classNameVariable);

				// Přidám položku do rozevíracího menu reprezentující metody z konréní třídy:
				menu.add(itemMethod);
			});

			menuInheritedMethods.add(menu);
		});
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá na danou položku v menu událost po kliknutí po kliknutí
	 * se zavolá metoda, kterou daná položka reprezentuje
	 * 
	 * @param item
	 *            - pložka v menu pro přidání události
	 * 
	 * @param method
	 *            - metoda, která se provede po kliknutí na danou položku
	 * 
	 * @param ownerMethod
	 *            - instance třídy, který byla označena v diagramu instancí a má se
	 *            nad ní zavolat označená metoda - method.
	 * 
	 * @param reference
	 *            - refernce na instanci.
	 */
	private void addListenerToMethodItem(final JMenuItem item, final Method method, final Object ownerMethod,
			final String reference) {
		item.addActionListener(e -> {
			if (method.getParameterCount() == 0) {
				// Metoda neobsahuje žádný parametr, tak ji mohu zavolat:
				final Called returnValue = operationWithInstances.callMethodWithoutParameters(method, ownerMethod,
						true);

				/*
				 * else - Zde se metodu nepodařilo zavolat, chyby byly již vypsány, tak bych
				 * mohl vypsat třeba neúspěch apod. Ale už nic dělat nebudu. Uživatel už o chybě
				 * ví.
				 */
				if (returnValue != null && returnValue.isWasCalled()) {
					// Zde byla metoda zavolána, tak zjistím, zda metoda má něco vracet:
					if (method.getReturnType().equals(Void.TYPE))
						// zde metoda nemá nic vracet:
						outputEditor.addResult(txtSuccess);
					else {
						// Zde metoda má něco vrátit, tak to vypíšu:
						printReturnedValueToEditor(txtSuccess + ": ",
								returnValue.getObjReturnValueWithTestedReference(), outputEditor);


						// Zda se mají zvýraznit příslušné / vrácené instance:
						instanceDiagram.checkHighlightOfInstances(returnValue.getObjReturnValue());


						// Otestuji, zda se vrátila instance, případně se zeptám, zda se má vytvořit
						// reprezentace v diagramu instancí.
						checkReturnedValue(returnValue.getObjReturnValue(), instanceDiagram,
								languageProperties);
					}
				}
				// else - Zde by se do editoru výstupů vypsali informace o nastalé chybě pri
				// zavolání metody: callMethodWithoutParameters
			}



			else {
				// Zde metoda obsahuje alespoň jeden parametr, tak ho musím načíst od uživatele:
				// Původní:
				final GetParametersMethodForm gpmf = new GetParametersMethodForm(method, languageProperties,
						outputEditor, makeVariablesAvailableForCallMethod,
						instanceDiagram.isShowCreateNewInstanceOptionInCallMethod(), instanceDiagram,
						ownerMethod.getClass(), reference, instanceDiagram.isMakeMethodsAvailable());

				final List<Object> parametersOfMethod = gpmf.getParameters(method.getName());

				if (parametersOfMethod != null && !parametersOfMethod.isEmpty()) {
					// Zjistím si návratovou hodnotu a případně ji vypíšu do editoru
					final Called returnValue = operationWithInstances.callMethodWithParameters(method, ownerMethod,
							true, parametersOfMethod.toArray());


					// Otestuji, zda se něco vrátilo a pokud ano, vypíši to do editoru výstupů
					if (returnValue != null && returnValue.isWasCalled()) {
						// Zde byla metoda zavolána, tak zjistím, zda metoda má něco vracet:
						if (method.getReturnType().equals(Void.TYPE))
							// zde metoda nemá nic vracet:
							outputEditor.addResult(txtSuccess);
						else {
							// Zde metoda má něco vrátit, tak to vypíšu:
							printReturnedValueToEditor(txtSuccess + ": ",
									returnValue.getObjReturnValueWithTestedReference(), outputEditor);


							// Zda se mají zvýraznit příslušné / vrácené instance:
							instanceDiagram.checkHighlightOfInstances(returnValue.getObjReturnValue());


							// Otestuji, zda se vrátila instance, případně se zeptám, zda se má vytvořit
							// reprezentace v diagramu instancí.
							checkReturnedValue(returnValue.getObjReturnValue(), instanceDiagram,
									languageProperties);
						}
					}
					/*
					 * else - Zde se metodu nepodařilo zavolat, chyby byly již vypsány, tak bych
					 * mohl vypsat třeba neúspěch apod. Ale už nic dělat nebudu. Uživatel už o chybě
					 * ví.
					 *
					 * else - Zde by se do editoru výstupů vypsali informace o nastalé chybě pri  zavolání metody:
					 * callMethodWithoutParameters
					 */
				}
			}
		});
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public void setLanguage(final Properties properties) {
		languageProperties = properties;		
		
		if (properties != null) {
			menuInheritedMethodsFromObject.setText(properties.getProperty("Id_Pm_MenuItemInheritedMethods", Constants.ID_PM_MENU_ITEM_INHERITED_METHODS));
			
			menuInheritedMethods.setText(properties.getProperty("Id_Pm_MenuItemInheritedMethodsFromAncestors", Constants.ID_PM_MENU_ITEM_INHERITED_METHODS_FROM_ANCESTORS));
			
			itemRemove.setText(properties.getProperty("Id_Pm_ItemRemove", Constants.ID_PM_ITEM_REMOVE));
			
			itemViewVariables.setText(properties.getProperty("Id_Pm_ItemVievVariables", Constants.ID_PM_ITEM_VIEW_VARIABLES));
			
			txtSuccess = properties.getProperty("Id_Pm_Txt_Success", Constants.ID_PM_TXT_SUCCESS);
		}
		
		
		else {
			menuInheritedMethodsFromObject.setText(Constants.ID_PM_MENU_ITEM_INHERITED_METHODS);
			
			menuInheritedMethods.setText(Constants.ID_PM_MENU_ITEM_INHERITED_METHODS_FROM_ANCESTORS);
			
			itemRemove.setText(Constants.ID_PM_ITEM_REMOVE);
			
			itemViewVariables.setText(Constants.ID_PM_ITEM_VIEW_VARIABLES);
			
			txtSuccess = Constants.ID_PM_TXT_SUCCESS;
		}

		menuInheritedMethodsFromObject.setText(menuInheritedMethodsFromObject.getText() + " Object");
	}
}