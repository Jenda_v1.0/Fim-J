package cz.uhk.fim.fimj.instance_diagram.overview_of_instance;

import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.List;
import java.util.stream.IntStream;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.commands_editor.ParseToJavaTypes;
import cz.uhk.fim.fimj.file.*;
import cz.uhk.fim.fimj.forms.ListCellRenderer;
import cz.uhk.fim.fimj.forms.SearchForm;
import cz.uhk.fim.fimj.forms.SelectInstancesForCreateReference;
import cz.uhk.fim.fimj.forms.SourceDialogForm;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;
import cz.uhk.fim.fimj.instances.Instances;
import cz.uhk.fim.fimj.instances.OperationsWithInstances;
import cz.uhk.fim.fimj.instances.OperationsWithInstancesInterface;
import cz.uhk.fim.fimj.output_editor.OutputEditor;
import cz.uhk.fim.fimj.reflection_support.ParameterToText;
import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;

/**
 * Tato třída slouží jako dialog pro prohlížení hodnot instance (konstruktory, metody a atributy).
 * <p>
 * Zobrazí se po kliknutí na položku "Prohlížet" v menu, které se zobrazí po kliknutí pravým tlačítkem myši na instancí
 * nějaké třídy v diagramu instancí.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class VariablesInstanceOfClassForm extends SourceDialogForm implements LanguageInterface {

    private static final long serialVersionUID = -7449832403639011242L;


    /**
     * "komponenta" pro volání metod získných pomocí reflexe.
     */
    public static OperationsWithInstancesInterface operationWithInstances;


    /**
     * Text, který se vkládá do labelu lblClassInfo.
     * <p>
     * Proměnná musí být globální / instanční protože je vhodné, aby se text zalamoval v případě, že je text "delší /
     * širší" než šířka dialogového okna. Proto je třeba tato proměnná, aby se při každé změně velikosti okna mohl
     * nastavit text do zmíněného labelu.
     */
    private String textForLblClassInfo;


    /**
     * Úvodní text se základními informacemi o prohlížené třídě:
     */
    private final JLabel lblClassInfo;

    // Proměnné pro doplnění textu s úvodními informace - výše
    private String txtClassName;
    private final String txtInstanceVariable;


    private final JButton btnClose;
    private JButton btnSearchConstructor;
    private JButton btnCallConstructor;
    private JButton btnSearchMethod;
    private JButton btnSearchVariable;
    private JButton btnCreateReference;
    private JButton btnCallMethod;


    // Kolekce pro zobrazení informací i instanci třídy uživateli:
    private static Set<ValuesInfo> constructorsList;
    private static Set<ValuesInfo> variablesList;
    private static Set<ValuesInfo> methodsList;


    // Panely, do kterých vložím List s nalezenými daty ze třídy a tlačítko pro vyhledání klíčových slov
    private JPanel pnlConstructors;
    private JPanel pnlMethods;
    private JPanel pnlVariables;


    // Jazyk pro předání do dialogu s vyhledávacího dialogu
    private Properties languageProperties;


    // Proměnné pro uchování textu pro doplnění do dialogu s vyhledáním nějaké hodnoty - řetězce, konkrétně co se v
    // daném dialogu vyhledává - jestli atribut, metoda, konstruktor
    private String constructor;
    private String method;
    private String variableTitle;
    private String txtFailedToCreateInstanceText;
    private String txtFailedToCreateInstanceTitle;


    /**
     * Prměnná, do kterého se bude vkládat text získané hodnoty z pole pro vložení do dialogu.
     *
     * <i>(Sestavení textu z pole.)</i>
     */
    private final StringBuilder txtTempForArray = new StringBuilder();


    // Jlisty, do kterých se vkládají jednotlivé položky pro zobrazení:
    private JList<ValuesInfo> constructorsJList;
    private JList<ValuesInfo> methodsJList;
    private JList<ValuesInfo> variablesJList;


    /**
     * Komponenta, která slouží pro zobrazení hodnot ze všech předků třídy obClass pedané v konstruktoru této třídy.
     */
    private final JCheckBox chcbIncludeInheritedValues;

    /**
     * Komponenta pro nastavení, zda se mají v dialogu zobrazit i hodnoty z vnitřních tříd. Tedy, zda se mají zobrazit
     * konstruktory, metody a proměnné, které se nachází ve vnitřních třídách zvolené instance.
     */
    private final JCheckBox chcbIncludeInnerClasses;


    /**
     * Komponenta, která slouží pro nastavení toho, zda se mají zalamovat texty / položky v komponentách JList nebo ne.
     * <p>
     * Pokud uživatel tento chcb označí, pak se položky / texty v komponentách JList pro a konstruktory, metody a
     * atributy budou zalamovat na více řádků, aby to pro uživatele bylo "přehlednější", aby nemusel pořád posovat
     * posuvník.
     */
    private final JCheckBox chcbWrapText;


    /**
     * Komponenta, která značí, zda se mají do okna s metodami zahrnout i metody, které jsou "převzaté" z nějakého
     * rozhraní.
     * <p>
     * Pokud bude tato komponenta označená, pak se v okně s metodami zobrazí i metody které jsou v příslušné třídě
     * implementovány ale jsou z nějakého rozhraní. Jinak, pokud tato komponenta nebude označena, pak se v okně s
     * metodami nebudou zobrazovat metody, která příslušná třída implementuje díky nějakému rozhraní.
     */
    private final JCheckBox chcbIncludeInterfaceMethods;


    /**
     * Proměnná pro uložení textu ve zvoleném jazyce.
     */
    private String txtSuccess;


    /**
     * Prměnná pro text 'Referenční proměnná' v uživatelem zvoleném jazyce.
     */
    private static String txtReferenceVarText;


    /**
     * Logická proměnná, která značí, zda se mají zobrazovat referenční proměnné.
     * <p>
     * Jde o to, aby uživatel veděl, o jakou se jedná instanc v nějaké proměnné, tak je zde doplněna možnost, která vždy
     * u každé instance v nějakých vybraných typech proměnných zobrazi i ten text reference,kterou uživatel zadal při
     * vytváření instance, ale to platí pouze v případě, že ta instance se nachází v diagramu instancí.
     */
    private final boolean showReferenceVariables;


    /**
     * Konstruktor této třídy.
     *
     * @param objClass
     *         - instance třídy, ze které se mají "vzít" data (metody, konstruktory a atributy / proměnné).
     * @param referenceName
     *         - reference na instanci, kterou zadal uživatel při jejím vytvořní. Resp. jedná se o referenck, která
     *         "ukazuje" na příslušnou instanci.
     * @param instanceDiagram
     *         - reference na diagram instancí pro předání do metody pro případně vytvoření reference.
     * @param outputEditor
     *         - reference na editor výstupů, kam se má vypsat hlášení ohledně zavolání metody.
     * @param showReferenceVariables
     *         - logická proměnná, která značí, zda se mají zobrazit referenční proměnné u hodnot v proměnných pole,
     *         list a "klasické" proměnné. Hodnota true značí, že budou zobrazeny ty reference u instancí v proměnných,
     *         poukd ta instance má svou reprezentaci v diagramu instnací, jinak false, když se nemají zobrazovat ty
     *         reference u instancí v proměnných.
     */
    public VariablesInstanceOfClassForm(final Object objClass, final String referenceName,
                                        final GraphInstance instanceDiagram, final OutputEditor outputEditor,
                                        final boolean showReferenceVariables) {

        txtInstanceVariable = referenceName;
        this.showReferenceVariables = showReferenceVariables;


        /*
         * Zde je potřeba si načíst objekt s jazyky již "dopředu" kvůli vytvoření
         * instance třídy pro operace s metodami akonstruktory.
         */
        final Properties languageProp = App.READ_FILE.getSelectLanguage();

        // Aby uživatel mohl volat metody a konstruktory:
        operationWithInstances = new OperationsWithInstances(outputEditor, languageProp, instanceDiagram);

        // Zde si potřebuji naplnit proměnnou, jinak by na poprvé byla null:
        fillTxtReferenceVar(languageProp);


        initGui(DISPOSE_ON_CLOSE, new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS),
                "/icons/app/VariableInstanceOfClassFormIcon.png", new Dimension(600, 800), true);

        /*
         * Nastavení minimální velikosti okna dialogu, pod kterou již okno dialogu
         * nepůjde zmenšit.
         */
        setMinimumSize(new Dimension(310, 610));


        // Kvůli zahrnutí metod z rozhraní je tuto komponentu třeba inicializovat již zde:
        chcbIncludeInheritedValues = new JCheckBox();

        // Je třeba instancovat zde kvůli následující metodě pro nalezené metod
        chcbIncludeInnerClasses = new JCheckBox();


        // Tuto komponentu je třeba inicializovat již zde, kvůli metodě pro chcb pro zahrnutí hodnot z předků:
        chcbIncludeInterfaceMethods = new JCheckBox();

        // Ve výchozím stavu bude označený:
        chcbIncludeInterfaceMethods.setSelected(true);

        // Zarovnání komponenty na střed - horizontálně:
        chcbIncludeInterfaceMethods.setAlignmentX(CENTER_ALIGNMENT);

        chcbIncludeInterfaceMethods.addActionListener(event -> {
            // Vymažu aktuálně naplněné metody:
            methodsList.clear();

            // Naplním si s potenciálně novými nebo naopak chybějícími metodami z rozhraní:
            findAllMethods(objClass.getClass(), objClass, referenceName, chcbIncludeInheritedValues.isSelected(),
                    chcbIncludeInterfaceMethods.isSelected(), chcbIncludeInnerClasses.isSelected());

            // Aktualizuji list v okně, aby uživatel viděl změny:
            setModelToJList(methodsJList, methodsList);
        });


        lblClassInfo = new JLabel();
        addComponent(lblClassInfo);
        /*
         * Posluchač reagující na změnu velikosti okna, aby se změnil přizpůsobilo zalomení textu s informacemi
         * velikosti okna.
         */
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                lblClassInfo.setText(getWrappedText(textForLblClassInfo, getWidth()));
            }
        });







        /*
         * Do dialogu se přidají komponenty, dle které se mohou případně i načíst hodnoty z předků třídy objClass
         * nebo vnitřních tříd:
         */
        chcbIncludeInheritedValues.addActionListener(getActionListenerForRefreshValues(objClass, referenceName));

        chcbIncludeInnerClasses.addActionListener(getActionListenerForRefreshValues(objClass, referenceName));

        /*
         * Panel, který bude obsahovat komponenty pro nastavení, zda se v dialogu mají zobrazit hodnoty z předků a
         * vnitřních tříd.
         *
         * Jedná se o úsporu prostoru okna.
         */
        final JPanel pnlChcbIncludeValues = new JPanel(new FlowLayout(FlowLayout.CENTER, 20, 0));
        pnlChcbIncludeValues.add(chcbIncludeInheritedValues);
        pnlChcbIncludeValues.add(chcbIncludeInnerClasses);
        addComponent(pnlChcbIncludeValues);










        /*
         * Vytvořím a přidám si do okna dialogu komponentu pro nastavení zalamování
         * dlouhých textů v JListech.
         */
        chcbWrapText = new JCheckBox();
        chcbWrapText.addActionListener(event -> setWrapTextToJLists(chcbWrapText.isSelected()));
        addComponent(chcbWrapText);


        if (objClass != null) {
            final Class<?> clazz = objClass.getClass();

            // Do titulku doplním pouze název třídy:
            setTitle(clazz.getSimpleName());

            // Do proměnné doplním celý název třídy - i s balíčky:
            txtClassName = clazz.getName();


            // doplnění konstukroů do dialogu:
            constructorsList = new HashSet<>();

            // List konstuktorů do dialogu:
            pnlConstructors = new JPanel();

            // Načtu si do listu constructorsList veškeré konstruktory, pouze z třídy
            // objClass (pro výchozí nastavení).
            findAllConstructors(clazz, false, chcbIncludeInnerClasses.isSelected());

            // Nastavím získaná listu do Jlistu pro zobrazení v dialogu:
            constructorsJList = new JList<>();
            /*
             * Pro nastavení hodnot do Listu výše se volá tato  metoda, aby se jednotlivé položky / hodnoty v listu
             * seřadily.
             */
            setModelToJList(constructorsJList, constructorsList);
            constructorsJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            /*
             * Přidám listener, který při každém ovlivnění rozměrů té komponenty - Jlistu s
             * konstruktory znovu zavolá metodu pro přepočet zalamování textů.
             */
            addComponentListenerForJList(constructorsJList);
            /*
             * Posluchač na uvolnění pravého tlačítka myši. V takovém případě se otevře kontextové menu, které bude
             * obsahovat možnosti pro zkopírování reference na konstruktor do schárnky.
             */
            addMouseAdapterForJList(constructorsJList, languageProp);


            btnSearchConstructor = new JButton();
            btnSearchConstructor.addActionListener(event -> createSearchDialog(constructorsList, constructor));

            btnCallConstructor = new JButton();
            btnCallConstructor.addActionListener(event -> {
                /*
                 * Získám si označenou položku ("konstruktor") v příslušném listu v dialogu:
                 */
                final Object objSelectedConstructor = constructorsJList.getSelectedValue();

                /*
                 * Otestuji, zda to není null a je to instance ConstructorInfo, toto by mělo
                 * nastat vždy, ale kdybych to v budoucnu já nebo nědko předělával a někde něco
                 * opoměl apod.
                 */
                if (objSelectedConstructor instanceof ConstructorInfo) {
                    /*
                     * Získám si přímo položku z listu, abych zpřístupnili zavolání metody.
                     */
                    final ConstructorInfo constructorInfo = (ConstructorInfo) objSelectedConstructor;

                    // Zavolám získanou metodu.
                    constructorInfo.callConstructor(languageProperties, outputEditor, txtFailedToCreateInstanceText,
                            txtFailedToCreateInstanceTitle, instanceDiagram);

                    /*
                     * Následující 3 řádky jsou potřeba kvůli aktualizaci hodnot v proměnných
                     * příslušné instance, protože když se zavolá metoda, tak se mohly změnit nějaké
                     * hodnoty některých proměnných, tak je třeba přenačíst veškeré hodnoty
                     * proměnných,
                     */
                    variablesList.clear();
                    findAllVariables(objClass.getClass(), objClass,
                            chcbIncludeInheritedValues.isSelected(), chcbIncludeInnerClasses.isSelected());
                    setModelToJList(variablesJList, variablesList);
                }
            });

            addListWithButtons(constructorsJList, btnSearchConstructor, btnCallConstructor, pnlConstructors);


            //Metody:
            // Intanciace listu, kam se budou vkládat metody v příslušné syntaxi:
            methodsList = new HashSet<>();

            // Najdu si veškeré metody - pouze z třídy objClass (pro výchozí nastavení)
            findAllMethods(clazz, objClass, referenceName, false, chcbIncludeInterfaceMethods.isSelected(),
                    chcbIncludeInnerClasses.isSelected());


            pnlMethods = new JPanel();

            // Přidání komponenty pro zahrnutí metod z rozhraní:
            pnlMethods.add(chcbIncludeInterfaceMethods);


            methodsJList = new JList<>();
            /*
             * Pro nastavení hodnot do Listu výše se volá tato  metoda, aby se jednotlivé položky / hodnoty v listu
             * seřadily.
             */
            setModelToJList(methodsJList, methodsList);
            methodsJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            /*
             * Přidám listener, který při každém ovlivnění rozměrů té komponenty - Jlistu s
             * metodami znovu zavolá metodu pro přepočet zalamování textů.
             */
            addComponentListenerForJList(methodsJList);
            /*
             * Přidání posluchače na otevření kontextového menu pro zkopírování reference do schránky.
             */
            addMouseAdapterForJList(methodsJList, languageProp);


            btnSearchMethod = new JButton();
            btnSearchMethod.addActionListener(event -> createSearchDialog(methodsList, method));

            btnCallMethod = new JButton();
            btnCallMethod.addActionListener(event -> {
                /*
                 * Získám si označenou položku ("metodu") v příslušném listu v dialogu:
                 */
                final Object objSelectedMethod = methodsJList.getSelectedValue();

                /*
                 * Otestuji, zda to není null a je to instance MethoInfo, toto by mělo nastat
                 * vždy, ale kdybych to v budoucnu já nebo nědko předělával a někde něco opoměl
                 * apod.
                 */
                if (!(objSelectedMethod instanceof MethodInfo))
                    return;

                /*
                 * Získám si přímo položku z listu, abych zpřístupnili zavolání metody.
                 */
                final MethodInfo methodInfo = (MethodInfo) objSelectedMethod;

                // Zavolám získanou metodu.
                methodInfo.callMethod(instanceDiagram, outputEditor, languageProperties, txtSuccess);

                /*
                 * Následující 3 řádky jsou potřeba kvůli aktualizaci hodnot v proměnných
                 * příslušné instance, protože když se zavolá metoda, tak se mohly změnit jějaké
                 * hodnoty některých proměnných, tak je třeba přenačíst veškeré hodnoty
                 * proměnných,
                 */
                variablesList.clear();
                findAllVariables(objClass.getClass(), objClass,
                        chcbIncludeInheritedValues.isSelected(), chcbIncludeInnerClasses.isSelected());
                setModelToJList(variablesJList, variablesList);
            });

            addListWithButtons(methodsJList, btnSearchMethod, btnCallMethod, pnlMethods);


            // Proměnné - udělám to jako skládání textu
            // Zjistím si modifikátor, pak typ a pak hodnotu
            // tyto vlastnosti poskládám do jednoho Stringu a ten zobrazím v listu v dialogu

            // List, kam se budou vkládat získané proměnné v příslušné syntaxi:
            variablesList = new HashSet<>();

            // Najdu se veškeré aributy v dané třídě:
            findAllVariables(clazz, objClass, false, chcbIncludeInnerClasses.isSelected());


            btnCreateReference = new JButton();
            btnCreateReference.setEnabled(false);


            pnlVariables = new JPanel();

            // Tvorba listu s výpisem proměnných:
            variablesJList = new JList<>();
            /*
             * Pro nastavení hodnot do Listu výše se volá tato  metoda, aby se jednotlivé položky / hodnoty v listu
             * seřadily.
             */
            setModelToJList(variablesJList, variablesList);
            variablesJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            /*
             * Přidám listener, který při každém ovlivnění rozměrů té komponenty - Jlistu s
             * proměnnými znovu zavolá metodu pro přepočet zalamování textů.
             */
            addComponentListenerForJList(variablesJList);
            /*
             * Přidání posluchače na otevření kontextového menu pro zkopírování reference do schránky.
             */
            addMouseAdapterForJList(variablesJList, languageProp);






            /*
             * Posluchač na list s promennými z příslušné třídy, popřípadě z předků, který
             * slouží na to, že při každém označení položky se otestuji, zda se jedná o
             * proměnnou, která obsahuje nějakou instanci třídy, která se nachází v diagramu
             * tříd, a ta instance ještě není reprezentována v diagramu instancí. Pokud jsou
             * tyto podmínky splněny, pak se zpřístupní tlačítko pro vytvoření nové
             * reference na příslušnou instanci, jinak se to tlačítko znepřístupní.
             */
            variablesJList.addListSelectionListener(event -> {
                final Object objSelectedValue = variablesJList.getSelectedValue();

                if (objSelectedValue instanceof VariableInfo) {
                    if (((VariableInfo) objSelectedValue).isPossibleToCreateReferenceToInstance())
                        btnCreateReference.setEnabled(true);
                    else
                        btnCreateReference.setEnabled(false);
                }
            });


            btnSearchVariable = new JButton();
            btnSearchVariable.addActionListener(event -> createSearchDialog(variablesList, variableTitle));



            /*
             * Přidám událost na kliknutí na tlačítko pro vytvoření reference.
             */
            btnCreateReference.addActionListener(event -> {
                /*
                 * Získám si označený objekt z listu v okně.
                 */
                final Object objSelectedValue = variablesJList.getSelectedValue();

                /*
                 * Otestuji, zda není null (nemělo by nastat) a je to instance příslušné třídy s
                 * proměnnými - mělo by být vždy v tomto případě
                 */
                if (!(objSelectedValue instanceof VariableInfo))
                    return;

                /*
                 * Přetypuji si získanou hodnotu na příslušnou instanci příslušné třídy.
                 */
                final VariableInfo varInfo = (VariableInfo) objSelectedValue;

                /*
                 * Jestli nebyla nalezena žádná instance třídy z diagramu tříd, která se ještě
                 * nenachází v diagramu instancí, tak mohu skončit, nemá smysl pokračovat.
                 */
                if (!varInfo.isPossibleToCreateReferenceToInstance())
                    return;


                /*
                 * Zde vím, že se našla alespoň jedna instance, na kterou je možné vytvořit
                 * referenci, tak zjistím, zda se jedná pouze o jednu instanci, pokud ano, tak
                 * rovnou nabídnu vytvoření reference, ale pokud je jich více, tak níže zobrazím
                 * dialog s příslušnými instancemi, aby se uživatel sám vybral, na které
                 * instance chce vytvořit reference.
                 */
                if (varInfo.getInstancesList().size() == 1)
                    /*
                     * Zavolám metodu pro vytvoření reference na příslušnou instanci, jelikož se
                     * jedná jen o jednu instanci, na kterou semůže vytvořit eference, nachází se v
                     * příslušném listu na prvním místě, tedy na indexu 0 (nula).
                     */
                    VariableInfo.createNewReferenceForInstance(varInfo.getInstancesList().get(0), instanceDiagram,
                            languageProp);


                    /*
                     * Zde by stačilo pouze else, už vím, že je možné vytvořit reference a byla
                     * nalezena více než jedna reference, tak vytvořím dialog a nechám uživatele
                     * vybrat všechny instance, na které je možné vytvořit reference.
                     *
                     * Toto je z mého pohledu lepší, než kdyby se vrátilo například 10 instancí a na
                     * uživatele vyskočilo pak 10 dialogů s dotazem, zda se na příslušnou instancí
                     * má vytvořit reference nebo ne, takto s tím bude počítat, ostatně si to sám
                     * zvolí.
                     */
                else if (varInfo.getInstancesList().size() > 1) {
                    /*
                     * Vytvořím si dialog a předám mu dostupné instance, na které je možné vytvořit
                     * referenci.
                     */
                    final SelectInstancesForCreateReference sifcr = new SelectInstancesForCreateReference(
                            varInfo.getInstancesList());
                    // Nastavím texty:
                    sifcr.setLanguage(languageProp);


                    /*
                     * Zde jej zobrazím a nechám si vrátit list označených instancí, na které semá
                     * vytvořit reference.
                     */
                    final List<Object> instancesList = sifcr.getInstances();

                    /*
                     * Pokud uživatel označil alespon jednu instanci, tak projdu celý list s
                     * označenými instancemi a pro každou nabídnu uživatel vytvoření nové reference.
                     */
                    if (!instancesList.isEmpty())
                        instancesList.forEach(
                                i -> VariableInfo.createNewReferenceForInstance(i, instanceDiagram, languageProp));
                }

                /*
                 * Nyní je potřeba přenačít celý list, protože se výše mohla vytvořit nová
                 * reference, tak aby se ta reference projevila i v dialogu, musím přenačíst
                 * všchny proměnné.
                 */

                /*
                 * Získám si aktuálně označenou položku, abych ji mohl znovu označit až se
                 * přenačtou hodnoty:
                 */
                final int selectedItem = variablesJList.getSelectedIndex();

                // Nejprve celý list vymažu:
                variablesList.clear();

                // Zavolám metody, které načtou do vymazaných / vyprázdněných listů výše nová
                // data:
                findAllVariables(objClass.getClass(), objClass, chcbIncludeInheritedValues.isSelected(),
                        chcbIncludeInnerClasses.isSelected());

                // Nastavím nově načtená data do Jlistu do dialogu, aby se projevily v okně
                // uživateli:
                setModelToJList(variablesJList, variablesList);

                // Znovu označím tu původní označenou položku, protože výše se mohla odznačit
                // kvůli přenačtení referení.
                variablesJList.setSelectedIndex(selectedItem);

                /*
                 * Znovu otestuji, zda uživatel vytvořil referenci nebo ne, pokud ano, pak musím
                 * znepřístupnit tlačítko pro opětovné vytvoření reference, na tu samou intanci,
                 * když už existuje.
                 */
                varInfo.checkCreationOfReference();

                if (!varInfo.isPossibleToCreateReferenceToInstance())
                    btnCreateReference.setEnabled(false);
            });

            //Přidání do okna:
            addListWithButtons(variablesJList, btnSearchVariable, btnCreateReference, pnlVariables);


            /*
             * Panel, do kterého se vloží panely s konstruktory, metodami a proměnnými.
             *
             * <i>Jedná se pouz o účel zpřehlednění hodnot v okně, aby byly jednotlivé Listy s hodnotami "vyšší" a
             * zobrazovali více hodnot, uživatel tak má větší přehled o hodnotách, navíc díky skrolování může vidět
             * veškeré hodnot dle potřeby a učitelé jej mohou na plátně také zobrazit.</i>
             */
            final JPanel pnlValuesFromInstance = new JPanel(new BorderLayout());
            pnlValuesFromInstance.add(pnlConstructors, BorderLayout.NORTH);
            pnlValuesFromInstance.add(pnlMethods, BorderLayout.CENTER);
            pnlValuesFromInstance.add(pnlVariables, BorderLayout.SOUTH);

            addComponent(new JScrollPane(pnlValuesFromInstance));
        }


        btnClose = new JButton();
        btnClose.addActionListener(event -> dispose());
        addComponent(btnClose);


        add(Box.createRigidArea(new Dimension(0, 20)));


        pack();
        setLocationRelativeTo(null);
    }


    /**
     * Metoda, která nastaví Jlistům pro zobrazení konstruktorů, metod a atributů zalamování textů na více řádků.
     * <p>
     * Metoda dle hodnota wrap nastaví výše zmíněným listům buď, že se mají dlouhé texty, resp. šíroké, že se nevejdou
     * na šířku dialogu tak se budou zalamovat. Nebo, když bude hodnota ve wrap false, tak se nebudou texty zalamovat,
     * uživatel tedy bude muset posouvat posuvníkem.
     *
     * @param wrap
     *         true, pokud se mají texty v JListech pro konstruktory, metody a atributy zalamovat, jinak false.
     */
    private void setWrapTextToJLists(final boolean wrap) {
        setCellRendererToJList(constructorsJList, wrap);
        setCellRendererToJList(methodsJList, wrap);
        setCellRendererToJList(variablesJList, wrap);
    }


    /**
     * Metoda, která nastaví komponentě list (typu JList) příslušný renderer pro to, aby se buď zalamovaly nebo
     * nezalamovaly šíroké řádky / texy - položky v řádcích.
     * <p>
     * Jde o to, že když je text příliš dlouhý, resp.šírkoky pro zobrazení v dialogu, tak má uživatel možnost jej
     * zalomit, aby ten text byl zobrazen na více řádků a uživatel tak nemusel posouvat posuvník.
     *
     * @param list
     *         - komponeta JList, které se má nastavit renderer pro to, aby se buď zalamovaly nebo nezalamovaly řádky v
     *         příslušném JListu.
     * @param wrap
     *         - logická hodnota, dle které se značí, zda se budou nebo nebudou zalamovat řádky v komponentě JList.
     *         Hodnota true značí, že se budou zalamovat řádky a hodnota false, že se nebudou zalamovat řádky v Jlistu
     *         list.
     */
    private static void setCellRendererToJList(final JList<ValuesInfo> list, final boolean wrap) {
        if (wrap)
            list.setCellRenderer(new ListCellRenderer(((list.getWidth() / 4) * 3) - 5));

        else
            list.setCellRenderer(new DefaultListCellRenderer());
    }


    /**
     * Vytvoření posluchače pro komponentu JList. Po kliknutí pravým tlačítkem myši na položku v uvedené komponentě se
     * zobrazí kontextové menu, které bude obsahovat možnosti pro zkopírování reference do schránky.
     *
     * @param jList
     *         - komponenta, ke které se má přidat výše popsaný posluchač pro otevření kontextového menu.
     * @param languageProp
     *         - objekt s texty pro aplikaci.
     */
    private static void addMouseAdapterForJList(final JList<ValuesInfo> jList, final Properties languageProp) {
        jList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.getButton() != MouseEvent.BUTTON3)
                    return;

                // Získání označené položky v JListu:
                final ValuesInfo valuesInfo = jList.getSelectedValue();

                // Zda je označena položka v listu:
                if (valuesInfo == null)
                    return;

                // Získání reference z příslušné položky:
                final String referenceToClipboard = valuesInfo.getReferenceTextToClipboard();

                // Vytvoření kontextového menu, aby si uživatel mohl vložit do schránky vytvořenou referenci:
                final PopupMenu popupMenu = new PopupMenu(referenceToClipboard);
                popupMenu.setLanguage(languageProp);
                popupMenu.show(jList, e.getX(), e.getY());
            }
        });
    }


    /**
     * Vrácí posluchač na kliknutí. Po kliknutí se aktualizují veškeré konstrktory, metody a atributy zobrazené v
     * dialogu.
     *
     * @param objClass
     *         - instance, jejíž hodnoty se zobrazují v dialogu.
     * @param referenceName
     *         - reference na instanci, jejiž hodnoty se zobrahují v dialogu
     *
     * @return výše zmíněný posluchač na aktualizaci hodnot zobrazených v dialogu.
     */
    private ActionListener getActionListenerForRefreshValues(final Object objClass, final String referenceName) {
        return e -> {
            /*
             * Pokud je načtená třída null, není co načítat -> nemělo by nastat.
             */
            if (objClass == null)
                return;

            /*
             * Nejprve vymažu veškeré dosud načtené hodnoty (konstruktory, metody a
             * atributy) a načtu je znovu, případně i další hodnotyz předků.
             */
            constructorsList.clear();
            methodsList.clear();
            variablesList.clear();

            // Zavolám metody, které načtou do vymazaných / vyprázdněných listů výše nová
            // data:
            findAllConstructors(objClass.getClass(), chcbIncludeInheritedValues.isSelected(),
                    chcbIncludeInnerClasses.isSelected());
            findAllMethods(objClass.getClass(), objClass, referenceName, chcbIncludeInheritedValues.isSelected(),
                    chcbIncludeInterfaceMethods.isSelected(), chcbIncludeInnerClasses.isSelected());
            findAllVariables(objClass.getClass(), objClass, chcbIncludeInheritedValues.isSelected(),
                    chcbIncludeInnerClasses.isSelected());

            // Nastavím nově načtená data do Jlistu do dialogu, aby se projevily v okně
            // uživateli:
            setModelToJList(constructorsJList, constructorsList);
            setModelToJList(methodsJList, methodsList);
            setModelToJList(variablesJList, variablesList);
        };
    }


    /**
     * Přidání posluchaře pro komponentu jList. Jedná se o posluchač reagující na změnu rozměrné to komponenty jList.
     * Jde o to, že když se změní její rozměry, například uživatel změní velikost okna dialogu, tak se zjistí, zda se
     * mají zalamovat texty v tom listu nebo ne a pokud ano, tak se přepočítají rozměry pro zalamování textů.
     *
     * @param jList
     *         - Komponenta, ke které se má přidat výše popsaný posluchač reagující na změnu velikosti jListu.
     */
    private void addComponentListenerForJList(final JList<ValuesInfo> jList) {
        jList.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                setCellRendererToJList(jList, chcbWrapText.isSelected());
            }
        });
    }


    /**
     * Metoda, která vloží hodnoty v listu data do komponeny Jlist list tak, aby se projevily změny v dialogu. Jedná se
     * o aktualizaci nově načtených hodnot z třídy, například i proměné z předků apod.
     *
     * @param list
     *         - list, kam se mají vložít data z listu data.
     * @param data
     *         - list, který obsahuje nová data (nové položky), které se mají vložit do listu list.
     */
    private static void setModelToJList(final JList<ValuesInfo> list, final Set<ValuesInfo> data) {
        // Pokud se jedná o prázdnou kolekci, tj. nebyly nalezeny žádné položky, tak se naplní modl s hodnotami, resp
        // . bude prázdný a vloží se do JListu zobrazný v dialogu.
        if (data.isEmpty()) {
            final DefaultListModel<ValuesInfo> tempModel = new DefaultListModel<>();

            data.forEach(tempModel::addElement);
            list.setModel(tempModel);

            return;
        }

        // Vložení seřazených hodnot do listu:
        list.setModel(getSortedListModel(data));
    }


    /**
     * Vytvoření modelu pro JList se seřazenými položkami v dialogu. Metoda vytvoři model, který bude obsahovat seřazené
     * položky v kolekci data.
     *
     * @param data
     *         - list, který obsahuje nová data (nové položky), které se mají seřadit a vložit do vráceného modelu.
     *
     * @return model pro JList, který bude obsahovat seřazené položky v kolekci data.
     */
    private static DefaultListModel<ValuesInfo> getSortedListModel(final Set<ValuesInfo> data) {
        final DefaultListModel<ValuesInfo> tempModel = new DefaultListModel<>();

        /*
         * Podmínky jsou pro to, aby se vědělo, jaké hodnoty se mají seřadit - zda se jedná o metody, konstruktory
         * nebo atributy.
         *
         * Je to potřeba pouze proto, že nefungoval postup pro seřazení kolekce data:
         *
         * Collections.sort(data);
         *
         * nebo
         *
         * final List<ValuesInfo> te = new ArrayList<>(data);
         *
         * Collections.sort(te);
         *
         * te.sort(Collections.reverseOrder());
         */

        // Seřazení konstruktorů:
        if (new ArrayList<>(data).get(0) instanceof ConstructorInfo) {
            final List<ConstructorInfo> constructorInfoList = new ArrayList<>();
            data.forEach(v -> constructorInfoList.add((ConstructorInfo) v));

            //noinspection unchecked
            Collections.sort(constructorInfoList);

            constructorInfoList.forEach(tempModel::addElement);
        }

        // Seřazení metod:
        else if (new ArrayList<>(data).get(0) instanceof MethodInfo) {
            final List<MethodInfo> methodInfoList = new ArrayList<>();
            data.forEach(v -> methodInfoList.add((MethodInfo) v));

            //noinspection unchecked
            Collections.sort(methodInfoList);

            methodInfoList.forEach(tempModel::addElement);
        }

        // Seřazení proměnných:
        else if (new ArrayList<>(data).get(0) instanceof VariableInfo) {
            final List<VariableInfo> variableInfoList = new ArrayList<>();
            data.forEach(v -> variableInfoList.add((VariableInfo) v));

            //noinspection unchecked
            Collections.sort(variableInfoList);

            variableInfoList.forEach(tempModel::addElement);
        }

        // Výchozí - nemělo by nastat:
        else data.forEach(tempModel::addElement);

        return tempModel;
    }


    /**
     * Metoda, která převede veškeré metody získané z třídy clazz (popřípadě i její předků a vnitřních tříd) na text v
     * požadované syntaxi a veškeré metody ve vytvořené syntaxi vloží do listu methodsList.
     *
     * @param clazz
     *         - třída, ze které se mají vzít veškeré její metody, popřípadě i z jejich předků.
     * @param objInstance
     *         - intance třídy, jejíž hodnoty jsou zobrazeny v "tomto" dialogu. Tato hodnota je zde potřeba pro předání
     *         do další proměnné do listu, aby bylo možné v případě potřeby zavolat příslušnou metodu.
     * @param reference
     *         - reference na instanci objInstance.
     * @param includeAncestors
     *         - logická proměnná o tom, zda se mají načíst i metody z předků třídy clazz nebo ne.
     * @param showMethodsFromInterfaces
     *         - logická prměnná, která značí, zda se mají zahrnout i metody z rozhraní nebo ne. Tzn. Pokud třída clazz
     *         implementuje nějaké rozhraní, tak je dost možné, že obsahuje i nějaké metody, která tedy musí třída clazz
     *         implementovat a uživatel má na výběr, zda se mají zobrazit i ty metody z rozhraní nebo ne.
     * @param includeInnerClasses
     *         - zda se mají zobrazit / načíst i vnitřní třídy.
     */
    private static void findAllMethods(final Class<?> clazz, final Object objInstance, final String reference,
                                       final boolean includeAncestors, final boolean showMethodsFromInterfaces,
                                       final boolean includeInnerClasses) {
        /*
         * True v případě, že se mají načíst i soukromé metody, jinak false.
         *
         * Proměnná je zde pouze kvůli rekurzi, protože v případě, že se mají načíst i hodnoty z předků třídy clazz
         * nebo nížev z vnitřní třídy, tak jako první bude hodnoty true, ale pak, když se pokračuje rekurzí ve
         * vyhledávání metod, tak bude proměnná false, protože se z předků budou brát pouze večejné nebo chráněné
         * metody, ne soukromé.
         */
        final boolean includePrivateMethods = true;

        // Získání metod z třídy clazz:
        findAllMethodsFromClass(clazz, objInstance, reference, includeAncestors, includePrivateMethods,
                showMethodsFromInterfaces, includeInnerClasses);


        // Vyhledání metod ve vnitřních třídách:
        if (includeInnerClasses) {
            // Získání veškerých vnitřních tříd:
            final List<Class<?>> allInnerClasses = ReflectionHelper.getAllInnerClasses(clazz.getDeclaredClasses(),
                    new ArrayList<>());

            allInnerClasses.forEach(c -> findAllMethodsFromClass(c, objInstance, reference, includeAncestors,
                    includePrivateMethods, showMethodsFromInterfaces, true));
        }
    }


    /**
     * Získání veškerých metod z třídy clazz. V případě procházení předků třídy clazz budou získány pouze veřejné a
     * chráněné metody.
     *
     * @param clazz
     *         - třída, ze které se mají získat metody.
     * @param objInstance
     *         - intance třídy, jejíž hodnoty jsou zobrazeny v "tomto" dialogu. Tato hodnota je zde potřeba pro předání
     *         do další proměnné do listu, aby bylo možné v případě potřeby zavolat příslušnou metodu.
     * @param reference
     *         - reference na instanci objInstance
     * @param includeAncestors
     *         - zda se mají načíst i metody z předků třídy clazz nebo ne.
     * @param includePrivateMethods
     *         - logická proměnná o tom, zda se mají načíst i privátní položky, protože pokud se mají získat i položky z
     *         předků, tak z předků se dle pravidel jazyka Java mohou brát pouze veřejné a chráněné proměnné, proto
     *         pokud se tato metoda volá rekurzí, pak tato proměnná bude false, jinak, pokud se volá poprvé s příslušnou
     *         třídou, pak bude true, protože z té třídy, ze které se mají vzít hodnoty, té kterou uživatel označil v
     *         diagramu instancí, tak z té se mohou vzít privátní metody.
     * @param showMethodsFromInterfaces
     *         - logická prměnná, která značí, zda se mají zahrnout i metody z rozhraní nebo ne. Tzn. Pokud třída clazz
     *         implementuje nějaké rozhraní, tak je dost možné, že obsahuje i nějaké metody, která tedy musí třída clazz
     *         implementovat a uživatel má na výběr, zda se mají zobrazit i ty metody z rozhraní nebo ne.
     * @param includeInnerClasses
     *         - true v případě, že se mají zobrazovat i vnitřní třídy.
     */
    private static void findAllMethodsFromClass(final Class<?> clazz, final Object objInstance, final String reference,
                                                final boolean includeAncestors, final boolean includePrivateMethods,
                                                final boolean showMethodsFromInterfaces,
                                                final boolean includeInnerClasses) {
        /*
         * Tato podmínka by něměla být splněna, ale kdyby náhodou, tak abych předešel
         * potenciálním potížím s výjimkou NullPointer..
         */
        if (clazz == null || clazz.getDeclaredMethods() == null)
            return;


        // Projdou se veškeré nalezené metody a převedou se do požadované syntaxe:
        for (final Method m : clazz.getDeclaredMethods()) {
            /*
             * Pokud se nemají zahrnou tmetody z rozhraní a jedná se o metodu
             * implementovanou z nějakého rozhraní, pak mohu pokračovat další iterací.
             */
            if (!showMethodsFromInterfaces && isMethodFromInterface(clazz, m))
                continue;


            /*
             * Pokud se nemají načíst privátní metody a aktuálně iterovaná metoda je
             * privátní, pak pokračuji další iterací - další metodou.
             */
            if (!includePrivateMethods && Modifier.isPrivate(m.getModifiers()))
                continue;


            // Zde je moetoda ve formátu: viditelnost (static) returnType methodName(parameters)
            // Tak si vezmu první 2 - 3 slova  - po název metody
            final String methodText = m.toString();

            /*
             * Note:
             * Místo získání následující textu metody bych ještě mohl zjišťovat, zda je
             * metoda staická, final, public,... a dle toho sestavovat text a u návratového
             * typu hodnoty bych mohl zobrazit jen samotná typ pomocí getSimpleName ale
             * návratová hodnota může být například nějaký objekt / třída v nějakém balíčku,
             * ale může existovat více tříd se stejným názvem akorát v různých balíččcích,
             * proto nechám ten návratový typ i s balíčky.
             */

            // Text metody po první otevírací závorku, tak se rozdělí dle mezer a vezmu si první 2 - 3 slova
            final String firstPartOfMethod = methodText.substring(0, methodText.indexOf('('));

            final String[] partsOfMethod = firstPartOfMethod.split("\\s+");

            /*
             * Proměnná, do které vložím "text" metody, může být například v následující
             * syntaxi:
             *
             * returnType name(...);
             * public returnType name(...);
             * public static returnType name(...);
             * public final returnType name(...);
             * public static final returnType name(...);
             *
             * Následující cyklus, který vloží do této proměnné veškerý text metody až po
             * název a pak se přidají parametry.
             *
             *
             *
             * Note:
             * Pokud se budou procházet i předkové třídy clazz, pak na začátek přidám i
             * název třídy, kde se příslušná metoda nachází, aby je uživatel mohl rozlišit -
             * v jaké třídě se jaká metoda nachází.
             *
             * Například:
             *
             * ClassName: public returnType name(...); atd.
             */
            final StringBuilder methodFinalText = new StringBuilder();




            /*
             * V případě, že se mají zahrnout i metody z rozhraní a právě iterovaná metoda
             * je implementovaná z rozhraní, tak na začátek doplním anotaci '@Override', aby
             * to uživatel lépe poznal (že se jedná o metodu z nějakého rozhraní).
             */
            if (showMethodsFromInterfaces && isMethodFromInterface(clazz, m))
                methodFinalText.append("@Override" + " ");





            /*
             * V případě, že se mají načíst metody i z předků, tak, aby je uživatel mohl v okně dialogu rozlišit,
             * přidá na začátek název třídy, kde se příslušná metoda nachází.
             *
             * V případě, že se jedná o vnitřní třídu, za název třídy se přidá dolar a ty oddělují jednotlivé vnitřní
             * třídy.
             *
             * Princip je takový, že se vezmě název třídy i s baličky a pouze se odebere veškerý text od začátku
             * textu / názvu třídy až po poslední desetinnou tečku. Protože tato část značí balíčky, kde se třída
             * nachází. Zbytek je název třídy a případně i její vnitřní třídy. Pak následují konkrétní metody.
             */

            if (includeInnerClasses || includeAncestors) {
                /*
                 * Zde nemohu dát simple name, ale musím využít getName, což vrátí název třídy i s balíčky, ale v
                 * případě, že se jedná o vnitřní třídu, tak vrátí i "cestu" ke všem vnitřním třídám, takže ze
                 * začátku odeberu veškerý text po poslední desetinnou tečku, což značí balíčky a zbytek je název
                 * třídy a případně za ní veškeré vnitřní třídy.
                 */
                final String classNameWithoutPackages = getClassNameWithoutPackages(clazz);

                methodFinalText.append(classNameWithoutPackages);

                methodFinalText.append(": ");
            }


            // Cyklus, který přidá začátek metody až po návratový typ metody - bez něj:
            IntStream.range(0, partsOfMethod.length - 2).forEach(i -> methodFinalText.append(partsOfMethod[i]).append(" "));

            // Návratový typ metody:
            methodFinalText.append(ParameterToText.getMethodReturnTypeInText(m, false)).append(" ");

            // Název metody:
            methodFinalText.append(m.getName());

            // Parametry metody:
            final String parameterText = ReflectionHelper.getParametersInText(m.getParameters());
            methodFinalText.append(parameterText);

            methodsList.add(new MethodInfo(methodFinalText.toString(), m, objInstance, reference, clazz));
        }




        /*
         * Zde otestuji, zda se má rekurzí pokračovat -> pokud se mají načíst i
         * konstruktory předků třídy clazz a předek není null nebo není Object.
         */
        if (includeAncestors && clazz.getSuperclass() != null && clazz.getSuperclass() != Object.class)
            findAllMethodsFromClass(clazz.getSuperclass(), objInstance, reference, includeAncestors, false,
                    showMethodsFromInterfaces, includeInnerClasses);
    }


    /**
     * Metoda, která zjistí, zda je metoda method implementovaná z rozhraní, které třída clazz implementuje.
     *
     * @param clazz
     *         - třída, u které se má zjistit, zda je její metoda method "převzatá / implementovaná" z rozhraní nebo
     *         ne.
     * @param method
     *         - metoda, která se nachází ve třídě clazz a má se o ní zjistit, zda je "převzatá" z rozhraní, které třída
     *         clazz implementuje nebo ne.
     *
     * @return true, pokud třída clazz implementuje nějaké rozhraní a metoda method je z něj "převzatá", jinak false.
     */
    private static boolean isMethodFromInterface(final Class<?> clazz, final Method method) {
        /*
         * Postup:
         *
         * - Zjistím si veškerá rozhraní, která implementuje třída clazz.
         *
         * - Projdu cyklem veškerá rozhraní, u každého rozhraní si načtu jeho metody a
         * ty cyklem projdu.
         *
         * - V tom cyklu (procházím metody z rozhraní) a pro každou metodu testuji, zda
         * se ta aktuálně iterovaná metoda z rozhraní shoduje její název, viditelnost
         * pouze public (protože v rozhraní jiná viditelnost není), zda je metoda
         * statická, návratový typ metod je stejný a typy a počet parametrů se také
         * shodují, pokud jsou tyto podmínky splněny a nějaká metoda v nějakém rozhraní
         * je stejná jako metoda method, pak se vrátí true, jinak false.
         *
         * Note:
         * Ještě by bylo možné testovat, zda je default a abstract, ale to už vynechám,
         * to se moc nevyužívá, už jej tyto podmínky by měli stačit.
         */

        /*
         * Získám si veškerá rozhraní, která třída clazz implementuje.
         */
        final Class<?>[] interfaces = clazz.getInterfaces();

        return Arrays.stream(interfaces)
                .anyMatch(i -> Arrays.stream(i.getDeclaredMethods())
                        .anyMatch(m -> m.getName().equals(method.getName())
                                && Modifier.isPublic(m.getModifiers()) == Modifier.isPublic(method.getModifiers())
                                && Modifier.isStatic(m.getModifiers()) == Modifier.isStatic(method.getModifiers())
                                && m.getReturnType().equals(method.getReturnType())
                                && Arrays.deepEquals(m.getParameterTypes(), method.getParameterTypes())));
    }


    /**
     * Metoda, která si získá veškeré konstruktory v třídě clazz a pokud je proměnná includeAncestors true, pak se
     * rekurzivně načtou i veškeré konstruktory ze všech předků úplně první předané třídy clazz až po třídu Object, z té
     * už se konstruktory brát nebudou.
     * <p>
     * Každý konstruktor převedený do textu se vloží do listu constructorsList.
     *
     * @param clazz
     *         - vždy se jedná o třídu, ze které se mají bát konstruktory.
     * @param includeAncestors
     *         - logická proměnná dle které se pozná, zda se mají načíst i konstruktory z předků třídy clazz (té první
     *         předané).
     * @param includeInnerClasses
     *         - true v případě, že se mají načíst i konstruktory, které se nachází ve vnitřních třídách, jinak false.
     */
    private static void findAllConstructors(final Class<?> clazz, final boolean includeAncestors,
                                            final boolean includeInnerClasses) {
        /*
         * Bude true v případě, že se mají načíst i soukromé konstruktory z třídy clazz. To bude při prvním zavolání
         * metody pro nalezení konstruktorů.
         *
         * V případě, že se metoda pro nalezení konstruktorů bude rekurzivně opakovat, bude se tato proměnná předávat
         * s hodnotou false, aby se nenačítali ze zdědných tříd / předků soukromé konstruktory.
         *
         * <i>Toto by v podstatě nemělo být vůbec, konstruktory se pouze volají, ale za účelem ukázky uživateli
         * konstruktorů v předkovi - které "může vidět" to je zde uméžněno / doplněno.</i>
         */
        final boolean includePrivateConstructors = true;

        findAllConstructorsFromClass(clazz, includeAncestors, includePrivateConstructors);

        /*
         * Je třeba, aby byly nalezené konstruktory z vnitřních tříd vkládány právě do kolekce typu Set, do které
         * nelze vkládat duplicitní hodnoty. Protože v případě, že jsou třídy provázadné například tak, že třída
         * obsahuje více vnitřních tříd, které
         * mezi sebou navzájem dědí, třeba A a B dědí z C apod. Pokud by tomu tak bylo, pak se některé hodnoty
         * načetly vícekrát, protože by se vícekrát procházel předek těch vnitřních tříd. Což je chyba.
         *
         * Vyřešeno právě tím, že se do Setu (kolekce) nepřidají duplicitní hodnoty.
         */
        if (includeInnerClasses) {
            // Získání veškerých vnitřních tříd:
            final List<Class<?>> allInnerClasses = ReflectionHelper.getAllInnerClasses(clazz.getDeclaredClasses(),
                    new ArrayList<>());

            allInnerClasses.forEach(c -> findAllConstructorsFromClass(c, includeAncestors, includePrivateConstructors));
        }
    }


    /**
     * Metoda, která si získá veškeré konstruktory v třídě clazz a pokud je proměnná includeAncestors true, pak se
     * rekurzivně načtou i veškeré konstruktory ze všech předků úplně první předané třídy clazz až po třídu Object, z té
     * už se konstruktory brát nebudou, podobně pro vnitřní třídy.
     * <p>
     * Každý konstruktor převedený do textu se vloží do listu constructorsList.
     *
     * @param clazz
     *         - vždy se jedná o třídu, ze které se mají bát konstruktory.
     * @param includeAncestors
     *         - logická proměnná dle které se pozná, zda se mají načíst i konstruktory z předků třídy clazz (té první
     *         předané).
     * @param includePrivateConstructors
     *         - logická proměnná, která značí, zda se mají načítat i soukromé konstruktory, tato proměnná bude true při
     *         prvním volání této metody, ale pokud se volá rekurzivně bude false, protože z předků už by neměly dle
     *         pravidel jazyka Java být přístupné soukromé konstruktory.
     */
    private static void findAllConstructorsFromClass(Class<?> clazz, boolean includeAncestors,
                                                     boolean includePrivateConstructors) {
        /*
         * Tato podmínka by neměla být splněna, ale kdyby náhodou.
         */
        if (clazz == null || clazz.getDeclaredConstructors() == null)
            return;

        for (final Constructor<?> c : clazz.getDeclaredConstructors()) {
            /*
             * Pokud se nemají načíst soukromé konstruktory a aktuálně iterovaný konstruktor
             * je soukromý, pak pokračuji další iterací - dalším konstruktorem.
             */
            if (!includePrivateConstructors && Modifier.isPrivate(c.getModifiers()))
                continue;

            /*
             * Název konstruktoru (jak to zám já) je "viditelnost" název třídy (parametry),
             * takže pokud jej chci bez balíčků, pak jej mohu získat tak, že si nejprve
             * zjistím, zda je veřejný apod. To mohu udělat například pomocí
             * Modifiers.isPublic, isProtected, ...
             *
             * ale raději se vezmu první "slovo" z textu celého konstruktoru, pak si získám
             * název třídy nebo si vezmu text za poslední desetinnou tečkou v
             * constructor.getName() a pak je přidám parametry.
             */


            // Viditelnost konstruktoru:
            final String visibility;

            final String[] partsOfConstructor = c.toString().split("\\s+");

            if (partsOfConstructor.length > 1)
                visibility = c.toString().split("\\s+")[0] + " ";
                // Není uvedena viditelnost - packagePrivate
            else visibility = "";


            // Název třídy bez balíčků, případně i s vnitřními třídami:
            final String classNameWithoutPackages = getClassNameWithoutPackages(clazz);

            /*
             * Finální text, který se zobrazí v dialogu
             *
             * Nejprve se přidá název třídy - bez balíčků (i s viditelnost):
             */
            String constructorFinalText = visibility + classNameWithoutPackages;

            // Parametry konstruktoru:
            constructorFinalText += ReflectionHelper.getParametersInText(c.getParameters());

            constructorsList.add(new ConstructorInfo(constructorFinalText, c, clazz));
        }



        /*
         * Zde otestuji, zda se má rekurzí pokračovat -> pokud se mají načíst i
         * konstruktory předků třídy clazz a předek není null nebo není Object.
         */
        if (includeAncestors && clazz.getSuperclass() != null && clazz.getSuperclass() != Object.class)
            findAllConstructorsFromClass(clazz.getSuperclass(), includeAncestors, false);
    }


    /**
     * Získání názvu třídy clazz bez balíčků.
     * <p>
     * Jde o to, že když je třídy clazz vnitřní třída, tak je třeba získat její text, tedy název třídy kde se ta vnitřní
     * třídy nachází a ta / ty vnitřní třídy, ale využíté metody vrací vždy název třídy s balíčky nebo samotný název
     * třídy, kde není poznat vnitřní třída. Proto je třeba vzít si celý text a odebrat balíčky - text od začátku po
     * poslední desetinnou tečku.
     *
     * @param clazz
     *         - třída, ze které se má sestavit název třídy bez balíčků.
     *
     * @return název třídy clazz bez balíčků
     */
    private static String getClassNameWithoutPackages(Class<?> clazz) {
        return clazz.getName().substring(clazz.getName().lastIndexOf('.') + 1);
    }


    /**
     * Metoda, která načte veškeré proměnné z třídy clazz a převede je do příslušné syntaxe - i s hodnotou příslušné
     * proměnné a každou takto převedenou proměnné do textové podoby se vloží do listu variablesList.
     *
     * @param clazz
     *         - třída, ze které se vezmou veškeré proměnné a převedou se do textové podoby a přidají do výše uvedeného
     *         listu. Pokud je proměnná includeAncestors true, pak se vezmou i proměnné z předků ale tam už se budou
     *         brát pouze veřejné nebo chráněné proměnné.
     * @param objClass
     *         - instance třídy pro získání hodnot.
     * @param includeAncestors
     *         - logická proměnná o tom, zda se mají načíst i veřejné a chráněné proměnné z předků třídy clazz true ano,
     *         false ne -> nemají se načítat proměnné z předků, mají se načíst pouze proměnné z třídy clazz.
     * @param includeInnerClasses
     *         - zda se mají načíst i hodnoty z vnitřních tříd.
     */
    private void findAllVariables(final Class<?> clazz, final Object objClass,
                                  final boolean includeAncestors, final boolean includeInnerClasses) {
        /*
         * Na začátek bude tato hodnoty vždy true. Je to pro případ, že se budou vyhledávat proměnné i z předků. V
         * takovém případě se již nebudou hledat soukromé proměnné, pouze veřejnéa chráněné. Proto se při rekurzivním
         * volání příslušné metody bude předávat hodnota false, aby se z předků nenačítaly hodnoty s nechtěnou
         * viditelností.
         */
        final boolean includePrivateVariables = true;

        findAllVariablesFromClass(clazz, objClass, includeAncestors, includePrivateVariables, includeInnerClasses);

        if (includeInnerClasses) {
            // Získání veškerých vnitřních tříd:
            final List<Class<?>> allInnerClasses = ReflectionHelper.getAllInnerClasses(clazz.getDeclaredClasses(),
                    new ArrayList<>());

            allInnerClasses.forEach(c -> findAllVariablesFromClass(c, objClass, includeAncestors,
                    includePrivateVariables, includeInnerClasses));
        }
    }


    /**
     * Metoda, která načte veškeré proměnné z třídy clazz a převede je do příslušné syntaxe - i s hodnotou příslušné
     * proměnné a každou takto převedenou proměnnou do textové podoby vloží do listu variablesList.
     *
     * @param clazz
     *         - třída, ze které se vezmou veškeré proměnné a převedou se do textové podoby a přidají do výše uvedeného
     *         listu. Pokud je proměnná includeAncestors true, pak se vezmou i proměnné z předků ale tam už se budou
     *         brát pouze veřejné nebo chráněné proměnné.
     * @param objClass
     *         - instance třídy pro získání hodnot.
     * @param includeAncestors
     *         - logická proměnná o tom, zda se mají načíst i veřejné a chráněné proměnné z předků třídy clazz true ano,
     *         false ne -> nemají se načítat proměnné z předků, mají se načíst pouze proměnné z třídy clazz.
     * @param includePrivateVariables
     *         - logická proměnná, která značí, zda se mají načíst soukromé proměnné z třídy clazz. Tato proměnná bude
     *         true pouze v případě, že se jedná o "první" volání toto metody, pokud se tato metoda volá již rekurzivně
     *         pro načtení proměnných z předků třídy clazz, pak bude tato proměnná vždy false, protože zpředků se mají
     *         brát pouze veřejné a chráněné proměnné, ne soukromé.
     * @param includeInnerClasses
     *         - true v případě, že se mají zahrnout i proměnné nacházející se ve vnitřních třídách. Tento parametr je
     *         potřeba v podstatě pouze pro to, "jak / v jaké syntaxi" se mají zobrazit proměnné v dialogu, tedy i s
     *         třídou, ve které se nachází.
     */
    private void findAllVariablesFromClass(Class<?> clazz, Object objClass,
                                           boolean includeAncestors, boolean includePrivateVariables,
                                           final boolean includeInnerClasses) {
        /*
         * Tato podmínka by nikdy neměla být splněna.
         */
        if (clazz == null || clazz.getDeclaredFields() == null)
            return;


        // Projdu veškeré získané proměnné:
        for (final Field f : clazz.getDeclaredFields()) {
            /*
             * Pokud se nemají načíst soukromé atributy / proměnné a aktuálně iterovaný
             * atribut / parametr / proměnná je soukromá, pak pokračuji další iterací -
             * dalším parametrem.
             */
            if (!includePrivateVariables && Modifier.isPrivate(f.getModifiers()))
                continue;

            f.setAccessible(true);


            // Zde nemusí být uveden modifikator - public, protected nebo private, pak muze byt i velikost 2:
            final StringBuilder variable = new StringBuilder();

            /*
             * Pokud se mají zpřístupnit i proměnné ze všech předků, tak na začátek přidám
             * syntxi: 'ClassName: ', aby uživatel věděl, v jaké třídě se příslušná proměnná
             * nachází.
             */
            if (includeAncestors || includeInnerClasses)
                variable.append(getClassNameWithoutPackages(clazz)).append(": ");


            /*
             * Přidání textu proměnné od začátku po datový typ, tedy například private static final transient, ...
             * dataType.
             *
             * Ale bez dataType. DatListCellRenderer#getListCellRendererComponentový typ se převede až níže do podoby textu bez balíčků.
             */
            if (f.getType().isArray())
                variable.append(f.toString(), 0, f.toString().indexOf(f.getType().getTypeName()));
            else variable.append(f.toString(), 0, f.toString().indexOf(f.getType().getName()));

            // Přidání datového typu proměnné v podobě textu:
            variable.append(ParameterToText.getFieldInText(f, false));

            // Mezera za datovým typem a před názvem proměnné:
            variable.append(" ");

            // Název proměnné:
            variable.append(f.getName());

            variable.append(" = ");


            /*
             * True v případě, že se na konec sestavené syntaxe má přidat středník, jinak false.
             *
             * Jedná se o to, že v případě, že se jedná o proměnnou, která obsahuje referenci na jinou instanci v
             * diagramu instancí, tak se za středník přidává referenční proměnná - je li povoleno, pak se na konec
             * středník nebude přidávat.
             */
            boolean addSemicolon = true;


            // Získání hodnoty z proměnné:
            final Object objFieldValue = ReflectionHelper.getValueFromField(f, objClass);


            // Otestuji, zda se nejedná o pole, pokud ano, tak se převede na text:
            if (f.getType().isArray()) {
                final String test = getArrayFromObject(f, objClass);

                /*
                 * Zde se přidá text pro zobrazení. Může se jednat například o null hodnotu, nebo o konkrétní hodnotu
                 * získanou z proměnné.
                 */
                variable.append(test);
            }


            /*
             * Pokud je null, tak to zcela určitě není instance nějaké třídy z diagramu tříd
             * která se nachází v diagramu instancí, takže nebudu na konec přidávat text s
             * referencí.
             */
            else if (objFieldValue == null)
                variable.append(objFieldValue);


                /*
                 * Zde otestuji, zda se jedná o list / kolekci, ale toto je potřeba pouze v
                 * případě, že se mají zobrazovat ty referenční proměnné za příslušnou instanci,
                 * protože jinak zde není ten list třeba nijak testovat, protože se do textové
                 * podoby převede "v pohodě" sám.
                 *
                 * Dále ještě potřebuji otestovat, zda se jedná o list typu nějaké třídy, která
                 * se nachází v diagramu tříd, protože jinak taky není potřeba ten list
                 * procházet.
                 */
            else if (showReferenceVariables && ReflectionHelper.isDataTypeOfList(f.getType())
                    && isClassInCd(ReflectionHelper.getDataTypeOfList(f).getDataType()))
                variable.append(getListValuesWithReferencesVar(objFieldValue));


            else if (f.getType().getSimpleName().equalsIgnoreCase("String"))
                variable.append("\"").append(objFieldValue).append("\"");


            else if (f.getType().getSimpleName().equalsIgnoreCase("char") || f.getType().getSimpleName().equalsIgnoreCase("Character"))
                variable.append("'").append(objFieldValue).append("'");



                /*
                 * Zde se zjistí, zda se mají zobrazovat
                 * referenční hodnoty, pokud ne, tak jen naplním příslušnou textovou proměnnou
                 * textem té proměnné, jinak pokud se mají zobrazovat ty referenční proměnné,
                 * tak zkusím otestovat tu hodnotu proměnné a případně, pokud je to instance,
                 * která se nachází v diagramu instancí, tak doplním tu referenci.
                 */
            else if (!showReferenceVariables)
                variable.append(objFieldValue);


            else {
                /*
                 * Zde si zjistím, zda je získaná hodnota z proměnné nějaká instance třídy z
                 * diagramu tříd, která se nachází v diagramu instancí.
                 */
                final String reference = Instances.getReferenceToInstanceFromCdInMap(objFieldValue);

                /*
                 * Pokud reference není null, pak je objFieldValue nějaká instance nějaké třídy
                 * z diagramu tříd a tato instance (objFieldValue) se nachází v diagramu
                 * instancí, tak za středník přidám referenci na tuto instancí, aby uživatel
                 * věděl, o jaký objekt se jedná.
                 */
                if (reference != null) {
                    variable.append(objFieldValue).append("; (").append(txtReferenceVarText).append(": ").append(reference).append(")");
                    // Na konec výrazu se již středník přidávat nebude:
                    addSemicolon = false;
                }

                /*
                 * Zde se nejedná o žádnou instanci z diagramu instancí, tak na konec za
                 * středník nic přidávat nebudu:
                 */
                else
                    variable.append(objFieldValue);
            }

            if (addSemicolon)
                variable.append(";");


            // Přidání nové proměnné:
            variablesList.add(new VariableInfo(variable.toString(), objFieldValue, clazz, f));
        }


        /*
         * Zde otestuji, zda se má rekurzí pokračovat -> pokud se mají načíst i
         * konstruktory předků třídy clazz a předek není null nebo není Object.
         */
        if (includeAncestors && clazz.getSuperclass() != null && clazz.getSuperclass() != Object.class)
            findAllVariablesFromClass(clazz.getSuperclass(), objClass, includeAncestors, false, includeInnerClasses);
    }


    /**
     * Metoda, která projde list objList tak, že ho převede na list objektů, celý list projde a pro každý objekt
     * otestuji, zda se náhodou nejdená o instanci třídy z diagramu tříd, a tato instnace se aktuálně nachází v diagramu
     * instancí, pokud ano, pak se za každou takovou instanci, která má svou reprezentaci v daigramu instnací přidá
     * ještě zadaná reference na tu příslušnou instanci.
     *
     * @param objList
     *         - list, jehož hodnoty se mají převést do textové podobny, aby je bylo možné zobrazit v tomto dialogu a u
     *         každé hodnoty, která je instance třídy z diagramu tříd a má svou reprezentaci v diagramu instancí tak, za
     *         každou takovou instanci se přidá ještě ten text reference, kterou zadal uživatel.
     *
     * @return text, který bude obsahovat hodnoty v listu, akorát v textové podobně a za každou hodnotou toho listu,
     * když je to instanci, která má reprezentaci v diagramu instancí, tak se za ni přídá ten text reference.
     */
    @SuppressWarnings("unchecked")
    private static String getListValuesWithReferencesVar(final Object objList) {
        /*
         * V případě, že ten list (objList) je null, pak není co řešit.
         */
        if (objList == null)
            return "null;";


        /*
         * Zde list není null hodnota, takže bude při nejmenším prázdný. Takže si ten
         * předaný list (objLIst) mohu převést na pole objektů.
         */
        final List<Object> instancesList = (List<Object>) objList;


        /*
         * Vytvořím si proměnnou, do které budu vkládat veškerý text z toho listu. A
         * tento text na na konci metody vrátí.
         */
        final StringBuilder result = new StringBuilder("[");

        // Projdu list:
        for (final Object o : instancesList) {
            /*
             * pokud je to null hodnota, tak není co řešit, přidám null hodnotu do textu a
             * pokračuji další iterací.
             */
            if (o == null) {
                result.append("null, ");
                continue;
            }


            /*
             * Zde se jedná o nějakou hodnota, která není null, tak zjistím, zda je to
             * instance:
             */
            final String reference = Instances.getReferenceToInstanceFromCdInMap(o);

            /*
             * Pokud není reference null, pak je to instance nějaké třídy z diagramu tříd,
             * která je reprezentována v diagramu instancí, tak na konec přidám ten text
             * reference.
             */
            if (reference != null)
                result.append(o).append(" (").append(txtReferenceVarText).append(": ").append(reference).append("), ");

                // Zde to není instance z diagramu instancí, tak nepřidávám text reference:
            else
                result.append(o).append(", ");
        }


        /*
         * Pokud byla přidána alespoň jedna hodnota, tj. list není prázdný, tak odeberu
         * poslední dva znaky, což je mezera a desetinná čárka.
         */
        if (result.length() > 1)
            result.setLength(result.length() - 2);

        // Přidám uzavírací závorku:
        result.append("]");

        // Vrátím výše získaný text:
        return result.toString();
    }


    /**
     * Metoda, která převede získaný objekt z proměnné na jednorozměrné pole objektů a to na text, aby se jeho obsah -
     * pole mohl zobrazit v dialogu uživateli
     *
     * @param field
     *         - položka instance třídy, u které otestuji, zda obsahuje pole
     * @param objClass
     *         - instance třídy, ve které se testuje výše zmíněná položka
     *
     * @return obsah pole - pokud se jedná o pole, jinak null
     */
    private String getArrayFromObject(final Field field, final Object objClass) {
        if (!field.getType().isArray())// Není třeba, ale pro případ budoucího volání metody i někde jinde
            return null;

        // Zpřístupním proměnnou (je třeba pouze u private a protected proměnných):
        field.setAccessible(true);

        /*
         * Získám si datový typ toho pole, abych jej mohl otestovat, zda se jedná o pole
         * typu nějaké třídy, která se nachází v diagramu tříd, protože dále potřebuji
         * testovat, zda se mají přidat reference za tu hodnotu nebo ne.
         */
        final Class<?> clazz = ReflectionHelper.getDataTypeOfArray(field.getType());

        /*
         * Získám si hodnotu z příslušné proměnné, resp. získám si pole.
         *
         * (Hodnotu by byloi možné předat v parametru metody.)
         */
        final Object objArray = ReflectionHelper.getValueFromField(field, objClass);


        /*
         * Pokud je to null hodnota, tak nemá smysl pokračovat.
         */
        if (objArray == null)
            return null;

        // Přidám otevírací závorku na začátek textu:
        txtTempForArray.setLength(0);
        txtTempForArray.append("[");


        /*
         * Zde zjistím, zda je to pole typu nějaké třídy z diagramu tříd, a pokud ano,
         * pak zavolám metodu, která ještě doplní k příslušným instancím reference na tu
         * instanci, kterou uživatel zadal - reference na samotnou instanci v diagramu
         * instnací.
         *
         * Dále ještě musí být povoleno, zda se mají ty referenční proměnné zobrazovat
         * nebo ne.
         */
        if (isClassInCd(clazz) && showReferenceVariables)
            convertObjArrayIntoTtext(objArray);

        else {
            /*
             * Zde se nejedná o pole typu nějaké třídy z diagramu tříd, tak pokračuji
             * "normálně", tj. bez testování, zda se mají přidat ty reference za příslušné
             * instanci z diagramu instnací (pokud se tam nacházejí).
             */
            if (ReflectionHelper.getCountOfDimensionArray(objArray) > 1)
                getValuesFromArray(objArray, true);

            else
                getValuesFromArray(objArray, false);
        }

        // zde odeberu poslední mezeru a čárku na konci:
        if (txtTempForArray.length() > 1)
            txtTempForArray.setLength(txtTempForArray.length() - 2);

        return txtTempForArray.append("]").toString();
    }


    /**
     * Metoda, která zjistí, zda je objekt clazz nějaká třída z diagramu tříd. To se zjistí tak, že se z této proměnné
     * clazz zjistí balíčky příslušné třídy a název příslušné třídy. O sestaveném textu (balíčky.ClassName) se zjistí, zda je to text třídy v diagramu tříd, pokud ano, jedná se o třídu v diagramu tříd.
     * @param clazz
     *         - nějaká třída, o které se má zjistit, zda se jedná o třídu, která je reprezentována v diagramu tříd.
     *
     * @return true, pokud je clazz nějaká třída, která má svou reprezentaci v daigramu tříd, jinak false.
     */
    private static boolean isClassInCd(final Object clazz) {
        if (clazz == null)
            return false;

        // Datový typ v podobě textu s balíčky:
        final String dataTypeInText = ParameterToText.getDataTypeInText(clazz, true);

        // Zjištění, jestli je v diagramu tříd název třídy s výše získaným textem:
        return GraphClass.isClassWithTextInClassDiagram(dataTypeInText);
    }


    /**
     * Metoda, která "proiteruje" X - rozměrné pole - array a jeho hodnoty vloží do proměnné: txtTempForArray, aby se
     * mohli hodnoty pole zobrazit v textu v diaglo, tak, aby jim uživatel rozumněl
     *
     * @param array
     *         -´pole načtené z nějaké proměnné z instance třídy a metoda z něj vytáhně hodnoty
     * @param isMultidimensionalArray
     *         - logická proměnná o tom, zda se jedná o jednorozměrné pole, nebo vícerozměrné pole, podle toho se pak
     *         vkládají hodnoty do proměnné txtTempForArray bud jako pole pomocí metod z Arrays, nebo "klasické vložení"
     *         do textové proměnné
     */
    private void getValuesFromArray(final Object array, final boolean isMultidimensionalArray) {
        // Zjistím si velikost pole:
        final int length = Array.getLength(array);

        new ParseToJavaTypes();

        // Budu iterovat celu velikost pole, pokaždé si získám jeho hodnotu a zeptám se, zda je ta hodnota další pole
        // pokud ano, provedu rekuzivní volání této metody s tou nově získanou hodnotou, pokud ne, jedná se o
        // konkrétní hodnotu,
        // tak ji  prostě vložím do proměnné:
        for (int i = 0; i < length; i++) {
            final Object value = Array.get(array, i);

            // Zde potřebuji otestovat, zda není hodnota v poli náhodou null,
            // pokud ano, tak se poouze přidá text coby null hodnota
            if (value == null) {
                txtTempForArray.append("null, ");
                continue;
            }

            if (!value.getClass().isArray()) {
                if (!isMultidimensionalArray)
                    txtTempForArray.append(value).append(", ");
                continue;
            }

            // Pokud se jedná o poslední dimenzi pole, tak jej vložím do textu pomocí metody ArrayToString nad polem
            if (ReflectionHelper.getCountOfDimensionArray(value) == 1) {
                final Object[] objArray = ParseToJavaTypes.convertToObjectArray(value);
                txtTempForArray.append(Arrays.toString(objArray)).append(", ");
            }

            // Zde to není jednorozměrné pole, ale je to pole, takže má další rozměr / dimezi, tak pokračuji rekurzí:
            else {
                /*
                 * Jelikož se jedná o další dimezi, tak musím i znázornit "další pole" pomocí
                 * hranatých závorek.
                 *
                 * Takže nejdříve přidám první hranatou závorku, pak zavolám rekurzí tuto metodu
                 * akorát s tím "dalším" polem / dimezí pole, mezi tím se do té proměnné
                 * txtTempForArray naplní hodnoty z té dimeze a pak až do doběhne, tak na konci
                 * jen přidám uzavírací hranatou závorku, abych tu "dimenzi / další pole"
                 * uzavřel - nějak vizuálně oddělil.
                 *
                 * Ale ještě před tím otestuji, zda ten text končí: ', ', protože pokud ne, pak
                 * to pole neboli dimeze byla prázdná a nepřidala se žádná hodnota z toho pole.
                 */

                txtTempForArray.append("[");

                // Rekuzí doplním do txtTempForArray hodnoty z té dimenze:
                getValuesFromArray(value, isMultidimensionalArray);

                // Otestuji, zda se něco přidalo, jinak by tam bylo jen: '[ ':
                if (txtTempForArray.toString().endsWith(", "))
                    txtTempForArray.setLength(txtTempForArray.length() - 2);

                // Uzavřu "dimezi / pole":
                txtTempForArray.append("], ");
            }
        }
    }


    /**
     * Metoda, která převede všechny hodnoty z pole objArray do textové podoby a u každé položky v tom poli ještě
     * otestuje, zda se jedná o instancí nějaké třídy, která se nachází v diagramu instancí, pokud ano, pak na konec
     * ještě přidá text té reference na příslušnou instanci, kterou uživatel zvolil při jejím vytváření.
     * <p>
     * O objektu objArray už se musí vědět, že je to X - rozměrné pole typu nějaké třídy, která se nachází v diagramu
     * tříd.
     *
     * @param objArray
     *         - X - rozměrné pole typu nějaké třídy, která se nachází v diagramu tříd.
     */
    private void convertObjArrayIntoTtext(final Object objArray) {
        /*
         * Získám si velikost pole (v případě více rozměrného pole je to velikost té
         * první dimeze).
         */
        final int length = Array.getLength(objArray);


        for (int i = 0; i < length; i++) {
            /*
             * Získám si hodnotu z příslušného pole na příslušném / iterovaném indexu.
             */
            final Object objValue = Array.get(objArray, i);

            // Pokud je to null hodnota, pak pokračuji další iterací (do textu se přidá jen
            // null)
            if (objValue == null) {
                txtTempForArray.append("null, ");
                continue;
            }


            /*
             * Zde, pokud je ta získaná hodnota z pole jednorozměrné pole, pak jej převedu
             * do podoby typu Object, aby bylo snaží jej převést do textové podoby, navíc,
             * díky tomu, že to provedu následjícím posutpem (jakoby pro celé pole zvlášť),
             * tak nemusím hlídat závorky kolem těch hodnot -> pole v poli.
             */
            if (objValue.getClass().isArray()) {
                if (ReflectionHelper.getCountOfDimensionArray(objValue) == 1) {
                    final Object[] editedArray = ParseToJavaTypes.convertToObjectArrayAndReferenceVariable(objValue,
                            txtReferenceVarText);
                    txtTempForArray.append(Arrays.toString(editedArray)).append(", ");
                }

                // Zde to pole má více dimezí, tak pokračuji rekurzí:
                else {
                    /*
                     * Jelikož se jedná o další dimezi, tak musím i znázornit "další pole" pomocí
                     * hranatých závorek.
                     *
                     * Takže nejdříve přidám první hranatou závorku, pak zavolám rekurzí tuto metodu
                     * akorát s tím "dalším" polem / dimezí pole, mezi tím se do té proměnné
                     * txtTempForArray naplní hodnoty z té dimeze a pak až do doběhne, tak na konci
                     * jen přidám uzavírací hranatou závorku, abych tu "dimenzi / další pole"
                     * uzavřel - nějak vizuálně oddělil.
                     *
                     * Ale ještě před tím otestuji, zda ten text končí: ', ', protože pokud ne, pak
                     * to pole neboli dimeze byla prázdná a nepřidala se žádná hodnota z toho pole.
                     */

                    txtTempForArray.append("[");

                    // Rekuzí doplním do txtTempForArray hodnoty z té dimenze:
                    convertObjArrayIntoTtext(objValue);

                    // Otestuji, zda se něco přidalo, jinak by tam bylo jen: '[ ':
                    if (txtTempForArray.toString().endsWith(", "))
                        txtTempForArray.setLength(txtTempForArray.length() - 2);

                    // Uzavřu "dimezi / pole":
                    txtTempForArray.append("], ");
                }
            }

            else {
                /*
                 * Zde se jedná o konkrétní hodnoty, která není null - je to nějaká hodnota v
                 * příslušném poli, tak otestuji, zda je to instancí, která se nachází v
                 * diagramu instnací a pokud ano, tak na konec přidám ten text referenční
                 * proměnné, kterou uživatel zvolil při jejím vytváření.
                 */

                /*
                 * Získám si text reference na příslušnou instanci - pokud je v diagramu
                 * instancí.
                 */
                final String reference = Instances.getReferenceToInstanceFromCdInMap(objValue);

                /*
                 * Otestuji, zda byla získána reference, pokud ano, je to instnace z diagramu
                 * instnací, tak ji na konec přidám, pokud bude null, tak to není instnace,
                 * která se nachází v diagramu instnací, tak přidám jen tu hodnotu.
                 */
                if (reference != null)
                    txtTempForArray.append(objValue).append(" (").append(txtReferenceVarText).append(": ").append(reference).append("), ");

                else
                    txtTempForArray.append(objValue).append(", ");
            }
        }
    }


    /**
     * Metoda, která vytvoří instanci Jscroll pane pro rolování listem objektů třídy a přidá je do panelu ještě s
     * tlačítkem, které otevře dialog pro vyhledávání řetězců. Dále přidá další tlačítko btn2 vedle talčítka pro
     * vyhledávání
     *
     * @param list
     *         - Jlist objektů třídy - buď je to list s konstruktory, metodami nebo proměnným třídy
     * @param btn1
     *         - tlačítko, které po kliknutí otevře dialog pro vyhledávání v daném listu
     * @param btn2
     *         - tlačítko, které po kliknutí otevře dialog pro vytvoření reference, popřípadě zavolání metody.
     * @param pnl
     *         - panel, do kterého budou tlačítka a ten list vloženy a ten panel, je dále vložen do okna dialogu
     */
    private static void addListWithButtons(final JList<ValuesInfo> list, final JButton btn1, final JButton btn2,
                                           final JPanel pnl) {

        final JScrollPane jsp = new JScrollPane(list);

        pnl.setLayout(new BoxLayout(pnl, BoxLayout.PAGE_AXIS));
        jsp.setAlignmentX(CENTER_ALIGNMENT);
        pnl.add(jsp);

        /*
         * Panel, do kterého se vloží příslušná tlačítka.
         */
        final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.CENTER));

        pnlButtons.add(btn1);
        pnlButtons.add(btn2);

        pnl.add(Box.createRigidArea(new Dimension(0, 10)));
        pnlButtons.setAlignmentX(CENTER_ALIGNMENT);
        pnl.add(pnlButtons);

        pnl.add(Box.createRigidArea(new Dimension(0, 10)));
    }


    /**
     * Metoda, která přidá komponentu v parametru do okna dialogu
     *
     * @param component
     *         - komponenta, která se přídá do okna dialogu s nastaveným rozestupem a zarovnaným umístěním na střed
     *         dalogu
     */
    private void addComponent(final JComponent component) {
        add(Box.createRigidArea(new Dimension(0, 20)));
        component.setAlignmentX(CENTER_ALIGNMENT);
        add(component);
    }


    /**
     * Metoda, která naplní proměnnou txtReferenceVarText, která obsahuje text: 'Referenční proměnná' v uživatelem
     * zvoleném jazyce.
     * <p>
     * Jde o to, že při zavolání konstruktoru tohoto dialogu se načtou veškeré proměnné a zobrazí se v dialogu, ale když
     * bude nastaveno, že se mají zobrazit referenční proěmnné u instancí v proměnných, tak bude tato proměnná při
     * prvním vytvoření tohoto dialogu ještě null, tak ji musím naplnit "předně".
     *
     * @param languageProp
     *         - objekt Properties, který obsahuje texty pro tuto aplikaci v uživatelem zvoleném jazyce.
     */
    private static void fillTxtReferenceVar(final Properties languageProp) {
        if (languageProp != null)
            txtReferenceVarText = languageProp.getProperty("Ppm_Txt_ReferenceVariable",
                    Constants.PPM_TXT_REFERENCE_VARIABLE);
        else
            txtReferenceVarText = Constants.PPM_TXT_REFERENCE_VARIABLE;
    }


    @Override
    public void setLanguage(final Properties properties) {
        languageProperties = properties;

        final String labelClassInfo1;
        final String labelClassInfo2;

        if (properties != null) {
            final String titleWithClassName = getTitle();
            final String titleText = properties.getProperty("Vioc_DialogTitle", Constants.VIOC_DIALOG_TITLE);
            setTitle(titleText + " " + titleWithClassName);

            chcbIncludeInheritedValues.setText(properties.getProperty("Vioc_ChcbIncludeInheritedValues",
                    Constants.VIOC_CHCB_INCLUDE_INHERITED_VALUES));
            chcbIncludeInnerClasses.setText(properties.getProperty("Vioc_ChcbIncludeInnerClassesValues",
                    Constants.VIOC_CHCB_INCLUDE_INNER_CLASSES_VALUES));

            chcbWrapText.setText(properties.getProperty("Vioc_ChcbWrapValuesInJLists",
                    Constants.VIOC_CHCB_WRAP_VALUES_IN_JLISTS));

            chcbIncludeInterfaceMethods.setText(properties.getProperty("Vioc_ChcbIncludeInterfaceMethods",
                    Constants.VIOC_CHCB_INCLUDE_INTERFACE_METHODS));

            labelClassInfo1 = properties.getProperty("Vioc_LabelClassInfo_1", Constants.VIOC_LBL_CLASS_INFO_1);
            labelClassInfo2 = properties.getProperty("Vioc_LabelClassInfo_2", Constants.VIOC_LBL_CLASS_INFO_2);

            btnClose.setText(properties.getProperty("Vioc_ButtonClose", Constants.VIOC_BTN_CLOSE));
            btnSearchConstructor.setText(properties.getProperty("Vioc_ButtonSearchConstructor",
                    Constants.VIOC_BTN_SEARCH_CONSTRUCTOR));
            btnCallConstructor.setText(properties.getProperty("Vioc_ButtonCallConstructor",
                    Constants.VIOC_BTN_CALL_CONSTRUCTOR));
            btnSearchMethod.setText(properties.getProperty("Vioc_ButtonSearchMethod",
                    Constants.VIOC_BTN_SEARCH_METHOD));
            btnSearchVariable.setText(properties.getProperty("Vioc_ButtonsSearchVariable",
                    Constants.VIOC_BTN_SEARCH_VARIABLE));
            btnCreateReference.setText(properties.getProperty("Vioc_ButtonsCreateReference",
                    Constants.VIOC_BTN_CREATE_REFERENCE));
            btnCallMethod.setText(properties.getProperty("Vioc_ButtonsCallMethod", Constants.VIOC_BTN_CALL_METHOD));
            txtSuccess = properties.getProperty("Vioc_TxtSuccess", Constants.VIOC_TXT_SUCCESS);

            pnlConstructors.setBorder(BorderFactory.createTitledBorder(properties.getProperty(
                    "Vioc_PanelConstructorsBorderTitle", Constants.VIOC_PNL_CONSTUCTORS_BORDER_TITLE)));
            pnlMethods.setBorder(BorderFactory.createTitledBorder(properties.getProperty(
                    "Vioc_PanelMethodsBorderTitle", Constants.VIOC_PNL_METHODS_BORDER_TITLE)));
            pnlVariables.setBorder(BorderFactory.createTitledBorder(properties.getProperty(
                    "Vioc_PanelVariablesBorderTitle", Constants.VIOC_PNL_VARIABLES_BORDER_TITLE)));


            constructor = properties.getProperty("Vioc_TextConstructor", Constants.VIOC_CONSTRUCTOR_TEXT);
            method = properties.getProperty("Vioc_TextMethod", Constants.VIOC_METHOD_TEXT);
            variableTitle = properties.getProperty("Vioc_TextVariable", Constants.VIOC_VARIABLE_TEXT);

            txtFailedToCreateInstanceText = properties.getProperty("Ppm_Txt_FailedToCreateInstanceText",
                    Constants.PPM_TXT_FAILED_TO_CREATE_INSTANCE_TEXT);
            txtFailedToCreateInstanceTitle = properties.getProperty("Ppm_Txt_FailedToCreateInstanceTitle",
                    Constants.PPM_TXT_FAILED_TO_CREATE_INSTANCE_TITLE);
        }

        else {
            final String titleWithClassName = getTitle();
            final String titleText = Constants.VIOC_DIALOG_TITLE;
            setTitle(titleText + " " + titleWithClassName);

            chcbIncludeInheritedValues.setText(Constants.VIOC_CHCB_INCLUDE_INHERITED_VALUES);
            chcbIncludeInnerClasses.setText(Constants.VIOC_CHCB_INCLUDE_INNER_CLASSES_VALUES);

            chcbWrapText.setText(Constants.VIOC_CHCB_WRAP_VALUES_IN_JLISTS);

            chcbIncludeInterfaceMethods.setText(Constants.VIOC_CHCB_INCLUDE_INTERFACE_METHODS);

            labelClassInfo1 = Constants.VIOC_LBL_CLASS_INFO_1;
            labelClassInfo2 = Constants.VIOC_LBL_CLASS_INFO_2;

            btnClose.setText(Constants.VIOC_BTN_CLOSE);
            btnSearchConstructor.setText(Constants.VIOC_BTN_SEARCH_CONSTRUCTOR);
            btnCallConstructor.setText(Constants.VIOC_BTN_CALL_CONSTRUCTOR);
            btnSearchMethod.setText(Constants.VIOC_BTN_SEARCH_METHOD);
            btnSearchVariable.setText(Constants.VIOC_BTN_SEARCH_VARIABLE);
            btnCreateReference.setText(Constants.VIOC_BTN_CREATE_REFERENCE);
            btnCallMethod.setText(Constants.VIOC_BTN_CALL_METHOD);
            txtSuccess = Constants.VIOC_TXT_SUCCESS;

            pnlConstructors.setBorder(BorderFactory.createTitledBorder(Constants.VIOC_PNL_CONSTUCTORS_BORDER_TITLE));
            pnlMethods.setBorder(BorderFactory.createTitledBorder(Constants.VIOC_PNL_METHODS_BORDER_TITLE));
            pnlVariables.setBorder(BorderFactory.createTitledBorder(Constants.VIOC_PNL_VARIABLES_BORDER_TITLE));


            constructor = Constants.VIOC_CONSTRUCTOR_TEXT;
            method = Constants.VIOC_METHOD_TEXT;
            variableTitle = Constants.VIOC_VARIABLE_TEXT;

            txtFailedToCreateInstanceText = Constants.PPM_TXT_FAILED_TO_CREATE_INSTANCE_TEXT;
            txtFailedToCreateInstanceTitle = Constants.PPM_TXT_FAILED_TO_CREATE_INSTANCE_TITLE;
        }

        textForLblClassInfo =
                labelClassInfo1 + ": '" + txtClassName + "' " + labelClassInfo2 + ": '" + txtInstanceVariable + "'.";
        lblClassInfo.setText(getWrappedText(textForLblClassInfo, getWidth()));
    }


    /**
     * Vytvoření "zalamovacího" textu (String).
     * <p>
     * bude se zalamovat text v parametru metody a text bude zarovnán na střed.
     *
     * @param text
     *         - text, jaký má obsahovat
     * @param width
     *         - šířka u jaké s má zalamovat
     *
     * @return text s html syntaxí pro zalamování a nastavení zarování na střed s definovaným textem
     */
    private static String getWrappedText(final String text, final int width) {
        return String.format("<html><div WIDTH=%d style=\"text-align:center\">%s</div><html>", width - 27, text);
    }


    /**
     * Metoda, která vytvoří instanci dialogu pro vyhledávání klíčových slov buď metod, konstruktorů nebo roměnných
     *
     * @param listWithObjects
     *         - Kolekce nalezených objektů ve třídě - buď se jedná o kolekce konstukrotů nebo metod nebo proměnných
     * @param parameterForSearchDialog
     *         - text, který se dále předá, s tím, že se v tom bude nacházet bud text konstukro, metoda nebo proměnna je
     *         to jako doplnek pro upřesnění vyhledávání v daném dialogu
     */
    private void createSearchDialog(final Set<?> listWithObjects, final String parameterForSearchDialog) {
        final SearchForm sf = new SearchForm(listWithObjects, parameterForSearchDialog);

        sf.setLanguage(languageProperties);
        sf.setVisible(true);
    }
}