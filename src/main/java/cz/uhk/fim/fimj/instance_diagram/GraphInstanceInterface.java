package cz.uhk.fim.fimj.instance_diagram;

/**
 * Toto rozhraní obsahuje "hlavní" metody potřebné pro diagram instancí, tj. vytvoření instance, přidání hran, apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface GraphInstanceInterface {

    /**
     * Metoda, která do grafu přidá buňku reprezentující instanci třídy
     *
     * @param classname
     *         - název třídy, ze které se vytvořila instance, kterou bud etato vytvořená buňka reprenetovat
     * @param packageName
     *         - balíčky, ve které se třída nachází
     * @param name
     *         - název reference na instanci třídy
     */
    void addInstanceCell(final String classname, final String packageName, final String name);


    /**
     * Metoda, která vytvoří hranu mezi buňkami srcCell a desCell, která bude v daigramu instancí reprezentovat vztah
     * asociace mezi příslušnými instancemi.
     *
     * @param srcCell
     *         - zdrojová buňka pro hranu
     * @param desCell
     *         - cílková buňka pro hranu
     * @param srcSide
     *         - text, který bude zobrazen u zdroje hrany, resp. začátku hrany, jedná se o název proměnné
     * @param desSide
     *         - text, který bude zobrazen na konci hrany, resp. u konce hrany, jedná se také o název nějaké proměné,
     *         která se naplnila a může se tak vytvořit příslušná hrana pro reprezentaci daného vztahu
     */
    void addAssociationEdge(final Object srcCell, final Object desCell, final String srcSide, final String desSide);


    /**
     * Metoda, která vytvoří hranu mezi buňkami srcCell a desCell, která reprezentuje agregaci 1 : 1 nebo 1 : N coby
     * vztah mezi příslušnými instancemi.
     *
     * @param srcCell
     *         - zdrojový buňka - zdroj hrany
     * @param desCell
     *         - cílová buňka - cíl hrany
     * @param desSide
     *         - popisek, který se bude nacházet na konci hrany, resp. proměnná, která se naplnila příslušnou instancí
     */
    void addAggregationEdge(final Object srcCell, final Object desCell, final String desSide);
}