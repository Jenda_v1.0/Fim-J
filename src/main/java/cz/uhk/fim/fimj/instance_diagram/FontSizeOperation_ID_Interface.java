package cz.uhk.fim.fimj.instance_diagram;

/**
 * Toto rozhraní deklaruje metody, které slouží pro nastavení velikosti písma objektům v diagramu instancí pomocí
 * dialogu pro "rychlé" nastavení velikosti písma dle potřeby.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface FontSizeOperation_ID_Interface {

    /**
     * Metoda, která slouží pro "rychlé" nastavení velikosti písma / fontu v nastavených objektech v diagramu instancí.
     * <p>
     * Metoda vždy nastaví příslušným objektům v diagramu instancí (které se tam aktuálně nacházejí) velikost fontu /
     * písma na size tak, že se projdou všechny objekty a každému se nastaví příslušná velikost, pak se tato nová
     * velikost size nastaví do objektu Properties, který je držen v aplikaci pro zjišťování typů objektů v diagramu
     * instancí a do objektu Properties, který je držen pro vytváření komponent.
     * <p>
     * Note: Šel by využít jen jeden objektu Propeties a ten pak předávat do metod pro zjišˇˇtování vztahů, ale původně
     * jsem měl vždy aktuální načítání ze souboru a pak jsem to takto ponechal na držení v aplkaci a pohou úpravu
     * hodnot, tak jsem to tak i nadále nechal, kdybych se rozhodl ještě někdy v budoucnu jinak.
     *
     * @param size
     *         - nová velikost písma / fontu pro příslušné objekty v diagramu instancí.
     * @param instanceDiagram
     *         - reference na diagram instancí.
     */
    void quickSetupOfFontSize(final int size, final GraphInstance instanceDiagram);


    /**
     * Metoda, která slouží pro nastavení výchozích velikostí písem pro veškeré objekty v diagramu instancí
     *
     * @param instanceDiagram
     *         - reference na diagram instancí.
     */
    void quickSetupDefaultFontSizes(final GraphInstance instanceDiagram);


    /**
     * Metoda, která slouží pro "rychlé" nastavení velikosti písma / fontu pro vybrané objekty v diagramu instancí.
     * Velikost písma / fontu se nastaví pouze těm objektům v diagramu instancí, které jsou označeny, resp. u kterých
     * bude proměnná is... true, jinak se s ní nic nedělá.
     *
     * @param size
     *         - nová velikost pro vybrané objekty.
     * @param isInstance
     *         - logická proměnná, která značí, zda se má nastavit nová velikost písma pro buňku reprezentující
     *         instanci
     *         v diagramu instancí.
     * @param isInstanceAttributes
     *         - logická proměnná, která značí, zda se nastavit nová velikost písma pro atributy, které mohou být
     *         vypsány v buňce reprezentující instanci v diagramu instancí.
     * @param isInstanceMethods
     *         - logická proměnná, která značí, zda se nastavit nová velikost písma pro metody, které mohou být vypsány
     *         v buňce reprezentující instanci v diagramu instancí.
     * @param isAssociation-
     *         logická proměnná, která značí, zda se má nastavit nová velikost písma pro hranu reprezentující vztah
     *                        typu
     *         asociace v diagramu instancí.
     * @param isAggregation-
     *         logická proměnná, která značí, zda se má nastavit nová velikost písma pro hranu reprezentující vztah
     *                        typu
     *         agregace v diagramu instancí.
     * @param instanceDiagram
     *         - reference na diagram instancí.
     */
    void quickSetupOfFontSize(final int size, final boolean isInstance, final boolean isInstanceAttributes,
                              final boolean isInstanceMethods, final boolean isAssociation, final boolean isAggregation,
                              final GraphInstance instanceDiagram);


    /**
     * Metoda, která slouží pro "rychlé" nastavení velikosti písma / fontu pro buňku, která v diagramu instancí
     * reprezentuje instancí.
     *
     * @param size
     *         - nová velikost písma pro buňku, která v diagramu instnací reprezentuje instanci.
     * @param instanceDiagram
     *         - reference na diagram instancí.
     */
    void quickSetupOfInstanceFontSize(final int size, final GraphInstance instanceDiagram);


    /**
     * Metoda, která slouží pro "rychlé" nastavení velikosti písma / fontu pro atributy, které mohou být vykreslovány v
     * buňce, která v diagramu instancí reprezentuje instanci.
     *
     * @param size
     *         - nová velikost písma pro atributy v buňce, která v diagramu instanci reprezentuje instanci.
     * @param instanceDiagram
     *         - reference na diagram instancí
     */
    void quickSetupOfAttributesFontSizeInInstance(final int size, final GraphInstance instanceDiagram);


    /**
     * Metoda, která slouží pro "rychlé" nastavení velikosti písma / fontu pro metody, které mohou být vykreslovány v
     * buňce, která v diagramu instancí reprezentuje instanci.
     *
     * @param size
     *         - nová velikost písma pro metody v buňce, která v diagramu instanci reprezentuje instanci.
     * @param instanceDiagram
     *         - reference na diagram instancí
     */
    void quickSetupOfMethodsFontSizeInInstance(final int size, final GraphInstance instanceDiagram);


    /**
     * Metoda, která slouží pro "rychlé" nastavení velikosti písma / fontu pro hranu, která v diagramu instnací
     * reprezentuje vztah typu asociace.
     *
     * @param size
     *         - velikost písma / fontu pro hranu, která v diagramu instancí reprezentuje asociace.
     * @param instanceDiagram
     *         - reference na diagram instancí.
     */
    void quickSetupOfAssociationFontSize(final int size, final GraphInstance instanceDiagram);


    /**
     * Metoda,která slouží pro "rychlé" nastavení velikosti písma / fontu pro hranu, která v diagramu instancí
     * reprezentuje vztah typu agregace.
     *
     * @param size
     *         - nová velikosti písma / fontu pro hranu, která v diagramu instancí reprezentuje vztah typu agregace.
     * @param instanceDiagram
     *         - reference na diagram instancí.
     */
    void quickSetupOfAgregationFontSize(final int size, final GraphInstance instanceDiagram);
}
