package cz.uhk.fim.fimj.code_editor_abstract;

import com.github.abrarsyed.jastyle.ASFormatter;
import com.github.abrarsyed.jastyle.constants.EnumFormatStyle;
import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.code_editor.generate_access_methods.GenerateAccessMethodHelper;
import cz.uhk.fim.fimj.code_editor.generate_access_methods.Visibility;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.DataTypeOfList;
import cz.uhk.fim.fimj.file.DataTypeOfListEnum;
import cz.uhk.fim.fimj.reflection_support.MapHelper;
import cz.uhk.fim.fimj.reflection_support.ParameterToText;
import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;
import org.fife.ui.autocomplete.*;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;

import javax.swing.text.Document;
import javax.swing.text.Element;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Tato abstraktní třída slouží pouze pro společné metody, vlastně by zde ani nemusela být, ale připsal jsem jí sem,
 * kvůli dalšímu editoru zdrojového kódu v dialogu {@link cz.uhk.fim.fimj.project_explorer.ProjectExplorerDialog}, kde
 * lze otevřít libovolný počet interních oken s editorem kodu, kde lze otevřít několik desítek předdefinovaných typů
 * souborů.
 * <p>
 * Takže tato třída slouží pouze pro společné metocdy, jako jsou textové nápovědy pro doplňování kódu - zkratky po
 * stisknutí kombinace kláves CTRL + Space pak pro zísání a nastavení výchozího nastavení pro tento editor (barva pisma,
 * pozadí, ...)
 * <p>
 * <i>Bylo by dobré, kdyby se přepsaly metody pro generování syntaxí do menu s doplňováním tak, aby vraceli konkrétní
 * hodnoty, například metody nebo proměnné apod. Z nich by se přes jinou metodu nebo rozhraní sestavovaly syntaxe do
 * toho okna, kde by je viděl uživatel a z tyto syntaxe by se posíaly do metody, která by je přidávala do okna s
 * nápovědou. Nyní je to již bohužel trochu nepřehledné (ne moc vhodný začátek). Bylo by možé využít podobny princip,
 * který je aplikován pro vytvoáření nápovedy pro editor příkazů v balíčku: cz.uhk.fim.fimj.commands_editor
 * .values_completion_window.
 * <p>
 * Dále by bylo vhodné psát metody pro získávání metod, proměnných apod. tak, že dostane například třídu a z té se
 * získají například všechny proměnné i z vnitřních tříd a z těch pak jen filtrovat potřebné hodnoty.</i>
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class CodeEditorAbstract extends RSyntaxTextArea {

    private static final long serialVersionUID = 4593819500336603924L;


    // Texty do popisků hodnot u cyklů v okně pro doplňování kódu:
    private static final String TXT_FOREACH = "forEach";
    private static final String TXT_ITERATOR_FOREACH = "Iterator forEach";
    private static final String TXT_ITERATOR_WHILE = "Iterator while";
    private static final String TXT_ITERATOR_DO_WHILE = "Iterator do - while";
    private static final String TXT_FOREACH_LAMBDA = "forEach lambda";


    /**
     * Proměnná, která slouží pro uložení přípony souboru, který se má otevřít v
     * tomto konkrétním editoru zdrojového kódu, je to zde potřeba jen kvůli tomu,
     * že se má nastavit zvýrazňování syntaxe dle typu - přípony otevřeného souboru,
     * tak abych mohl nastavit ten typ zvýrazňování, potřebuji znát typ souboru.
     *
     * Syntaxe: '.extension'
     */
    protected final String extensionOfOpenFile;


    /**
     * Proměnná, dle které se pozná, zda se má přidat do editoru kódu posluchač na
     * stisknutí klaves pro strednik nebo zaviraci hranata a spicata zavorka pokud
     * se stisknou spusti se vlakno, ... viz to vlakno
     */
    protected boolean compiledClassInBackground;




    // Proměnné pro cykly:
    private static String LOOP_FOR, LOOP_WHILE, LOOP_DO_WHILE, LOOP_FOREACH, LOOP_INT_STREAM, INFINITE_WHILE_LOOP,
            LOOP_USING_LAMBDA, PREPARED_LOOP_FOR;


    // Proměnné pro posluchače:
    private static String COMPONENT_LISTENER, COMPONENT_ADAPTER, FOCUS_ADAPTER, FOCUS_LISTENER, KEY_ADAPTER,
            KEY_LISTENER, MOUSE_ADAPTER, MOUSE_LISTENER, MOUSE_MOTION_ADAPTER, MOUSE_MOTION_LISTENER,
            MOUSE_WHEEL_LISTENER, HIERARCHY_LISTENER, HIERARCHY_BOUNDS_ADAPTER, HIERARCHY_BOUNDS_LISTENER,
            ACTION_LISTENER;


    // Proměnné pro texty do komentářu v okně s doplňováním klíčových slov:
    private static String loopForSummary, loopForPreparedSummary, loopWhileSummary, loopDoWhileSummary, loopForEach,
            loopUsingLambda, loopUsingIntStream, loopInfiniteWhile, putYourCode, loopForInfo, loopWhileInfo,
            loopDoWhileInfo, loopForEachInfo, loopWithLambdaInfo, loopWithIntStreamInfo, loopInfiniteWhileInfo;


    // Proměnné pro texty pro vygenerování cyklů pro konkrétní proměnné:
    private static String txtWithVariable, txtWithoutVariable;


    /**
     * Text pro vygenerování přístupové metody - getru.
     */
    private static String generateGetter;
    /**
     * Text pro vygenerování přístupové metody - setru.
     */
    private static String generateSetter;


    /**
     * Objekt pro formátování zdrojového kódu. Tato proměnná bude naplněna pouze v případě, že se jedná o instanci pro
     * editor v dialogu editor zdrojového kódu nebo editor v interních oknech v dialogu průzkumník projektů. V ostatních
     * případech není tato proměnná potřeba.
     */
    private ASFormatter formatter;



    /**
     * Konstruktor této třídy (puze pro naplnění proměnné).
     *
     * @param extensionOfOpenFile
     *         - přípona otevřeného souboru, je zde potřeba, aby se dále mohlo otestovat, jaká syntaxe pro zvýraznění
     *         textu v editoru se má využít, například syntaxe pro zvýraznění syntaxe jazyka Java, C, C++ apod.
     */
    public CodeEditorAbstract(final String extensionOfOpenFile) {
        this.extensionOfOpenFile = extensionOfOpenFile;
    }


    /**
     * Nastavení, zda se má zalamovat text, který je zobrazen v editoru. Tzn. že se budou zalamovat dlouhé text tak, aby
     * nešli "do stran", aby uživatel nemusel používat posuvník do stran.
     *
     * @param wrap
     *         - true, pokud se mají zalamovat text, jinak false.
     */
    public void wrapTextInEditor(final boolean wrap) {
        setWrapStyleWord(wrap);
        setLineWrap(wrap);
    }


    /**
     * Nastavení zobrazení bílých znaků.
     *
     * @param show
     *         - true v případě, že se mají zobrazovat bílé znaky v editoru, jinak false.
     */
    public void showWhiteSpace(final boolean show) {
        setWhitespaceVisible(show);
    }


    /**
     * Nastavení "čištění" bílých znaků na volných řádcích.
     *
     * <i>Při vložení nového řádku (klávesa Enter) se odeberou bílé znaky vždy na předchozím řádku.</i>
     *
     * @param clearWhiteSpaceLine
     *         - true v případě, že se mají čistit bílé znaky, jinak false.
     */
    public void setClearWhiteSpaceLine(final boolean clearWhiteSpaceLine) {
        setClearWhitespaceLinesEnabled(clearWhiteSpaceLine);
    }






    /**
     * Metoda, která vytvoří a nastaví to tzv. "autodoplňování", resp. to, že se po stisknutí kombinace kláves otevře to
     * dialogové okno se skratkami pro doplnění kódu
     *
     * @param provider
     *         - v podstatě klíčová slova, která se předají do toho okna s doplňováním, ze kterého si bude uživatel
     *         vybírat
     */
    private void createAutoCompletion(final CompletionProvider provider) {
        final AutoCompletion ac = new AutoCompletion(provider);

        ac.setAutoCompleteEnabled(true);

        // Zobrazí se popis u slova z nápovědy
        ac.setShowDescWindow(true);

        ac.setAutoActivationEnabled(true);

        ac.setAutoCompleteSingleChoices(true);

        ac.install(this);
    }








    /**
     * Metoda, která porovná zdrojové kódy z editoru kódu - z dialogu se zdrojovým
     * kódem v parametru této metody
     *
     * @param sourceCodeList
     *            - zdrojový kó, který semá porovnat se zdrojovým kódem v editoru
     *            kódu - v dialogu tento parametr je většinou zdrojový kód načtený z
     *            původní třídy
     *
     * @return true, pkud jsou oba kódy stejné, jinak false
     */
    public final boolean compareSourceCode(final List<String> sourceCodeList) {
        return Arrays.equals(getTextFromCodeEditor().toArray(), sourceCodeList.toArray());
    }










    /**
     * Metoda, která vrátí text z editoru kódu v podobě kolekce - jedna hodnota v
     * kolekce = jeden řádek v editoru kódu
     *
     * Je to takto udělané, protože pro čtení zdrojového kódu třídy čtu její text po
     * řádcích a abych je mohl zkoontrolovat, zda jsou stejně, tak je potřebuji
     * převést do kolekce dále je to také proto, že když chci uložit text (zdrojový
     * kód) do třídy, tak ho musím také nejprve vložit do kolekce a tu předám do
     * metody, která jej zapíše zpět do třídy
     *
     * @return kolekci se zdrojovým kódem z editoru kódu - z dialogu nebo null
     */
    public final List<String> getTextFromCodeEditor() {
        final String[] linesFromEditor = getText().split("\n");

        return Arrays.asList(linesFromEditor);
    }


    /**
     * Metoda, která vloží text v parametru do editoru zdrojového kódu - v tomto dialogu.
     *
     * @param codeToInsertToCodeEditor
     *         - text, resp. kód, který se má vložit do "tohoto" editoru kódu.
     */
    public final void setTextToCodeEditor(final List<String> codeToInsertToCodeEditor) {
        // index aktuálního řádku:
        final int currentLine = getCaretLineNumber();

        // Nejprve se vymaže veškerý text v editoru a pak se do něj vloží ten v codeToInsertToCodeEditor:
        setText(null);

        for (int i = 0; i < codeToInsertToCodeEditor.size(); i++) {
            append(codeToInsertToCodeEditor.get(i));

            /*
             * Dokud se nejedná o poslední položku, bude se přidávat nový řádek, ale v případě, že se jedná o
             * poslední položku (/ řádek), už se na konec nebude přidávat nový řádek, protože by se neshodovaly (/
             * nemusely by) texty v originálním souboru. S výjimkou bílých znaků. Například odentrování nového
             * pázdného řádku, se nemusí jako poslední řádek načíst - může být null.
             */
            if (i != codeToInsertToCodeEditor.size() - 1)
                append("\n");
        }

        /*
         * Výše se nastavil text do editoru. Zde se zjistí, jestli ještě existuje, resp. je počet řádků stejný, který
         * se uložil před nastavením textu. Pokud ano, nastaví se kurzor na začátek stejného řádku. Pokud ne,
         * nastaví se pozice kurzoru na nulu, tedy na začátek dokumentu.
         *
         * Note:
         *
         * V případě, že se jedná o vložení textu při otevření editoru se nastaví kurzor vždy na začátek dokumentu,
         * ale v případě, že se kód zformátuje, mohou se nějaké řádky odebrat, přidat apod. Proto kurzor nemusí vždy
         * být na stejném řádku, na kterém byl před zformátováním kódu. Resp. Kurzor si bude držet správný index
         * řádku, ale pro uživatele to může být jinak, když se nějaké řádky odeberou nebo přidají, ale záleží, v jaké
         * části se odeberou, když se bude pod umístním kurzoru, tak je to OK, jinak se pozic změní.
         */
        if (currentLine <= getLineCount()) {
            final Document document = getDocument();
            final Element rootElem = document.getDefaultRootElement();

            // Získám si řádek z editoru:
            final Element lineElem = rootElem.getElement(currentLine);

            // Index začátku řádku:
            final int lineStart = lineElem.getStartOffset();

            setCaretPosition(lineStart);
        } else setCaretPosition(0);
    }














    /**
     * Metoda, která této třídě - editoru přidá reakci na myš konkrétně na rolování
     * kolečkem myši, pokud bude stisknuta klávesa CTRL pak se bude zvětšovat /
     * změnšovat písmo editoru
     */
    protected final void addMyMouseWheelListener() {
        addMouseWheelListener(e -> {

            if (e.getWheelRotation() < 0 && e.isControlDown()) { // Rotace kolečkem myši nahoru - zvětšování písma
                float fontSize = getFont().getSize();

                if (fontSize < 100)
                    setFont(getFont().deriveFont(++fontSize));
            }

            else if (e.getWheelRotation() > 0 && e.isControlDown()) { // Rotování kolečkem myši dolů - zmenšování písma
                float fontSize = getFont().getSize();

                if (fontSize > 8)
                    setFont(getFont().deriveFont(--fontSize));
            }
        });
    }














    /**
     * Metoda, která nastaví pro příslušnou instanci editoru zdrojového kódu
     * konkrétní syntaxi pro zvýrazňování syntaxe.
     *
     * @param extension
     *            - přípona otevřeného souboru, dle které se pozná, jaká syntaxe pro
     *            zvýraznění kódu se má využít.
     */
    protected final void setHighlightingCodeByKindOfFile(final String extension) {
        if (extension == null || extension.isEmpty())
            setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);

        else if (extension.equalsIgnoreCase(".java") || extension.equalsIgnoreCase(".class"))
            setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);

            // Jazyk C
        else if (extension.equalsIgnoreCase(".c") || extension.equalsIgnoreCase(".cproject"))
            setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_C);

            // Pro hlavičkové soubory a tridy jazyka C++ (hlavičkové soubory .h jsou i v
            // jazyce C)
        else if (extension.equalsIgnoreCase(".h") || extension.equalsIgnoreCase(".cpp"))
            setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_CPLUSPLUS);

            // Pro C#:
        else if (extension.equalsIgnoreCase(".cs"))
            setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_CSHARP);

            // kaskádové styly
        else if (extension.equalsIgnoreCase(".css"))
            setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_CSS);

            // cshtml je pro C#
        else if (extension.equalsIgnoreCase(".html") || extension.equalsIgnoreCase(".cshtml")
                || extension.equalsIgnoreCase(".xhtml") || extension.equalsIgnoreCase(".htm"))
            setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_HTML);

        else if (extension.equalsIgnoreCase(".json"))
            setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JSON);

        else if (extension.equalsIgnoreCase(".xml"))
            setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_XML);

        else if (extension.equalsIgnoreCase(".js"))
            setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT);

        else if (extension.equalsIgnoreCase(".jsp"))
            setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JSP);

        else if (extension.equalsIgnoreCase(".properties"))
            setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PROPERTIES_FILE);

        else if (extension.equalsIgnoreCase(".php"))
            setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PHP);

        else if (extension.equalsIgnoreCase(".groovy"))
            setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_GROOVY);

        else if (extension.equalsIgnoreCase(".scala"))
            setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_SCALA);

        else if (extension.equalsIgnoreCase(".py") || extension.equalsIgnoreCase(".pyc")
                || extension.equalsIgnoreCase(".pyo") || extension.equalsIgnoreCase(".pyd"))
            setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PYTHON);

        else if (extension.equalsIgnoreCase(".rb"))
            setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_RUBY);

            // Výchozí syntaxe bude vždy jazyk Java:
        else
            setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
    }


    /**
     * Vytvoření objektu pro zformátování zdrojového kódu dle nastavencýh vlastností.
     *
     * @param properties
     *         - objekt, ze kterého se získá nastavení pro formátování zdrojového kódu. Pokud bude null, využíje se
     *         výchozí nastavení.
     */
    public void createFormatter(final Properties properties) {
        final boolean deleteEmptyLines;
        final boolean tabSpaceConversion;
        final boolean breakElseIf;
        final boolean singleStatement;
        final boolean breakOneLineBlocks;
        final boolean breakClosingHeaderBrackets;
        final boolean breakBlocks;
        final boolean operatorPaddingMode;

        if (properties != null) {
            deleteEmptyLines = Boolean.parseBoolean(properties.getProperty("DeleteEmptyLines",
                    String.valueOf(Constants.SCF_DELETE_EMPTY_LINES)));
            tabSpaceConversion = Boolean.parseBoolean(properties.getProperty("TabSpaceConversion",
                    String.valueOf(Constants.SCF_TAB_SPACE_CONVERSION)));
            breakElseIf = Boolean.parseBoolean(properties.getProperty("BreakElseIf",
                    String.valueOf(Constants.SCF_BREAK_ELSE_IF)));
            singleStatement = Boolean.parseBoolean(properties.getProperty("SingleStatement",
                    String.valueOf(Constants.SCF_SINGLE_STATEMENT)));
            breakOneLineBlocks = Boolean.parseBoolean(properties.getProperty("BreakOneLineBlock",
                    String.valueOf(Constants.SCF_BREAK_ONE_LINE_BLOCK)));
            breakClosingHeaderBrackets = Boolean.parseBoolean(properties.getProperty("BreakClosingHeaderBrackets",
                    String.valueOf(Constants.SCF_BREAK_CLOSING_HEADER_BRACKETS)));
            breakBlocks = Boolean.parseBoolean(properties.getProperty("BreakBlocks",
                    String.valueOf(Constants.SCF_BREAK_BLOCKS)));
            operatorPaddingMode = Boolean.parseBoolean(properties.getProperty("OperatorPadding",
                    String.valueOf(Constants.SCF_OPERATOR_PADDING_MODE)));
        } else {
            deleteEmptyLines = Constants.SCF_DELETE_EMPTY_LINES;
            tabSpaceConversion = Constants.SCF_TAB_SPACE_CONVERSION;
            breakElseIf = Constants.SCF_BREAK_ELSE_IF;
            singleStatement = Constants.SCF_SINGLE_STATEMENT;
            breakOneLineBlocks = Constants.SCF_BREAK_ONE_LINE_BLOCK;
            breakClosingHeaderBrackets = Constants.SCF_BREAK_CLOSING_HEADER_BRACKETS;
            breakBlocks = Constants.SCF_BREAK_BLOCKS;
            operatorPaddingMode = Constants.SCF_OPERATOR_PADDING_MODE;
        }

        formatter = new ASFormatter();
        formatter.setFormattingStyle(EnumFormatStyle.JAVA);

        /*
         * Info k metodám:
         *
         * https://appdoc.app/artifact/com.github.abrarsyed.jastyle/jAstyle/0
         * .8/com/github/abrarsyed/jastyle/ASFormatter.html#setBracketFormatMode(com.github.abrarsyed.jastyle
         * .constants.EnumBracketMode)
         */
        formatter.setDeleteEmptyLinesMode(deleteEmptyLines);
        formatter.setTabSpaceConversionMode(tabSpaceConversion);
        // Note: Toto ne vždy funguje, nejspíše vyžaduje kombinaci dalšího nastavení:
        formatter.setBreakElseIfsMode(breakElseIf);
        formatter.setSingleStatementsMode(singleStatement);
        formatter.setBreakOneLineBlocksMode(breakOneLineBlocks);
        formatter.setBreakClosingHeaderBracketsMode(breakClosingHeaderBrackets);
        formatter.setBreakBlocksMode(breakBlocks);
        formatter.setOperatorPaddingMode(operatorPaddingMode);
    }


    /**
     * Getr na objekt formatter, který slouží pro zformátování zdrojového kódu dle nastavených pravidel.
     *
     * @return - formatter s nastavenými vlastnosti pro formátování zdrojového kódu.
     */
    public ASFormatter getFormatter() {
        return formatter;
    }



    /**
     * Metoda, která do proměnné typu provide naplní výchozí mnou definované texty
     * coby nějaké hodnoty, které půjdou doplnit pomocí kombinace kláves CTRL +
     * Space, po sitknutí těchto kláves se zobrazí okno, kde budou zobrazeny
     * informace s těmito hodnotami
     *
     * @param provider
     *            - v podstatě okno, kde budou uvedeny texty níže v metodě
     */
    private static void createShortcuts(final DefaultCompletionProvider provider) {
        // Texty do automatického doplňování:

        // Metody:
        addCompletion(provider, "clone()", "clone()", "Object");
        addCompletion(provider, "getClass()", "getClass()", "Class<?>");
        addCompletion(provider, "notify()", "notify()", "void");
        addCompletion(provider, "notifyAll()", "notifyAll()", "void");
        addCompletion(provider, "wait()", "wait()", "void");
        addCompletion(provider, "wait(long timeout)", "wait(timeout)", "void");
        addCompletion(provider, "wait(long timeout, int nanos)", "wait(timeout, nanos)", "void");




        // Slovička (U nic neni třeba nic dodávat - pouze text v menu, a text, ktery se vlozi do editoru po potvrzeni):
        addCompletion(provider, "abstract");
        addCompletion(provider, "assert");
        addCompletion(provider, "break");
        addCompletion(provider, "case");
        addCompletion(provider, "char");
        addCompletion(provider, "class");
        addCompletion(provider, "continue");
        addCompletion(provider, "default");
        addCompletion(provider, "do");
        addCompletion(provider, "double");
        addCompletion(provider, "else");
        addCompletion(provider, "extends");
        addCompletion(provider, "enum");
        addCompletion(provider, "true");
        addCompletion(provider, "false");
        addCompletion(provider, "final");
        addCompletion(provider, "finally");
        addCompletion(provider, "float");
        addCompletion(provider, "for");
        addCompletion(provider, "if");
        addCompletion(provider, "implements");
        addCompletion(provider, "import");
        addCompletion(provider, "instanceof");
        addCompletion(provider, "long");
        addCompletion(provider, "Native", "Native", "java.lang.annotation");
        addCompletion(provider, "native");
        addCompletion(provider, "new");
        addCompletion(provider, "null");
        addCompletion(provider, "package");
        addCompletion(provider, "return");
        addCompletion(provider, "short");
        addCompletion(provider, "static");
        addCompletion(provider, "super");
        addCompletion(provider, "super();");
        addCompletion(provider, "switch");
        addCompletion(provider, "synchronized");
        addCompletion(provider, "this");
        addCompletion(provider, "throw");
        addCompletion(provider, "throws");
        addCompletion(provider, "byte");
        addCompletion(provider, "catch");
        addCompletion(provider, "transient");
        addCompletion(provider, "try");
        addCompletion(provider, "void");
        addCompletion(provider, "volatile");
        addCompletion(provider, "while");
        addCompletion(provider, "public");
        addCompletion(provider, "private");
        addCompletion(provider, "protected");
        addCompletion(provider, "int");
        addCompletion(provider, "boolean");

        addCompletion(provider, "System");
        addCompletion(provider, "err");
        addCompletion(provider, "in");
        addCompletion(provider, "out");

        // metody pro výpis - pomocí metody System.out nebo System.err:
        addCompletion(provider, "doubles()", "doubles()", "DoubleStream", "Returns an effectively unlimited stream of pseudorandom double values, each between zero (inclusive) and one (exclusive).");
        addCompletion(provider, "print(boolean b)", "print(b)", "void", "Prints a boolean value.");
        addCompletion(provider, "print(char c)", "print(c) ", "void", "Prints a character.");
        addCompletion(provider, "print(char[] s)", "print(s)", "void", "Prints an array of characters.");
        addCompletion(provider, "print(double d)", "print(d)", "void", "Prints a double-precision floating-point number.");
        addCompletion(provider, "print(float f)", "print(f)", "void", "Prints a floating-point number.");
        addCompletion(provider, "print(int i)", "print(i)", "void", "Prints an integer.");
        addCompletion(provider, "print(long l)", "print(l)", "void", "Prints a long integer.");
        addCompletion(provider, "print(Object obj)", "print(obj)", "void", "Prints an object.");
        addCompletion(provider, "print(String s)", "print(s)", "void", "Prints a string.");
        addCompletion(provider, "printf(Locale l, String format, Object... args)", "printf(l, format, args);", "PrintStream", "A convenience method to write a formatted string to this output stream using the specified format string and arguments.");
        addCompletion(provider, "printf(String format, Object... args)", "printf(format, args);", "PrintStream", "A convenience method to write a formatted string to this output stream using the specified format string and arguments.");
        addCompletion(provider, "println()", "println()", "void", "Terminates the current line by writing the line separator string.");
        addCompletion(provider, "println(boolean x)", "println(x)", "void", "Prints a boolean and then terminate the line.");
        addCompletion(provider, "println(char x)", "println(x)", "void", "Prints a character and then terminate the line.");
        addCompletion(provider, "println(char[] x)", "println(x)", "void", "Prints an array of characters and then terminate the line.");
        addCompletion(provider, "println(double x)", "println(x)", "void", "Prints a double and then terminate the line.");
        addCompletion(provider, "println(float x)", "println(x)", "void", "Prints a float and then terminate the line.");
        addCompletion(provider, "println(int x)", "println(x)", "void", "Prints an integer and then terminate the line.");
        addCompletion(provider, "println(long x)", "println(x)", "void", "Prints a long and then terminate the line.");
        addCompletion(provider, "println(Object x", "println(x)", "void", "Prints an Object and then terminate the line.");
        addCompletion(provider, "println(String x)", "println(x)", "void", "Prints a String and then terminate the line.");
        addCompletion(provider, "setError()", "setError()", "protected void", "Sets the error state of the stream to true.");
        addCompletion(provider, "write(byte[] buf, int off, int len)", "write(buf, off, len", "void", "Writes len bytes from the specified byte array starting at offset off to this stream.");
        addCompletion(provider, "write(int b)", "write(b)", "void", "Writes the specified byte to this stream.");
        addCompletion(provider, "format(Locale l, String format, Object... args)", "format(l, format, args)", "PrintStream", "Writes a formatted string to this output stream using the specified format string and arguments.");
        addCompletion(provider, "format(String format, Object... args)", "format(format, args)", "PrintStream", "Writes a formatted string to this output stream using the specified format string and arguments.");

        addCompletion(provider, "sysout", "System.out.println();");
        addCompletion(provider, "syserr", "System.err.println();");


        // Položky, kde uvedu i datové typy:
        addCompletion(provider, "String", "String", "java.lang");
        addCompletion(provider, "Integer", "Integer", "java.lang");
        addCompletion(provider, "Byte", "Byte", "java.lang");
        addCompletion(provider, "Short", "Short", "java.lang");
        addCompletion(provider, "Long", "Long", "java.lang");
        addCompletion(provider, "Float", "Float", "java.lang");
        addCompletion(provider, "Double", "Double", "java.lang");
        addCompletion(provider, "Character", "Character", "java.lang");
        addCompletion(provider, "Boolean", "Boolean", "java.lang");












        // Výchozí anotace:
        addCompletion(provider, "@Retention", "@Retention", "java.lang.annotation");
        addCompletion(provider, "@Documented", "@Documented", "java.lang.annotation");
        addCompletion(provider, "@Target", "@Target", "java.lang.annotation");
        addCompletion(provider, "@Inherited", "@Inherited", "java.lang.annotation");
        addCompletion(provider, "@Repeatable", "@Repeatable", "java.lang.annotation");
        addCompletion(provider, "@Override", "@Override", "java.lang");
        addCompletion(provider, "@Deprecated", "@Deprecated", "java.lang");
        addCompletion(provider, "@SuppressWarnings", "@SuppressWarnings", "java.lang");
        addCompletion(provider, "@SafeVarargs", "@SafeVarargs", "java.lang");
        addCompletion(provider, "@FunctionalInterface", "@FunctionalInterface", "java.lang");











        // Výchozí balíčky, definuji pouze balíčky pro Oracle a Java - pouze ty výchozí, podobné, které umožňuje Eclipse,
        // ale ne přesně - "přísliš moc kopírování a málo času":
        // Balíčky od Oracle:
        addCompletion(provider, "oracle", "oracle.*;");
        addCompletion(provider, "oracle.jrockit", "oracle.jrockit.*;");
        addCompletion(provider, "oracle.jrockit.jfr", "oracle.jrockit.jfr.*;");
        addCompletion(provider, "oracle.jrockit.jfr.events", "oracle.jrockit.jfr.events.*;");
        addCompletion(provider, "oracle.jrockit.jfr.jdkevents", "oracle.jrockit.jfr.jdkevents.*;");
        addCompletion(provider, "oracle.jrockit.jfr.jdkevents.throwabletransform", "oracle.jrockit.jfr.jdkevents.throwabletransform.*;");
        addCompletion(provider, "oracle.jrockit.jfr.openmbean", "oracle.jrockit.jfr.openmbean.*;");
        addCompletion(provider, "oracle.jrockit.jfr.parser", "oracle.jrockit.jfr.parser.*;");
        addCompletion(provider, "oracle.jrockit.jfr.settings.", "oracle.jrockit.jfr.settings.*;");
        addCompletion(provider, "oracle.jrockit.jfr.tools.", "oracle.jrockit.jfr.tools.*;");

        // A balíčky od Javy:
        addCompletion(provider, "java", "java.*;");
        addCompletion(provider, "java.applet", "java.applet.*;");
        addCompletion(provider, "java.awt", "java.awt.*;");
        addCompletion(provider, "java.awt.color", "java.awt.color.*;");
        addCompletion(provider, "java.awt.datatransfer", "java.awt.datatransfer.*;");
        addCompletion(provider, "java.awt.dnd", "java.awt.dnd.*;");
        addCompletion(provider, "java.awt.dnd.peer", "java.awt.dnd.peer.*;");
        addCompletion(provider, "java.awt.event", "java.awt.event.*;");
        addCompletion(provider, "java.awt.font", "java.awt.font.*;");
        addCompletion(provider, "java.awt.geom", "java.awt.geom.*;");
        addCompletion(provider, "java.awt.im", "java.awt.im.*;");
        addCompletion(provider, "java.awt.im.spi", "java.awt.im.spi.*;");
        addCompletion(provider, "java.awt.image", "java.awt.image.*;");
        addCompletion(provider, "java.awt.image.renderable", "java.awt.image.renderable.*;");
        addCompletion(provider, "java.awt.peer", "java.awt.peer.*;");
        addCompletion(provider, "java.awt.print", "java.awt.print.*;");
        addCompletion(provider, "java.beans", "java.beans.*;");
        addCompletion(provider, "java.beans.beancontext", "java.beans.beancontext.*;");
        addCompletion(provider, "java.io", "java.io.*;");
        addCompletion(provider, "java.lang", "java.lang.*;");
        addCompletion(provider, "java.lang.annotation", "java.lang.annotation.*;");
        addCompletion(provider, "java.lang.instrument", "java.lang.instrument.*;");
        addCompletion(provider, "java.lang.invoke", "java.lang.invoke.*;");
        addCompletion(provider, "java.lang.management", "java.lang.management.*;");
        addCompletion(provider, "java.lang.ref", "java.lang.ref.*;");
        addCompletion(provider, "java.lang.reflect", "java.lang.reflect.*;");
        addCompletion(provider, "java.math", "java.math.*;");
        addCompletion(provider, "java.net", "java.net.*;");
        addCompletion(provider, "java.nio", "java.nio.*;");
        addCompletion(provider, "java.nio.channels", "java.nio.channels.*;");
        addCompletion(provider, "java.nio.channels.spi", "java.nio.channels.spi.*;");
        addCompletion(provider, "java.nio.charset", "java.nio.charset.*;");
        addCompletion(provider, "java.nio.charset.spi", "java.nio.charset.spi.*;");
        addCompletion(provider, "java.nio.file", "java.nio.file.*;");
        addCompletion(provider, "java.nio.file.attribute", "java.nio.file.attribute.*;");
        addCompletion(provider, "java.nio.file.spi", "java.nio.file.spi.*;");
        addCompletion(provider, "java.rmi", "java.rmi.*;");
        addCompletion(provider, "java.rmi.activation", "java.rmi.activation.*;");
        addCompletion(provider, "java.rmi.dgc", "java.rmi.dgc.*;");
        addCompletion(provider, "java.rmi.registry", "java.rmi.registry.*;");
        addCompletion(provider, "java.rmi.server", "java.rmi.server.*;");
        addCompletion(provider, "java.security", "java.security.*;");
        addCompletion(provider, "java.security.acl", "java.security.acl.*;");
        addCompletion(provider, "java.security.cert", "java.security.cert.*;");
        addCompletion(provider, "java.security.interfaces", "java.security.interfaces.*;");
        addCompletion(provider, "java.security.spec", "java.security.spec.*;");
        addCompletion(provider, "java.sql", "java.sql.*;");
        addCompletion(provider, "java.text", "java.text.*;");
        addCompletion(provider, "java.text.spi", "java.text.spi.*;");
        addCompletion(provider, "java.time", "java.time.*;");
        addCompletion(provider, "java.time.chrono", "java.time.chrono.*;");
        addCompletion(provider, "java.time.format", "java.time.format.*;");
        addCompletion(provider, "java.time.temporal", "java.time.temporal.*;");
        addCompletion(provider, "java.time.zone", "java.time.zone.*;");
        addCompletion(provider, "java.util", "java.util.*;");
        addCompletion(provider, "java.util.concurrent", "java.util.concurrent.*;");
        addCompletion(provider, "java.util.concurrent.atomic", "java.util.concurrent.atomic.*;");
        addCompletion(provider, "java.util.concurrent.locks", "java.util.concurrent.locks.*;");
        addCompletion(provider, "java.util.function", "java.util.function.*;");
        addCompletion(provider, "java.util.jar", "java.util.jar.*;");
        addCompletion(provider, "java.util.logging", "java.util.logging.*;");
        addCompletion(provider, "java.util.prefs", "java.util.prefs.*;");
        addCompletion(provider, "java.util.regex", "java.util.regex.*;");
        addCompletion(provider, "java.util.spi", "java.util.spi.*;");
        addCompletion(provider, "java.util.stream", "java.util.stream.*;");
        addCompletion(provider, "java.util.zip", "java.util.zip.*;");

        // Balíčky od JavaFx:
        addCompletion(provider, "javafx", "javafx.*;");
        addCompletion(provider, "javafx.animation", "javafx.animation.*;");
        addCompletion(provider, "javafx.application", "javafx.application.*;");
        addCompletion(provider, "javafx.beans", "javafx.beans.*;");
        addCompletion(provider, "javafx.beans.binding", "javafx.beans.binding.*;");
        addCompletion(provider, "javafx.beans.property", "javafx.beans.property.*;");
        addCompletion(provider, "javafx.beans.property.adapter", "javafx.beans.property.adapter.*;");
        addCompletion(provider, "javafx.beans.value", "javafx.beans.value.*;");
        addCompletion(provider, "javafx.collections", "javafx.collections.*;");
        addCompletion(provider, "javafx.collections.transformation", "javafx.collections.transformation.*;");
        addCompletion(provider, "javafx.concurrent", "javafx.concurrent.*;");
        addCompletion(provider, "javafx.css", "javafx.css.*;");
        addCompletion(provider, "javafx.embed", "javafx.embed.*;");
        addCompletion(provider, "javafx.embed.swing", "javafx.embed.swing.*;");
        addCompletion(provider, "javafx.event", "javafx.event.*;");
        addCompletion(provider, "javafx.fxml", "javafx.fxml.*;");
        addCompletion(provider, "javafx.geometry", "javafx.geometry.*;");
        addCompletion(provider, "javafx.print", "javafx.print.*;");
        addCompletion(provider, "javafx.scene", "javafx.scene.*;");
        addCompletion(provider, "javafx.scene.canvas", "javafx.scene.canvas.*;");
        addCompletion(provider, "javafx.scene.chart", "javafx.scene.chart.*;");
        addCompletion(provider, "javafx.scene.control", "javafx.scene.control.*;");
        addCompletion(provider, "javafx.scene.control.cell", "javafx.scene.control.cell.*;");
        addCompletion(provider, "javafx.scene.effect", "javafx.scene.effect.*;");
        addCompletion(provider, "javafx.scene.image", "javafx.scene.image.*;");
        addCompletion(provider, "javafx.scene.input", "javafx.scene.input.*;");
        addCompletion(provider, "javafx.scene.layout", "javafx.scene.layout.*;");
        addCompletion(provider, "javafx.scene.media", "javafx.scene.media.*;");
        addCompletion(provider, "javafx.scene.paint", "javafx.scene.paint.*;");
        addCompletion(provider, "javafx.scene.shape", "javafx.scene.shape.*;");
        addCompletion(provider, "javafx.scene.text", "javafx.scene.text.*;");
        addCompletion(provider, "javafx.scene.transform", "javafx.scene.transform.*;");
        addCompletion(provider, "javafx.scene.web", "javafx.scene.web.*;");
        addCompletion(provider, "javafx.stage", "javafx.stage.*;");
        addCompletion(provider, "javafx.util", "javafx.util.*;");
        addCompletion(provider, "javafx.util.converter", "javafx.util.converter.*;");

        // Balíčky od JavaX:
        addCompletion(provider, "javax", "javax.*;");
        addCompletion(provider, "javax.accessibility", "javax.accessibility.*;");
        addCompletion(provider, "javax.activation", "javax.activation.*;");
        addCompletion(provider, "javax.activity", "javax.activity.*;");
        addCompletion(provider, "javax.annotation", "javax.annotation.*;");
        addCompletion(provider, "javax.annotation.processing", "javax.annotation.processing.*;");
        addCompletion(provider, "javax.crypto", "javax.crypto.*;");
        addCompletion(provider, "javax.crypto.interfaces", "javax.crypto.interfaces.*;");
        addCompletion(provider, "javax.crypto.spec", "javax.crypto.spec.*;");
        addCompletion(provider, "javax.imageio", "javax.imageio.*;");
        addCompletion(provider, "javax.imageio.event", "javax.imageio.event.*;");
        addCompletion(provider, "javax.imageio.metadata", "javax.imageio.metadata.*;");
        addCompletion(provider, "javax.imageio.plugins", "javax.imageio.plugins.*;");
        addCompletion(provider, "javax.imageio.plugins.bmp", "javax.imageio.plugins.bmp.*;");
        addCompletion(provider, "javax.imageio.plugins.jpeg", "javax.imageio.plugins.jpeg.*;");
        addCompletion(provider, "javax.imageio.spi", "javax.imageio.spi.*;");
        addCompletion(provider, "javax.imageio.stream", "javax.imageio.stream.*;");
        addCompletion(provider, "javax.jws", "javax.jws.*;");
        addCompletion(provider, "javax.jws.soap", "javax.jws.soap.*;");
        addCompletion(provider, "javax.lang", "javax.lang.*;");
        addCompletion(provider, "javax.lang.model", "javax.lang.model.*;");
        addCompletion(provider, "javax.lang.model.element", "javax.lang.model.element.*;");
        addCompletion(provider, "javax.lang.model.type", "javax.lang.model.type.*;");
        addCompletion(provider, "javax.lang.model.util", "javax.lang.model.util.*;");
        addCompletion(provider, "javax.management", "javax.management.*;");
        addCompletion(provider, "javax.management.loading", "javax.management.loading.*;");
        addCompletion(provider, "javax.management.modelmbean", "javax.management.modelmbean.*;");
        addCompletion(provider, "javax.management.monitor", "javax.management.monitor.*;");
        addCompletion(provider, "javax.management.openmbean", "javax.management.openmbean.*;");
        addCompletion(provider, "javax.management.relation", "javax.management.relation.*;");
        addCompletion(provider, "javax.management.remote", "javax.management.remote.*;");
        addCompletion(provider, "javax.management.remote.rmi", "javax.management.remote.rmi.*;");
        addCompletion(provider, "javax.management.timer", "javax.management.timer.*;");
        addCompletion(provider, "javax.naming", "javax.naming.*;");
        addCompletion(provider, "javax.naming.directory", "javax.naming.directory.*;");
        addCompletion(provider, "javax.naming.event", "javax.naming.event.*;");
        addCompletion(provider, "javax.naming.ldap", "javax.naming.ldap.*;");
        addCompletion(provider, "javax.naming.spi", "javax.naming.spi.*;");
        addCompletion(provider, "javax.net", "javax.net.*;");
        addCompletion(provider, "javax.net.ssl", "javax.net.ssl.*;");
        addCompletion(provider, "javax.print", "javax.print.*;");
        addCompletion(provider, "javax.print.attribute", "javax.print.attribute.*;");
        addCompletion(provider, "javax.print.attribute.standard", "javax.print.attribute.standard.*;");
        addCompletion(provider, "javax.print.event", "javax.print.event.*;");
        addCompletion(provider, "javax.rmi", "javax.rmi.*;");
        addCompletion(provider, "javax.rmi.CORBA", "javax.rmi.CORBA.*;");
        addCompletion(provider, "javax.rmi.ssl", "javax.rmi.ssl.*;");
        addCompletion(provider, "javax.script", "javax.script.*;");
        addCompletion(provider, "javax.security", "javax.security.*;");
        addCompletion(provider, "javax.security.auth", "javax.security.auth.*;");
        addCompletion(provider, "javax.security.auth.callback", "javax.security.auth.callback.*;");
        addCompletion(provider, "javax.security.auth.kerberos", "javax.security.auth.kerberos.*;");
        addCompletion(provider, "javax.security.auth.login", "javax.security.auth.login.*;");
        addCompletion(provider, "javax.security.auth.spi", "javax.security.auth.spi.*;");
        addCompletion(provider, "javax.security.auth.x500", "javax.security.auth.x500.*;");
        addCompletion(provider, "javax.security.cert", "javax.security.cert.*;");
        addCompletion(provider, "javax.security.sasl", "javax.security.sasl.*;");
        addCompletion(provider, "javax.smartcardio", "javax.smartcardio.*;");
        addCompletion(provider, "javax.sound", "javax.sound.*;");
        addCompletion(provider, "javax.sound.midi", "javax.sound.midi.*;");
        addCompletion(provider, "javax.sound.midi.spi", "javax.sound.midi.spi.*;");
        addCompletion(provider, "javax.sound.sampled", "javax.sound.sampled.*;");
        addCompletion(provider, "javax.sound.sampled.spi", "javax.sound.sampled.spi.*;");
        addCompletion(provider, "javax.sql", "javax.sql.*;");
        addCompletion(provider, "javax.sql.rowset", "javax.sql.rowset.*;");
        addCompletion(provider, "javax.sql.rowset.serial", "javax.sql.rowset.serial.*;");
        addCompletion(provider, "javax.sql.rowset.spi", "javax.sql.rowset.spi.*;");
        addCompletion(provider, "javax.swing", "javax.swing.*;");
        addCompletion(provider, "javax.swing.border", "javax.swing.border");
        addCompletion(provider, "javax.swing.colorchooser", "javax.swing.colorchooser.*;");
        addCompletion(provider, "javax.swing.event", "javax.swing.event.*;");
        addCompletion(provider, "javax.swing.filechooser", "javax.swing.filechooser.*;");
        addCompletion(provider, "javax.swing.plaf", "javax.swing.plaf.*;");
        addCompletion(provider, "javax.swing.plaf.basic", "javax.swing.plaf.basic.*;");
        addCompletion(provider, "javax.swing.plaf.basic.icons", "javax.swing.plaf.basic.icons.*;");
        addCompletion(provider, "javax.swing.plaf.metal", "javax.swing.plaf.metal.*;");
        addCompletion(provider, "javax.swing.plaf.metal.icons", "javax.swing.plaf.metal.icons.*;");
        addCompletion(provider, "javax.swing.plaf.metal.icons.ocean", "javax.swing.plaf.metal.icons.ocean.*;");
        addCompletion(provider, "javax.swing.plaf.metal.sounds", "javax.swing.plaf.metal.sounds.*;");
        addCompletion(provider, "javax.swing.plaf.multi", "javax.swing.plaf.multi.*;");
        addCompletion(provider, "javax.swing.plaf.nimbus", "javax.swing.plaf.nimbus.*;");
        addCompletion(provider, "javax.swing.plaf.synth", "javax.swing.plaf.synth.*;");
        addCompletion(provider, "javax.swing.table", "javax.swing.table.*;");
        addCompletion(provider, "javax.swing.text", "javax.swing.text.*;");
        addCompletion(provider, "javax.swing.text.html", "javax.swing.text.html.*;");
        addCompletion(provider, "javax.swing.text.html.parser", "javax.swing.text.html.parser.*;");
        addCompletion(provider, "javax.swing.text.rtf", "javax.swing.text.rtf.*;");
        addCompletion(provider, "javax.swing.text.rtf.charsets", "javax.swing.text.rtf.charsets.*;");
        addCompletion(provider, "javax.swing.tree", "javax.swing.tree.*;");
        addCompletion(provider, "javax.swing.undo", "javax.swing.undo.*;");
        addCompletion(provider, "javax.tools", "javax.tools.*;");
        addCompletion(provider, "javax.transaction", "javax.transaction.*;");
        addCompletion(provider, "javax.transaction.xa", "javax.transaction.xa.*;");
        addCompletion(provider, "javax.xml", "javax.xml.*;");
        addCompletion(provider, "javax.xml.bind", "javax.xml.bind.*;");
        addCompletion(provider, "javax.xml.bind.annotation", "javax.xml.bind.annotation.*;");
        addCompletion(provider, "javax.xml.bind.annotation.adapters", "javax.xml.bind.annotation.adapters.*;");
        addCompletion(provider, "javax.xml.bind.attachment", "javax.xml.bind.attachment.*;");
        addCompletion(provider, "javax.xml.bind.helpers", "javax.xml.bind.helpers.*;");
        addCompletion(provider, "javax.xml.bind.util", "javax.xml.bind.util.*;");
        addCompletion(provider, "javax.xml.crypto", "javax.xml.crypto.*;");
        addCompletion(provider, "javax.xml.crypto.dom", "javax.xml.crypto.dom.*;");
        addCompletion(provider, "javax.xml.crypto.dsig", "javax.xml.crypto.dsig.*;");
        addCompletion(provider, "javax.xml.crypto.dsig.dom", "javax.xml.crypto.dsig.dom.*;");
        addCompletion(provider, "javax.xml.crypto.dsig.keyinfo", "javax.xml.crypto.dsig.keyinfo.*;");
        addCompletion(provider, "javax.xml.crypto.dsig.spec", "javax.xml.crypto.dsig.spec.*;");
        addCompletion(provider, "javax.xml.datatype", "javax.xml.datatype.*;");
        addCompletion(provider, "javax.xml.namespace", "javax.xml.namespace.*;");
        addCompletion(provider, "javax.xml.parsers", "javax.xml.parsers.*;");
        addCompletion(provider, "javax.xml.soap", "javax.xml.soap.*;");
        addCompletion(provider, "javax.xml.stream", "javax.xml.stream.*;");
        addCompletion(provider, "javax.xml.stream.events", "javax.xml.stream.events.*;");
        addCompletion(provider, "javax.xml.stream.util", "javax.xml.stream.util.*;");
        addCompletion(provider, "javax.xml.transform", "javax.xml.transform.*;");
        addCompletion(provider, "javax.xml.transform.dom", "javax.xml.transform.dom.*;");
        addCompletion(provider, "javax.xml.transform.sax", "javax.xml.transform.sax.*;");
        addCompletion(provider, "javax.xml.transform.stax", "javax.xml.transform.stax.*;");
        addCompletion(provider, "javax.xml.transform.stream", "javax.xml.transform.stream.*;");
        addCompletion(provider, "javax.xml.validation", "javax.xml.validation.*;");
        addCompletion(provider, "javax.xml.ws", "javax.xml.ws.*;");
        addCompletion(provider, "javax.xml.ws.handler", "javax.xml.ws.handler.*;");
        addCompletion(provider, "javax.xml.ws.handler.soap", "javax.xml.ws.handler.soap.*;");
        addCompletion(provider, "javax.xml.ws.http", "javax.xml.ws.http.*;");
        addCompletion(provider, "javax.xml.ws.soap", "javax.xml.ws.soap.*;");
        addCompletion(provider, "javax.xml.ws.spi", "javax.xml.ws.spi.*;");
        addCompletion(provider, "javax.xml.ws.spi.http", "javax.xml.ws.spi.http.*;");
        addCompletion(provider, "javax.xml.ws.wsaddressing", "javax.xml.ws.wsaddressing.*;");
        addCompletion(provider, "javax.xml.xpath", "javax.xml.xpath.*;");









        //	      provider.addCompletion(new ShorthandCompletion(provider, "text v menu", "text, ktery se vlozi do editoru", "kratky popis za textem v menu", "Komentar v dalsim okne"));

        // Operace nad Listem:
        addCompletion(provider, "add(E e)", "add(e)", "boolean", "Appends the specified element to the end of this list (optional operation).");
        addCompletion(provider, "add(int index, E element)", "add(element)", "void", "Inserts the specified element at the specified position in this list (optional operation).");
        addCompletion(provider, "addAll(Collection<? extends E> c)", "addAll(c)", "boolean", "Appends all of the elements in the specified collection to the end of this list, in the order that they are returned by the specified collection's iterator (optional operation).");
        addCompletion(provider, "addAll(int index, Collection<? extends E> c)", "addAll(index, c)", "boolean", "Inserts all of the elements in the specified collection into this list at the specified position (optional operation).");
        addCompletion(provider, "clear()", "clear()", "void", "Removes all of the elements from this list (optional operation).");
        addCompletion(provider, "contains(Object o)", "contains(o)", "boolean", "Returns true if this list contains the specified element.");
        addCompletion(provider, "containsAll(Collection<?> c)", "containsAll(c)", "boolean", "Returns true if this list contains all of the elements of the specified collection.");
        addCompletion(provider, "equals(Object o)", "equals(o)", "boolean", "Compares the specified object with this list for equality.");
        addCompletion(provider, "get(int index)", "get(index)", "E", "Returns the element at the specified position in this list.");
        addCompletion(provider, "hashCode()", "hashCode()", "int", "Returns the hash code value for this list.");
        addCompletion(provider, "indexOf(Object o)", "indexOf(o)", "int", "Returns the index of the first occurrence of the specified element in this list, or -1 if this list does not contain the element.");
        addCompletion(provider, "isEmpty()", "isEmpty()", "boolean", "Returns true if this list contains no elements.");
        addCompletion(provider, "iterator()", "iterator()", "Iterator<E>", "Returns an iterator over the elements in this list in proper sequence.");
        addCompletion(provider, "lastIndexOf(Object o)", "lastIndexOf(o)", "int", "Returns the index of the last occurrence of the specified element in this list, or -1 if this list does not contain the element.");
        addCompletion(provider, "listIterator()", "listIterator()", "ListIterator<E>", "Returns a list iterator over the elements in this list (in proper sequence).");
        addCompletion(provider, "listIterator(int index)", "listIterator(index)", "ListIterator<E>", "Returns a list iterator over the elements in this list (in proper sequence), starting at the specified position in the list.");
        addCompletion(provider, "remove(int index)", "remove(index)", "E", "Removes the element at the specified position in this list (optional operation).");
        addCompletion(provider, "remove(Object o)", "remove(o)", "boolean", "Removes the first occurrence of the specified element from this list, if it is present (optional operation).");
        addCompletion(provider, "removeAll(Collection<?> c)", "removeAll(c)", "boolean", "Removes from this list all of its elements that are contained in the specified collection (optional operation).");
        addCompletion(provider, "replaceAll(UnaryOperator<E> operator)", "replaceAll(operator)", "default void");
        addCompletion(provider, "retainAll(Collection<?> c)", "retainAll(c)", "boolean", "Retains only the elements in this list that are contained in the specified collection (optional operation).");
        addCompletion(provider, "set(int index, E element)", "set(index, element)", "E", "Replaces the element at the specified position in this list with the specified element (optional operation).");
        addCompletion(provider, "size()", "size()", "int", "Returns the number of elements in this list.");
        addCompletion(provider, "sort(Comparator<? super E> c)", "sort(c)", "default void");
        addCompletion(provider, "spliterator()", "spliterator()", "default Spliterator<E>");
        addCompletion(provider, "subList(int fromIndex, int toIndex)", "subList(fromIndex, toIndex)", "List<E>", "Returns a view of the portion of this list between the specified fromIndex, inclusive, and toIndex, exclusive.");
        addCompletion(provider, "toArray()", "toArray()", "Object[]", "Returns an array containing all of the elements in this list in proper sequence (from first to last element).");
        addCompletion(provider, "toArray(T[] a)", "toArray(a)", "<T> T[]", "Returns an array containing all of the elements in this list in proper sequence (from first to last element); the runtime type of the returned array is that of the specified array.");


        // vlastnost pole: - zjistěni jeho delky:
        addCompletion(provider, "length", "length", "int");


        // metody nad HashMapou:
        addCompletion(provider, "clear()", "clear()", "void", "Removes all of the mappings from this map.");
        addCompletion(provider, "clone()", "clone()", "Object", "Returns a shallow copy of this HashMap instance: the keys and values themselves are not cloned.");
        addCompletion(provider, "containsKey(Object key)", "containsKey(key)", "boolean", "Returns true if this map contains a mapping for the specified key.");
        addCompletion(provider, "get(Object key)", "get(key)", "V", "Returns the value to which the specified key is mapped, or null if this map contains no mapping for the key.");
        addCompletion(provider, "isEmpty()", "isEmpty()", "boolean", "Returns true if this map contains no key-value mappings.");
        addCompletion(provider, "keySet()", "keySet()", "Set<K>", "Returns a Set view of the keys contained in this map.");
        addCompletion(provider, "put(K key, V value)", "put(key, value)", "V", "Associates the specified value with the specified key in this map.");
        addCompletion(provider, "putAll(Map<? extends K,? extends V> m)", "putAll(m)", "void", "Copies all of the mappings from the specified map to this map.");
        addCompletion(provider, "remove(Object key)", "remove(key)", "V", "Removes the mapping for the specified key from this map if present.");
        addCompletion(provider, "size()", "size()", "int", "Returns the number of key-value mappings in this map.");
        addCompletion(provider, "values()", "values()", "Collection<V>", "Returns a Collection view of the values contained in this map.");







        addCompletion(provider, "Arrays", "Arrays", "java.util", "This class contains various methods for manipulating arrays (such as sorting and searching). This class also contains a static factory that allows arrays to be viewed as lists. \r\n"+
                "The methods in this class all throw a NullPointerException, if the specified array reference is null, except where noted. \r\n" +
                "\r\n" +
                "The documentation for the methods contained in this class includes briefs description of the implementations. Such descriptions should be regarded as implementation notes, rather than parts of the specification. Implementors should feel free to substitute other algorithms, so long as the specification itself is adhered to. (For example, the algorithm used by sort(Object[]) does not have to be a MergeSort, but it does have to be stable.)");


        // Operace nad Arrays:
        addCompletion(provider, "asList(T... a)", "asList(a)", "static <T> List<T>", "Returns a fixed-size list backed by the specified array.");

        addCompletion(provider, "binarySearch(byte[] a, byte key)", "binarySearch(a, key)", "static int", "Searches the specified array of bytes for the specified value using the binary search algorithm.");
        addCompletion(provider, "binarySearch(byte[] a, int fromIndex, int toIndex, byte key)", "binarySearch(a, fromIndex, toIndex, key)", "static int", "Searches a range of the specified array of bytes for the specified value using the binary search algorithm.");
        addCompletion(provider, "binarySearch(char[] a, char key)", "binarySearch(a, key)", "static int", "Searches the specified array of chars for the specified value using the binary search algorithm.");
        addCompletion(provider, "binarySearch(char[] a, int fromIndex, int toIndex, char key)", "binarySearch(a, fromIndex, toIndex, key)", "static int", "Searches a range of the specified array of chars for the specified value using the binary search algorithm.");
        addCompletion(provider, "binarySearch(double[] a, double key)", "binarySearch(a, key)", "static int", "Searches the specified array of doubles for the specified value using the binary search algorithm.");
        addCompletion(provider, "binarySearch(double[] a, int fromIndex, int toIndex, double key)", "binarySearch(a, fromIndex, toIndex, key)", "static int", "Searches a range of the specified array of doubles for the specified value using the binary search algorithm.");
        addCompletion(provider, "binarySearch(float[] a, float key)", "binarySearch(a, key)", "static int", "Searches the specified array of floats for the specified value using the binary search algorithm.");
        addCompletion(provider, "binarySearch(float[] a, int fromIndex, int toIndex, float key)", "binarySearch(a, fromIndex, toIndex, key)", "static int", "Searches a range of the specified array of floats for the specified value using the binary search algorithm.");
        addCompletion(provider, "binarySearch(int[] a, int key)", "binarySearch( a,nt key)", "static int", "Searches the specified array of ints for the specified value using the binary search algorithm.");
        addCompletion(provider, "binarySearch(int[] a, int fromIndex, int toIndex, int key)", "binarySearch(a, fromIndex, toIndex, key)", "static int", "Searches a range of the specified array of ints for the specified value using the binary search algorithm.");
        addCompletion(provider, "binarySearch(long[] a, int fromIndex, int toIndex, long key)", "binarySearch(a, fromIndex, toIndex, key)", "static int", "Searches a range of the specified array of longs for the specified value using the binary search algorithm.");
        addCompletion(provider, "binarySearch(long[] a, long key)", "binarySearch(a, key)", "static int", "Searches the specified array of longs for the specified value using the binary search algorithm.");
        addCompletion(provider, "binarySearch(Object[] a, int fromIndex, int toIndex, Object key)", "binarySearch(a, fromIndex, toIndex, key)", "static int", "Searches a range of the specified array for the specified object using the binary search algorithm.");
        addCompletion(provider, "binarySearch(Object[] a, Object key)", "binarySearch(a, key)", "static int", "Searches the specified array for the specified object using the binary search algorithm.");
        addCompletion(provider, "binarySearch(short[] a, int fromIndex, int toIndex, short key)", "binarySearch(a, fromIndex, toIndex, key)", "static int", "Searches a range of the specified array of shorts for the specified value using the binary search algorithm.");
        addCompletion(provider, "binarySearch(short[] a, short key)", "binarySearch(a, key)", "static int", "Searches the specified array of shorts for the specified value using the binary search algorithm.");
        addCompletion(provider, "binarySearch(T[] a, int fromIndex, int toIndex, T key, Comparator<? super T> c)", "binarySearch(a, fromIndex, toIndex, key, c)", "static <T> int", "Searches a range of the specified array for the specified object using the binary search algorithm.");
        addCompletion(provider, "binarySearch(T[] a, T key, Comparator<? super T> c)", "binarySearch(a, key, c)", "static <T> int", "Searches the specified array for the specified object using the binary search algorithm.");

        addCompletion(provider, "copyOf(boolean[] original, int newLength)", "copyOf(original, newLength)", "static boolean[]", "Copies the specified array, truncating or padding with false (if necessary) so the copy has the specified length.");
        addCompletion(provider, "copyOf(byte[] original, int newLength)", "copyOf(original, newLength)", "static byte[]", "Copies the specified array, truncating or padding with zeros (if necessary) so the copy has the specified length.");
        addCompletion(provider, "copyOf(char[] original, int newLength)", "copyOf(original, newLength)", "static char[]", "Copies the specified array, truncating or padding with null characters (if necessary) so the copy has the specified length.");
        addCompletion(provider, "copyOf(double[] original, int newLength)", "copyOf(original, newLength)", "static double[]", "Copies the specified array, truncating or padding with zeros (if necessary) so the copy has the specified length.");
        addCompletion(provider, "copyOf(float[] original, int newLength)", "copyOf(original, newLength)", "static float[]", "Copies the specified array, truncating or padding with zeros (if necessary) so the copy has the specified length.");
        addCompletion(provider, "copyOf(int[] original, int newLength)", "copyOf(original, newLength)", "static int[]", "Copies the specified array, truncating or padding with zeros (if necessary) so the copy has the specified length.");
        addCompletion(provider, "copyOf(long[] original, int newLength)", "copyOf(original, newLength)", "static long[]", "Copies the specified array, truncating or padding with zeros (if necessary) so the copy has the specified length.");
        addCompletion(provider, "copyOf(short[] original, int newLength)", "copyOf(original, newLength)", "static short[]", "Copies the specified array, truncating or padding with zeros (if necessary) so the copy has the specified length.");
        addCompletion(provider, "copyOf(T[] original, int newLength)", "copyOf(original, newLength)", "static <T> T[]", "Copies the specified array, truncating or padding with nulls (if necessary) so the copy has the specified length.");
        addCompletion(provider, "copyOf(U[] original, int newLength, Class<? extends T[]> newType)", "copyOf(original, newLength, ewType)", "static <T, U> T[]", "Copies the specified array, truncating or padding with nulls (if necessary) so the copy has the specified length.");

        addCompletion(provider, "copyOfRange(boolean[] original, int from, int to)", "	copyOfRange(original, from, to)", "static boolean[]", "Copies the specified range of the specified array into a new array.");
        addCompletion(provider, "copyOfRange(byte[] original, int from, int to)", "copyOfRange(original, from, to)", "static byte[]", "Copies the specified range of the specified array into a new array.");
        addCompletion(provider, "copyOfRange(char[] original, int from, int to)", "copyOfRange(original, from, to)", "static char[]", "Copies the specified range of the specified array into a new array.");
        addCompletion(provider, "copyOfRange(double[] original, int from, int to)", "copyOfRange(original, from, to)", "static double[]", "Copies the specified range of the specified array into a new array.");
        addCompletion(provider, "copyOfRange(float[] original, int from, int to)", "copyOfRange(original, from, to)", "static float[]", "Copies the specified range of the specified array into a new array.");
        addCompletion(provider, "copyOfRange(int[] original, int from, int to)", "copyOfRange(original, from, to)", "static int[]", "Copies the specified range of the specified array into a new array.");
        addCompletion(provider, "copyOfRange(long[] original, int from, int to)", "copyOfRange(original, from, to)", "static long[]", "Copies the specified range of the specified array into a new array.");
        addCompletion(provider, "copyOfRange(short[] original, int from, int to)", "copyOfRange(original, from, to)", "static short[]", "Copies the specified range of the specified array into a new array.");
        addCompletion(provider, "copyOfRange(T[] original, int from, int to)", "copyOfRange(original, from, to)", "static <T> T[]", "Copies the specified range of the specified array into a new array.");
        addCompletion(provider, "copyOfRange(U[] original, int from, int to, Class<? extends T[]> newType)", "copyOfRange(original, from, to, newType)", "static <T, U> T[]", "Copies the specified range of the specified array into a new array.");

        addCompletion(provider, "deepEquals(Object[] a1, Object[] a2)", "deepEquals(a1, a2)", "static boolean", "Returns true if the two specified arrays are deeply equal to one another.");
        addCompletion(provider, "deepHashCode(Object[] a)", "deepHashCode(a)", "static int", "Returns a hash code based on the \"deep contents\" of the specified array.");
        addCompletion(provider, "deepToString(Object[] a)", "deepToString(a)", "static String", "Returns a string representation of the \"deep contents\" of the specified array.");
        addCompletion(provider, "equals(boolean[] a, boolean[] a2)", "equals(a, a2)", "static boolean", "Returns true if the two specified arrays of booleans are equal to one another.");
        addCompletion(provider, "equals(byte[] a, byte[] a2)", "equals(a, a2)", "static boolean", "Returns true if the two specified arrays of bytes are equal to one another.");
        addCompletion(provider, "equals(char[] a, char[] a2)", "equals(a, a2)", "static boolean", "Returns true if the two specified arrays of chars are equal to one another.");
        addCompletion(provider, "equals(double[] a, double[] a2)", "equals(a, a2)", "static boolean", "Returns true if the two specified arrays of doubles are equal to one another.");
        addCompletion(provider, "equals(float[] a, float[] a2)", "equals(a, a2)", "static boolean", "Returns true if the two specified arrays of floats are equal to one another.");
        addCompletion(provider, "equals(int[] a, int[] a2)", "equals(a, a2)", "static boolean", "Returns true if the two specified arrays of ints are equal to one another.");
        addCompletion(provider, "equals(long[] a, long[] a2)", "equals(a, a2)", "static boolean", "Returns true if the two specified arrays of longs are equal to one another.");
        addCompletion(provider, "equals(Object[] a, Object[] a2)", "equals(a, a2)", "static boolean", "Returns true if the two specified arrays of Objects are equal to one another.");
        addCompletion(provider, "equals(short[] a, short[] a2)", "equals(a, a2)", "static boolean", "Returns true if the two specified arrays of shorts are equal to one another.");

        addCompletion(provider, "fill(boolean[] a, boolean val)", "fill(a, val)", "static void", "Assigns the specified boolean value to each element of the specified array of booleans.");
        addCompletion(provider, "fill(boolean[] a, int fromIndex, int toIndex, boolean val)", "fill(a, fromIndex, toIndex, val)", "static void", "Assigns the specified boolean value to each element of the specified range of the specified array of booleans.");
        addCompletion(provider, "fill(byte[] a, byte val)", "fill(a, val)", "static void", "Assigns the specified byte value to each element of the specified array of bytes.");
        addCompletion(provider, "fill(byte[] a, int fromIndex, int toIndex, byte val)", "fill(a, fromIndex, toIndex, val)", "static void", "Assigns the specified byte value to each element of the specified range of the specified array of bytes.");
        addCompletion(provider, "fill(char[] a, char val)", "fill(a, val)", "static void", "Assigns the specified char value to each element of the specified array of chars.");
        addCompletion(provider, "fill(char[] a, int fromIndex, int toIndex, char val)", "fill(a, fromIndex, toIndex, val)", "static void", "Assigns the specified char value to each element of the specified range of the specified array of chars.");
        addCompletion(provider, "fill(double[] a, double val)", "fill(a, val)", "static void", "Assigns the specified double value to each element of the specified array of doubles.");
        addCompletion(provider, "fill(double[] a, int fromIndex, int toIndex, double val)", "fill(a, fromIndex, toIndex, val)", "static void", "Assigns the specified double value to each element of the specified range of the specified array of doubles.");
        addCompletion(provider, "fill(float[] a, float val)", "fill(a, val)", "static void", "Assigns the specified float value to each element of the specified array of floats.");
        addCompletion(provider, "fill(float[] a, int fromIndex, int toIndex, float val)", "fill(a, fromIndex, toIndex, val)", "static void", "Assigns the specified float value to each element of the specified range of the specified array of floats.");
        addCompletion(provider, "fill(int[] a, int val)", "fill(a, val)", "static void", "Assigns the specified int value to each element of the specified array of ints.");
        addCompletion(provider, "fill(int[] a, int fromIndex, int toIndex, int val)", "fill(a, fromIndex, toIndex, val)", "static void", "Assigns the specified int value to each element of the specified range of the specified array of ints.");
        addCompletion(provider, "fill(long[] a, int fromIndex, int toIndex, long val)", "fill(a, fromIndex, toIndex, val)", "static void", "Assigns the specified long value to each element of the specified range of the specified array of longs.");
        addCompletion(provider, "fill(long[] a, long val)", "fill(a, val)", "static void", "Assigns the specified long value to each element of the specified array of longs.");
        addCompletion(provider, "fill(Object[] a, int fromIndex, int toIndex, Object val)", "fill(a, fromIndex, toIndex, val)", "static void", "Assigns the specified Object reference to each element of the specified range of the specified array of Objects.");
        addCompletion(provider, "fill(Object[] a, Object val)", "fill(a, val)", "static void", "Assigns the specified Object reference to each element of the specified array of Objects.");
        addCompletion(provider, "fill(short[] a, int fromIndex, int toIndex, short val)", "fill(a, fromIndex, toIndex, val)", "static void", "Assigns the specified short value to each element of the specified range of the specified array of shorts.");
        addCompletion(provider, "fill(short[] a, short val)", "fill(a, val)", "static void", "Assigns the specified short value to each element of the specified array of shorts.");

        addCompletion(provider, "hashCode(boolean[] a)", "hashCode(a)", "static int", "Returns a hash code based on the contents of the specified array.");
        addCompletion(provider, "hashCode(byte[] a)", "hashCode(a)", "static int", "Returns a hash code based on the contents of the specified array.");
        addCompletion(provider, "hashCode(char[] a)", "hashCode(a)", "static int", "Returns a hash code based on the contents of the specified array.");
        addCompletion(provider, "hashCode(double[] a)", "hashCode(a)", "static int", "Returns a hash code based on the contents of the specified array.");
        addCompletion(provider, "hashCode(float[] a)", "hashCode(a)", "static int", "Returns a hash code based on the contents of the specified array.");
        addCompletion(provider, "hashCode(int[] a)", "hashCode(a)", "static int", "Returns a hash code based on the contents of the specified array.");
        addCompletion(provider, "hashCode(long[] a)", "hashCode(a)", "static int", "Returns a hash code based on the contents of the specified array.");
        addCompletion(provider, "hashCode(Object[] a)", "hashCode(a)", "static int", "Returns a hash code based on the contents of the specified array.");
        addCompletion(provider, "hashCode(short[] a)", "hashCode(a)", "static int", "Returns a hash code based on the contents of the specified array.");

        addCompletion(provider, "sort(byte[] a)", "sort(a)", "static void", "Sorts the specified array into ascending numerical order.");
        addCompletion(provider, "sort(byte[] a, int fromIndex, int toIndex)", "sort(a, fromIndex, toIndex)", "static void", "Sorts the specified range of the array into ascending order.");
        addCompletion(provider, "sort(char[] a)", "sort(a)", "static void", "Sorts the specified array into ascending numerical order.");
        addCompletion(provider, "sort(char[] a, int fromIndex, int toIndex)", "sort(a, fromIndex, toIndex)", "static void", "Sorts the specified range of the array into ascending order.");
        addCompletion(provider, "sort(double[] a)", "sort(a)", "static void", "Sorts the specified array into ascending numerical order.");
        addCompletion(provider, "sort(double[] a, int fromIndex, int toIndex)", "sort(a, fromIndex, toIndex)", "static void", "Sorts the specified range of the array into ascending order.");
        addCompletion(provider, "sort(float[] a)", "sort(a)", "static void", "Sorts the specified array into ascending numerical order.");
        addCompletion(provider, "sort(float[] a, int fromIndex, int toIndex)", "sort(a, fromIndex, toIndex)", "static void", "Sorts the specified range of the array into ascending order.");
        addCompletion(provider, "sort(int[] a)", "sort(a)", "static void", "Sorts the specified array into ascending numerical order.");
        addCompletion(provider, "sort(int[] a, int fromIndex, int toIndex)", "sort(a, fromIndex, toIndex)", "static void", "Sorts the specified range of the array into ascending order.");
        addCompletion(provider, "sort(long[] a)", "sort(a)", "static void", "Sorts the specified array into ascending numerical order.");
        addCompletion(provider, "sort(long[] a, int fromIndex, int toIndex)", "sort(a, fromIndex, toIndex)", "static void", "Sorts the specified range of the array into ascending order.");
        addCompletion(provider, "sort(Object[] a)", "sort(a)", "static void", "Sorts the specified array of objects into ascending order, according to the natural ordering of its elements.");
        addCompletion(provider, "sort(Object[] a, int fromIndex, int toIndex)", "sort(a, fromIndex, toIndex)", "static void", "Sorts the specified range of the specified array of objects into ascending order, according to the natural ordering of its elements.");
        addCompletion(provider, "sort(short[] a)", "sort(a)", "static void", "Sorts the specified array into ascending numerical order.");
        addCompletion(provider, "sort(short[] a, int fromIndex, int toIndex)", "sort(a, fromIndex, toIndex)", "static void", "Sorts the specified range of the array into ascending order.");
        addCompletion(provider, "sort(T[] a, Comparator<? super T> c)", "sort(a, c)", "static <T> void", "Sorts the specified array of objects according to the order induced by the specified comparator.");
        addCompletion(provider, "sort(T[] a, int fromIndex, int toIndex, Comparator<? super T> c)", "sort(a, fromIndex, toIndex, c)", "static <T> void", "Sorts the specified range of the specified array of objects according to the order induced by the specified comparator.");

        addCompletion(provider, "toString(boolean[] a)", "toString(a)", "static String", "Returns a string representation of the contents of the specified array.");
        addCompletion(provider, "toString(byte[] a)", "toString(a)", "static String", "Returns a string representation of the contents of the specified array.");
        addCompletion(provider, "toString(char[] a)", "toString(a)", "static String", "Returns a string representation of the contents of the specified array.");
        addCompletion(provider, "toString(double[] a)", "toString(a)", "static String", "Returns a string representation of the contents of the specified array.");
        addCompletion(provider, "toString(float[] a)", "toString(a)", "static String", "Returns a string representation of the contents of the specified array.");
        addCompletion(provider, "toString(int[] a)", "toString(a)", "static String", "Returns a string representation of the contents of the specified array.");
        addCompletion(provider, "toString(long[] a)", "toString(a)", "static String", "Returns a string representation of the contents of the specified array.");
        addCompletion(provider, "toString(Object[] a)", "toString(a)", "static String", "Returns a string representation of the contents of the specified array.");
        addCompletion(provider, "toString(short[] a)", "toString(a)", "static String", "Returns a string representation of the contents of the specified array.");







        // Operace - methody nad Class Method :
        // tj. metody nad proměnnou typu Method coby mětoda načtená pomocí reflexe
        addCompletion(provider, "getAnnotation(Class<T> annotationClass)", "getAnnotation(annotationClass)", "<T extends Annotation> T", "Returns this element's annotation for the specified type if such an annotation is present, else null.");
        addCompletion(provider, "getDeclaredAnnotations()", "getDeclaredAnnotations()", "Annotation[]", "Returns all annotations that are directly present on this element.");
        addCompletion(provider, "getDeclaringClass()", "getDeclaringClass()", "Class<?>", "Returns the Class object representing the class or interface that declares the method represented by this Method object.");
        addCompletion(provider, "getDefaultValue()", "getDefaultValue()", "Object", "Returns the default value for the annotation member represented by this Method instance.");
        addCompletion(provider, "getExceptionTypes()", "getExceptionTypes()", "Class<?>[]", "Returns an array of Class objects that represent the types of the exceptions declared to be thrown by the underlying method represented by this Method object.");
        addCompletion(provider, "getGenericExceptionTypes()", "getGenericExceptionTypes()", "Type[]", "Returns an array of Type objects that represent the exceptions declared to be thrown by this Method object.");
        addCompletion(provider, "getGenericParameterTypes()", "getGenericParameterTypes()", "Type[]", "Returns an array of Type objects that represent the formal parameter types, in declaration order, of the method represented by this Method object.");
        addCompletion(provider, "getGenericReturnType()", "getGenericReturnType()", "Type", "Returns a Type object that represents the formal return type of the method represented by this Method object.");
        addCompletion(provider, "getModifiers()", "getModifiers()", "int", "Returns the Java language modifiers for the method represented by this Method object, as an integer.");
        addCompletion(provider, "getName()", "getName()", "String", "Returns the name of the method represented by this Method object, as a String.");
        addCompletion(provider, "getParameterAnnotations()", "getParameterAnnotations()", "Annotation[][]", "Returns an array of arrays that represent the annotations on the formal parameters, in declaration order, of the method represented by this Method object.");
        addCompletion(provider, "getParameterTypes()", "getParameterTypes()", "Class<?>[]", "Returns an array of Class objects that represent the formal parameter types, in declaration order, of the method represented by this Method object.");
        addCompletion(provider, "getReturnType()", "getReturnType()", "Class<?>", "Returns a Class object that represents the formal return type of the method represented by this Method object.");
        addCompletion(provider, "getTypeParameters()", "getTypeParameters()", "TypeVariable<Method>[]", "Returns an array of TypeVariable objects that represent the type variables declared by the generic declaration represented by this GenericDeclaration object, in declaration order.");
        addCompletion(provider, "hashCode()", "hashCode()", "int", "Returns a hashcode for this Method.");
        addCompletion(provider, "invoke(Object obj, Object... args)", "invoke(obj, args)", "Object", "Invokes the underlying method represented by this Method object, on the specified object with the specified parameters.");
        addCompletion(provider, "isBridge()", "isBridge()", "boolean", "Returns true if this method is a bridge method; returns false otherwise.");
        addCompletion(provider, "isSynthetic()", "isSynthetic()", "boolean", "Returns true if this method is a synthetic method; returns false otherwise.");
        addCompletion(provider, "isVarArgs()", "isVarArgs()", "boolean", "Returns true if this method was declared to take a variable number of arguments; returns false otherwise.");
        addCompletion(provider, "toGenericString()", "toGenericString()", "String", "Returns a string describing this Method, including type parameters.");
        addCompletion(provider, "toString()", "toString()", "String", "Returns a string describing this Method.");



        // Metody nad proměnnou typu: java.util.Class<T>
        addCompletion(provider, "asSubclass(Class<U> clazz)", "asSubclass(clazz)", "<U> Class<? extends U>", "Casts this Class object to represent a subclass of the class represented by the specified class object.");
        addCompletion(provider, "cast(Object obj)", "cast(obj)", "T", "Casts an object to the class or interface represented by this Class object.");
        addCompletion(provider, "desiredAssertionStatus()", "desiredAssertionStatus()", "boolean", "Returns the assertion status that would be assigned to this class if it were to be initialized at the time this method is invoked.");
        addCompletion(provider, "forName(String className)", "forName(className)", "static Class<?>", "Returns the Class object associated with the class or interface with the given string name.");
        addCompletion(provider, "forName(String name, boolean initialize, ClassLoader loader)", "forName(name, initialize, loader)", "static Class<?>", "Returns the Class object associated with the class or interface with the given string name, using the given class loader.");
        addCompletion(provider, "getAnnotation(Class<A> annotationClass)", "getAnnotation(annotationClass)", "<A extends Annotation> A", "Returns this element's annotation for the specified type if such an annotation is present, else null.");
        addCompletion(provider, "getAnnotations()", "getAnnotations()", "Annotation[]", "Returns all annotations present on this element.");
        addCompletion(provider, "getCanonicalName()", "getCanonicalName()", "String", "Returns the canonical name of the underlying class as defined by the Java Language Specification.");
        addCompletion(provider, "getClasses()", "getClasses()", "Class<?>[]", "Returns an array containing Class objects representing all the public classes and interfaces that are members of the class represented by this Class object.");
        addCompletion(provider, "getClassLoader()", "getClassLoader()", "ClassLoader", "Returns the class loader for the class.");
        addCompletion(provider, "getComponentType()", "getComponentType()", "Class<?>", "Returns the Class representing the component type of an array.");
        addCompletion(provider, "getConstructor(Class<?>... parameterTypes)", "	getConstructor(parameterTypes)", "Constructor<T>", "Returns a Constructor object that reflects the specified public constructor of the class represented by this Class object.");
        addCompletion(provider, "getConstructors()", "getConstructors()", "Constructor<?>[]", "Returns an array containing Constructor objects reflecting all the public constructors of the class represented by this Class object.");
        addCompletion(provider, "getDeclaredAnnotations()", "getDeclaredAnnotations()", "Annotation[]", "Returns all annotations that are directly present on this element.");
        addCompletion(provider, "getDeclaredClasses()", "getDeclaredClasses()", "Class<?>[]", "Returns an array of Class objects reflecting all the classes and interfaces declared as members of the class represented by this Class object.");
        addCompletion(provider, "getDeclaredConstructor(Class<?>... parameterTypes)", "getDeclaredConstructor(parameterTypes)", "Constructor<T>", "Returns a Constructor object that reflects the specified constructor of the class or interface represented by this Class object.");
        addCompletion(provider, "getDeclaredConstructors()", "getDeclaredConstructors()", "Constructor<?>[]", "Returns an array of Constructor objects reflecting all the constructors declared by the class represented by this Class object.");
        addCompletion(provider, "getDeclaredField(String name)", "getDeclaredField(name)", "Field", "Returns a Field object that reflects the specified declared field of the class or interface represented by this Class object.");
        addCompletion(provider, "getDeclaredFields()", "getDeclaredFields()", "Field[]", "Returns an array of Field objects reflecting all the fields declared by the class or interface represented by this Class object.");
        addCompletion(provider, "getDeclaredMethod(String name, Class<?>... parameterTypes)", "getDeclaredMethod(name, parameterTypes)", "Method", "Returns a Method object that reflects the specified declared method of the class or interface represented by this Class object.");
        addCompletion(provider, "getDeclaredMethods()", "getDeclaredMethods()", "Method[]", "Returns an array of Method objects reflecting all the methods declared by the class or interface represented by this Class object.");
        addCompletion(provider, "getDeclaringClass()", "getDeclaringClass()", "Class<?>", "If the class or interface represented by this Class object is a member of another class, returns the Class object representing the class in which it was declared.");
        addCompletion(provider, "getEnclosingClass()", "getEnclosingClass()", "Class<?>", "Returns the immediately enclosing class of the underlying class.");
        addCompletion(provider, "getEnclosingConstructor()", "getEnclosingConstructor()", "Constructor<?>", "If this Class object represents a local or anonymous class within a constructor, returns a Constructor object representing the immediately enclosing constructor of the underlying class.");
        addCompletion(provider, "getEnclosingMethod()", "getEnclosingMethod()", "Method", "If this Class object represents a local or anonymous class within a method, returns a Method object representing the immediately enclosing method of the underlying class.");
        addCompletion(provider, "getEnumConstants()", "getEnumConstants()", "T[]", "Returns the elements of this enum class or null if this Class object does not represent an enum type.");
        addCompletion(provider, "getField(String name)", "getField(name)", "Field", "Returns a Field object that reflects the specified public member field of the class or interface represented by this Class object.");
        addCompletion(provider, "getFields()", "getFields()", "Field[]", "Returns an array containing Field objects reflecting all the accessible public fields of the class or interface represented by this Class object.");
        addCompletion(provider, "getGenericInterfaces()", "getGenericInterfaces()", "Type[]", "Returns the Types representing the interfaces directly implemented by the class or interface represented by this object.");
        addCompletion(provider, "getGenericSuperclass()", "getGenericSuperclass()", "Type", "Returns the Type representing the direct superclass of the entity (class, interface, primitive type or void) represented by this Class.");
        addCompletion(provider, "getInterfaces()", "getInterfaces()", "Class<?>[]", "Determines the interfaces implemented by the class or interface represented by this object.");
        addCompletion(provider, "getMethod(String name, Class<?>... parameterTypes)", "getMethod(name, parameterTypes)", "Method", "Returns a Method object that reflects the specified public member method of the class or interface represented by this Class object.");
        addCompletion(provider, "getMethods()", "getMethods()", "Method[]", "Returns an array containing Method objects reflecting all the public member methods of the class or interface represented by this Class object, including those declared by the class or interface and those inherited from superclasses and superinterfaces.");
        addCompletion(provider, "getModifiers()", "getModifiers()", "int", "Returns the Java language modifiers for this class or interface, encoded in an integer.");
        addCompletion(provider, "getName()", "getName()", "String", "Returns the name of the entity (class, interface, array class, primitive type, or void) represented by this Class object, as a String.");
        addCompletion(provider, "getPackage()", "getPackage()", "Package", "Gets the package for this class.");
        addCompletion(provider, "getProtectionDomain()", "getProtectionDomain()", "ProtectionDomain", "Returns the ProtectionDomain of this class.");
        addCompletion(provider, "getResource(String name)", "getResource(name)", "URL", "Finds a resource with a given name.");
        addCompletion(provider, "getResourceAsStream(String name)", "getResourceAsStream(name)", "InputStream", "Finds a resource with a given name.");
        addCompletion(provider, "getSigners()", "getSigners()", "Object[]", "Gets the signers of this class.");
        addCompletion(provider, "getSimpleName()", "getSimpleName()", "String", "Returns the simple name of the underlying class as given in the source code.");
        addCompletion(provider, "getSuperclass()", "getSuperclass()", "Class<? super T>", "Returns the Class representing the superclass of the entity (class, interface, primitive type or void) represented by this Class.");
        addCompletion(provider, "getTypeParameters()", "getTypeParameters()", "TypeVariable<Class<T>>[]", "Returns an array of TypeVariable objects that represent the type variables declared by the generic declaration represented by this GenericDeclaration object, in declaration order.");
        addCompletion(provider, "isAnnotation()", "isAnnotation()", "boolean", "Returns true if this Class object represents an annotation type.");
        addCompletion(provider, "isAnnotationPresent(Class<? extends Annotation> annotationClass)", "isAnnotationPresent(annotationClass)", "boolean", "Returns true if an annotation for the specified type is present on this element, else false.");
        addCompletion(provider, "isAnonymousClass()", "isAnonymousClass()", "boolean", "Returns true if and only if the underlying class is an anonymous class.");
        addCompletion(provider, "isArray()", "isArray()", "boolean", "Determines if this Class object represents an array class.");
        addCompletion(provider, "isAssignableFrom(Class<?> cls)", "isAssignableFrom(cls)", "boolean", "Determines if the class or interface represented by this Class object is either the same as, or is a superclass or superinterface of, the class or interface represented by the specified Class parameter.");
        addCompletion(provider, "isEnum()", "isEnum()", "boolean", "Returns true if and only if this class was declared as an enum in the source code.");
        addCompletion(provider, "isInstance(Object obj)", "isInstance(obj)", "boolean", "Determines if the specified Object is assignment-compatible with the object represented by this Class.");
        addCompletion(provider, "isInterface()", "isInterface()", "boolean", "Determines if the specified Class object represents an interface type.");
        addCompletion(provider, "isLocalClass()", "isLocalClass()", "boolean", "Returns true if and only if the underlying class is a local class.");
        addCompletion(provider, "isMemberClass()", "isMemberClass()", "boolean", "Returns true if and only if the underlying class is a member class.");
        addCompletion(provider, "isPrimitive()", "isPrimitive()", "boolean", "Determines if the specified Class object represents a primitive type.");
        addCompletion(provider, "isSynthetic()", "isSynthetic()", "boolean", "Returns true if this class is a synthetic class; returns false otherwise.");
        addCompletion(provider, "newInstance()", "newInstance()", "T", "Creates a new instance of the class represented by this Class object.");





        // Metody pro Random:
        addCompletion(provider, "doubles()", "doubles()", "DoubleStream", "Returns an effectively unlimited stream of pseudorandom double values, each between zero (inclusive) and one (exclusive).");
        addCompletion(provider, "doubles(double randomNumberOrigin, double randomNumberBound)", "doubles(randomNumberOrigin, randomNumberBound)", "DoubleStream", "Returns an effectively unlimited stream of pseudorandom double values, each conforming to the given origin (inclusive) and bound (exclusive).");
        addCompletion(provider, "doubles(long streamSize)", "doubles(streamSize)", "DoubleStream", "Returns a stream producing the given streamSize number of pseudorandom double values, each between zero (inclusive) and one (exclusive).");
        addCompletion(provider, "doubles(long streamSize, double randomNumberOrigin, double randomNumberBound)", "doubles(streamSize, randomNumberOrigin, randomNumberBound)", "DoubleStream", "Returns a stream producing the given streamSize number of pseudorandom double values, each conforming to the given origin (inclusive) and bound (exclusive).");
        addCompletion(provider, "ints()", "ints()", "IntStream", "Returns an effectively unlimited stream of pseudorandom int values.");
        addCompletion(provider, "ints(int randomNumberOrigin, int randomNumberBound)", "ints(randomNumberOrigin, randomNumberBound)", "IntStream", "Returns an effectively unlimited stream of pseudorandom int values, each conforming to the given origin (inclusive) and bound (exclusive).");
        addCompletion(provider, "ints(long streamSize)", "ints(streamSize)", "IntStream", "Returns a stream producing the given streamSize number of pseudorandom int values.");
        addCompletion(provider, "ints(long streamSize, int randomNumberOrigin, int randomNumberBound)", "ints(streamSize, randomNumberOrigin, randomNumberBound)", "IntStream", "Returns a stream producing the given streamSize number of pseudorandom int values, each conforming to the given origin (inclusive) and bound (exclusive).");
        addCompletion(provider, "longs()", "longs()", "LongStream", "Returns an effectively unlimited stream of pseudorandom long values.");
        addCompletion(provider, "longs(long streamSize)", "longs(streamSize)", "LongStream", "Returns a stream producing the given streamSize number of pseudorandom long values.");
        addCompletion(provider, "longs(long randomNumberOrigin, long randomNumberBound)", "longs(randomNumberOrigin, randomNumberBound)", "LongStream", "Returns an effectively unlimited stream of pseudorandom long values, each conforming to the given origin (inclusive) and bound (exclusive).");
        addCompletion(provider, "longs(long streamSize, long randomNumberOrigin, long randomNumberBound)", "longs(streamSize, randomNumberOrigin, randomNumberBound)", "LongStream", "Returns a stream producing the given streamSize number of pseudorandom long, each conforming to the given origin (inclusive) and bound (exclusive).");
        addCompletion(provider, "next(int bits)", "next(bits)", "protected int", "Generates the next pseudorandom number.");
        addCompletion(provider, "nextBoolean()", "nextBoolean()", "boolean", "Returns the next pseudorandom, uniformly distributed boolean value from this random number generator's sequence.");
        addCompletion(provider, "nextBytes(byte[] bytes)", "nextBytes(bytes)", "void", "Generates random bytes and places them into a user-supplied byte array.");
        addCompletion(provider, "nextDouble()", "nextDouble()", "double", "Returns the next pseudorandom, uniformly distributed double value between 0.0 and 1.0 from this random number generator's sequence.");
        addCompletion(provider, "nextFloat()", "nextFloat()", "float", "Returns the next pseudorandom, uniformly distributed float value between 0.0 and 1.0 from this random number generator's sequence.");
        addCompletion(provider, "nextGaussian()", "nextGaussian()", "double", "Returns the next pseudorandom, Gaussian (\"normally\") distributed double value with mean 0.0 and standard deviation 1.0 from this random number generator's sequence.");
        addCompletion(provider, "nextInt()", "nextInt()", "int", "Returns the next pseudorandom, uniformly distributed int value from this random number generator's sequence.");
        addCompletion(provider, "nextInt(int bound)", "nextInt(bound)", "int", "Returns a pseudorandom, uniformly distributed int value between 0 (inclusive) and the specified value (exclusive), drawn from this random number generator's sequence.");
        addCompletion(provider, "nextLong()", "nextLong()", "long", "Returns the next pseudorandom, uniformly distributed long value from this random number generator's sequence.");
        addCompletion(provider, "setSeed(long seed)", "setSeed(seed)", "void", "Sets the seed of this random number generator using a single long seed.");









        // Object:
        addCompletion(provider, "Object", "Object", "java.lang", "Class Object is the root of the class hierarchy. Every class has Object as a superclass. All objects, including arrays, implement the methods of this class.");











        // Map:

        // AbstrqactMap
        addCompletion(provider, "AbstractMap<K, V>", "AbstractMap<K, V>", "java.util", "This class provides a skeletal implementation of the Map interface, to minimize the effort required to implement this interface. \r\n"+
                "To implement an unmodifiable map, the programmer needs only to extend this class and provide an implementation for the entrySet method, which returns a set-view of the map's mappings. Typically, the returned set will, in turn, be implemented atop AbstractSet. This set should not support the add or remove methods, and its iterator should not support the remove method. \r\n" +
                "\r\n" +
                "To implement a modifiable map, the programmer must additionally override this class's put method (which otherwise throws an UnsupportedOperationException), and the iterator returned by entrySet().iterator() must additionally implement its remove method. \r\n" +
                "\r\n" +
                "The programmer should generally provide a void (no argument) and map constructor, as per the recommendation in the Map interface specification. \r\n" +
                "\r\n" +
                "The documentation for each non-abstract method in this class describes its implementation in detail. Each of these methods may be overridden if the map being implemented admits a more efficient implementation. \r\n" +
                "\r\n" +
                "This class is a member of the  Java Collections Framework.");

        addCompletion(provider, "AbstractMap<K, V>()", "AbstractMap<K, V>()", "java.util", "Constructor");


        addCompletion(provider, "HashMapHashMap<K,V>", "HashMap<K,V>", "java.util", "Hash table based implementation of the Map interface. This implementation provides all of the optional map operations, and permits null values and the null key. (The HashMap class is roughly equivalent to Hashtable, except that it is unsynchronized and permits nulls.) This class makes no guarantees as to the order of the map; in particular, it does not guarantee that the order will remain constant over time. \r\n" +
                "This implementation provides constant-time performance for the basic operations (get and put), assuming the hash function disperses the elements properly among the buckets. Iteration over collection views requires time proportional to the \"capacity\" of the HashMap instance (the number of buckets) plus its size (the number of key-value mappings). Thus, it's very important not to set the initial capacity too high (or the load factor too low) if iteration performance is important. \r\n" +
                "\r\n" +
                "An instance of HashMap has two parameters that affect its performance: initial capacity and load factor. The capacity is the number of buckets in the hash table, and the initial capacity is simply the capacity at the time the hash table is created. The load factor is a measure of how full the hash table is allowed to get before its capacity is automatically increased. When the number of entries in the hash table exceeds the product of the load factor and the current capacity, the hash table is rehashed (that is, internal data structures are rebuilt) so that the hash table has approximately twice the number of buckets. \r\n" +
                "\r\n" +
                "As a general rule, the default load factor (.75) offers a good tradeoff between time and space costs. Higher values decrease the space overhead but increase the lookup cost (reflected in most of the operations of the HashMap class, including get and put). The expected number of entries in the map and its load factor should be taken into account when setting its initial capacity, so as to minimize the number of rehash operations. If the initial capacity is greater than the maximum number of entries divided by the load factor, no rehash operations will ever occur. \r\n" +
                "\r\n" +
                "If many mappings are to be stored in a HashMap instance, creating it with a sufficiently large capacity will allow the mappings to be stored more efficiently than letting it perform automatic rehashing as needed to grow the table. Note that using many keys with the same hashCode() is a sure way to slow down performance of any hash table. To ameliorate impact, when keys are Comparable, this class may use comparison order among keys to help break ties. \r\n" +
                "\r\n" +
                "Note that this implementation is not synchronized. If multiple threads access a hash map concurrently, and at least one of the threads modifies the map structurally, it must be synchronized externally. (A structural modification is any operation that adds or deletes one or more mappings; merely changing the value associated with a key that an instance already contains is not a structural modification.) This is typically accomplished by synchronizing on some object that naturally encapsulates the map. If no such object exists, the map should be \"wrapped\" using the Collections.synchronizedMap method. This is best done at creation time, to prevent accidental unsynchronized access to the map:\r\n" +
                "   Map m = Collections.synchronizedMap(new HashMap(...));\r\n" +
                "\r\n" +
                "The iterators returned by all of this class's \"collection view methods\" are fail-fast: if the map is structurally modified at any time after the iterator is created, in any way except through the iterator's own remove method, the iterator will throw a ConcurrentModificationException. Thus, in the face of concurrent modification, the iterator fails quickly and cleanly, rather than risking arbitrary, non-deterministic behavior at an undetermined time in the future. \r\n" +
                "\r\n" +
                "Note that the fail-fast behavior of an iterator cannot be guaranteed as it is, generally speaking, impossible to make any hard guarantees in the presence of unsynchronized concurrent modification. Fail-fast iterators throw ConcurrentModificationException on a best-effort basis. Therefore, it would be wrong to write a program that depended on this exception for its correctness: the fail-fast behavior of iterators should be used only to detect bugs.");


        // HashMap:
        addCompletion(provider, "HashMap()", "HashMap<>()", "java.util.HashMap", "Constructs an empty HashMap with the default initial capacity (16) and the default load factor (0.75).");
        addCompletion(provider, "HashMap(int initialCapacity)", "HashMap(initialCapacity)", "java.util.HashMap", "Constructs an empty HashMap with the specified initial capacity and the default load factor (0.75).");
        addCompletion(provider, "HashMap(int initialCapacity, float loadFactor)", "HashMap(initialCapacity, loadFactor)", "java.util.HashMap", "Constructs an empty HashMap with the specified initial capacity and load factor.");
        addCompletion(provider, "HashMap(Map<? extends K,? extends V> m)", "HashMap(m)", "java.util.HashMap", "Constructs a new HashMap with the same mappings as the specified Map.");

        // HashTable:
        addCompletion(provider, "Hashtable<K, V>", "Hashtable<K, V>", "java.util", "This class implements a hash table, which maps keys to values. Any non-null object can be used as a key or as a value. \r\n" +
                "To successfully store and retrieve objects from a hashtable, the objects used as keys must implement the hashCode method and the equals method. \r\n" +
                "\r\n" +
                "An instance of Hashtable has two parameters that affect its performance: initial capacity and load factor. The capacity is the number of buckets in the hash table, and the initial capacity is simply the capacity at the time the hash table is created. Note that the hash table is open: in the case of a \"hash collision\", a single bucket stores multiple entries, which must be searched sequentially. The load factor is a measure of how full the hash table is allowed to get before its capacity is automatically increased. The initial capacity and load factor parameters are merely hints to the implementation. The exact details as to when and whether the rehash method is invoked are implementation-dependent.\r\n" +
                "\r\n" +
                "Generally, the default load factor (.75) offers a good tradeoff between time and space costs. Higher values decrease the space overhead but increase the time cost to look up an entry (which is reflected in most Hashtable operations, including get and put).\r\n" +
                "\r\n" +
                "The initial capacity controls a tradeoff between wasted space and the need for rehash operations, which are time-consuming. No rehash operations will ever occur if the initial capacity is greater than the maximum number of entries the Hashtable will contain divided by its load factor. However, setting the initial capacity too high can waste space.\r\n" +
                "\r\n" +
                "If many entries are to be made into a Hashtable, creating it with a sufficiently large capacity may allow the entries to be inserted more efficiently than letting it perform automatic rehashing as needed to grow the table. \r\n" +
                "\r\n" +
                "This example creates a hashtable of numbers. It uses the names of the numbers as keys: \r\n" +
                "   \r\n" +
                "   Hashtable<String, Integer> numbers\r\n" +
                "     = new Hashtable<String, Integer>();\r\n" +
                "   numbers.put(\"one\", 1);\r\n" +
                "   numbers.put(\"two\", 2);\r\n" +
                "   numbers.put(\"three\", 3);\r\n" +
                "\r\n" +
                "To retrieve a number, use the following code: \r\n" +
                "   \r\n" +
                "   Integer n = numbers.get(\"two\");\r\n" +
                "   if (n != null) {\r\n" +
                "     System.out.println(\"two = \" + n);\r\n" +
                "   }\r\n" +
                "\r\n" +
                "The iterators returned by the iterator method of the collections returned by all of this class's \"collection view methods\" are fail-fast: if the Hashtable is structurally modified at any time after the iterator is created, in any way except through the iterator's own remove method, the iterator will throw a ConcurrentModificationException. Thus, in the face of concurrent modification, the iterator fails quickly and cleanly, rather than risking arbitrary, non-deterministic behavior at an undetermined time in the future. The Enumerations returned by Hashtable's keys and elements methods are not fail-fast. \r\n" +
                "\r\n" +
                "Note that the fail-fast behavior of an iterator cannot be guaranteed as it is, generally speaking, impossible to make any hard guarantees in the presence of unsynchronized concurrent modification. Fail-fast iterators throw ConcurrentModificationException on a best-effort basis. Therefore, it would be wrong to write a program that depended on this exception for its correctness: the fail-fast behavior of iterators should be used only to detect bugs. \r\n" +
                "\r\n" +
                "As of the Java 2 platform v1.2, this class was retrofitted to implement the Map interface, making it a member of the  Java Collections Framework. Unlike the new collection implementations, Hashtable is synchronized. If a thread-safe implementation is not needed, it is recommended to use HashMap in place of Hashtable. If a thread-safe highly-concurrent implementation is desired, then it is recommended to use ConcurrentHashMap in place of Hashtable.");

        addCompletion(provider, "Hashtable()", "Hashtable()", "java.util.Hashtable", "Constructs a new, empty hashtable with a default initial capacity (11) and load factor (0.75).");
        addCompletion(provider, "Hashtable(int initialCapacity)", "Hashtable(initialCapacity)", "java.util.Hashtable", "Constructs a new, empty hashtable with the specified initial capacity and default load factor (0.75).");
        addCompletion(provider, "Hashtable(int initialCapacity, float loadFactor)", "Hashtable(initialCapacity, loadFactor)", "java.util.Hashtable", "Constructs a new, empty hashtable with the specified initial capacity and the specified load factor.");
        addCompletion(provider, "Hashtable(Map<? extends K,? extends V> t)", "Hashtable(t)", "java.util.Hashtable", "Constructs a new hashtable with the same mappings as the given Map.");

        // LinkedHashMap:
        addCompletion(provider, "LinkedHashMap<K, V>", "LinkedHashMap<K, V>", "java.util", "Hash table and linked list implementation of the Map interface, with predictable iteration order. This implementation differs from HashMap in that it maintains a doubly-linked list running through all of its entries. This linked list defines the iteration ordering, which is normally the order in which keys were inserted into the map (insertion-order). Note that insertion order is not affected if a key is re-inserted into the map. (A key k is reinserted into a map m if m.put(k, v) is invoked when m.containsKey(k) would return true immediately prior to the invocation.) \r\n" +
                "\r\n" +
                "This implementation spares its clients from the unspecified, generally chaotic ordering provided by HashMap (and Hashtable), without incurring the increased cost associated with TreeMap. It can be used to produce a copy of a map that has the same order as the original, regardless of the original map's implementation: \r\n" +
                "     void foo(Map m) {\r\n" +
                "         Map copy = new LinkedHashMap(m);\r\n" +
                "         ...\r\n" +
                "     }\r\n" +
                " \r\n" +
                "This technique is particularly useful if a module takes a map on input, copies it, and later returns results whose order is determined by that of the copy. (Clients generally appreciate having things returned in the same order they were presented.) \r\n" +
                "A special constructor is provided to create a linked hash map whose order of iteration is the order in which its entries were last accessed, from least-recently accessed to most-recently (access-order). This kind of map is well-suited to building LRU caches. Invoking the put, putIfAbsent, get, getOrDefault, compute, computeIfAbsent, computeIfPresent, or merge methods results in an access to the corresponding entry (assuming it exists after the invocation completes). The replace methods only result in an access of the entry if the value is replaced. The putAll method generates one entry access for each mapping in the specified map, in the order that key-value mappings are provided by the specified map's entry set iterator. No other methods generate entry accesses. In particular, operations on collection-views do not affect the order of iteration of the backing map. \r\n" +
                "\r\n" +
                "The removeEldestEntry(Map.Entry) method may be overridden to impose a policy for removing stale mappings automatically when new mappings are added to the map. \r\n" +
                "\r\n" +
                "This class provides all of the optional Map operations, and permits null elements. Like HashMap, it provides constant-time performance for the basic operations (add, contains and remove), assuming the hash function disperses elements properly among the buckets. Performance is likely to be just slightly below that of HashMap, due to the added expense of maintaining the linked list, with one exception: Iteration over the collection-views of a LinkedHashMap requires time proportional to the size of the map, regardless of its capacity. Iteration over a HashMap is likely to be more expensive, requiring time proportional to its capacity. \r\n" +
                "\r\n" +
                "A linked hash map has two parameters that affect its performance: initial capacity and load factor. They are defined precisely as for HashMap. Note, however, that the penalty for choosing an excessively high value for initial capacity is less severe for this class than for HashMap, as iteration times for this class are unaffected by capacity. \r\n" +
                "\r\n" +
                "Note that this implementation is not synchronized. If multiple threads access a linked hash map concurrently, and at least one of the threads modifies the map structurally, it must be synchronized externally. This is typically accomplished by synchronizing on some object that naturally encapsulates the map. If no such object exists, the map should be \"wrapped\" using the Collections.synchronizedMap method. This is best done at creation time, to prevent accidental unsynchronized access to the map:\r\n" +
                "   Map m = Collections.synchronizedMap(new LinkedHashMap(...));\r\n" +
                "A structural modification is any operation that adds or deletes one or more mappings or, in the case of access-ordered linked hash maps, affects iteration order. In insertion-ordered linked hash maps, merely changing the value associated with a key that is already contained in the map is not a structural modification. In access-ordered linked hash maps, merely querying the map with get is a structural modification. ) \r\n" +
                "The iterators returned by the iterator method of the collections returned by all of this class's collection view methods are fail-fast: if the map is structurally modified at any time after the iterator is created, in any way except through the iterator's own remove method, the iterator will throw a ConcurrentModificationException. Thus, in the face of concurrent modification, the iterator fails quickly and cleanly, rather than risking arbitrary, non-deterministic behavior at an undetermined time in the future. \r\n" +
                "\r\n" +
                "Note that the fail-fast behavior of an iterator cannot be guaranteed as it is, generally speaking, impossible to make any hard guarantees in the presence of unsynchronized concurrent modification. Fail-fast iterators throw ConcurrentModificationException on a best-effort basis. Therefore, it would be wrong to write a program that depended on this exception for its correctness: the fail-fast behavior of iterators should be used only to detect bugs. \r\n" +
                "\r\n" +
                "The spliterators returned by the spliterator method of the collections returned by all of this class's collection view methods are late-binding, fail-fast, and additionally report Spliterator.ORDERED.");

        addCompletion(provider, "LinkedHashMap()", "LinkedHashMap()", "java.util.LinkedHashMap", "Constructs an empty insertion-ordered LinkedHashMap instance with the default initial capacity (16) and load factor (0.75).");
        addCompletion(provider, "LinkedHashMap(int initialCapacity)", "LinkedHashMap(initialCapacity)", "java.util.LinkedHashMap", "Constructs an empty insertion-ordered LinkedHashMap instance with the specified initial capacity and a default load factor (0.75).");
        addCompletion(provider, "LinkedHashMap(int initialCapacity, float loadFactor)", "LinkedHashMap(initialCapacity, loadFactor)", "java.util.LinkedHashMap", "Constructs an empty insertion-ordered LinkedHashMap instance with the specified initial capacity and load factor.");
        addCompletion(provider, "LinkedHashMap(int initialCapacity, float loadFactor, boolean accessOrder)", "LinkedHashMap(initialCapacity, loadFactor, accessOrder)", "java.util.LinkedHashMap", "Constructs an empty LinkedHashMap instance with the specified initial capacity, load factor and ordering mode.");
        addCompletion(provider, "LinkedHashMap(Map<? extends K,? extends V> m)", "LinkedHashMap(m)", "java.util.LinkedHashMap", "Constructs an insertion-ordered LinkedHashMap instance with the same mappings as the specified map.");

        // Properties:
        addCompletion(provider, "Properties", "Properties", "java.util", "The Properties class represents a persistent set of properties. The Properties can be saved to a stream or loaded from a stream. Each key and its corresponding value in the property list is a string. \r\n" +
                "A property list can contain another property list as its \"defaults\"; this second property list is searched if the property key is not found in the original property list. \r\n" +
                "\r\n" +
                "Because Properties inherits from Hashtable, the put and putAll methods can be applied to a Properties object. Their use is strongly discouraged as they allow the caller to insert entries whose keys or values are not Strings. The setProperty method should be used instead. If the store or save method is called on a \"compromised\" Properties object that contains a non-String key or value, the call will fail. Similarly, the call to the propertyNames or list method will fail if it is called on a \"compromised\" Properties object that contains a non-String key. \r\n" +
                "\r\n" +
                "The load(Reader) / store(Writer, String) methods load and store properties from and to a character based stream in a simple line-oriented format specified below. The load(InputStream) / store(OutputStream, String) methods work the same way as the load(Reader)/store(Writer, String) pair, except the input/output stream is encoded in ISO 8859-1 character encoding. Characters that cannot be directly represented in this encoding can be written using Unicode escapes as defined in section 3.3 of The Java™ Language Specification; only a single 'u' character is allowed in an escape sequence. The native2ascii tool can be used to convert property files to and from other character encodings. \r\n" +
                "\r\n" +
                "The loadFromXML(InputStream) and storeToXML(OutputStream, String, String) methods load and store properties in a simple XML format. By default the UTF-8 character encoding is used, however a specific encoding may be specified if required. Implementations are required to support UTF-8 and UTF-16 and may support other encodings. An XML properties document has the following DOCTYPE declaration: \r\n" +
                " <!DOCTYPE properties SYSTEM \"http://java.sun.com/dtd/properties.dtd\">\r\n" +
                " \r\n" +
                "Note that the system URI (http://java.sun.com/dtd/properties.dtd) is not accessed when exporting or importing properties; it merely serves as a string to uniquely identify the DTD, which is:     <?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" +
                "\r\n" +
                "    <!-- DTD for properties -->\r\n" +
                "\r\n" +
                "    <!ELEMENT properties ( comment?, entry* ) >\r\n" +
                "\r\n" +
                "    <!ATTLIST properties version CDATA #FIXED \"1.0\">\r\n" +
                "\r\n" +
                "    <!ELEMENT comment (#PCDATA) >\r\n" +
                "\r\n" +
                "    <!ELEMENT entry (#PCDATA) >\r\n" +
                "\r\n" +
                "    <!ATTLIST entry key CDATA #REQUIRED>\r\n" +
                " \r\n" +
                "\r\n" +
                "This class is thread-safe: multiple threads can share a single Properties object without the need for external synchronization.");

        addCompletion(provider, "Properties()", "Properties()", "java.util.Properties", "Creates an empty property list with no default values.");
        addCompletion(provider, "Properties(Properties defaults)", "Properties(defaults)", "java.util", "Creates an empty property list with the specified defaults.");

        // TreeMap:
        addCompletion(provider, "TreeMap<K, V>", "TreeMap<K, V>", "java.util", "A Red-Black tree based NavigableMap implementation. The map is sorted according to the natural ordering of its keys, or by a Comparator provided at map creation time, depending on which constructor is used. \r\n" +
                "This implementation provides guaranteed log(n) time cost for the containsKey, get, put and remove operations. Algorithms are adaptations of those in Cormen, Leiserson, and Rivest's Introduction to Algorithms. \r\n" +
                "\r\n" +
                "Note that the ordering maintained by a tree map, like any sorted map, and whether or not an explicit comparator is provided, must be consistent with equals if this sorted map is to correctly implement the Map interface. (See Comparable or Comparator for a precise definition of consistent with equals.) This is so because the Map interface is defined in terms of the equals operation, but a sorted map performs all key comparisons using its compareTo (or compare) method, so two keys that are deemed equal by this method are, from the standpoint of the sorted map, equal. The behavior of a sorted map is well-defined even if its ordering is inconsistent with equals; it just fails to obey the general contract of the Map interface. \r\n" +
                "\r\n" +
                "Note that this implementation is not synchronized. If multiple threads access a map concurrently, and at least one of the threads modifies the map structurally, it must be synchronized externally. (A structural modification is any operation that adds or deletes one or more mappings; merely changing the value associated with an existing key is not a structural modification.) This is typically accomplished by synchronizing on some object that naturally encapsulates the map. If no such object exists, the map should be \"wrapped\" using the Collections.synchronizedSortedMap method. This is best done at creation time, to prevent accidental unsynchronized access to the map: \r\n" +
                "   SortedMap m = Collections.synchronizedSortedMap(new TreeMap(...));\r\n" +
                "\r\n" +
                "The iterators returned by the iterator method of the collections returned by all of this class's \"collection view methods\" are fail-fast: if the map is structurally modified at any time after the iterator is created, in any way except through the iterator's own remove method, the iterator will throw a ConcurrentModificationException. Thus, in the face of concurrent modification, the iterator fails quickly and cleanly, rather than risking arbitrary, non-deterministic behavior at an undetermined time in the future. \r\n" +
                "\r\n" +
                "Note that the fail-fast behavior of an iterator cannot be guaranteed as it is, generally speaking, impossible to make any hard guarantees in the presence of unsynchronized concurrent modification. Fail-fast iterators throw ConcurrentModificationException on a best-effort basis. Therefore, it would be wrong to write a program that depended on this exception for its correctness: the fail-fast behavior of iterators should be used only to detect bugs. \r\n" +
                "\r\n" +
                "All Map.Entry pairs returned by methods in this class and its views represent snapshots of mappings at the time they were produced. They do not support the Entry.setValue method. (Note however that it is possible to change mappings in the associated map using put.)");

        addCompletion(provider, "TreeMap()", "TreeMap()", "java.util.TreeMap", "Constructs a new, empty tree map, using the natural ordering of its keys.");
        addCompletion(provider, "TreeMap(Comparator<? super K> comparator)", "TreeMap(comparator)", "java.util.TreeMap", "Constructs a new, empty tree map, ordered according to the given comparator.");
        addCompletion(provider, "TreeMap(Map<? extends K,? extends V> m)", "TreeMap(m)", "java.util.TreeMap", "Constructs a new tree map containing the same mappings as the given map, ordered according to the natural ordering of its keys.");
        addCompletion(provider, "TreeMap(SortedMap<K,? extends V> m)", "TreeMap(m)", "java.util.TreeMap", "Constructs a new tree map containing the same mappings and using the same ordering as the specified sorted map.");



        // Operace:
        addCompletion(provider, "compute(K key, BiFunction<? super K,? super V,? extends V> remappingFunction)", "compute(key, remappingFunction)", "default V", "Attempts to compute a mapping for the specified key and its current mapped value (or null if there is no current mapping).");
        addCompletion(provider, "computeIfAbsent(K key, Function<? super K,? extends V> mappingFunction)", "computeIfAbsent(key, mappingFunction)", "default V", "If the specified key is not already associated with a value (or is mapped to null), attempts to compute its value using the given mapping function and enters it into this map unless null.");
        addCompletion(provider, "computeIfPresent(K key, BiFunction<? super K,? super V,? extends V> remappingFunction)", "computeIfPresent(key, emappingFunction)", "default V", "If the value for the specified key is present and non-null, attempts to compute a new mapping given the key and its current mapped value.");
        addCompletion(provider, "containsValue(Object value)", "containsValue(value)", "boolean", "Returns true if this map maps one or more keys to the specified value.");
        addCompletion(provider, "entrySet()", "entrySet()", "Set<Map.Entry<K,V>>", "Returns a Set view of the mappings contained in this map.");
        addCompletion(provider, "forEach(BiConsumer<? super K,? super V> action)", "forEach(action)", "default void", "Performs the given action for each entry in this map until all entries have been processed or the action throws an exception.");
        addCompletion(provider, "getOrDefault(Object key, V defaultValue)", "getOrDefault(key, defaultValue)", "default V", "Returns the value to which the specified key is mapped, or defaultValue if this map contains no mapping for the key.");
        addCompletion(provider, "merge(K key, V value, BiFunction<? super V,? super V,? extends V> remappingFunction)", "merge(key, value, remappingFunction)", "default V", "If the specified key is not already associated with a value or is associated with null, associates it with the given non-null value.");
        addCompletion(provider, "putIfAbsent(K key, V value)", "putIfAbsent(key, value)", "default V", "If the specified key is not already associated with a value (or is mapped to null) associates it with the given value and returns null, else returns the current value.");
        addCompletion(provider, "remove(Object key, Object value)", "remove(key, value)", "default boolean", "Removes the entry for the specified key only if it is currently mapped to the specified value.");
        addCompletion(provider, "replace(K key, V value)", "replace(key, value)", "default V", "Replaces the entry for the specified key only if it is currently mapped to some value.");
        addCompletion(provider, "replace(K key, V oldValue, V newValue)", "replace(key, oldValue, newValue)", "default boolean", "Replaces the entry for the specified key only if currently mapped to the specified value.");
        addCompletion(provider, "replaceAll(BiFunction<? super K,? super V,? extends V> function)", "replaceAll(function)", "default void", "Replaces each entry's value with the result of invoking the given function on that entry until all entries have been processed or the function throws an exception.");















        // File (z java.io.File):
        addCompletion(provider, "File", "File", "java.io", "An abstract representation of file and directory pathnames. \r\n" +
                "User interfaces and operating systems use system-dependent pathname strings to name files and directories. This class presents an abstract, system-independent view of hierarchical pathnames. An abstract pathname has two components: \r\n" +
                "1. An optional system-dependent prefix string, such as a disk-drive specifier, \"/\" for the UNIX root directory, or \"\\\\\\\\\" for a Microsoft Windows UNC pathname, and \r\n" +
                "2. A sequence of zero or more string names. \r\n" +
                "The first name in an abstract pathname may be a directory name or, in the case of Microsoft Windows UNC pathnames, a hostname. Each subsequent name in an abstract pathname denotes a directory; the last name may denote either a directory or a file. The empty abstract pathname has no prefix and an empty name sequence. \r\n" +
                "The conversion of a pathname string to or from an abstract pathname is inherently system-dependent. When an abstract pathname is converted into a pathname string, each name is separated from the next by a single copy of the default separator character. The default name-separator character is defined by the system property file.separator, and is made available in the public static fields separator and separatorChar of this class. When a pathname string is converted into an abstract pathname, the names within it may be separated by the default name-separator character or by any other name-separator character that is supported by the underlying system. \r\n" +
                "\r\n" +
                "A pathname, whether abstract or in string form, may be either absolute or relative. An absolute pathname is complete in that no other information is required in order to locate the file that it denotes. A relative pathname, in contrast, must be interpreted in terms of information taken from some other pathname. By default the classes in the java.io package always resolve relative pathnames against the current user directory. This directory is named by the system property user.dir, and is typically the directory in which the Java virtual machine was invoked. \r\n" +
                "\r\n" +
                "The parent of an abstract pathname may be obtained by invoking the getParent() method of this class and consists of the pathname's prefix and each name in the pathname's name sequence except for the last. Each directory's absolute pathname is an ancestor of any File object with an absolute abstract pathname which begins with the directory's absolute pathname. For example, the directory denoted by the abstract pathname \"/usr\" is an ancestor of the directory denoted by the pathname \"/usr/local/bin\". \r\n" +
                "\r\n" +
                "The prefix concept is used to handle root directories on UNIX platforms, and drive specifiers, root directories and UNC pathnames on Microsoft Windows platforms, as follows: \r\n" +
                "• For UNIX platforms, the prefix of an absolute pathname is always \"/\". Relative pathnames have no prefix. The abstract pathname denoting the root directory has the prefix \"/\" and an empty name sequence. \r\n" +
                "• For Microsoft Windows platforms, the prefix of a pathname that contains a drive specifier consists of the drive letter followed by \":\" and possibly followed by \"\\\\\" if the pathname is absolute. The prefix of a UNC pathname is \"\\\\\\\\\"; the hostname and the share name are the first two names in the name sequence. A relative pathname that does not specify a drive has no prefix. \r\n" +
                "\r\n" +
                "Instances of this class may or may not denote an actual file-system object such as a file or a directory. If it does denote such an object then that object resides in a partition. A partition is an operating system-specific portion of storage for a file system. A single storage device (e.g. a physical disk-drive, flash memory, CD-ROM) may contain multiple partitions. The object, if any, will reside on the partition named by some ancestor of the absolute form of this pathname. \r\n" +
                "\r\n" +
                "A file system may implement restrictions to certain operations on the actual file-system object, such as reading, writing, and executing. These restrictions are collectively known as access permissions. The file system may have multiple sets of access permissions on a single object. For example, one set may apply to the object's owner, and another may apply to all other users. The access permissions on an object may cause some methods in this class to fail. \r\n" +
                "\r\n" +
                "Instances of the File class are immutable; that is, once created, the abstract pathname represented by a File object will never change. \r\n" +
                "\r\n" +
                "Interoperability with java.nio.file package\r\n" +
                "\r\n" +
                "The java.nio.file package defines interfaces and classes for the Java virtual machine to access files, file attributes, and file systems. This API may be used to overcome many of the limitations of the java.io.File class. The toPath method may be used to obtain a Path that uses the abstract path represented by a File object to locate a file. The resulting Path may be used with the Files class to provide more efficient and extensive access to additional file operations, file attributes, and I/O exceptions to help diagnose errors when an operation on a file fails.");


        // Field:
        addCompletion(provider, "pathSeparator", "pathSeparator", "String", "The system-dependent path-separator character, represented as a string for convenience.");
        addCompletion(provider, "pathSeparatorChar", "pathSeparatorChar", "char", "The system-dependent path-separator character.");
        addCompletion(provider, "separator", "separator", "String", "The system-dependent default name-separator character, represented as a string for convenience.");
        addCompletion(provider, "separatorChar", "separatorChar", "char", "The system-dependent default name-separator character.");

        // Konstruktory:
        addCompletion(provider, "File(File parent, String child)", "File(parent, child)", "java.io.File", "Creates a new File instance from a parent abstract pathname and a child pathname string.");
        addCompletion(provider, "File(String pathname)", "File(pathname)", "java.io.File", "Creates a new File instance by converting the given pathname string into an abstract pathname.");
        addCompletion(provider, "File(String parent, String child)", "File(parent, child)", "java.io.File", "Creates a new File instance from a parent pathname string and a child pathname string.");
        addCompletion(provider, "File(URI uri)", "File(uri)", "java.io.File", "Creates a new File instance by converting the given file: URI into an abstract pathname.");
        // Metody:
        addCompletion(provider, "canExecute()", "canExecute()", "boolean", "Tests whether the application can execute the file denoted by this abstract pathname.");
        addCompletion(provider, "canRead()", "canRead()", "boolean", "Tests whether the application can read the file denoted by this abstract pathname.");
        addCompletion(provider, "canWrite()", "canWrite()", "boolean", "Tests whether the application can modify the file denoted by this abstract pathname.");
        addCompletion(provider, "compareTo(File pathname)", "compareTo(pathname)", "int", "Compares two abstract pathnames lexicographically.");
        addCompletion(provider, "createNewFile()", "createNewFile()", "boolean", "Atomically creates a new, empty file named by this abstract pathname if and only if a file with this name does not yet exist.");
        addCompletion(provider, "createTempFile(String prefix, String suffix)", "createTempFile(prefix, suffix)", "static java.io.File", "Creates an empty file in the default temporary-file directory, using the given prefix and suffix to generate its name.");
        addCompletion(provider, "createTempFile(String prefix, String suffix, File directory)", "createTempFile(prefix, suffix, directory)", "static java.io.File", "Creates a new empty file in the specified directory, using the given prefix and suffix strings to generate its name.");
        addCompletion(provider, "delete()", "delete()", "boolean", "Deletes the file or directory denoted by this abstract pathname.");
        addCompletion(provider, "deleteOnExit()", "deleteOnExit()", "void", "Requests that the file or directory denoted by this abstract pathname be deleted when the virtual machine terminates.");
        addCompletion(provider, "equals(Object obj)", "equals(obj)", "boolean", "Tests this abstract pathname for equality with the given object.");
        addCompletion(provider, "exists()", "exists()", "boolean", "Tests whether the file or directory denoted by this abstract pathname exists.");
        addCompletion(provider, "getAbsoluteFile()", "getAbsoluteFile()", "java.io.File", "Returns the absolute form of this abstract pathname.");
        addCompletion(provider, "getAbsolutePath()", "getAbsolutePath()", "String", "Returns the absolute pathname string of this abstract pathname.");
        addCompletion(provider, "getCanonicalFile()", "getCanonicalFile()", "java.io.File", "Returns the canonical form of this abstract pathname.");
        addCompletion(provider, "getCanonicalPath()", "getCanonicalPath()", "String", "Returns the canonical pathname string of this abstract pathname.");
        addCompletion(provider, "getFreeSpace()", "getFreeSpace()", "long", "Returns the number of unallocated bytes in the partition named by this abstract path name.");
        addCompletion(provider, "getName()", "getName()", "String", "Returns the name of the file or directory denoted by this abstract pathname.");
        addCompletion(provider, "getParent()", "getParent()", "String", "Returns the pathname string of this abstract pathname's parent, or null if this pathname does not name a parent directory.");
        addCompletion(provider, "getParentFile()", "getParentFile()", "java.io.File", "Returns the abstract pathname of this abstract pathname's parent, or null if this pathname does not name a parent directory.");
        addCompletion(provider, "getPath()", "getPath()", "String", "Converts this abstract pathname into a pathname string.");
        addCompletion(provider, "getTotalSpace()", "getTotalSpace()", "long", "Returns the size of the partition named by this abstract pathname.");
        addCompletion(provider, "getUsableSpace()", "getUsableSpace()", "long", "Returns the number of bytes available to this virtual machine on the partition named by this abstract pathname.");
        addCompletion(provider, "hashCode()", "hashCode()", "int", "Computes a hash code for this abstract pathname.");
        addCompletion(provider, "isAbsolute()", "isAbsolute()", "boolean", "Tests whether this abstract pathname is absolute.");
        addCompletion(provider, "isDirectory()", "isDirectory()", "boolean", "Tests whether the file denoted by this abstract pathname is a directory.");
        addCompletion(provider, "isFile()", "isFile()", "boolean", "Tests whether the file denoted by this abstract pathname is a normal file.");
        addCompletion(provider, "isHidden()", "isHidden()", "boolean", "Tests whether the file named by this abstract pathname is a hidden file.");
        addCompletion(provider, "lastModified()", "lastModified()", "long", "Returns the time that the file denoted by this abstract pathname was last modified.");
        addCompletion(provider, "length()", "length()", "long", "Returns the length of the file denoted by this abstract pathname.");
        addCompletion(provider, "list()", "list()", "String[]", "Returns an array of strings naming the files and directories in the directory denoted by this abstract pathname.");
        addCompletion(provider, "list(FilenameFilter filter)", "list(filter)", "String[]", "Returns an array of strings naming the files and directories in the directory denoted by this abstract pathname that satisfy the specified filter.");
        addCompletion(provider, "listFiles()", "listFiles()", "java.io.File[]", "Returns an array of abstract pathnames denoting the files in the directory denoted by this abstract pathname.");
        addCompletion(provider, "listFiles(FileFilter filter)", "listFiles(filter)", "java.io.File[]", "Returns an array of abstract pathnames denoting the files and directories in the directory denoted by this abstract pathname that satisfy the specified filter.");
        addCompletion(provider, "listFiles(FilenameFilter filter)", "listFiles(filter)", "java.io.File", "Returns an array of abstract pathnames denoting the files and directories in the directory denoted by this abstract pathname that satisfy the specified filter.");
        addCompletion(provider, "listRoots()", "listRoots()", "java.io.File[]", "List the available filesystem roots.");
        addCompletion(provider, "mkdir()", "mkdir()", "boolean", "Creates the directory named by this abstract pathname.");
        addCompletion(provider, "mkdirs()", "mkdirs()", "boolean", "Creates the directory named by this abstract pathname, including any necessary but nonexistent parent directories.");
        addCompletion(provider, "renameTo(File dest)", "renameTo(dest)", "boolean", "Renames the file denoted by this abstract pathname.");
        addCompletion(provider, "setExecutable(boolean executable)", "setExecutable(executable)", "boolean", "A convenience method to set the owner's execute permission for this abstract pathname.");
        addCompletion(provider, "setExecutable(boolean executable, boolean ownerOnly)", "setExecutable(executable, ownerOnly)", "boolean", "Sets the owner's or everybody's execute permission for this abstract pathname.");
        addCompletion(provider, "setLastModified(long time)", "setLastModified(time)", "boolean", "Sets the last-modified time of the file or directory named by this abstract pathname.");
        addCompletion(provider, "setReadable(boolean readable)", "setReadable(readable)", "boolean", "A convenience method to set the owner's read permission for this abstract pathname.");
        addCompletion(provider, "setReadable(boolean readable, boolean ownerOnly)", "setReadable(readable, ownerOnly)", "boolean", "Sets the owner's or everybody's read permission for this abstract pathname.");
        addCompletion(provider, "setReadOnly()", "setReadOnly()", "boolean", "Marks the file or directory named by this abstract pathname so that only read operations are allowed.");
        addCompletion(provider, "setWritable(boolean writable)", "setWritable(writable)", "boolean", "A convenience method to set the owner's write permission for this abstract pathname.");
        addCompletion(provider, "setWritable(boolean writable, boolean ownerOnly)", "setWritable(writable, ownerOnly)", "boolean", "Sets the owner's or everybody's write permission for this abstract pathname.");
        addCompletion(provider, "toPath()", "toPath()", "java.nio.file.Path", "Returns a java.nio.file.Path object constructed from the this abstract path.");
        addCompletion(provider, "toString()", "toString()", "String", "Returns the pathname string of this abstract pathname.");
        addCompletion(provider, "toURI()", "toURI()", "java.net.URI", "Constructs a file: URI that represents this abstract pathname.");







        // Některé metody ke Collections:
        addCompletion(provider, "addAll(Collection<? super T> c, T... elements)", "addAll(c, elements)", "static <T> boolean", "Adds all of the specified elements to the specified collection.");
        addCompletion(provider, "asLifoQueue(Deque<T> deque)", "asLifoQueue(deque)", "static <T> Queue<T>", "Returns a view of a Deque as a Last-in-first-out (Lifo) Queue.");
        addCompletion(provider, "binarySearch(List<? extends Comparable<? super T>> list, T key)", "binarySearch(list, key)", "static <T> int", "Searches the specified list for the specified object using the binary search algorithm.");
        addCompletion(provider, "binarySearch(List<? extends T> list, T key, Comparator<? super T> c)", "binarySearch(list, key, c)", "static <T> int", "Searches the specified list for the specified object using the binary search algorithm.");
        addCompletion(provider, "checkedCollection(Collection<E> c, Class<E> type)", "checkedCollection(c, type)", "static <E> Collection<E>", "Returns a dynamically typesafe view of the specified collection.");
        addCompletion(provider, "checkedList(List<E> list, Class<E> type)", "checkedList(list, type)", "static <E> List<E>", "Returns a dynamically typesafe view of the specified list.");
        addCompletion(provider, "checkedMap(Map<K,V> m, Class<K> keyType, Class<V> valueType)", "checkedMap(m, keyType, valueType)", "static <K,V> Map<K,V>", "Returns a dynamically typesafe view of the specified map.");
        addCompletion(provider, "checkedSet(Set<E> s, Class<E> type)", "checkedSet(s, type)", "static <E> Set<E>", "Returns a dynamically typesafe view of the specified set.");
        addCompletion(provider, "checkedSortedMap(SortedMap<K,V> m, Class<K> keyType, Class<V> valueType)", "checkedSortedMap(m, keyType, valueType)", "static <K,V> SortedMap<K,V>", "Returns a dynamically typesafe view of the specified sorted map.");
        addCompletion(provider, "checkedSortedSet(SortedSet<E> s, Class<E> type)", "checkedSortedSet(s, type)", "static <E> SortedSet<E>", "Returns a dynamically typesafe view of the specified sorted set.");
        addCompletion(provider, "copy(List<? super T> dest, List<? extends T> src)", "copy(dest, src)", "static <T> void", "Copies all of the elements from one list into another.");
        addCompletion(provider, "disjoint(Collection<?> c1, Collection<?> c2)", "disjoint(c1, c2)", "static boolean", "Returns true if the two specified collections have no elements in common.");
        addCompletion(provider, "emptyEnumeration()", "emptyEnumeration()", "static <T> Enumeration<T>", "Returns an enumeration that has no elements.");
        addCompletion(provider, "emptyIterator()", "emptyIterator()", "static <T> Iterator<T>", "Returns an iterator that has no elements.");
        addCompletion(provider, "emptyList()", "emptyList()", "static <T> List<T>", "Returns the empty list (immutable).");
        addCompletion(provider, "emptyListIterator()", "emptyListIterator()", "static <T> ListIterator<T>", "Returns a list iterator that has no elements.");
        addCompletion(provider, "emptyMap()", "emptyMap()", "static <K,V> Map<K,V>", "Returns the empty map (immutable).");
        addCompletion(provider, "emptySet()", "emptySet()", "static <T> Set<T>", "Returns the empty set (immutable).");
        addCompletion(provider, "enumeration(Collection<T> c)", "enumeration(c)", "static <T> Enumeration<T>", "Returns an enumeration over the specified collection.");
        addCompletion(provider, "fill(List<? super T> list, T obj)", "fill(list, obj)", "static <T> void", "Replaces all of the elements of the specified list with the specified element.");
        addCompletion(provider, "frequency(Collection<?> c, Object o)", "frequency(c, o)", "static int", "Returns the number of elements in the specified collection equal to the specified object.");
        addCompletion(provider, "indexOfSubList(List<?> source, List<?> target)", "indexOfSubList(source, target)", "static int", "Returns the starting position of the first occurrence of the specified target list within the specified source list, or -1 if there is no such occurrence.");
        addCompletion(provider, "lastIndexOfSubList(List<?> source, List<?> target)", "lastIndexOfSubList(source, target)", "static int", "Returns the starting position of the last occurrence of the specified target list within the specified source list, or -1 if there is no such occurrence.");
        addCompletion(provider, "list(Enumeration<T> e)", "list(e)", "static <T> ArrayList<T>", "Returns an array list containing the elements returned by the specified enumeration in the order they are returned by the enumeration.");
        addCompletion(provider, "max(Collection<? extends T> coll)", "max(coll)", "static <T extends Object & Comparable<? super T>> T", "Returns the maximum element of the given collection, according to the natural ordering of its elements.");
        addCompletion(provider, "max(Collection<? extends T> coll, Comparator<? super T> comp)", "max(coll, comp)", "static <T> T", "Returns the maximum element of the given collection, according to the order induced by the specified comparator.");
        addCompletion(provider, "min(Collection<? extends T> coll)", "min(coll)", "static <T extends Object & Comparable<? super T>> T", "Returns the minimum element of the given collection, according to the natural ordering of its elements.");
        addCompletion(provider, "min(Collection<? extends T> coll, Comparator<? super T> comp)", "min(coll, comp)", "static <T> T", "Returns the minimum element of the given collection, according to the order induced by the specified comparator.");
        addCompletion(provider, "nCopies(int n, T o)", "nCopies(n, o)", "static <T> List<T>", "Returns an immutable list consisting of n copies of the specified object.");
        addCompletion(provider, "newSetFromMap(Map<E,Boolean> map)", "newSetFromMap(map)", "static <E> Set<E>", "Returns a set backed by the specified map.");
        addCompletion(provider, "replaceAll(List<T> list, T oldVal, T newVal)", "replaceAll(list, oldVal, newVal)", "static <T> boolean", "Replaces all occurrences of one specified value in a list with another.");
        addCompletion(provider, "reverse(List<?> list)", "reverse(list)", "static void", "Reverses the order of the elements in the specified list.");
        addCompletion(provider, "reverseOrder()", "reverseOrder()", "static <T> Comparator<T>", "Returns a comparator that imposes the reverse of the natural ordering on a collection of objects that implement the Comparable interface.");
        addCompletion(provider, "reverseOrder(Comparator<T> cmp)", "reverseOrder(cmp)", "static <T> Comparator<T>", "Returns a comparator that imposes the reverse ordering of the specified comparator.");
        addCompletion(provider, "rotate(List<?> list, int distance)", "rotate(list, distance)", "static void", "Rotates the elements in the specified list by the specified distance.");
        addCompletion(provider, "shuffle(List<?> list)", "shuffle(list)", "static void", "Randomly permutes the specified list using a default source of randomness.");
        addCompletion(provider, "shuffle(List<?> list, Random rnd)", "shuffle(list, rnd)", "static void", "Randomly permute the specified list using the specified source of randomness.");
        addCompletion(provider, "singleton(T o)", "singleton(o)", "static <T> Set<T>", "Returns an immutable set containing only the specified object.");
        addCompletion(provider, "singletonList(T o)", "singletonList(o)", "static <T> List<T>", "Returns an immutable list containing only the specified object.");
        addCompletion(provider, "singletonMap(K key, V value)", "singletonMap(key, value)", "static <K,V> Map<K,V>", "Returns an immutable map, mapping only the specified key to the specified value.");
        addCompletion(provider, "sort(List<T> list)", "sort(list)", "static <T extends Comparable<? super T>> void", "Sorts the specified list into ascending order, according to the natural ordering of its elements.");
        addCompletion(provider, "sort(List<T> list, Comparator<? super T> c)", "sort(list, c)", "static <T> void", "Sorts the specified list according to the order induced by the specified comparator.");
        addCompletion(provider, "swap(List<?> list, int i, int j)", "swap(list, i, j)", "static void", "Swaps the elements at the specified positions in the specified list.");
        addCompletion(provider, "synchronizedCollection(Collection<T> c)", "synchronizedCollection(c)", "static <T> Collection<T>", "Returns a synchronized (thread-safe) collection backed by the specified collection.");
        addCompletion(provider, "synchronizedList(List<T> list)", "synchronizedList(list)", "static <T> List<T>", "Returns a synchronized (thread-safe) list backed by the specified list.");
        addCompletion(provider, "synchronizedMap(Map<K,V> m)", "synchronizedMap(m)", "static <K,V> Map<K,V>", "Returns a synchronized (thread-safe) map backed by the specified map.");
        addCompletion(provider, "synchronizedSet(Set<T> s)", "synchronizedSet(s)", "static <T> Set<T>", "Returns a synchronized (thread-safe) set backed by the specified set.");
        addCompletion(provider, "synchronizedSortedMap(SortedMap<K,V> m)", "synchronizedSortedMap(m)", "static <K,V> SortedMap<K,V>", "Returns a synchronized (thread-safe) sorted map backed by the specified sorted map.");
        addCompletion(provider, "synchronizedSortedSet(SortedSet<T> s)", "synchronizedSortedSet(s)", "static <T> SortedSet<T>", "Returns a synchronized (thread-safe) sorted set backed by the specified sorted set.");
        addCompletion(provider, "unmodifiableCollection(Collection<? extends T> c)", "unmodifiableCollection(c)", "static <T> Collection<T>", "Returns an unmodifiable view of the specified collection.");
        addCompletion(provider, "unmodifiableList(List<? extends T> list)", "unmodifiableList(list)", "static <T> List<T>", "Returns an unmodifiable view of the specified list.");
        addCompletion(provider, "unmodifiableMap(Map<? extends K,? extends V> m)", "unmodifiableMap(m)", "static <K,V> Map<K,V>", "Returns an unmodifiable view of the specified map.");
        addCompletion(provider, "unmodifiableSet(Set<? extends T> s)", "unmodifiableSet(s)", "static <T> Set<T>", "Returns an unmodifiable view of the specified set.");
        addCompletion(provider, "unmodifiableSortedMap(SortedMap<K,? extends V> m)", "unmodifiableSortedMap(m)", "static <K,V> SortedMap<K,V>", "Returns an unmodifiable view of the specified sorted map.");
        addCompletion(provider, "unmodifiableSortedSet(SortedSet<T> s)", "unmodifiableSortedSet(s)", "static <T> SortedSet<T>", "Returns an unmodifiable view of the specified sorted set.");















        // Některé konstruktory pro listy:
        addCompletion(provider, "ArrayList()", "ArrayList()", "java.util.ArrayList<E>", "Constructs an empty list with an initial capacity of ten.");
        addCompletion(provider, "ArrayList(int initialCapacity)", "ArrayList(initialCapacity)", "java.util.ArrayList<E>", "Constructs an empty list with the specified initial capacity.");

        addCompletion(provider, "AttributeList()", "AttributeList()", "javax.management.AttributeList", "Constructs an empty AttributeList.");
        addCompletion(provider, "AttributeList(AttributeList list)", "AttributeList(list)", "javax.management.AttributeList", "Constructs an AttributeList containing the elements of the AttributeList specified, in the order in which they are returned by the AttributeList's iterator.");
        addCompletion(provider, "AttributeList(int initialCapacity)", "AttributeList(initialCapacity)", "javax.management.AttributeList", "Constructs an empty AttributeList with the initial capacity specified.");
        addCompletion(provider, "AttributeList(List<Attribute> list)", "AttributeList(list)", "javax.management.AttributeList", "Constructs an AttributeList containing the elements of the List specified, in the order in which they are returned by the List's iterator.");

        addCompletion(provider, "CopyOnWriteArrayList()", "CopyOnWriteArrayList()", "java.util.concurrent.CopyOnWriteArrayList<E>", "Creates an empty list.");
        addCompletion(provider, "CopyOnWriteArrayList(Collection<? extends E> c)", "CopyOnWriteArrayList(c)", "java.util.concurrent.CopyOnWriteArrayList<E>", "Creates a list containing the elements of the specified collection, in the order they are returned by the collection's iterator.");
        addCompletion(provider, "CopyOnWriteArrayList(E[] toCopyIn)", "CopyOnWriteArrayList(toCopyIn)", "java.util.concurrent.CopyOnWriteArrayList<E>", "Creates a list holding a copy of the given array.");

        addCompletion(provider, "LinkedList()", "LinkedList()", "java.util.LinkedList<E>", "Constructs an empty list.");
        addCompletion(provider, "LinkedList(Collection<? extends E> c)", "LinkedList(c)", "java.util.LinkedList<E>", "Constructs a list containing the elements of the specified collection, in the order they are returned by the collection's iterator.");

        addCompletion(provider, "RoleList()", "RoleList()", "javax.management.relation.RoleList", "Constructs an empty RoleList.");
        addCompletion(provider, "RoleList(int initialCapacity)", "RoleList(initialCapacity)", "javax.management.relation.RoleList", "Constructs an empty RoleList with the initial capacity specified.");
        addCompletion(provider, "RoleList(List<Role> list)", "RoleList(list)", "javax.management.relation.RoleList", "Constructs a RoleList containing the elements of the List specified, in the order in which they are returned by the List's iterator.");

        addCompletion(provider, "RoleUnresolvedList()", "RoleUnresolvedList()", "javax.management.relation.RoleUnresolvedList", "Constructs an empty RoleUnresolvedList.");
        addCompletion(provider, "RoleUnresolvedList(int initialCapacity)", "RoleUnresolvedList(initialCapacity)", "javax.management.relation.RoleUnresolvedList", "Constructs an empty RoleUnresolvedList with the initial capacity specified.");
        addCompletion(provider, "RoleUnresolvedList(List<RoleUnresolved> list)", "RoleUnresolvedList(list)", "javax.management.relation.RoleUnresolvedList", "Constructs a RoleUnresolvedList containing the elements of the List specified, in the order in which they are returned by the List's iterator.");

        addCompletion(provider, "Stack()", "Stack()", "java.util.Stack<E>", "Creates an empty Stack.");

        addCompletion(provider, "Vector()", "Vector()", "java.util.Vector<E>", "Constructs an empty vector so that its internal data array has size 10 and its standard capacity increment is zero.");
        addCompletion(provider, "Vector(Collection<? extends E> c)", "Vector(c)", "java.util.Vector<E>", "Constructs a vector containing the elements of the specified collection, in the order they are returned by the collection's iterator.");
        addCompletion(provider, "Vector(int initialCapacity)", "Vector(initialCapacity)", "java.util.Vector<E>", "Constructs an empty vector with the specified initial capacity and with its capacity increment equal to zero.");
        addCompletion(provider, "Vector(int initialCapacity, int capacityIncrement)", "Vector(initialCapacity, capacityIncrement)", "java.util.Vector<E>", "Constructs an empty vector with the specified initial capacity and capacity increment.");

















        // Kolekce:
        addCompletion(provider, "Collection", "Collection<E>", "java.util", "The root interface in the collection hierarchy. A collection represents a group of objects, known as its elements. Some collections allow duplicate elements and others do not. Some are ordered and others unordered. The JDK does not provide any direct implementations of this interface: it provides implementations of more specific subinterfaces like Set and List. This interface is typically used to pass collections around and manipulate them where maximum generality is desired. \r\n" +
                "Bags or multisets (unordered collections that may contain duplicate elements) should implement this interface directly. \r\n" +
                "\r\n" +
                "All general-purpose Collection implementation classes (which typically implement Collection indirectly through one of its subinterfaces) should provide two \"standard\" constructors: a void (no arguments) constructor, which creates an empty collection, and a constructor with a single argument of type Collection, which creates a new collection with the same elements as its argument. In effect, the latter constructor allows the user to copy any collection, producing an equivalent collection of the desired implementation type. There is no way to enforce this convention (as interfaces cannot contain constructors) but all of the general-purpose Collection implementations in the Java platform libraries comply. \r\n" +
                "\r\n" +
                "The \"destructive\" methods contained in this interface, that is, the methods that modify the collection on which they operate, are specified to throw UnsupportedOperationException if this collection does not support the operation. If this is the case, these methods may, but are not required to, throw an UnsupportedOperationException if the invocation would have no effect on the collection. For example, invoking the addAll(Collection) method on an unmodifiable collection may, but is not required to, throw the exception if the collection to be added is empty. \r\n" +
                "\r\n" +
                " Some collection implementations have restrictions on the elements that they may contain. For example, some implementations prohibit null elements, and some have restrictions on the types of their elements. Attempting to add an ineligible element throws an unchecked exception, typically NullPointerException or ClassCastException. Attempting to query the presence of an ineligible element may throw an exception, or it may simply return false; some implementations will exhibit the former behavior and some will exhibit the latter. More generally, attempting an operation on an ineligible element whose completion would not result in the insertion of an ineligible element into the collection may throw an exception or it may succeed, at the option of the implementation. Such exceptions are marked as \"optional\" in the specification for this interface. \r\n" +
                "\r\n" +
                "It is up to each collection to determine its own synchronization policy. In the absence of a stronger guarantee by the implementation, undefined behavior may result from the invocation of any method on a collection that is being mutated by another thread; this includes direct invocations, passing the collection to a method that might perform invocations, and using an existing iterator to examine the collection. \r\n" +
                "\r\n" +
                "Many methods in Collections Framework interfaces are defined in terms of the equals method. For example, the specification for the contains(Object o) method says: \"returns true if and only if this collection contains at least one element e such that (o==null ? e==null : o.equals(e)).\" This specification should not be construed to imply that invoking Collection.contains with a non-null argument o will cause o.equals(e) to be invoked for any element e. Implementations are free to implement optimizations whereby the equals invocation is avoided, for example, by first comparing the hash codes of the two elements. (The Object.hashCode() specification guarantees that two objects with unequal hash codes cannot be equal.) More generally, implementations of the various Collections Framework interfaces are free to take advantage of the specified behavior of underlying Object methods wherever the implementor deems it appropriate. \r\n" +
                "\r\n" +
                "Some collection operations which perform recursive traversal of the collection may fail with an exception for self-referential instances where the collection directly or indirectly contains itself. This includes the clone(), equals(), hashCode() and toString() methods. Implementations may optionally handle the self-referential scenario, however most current implementations do not do so. \r\n");

        addCompletion(provider, "AbstractList", "AbstractList<E>", "java.util", "This class provides a skeletal implementation of the List interface to minimize the effort required to implement this interface backed by a \"random access\" data store (such as an array). For sequential access data (such as a linked list), AbstractSequentialList should be used in preference to this class. \r\n" +
                "To implement an unmodifiable list, the programmer needs only to extend this class and provide implementations for the get(int) and size() methods. \r\n" +
                "\r\n" +
                "To implement a modifiable list, the programmer must additionally override the set(int, E) method (which otherwise throws an UnsupportedOperationException). If the list is variable-size the programmer must additionally override the add(int, E) and remove(int) methods. \r\n" +
                "\r\n" +
                "The programmer should generally provide a void (no argument) and collection constructor, as per the recommendation in the Collection interface specification. \r\n" +
                "\r\n" +
                "Unlike the other abstract collection implementations, the programmer does not have to provide an iterator implementation; the iterator and list iterator are implemented by this class, on top of the \"random access\" methods: get(int), set(int, E), add(int, E) and remove(int). \r\n" +
                "\r\n" +
                "The documentation for each non-abstract method in this class describes its implementation in detail. Each of these methods may be overridden if the collection being implemented admits a more efficient implementation. \r\n" +
                "\r\n" +
                "This class is a member of the  Java Collections Framework.\r\n");

        addCompletion(provider, "AbstractSequentialList", "AbstractSequentialList<E>", "java.util", "This class provides a skeletal implementation of the List interface to minimize the effort required to implement this interface backed by a \"sequential access\" data store (such as a linked list). For random access data (such as an array), AbstractList should be used in preference to this class.\r\n" +
                "This class is the opposite of the AbstractList class in the sense that it implements the \"random access\" methods (get(int index), set(int index, E element), add(int index, E element) and remove(int index)) on top of the list's list iterator, instead of the other way around.\r\n" +
                "\r\n" +
                "To implement a list the programmer needs only to extend this class and provide implementations for the listIterator and size methods. For an unmodifiable list, the programmer need only implement the list iterator's hasNext, next, hasPrevious, previous and index methods.\r\n" +
                "\r\n" +
                "For a modifiable list the programmer should additionally implement the list iterator's set method. For a variable-size list the programmer should additionally implement the list iterator's remove and add methods.\r\n" +
                "\r\n" +
                "The programmer should generally provide a void (no argument) and collection constructor, as per the recommendation in the Collection interface specification.\r\n" +
                "\r\n" +
                "This class is a member of the  Java Collections Framework.\r\n");

        addCompletion(provider, "ArrayList", "ArrayList<E>", "java.util", "Resizable-array implementation of the List interface. Implements all optional list operations, and permits all elements, including null. In addition to implementing the List interface, this class provides methods to manipulate the size of the array that is used internally to store the list. (This class is roughly equivalent to Vector, except that it is unsynchronized.) \r\n" +
                "The size, isEmpty, get, set, iterator, and listIterator operations run in constant time. The add operation runs in amortized constant time, that is, adding n elements requires O(n) time. All of the other operations run in linear time (roughly speaking). The constant factor is low compared to that for the LinkedList implementation. \r\n" +
                "\r\n" +
                "Each ArrayList instance has a capacity. The capacity is the size of the array used to store the elements in the list. It is always at least as large as the list size. As elements are added to an ArrayList, its capacity grows automatically. The details of the growth policy are not specified beyond the fact that adding an element has constant amortized time cost. \r\n" +
                "\r\n" +
                "An application can increase the capacity of an ArrayList instance before adding a large number of elements using the ensureCapacity operation. This may reduce the amount of incremental reallocation. \r\n" +
                "\r\n" +
                "Note that this implementation is not synchronized. If multiple threads access an ArrayList instance concurrently, and at least one of the threads modifies the list structurally, it must be synchronized externally. (A structural modification is any operation that adds or deletes one or more elements, or explicitly resizes the backing array; merely setting the value of an element is not a structural modification.) This is typically accomplished by synchronizing on some object that naturally encapsulates the list. If no such object exists, the list should be \"wrapped\" using the Collections.synchronizedList method. This is best done at creation time, to prevent accidental unsynchronized access to the list:\r\n" +
                "   List list = Collections.synchronizedList(new ArrayList(...));\r\n" +
                "\r\n" +
                " The iterators returned by this class's iterator and listIterator methods are fail-fast: if the list is structurally modified at any time after the iterator is created, in any way except through the iterator's own remove or add methods, the iterator will throw a ConcurrentModificationException. Thus, in the face of concurrent modification, the iterator fails quickly and cleanly, rather than risking arbitrary, non-deterministic behavior at an undetermined time in the future. \r\n" +
                "\r\n" +
                "Note that the fail-fast behavior of an iterator cannot be guaranteed as it is, generally speaking, impossible to make any hard guarantees in the presence of unsynchronized concurrent modification. Fail-fast iterators throw ConcurrentModificationException on a best-effort basis. Therefore, it would be wrong to write a program that depended on this exception for its correctness: the fail-fast behavior of iterators should be used only to detect bugs. \r\n" +
                "\r\n" +
                "This class is a member of the  Java Collections Framework.\r\n");

        addCompletion(provider, "AttributeList", "AttributeList", "java.util", "\r\n" +
                "Represents a list of values for attributes of an MBean. See the getAttributes and setAttributes methods of MBeanServer and MBeanServerConnection.\r\n" +
                "\r\n" +
                "For compatibility reasons, it is possible, though highly discouraged, to add objects to an AttributeList that are not instances of Attribute. However, an AttributeList can be made type-safe, which means that an attempt to add an object that is not an Attribute will produce an IllegalArgumentException. An AttributeList becomes type-safe when the method asList() is called on it.\r\n");

        addCompletion(provider, "CopyOnWriteArrayList", "CopyOnWriteArrayList<E>", "java.util.concurrent", "A thread-safe variant of ArrayList in which all mutative operations (add, set, and so on) are implemented by making a fresh copy of the underlying array. \r\n" +
                "This is ordinarily too costly, but may be more efficient than alternatives when traversal operations vastly outnumber mutations, and is useful when you cannot or don't want to synchronize traversals, yet need to preclude interference among concurrent threads. The \"snapshot\" style iterator method uses a reference to the state of the array at the point that the iterator was created. This array never changes during the lifetime of the iterator, so interference is impossible and the iterator is guaranteed not to throw ConcurrentModificationException. The iterator will not reflect additions, removals, or changes to the list since the iterator was created. Element-changing operations on iterators themselves (remove, set, and add) are not supported. These methods throw UnsupportedOperationException. \r\n" +
                "\r\n" +
                "All elements are permitted, including null. \r\n" +
                "\r\n" +
                "Memory consistency effects: As with other concurrent collections, actions in a thread prior to placing an object into a CopyOnWriteArrayList happen-before actions subsequent to the access or removal of that element from the CopyOnWriteArrayList in another thread. \r\n" +
                "\r\n" +
                "This class is a member of the  Java Collections Framework.\r\n");

        addCompletion(provider, "LinkedList", "LinkedList<E>", "java.util", "Doubly-linked list implementation of the List and Deque interfaces. Implements all optional list operations, and permits all elements (including null). \r\n" +
                "All of the operations perform as could be expected for a doubly-linked list. Operations that index into the list will traverse the list from the beginning or the end, whichever is closer to the specified index. \r\n" +
                "\r\n" +
                "Note that this implementation is not synchronized. If multiple threads access a linked list concurrently, and at least one of the threads modifies the list structurally, it must be synchronized externally. (A structural modification is any operation that adds or deletes one or more elements; merely setting the value of an element is not a structural modification.) This is typically accomplished by synchronizing on some object that naturally encapsulates the list. If no such object exists, the list should be \"wrapped\" using the Collections.synchronizedList method. This is best done at creation time, to prevent accidental unsynchronized access to the list:\r\n" +
                "   List list = Collections.synchronizedList(new LinkedList(...));\r\n" +
                "\r\n" +
                "The iterators returned by this class's iterator and listIterator methods are fail-fast: if the list is structurally modified at any time after the iterator is created, in any way except through the Iterator's own remove or add methods, the iterator will throw a ConcurrentModificationException. Thus, in the face of concurrent modification, the iterator fails quickly and cleanly, rather than risking arbitrary, non-deterministic behavior at an undetermined time in the future. \r\n" +
                "\r\n" +
                "Note that the fail-fast behavior of an iterator cannot be guaranteed as it is, generally speaking, impossible to make any hard guarantees in the presence of unsynchronized concurrent modification. Fail-fast iterators throw ConcurrentModificationException on a best-effort basis. Therefore, it would be wrong to write a program that depended on this exception for its correctness: the fail-fast behavior of iterators should be used only to detect bugs. \r\n" +
                "\r\n" +
                "This class is a member of the  Java Collections Framework.\r\n");

        addCompletion(provider, "RoleList", "RoleList", "javax.management.relation", "Doubly-linked list implementation of the List and Deque interfaces. Implements all optional list operations, and permits all elements (including null). \r\n" +
                "All of the operations perform as could be expected for a doubly-linked list. Operations that index into the list will traverse the list from the beginning or the end, whichever is closer to the specified index. \r\n" +
                "\r\n" +
                "Note that this implementation is not synchronized. If multiple threads access a linked list concurrently, and at least one of the threads modifies the list structurally, it must be synchronized externally. (A structural modification is any operation that adds or deletes one or more elements; merely setting the value of an element is not a structural modification.) This is typically accomplished by synchronizing on some object that naturally encapsulates the list. If no such object exists, the list should be \"wrapped\" using the Collections.synchronizedList method. This is best done at creation time, to prevent accidental unsynchronized access to the list:\r\n" +
                "   List list = Collections.synchronizedList(new LinkedList(...));\r\n" +
                "\r\n" +
                "The iterators returned by this class's iterator and listIterator methods are fail-fast: if the list is structurally modified at any time after the iterator is created, in any way except through the Iterator's own remove or add methods, the iterator will throw a ConcurrentModificationException. Thus, in the face of concurrent modification, the iterator fails quickly and cleanly, rather than risking arbitrary, non-deterministic behavior at an undetermined time in the future. \r\n" +
                "\r\n" +
                "Note that the fail-fast behavior of an iterator cannot be guaranteed as it is, generally speaking, impossible to make any hard guarantees in the presence of unsynchronized concurrent modification. Fail-fast iterators throw ConcurrentModificationException on a best-effort basis. Therefore, it would be wrong to write a program that depended on this exception for its correctness: the fail-fast behavior of iterators should be used only to detect bugs. \r\n" +
                "\r\n" +
                "This class is a member of the  Java Collections Framework.\r\n");

        addCompletion(provider, "RoleUnresolvedList", "RoleUnresolvedList", "javax.management.relation", "Doubly-linked list implementation of the List and Deque interfaces. Implements all optional list operations, and permits all elements (including null). \r\n" +
                "All of the operations perform as could be expected for a doubly-linked list. Operations that index into the list will traverse the list from the beginning or the end, whichever is closer to the specified index. \r\n" +
                "\r\n" +
                "Note that this implementation is not synchronized. If multiple threads access a linked list concurrently, and at least one of the threads modifies the list structurally, it must be synchronized externally. (A structural modification is any operation that adds or deletes one or more elements; merely setting the value of an element is not a structural modification.) This is typically accomplished by synchronizing on some object that naturally encapsulates the list. If no such object exists, the list should be \"wrapped\" using the Collections.synchronizedList method. This is best done at creation time, to prevent accidental unsynchronized access to the list:\r\n" +
                "   List list = Collections.synchronizedList(new LinkedList(...));\r\n" +
                "\r\n" +
                "The iterators returned by this class's iterator and listIterator methods are fail-fast: if the list is structurally modified at any time after the iterator is created, in any way except through the Iterator's own remove or add methods, the iterator will throw a ConcurrentModificationException. Thus, in the face of concurrent modification, the iterator fails quickly and cleanly, rather than risking arbitrary, non-deterministic behavior at an undetermined time in the future. \r\n" +
                "\r\n" +
                "Note that the fail-fast behavior of an iterator cannot be guaranteed as it is, generally speaking, impossible to make any hard guarantees in the presence of unsynchronized concurrent modification. Fail-fast iterators throw ConcurrentModificationException on a best-effort basis. Therefore, it would be wrong to write a program that depended on this exception for its correctness: the fail-fast behavior of iterators should be used only to detect bugs. \r\n" +
                "\r\n" +
                "This class is a member of the  Java Collections Framework.\r\n");

        addCompletion(provider, "Stack", "Stack<E>", "java.util", "The Stack class represents a last-in-first-out (LIFO) stack of objects. It extends class Vector with five operations that allow a vector to be treated as a stack. The usual push and pop operations are provided, as well as a method to peek at the top item on the stack, a method to test for whether the stack is empty, and a method to search the stack for an item and discover how far it is from the top. \r\n" +
                "When a stack is first created, it contains no items. \r\n" +
                "\r\n" +
                "A more complete and consistent set of LIFO stack operations is provided by the Deque interface and its implementations, which should be used in preference to this class. For example: \r\n" +
                "   \r\n" +
                "   Deque<Integer> stack = new ArrayDeque<Integer>();\r\n");

        addCompletion(provider, "Vector", "Vector<E>", "java.util", "The Vector class implements a growable array of objects. Like an array, it contains components that can be accessed using an integer index. However, the size of a Vector can grow or shrink as needed to accommodate adding and removing items after the Vector has been created. \r\n" +
                "Each vector tries to optimize storage management by maintaining a capacity and a capacityIncrement. The capacity is always at least as large as the vector size; it is usually larger because as components are added to the vector, the vector's storage increases in chunks the size of capacityIncrement. An application can increase the capacity of a vector before inserting a large number of components; this reduces the amount of incremental reallocation. \r\n" +
                "\r\n" +
                " The iterators returned by this class's iterator and listIterator methods are fail-fast: if the vector is structurally modified at any time after the iterator is created, in any way except through the iterator's own remove or add methods, the iterator will throw a ConcurrentModificationException. Thus, in the face of concurrent modification, the iterator fails quickly and cleanly, rather than risking arbitrary, non-deterministic behavior at an undetermined time in the future. The Enumerations returned by the elements method are not fail-fast. \r\n" +
                "\r\n" +
                "Note that the fail-fast behavior of an iterator cannot be guaranteed as it is, generally speaking, impossible to make any hard guarantees in the presence of unsynchronized concurrent modification. Fail-fast iterators throw ConcurrentModificationException on a best-effort basis. Therefore, it would be wrong to write a program that depended on this exception for its correctness: the fail-fast behavior of iterators should be used only to detect bugs. \r\n" +
                "\r\n" +
                "As of the Java 2 platform v1.2, this class was retrofitted to implement the List interface, making it a member of the  Java Collections Framework. Unlike the new collection implementations, Vector is synchronized. If a thread-safe implementation is not needed, it is recommended to use ArrayList in place of Vector.\r\n");















        // Puze některé balíčky z balíčku sun::
        final Package[] packaes = Package.getPackages();

        Arrays.stream(packaes).filter(p -> p.getName().startsWith("sun.")).forEach(p -> addCompletion(provider,
                p.getName(), p.getName() + ".*;"));









        createListeners();

        // Nějaké listenery:
        addCompletion(provider, "ComponentListener()", COMPONENT_LISTENER,
                "Anonymous Inner Type - java.awt.event",
                "The listener interface for receiving component events. The class that is interested in processing a component event either implements this interface (and all the methods it contains) or extends the abstract ComponentAdapter class (overriding only the methods of interest). The listener object created from that class is then registered with a component using the component's addComponentListener method. When the component's size, location, or visibility changes, the relevant method in the listener object is invoked, and the ComponentEvent is passed to it. \r\n"
                        + "Component events are provided for notification purposes ONLY; The AWT will automatically handle component moves and resizes internally so that GUI layout works properly regardless of whether a program registers a ComponentListener or not.");

        addCompletion(provider, "ComponentAdapter()", COMPONENT_ADAPTER,
                "Anonymous Inner Type - java.awt.event",
                "An abstract adapter class for receiving component events. The methods in this class are empty. This class exists as convenience for creating listener objects. \r\n"
                        + "Extend this class to create a ComponentEvent listener and override the methods for the events of interest. (If you implement the ComponentListener interface, you have to define all of the methods in it. This abstract class defines null methods for them all, so you can only have to define methods for events you care about.) \r\n"
                        + "\r\n"
                        + "Create a listener object using your class and then register it with a component using the component's addComponentListener method. When the component's size, location, or visibility changes, the relevant method in the listener object is invoked, and the ComponentEvent is passed to it.");




        addCompletion(provider, "FocusListener()", FOCUS_LISTENER,
                "Anonymous Inner Type - java.awt.event",
                "The listener interface for receiving keyboard focus events on a component. The class that is interested in processing a focus event either implements this interface (and all the methods it contains) or extends the abstract FocusAdapter class (overriding only the methods of interest). The listener object created from that class is then registered with a component using the component's addFocusListener method. When the component gains or loses the keyboard focus, the relevant method in the listener object is invoked, and the FocusEvent is passed to it.");

        addCompletion(provider, "FocusAdapter()", FOCUS_ADAPTER,
                "Anonymous Inner Type - java.awt.event",
                "An abstract adapter class for receiving keyboard focus events. The methods in this class are empty. This class exists as convenience for creating listener objects. \r\n"
                        + "Extend this class to create a FocusEvent listener and override the methods for the events of interest. (If you implement the FocusListener interface, you have to define all of the methods in it. This abstract class defines null methods for them all, so you can only have to define methods for events you care about.) \r\n"
                        + "\r\n"
                        + "Create a listener object using the extended class and then register it with a component using the component's addFocusListener method. When the component gains or loses the keyboard focus, the relevant method in the listener object is invoked, and the FocusEvent is passed to it.");





        addCompletion(provider, "KeyListener()", KEY_LISTENER,
                "Anonymous Inner Type - java.awt.event",
                "The listener interface for receiving keyboard events (keystrokes). The class that is interested in processing a keyboard event either implements this interface (and all the methods it contains) or extends the abstract KeyAdapter class (overriding only the methods of interest). \r\n"
                        + "The listener object created from that class is then registered with a component using the component's addKeyListener method. A keyboard event is generated when a key is pressed, released, or typed. The relevant method in the listener object is then invoked, and the KeyEvent is passed to it.");

        addCompletion(provider, "KeyAdapter()", KEY_ADAPTER,
                "Anonymous Inner Type - java.awt.event",
                "An abstract adapter class for receiving keyboard events. The methods in this class are empty. This class exists as convenience for creating listener objects. \r\n"
                        + "Extend this class to create a KeyEvent listener and override the methods for the events of interest. (If you implement the KeyListener interface, you have to define all of the methods in it. This abstract class defines null methods for them all, so you can only have to define methods for events you care about.) \r\n"
                        + "\r\n"
                        + "Create a listener object using the extended class and then register it with a component using the component's addKeyListener method. When a key is pressed, released, or typed, the relevant method in the listener object is invoked, and the KeyEvent is passed to it.");






        addCompletion(provider, "MouseListener()", MOUSE_LISTENER,
                "Anonymous Inner Type - java.awt.event",
                "The listener interface for receiving \"interesting\" mouse events (press, release, click, enter, and exit) on a component. (To track mouse moves and mouse drags, use the MouseMotionListener.) \r\n"
                        + "The class that is interested in processing a mouse event either implements this interface (and all the methods it contains) or extends the abstract MouseAdapter class (overriding only the methods of interest). \r\n"
                        + "\r\n"
                        + "The listener object created from that class is then registered with a component using the component's addMouseListener method. A mouse event is generated when the mouse is pressed, released clicked (pressed and released). A mouse event is also generated when the mouse cursor enters or leaves a component. When a mouse event occurs, the relevant method in the listener object is invoked, and the MouseEvent is passed to it.");

        addCompletion(provider, "MouseAdapter()", MOUSE_ADAPTER,
                "Anonymous Inner Type - java.awt.event",
                "An abstract adapter class for receiving mouse events. The methods in this class are empty. This class exists as convenience for creating listener objects. \r\n"
                        + "Mouse events let you track when a mouse is pressed, released, clicked, moved, dragged, when it enters a component, when it exits and when a mouse wheel is moved. \r\n"
                        + "\r\n"
                        + "Extend this class to create a MouseEvent (including drag and motion events) or/and MouseWheelEvent listener and override the methods for the events of interest. (If you implement the MouseListener, MouseMotionListener interface, you have to define all of the methods in it. This abstract class defines null methods for them all, so you can only have to define methods for events you care about.) \r\n"
                        + "\r\n"
                        + "Create a listener object using the extended class and then register it with a component using the component's addMouseListener addMouseMotionListener, addMouseWheelListener methods. The relevant method in the listener object is invoked and the MouseEvent or MouseWheelEvent is passed to it in following cases: \r\n"
                        + "•when a mouse button is pressed, released, or clicked (pressed and released) \r\n"
                        + "•when the mouse cursor enters or exits the component \r\n"
                        + "•when the mouse wheel rotated, or mouse moved or dragged");






        addCompletion(provider, "MouseMotionListener()", MOUSE_MOTION_LISTENER,
                "Anonymous Inner Type - java.awt.event",
                "The listener interface for receiving mouse motion events on a component. (For clicks and other mouse events, use the MouseListener.) \r\n"
                        + "The class that is interested in processing a mouse motion event either implements this interface (and all the methods it contains) or extends the abstract MouseMotionAdapter class (overriding only the methods of interest). \r\n"
                        + "\r\n"
                        + "The listener object created from that class is then registered with a component using the component's addMouseMotionListener method. A mouse motion event is generated when the mouse is moved or dragged. (Many such events will be generated). When a mouse motion event occurs, the relevant method in the listener object is invoked, and the MouseEvent is passed to it.");

        addCompletion(provider, "MouseMotionAdapter()", MOUSE_MOTION_ADAPTER,
                "Anonymous Inner Type - java.awt.event",
                "An abstract adapter class for receiving mouse motion events. The methods in this class are empty. This class exists as convenience for creating listener objects. \r\n"
                        + "Mouse motion events occur when a mouse is moved or dragged. (Many such events will be generated in a normal program. To track clicks and other mouse events, use the MouseAdapter.) \r\n"
                        + "\r\n"
                        + "Extend this class to create a MouseEvent listener and override the methods for the events of interest. (If you implement the MouseMotionListener interface, you have to define all of the methods in it. This abstract class defines null methods for them all, so you can only have to define methods for events you care about.) \r\n"
                        + "\r\n"
                        + "Create a listener object using the extended class and then register it with a component using the component's addMouseMotionListener method. When the mouse is moved or dragged, the relevant method in the listener object is invoked and the MouseEvent is passed to it.");









        addCompletion(provider, "MouseWheelListener()", MOUSE_WHEEL_LISTENER,
                "Anonymous Inner Type - java.awt.event",
                "The listener interface for receiving mouse wheel events on a component. (For clicks and other mouse events, use the MouseListener. For mouse movement and drags, use the MouseMotionListener.) \r\n"
                        + "The class that is interested in processing a mouse wheel event implements this interface (and all the methods it contains). \r\n"
                        + "\r\n"
                        + "The listener object created from that class is then registered with a component using the component's addMouseWheelListener method. A mouse wheel event is generated when the mouse wheel is rotated. When a mouse wheel event occurs, that object's mouseWheelMoved method is invoked.");









        addCompletion(provider, "HierarchyListener()", HIERARCHY_LISTENER,
                "Anonymous Inner Type - java.awt.event",
                "The listener interface for receiving hierarchy changed events. The class that is interested in processing a hierarchy changed event should implement this interface. The listener object created from that class is then registered with a Component using the Component's addHierarchyListener method. When the hierarchy to which the Component belongs changes, the hierarchyChanged method in the listener object is invoked, and the HierarchyEvent is passed to it. \r\n"
                        + "Hierarchy events are provided for notification purposes ONLY; The AWT will automatically handle changes to the hierarchy internally so that GUI layout, displayability, and visibility work properly regardless of whether a program registers a HierarchyListener or not.");






        addCompletion(provider, "HierarchyBoundsListener()", HIERARCHY_BOUNDS_LISTENER,
                "Anonymous Inner Type - java.awt.event",
                "The listener interface for receiving ancestor moved and resized events. The class that is interested in processing these events either implements this interface (and all the methods it contains) or extends the abstract HierarchyBoundsAdapter class (overriding only the method of interest). The listener object created from that class is then registered with a Component using the Component's addHierarchyBoundsListener method. When the hierarchy to which the Component belongs changes by the resizing or movement of an ancestor, the relevant method in the listener object is invoked, and the HierarchyEvent is passed to it. \r\n"
                        + "Hierarchy events are provided for notification purposes ONLY; The AWT will automatically handle changes to the hierarchy internally so that GUI layout works properly regardless of whether a program registers an HierarchyBoundsListener or not.");

        addCompletion(provider, "HierarchyBoundsAdapter()", HIERARCHY_BOUNDS_ADAPTER,
                "Anonymous Inner Type - java.awt.event",
                "An abstract adapter class for receiving ancestor moved and resized events. The methods in this class are empty. This class exists as a convenience for creating listener objects. \r\n"
                        + "Extend this class and override the method for the event of interest. (If you implement the HierarchyBoundsListener interface, you have to define both methods in it. This abstract class defines null methods for them both, so you only have to define the method for the event you care about.) \r\n"
                        + "\r\n"
                        + "Create a listener object using your class and then register it with a Component using the Component's addHierarchyBoundsListener method. When the hierarchy to which the Component belongs changes by resize or movement of an ancestor, the relevant method in the listener object is invoked, and the HierarchyEvent is passed to it.");




        addCompletion(provider, "ActionListener()", ACTION_LISTENER,
                "Anonymous Inner Type - java.awt.event",
                "The listener interface for receiving action events. The class that is interested in processing an action event implements this interface, and the object created with that class is registered with a component, using the component's addActionListener method. When the action event occurs, that object's actionPerformed method is invoked.");







        // Ukázky cyklů:

        createLoops();

        addCompletion(provider, "loop for", PREPARED_LOOP_FOR, loopForInfo, loopForPreparedSummary + "\n" + PREPARED_LOOP_FOR);

        addCompletion(provider, "loop for", LOOP_FOR, loopForInfo, loopForSummary + "\n" + LOOP_FOR);

        addCompletion(provider, "loop while", LOOP_WHILE, loopWhileInfo, loopWhileSummary + "\n" + LOOP_WHILE);

        addCompletion(provider, "loop do-while", LOOP_DO_WHILE, loopDoWhileInfo, loopDoWhileSummary + "\n" + LOOP_DO_WHILE);

        addCompletion(provider, "loop for each", LOOP_FOREACH, loopForEachInfo, loopForEach + "\n" + LOOP_FOREACH);

        addCompletion(provider, "loop lambda", LOOP_USING_LAMBDA, loopWithLambdaInfo, loopUsingLambda + "\n" + LOOP_USING_LAMBDA);

        addCompletion(provider, "loop IntStream", LOOP_INT_STREAM, loopWithIntStreamInfo, loopUsingIntStream + "\n" + LOOP_INT_STREAM);

        addCompletion(provider, "loop infinite", INFINITE_WHILE_LOOP, loopInfiniteWhileInfo, loopInfiniteWhile + "\n" + INFINITE_WHILE_LOOP);





        // Generování 'main' metody:
        addMainMethod(provider);
    }


    /**
     * Přidání nové položky do okna pro doplňování / dokončování příkazů v editoru.
     *
     * @param provider
     *         - "okno", kam se má přidat nová položka
     * @param inputText
     *         - text, který má být zobrazen pro vyhledání. Tento text se bude filtrovat.
     * @param replacementText
     *         - text, kterým se má nahradit původní text, resp. text, který se má vložit do editoru po zvolení
     *         příslušné položky.
     * @param shortDesc
     *         - text, který bude zobrazen za pomlčkou. Tedy bude syntaxe: "inputText - shortDesc". Tato část bude vidět
     *         v okně. Dle těchto hodnot se bude uživatel orientovat.
     * @param summary
     *         - text, který bude zobrazen v "druhém" okně, které se otevře po označení položky v okně, které se zobrazí
     *         po stisknutí kláves Ctrl + Space.
     */
    private static void addCompletion(final DefaultCompletionProvider provider, final String inputText,
                                      final String replacementText, final String shortDesc, final String summary) {
        provider.addCompletion(new ShorthandCompletion(provider, inputText, replacementText, shortDesc, summary));

        // Možné syntaxe:
//            provider.addCompletion(new BasicCompletion(provider, "abstract"));
////            nebo:
//            provider.addCompletion(new ShorthandCompletion(provider, "text v menu", "text, ktery se vlozi do editoru", "kratky popis za textem v menu"));
//            provider.addCompletion(new ShorthandCompletion(provider, "text v menu", "text, ktery se vlozi do editoru", "kratky popis za textem v menu", "Komentar v dalsim okne"));
    }


    /**
     * Přidání nové položky do okna pro doplňování / dokončování příkazů v editoru.
     *
     * <i>V "druhém" okně, které obsahuje informace o označené položce bude zobrazen text, který se vloží do editoru
     * (místo parametru 'summary').</i>
     *
     * @param provider
     *         - "okno", kam se má přidat nová položka
     * @param inputText
     *         - text, který má být zobrazen pro vyhledání. Tento text se bude filtrovat.
     * @param replacementText
     *         - text, kterým se má nahradit původní text, resp. text, který se má vložit do editoru po zvolení
     *         příslušné položky.
     * @param shortDesc
     *         - text, který bude zobrazen za pomlčkou. Tedy bude syntaxe: "inputText - shortDesc". Tato část bude vidět
     *         v okně. Dle těchto hodnot se bude uživatel orientovat.
     */
    private static void addCompletion(final DefaultCompletionProvider provider, final String inputText,
                                      final String replacementText, final String shortDesc) {
        provider.addCompletion(new ShorthandCompletion(provider, inputText, replacementText, shortDesc));
    }


    /**
     * Přidání nové položky do okna pro doplňování / dokončování příkazů v editoru.
     *
     * <i>V "druhém" okně, které obsahuje informace o označené položce bude zobrazen text, který se vloží do editoru
     * (místo parametru 'summary').</i>
     *
     * @param provider
     *         - "okno", kam se má přidat nová položka
     * @param inputText
     *         - text, který má být zobrazen pro vyhledání. Tento text se bude filtrovat.
     * @param replacementText
     *         - text, kterým se má nahradit původní text, resp. text, který se má vložit do editoru po zvolení
     *         příslušné položky.
     */
    private static void addCompletion(final DefaultCompletionProvider provider, final String inputText,
                                      final String replacementText) {
        provider.addCompletion(new ShorthandCompletion(provider, inputText, replacementText));
    }


    /**
     * Přidání nové položky do okna pro doplňování / dokončování příkazů v editoru.
     *
     * <i>replacementText je text, který bude zobrazen v okně pro filtrování hodnot a zároveň je to text, který se
     * vloží do editoru po zvolení položky.</i>
     *
     * @param provider
     *         - "okno", kam se má přidat nová položka
     * @param replacementText
     *         - text, kterým se má nahradit původní text, resp. text, kter7 se má vložit do editoru po zvolení
     *         příslušné položky.
     */
    private static void addCompletion(final DefaultCompletionProvider provider, final String replacementText) {
        provider.addCompletion(new BasicCompletion(provider, replacementText));
    }







    /**
     * Metoda, která naplní příslušné statické proměnné s texty pro vybrané
     * listenery, které se později přidají jako položky do auto - doplňování do
     * zdrojového kódu.
     */
    private static void createListeners() {
        COMPONENT_LISTENER = "\tComponentListener() {\n"

                + "\n"
                + "\t\t@Override\n"
                + "\t\tpublic void componentShown(ComponentEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n\n"

                + "\t\t@Override\n"
                + "\t\tpublic void componentResized(ComponentEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n\n"

                + "\t\t@Override\n"
                + "\t\tpublic void componentMoved(ComponentEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n\n"

                + "\t\t@Override\n"
                + "\t\tpublic void componentHidden(ComponentEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n"

                + "\t};";

        COMPONENT_ADAPTER = "\tComponentAdapter() {\n\t};";



        FOCUS_ADAPTER = "\tFocusAdapter() {\n\t};";

        FOCUS_LISTENER = "\tFocusListener() {\n"

                + "\n"
                + "\t\t@Override\n"
                + "\t\tpublic void focusLost(FocusEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n\n"

                + "\t\t@Override\n"
                + "\t\tpublic void focusGained(FocusEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n"

                + "\t};";




        KEY_ADAPTER = "\tKeyAdapter() {\n\t};";

        KEY_LISTENER = "\tKeyListener() {\n"
                + "\n"
                + "\t\t@Override\n"
                + "\t\tpublic void keyTyped(KeyEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n\n"

                + "\t\t@Override\n"
                + "\t\tpublic void keyReleased(KeyEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n\n"

                + "\t\t@Override\n"
                + "\t\tpublic void keyPressed(KeyEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n"

                + "\t};";




        MOUSE_ADAPTER = "\tMouseAdapter() {\n\t};";

        MOUSE_LISTENER = "\tMouseListener() {\n"
                + "\n"
                + "\t\t@Override\n"
                + "\t\tpublic void mouseReleased(MouseEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n\n"

                + "\t\t@Override\n"
                + "\t\tpublic void mousePressed(MouseEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n\n"

                + "\t\t@Override\n"
                + "\t\tpublic void mouseExited(MouseEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n\n"

                + "\t\t@Override\n"
                + "\t\tpublic void mouseEntered(MouseEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n\n"

                + "\t\t@Override\n"
                + "\t\tpublic void mouseClicked(MouseEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n"

                + "\t};";



        MOUSE_MOTION_ADAPTER = "\tMouseMotionAdapter() {\n\t};";

        MOUSE_MOTION_LISTENER = "\tMouseMotionListener() {\n"

                + "\n"
                + "\t\t@Override\n"
                + "\t\tpublic void mouseMoved(MouseEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n\n"

                + "\t\t@Override\n"
                + "\t\tpublic void mouseDragged(MouseEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n"

                + "\t};";


        MOUSE_WHEEL_LISTENER = "\tMouseWheelListener() {\n"
                + "\n"
                + "\t\t@Override\n"
                + "\t\tpublic void mouseWheelMoved(MouseWheelEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n"

                + "\t};";


        HIERARCHY_LISTENER = "\tHierarchyListener() {\n"
                + "\n"
                + "\t\t@Override\n"
                + "\t\tpublic void hierarchyChanged(HierarchyEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n"

                + "\t};";


        HIERARCHY_BOUNDS_ADAPTER = "\tHierarchyBoundsAdapter() {\n\t};";

        HIERARCHY_BOUNDS_LISTENER = "\tHierarchyBoundsListener() {\n"
                + "\n"

                + "\t\t@Override\n"
                + "\t\tpublic void ancestorResized(HierarchyEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n\n"

                + "\t\t@Override\n"
                + "\t\tpublic void ancestorMoved(HierarchyEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n"

                + "\t};";


        ACTION_LISTENER = "\tActionListener() {\n"
                + "\n"

                + "\t\t@Override\n"
                + "\t\tpublic void actionPerformed(ActionEvent e) {\n"
                + "\t\t\t// TODO Auto-generated method stub\n"
                + "\n"
                + "\t\t}\n"

                + "\t};";
    }


    /**
     * Doplnění hlavní / spuštěcí 'main' metody do zdrojového kódu.
     *
     * @param provider
     *         - "okno", kde se bude zobrazovat metoda pro doplnění.
     */
    private static void addMainMethod(final DefaultCompletionProvider provider) {
        final String mainMethod = "\n\tpublic static void main(String[] args) {\n" +
                "\n" +
                "\t}";

        addCompletion(provider, "main", mainMethod, "Generate 'main' method");
    }







    /**
     * Metoda, která naplněí promenné s cykly danými hodnotami pro cyklus a také hodnotami s texhtem ve správném
     * jazyce.
     * <p>
     * Musím to vytvářet zde, protože kdybych je udělal statické a final, tak by se nestihly načíst jazyky ve správném
     * jazyce a nebyly by tam správné hodnoty - v komentářích cyklu
     */
    private static void createLoops() {
        // Proměnné pro cykly:

        PREPARED_LOOP_FOR = "\n\tfor (int i = 0; i < 10; i++) {\n"
                + "\t\t\n"
                + "\t}\n";


        LOOP_FOR = "\n\tfor (int i = 0; i < 10; i++) {\n"
                + "\t\t// " + putYourCode + "\n"
                + "\t\tSystem.out.println(i);\n"
                + "\t}\n";


        LOOP_WHILE = "\n\tint i = 0;\n"
                + "\twhile(i < 10) {\n"
                + "\t\t// " + putYourCode + "\n"
                + "\t\tSystem.out.println(i);\n"
                + "\t\ti++;\n"
                + "\t\t// i = i + 1;\n"
                + "\t\t// i += 1;\n"
                + "\t}\n";


        LOOP_DO_WHILE = "\n\tint i = 0;\n"
                + "\tdo {\n"
                + "\t\t// " + putYourCode + "\n"
                + "\t\tSystem.out.println(i);\n"
                + "\t\ti++;\n"
                + "\t\t// i = i + 1;\n"
                + "\t\t// i += 1;\n"
                + "\t} while(i < 10);\n";


        LOOP_FOREACH = "\n\tfinal int[] intArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};\n"
                + "\t\tfor (final int i : intArray) {\n"
                + "\t\t// " + putYourCode + "\n"
                + "\t\tSystem.out.println(i);\n"
                + "\t\t}\n";


        LOOP_USING_LAMBDA = "\n\tjava.util.List<java.lang.Integer> integerList = new java.util.ArrayList<>(java.util" +
                ".Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));\n"
                + "\tintegerList.forEach(i -> System.out.println(i));\n";


        LOOP_INT_STREAM = "java.util.stream.IntStream.range(0, 10).forEach(System.out::println);";


        INFINITE_WHILE_LOOP = "\n\tint i = 10;\n"
                + "\twhile (i > 1) {\n"
                + "\t\tSystem.out.println(i);\n"
                + "\t\ti++;\n"
                + "\t}\n";
    }













    /**
     * Metoda, která naplní textové proměnné s textem v nápovědě u některých částí
     * kódu, u kterých je dobré vědět o co jde, převážně se jedná o cykly, apod.
     *
     * @param properties
     *            - proměnná typu Properties s texty ve zvoleném jazyce
     */
    protected static void setFulfillmentVarForCodeComment(final Properties properties) {
        if (properties != null) {
            loopForSummary = properties.getProperty("CodEdit_LoopForSummary", Constants.CODE_EDIT_LOOP_FOR_SUMMARY);
            loopForPreparedSummary = properties.getProperty("CodEdit_PreparedLoopForSummary", Constants.CODE_EDIT_PREPARED_LOOP_SUMMARY);
            loopWhileSummary = properties.getProperty("CodEdit_LoopWhileSummary", Constants.CODE_EDIT_LOOP_WHILE_SUMMARY);
            loopDoWhileSummary = properties.getProperty("CodEdit_LoopDoWhileSummary", Constants.CODE_EDIT_LOOP_DO_WHILE_SUMMARY);
            loopForEach = properties.getProperty("CodEdit_LoopForEachSummary", Constants.CODE_EDIT_LOOP_FOR_REACH);
            loopUsingLambda = properties.getProperty("CodEdit_loopUsingLambdaSummary", Constants.CODE_EDIT_LOOP_USING_LAMBDA);
            loopUsingIntStream = properties.getProperty("CodEdit_loopUsingIntStream", Constants.CODE_EDIT_LOOP_INT_STREAM);
            loopInfiniteWhile = properties.getProperty("CodEdit_InfiniteWhileLoop", Constants.CODE_EDIT_INFINITE_WHILE_LOOP);
            putYourCode = properties.getProperty("CodEdit_PutYourCodeHere", Constants.CODE_EDIT_PUT_YOUR_CODE);

            loopForInfo = properties.getProperty("CodEdit_LoopForInfo", Constants.CODE_EDIT_LOOP_FOR_INFO);
            loopWhileInfo = properties.getProperty("CodEdit_LoopWhileInfo", Constants.CODE_EDIT_LOOP_WHILE_INFO);
            loopDoWhileInfo = properties.getProperty("CodEdit_LoopDoWhileInfo", Constants.CODE_EDIT_LOOP_DO_WHILE_INFO);
            loopForEachInfo = properties.getProperty("CodEdit_LoopForeachInfo", Constants.CODE_EDIT_LOOP_FOREACH_INFO);
            loopWithLambdaInfo = properties.getProperty("CodEdit_LoopWithLambdaExpressionInfo", Constants.CODE_EDIT_LOOP_WITH_LAMBDA_INFO);
            loopWithIntStreamInfo = properties.getProperty("CodEdit_LoopWithIntStreamInfo", Constants.CODE_EDIT_LOOP_INT_STREAM_INFO);
            loopInfiniteWhileInfo = properties.getProperty("CodEdit_InfiniteWhileLoopInfo", Constants.CODE_EDIT_INFINITE_WHILE_LOOP_INFO);

            // Přístupové metody:
            generateSetter = properties.getProperty("CodEdit_GenerateSetter", Constants.CODE_EDIT_GENERATE_SETTER);
            generateGetter = properties.getProperty("CodEdit_GenerateGetter", Constants.CODE_EDIT_GENERATE_GETTER);

            // Proměnné pro texty pro vygenerování cyklů pro konkrétní proměnné:
            txtWithVariable = properties.getProperty("CodEdit_Txt_WithVariable", Constants.CODE_EDIT_TXT_WITH_VARIABLE);
            txtWithoutVariable = properties.getProperty("CodEdit_Txt_WithoutVariable", Constants.CODE_EDIT_TXT_WITHOUT_VARIABLE);
        }

        else {
            loopForSummary = Constants.CODE_EDIT_LOOP_FOR_SUMMARY;
            loopForPreparedSummary = Constants.CODE_EDIT_PREPARED_LOOP_SUMMARY;
            loopWhileSummary = Constants.CODE_EDIT_LOOP_WHILE_SUMMARY;
            loopDoWhileSummary = Constants.CODE_EDIT_LOOP_DO_WHILE_SUMMARY;
            loopForEach = Constants.CODE_EDIT_LOOP_FOR_REACH;
            loopUsingLambda = Constants.CODE_EDIT_LOOP_USING_LAMBDA;
            loopUsingIntStream = Constants.CODE_EDIT_LOOP_INT_STREAM;
            loopInfiniteWhile = Constants.CODE_EDIT_INFINITE_WHILE_LOOP;
            putYourCode = Constants.CODE_EDIT_PUT_YOUR_CODE;

            loopForInfo = Constants.CODE_EDIT_LOOP_FOR_INFO;
            loopWhileInfo = Constants.CODE_EDIT_LOOP_WHILE_INFO;
            loopDoWhileInfo = Constants.CODE_EDIT_LOOP_DO_WHILE_INFO;
            loopForEachInfo = Constants.CODE_EDIT_LOOP_FOREACH_INFO;
            loopWithLambdaInfo = Constants.CODE_EDIT_LOOP_WITH_LAMBDA_INFO;
            loopWithIntStreamInfo = Constants.CODE_EDIT_LOOP_INT_STREAM_INFO;
            loopInfiniteWhileInfo = Constants.CODE_EDIT_INFINITE_WHILE_LOOP_INFO;

            // Přístupové metody:
            generateSetter = Constants.CODE_EDIT_GENERATE_SETTER;
            generateGetter = Constants.CODE_EDIT_GENERATE_GETTER;

            // Proměnné pro texty pro vygenerování cyklů pro konkrétní proměnné:
            txtWithVariable = Constants.CODE_EDIT_TXT_WITH_VARIABLE;
            txtWithoutVariable = Constants.CODE_EDIT_TXT_WITHOUT_VARIABLE;
        }
    }



















    /**
     * Metoda, která nastaví v dialogu pro editor zdrojového kódu tzv.
     * "autodoplňování". tj. po stisknutí kombinace kláves Ctrl + Space se otevře
     * dialogové okno s nápovědou, takto jsem na to napsal metodu, abych to ještě
     * trochu "vylepšíl" v tom ohledu, že když se nějaká třída z diagramu tříd v
     * hlavním okně aplikace otevře v dialogu - v editoru zdrojového kódu, tak se
     * pokusí načíst soubor .class dané zkompilované třídy, a pokud se načte, pokusí
     * se z něj získat metody a proměnné, které uživateli mohou urychlit práci s
     * editorem kódu
     *
     * @param clazz
     *            - načtená reference na soubor .class zkompilavané označené třídy v
     *            diagramu tříd nebo null, pokud se daný soubor nenašel nebo se
     *            třída nepodařial zkompilovat
     */
    public final void setAutoCompletion(final Class<?> clazz) {
        // Auto doplnovani - doplnění částí kódu po stisknutí kombinace kláves
        // CTRL + Space
        final CompletionProvider provider = createCompletionProvider(clazz);

        createAutoCompletion(provider);
    }























    /**
     * Metoda, která v podstatě vytvoři klíčová slova, která se zobrazí v "okně
     * menu" - po stisknutí kombinace kláves Ctrl + Space pro výběr klíčového slova
     * pro vložení do editoru kódu
     *
     * @param clazz
     *            - načtená zkompilovaná třída, jejíž kód se má otevřit v editoru,
     *            tak si z ní načtu některé hodnoty
     *
     * @return výše zmíněné texty pro doplnění do editoru
     */
    private static CompletionProvider createCompletionProvider(final Class<?> clazz) {

        // A DefaultCompletionProvider is the simplest concrete implementation
        // of CompletionProvider. This provider has no understanding of
        // language semantics. It simply checks the text entered up to the
        // caret position for a match against known completions. This is all
        // that is needed in the majority of cases.
        final DefaultCompletionProvider provider = new DefaultCompletionProvider();

        // Add completions for all Java keywords. A BasicCompletion is just
        // a straightforward word completion.


        // SYNTAXE:
        //	      provider.addCompletion(new BasicCompletion(provider, "abstract"));
        // nebo:
        //	      provider.addCompletion(new ShorthandCompletion(provider, "text v menu", "text, ktery se vlozi do editoru", "kratky popis za textem v menu", "Komentar v dalsim okne"));




        createShortcuts(provider);




        // Nyní se pokusím doplnit proměnné a metody z načteného souboru .class zkompilované třídy
        // a doplnit je do této "nápovědy" - nebo doplňovacího okna či jak to nazvat

        if (clazz != null) {
            // Zde se podařilo načíst požadovaný soubor, tak se mohu pokusit získat proměnné a metody:

            /*
             * V této části vím, že se podařilo načíst a předat nějaku třídu z diagramu tříd
             * (clazz), takže se podařilo třídy v diagramu tříd zkompilovat, takže si mohu
             * dále načíst všechny třídy v diagramu tříd a zjistit si jejich konstruktory a
             * statické metody a proměnné (veřejné).
             *
             * Jde pouze o to, že by bylo pro uživatele vhodné, aby měl na výběr například i
             * doplnění konstruktoru jiných trid apod. Aby nebyl odkázán pouze na předky.
             */
            checkOtherClassesFromCd(clazz, provider);



            // Nejprve si načtu požadované metody a proměnné v dané třídy
            final Constructor<?>[] constructors = clazz.getDeclaredConstructors();

            final Field[] fields = clazz.getDeclaredFields();

            final Method[] methods = clazz.getDeclaredMethods();


            // Zjistím, zda se v aktuálně testované třídě nachází ještě nějaké vnořené třídy, pokud ano,
            // tak se pokusím opět získat jejich data - konstruktory, proměnné a metody:
            final Class<?>[] declaredClassesArray = clazz.getDeclaredClasses();


            // Zde jsou načtená data, tak mohu cyklem "prohledat" získaná pole hodnot a pokusit se z nich
            // získat "použitelná data", abych je mohl případně vložit do editrou po označeí v tomto "doplňovacím okně"


            // Metoda, která předá položky do nápovědy:
            addFieldsToProvider(provider, Arrays.asList(fields));

            // Pridání metod do okna s nápovědou - autodoplnováním:
            addMethodToProvider(provider, Arrays.asList(methods));

            /*
             * Vygenerování přístupových metod i pro proměnné ve vnitřních třídách.
             *
             * Neřeší se viditelnost vnitřních tříd ani proměnných. Stručně vytvoří se syntaxe pro vygenerování
             * přístupových metod pro veškeré proměnné ve veškerých vnitřních třídách.
             */
            addAccessMethodsForFieldsInInnerClasses(provider, clazz);

            // Vygenerování cyklů:
            addLoops(provider, Arrays.asList(fields));


            /*
             * Zde se přidají možnosti pro vygenerování cyklů k polím, Listům a mapám pro vnitřní třídy.
             *
             * Toto je ale třeba provést tak, že se získají veškeré proměnné z vnitřních tříd nehledě na viditelnost
             * jak proměnných, tak i vnitřních tříd. Protože se nezjišťuje, kde má uživatel kurzor, tedy v jaké
             * konkrétní třídě se tedy nachází, proto budou na výběr veškeré proměnné ze všech vnitřních tříd a
             * uživatel už "si s tím bude muset nějak poradit" ve smyslu, aby rozlišil, jaká proměnná patří do jaké
             * třídy apod..
             */
            final List<Field> innerClassesFieldList = getAllFieldsInAllInnerClasses(declaredClassesArray,
                    new ArrayList<>());
            // Vygenerování cyklů pro proměnné ve vnitřních třídách:
            addLoops(provider, innerClassesFieldList);







            // Přidání výchozího konstruktoru:

            // Vždy se bude moci přidat výchozí parametr - Defaultní, bez parametrů, ... ten klasický:
            // název třídy - bez balíčků
            final String clazzName = clazz.getSimpleName();

            // do do okna nápovědy:
            final String defaultConstructorForView = clazzName + "()";

            // text pro doplnění do editoru:
            final String defaultConstructorForInsert = "\tpublic " + clazzName + "() {\n"
                    + "\t\t// TODO Auto-generated constructor stub\n"
                    + "\t}\n";

            // přidání do nápovědy:
            addCompletion(provider, defaultConstructorForView, defaultConstructorForInsert, "Constructor");







            /*
             * Metoda, která do okna s nápovědou přidá veřejné a nebo chráněné konstruktory,
             * metody a nebo položky z předků třídy v parametru, které se nachází v diagramu
             * tříd
             */
            addFieldToProviderFromClass(clazz, provider);

            /*
             * Zde přidám veškeré konstruktory na tuto aktuální třídu, sice bude dvakrát ten
             * výchozí konstruktor, ale to pouze pro to, že ten výše je již "implementovaný"
             * tj. že se rovnou přidá ten konstruktor, ale ty konstruktory, které se přidají
             * v následující metodě budou sloužit pouze pro "zavolní" toho konstruktoru, tj.
             * bude se jednat o konstruktory, které slouží jen jako doplněné do kodu v
             * syntaxi pro zavolání ne jako doplnění.
             */
            addConstructorsToProvider(provider, clazz, Arrays.asList(constructors));


            // Note:
            // U následujícího cyklu bych neměl načítat všechny proměnné u vnitrnich trid, v podstatě bych je neměl
            // načitat vůbec,
            // protože například Eclipse to děla tak, ze pozna, kde se nachází kurzor, a kdyz se bude nachazet u
            // vnitrni tridy, tak se
            // v napodede zobrazi pouze polozky z dane vnitrni tridy, ovsem toto muj editor neumi, tak si načte
            // vsechny prvky, a zobrazi
            // je v napodede vzdy, az se kurzor nachází kdekoliv, nehlde na to, ze se kurzor nachází ve vnitrni tride
            // nebo ne, apod.

            // Jenže kdybych je nenečítal vubec, tak by zase mohlo být uživateli trochu "divné", proc tam dané
            // hodnoty dané tridy nejsou
            // takže by u vnorenych trid nebyly hodnoty v napovede vubec, coz by taky "nebylo uplne v poradku"


            // Projdu cyklem všechny vnořené třídy a případně přidám jejich hodnoty do okna s doplňováním:
            for (final Class<?> d : declaredClassesArray)
                addDeclaredValueOfInnerClass(d, provider, false);
        }

        return provider;
    }














    /**
     * Metoda, která slouží pro načtení konstruktorů, veřejných statických
     * proměnných a veřejných statických metod ze všech tříd v diagramu tříd s
     * výjimkou třídy clazz a všech jejich předků, protože kdybych načítal hodnoty v
     * této metodě i z předků třídy clazz, pak by vznikaly duplicity.
     *
     * @param clazz
     *            - třídy, která je aktuálně otevřená v nějakém editoru zdrojového
     *            kódu nebo v dialogu průzkumníku projektů. Mají se načíst
     *            konstruktory a veřejné a statické metody a proměnné ze všech tříd
     *            v diagramu tříd s výjimkou právě této třídy clazz a všech jejich
     *            předků.
     *
     * @param provider
     *            - v podstatě něco, kam se přidají texty pro doplnění do nápovědy
     */
    private static void checkOtherClassesFromCd(final Class<?> clazz, final DefaultCompletionProvider provider) {
        /*
         * Postup:
         *
         * - načtu si všechny předky třídy clazz.
         *
         * - pak všechny třídy v diagramu tříd.
         *
         * - do nějaké kolekce si vyfiltruji pouze ty třídy z diagramu tříd, které
         * nejsou clazz ani její předci, jinak by vznikly duplicity.
         *
         * - A tuto načtou kolekci projdu a v každé iteraci si zjistím veřejné
         * konstruktory, statické veřejné metody a statické veřejné proměnné. Podobné
         * pro vnitřní třídy.
         */

        /*
         * List, který bude obsahovat třídu clazz a všechny její předky, resp. jedná se
         * o veškeré třídy, které se mají vynechat při získávání statických veřejných
         * hodnot (konstruktorů, proměnných a metod). Viz postup výše.
         */
        final List<Class<?>> ancestorsList = getAllAncestors(clazz);


        /*
         * List, který bude obsahovat načtené přeložené / zkompilované třídy (soubory
         * .class) tříd z diagramu tříd.
         */
        final List<Class<?>> allClassesList = getAllClassesFromCd();


        /*
         * List, do kterého si vložím třídy, ze kterých je možné načíst konstruktory a
         * veřejné statické proměnné a veřejné statické metody.
         *
         * Tento krok by bylo možné načístu už v jedné z metod výše, například u
         * načítání všech tříd z diagramu tříd, ale chtěl jsem to napsat "oddělené",
         * kdybych nějaké z těchto metod v budoucnu ještě potřeboval, nebo jsem něco
         * doplňoval apod. Navíc je to trochu přehlednější.
         */
        final List<Class<?>> classesForFindingValuesList = getFilteredClasses(allClassesList, ancestorsList);




        classesForFindingValuesList.forEach(c -> {
            /*
             * Zjistím si všechny veřejné a zároveň statické proměnné v aktuálně iterované
             * třídě c a přidám je do okna s auto doplňováním kódu.
             */
            final List<Field> publicStaticFieldsList = getPublicStaticFields(c);
            // Přidám položky do okna s nápovědou:
            addFieldsToProvider(provider, publicStaticFieldsList);

            /*
             * Zjistím si veškeré metody, které jsou veřejné a zároveň statické - metody,
             * které se nachází v aktuálně iterované třídě c a přdám je do okna s auto -
             * doplňováním.
             */
            final List<Method> publicStaticMethodsList = getPublicStaticMethods(c);
            // Přidání metod do okna s nápovědou:
            addMethodToProvider(provider, publicStaticMethodsList);

            /*
             * Získám si veškeré veřejné konstruktory v aktuálně iterované třídě c a přidám
             * je do okna s auto - doplňováním.
             */
            final List<Constructor<?>> publicConstructorsList = getPublicConstructors(c);
            // Přidání do okna s auto - doplňováním:
            addConstructorsToProvider(provider, c, publicConstructorsList);





            /*
             * Prohlednám všechny zjištěné vnorene a vnitrni tridy a budu postupovat stejne
             * jako vyse pro metody, konstruktory a proměnné, tj. budu si pro všechny
             * (veřejné) vnořené třídy načítat její konstruktory, metody a proměnné
             * (statické).
             */
            for (final Class<?> cl : c.getDeclaredClasses()) {
                /*
                 * Zde u třídy nebudu řešit, zda je statická, protože u uživatele v kódu
                 * nedokážu říci, zda se na tu tridu bude odkazovat přes její nazev nebo přes
                 * vytvořenou referenci, tak budu chtit aby tam byla, když tak už kompilátor
                 * oznámí potenciální chybu, zde řeším pouze doplnění kódu.
                 */
                if (!Modifier.isPublic(cl.getModifiers()) /* && !Modifier.isStatic(cl.getModifiers()) */)
                    continue;
                /*
                 * Zjistím si všechny veřejné a zároveň statické proměnné v aktuálně iterované
                 * třídě c a přidám je do okna s auto doplňováním kódu.
                 */
                final List<Field> publicStaticFieldsList2 = getPublicStaticFields(cl);
                // Přidám položky do okna s nápovědou:
                addFieldsToProvider(provider, publicStaticFieldsList2);

                /*
                 * Zjistím si veškeré metody, které jsou veřejné a zároveň statické - metody,
                 * které se nachází v aktuálně iterované třídě c a přdám je do okna s auto -
                 * doplňováním.
                 */
                final List<Method> publicStaticMethodsList2 = getPublicStaticMethods(cl);
                // Přidání metod do okna s nápovědou:
                addMethodToProvider(provider, publicStaticMethodsList2);

                /*
                 * Získám si veškeré veřejné konstruktory v aktuálně iterované třídě c a přidám
                 * je do okna s auto - doplňováním.
                 */
                final List<Constructor<?>> publicConstructorsList2 = getPublicConstructors(cl);
                // Přidání do okna s auto - doplňováním:
                addConstructorsToProvider(provider, cl, publicConstructorsList2);
            }
        });
    }











    /**
     * Metoda, která slouží pro získání / vyfiltrování pouze veřejných konstruktorů
     * ze třídy clazz.
     *
     * @param clazz
     *            - třída, ze které se mají získat pouze veřejné konstruktory.
     *
     * @return list, který bude obsahovat pouze veřejné konstruktory z třídy clazz.
     */
    private static List<Constructor<?>> getPublicConstructors(final Class<?> clazz) {
        return Arrays.stream(clazz.getDeclaredConstructors()).filter(c -> Modifier.isPublic(c.getModifiers()))
                .collect(Collectors.toList());
    }




    /**
     * Metoda, která slouží pro získání / vyfiltrování pouze statických a zároveň
     * veřejných proměnných / atributů ve třídě clazz.
     *
     * @param clazz
     *            - třída, ze které se mají získat veřejné statické proměnné.
     *
     * @return list, který bude obsahovat veřejné statické proměnné / atributy,
     *         které obsahuje třída clazz.
     */
    private static List<Field> getPublicStaticFields(final Class<?> clazz) {
        return Arrays.stream(clazz.getDeclaredFields())
                .filter(f -> Modifier.isPublic(f.getModifiers()) && Modifier.isStatic(f.getModifiers()))
                .collect(Collectors.toList());
    }




    /**
     * Metoda, která slouží pro získání / vyfiltrování pouze statických a zároveň
     * veřejných metod ve třídě clazz.
     *
     * @param clazz
     *            - třída, ze které se mají získat veřejné statické metody.
     *
     * @return list, který bude obsahovat veřejné statické metody, které obsahuje
     *         třída clazz.
     */
    private static List<Method> getPublicStaticMethods(final Class<?> clazz) {
        return Arrays.stream(clazz.getDeclaredMethods())
                .filter(m -> Modifier.isPublic(m.getModifiers()) && Modifier.isStatic(m.getModifiers()))
                .collect(Collectors.toList());
    }
















    /**
     * Metoda, která z listu allClasses vyfiltruje, tj. vrátí z listu allClasses
     * pouze ty třídy, které se nenachází v listu ancestors.
     *
     * @param allClasses
     *            - list, který obsahuje veškeré načtené třídy z diagramu tříd.
     *
     * @param ancestors
     *            - list, který obsahuje aktuální třídu, ze které se mají vzít
     *            hodnoty do dialogu pro auto doplňování a veškeré její předky. Tyto
     *            hodnoty nesmí být vlistu allClasses.
     *
     * @return list, který bude obsahovat veškeré třídy z listu allClasses, které se
     *         nenachází v listu ancestors.
     */
    private static List<Class<?>> getFilteredClasses(final List<Class<?>> allClasses, final List<?> ancestors) {
        return allClasses.stream().filter(c -> !ancestors.contains(c)).collect(Collectors.toList());
    }















    /**
     * Metoda, která načte veškeré třídy z diagramu tříd.
     *
     * @return list, který bude obsahovat veškeré načtené třídy zdiagramu tříd, tedy
     *         jejich přeložené / zkompilované verze, tj. soubory .class.
     */
    private static List<Class<?>> getAllClassesFromCd() {
        /*
         * List, do kterého vložím veškeré načtené třídy v diagramu tříd.
         */
        final List<Class<?>> classesList = new ArrayList<>();

        GraphClass.getCellsList().stream()
                /*
                 * Vyfiltruji ze všech objektů v diagramu tříd pouze třídy, tj. ne hrany nebo
                 * komentáře:
                 */
                .filter(c -> !(c instanceof DefaultEdge) && GraphClass.KIND_OF_EDGE.isClassCell(c)).forEach(clazz -> {
            /*
             * Získám si text buňky, dle toho si načtu požadovanou třídu z příslušného
             * balíčku.
             */
            final String cellTextWithDot = clazz.toString();

            /*
             * Načtu si příslušnou třídu, která se přívě iteruje.
             */
            final Class<?> loadedClazz = App.READ_FILE.loadCompiledClass(cellTextWithDot, null);

            /*
             * Zjistím, zda se podařilo načíst příslušnou třídu, pokud ano, pak pro jistotu
             * otestuji, zda se v listu ještě ta načtená třída nenachází, ale toto je
             * zbytečná část, protože v diagramu tříd je každá třída jednou, takže by ani
             * nemohly být duplicity a i kdyby byly, ničemu by to nevadilo.
             */
            if (loadedClazz != null && !classesList.contains(loadedClazz))
                classesList.add(loadedClazz);
        });

        // Vrátím načtené třídy z diagramu tříd:
        return classesList;
    }

















    /**
     * Metoda, která vrátí list, který bude obsahovat veškeré předky třídy clazz až
     * po třídu Object, ten list bude obsahovat i třídu clazz.
     *
     * @param clazz
     *            - třída, ze které se mají načíst předci, kteří se nachází v
     *            diagramu tříd.
     *
     * @return list, který bude obsahovat třídu clazz a všechny její předky, které
     *         se nachází v diagramu tříd.
     */
    private static List<Class<?>> getAllAncestors(final Class<?> clazz) {
        /*
         * List, do kterého vložím veškeré načtené předky třídy clazz, které se nachází
         * v diagramu tříd.
         */
        final List<Class<?>> ancestors = new ArrayList<>();

        // Přidám do listu i tu aktuální třídu:
        ancestors.add(clazz);


        /*
         * Proměnná, která značí, dokud se má iterovat cyklem.
         */
        boolean search = true;

        /*
         * Proměnná pro uložení předků pro testování.
         */
        Class<?> tempClass = clazz.getSuperclass();

        while (search) {
            /*
             * Pokud je načtený předek null (nemělo by nastat) nebo Object, pak mohu
             * skončit, z těchto třídy již hodnoty brát nechci.
             */
            if (tempClass == null || tempClass.equals(Object.class)) {
                search = false;
                break;
            }

            /*
             * Jestliže se předek třídy clazz nachází v diagramu tříd, pak se jedná o
             * nějakého předka třídy clazz nebo předka nějakého jejího předka (třídy clazz).
             */
            if (isInheriting(tempClass.getName()) && !ancestors.contains(tempClass))
                ancestors.add(tempClass);

            /*
             * Získám si předka aktuálí třídy pro další iterování.
             */
            tempClass = tempClass.getSuperclass();
        }


        // Vrátím list získaných předků, které se nachází v diagramu tříd.
        return ancestors;
    }

















    /**
     * Metoda, která do pole s nápovědou přidá veřejné a chráněné konstruktory,
     * metody a proměnné třídy v parametru.
     *
     * Z dané třídy v parametru si dále pomocí rekurze vytáhne některé vnořené třídy
     * a tridy, ze kterych dedi, ... - opět veřejné a chráněné a pomocí rekurze
     * zavolá opět tuto metodu a dále si z ní vytáhne zmíněné položky
     *
     * @param clazz
     *            - načtená zkompilované - přeložená třída, ze které se mají převzít
     *            hodnoty
     *
     * @param provider
     *            - v podstatě něco, kam se přidají texty pro doplnění do nápovědy
     */
    private static void addFieldToProviderFromClass(final Class<?> clazz,
                                                    final DefaultCompletionProvider provider) {
        // zkusím si načíst, zda třída z nečeho dědí a ta třída se nachází v diagramu tříd nebo se jedná o objekt, apod.:
        // reagovat budu pouze, pokud třída z nečeho dědí a jedná se o třídu, která se nachází v diagramu tříd, pak si z ní
        // načtu atributy a metody, konstruktory, a vložím je také do nápovědy:
        Class<?> clazzSuper = clazz.getSuperclass();


        // Otestuji, zda třída z nečeho dědí a jsou načteny objekty z diagramu
        // tříd:
        if (clazzSuper != null) {
            boolean varContinue = true;

            // Dokud bude trida z neceho dedit a ta se bude nachate v diagramu
            // trid, bude se cyklus opakovat:
            while (varContinue && isInheriting(clazzSuper.getName())) {

                // Zavolání metody pro zpřistupnění veřejných a chráněných
                // položek aktuálně zjištěné třídy:
                addDeclaredValueOfInnerClass(clazzSuper, provider, true);

                // Zde si do proměnné clazzSuper zkusím načíst třídu, ze které
                // dědí, pokud se to povede, tak varContinue bude true,
                // jinak false, a při další iteraci se otestuje, zda nově
                // načtená třída se nachází v diagramu tříd nebo ne,
                // pokud ano, proběhne další iterace pro načtení hodnotu, jinak
                // ne, a cyklus skončí:
                clazzSuper = clazzSuper.getSuperclass();

                // Zde již třída buď nedědí, nebo dědi z třídy, která se
                // nenachází v diagramu tříd, tak mohu ukončit cyklus:
                varContinue = clazzSuper != null;
            }
        }
    }


















    /**
     * Metoda, která přidá do okna s doplnovanim proměnné, metody a konstruktory z
     * třídy v parametru, ale na základě hodnoty proměnné visibilityTest - coby test
     * viditelnost, pokud bude true, pak se má otestovat viditelnost položek.
     *
     * TJ. Pokud bude visitbilityTest == true, pak se ma testovat, zda jsou hodnoty
     * verejne nebo chranene, protoze se jedna o vnitrni tridy nebo tridy z
     * dedicnosti, takze lze videt pouze verejne nebo chranene prvky, pokud bude
     * tato promenna nastavena na false, pak se pridaji vsechny polozky, protoze
     * tato metoda s touto hodnotou parametru se vola pouze s parametrem tridy,
     * jejiz kod je nacten v editoru
     *
     * @param clazz
     *            - třída, ze které se mají vzít položky
     *
     * @param provider
     *            - okno, kam se mají přidat, aby se zobrazily vnápovědě
     *
     * @param visibilityTest
     *            - logická proměnná, dle které poznám, zda se má otestovat
     *            viditelnost položek nebo ne, aby se přidaly bud vsechny nebo je
     *            verejne a chranene
     */
    private static void addDeclaredValueOfInnerClass(final Class<?> clazz,
                                                     final DefaultCompletionProvider provider, final boolean visibilityTest) {
        // Získám si všechny položky do nápovědy:
        final Field[] fieldsInherit = clazz.getDeclaredFields();

        final Method[] methodsInherit = clazz.getDeclaredMethods();

        final Constructor<?>[] constructorInherit = clazz.getConstructors();

        final Class<?>[] declaredClassesArray = clazz.getDeclaredClasses();


        // Nyní mám ve výše uvedených promenných atrubuty, metoda a konstrukotry dané třídy, ale přístupné budou
        // pouze ty veřejné nebo chráněné

        // Vytvořím si kolekci - List typu Field a vložím do něj proměnné třídy, které jsou veřejné nebo chráněné:
        final List<Field> publicProtectedFieldsList = new ArrayList<>();

        for (final Field f : fieldsInherit) {
            // Otestuji, zda mám testovat viditelnost:
            if (visibilityTest) {
                if (Modifier.isProtected(f.getModifiers()) || Modifier.isPublic(f.getModifiers()))
                    publicProtectedFieldsList.add(f);
            }
            else publicProtectedFieldsList.add(f);
        }


        // List - kolekce, do které vložím veřejné nebo chráněné metody:
        final List<Method> publicProtectedMethodsList = new ArrayList<>();

        for (final Method m : methodsInherit) {
            if (visibilityTest) {
                if (Modifier.isProtected(m.getModifiers()) || Modifier.isPublic(m.getModifiers()))
                    publicProtectedMethodsList.add(m);
            }
            else publicProtectedMethodsList.add(m);
        }


        // List, do kterého vložím veřejné a nebo chráněné konstruktory:
        final List<Constructor<?>> publicProtectedConstructorList = new ArrayList<>();

        for (final Constructor<?> c : constructorInherit) {
            if (visibilityTest) {
                if (Modifier.isProtected(c.getModifiers()) || Modifier.isPublic(c.getModifiers()))
                    publicProtectedConstructorList.add(c);
            }
            else publicProtectedConstructorList.add(c);
        }


        // Nyní mám ve výše uvedených listech zmíněné hodnoty bud viditelnosti verejne nebo chranene,
        // tak z nich sestavím hodnoty, tak aby byly přehledné pro uživatele a mohl s nimi pracovat:

        // Přidám položky do okna s nápovědou:
        addFieldsToProvider(provider, publicProtectedFieldsList);

        // Přidání metod do okna s nápovědou:
        addMethodToProvider(provider, publicProtectedMethodsList);

        // Metoda pro přidání konstruktorů do okna s nápovědou - autodoplňování:
        addConstructorsToProvider(provider, clazz, publicProtectedConstructorList);



        // Prohlednám všechny zjištěné vnorene a vnitrni tridy a budu postup zmineny u metody opakovat pro
        // vsechny vnorene a vnitrni tridy
        for (final Class<?> c : declaredClassesArray) {
            if (visibilityTest) {
                if (Modifier.isProtected(c.getModifiers()) || Modifier.isPublic(c.getModifiers()))
                    addDeclaredValueOfInnerClass(c, provider, visibilityTest);
            }
            else addDeclaredValueOfInnerClass(c, provider, visibilityTest);
        }
    }










    /**
     * Metoda, která přidá do dialogu s auto doplňováním potřebné informace pro
     * doplnění konstruktoru do editoru kodu po označení uživatelem
     *
     * @param provider
     *            - "komponenta, kam se přidají informace pro doplnění"
     *
     * @param clazz
     *            - třída - soubor .class třídy, ze které se mají vzít konstruktory,
     *            jedná se o třídu, ze které dědí buď, třída jejíž kod je načten v
     *            editoru kodu nebo některé z jeji rodicu
     *
     * @param publicProtectedConstructorList
     *            - list konstruktorů, ze kterého se vezmou informace pro doplnění
     *            do dialogu
     */
    private static void addConstructorsToProvider(final DefaultCompletionProvider provider, final Class<?> clazz,
                                                  final List<Constructor<?>> publicProtectedConstructorList) {


        /*
         * Toto slouží pouze pro to, že chci, aby byl příslušný konstruktor, resp. typ
         * třídy na výběr i jako když se píše text, resp. datový typ proměnné.
         *
         * Uživatel píše například private ClassName variableName; a u té části, kdy
         * píše ClassName, tak může sitknout Ctrl + Space a zvolit si jednu z možných
         * tříd, jako když píše referenci na nějakou jinou třídu v diagramu tříd. Jinak
         * by ty konstruktory byly pouze jako konstruktory, a uživatel by vždy musel
         * odmazat ty kulaté závorky na konci.
         */
        addCompletion(provider, clazz.getSimpleName(), clazz.getSimpleName(), clazz.getName());



        for (final Constructor<?> c : publicProtectedConstructorList) {
            // Název třídy pro konstruktor:
            final String clazzSuperName = clazz.getSimpleName();

            // Parametry konstruktoru:
            final Parameter[] parameters = c.getParameters();


            // Získám si parametry v textu, tak abych je mohl vložit do editoru:
            final String parametersInText = ParameterToText.getParametersInText(parameters, false);


            // Nyní mám v proměnné parametersInText všechny parametry metody,
            // mohu sestavit příkazy, které vložím do okna
            // aby uživatel mohl vidět text metody i s parametry:
            final String constructorInText = clazzSuperName + "(" + parametersInText + ")";

            // Získání parametrů v textu, podobně jako výše, akorát tato metoda
            // vrátí název parametru vždy s prvním malým písmenem:
            final String parametersFirstCharSmall = getParametersInTextWithFirsthSmallWord(parameters);

            // Nyní sestavím celkový - finální text, který se vloži do editoru
            // po zvolení dané metody uživatelem:
            final String textOfConstructorToInsert = clazzSuperName + "(" + parametersFirstCharSmall + ");";


            /*
             * Přidání konstruktoru do dialogu s nápovědou - auto doplněním.
             *
             * Ale na konec přidám celý konstruktor v textu, protože mohou existovat dvě
             * třídy se stejným názvem, ale v různých balíčcích, tak aby si uživatel
             * nemyslel, že se jendá o chybu apod.
             *
             * Jinak, kdybych nechal ten zakomentovaný kód, tak bude v tom druhém okne u
             * zvýrazněné položky v dialogu s auto - doplňováním jen text, který se má
             * přidat, ale já tam chci mít zobrazený celý text konstruktoru.
             */
            //			provider.addCompletion(new ShorthandCompletion(provider, constructorInText, textOfConstructorToInsert, c.getName()));
            addCompletion(provider, constructorInText, textOfConstructorToInsert, c.getName(), c.toString());
        }
    }







    /**
     * Metoda, která do okna s nápovědou - autodoplňováním přidá informace a údaje potřebné pro "automatické" doplnění
     * metody
     *
     * @param provider
     *         - "něco jako okno, s kody pro doplnění" do editoru, po zvolení uživatelem
     * @param methodsList
     *         - list metod, které se mají přidat do okna s doplnováním
     */
    private static void addMethodToProvider(final DefaultCompletionProvider provider, final List<Method> methodsList) {
        // Nyní cyklus pro přidání metod v dané třídě:
        for (final Method m : methodsList) {
            // Postup:
            // Pro každou metodu si zjistím počet parametrů, jejich datové typy sestavím
            // coby text a pridam do ukazky v dialougu,
            // aby uživatel vědel co obsahuje metda za parametry a o jakou metodu se jedna:

            final Parameter[] parameters = m.getParameters();

            // Získám si parametry v textu, tak abych je mohl vložit do editoru:
            final String parametersInText = ParameterToText.getParametersInText(parameters, false);

            /*
             * Nyní mám v proměnné parametersInText všechny parametry metody, mohu sestavit
             * příkaz, která vložím do okna aby uživatel mohl vidět text metody i s
             * parametry:
             */
            final String methodInText = m.getName() + "(" + parametersInText + ")";

            /*
             * Proměnná, do které vložím návratovou hodnotu metody, bud s balíčky nebo bez
             * nich
             */
            final String returnType = ParameterToText.getMethodReturnTypeInText(m, false);

            /*
             * Získání parametrů v textu, podobně jako výše, akorát tato metoda vrátí název
             * parametru vždy s prvním malým písmenem:
             */
            final String parametersFirstCharSmall = getParametersInTextWithFirsthSmallWord(parameters);

            /*
             * Nyní sestavím celkový - finální text, který se vloži do editoru po zvolení
             * dané metody uživatelem:
             */
            final String textOfMethodToInsert = m.getName() + "(" + parametersFirstCharSmall + ");";

            /*
             * Note:
             *
             * Zde bych mohl nechat jen ten první řádek, kde není doplněn popisek k té
             * metodě, takže by v tom druhém okně kde má být popisek k označené hodnotě v
             * dialogu auto - doplňování byla ta hodnota, která se doplní, ale dám tam celý
             * text, resp. celou hlavičku té metody. Toto přijde vhod například, když
             * uživatel využívá metodu z předků, nebo například statické metody z jiných
             * tříd.
             */
            //			provider.addCompletion(new ShorthandCompletion(provider, methodInText, textOfMethodToInsert, returnType));
            addCompletion(provider, methodInText, textOfMethodToInsert, returnType, m.toString());
        }
    }


    /**
     * Získání veškerých proměnných z veškerých vnitřních tříd.
     * <p>
     * Princip je takový, že classes jsou vždy vnitřní třídy. Veškeré vnitřní třídy se proiterují a získají se z nich
     * veškeré proměnné, které se přidají do listu fieldList. Rekurzí se pokračuje pro všechny vnitřní třídy iterované
     * vnitřní třídy v classes.
     * <p>
     * Neřeší se zde viditelnost vnitřních tříd nebo proměnných, ani nemusí být statické apod. Stručné se získají
     * veškeré proměnné ze všech vnitřních tříd.
     *
     * @param classes
     *         - pole obsahující vnitřní třídy nějaké třídy. Tyto třídy se proiterují a pro každou vnitřní třídu se
     *         rekurzí pokračuje.
     * @param fieldList
     *         - list, do kterého se vkládají veškeré nalezené proměnné ze všech vnitřních tříd v classes.
     *
     * @return fieldlist, který bude obsahovat veškeré získané proměnné z vnitřních tříd (classes).
     */
    private static List<Field> getAllFieldsInAllInnerClasses(final Class<?>[] classes, final List<Field> fieldList) {

        if (classes == null)
            return fieldList;

        Arrays.stream(classes).forEach(c -> {
            /*
             * Zde není třeba řešit viditelnost vnitřních tříd, nebo zda jsou statické apod. Jde pouze o proměnné.
             */
            final Field[] fields = c.getDeclaredFields();

            fieldList.addAll(Arrays.asList(fields));

            // Rekurzí se pokračuje pro vnitřní třídy vnitřní třídy c:
            getAllFieldsInAllInnerClasses(c.getDeclaredClasses(), fieldList);
        });

        return fieldList;
    }


    /**
     * Vtvoření přístupových metod pro veškeré proměnné, které se nachází ve všech vnitřních třídách a v třídě clazz.
     *
     * @param provider
     *         - "okno", kam se mají přidat syntaxe pro vygenerování.
     * @param clazz
     *         - třída, ze které se mají vzít veškeré vnitřní třídy a pro ty vygenerovat syntaxe pro přístupové metody.
     */
    private static void addAccessMethodsForFieldsInInnerClasses(final DefaultCompletionProvider provider,
                                                                final Class<?> clazz) {

        addAccessMethods(provider, Arrays.asList(clazz.getDeclaredFields()), clazz);

        Arrays.stream(clazz.getDeclaredClasses()).forEach(c -> addAccessMethodsForFieldsInInnerClasses(provider, c));
    }


    /**
     * Vygenerování přístupových metod k proměnným v načtené třídě v editoru kódu.
     *
     * @param provider
     *         - "okno", kam se mají přidat hodnoty, které po zvolení uživatelem vygenerují příslušnou / zvolenou
     *         přístupovou metodu.
     * @param fieldList
     *         - list s proměnnými, které se nachází v třídě otevřené editoru. Resp. jedná se o proměné, ke kterým se
     *         mají vygenerovat přístupové metody.
     * @param clazz
     *         - třída, ze které byly načteny proměnné (fieldsList).
     */
    private static void addAccessMethods(final DefaultCompletionProvider provider, final List<Field> fieldList,
                                         final Class<?> clazz) {
        // Výchozí viditelnost přístupových metod.
        final Visibility defaultVisibility = Visibility.PUBLIC;

        fieldList.forEach(f -> {
            if (f == null)
                return;

            // Pouze v případě, že proměnná není "final" bude možné vygenerovat setr na příslušnou proměnnou:
            if (!Modifier.isFinal(f.getModifiers())) {
                final String accessMethod = GenerateAccessMethodHelper.getAccessMethod(f, clazz, true, true,
                        defaultVisibility, false, false);

                final String inputText =
                        "set" + GenerateAccessMethodHelper.getFieldNameFirstLetterUpperCase(f.getName());
                addCompletion(provider, inputText, accessMethod, generateSetter);
            }

            // Getr bude možné vygenerovat v každém případě:
            final String accessMethod = GenerateAccessMethodHelper.getAccessMethod(f, clazz, true, false,
                    defaultVisibility, false, false);

            final String inputText =
                    getStartOfInputTextForGetr(f.getType()) + GenerateAccessMethodHelper.getFieldNameFirstLetterUpperCase(f.getName());

            addCompletion(provider, inputText, accessMethod, generateGetter);
        });
    }


    /**
     * Získání prvních dvou nebo tří písmen, která značí getr pro nějakou proměnnou.
     * <p>
     * V případě, že se jedná o logický datový typ proměnné clazz, pak se vrátí "is", jinak se vrátí "get".
     *
     * @param clazz
     *         - datový typ nějaké proměnné, pro niž se má vrátit začátek metody - getru.
     *
     * @return "is" v případě, že je clazz logického datového typu, jinak "get".
     */
    private static String getStartOfInputTextForGetr(final Class<?> clazz) {
        if (clazz.equals(Boolean.class) || clazz.equals(boolean.class))
            return "is";
        return "get";
    }


    /**
     * Vygenerování syntaxe pro doplnění cyklů k proměnným, které jsou datového typu List, pole nebo mapa.
     *
     * @param provider
     *         - "okno", kam se mají přidat hodnoty, které se vy zvolení uživatelem doplní.
     * @param fieldsList
     *         - list obsahující veškeré proměnné, které se mají otestovat, zda se jedná o výše zmíněné typy proměnných
     *         a pro ty se vygeneruje syntaxe pro sestavení cyklu.
     */
    private static void addLoops(final DefaultCompletionProvider provider, final List<Field> fieldsList) {
        fieldsList.forEach(field -> {
            if (field == null)
                return;

            // Cykly pro N - rozměrné pole:
            if (field.getType().isArray())
                addArrayLoops(provider, field);


                // Cykly pro List:
            else if (ReflectionHelper.isDataTypeOfList(field.getType()))
                addListLoops(provider, field);


                // Cykly pro mapu:
            else if (ReflectionHelper.isDataTypeOfMap(field.getType()))
                addMapLoops(provider, field);
        });
    }


    /**
     * Vytvoření syntaxe pro cykly procházejí pole field.
     *
     * @param provider
     *         - "okno", kam se mají přidat vytvořené syntaxe, aby je uživatel mohl doplnit do editoru.
     * @param field
     *         - proměnná typu N - rozměrné pole, ke kterému se mají vytvořit cykly pro jeho iterování.
     */
    private static void addArrayLoops(final DefaultCompletionProvider provider, final Field field) {
        // forEach
        final String arrayTypeInText = ParameterToText.getFirstArrayTypeOfFieldInText(field, false);

        if (arrayTypeInText != null) {
            final String forEachLoop = LoopHelper.getArrayForEachLoop(field.getName(), arrayTypeInText);
            addCompletion(provider, field.getName(), forEachLoop, TXT_FOREACH);
        }


        // for - inkrementálně:
        final String foriLoop = LoopHelper.getArrayForiLoop(field.getName());
        addCompletion(provider, field.getName(), foriLoop, "fori");


        // for - dekrementálně:
        final String arrayForrLoop = LoopHelper.getArrayForrLoop(field.getName());
        addCompletion(provider, field.getName(), arrayForrLoop, "forr");


        // while:
        final String whileLoop = LoopHelper.getArrayWhileLoop(field.getName());
        addCompletion(provider, field.getName(), whileLoop, "while");


        // do - while
        final String doWhileLoop = LoopHelper.getArrayDoWhileLoop(field.getName());
        addCompletion(provider, field.getName(), doWhileLoop, "do - while");


        // arrays.stream(x).foreach:
        final String arraysStream1 = LoopHelper.getArrayStreamForEachWithoutVar(field.getName());
        final String arraysStream2 = LoopHelper.getArrayStreamForEachWithVar(field.getName());
        addCompletion(provider, field.getName(), arraysStream1, "forEach stream (" + txtWithoutVariable + ")");
        addCompletion(provider, field.getName(), arraysStream2, "forEach stream (" + txtWithVariable + ")");


        // IntStream
        final String intStreamLoop = LoopHelper.getArrayIntStream(field.getName());
        addCompletion(provider, field.getName(), intStreamLoop, "IntStream");
    }


    /**
     * Vytvoření syntaxe pro cykly procházejí List field.
     *
     * @param provider
     *         - "okno", kam se mají přidat vytvořené syntaxe, aby je uživatel mohl doplnit do editoru.
     * @param field
     *         - proměnná typu List, ke kterému se mají vytvořit cykly pro jeho iterování.
     */
    private static void addListLoops(final DefaultCompletionProvider provider, final Field field) {
        // forEach a Iterator

        // Datový typ Listu:
        final DataTypeOfList dataTypeOfList = ReflectionHelper.getDataTypeOfList(field);

        if (dataTypeOfList.getDataTypeOfListEnum() == DataTypeOfListEnum.SPECIFIC_DATA_TYPE) {
            // Datový typ Listu v textu:
            final String dataTypeInText = ParameterToText.getDataTypeInText(dataTypeOfList.getDataType(),
                    false);

            // Text cyklu forEach:
            final String textOfForEachLoop = LoopHelper.getListForEach(field.getName(), dataTypeInText);
            addCompletion(provider, field.getName(), textOfForEachLoop, TXT_FOREACH);

            // Text cyklu forEach pro Iterator:
            final String forEachIterator = LoopHelper.getListForEachIterator(field.getName(), dataTypeInText);
            addCompletion(provider, field.getName(), forEachIterator, TXT_ITERATOR_FOREACH);

            // Text cyklu while pro Iterator:
            final String whileIterator = LoopHelper.getListWhileIterator(field.getName(), dataTypeInText);
            addCompletion(provider, field.getName(), whileIterator, TXT_ITERATOR_WHILE);

            // Text cyklu do - while pro Iterator:
            final String doWhileIterator = LoopHelper.getListDoWhileIterator(field.getName(), dataTypeInText);
            addCompletion(provider, field.getName(), doWhileIterator, TXT_ITERATOR_DO_WHILE);
        } else if (dataTypeOfList.getDataTypeOfListEnum() == DataTypeOfListEnum.WITHOUT_DATA_TYPE || dataTypeOfList.getDataTypeOfListEnum() == DataTypeOfListEnum.WILDCARD_TYPE) {
            /*
             * V případě, že List nemá definován datový typ. Iteruje se skrze typ Object. Popř. Wildcard.
             */

            // Text cyklu forEach:
            final String textOfForEachLoop = LoopHelper.getListForEach(field.getName(), "Object");
            addCompletion(provider, field.getName(), textOfForEachLoop, TXT_FOREACH);

            // Text cyklu forEach pro Iterator:
            final String forEachIterator = LoopHelper.getListForEachIterator(field.getName(), "?");
            addCompletion(provider, field.getName(), forEachIterator, TXT_ITERATOR_FOREACH);

            // Text cyklu while pro Iterator:
            final String whileIterator = LoopHelper.getListWhileIterator(field.getName(), "?");
            addCompletion(provider, field.getName(), whileIterator, TXT_ITERATOR_WHILE);

            // Text cyklu do - while pro Iterator:
            final String doWhileIterator = LoopHelper.getListDoWhileIterator(field.getName(), "?");
            addCompletion(provider, field.getName(), doWhileIterator, TXT_ITERATOR_DO_WHILE);
        }


        // for - inkrementálně:
        final String foriLoop = LoopHelper.getListFori(field.getName());
        addCompletion(provider, field.getName(), foriLoop, "fori");


        // for - dekrementálně:
        final String forrLoop = LoopHelper.getListForr(field.getName());
        addCompletion(provider, field.getName(), forrLoop, "forr");


        // while
        final String whileLoop = LoopHelper.getListWhile(field.getName());
        addCompletion(provider, field.getName(), whileLoop, "while");


        // do-while
        final String doWhileLoop = LoopHelper.getListDoWhile(field.getName());
        addCompletion(provider, field.getName(), doWhileLoop, "do - while");


        // forEach lambda
        final String lambdaForEach1 = LoopHelper.getListForEachLambdaWithoutVar(field.getName());
        final String lambdaForEach2 = LoopHelper.getListForEachLambdaWithVar(field.getName());
        addCompletion(provider, field.getName(), lambdaForEach1, TXT_FOREACH_LAMBDA + " (" + txtWithoutVariable + ")");
        addCompletion(provider, field.getName(), lambdaForEach2, TXT_FOREACH_LAMBDA + " (" + txtWithVariable + ")");


        // IntStream
        final String intStreamLoop = LoopHelper.getListIntStreamForEach(field.getName());
        addCompletion(provider, field.getName(), intStreamLoop, "IntStream");
    }


    /**
     * Vytvoření syntaxe pro cykly procházejí Mapu field.
     *
     * @param provider
     *         - "okno", kam se mají přidat vytvořené syntaxe, aby je uživatel mohl doplnit do editoru.
     * @param field
     *         - proměnná Map, ke které se mají vytvořit cykly pro její iterování.
     */
    private static void addMapLoops(final DefaultCompletionProvider provider, final Field field) {
        // Lambda forEach:
        final String mapForEach = LoopHelper.getMapForeachLambda(field.getName());

        // Lambda keySet:
        final String mapKeySetForEach1 = LoopHelper.getMapKeySetForEachWithoutVar(field.getName());
        final String mapKeySetForEach2 = LoopHelper.getMapKeySetForEachWithVar(field.getName());

        // Lambda values:
        final String mapValuesForEach1 = LoopHelper.getMapValuesForEachWithoutVar(field.getName());
        final String mapValuesForEach2 = LoopHelper.getMapValuesForEachWithVar(field.getName());

        // for - inkrementálně
        final String mapEntrySetFori = LoopHelper.getMapEntrySetFori(field.getName());
        // for - dekrementálně
        final String mapEntrySetForr = LoopHelper.getMapEntrySetForr(field.getName());

        // for keySet (inkrementálně)
        final String mapKeySetFori = LoopHelper.getMapKeySetFori(field.getName());
        // for keySet (dekrementálně)
        final String mapKeySetForr = LoopHelper.getMapKeySetForr(field.getName());

        // for values (inkrementálně)
        final String mapValuesFori = LoopHelper.getMapValuesFori(field.getName());
        // for values (dekrementálně)
        final String mapValuesForr = LoopHelper.getMapValuesForr(field.getName());


        addCompletion(provider, field.getName(), mapForEach, TXT_FOREACH_LAMBDA);

        addCompletion(provider, field.getName(), mapKeySetForEach1, "keySet forEach" + " (" + txtWithoutVariable + ")");
        addCompletion(provider, field.getName(), mapKeySetForEach2, "keySet forEach" + " (" + txtWithVariable + ")");

        addCompletion(provider, field.getName(), mapValuesForEach1, "values forEach" + " (" + txtWithoutVariable + ")");
        addCompletion(provider, field.getName(), mapValuesForEach2, "values forEach" + " (" + txtWithVariable + ")");

        addCompletion(provider, field.getName(), mapEntrySetFori, "fori entrySet");
        addCompletion(provider, field.getName(), mapEntrySetForr, "forr entrySet");

        addCompletion(provider, field.getName(), mapKeySetFori, "fori keySet");
        addCompletion(provider, field.getName(), mapKeySetForr, "forr keySet");

        addCompletion(provider, field.getName(), mapValuesFori, "fori values");
        addCompletion(provider, field.getName(), mapValuesForr, "forr values");


        // Doplnění cyklů, kde je potřeba znát typy mapy:
        if (ReflectionHelper.isParameterizedType(field.getGenericType())) {
            /*
             * Zde se jedná o mapu s definovanými parametry.
             *
             * Note:
             *
             * Může se jednat o generický nebo konkrétní datový typ. U definovaných typů v cyklu v tom není rozdíl.
             */

            // Datové typy klíče a hodnoty mapy:
            final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(field);
            final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(field);

            // Datové typy klíče a hodnoty mapy převedené do textové podoby:
            final String dataTypeOfKeyInText = ParameterToText.getDataTypeInText(keyTypeOfMap, false);
            final String dataTypeOfValueInText = ParameterToText.getDataTypeInText(valueTypeOfMap, false);

            final String mapEntryForEach = LoopHelper.getMapEntryForEach(field.getName(), dataTypeOfKeyInText,
                    dataTypeOfValueInText);
            addCompletion(provider, field.getName(), mapEntryForEach, "forEach entrySet");


            // Iterator:
            final String forEachIteratorForMap;
            final String whileIteratorForMap;
            final String doWhileIterator;

            if (keyTypeOfMap instanceof WildcardType || valueTypeOfMap instanceof WildcardType) {
                /*
                 * V případě, že je alespoň jeden z typů objektu Map (proměnné) "wildcard", je třeba přidat syntaxi:
                 * "? extends" do typu objektu Iterátor.
                 */
                forEachIteratorForMap = LoopHelper.getMapForEachIterator(field.getName(), dataTypeOfKeyInText,
                        dataTypeOfValueInText, true);
                whileIteratorForMap = LoopHelper.getMapWhileIterator(field.getName(), dataTypeOfKeyInText,
                        dataTypeOfValueInText, true);
                doWhileIterator = LoopHelper.getMapDoWhileIterator(field.getName(), dataTypeOfKeyInText,
                        dataTypeOfValueInText, true);
            } else {
                forEachIteratorForMap = LoopHelper.getMapForEachIterator(field.getName(), dataTypeOfKeyInText,
                        dataTypeOfValueInText, false);
                whileIteratorForMap = LoopHelper.getMapWhileIterator(field.getName(), dataTypeOfKeyInText,
                        dataTypeOfValueInText, false);
                doWhileIterator = LoopHelper.getMapDoWhileIterator(field.getName(), dataTypeOfKeyInText,
                        dataTypeOfValueInText, false);
            }

            addCompletion(provider, field.getName(), forEachIteratorForMap, TXT_ITERATOR_FOREACH);
            addCompletion(provider, field.getName(), whileIteratorForMap, TXT_ITERATOR_WHILE);
            addCompletion(provider, field.getName(), doWhileIterator, TXT_ITERATOR_DO_WHILE);


            final String keySetForEachIterator = LoopHelper.getMapKeySetForEachIterator(field.getName(),
                    dataTypeOfKeyInText);
            final String keySetWhileIterator = LoopHelper.getMapKeySetWhileIterator(field.getName(),
                    dataTypeOfKeyInText);
            final String keySetDoWhileIterator = LoopHelper.getMapKeySetDoWhileIterator(field.getName(),
                    dataTypeOfKeyInText);
            addCompletion(provider, field.getName(), keySetForEachIterator, "keySet forEach Iterator");
            addCompletion(provider, field.getName(), keySetWhileIterator, "keySet while Iterator");
            addCompletion(provider, field.getName(), keySetDoWhileIterator, "keySet do-while Iterator");

            final String valuesForEachIterator = LoopHelper.getMapValuesForEachIterator(field.getName(),
                    dataTypeOfValueInText);
            final String valuesWhileIterator = LoopHelper.getMapValuesWhileIterator(field.getName(),
                    dataTypeOfValueInText);
            final String valuesDoWhileIterator = LoopHelper.getMapValuesDoWhileIterator(field.getName(),
                    dataTypeOfValueInText);
            addCompletion(provider, field.getName(), valuesForEachIterator, "values forEach Iterator");
            addCompletion(provider, field.getName(), valuesWhileIterator, "values while Iterator");
            addCompletion(provider, field.getName(), valuesDoWhileIterator, "values do-while Iterator");


            // forEach keySet:
            final String keySetForEachOfMap;
            if (keyTypeOfMap instanceof WildcardType)
                keySetForEachOfMap = LoopHelper.getMapKeySetForEach(field.getName(), "Object");
            else keySetForEachOfMap = LoopHelper.getMapKeySetForEach(field.getName(), dataTypeOfKeyInText);
            addCompletion(provider, field.getName(), keySetForEachOfMap, "forEach keySet");


            // forEach values:
            final String valuesForEachOfMap;
            if (valueTypeOfMap instanceof WildcardType)
                valuesForEachOfMap = LoopHelper.getMapValuesForEach(field.getName(), "Object");
            else valuesForEachOfMap = LoopHelper.getMapValuesForEach(field.getName(), dataTypeOfValueInText);
            addCompletion(provider, field.getName(), valuesForEachOfMap, "forEach values");
        } else {
            // Zde se jedná o mapu bez definovaných datových typů:
            final String entrySetForeach = LoopHelper.getMapEntryForEachObject(field.getName());
            addCompletion(provider, field.getName(), entrySetForeach, "forEach entrySet");

            // Foreach cyklus pro keySet s iterovaným objektem typu Object:
            final String keySetForEachOfMap = LoopHelper.getMapKeySetForEach(field.getName(), "Object");
            addCompletion(provider, field.getName(), keySetForEachOfMap, "forEach keySet");

            // Foreach cyklus pro values s iterovaným objektem typu Object:
            final String valuesForEachOfMap = LoopHelper.getMapValuesForEach(field.getName(), "Object");
            addCompletion(provider, field.getName(), valuesForEachOfMap, "forEach values");

            // Iterator forEach:
            final String forEachIteratorForMap = LoopHelper.getMapForEachIterator(field.getName(), null, null, false);
            addCompletion(provider, field.getName(), forEachIteratorForMap, TXT_ITERATOR_FOREACH);

            // Iterator while:
            final String whileIteratorForMap = LoopHelper.getMapWhileIterator(field.getName(), null, null, false);
            addCompletion(provider, field.getName(), whileIteratorForMap, TXT_ITERATOR_WHILE);

            // Iterator do - while:
            final String doWhileIterator = LoopHelper.getMapDoWhileIterator(field.getName(), null, null, false);
            addCompletion(provider, field.getName(), doWhileIterator, TXT_ITERATOR_DO_WHILE);

            // keySet forEach Iterator:
            final String keySetForEachIterator = LoopHelper.getMapKeySetForEachIterator(field.getName(), null);
            addCompletion(provider, field.getName(), keySetForEachIterator, "keySet forEach Iterator");

            // keySet while Iterator:
            final String keySetWhileIterator = LoopHelper.getMapKeySetWhileIterator(field.getName(), null);
            addCompletion(provider, field.getName(), keySetWhileIterator, "keySet while Iterator");

            // keySet do - while Iterator:
            final String keySetDoWhileIterator = LoopHelper.getMapKeySetDoWhileIterator(field.getName(), null);
            addCompletion(provider, field.getName(), keySetDoWhileIterator, "keySet do-while Iterator");

            // values forEach Iterator:
            final String valuesForEachIterator = LoopHelper.getMapValuesForEachIterator(field.getName(), null);
            addCompletion(provider, field.getName(), valuesForEachIterator, "values forEach Iterator");

            // values while Iterator:
            final String valuesWhileIterator = LoopHelper.getMapValuesWhileIterator(field.getName(), null);
            addCompletion(provider, field.getName(), valuesWhileIterator, "values while Iterator");

            // values do - while Iterator:
            final String valuesDoWhileIterator = LoopHelper.getMapValuesDoWhileIterator(field.getName(), null);
            addCompletion(provider, field.getName(), valuesDoWhileIterator, "values do-while Iterator");
        }
    }


    /**
     * Metoda, která přidá položky do nápovedy, zjistí si jejich název a zda se nacházejí v nějakém balíčku nebo ne, a
     * dle toho se přidají do okna s nápovědou, resp. doplnováním příslušné informace o položce
     *
     * @param provider
     *         - "něco jako okno s nápovědou", kam se přidají informace o položce a půjdou doplnit do editoru
     * @param fieldsList
     *         - List - kolekce, která obsahuje položky, které se mají přidat do "editoru":
     */
    private static void addFieldsToProvider(final DefaultCompletionProvider provider, final List<Field> fieldsList) {
        /*
         * Note:
         *
         * Níže u přidávání proměnných je vždy zakomentován jeden řádek, který značí
         * způsob přidání příslušné proměnné do dialogu / okna s auto - doplnováním. Ta
         * zakomentovaná varianta značí, že se v tom dialogu s auto doplnováním v druhém
         * okne, ktere se zobrazi po označení nějako polozky, tak v tom druhém okne bude
         * text, který se má přidat do editoru kodu, ale ja chci, aby tam byl zobrazen
         * cely text té proměnné, to se navíc hodí, kdyz se využivá proměnná například z
         * předků příslušné třídy nebo statická hodnota, tak aby se vědělo, z jaké třídy
         * se ta proěmnná vztala.
         */


        // Cyklus pro přidání proměnných v dané třídě:
        for (final Field f : fieldsList) {
            // dat to do napovedy:
            final String fieldName = f.getName();
            final String fieldDataType = ParameterToText.getFieldInText(f, false);

            addCompletion(provider, fieldName, fieldName, fieldDataType, f.toString());
        }
    }


    /**
     * Metoda, která otestuje, zda se jedná o třídu, která se nachází v diagramu tříd, co se zjistí tak, že se
     * prohledají všechny obejkty v diagramu tříd, otestuje se, zda se nejedná o hranu a komentář, a pokud ne, pak už
     * zbývají jen třídy, tak otestuje jejich názvy, a pokud se bude nějaký shodovat, pak se jedná o třídu, která se
     * nachází v diagramu tříd, a tak mohu získat její hodnoty, apod. ale tato metoda v takovém příadpě vrátí jen true,
     * zbytek je jiná část
     *
     * @param classNameWithPackages
     *         - název třídy, která se má nacházet v nějakém balíčku v diagramu tříd, resp. třída, ze které ta třída,
     *         jejíž kód je otevřen v editoru dědí
     *
     * @return true, pokud se budou nějaké názvy tříd shodovat, jinak false
     */
    private static boolean isInheriting(final String classNameWithPackages) {

        for (final DefaultGraphCell d : GraphClass.getCellsList()) {
            // Nemá se jednat o hranu ale o třídu - resp. buNku reprezentující
            // tridu v diagramu tříd:
            // Note - stacilo by testovat pouze druhou podminku pro typ bunky v
            // grafu - pouze prevence
            if (!(d instanceof DefaultEdge) && GraphClass.KIND_OF_EDGE.isClassCell(d)) {
                // zde se nejedná o hranu ani komentár, tak otestuji názvy:
                if (classNameWithPackages.equals(d.toString()))
                    return true;
            }
        }

        return false;
    }


    /**
     * Metoda, která cyklem prohledá celé zadané jednorozměrné pole v parametru metody a vezme si datový typ každé
     * položky v poli a vezme si z něj jeho jméno, u tohoto jména převede první písmeno na malé a opět z toho utvoří
     * text pro parametry metody či konstruktoru
     *
     * @param parameters
     *         - jednorozměrné pole s parametry metody či konstruktoru
     *
     * @return text pro parametr metody či konstruktoru s prvními malými písmeny u typu proměnné v parametru
     */
    private static String getParametersInTextWithFirsthSmallWord(final Parameter[] parameters) {
        // Kvůli tomu, že si nedokážu zjistit názvy proměnných, tak po vložení metody si musím
        // znovu vzít datové typy parametrů metody, akorát jim dám první písmeno malé - coby název proměnné
        // a vložím je podobně jako v cyklu výše do jednoho textu, který přidám k metodě, coby její parametry, které
        // se pak v celku
        // vloží do editrou na pozici kurzoru

        final StringBuilder parametersFirstCharSmall = new StringBuilder();

        for (final Parameter p : parameters) {
            final String dataType = p.getType().getSimpleName();
            final String firstCharSmall = String.valueOf(Character.toLowerCase(dataType.charAt(0)));
            parametersFirstCharSmall.append(firstCharSmall).append(dataType.substring(1)).append(", ");
        }

        // Na konec opět odeberu poslední čárku:
        if (parametersFirstCharSmall.toString().contains(","))
            parametersFirstCharSmall.setLength(parametersFirstCharSmall.length() - 2);

        return parametersFirstCharSmall.toString();
    }
}