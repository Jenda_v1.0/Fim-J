package cz.uhk.fim.fimj.code_editor_abstract;

/**
 * Metody pro sestavení syntaxe cyklů.
 *
 * <i>Jedná se o třídu, který obsahuje pouze metody, které slouží pro generování syntaxe vybraných cyklů pro pole,
 * Listy a Mapy.</i>
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 18.09.2018 22:30
 */

final class LoopHelper {

    private static final String ENTRY_VARIABLE = "\t\tMap.Entry<?, ?> entry = (Map.Entry<?, ?>) iterator.next();\n";

    private static final String PRINT_KEY_AND_VALUE_OF_MAP = "\t\tSystem.out.println(\"Key: \" + entry.getKey() + \"," +
            " Value: \" + entry.getValue());\n";

    private static final String PRINT_ITERATOR_NEXT_VALUE = "\t\tSystem.out.println(iterator.next());\n";

    private static final String PRINT_KEY_FROM_ITERATOR = "\t\tSystem.out.println(\"Key: \" + iterator.next());\n";

    private static final String PRINT_VALUE_FROM_ITERATOR = "\t\tSystem.out.println(\"Value: \" + iterator.next());\n";

    private static final String WHILE_ITERATOR_CONDITION = "\twhile (iterator.hasNext()) {\n";

    private static final String DO_WHILE_ITERATOR_CONDITION = "\t} while (iterator.hasNext());\n";


    private LoopHelper() {
    }


    /**
     * Získání syntaxe cyklu for procházející pole.
     *
     * @param fieldName
     *         - název proměnné (/ pole, které se bude procházet).
     *
     * @return syntaxi pro cyklus for procházející pole fieldName.
     */
    static String getArrayForiLoop(final String fieldName) {
        return "\n\tfor (int i = 0; i < " + fieldName + ".length; i++) {\n" +
                "\t\t" + "System.out.println(" + fieldName + "[i]);\n" +
                "\t}\n";
    }


    /**
     * Získání syntaxe cyklu for procházející pole v opačném pořadí (od posledního prvku k prvnímu).
     *
     * @param fieldName
     *         - název proměnné (/ pole, které se bude procházet).
     *
     * @return syntaxi pro cyklus for procházející pole fieldName v zestupném pořadí.
     */
    static String getArrayForrLoop(final String fieldName) {
        return "\n\tfor (int i = " + fieldName + ".length - 1; i >= 0; i--) {\n" +
                "\t\t" + "System.out.println(" + fieldName + "[i]);\n" +
                "\t}\n";
    }


    /**
     * Získání syntaxe pro cyklus forEach procházející pole.
     *
     * @param fieldName
     *         - název proměnné (/ pole, které se bude procházet).
     *
     * @return syntaxi pro cyklus forEach procházející pole fieldName.
     */
    static String getArrayForEachLoop(final String fieldName, final String dataType) {
        return "\n\tfor (" + dataType + " i : " + fieldName + ") {\n" +
                "\t\tSystem.out.println(i);\n" +
                "\t}\n";
    }


    /**
     * Získání syntaxe pro cyklus while procházející pole.
     *
     * @param fieldName
     *         - název proměnné (/ pole, které se bude procházet).
     *
     * @return syntaxi pro cyklus while procházející pole fieldName.
     */
    static String getArrayWhileLoop(final String fieldName) {
        return "\n\tint i = 0;\n" +
                "\twhile (i < " + fieldName + ".length) {\n" +
                "\t\tSystem.out.println(" + fieldName + "[i]);\n" +
                "\t\ti++;\n" +
                "\t}\n";
    }


    /**
     * Získání syntaxe pro cyklus do - while procházející pole.
     *
     * @param fieldName
     *         - název proměnné (/ pole, které se bude procházet).
     *
     * @return syntaxi pro cyklus do - while procházející pole fieldName.
     */
    static String getArrayDoWhileLoop(final String fieldName) {
        return "\n\tint i = 0;\n" +
                "\tdo {\n" +
                "\t\tSystem.out.println(" + fieldName + "[i]);\n" +
                "\t\ti++;\n" +
                "\t} while(i < " + fieldName + ".length);\n";
    }


    /**
     * Získání syntaxe pro cyklus forEach procházející pole fieldName pomocí stream.
     *
     * <i>Každá hodnota se v iteraci vypíše - sestavení výpisu bez proměnné.</i>
     *
     * @param fieldName
     *         - název proměnné (/ pole, které se bude procházet).
     *
     * @return syntaxi pro cyklus forEach procházející pole fieldName pomocí stream.
     */
    static String getArrayStreamForEachWithVar(final String fieldName) {
        return "java.util.Arrays.stream(" + fieldName + ").forEach(i -> System.out.println(i));";
    }


    /**
     * Získání syntaxe pro cyklus forEach procházející pole fieldName pomocí stream.
     *
     * <i>Každá hodnota se v iteraci vypíše - sestavení výpisu s proměnnou.</i>
     *
     * @param fieldName
     *         - název proměnné (/ pole, které se bude procházet).
     *
     * @return syntaxi pro cyklus forEach procházející pole fieldName pomocí stream.
     */
    static String getArrayStreamForEachWithoutVar(final String fieldName) {
        return "java.util.Arrays.stream(" + fieldName + ").forEach(System.out::println);";
    }


    /**
     * Získání syntaxe pro cyklus forEach procházející pole fieldName pomocí IntStream.
     *
     * @param fieldName
     *         - název proměnné (/ pole, které se bude procházet).
     *
     * @return syntaxi pro cyklus forEach procházející pole fieldName pomocí IntStream.
     */
    static String getArrayIntStream(final String fieldName) {
        return "java.util.stream.IntStream.range(0, " + fieldName + ".length).forEach(i -> System.out.println(" + fieldName + "[i]));";
    }


    /**
     * Získání syntaxe pro cyklus for procházející List fieldName.
     *
     * @param fieldName
     *         - název proměnné (/ Listu, který se bude procházet).
     *
     * @return syntaxi pro cyklus for procházející List fieldName.
     */
    static String getListFori(final String fieldName) {
        return "\n\tfor (int i = 0; i < " + fieldName + ".size(); i++) {\n" +
                "\t\t" + "System.out.println(" + fieldName + ".get(i));\n" +
                "\t}\n";
    }


    /**
     * Získání syntaxe pro cyklus for procházející List fieldName v opačném pořadí (od posledního prvku k prvnímu).
     *
     * @param fieldName
     *         - název proměnné (/ Listu, který se bude procházet).
     *
     * @return syntaxi pro cyklus for procházející List fieldName v zestupném pořadí.
     */
    static String getListForr(final String fieldName) {
        return "\n\tfor (int i = " + fieldName + ".size() - 1; i >= 0; i--) {\n" +
                "\t\t" + "System.out.println(" + fieldName + ".get(i));\n" +
                "\t}\n";
    }


    /**
     * Získání syntaxe pro cyklus forEach procházející List fieldName.
     *
     * @param fieldName
     *         - název proměnné (/ Listu, který se bude procházet).
     * @param dataType
     *         - datový typ Listu v textové podobě.
     *
     * @return syntaxi pro cyklus forEach procházející List fieldName.
     */
    static String getListForEach(final String fieldName, final String dataType) {
        return "\n\tfor (" + dataType + " i : " + fieldName + ") {\n" +
                "\t\tSystem.out.println(i);\n" +
                "\t}\n";
    }


    /**
     * Získání syntaxe pro Iterator, který je procházen cyklem forEach.
     *
     * @param fieldName
     *         - název proměnné (/ Listu, který se bude procházet).
     * @param dataType
     *         - datový typ Listu v textové podobě.
     *
     * @return syntaxi pro Iterator proměnné fieldName, který je procházen cyklem forEach.
     */
    static String getListForEachIterator(final String fieldName, final String dataType) {
        return "\n\tfor (Iterator<" + dataType + "> iterator = " + fieldName + ".iterator(); iterator.hasNext();) {\n" +
                PRINT_ITERATOR_NEXT_VALUE +
                "\t}\n";
    }


    /**
     * Získání syntaxe pro Iterator, který je procházen cyklem while.
     *
     * @param fieldName
     *         - název proměnné (/ Listu, který se bude procházet).
     * @param dataType
     *         - datový typ Listu v textové podobě.
     *
     * @return syntaxi pro Iterator proměnné fieldName, který je procházen cyklem while.
     */
    static String getListWhileIterator(final String fieldName, final String dataType) {
        return "\n\tIterator<" + dataType + "> iterator = " + fieldName + ".iterator();\n" +
                WHILE_ITERATOR_CONDITION +
                PRINT_ITERATOR_NEXT_VALUE +
                "\t}\n";
    }


    /**
     * Získání syntaxe pro Iterator, který je procházen cyklem do - while.
     *
     * @param fieldName
     *         - název proměnné (/ Listu, který se bude procházet).
     * @param dataType
     *         - datový typ Listu v textové podobě.
     *
     * @return syntaxi pro Iterator proměnné fieldName, který je procházen cyklem do - while.
     */
    static String getListDoWhileIterator(final String fieldName, final String dataType) {
        return "\n\tIterator<" + dataType + "> iterator = " + fieldName + ".iterator();\n" +
                "\tdo {\n" +
                PRINT_ITERATOR_NEXT_VALUE +
                DO_WHILE_ITERATOR_CONDITION;
    }


    /**
     * Získání syntaxe pro cyklus while procházející List fieldName.
     *
     * @param fieldName
     *         - název proměnné (/ Listu, který se bude procházet).
     *
     * @return syntaxi pro cyklus while procházející List fieldName.
     */
    static String getListWhile(final String fieldName) {
        return "\n\tint i = 0;\n" +
                "\twhile (i < " + fieldName + ".size()) {\n" +
                "\t\tSystem.out.println(" + fieldName + ".get(i));\n" +
                "\t\ti++;\n" +
                "\t}\n";
    }


    /**
     * Získání syntaxe pro cyklus do - while procházející List fieldName.
     *
     * @param fieldName
     *         - název proměnné (/ Listu, který se bude procházet).
     *
     * @return syntaxi pro cyklus do - while procházející List fieldName.
     */
    static String getListDoWhile(final String fieldName) {
        return "\n\tint i = 0;\n" +
                "\tdo {\n" +
                "\t\tSystem.out.println(" + fieldName + ".get(i));\n" +
                "\t\ti++;\n" +
                "\t} while(i < " + fieldName + ".size());\n";
    }


    /**
     * Získání syntaxe pro cyklus forEach zapsaný lambda výrazy procházející List fieldName.
     *
     * <i>V každé iteraci dojde k vypsání položky do konzole. Jedná se o syntaxi pro vypsání položky bez proměnné.</i>
     *
     * @param fieldName
     *         - název proměnné (/ Listu, který se bude procházet).
     *
     * @return syntaxi pro cyklus lambda forEach procházející List fieldName.
     */
    static String getListForEachLambdaWithoutVar(final String fieldName) {
        return fieldName + ".forEach(System.out::println);";
    }


    /**
     * Získání syntaxe pro cyklus forEach zapsaný lambda výrazy procházející List fieldName.
     *
     * <i>V každé iteraci dojde k vypsání položky do konzole. Jedná se o syntaxi pro vypsání položky s proměnnou.</i>
     *
     * @param fieldName
     *         - název proměnné (/ Listu, který se bude procházet).
     *
     * @return syntaxi pro cyklus lambda forEach procházející List fieldName.
     */
    static String getListForEachLambdaWithVar(final String fieldName) {
        return fieldName + ".forEach(i -> System.out.println(i));";
    }


    /**
     * Získání syntaxe pro cyklus forEach zapsaný pomocí IntStream procházející List fieldName.
     *
     * @param fieldName
     *         - název proměnné (/ Listu, který se bude procházet).
     *
     * @return syntaxi pro cyklus IntStream forEach procházející List fieldName.
     */
    static String getListIntStreamForEach(final String fieldName) {
        return "java.util.stream.IntStream.range(0, " + fieldName + ".size()).forEach(i -> System.out.println(" + fieldName + ".get(i)));";
    }


    /**
     * Získání syntaxe pro cyklus forEach zapsaný pomocí lambda výrazů procházející Mapu fieldName.
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, která se bude procházet).
     *
     * @return syntaxi pro cyklus forEach procházející Mapu fieldName.
     */
    static String getMapForeachLambda(final String fieldName) {
        return fieldName + ".forEach((k, v) -> System.out.println(\"Key: \" + k + \", Value: \" + v));";
    }


    /**
     * Získání syntaxe pro cyklus for procházející Mapu fieldName.
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, která se bude procházet).
     *
     * @return syntaxi pro cyklus for procházející Mapu fieldName.
     */
    static String getMapEntrySetFori(final String fieldName) {
        return "\n\tfor (int i = 0; i < " + fieldName + ".entrySet().size(); i++) {\n\n" +
                "\t}\n";
    }


    /**
     * Získání syntaxe pro cyklus for procházející Mapu fieldName v opačném pořadí (od posledního prvku k prvnímu).
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, která se bude procházet).
     *
     * @return syntaxi pro cyklus for procházející Mapu fieldName v sestupném pořadí.
     */
    static String getMapEntrySetForr(final String fieldName) {
        return "\n\tfor (int i = " + fieldName + ".entrySet().size() - 1; i >= 0; i--) {\n\n" +
                "\t}\n";
    }


    /**
     * Získání syntaxe pro cyklus for procházející klíče Mapy fieldName.
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, která se bude procházet).
     *
     * @return syntaxi pro cyklus for procházející klíče Mapy fieldName.
     */
    static String getMapKeySetFori(final String fieldName) {
        return "\n\tfor (int i = 0; i < " + fieldName + ".keySet().size(); i++) {\n\n" +
                "\t}\n";
    }


    /**
     * Získání syntaxe pro cyklus for procházející klíče Mapy fieldName v opačném pořadí (od posledního prvku / klíče k
     * prvnímu).
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, která se bude procházet).
     *
     * @return syntaxi pro cyklus for procházející klíče Mapy fieldName v sestupném pořadí.
     */
    static String getMapKeySetForr(final String fieldName) {
        return "\n\tfor (int i = " + fieldName + ".keySet().size() - 1; i >= 0; i--) {\n\n" +
                "\t}\n";
    }


    /**
     * Získání syntaxe pro cyklus for procházející hodnoty Mapy fieldName.
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, která se bude procházet).
     *
     * @return syntaxi pro cyklus for procházející hodnoty Mapy fieldName.
     */
    static String getMapValuesFori(final String fieldName) {
        return "\n\tfor (int i = 0; i < " + fieldName + ".values().size(); i++) {\n\n" +
                "\t}\n";
    }


    /**
     * Získání syntaxe pro cyklus for procházející hodnoty Mapy fieldName v opačném pořadí (od posledního prvku k
     * prvnímu).
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, která se bude procházet).
     *
     * @return syntaxi pro cyklus for procházející hodnoty Mapy fieldName v sestupném pořadí.
     */
    static String getMapValuesForr(final String fieldName) {
        return "\n\tfor (int i = " + fieldName + ".values().size() - 1; i >= 0; i--) {\n\n" +
                "\t}\n";
    }


    /**
     * Získání syntaxe pro cyklus forEach zapsaný pomocí lambda výrazů procházející klíče Mapy fieldName. (Výpis hodnot
     * bez využití proměnné.)
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, která se bude procházet).
     *
     * @return syntaxi pro cyklus forEach procházející klíče Mapy fieldName.
     */
    static String getMapKeySetForEachWithoutVar(final String fieldName) {
        return fieldName + ".keySet().forEach(System.out::println);";
    }


    /**
     * Získání syntaxe pro cyklus forEach zapsaný pomocí lambda výrazů procházející klíče Mapy fieldName. (Výpis hodnot
     * s využitím proměnné.)
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, která se bude procházet).
     *
     * @return syntaxi pro cyklus forEach procházející klíče Mapy fieldName.
     */
    static String getMapKeySetForEachWithVar(final String fieldName) {
        return fieldName + ".keySet().forEach(k -> System.out.println(\"Key: \" + k));";
    }


    /**
     * Získání syntaxe pro cyklus forEach zapsaný pomocí lambda výrazů procházející hodnoty Mapy fieldName. (Výpis
     * hodnot bez využití proměnné.)
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, která se bude procházet).
     *
     * @return syntaxi pro cyklus forEach procházející hodnoty Mapy fieldName.
     */
    static String getMapValuesForEachWithoutVar(final String fieldName) {
        return fieldName + ".values().forEach(System.out::println);";
    }


    /**
     * Získání syntaxe pro cyklus forEach zapsaný pomocí lambda výrazů procházející hodnoty Mapy fieldName. (Výpis
     * hodnot s využitím proměnné.)
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, která se bude procházet).
     *
     * @return syntaxi pro cyklus forEach procházející hodnoty Mapy fieldName.
     */
    static String getMapValuesForEachWithVar(final String fieldName) {
        return fieldName + ".values().forEach(v -> System.out.println(\"Value: \" + v));";
    }


    /**
     * Získání syntaxe pro cyklus forEach procházející (entrySet) veškeré položky Mapy fieldName.
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, která se bude procházet).
     *
     * @return syntaxi pro cyklus forEach procházející položky Mapy fieldName.
     */
    static String getMapEntryForEach(final String fieldName, final String dataTypeOfKey, final String dataTypeOfValue) {
        return "\n\tfor (Map.Entry<" + dataTypeOfKey + ", " + dataTypeOfValue + "> entry : " + fieldName + ".entrySet" +
                "()) {\n" +
                PRINT_KEY_AND_VALUE_OF_MAP +
                "\t}\n";
    }


    /**
     * Získání syntaxe pro cyklus forEach procházející veškeré položky Mapy fieldName.
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, která se bude procházet).
     *
     * @return syntaxi pro cyklus forEach procházející položky Mapy fieldName.
     */
    static String getMapEntryForEachObject(final String fieldName) {
        return "\n\tfor (Object o : " + fieldName + ".entrySet()) {\n" +
                "\t\tMap.Entry<?, ?> entry = (Map.Entry<?, ?>) o;\n" +
                PRINT_KEY_AND_VALUE_OF_MAP +
                "\t}\n";
    }


    /**
     * Získání syntaxe pro Iterátor, který je procházen cyklem forEach.
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, která se bude procházet).
     * @param keyType
     *         - datový typ klíče mapy v textové podobě.
     * @param valueType
     *         - datový typ hodnoty mapy v textové podobě.
     * @param addExtends
     *         - true v případě, že se má přidat syntaxe: "? extends" do typu Iterator. Toto nastane v případě, když má
     *         proměnná typu Map alespoň jeden z typů klíče nebo hodnoty wildcard: "?". Jinak false - uvedená syntaxe se
     *         nepřidá.
     *
     * @return syntaxi pro Iterator proměnné fieldName, který je procházen cyklem forEach.
     */
    static String getMapForEachIterator(final String fieldName, final String keyType, final String valueType,
                                        final boolean addExtends) {
        final String iteratorForEach;

        if (keyType == null && valueType == null)
            iteratorForEach = "\n\tfor (Iterator<?> iterator = " + fieldName + ".entrySet().iterator(); iterator" +
                    ".hasNext();) {\n" +
                    ENTRY_VARIABLE +
                    PRINT_KEY_AND_VALUE_OF_MAP +
                    "\t}\n";
        else {
            if (addExtends)
                iteratorForEach =
                        "\n\tfor (Iterator<? extends Map.Entry<" + keyType + ", " + valueType + ">> iterator = " + fieldName +
                                ".entrySet().iterator(); iterator.hasNext();) {\n" +
                                "\t\tMap.Entry<" + keyType + ", " + valueType + "> entry = iterator.next();\n" +
                                PRINT_KEY_AND_VALUE_OF_MAP +
                                "\t}\n";
            else iteratorForEach =
                    "\n\tfor (Iterator<Map.Entry<" + keyType + ", " + valueType + ">> iterator = " + fieldName +
                            ".entrySet().iterator(); iterator.hasNext();) {\n" +
                            "\t\tMap.Entry<" + keyType + ", " + valueType + "> entry = iterator.next();\n" +
                            PRINT_KEY_AND_VALUE_OF_MAP +
                            "\t}\n";
        }


        return iteratorForEach;
    }


    /**
     * Získání syntaxe pro Iterátor, který je procházen cyklem while.
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, která se bude procházet).
     * @param keyType
     *         - datový typ klíče mapy v textové podobě.
     * @param valueType
     *         - datový typ hodnoty mapy v textové podobě.
     * @param addExtends
     *         - true v případě, že se má přidat syntaxe: "? extends" do typu Iterator. Toto nastane v případě, když má
     *         proměnná typu Map alespoň jeden z typů klíče nebo hodnoty wildcard: "?". Jinak false - uvedená syntaxe se
     *         nepřidá.
     *
     * @return syntaxi pro Iterator proměnné fieldName, který je procházen cyklem while.
     */
    static String getMapWhileIterator(final String fieldName, final String keyType, final String valueType,
                                      final boolean addExtends) {
        final String iteratorWhile;

        if (keyType == null && valueType == null)
            iteratorWhile = "\n\tIterator<?> iterator = " + fieldName + ".entrySet().iterator();\n" +
                    WHILE_ITERATOR_CONDITION +
                    ENTRY_VARIABLE +
                    PRINT_KEY_AND_VALUE_OF_MAP +
                    "\t}\n";
        else {
            if (addExtends)
                iteratorWhile =
                        "\n\tIterator<? extends Map.Entry<" + keyType + ", " + valueType + ">> iterator = " + fieldName +
                                ".entrySet().iterator();\n" +
                                WHILE_ITERATOR_CONDITION +
                                "\t\tMap.Entry<" + keyType + ", " + valueType + "> entry = iterator.next();\n" +
                                PRINT_KEY_AND_VALUE_OF_MAP +
                                "\t}\n";

            else iteratorWhile = "\n\tIterator<Map.Entry<" + keyType + ", " + valueType + ">> iterator = " + fieldName +
                    ".entrySet().iterator();\n" +
                    WHILE_ITERATOR_CONDITION +
                    "\t\tMap.Entry<" + keyType + ", " + valueType + "> entry = iterator.next();\n" +
                    PRINT_KEY_AND_VALUE_OF_MAP +
                    "\t}\n";
        }


        return iteratorWhile;
    }


    /**
     * Získání syntaxe pro Iterátor, který je procházen cyklem do - while.
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, která se bude procházet).
     * @param keyType
     *         - datový typ klíče mapy v textové podobě.
     * @param valueType
     *         - datový typ hodnoty mapy v textové podobě.
     * @param addExtends
     *         - true v případě, že se má přidat syntaxe: "? extends" do typu Iterator. Toto nastane v případě, když má
     *         proměnná typu Map alespoň jeden z typů klíče nebo hodnoty wildcard: "?". Jinak false - uvedená syntaxe se
     *         nepřidá.
     *
     * @return syntaxi pro Iterator proměnné fieldName, který je procházen cyklem do - while.
     */
    static String getMapDoWhileIterator(final String fieldName, final String keyType, final String valueType,
                                        final boolean addExtends) {
        final String iteratorDoWhile;

        if (keyType == null && valueType == null)
            iteratorDoWhile = "\n\tIterator<?> iterator = " + fieldName + ".entrySet().iterator();\n" +
                    "\tdo {\n" +
                    ENTRY_VARIABLE +
                    PRINT_KEY_AND_VALUE_OF_MAP +
                    DO_WHILE_ITERATOR_CONDITION;
        else {
            if (addExtends)
                iteratorDoWhile =
                        "\n\tIterator<? extends Map.Entry<" + keyType + ", " + valueType + ">> iterator = " + fieldName +
                                ".entrySet().iterator();\n" +
                                "\tdo {\n" +
                                "\t\tMap.Entry<" + keyType + ", " + valueType + "> entry = iterator.next();\n" +
                                PRINT_KEY_AND_VALUE_OF_MAP +
                                DO_WHILE_ITERATOR_CONDITION;

            else iteratorDoWhile =
                    "\n\tIterator<Map.Entry<" + keyType + ", " + valueType + ">> iterator = " + fieldName +
                            ".entrySet().iterator();\n" +
                            "\tdo {\n" +
                            "\t\tMap.Entry<" + keyType + ", " + valueType + "> entry = iterator.next();\n" +
                            PRINT_KEY_AND_VALUE_OF_MAP +
                            DO_WHILE_ITERATOR_CONDITION;
        }

        return iteratorDoWhile;
    }


    /**
     * Získání syntaxe pro keySet Iterátor, který je procházen cyklem forEach.
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, jejíž klíče se budou procházet).
     * @param keyType
     *         - datový typ klíče mapy v textové podobě.
     *
     * @return syntaxi pro keySet Iterator proměnné fieldName, který je procházen cyklem forEach.
     */
    static String getMapKeySetForEachIterator(final String fieldName, final String keyType) {
        final String forEachIterator;

        if (keyType == null)
            forEachIterator = "\n\tfor (Iterator<?> iterator = " + fieldName + ".keySet().iterator(); iterator" +
                    ".hasNext();) {\n";

        else
            forEachIterator = "\n\tfor (Iterator<" + keyType + "> iterator = " + fieldName + ".keySet().iterator(); " +
                    "iterator.hasNext();) {\n";

        final String body = PRINT_KEY_FROM_ITERATOR + "\t}\n";

        return forEachIterator + body;
    }


    /**
     * Získání syntaxe pro keySet Iterátor, který je procházen cyklem while.
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, jejíž klíče se budou procházet).
     * @param keyType
     *         - datový typ klíče mapy v textové podobě.
     *
     * @return syntaxi pro keySet Iterator proměnné fieldName, který je procházen cyklem while.
     */
    static String getMapKeySetWhileIterator(final String fieldName, final String keyType) {
        final String iteratorWhile;

        if (keyType == null)
            iteratorWhile = "\n\tIterator<?> iterator = " + fieldName + ".keySet().iterator();\n";

        else iteratorWhile = "\n\tIterator<" + keyType + "> iterator = " + fieldName + ".keySet().iterator();\n";

        final String body = WHILE_ITERATOR_CONDITION +
                PRINT_KEY_FROM_ITERATOR +
                "\t}\n";

        return iteratorWhile + body;
    }


    /**
     * Získání syntaxe pro keySet Iterátor, který je procházen cyklem do - while.
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, jejíž klíče se budou procházet).
     * @param keyType
     *         - datový typ klíče mapy v textové podobě.
     *
     * @return syntaxi pro keySet Iterator proměnné fieldName, který je procházen cyklem do - while.
     */
    static String getMapKeySetDoWhileIterator(final String fieldName, final String keyType) {
        final String iteratorDoWhile;

        if (keyType == null)
            iteratorDoWhile = "\n\tIterator<?> iterator = " + fieldName + ".keySet().iterator();\n";

        else iteratorDoWhile = "\n\tIterator<" + keyType + "> iterator = " + fieldName + ".keySet().iterator();\n";

        final String body = "\tdo {\n" +
                PRINT_KEY_FROM_ITERATOR +
                DO_WHILE_ITERATOR_CONDITION;

        return iteratorDoWhile + body;
    }


    /**
     * Získání syntaxe pro values Iterátor, který je procházen cyklem forEach.
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, jejíž hodnoty se budou procházet).
     * @param valueType
     *         - datový typ hodnoty mapy v textové podobě.
     *
     * @return syntaxi pro values Iterator proměnné fieldName, který je procházen cyklem forEach.
     */
    static String getMapValuesForEachIterator(final String fieldName, final String valueType) {
        final String forEachIterator;

        if (valueType == null)
            forEachIterator = "\n\tfor (Iterator<?> iterator = " + fieldName + ".values().iterator(); iterator" +
                    ".hasNext();) {\n";

        else
            forEachIterator = "\n\tfor (Iterator<" + valueType + "> iterator = " + fieldName + ".values().iterator();" +
                    " iterator.hasNext();) {\n";

        final String body = PRINT_VALUE_FROM_ITERATOR + "\t}\n";

        return forEachIterator + body;
    }


    /**
     * Získání syntaxe pro values Iterátor, který je procházen cyklem while.
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, jejíž hodnoty se budou procházet).
     * @param valueType
     *         - datový typ hodnoty mapy v textové podobě.
     *
     * @return syntaxi pro values Iterator proměnné fieldName, který je procházen cyklem while.
     */
    static String getMapValuesWhileIterator(final String fieldName, final String valueType) {
        final String iteratorWhile;

        if (valueType == null)
            iteratorWhile = "\n\tIterator<?> iterator = " + fieldName + ".values().iterator();\n";

        else iteratorWhile = "\n\tIterator<" + valueType + "> iterator = " + fieldName + ".values().iterator();\n";

        final String body = WHILE_ITERATOR_CONDITION +
                PRINT_VALUE_FROM_ITERATOR +
                "\t}\n";

        return iteratorWhile + body;
    }


    /**
     * Získání syntaxe pro values Iterátor, který je procházen cyklem do - while.
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, jejíž hodnoty se budou procházet).
     * @param valueType
     *         - datový typ hodnoty mapy v textové podobě.
     *
     * @return syntaxi pro values Iterator proměnné fieldName, který je procházen cyklem do - while.
     */
    static String getMapValuesDoWhileIterator(final String fieldName, final String valueType) {
        final String iteratorDoWhile;

        if (valueType == null)
            iteratorDoWhile = "\n\tIterator<?> iterator = " + fieldName + ".values().iterator();\n";

        else iteratorDoWhile = "\n\tIterator<" + valueType + "> iterator = " + fieldName + ".values().iterator();\n";

        final String body = "\tdo {\n" +
                PRINT_VALUE_FROM_ITERATOR +
                DO_WHILE_ITERATOR_CONDITION;

        return iteratorDoWhile + body;
    }


    /**
     * Získání syntaxe pro cyklus forEach procházející klíče Mapy fieldName.
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, která se bude procházet).
     *
     * @return syntaxi pro cyklus forEach procházející klíče Mapy fieldName.
     */
    static String getMapKeySetForEach(final String fieldName, final String dataType) {
        return "\n\tfor (" + dataType + " i : " + fieldName + ".keySet()) {\n" +
                "\t\tSystem.out.println(\"Key: \" + i);\n" +
                "\t}\n";
    }


    /**
     * Získání syntaxe pro cyklus forEach procházející hodnoty Mapy fieldName.
     *
     * @param fieldName
     *         - název proměnné (/ Mapy, která se bude procházet).
     *
     * @return syntaxi pro cyklus forEach procházející hodnoty Mapy fieldName.
     */
    static String getMapValuesForEach(final String fieldName, final String dataType) {
        return "\n\tfor (" + dataType + " i : " + fieldName + ".values()) {\n" +
                "\t\tSystem.out.println(\"Value: \" + i);\n" +
                "\t}\n";
    }
}