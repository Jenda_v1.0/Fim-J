package cz.uhk.fim.fimj.code_editor_abstract;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

import javax.swing.*;

/**
 * Třída byla vytvořena pouze proto, že v editoru příkazů {@link cz.uhk.fim.fimj.commands_editor.Editor} je třeba
 * vypnout některé zkratky, které podporuje využitá knihovna {@link org.fife.ui.rsyntaxtextarea.RSyntaxTextArea}.
 * Například zkratka pro Ctrl + Z apod.
 * <p>
 * Je to z toho důvodu, že když se v editoru příkazů vypnou (libovolné) zkratky, vypnou se ve všech implementacích
 * editoru {@link org.fife.ui.rsyntaxtextarea.RSyntaxTextArea}. Z tohoto důvodu byla napsána tato třída, která příslušné
 * zkratky "zapne / vypne" dle toho, zda jsou potřeba nebo ne.
 * <p>
 * Například při spuštění aplikace při vytvoření editoru příkazů se zkratky zakážou. Při otevření jednoho z editorů kódu
 * se zkratky povolí, aby byly zkratky například Ctrl + Z dostupné apod. <i>V editoru příkazů nejsou některé zkratky
 * povoleny, zejména Ctrl + Z, protože se může odebrat většítko na začátku řádku, pak by díky zakázání editace editoru
 * (příkazů) nešlo editor využívat.</i>
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 24.09.2018 22:36
 */

public final class RSyntaxTextAreaKeyActionHelper {

    /**
     * Zvuk po stisknutí klávesové zkratky Ctrl + Enter.
     *
     * <i>
     * Zdroj:
     * <p>
     * https://stackoverflow.com/questions/13427174/disable-beep-when-backspace-is-pressed-in-an-empty-jtextfield
     * <p>
     * Na výše uvedeném zdroji jsou informace pro {@link JTextArea}, ale pro {@link RSyntaxTextArea} to bylo potřeba
     * provést trochu jinak.
     * <p>
     * Bylo třeba projít veškeré předky až k mapě s akcemi pro tu {@link RSyntaxTextArea}, tu postupně celou projít a
     * jednotlivé vypínat příslušné akce až dokud jsem nenašel tu, která vypíná ten zvuk ("RTA.DumbCompleteWordAction").
     * Tu stačí vypnout.
     * <p>
     * (Jedná se o akci spojenou právě s touto klávesovou zkratkou - Ctrl + Enter.)
     * </i>
     */
    private static final String CTRL_ENTER_SOUND_ACTION = "RTA.DumbCompleteWordAction";

    /**
     * Akce "Zpět" - stisknutí klávesové zkratky Ctrl + Z.
     */
    private static final String UNDO_ACTION = "RTA.UndoAction";

    /**
     * Akce "Zpět" - stisknutí klávesové zkratky Ctrl + Y.
     */
    private static final String REDO_ACTION = "RTA.RedoAction";


    /**
     * Reference na instanci editoru příkazů (jednu z implementací komponenty {@link RSyntaxTextArea}). <i>Tato
     * konkrétní instance není potřeba, je možné využít libovolnou instanci uvedené komponenty, změny se projeví ve
     * všech implementacích.</i>
     */
    private static RSyntaxTextArea rSyntaxTextArea;


    private RSyntaxTextAreaKeyActionHelper() {
    }


    /**
     * Povolení / Zakázání vybraných klávesových zkratek pro komponentu {@link RSyntaxTextArea}.
     *
     * <i>Jedná se o povolení nebo zakázání vybraných akcí, které jsou zakázány pro editor příkazů, ale jsou povoleny
     * pro veškeré ostatní editory, které využívají uvedenou komponentu.</i>
     *
     * @param enable
     *         - true v případě, že se mají zkratky povolit. Jinak false pro zakázání.
     */
    public static void enableSpecificAction(final boolean enable) {
        enableAction(CTRL_ENTER_SOUND_ACTION, enable);
        enableAction(UNDO_ACTION, enable);
        enableAction(REDO_ACTION, enable);
    }


    /**
     * Zakázání / Povolení akce s názvem actionName v komponentě rSyntaxTextArea.
     *
     * @param actionName
     *         - název akce v mapě s akcemi v komponentě rSyntaxTextArea, která se má zakázat nebo povolit (enable).
     * @param enable
     *         - true v případě, že se má akce actionName povolit. Jinak false pro zakázání akce.
     */
    private static void enableAction(final String actionName, final boolean enable) {
        // Nemělo by nikdy nastat:
        if (rSyntaxTextArea == null)
            return;

        final Action actSound = rSyntaxTextArea.getActionMap().getParent().getParent().getParent().get(actionName);

        if (actSound != null)
            actSound.setEnabled(enable);
    }


    /**
     * Nastavení komponenty, na které se budou volat metody pro zapnutí / vypnutí příslušných klávesových zkratech.
     *
     * @param rSyntaxTextArea
     *         - komponenta, které se budou vypínat / zapínat klávesové zkratky (platí pro veškeré její implementace).
     */
    public static void setSyntaxTextArea(final RSyntaxTextArea rSyntaxTextArea) {
        RSyntaxTextAreaKeyActionHelper.rSyntaxTextArea = rSyntaxTextArea;
    }
}