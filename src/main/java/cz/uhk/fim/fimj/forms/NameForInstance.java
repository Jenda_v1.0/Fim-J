package cz.uhk.fim.fimj.forms;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;

/**
 * Tato třída slouží pouze jako dialog, do kterého uživatel zadá název reference, resp. referenci, pro nový objekt.
 * <p>
 * Jde o to, že uživatel zavolal nějakou metodu pomocí kontextového menu nad instancí nebo třídou a ta metoda vrátila
 * nějakou instanci třídy, jejíž reprezentace není v diagramu instancí, ale tato instance je instance nějaké třídy,
 * která se nachází v diagramu tříd. Takže do toho dialogu se zadá pouze reference na tuto získanou instanci, ale jen v
 * případě, že tak uživatel bude chtít.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class NameForInstance extends SourceDialogForm implements LanguageInterface, ActionListener {

	private static final long serialVersionUID = 1L;

	

	// lblInstanceClassInfo - informace o třídě - model.data.ClassName
	// lblInstance - Text pro slovo: Instance
	// lblInsertReference - text pro informaci pro uživatele, že má zadat referenci nainstanci
	// lblErrDupliciteReference - chybový label, že zadal duplicitní referenci
	private final JLabel lblInstanceClassInfo, lblInstance, lblInsertReference, lblErrDupliciteReference;
	
	
	
	
	
	/**
	 * Textové pole pro zadání nové reference.
	 */
	private final JTextField txtReference;
	
	
	
	
	
	
	/**
	 * List, který obsahuje názvy veškerých dosud vytvořených referencí na instance
	 * v diagramu tříd a prměnné v editor příkazů. Tento list zde není povinný,
	 * stačí využit metodu ve třídě Instances, ale pro přehlednost si je zde předám.
	 */
	private final List<String> referencesList;
	
	
	
	
	
	// Texty pro chybová hlášení:
	private static String txtBadSyntaxInReference, txtReferenceAlreadyExist, txtJopEmptyFieldText,
			txtJopEmptyFieldTitle, txtJopBadSyntaxText, txtJopBadSyntaxTitle, txtJopDupliciteReferenceText,
			txtJopDupliciteReferenceTitle;
	
	



	/**
	 * Konstruktor této třídy.
	 * 
	 * @param referencesList
	 *            - list, který obsahuje veškeré zadané reference na instance v
	 *            diagramu instancí i na vytvořené "dočasné" proměnné v editoru
	 *            příkazů. Tento list by zde nemusel být lze získat dle potřeby
	 *            staticky, ale pro přehlednost je zde.
	 * 
	 * @param classNameWithPackages
	 *            - název třídy i s balíčky, například: model.data.ClassName
	 * 
	 * @param objInstance
	 *            - instance nějaké třídy, která se nachází v diagramu tříd, ale
	 *            tato konkrétní instance ještě nemá reprezentaci v diagramu
	 *            instancí.
	 */
	public NameForInstance(final List<String> referencesList, final String classNameWithPackages,
			final Object objInstance) {
		super();
		
		this.referencesList = referencesList;
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/app/ReferenceIcon.png", new Dimension(500, 300), true);
		
		
		gbc = createGbc();
		
		
		
		index = 0;
		
		
		
		lblInstanceClassInfo = new JLabel(classNameWithPackages);
		setGbc(index, 0, lblInstanceClassInfo);
		

		
		lblInstance = new JLabel(objInstance.toString());
		setGbc(++index, 0, lblInstance);
		
		
		lblInsertReference = new JLabel();
		setGbc(++index, 0, lblInsertReference);
		
		
		
		

		
		
		
		
		
		
		
		
		/*
		 * Název třídy:
		 */
		final String className = objInstance.getClass().getSimpleName();

		/*
		 * První písmeno bude vždy malě, zbytek nachám.
		 */
		final String firstClassName = String.valueOf(Character.toLowerCase(className.charAt(0)))
				+ className.substring(1);
		
		
		/*
		 * Nastavím si index pro název reference, aby se nepletl název s jiným, již
		 * použitý:
		 */
		int instanceIndex = 1;
		
		/*
		 * Nastavím si název reference
		 */
		String instanceNameForUse = firstClassName + instanceIndex;
		
		
		/*
		 * Náelsující cyklus bude inkrementovat instanci, dokud bude název instance již
		 * použit, až se najde název, instnce (reference), který ještě není pooužit, tak
		 * cyklus skončí a zísaný název reference se nastaví na použití:
		 */
		while (referencesList.contains(instanceNameForUse))
			// Zde je již název reference použit, tak zvýším index:
			instanceNameForUse = instanceNameForUse.substring(0, instanceNameForUse.length() - 1) + ++instanceIndex;
		
		
		
		txtReference = new JTextField(instanceNameForUse);
		txtReference.addKeyListener(getKeyAdapter());
		setGbc(++index, 0, txtReference);
		
		
		
		
		lblErrDupliciteReference = new JLabel();
		lblErrDupliciteReference.setFont(ERROR_FONT);
		lblErrDupliciteReference.setForeground(Color.RED);
		lblErrDupliciteReference.setVisible(false);
		setGbc(++index, 0, lblErrDupliciteReference);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		btnOk = new JButton();
		btnCancel = new JButton();
		
		btnOk.addActionListener(this);
		btnCancel.addActionListener(this);
		
		pnlButtons.add(btnOk);
		pnlButtons.add(btnCancel);
		
		setGbc(++index, 0, pnlButtons);
		
		
		
		
		
		
		addWindowListener(getWindowsAdapter());
		
		
		
		pack();
		setLocationRelativeTo(null);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zobrazí tento dialog a až tento dialog uživatel zavře ať už
	 * tak, že potvrdí název reference nebo zavře dialog bez potvrzení nové
	 * reference, tak se vrátí buď null, pokud dialog zavřen bez potvrzení
	 * reference, nebo se vrátí text z textového pole, pokud uživatel potvrdil název
	 * nové reference, ale pouze v případě, že je zadaná reference v pořádku, tj.
	 * pole není prázdné, splňuje požadovanou syntaxi a nejedná se o duplicitní
	 * referenci.
	 * 
	 * @return null nebo zadanou referenci (viz popis výše).
	 */
	public final String getNewReference() {
		setVisible(true);

		if (variable)
			return txtReference.getText().trim();

		return null;
	}
	
	
	

	
	
	
	
	/**
	 * Metoda, která vytvoří Key adapter, který reaguje na stisknutí klávesy enter
	 * tak, že se otestují data pro potvrzení reference. Dále klávesa Escape pro
	 * ukončení / zavření dialogu a pro ostatní klávesy se testuje, zda se zadán
	 * název reference v požadované syntaxi.
	 * 
	 * @return výše popsaný KeyAdapter
	 */
	private KeyAdapter getKeyAdapter() {
		return new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					// Provede se kontrola dat:
					testData();
				
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					variable = false;
					dispose();
				}
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				if (!txtReference.getText().replaceAll("\\s", "").isEmpty()) {
					// Symtaxe:
					if (!txtReference.getText().trim().matches(REG_EX_FOR_REFERENCE)) {						
						lblErrDupliciteReference.setText(txtBadSyntaxInReference);						
						
						if (!lblErrDupliciteReference.isVisible())
							lblErrDupliciteReference.setVisible(true);
					}
					
					
					// Duplicity:
					else if (referencesList.stream().anyMatch(r -> r.equalsIgnoreCase(txtReference.getText().trim()))) {						
						lblErrDupliciteReference.setText(txtReferenceAlreadyExist);
						
						if (!lblErrDupliciteReference.isVisible())
							lblErrDupliciteReference.setVisible(true);
					}
					
					else if (lblErrDupliciteReference.isVisible())
						lblErrDupliciteReference.setVisible(false);
				}

				else if (lblErrDupliciteReference.isVisible())
					lblErrDupliciteReference.setVisible(false);
			}
		};
	}
	
	
	
	
	

	
	
	/**
	 * Metoda, která zkontroluje zadaná data, resp. zadaný název reference. Zda je
	 * vůbec nějaká reference zadána (není prázdné pole) a zda splňuje požadovanou
	 * syntaxi, pokud ne, vypíše se chybové hlášení, pokud ano, nastaví se příslušná
	 * proměnná a zavře se dalog, takže se vrátí příslušé hodnoty pro vytvoření
	 * reprezentace pro příslušnou instanci.
	 */
	private void testData() {
		// Prázdné pole:
		if (txtReference.getText().replaceAll("\\s", "").isEmpty()) {
			JOptionPane.showMessageDialog(this, txtJopEmptyFieldText, txtJopEmptyFieldTitle, JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		// Syntaxe pro referenci:
		if (!txtReference.getText().matches(REG_EX_FOR_REFERENCE)) {
			JOptionPane.showMessageDialog(this, txtJopBadSyntaxText, txtJopBadSyntaxTitle, JOptionPane.ERROR_MESSAGE);
			return;
		}

		
		/*
		 * Zadaná reference v textovém poli.
		 */
		final String enteredReference = txtReference.getText().trim();
		
		/*
		 * Zjistím si, zda již byla nalezena nějaká duplicity:
		 */
		final boolean duplicite = referencesList.stream().anyMatch(r -> r.equalsIgnoreCase(enteredReference));
		
		// Zda byla zadána duplicitní reference:
		if (duplicite) {
			JOptionPane.showMessageDialog(this, txtJopDupliciteReferenceText, txtJopDupliciteReferenceTitle,
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		// Zde je vše v pořádku, tak mohu nastavit proměnnou a uzavřít dialog:
		variable = true;
		dispose();
	}
	
	
	

	
	

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {// Zbytečná podmínka
			if (e.getSource() == btnOk)
				testData();
			
			
			else if (e.getSource() == btnCancel) {
				variable = false;
				dispose();
			}
		}
	}
	
	
	
	
	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			setTitle(properties.getProperty("NFI_DialogTitle", Constants.NFI_DIALOG_TITLE));
			
			lblInstanceClassInfo.setText(properties.getProperty("NFI_LblInstanceClassInfo", Constants.NFI_LBL_INSTANCE_CLASS_INFO) + ": " + lblInstanceClassInfo.getText());
			lblInstance.setText(properties.getProperty("NFI_LblInstance", Constants.NFI_LBL_INSTANCE) + ": " + lblInstance.getText());
			lblInsertReference.setText(properties.getProperty("NFI_InsertReference", Constants.NFI_INSERT_REFERENCE));
			
			txtBadSyntaxInReference = properties.getProperty("NFI_TxtBadSyntaxInReference", Constants.NFI_TXT_BAD_SYNTAX_IN_REFERENCE);

			txtReferenceAlreadyExist = properties.getProperty("NFI_TxtReferenceAlreadyExist", Constants.NFI_TXT_REFERENCE_ALREADY_EXIST);

			txtJopEmptyFieldText = properties.getProperty("NFI_TxtJopEmptyFieldText", Constants.NFI_TXT_JOP_EMPTY_FIELD_TEXT);
			txtJopEmptyFieldTitle = properties.getProperty("NFI_TxtJopEmptyFieldTitle", Constants.NFI_TXT_JOP_EMPTY_FIELD_TITLE);

			txtJopBadSyntaxText = properties.getProperty("NFI_TxtjopBadSyntaxText", Constants.NFI_TXT_JOP_BAD_SYNTAX_TEXT);
			txtJopBadSyntaxTitle = properties.getProperty("NFI_TxtJopBadSyntaxTitle", Constants.NFI_TXT_JOP_BAD_SYNTAX_TITLE);

			txtJopDupliciteReferenceText = properties.getProperty("NFI_TxtJopDupliciteReferenceText", Constants.NFI_TXT_JOP_DUPLICITE_REFERENCE_TEXT);
			txtJopDupliciteReferenceTitle = properties.getProperty("NFI_TxtJopDupliciteReferenceTitle", Constants.NFI_TXT_JOP_DUPLICITE_REFERENCE_TITLE);
			
			btnOk.setText(properties.getProperty("NFI_BtnOk", Constants.NFI_BTN_OK));
			btnCancel.setText(properties.getProperty("NFI_BtnCancel", Constants.NFI_BTN_CANCEL));
		}
		
		
		else {
			setTitle(Constants.NFI_DIALOG_TITLE);
			
			lblInstanceClassInfo.setText(Constants.NFI_LBL_INSTANCE_CLASS_INFO + ": " + lblInstanceClassInfo.getText());
			lblInstance.setText(Constants.NFI_LBL_INSTANCE + ": " + lblInstance.getText());
			lblInsertReference.setText(Constants.NFI_INSERT_REFERENCE);
			
			txtBadSyntaxInReference = Constants.NFI_TXT_BAD_SYNTAX_IN_REFERENCE;

			txtReferenceAlreadyExist = Constants.NFI_TXT_REFERENCE_ALREADY_EXIST;

			txtJopEmptyFieldText = Constants.NFI_TXT_JOP_EMPTY_FIELD_TEXT;
			txtJopEmptyFieldTitle = Constants.NFI_TXT_JOP_EMPTY_FIELD_TITLE;

			txtJopBadSyntaxText = Constants.NFI_TXT_JOP_BAD_SYNTAX_TEXT;
			txtJopBadSyntaxTitle = Constants.NFI_TXT_JOP_BAD_SYNTAX_TITLE;

			txtJopDupliciteReferenceText = Constants.NFI_TXT_JOP_DUPLICITE_REFERENCE_TEXT;
			txtJopDupliciteReferenceTitle = Constants.NFI_TXT_JOP_DUPLICITE_REFERENCE_TITLE;
			
			btnOk.setText(Constants.NFI_BTN_OK);
			btnCancel.setText(Constants.NFI_BTN_CANCEL);
		}
		
		lblInsertReference.setText(lblInsertReference.getText() + ": ");
	}
}
