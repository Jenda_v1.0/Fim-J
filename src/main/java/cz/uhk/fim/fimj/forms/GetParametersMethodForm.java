package cz.uhk.fim.fimj.forms;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;
import cz.uhk.fim.fimj.instances.Instances;
import cz.uhk.fim.fimj.instances.OperationsWithInstances;
import cz.uhk.fim.fimj.output_editor.OutputEditor;
import cz.uhk.fim.fimj.parameters_for_method.ClassOrInstanceForParameter;
import cz.uhk.fim.fimj.parameters_for_method.FieldsInfo;
import cz.uhk.fim.fimj.parameters_for_method.FlexibleCountOfParameters;
import cz.uhk.fim.fimj.parameters_for_method.KindOfVariable;
import cz.uhk.fim.fimj.parameters_for_method.ParameterValue;
import cz.uhk.fim.fimj.reflection_support.ParameterToText;

/**
 * Tento dialog slouží pro získání hodnot parametrů do metody, kterou se uživatel rozhodl zavolat
 * <p>
 * Metoda se zavolá po kliknutí na příslušnou položku v metnu, které se zobrazí po kliknutí pravým tlačítkem na instanci
 * třídy
 * <p>
 * když se v tomto menu klikne na metodu, která potřebuje parametry, zobrazí se tento dialog, který v příslušné metodě
 * vrátí získané hodnoty pro parametry do metoduy od uživatele
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class GetParametersMethodForm extends FlexibleCountOfParameters implements LanguageInterface, ActionListener {

	private static final long serialVersionUID = 1L;





	// výchozí velikost okna dialogu:
	private static final int FRAME_WIDTH = 650, FRAME_HEIGHT = 665;
	
	
	
	
	private static String emptyFieldParameterTitle, emptyFieldParameterText, formatErrorParamterValueTitle,
			formatErrorParameterValueText, cannotCallTheMethod;
	
	
	
	/**
	 * Pole pro získání typu parametru metod:
	 */
	private final Parameter[] parametersOfMethod;
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param method
	 *            - metoda pro zavolání.
	 * 
	 * @param languageProperties
	 *            - soubor s texty pro aplikaci, resp. v tomto konkrotní případě s
	 *            texty pro tento dialog.
	 * 
	 * @param outputEditor
	 *            - editor výstupů pro potenciální vypsání hlášek ohledě zavolání
	 *            metody, například návratová hodnota adpod
	 * 
	 * @param makeVariablesAvailable
	 *            - true, když se mají zpřístupnit proměnné z tříd, instancí tříd a
	 *            proměné vytvořené pomocí editoru příkazů a jejich hodnoty bude
	 *            možné předatdo parametru metody.
	 * 
	 * @param showCreateNewInstanceOptionInCallMethod
	 *            - logická proměnná, která značí, zda se má v případě, že se v
	 *            parametru zavolané metody nachází parametr, který je typu třídy z
	 *            diagramu tříd, tak zda se má nabídnout možnost pro vytvoření nové
	 *            instance příslušné třídy, která je v parametru metody, true ano,
	 *            false ne. Pokud se nemá zpřístupnit možnost pro vytvoření instance
	 *            příslušné třídy, tak se bude moci vybírat pouze z těch instancí,
	 *            které se aktuálně nachází v diagramu instancí, a pokud tam žádná
	 *            není, pak ani nepůjde metoda zavolat.
	 * 
	 * @param instanceDiagram
	 *            - reference na diagram instancí, je zde potřeba pouze pro předání
	 *            do objektu pro operace nad instancemi.
	 * 
	 * @param clazz
	 *            - třída, ve které se má zavolat metoda method. Tato hodnota je zde
	 *            potřeba, kvůli tomu, aby se mohli vybrat nějaké metody z předků
	 *            příslušné třídy.
	 * 
	 * @param reference
	 *            - reference na instanci v diagramu instancí, tato proměnná bude
	 *            naplněna pouze v případě, že se jedná o zavolání metody nad
	 *            instanci v díagramu instancí, jinak pokud sejedná o zavolání
	 *            statické metody nad třídou v diagramu tříd, pak bude tato proměnná
	 *            null.
	 * 
	 * @param makeMethodsAvailable
	 *            - logická proměnná, která značí, zda se mají zpřístupnit metody
	 *            pro zavolání, aby se její návratová hodnota mohla předat do
	 *            parametru metody.
	 */
	public GetParametersMethodForm(final Method method, final Properties languageProperties,
			final OutputEditor outputEditor, final boolean makeVariablesAvailable,
			final boolean showCreateNewInstanceOptionInCallMethod, final GraphInstance instanceDiagram,
			final Class<?> clazz, final String reference, final boolean makeMethodsAvailable) {

		super(makeVariablesAvailable, showCreateNewInstanceOptionInCallMethod, makeMethodsAvailable);

		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/app/ParameterOfMethodIcon.jpg",
				new Dimension(FRAME_WIDTH, FRAME_HEIGHT), true);
		
		// minimální velikost okna:
		setMinimumSize(new Dimension(350, 585));
		
		// naplnění reference na editor výstpupů, kdyby nastala chyba při vytváření instance:
		this.outputEditor = outputEditor;
		
		FlexibleCountOfParameters.instanceDiagram = instanceDiagram;
		
		operationWithInstances = new OperationsWithInstances(outputEditor, languageProperties, instanceDiagram);


		
		gbc = createGbc();
		
		index = 0;
		
		gbc.gridwidth = 2;
		
		lblInfoText = new JLabel();
		setGbc(gbc, index, 0, lblInfoText);
		
		
		
		
		
		
		
		// Vytvoření polí pro zadání hodnot pro parametry metody:
		parametersOfMethod = method.getParameters();
		
		
		
		
		
		
		
		
		// nastavím popisek meody, resp. její syntaxi s parametry:
		
		/*
		 * Získám si návratový datový typ metody, pak přidám mezeru a pak název metody.
		 * 
		 * Například: returnType methodName
		 */
		final String partOfMethod = ParameterToText.getMethodReturnTypeInText(method, true) + " " + method.getName();

		// Uložím si metodu do textové podoby i s parametry:
		textWithParameters = getParametersOfMethod(partOfMethod, parametersOfMethod);

		/*
		 * Vytvořím si label, do kterého vložím text metody i s parametry, aby uživatel
		 * měl "větší přehled" o metodě, kterou volá.
		 */
		final JLabel lblParametersInfo = new JLabel(getWrappedText(textWithParameters, FRAME_WIDTH));

		// Přidám label do okna dialogu:
		setGbc(gbc, ++index, 0, lblParametersInfo);

		/*
		 * Nastavím posluchač, který při každé změně velikosti okna dialogu aktualizuje
		 * velikost labelu s metodou, aby se příslušný text s metodou mohl "správně"
		 * zalomit.
		 */
		addComponentListener(new ComponentAdapter() {

			@Override
			public void componentResized(ComponentEvent e) {
				lblParametersInfo.setText(getWrappedText(textWithParameters, getWidth()));
			}
		});
		
		
		
		
		
		
		
			
		
		
		
		
		listOfParametersValues = new ArrayList<>();
		
		// Labley pro oznámení zadaání hodnoty parametru v epštném formátu
		errorTextList = new ArrayList<>();
		
		// pro naplnení textu do daného labelu s informací o hodnotě pro daný parametr
		labelsList = new ArrayList<>();
		
		
		
		
		fillTextVariables(languageProperties);
		
		

		
		
		
		

		
		
		
		/*
		 * Note:
		 * 
		 * Následujících několik metod bych mohl dát aké do nějaké metody, vlastně bych to vše mohl dát do nějaké metody,
		 * která by vrátila až objekty do listu: sortedAvailableFields. ale nechám to zde trochu "přehledněji", kdybych to později
		 * potřeboval ještě nějak upravit, abych měl "oddělené" získání jednotlivých hodnot.
		 */
		
		
		/*
		 * Získám si Veškeré veřejné a chráněné proměnné z vytvořených instancí, které
		 * se nachází v diagramu instancí a všechny statické veřejné a chráněné proměnné
		 * z tříd v diagramu tříd.
		 */
		final List<FieldsInfo> fieldsInfoList = getAvailableFieldsFromClassesAndInstances(outputEditor);
		
		
		/*
		 * Vytvořím si list, do kterého postupně vložím takové objekty, které je bude
		 * možné vložit do komponenty JCombobox v tomto dialogu, aby uživatel mohl
		 * zvolit hodnotu, kterou chce předat do parametru příslušné metody, kterou chce
		 * zavolat.
		 */
		final List<ParameterValue> valuesForCmbList = new ArrayList<>();

		
		/*
		 * Vložím si do listu valuesForCmbList výše získané proměnné z tříd a instanci
		 * tříd.
		 */
		fieldsInfoList.forEach(v -> valuesForCmbList
				.addAll(getParameterValuesFromFields(v.getFieldsList(), v.getReferenceName(), v.getObjInstance())));

		
		
		/*
		 * Do listu valuesForCmbList dále vložím veškeré proměnné, které uživatel
		 * vytvořil pomocí editoru příkazů.
		 */
		getCommandsEditorVariables(valuesForCmbList);
		

		
		
		
		/*
		 * List, do kterého si vložím vekšeré získané proměnné (z editoru příkazů, s
		 * tatické proměnné z tříd v diagramu tříd a proměnné z instancí tříd v diagramu
		 * instancí).
		 * 
		 * Tyto proměnné budou hlavně seřazené, nejprve proměnné vytvořené v editoru
		 * příkazů, pak statické proměnné z tříd v diagramu tříd a pak proměnné, které
		 * se nachází v nějaké instanci, na tyto proměnné budou uvedeny i reference.
		 */
		final List<ParameterValue> sortedAvailableFields = getSortedAvailableFields(valuesForCmbList);
		
		
		

		
		
		
		
		// načtu si list zkompilovaných tříd z diagramu tříd: (načtené soubory .class) a
		// všechny instance vytvořené uživatelem:
		final List<ClassOrInstanceForParameter> classOrInstancesList = getClassOrInstanceList(
				Instances.getAllInstances());	
		
		
		
		final JPanel pnlValues = addFieldForValuesToDialog(parametersOfMethod, false, classOrInstancesList,
				sortedAvailableFields, this, null, clazz, reference, isSetrInInstance(method));
		
		final JScrollPane jspValues = new JScrollPane(pnlValues);
		jspValues.setPreferredSize(new Dimension(0, 200));
		
		setGbc(gbc, ++index, 0, jspValues);
		
		
		
		
		
		// Přidání panelu s informacemi o chybách:
		setGbc(gbc, ++index, 0, getCreatedErrorList());
		
		
		
		
		
		// Zde je již list pro chybové zprávy vytvořen, tak mohu vkládat chybová upozornění,
		// tak otestuji, zda jsou všechny datové typy zadaných parametrů podporovány, pokud ne, tak to ve zmíněním listu
		// oznámím:
		if (errorTextForBadTypeOfParameter != null)
			addErrorToFieldOfDialog(null, errorTextForBadTypeOfParameter);
		
		
		
		
		
		
		
		final JPanel pnlButtons = new JPanel();
		pnlButtons.setLayout(new FlowLayout(FlowLayout.RIGHT));
		btnCancel = new JButton();
		btnOk = new JButton();
		btnCancel.addActionListener(this);
		btnOk.addActionListener(this);
		pnlButtons.add(btnOk);
		pnlButtons.add(btnCancel);				
		
		
		setGbc(gbc, ++index, 0, pnlButtons);
		
		
		
		
		
		
		

		
		
		setLanguage(languageProperties);
		
					
		
		
		addWindowListener(getWindowsAdapter());
		
		
		
		pack();
		setLocationRelativeTo(null);
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí parametry získanhé od uživatele z hodnot v jednotlivých
	 * polích
	 * 
	 * @return Jednorozměrné pole s hodnotami parametrů
	 */
	public final List<Object> getParameters(final String methodName) {
		setTitle(getTitle() + ": " + methodName);
		
		setVisible(true);
		
		// List objektů pro vrácení hodnot do zadání parametru metody
		final List<Object> objList = new ArrayList<>();
		
		if (variable) {
			// index pro typ parametru metody - z nastených parametrů z reflexe
			int index = 0;
			
			for (final JComponent c : listOfParametersValues)
				objList.add(getWrittenParameter(c, parametersOfMethod[index++]));
			
			return objList;
		}
		return objList;
	}
	
	
	

	
	
	




	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			setTitle(properties.getProperty("Gpmf_DialogTitle", Constants.GPMF_DIALOG_TITLE));
			
			lblInfoText.setText(properties.getProperty("Gpmf_LabelTextInfo", Constants.GPMF_LBL_TEXT_INFO));
			
			
			final String textToLabel = properties.getProperty("Fcop_LabelForParameter", Constants.FCOP_LBL_TEXT_INFO_PARAM);
			
			labelsList.forEach(label -> {
				final String variableText = label.getText();
				label.setText(textToLabel + " " + variableText);
			});
			
			
			btnCancel.setText(properties.getProperty("Gpmf_Button_Cancel", Constants.GPMF_BTN_CANCEL));
			btnOk.setText(properties.getProperty("Gpmf_Button_Ok", Constants.GPMF_BTN_OK));
			
			// Texty do hlášek:
			emptyFieldParameterTitle = properties.getProperty("Gpmf_JopEmptyFieldParameterTitle", Constants.GPMF_EMPTY_FIELD_PARAMETER_TITLE);
			emptyFieldParameterText = properties.getProperty("Gpmf_JopEmptyFieldParameterText", Constants.GPMF_EMPTY_FIELD_PARAMETER_TEXT);
			formatErrorParamterValueTitle = properties.getProperty("Gpmf_JopFormatErrorParameterValueTitle", Constants.GPMF_FORMAT_ERROR_PARAMETER_VALUE_TITLE);
			formatErrorParameterValueText = properties.getProperty("Gpmf_JopFormatErrorParameterValueText", Constants.GPMF_FORMAT_ERROR_PARAMETER_VALUE_TEXT);
			badFormatSomeParameterTitle = properties.getProperty("Gpmf_JopBadFormatSomeParameterTitle", Constants.GPMF_BAD_FORMAT_SOME_PARAMETER_TITLE);
			badFormatSomeParameterText = properties.getProperty("Gpmf_JopBadFormatSomeParameterText", Constants.GPMF_BAD_FORMAT_SOME_PARAMETER_TEXT);
			cannotCallTheMethod = properties.getProperty("Gpmf_JopCannotCallTheMethod", Constants.GPMF_CANNOT_CALL_THE_METHOD);
		}
		
		
		else {
			setTitle(Constants.GPMF_DIALOG_TITLE);
			
			lblTextInfo.setText(Constants.GPMF_LBL_TEXT_INFO);
						
			
			final String textTolabel = Constants.FCOP_LBL_TEXT_INFO_PARAM;
			
			labelsList.forEach(label -> {
				final String variableText = label.getText();
				label.setText(textTolabel + " " + variableText);
			});
						
			
			btnCancel.setText(Constants.GPMF_BTN_CANCEL);
			btnOk.setText(Constants.GPMF_BTN_OK);
			
			
			// Texty do hlášek:
			emptyFieldParameterTitle = Constants.GPMF_EMPTY_FIELD_PARAMETER_TITLE;
			emptyFieldParameterText = Constants.GPMF_EMPTY_FIELD_PARAMETER_TEXT;
			formatErrorParamterValueTitle = Constants.GPMF_FORMAT_ERROR_PARAMETER_VALUE_TITLE;
			formatErrorParameterValueText = Constants.GPMF_FORMAT_ERROR_PARAMETER_VALUE_TEXT;
			badFormatSomeParameterTitle = Constants.GPMF_BAD_FORMAT_SOME_PARAMETER_TITLE;
			badFormatSomeParameterText = Constants.GPMF_BAD_FORMAT_SOME_PARAMETER_TEXT;
			cannotCallTheMethod = Constants.GPMF_CANNOT_CALL_THE_METHOD;
		}
	}






	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnOk)
				testData();
			
			
			else if (e.getSource() == btnCancel) {
				variable = false;
				dispose();
			}
		}
	}
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda jsou správně zadaná data v parametrech
	 * 
	 * Postup:
	 * Prohledám všechna textová pole a zjistím, zda není nějaké prázdné -
	 * nevyplněné, případně vypíšu hlášku při tommto testování e třeba otestovat,
	 * zda se zadaná hodnota nachází ve formátu pro parametr - ten základní formát -
	 * "alespoň neco"
	 * 
	 * Poté otestuji zda nění nějaký label s chybovým upozorněním na formát hodnoty
	 * zobrazen pokud ne, tak mohu vrátit hodnoty
	 */
	public final void testData() {
		boolean isEmptyOrBadFormat = false;
		
		int index = 1;
		
		for (final JComponent c : listOfParametersValues) {
			// Otestuji, zda není pole prázdné:
			if (c instanceof JTextField) {
				if (((JTextField) c).getText().trim().isEmpty()) {
					isEmptyOrBadFormat = true;
					JOptionPane.showMessageDialog(this, index + ". " + emptyFieldParameterText,
							emptyFieldParameterTitle, JOptionPane.ERROR_MESSAGE);
					break;
				}
				
				// Otestuji, zda je v něm zadaná alespoň nějaká hodnota:			
				if (!((JTextField) c).getText().trim().matches(REG_EX_FOR_PARAMEER)) {
					isEmptyOrBadFormat = true;
					JOptionPane.showMessageDialog(this, index + ". " + formatErrorParameterValueText,
							formatErrorParamterValueTitle, JOptionPane.ERROR_MESSAGE);
					break;
				}
			}

			/*
			 * Note:
			 * 
			 * Zde bych ještě mohl testovat, zda se jedná o instanci komponenty JComboBox,
			 * ale u té mám "zajištěné", že tam vždy nějaká hodnota bude, jinak bude při
			 * nejmenším vypsána chyba.
			 * 
			 * Pokud půjde o JcomboBox typu Boolean, pak tam je vždy na výběr jen true a
			 * false a jedno z toho vždy bude označeno, nebo si může uživatel vybrat
			 * instanci nějaké třídy, případně vytvořit novou v tomto typu JComboBoxu také
			 * vždy bude označena nějaká možnost a pak je tu třetí typ JComboBoxu, kde jsou
			 * na výběr hodnoty z proměnných a první položka v tomto JComboBoxu je pro
			 * zadání hodnoty od uživatele, ale tam je to pohlídané, že vždy něco bude
			 * zadané, ale pro jistotu, abych náhodou nedošlo k nějaké chybě zde bude
			 * testována pouze tato jedna hodnota - ta první položka, nikde jinde by k chybě
			 * dojít nemělo.
			 */
			else if (c instanceof JComboBox<?>) {
				// V této podmínce pro cmb bylo potřeba využít ? pro generické typy, nemohl jsem
				// zadat parametr, tak kvůli tomu musím napsat více podmínek:

				// Zda je označena první položka, jen první lze editovat, jiná ne.
				if (((JComboBox<?>) c).getSelectedIndex() != 0)
					continue;

				/*
				 * Získám si konkrétní hodnotu z cmb:
				 */
				final Object objValue = ((JComboBox<?>) c).getSelectedItem();

				// Zde otestuji, zda je označena hodnota typu pro zadání hodnoty:
				if (objValue instanceof ParameterValue) {
					final ParameterValue pValue = (ParameterValue) objValue;

					if (pValue.getKindOfVariable().equals(KindOfVariable.TEXT_VARIABLE)) {
						/*
						 * Tato podmínka můžen nastat, ale u objektových datový typů, kde to bude
						 * nastaveno úmyslně nebo jako výchozí hodnota, Jelikož v těchto případěch
						 * nevím, čím jinám tuto null hodnotu nahradit, nebo jak to obejít, tak zde
						 * výjimečně povolám předánínull hodnoty. Je to asi lepší, než kdybych uživatele
						 * pořád "nutil" mít v každé proměnné nějakou hodnotu.
						 */
						if (pValue.getTextValue() == null)
							continue;

						if (pValue.getTextValue().trim().isEmpty()) {
							isEmptyOrBadFormat = true;
							JOptionPane.showMessageDialog(this, index + ". " + emptyFieldParameterText,
									emptyFieldParameterTitle, JOptionPane.ERROR_MESSAGE);
							break;
						}

						// Otestuji, zda je v něm zadaná alespoň nějaká hodnota:
						if (!pValue.getTextValue().trim().matches(REG_EX_FOR_PARAMEER)) {
							isEmptyOrBadFormat = true;
							JOptionPane.showMessageDialog(this, index + ". " + formatErrorParameterValueText,
									formatErrorParamterValueTitle, JOptionPane.ERROR_MESSAGE);
							break;
						}
					}
				}
			}

			index++;
		}
		
		
		if (!isEmptyOrBadFormat) {
			// Zde je vše OK:
			
			// Tak ještě otestuji, zda neni zobrazeny nějaká chybová hláška:
			if (errorTextList.isEmpty()) {
				variable = true;
				dispose();
			}
			
			// JList s chybovými hláškami není prázdný, tak nejprve otestuje, zda lze vůbec konstruktor zavolat:
			else if (errorTextForBadTypeOfParameter != null)
				JOptionPane.showMessageDialog(this, errorTextForBadTypeOfParameter, cannotCallTheMethod,
						JOptionPane.ERROR_MESSAGE);
			
			// Zde se jedná pouze o špatné parametry:
			else viewIncorrectParameters();
		}
		// Zde nic vypisovat nemusím, hlášky bly zobrazeny před ukončením cyklu
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjisti, zda se jedná o setr na nějakou proměnnou v příslušné
	 * třídě, kde se ta metoda (setr) zavolal.
	 * 
	 * To se pozná tak, že otestuji, zda je metoda veřejná, není statická, nemá
	 * návratový datový typ - nic nevrací (je void), název je v syntaxi:
	 * 'setSomeVar' - SomeVar je v podstatě libolvolná text, který pak musí
	 * obsahovat i getr, aby nebyl vybrán.
	 * 
	 * @param method
	 *            - metoda, o které se má zjistit, zda se jedná o setr na nějakou
	 *            hodnotu v příslušné instanci instanci třídy, ve které se ta metoda
	 *            nachází.
	 * 
	 * @return true, pokud je metoda setr na příslušnou instanci třídy, jinak false.
	 */
	private static String isSetrInInstance(final Method method) {
		if (Modifier.isPublic(method.getModifiers()) && !Modifier.isStatic(method.getModifiers())
				&& method.getReturnType().equals(Void.TYPE) && method.getName().startsWith("set")
				&& method.getParameterCount() == 1)
			return method.getName().substring(3);

		return null;
	}
}