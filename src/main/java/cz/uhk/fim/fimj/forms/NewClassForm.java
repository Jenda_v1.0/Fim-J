package cz.uhk.fim.fimj.forms;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.buttons_panel.ClassTypeEnum;
import cz.uhk.fim.fimj.buttons_panel.FindPackages;
import cz.uhk.fim.fimj.buttons_panel.NewClassInfo;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.ReadFile;

/**
 * Třída slouží pro vytvoření nové třídy Do tohoto dialogu se bude zadávat název třídy v podobě: model.data.třída Text
 * za poslední tečkou je název třídy, zbytek jsou balíčky
 * <p>
 * Dále se zde označí, jaký typ třídy se má vytvořit: Klasická třída, rozhraní, Abstraktní třída, Applet, Výčet, Unit
 * Test
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class NewClassForm extends CreateAndRenameClassFormAbst implements LanguageInterface, ActionListener {

	private static final long serialVersionUID = 1L;


	/**
	 * Regulární výraz pro název balíčků.
	 * 
	 * Může být v syntaxe packaName (.anotherPackage)
	 * 
	 * Tj. může obsahovat velká a malá písmena bez diakritiky, číslice a podtržítka,
	 * jednotlivé balíčky jsou oddělené desetinnou tečkou a jsou hlídány desetinné
	 * tečky na začátku a na konci.
	 */
	private static final String REG_EX_PACKAGE_TEXT = "^\\s*\\w+\\s*(\\.\\s*\\w+\\s*)*\\s*$";
	

	
	/**
	 * Proměnná, která obsahuje text coby výchozí balíček, který bude označen pro
	 * výběr balíčku, kam se má vložit třída.
	 */
	private static final String DEFAULT_PACKAGE_EXAMPLE = "model";
	
	
	
	
	/**
	 * Text pro zvolení balíčků.
	 */
	private final JLabel lblPackageName;
	
	
	/**
	 * Komponenta pro zvolení balíčku, kam se má přidat třída.
	 */
	private final JComboBox<String> cmbPackages;
	
	
	/**
	 * Text pro zadáné názvu třídy
	 */
	private final JLabel lblClassName;
	
	
	/**
	 * Textové pole pro zadání názvu třídy
	 */
	private final JTextField txtClassName;
	
	// Začkrtávací tlačítka, pro označení, jaký typ třídy chce uživatel vytvořit:
	private final JRadioButton rbClass, rbAbstractClass, rbInterface, rbApplet, rbEnum, rbUnitTest;
	
	
	/**
	 * JCheckBox pro označení, zda se má do dané třídy vygenerovat spouštěcí - hlavní metoda main:
	 */
	private final JCheckBox chcbMainMethod;
	
	
	/**
	 * pro název ohraníčení panelu s typy tříd:
	 */
	private static String txtKindOfClassTitleBorder;
	
	// Texty pro chybovou hlášku pro balíček v chybné syntaxi:
	private static String txtPackageIsInWrongSyntaxText_1, txtPackageIsInWrongSyntaxText_2,
			txtPackageIsInWrongSyntaxText_3, txtPackageIsInWrongSyntaxTitle;
	
	
	
	private final JPanel pnlKindOfClass;
	
	
	
	/**
	 * Objekt, který slouží pro testování, když se stiskne klávesy Esc nebo Enter,
	 * tak se buď zavře nebo zkusí potvrdit data zadaná v tomto dialogu pro
	 * vytvoření nové třídy.
	 */
	private KeyAdapter keyAdapter;
		
	
		
	
	/**
	 * Konstruktor této třídy.
	 */
	public NewClassForm() {
		initGui(DISPOSE_ON_CLOSE, new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS), "/icons/app/NewClassIcon.png",
				new Dimension(340, 460), true);
		
		
		
		createKeyListener();
		
		
		
		lblPackageName = new JLabel();
		add(Box.createRigidArea(new Dimension(0, 10)));
		lblPackageName.setAlignmentX(CENTER_ALIGNMENT);
		add(lblPackageName);
		
		
		

		/*
		 * List s balíčky, které senachází v adresáři src v aktuálně otevřeném projektu.
		 */
		final List<String> packagesList = getPackagesInSrcDir();
		
		cmbPackages = new JComboBox<>(packagesList.toArray(new String[] {}));		
		cmbPackages.setSelectedItem(DEFAULT_PACKAGE_EXAMPLE);
		cmbPackages.setEditable(true);
		
		// Následující dva řádky jsou pro označení textu v cmb:
		cmbPackages.getEditor().selectAll();
		cmbPackages.requestFocus();
		
		// Nastavení posluchače pro stisknutí klávesy:
		cmbPackages.getEditor().getEditorComponent().addKeyListener(keyAdapter);
		
		// Panel, kam vložím komponentu JComboBox, aby nebyla roztažená po celé šíři
		// dialogu.
		final JPanel pnlCmbPackages = new JPanel(new FlowLayout(FlowLayout.CENTER));	
		pnlCmbPackages.add(cmbPackages);
		add(pnlCmbPackages);
		
		
		
		
		
		
		lblClassName = new JLabel();		
		add(Box.createRigidArea(new Dimension(0, 10)));
		lblClassName.setAlignmentX(CENTER_ALIGNMENT);
		add(lblClassName);
		
		
		
		txtClassName = new JTextField(5);		
		txtClassName.setAlignmentX(CENTER_ALIGNMENT);
		add(Box.createRigidArea(new Dimension(0, 10)));
		txtClassName.setAutoscrolls(true);
		txtClassName.setMaximumSize(new Dimension(240, 0));
		txtClassName.addKeyListener(keyAdapter);
		txtClassName.setText("ClassName");
		// Označení textu:
		txtClassName.selectAll();
		add(txtClassName);
		
		
		
		// Vytvoření panelu pro označení typu třídy:
		pnlKindOfClass = new JPanel();
		pnlKindOfClass.setLayout(new BoxLayout(pnlKindOfClass, BoxLayout.PAGE_AXIS));
		
		
		// Instance komponent do panelu:
		rbClass = new JRadioButton();
		rbAbstractClass = new JRadioButton();
		rbInterface = new JRadioButton();
		rbApplet = new JRadioButton();
		rbEnum = new JRadioButton();
		rbUnitTest = new JRadioButton();
		
		
		// Přidám události na kliknutí na RadioButtony s typy tříd - 
		// pokud se bude jednat o zaškrtnutý JRadioButton pro typu třídy: Rozhraní nebo Výčet,
		// pak nepůjde vygenerovat hlavní - spouštěcí metoda main - znemožním zaškrtnout daný JCheckBox:
		rbClass.addActionListener(this);
		rbAbstractClass.addActionListener(this);
		rbInterface.addActionListener(this);
		rbApplet.addActionListener(this);
		rbEnum.addActionListener(this);
		rbUnitTest.addActionListener(this);
		
		
		
		
		// Aby šel označit pouzejeden typ třídy, resp. jedno zaškrtávací tlačíko:
		final ButtonGroup bg = new ButtonGroup();
		bg.add(rbClass);
		bg.add(rbAbstractClass);
		bg.add(rbInterface);
		bg.add(rbApplet);
		bg.add(rbEnum);
		bg.add(rbUnitTest);
		
		// Na začátku označím tlačítko pro tvorbu klasické Javovské třídy:
		rbClass.setSelected(true);			
		
		
		// Přidání tlačítek do panelu:
		pnlKindOfClass.add(Box.createRigidArea(new Dimension(0, 6)));
		pnlKindOfClass.add(rbClass);
		
		pnlKindOfClass.add(Box.createRigidArea(new Dimension(0, 6)));
		pnlKindOfClass.add(rbAbstractClass);
		
		pnlKindOfClass.add(Box.createRigidArea(new Dimension(0, 6)));
		pnlKindOfClass.add(rbInterface);
		
		pnlKindOfClass.add(Box.createRigidArea(new Dimension(0, 6)));
		pnlKindOfClass.add(rbApplet);
		
		pnlKindOfClass.add(Box.createRigidArea(new Dimension(0, 6)));
		pnlKindOfClass.add(rbEnum);
		
		pnlKindOfClass.add(Box.createRigidArea(new Dimension(0, 6)));
		pnlKindOfClass.add(rbUnitTest);
		
		
		// menší mezera od názvu třídy a přídám penl pro typ třídy:
		add(Box.createRigidArea(new Dimension(0, 15)));
		// Zarovníní na center:
		pnlKindOfClass.setAlignmentX(CENTER_ALIGNMENT);
		add(pnlKindOfClass);


		
		
		
		
		// JCheckBox pro označení, zda se má vygenerovat main metoda:
		chcbMainMethod = new JCheckBox("public static void main(String[] args)");
		chcbMainMethod.setAlignmentX(CENTER_ALIGNMENT);
		add(Box.createRigidArea(new Dimension(0, 10)));		
		add(chcbMainMethod);
		
		
		
		
		
		
		
		// Tlačítka:
		final JPanel pnlButtons = new JPanel();
		pnlButtons.setLayout(new FlowLayout(FlowLayout.CENTER));
		
		btnOk = new JButton();
		btnCancel = new JButton();
		
		btnOk.addActionListener(this);
		btnCancel.addActionListener(this);
		
		pnlButtons.add(btnOk);
		pnlButtons.add(btnCancel);
		
		
		// Přidání panelu s tlačítkami
		pnlButtons.setAlignmentX(CENTER_ALIGNMENT);
		add(Box.createRigidArea(new Dimension(0, 5)));
		add(pnlButtons);
		
		// Spodní mezera mezi koncem aplikace a tlačítky:
		add(Box.createRigidArea(new Dimension(0, 10)));
		
		
		
		
		
		
		addWindowListener(getWindowsAdapter());

		// vycentrovaní okna:
		setLocationRelativeTo(null);		
		
		
		pack();
		setLocationRelativeTo(null);
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá reakci na klávesu enter do textového pole jako reakce, na
	 * to, že uživatel chce povrdit přidání nové třídy:
	 */
	private void createKeyListener() {
		keyAdapter = new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					testData();

				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					variable = false;
					dispose();
				}
			}
		};
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která získá list typu String, který obsahuje balíčky, resp. veškeré
	 * soubory, které se nachází v adresáři src v aktuálně otevřeném projektu.
	 * 
	 * @return list s balíčky načtenými z aktuálně otevřeného projektu z adresáře
	 *         src, resp. všechny jeho podadresáře.
	 */
	private static List<String> getPackagesInSrcDir() {
		/*
		 * Získám si cestu k adresáři src v aktuálně otevřeném projektu.
		 */
		final String pathToSrc = App.READ_FILE.getPathToSrc();
		/*
		 * Získám si veškeré podadresáře, resp. balíčky v adresáři src v aktuálně
		 * otevřeném projektu.
		 */
		final List<String> packagesList = FindPackages.getPackages(pathToSrc);
		/*
		 * Přidám prázdné pole do listu, kdyby uživatel nechtěl zvolit žádný balíček,
		 * pak se přidá výchozí balíček, kam se třída vloží.
		 */
		if (!packagesList.contains(DEFAULT_PACKAGE_EXAMPLE))
			packagesList.add(0, DEFAULT_PACKAGE_EXAMPLE);
		
		if (!packagesList.contains(""))
			packagesList.add(0, "");
		
		return packagesList;
	}
	
	
	
	
	
	
	
	
	
	



	/**
	 * Metoda, která otestuje data, stačílo by to být v reakci na kliknutí na
	 * tlačítko OK ale stejný kó je třeba provést i po stisknutí klávesy Enter při
	 * zadávání názvu třídy v textovém poli
	 */
	private void testData() {
		if (!txtClassName.getText().replaceAll("\\s", "").isEmpty()) {
			/*
			 * Proměnná, do které si vložím text z textového pole. Jedná se o název třídy,
			 * který musí začínat velkým písmenem a může obsahovat pouze velká a malá
			 * písmena bez diakritiky, číslice a podtržítka.
			 */
			final String classNameTemp = txtClassName.getText().replaceAll("\\s", "");
			
			/*
			 * Tato podmínka je zde sice dvakrát, jednou zde a podruhé v metodě
			 * isClassNameRight, ale jen prot to, že jsem doplnil komponentu JComboBox pro
			 * výběr balíčku pro umístění třídy, ale stejně potřebuji nejprve otestovat
			 * název třídy zde, aby se do toho textového ple txtClassName nezadaly tečky,
			 * protože by se pak braly také jako balíčky, což nechci, takže to zde stejně
			 * musí být.
			 */
			if (!classNameTemp.matches(REG_EX_FOR_CLASS_NAME)) {
				JOptionPane.showMessageDialog(this,
						wrongNameOfClassNameText + "\n" + txtError + ": '" + classNameTemp + "'",
						wrongNameOfClassNameTitle, JOptionPane.ERROR_MESSAGE);
				return;
			}			
			
			
			
			/*
			 * Proměnná, do které si uložím název balíčků / balíčků, které uživatel zadal,
			 * pokud se ale jedná o prázdný text, tj. uživatel nezadal žádný balíček, pak
			 * jej nemohu přidat do "celkového" názvu -> tj. do proměnné classNameVariable,
			 * protože by byl prázdný test, pak desetinná tečka a pak název tříd, což by
			 * neprošlo regulárním výrazem.
			 * 
			 * Proto pokud neni zadán balíček, pak se do výše uvedené proměné vloží pouze
			 * zadaný název třídy a výchozí balíček se k tomu později přidá. Pokud je zadán
			 * balíček, pak se přidá nejprve baliček, pak název třídy.
			 */
			final String packageNameText = cmbPackages.getSelectedItem().toString().replaceAll("\\s", "");
			
			
			
			if (packageNameText.isEmpty())
				classNameVariable = classNameTemp;
			
			else {
				/*
				 * Zde je ještě potřeba otestovat, zda je zadán balíček ve správné syntaxi, tj.
				 * obsahuje pouze velká a malá písmena bez diakritiky, číslice nebo podtržítka.
				 * Jednotlivé balíčky jsou odděleny desetinnými tečkami a výraz nemůže končit
				 * nebo začínat desetinnou tečkou.
				 */
				if (!packageNameText.matches(REG_EX_PACKAGE_TEXT)) {
					JOptionPane.showMessageDialog(this,
							txtPackageIsInWrongSyntaxText_1 + "\n" + txtPackageIsInWrongSyntaxText_2 + "\n"
									+ txtPackageIsInWrongSyntaxText_3 + ": '" + packageNameText + "'",
							txtPackageIsInWrongSyntaxTitle, JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				classNameVariable = packageNameText + "." + classNameTemp;
			}


			
			
			final boolean var = isClassNameRight();
			
			if (var) {
				variable = true;
				dispose();
			}			
		}
		else
			JOptionPane.showMessageDialog(this, emptyClassNameText, emptyClassNameTitle, JOptionPane.ERROR_MESSAGE);
	}
	
	
	
	
	
	/**
	 * Metoda, která zviditelní dialog, a pokud se klikne n tlačítko OK, a bude "vše
	 * v pohodě" - ohledně textu tridy a značení tak vrátí nový objekt, ve kterém
	 * bude název třídy a typ označené třídy
	 */
	public final NewClassInfo getNewClassInfo() {
		setVisible(true);
		
		
		// Výchozí hodnota bude klasická Javovská třída - pro případ, že by něco náhodou selhalo
		// ale to by němělo:
		ClassTypeEnum classType = ClassTypeEnum.CLASS;
		
		if (rbAbstractClass.isSelected())
			classType = ClassTypeEnum.ABSTRACT_CLASS;
			
		else if (rbInterface.isSelected())
			classType = ClassTypeEnum.INTERFACE;
		
		
		else if (rbApplet.isSelected())
			classType = ClassTypeEnum.APPLET;
		
		
		else if (rbEnum.isSelected())
			classType = ClassTypeEnum.ENUM;
		
		
		else if (rbUnitTest.isSelected())
			classType = ClassTypeEnum.UNIT_TEST;
					
		
		if (variable)
			return new NewClassInfo(classNameVariable, classType, chcbMainMethod.isSelected());
		
		return null;
	}
	
	

	
	
	
	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		// naplním společné proměnné:
		fillIdenticalyVariables(properties);
				
		
		if (properties != null) {
			// Text do titulku dialogu:
			setTitle(properties.getProperty("NcfDialogTitle", Constants.NCF_DIALOG_TITLE));		
			
			// Titulek ohraničení druhu tříd:
			txtKindOfClassTitleBorder = properties.getProperty("NcfBorderTitleClassType", Constants.NCF_CLASS_KIND_BORDER_TITLE);

			// Text labelu pro výběr balíčku:
			lblPackageName.setText(properties.getProperty("NcfLblPackageName", Constants.NCF_CLASS_LBL_PACKAGE_NAME + ": "));

			// Text do albelu pro nazev tridy:
			lblClassName.setText(properties.getProperty("NcfLabelClassName", Constants.NCF_LBL_CLASS) + ": ");
			
			// Texty do zaškrtávacích tlačítek:
			rbClass.setText(properties.getProperty("NcfRadioButtonClass", Constants.NCF_RB_CLASS));
			rbAbstractClass.setText(properties.getProperty("NcfRadioButtonAbstractClass", Constants.NCF_RB_ABSTRACT_CLASS));
			rbInterface.setText(properties.getProperty("NcfRadioButtonInterface", Constants.NCF_RB_INTERFACE));
			rbApplet.setText(properties.getProperty("NcfRadioButtonApplet", Constants.NCF_APPLET));
			rbEnum.setText(properties.getProperty("NcfRadioButtonEnum", Constants.NCF_RB_ENUM));
			rbUnitTest.setText(properties.getProperty("NcfRadioButtonUnitTest", Constants.NCF_RB_UNIT_TEST));
			
			// texty do tlačítek 
			btnOk.setText(properties.getProperty("NcfButtonOk", Constants.NCF_BTN_OK));
			btnCancel.setText(properties.getProperty("NcfButtonCancel", Constants.NCF_BTN_CANCEL));
			
			// Texty pro chybovou hlášku pro balíček v chybné syntaxi:
			txtPackageIsInWrongSyntaxText_1 = properties.getProperty("Ncf_TxtPackageIsInWrongSyntaxText_1", Constants.NCF_TXT_PACKAGE_IS_IN_WRONG_SYNTAX_TEXT_1);
			txtPackageIsInWrongSyntaxText_2 = properties.getProperty("Ncf_TxtPackageIsInWrongSyntaxText_2", Constants.NCF_TXT_PACKAGE_IS_IN_WRONG_SYNTAX_TEXT_2);
			txtPackageIsInWrongSyntaxText_3 = properties.getProperty("Ncf_TxtPackageIsInWrongSyntaxText_3", Constants.NCF_TXT_PACKAGE_IS_IN_WRONG_SYNTAX_TEXT_3);
			txtPackageIsInWrongSyntaxTitle = properties.getProperty("Ncf_TxtPackageIsInWrongSyntaxTitle", Constants.NCF_TXT_PACKAGE_IS_IN_WRONG_SYNTAX_TITLE);
		}
		
		
		else {
			// Text do titulku dialogu:
			setTitle(Constants.NCF_DIALOG_TITLE);		
			
			// Titulek ohraničení druhu tříd:
			txtKindOfClassTitleBorder = Constants.NCF_CLASS_KIND_BORDER_TITLE;

			// Text labelu pro výběr balíčku:
			lblPackageName.setText(Constants.NCF_CLASS_LBL_PACKAGE_NAME + ": ");

			// Text do albelu pro nazev tridy:
			lblClassName.setText(Constants.NCF_LBL_CLASS + ": ");
			
			// Texty do zaškrtávacích tlačítek:
			rbClass.setText(Constants.NCF_RB_CLASS);
			rbAbstractClass.setText(Constants.NCF_RB_ABSTRACT_CLASS);
			rbInterface.setText(Constants.NCF_RB_INTERFACE);
			rbApplet.setText(Constants.NCF_APPLET);
			rbEnum.setText(Constants.NCF_RB_ENUM);
			rbUnitTest.setText(Constants.NCF_RB_UNIT_TEST);
			
			// texty do tlačítek 
			btnOk.setText(Constants.NCF_BTN_OK);
			btnCancel.setText(Constants.NCF_BTN_CANCEL);
			
			// Texty pro chybovou hlášku pro balíček v chybné syntaxi:
			txtPackageIsInWrongSyntaxText_1 = Constants.NCF_TXT_PACKAGE_IS_IN_WRONG_SYNTAX_TEXT_1;
			txtPackageIsInWrongSyntaxText_2 = Constants.NCF_TXT_PACKAGE_IS_IN_WRONG_SYNTAX_TEXT_2;
			txtPackageIsInWrongSyntaxText_3 = Constants.NCF_TXT_PACKAGE_IS_IN_WRONG_SYNTAX_TEXT_3;
			txtPackageIsInWrongSyntaxTitle = Constants.NCF_TXT_PACKAGE_IS_IN_WRONG_SYNTAX_TITLE;
		}	
		
		pnlKindOfClass.setBorder(BorderFactory.createTitledBorder(txtKindOfClassTitleBorder));
		
		
		
		// Postup:
		// Načtu si výchozí nastavení z adresáře ve vorkspacu (soubor Default.properties) a pokud nebude nalazen,
		// tak si načtu ten samý soubor z adresáře aplikace, a pokud se ani ten nenajde, tak jako výchozí cesta se 
		// použije čeština, tak s tím "budu" počítat a přidím texty k možným třídám:
		final Properties defaultPropeties = ReadFile.getDefaultPropertiesInWorkspace();
			
		// proměnná pro načtení iniciálu jazyka:
		String appLanguage = "";
		
		if (defaultPropeties != null)
			appLanguage = defaultPropeties.getProperty("Language", Constants.DEFAULT_LANGUAGE_NAME);
		
		else {
			// Zde nebyl nalezen adresář ve workspacu, tak ho zkusím načíst z adresáře aplikace:
			final Properties defaultPropertiesInApp = ReadFile.getDefaultPropertiesInApp();
			
			// Pokud byl nalezen, tak pouiji získaný jazyk případně výchozí cestu coby čeký jazyk:
			if (defaultPropertiesInApp != null)
				appLanguage = defaultPropertiesInApp.getProperty("Language", Constants.DEFAULT_LANGUAGE_NAME);
		}
		
		// Otestuji, zda není anglický jazyk načtený v aplikaci, a pokud není,
		// tak mohu za název typu tříd zobrazit i jejich anglické názvy (pro lepší přehled)
		if (!appLanguage.equalsIgnoreCase("EN")) {
			// Zde není načten anglický jazyk v aplikaci, tak zobrazím za typ třídy i jejich anglický název:	
			
			rbClass.setText(rbClass.getText() + " (Class)");
			rbAbstractClass.setText(rbAbstractClass.getText() + " (Abstract Class)");
			rbInterface.setText(rbInterface.getText() + " (Interface)");
			rbApplet.setText(rbApplet.getText() + " (Applet)");
			rbEnum.setText(rbEnum.getText() + " (Enum)");
			rbUnitTest.setText(rbUnitTest.getText() + " (Unit Test)");
		}
	}


	
	
	


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnOk)
				testData();
			
			else if (e.getSource() == btnCancel) {
				variable = false;
				dispose();
			}
		}	
		
		
		else if (e.getSource() instanceof JRadioButton) {
			if (e.getSource() == rbClass) {
				if (rbClass.isSelected() && !chcbMainMethod.isEnabled())
					chcbMainMethod.setEnabled(true);
			}
			
			else if (e.getSource() == rbAbstractClass) {
				if (rbAbstractClass.isSelected() && !chcbMainMethod.isEnabled())
					chcbMainMethod.setEnabled(true);
			}
			
			else if (e.getSource() == rbInterface) {
				if (rbInterface.isSelected() && chcbMainMethod.isEnabled())
					chcbMainMethod.setEnabled(false);
			}
			
			else if (e.getSource() == rbApplet) {
				if (rbApplet.isSelected() && !chcbMainMethod.isEnabled())
					chcbMainMethod.setEnabled(true);
			}
			
			else if (e.getSource() == rbEnum) {
				if (rbEnum.isSelected() && chcbMainMethod.isEnabled())
					chcbMainMethod.setEnabled(false);
			}
			
			else if (e.getSource() == rbUnitTest) {
				if (rbUnitTest.isSelected() && !chcbMainMethod.isEnabled())
					chcbMainMethod.setEnabled(true);
			}
		}
	}
}