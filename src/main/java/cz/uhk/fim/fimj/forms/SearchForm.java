package cz.uhk.fim.fimj.forms;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;

/**
 * Tato třída slouží jako dialog pro vyhledávání metod, konstruktorů nebo proměnných Dialog se zobrazí po kliknutí na
 * tlačítko vyhledat v dialogu s přehledem hodnot instance třídy (dialog: VariablesInstanceOfClassForm)
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class SearchForm extends SourceDialogForm implements LanguageInterface {
	
	private static final long serialVersionUID = 1L;


	/**
	 * Informační popisek s informace, co se má zadat do vyhledávacího pole
	 */
	private final JLabel lblInfo;
	
	
	/**
	 * Textové ple pro vyhledání textového řetězce -klíčového slova
	 */
	private final JTextField txtSearchText;
	

	
	/**
	 * Komponenta - Jlist, do které vložím nalezeny text dle uživatelem zadaného
	 * řetězce
	 */
	private final JList<Object> searchedTextList;
	
	
	/*
	 * Komponenta pro - "ohraničení" - listu se nalezenou shodou - je z ní
	 * "globální" neboli instanční proměnná, abych jí mohl nastavit text ohraničení
	 * ve uživatelem zvoleném jazyce
	 */
	private final JScrollPane jspSearchedText;
	

	
	/**
	 * Tlačítka pro vyhledání klíčového slova - řetězce a uzavření dialogu
	 */
	private final JButton btnClose;
	private final JButton btnSearch;
	
	/**
	 * Kolekce, do které vložím v parametru kontrukrou předaném hodnoty - všechny
	 * konstruktory, metody, nebo atributy dané třídy ve které chce uživatel něco
	 * vyhledat
	 */
	private final transient Set<?> textForSearching;
	
	/**
	 * Proměnná, do které vložím text, a ten pak nastavím v titulku dialogu jedná se
	 * o to, co se v diaogu bude vyhledávat - konstukror, metoda, nebo proměnná
	 */
	private final String objectForSearch;
	
	
	
	public SearchForm(final Set<?> text, final String objectForSearch) {
		textForSearching = text;
		
		this.objectForSearch = objectForSearch;
		
		initGui(DISPOSE_ON_CLOSE, new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS), "/icons/app/SearchFormIcon.jpg",
				new Dimension(600, 400), true);
		
		
		
		lblInfo = new JLabel();
		addComponent(lblInfo);
		
		
		
		
		txtSearchText = new JTextField(20);
		txtSearchText.addKeyListener(new KeyAdapter() {			
			
			@Override
			public void keyReleased(KeyEvent e) {
				searchText();
			}
		});
		
		btnSearch = new JButton();
		btnSearch.addActionListener(event -> searchText());
		
		final JPanel pnlSearchComponents = new JPanel(new FlowLayout(FlowLayout.CENTER, 20, 0));		
		
		pnlSearchComponents.add(txtSearchText);
		pnlSearchComponents.add(btnSearch);
		
		addComponent(pnlSearchComponents);
		
		
		
		
		
		// komponenta pro zobrazení nalezeného textu:
		searchedTextList = new JList<>(text.toArray());
		searchedTextList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		jspSearchedText = new JScrollPane(searchedTextList);
		jspSearchedText.setMaximumSize(new Dimension(jspSearchedText.getMinimumSize().width + 500,
				jspSearchedText.getMinimumSize().height + 300));
		addComponent(jspSearchedText);
		
		
		
		
		
		btnClose = new JButton();
		btnClose.addActionListener(event -> dispose());
		addComponent(btnClose);
		
		
		
		add(Box.createRigidArea(new Dimension(0, 20)));
		
		
		
		pack();
		setLocationRelativeTo(null);
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která v získaném textu vyhledá nalezený řětězec znaků a zobrazí je v
	 * Jlistu pod vyhledávacím polem
	 */
	private void searchText() {
		final String keyWord = txtSearchText.getText();
		
		
		// Vytvořím novou koelci, do které vložím nalezený text:
		List<String> foundTextList = new ArrayList<>();
		
		
		// Pomocí Pattern vyhledávám s tím, že nerozlišuji velká a malá písmena:
		for (final Object s : textForSearching)
			if (Pattern.compile(Pattern.quote(keyWord), Pattern.CASE_INSENSITIVE).matcher(s.toString()).find())
				foundTextList.add(s.toString());

		
		// Nastavím nalezený text do listu pro zobrení v dialogu:
		searchedTextList.setListData(foundTextList.toArray());
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá komponentu v parametru do okna dialogu
	 * 
	 * @param component
	 *            - komponenta pro přidání do dialogu
	 */
	private void addComponent(final JComponent component) {
		add(Box.createRigidArea(new Dimension(0, 20)));
		component.setAlignmentX(CENTER_ALIGNMENT);
		add(component);
	}
	
	
	

	
	

	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			final String dialogTitle = properties.getProperty("SForm_DialogTitle", Constants.SFORM_DILOG_TITLE);
			setTitle(dialogTitle + " " + objectForSearch + ".");
			
			final String textForSearchObj = properties.getProperty("SForm_LabelInfo", Constants.SFORM_LBL_INFO);
			lblInfo.setText(textForSearchObj + " " + objectForSearch);
			
			btnClose.setText(properties.getProperty("SForm_ButtonClose", Constants.SFORM_BTN_CLOSE));
			btnSearch.setText(properties.getProperty("SForm_ButtonSearch", Constants.SFORM_BTN_SEARCH));
			
			jspSearchedText.setBorder(BorderFactory.createTitledBorder(properties.getProperty("SForm_JspSearchedTextBorderTitle", Constants.SFORM_JSP_SEARCHED_TEXT)));
		}
		
		else {
			final String dialogTitle = Constants.SFORM_DILOG_TITLE;
			setTitle(dialogTitle + " " + objectForSearch);
			
			final String textForSearchObj = Constants.SFORM_LBL_INFO;
			lblInfo.setText(textForSearchObj + " " + objectForSearch);
			
			btnClose.setText(Constants.SFORM_BTN_CLOSE);
			btnSearch.setText(Constants.SFORM_BTN_SEARCH);
			
			jspSearchedText.setBorder(BorderFactory.createTitledBorder(Constants.SFORM_JSP_SEARCHED_TEXT));
		}
	}
}