package cz.uhk.fim.fimj.forms.about_app_form;

import cz.uhk.fim.fimj.code_editor.CodeEditorDialog;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.DesktopSupport;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.project_explorer.ProjectExplorerDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.*;

/**
 * Kontextové menu, které se zobrazí po kliknutí pravým tlačítkem myši na cestu k jednomu z adresářů nebo souborů
 * zobrazených v okně dialogu s přehledem o autorovi aplikaci.
 * <p>
 * Toto menu obsahuje možnosti pro otevření příslušného adresáře nebo souboru například v průzkumníku projektů nebo
 * průzkumníku souborů (dle využívané platoformy) apod.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 19.08.2018 13:45
 */

public class PopupMenu extends JPopupMenu implements ActionListener, LanguageInterface {

    private static final long serialVersionUID = -8925675138967138368L;


    // Položky pro otevření souboru nebo adresáře ve výchozí aplikaci nebo v jedné z komponent této aplikace:
    private final JMenuItem itemOpenInFileExplorer;
    private final JMenuItem itemOpenInProjectExplorer;
    private final JMenuItem itemOpenInCodeEditor;
    private final JMenuItem itemOpenInDefaultApp;


    /**
     * Cesta k souboru nebo adresáři, který bude možné otevřít v jedné z výše uvedených komponent. Například průzkumník
     * souborů (dle využívané platofmy) nebo průzkumník projektů, editor kódu apod.
     */
    private final File file;


    /**
     * Objekt pro uložení reference na načtený soubor s texty pro tuto aplikaci v uživatelem zvoleném jazyce pro předání
     * do editoru kódu.
     */
    private Properties languageProperties;


    /**
     * Konstruktor třídy.
     *
     * @param file
     *         cesta k souboru nebo adresáři, který se má otevřít po zvolení komponenty. Nesmí být null.
     */
    public PopupMenu(final File file) {
        this.file = file;

        itemOpenInFileExplorer = new JMenuItem();
        itemOpenInProjectExplorer = new JMenuItem();
        itemOpenInCodeEditor = new JMenuItem();
        itemOpenInDefaultApp = new JMenuItem();

        itemOpenInFileExplorer.addActionListener(this);
        itemOpenInProjectExplorer.addActionListener(this);
        itemOpenInCodeEditor.addActionListener(this);
        itemOpenInDefaultApp.addActionListener(this);

        add(itemOpenInFileExplorer);

        if (ReadFile.isFileInWorkspace(file.getAbsolutePath())) {
            addSeparator();
            add(itemOpenInProjectExplorer);
        }

        /*
         * V případě, že se jedná o soubor, budou k dispozici položky pro otevření souboru v editoru kódu a výchozí
         * aplikaci. Ale v případě, že se nejedná o soubor, tedy jedná se o adresář, pak tyto dvě možnosti nebudou k
         * dispozici.
         */
        if (file.isFile()) {
            addSeparator();
            add(itemOpenInCodeEditor);
            addSeparator();
            add(itemOpenInDefaultApp);
        }
    }


    @Override
    public void setLanguage(final Properties properties) {
        languageProperties = properties;

        if (properties != null) {
            itemOpenInFileExplorer.setText(properties.getProperty("Aaf_Pm_Item_OpenInFileExplorer_Text",
                    Constants.AAF_PM_ITEM_OPEN_IN_FILE_EXPLORER_TEXT));
            itemOpenInProjectExplorer.setText(properties.getProperty("Aaf_Pm_Item_OpenInProjectExplorer_Text",
                    Constants.AAF_PM_ITEM_OPEN_IN_PROJECT_EXPLORER_TEXT));
            itemOpenInCodeEditor.setText(properties.getProperty("Aaf_Pm_Item_OpenInCodeEditor_Text",
                    Constants.AAF_PM_ITEM_OPEN_IN_CODE_EDITOR_TEXT));
            itemOpenInDefaultApp.setText(properties.getProperty("Aaf_Pm_Item_OpenInDefaultApp_Text",
                    Constants.AAF_PM_ITEM_OPEN_IN_DEFAULT_APP));

            itemOpenInFileExplorer.setToolTipText(properties.getProperty("Aaf_Pm_Item_OpenInFileExplorer_TT",
                    Constants.AAF_PM_ITEM_OPEN_IN_FILE_EXPLORER_TT));
            itemOpenInProjectExplorer.setToolTipText(properties.getProperty("Aaf_Pm_Item_OpenInProjectExplorer_TT",
                    Constants.AAF_PM_ITEM_OPEN_IN_PROJECT_EXPLORER_TT));
            itemOpenInCodeEditor.setToolTipText(properties.getProperty("Aaf_Pm_Item_OpenInCodeEditor_TT",
                    Constants.AAF_PM_ITEM_OPEN_IN_CODE_EDITOR_TT));
            itemOpenInDefaultApp.setToolTipText(properties.getProperty("Aaf_Pm_Item_OpenInDefaultApp_TT",
                    Constants.AAF_PM_ITEM_OPEN_IN_DEFAULT_APP_TT));
        }

        else {
            itemOpenInFileExplorer.setText(Constants.AAF_PM_ITEM_OPEN_IN_FILE_EXPLORER_TEXT);
            itemOpenInProjectExplorer.setText(Constants.AAF_PM_ITEM_OPEN_IN_PROJECT_EXPLORER_TEXT);
            itemOpenInCodeEditor.setText(Constants.AAF_PM_ITEM_OPEN_IN_CODE_EDITOR_TEXT);
            itemOpenInDefaultApp.setText(Constants.AAF_PM_ITEM_OPEN_IN_DEFAULT_APP);

            itemOpenInFileExplorer.setToolTipText(Constants.AAF_PM_ITEM_OPEN_IN_FILE_EXPLORER_TT);
            itemOpenInProjectExplorer.setToolTipText(Constants.AAF_PM_ITEM_OPEN_IN_PROJECT_EXPLORER_TT);
            itemOpenInCodeEditor.setToolTipText(Constants.AAF_PM_ITEM_OPEN_IN_CODE_EDITOR_TT);
            itemOpenInDefaultApp.setToolTipText(Constants.AAF_PM_ITEM_OPEN_IN_DEFAULT_APP_TT);
        }
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof JMenuItem) {
            if (e.getSource() == itemOpenInFileExplorer)
                DesktopSupport.openFileExplorer(file, true);

            else if (e.getSource() == itemOpenInProjectExplorer) {
                /*
                 * Do průzkumníku projektů si potřebuji načíst i adresář, který je označen jako
                 * workspace.
                 *
                 * (Cestu k workspace by bylo možné předat v konstruktoru.)
                 */
                final String pathToWorkspace = ReadFile.getPathToWorkspace();

                if (pathToWorkspace != null) {
                    if (file.isDirectory()) {
                        /*
                         * Zde se jedná o adresář, tak se v průzkumníku projektů pouze rozbalí příslušný adresáře ve
                         * stromové struktuře, ale žádný nový soubor se otevřít nebude, pouze ty, které se v něm
                         * nacházely otevřené při zavření dialogu.
                         */
                        final List<File> fileList = new ArrayList<>(Collections.singletonList(file));
                        new ProjectExplorerDialog(new File(pathToWorkspace), languageProperties, null, fileList, false);
                    }

                    // Zde se jedná o tevření dialogu průzkumníku projektů a v něm označený soubor ne adresář:
                    else new ProjectExplorerDialog(new File(pathToWorkspace), languageProperties, file, null, false);
                }
            }

            else if (e.getSource() == itemOpenInCodeEditor) {
                final CodeEditorDialog ced = new CodeEditorDialog(file.getAbsolutePath(), languageProperties);
                ced.setLanguage(languageProperties);
                ced.setVisible(true);
            }

            else if (e.getSource() == itemOpenInDefaultApp)
                DesktopSupport.openFileExplorer(file, false);
        }
    }
}
