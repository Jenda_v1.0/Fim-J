package cz.uhk.fim.fimj.forms;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.IntStream;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.instance_diagram.InstanceAndCheckBox;
import cz.uhk.fim.fimj.instance_diagram.InstanceCheckBoxListCellRenderer;

/**
 * Tato třída slouží jako dialog, pro označení instancí třídy z diagramu tříd, které ještě nejsou reprezentovány v
 * diagramu instancí na které se má vytvořit nějaká reference.
 * <p>
 * Tento dialog se zobrazí v případě, že si uživatel při prohlížení hodnot instance nějaké třídy z diagramu tříd v
 * dialogu pro prohlížení hodnot (konstruktor, metod a proměnných) zvolí proměnnou typu list, pole nebo "klasická"
 * proměnná, která obsahuje instanci třídy z diagramu tříd, která ještě není v diagramu instancí a uživatel klikne na
 * tlačítko vytvořit referenci, tak pokud těch instancí bude na výběr více, tj. v listu nebo poli, tak se zobrazí tento
 * dialog, kde si uživatel zvolí pouze ty instance, na které je možné vytviřt reference.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class SelectInstancesForCreateReference extends SourceDialogForm implements LanguageInterface, ActionListener {

	private static final long serialVersionUID = 1L;


	/**
	 * Label, který louží pro zobrazení informace o tom, že je v tomto dialogu
	 * potřeba označit instance, na které chce uživatel vytvoři reference.
	 */
	private final JLabel lblInfo;
	
	
	/**
	 * List, který bude obsahovat příslušné položky / instance a může si označit ty,
	 * na které se má vytvořit reference.
	 */
	private final JList<InstanceAndCheckBox> list;
	
	
	
	
	// Tlačítka pro označení a odznačení všech položek v listu:
	private final JButton btnSelectAll;
	private final JButton btnDeselectAll;
	
	
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param instancesList
	 *            - list, který obsahuje veškeré instance tříd z diagramu tříd, na
	 *            které je možné vytvořit reference a znázornit je tak v diagramu
	 *            instancí.
	 */
	public SelectInstancesForCreateReference(final List<Object> instancesList) {
		super();
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/app/CreateReferenceDialogIcon.png", new Dimension(450, 400),
				true);
		
		
		gbc = createGbc();
		
		index = 0;
		
		
		lblInfo = new JLabel();
		setGbc(index, 0, lblInfo);
		
		
		
		
		
		
		/*
		 * Model pro vložení položek:
		 */
		final DefaultListModel<InstanceAndCheckBox> model = new DefaultListModel<>();
		
		// inicializace listu:
		list = new JList<>(model);
		
		list.setCellRenderer(new InstanceCheckBoxListCellRenderer());
		
		// Vložení instancí / položek do modelu listu:
		instancesList.forEach(i -> model.addElement(new InstanceAndCheckBox(i)));
		
		
		/*
		 * Posluchač na kliknutí myši, aby se mohla označit komponenta chcb - aby se
		 * opravdu označila.
		 */
		list.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				@SuppressWarnings("unchecked")
				final JList<InstanceAndCheckBox> lchcbsList = (JList<InstanceAndCheckBox>) e.getSource();
				
				/*
				 * Index položky, na kterou klikl uživatel.
				 */
				final int index = lchcbsList.locationToIndex(e.getPoint());
				
				/*
				 * Získám si konkrétní položku, na kterou uživatel klikl, abych ji mohl změnit,
				 * resp. znegovat označení.
				 */
				final InstanceAndCheckBox item = lchcbsList.getModel().getElementAt(index);
				
				// Zneguji označení položky:
				item.setSelected(!item.isChcbSelected());
				
				// Překreslim tu položku, aby provedené změny viděl i uživatel:
				lchcbsList.repaint(lchcbsList.getCellBounds(index, index));
			}
		});
		
		final JScrollPane jsplist = new JScrollPane(list);
		
		setGbc(++index, 0, jsplist);
		
		
		
		
		
		
		
		/*
		 * Panel pro tlačítka, která slouží pro označení nebo odznačení všech položek v
		 * listu s instancemi pro vytvoření referencí.
		 */
		final JPanel pnlButtonsForSelect = new JPanel(new FlowLayout(FlowLayout.CENTER));
		
		btnSelectAll = new JButton();
		btnDeselectAll = new JButton();
		
		btnSelectAll.addActionListener(this);
		btnDeselectAll.addActionListener(this);
		
		pnlButtonsForSelect.add(btnSelectAll);
		pnlButtonsForSelect.add(btnDeselectAll);
		
		setGbc(++index, 0, pnlButtonsForSelect);
		
		
		
		
		
		
		
		
		
		
		// Panel s tlačítky:
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		btnOk = new JButton();
		btnCancel = new JButton();
		
		btnOk.addActionListener(this);
		btnCancel.addActionListener(this);
		
		pnlButtons.add(btnOk);
		pnlButtons.add(btnCancel);
		
		setGbc(++index, 0, pnlButtons);
		
		
		
		
		
		
		
		// Aby se při zavření dialogu poznalo, že se mají nebo nemají vrátit hodnoty,
		// tj. aby se poznalo, zda uživate zavřel nebo potvrdil dialog.
		addWindowListener(getWindowsAdapter());
		
		
		
		pack();
		setLocationRelativeTo(null);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro zobrazení tohoto dialogu a až uživatel dialog zavře
	 * nebo potvrdí kliknutím na tlačítko OK, tak se vrátí list s označenými
	 * položkami / instancemi, na které se má vytvořit reference.
	 * 
	 * @return list s instancemi, na které se má vytvořit reference, nenbo prázdný
	 *         list pokud uživatel žádnou instanci neboznačí nebo zavře dialog.
	 */
	public final List<Object> getInstances() {
		setVisible(true);

		if (variable) {
			/*
			 * List, do kterého vložím označené instance, na které se má vytvořit reference.
			 */
			final List<Object> objInstancesList = new ArrayList<>();

			/*
			 * Velikost listu s položkami.
			 */
			final int length = list.getModel().getSize();

			// Projdu celý list a zjistím si označené položky:
			for (int i = 0; i < length; i++) {
				/*
				 * Získám si hodnot v listu:
				 */
				final InstanceAndCheckBox value = list.getModel().getElementAt(i);

				// pokud je položka označena přidám si příslušnou instanci do listu pro
				// vytvoření reference.
				if (value.isChcbSelected())
					objInstancesList.add(value.getObjInstance());
			}

			// Vrátím list s označenými instancemi pro vytvoření reference.
			return objInstancesList;
		}

		return new ArrayList<>();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public void setLanguage(Properties properties) {
		if (properties != null) {
			setTitle(properties.getProperty("SIFCR_DialogTitle", Constants.SIFCR_DIALOG_TITLE));
			
			lblInfo.setText(properties.getProperty("SIFCR_Lbl_Info", Constants.SIFCR_LBL_INFO));
			
			btnOk.setText(properties.getProperty("SIFCR_Btn_OK", Constants.SIFCR_BTN_OK));
			btnCancel.setText(properties.getProperty("SIFCR_Btn_Cancel", Constants.SIFCR_BTN_CANCEL));
			
			btnSelectAll.setText(properties.getProperty("SIFCR_Btn_SelectAll", Constants.SIFCR_BTN_SELECT_ALL));
			btnDeselectAll.setText(properties.getProperty("SIFCR_Btn_DeselectAll", Constants.SIFCR_BTN_DESELECT_ALL));
		}

		else {
			setTitle(Constants.SIFCR_DIALOG_TITLE);

			lblInfo.setText(Constants.SIFCR_LBL_INFO);

			btnOk.setText(Constants.SIFCR_BTN_OK);
			btnCancel.setText(Constants.SIFCR_BTN_CANCEL);

			btnSelectAll.setText(Constants.SIFCR_BTN_SELECT_ALL);
			btnDeselectAll.setText(Constants.SIFCR_BTN_DESELECT_ALL);
		}
	}


	
	
	
	

	/**
	 * Metoda, která slouží pro označení nebo odznačení všech komponent typu
	 * JCheckBox, které značí položky / instance v tomto dialogu, resp. v listu
	 * list.
	 * 
	 * @param selected
	 *            - logická hodnota, která značí, zda se mají všechny položky v
	 *            listu list označit nebo ne. True značí označení všech položek a
	 *            false značí odznašení všech položek.
	 */
	private void setSelectedItem(final boolean selected) {
		final int length = list.getModel().getSize();

		IntStream.range(0, length)
				.forEach(i -> list.getModel().getElementAt(i).setSelected(selected));

		// Aby se změny projevily, je třeba překreslit list:
		list.repaint();
	}
	
	
	
	
	
	
	



	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {// Zbytečná podmínka, pouze pro přehlednost.
			if (e.getSource() == btnOk) {
				variable = true;
				dispose();
			}

			else if (e.getSource() == btnCancel)
				dispose();

			else if (e.getSource() == btnSelectAll)
				setSelectedItem(true);

			else if (e.getSource() == btnDeselectAll)
				setSelectedItem(false);
		}
	}
}
