package cz.uhk.fim.fimj.forms;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.List;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import cz.uhk.fim.fimj.check_configuration_files.ErrorInfo;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;

/**
 * Tato třída slouží jako dialog pro zobrazení chyb, které uživatel zadal do nějakého konfiguračního souboru.
 * <p>
 * Tento dialog není nějak extra potřeba, ale v případě, že uživatel změní nějakou hodnotu v jednom z konfiguračních
 * souborů, pak v případě, že ta chybu nebude validní, tj. nebude zadána jedna z validních hodnot, pak se na příslušné
 * umístění vytvoří nový soubor s výchozím nastavením, tak aby o tom jen uživatel věděl, že by bylo vhodně restartovat
 * aplikaci a že ty jeho změny nebudou uloženy, resp. povoleny apod. Prostě se neprojeví.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ErrorInConfigFilesForm extends SourceDialogForm implements LanguageInterface {

	private static final long serialVersionUID = 1L;

	
	/**
	 * Label, který slouží pro zobrazení informací ohledně chyby / chyb nalezených v
	 * příslušném souboru.
	 */
	private final JLabel lblInfo;
	
	
	
	/**
	 * Komponenta, která slouží k tomu, zda se mají zalamovat texty v příslušných
	 * oknech nebo ne.
	 */
	private final JCheckBox chcbWrapTexts;
	
	
	
	
	/**
	 * List, který slouží pro zobrazení chyb, které byly nalezeny v nějakém
	 * konfiguračním souboru.
	 */
	private final JList<ErrorInfo> errorListReview;

	/**
	 * List, který slouží pro zobrazení duplicitních hodnot v příslušném
	 * konfiguračním souboru.
	 */
	private final JList<ErrorInfo> duplicatesListReview;

	/**
	 * Komponenta, která slouží pro skrolování listem s chybami nalezenými v
	 * příslušném souboru.
	 */
	private final JScrollPane jspErrorList;

	/**
	 * Komponenta, která slouží pro skrolování listem s duplicitami nalezenými v
	 * příslušném souboru.
	 */
	private final JScrollPane jspDuplicatesList;
	
	
	/**
	 * Proměnná, která slouží pro uložení názvu souboru, ve kterém byla nalezena
	 * nějaká chyba. Tento název, resp. tato proměnné je zde jako instanční neboli
	 * globální proměnná, jen aby se mohl ten název souboru vložit do proměnné při
	 * naplnění / nastavení textů.
	 */
	private final String fileName;
	
	
	
	/**
	 * Proměnná, která slouží pro "ukládání" textu, který obsahuje informace pro
	 * uživatele - na začátku dialogu.
	 */
	private String txtInfo;
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param fileName
	 *            - název konfiguračního souboru, ve kterém byly nalezeny příslušné
	 *            chyby nebo duplicitní hodnoty.
	 * 
	 * @param errorList
	 *            - list, který obsahuje chyby, které byly nalezeny v nějakém
	 *            konfiguračním souboru.
	 * 
	 * @param duplicatesList
	 *            - list, který obsahuje duplicity, které byly nalezeny v
	 *            konfiguračním souboru pro diagram tříd nebo diagram instancí.
	 *            Jedná se o přehled duplicitních nastavení pro nějaké hrany nebo
	 *            buňky pro třídy a komentáře apod.
	 */
	public ErrorInConfigFilesForm(final String fileName, final List<ErrorInfo> errorList,
			final List<ErrorInfo> duplicatesList) {

		super();

		this.fileName = fileName;

		initGui(DISPOSE_ON_CLOSE, new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS), "/icons/app/ErrorListFormIcon.png",
				new Dimension(500, 500), true);

		// Minimální velikost okna dialogu:
		setMinimumSize(new Dimension(320, 330));
		
		
		
		
		lblInfo = new JLabel();
		addComponent(lblInfo);
		
		
		
		
		chcbWrapTexts = new JCheckBox();
		chcbWrapTexts.addActionListener(event -> wrapTexts(chcbWrapTexts.isSelected()));
		addComponent(chcbWrapTexts);
		
		
		
		
		
		
		
		

		
		// list s duplicitami:
		duplicatesListReview = new JList<>(duplicatesList.toArray(new ErrorInfo[] {}));
		duplicatesListReview.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		jspDuplicatesList = new JScrollPane(duplicatesListReview);

		/*
		 * V případě, že byly nalezeny i duplicitní hodnoty, pak mohu přidat i list s
		 * duplicitami, jinak jej mohu vynechat.
		 */
		if (!duplicatesList.isEmpty())
			addComponent(jspDuplicatesList);
		
		
		
		
		
		// list s chybami:
		errorListReview = new JList<>(errorList.toArray(new ErrorInfo[] {}));
		errorListReview.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		jspErrorList = new JScrollPane(errorListReview);

		if (!errorList.isEmpty())
			addComponent(jspErrorList);
		
		

		
		
		
		
		
		
		/*
		 * Panel s tlačítkem OK pro potvrzení, resp. zavření dialogu:
		 */
		final JPanel pnlButtonOk = new JPanel(new FlowLayout(FlowLayout.CENTER));

		btnOk = new JButton();
		btnOk.addActionListener(event -> dispose());

		pnlButtonOk.add(btnOk);

		addComponent(pnlButtonOk);
		
		
		
		
		add(Box.createRigidArea(new Dimension(0, 10)));
		
		
		
		
		
		
		
		
		
		/*
		 * Posluchač, který reaguje na změnu velikosti okna dialogu, v takovém případě
		 * se aktualizuje zalamování textů jak v listech s chybami nebo duplicitami, tak
		 * i label s informacemi.
		 */
		addComponentListener(new ComponentAdapter() {

			@Override
			public void componentResized(ComponentEvent e) {
				wrapTexts(chcbWrapTexts.isSelected());
				setTextToLblInfo(txtInfo);
			}
		});
		
		
		
		
		
		pack();
		setLocationRelativeTo(null);
	}

	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro přidání komponenty do okna dialogu.
	 * 
	 * Jedná se o přidání komponenty component do okna dialogu s tím, že se od shora
	 * přidá mezera (nahoře nad tou komponentou).
	 * 
	 * @param component
	 *            - komponenta, která se přidá do okna dialogu.
	 */
	private void addComponent(final JComponent component) {
		add(Box.createRigidArea(new Dimension(0, 20)));
		component.setAlignmentX(CENTER_ALIGNMENT);
		add(component);
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro nastavení textu (text) do labelu s informacemi
	 * (první label s texty v okně dialogu)
	 * 
	 * @param text
	 *            - text, který se má nastavit do labelu s informacemi.
	 */
	private void setTextToLblInfo(final String text) {
		lblInfo.setText(getWrappedText(text, getWidth()));
	}
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří "zalamovací" text (String), nastavím mu text v
	 * parametru a velikost u jaké se má zalamovat
	 * 
	 * @param text
	 *            - text, jaký má obsahovat
	 * 
	 * @param width
	 *            - šířka u jaké s má zalamovat
	 * 
	 * @return text s html syntaxí pro zalamování.
	 */
	private static String getWrappedText(final String text, final int width) {
		return String.format("<html><div WIDTH=%d>%s</div><html>", width - 15, text);
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží k tomu, aby příslušným listům nastavila zalamování nebo
	 * ne.
	 * 
	 * Pokud je proměnná wrap true, pak se příslušným listům s texty nastaví
	 * příslušný renderer, který umožňuje zalamování textů, jinak pokud bude
	 * proměnná wrap false, pak se příslušným listům nastaví výchozí renderer,který
	 * to zalamování neumožňuje.
	 * 
	 * @param wrap
	 *            true, pokud se mají texty v listech s chybami zalamovat, jinak
	 *            false.
	 */
	private void wrapTexts(final boolean wrap) {
		if (wrap) {
			final int temp = ((getWidth() / 4) * 3) - 20;
			duplicatesListReview.setCellRenderer(new ListCellRenderer(temp));
			errorListReview.setCellRenderer(new ListCellRenderer(temp));
		}

		else {
			errorListReview.setCellRenderer(new DefaultListCellRenderer());
			duplicatesListReview.setCellRenderer(new DefaultListCellRenderer());
		}
	}
	
	
	
	
	
	
	
	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		final String txtFile, txtInfoRest;
		
		if (properties != null) {
			setTitle(properties.getProperty("EICFF_DialogTitle", Constants.EICFF_DIALOG_TITLE));

			txtFile = properties.getProperty("EICFF_TxtFile", Constants.EICFF_TXT_FILE);
			txtInfoRest = properties.getProperty("EICFF_TxtInfoRest", Constants.EICFF_TXT_INFO_REST);

			jspErrorList.setBorder(BorderFactory.createTitledBorder(properties
					.getProperty("EICFF_JspErrorListBorderTitle", Constants.EICFF_JSP_ERROR_LIST_BORDER_TITLE)));
			jspDuplicatesList.setBorder(BorderFactory.createTitledBorder(properties.getProperty(
					"EICFF_JspDuplicatesListBorderTitle", Constants.EICFF_JSP_DUPLICATES_LIST_BORDER_TITLE)));

			chcbWrapTexts.setText(properties.getProperty("EICFF_ChcbWrapText", Constants.EICFF_CHCB_WRAP_TEXT));

			btnOk.setText(properties.getProperty("EICFF_BtnOk", Constants.EICFF_BTN_OK));
		}

		else {
			setTitle(Constants.EICFF_DIALOG_TITLE);

			txtFile = Constants.EICFF_TXT_FILE;
			txtInfoRest = Constants.EICFF_TXT_INFO_REST;

			jspErrorList.setBorder(BorderFactory.createTitledBorder(Constants.EICFF_JSP_ERROR_LIST_BORDER_TITLE));
			jspDuplicatesList
					.setBorder(BorderFactory.createTitledBorder(Constants.EICFF_JSP_DUPLICATES_LIST_BORDER_TITLE));

			chcbWrapTexts.setText(Constants.EICFF_CHCB_WRAP_TEXT);

			btnOk.setText(Constants.EICFF_BTN_OK);
		}
		
		
		txtInfo = txtFile + " '" + fileName + "' " + txtInfoRest;
		setTextToLblInfo(txtInfo);
	}
}
