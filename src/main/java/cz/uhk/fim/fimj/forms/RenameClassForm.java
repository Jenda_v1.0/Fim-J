package cz.uhk.fim.fimj.forms;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Properties;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;

/**
 * Třída slouží jako dialog pro přejmenování třídy
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class RenameClassForm extends CreateAndRenameClassFormAbst implements ActionListener, LanguageInterface {

	private static final long serialVersionUID = 1L;


	private JTextField txtNameOfClass;
	
	private JLabel lblNameOfClass;
	
	
	/**
	 * Promenná, do které vložím text třídy pro kontrolu správnosti syntaxe, apod. a
	 * pro původní název třídy - kontrola identity a tento text pak vrátím pokud
	 * "projde kontrolou"
	 */
	private String txtOldClassName;
	
	
	
	
	// Texty do chybových hlášek:
	private static String duplicateClassNameTitle, duplicateClassNameText;
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 */
	public RenameClassForm() {
		super();
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/app/RenameClassIcon.png", new Dimension(500, 200), true);
		
		
		gbc = createGbc();
		
		index = 0;
		
		
		
		lblNameOfClass = new JLabel();
		setGbc(index, 0, lblNameOfClass);
		
		
		
		txtNameOfClass = new JTextField(20);
		setGbc(index, 1, txtNameOfClass);
		
		
		
		
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		btnOk = new JButton();
		btnCancel = new JButton();
		
		btnOk.addActionListener(this);
		btnCancel.addActionListener(this);
		
		pnlButtons.add(btnOk);
		pnlButtons.add(btnCancel);
		
		gbc.gridwidth = 2;
		setGbc(++index, 0, pnlButtons);
		
		
		pack();
		setLocationRelativeTo(null);
		
		
		
		
		addKeyListener();
		
		
		

		
		addWindowListener(getWindowsAdapter());
	}
	
	
	
	
	
	
	
	
	
	// Reakce textového pole na klávesu enter:
	private void addKeyListener() {
		txtNameOfClass.addKeyListener(new KeyAdapter() {
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					testData();
				
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					variable = false;
					dispose();
				}
			}
		});
	}
	
	
	
	
	
	
	
	


	
	
	
	
	
	
	public final String getNewNameOfClass(final String oldName) {
		txtOldClassName = oldName;
		txtNameOfClass.setText(oldName);
		
		setVisible(true);
		
		if (variable)
			return classNameVariable;
		
		return null;
	}
	
	
	
	
	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		// naplním společné proměnné:
		fillIdenticalyVariables(properties);
		
		
		if (properties != null) {
			setTitle(properties.getProperty("Rcf_DialogTitle", Constants.RCF_DIALOG_TITLE));
			
			lblNameOfClass.setText(properties.getProperty("Rcf_LabelNameOfClass", Constants.RCF_LBL_NAME_OF_CLASS) + ": ");
			
			duplicateClassNameTitle = properties.getProperty("NcfDuplicateClassNameErrorTitle", Constants.NCF_DUPLICATE_CLASS_NAME_ERROR_TITLE);
			duplicateClassNameText = properties.getProperty("NcfDuplicateClassNameErrorText", Constants.NCF_DUPLICATE_CLASS_NAME_ERROR_TEXT);
			
			btnOk.setText(properties.getProperty("NcfButtonOk", Constants.NCF_BTN_OK));
			btnCancel.setText(properties.getProperty("NcfButtonCancel", Constants.NCF_BTN_CANCEL));	
		}
		
		
		else {
			setTitle(Constants.RCF_DIALOG_TITLE);
			
			lblNameOfClass.setText(Constants.RCF_LBL_NAME_OF_CLASS + ": ");
			
			duplicateClassNameTitle = Constants.NCF_DUPLICATE_CLASS_NAME_ERROR_TITLE;
			duplicateClassNameText = Constants.NCF_DUPLICATE_CLASS_NAME_ERROR_TEXT;
			
			btnOk.setText(Constants.NCF_BTN_OK);
			btnCancel.setText(Constants.NCF_BTN_CANCEL);
		}
	}
	
	
	



	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnOk)
				testData();
			
			
			else if (e.getSource() == btnCancel) {
				variable = false;
				dispose();
			}
		}
	}
	
	
	
	
	
	private void testData() {
		if (!txtNameOfClass.getText().isEmpty()) {
			classNameVariable = txtNameOfClass.getText().replaceAll("\\s", "");
			
			if (!classNameVariable.equals(txtOldClassName)) {
												
				final boolean var = isClassNameRight();
				
				if (var) {
					variable = true;
					dispose();
				}
			}
			else JOptionPane.showMessageDialog(this, duplicateClassNameText, duplicateClassNameTitle, JOptionPane.ERROR_MESSAGE);
		}
		else JOptionPane.showMessageDialog(this, emptyClassNameText, emptyClassNameTitle, JOptionPane.ERROR_MESSAGE);
	}
}