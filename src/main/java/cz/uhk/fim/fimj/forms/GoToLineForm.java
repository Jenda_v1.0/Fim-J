package cz.uhk.fim.fimj.forms;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;

/**
 * Tato třída slouží jako dialog pro zadání řádku, na který chce uživatel přesunout kurzor myši
 * <p>
 * Dialog má textvé pole, do kterého se zadá číslo řádku, na který chce uživatel přesunout kurzor myši Pokud bude tato
 * hodnoty chybně zadaná pole zčervená, například pokud budou zadány jiné znaky než - li číslice apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class GoToLineForm extends SourceDialogForm implements LanguageInterface, ActionListener {

	private static final long serialVersionUID = 1L;


	// Regulární výraz, který rozpozná, zda jsou v daném řetězci pouze přirozená čísla
	private static final String regExForOnlyNumbers = "^\\d+$";
	
	
	/**
	 * Proměnná, do které uložím počet řádků coby interval, který lze zadat:
	 */
	private final int lineAge;
	
	
	private JLabel lblLine, lblErrorText;
	
	private JTextField txtLine;
	
	
	// Prmnné pro uložení textů do chybových hlášek:
	private static String lineNumberIsNotCorrectText, lineNumberIsNotCorrectTitle, emptyFieldText, emptyFieldTitle; 
	
	
	
	/**
	 * Proměnná, do které vložím číslo řádku, na který chce uživatel přesunout
	 * kurzor
	 */
	private int line = -1;
	
	
	
	
	
	public GoToLineForm(final int lineAge) {
		super();
		
		this.lineAge = lineAge;
		
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/app/GoToLineIcon.jpeg", new Dimension(450, 200), true);
		
		
		
		
		gbc = createGbc();
		
		index = 0;
		
		
		
		gbc.gridwidth = 2;
		
		final JPanel pnlErrorText = new JPanel(new FlowLayout(FlowLayout.CENTER));
		
		lblErrorText = new JLabel();
		lblErrorText.setFont(ERROR_FONT);
		lblErrorText.setForeground(Color.RED);
		lblErrorText.setVisible(false);
		pnlErrorText.add(lblErrorText);
		
		setGbc(index, 0, pnlErrorText);
		
		
		
		
		
		
		
		gbc.gridwidth = 1;
		
		lblLine = new JLabel("1 - " + lineAge);
		setGbc(++index, 0, lblLine);
		
		
		
		
		
		txtLine = new JTextField(20);
		addKeyListenerToLineField();
		setGbc(index, 1, txtLine);
		
		
		
		
		
		
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		btnOk = new JButton();
		btnCancel = new JButton();
		
		btnOk.addActionListener(this);
		btnCancel.addActionListener(this);
		
		pnlButtons.add(btnOk);
		pnlButtons.add(btnCancel);
		
		gbc.gridwidth = 2;
		
		setGbc(++index, 0, pnlButtons);
		
		
		
		
		
		

		
		addWindowListener(getWindowsAdapter());
		
		
		pack();
		setLocationRelativeTo(null);
	}


	
	
	
	
	
	/**
	 * Metoda, která pole pro zadání čísla řádku zčervená, pokud nebude číslo v
	 * potřebném intervalu nebo v příslušné syntaxi - nebude obsahovat čísla, apod.
	 */
	private void addKeyListenerToLineField() {
		txtLine.addKeyListener(new KeyAdapter() {
			
			@Override
			public void keyReleased(KeyEvent e) {
				final String textFromLineField = txtLine.getText().replace("\\s", "");
				
				if (textFromLineField.matches(regExForOnlyNumbers)) {
					// Zde jsou v řetězci pouze přirozená čísla:
					
					final int numberFromLine = Integer.parseInt(textFromLineField);
					
					if (numberFromLine >= 1 && numberFromLine <= lineAge) {
						// Zde je zadané číslo v povoleném rozsahu:
						line = numberFromLine;
						
						if (txtLine.getBackground() != Color.WHITE)
							txtLine.setBackground(Color.WHITE);
						
						if (lblErrorText.isVisible())
							lblErrorText.setVisible(false);
					}
					
					else {
						// Zde je číslo mimo rozsah:
						if (!lblErrorText.isVisible())
							lblErrorText.setVisible(true);
						
						if (txtLine.getBackground() != Color.RED)
							txtLine.setBackground(Color.RED);
						
						line = -1;
					}
				}
				
				else {
					// Zde nejsou v řezězci pouze přirozená čísla
					if (!lblErrorText.isVisible())
						lblErrorText.setVisible(true);
					
					if (txtLine.getBackground() != Color.RED)
						txtLine.setBackground(Color.RED);
					
					line = -1;
				}
			}
			
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					testData();
			}
		});
	}
	
	
	
	
	


	
	
	
	
	
	
	/**
	 * Metoda, která zobrazí dialog a tím ho udělá modálním, dále pokud se klikne na
	 * tlačítko OK, tak se otestuje, zda je zadáno číslo řádku ve správném rozsahu a
	 * syntaxi, případně o vrátí, jinak se zavře dialog (po kliknutí na OK nebo
	 * křížek)
	 * 
	 * @return číslo řádku, kam se má přesunou kurzor myši v editoru zdrojov´ho kódu
	 *         nebo -1, pokud se dialog uzavřel
	 */
	public final int getLineToGo() {
		setVisible(true);
		
		if (variable)
			return line;
		
		return -1;
	}
	
	
	
	
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnOk)
				testData();
			
			else if (e.getSource() == btnCancel) {
				variable = false;
				dispose();
			}
		}
	}
	
	
	
	
	
	
	/**
	 * Metoda, která po kliknutí na tlačítko OK nebo stisknutí klávesy Enter
	 * otestuje data v dialogu
	 * 
	 * je to takto udělané, aby mohl dialog reagovat i na stisknutí enteru
	 */
	private void testData() {
		if (!txtLine.getText().isEmpty()) {
			if (line > -1) {
				// Zde je již zadaáno číslo ve správném formátu, mohu ji vrátit:
				variable = true;
				dispose();
			}
			else
				JOptionPane.showMessageDialog(this, lineNumberIsNotCorrectText, lineNumberIsNotCorrectTitle,
						JOptionPane.ERROR_MESSAGE);
		}
		else
			JOptionPane.showMessageDialog(this, emptyFieldText, emptyFieldTitle, JOptionPane.ERROR_MESSAGE);
	}
	
	
	

	
	
	
	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {		
			setTitle(properties.getProperty("Ced_Gtlf_DialogTitle", Constants.GTLM_TITLE_DIALOG));
			
			lblLine.setText(properties.getProperty("Ced_Gtlf_LabelLine", Constants.GTLM_LBL_LINE) + " ( " + lblLine.getText() + " ):");
			
			lblErrorText.setText(properties.getProperty("Ced_Gtlf_LabelErrorText", Constants.GTLM_LBL_ERROR_TEXT));
			
			
			btnOk.setText(properties.getProperty("Ced_Gtlf_ButtonOk", Constants.GTLM_BTN_OK));
			btnCancel.setText(properties.getProperty("Ced_Gtlf_ButtonCancel", Constants.GTLM_BTN_CANCEL));
			
			lineNumberIsNotCorrectText = properties.getProperty("Ced_Gtlf_LineNumberIsNotCorrectText", Constants.GTLM_LINE_NUMBER_IS_NOT_CORRECT_TEXT);
			lineNumberIsNotCorrectTitle = properties.getProperty("Ced_Gtlf_LineNumberIsNotCorrectTitle", Constants.GTLM_LINE_NUMBER_IS_NOT_CORRECT_TITLE);
			
			emptyFieldText = properties.getProperty("Ced_Gtlf_EmptyFieldText", Constants.GTLM_EMPTY_FIELD_TEXT);
			emptyFieldTitle = properties.getProperty("Ced_Gtlf_EmptyFieldTitle", Constants.GTLM_EMPTY_FIELD_TITLE);
		}
		
		else {
			setTitle(Constants.GTLM_TITLE_DIALOG);
			
			lblLine.setText(Constants.GTLM_LBL_LINE + " ( " + lblLine.getText() + " ):");
			
			lblErrorText.setText(Constants.GTLM_LBL_ERROR_TEXT);
			
			
			btnOk.setText(Constants.GTLM_BTN_OK);
			btnCancel.setText( Constants.GTLM_BTN_CANCEL);
			
			lineNumberIsNotCorrectText = Constants.GTLM_LINE_NUMBER_IS_NOT_CORRECT_TEXT;
			lineNumberIsNotCorrectTitle = Constants.GTLM_LINE_NUMBER_IS_NOT_CORRECT_TITLE;
			
			emptyFieldText = Constants.GTLM_EMPTY_FIELD_TEXT;
			emptyFieldTitle = Constants.GTLM_EMPTY_FIELD_TITLE;
		}
	}
}