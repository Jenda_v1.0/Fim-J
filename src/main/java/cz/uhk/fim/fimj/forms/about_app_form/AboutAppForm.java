package cz.uhk.fim.fimj.forms.about_app_form;

import java.awt.*;
import java.awt.event.*;
import java.awt.font.TextAttribute;
import java.io.File;
import java.net.URL;
import java.util.Map;
import java.util.Properties;

import javax.swing.*;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.*;
import cz.uhk.fim.fimj.forms.SourceDialogForm;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.preferences.PreferencesApi;
import cz.uhk.fim.fimj.redirect_sysout.RedirectApp;

/**
 * Tato třída sloluží jako dialog, který obsahuje informace o mně, jakožto o autorovi této aplikace a nějaké základní
 * iformace o aplikaci - název, verze, a nějaké umístění "základních" adresářů, podobně, jako to má BlueJ.
 *
 * <i>V událostech po kliknutí na label s odkazy na konfigurační soubory nebo adresáře pravým tlačítkem myši by bylo
 * neotevírat kontextové menu, ale v podstatě "nedělat nic". Ale raději se vypíše hláška, že příslušný adresář nebo
 * soubor neexistuje, aby o tom uživatel věděl.</i>
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class AboutAppForm extends SourceDialogForm implements LanguageInterface, MouseListener {

    private static final long serialVersionUID = -1882019962807998013L;


    private static final String EMAIL = "kruncikjan95@seznam.cz";

    /**
     * Výchozí stránka projektu / aplikace Fim-J na GitLabu.
     */
    private static final String GITLAB_PROJECT_URL_PAGE = "https://gitlab.com/Jenda_v1.0/Fim-J";

    /**
     * Výchozí stránka, kde se nachází Wiki (o projektu / apikaci) na GitLabu.
     */
    private static final String GITLAB_WIKI_URL_PAGE = "https://gitlab.com/Jenda_v1.0/Fim-J/wikis/home";


    private String txtUnknown;
    private String txtDoesNotExist;


    // Labely, do kterých se vloží popisky ohledně iformací, které reprezentují:
    private final JLabel lblAppName;
    private final JLabel lblAppNameValue;

    private final JLabel lblVersion;
    private final JLabel lblAuthor;
    private final JLabel lblWiki;

    private final JLabel lblEmail;
    private final JLabel lblEmailValue;

    private final JLabel lblJavaHome;
    private final JLabel lblJavaHomeValue;

    private final JLabel lblJavaHomeForApp;
    private final JLabel lblJavaHomeForAppValue;

    private final JLabel lblWorkspaceValue;

    private final JLabel lblDebugInfo;
    private final JLabel lblDebugInfoValue;


    /**
     * Tlačítko pro zavření tohoto dialogu
     */
    private final JButton btnClose;


    /**
     * Talčítko, které slouží pro výpis podporovaných verzí Javy, které umí tato aplikace zkompilovat.
     */
    private final JButton btnWriteJavaVersion;


    /**
     * Proměnná, která obsahuje text "Java Home", abych jej nemusel psát pořád do kola.
     */
    private static final String TXT_JAVA_HOME = "Java Home";


    /**
     * Uložení reference na texty pro tuto aplikaci v uživatelem zvoleném jazyce, aby se mohly předat do kontextového
     * menu.
     */
    private Properties languageProperties;


    // Texty pro chybové hlášky pro otevření kontextového menu pro otevření souboru nebo adresáře:
    private String txtPath;

    // Zda existuje adresář:
    private String txtDirDoesNotExistText;
    private String txtDirDoesNotExistTitle;

    // Zda existuje soubor:
    private String txtFileDoesNotExistText;
    private String txtFileDoesNotExistTitle;


    /**
     * Konstruktor třídy.
     */
    public AboutAppForm() {
        initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/app/AuthorIcon.png", new Dimension(525, 500), true);

        // Nastavení minimální velikosti okna dialogu, pod kterou již okno dialogu nepůjde zmenšit.
        setMinimumSize(new Dimension(290, 400));


        gbc = createGbc();
        gbc.gridheight = 5;


        // Jako první zobrazím  obrázek coby ikona administrátora:

        // Note:
        // Zde si potřebuji načíst obrázek normálně pomocí streamů a změnšít jeho velikost
        // pomocí metody getScaledInstance, kdybych zmenšíl například Jlabel, tak se tomu nepřízpůsobí
        // obrázek

        final URL url = getClass().getResource("/icons/app/AuthorIcon.png");
        final Image img = new ImageIcon(url).getImage();

        final JLabel lblImg = new JLabel(new ImageIcon(img.getScaledInstance(220, 240, Image.SCALE_SMOOTH)));
        setGbc(0, 0, lblImg);


        gbc.gridheight = 1;

        index = 0;


        lblAppName = new JLabel();
        lblAppNameValue = new JLabel(Constants.APP_TITLE);
        lblAppNameValue.addMouseListener(this);
        setLinkAttributesToLinkLabel(lblAppNameValue);
        setGbc(index, 1, createPanel(lblAppName, lblAppNameValue));


        lblVersion = new JLabel(": 2.0");
        setGbc(++index, 1, createPanel(lblVersion));

        lblAuthor = new JLabel(": Jan Krunčík");
        setGbc(++index, 1, createPanel(lblAuthor));

        lblWiki = new JLabel();
        lblWiki.addMouseListener(this);
        setLinkAttributesToLinkLabel(lblWiki);
        setGbc(++index, 1, createPanel(lblWiki));


        lblEmail = new JLabel();
        lblEmailValue = new JLabel(EMAIL);
        lblEmailValue.addMouseListener(this);
        setLinkAttributesToLinkLabel(lblEmailValue);
        setGbc(++index, 1, createPanel(lblEmail, lblEmailValue));


        gbc.gridwidth = 2;


        // Cesta k adresáři, kde je nainstalovaná Java na daném zařízení.
        lblJavaHome = new JLabel();
        lblJavaHomeValue = new JLabel();
        setGbc(++index, 0, createPanel(lblJavaHome, lblJavaHomeValue));


        lblJavaHomeForApp = new JLabel();
        lblJavaHomeForAppValue = new JLabel();
        setGbc(++index, 0, createPanel(lblJavaHomeForApp, lblJavaHomeForAppValue));


        final JLabel lblWorkspace = new JLabel("Workspace: ");
        lblWorkspaceValue = new JLabel();
        setGbc(++index, 0, createPanel(lblWorkspace, lblWorkspaceValue));


        lblDebugInfo = new JLabel();
        lblDebugInfoValue = new JLabel();
        setGbc(++index, 0, createPanel(lblDebugInfo, lblDebugInfoValue));







        /*
         * Aby byla tlačítka zarovnaná na střed, je třeba je vložit do panelu a ten pak do okna:
         */
        final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.CENTER));

        btnClose = new JButton();
        btnClose.addActionListener(event -> dispose());

        btnWriteJavaVersion = new JButton();
        btnWriteJavaVersion
                .addActionListener(event -> RedirectApp.addSupportedJavaVersions(CompileClass.getJavaVersions()));

        pnlButtons.add(btnWriteJavaVersion);
        pnlButtons.add(btnClose);
        setGbc(++index, 0, pnlButtons);


        pack();
        setLocationRelativeTo(null);
    }


    /**
     * Nastavení labelům, které obsahují informace, konkrétně cesty k vybraným souborům a adresářům, které jsou důležité
     * pro běh aplikace nebo pro uživatele.
     * <p>
     * Dále se nastaví labelům v případě, že příslušný adresář nebo soubor existuje formát / syntaxe odkazu a událost na
     * kliknutí myši.
     */
    public void setValueToConfigurationInfoLabels() {
        // Nastavení labelu pro cestu k Java Home adresáři:
        prepareLabelForLink(lblJavaHomeValue, App.PATH_TO_JAVA_HOME_DIR, true);



        /*
         * Cesta k adresáři, kterou využívá tato aplikace, aby mohla kompilovat Javovské třídy. Je to cesta k
         * adresáři, kde se nachází soubory potřebné pro kompilování tříd v této aplikaci.
         */
        final String pathToJavaHomeDir = PreferencesApi.getPreferencesValue(PreferencesApi.COMPILER_DIR, null);
        prepareLabelForLink(lblJavaHomeForAppValue, pathToJavaHomeDir, true);



        // Pokusim se získat cestu k adresáři zvolený jako workspace, pokud se najde a příslušný adresář ještě
        // existuje, tak se nastaví příslušná cesta, pokud se nenajde cesta nebo příslušný adresář již neexistuje,
        // tak se nastaví na "Neuvedeno"
        final String pathToWorkspace = ReadFile.getPathToWorkspace();
        /*
         * Note:
         *
         * V této metodě se bude znovu testovat, zda existuje výše získaná cesta k adresáři workspace, ale to už není
          * třeba, protože se vrátí cesta k workspace null v případě, že neexistuje nebo nebyla nalezena (/
          * neexistuje), takže se nikdy nevrátí cesta k neexistujícímu adresáři. Proto není třeba takovou možnost
          * testovat, protože se příslušný text s informací o neexistenci adresáře nezobrazí.
         *
         * (Pro přehlednost se zde ponechá tato metoda pro testování.)
         */
        prepareLabelForLink(lblWorkspaceValue, pathToWorkspace, true);



        if (pathToWorkspace != null) {
            /*
             * Proměnná, která drží cestu k souboru, který reprezentuje informace o právě spuštěné aplikací. Obsahuje
              * informace, jako když vypadla jaká vyjímka (zachytávaná) a spuštění či ukončení aplikace (cestu k logu).
             */
            final String pathToLog = pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME + File.separator
                    + Constants.LOGS_NAME + File.separator + ExceptionLogger.getDateTimeNameFile() + "log";

            prepareLabelForLink(lblDebugInfoValue, pathToLog, false);
        }

        else lblDebugInfoValue.setText(txtDoesNotExist);
    }


    /**
     * Nastavení labelu label text a syntaxi odkazu v případě, že path je cesta k existujícímu souboru nebo adresáři.
     * <p>
     * V případě, že checkExistenceOfDir je true, pak path musí být existující adresář, jinak existující soubor.
     * <p>
     * V případě, že je path null, label bude mát text "Neuvedeno". V případě, že path je neexistující soubor nebo
     * adresář, label bude mít text "Neexistuje".
     *
     * @param label
     *         - label, kterému se má nastavit výše zmíněný formát / syntaxe pro odkaz a příslušný text.
     * @param path
     *         - cesta k souboru nebo adresáři, který má existovat. V případě že existuje, nastaví se tato cesta do
     *         labelu label.
     * @param checkExistenceOfDir
     *         true v případě, že se má testovat, že je path existující adresář, jinak false, pak se bude testovat, zda
     *         je path existující soubor.
     */
    private void prepareLabelForLink(final JLabel label, final String path, final boolean checkExistenceOfDir) {
        if (path == null)
            label.setText(txtUnknown);

        else if ((checkExistenceOfDir && ReadFile.existsDirectory(path)) || (!checkExistenceOfDir && ReadFile.existsFile(path))) {
            label.addMouseListener(this);
            setLinkAttributesToLinkLabel(label);
            label.setText(path);
        }

        else label.setText(txtDoesNotExist);
    }


    /**
     * Vytvoření panelu, který bude obsahovat nejprve label1 a vedle něj vpravo label2.
     *
     * <i>Panel bude mít jako výchozí layout flowLayout se zarovnáním k levé straně.</i>
     *
     * @param label1
     *         - label, který se má přidat jako první do panelu (na levou stranu).
     * @param label2
     *         - label, který se má přidat jako druhý do panelu (na pravou stranu).
     *
     * @return výše popsaný panel, který bude obsaovat výše zmíněné labely.
     */
    private static JPanel createPanel(final JLabel label1, final JLabel label2) {
        /*
         * Je třeba využít BorderLayout (například), protože je to jeden z layout managerů, který respektuje
         * nastavenou preferedsize, takže jednotlivé objekty v kontejneru s tímto layout managerem nezmizí, jako když
          * se použije například FlowLayout.
         */
        final JPanel panel = new JPanel(new BorderLayout());

        panel.add(label1, BorderLayout.WEST);
        panel.add(label2, BorderLayout.CENTER);

        return panel;
    }


    /**
     * Vytvoření panelu, který bude obsahovat label.
     *
     * <i>Panel bude mít jako výchozí layout flowLayout se zarovnáním k levé straně.</i>
     *
     * <i>Jedná se o to, aby byly hodnoty v okně zarovnýny "stejně", tak se využívá stejný panel apod.</i>
     *
     * @param label
     *         - label, který se má přidat do panelu.
     *
     * @return výše popsaný panel, který bude obsaovat výše zmíněný label.
     */
    private static JPanel createPanel(final JLabel label) {
        final JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));

        panel.add(label);

        return panel;
    }


    /**
     * Nastavení parametrů komponentě (Jlabel) tak, že bude vypadat / připomínat (měla by) odkaz.
     * <p>
     * Jedná se o nastavení modré barvy písma a podtržení textu.
     *
     * @param label
     *         - label, kterému se mají nastavit výše zmíněné parametry.
     */
    private static void setLinkAttributesToLinkLabel(final JLabel label) {
        final Map attributes = label.getFont().getAttributes();

        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        label.setFont(label.getFont().deriveFont(attributes));

        // Modrá barva labelu, aby bylo vidět, že se jedná o odkaz:
        label.setForeground(new Color(0, 0, 238));

        // Po najetí kurzoru myší na label se zobrazí šipka kurzoru jako ikona:
        label.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    }


    @Override
    public void setLanguage(final Properties properties) {
        languageProperties = properties;

        // Texty pro dialog ve zvoleném jazyce:
        final String txtName;
        final String txtVersion;
        final String txtAuthor;
        final String txtWikiAboutApp;
        final String txtEmail;
        final String txtDebugInfo;
        final String txtForApp;

        if (properties != null) {
            setTitle(properties.getProperty("Aaf_DialogTitle", Constants.AAF_DIALOG_TITLE));

            btnClose.setText(properties.getProperty("Aaf_BtnClose", Constants.AAF_BTN_CLOSE));
            btnWriteJavaVersion.setText(properties.getProperty("Aaf_BtnJavaVersion", Constants.AAF_BTN_JAVA_VERSION));

            // Naplnění textových proměnných:
            txtName = properties.getProperty("Aaf_Txt_Name", Constants.AAF_TXT_NAME);
            txtVersion = properties.getProperty("Aaf_Txt_Version", Constants.AAF_TXT_VERSION);
            txtAuthor = properties.getProperty("Aaf_Txt_Author", Constants.AAF_TXT_AUTHOR);
            txtWikiAboutApp = properties.getProperty("Aaf_Txt_WikiAboutApp", Constants.AAF_TXT_WIKI_ABOUT_APP);
            txtEmail = properties.getProperty("Aaf_Txt_Email", Constants.AAF_TXT_EMAIL);
            txtUnknown = properties.getProperty("Aaf_Txt_Unknown", Constants.AAF_TXT_UNKNOWN);
            txtDoesNotExist = properties.getProperty("Aaf_Txt_DoesNotExist", Constants.AAF_TXT_DOES_NOT_EXIST);
            txtDebugInfo = properties.getProperty("Aaf_Txt_DebugInfo", Constants.AAF_TXT_DEBUG_INFO);
            txtForApp = properties.getProperty("Aaf_Txt_ForApp", Constants.AAF_TXT_FOR_APP);


            // Texty pro chybové hlášky pro otevření kontextového menu pro otevření souboru nebo adresáře:
            txtPath = properties.getProperty("Aaf_Txt_Path", Constants.AAF_TXT_PATH);

            // Zda existuje adresář:
            txtDirDoesNotExistText = properties.getProperty("Aaf_Txt_DirDoesNotExist_Text", Constants.AAF_TXT_DIR_DOES_NOT_EXIST_TEXT);
            txtDirDoesNotExistTitle = properties.getProperty("Aaf_Txt_DirDoesNotExist_Title", Constants.AAF_TXT_DIR_DOES_NOT_EXIST_TITLE);

            // Zda existuje soubor:
            txtFileDoesNotExistText = properties.getProperty("Aaf_Txt_FileDoesNotExist_Text", Constants.AAF_TXT_FILE_DOES_NOT_EXIST_TEXT);
            txtFileDoesNotExistTitle = properties.getProperty("Aaf_Txt_FileDoesNotExist_Title", Constants.AAF_TXT_FILE_DOES_NOT_EXIST_TITLE);
        }

        else {
            setTitle(Constants.AAF_DIALOG_TITLE);

            btnClose.setText(Constants.AAF_BTN_CLOSE);
            btnWriteJavaVersion.setText(Constants.AAF_BTN_JAVA_VERSION);

            // Naplnění textových proměnných:
            txtName = Constants.AAF_TXT_NAME;
            txtVersion = Constants.AAF_TXT_VERSION;
            txtAuthor = Constants.AAF_TXT_AUTHOR;
            txtWikiAboutApp = Constants.AAF_TXT_WIKI_ABOUT_APP;
            txtEmail = Constants.AAF_TXT_EMAIL;
            txtUnknown = Constants.AAF_TXT_UNKNOWN;
            txtDoesNotExist = Constants.AAF_TXT_DOES_NOT_EXIST;
            txtDebugInfo = Constants.AAF_TXT_DEBUG_INFO;
            txtForApp = Constants.AAF_TXT_FOR_APP;


            // Texty pro chybové hlášky pro otevření kontextového menu pro otevření souboru nebo adresáře:
            txtPath = Constants.AAF_TXT_PATH;

            // Zda existuje adresář:
            txtDirDoesNotExistText = Constants.AAF_TXT_DIR_DOES_NOT_EXIST_TEXT;
            txtDirDoesNotExistTitle = Constants.AAF_TXT_DIR_DOES_NOT_EXIST_TITLE;

            // Zda existuje soubor:
            txtFileDoesNotExistText = Constants.AAF_TXT_FILE_DOES_NOT_EXIST_TEXT;
            txtFileDoesNotExistTitle = Constants.AAF_TXT_FILE_DOES_NOT_EXIST_TITLE;
        }


        setTitle(getTitle() + " " + Constants.APP_TITLE);


        // Naplnění Labelů:
        lblAppName.setText(txtName + ": ");
        lblVersion.setText(txtVersion + lblVersion.getText());
        lblAuthor.setText(txtAuthor + lblAuthor.getText());
        lblWiki.setText(txtWikiAboutApp);
        lblEmail.setText(txtEmail + ": ");
        // Nastavím cestu k adresáři, kde je na daném zařízení nainstalovaná Java:
        lblJavaHome.setText(TXT_JAVA_HOME + ": ");
        // V případě, že se nepodařilo nalézt cestu k Java Home adresáři pro aplikaci, zobrazí se text "neuvedeno":
        lblJavaHomeForApp.setText(TXT_JAVA_HOME + " " + txtForApp + ": ");
        lblDebugInfo.setText(txtDebugInfo + ": ");
    }


    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1 && e.getSource() instanceof JLabel) {
            if (e.getSource() == lblAppNameValue)
                DesktopSupport.openBrowser(GITLAB_PROJECT_URL_PAGE);

            else if (e.getSource() == lblWiki)
                DesktopSupport.openBrowser(GITLAB_WIKI_URL_PAGE);

            else if (e.getSource() == lblEmailValue)
                DesktopSupport.openEmailClient(EMAIL, Constants.APP_TITLE);


            else if (e.getSource() == lblJavaHomeValue)
                /*
                 * V případě, že tato událost bude fungovat, pak byl na příslušný label přidán posluchač na kliknutí
                 * myší. V takovém případě soubor existoval při otevření dialogu.
                 *
                 * Je tedy možné zavolat následující metodu pro otevření adresáře JavaHome, ale v té metodě se pro
                 * jistotu otestuje, zda adresář existuje, protože jej uživatel mohl mezi tím smazat, přesunout,
                 * přejmenovat apod.
                 */
                DesktopSupport.openFileExplorer(new File(lblJavaHomeValue.getText()), true);


            else if (e.getSource() == lblJavaHomeForAppValue)
                /*
                 * V případě, že tato událost bude fungovat, pak byl na příslušný label přidán posluchač na kliknutí
                 * myší. V takovém případě soubor existoval při otevření dialogu.
                 *
                 * Je tedy možné zavolat následující metodu pro otevření adresáře JavaHome, ale v té metodě se pro
                 * jistotu otestuje, zda adresář existuje, protože jej uživatel mohl mezi tím smazat, přesunout,
                 * přejmenovat apod.
                 */
                DesktopSupport.openFileExplorer(new File(lblJavaHomeForAppValue.getText()), true);


            else if (e.getSource() == lblWorkspaceValue)
                /*
                 * V případě, že tato událost bude fungovat, pak byl na příslušný label přidán posluchač na kliknutí
                 * myší. V takovém případě soubor existoval při otevření dialogu.
                 *
                 * Je tedy možné zavolat následující metodu pro otevření adresáře JavaHome, ale v té metodě se pro
                 * jistotu otestuje, zda adresář existuje, protože jej uživatel mohl mezi tím smazat, přesunout,
                 * přejmenovat apod.
                 */
                DesktopSupport.openFileExplorer(new File(lblWorkspaceValue.getText()), true);


            else if (e.getSource() == lblDebugInfoValue)
                /*
                 * V případě, že tato událost bude fungovat, pak byl na příslušný label přidán posluchač na kliknutí
                 * myší. V takovém případě soubor existoval při otevření dialogu.
                 *
                 * Je tedy možné zavolat následující metodu pro otevření adresáře JavaHome, ale v té metodě se pro
                 * jistotu otestuje, zda adresář existuje, protože jej uživatel mohl mezi tím smazat, přesunout,
                 * přejmenovat apod.
                 */
                DesktopSupport.openFileExplorer(new File(lblDebugInfoValue.getText()), false);

        }


        else if (e.getButton() == MouseEvent.BUTTON3 && e.getSource() instanceof JLabel) {
            if (e.getSource() == lblJavaHomeValue) {
                /*
                 * Jelikož tato událost může nastat pouze v případě, že na tento label byl doplněn posluchač na
                 * kliknutí, tak příslušný adresář existoval alespoň pří otevření dialogu. Takže zde stačí znovu
                 * otestovat, zda příslušný adresář stále existuje a pokud ano, otevřít kontextové menu, pokud
                 * neexistuje, zobrazí se chybové oznámění.
                 */
                if (checkFileForOpenContextMenu(lblJavaHomeValue.getText(), true))
                    openContextMenu(lblJavaHomeValue, e.getX(), e.getY());
            }

            else if (e.getSource() == lblJavaHomeForAppValue) {
                /*
                 * Jelikož tato událost může nastat pouze v případě, že na tento label byl doplněn posluchač na
                 * kliknutí, tak příslušný adresář existoval alespoň pří otevření dialogu. Takže zde stačí znovu
                 * otestovat, zda příslušný adresář stále existuje a pokud ano, otevřít kontextové menu, pokud
                 * neexistuje, zobrazí se chybové oznámění.
                 */
                if (checkFileForOpenContextMenu(lblJavaHomeForAppValue.getText(), true))
                    openContextMenu(lblJavaHomeForAppValue, e.getX(), e.getY());
            }

            else if (e.getSource() == lblWorkspaceValue) {
                /*
                 * Jelikož tato událost může nastat pouze v případě, že na tento label byl doplněn posluchač na
                 * kliknutí, tak příslušný adresář existoval alespoň pří otevření dialogu. Takže zde stačí znovu
                 * otestovat, zda příslušný adresář stále existuje a pokud ano, otevřít kontextové menu, pokud
                 * neexistuje, zobrazí se chybové oznámění.
                 */
                if (checkFileForOpenContextMenu(lblWorkspaceValue.getText(), true))
                    openContextMenu(lblWorkspaceValue, e.getX(), e.getY());
            }

            else if (e.getSource() == lblDebugInfoValue) {
                /*
                 * Jelikož tato událost může nastat pouze v případě, že na tento label byl doplněn posluchač na
                 * kliknutí, tak příslušný adresář existoval alespoň pří otevření dialogu. Takže zde stačí znovu
                 * otestovat, zda příslušný adresář stále existuje a pokud ano, otevřít kontextové menu, pokud
                 * neexistuje, zobrazí se chybové oznámění.
                 */
                if (checkFileForOpenContextMenu(lblDebugInfoValue.getText(), false))
                    openContextMenu(lblDebugInfoValue, e.getX(), e.getY());
            }
        }
    }


    @Override
    public void mousePressed(MouseEvent e) {
        // Metoda není potřeba.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // Metoda není potřeba.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // Metoda není potřeba.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // Metoda není potřeba.
    }


    /**
     * Zjištění, zda označený soubor nebo adresář, pro který se má otevřít kontextové menu stále existuje.
     *
     * @param path
     *         - cesta k souboru nebo adresáři, o kterém se má zjistit, zda stále existuje.
     * @param checkExistenceOfDir
     *         - true v případě, že se má zjistit, zda existuje adresář na cestě path. False v případě, že se má
     *         zjistit, zda existuje soubor na cestě path.
     *
     * @return true v případě, že path je existující soubor nebo adresář, jinak false.
     */
    private boolean checkFileForOpenContextMenu(final String path, final boolean checkExistenceOfDir) {
        if (checkExistenceOfDir) {
            if (!ReadFile.existsDirectory(path)) {
                final String textForShow = txtDirDoesNotExistText + "\n" + txtPath + ": " + path;

                JOptionPane.showMessageDialog(this, textForShow, txtDirDoesNotExistTitle, JOptionPane.ERROR_MESSAGE);

                return false;
            }
        }

        else if (!ReadFile.existsFile(path)) {
            final String textForShow = txtFileDoesNotExistText + "\n" + txtPath + ": " + path;

            JOptionPane.showMessageDialog(this, textForShow, txtFileDoesNotExistTitle, JOptionPane.ERROR_MESSAGE);

            return false;
        }

        return true;
    }


    /**
     * Otevření kontextového menu s možnostmi pro otevření souboru nebo adresáře na cestě uvedené v labelu v jedné z
     * komponent této aplikaci nebo využité platofmy.
     *
     * @param label
     *         - label obsahující cestu k souboru nebo adresáři, pro který se otevře kontextové menu.
     * @param x
     *         - souřadnice x, kde se má otevřít kontextové menu.
     * @param y
     *         - souřadnice y, kde se má otevřít kontextové menu.
     */
    private void openContextMenu(final JLabel label, final int x, final int y) {
        final PopupMenu popupMenu = new PopupMenu(new File(label.getText()));

        popupMenu.setLanguage(languageProperties);
        popupMenu.show(label, x, y);
    }
}


