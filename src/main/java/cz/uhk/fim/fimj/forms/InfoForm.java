package cz.uhk.fim.fimj.forms;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Properties;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;

/**
 * Tato třída slouží jako dialog, který obsahuje pouze základní informace ohledně zacházení, resp. ovládání této
 * aplikace - nějaké základní funkce a postupy jak s touto aplikací pracovat apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class InfoForm extends SourceDialogForm implements LanguageInterface {

	private static final long serialVersionUID = 1L;

	
	/**
	 * Komponenta, do které budou vypsány základní informace ohledně ovládání
	 * aplikace.
	 */
	private final JTextArea txtArea;


	
	/**
	 * Konstruktor této třídy.
	 */
	public InfoForm() {
		super();
		
		initGui(DISPOSE_ON_CLOSE, new BorderLayout(), "/icons/app/InfoIcon.png", new Dimension(700, 500), true);
		
		
		
		txtArea = new JTextArea();
		add(new JScrollPane(txtArea), BorderLayout.CENTER);
		
		// TextArea nepůjde editovat - text bude na pevno zobrazen
		txtArea.setEditable(false);				
				
		
		
		pack();
		setLocationRelativeTo(null);
	}


	
	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		// Proměnná, do které se vloží text s informacemo ohledně ovládání aplikace - základní ifno:
		final String textWithInformation;
		
		if (properties != null) {
			setTitle(properties.getProperty("If_Txt_DialogTitle", Constants.IF_DIALOG_TITLE));						
			
			textWithInformation = properties.getProperty("If_Txt_BasicInfo", Constants.IF_TXT_BASIC_INFO) + ":\n\n"
					+ properties.getProperty("If_Txt_ClassDiagram", Constants.IF_TXT_CLASS_DIAGRAM) + "\n"
					+ properties.getProperty("If_Txt_CreateClass", Constants.IF_TXT_CREATE_CLASS) + "\n"				
					+ "- " + properties.getProperty("If_Txt_InfoHowToCreateClass", Constants.IF_TXT_INFO_HOW_TO_CREATE_CLASS) + "\n"
					+ "- " + properties.getProperty("If_Txt_HowToWriteClassName", Constants.IF_TXT_INFO_HOW_TO_WRITE_CLASS_NAME) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoForClassNameWithoutPackages", Constants.IF_TXT_INFO_FOR_CLASS_NAME_WITHOUT_PACKAGES) + "\n"
					+ "- " + properties.getProperty("If_Txt_ChooseTemplateForClass", Constants.IF_TXT_CHOOSE_TEMPLATE_FOR_CLASS) + "\n"
					+ "\n"
					+ " " + properties.getProperty("If_Txt_AddRelations", Constants.IF_TXT_ADD_RELATIONS) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoForClickOnButton", Constants.IF_TXT_INFO_FOR_CLICK_ON_BUTTON) + "\n"
					+ "- " + properties.getProperty("If_Txt_ChooseClassesByClick", Constants.IF_TXT_CHOOSE_CLASSES_BY_CLICK) + "\n"
					+ "- " + properties.getProperty("If_Txt_AddedSyntaxeToSourceCode", Constants.IF_TXT_ADDED_SYNTAXE_TO_SOURCE_CODE) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoForCreateEdge", Constants.IF_TXT_INFO_FOR_CREATE_EDGE) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoForAddComment", Constants.IF_TXT_INFO_FOR_ADD_COMMENT) + "\n"
					+ "\n"
					+ " " + properties.getProperty("If_Txt_RemoveObjectOrRelations", Constants.IF_TXT_REMOVE_OBJECT_OR_RELATIONS) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoHowToRemoveObject", Constants.IF_TXT_INFO_HOW_TO_REMOVE_OBJECT) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoForRemoveComment", Constants.IF_TXT_INFO_FOR_REMOVE_COMMENT) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoHowToRemoveRelations", Constants.IF_TXT_INFO_HOW_TO_REMOVE_RELATIONS) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoHowToRemoveClass", Constants.IF_TXT_INFO_HOW_TO_REMOVE_CLASS) + "\n"
					+ "\n"
					
					+ " " + properties.getProperty("If_Txt_InstancesDiagram", Constants.IF_TXT_INSTANCES_DIAGRAM) + "\n"
					+ " " + properties.getProperty("If_Txt_CreateInstanceText", Constants.IF_TXT_CREATE_INSTANCE_TEXT) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoHowToCreateInstance", Constants.IF_TXT_INFO_HOW_TO_CREATE_INSTANCE) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoAboutDialogForReference", Constants.IF_TXT_INFO_ABOUT_DIALOG_FOR_REFERENCE) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoAboutAddedEdgeToDiagram", Constants.IF_TXT_INFO_ABOUT_ADDED_EDGE_TO_DIAGRAM) + "\n"
					+ "\n"
					+ " " + properties.getProperty("If_Txt_RemoveInstanceText", Constants.IF_TXT_REMOVE_INSTANCE_TEXT) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoHowToRemoveInstance", Constants.IF_TXT_INFO_HOW_TO_REMOVE_INSTANCE) + "\n"
					+ "- " + properties.getProperty("If_Txt_ShowContentMenu", Constants.IF_TXT_SHOW_CONTEXT_MENU) + "\n"
					+ "- " + properties.getProperty("If_Txt_DeleteInstanceByDelKey", Constants.IF_TXT_DELETE_INSTANCE_BY_DEL_KEY) + "\n"
					+ "\n"
					+ " " + properties.getProperty("If_Txt_CreateRelationsText", Constants.IF_TXT_CREATE_RELATIONS_TEXT) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoHowToCreateRelations", Constants.IF_TXT_INFO_HOW_TO_CREATE_RELATIONS) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoHowToFillVariable", Constants.IF_TXT_INFO_HOW_TO_FILL_VARIABLE) + "\n"
					+ "\n"
					+ " " + properties.getProperty("If_Txt_CallingMethodText", Constants.IF_TXT_CALLING_METHOD_TEXT) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoHowToCallMethod", Constants.IF_TXT_INFO_HOW_TO_CALL_METHOD) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoCallStaticMethodInContextMenuOverClass", Constants.IF_TXT_INFO_CALL_STATIC_METHOD_IN_CONTEXT_MENU_OVER_CLASS) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoCallMethodOverInstanceInId", Constants.IF_TXT_INFO_CALL_METHOD_OVER_INSTANCE_IN_ID) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoHowToCallMethod_2", Constants.IF_TXT_INFO_HOW_TO_CALL_METHOD_2) + "\n"
					+ "- " + properties.getProperty("If_Txt_CallMethodDialogForParameters", Constants.IF_TXT_CALL_METHOD_DIALOG_FOR_PARAMETERS) + "\n"					
					+ "- " + properties.getProperty("If_Txt_InfoOnlyFewSupportedDataTypesInMethodParameters", Constants.IF_TXT_INFO_ONLY_FEW_SUPPORTED_DATA_TYPES_IN_METHOD_PARAMETERS) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoAboutLoadingVarsFromClasses", Constants.IF_TXT_INFO_ABOUT_LOADING_VARS_FROM_CLASSES) + "\n"
					+ "\n"
					
					+ " " + properties.getProperty("If_Txt_CommandEditorText", Constants.IF_TXT_COMMAND_EDITOR_TEXT) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoAboutCommandEditor", Constants.IF_TXT_INFO_ABOUT_COMMAND_EDITOR) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoHowToPrintAllCommands", Constants.IF_TXT_INFO_HOW_TO_PRINT_ALL_COMMANDS) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoAboutListOfCommands", Constants.IF_TXT_INFO_ABOUT_LIST_OF_COMMANDS) + "\n"
					+ "\n"
					
					+ " " + properties.getProperty("If_Txt_SourceCodeEditorText", Constants.IF_TXT_SOURCE_CODE_EDITOR_TEXT) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoAboutSourceCodeEditor", Constants.IF_TXT_INFO_ABOUT_SOURCE_CODE_EDITOR) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoAboutFunctionsOfEditor", Constants.IF_TXT_INFO_ABOUT_FUNCTIONS_OF_EDITOR) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoAboutGenerateConstructor", Constants.IF_TXT_INFO_ABOUT_GENERATE_CONSTRUCTOR) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoAboutGenerateAccessMethods", Constants.IF_TXT_INFO_ABOUT_GENERATE_ACCESS_METHODS) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoAboutContentOfCodeEditor", Constants.IF_TXT_INFO_ABOUT_CONTENT_OF_CODE_EDITOR) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoHowToOpenCodeEditor", Constants.IF_TXT_INFO_HOW_TO_OPEN_CODE_EDITOR) + "\n"
					+ "- " + properties.getProperty("If_Txt_OpenDeferentFilesInProjectExplorerDialog", Constants.IF_TXT_OPEN_DIFERENT_FILES_IN_PROJECT_EXPLORER_DIALOG) + "\n"
					+ "\n"
					
					+ " " + properties.getProperty("If_Txt_ProjectExplorerText", Constants.IF_TXT_PROJECT_EXPLORER_TEXT) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoHowToOpenProjectExplorer", Constants.IF_TXT_INFO_HOW_TO_OPEN_PROJECT_EXPLORER) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoHowToOpenCodeEditor_2", Constants.IF_TXT_INFO_HOW_TO_OPEN_CODE_EDITOR_2) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoAboutOpennedWorkspaceDir", Constants.IF_TXT_INFO_ABOUT_OPENNED_WORKSPACE_DIR) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoAboutAccessedmenuField", Constants.IF_TXT_INFO_ABOUT_ACCESSED_MENU_FIELD) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoAboutOpenMoreKindOfFiles", Constants.IF_TXT_INFO_ABOUT_OPEN_MORE_KIND_OF_FILES) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoAboutWritingSourceCode", Constants.IF_TXT_INFO_ABOUT_WRITING_SOURCE_CODE) + "\n"
					+ "- " + properties.getProperty("If_Txt_InfoHowToOpenFile", Constants.IF_TXT_INFO_HOW_TO_OPEN_FILE) + "\n"
					+ "- " + properties.getProperty("If_Txt_HowToOpenOtherFile", Constants.IF_TXT_HOW_TO_OPEN_OTHER_FILE)
					+ "\n";
		}
		
		
		else {
			setTitle(Constants.IF_DIALOG_TITLE);						
			
			textWithInformation = Constants.IF_TXT_BASIC_INFO + ":\n\n"
					+ Constants.IF_TXT_CLASS_DIAGRAM + "\n"
					+ Constants.IF_TXT_CREATE_CLASS + "\n"				
					+ "- " + Constants.IF_TXT_INFO_HOW_TO_CREATE_CLASS + "\n"
					+ "- " + Constants.IF_TXT_INFO_HOW_TO_WRITE_CLASS_NAME + "\n"
					+ "- " + Constants.IF_TXT_INFO_FOR_CLASS_NAME_WITHOUT_PACKAGES + "\n"
					+ "- " + Constants.IF_TXT_CHOOSE_TEMPLATE_FOR_CLASS + "\n"
					+ "\n"
					+ " " + Constants.IF_TXT_ADD_RELATIONS + "\n"
					+ "- " + Constants.IF_TXT_INFO_FOR_CLICK_ON_BUTTON + "\n"
					+ "- " + Constants.IF_TXT_CHOOSE_CLASSES_BY_CLICK + "\n"
					+ "- " + Constants.IF_TXT_ADDED_SYNTAXE_TO_SOURCE_CODE + "\n"
					+ "- " + Constants.IF_TXT_INFO_FOR_CREATE_EDGE + "\n"
					+ "- " + Constants.IF_TXT_INFO_FOR_ADD_COMMENT + "\n"
					+ "\n"
					+ " " + Constants.IF_TXT_REMOVE_OBJECT_OR_RELATIONS + "\n"
					+ "- " + Constants.IF_TXT_INFO_HOW_TO_REMOVE_OBJECT + "\n"
					+ "- " + Constants.IF_TXT_INFO_FOR_REMOVE_COMMENT + "\n"
					+ "- " + Constants.IF_TXT_INFO_HOW_TO_REMOVE_RELATIONS + "\n"
					+ "- " + Constants.IF_TXT_INFO_HOW_TO_REMOVE_CLASS + "\n"
					+ "\n"
					
					+ " " + Constants.IF_TXT_INSTANCES_DIAGRAM + "\n"
					+ " " + Constants.IF_TXT_CREATE_INSTANCE_TEXT + "\n"
					+ "- " + Constants.IF_TXT_INFO_HOW_TO_CREATE_INSTANCE + "\n"
					+ "- " + Constants.IF_TXT_INFO_ABOUT_DIALOG_FOR_REFERENCE + "\n"
					+ "- " + Constants.IF_TXT_INFO_ABOUT_ADDED_EDGE_TO_DIAGRAM + "\n"
					+ "\n"
					+ " " + Constants.IF_TXT_REMOVE_INSTANCE_TEXT + "\n"
					+ "- " + Constants.IF_TXT_INFO_HOW_TO_REMOVE_INSTANCE + "\n"
					+ "- " + Constants.IF_TXT_SHOW_CONTEXT_MENU + "\n"
					+ "- " + Constants.IF_TXT_DELETE_INSTANCE_BY_DEL_KEY + "\n"
					+ "\n"
					+ " " + Constants.IF_TXT_CREATE_RELATIONS_TEXT + "\n"
					+ "- " + Constants.IF_TXT_INFO_HOW_TO_CREATE_RELATIONS + "\n"
					+ "- " + Constants.IF_TXT_INFO_HOW_TO_FILL_VARIABLE + "\n"
					+ "\n"
					+ " " + Constants.IF_TXT_CALLING_METHOD_TEXT + "\n"
					+ "- " + Constants.IF_TXT_INFO_HOW_TO_CALL_METHOD + "\n"
					+ "- " + Constants.IF_TXT_INFO_CALL_STATIC_METHOD_IN_CONTEXT_MENU_OVER_CLASS + "\n"
					+ "- " + Constants.IF_TXT_INFO_CALL_METHOD_OVER_INSTANCE_IN_ID + "\n"
					+ "- " + Constants.IF_TXT_INFO_HOW_TO_CALL_METHOD_2 + "\n"
					+ "- " + Constants.IF_TXT_CALL_METHOD_DIALOG_FOR_PARAMETERS + "\n"
					+ "- " + Constants.IF_TXT_INFO_ONLY_FEW_SUPPORTED_DATA_TYPES_IN_METHOD_PARAMETERS + "\n"
					+ "- " + Constants.IF_TXT_INFO_ABOUT_LOADING_VARS_FROM_CLASSES + "\n"
					+ "\n"
					
					+ " " + Constants.IF_TXT_COMMAND_EDITOR_TEXT + "\n"
					+ "- " + Constants.IF_TXT_INFO_ABOUT_COMMAND_EDITOR + "\n"
					+ "- " + Constants.IF_TXT_INFO_HOW_TO_PRINT_ALL_COMMANDS + "\n"
					+ "- " + Constants.IF_TXT_INFO_ABOUT_LIST_OF_COMMANDS + "\n"
					+ "\n"
					
					+ " " + Constants.IF_TXT_SOURCE_CODE_EDITOR_TEXT + "\n"
					+ "- " + Constants.IF_TXT_INFO_ABOUT_SOURCE_CODE_EDITOR + "\n"
					+ "- " + Constants.IF_TXT_INFO_ABOUT_FUNCTIONS_OF_EDITOR + "\n"
					+ "- " + Constants.IF_TXT_INFO_ABOUT_GENERATE_CONSTRUCTOR + "\n"
					+ "- " + Constants.IF_TXT_INFO_ABOUT_GENERATE_ACCESS_METHODS + "\n"
					+ "- " + Constants.IF_TXT_INFO_ABOUT_CONTENT_OF_CODE_EDITOR + "\n"
					+ "- " + Constants.IF_TXT_INFO_HOW_TO_OPEN_CODE_EDITOR + "\n"
					+ "- " + Constants.IF_TXT_OPEN_DIFERENT_FILES_IN_PROJECT_EXPLORER_DIALOG + "\n"
					+ "\n"
					
					+ " " + Constants.IF_TXT_PROJECT_EXPLORER_TEXT + "\n"
					+ "- " + Constants.IF_TXT_INFO_HOW_TO_OPEN_PROJECT_EXPLORER + "\n"
					+ "- " + Constants.IF_TXT_INFO_HOW_TO_OPEN_CODE_EDITOR_2 + "\n"
					+ "- " + Constants.IF_TXT_INFO_ABOUT_OPENNED_WORKSPACE_DIR + "\n"
					+ "- " + Constants.IF_TXT_INFO_ABOUT_ACCESSED_MENU_FIELD + "\n"
					+ "- " + Constants.IF_TXT_INFO_ABOUT_OPEN_MORE_KIND_OF_FILES + "\n"
					+ "- " + Constants.IF_TXT_INFO_ABOUT_WRITING_SOURCE_CODE + "\n"
					+ "- " + Constants.IF_TXT_INFO_HOW_TO_OPEN_FILE + "\n"
					+ "- " + Constants.IF_TXT_HOW_TO_OPEN_OTHER_FILE
					+ "\n";
		}
		
		
		// Nastavím text s informacemi ve zvoleném jazyce (popř. ve výchozím jazyce) do textArey:
		txtArea.setText(textWithInformation);
	}
}