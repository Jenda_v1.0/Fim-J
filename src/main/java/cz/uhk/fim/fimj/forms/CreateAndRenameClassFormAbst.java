package cz.uhk.fim.fimj.forms;

import java.util.Properties;

import javax.swing.JOptionPane;

import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;

import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.file.Constants;

/**
 * Tato abstraktní třída slouží jako "šablona" pro dialogy pro přejmenování a vytvoření nové třídy a i pro přidání třídy
 * ze souboru - tam se také testuje nové zadaný název třídy.
 * <p>
 * Oba dialogy obsahuje stejné metody pro otestování zdané třídy a k nim príslušné texty, takže i metoda pro naplnění
 * !některých! proměnných s texty do dialogu budou identické
 * <p>
 * Tato třída dálě dědí z také abstraktní třídy: SourceDialogForm, což je třída, ve které jsou definované základní
 * metody pro většinu dialogů s layoutem GridBagLayout - obsahuje metody pro jeho deklaraci prměnné GridBagConstraint a
 * některé proměnné, které dále v dialogu využívám například pro potvrzení variable, ...
 * <p>
 * <p>
 * Note:
 * Zde bych ještě mohl "sdílet" mezi zmíněnými třídami - dialogy metodu pro otestování zadané hodnoty - názvu
 * třídy, jenže v dialogu pro přejmenování třídy ještě testujy půvnodní název, takže bych musel testovat, jen některé
 * části této metody, navíc bych si to ještě trochu komplikoval s Jtextfieldem
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class CreateAndRenameClassFormAbst extends SourceDialogForm {

	private static final long serialVersionUID = 1L;

	
	


	
	/**
	 * Regulární výraz pro název třídy i s balíčky: (syntaxe: p1.p2... ClassName)
	 */
	private static final String REGEX_FOR_CLASS_NAME_WITH_PACKAGES = "^\\s*([A-Z]\\w*|\\w+\\s*(\\.\\s*\\w+\\s*)*)\\s*$";
	
	
	

	/**
	 * Regulární výrazpro rozpoznání, zda se jedná o název třídy ve správné syntaxi:
	 * Prní velké písmeno, pak pouze písmena bez diakritiky, číslice a podtržítka:
	 */
	static final String REG_EX_FOR_CLASS_NAME = "^[A-Z]\\w*$";
	
	
	
	
	
	
	/**
	 * Promenná, do které vložím text třídy pro kontrolu správnosti syntaxe, apod. a
	 * pro původní název třídy - kontrola identity a tento text pak vrátím pokud
	 * "projde kontrolou"
	 */
	protected String classNameVariable;
	
	
	
	
	
	// Textové proměnné, kterré jsou stejné pro oba dialogy:
	static String emptyClassNameText, emptyClassNameTitle, wrongNameOfClassNameText, wrongNameOfClassNameTitle,
			txtError;

	private String formatErrorText, formatErrorTitle, endClassNameErrorTitle, endClassNameErrorText, keyWordErrorText,
			keyWordErrorTitle, txtClassAlreadyExistInSamePackageText, txtClassAlreadyExistInSamePackageTitle;
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která naplní textové proměnné, které jsou stejné pro dialogy pro
	 * přejmenováíní a vytvoření třídy.
	 * 
	 * Abych nemusel stejné proěnné naplnit zvlášť v každém dialogu, tak na to zde
	 * napíšu metodu, která je naplnění, metoda lze zavolat z obou dialogů
	 * 
	 * @param languageProperties
	 *            - reference na načtený souibor Properties s jazykem zvolenmým
	 *            uživatelem
	 */
	void fillIdenticalyVariables(final Properties languageProperties) {
		if (languageProperties != null) {
			emptyClassNameText = languageProperties.getProperty("NcfErrorEmptyClassNameText", Constants.NCF_EMPTY_CLASS_NAME_TEXT);
			emptyClassNameTitle = languageProperties.getProperty("NcfErrorEmptyClassNameTitle", Constants.NCF_EMPTY_CLASS_NAME_TITLE);
			formatErrorText = languageProperties.getProperty("NcfErrorFormatText", Constants.NCF_FORMAT_ERROR_TEXT);
			formatErrorTitle = languageProperties.getProperty("NcfErrorFormatTitle", Constants.NCF_FORMAT_ERROR_TITLE);
			endClassNameErrorTitle = languageProperties.getProperty("NcfErrorClassNameTitle", Constants.NCF_END_CLASS_NAME_ERROR_TITLE);
			endClassNameErrorText = languageProperties.getProperty("NcfErrorClassNameText", Constants.NCF_END_CLASS_NAME_ERROR_TEXT);
			keyWordErrorText = languageProperties.getProperty("NcfErrorKeyWordInClassNameText", Constants.NCF_ERROR_KEY_WORD_TEXT);
			keyWordErrorTitle = languageProperties.getProperty("NcfErrorKeyWordInClassNameTitle", Constants.NCF_ERROR_KEY_WORD_TITLE);						
			wrongNameOfClassNameText = languageProperties.getProperty("Ncf_WrongNameOfClassText", Constants.NCF_WRONG_NAME_OF_CLASS_TEXT);
			wrongNameOfClassNameTitle = languageProperties.getProperty("NcfWrongNameOfClassTitle", Constants.NCF_WRONG_NAME_OF_CLASS_TITLE);
			txtError = languageProperties.getProperty("NcfTxtError", Constants.NCF_TXT_ERROR);
						
			txtClassAlreadyExistInSamePackageText = languageProperties.getProperty("Ncf_TextClassAlreadyExistInSamePackageText", Constants.NCF_TXT_CLASS_ALREADY_EXIST_IN_SAME_PACKAGE_TEXT);
			txtClassAlreadyExistInSamePackageTitle = languageProperties.getProperty("Ncf_TextClassAlreadyExistInSamePackageTitle", Constants.NCF_TXT_CLASS_ALREADY_EXIST_IN_SAME_PACKAGE_TITLE);
		}
		
		else {
			emptyClassNameText = Constants.NCF_EMPTY_CLASS_NAME_TEXT;
			emptyClassNameTitle = Constants.NCF_EMPTY_CLASS_NAME_TITLE;
			formatErrorText = Constants.NCF_FORMAT_ERROR_TEXT;
			formatErrorTitle = Constants.NCF_FORMAT_ERROR_TITLE;
			endClassNameErrorTitle = Constants.NCF_END_CLASS_NAME_ERROR_TITLE;
			endClassNameErrorText = Constants.NCF_END_CLASS_NAME_ERROR_TEXT;
			keyWordErrorText = Constants.NCF_ERROR_KEY_WORD_TEXT;
			keyWordErrorTitle = Constants.NCF_ERROR_KEY_WORD_TITLE;			
			wrongNameOfClassNameText = Constants.NCF_WRONG_NAME_OF_CLASS_TEXT;
			wrongNameOfClassNameTitle = Constants.NCF_WRONG_NAME_OF_CLASS_TITLE;
			txtError = Constants.NCF_TXT_ERROR;
			
			txtClassAlreadyExistInSamePackageText = Constants.NCF_TXT_CLASS_ALREADY_EXIST_IN_SAME_PACKAGE_TEXT;
			txtClassAlreadyExistInSamePackageTitle = Constants.NCF_TXT_CLASS_ALREADY_EXIST_IN_SAME_PACKAGE_TITLE;
		}
	}
	
	
	
	
	

	
	
	
	
	/**
	 * Metoda, která Otestuje zadaný text - coby název třídy i s balíčky a vrátí
	 * logickou hodnotu o tom, zda odpovídá povolené syntaxi Javy coby název
	 * Javovské třídy, případně se vypíše chybová hláška s informace o vzniklé chybě
	 * 
	 * @return true, pokud se jedná o povolený formát názvu třídy, jinak false
	 */
	final boolean isClassNameRight() {		
		if (classNameVariable.matches(REGEX_FOR_CLASS_NAME_WITH_PACKAGES)) {
			if (!classNameVariable.endsWith(".")) {
				// Otestuje, zda se název nerovná nějakému klíčovému slovu z Javy:
				// Jelikož se cesta k třídě - balíček dané třídy dále importuje do třídy, anim názvy balíčků
				// nemsí obsahovat klíčová slova!
				
				// Otestuje název třídy na klíčová slova:
				final String[] partsOfClassName = classNameVariable.split("\\.");
				
				String variableKeyWord = "";
				
				for (final String keyWord : Constants.getListOfKeyWords()) {
					for (final String packageName : partsOfClassName) {
						if (keyWord.equalsIgnoreCase(packageName)) {
							variableKeyWord = keyWord;
							break;
						}
					}
				}
				
				// Pokud název třídy není klíčové slovo, název se akceptuje, jinak se to oznámí uživateli
				if (variableKeyWord.equals("")) {
					// Zde ještě otestuji, zda je název třídy ve správné syntaxi, tj. první velké písmeno, pak na velikost nezalezi a jsou 
					// v něm pouze číslice a písmena bez diakgritika a podtržítko
					
					final String className = partsOfClassName[partsOfClassName.length - 1];
					if (className.matches(REG_EX_FOR_CLASS_NAME)) {
						
						// Zde je všechono s třídou v pořádku, akorát musím ještě zkontrolovat, zda se již zadaný název třídy
						// v grafu nevyskytuje:
						
						// takže prjedu všechny prvky grafu, a pokud se nenedná o hranu, tak si vemu její text a otestuje, zda se neshodují názvy tříd:
						
						// Pokud název neobsahuje balíčky, tak k němu přidám výchozí:
						if (!classNameVariable.contains("."))
							classNameVariable = Constants.DEFAULT_PACKAGE + "." + classNameVariable;
						
						
						boolean isClassNameInClassDiagram = false;
						
						for (final DefaultGraphCell d : GraphClass.getCellsList()) {
							
							// Podmínka je: aby to nebyla hrana a komentář, neboli byla to třída:
							if (!(d instanceof DefaultEdge) && GraphClass.KIND_OF_EDGE.isClassCell(d) && d.toString()
									.equals(classNameVariable))
								isClassNameInClassDiagram = true;
						}
						
						if (!isClassNameInClassDiagram)
							return true;
						
						else
							JOptionPane.showMessageDialog(this, txtClassAlreadyExistInSamePackageText,
									txtClassAlreadyExistInSamePackageTitle, JOptionPane.ERROR_MESSAGE);					
					}
					else
						JOptionPane.showMessageDialog(this,
								wrongNameOfClassNameText + "\n" + txtError + ": '" + className + "'",
								wrongNameOfClassNameTitle, JOptionPane.ERROR_MESSAGE);
				}
				else
					JOptionPane.showMessageDialog(this, keyWordErrorText + " != " + variableKeyWord + " !",
							keyWordErrorTitle, JOptionPane.ERROR_MESSAGE);
			}
			else JOptionPane.showMessageDialog(this, endClassNameErrorText, endClassNameErrorTitle, JOptionPane.ERROR_MESSAGE);
		}
		else JOptionPane.showMessageDialog(this, formatErrorText, formatErrorTitle, JOptionPane.ERROR_MESSAGE);
		
		return false;
	}
}