package cz.uhk.fim.fimj.forms;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.lang.reflect.Constructor;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;
import cz.uhk.fim.fimj.instances.Instances;
import cz.uhk.fim.fimj.instances.OperationsWithInstances;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.output_editor.OutputEditor;
import cz.uhk.fim.fimj.parameters_for_method.ClassOrInstanceForParameter;
import cz.uhk.fim.fimj.parameters_for_method.FieldsInfo;
import cz.uhk.fim.fimj.parameters_for_method.FlexibleCountOfParameters;
import cz.uhk.fim.fimj.parameters_for_method.KindOfVariable;
import cz.uhk.fim.fimj.parameters_for_method.ParameterValue;

/**
 * Tato třída slouží jako dialog pro vytvoření nové instance označené třídy
 * <p>
 * Zadávají se sem parametry konstrktoru - pokud jsou a název proměnné, která bude instanci zvolené třídy reprezentovat
 * - "bude na ní ukazovat"
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class NewInstanceForm extends FlexibleCountOfParameters implements LanguageInterface, ActionListener {

	private static final long serialVersionUID = 1L;

	
	/**
	 * Předdefinované - výchozí velikost okna:
	 */
	private static final int FRAME_WIDTH = 700, FRAME_HEIGHT = 675;
	
	
	
	
	private final JLabel lblReferenceName;
	
	private final JTextField txtClassNameVariable;
	
	
	
	
	
	// Proměnné pro texty do chybových hlášek:
	private static String fieldIsEmptyTitle, fieldIsEmptyText, parameterFormatTitle, parameterFormatText,
			referenceNameIsAlreadyTakenTitle, referenceNameIsAlreadyTakenText, formatErrorOfReferenceNameTitle,
			formatErrorOfReferenceNameText, emptyFieldErrorTitle, emptyFieldErrorText, errorTextForReferenceName,
			cannotCallTheConstructorTitle;
	
	
	
	
	
	
	
	/**
	 * List názvů proměnných - instancí třídy, potřebuji je, abych mohl otestovat,
	 * zda zadaný název ještě nebyl použit
	 */
	private final List<String> listOfVariables;
	
	
	
	/**
	 * Parametry konstukroru, abych předal spravné typy hodnot parametrů
	 */
	private final transient Parameter[] parametersOfConstructor;
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param constructor
	 *            - konstruktor pro zavolání.
	 * 
	 * @param languageProperties
	 *            - objekt s texty pro tuto aplikace ve zvoleném jazyce.
	 * 
	 * @param outputEditor
	 *            - editor výstupů pro vypsání výsledku ohledně zavolání
	 *            konstruktoru, například došlo k nějaké chybě, neo Úspěch apod.
	 * 
	 * @param makeVariablesAvailable
	 *            - Zda se mají zpřístupnit veřejné a chráněné proměnné z tříd v
	 *            diagramu tříd, instancí v diagramu instancí a proměnných
	 *            vytvořených v editoru příkazů pro předání do parametru
	 *            konstruktoru.
	 * 
	 * @param showCreateNewInstanceOptionInCallConstructor
	 *            - logická proměnná, která značí, zda se má zpřístupnit možnost pro
	 *            vytvoření nové instance příslušné třídy. Tato možnost je potřeba v
	 *            případě, že se v příslušné konstruktoru constructor nachází
	 *            parametr, který je typu nějakké třídy z diagramu tříd, pokud tomu
	 *            tak je, pak pokud bude tato hodnota true, tak se zpřístupní
	 *            možnost pro vytvoření i nové instance, ale pokud bude tato hodnota
	 *            false, pak se bude moci vybírat pouze mezi aktuálními instancemi,
	 *            které se nachází v diagramu instancí a pokud v tomto diagramu
	 *            žádná instance nebude, pak příslušnáý konstruktor ani nepůjde
	 *            zavolat.
	 * 
	 * @param instanceDiagram
	 *            - reference na diagram instancí, aby jej bylo možné předat do
	 *            objektu pro zavolání metod, kdyby se mělo jednat o předání
	 *            návratového hodnoty metody do parametru konstruktoru.
	 * 
	 * @param clazz
	 *            - třída, ve které se má zavolat konstruktor. Jde zde tato třída
	 *            potřeba, protože potřebuji zjistit některé chráněné metody, které
	 *            se mají aplikovat, resp. které je možné zavolat.
	 * 
	 * @param makeMethodsAvailable
	 *            - logická proměnná, která značí, zda se mají zpřístupnit metody
	 *            pro předání jejich návratové hodnoty do příslušného parametru
	 *            konstruktoru.
	 */
	public NewInstanceForm(final Constructor<?> constructor, final Properties languageProperties,
			final OutputEditor outputEditor, final boolean makeVariablesAvailable,
			final boolean showCreateNewInstanceOptionInCallConstructor, final GraphInstance instanceDiagram,
			final Class<?> clazz, final boolean makeMethodsAvailable) {
		
		super(makeVariablesAvailable, showCreateNewInstanceOptionInCallConstructor, makeMethodsAvailable);
		
		// do následující proměnné si vložím názvy referencí na instance, abych mohl
		// otestovat, zde se nové zadaná instance již a pliakaci nevyskytuje:
		this.listOfVariables = Instances.getListOfVariablesOfClassInstances();
		
		
		// Naplnění reference na editoru výstupů, abych mohl oznámit uživateli, že došlo
		// k chybě při vytvíření instance při předávíní v parametru:
		this.outputEditor = outputEditor;
		
		FlexibleCountOfParameters.instanceDiagram = instanceDiagram;

		operationWithInstances = new OperationsWithInstances(outputEditor, languageProperties, instanceDiagram);
		
		
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/app/NewInstanceIcon.png",
				new Dimension(FRAME_WIDTH, FRAME_HEIGHT), true);

		// Nejmenší povolená velikost okna:
		setMinimumSize(new Dimension(650, 580));

		
		
		// Vytvoření instance a nastavení parametrů pro komponenttu gbc
		// pomocí které rozmístím komponenty v okně - dialogu
		gbc = createGbc();
		
		index = 0;
		
		
		
		// Instance listu pro komponenty pro zadání hodnot parametrů
		listOfParametersValues = new ArrayList<>();
		
		//instance kolekce pro labely s chybovým oznámením o chybně zadané hodnotě parametru
		errorTextList = new ArrayList<>();
		
		// intance listu pro labely s informacemi o hodnote parametru:
		labelsList = new ArrayList<>();
		
		

		
		
		
		
		// Komponenty do okna:
		gbc.gridwidth = 2;
		
		lblInfoText = new JLabel();
		setGbc(gbc, index, 0, lblInfoText);
		
		gbc.gridwidth = 1;
		
		
		// Přidání ostatních hodnot do okna:
		
		// Získám si název třídy z konstruktoru:
		final String className = constructor.getName().substring(constructor.getName().lastIndexOf('.') + 1);
		
		// První písmeno bude vždy malě, zbytek nachám:
		final String firstClassName = Character.toLowerCase(className.charAt(0)) + className.substring(1);
		
		
		// Nastavím si index pro název reference, aby se nepletl název s jiným, již použitý:
		long instanceIndex = 1;
		// Nastavíme název reference:
		String instanceNameForUse = firstClassName + instanceIndex;
		
		
		// Následující cyklus bude inkrementovat instanci, dokud bude název instance již použit,
		// až se najde název, instnce (reference), který ještě není pooužit, tak cyklus skončí a
		// zísaný název reference se nastaví na použití:
        while (listOfVariables.contains(instanceNameForUse))
            // Zde je již název reference použit, tak zvýším index:
            instanceNameForUse = instanceNameForUse.substring(0,
                    instanceNameForUse.length() - String.valueOf(instanceIndex).length()) + ++instanceIndex;
		
		lblReferenceName = new JLabel();
		setGbc(gbc, ++index, 0, lblReferenceName);
		txtClassNameVariable = new JTextField(instanceNameForUse, 20);
		// Označení textu:
		txtClassNameVariable.selectAll();
		
		addMyKeyListener(1, txtClassNameVariable);
		setGbc(gbc, index, 1, txtClassNameVariable);
		
		
		
		
		gbc.gridwidth = 2;
		
		
		
		
		// Do jednorozměného pole si načtu parametry označeného kontruktoru
		// Dle počtu a typu do dialogu připravím příslušná pole pro zadání hodnot
		parametersOfConstructor = constructor.getParameters();
		
		
		
		
		
		
		
		
		
		
		// Uložím si text konstruktoru (konstruktor v textu), abych jej mohl při změně
		// velikosti okna nastavit nové rozměrny textu pro zaloomení.
		textWithParameters = getParametersOfMethod(constructor.getName(), parametersOfConstructor);

		/*
		 * Vytvořím si label, do kterého se vloží text s konstruktorem tak, aby byly
		 * správně zobrazeny potřebné parametry apod.
		 */
		final JLabel lblParametersInfo = new JLabel(getWrappedText(textWithParameters, FRAME_WIDTH));

		// Přidání labelu do okna dialogu:
		setGbc(gbc, ++index, 0, lblParametersInfo);
		
		/*
		 * Při změně velikosti okna dialogu potřebuji přenačíst i velikost textu, resp.
		 * šířku, u které se má zalamovat.
		 */
		addComponentListener(new ComponentAdapter() {

			@Override
			public void componentResized(ComponentEvent e) {
				lblParametersInfo.setText(getWrappedText(textWithParameters, getWidth()));
			}
		});

		

		
		
		
		
		
		
		// Zde musím naplnit proměnné, zekterých se budou získávat texty do hlášek v Jlabelech u parametruů
		fillTextVariables(languageProperties);
		
		
		

		
		
		
		
		
		
		
		
		
		/*
		 * Note:
		 * 
		 * Následujících několik metod bych mohl dát aké do nějaké metody, vlastně bych to vše mohl dát do nějaké metody,
		 * která by vrátila až objekty do listu: sortedAvailableFields. ale nechám to zde trochu "přehledněji", kdybych to později
		 * potřeboval ještě nějak upravit, abych měl "oddělené" získání jednotlivých hodnot.
		 */
		
		
		/*
		 * Získám si Veškeré veřejné a chráněné proměnné z vytvořených instancí, které
		 * se nachází v diagramu instancí a všechny statické veřejné a chráněné proměnné
		 * z tříd v diagramu tříd.
		 */
		final List<FieldsInfo> fieldsInfoList = getAvailableFieldsFromClassesAndInstances(outputEditor);
		
		
		/*
		 * Vytvořím si list, do kterého postupně vložím takové objekty, které je bude
		 * možné vložit do komponenty JCombobox v tomto dialogu, aby uživatel mohl
		 * zvolit hodnotu, kterou chce předat do parametru příslušné metody, kterou chce
		 * zavolat.
		 */
		final List<ParameterValue> valuesForCmbList = new ArrayList<>();

		
		/*
		 * Vložím si do listu valuesForCmbList výše získané proměnné z tříd a instanci
		 * tříd.
		 */
		fieldsInfoList.forEach(v -> valuesForCmbList
				.addAll(getParameterValuesFromFields(v.getFieldsList(), v.getReferenceName(), v.getObjInstance())));

		
		
		/*
		 * Do listu valuesForCmbList dále vložím veškeré proměnné, které uživatel
		 * vytvořil pomocí editoru příkazů.
		 */
		getCommandsEditorVariables(valuesForCmbList);
		

		
		
		
		/*
		 * List, do kterého si vložím vekšeré získané proměnné (z editoru příkazů, s
		 * tatické proměnné z tříd v diagramu tříd a proměnné z instancí tříd v diagramu
		 * instancí).
		 * 
		 * Tyto proměnné budou hlavně seřazené, nejprve proměnné vytvořené v editoru
		 * příkazů, pak statické proměnné z tříd v diagramu tříd a pak proměnné, které
		 * se nachází v nějaké instanci, na tyto proměnné budou uvedeny i reference.
		 */
		final List<ParameterValue> sortedAvailableFields = getSortedAvailableFields(valuesForCmbList);
		
		
		
		
		
		
		
		
		
		
		// Podle počtu a typu parametrů si vytvořím potřebný počet položek v dialogu:
		
		
		// načtu si list zkompilovaných tříd z diagramu tříd: (načtené soubory .class) a všechny instance vytvořené uživatelem:		
		final List<ClassOrInstanceForParameter> classOrInstancesList = getClassOrInstanceList(Instances.getAllInstances());
		
		
		
		final JPanel pnlValues = addFieldForValuesToDialog(parametersOfConstructor, true, classOrInstancesList,
				sortedAvailableFields, null, this, clazz, null, null);
		
		
		final JScrollPane jspValues = new JScrollPane(pnlValues);
		jspValues.setPreferredSize(new Dimension(0, 200));
		
		gbc.gridwidth = 2;
		setGbc(gbc, ++index, 0, jspValues);
		
		
		
		
		setGbc(gbc, ++index, 0, getCreatedErrorList());
		
		
		// Zde je již list pro chybové zprávy vytvořen, tak mohu vkládat chybová upozornění,
		// tak otestuji, zda jsou všechny datové typy zadaných parametrů podporovány, pokud ne, tak to ve zmíněním listu
		// oznámím:
		if (errorTextForBadTypeOfParameter != null)
			addErrorToFieldOfDialog(null, errorTextForBadTypeOfParameter);
		
		
		
		
		// Přidání tlačítek:
		final JPanel pnlButtons = new JPanel();
		pnlButtons.setLayout(new FlowLayout(FlowLayout.RIGHT));
		btnOk = new JButton();
		btnCancel = new JButton();
		btnOk.addActionListener(this);
		btnCancel.addActionListener(this);
		pnlButtons.add(btnOk);
		pnlButtons.add(btnCancel);				
		
		setGbc(gbc, ++index, 0, pnlButtons);
		
		
		
		
		
		pack();
		setLocationRelativeTo(null);
		
				
		
		// Zde je možné nastavit texty do celé aplikace:
		setLanguage(languageProperties);
		
		

		
		addWindowListener(getWindowsAdapter());
	}





	
	
	
	/**
	 * Metoda, která na události na tlačítka přidá reakci na klávesu enter:
	 * po stisknutí klávesy se provede kontrola dat dialog a bud se vypise hlaska nebo
	 * se dokonci metoda getValuesForNewInstance
	 */
	private void addMyKeyListener(final int orderTextField, final JTextField txtField) {
		txtField.addKeyListener(new KeyAdapter() {
			
			@Override
			public void keyReleased(KeyEvent e) {
				if (!txtField.getText().trim().isEmpty()) {
					final String textToErrorList = orderTextField + ". " + txtFieldContainError + " " +
							errorTextForReferenceName;

					if (txtField.getText().trim().matches(REG_EX_FOR_REFERENCE)) {
						try {
							String.valueOf(txtField.getText().trim());
							
							removeErrorFromFieldOfDialog(txtField, textToErrorList);
							
						} catch (Exception e2) {
							addErrorToFieldOfDialog(txtField, textToErrorList);

							/*
							 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
							 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
							 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
							 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
							 */
							final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

							// Otestuji, zda se podařilo načíst logger:
							if (logger != null) {
								// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

								// Nejprve mou informaci o chybě:
								logger.log(Level.INFO,
										"Zachycena výjimka při parsování textu z textového pole na datový typ String, " +
												"třída NewInstanceForm.java, metoda - po puštění"
												+ " libovolné klávesy v události na pole pro zadání reference!");

								/*
								 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
								 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
								 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
								 */
								logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e2));
							}
							/*
							 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
							 */
							ExceptionLogger.closeFileHandler();
						}						
					}
					else addErrorToFieldOfDialog(txtField, textToErrorList);
				}
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					// Provede se kontrola dat:
					testData();
				
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					variable = false;
					dispose();
				}
			}
		});
	}
	
	
	
	
	




	
	
	
	
	
	/**
	 * Metoda, která zviditelní dialog a vyzve uživatele k zadání údajů pro
	 * vytvoření nové instance označené třídy
	 * 
	 * @return null nebo kolekcis informacemi zadaných do tohoto dialogu informace
	 *         jako název proměnné ukazující na danou instanci třídy pokud
	 *         konstruktor obsahuje parametry, tak i je
	 * 
	 *         Na prvním indexu bude vždy proměnná coby reference na instanci třídy
	 *         V následujících indexech - hodnotách v kolekci budou zbyvající
	 *         hodnoty coby parametry konstruktoru
	 */
	public final List<Object> getValuesForNewInstance(final String className) {
		setTitle(getTitle() + ": " + className);
		
		setVisible(true);
		
		// Vytvořím si list objektů, do kterého budu vkládat jednotlivé hodnoty získané od uživatele z tohoto dialogu:
		final List<Object> objList = new ArrayList<>();
				
		if (variable) {
			// Uživatel klikl na OK:
						
			// Na první místo vložím proměnnou coby referene na instanci třídy:
			objList.add(txtClassNameVariable.getText().trim().replaceAll("\\s", ""));
			
			
			// Následující index, potřebuji, abych mohl získávat typy parametrů z jednorozměrného pole
			int index = 0;								
			
			
			for (final JComponent c : listOfParametersValues)
				objList.add(getWrittenParameter(c, parametersOfConstructor[index++]));
			
			return objList;
		}
		return objList;
	}

	
	
	
	
	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			setTitle(properties.getProperty("Nif_DialogTitle", Constants.NIF_DIALOG_TITLE));
			
			lblInfoText.setText(properties.getProperty("Nif_LabelInfoText", Constants.NIF_LBL_INFO_TEXT));
			lblReferenceName.setText(properties.getProperty("Nif_LabelReferenceName", Constants.NIF_LBL_REFERENCE_NAME) + ": ");
			
			
			// Tyto hodnoty potřebuji načíst zde, protože kdybych to udělal v metodě výše, tak některé labely
			// ještě nebudou vytvořeny
			final String textTolabel = properties.getProperty("Fcop_LabelForParameter", Constants.FCOP_LBL_TEXT_INFO_PARAM);
			
			labelsList.forEach(label -> {
				final String variableText = label.getText();
				label.setText(textTolabel + " " + variableText);
			});	

			
			btnOk.setText(properties.getProperty("Nif_Button_OK", Constants.NIF_BTN_OK));
			btnCancel.setText(properties.getProperty("Nif_Button_Cancel", Constants.NIF_BTN_CANCEL));		
			
			// Texty do hlášek:
			fieldIsEmptyTitle = properties.getProperty("Nif_SomeFieldIsEmptyTitle", Constants.NIF_FIELD_IS_EMPTY_ERROR_TITLE);
			fieldIsEmptyText = properties.getProperty("Nif_SomeFieldIsEmptyText", Constants.NIF_FIELD_IS_EMPTY_ERROR_TEXT);
			parameterFormatTitle = properties.getProperty("Nif_ConstructorParametrFormatErrorTitle", Constants.NIF_PARAMETER_FORMAT_ERROR_TITLE);
			parameterFormatText = properties.getProperty("Nif_ConstructorParametrFormatErrorText", Constants.NIF_PARAMETER_FORMAT_ERROR_TEXT);
			referenceNameIsAlreadyTakenTitle = properties.getProperty("Nif_NameOfReferenceIsAlreadyTakenTitle", Constants.NIF_NAME_OF_REFERENCE_IS_ALREADY_TAKEN_TITLE);
			referenceNameIsAlreadyTakenText = properties.getProperty("Nif_NameOfReferenceIsAlreadyTakenText", Constants.NIF_NAME_OF_REFERENCE_IS_ALREADY_TAKEN_TEXT);
			formatErrorOfReferenceNameTitle = properties.getProperty("Nif_ReferenceNameFormatErrorTitle", Constants.NIF_FORMAT_ERROR_REFERENCE_NAME_TITLE);
			formatErrorOfReferenceNameText = properties.getProperty("Nif_ReferenceNameFormatErrorText", Constants.NIF_FORMAT_ERROR_REFERENCE_NAME_TEXT);
			emptyFieldErrorTitle = properties.getProperty("Nif_ReferenceFieldIsEmptyTitle", Constants.NIF_EMTY_FIELD_ERROR_TITLE);
			emptyFieldErrorText = properties.getProperty("Nif_ReferenceFieldIsEmptyText", Constants.NIF_EMPTY_FIELD_ERROR_TEXT);					
			errorTextForReferenceName = properties.getProperty("Nif_BadFormatReferenceNameText", Constants.NIF_BAD_FORMAT_REFERENCE_NAME);
			cannotCallTheConstructorTitle = properties.getProperty("Nif_CannotCallTheConstructorTitle", Constants.NIF_CANNOT_CALL_THE_CONSTRUCTOR_TITLE);
		}
		
		
		else {
			setTitle(Constants.NIF_DIALOG_TITLE);
			
			lblInfoText.setText(Constants.NIF_LBL_INFO_TEXT);
			lblReferenceName.setText(Constants.NIF_LBL_REFERENCE_NAME + ": ");
			
			
			final String textTolabel = Constants.FCOP_LBL_TEXT_INFO_PARAM;
			
			labelsList.forEach(label -> {
				final String variableText = label.getText();
				label.setText(textTolabel + " " + variableText);
			});	

			
			btnOk.setText(Constants.NIF_BTN_OK);
			btnCancel.setText(Constants.NIF_BTN_CANCEL);	
			
			// Texty do hlášek:
			fieldIsEmptyTitle = Constants.NIF_FIELD_IS_EMPTY_ERROR_TITLE;
			fieldIsEmptyText = Constants.NIF_FIELD_IS_EMPTY_ERROR_TEXT;
			parameterFormatTitle = Constants.NIF_PARAMETER_FORMAT_ERROR_TITLE;
			parameterFormatText = Constants.NIF_PARAMETER_FORMAT_ERROR_TEXT;
			referenceNameIsAlreadyTakenTitle = Constants.NIF_NAME_OF_REFERENCE_IS_ALREADY_TAKEN_TITLE;
			referenceNameIsAlreadyTakenText = Constants.NIF_NAME_OF_REFERENCE_IS_ALREADY_TAKEN_TEXT;
			formatErrorOfReferenceNameTitle = Constants.NIF_FORMAT_ERROR_REFERENCE_NAME_TITLE;
			formatErrorOfReferenceNameText = Constants.NIF_FORMAT_ERROR_REFERENCE_NAME_TEXT;
			emptyFieldErrorTitle = Constants.NIF_EMTY_FIELD_ERROR_TITLE;
			emptyFieldErrorText = Constants.NIF_EMPTY_FIELD_ERROR_TEXT;						
			errorTextForReferenceName = Constants.NIF_BAD_FORMAT_REFERENCE_NAME;
			cannotCallTheConstructorTitle = Constants.NIF_CANNOT_CALL_THE_CONSTRUCTOR_TITLE;
		}
	}





	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnOk)
				testData();
			
			
			else if (e.getSource() == btnCancel) {
				variable = false;
				dispose();
			}
		}
	} 
	
	
	
	
	
	
	/**
	 * Metoda, jejíž obsah by mohl být pouze po kliknutí na tlačítko OK, ale to samé
	 * se provede i po sisknutí klávesy enter v některém z polí:
	 * 
	 * Postup:
	 * Otetuji, zda není nějaké ple prázdné a obsahuje alespoň základní - nějakou
	 * hodnotu pro parametr Pak otestuji název reference - zda ještě nebyl použit
	 * pak, zda ještě není zobrazen nějaký popisek s upozornením na chybový formát
	 * hodnoty parametru a když jsou tyto podmínky splněny, mohu do proměnné
	 * variabel dát true, a vrátit tak zadané hodnoty z tohoto dialogu
	 */
	public final void testData() {
		if (!txtClassNameVariable.getText().replaceAll("\\s", "").isEmpty()) {
			boolean isEmptyVariableOrBadFormat = false;

			int indexOfField = 1;

			// V následujícím cyklu zjistím, zda jsou všechna pole vyplnění a zadaná ve
			// "nějakém formát" parametru - zde jsou potlačeny bíle znaky,apod.
			for (final JComponent c : listOfParametersValues) {
				if (c instanceof JTextField) {
					// Otestuji, zda pole není prázdné:
					if (((JTextField) c).getText().trim().isEmpty()) {
						JOptionPane.showMessageDialog(this, indexOfField + ". " + fieldIsEmptyText, fieldIsEmptyTitle,
								JOptionPane.ERROR_MESSAGE);
						isEmptyVariableOrBadFormat = true;
						break;
					}
					// Dále otestuji, zda obsahuje elespoň nějaký znaky - aby neobsahovalo jen bíle znaky, apod.
					// Konkrétní hodnota je testována při zadávání - což poznám dle zviditelněného labelu -viz dále
					
					if (!((JTextField) c).getText().trim().matches(REG_EX_FOR_PARAMEER)) {
						JOptionPane.showMessageDialog(this, indexOfField + ". " + parameterFormatText,
								parameterFormatTitle, JOptionPane.ERROR_MESSAGE);
						isEmptyVariableOrBadFormat = true;
						break;
					}
				}
				
				/*
				 * Note:
				 * 
				 * Zde bych ještě mohl testovat, zda se jedná o instanci komponenty JComboBox,
				 * ale u té mám "zajištěné", že tam vždy nějaká hodnota bude, jinak bude při
				 * nejmenším vypsána chyba.
				 * 
				 * Pokud půjde o JcomboBox typu Boolean, pak tam je vždy na výběr jen true a
				 * false a jedno z toho vždy bude označeno, nebo si může uživatel vybrat
				 * instanci nějaké třídy, případně vytvořit novou v tomto typu JComboBoxu také
				 * vždy bude označena nějaká možnost a pak je tu třetí typ JComboBoxu, kde jsou
				 * na výběr hodnoty z proměnných a první položka v tomto JComboBoxu je pro
				 * zadání hodnoty od uživatele, ale tam je to pohlídané, že vždy něco bude
				 * zadané, ale pro jistotu, abych náhodou nedošlo k nějaké chybě zde bude
				 * testována pouze tato jedna hodnota - ta první položka, nikde jinde by k chybě
				 * dojít nemělo.
				 */
				else if (c instanceof JComboBox<?>) {
					// V této podmínce pro cmb bylo potřeba využít ? pro generické typy, nemohl jsem
					// zadat parametr, tak kvůli tomu musím napsat více podmínek:

					// Zda je označena první položka, jen první lze editovat, jiná ne.
					if (((JComboBox<?>) c).getSelectedIndex() != 0)
						continue;

					/*
					 * Získám si konkrétní hodnotu z cmb:
					 */
					final Object objValue = ((JComboBox<?>) c).getSelectedItem();

					// Zde otestuji, zda je označena hodnota typu pro zadání hodnoty:
					if (objValue instanceof ParameterValue) {
						final ParameterValue pValue = (ParameterValue) objValue;

						if (pValue.getKindOfVariable().equals(KindOfVariable.TEXT_VARIABLE)) {
							/*
							 * Tato podmínka můžen nastat, ale u objektových datový typů, kde to bude
							 * nastaveno úmyslně nebo jako výchozí hodnota, Jelikož v těchto případěch
							 * nevím, čím jinám tuto null hodnotu nahradit, nebo jak to obejít, tak zde
							 * výjimečně povolám předánínull hodnoty. Je to asi lepší, než kdybych uživatele
							 * pořád "nutil" mít v každé proměnné nějakou hodnotu.
							 */
							if (pValue.getTextValue() == null)
								continue;

							if (pValue.getTextValue().trim().isEmpty()) {
								JOptionPane.showMessageDialog(this, indexOfField + ". " + fieldIsEmptyText, fieldIsEmptyTitle,
										JOptionPane.ERROR_MESSAGE);
								isEmptyVariableOrBadFormat = true;
								break;
							}

							// Otestuji, zda je v něm zadaná alespoň nějaká hodnota:
							if (!pValue.getTextValue().trim().matches(REG_EX_FOR_PARAMEER)) {
								JOptionPane.showMessageDialog(this, indexOfField + ". " + parameterFormatText,
										parameterFormatTitle, JOptionPane.ERROR_MESSAGE);
								isEmptyVariableOrBadFormat = true;
								break;
							}
						}
					}
				}
				
				indexOfField++;
			}
			
			
			if (!isEmptyVariableOrBadFormat) {
				if (txtClassNameVariable.getText().trim().matches(REG_EX_FOR_REFERENCE)) {
					
					
					if (!isInstanceNameUsed(txtClassNameVariable.getText().trim())) {												
						// Zde je vše ve "správném formátu" - coby zadaného parametru, žádné pole není prázdné - nebo neobsahuje pouze mezery
						// nebo nějaké bíle znaky, apod (viz regulární výrazy), tak ještě otestuji, zda
						// se nějaký popisek není zobrazen - pokud ano, tak nějaký parametr má sice zadanou hodnotu, ale pro danou konkrétní
						// hodnotu je ve špatném formátu, napři pro celé číslo, apod:							
												
						
						if (errorTextList.isEmpty()) {
							// Zde žádný popisek není zobrazen, tak mohu vrátit hodnoty z tohoto dialogu:
							variable = true;
							dispose();
						}
						
						// JList s chybovými hláškami není prázdný, tak nejprve otestuje, zda lze vůbec konstruktor zavolat:
						else if (errorTextForBadTypeOfParameter != null)
							JOptionPane.showMessageDialog(this, errorTextForBadTypeOfParameter,
									cannotCallTheConstructorTitle, JOptionPane.ERROR_MESSAGE);
						
						// Zde jsou podoporovány všechny datové typy, akorát jsou nějaké zadány ve špatném formátu, tak to poouze oznámím užvateli:
						else viewIncorrectParameters();
					}
					else
						JOptionPane.showMessageDialog(this, referenceNameIsAlreadyTakenText,
								referenceNameIsAlreadyTakenTitle, JOptionPane.ERROR_MESSAGE);
				}
				else
					JOptionPane.showMessageDialog(this, formatErrorOfReferenceNameText, formatErrorOfReferenceNameTitle,
							JOptionPane.ERROR_MESSAGE);
			}
			// Zde else být nemusí, potřebné hlášky byly vypsány před ukončením cyklu
		}
		else
			JOptionPane.showMessageDialog(this, emptyFieldErrorText, emptyFieldErrorTitle, JOptionPane.ERROR_MESSAGE);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda je již zadaný název reference použit nebo ne.
	 * 
	 * @param name
	 *            - název instance
	 * 
	 * @return true, pokud je název použit, jinak false, pokud ještě není název
	 *         instance použit
	 */
	private boolean isInstanceNameUsed(final String name) {
		for (final String s : listOfVariables) {
			if (s.equalsIgnoreCase(name)) {
				return true;
			}
		}		
		return false;
	}
}