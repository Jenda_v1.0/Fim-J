package cz.uhk.fim.fimj.forms;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cz.uhk.fim.fimj.app.DetectOS;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.FileChooser;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.project_explorer.NameFormAbstract;

/**
 * Tato třída - dialog slouži pro přidání třídy ze souboru Dialog obsahuje pole pro výběr cesty k souboru a pole pro
 * zadání - změnu názvu třídy - kvůl balíčkům a umístění a adresáři projektu této aplikace
 * <p>
 * asmozřejmně tlačítka pro zvolení cesty, OK, a zrušení:
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ClassFromFileForm extends CreateAndRenameClassFormAbst implements ActionListener, LanguageInterface {

	private static final long serialVersionUID = 1L;


	
	/**
	 * Regulární výraz pro cestu k souboru - javovské třídě:
	 * 
	 * například:
	 * C:\Users\krunc\Desktop\BC_Workspace\Test\src\defaultPackage\ClassNameTest.java
	 * 
	 * Povolené jsou veškeré znaky kromě: ^, /, |, *, %, $, <, >, ?, :, ", =
	 */
	private static final String PATTERN_FOR_PATH_WINDOWS = "^\\s*\\w+\\s*:\\s*(\\\\[^/\\|\\*\\%\\&<>\\?:\"#=]+)+\\s*\\.[jJ][aA][vV][aA]\\s*$";
	
	/**
	 * Reglrání výraz, který slouží pro rozpoznání k třídě jazyka Java v následující
	 * syntaxi:
	 * 
	 * /home/dir/anotherDir/ClassName.java
	 * 
	 * V podstatě text musí začítan normálním lomítkem, pak mohou být libovolné
	 * znaky kromě normálního lomítka a na konec musí být desetinná tečka a javat
	 * (.java) coby přípona souboru.
	 */
	private static final String PATTERN_FOR_PATH_LINUX = "^\\s*(/[^/]+)+\\s*\\.[jJ][aA][vV][aA]\\s*$";
	
	
	
	
	private JTextField txtPathToClass, txtClassName;
	
	private JLabel lblChooseClass, lblPlace, lblClassName;
	
	private JButton btnBrowse;
	
	
	/**
	 * Proměnná,k do které vložím cestu ke zvolené třídě pro přidání, a její název:
	 */
	private String pathToClass;	
	
	
	// Promenné pro textty do chybových hlášek:
	private static String formatErrTitle, pathFormatErrText, emptyFieldErrTitle, emptyFieldErrText;
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 */
	public ClassFromFileForm() {
		super();
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/app/AddJavaClassIcon.png", new Dimension(600, 250), true);

		
		gbc = createGbc();
		
		index = 0;
		
		
		// Popisek pro zvolení tridy
		lblChooseClass = new JLabel();
		gbc.gridwidth = 3;
		setGbc(index, 0, lblChooseClass);
		
		
		
		
		// Popisek pro umístění pres textvoym polem pro cestu ke třídě+
		lblPlace = new JLabel();
		gbc.gridwidth = 1;
		setGbc(++index, 0, lblPlace);
		
		
		
		
		// textove pole pro cestu k třídě:
		txtPathToClass = new JTextField(20);
		setGbc(index, 1, txtPathToClass);
		
		
		
		
		// Tlačítko pro označení třídy pro přidání:
		btnBrowse = new JButton();
		btnBrowse.addActionListener(this);
		setGbc(index, 2, btnBrowse);
		
		
		
		
		// Popisek pro název třídy:
		lblClassName = new JLabel();
		gbc.gridwidth = 1;		
		setGbc(++index, 0, lblClassName);
		
		
		
		
		txtClassName = new JTextField(20);
		gbc.gridwidth = 2;
		setGbc(index, 1, txtClassName);
		
		
		
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		btnOk = new JButton();
		btnCancel = new JButton();
		btnOk.addActionListener(this);
		btnCancel.addActionListener(this);
		
		pnlButtons.add(btnOk);
		pnlButtons.add(btnCancel);
		
		gbc.gridwidth = 1;
		setGbc(++index, 2, pnlButtons);
		
		
		
		
		pack();
		setLocationRelativeTo(null);
		
		
		

		
		
		addWindowListener(getWindowsAdapter());
		
		
		
		
		addKeyListenerToTxtPath();
		
		
		addkeyListenerToTxtClassName();
	}
	
	
	
	
	
	/**
	 * Metoda, která přidá reakci na stisknutí klávesu enter
	 * do textového pole pro zadání cesty
	 */
	private void addKeyListenerToTxtPath() {
		txtPathToClass.addKeyListener(new KeyAdapter() {
			
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					testData();

				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					variable = false;
					dispose();
				}

				else
					NameFormAbstract.highlightTextField(txtPathToClass, null,
							DetectOS.getRegExByKindOfOs(PATTERN_FOR_PATH_WINDOWS, PATTERN_FOR_PATH_LINUX));
			}
		});
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá reakci na stisknutí klávesu enter do textového pole pro
	 * zadání názvu třídy
	 */
	private void addkeyListenerToTxtClassName() {
		txtClassName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					testData();
				
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					variable = false;
					dispose();
				}	
			}
		});
	}

	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zviditelní tento dialog Dále pokud se klikne na OK - tlačítku
	 * nastaví se proměnná na true a tato metoda vrátí jednorozměrné pole typu
	 * Object, ve kterém budou dvě hodnoty, na prvním indexu bude název třídy, a na
	 * druhém cesta k zvolené třídě, která se má přidat do projektu aplikace
	 * 
	 * Note:
	 * mohl jsem zvolit trochu více objektový přístup pro návratovou hodnotu metody
	 * a sice napst nejaou tridu, ktera by obsahovala dve promenne typu String,
	 * jedna pro nazev tridy a druha pro cestu k tride, ale v tomto pripade vim, ze
	 * jine moznosti nenastanou, tak jsem tu tridu nechtel moc vytvaret
	 * 
	 * @return jednorozměrné pole typu objekt, s dvěma hodnotami - název třídy a
	 *         její umístění na disku - odkud se má zkopírovat
	 */
	public final Object[] getSelectedClass() {
		setVisible(true);
		
		if (variable)
			return new Object[] {classNameVariable, pathToClass};
		
		return new Object[]{};
	}
	
	
	
	
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnOk)
				testData();
			
			
			
			else if (e.getSource() == btnBrowse) {
				pathToClass = new FileChooser().getPathToClass();
				
				if (pathToClass != null) {
					txtPathToClass.setText(pathToClass);

					// nastavím název do pole pro název:
					classNameVariable = pathToClass.substring(pathToClass.lastIndexOf(File.separator) + 1,
							pathToClass.lastIndexOf('.'));

					txtClassName.setText(classNameVariable);
				}
			}
			
			
			else if (e.getSource() == btnCancel) {
				variable = false;
				dispose();
			}
		}
	}





	/**
	 * Metoda, která otestuje název třídy a cesty po kliknutí na tlačítko OK nebo
	 * stisktnutí enteru v textovém poli pro cestu k souboru nebo název třídy
	 */
	private void testData() {
		// Otestuji, zda se cesta nachází ve správném formátu - kdyby uživatel zadal
		// cestu "ručně"
		if (!txtPathToClass.getText().isEmpty() && !txtClassName.getText().isEmpty()) {
			pathToClass = txtPathToClass.getText().trim();

			if (pathToClass.matches(DetectOS.getRegExByKindOfOs(PATTERN_FOR_PATH_WINDOWS, PATTERN_FOR_PATH_LINUX))) {
				// Zde cesta odpovídá, tak otestuji název třídy:
				// Zda neobshauje klíčová slova a je ve správném formátu:

				classNameVariable = txtClassName.getText().replaceAll("\\s", "");

				final boolean var = isClassNameRight();

				if (var) {
					variable = true;
					dispose();
				}
			}

			else
				JOptionPane.showMessageDialog(this, pathFormatErrText, formatErrTitle, JOptionPane.ERROR_MESSAGE);
		}

		else
			JOptionPane.showMessageDialog(this, emptyFieldErrText, emptyFieldErrTitle, JOptionPane.ERROR_MESSAGE);
	}

	
	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		// naplním společné proměnné:
		fillIdenticalyVariables(properties);
		
		final String txtExample = "model.data.NewClassName";
		
		if (properties != null) {
			setTitle(properties.getProperty("Cfff_DialogTitle", Constants.CFFF_DIALOG_TITLE));
			
			// Popisky
			lblChooseClass.setText(properties.getProperty("Cfff_LabelChooseClass", Constants.CFFF_LBL_CHOOSE_CLASS) + ": ");
			lblPlace.setText(properties.getProperty("Cfff_LabelPlace", Constants.CFFF_LBL_PLACE) + ": ");
			lblClassName.setText("? " + properties.getProperty("Cfff_LabelClassName", Constants.CFFF_LBL_CLASS_NAME) + ": ");
			lblClassName.setToolTipText(properties.getProperty("Cfff_LabelClassNameTt", Constants.CFFF_LBL_CLASS_NAME_TT) + ": " + txtExample);
			
			// Texty do tlačítek:
			 btnOk.setText(properties.getProperty("Cfff_ButtonOk", Constants.CFFF_BTN_OK));
			 btnCancel.setText(properties.getProperty("Cfff_ButtonCancel", Constants.CFFF_BTN_CANCEL));
			 btnBrowse.setText(properties.getProperty("Cfff_ButtonBrowse", Constants.CFFF_BTN_BROWSE) + "...");
			 
			 
			 // Texty do chybových hlášek:
			formatErrTitle = properties.getProperty("Cfff_Jop_FormatErrorTitle", Constants.CFFF_FORMAT_ERROR_TITLE);
			pathFormatErrText = properties.getProperty("Cfff_Jop_PathFormatErrorText", Constants.CFFF_PATH_FORMAT_ERROR_TEXT);
			emptyFieldErrTitle = properties.getProperty("Cfff_Jop_EmptyFieldErrorTitle", Constants.CFFF_EMPTY_FIELD_ERROR_TITLE);
			emptyFieldErrText = properties.getProperty("Cfff_Jop_EmptyFieldErrorText", Constants.CFFF_EMPTY_FIELD_ERROR_TEXT);	
		}
		
		
		else {
			setTitle(Constants.CFFF_DIALOG_TITLE);
			
			// Popisky
			lblChooseClass.setText(Constants.CFFF_LBL_CHOOSE_CLASS + ": ");
			lblPlace.setText(Constants.CFFF_LBL_PLACE + ": ");
			lblClassName.setText("? " + Constants.CFFF_LBL_CLASS_NAME + ": ");
			lblClassName.setToolTipText(Constants.CFFF_LBL_CLASS_NAME_TT + ": " + txtExample);
			
			// Texty do tlačítek:
			 btnOk.setText(Constants.CFFF_BTN_OK);
			 btnCancel.setText(Constants.CFFF_BTN_CANCEL);
			 btnBrowse.setText(Constants.CFFF_BTN_BROWSE + "...");
			 
			 
			 // Texty do chybových hlášek:
			formatErrTitle = Constants.CFFF_FORMAT_ERROR_TITLE;
			pathFormatErrText = Constants.CFFF_PATH_FORMAT_ERROR_TEXT;
			emptyFieldErrTitle = Constants.CFFF_EMPTY_FIELD_ERROR_TITLE;
			emptyFieldErrText = Constants.CFFF_EMPTY_FIELD_ERROR_TEXT;
		}
	}
}