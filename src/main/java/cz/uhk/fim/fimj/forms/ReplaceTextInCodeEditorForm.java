package cz.uhk.fim.fimj.forms;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Properties;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;

/**
 * Tato třída slouží jako Dialog, do kterého uživtel zadá text, který chce nahradit, a text, kterým chce předchozí text
 * nahradit
 * <p>
 * bude mít na výběr z možnosti ,zda chce nahradit pouze první nalezený text nebo všechny nalezené
 * <p>
 * Tento dialog se zobrazí po kliknutí na položku Nahradit (v češtine) v menu v dialogu pro editoru zdrojov=ho kódu
 * třídy, který lze zobrazit po kliknuti na Otevřit v editoru nad buňkou třídy v diagramu tříd
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ReplaceTextInCodeEditorForm extends SourceDialogForm implements LanguageInterface, ActionListener {
	private static final long serialVersionUID = 1L;


	/**
	 * Regulární výraz pro bíle znaky:
	 */
	private static final String regExForWhiteSymbols = "^\\s*$";
			
	
	/**
	 * Proměnná, ve které si uchovám zdrojový kód z editoru, abych mohl zjistit, zda
	 * se v něm daný řetězec vysktyuje nebo nikoliv a dle toho vybarvím pozadí
	 * daného okna pro zadaání řetězce pro nahrazení textu v kóduu
	 */
	private final String codeFromEditor;
	
	private JLabel lblFind, lblReplace, lblInfo, lblMissingValue;
	
	private JTextField txtFind, txtReplace;
	
	
	private JCheckBox chcbReplaceFirst, chcbReplaceAll;
	
	private String replaceText = "", findText;
	
	
	// Proměnné pro texty do chybových hlášek:
	private String codeDontContainString, textString, missingValue, emptryFieldText, emptyFieldTitle;
	
	
	
	
	
	
	public ReplaceTextInCodeEditorForm(final String codeFromEditor) {
		super();
		
		this.codeFromEditor = codeFromEditor;
		
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/app/FindAndReplaceIcon.png", new Dimension(500, 300), true);
		
		// Vytvořím si instanci pro rozvržení komponent v dialogu / okně
		gbc = createGbc();
		
		index = 0;
		
		
		gbc.gridwidth = 2;
		
		lblInfo = new JLabel();		
		setGbc(index, 0, lblInfo);
		
		
		
		
		
		
		final JPanel pnlErrorText = new JPanel(new FlowLayout(FlowLayout.CENTER));
		
		lblMissingValue = new JLabel();		
		lblMissingValue.setFont(ERROR_FONT);
		lblMissingValue.setForeground(Color.RED);
		lblMissingValue.setVisible(false);
		pnlErrorText.add(lblMissingValue);
		
		setGbc(++index, 0, pnlErrorText);
		
		
		
		
		
		
		gbc.gridwidth = 1;
		
		lblFind = new JLabel();
		setGbc(++index, 0, lblFind);
		
		
		
		txtFind = new JTextField(20);
		addKeyListenerToFindField();
		setGbc(index, 1, txtFind);
		
		
		
		lblReplace = new JLabel();
		setGbc(++index, 0, lblReplace);
		
		
		
		
		txtReplace = new JTextField(20);
		addKeyListenerToReplaceField();
		setGbc(index, 1, txtReplace);
		
		
		
		
		
		final JPanel pnlChcbs = new JPanel();
		pnlChcbs.setLayout(new BoxLayout(pnlChcbs, BoxLayout.PAGE_AXIS));

		chcbReplaceFirst = new JCheckBox();
		chcbReplaceAll = new JCheckBox();
		
		
		final ButtonGroup bg = new ButtonGroup();
		bg.add(chcbReplaceFirst);
		bg.add(chcbReplaceAll);
		
		chcbReplaceFirst.setSelected(true);
		
		
		chcbReplaceFirst.setAlignmentX(CENTER_ALIGNMENT);
		chcbReplaceAll.setAlignmentX(CENTER_ALIGNMENT);
		
		pnlChcbs.add(chcbReplaceFirst);
		pnlChcbs.add(Box.createRigidArea(new Dimension(15, 0)));
		pnlChcbs.add(chcbReplaceAll);
		
		gbc.gridwidth = 2;
		setGbc(++index, 0, pnlChcbs);
		
				

		
		
		
		
		
		
		final JPanel pnlButons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		btnOk = new JButton();
		btnCancel = new JButton();
		
		pnlButons.add(btnOk);
		pnlButons.add(btnCancel);
		
		btnOk.addActionListener(this);
		btnCancel.addActionListener(this);
		
		setGbc(++index, 0, pnlButons);
		
		
		
		
		
		
		
		
		// Událost na zavření okna:
		addWindowListener(getWindowsAdapter());
		
		
		
		pack();
		setLocationRelativeTo(null);		
	}
	
	
	
	

	
	
	
	
	
	
	/**
	 * Metoda, která zobrazí dialog a tím ho udělá modální, dále pokud se klikne na
	 * tlačítko OK, tak získá nahradí zadaá slova z editoru a tento text vrátí,
	 * jinak kdytž se klikne na Zrušit nebo uživatel zavře dialog, tak se vrátí null
	 * 
	 * @return null, neo zdrojový kód s nahrazenými texty
	 */
	public final String findAndReplace() {
		setVisible(true);
		
		if (variable) {
			final String replacementText; 
			
			
			if (chcbReplaceFirst.isSelected())
				replacementText = codeFromEditor.replaceFirst(findText, replaceText);
			
			else replacementText = codeFromEditor.replaceAll(findText, replaceText);
			
			
			return replacementText;
		}
		return null;
	}
	
	
	
	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {	
			setTitle(properties.getProperty("Rtice_DialogTitle", Constants.RTICE_DIALOG_TITLE));
			
			lblInfo.setText(properties.getProperty("Rtice_LabelInfo", Constants.RTICE_LBL_INFO));
			
			lblMissingValue.setText(properties.getProperty("Rtice_LabelMissingValue", Constants.RTICE_LBL_MISSING_VALUE));			
			
			lblFind.setText(properties.getProperty("Rtice_LabelFind", Constants.RTICE_LBL_FIND));
			lblReplace.setText(properties.getProperty("Rtice_LabelReplace", Constants.RTICE_LBL_REPLACE));
			
			btnOk.setText(properties.getProperty("Rtice_ButtonOk", Constants.RTICE_BTN_OK));
			btnCancel.setText(properties.getProperty("Rtice_ButtonCancel", Constants.RTICE_BTN_CANCEL));
			
			chcbReplaceFirst.setText(properties.getProperty("Rtice_ChcbReplaceFirst", Constants.RTICE_CHCB_REPLACE_FIRST));
			chcbReplaceAll.setText(properties.getProperty("Rtice_ChcbReplaceAll", Constants.RTICE_CHCB_REPLACE_ALL));
			
			codeDontContainString = properties.getProperty("Rtice_TextCodeDontContainsString", Constants.RTICE_TXT_CODE_DONT_CONTAINS_STRING);
			textString = properties.getProperty("Rtice_TextString", Constants.RTICE_TEXT_STRING);
			missingValue = properties.getProperty("Rtice_MissingValue", Constants.RTICE_MISSING_VALUE);
			
			emptyFieldTitle = properties.getProperty("Rtice_EmptyFieldTitle", Constants.RTICE_EMPTY_FIELD_TITLE);
			emptryFieldText = properties.getProperty("Rtice_EmptyFieldText", Constants.RTICE_EMPTY_FIELD_TEXT);
		}
		
		else {
			setTitle(Constants.RTICE_DIALOG_TITLE);
			
			lblInfo.setText(Constants.RTICE_LBL_INFO);
			
			lblMissingValue.setText(Constants.RTICE_LBL_MISSING_VALUE);			
			
			lblFind.setText(Constants.RTICE_LBL_FIND);
			lblReplace.setText(Constants.RTICE_LBL_REPLACE);
			
			btnOk.setText(Constants.RTICE_BTN_OK);
			btnCancel.setText(Constants.RTICE_BTN_CANCEL);
			
			chcbReplaceFirst.setText(Constants.RTICE_CHCB_REPLACE_FIRST);
			chcbReplaceAll.setText(Constants.RTICE_CHCB_REPLACE_ALL);
			
			codeDontContainString = Constants.RTICE_TXT_CODE_DONT_CONTAINS_STRING;
			textString = Constants.RTICE_TEXT_STRING;
			missingValue = Constants.RTICE_MISSING_VALUE;
			
			emptyFieldTitle = Constants.RTICE_EMPTY_FIELD_TITLE;
			emptryFieldText = Constants.RTICE_EMPTY_FIELD_TEXT;
		}
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá reakci na stisknutí klávesy Enter na textové pole pro
	 * zadání textu, kterým se má nahradit text v poli najít
	 * 
	 * Po stisknutí klávesy enter se zavolá metoda testData(), která otestuje data v
	 * dialogu
	 */
	private void addKeyListenerToReplaceField() {
		txtReplace.addKeyListener(new KeyAdapter() {			
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					testData();
			}
		});
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá reakce na stisknutí klávesy na dialog pro zadaání
	 * řetězce, který se má nahradit, pokud daný řetězec nebu v kódu nalezen, pozadí
	 * pole zčervená, jiank bude bíle
	 */
	private void addKeyListenerToFindField() {
		txtFind.addKeyListener(new KeyAdapter() {
			
			@Override
			public void keyReleased(KeyEvent e) {
				// Musím tyto podmínky testovat po uvolnení nějaké klávesy, protože při stisknutí v poli nebude zadán
				// nový znak
				
				// Při uvolnení klávesy otestuji, zda není zadán pouze bílý znak
				// a pokud není, tak otestuji, zda se zadaný text nachází ve zdrojém kódu
				
				final String textFromFindField = txtFind.getText();
				
				if (!textFromFindField.matches(regExForWhiteSymbols)) {
					// Zde zadaný text v poli pro nalezení řetězce nebosahuje pouze bíle znaky
					
					// nyní otestuji, zda se zadaný text vyskytuje v kódu:
					if (codeFromEditor.contains(textFromFindField)) {
						findText = textFromFindField;
						
						if (txtFind.getBackground() != Color.WHITE)
							txtFind.setBackground(Color.WHITE);
						
						if (lblMissingValue.isVisible())
							lblMissingValue.setVisible(false);
					}
					
					else {
						findText = null;
						
						if (txtFind.getBackground() != Color.RED)
							txtFind.setBackground(Color.RED);
						
						if (!lblMissingValue.isVisible())
							lblMissingValue.setVisible(true);
					}
				}
			}
			
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					testData();
			}
		});
	}
	
	
	
	
	
	





	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnOk)
				testData();
			
			else if (e.getSource() == btnCancel) {
				variable = false;
				dispose();
			}
		}
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje data po stisknutí klávesy Enter nebo kliknuti na
	 * tlačítko OK v dialogu Je to vypsané do teéto metody, aby mohl dialog
	 * "reagovat" i na stisknutí klávesy enter
	 */
	private void testData() {
		// Zde stači otestovat pouze pole pro zadání nalezení řetězce pro nahrazení,
		// protže chci aby když bude pole pro nahrazení prázdné, tak se daný text nahradí "" - prádzným znakem
		
		if (!txtFind.getText().isEmpty()) {
			if (findText != null) {
				// Zde zdrojový kód třídy obsahuje zadaný řetězec, tak ho mohu nahradit:
				replaceText = txtReplace.getText();
				
				variable = true;
				dispose();
			}
			else
				JOptionPane.showMessageDialog(this, codeDontContainString + "\n" + textString + ": " + findText,
						missingValue, JOptionPane.ERROR_MESSAGE);
		}
		else
			JOptionPane.showMessageDialog(this, emptryFieldText, emptyFieldTitle, JOptionPane.ERROR_MESSAGE);
	}
}