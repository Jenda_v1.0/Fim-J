package cz.uhk.fim.fimj.forms;

import java.awt.Component;

import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.ListCellRenderer;

import cz.uhk.fim.fimj.menu.MainMethodInfo;

/**
 * Tato třída slouží jako Renderer pro vykreslování JradioButtonů v Jlistu pro třídu (objekt), resp. dialog
 * SelectMainMethod.java
 * <p>
 * Chtěl jsem použít třídu CheckBoxListCellRenderer, jenže rozhraní ListCellRenderer nepodporuje generické typy, tak
 * jsem pro tuto třídu napsal implementaci pro radiobutton
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class JRadioButtonListCellRenderer extends JRadioButton implements ListCellRenderer<MainMethodInfo> {

    private static final long serialVersionUID = 1L;

    @Override
    public Component getListCellRendererComponent(JList<? extends MainMethodInfo> list, MainMethodInfo value, int index,
                                                  boolean isSelected, boolean cellHasFocus) {

        setComponentOrientation(list.getComponentOrientation());
        setFont(list.getFont());
        setBackground(list.getBackground());
        setForeground(list.getForeground());
        setSelected(value.isRbMainSelected());
        setEnabled(list.isEnabled());

        setText(value.getRbMain().getText());

        return this;
    }
}