package cz.uhk.fim.fimj.forms;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cz.uhk.fim.fimj.app.DetectOS;
import cz.uhk.fim.fimj.app.InfoAboutWorkspace;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.FileChooser;
import cz.uhk.fim.fimj.file.FileChooserInterface;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.project_explorer.NameFormAbstract;

/**
 * Tato třída slouží jako dialog pro výběr adresáře coby Workspace (pracovní prostor) kam se budou primárně ukládat
 * projekty této aplikace (pokud uživatel nezvolí jinak).
 * <p>
 * Dialog ve který se spustí po spuštění aplikace nebo při změné místa uložení Workspacu Obsahuje metod chooseWorkspace,
 * která vrátí zvolený cestu k zvolené složce coby k Workspacu
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class WorkspaceForm extends SourceDialogForm implements LanguageInterface, ActionListener {

	private static final long serialVersionUID = -183813067966463683L;


	/**
	 * Tlačítko editace místa k Workspacu:
	 */
	private final JButton btnBrowse;
	
	
	

	/**
	 * Zatrhávací tlačítko, pokud ho uživatel zaškrtne, již se aplikace nebude po
	 * spuštění na Workspace dotazovat
	 */
	private final JCheckBox chcbAskForWorkspace;
	
	
	/**
	 * Pole pro zadání, resp výběr cesty k Workspacu
	 */
	private final JTextField txtPathToWorkspace;
	
	
	// Popisky s informacemi pro uživatele, informace o dialogu - k cemu slouží, co se má zadávat, apod.
	private final JLabel lblSelectWorkspace, lblInfo, lblChoose, lblWorkspace;	
	
	

	
	/**
	 * Výchozí Font pro test lblSelectWorkspace
	 */
	private static Font fontSelectedWorkspace;
	
	
	// Proměnné, do který načtu hodnotu ze souboru - jazyk - hodnoty jsou pro chybové hlášky:
	private static String wsPathEmptyTitle, wsPathEmptyText, wsPathFormatTitle, wsPathFormatText;
	
	
	
	/**
	 * Slouží pro označení adresářů a souborů pomocí dialogového okna.
	 */
	private final FileChooserInterface fileChooser;
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 */
	public WorkspaceForm() {
		super();
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/app/WorkspaceIcon.png", new Dimension(600, 350), true);
		
		
		fileChooser = new FileChooser();
		
		
		
		// Výchozí nastavení pro gbc - jak se má okno chovat při změné velikosti uživatelem, směr vyplňování, rozestupy,, ...
		gbc = createGbc();
		
		index = 0;
	
		
		
		gbc.gridwidth = 3;
		
		
		// Vytvoření instancí potřebných komponent a jejich přidání do okna dialogu:
		fontSelectedWorkspace = new Font("Font for select workspace", Font.BOLD, 15);
		lblSelectWorkspace = new JLabel();
		lblSelectWorkspace.setFont(fontSelectedWorkspace);
		setGbc(index, 0, lblSelectWorkspace);
		
		
		
		
		lblInfo = new JLabel();
		setGbc(++index, 0, lblInfo);
		
		
		
		lblChoose = new JLabel();
		setGbc(++index, 0, lblChoose);
		
		
		gbc.gridwidth = 1;
		
		
		lblWorkspace = new JLabel();
		setGbc(++index, 0, lblWorkspace);
		
		
		
		// Výchozí cesta: 'user.dir' , 'user.home', 'user.name'
		// Nejprve si otestuji, zda je uložen nějaký workspace a a pokud ještě existuje, tak k němu nastavím cestu,
		// ale pokud neexistuje, tak použijí výchozí cestu k adresáři Workspace, - podobně jako například eclipse,
		// který nastaví adresář Workspace v adresáři uživatele:
		final String pathToWorkspace = ReadFile.getPathToWorkspace();
		
		// Proměnná, do které se vloží text coby cesta k adresáři workspace, buď ten, který existuje, nebo
		// ten, výchozí - pokd neexistuje:
		final String defaultPathToTxtField;
		
		if (pathToWorkspace != null)
			defaultPathToTxtField = pathToWorkspace;
		
		else defaultPathToTxtField = System.getProperty("user.home") + File.separator + "workspace";
		

		
		
		txtPathToWorkspace = new JTextField(defaultPathToTxtField, 0);
		setGbc(index, 1, txtPathToWorkspace);
		
		
		
		btnBrowse = new JButton();
		btnBrowse.addActionListener(this);
		setGbc(index, 2, btnBrowse);
		
		
		gbc.gridwidth = 3;
		

		
		
		chcbAskForWorkspace = new JCheckBox();
		final boolean select = askForWorkspace();
		chcbAskForWorkspace.setSelected(select);
		setGbc(++index, 0, chcbAskForWorkspace);
		
		
		gbc.gridwidth = 1;
		
		
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		btnOk = new JButton();
		btnCancel = new JButton();
		
		btnOk.addActionListener(this);
		btnCancel.addActionListener(this);
		
		pnlButtons.add(btnOk);
		pnlButtons.add(btnCancel);
		
		setGbc(++index, 2, pnlButtons);
		
		
		
		
		
		
		addKeyListenerToTxtForPathToWorkspace();
		
		
		
		addWindowListener(getWindowsAdapter());
		
		
		
		// Přizpůsobení okna velikosti kompnenentám a zarovnání na střed
		pack();
		setLocationRelativeTo(null);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá do textového pole pro zadání cesty k adresáři coby 
	 * Workspace reakci na klávesu Enter - potvrzení cesty 
	 */
	private void addKeyListenerToTxtForPathToWorkspace() {
		txtPathToWorkspace.addKeyListener(new KeyAdapter() {
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					testData();
				
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					variable = false;
					dispose();
				}
			}
			
			
			@Override
			public void keyReleased(KeyEvent e) {
				NameFormAbstract.highlightTextField(txtPathToWorkspace, null,
						DetectOS.getRegExByKindOfOs(REG_EX_FOR_PATH_TO_DIR_WINDOWS, REG_EX_FOR_PATH_TO_DIR_LINUX));
			}
		});
	}
	
	
	
	
	

	
	
	
	/**
	 * Metoda, která se zavolá při spuštení aplikace pro výběr Workspacu, nebo při
	 * změně Workspacu jelikož se zavolá po spuštění aplikace, tak pokud uživatel
	 * dialog zavře, ukončí se i aplikace protože není zvolen pracovní Workspace
	 * 
	 * Při změně Workspacu se dialog zavře, ale aplikace beží dál
	 * 
	 * Note:
	 * V objektu InfoAboutWorkspace, který vrátí tato metody se bude hodnota
	 * (askForWorkspace) nacházet v "opačné syntaxei", tzn. že pokud v tomto dialogu
	 * nebude chcb pro to, zda se má znovu načíst workspace při startu aplikace
	 * označen, pak se nastaví do výše uvedeného objektu hodnota true a naopak,
	 * pokud bude chcb označen, vrátí se false. je to pouze z toho důvodu, abych
	 * "zachoval" dotaz ve správné syntaxi, tj. v Default.properties je jako klíč:
	 * Display WorkspaceChooser, takže "říkám", že pokud je ta hodnota true, pak se
	 * má zobrazit jinak ne. Toto je pro případ, že by to chtěl uživatel přepsat v
	 * tomto souboru, tak aby to "správně" pochopil, i když zde jsou jen
	 * dvěmožnosti, kdybych toto neudělal, pak bych musel do toho klíše psát false,
	 * když se má tento dialog zobrazit a naopak a otočit podmínku ve třídě Start
	 * při zjišťování dotazu na workspace.
	 * 
	 * @return zvolená cesta k Workspacu a logická hodnota o tom, zda se má při
	 *         spuštění aplikace aplikace dotázat na umístění Workspacu
	 */
	public final InfoAboutWorkspace chooseWorkspace() {
		setVisible(true);
		
		if (variable)
			return new InfoAboutWorkspace(txtPathToWorkspace.getText().trim(), !chcbAskForWorkspace.isSelected());

		return null;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která načte nastavenou (popř. výchozí hodnotu) o tom, zda se má
	 * označit chcb pro dotaz, zda se má při spuštění aplikace dotázat na adresář
	 * označený jako workspace nebo ne.
	 * 
	 * @return true, nebo false - dle toho, zda se má aplikace při jejím spuštění
	 *         dotázat na adresář označený jako workspace nebo ne. Jedna se o
	 *         získanou hodnotu z defult.properties z adresáře workspace nebo z
	 *         aplikace, popř výchozí hodnota z Constants.
	 */
	private static boolean askForWorkspace() {
		/*
		 * Zkusím si načíst soubor Default.properties z adresáře označeného jako
		 * workspace (/ popř. z aplikace):
		 */
		final Properties defaultProp = ReadFile.getDefaultPropAnyway();

		/*
		 * Vrátím hodnotu s dotazem, zda se mám dotazovat na adresář workspace, abych
		 * dle toho mohl označit chcb v okně, aby uživatel viděl aktuální nastavenou
		 * hodnotu.
		 *
		 * Note:
		 * Je třeba jej znegovat, protože mám nastaven dotaz trochu jinak, než je pak
		 * hodnota, tj. když je chcb označen, pak je hodnota false a naopak.
		 */
		return !Boolean.parseBoolean(defaultProp.getProperty("Display WorkspaceChooser",
				String.valueOf(Constants.DEFAULT_DISPLAY_WORKSPACE)));
	}
	
	
	
	
	
	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			setTitle(properties.getProperty("Wf_Title", Constants.TITLE));
			btnOk.setText(properties.getProperty("Wf_BtnOk", Constants.BTN_OK));
			btnCancel.setText(properties.getProperty("Wf_Btn_Cancel", Constants.BTN_CANCEL));
			btnBrowse.setText(properties.getProperty("Wf_BtnBrowse", Constants.BTN_BROWSE) + "...");
			lblSelectWorkspace.setText(properties.getProperty("Wf_Lbl_Select_Workspace", Constants.LBL_SELECT_WORKSPACE));
			lblInfo.setText(properties.getProperty("Wf_Lbl_Info", Constants.LBL_INFO));
			lblChoose.setText(properties.getProperty("Wf_Lbl_Choose", Constants.LBL_CHOOSE));
			lblWorkspace.setText(properties.getProperty("Wf_Lbl_Workspace", Constants.LBL_WORKSPACE) + ":");
			chcbAskForWorkspace.setText(properties.getProperty("Wf_Chcb_Remember_Path", Constants.CHCB_REMEMBER_PATH));
			wsPathEmptyText = properties.getProperty("Wf_Workspace_Path_Empty_Text", Constants.WORKSPACE_PATH_EMPTY_TEXT);
			wsPathEmptyTitle = properties.getProperty("Wf_Workspace_Path_Empty_Title", Constants.WORKSPACE_PATH_EMPTY_TITLE);
			wsPathFormatTitle = properties.getProperty("Wf_Workspace_Path_Format_Title", Constants.WORKSPACE_PATH_FORMAT_TITLE);
			wsPathFormatText = properties.getProperty("Wf_Workspace_Path_Format_Text", Constants.WORKSPACE_PATH_FORMAT_TEXT);			
		}
		
		
		else {
			setTitle(Constants.TITLE);
			btnOk.setText(Constants.BTN_OK);
			btnCancel.setText(Constants.BTN_CANCEL);
			btnBrowse.setText(Constants.BTN_BROWSE + "...");
			lblSelectWorkspace.setText(Constants.LBL_SELECT_WORKSPACE);
			lblInfo.setText(Constants.LBL_INFO);
			lblChoose.setText(Constants.LBL_CHOOSE);
			lblWorkspace.setText(Constants.LBL_WORKSPACE + ":");
			chcbAskForWorkspace.setText(Constants.CHCB_REMEMBER_PATH);
			wsPathEmptyText = Constants.WORKSPACE_PATH_EMPTY_TEXT;
			wsPathEmptyTitle = Constants.WORKSPACE_PATH_EMPTY_TITLE;
			wsPathFormatTitle = Constants.WORKSPACE_PATH_FORMAT_TITLE;
			wsPathFormatText = Constants.WORKSPACE_PATH_FORMAT_TEXT;
		}
	}

	
	
	
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnBrowse) {
				final String selectedWorkspace = fileChooser.getPathToWorkspaceDir();

				if (selectedWorkspace != null)
					// nastavím do pole - textfieldu zvolenou cestu do Workspacu:
					txtPathToWorkspace.setText(selectedWorkspace);
			}
			
			
			else if (e.getSource() == btnOk)
				testData();
			
			
			else if (e.getSource() == btnCancel) {
				variable = false;
				dispose();
			}			
		}
	}

	
	
	
	
	

	/**
	 * Metoda, jejíž kód by mohl být pouze v části coby reakce na kliknutí na
	 * tlačítko OK - potvrzení cesty k adresáři coby Workspace, ale dal jsem tento
	 * kó do následující metody, abych jí mohl zavolat i po stisknutí klávesy Enter
	 * v zadávání cesty k projektu "ručně" - uživatelem
	 */
	private void testData() {
		if (!txtPathToWorkspace.getText().replaceAll("\\s", "").isEmpty()) {
			if (txtPathToWorkspace.getText().matches(
					DetectOS.getRegExByKindOfOs(REG_EX_FOR_PATH_TO_DIR_WINDOWS, REG_EX_FOR_PATH_TO_DIR_LINUX))) {
				variable = true;
				dispose();
			}

			else
				JOptionPane.showMessageDialog(this, wsPathFormatText, wsPathFormatTitle, JOptionPane.ERROR_MESSAGE);
		}
		else
			JOptionPane.showMessageDialog(this, wsPathEmptyText, wsPathEmptyTitle, JOptionPane.ERROR_MESSAGE);
	}
}