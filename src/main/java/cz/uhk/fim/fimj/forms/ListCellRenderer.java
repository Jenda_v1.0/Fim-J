package cz.uhk.fim.fimj.forms;

import java.awt.Component;
import java.util.regex.Pattern;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import cz.uhk.fim.fimj.file.Constants;

/**
 * Tato třída slouží pouze jako renderer, který slouží / "umožňuje" příslušné komponentě JList, ve kterém bude "tento"
 * renderer nastaven, aby se v té komponentě Jlist zalamovaly texty.
 * <p>
 * Jde o to, že některé texty mohou být přísliš široké, aby aby uživatel nemusel pořád posouvat posuvník, tak dám
 * uživateli na výběr, že se budou moci zalamovat texty v příslušném JListu, takže pokud ten text / konkrétní položka v
 * JListu bude širší než definovaná velikost (budu to dávat na šířku toho listu nebo okna dialogu), tak se ten text
 * zalomí na více řádků.
 * <p>
 * <p>
 * Zdroj: https://stackoverflow.com/questions/8197167/word-wrap-in-jlist-items
 * <p>
 * a
 * <p>
 * https://stackoverflow.com/questions/7861724/is-there-a-word-wrap-property-for-jlabel/7861833#7861833
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ListCellRenderer extends DefaultListCellRenderer {

	private static final long serialVersionUID = 1L;

	 private static final String HTML_1 = "<html><body style='width: ";
	 private static final String HTML_2 = "px'>";
	 private static final String HTML_3 = "</html>";

    private static final Pattern OPENING_POINTED_BRACKET = Pattern.compile("<");
    private static final Pattern CLOSING_POINTED_BRACKET = Pattern.compile(">");

    private final int widthOfText;

	 
	 public ListCellRenderer(final int width) {
		 this.widthOfText = width;
	}
	 
	 

	 
	@Override
	public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index, final boolean isSelected, final boolean cellHasFocus) {

		/*
		 * Toto by stačilo, kdybych nechtěl nahrazovat ty špičaté závorky (tagy) za příslušné znaky, aby se mohli vykreslit.
		 */
//		final String text = HTML_1 + String.valueOf(width) + HTML_2 + value + HTML_3;
		
		
		/*
		 * Zde si potřebuji přepsat veškeré hodnoty pro otevírací a uzavírací tag za
		 * příslušné speciální znaky, protože jelikož využívám pro zalamování textů html
		 * jazyk, a ten nevypisuje příslušné tagy. tak tyto znaky potřebuji nahradit za
		 * speciální "symboly", aby se znaky vypsaly.
		 */
		final String temp;

		if (value != null)
			temp = value.toString();
		else
			temp = "";

		// Nahrazení špičatých závorek (tagů) za znaky, které se zobrazují jako tagy v textu:
		String textWithTagsForWrap = OPENING_POINTED_BRACKET.matcher(temp).replaceAll(Constants.HTML_OPEN_TAG);
		textWithTagsForWrap = CLOSING_POINTED_BRACKET.matcher(textWithTagsForWrap).replaceAll(Constants.HTML_CLOSE_TAG);

		final String text = HTML_1 + String.valueOf(widthOfText) + HTML_2 + textWithTagsForWrap  + HTML_3;

		return super.getListCellRendererComponent(list, text, index, isSelected, cellHasFocus);
	}
}
