package cz.uhk.fim.fimj.forms;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import cz.uhk.fim.fimj.app.DetectOS;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.FileChooser;
import cz.uhk.fim.fimj.file.FileChooserInterface;
import cz.uhk.fim.fimj.file.JavaHomeInfo;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.ReadFile;

/**
 * Tato třída slouží jako dialog pro "nastavení" buď domovského adresáře Javy pro kompilátor nebo pro umístění některých
 * vybraných a zároveň potřebných souborů do adresáře aplikace - libTools, kde jsou soubory potřehné pro to, abymohla
 * aplikace kompilvat třídy v diagramu tříd.
 * <p>
 * Dialog umožňuje zadání bud cesty k domovskému adresáři Javy na počítačí uživatele. nebo si budou moci označit soubory
 * - currency.data nebo tools.jar a nebo flavormap.properties a tyto soubory se do slozky libTools v aplikaci se do ní
 * nakopírují.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class JavaHomeForm extends SourceDialogForm implements LanguageInterface, ActionListener {

	private static final long serialVersionUID = 1L;


	/**
	 * Proměnná, pomocí které si budu načítat jednotlivé cesty k souborů a
	 * adresářům: zavolám nad ní metody, které otevřou dialog pro výběr umístění a
	 * tuto cestu vrátí:
	 */
	private static final FileChooserInterface fileChooser = new FileChooser();
	
	
	private static final int DEFAULT_TXT_FIELD_HEIGHT = 20, DEFAULT_TXT_FIELD_WIDTH = 350;
	
	
	
	/**
	 * Regulární výraz, který slouží pro rozpoznání cesty k nějakému souboru na OS
	 * Windows, jedná se pouze o první část, Povolená syntaxe:
	 * 
	 * C:\\Users\\krunc\\Desktop\\BC_ W1orkspace\\Test\\src\\defau
	 * ltPackage\\tools.jar
	 * 
	 * Cesta nesmí obsahovat znaky: ^, /, |, *, %, $, <, >, ?, :, ", =
	 */
	private static final String REG_EX_FOR_PATH_TO_FILE_FOR_WINDOWS_PART_1 = "^\\s*\\w+\\s*:\\s*(\\\\[^/\\|\\*\\%\\&<>\\?:\"#=]+)*(\\\\\\s*";
	
	/**
	 * Regulární výraz, který slouží pro rozpoznání cesty k nějakému souboru na OS
	 * Linux, jedná se pouze o první část, Povolená syntaxe:
	 * 
	 * / Users/ krunc/Desktop/ someFile.extension
	 */
	private static final String REG_EX_FOR_PATH_TO_FILE_FOR_LINUX_PART_1 = "^\\s*(/[^/]+)+\\s*";
	
	
	/**
	 * Toto je zbývající část regulárního výrazu, který je potřeba pro ukončení, tak
	 * trochu je to zbytečné, ale kdyby náhodou uživatel zadalnakonci cesty mezery
	 * či jiné bílé znaky, tak by to mohl být problém zde ve výrazu, ale pak v kódu
	 * se na začátku a na konci bílé znaky odeberou. Ale výrazem by neprošly, tak by
	 * se stejně nic nestalo, proto jej zde potřebuji.
	 */
	private static final String REG_EX_FOR_PATH_TO_FILE_FOR_WINDOWS_PART_2 = "\\s*)\\s*$";
	
	
	/**
	 * Druhý část regulráního výrazu pro rozpoznání syntaxe cesty k nějakému souboru
	 * v OS Linux. Tato část je pouze pro to, aby se nakonci rozpoznali i mezery, až
	 * už je tam uživatel zadal úmyslně nebo ne.
	 */
	private static final String REG_EX_FOR_PATH_TO_FILE_FOR_LINUX_PART_2 = "\\s*$";;
	
	
	
	
	
	
	
	/**
	 * Regulární výraz pro rozeznání cesty k souboru tools.jar v syntaxi pro cestu v
	 * OS Widows, tj. například:
	 * 
	 * C:\\Users\\krunc\\Desktop\\dir\\tools.jar
	 * 
	 * Cesta nesmí obsahovat znaky: ^, /, |, *, %, $, <, >, ?, :, ", =
	 */
	private static final String REG_EX_FOR_PATH_TO_TOOLS_JAR_WINDOWS = REG_EX_FOR_PATH_TO_FILE_FOR_WINDOWS_PART_1
			+ Constants.COMPILER_FILE_TOOLS_JAR + REG_EX_FOR_PATH_TO_FILE_FOR_WINDOWS_PART_2;
	

	/**
	 * Regulární výraz pro rozeznání cesty k souboru tools.jar v syntaxi pro cestu v
	 * OS Linux, tj. například:
	 * 
	 * / Users/ krunc/Desktop/othersDir/tools.jar
	 */
	private static final String REG_EX_FOR_PATH_TO_TOOLS_JAR_LINUX = REG_EX_FOR_PATH_TO_FILE_FOR_LINUX_PART_1
			+ Constants.COMPILER_FILE_TOOLS_JAR + REG_EX_FOR_PATH_TO_FILE_FOR_LINUX_PART_2;
	
	
	
	
	
	
	
	/**
	 * Regulární výraz pro rozeznání cesty k souboru currency.data v syntaxi pro
	 * cestu v OS Widows, tj. například:
	 * 
	 * C:\\Users\\krunc\\anotherDirs...\\dir\\currency.data
	 * 
	 * Cesta nesmí obsahovat znaky: ^, /, |, *, %, $, <, >, ?, :, ", =
	 */
	private static final String REG_EX_FOR_PATH_TO_CURRENCY_DATA_WINDOWS = REG_EX_FOR_PATH_TO_FILE_FOR_WINDOWS_PART_1
			+ Constants.COMPILER_FILE_CURRENCY_DATA + REG_EX_FOR_PATH_TO_FILE_FOR_WINDOWS_PART_2;
	
	
	/**
	 * Regulární výraz pro rozeznání cesty k souboru currency.data v syntaxi pro
	 * cestu v OS Linux, tj. například:
	 * 
	 * /Users/ krunc/Desktop/othersDir/currency.data
	 */
	private static final String REG_EX_FOR_PATH_TO_CURRENCY_DATA_LINUX = REG_EX_FOR_PATH_TO_FILE_FOR_LINUX_PART_1
			+ Constants.COMPILER_FILE_CURRENCY_DATA + REG_EX_FOR_PATH_TO_FILE_FOR_LINUX_PART_2;
	
	
	
	
	
	
	
	
	
	/**
	 * Regulární výraz pro rozeznání cesty k souboru flavormap.properties v syntaxi
	 * pro cestu v OS Widows, tj. například:
	 * 
	 * C:\\Users\\krunc\\anotherDirs...\\dir\\flavormap.properties
	 * 
	 * Cesta nesmí obsahovat znaky: ^, /, |, *, %, $, <, >, ?, :, ", =
	 */
	private static final String REG_EX_FOR_PATH_TO_FLAVORMAP_PROPERTIES_WINDOWS = REG_EX_FOR_PATH_TO_FILE_FOR_WINDOWS_PART_1
			+ Constants.COMPILER_FILE_FLAVORMAP_PROPERTIES + REG_EX_FOR_PATH_TO_FILE_FOR_WINDOWS_PART_2;
	

	/**
	 * Regulární výraz pro rozeznání cesty k souboru flavormap.properties v syntaxi
	 * pro cestu v OS Linux, tj. například:
	 * 
	 * /Users/ krunc/Desktop/othersDir/flavormap.properties
	 */
	private static final String REG_EX_FOR_PATH_TO_FLAVORMAP_PROPERTIES_LINUX = REG_EX_FOR_PATH_TO_FILE_FOR_LINUX_PART_1
			+ Constants.COMPILER_FILE_FLAVORMAP_PROPERTIES + REG_EX_FOR_PATH_TO_FILE_FOR_LINUX_PART_2;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * RadioButtony, dle kterých poznám, zda se má nastavit buď domovský adresář pro
	 * Javu, nebo se zkopírovat nějaké soubory do adrsáře libTools v aplikaci
	 */
	private JRadioButton rbSetJavaHomeDir, rbCopySomeFiles, rbSetDefault;
	
	
	
	
	/**
	 * Panely, ve kterých budou umístěny komponenty pro bud nastavení domovského
	 * adresáře Javy nebo na zkopírování souborů - dle toho, který radioButton bude
	 * zaškrtnutý, tak se povolí příslušný panel
	 */
	private JPanel pnlJavaHomeDir, pnlCopySomeFiles;
	
	
	
	
	// Labely, do kterých se vloží "informační text", ohledně toho, co se má do
	// příslušných polí u textu vložit za údaje
	private JLabel lblJavaHomeDirInfo, lblJavaHomeLibDirInfo, lblFilesInfo, lblCopySomeFilesInfo,
			// Labely s informacemi o souborech, které se zkopírují:
			lblCopyToolsJar, lblCopyCurrencyData, lblCopyFlavormapProperties;
	
	
	
	
	// Textová pole, kde se zobrazí cesta, kterou uživatel zvolí, a kde ji bude moci snadno přepsat v případě chybně zvoleného
	// cíle, apod. Tyto pole budou ukazovat cestu k zvolenému souboru pro zkopírování nebo adresáři s Javou
	private JTextField txtJavaHomeDir, txtCopyToolsJar, txtCopyCurrencyData, txtCopyFlavormapProperties;
	
	
	
	// Tlačítka, která umožňují otevřít dialog pro výběr adresáře či souboru pro zkopírování, apod.
	private JButton btnSelectJavaHomeDir, btnCopyToolsJar, btnCopyCurrencyData, btnCopyflavormapProperties;
	
	
	
	
	/**
	 * Proměnná pro uchování zvolené cesty z dialogu pro domovský adresář Javy:
	 */
	private String javaHomeDir;
	
	/*
	 * Proměnná pro uchování cesty k souboru currency.data:
	 */
	private String pathToCurrencyData;
	
	
	/*
	 * Proměnná pro ucvhování cesty k souboru tools.jar
	 */
	private String pathToToolsJar;
	
	
	/**
	 * Proměnná, do které se vloží zadaná cesta k souboru: flavormap.properties
	 */
	private String pathToFlavormapProp;
	
	
	
	
	// Proměnné pro texty do chybových hlášek:
	private static String txtFilesNotFoundText, txtFilesNotFoundTitle, txtBadFormatClassPathText,
			txtBadFormatClassPathTitle, txtIsNeededToFillPathText, txtIsNeededToFillPathTitle,

			txtMissingLibOrImportantFilesText, txtMissingLibOrImportantFilesTitle, txtChoosedDirDoesNotExistText,
			txtChoosedDirDoesNotExistTitle, txtPathIsNotInCorrectFormatText, txtPathIsNotInCorrectFormatTitle,
			txtPathToHomeDirIsEmptyText, txtPathToHomeDirIsEmptyTitle;
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 */
	public JavaHomeForm() {
		super();
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/app/JavaSettingsIcon.png", new Dimension(600, 580), true);
		
		
		gbc = createGbc();
		
		
		
		rbSetJavaHomeDir = new JRadioButton();
		rbCopySomeFiles = new JRadioButton();
		rbSetDefault = new JRadioButton();
		
		rbSetJavaHomeDir.addActionListener(this);
		rbCopySomeFiles.addActionListener(this);
		rbSetDefault.addActionListener(this);
		
		
		final ButtonGroup bg = new ButtonGroup();
		bg.add(rbCopySomeFiles);
		bg.add(rbSetJavaHomeDir);
		bg.add(rbSetDefault);
		
		rbSetJavaHomeDir.setSelected(true);		
		
		
		gbc.gridwidth = 2;
		
		setGbc(0, 0, rbSetJavaHomeDir);
		
		
		// vytvoření panelu s komponentami pro natavení cesty k domovskému adresáři:
		createJavaHomeDirPanel();
		
		setGbc(1, 0, pnlJavaHomeDir);
		
		
		
		
		
		
		setGbc(2, 0, rbCopySomeFiles);
		
		// Metoda pro vytvoření instance panelu pro zkopírování souborů do adresáře libTools v aplikace a k 
		// panelu příslušné komponenty:
		createCopySomeFilesPanel();
		
		
		setGbc(3, 0, pnlCopySomeFiles);
		
		enablePanel(pnlCopySomeFiles, false);
		
		
		
		
		
		
		setGbc(4, 0, rbSetDefault);
		
		
		
		
		
		
		// Panel s tlačtkami pro potvrzení dat nebo zrušení - zavření dialogu:
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		btnOk = new JButton();
		btnCancel = new JButton();
		
		btnOk.addActionListener(this);
		btnCancel.addActionListener(this);
		
		pnlButtons.add(btnOk);
		pnlButtons.add(btnCancel);
		
		
		setGbc(5, 0, pnlButtons);
		
		
		
		
		
		
		
		
		
		
		// Přidání poslucháčů textovým polím na stisknutí klávesy
		// vždy se otestuje, zda zadaná cesta odpovídá regulárnímu výrazu, pokud ne, pole se červeně zvýrazní:
		txtJavaHomeDir.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				testRegExInTextField(txtJavaHomeDir, DetectOS.getRegExByKindOfOs(
						REG_EX_FOR_PATH_TO_DIR_WINDOWS, REG_EX_FOR_PATH_TO_DIR_LINUX));
			}
			
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					testData();
				
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					variable = false;
					dispose();
				}
			}
		});
		
		
		
		
		
		txtCopyToolsJar.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				testRegExInTextField(txtCopyToolsJar, DetectOS.getRegExByKindOfOs(REG_EX_FOR_PATH_TO_TOOLS_JAR_WINDOWS,
						REG_EX_FOR_PATH_TO_TOOLS_JAR_LINUX));
			}
			
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					testData();
				
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					variable = false;
					dispose();
				}
			}
		});
		
		
		
		
		txtCopyCurrencyData.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				testRegExInTextField(txtCopyCurrencyData, DetectOS.getRegExByKindOfOs(
						REG_EX_FOR_PATH_TO_CURRENCY_DATA_WINDOWS, REG_EX_FOR_PATH_TO_CURRENCY_DATA_LINUX));
			}
			
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					testData();
				
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					variable = false;
					dispose();
				}
			}
		});
		
		
		
		
		
		txtCopyFlavormapProperties.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				testRegExInTextField(txtCopyFlavormapProperties,
						DetectOS.getRegExByKindOfOs(REG_EX_FOR_PATH_TO_FLAVORMAP_PROPERTIES_WINDOWS,
								REG_EX_FOR_PATH_TO_FLAVORMAP_PROPERTIES_LINUX));
			}
			
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					testData();
				
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					variable = false;
					dispose();
				}
			}
		});
			
		
		
		
		
		// Musím znepřístupnit položku pro nastavení výchozího adresáře Javy v aplikaci, protože 
		// se mi nepodařilo nastavit tu cestu k adresáři ve vygenerované aplikaci (souiboru .jar)
		rbSetDefault.setEnabled(false);
		
		
		
		
		
		addWindowListener(getWindowsAdapter());
		
		
		
		pack();
		setLocationRelativeTo(null);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda je text v textvoém poli v parametru metody ve
	 * správné syntaxi, která odpovídá regulárnímu výrazu v parametru metody
	 * 
	 * Pokud text v daném textovém poli neodpovídá regulárnímu výrazu, tak jeho
	 * pozadí zčervená, poku odpovídá, tak zbělá (pokud je červené a naopak)
	 * 
	 * @param txtField
	 *            - textové pole, jehož text se testuje regulárním výrazem
	 * 
	 * @param REG_EX
	 *            - regulární výraz, kterým se testuje, zda odpovídá tomuto výrazu
	 *            text v textovém poli
	 */
	private static void testRegExInTextField(final JTextField txtField, final String REG_EX) {
		if (txtField.getText().isEmpty()) {
			if (!txtField.getBackground().equals(Color.WHITE))
				txtField.setBackground(Color.WHITE);
		}
		
		else if (!txtField.getText().matches(REG_EX)) {
			if (!txtField.getBackground().equals(Color.RED))
				txtField.setBackground(Color.RED);
		}
			
		
		else {
			if (!txtField.getBackground().equals(Color.WHITE))
				txtField.setBackground(Color.WHITE);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zobrazí dialog a udělá ho modálním, po té, co uživatel klikne
	 * ne OK nebo zavře dialog se buď vrátí null nebo získané hodnoty z dialogu od
	 * uživatele
	 * 
	 * @return null - pokud uživtel ukončil dialog, jinak instance třídy
	 *         JavahomeInfo, kde jsou uloženy dle potřebby příslušné informace o
	 *         tom, co se má udělat, jestli nastavit cesta k domovskému adresáři
	 *         Javy nebo zkopírovat soubory na zadaných cestách - jiná část
	 */
	public final JavaHomeInfo getJavaHomeInfo() {
		setVisible(true);
		
		
		if (variable) {
			if (rbSetJavaHomeDir.isSelected())
				return new JavaHomeInfo(javaHomeDir);
			
			else if (rbCopySomeFiles.isSelected()) 
				return new JavaHomeInfo(pathToCurrencyData, pathToToolsJar, pathToFlavormapProp);
			
			// Zde je zaškrtnuta výchozí volba, tak nastavím klasicky ten adresář libTools:
			else return new JavaHomeInfo(Constants.COMPILER_DIR);
		}
		
		return null;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří instanci jak panelu, tak i jeho komponent potřebných
	 * pro zkopírování souborů - tj. knihovna tools.jar , ... výše zmíněné
	 */
	private void createCopySomeFilesPanel() {
		pnlCopySomeFiles = new JPanel(new GridBagLayout());
		
		final GridBagConstraints gbc = createGbc();
		
		index = 0;
		
		gbc.gridwidth = 2;
		
		// Label s informací o souborech pro zkopírování:
		lblCopySomeFilesInfo = new JLabel();
		setGbc(index, 0, pnlCopySomeFiles, lblCopySomeFilesInfo, gbc);
		
		
		
		
		// Komponenty pro výběr souboru tools.jar:
		// Label s informacemi o výběru souboru tools.jar:
		lblCopyToolsJar = new JLabel();
		setGbc(++index, 0, pnlCopySomeFiles, lblCopyToolsJar, gbc);
		
		
		gbc.gridwidth = 1;
		
		// pole pro zadání cesty k souboru tools.jar:
		txtCopyToolsJar = new JTextField();
		txtCopyToolsJar.setPreferredSize(new Dimension(DEFAULT_TXT_FIELD_WIDTH, DEFAULT_TXT_FIELD_HEIGHT));
		setGbc(++index, 0, pnlCopySomeFiles, txtCopyToolsJar, gbc);
		
		
		// tlačítko pro otevření dialogu pro výběr umístění souboru tools.jar:
		btnCopyToolsJar = new JButton();
		btnCopyToolsJar.addActionListener(this);
		setGbc(index, 1, pnlCopySomeFiles, btnCopyToolsJar, gbc);
		
		
		
		
		
		// Komponenty pro výběr souboru currency.data:
		// Label s informacemi o výběru souboru tools.jar:
		lblCopyCurrencyData = new JLabel();
		setGbc(++index, 0, pnlCopySomeFiles, lblCopyCurrencyData, gbc);
		
		
		gbc.gridwidth = 1;
		
		// pole pro zadání cesty k souboru tools.jar:
		txtCopyCurrencyData = new JTextField();
		txtCopyCurrencyData.setPreferredSize(new Dimension(DEFAULT_TXT_FIELD_WIDTH, DEFAULT_TXT_FIELD_HEIGHT));
		setGbc(++index, 0, pnlCopySomeFiles, txtCopyCurrencyData, gbc);
		
		
		// tlačítko pro otevření dialogu pro výběr umístění souboru tools.jar:
		btnCopyCurrencyData = new JButton();
		btnCopyCurrencyData.addActionListener(this);
		setGbc(index, 1, pnlCopySomeFiles, btnCopyCurrencyData, gbc);
		
		
		
		
		
		
		// Komponenty pro výběr souboru FlavormapProperties :
		// Label s informacemi o výběru souboru tools.jar:
		lblCopyFlavormapProperties = new JLabel();
		setGbc(++index, 0, pnlCopySomeFiles, lblCopyFlavormapProperties, gbc);
		
		
		gbc.gridwidth = 1;
		
		// pole pro zadání cesty k souboru tools.jar:
		txtCopyFlavormapProperties = new JTextField();
		txtCopyFlavormapProperties.setPreferredSize(new Dimension(DEFAULT_TXT_FIELD_WIDTH, DEFAULT_TXT_FIELD_HEIGHT));
		setGbc(++index, 0, pnlCopySomeFiles, txtCopyFlavormapProperties, gbc);
		
		
		// tlačítko pro otevření dialogu pro výběr umístění souboru tools.jar:
		btnCopyflavormapProperties = new JButton();
		btnCopyflavormapProperties.addActionListener(this);
		setGbc(index, 1, pnlCopySomeFiles, btnCopyflavormapProperties, gbc);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří instanci panelu pro s komponentami pro nastaavení
	 * domovského adresáře Javy včetně inicializace všech potřebných komponent,
	 * apod. - více viz tělo metody
	 */
	private void createJavaHomeDirPanel() {
		// instanciace panelu a nastavení layoutu pro rozvržení komponent
		pnlJavaHomeDir = new JPanel(new GridBagLayout());		
		
		// Inicializac ekomponenty pro rozvržení kompoennt
		final GridBagConstraints gbc = createGbc();
		
		// Instance Labelu s informacemi o funkci výběru tlačítka a pole níže v panelu
		lblJavaHomeDirInfo = new JLabel();
		
				
		gbc.gridwidth = 2;
		setGbc(0, 0, pnlJavaHomeDir, lblJavaHomeDirInfo, gbc);
		
		
		lblJavaHomeLibDirInfo = new JLabel();
		setGbc(1, 0, pnlJavaHomeDir, lblJavaHomeLibDirInfo, gbc);
		

		lblFilesInfo = new JLabel();
		setGbc(2, 0, pnlJavaHomeDir, lblFilesInfo, gbc);
		
		
		gbc.gridwidth = 1;
		
		// Instance textového pole pro zobrazení a předělání cesty k domovskému adresáři Javy
		txtJavaHomeDir = new JTextField();
		txtJavaHomeDir.setPreferredSize(new Dimension(DEFAULT_TXT_FIELD_WIDTH + 50, DEFAULT_TXT_FIELD_HEIGHT));
		setGbc(3, 0, pnlJavaHomeDir, txtJavaHomeDir, gbc);
		
		
		// Inicializace tlačítka pro výběr umístění adresáře javy na disku
		btnSelectJavaHomeDir = new JButton();
		btnSelectJavaHomeDir.addActionListener(this);
		setGbc(3, 1, pnlJavaHomeDir, btnSelectJavaHomeDir, gbc);
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která prida komponentu v parametru do nějakého panelu na nějaké
	 * pozici dle layoutu
	 * 
	 * @param rowIndex
	 *            - index řádku pro umístění komponenty
	 * 
	 * @param columnIndex
	 *            - index sloupce pro umístění komponenty
	 * 
	 * @param pnl
	 *            - Jpanel, kam se má umístit komponenta
	 * 
	 * @param component
	 *            - komponenta, která se má umístit do panelu na index řádku a
	 *            sloupce
	 * 
	 * @param gbc
	 *            - komponenta, ve které se určuje umístění komponenty pro vložení,
	 *            resp. jedná se o komponentu pro rozvržení
	 */
	private static void setGbc(final int rowIndex, final int columnIndex, final JPanel pnl,
			final JComponent component, final GridBagConstraints gbc) {
		gbc.gridx = columnIndex;
		gbc.gridy = rowIndex;
		pnl.add(component, gbc);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	





	@Override
	public void setLanguage(final Properties properties) {
        final String pathInfo = " (..." + File.separator + "Java" + File.separator + "jreX.Y...):";
        final String pathToLib = "Workspace" + File.separator + Constants.CONFIGURATION_NAME + File.separator +
                "libTools" + File.separator + "lib" + File.separator + "xxx";
		
		if (properties != null) {
			setTitle(properties.getProperty("Jhf_DialogTitle", Constants.JHF_DIALOG_TITLE));
			// Radiobuttony s výběrem, co se má udělat:
			rbSetJavaHomeDir.setText(properties.getProperty("Jhf_RbSetJavaHomeDir", Constants.JHF_RB_SET_JAVA_HOME_DIR));
			rbCopySomeFiles.setText(properties.getProperty("Jhf_RbCopySomeFiles", Constants.JHF_RB_COPY_SOME_FILES));
			rbSetDefault.setText(properties.getProperty("Jhf_RbSetDefault", Constants.JHF_RB_SET_DEFAULT) + ": '" + Constants.COMPILER_DIR + "'.");
			
			// Texty s popisky:
			lblJavaHomeDirInfo.setText(properties.getProperty("Jhf_LblJavaHomeDirInfo", Constants.JHF_LBL_JAVA_HOME_DIR_INFO));
			lblJavaHomeLibDirInfo.setText(properties.getProperty("Jhf_LblJavaHomeLibDirInfo", Constants.JHF_LBL_JAVA_HOME_LIB_DIR_INFO));
			lblCopySomeFilesInfo.setText(properties.getProperty("Jhf_LblCopySomeFilesInfo", Constants.JHF_LBL_COPY_SOME_FILES_INFO) + ":");
			
			
			// Texty do tlačítek pro zkopírování souborů a umísění domovského adresáře:
			btnSelectJavaHomeDir.setText(properties.getProperty("Jhf_BtnSelectJavaHomeDir", Constants.JHF_BTN_SELECT_JAVA_HOME_DIR));
			
			final String chooseText = properties.getProperty("Jhf_TextChooseText", Constants.JHF_TEXT_CHOOSE_TEXT);
			btnCopyToolsJar.setText(chooseText);
			btnCopyCurrencyData.setText(chooseText);
			btnCopyflavormapProperties.setText(chooseText);
			
			
			// Labely s informacemi o souborech, které se mají vybrat a které se následně zkopíruje:
			final String chooseFile = properties.getProperty("Jhf_TextChooseFile", Constants.JHF_TEXT_CHOOSE_FILE);
			
			lblCopyToolsJar.setText(chooseFile + ": \"" + Constants.COMPILER_FILE_TOOLS_JAR + "\":");
			lblCopyCurrencyData.setText(chooseFile + ": \"" + Constants.COMPILER_FILE_CURRENCY_DATA + "\":");
			lblCopyFlavormapProperties.setText(chooseFile + ": \"" + Constants.COMPILER_FILE_FLAVORMAP_PROPERTIES + "\":" );
			
			
			btnOk.setText(properties.getProperty("Jhf_Btn_OK", Constants.JHF_BTN_OK));
			btnCancel.setText(properties.getProperty("Jhf_Btn_Cancel", Constants.JHF_BTN_CANCEL));
						
			
			txtFilesNotFoundText = properties.getProperty("Jhf_TxtFilesNotFoundText", Constants.JHF_TXT_FILES_NOT_FOUND_TEXT);
			txtFilesNotFoundTitle = properties.getProperty("Jhf_TxtFilesNotFoundTitle", Constants.JHF_TXT_FILES_NOT_FOUND_TITLE);
			txtBadFormatClassPathText = properties.getProperty("Jhf_TxtBadFormatClassPathText", Constants.JHF_TXT_BAD_FORMAT_CLASS_PATH_TEXT);
			txtBadFormatClassPathTitle = properties.getProperty("Jhf_TxtBadFormatClassPathTitle", Constants.JHF_TXT_BAD_FORMAT_CLASS_PATH_TITLE);
			txtIsNeededToFillPathText = properties.getProperty("Jhf_TxtIsNeededToFillPathText", Constants.JHF_TXT_IS_NEEDED_TO_FILL_PATH_TEXT);
			txtIsNeededToFillPathTitle = properties.getProperty("Jhf_TxtIsNeededToFillPathTitle", Constants.JHF_TXT_IS_NEEDED_TO_FILL_PATH_TITLE);
				
			txtMissingLibOrImportantFilesText = properties.getProperty("Jhf_TxtMissingLibOrImportantFilesText", Constants.JHF_TXT_MISSING_LIB_OR_IMPORTANT_FILES_TEXT);
			txtMissingLibOrImportantFilesTitle = properties.getProperty("Jhf_TxtMissingLibOrImportantFilesTitle", Constants.JHF_TXT_MISSING_LIB_OR_IMPORTANT_FILES_TITLE);
			txtChoosedDirDoesNotExistText = properties.getProperty("Jhf_TxtChoosedDirDoesNotExistText", Constants.JHF_TXT_CHOOSED_DIR_DOES_NOT_EXIST_TEXT);
			txtChoosedDirDoesNotExistTitle = properties.getProperty("Jhf_TxtChoosedDirDoesNotExistTitle", Constants.JHF_TXT_CHOOSED_DIR_DOES_NOT_EXIST_TITLE);
			txtPathIsNotInCorrectFormatText = properties.getProperty("Jhf_TxtPathIsNotInCorrectFormatText", Constants.JHF_TXT_PATH_IS_NOT_IN_CORRECT_FORMAT_TEXT);
			txtPathIsNotInCorrectFormatTitle = properties.getProperty("Jhf_TxtPathIsNotInCorrectFormatTitle", Constants.JHF_TXT_PATH_IS_NOT_IN_CORRECT_FORMAT_TITLE);
			txtPathToHomeDirIsEmptyText = properties.getProperty("Jhf_TxtPathToHomeDirIsEmptyText", Constants.JHF_TXT_PATH_TO_HOME_DIR_IS_EMPTY_TEXT);
			txtPathToHomeDirIsEmptyTitle = properties.getProperty("Jhf_TxtPathToHomeDirIsEmptyTitle", Constants.JHF_TXT_PATH_TO_HOME_DIR_IS_EMPTY_TITLE);	
		}
		
		
		else {
			setTitle(Constants.JHF_DIALOG_TITLE);
			
			// Radiobuttony s výběrem, co se má udělat:
			rbSetJavaHomeDir.setText(Constants.JHF_RB_SET_JAVA_HOME_DIR);
			rbCopySomeFiles.setText(Constants.JHF_RB_COPY_SOME_FILES);
			rbSetDefault.setText(Constants.JHF_RB_SET_DEFAULT + ": '" + Constants.COMPILER_DIR + "'.");
			
			// Texty s popisky:
			lblJavaHomeDirInfo.setText(Constants.JHF_LBL_JAVA_HOME_DIR_INFO);
			lblJavaHomeLibDirInfo.setText(Constants.JHF_LBL_JAVA_HOME_LIB_DIR_INFO);
			lblCopySomeFilesInfo.setText(Constants.JHF_LBL_COPY_SOME_FILES_INFO + ":");
			
			
			// Texty do tlačítek pro zkopírování souborů a umísění domovského adresáře:
			btnSelectJavaHomeDir.setText(Constants.JHF_BTN_SELECT_JAVA_HOME_DIR);
			
			final String chooseText = Constants.JHF_TEXT_CHOOSE_TEXT;
			btnCopyToolsJar.setText(chooseText);
			btnCopyCurrencyData.setText(chooseText);
			btnCopyflavormapProperties.setText(chooseText);
			
			
			// Labely s informacemi o souborech, které se mají vybrat a které se následně zkopíruje:
			final String chooseFile = Constants.JHF_TEXT_CHOOSE_FILE;
			
			lblCopyToolsJar.setText(chooseFile + ": \"" + Constants.COMPILER_FILE_TOOLS_JAR + "\":");
			lblCopyCurrencyData.setText(chooseFile + ": \"" + Constants.COMPILER_FILE_CURRENCY_DATA + "\":");
			lblCopyFlavormapProperties.setText(chooseFile + ": \"" + Constants.COMPILER_FILE_FLAVORMAP_PROPERTIES + "\":" );
			
			
			btnOk.setText(Constants.JHF_BTN_OK);
			btnCancel.setText(Constants.JHF_BTN_CANCEL);
						
			
			txtFilesNotFoundText = Constants.JHF_TXT_FILES_NOT_FOUND_TEXT;
			txtFilesNotFoundTitle = Constants.JHF_TXT_FILES_NOT_FOUND_TITLE;
			txtBadFormatClassPathText = Constants.JHF_TXT_BAD_FORMAT_CLASS_PATH_TEXT;
			txtBadFormatClassPathTitle = Constants.JHF_TXT_BAD_FORMAT_CLASS_PATH_TITLE;
			txtIsNeededToFillPathText = Constants.JHF_TXT_IS_NEEDED_TO_FILL_PATH_TEXT;
			txtIsNeededToFillPathTitle = Constants.JHF_TXT_IS_NEEDED_TO_FILL_PATH_TITLE;
				
			txtMissingLibOrImportantFilesText = Constants.JHF_TXT_MISSING_LIB_OR_IMPORTANT_FILES_TEXT;
			txtMissingLibOrImportantFilesTitle = Constants.JHF_TXT_MISSING_LIB_OR_IMPORTANT_FILES_TITLE;
			txtChoosedDirDoesNotExistText = Constants.JHF_TXT_CHOOSED_DIR_DOES_NOT_EXIST_TEXT;
			txtChoosedDirDoesNotExistTitle = Constants.JHF_TXT_CHOOSED_DIR_DOES_NOT_EXIST_TITLE;
			txtPathIsNotInCorrectFormatText = Constants.JHF_TXT_PATH_IS_NOT_IN_CORRECT_FORMAT_TEXT;
			txtPathIsNotInCorrectFormatTitle = Constants.JHF_TXT_PATH_IS_NOT_IN_CORRECT_FORMAT_TITLE;
			txtPathToHomeDirIsEmptyText = Constants.JHF_TXT_PATH_TO_HOME_DIR_IS_EMPTY_TEXT;
			txtPathToHomeDirIsEmptyTitle = Constants.JHF_TXT_PATH_TO_HOME_DIR_IS_EMPTY_TITLE;
		}
		
		setTitle("Java - " + getTitle());

		lblJavaHomeLibDirInfo.setText(lblJavaHomeLibDirInfo.getText() + ":");

        lblJavaHomeDirInfo.setText(lblJavaHomeDirInfo.getText() + pathInfo);
        lblCopySomeFilesInfo.setText(lblCopySomeFilesInfo.getText() + " " + pathToLib + ": ");

		lblFilesInfo.setText(Constants.COMPILER_FILE_TOOLS_JAR + ", " + Constants.COMPILER_FILE_CURRENCY_DATA + ", "
				+ Constants.COMPILER_FILE_FLAVORMAP_PROPERTIES);
	}









	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JRadioButton) {
			if (e.getSource() == rbSetJavaHomeDir) {
				enablePanel(pnlJavaHomeDir, true);
				enablePanel(pnlCopySomeFiles, false);
			}
			
			else if (e.getSource() == rbCopySomeFiles) {
				enablePanel(pnlJavaHomeDir, false);
				enablePanel(pnlCopySomeFiles, true);
			}
			
			else if (e.getSource() == rbSetDefault) {
				enablePanel(pnlJavaHomeDir, false);
				enablePanel(pnlCopySomeFiles, false);
			}
		}
		
		
		
		
		else if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnSelectJavaHomeDir) {
				/*
				 * Získám si text / cestu v poli pro zadání cesty k adresáři, kde se mají
				 * nacházet ty 3 soubory potřebné pro kompilaci.
				 */
				final String tempDir = txtJavaHomeDir.getText().trim();
				
				/*
				 * Zde otestuji, jestli zadaný adresář existuje a pokud ano, tak otevřu okno pro
				 * výběr adresáře v tom již zadaném adresáři a ne ve výchozím umístění.
				 */
				if (!tempDir.isEmpty() && ReadFile.existsDirectory(tempDir))
					javaHomeDir = fileChooser.getPathToJavaHomeDir(new File(tempDir));

				// Zde je třeba vybrat umístění k domovskému adresáři Javy někde na uživatelově
				// disku (otevřeno s výchozím umístěním - adresář, kde je nainstalovaná Java):
				else
					javaHomeDir = fileChooser.getPathToJavaHomeDir(null);
				
				
				if (javaHomeDir != null)
					// Nastavím cestu do textového pole:
					txtJavaHomeDir.setText(javaHomeDir);					
			}
			
			
			else if (e.getSource() == btnCopyToolsJar) {
				/*
				 * Načtu si text zadaný v textovém poli:
				 */
				final String toolsTemp = txtCopyToolsJar.getText().trim();

				/*
				 * Otestuji, zda soubor zadaný do textového pole existuje a pokud ano, dám na
				 * výběr do příslušného dialogu pro výběr souboru jako označený adresář ten
				 * adresář, kde senachází označený soubor a jako označený soubor ten označený
				 * soubor, i když bych mohl testovat jeho název a případně zvolit výchozí
				 * hodnoty apod.
				 */
				if (!toolsTemp.isEmpty() && ReadFile.existsFile(toolsTemp))
					pathToToolsJar = fileChooser.getPathToToolsJar(new File(toolsTemp));

				// Zde zadám výchozí hodnoty, uživatel ještě soubor nezadal nebo neexistuje.
				else
					pathToToolsJar = fileChooser.getPathToToolsJar(null);
				
				
				// Zde pouze nastavím zadanou cestu k souboru (pokud uživatel vybral soubor):
				if (pathToToolsJar != null)
					txtCopyToolsJar.setText(pathToToolsJar);
			}
			
			
			else if (e.getSource() == btnCopyCurrencyData) {
				/*
				 * Načtu si text zadaný v textovém poli:
				 */
				final String currencyDataTemp = txtCopyCurrencyData.getText().trim();

				/*
				 * Otestuji, zda soubor zadaný do textového pole existuje a pokud ano, dám na
				 * výběr do příslušného dialogu pro výběr souboru jako označený adresář ten
				 * adresář, kde senachází označený soubor a jako označený soubor ten označený
				 * soubor, i když bych mohl testovat jeho název a případně zvolit výchozí
				 * hodnoty apod.
				 */
				if (!currencyDataTemp.isEmpty() && ReadFile.existsFile(currencyDataTemp))
					pathToCurrencyData = fileChooser.getPathToCurrencyData(new File(currencyDataTemp));

				// Zde zadám výchozí hodnoty, uživatel ještě soubor nezadal nebo neexistuje.
				else
					pathToCurrencyData = fileChooser.getPathToCurrencyData(null);
				
				
				// Zde pouze nastavím zadanou cestu k souboru (pokud uživatel vybral soubor):
				if (pathToCurrencyData != null)
					txtCopyCurrencyData.setText(pathToCurrencyData);
			}
			
			
			else if (e.getSource() == btnCopyflavormapProperties) {
				
				/*
				 * Načtu si text zadaný v textovém poli:
				 */
				final String flavormapPropTemp = txtCopyFlavormapProperties.getText().trim();

				/*
				 * Otestuji, zda soubor zadaný do textového pole existuje a pokud ano, dám na
				 * výběr do příslušného dialogu pro výběr souboru jako označený adresář ten
				 * adresář, kde senachází označený soubor a jako označený soubor ten označený
				 * soubor, i když bych mohl testovat jeho název a případně zvolit výchozí
				 * hodnoty apod.
				 */
				if (!flavormapPropTemp.isEmpty() && ReadFile.existsFile(flavormapPropTemp))
					pathToFlavormapProp = fileChooser.getPathToFlavormapProp(new File(flavormapPropTemp));

				// Zde zadám výchozí hodnoty, uživatel ještě soubor nezadal nebo neexistuje.
				else
					pathToFlavormapProp = fileChooser.getPathToFlavormapProp(null);
				
				
				// Zde pouze nastavím zadanou cestu k souboru (pokud uživatel vybral soubor):				
				if (pathToFlavormapProp != null)
					txtCopyFlavormapProperties.setText(pathToFlavormapProp);
			}
			
			
			
			else if (e.getSource() == btnOk)
				testData();
			
			
			else if (e.getSource() == btnCancel) {
				variable = false;
				dispose();
			}
		}
	}












	/**
	 * Metoda, která otestuje data po klikutí na tlačítko OK nebo povrzení dat stisknutím enteru při
	 * zadávání dat, resp. po jejich zadání
	 * 
	 * Metoda otestuje, zda existují zadané soubory či adresáře, ..
	 * 
	 * Nejprve pokud je označeno, že se má nastavit vesta k domovskému adresáři, tak se otestuje,k zda existuje,
	 * zda obsahuje adresář lib a v něm hlavní tří soubory (tools.jar, currency.data a flavormap.properties), jinak 
	 * ohlásí uživateli chybové oznámení s informací o chybě, která nastala,
	 * 
	 * Dále pokud je zaškrtnuto zkopírování souborů do adresáře aplikace, tak stačí zadat cestu k výše zmíněným
	 * třem souborům nebo alespoň nějakému, které se má zkopírovat do adresáře libTools v apikaci - aby se nemusela
	 * nastavovat cesta k java adresáři na disku, ale měla aplikace vždy u sebe aktuální verzi pro kompilátor
	 */
	private void testData() {
		if (rbSetJavaHomeDir.isSelected()) {
			// Zde musím otestovat, zda je pole pro zadání cesty k adresáří Javy v pořádku - není prázdné a zvýrazněné:
			if (!txtJavaHomeDir.getText().replaceAll("\\s", "").isEmpty()) {
				javaHomeDir = txtJavaHomeDir.getText().trim();
				
				if (javaHomeDir.matches(DetectOS.getRegExByKindOfOs(REG_EX_FOR_PATH_TO_DIR_WINDOWS,
						REG_EX_FOR_PATH_TO_DIR_LINUX))) {
					// Nyní otestuji, zda vybraný adresář ještě existuje:
					final File fileDir = new File(javaHomeDir);
					
					if (fileDir.exists() && fileDir.isDirectory()) {
						// Na konec otestuji, zda zvolená složka obsahuje adresář lib a ten obsahuje minimálně soubory currency.data
						// tool.jar a flavormap.properties, pokud ne oznámí se to uživateli:
						final boolean existNeededFiles = existNeededFiles(fileDir);
						
						if (existNeededFiles) {
							// Zde pole není prázdné a je ve správném formátu a existuje, takže mohu vrátit hodnotu:													
							variable = true;
							dispose();									
						}
						else
							JOptionPane.showMessageDialog(this,
									txtMissingLibOrImportantFilesText + ": " + Constants.COMPILER_FILE_TOOLS_JAR + ", "
											+ Constants.COMPILER_FILE_FLAVORMAP_PROPERTIES + ", "
											+ Constants.COMPILER_FILE_CURRENCY_DATA + "!",
									txtMissingLibOrImportantFilesTitle, JOptionPane.ERROR_MESSAGE);
		
					}
					else
						JOptionPane.showMessageDialog(this, txtChoosedDirDoesNotExistText,
								txtChoosedDirDoesNotExistTitle, JOptionPane.ERROR_MESSAGE);
				}
				else
					JOptionPane.showMessageDialog(this, txtPathIsNotInCorrectFormatText,
							txtPathIsNotInCorrectFormatTitle, JOptionPane.ERROR_MESSAGE);
			}
			else
				JOptionPane.showMessageDialog(this, txtPathToHomeDirIsEmptyText, txtPathToHomeDirIsEmptyTitle,
						JOptionPane.ERROR_MESSAGE);
		}
		
		
		
		else if (rbCopySomeFiles.isSelected()) {
			// Zde musím otestovat, zda není alespoň jedno prázdné a zároveň zvýrazněné:
			if (!txtCopyToolsJar.getText().isEmpty() || !txtCopyCurrencyData.getText().isEmpty()
					|| !txtCopyFlavormapProperties.getText().isEmpty()) {
				pathToToolsJar = null;
				pathToCurrencyData = null;
				pathToFlavormapProp = null;
				
				boolean var = true;
				
				// Postup:
				// Potřebuji otstovat, zda není pole prázdné, pokud je, tak není co řešit a jdu dál, pokud není prázdné,
				// tak ho potřebuji otestovat, zda uživatel zadal správný formát pro cestu k danému souboru či adresáři
				
				// Zde není alespoň jedno prázdné, tak je mohu otstovat:						
				if (!txtCopyToolsJar.getText().replaceAll("\\s", "").isEmpty()) {
					if (txtCopyToolsJar.getText().matches(DetectOS.getRegExByKindOfOs(
							REG_EX_FOR_PATH_TO_TOOLS_JAR_WINDOWS, REG_EX_FOR_PATH_TO_TOOLS_JAR_LINUX)))
						pathToToolsJar = txtCopyToolsJar.getText().trim();

					else
						var = false;
				}
					
				
				if (!txtCopyCurrencyData.getText().replaceAll("\\s", "").isEmpty()) {
					if (txtCopyCurrencyData.getText().matches(DetectOS.getRegExByKindOfOs(
							REG_EX_FOR_PATH_TO_CURRENCY_DATA_WINDOWS, REG_EX_FOR_PATH_TO_CURRENCY_DATA_LINUX)))
						pathToCurrencyData = txtCopyCurrencyData.getText().trim();

					else
						var = false;
				}
					
				
				if (!txtCopyFlavormapProperties.getText().replaceAll("\\s", "").isEmpty()) {
					if (txtCopyFlavormapProperties.getText()
							.matches(DetectOS.getRegExByKindOfOs(REG_EX_FOR_PATH_TO_FLAVORMAP_PROPERTIES_WINDOWS,
									REG_EX_FOR_PATH_TO_FLAVORMAP_PROPERTIES_LINUX)))
						pathToFlavormapProp = txtCopyFlavormapProperties.getText().trim();

					else
						var = false;
				}
				
				
				if (var) {
					// Zde neobsahuje žádné pole chybný formát textu a alespoň jedno je vyplněno, v 
					// příslušných proměnných již mám naplněné hodnoty z polí, tak mohu otestovat, zda příslušné soubory ještě existují,
					// a pokud ano, tak je mohu zkopírovat či nastavit, jinak to oznámím:
					
					String filesMissing = "";
					
					if (pathToToolsJar != null) {
						final File f1 = new File(pathToToolsJar);
						
						if (!f1.exists() || !f1.isFile())
							filesMissing += f1.getAbsolutePath() + "\n";
					}
					
					
					if (pathToCurrencyData != null) {
						final File f1 = new File(pathToCurrencyData);
						
						if (!f1.exists() || !f1.isFile())
							filesMissing += f1.getAbsolutePath() + "\n";
					}
					
					
					if (pathToFlavormapProp != null) {
						final File f1 = new File(pathToFlavormapProp);
						
						if (!f1.exists() || !f1.isFile())
							filesMissing += f1.getAbsolutePath() + "\n";
					}
					
					
					if (filesMissing.equals("")) {
						// Zde je vše v pořádku:
						variable = true;
						dispose();
					}
					
					// Zde alespoň jeden soubor neexistuje, tak o tom informuji uživatle:
					else
						JOptionPane.showMessageDialog(this, txtFilesNotFoundText + ":\n" + filesMissing,
								txtFilesNotFoundTitle, JOptionPane.ERROR_MESSAGE);
				}	
				else
					JOptionPane.showMessageDialog(this, txtBadFormatClassPathText, txtBadFormatClassPathTitle,
							JOptionPane.ERROR_MESSAGE);
			}
			else
				JOptionPane.showMessageDialog(this, txtIsNeededToFillPathText, txtIsNeededToFillPathTitle,
						JOptionPane.ERROR_MESSAGE);
		}
		
		
		
		else {
			// Zde je zaškrtnuta výchozí možnost (rbSetDefault), tak mi stačí nastavit prmannou variable na true 
			// dokončit tak volání metody a vrátí se ta výchzí cesta do adresáře v aplikace - adresář libTools:
			variable = true;
			dispose();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se v zadaném adresáři coby adresář pro Javu
	 * vyskytují hlavně adresář lib a v něm ty tří soubory důležité pro kompilaci ->
	 * tools.jar, currency.data a flavormap.properties
	 * 
	 * @param file
	 *            - adresář, kde se mají nacházet výše zmíněné soubory
	 * 
	 * @return false, pokud nebude nalezen adreář lib nebo v něm alespoň jeden ze
	 *         zmíněncýh souborů, jinak pokud bude vše nalezeno, metoda vrátí true
	 */
	private static boolean existNeededFiles(final File file) {

        // Zde už nemusím testovat ,zda se jedná o adresář, to už vím, nyní si pouze vytáhnu všechny soubory a
        // adresáře v dané složce v parametru, podívám se, zda obsahuje adresář lib (pokud ne, oznámí se to), pokud ano,
        // tak v něm zkontroluji ty tři potřebné soubory, případně ohlásím jejich absenci:

        final String[] filesArray = file.list();

        if (filesArray == null)
            return false;

        for (final String s : filesArray) {
            if (s.equals("lib")) {
                final File fileLibDir = new File(file.getAbsolutePath() + File.separator + s);

                if (!fileLibDir.exists() || !fileLibDir.isDirectory())
                    continue;

                // Zde jsem našel adresář lib, který by měl obsahovat potřebné soubory, tak si opět zjistím jeho obsah
                // a otestuji existeni těch zmíněných souborů:
                final String[] filesArrayInLib = fileLibDir.list();

                if (filesArrayInLib != null) {
                    final List<String> listOfFilesAtLibDir = Arrays.asList(filesArrayInLib);

                    /*
                     * Otestuji existenci zmíněných souborů.
                     *
                     * Pokud se dostanu sem, tak v adresáři lib není jeden ze zmíněných tří požadovaných souborů
                     * (minimálně)
                     */
                    return listOfFilesAtLibDir.contains(Constants.COMPILER_FILE_TOOLS_JAR)
                            && listOfFilesAtLibDir.contains(Constants.COMPILER_FILE_CURRENCY_DATA)
                            && listOfFilesAtLibDir.contains(Constants.COMPILER_FILE_FLAVORMAP_PROPERTIES);
                }
            }
        }


        return false;
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která komponenty v panelu v parametru metody povolí nebo zakáže
	 * 
	 * @param panel
	 *            - panel, ve kterém se mají povolit nebo zakázat komponenty
	 * 
	 * @param enabled
	 *            - logická hodnota, pokud je true, mají se komponenty v panelu
	 *            povolit jinak zaákazat
	 */
	private static void enablePanel(final JPanel panel, final boolean enabled) {
		for (final Component c : panel.getComponents())
			c.setEnabled(enabled);
	}
}