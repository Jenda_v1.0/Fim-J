package cz.uhk.fim.fimj.forms;

/**
 * Tento výčet slouží pro určení datového typu hodnoty, kterou lze zadat to textového pole (JtextField) v dialozích pro
 * vytvoření nové instance a zavolání metody.
 * <p>
 * V těchto dialozích se vždy vytvoří přistlučný počet parametrů, který potřebuje metoda či konstruktor a pro určení a
 * zjištění na jaký datový typ mám naparsovat hodnotu v daném textvém poli využiji tento výčet
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public enum DataTypeOfJTextField {

    // Datové typy, na které lze přetypovat zadanou hodnotu v JTextfieldu v
    // zmíněných dialozích
    BYTE, SHORT, INT, LONG, FLOAT, DOUBLE, CHAR, STRING,


    // Následujicí hodnoty pro rozpoznání datových typů v Listu a v poli:
    // V podstat2 jsou se jedn8 o stejn0 szntaxi, tak nen9 d;vod pro4 ps8t yvl83t
    // szntaxi pro jednoroym2rn0 pole a List
    LIST_AND_ARRAY_BYTE, LIST_AND_ARRAY_SHORT, LIST_AND_ARRAY_INTEGER, LIST_AND_ARRAY_LONG, LIST_AND_ARRAY_FLOAT,
    LIST_AND_ARRAY_DOUBLE, LIST_AND_ARRAY_CHARACTER, LIST_AND_ARRAY_BOOLEAN, LIST_AND_ARRAY_STRING,


    // Následující výčtové typy jsou pro datový typ dvojrozměrného pole - aby bylo
    // poznat, na jaké datové typy
    // se májí přetypovat zadané hodnoty
    TWO_DIMENSIONAL_ARRAY_BYTE, TWO_DIMENSIONAL_ARRAY_SHORT, TWO_DIMENSIONAL_ARRAY_INTEGER,
    TWO_DIMENSIONAL_ARRAY_LONG, TWO_DIMENSIONAL_ARRAY_FLOAT, TWO_DIMENSIONAL_ARRAY_DOUBLE,
    TWO_DIMENSIONAL_ARRAY_CHARACTER, TWO_DIMENSIONAL_ARRAY_BOOLEAN, TWO_DIMENSIONAL_ARRAY_STRING
}