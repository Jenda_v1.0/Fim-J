package cz.uhk.fim.fimj.forms;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cz.uhk.fim.fimj.app.DetectOS;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.FileChooser;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.project_explorer.NameFormAbstract;

/**
 * Tato třída slouží jako dialog pro založení nového projektu v aplikaci. Obsahuje okna pro zadání názvu projektu, a kam
 * semá uožit
 * <p>
 * Tento dialog slouží pro zadání názvu projektu a jeho umístění
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class NewProjectForm extends SourceDialogForm implements LanguageInterface {

	private static final long serialVersionUID = 1L;


	
	private final JTextField txtName, txtPath;
	
	private final JButton btnBrowse;
	
	private final JLabel lblName, lblPlace, lblChangePath, lblWorkspace, lblProjectFolderExist;
	
	private static String pathToWorkspace, pathErrorTitle, pathErrorText, emptyFieldErrorTitle, emptyFieldErrorText,
			projectFolderExistTitle, projectFolderExistText;
	
	
	
	/**
	 * Konstruktor této třídy.
	 */
	public NewProjectForm() {
		super();
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/app/NewProjectIcon.png", new Dimension(570, 300), true);

		
		gbc = createGbc();
		
		index = 0;
		
		
		gbc.gridwidth = 3;
		lblProjectFolderExist = new JLabel();
		lblProjectFolderExist.setFont(ERROR_FONT);
		lblProjectFolderExist.setForeground(Color.RED);
		setGbc(index, 0, lblProjectFolderExist);
		lblProjectFolderExist.setVisible(false);
		
		
		
		
		gbc.gridwidth = 1;
		lblName = new JLabel();
		setGbc(++index, 0, lblName);
		
		
		
		gbc.gridwidth = 2;
		txtName = new JTextField(20);
		setGbc(index, 1, txtName);
		
		
		
		
		gbc.gridwidth = 3;
		lblWorkspace = new JLabel();
		setGbc(++index, 0, lblWorkspace);
		
		
		
		gbc.gridwidth = 3;
		lblChangePath = new JLabel();
		setGbc(++index, 0, lblChangePath);
		
		
		
		gbc.gridwidth = 1;
		lblPlace = new JLabel();
		setGbc(++index, 0, lblPlace);
		
		
		
		txtPath = new JTextField(25);
		setGbc(index, 1, txtPath);
		
		
		// Nastavím cestu do Workspace do pole pro umístění projektu:
		pathToWorkspace = ReadFile.getPathToWorkspace();
		txtPath.setText(pathToWorkspace);
		
		
		
		
		
		btnBrowse = new JButton();
		setGbc(index, 2, btnBrowse);
		
		
		
		
		// Tlačítka:
		final JPanel pnlButtons = new JPanel();
		pnlButtons.setLayout(new FlowLayout(FlowLayout.LEFT));
		btnOk = new JButton();
		btnCancel = new JButton();
		pnlButtons.add(btnOk);
		pnlButtons.add(btnCancel);
		
		setGbc(++index, 2, pnlButtons);
		
		
		addActionsListenersToButtons();
		
		addKeyListenerToTxtNameOfProject();
		
		addKeyListenerToTxtOfProjectPath();
		
		
		
		pack();
		setLocationRelativeTo(null);
		
		
		
		
		addWindowListener(getWindowsAdapter());
	}
	
	
	
	
	
	/**
	 * Metoda, která přidá reakci na klávesu enter - pro potvrzení projektu
	 */
	private void addKeyListenerToTxtOfProjectPath() {
		txtPath.addKeyListener(new KeyAdapter() {
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					testData();
				
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					variable = false;
					dispose();
				}
			}
			
			
			@Override
			public void keyReleased(KeyEvent e) {
				NameFormAbstract.highlightTextField(txtPath, null, DetectOS.getRegExByKindOfOs(
						REG_EX_FOR_PATH_TO_DIR_WINDOWS, REG_EX_FOR_PATH_TO_DIR_LINUX));
			}
		});
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí cestu k novému projektu
	 * 
	 * @return cesta k novému projektu
	 */
	public final String getPathToProject() {
		setVisible(true);		
		
		if (variable) {
			final String pathVariable = txtPath.getText().trim();
		
			if (pathVariable.endsWith(File.separator))
				return pathVariable.substring(0, pathVariable.length() - 1);
			
			return pathVariable;
		}
		return null;
	}

	
	
	
	
	
	
	/**
	 * Metroda, která přidá KeyListener na textové pole pro zadání názvu projektu
	 * když uživatel zadá název projektu, promítně se i do cesty ve složce zvolené
	 * jako Workspace
	 */
	private void addKeyListenerToTxtNameOfProject() {
		txtName.addKeyListener(new KeyAdapter() {
			
			@Override
			public void keyReleased(KeyEvent e) {
				txtPath.setText(pathToWorkspace + File.separator + txtName.getText());
				
				// Otestuji, zda se v daném adresáři již nevyskytuje projekt se stejným názvem:
				if (ReadFile.existsDirectory(pathToWorkspace + File.separator + txtName.getText().trim())
						&& !txtName.getText().replaceAll("\\s", "").isEmpty())
					lblProjectFolderExist.setVisible(true);
				
				else lblProjectFolderExist.setVisible(false);
				
				
				NameFormAbstract.highlightTextField(txtName, null, REGEX_FOR_FILE_NAME);
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					testData();
				
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					variable = false;
					dispose();
				}
			}
		});
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá události na tlačítka: btnOk, btnCancel a btnBrowse
	 */
	private void addActionsListenersToButtons() {
		btnOk.addActionListener(event -> testData());
				
		
		btnBrowse.addActionListener(event -> {
			// Zjistím si novou cestu a zapíšeu ji do pole:
			final String newPathToWorkspace = new FileChooser().getPathToNewProjectDirectory(pathToWorkspace);
			
			if (newPathToWorkspace != null) {
				pathToWorkspace = newPathToWorkspace;
				txtPath.setText(pathToWorkspace + File.separator + txtName.getText());
			}
		});
		
		
		
		// "Ukončím" dialog
		btnCancel.addActionListener(event -> {
			variable = false;
			dispose();
		});
	}

	
	
	
	
	
	
	/**
	 * Metoda, která otestuje data - kód v této metodě by mohl být jen na reakci po
	 * kliknutí na tlačítko OK, ale obě textová pole - pro název projektu i cesty
	 * reagují na klávesu enter, kdy se otestuje kó pro potvrzení nového projektu
	 */
	private void testData() {
		if (!txtName.getText().replaceAll("\\s", "").isEmpty() && !txtPath.getText().replaceAll("\\s", "").isEmpty()) {
			// Otestuji, zda se cesta k souboru nachází ve správném formátu:
			
			
			if (txtPath.getText().matches(DetectOS.getRegExByKindOfOs(REG_EX_FOR_PATH_TO_DIR_WINDOWS,
					REG_EX_FOR_PATH_TO_DIR_LINUX))) {
				// Zde je cesta k souboru ve správném formátu:
				// Otestuji, zda je zadán název projektu, který se v adresáře ještě nevyskytuje:
				if (!lblProjectFolderExist.isVisible()) {
					variable = true;
					dispose();
				}

				else
					JOptionPane.showMessageDialog(this, projectFolderExistText, projectFolderExistTitle,
							JOptionPane.ERROR_MESSAGE);
			}

			else
				JOptionPane.showMessageDialog(this, pathErrorText, pathErrorTitle, JOptionPane.ERROR_MESSAGE);
		}

		else
			JOptionPane.showMessageDialog(this, emptyFieldErrorText, emptyFieldErrorTitle, JOptionPane.ERROR_MESSAGE);
	}
	
	
	
	

	
	
	
	

	
	
	
	
	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			// Titulek apliakce
			setTitle(properties.getProperty("NpfDialogTitle", Constants.NPF_DIALOG_TITLE));
			// Tlačítka
			btnOk.setText(properties.getProperty("NpfButtonOk", Constants.NPF_BTN_OK));
			btnCancel.setText(properties.getProperty("NpfButtonCancel", Constants.NPF_BTN_CANCEL));
			btnBrowse.setText(properties.getProperty("NpfButtonBrowse", Constants.NPF_BTN_BROWSE) + "...");
			// Popisky
			lblName.setText(properties.getProperty("NpfLabelNameOfProject", Constants.NPF_LBL_NAME) + ": ");
			lblWorkspace.setText(properties.getProperty("NpfLabelWorkspace", Constants.NPF_LBL_WORKSPACE));
			lblChangePath.setText(properties.getProperty("NpfLabelChangePath", Constants.NPF_LBL_CHANGE_PATH));
			lblPlace.setText(properties.getProperty("NpfLabelPlace", Constants.NPF_LBL_PLACE) + ": ");
			
			// text do chybové hlášky:
			pathErrorText = properties.getProperty("NpfPathErrorText", Constants.NPF_PATH_ERROR_TEXT);
			pathErrorTitle = properties.getProperty("NpfPathErrorTitle", Constants.NPF_PATH_ERROR_TITLE);
			
			emptyFieldErrorTitle = properties.getProperty("NpfEmptyFieldErrorTitle", Constants.NPF_EMPTY_FIELD_ERROR_TITLE);
			emptyFieldErrorText = properties.getProperty("NpfEmptyFieldErrorText", Constants.NPF_EMPTY_FIELD_ERROR_TEXT);
			
			projectFolderExistText = properties.getProperty("NpfProjectFolderExistText", Constants.NPF_PROJECT_FOLDER_EXIST_TEXT);
			projectFolderExistTitle = properties.getProperty("NpfProjectFolderExistTitle", Constants.NPF_PROJECT_FOLDER_EXIST_TITLE);
			
			// text do popisku, že složka projektu s danm názvem již existuje:
			lblProjectFolderExist.setText(properties.getProperty("NpfProjectFolderExist", Constants.NPF_PROJECT_FOLDER_NAME_EXIST));
		}
		
		
		else {
			// Titulek apliakce
			setTitle(Constants.NPF_DIALOG_TITLE);
			// Tlačítka
			btnOk.setText(Constants.NPF_BTN_OK);
			btnCancel.setText(Constants.NPF_BTN_CANCEL);
			btnBrowse.setText(Constants.NPF_BTN_BROWSE + "...");
			// Popisky
			lblName.setText(Constants.NPF_LBL_NAME + ": ");
			lblWorkspace.setText(Constants.NPF_LBL_WORKSPACE);
			lblChangePath.setText(Constants.NPF_LBL_CHANGE_PATH);
			lblPlace.setText(Constants.NPF_LBL_PLACE + ": ");			
			
			pathErrorText = Constants.NPF_PATH_ERROR_TEXT;
			pathErrorTitle = Constants.NPF_PATH_ERROR_TITLE;
			emptyFieldErrorTitle = Constants.NPF_EMPTY_FIELD_ERROR_TITLE;
			emptyFieldErrorText = Constants.NPF_EMPTY_FIELD_ERROR_TEXT;
			
			projectFolderExistTitle = Constants.NPF_PROJECT_FOLDER_EXIST_TITLE;
			projectFolderExistText = Constants.NPF_PROJECT_FOLDER_EXIST_TEXT;
			
			// text do popisku, že složka projektu s danm názvem již existuje:
			lblProjectFolderExist.setText(Constants.NPF_PROJECT_FOLDER_NAME_EXIST);
		}
	}
}