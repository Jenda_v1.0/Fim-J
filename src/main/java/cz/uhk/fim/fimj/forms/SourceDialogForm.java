package cz.uhk.fim.fimj.forms;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;

import cz.uhk.fim.fimj.file.GetIconInterface;

/**
 * Tato třída slouží pouze jako základní kód pro formuláře, ve které se nachází metody pro základní vytvoření některých
 * komponent Gui, jako je například GridBagConstraint, metoda pro přidání kompnent do okna apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class SourceDialogForm extends JDialog {

	private static final long serialVersionUID = 1L;


	
	/**
	 * Výrazy pro zadané hodnoty v poli: V názvu reference mohou být velka a mala
	 * pismena bez diakritiky a cisly a _
	 */
	protected static final String REG_EX_FOR_REFERENCE = "^\\s*[a-zA-Z]\\w*\\s*$";
	

	
	
	/*
	 * Regulární výraz pro rozpznání cesty k adresáři, adresář může mít libovolný
	 * název, ale nikde na cestě, ani v názvu toho adresáře nesmí být následující
	 * znaky: ^, /, |, *, %, $, <, >, ?, :, ", =
	 * 
	 * A adresář, resp. ten cílový název adresáře hned za posledním zpětným lomítkem
	 * nesmí být mezera, ale pak za názvem už může. Hlavně nesmí končit zpětným
	 * lomítkem
	 */
	static final String REG_EX_FOR_PATH_TO_DIR_WINDOWS = "^\\s*\\w+\\s*:\\s*(\\\\[^/\\|\\*\\%\\&<>\\?:\"#=]+)(\\\\[^/\\|\\*\\%\\&<>\\?:\"#=]+)*\\s*$";
	
	
	/**
	 * Regulární výraz pro rozpoznání syntaxe k cestě k nějakému adresáři, cesta
	 * může končit libovolným znakem kromě normálního lomítka. Resp. název toho
	 * adresáře může být cokoliv kromě normálního lomítka.
	 * 
	 * Například:
	 * 
	 * / Users/ krunc/Desktop/src/Package/dirName
	 */
	static final String REG_EX_FOR_PATH_TO_DIR_LINUX = "^\\s*(/[^/]+)+\\s*$";
	
	
	
	
	


	/**
	 * Proměnná coby regulární výraz, kterému musí odpovídat zadaný název souboru.
	 * Jedná o text, resp. název souboru bez mezer a bez speciálních znaků - pouze
	 * velká a malá písmena, číslice a podtržítka.
	 * 
	 * Note:
	 * tuto proměnnou používají akorát dialog v průzkumníku projektu - dialogy:
	 * NewFileForm.java a NewExcelListNameForm.java pro zadání názvu bud nového
	 * souboru nebo nového listu v excleovském sešitu.
	 */
	protected static final String REGEX_FOR_FILE_NAME = "^\\s*[\\wáÁčČďĎěéÉĚíÍňŇóÓřŘšŠťŤúÚůŮýÝžŽ]+\\s*$";
	
	
	
	
	
	/**
	 * Tato proměnná coby font je zde definován pro ostatní dialogy, které obsahují
	 * nějaký text, který informuje o nastale chybě
	 */
	protected static final Font ERROR_FONT = new Font("Default font for error text in Jlabel", Font.BOLD, 13);
	
	
	
	
	
	/**
	 * Tato proměnná slouží pro indexování řádků, kam se má přidat nějaká komponenta
	 * dle umístění na příslušném řádku v gbc.
	 */
	protected int index;
	
	
	
	protected GridBagConstraints gbc;
	
	
	/**
	 * Logická proměnné, do které vložím true / false, dle toho, zda se má vrátit
	 * nejaká data z daného dialogu / formáláře nebo ne, false = nemají se vrátit
	 * žádná data, dialog byl zavřen křížkem nebo klikmnutím na tlačítko Zrušit true
	 * = mají se vrátít data z dialogu - kliknutí na OK, nebo potvrzení Enterem
	 * (pokud jsou posluchači na klávesy)
	 */
	protected boolean variable;
	
	
	
	
	// Tlačítka pro potvrzení či uzavření dialogu: 
	protected JButton btnOk;
	protected JButton btnCancel;
	
	
	
	
	/**
	 * Metoda, která vytvoří instanci objktu GridBagConstraint pro rozvržení
	 * komponent a doplní mu výchozí nastavení, které potřebui
	 * 
	 * @return vrátí instanci výše zmíněného objektu se základním nastavením
	 */
	protected static GridBagConstraints createGbc() {
		final GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		
		gbc.insets = new Insets(5, 5, 5, 5);
		
		gbc.weightx = 0.2;
		gbc.weighty = 0.2;
		
		// Následující není povinné, je to myslím implicitně, ale kdybych to náhodou
		// v nějakém dialogu zapoměl změnit: ,..
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		
		return gbc;
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která nastaví výchozí "design" přísluěného dialogu "této" instance
	 * dialogu, konkrétně nastaví oknu - tomuto dialogu následující parametry.
	 * 
	 * @param closeOperation
	 *            - co se má státi po zavření dialogu, resp. způsob zavření dialogu
	 *            - končit aplikaci, nebo jen zavřít dialog, apod.
	 * 
	 * @param layoutManager
	 *            - způsob rozvržení komponent v dialogu
	 * 
	 * @param pathToIcon
	 *            - cesta k ikoně dialogu, syntaxe: /path/path.extension
	 * 
	 * @param dimension
	 *            - instance třídy Dimension, která definuje velikost dialogu -
	 *            resp. rozměry okna dialogu
	 * 
	 * @param modal
	 *            - logická proměnná o tom, zda má být dialog modální nebo ne, true
	 *            = je bude modální, false = nebude modální
	 */
	protected final void initGui(final int closeOperation, final LayoutManager layoutManager, final String pathToIcon,
			final Dimension dimension, final boolean modal) {

		setDefaultCloseOperation(closeOperation);
		setLayout(layoutManager);
		setIconImage(GetIconInterface.getMyIcon(pathToIcon, true).getImage());
		setPreferredSize(dimension);
		setModal(modal);
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vloží komponentu v parametru na zadnou pozici v dialogu - dle
	 * zadaných parametrů pro řádek a sloupec
	 * 
	 * @param rowIndex
	 *            - index řádku, na který se má vložit komponenta v posledním
	 *            parametru
	 * 
	 * @param columnIndex
	 *            - index sloupce, na který se má vložit komponenta v posledním
	 *            parametru
	 * 
	 * @param component
	 *            - komponenta, která se má vložit do dialogu na výše zadanou pozici
	 *            v layoutu pro GridBagConstraint
	 */
	protected final void setGbc(final int rowIndex, final int columnIndex, final JComponent component) {
		gbc.gridx = columnIndex;
		gbc.gridy = rowIndex;
		
		add(component, gbc);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí instanci WindowsAdapteru, který má definované dvě metoda,
	 * a sice při a na zavřené okna se nastaví proměnná variable na false díky čemuž
	 * se v konkrétním díalogu pozná, že se má vrátit null hodnotu (nebo něco, co
	 * značí, že se dialog užavřel a nekliklo na na tlačítko OK), nemají se vracet
	 * hodnoty z dialogu
	 * 
	 * @return výše popsaný windows adapter.
	 */
	protected final WindowAdapter getWindowsAdapter() {
		return new WindowAdapter() {
			// Při zavření dialogu musím nastavit promennou variable na false, aby se vrátilo null
			// protože uživatel dialog "zavřel" - a nechce potvrdit údaje - ale ukončit dialog
			// a díky tomu, že bude proměnna variable false, pak se vrátí null
			// jinak by se mohli provedly operace podobně jako po kliknutí na tlačítko OK
			
			@Override
			public void windowClosing(WindowEvent e) {
				variable = false;	
			}
			
			
			// Tuto metodu na zavření nepoužiju, protože, když například zavírám
			// dialog pro výběr workspacu, tak by se po zavolání metody dispose vrátilo null, 
			// protože by se příslušná proměnná variable nastavila na false
//			@Override
//			public void windowClosed(WindowEvent e) {
//				variable = false;
//				System.out.println("Closed");
//			}			
		};
	}
}