package cz.uhk.fim.fimj.forms;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Properties;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.menu.MainMethodInfo;

/**
 * Tato třída slouží jako dialog, ve kterém lze označit hlavní - spuštěcí metodu main, která se má spustit.
 * <p>
 * Tento dialog se zobrazí pouze v případě, že se v hlavním menu v okně aplikace v aktuálně otevřeném projektu nachází
 * více tříd, které obsahují metodu main
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class SelectMainMethodForm extends SourceDialogForm implements LanguageInterface, ActionListener {
	
	private static final long serialVersionUID = 1L;


	private final JLabel lblInfo1, lblInfo2;
	
	
	private final List<MainMethodInfo> mainMethodList;
	
	
	private MainMethodInfo selectedMainMethod;
	
	
	
	
	// Texty pro promenně:
	private static String txtIsNotChoosedClassText, txtIsNotChoosedClassTitle;
	
	
	
	
	
	/**
	 * Konstruktor, který se zavolá pro vytvoření instance této třídy, potřebuje
	 * jeden parametr, a sice, vytvořené instance třídy MainMethodInfo, ve které se
	 * nachází potřebné informace o třídách a main metodách, kterou uživatel označeí
	 * a ta se zavolá
	 * 
	 * @param mainMethodList
	 *            - list instancí třídy MainMethodinfo
	 */
	public SelectMainMethodForm(final List<MainMethodInfo> mainMethodList) {
		super();
		
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/app/SelectMainMethodForm.png", new Dimension(500, 400), true);
		setResizable(false);
		
		
		this.mainMethodList = mainMethodList;
		
		
		gbc = createGbc();
		
		index = 0;
		
		
		
		// Panel s informacemi o dialogu:
		final JPanel pnlInfo = new JPanel(new BorderLayout());
		
		lblInfo1 = new JLabel();
		lblInfo2 = new JLabel();		
		
		pnlInfo.add(lblInfo1, BorderLayout.NORTH);
		pnlInfo.add(lblInfo2, BorderLayout.CENTER);
		
		setGbc(index, 0, pnlInfo);
		
		
		
		
		
		
		
		
		
		
		// List pro označení hlavní metody:
		
		// Model do Jlistu:
		final DefaultListModel<MainMethodInfo> model = new DefaultListModel<>();
		
		// Jlist, do kterého vložím JChecboxy pro označení položky pro přidání jako parametru do výsledného konstruktoru
		final JList<MainMethodInfo> list = new JList<>(model);
		
		// Renderer, aby sli vykreslovat JRadioButtony:
		list.setCellRenderer(new JRadioButtonListCellRenderer());
	
		
		// Komponenta, do které vložím všechny načtené JRadioButtony, aby byl šel zaškrtnout vždy jen jeden
		// - vždy půjde zavolat pouze jedna metoda:
		final ButtonGroup bg = new ButtonGroup();
		
		// Přidám do modelu radibuttony coby reprezentace mainMetod
		for (final MainMethodInfo m : this.mainMethodList) {
			model.addElement(m);
			bg.add(m.getRbMain());
		}
			
		
		
		
		
		// Událost po kliknutí na JRadiobutton:
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				@SuppressWarnings("unchecked")
				final JList<MainMethodInfo> list = (JList<MainMethodInfo>) e.getSource();
				
				// Index položky, na kterou uživatel kikl
				final int index = list.locationToIndex(e.getPoint());
				
				// zjistím si položku (konkrétně mně zajímá JcheckBox), na kterou se klilo
				final MainMethodInfo item = list.getModel().getElementAt(index);
				
				// Změním její hodnotu 
				item.setRbMainSelected(!item.isRbMainSelected());
				
				// Překlreslení
				list.repaint(list.getCellBounds(index, index));
				
				repaint();
			}
		});
		
		
		
		final JScrollPane jspList = new JScrollPane(list);
		setGbc(++index, 0, jspList);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		// panel s tlačítky - OK a Zrušit:
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		btnOk = new JButton();
		btnCancel = new JButton();
		
		btnOk.addActionListener(this);
		btnCancel.addActionListener(this);
		
		pnlButtons.add(btnOk);
		pnlButtons.add(btnCancel);
		
		setGbc(++index, 0, pnlButtons);
		
		
		
		
		
		
		
		
		
		addWindowListener(getWindowsAdapter());
		
		
		pack();
		setLocationRelativeTo(null);
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která po zavolání zobrazí tento dialog a pokud se klikne na tlačítko
	 * OK, tak vrátí jednorozměrné pole, ve kterém se bude na prvním místě nacházet:
	 * označená metoda main a na druhém místě bude název třídy, ve které se nachází
	 * zmíněná metoda main:
	 * 
	 * @return výše zmíněné jednorozměrné pole se dvěma položkami, jinak null, pokud
	 *         se dialog jakkoli zavře
	 */
	public final Object[] getSelectedMainMethodInfo() {
		setVisible(true);
		
		if (variable)
			// Zde bylo kliknuto na OK:
			return new Object[] {selectedMainMethod.getMainMethod(), selectedMainMethod.getRbMain().getText()};
		
		return null;
	}
	
	
	
	
	

	
	
	
	
	
	
	
	


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnOk) {
				// Postup:
				// Otestuji, zda je nějaký JradioButton označený, pokud ne vypíšu hlášku,
				// pokud ano, tak si ho uložím do proměnné a mohu nastavit promennou variable na true a vrátit
				// označenou metodu:
				
				MainMethodInfo selectedMainInfo = null;
				
				for (final MainMethodInfo m : mainMethodList)
					if (m.isRbMainSelected()) {
						selectedMainInfo = m;
						break;
					}						
				
				
				if (selectedMainInfo != null) {
					// Zde je alespoň jedna položka označena:
					selectedMainMethod = selectedMainInfo;
					variable = true;
					dispose();
				}
				else
					JOptionPane.showMessageDialog(this, txtIsNotChoosedClassText, txtIsNotChoosedClassTitle,
							JOptionPane.ERROR_MESSAGE);
			}
			
			
			else if (e.getSource() == btnCancel) {
				variable = false;
				dispose();
			}
		}
	}


	
	
	

	@Override
	public void setLanguage(final Properties properties) {		
		if (properties != null) {
			setTitle(properties.getProperty("Smmf_DialogTitle", Constants.SMMF_DIALOG_TITLE));
			
			lblInfo1.setText(properties.getProperty("Smmf_LabelInfo_1", Constants.SMMF_LBL_INFO_1));
			lblInfo2.setText(properties.getProperty("Smmf_LabelInfo_2", Constants.SMMF_LBL_INFO_2));
			
			btnOk.setText(properties.getProperty("Smmf_Button_OK", Constants.SMMF_BTN_OK));
			btnCancel.setText(properties.getProperty("Smmf_Button_Cancel", Constants.SMMF_BTN_CANCEL));
			
			txtIsNotChoosedClassText = properties.getProperty("Smmf_TxtIsNotChoosedClassText", Constants.SMMF_TXT_IS_NOT_CHOOSED_CLASS_TEXT);
			txtIsNotChoosedClassTitle = properties.getProperty("Smmf_TxtIsNotChoosedClassTitle", Constants.SMMF_TXT_IS_NOT_CHOOSED_CLASS_TITLE);
		}
		
		else {
			setTitle(Constants.SMMF_DIALOG_TITLE);
			
			lblInfo1.setText( Constants.SMMF_LBL_INFO_1);
			lblInfo2.setText(Constants.SMMF_LBL_INFO_2);
			
			btnOk.setText(Constants.SMMF_BTN_OK);
			btnCancel.setText(Constants.SMMF_BTN_CANCEL);
			
			txtIsNotChoosedClassText = Constants.SMMF_TXT_IS_NOT_CHOOSED_CLASS_TEXT;
			txtIsNotChoosedClassTitle = Constants.SMMF_TXT_IS_NOT_CHOOSED_CLASS_TITLE;
		}
	}
}