package cz.uhk.fim.fimj.language;

import java.util.Locale;

/**
 * Rozhraní obsahuje pouze metody pro vytvoření jednotlivých jazyků
 * <p>
 * - Texty do apikace v různých jazycích
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface WriteLanguageInterface {

    /**
     * Metoda, která na uvedenou cestu zapíše CZ.properties Což je jazyk aplikace v češtině, soubor nebude obsahovat
     * komentáře.
     *
     * @param path
     *         - cesta, kam se má soubor zapsat syntaxe parametru bude path a automaticky se přidá: \\CZ.properteis
     * @param keysOrder
     *         - Výčtová hodnota, která je třeba, aby se dle toho řadily hodnoty v příslušném souboru .properties,
     *         který
     *         se vytvoří s texty v příslušném jazyce.
     * @param locale
     *         - "Globální umístění", dle kterého se vytvoří syntaxe datumu vytvoření souboru .properties s danými
     *         texty
     *         pro aplikaci.
     */
    void writeLanguageCZ(final String path, final KeysOrder keysOrder, final Locale locale);


    /**
     * Metoda, která na uvedenou cestu zapíše CZ.properties Což je jazyk aplikace v češtině, soubor bude obsahovat
     * komentáře.
     *
     * @param path
     *         - cesta, kam se má soubor zapsat syntaxe parametru bude path a automaticky se přidá: \\CZ.properteis
     * @param keysOrder
     *         - Výčtová hodnota, která je třeba, aby se dle toho řadily hodnoty v příslušném souboru .properties,
     *         který
     *         se vytvoří s texty v příslušném jazyce.
     * @param countOfFreeLinesBeforeComment
     *         - počet volných řádků nad komentřem (je třeba zadat pouze v případě, že se mají vkládat komentáře)
     * @param locale
     *         - "Globální umístění", dle kterého se vytvoří syntaxe datumu vytvoření souboru .properties s danými
     *         texty
     *         pro aplikaci.
     */
    void writeLanguageCZ(final String path, final KeysOrder keysOrder, final int countOfFreeLinesBeforeComment,
                         final Locale locale);


    /**
     * Metoda, která na uvedenou cestu zapíše EN.properties Což je jazyk aplikace v angličtině, soubor nebude obsahovat
     * komentáře.
     *
     * @param path
     *         - cesta, kam se má soubor zapsat syntaxe parametru bude path a automaticky se přidá: \\EN.properteis
     * @param keysOrder
     *         - Výčtová hodnota, která je třeba, aby se dle toho řadily hodnoty v příslušném souboru .properties,
     *         který
     *         se vytvoří s texty v příslušném jazyce.
     * @param locale
     *         - "Globální umístění", dle kterého se vytvoří syntaxe datumu vytvoření souboru .properties s danými
     *         texty
     *         pro aplikaci.
     */
    void writeLanguageEN(final String path, final KeysOrder keysOrder, final Locale locale);


    /**
     * Metoda, která na uvedenou cestu zapíše EN.properties Což je jazyk aplikace v angličtině, soubor bude obsahovat
     * komentáře.
     *
     * @param path
     *         - cesta, kam se má soubor zapsat syntaxe parametru bude path a automaticky se přidá: \\EN.properteis
     * @param keysOrder
     *         - Výčtová hodnota, která je třeba, aby se dle toho řadily hodnoty v příslušném souboru .properties,
     *         který
     *         se vytvoří s texty v příslušném jazyce.
     * @param countOfFreeLinesBeforeComment
     *         - počet volných řádků nad komentřem (je třeba zadat pouze v případě, že se mají vkládat komentáře)
     * @param locale
     *         - "Globální umístění", dle kterého se vytvoří syntaxe datumu vytvoření souboru .properties s danými
     *         texty
     *         pro aplikaci.
     */
    void writeLanguageEN(final String path, final KeysOrder keysOrder, final int countOfFreeLinesBeforeComment,
                         final Locale locale);
}