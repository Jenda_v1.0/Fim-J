package cz.uhk.fim.fimj.language;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Tato třída slouží pro objekt Properties, konkrétně pro třídu SortedProperties, kde je využita pro to, aby se při
 * zápisu souboru .properties přeskočily první dva řádky, které obsahují datum vytvoření daného souboru a potenciálně i
 * ten první - úvodní kmentář, který je volitelný.
 * <p>
 * Toto jsem napsal protože, chci dát uživateli možnost zvolit si Locale - "globální umístění", dle kterého se bude
 * formátovat datum vytvoření v daném souboru .properties, je zde možnost vždy dát výchozí dle JVM, ale ne každý
 * uživatel to tak chce.
 * <p>
 * Ty první dva řádky (komentář a ten datum vytvoření) se nezapíšou, protože je pak zapíšu jinou metodou, která na
 * začátek toho souboru zapíše ten komentář a hlavně ten datum vytvoření ale v zadaném formátu, sice by měl být stejný,
 * jako ten v objektu Properties, ale přizpůsoben zvolenému Locale, popř. výchozímu dle nastavení aplikace.
 * <p>
 * Zdroj:
 * <p>
 * https://stackoverflow.com/questions/6184335/properties-store-suppress-timestamp-comment
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class StripFirstLineStream extends FilterOutputStream {

    /**
     * Proměnná, dle které se pozná zápis "prvních" řádků.
     */
    private boolean firstLineseen = false;


    StripFirstLineStream(final OutputStream out) {
        super(out);
    }


    @Override
    public void write(final int b) throws IOException {
        if (firstLineseen)
            super.write(b);

        else if (b == '\n')
            firstLineseen = true;
    }
}
