package cz.uhk.fim.fimj.language;

import java.util.Locale;
import java.util.Properties;

import cz.uhk.fim.fimj.file.ConfigurationFiles;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.WriteToFile;

/**
 * Třída implementuje rozhraní, které obsahuje metody pro vytvoření souborů typu .properties, které obsahují každá texty
 * pro celou aplikace v nějakém jazyce
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class WriteLanguage implements WriteLanguageInterface {

	private static final String TITLE_CZ_LANGUAGE = "Texty pro aplikaci " + Constants.APP_TITLE + " v českém jazyce.";

	private static final String TITLE_EN_LANGUAGE = "Texts for the application " + Constants.APP_TITLE + " in english " +
			"language.";
	
	
	
	@Override
	public void writeLanguageCZ(final String path, final KeysOrder keysOrder, final Locale locale) {
		
		// Získám si texty pro aplikaci v českém jazyce:
		final Properties languageProp = Languages.getLanguage_CZ(keysOrder);
		
		/*
		 * Vytvoří se soubor .properties bez úvodních komentářů - tenprvní volitelný
		 * komentář a pod ním ten datum vytvoření.
		 * 
		 * Tyto dva komentáře se zapíšou až v metodě insertCreationComments, takto jsem
		 * to napsal kvůli možnosti zvolení Locale, proto se dle zvoleného Locale
		 * vytvoří datum vytvoření a zapíše se v požadovaném formátu do souboru, toto
		 * nelze ovlivnit nad objektem Properties jako takovým, proto bylo potřeba ve
		 * třídě SortedProperties přepsat metodu store, aby ty dva zmíněné komentáře
		 * nezapisovala a zapíše je až zmíněná metoda.
		 */
		
		// Zapíču soubor .propertiex na zadanou cestu path:
		WriteToFile.writeProperties(languageProp, path);

		// Zapíču do něj původní komentář a pod něj datum vytvoření:
		WriteToFile.insertCreationComments(path, TITLE_CZ_LANGUAGE, locale);
	}

	
	
	



	@Override
	public void writeLanguageCZ(String path, KeysOrder keysOrder, int countOfFreeLinesBeforeComment,
			final Locale locale) {
		/*
		 * Pro zápis komentářů je třeba zapsat soubor .properties s texty pro tuto
		 * aplikaci celkem 3 krát.
		 * 
		 * 1) Nejprve se "normálně" zapíše souboru pomocí metody writeProperties, který
		 * bude obsahovat zadané texty pro aplikaci. A první komentáře (pokud bude
		 * zadán) a na začátku souboru řádek s datumem vytvoření v příslušném formátu.
		 * 
		 * 2) Jaok druhý krok se zavolá metoda insertComments (pouze v případě, že se
		 * mají vložit komentáře - zvoleno v nastavení), a pomocí příslušných knihoven
		 * vloží do příslušného souboru .properties zadané komentáře, jenže toto má jednu
		 * vadu a sice, že přepíše již existující komentáře, takže ty první dva komentře
		 * s datumem vytvoření souboru a volitelným popiskem budou vymazány - pokud byly
		 * zadány.
		 * 
		 * 3) Tento krok je volitelný kvůli znovu zapsání výše zmíněných prvních dvou
		 * komentářů -> ten datum vytvoření a volitelný popisek. metoda
		 * insertCreationComments vloží na první dva řádky souboru datum vytvoření v
		 * příslušném formátu a volitelný komentář, které smazala metoda insertComments
		 * - vada knihoven.
		 */
		
		
		/*
		 * Note pro zápis prvních dvou řádků s komentáři:
		 * 
		 * Vytvoří se soubor .properties bez úvodních komentářů - tenprvní volitelný
		 * komentář a pod ním ten datum vytvoření.
		 * 
		 * Tyto dva komentáře se zapíšou až v metodě insertCreationComments, takto jsem
		 * to napsal kvůli možnosti zvolení Locale, proto se dle zvoleného Locale
		 * vytvoří datum vytvoření a zapíše se v požadovaném formátu do souboru, toto
		 * nelze ovlivnit nad objektem Properties jako takovým, proto bylo potřeba ve
		 * třídě SortedProperties přepsat metodu store, aby ty dva zmíněné komentáře
		 * nezapisovala a zapíše je až zmíněná metoda.
		 */
		
		
		final Properties languageProp = Languages.getLanguage_CZ(keysOrder);
		
		
		// Zapíšu soubor na cestu path
		WriteToFile.writeProperties(languageProp, path);
		
		// Vložím do něj zadané komentáře:
		ConfigurationFiles.insertComments(path, countOfFreeLinesBeforeComment);
		
		// Na začátek souboru zapíšu ten úvodní komentář o souboru a pod něj datum vytvořní:
		WriteToFile.insertCreationComments(path, TITLE_CZ_LANGUAGE, locale);
	}



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public void writeLanguageEN(final String path, final KeysOrder keysOrder, final Locale locale) {
		// Získám si texty pro aplikaci v anglickém jazyce:
		final Properties languageProp = Languages.getLanguage_EN(keysOrder);
				
		
		/*
		 * Vytvoří se soubor .properties bez úvodních komentářů - tenprvní volitelný
		 * komentář a pod ním ten datum vytvoření.
		 * 
		 * Tyto dva komentáře se zapíšou až v metodě insertCreationComments, takto jsem
		 * to napsal kvůli možnosti zvolení Locale, proto se dle zvoleného Locale
		 * vytvoří datum vytvoření a zapíše se v požadovaném formátu do souboru, toto
		 * nelze ovlivnit nad objektem Properties jako takovým, proto bylo potřeba ve
		 * třídě SortedProperties přepsat metodu store, aby ty dva zmíněné komentáře
		 * nezapisovala a zapíše je až zmíněná metoda.
		 */
		
		// Zapíšu soubor, kde se vytvoří datum dle nastaveného Locale:
		WriteToFile.writeProperties(languageProp, path);

		// Na začátek souboru zapíšu ten úvodní komentář o souboru a pod něj datum
		// vytvořní:
		WriteToFile.insertCreationComments(path, TITLE_EN_LANGUAGE, locale);
	}














	@Override
	public void writeLanguageEN(String path, KeysOrder keysOrder, int countOfFreeLinesBeforeComment,
			final Locale locale) {
		/*
		 * Pro zápis komentářů je třeba zapsat soubor .properties s texty pro tuto
		 * aplikaci celkem 3 krát.
		 * 
		 * 1) Nejprve se "normálně" zapíše souboru pomocí metody writeProperties, který
		 * bude obsahovat zadané texty pro aplikaci. A první komentáře (pokud bude
		 * zadán) a na začátku souboru řádek s datumem vytvoření v příslušném formátu.
		 * 
		 * 2) Jaok druhý krok se zavolá metoda insertComments (pouze v případě, že se
		 * mají vložit komentáře - zvoleno v nastavení), a pomocí příslušných knihoven
		 * vloží do příslušného souboru .properties zadané komentáře, jenže toto má jednu
		 * vadu a sice, že přepíše již existující komentáře, takže ty první dva komentře
		 * s datumem vytvoření souboru a volitelným popiskem budou vymazány - pokud byly
		 * zadány.
		 * 
		 * 3) Tento krok je volitelný kvůli znovu zapsání výše zmíněných prvních dvou
		 * komentářů -> ten datum vytvoření a volitelný popisek. metoda
		 * insertCreationComments vloží na první dva řádky souboru datum vytvoření v
		 * příslušném formátu a volitelný komentář, které smazala metoda insertComments
		 * - vada knihoven.
		 */
		
		/*
		 * Note pro zápis prvních dvou řádků s komentáři:
		 * 
		 * Vytvoří se soubor .properties bez úvodních komentářů - tenprvní volitelný
		 * komentář a pod ním ten datum vytvoření.
		 * 
		 * Tyto dva komentáře se zapíšou až v metodě insertCreationComments, takto jsem
		 * to napsal kvůli možnosti zvolení Locale, proto se dle zvoleného Locale
		 * vytvoří datum vytvoření a zapíše se v požadovaném formátu do souboru, toto
		 * nelze ovlivnit nad objektem Properties jako takovým, proto bylo potřeba ve
		 * třídě SortedProperties přepsat metodu store, aby ty dva zmíněné komentáře
		 * nezapisovala a zapíše je až zmíněná metoda.
		 */
		
		
		
		final Properties languageProp = Languages.getLanguage_EN(keysOrder);
		
		// Zapíšou soubor .properties na zadané umístění:
		WriteToFile.writeProperties(languageProp, path);
		
		// Vložím do něj zadané komentáře:
		ConfigurationFiles.insertComments(path, countOfFreeLinesBeforeComment);
		
		// Na začátek souboru zapíšu úvodní komentář a pod něj datum vytvoření v
		// příslušném formátu a globálním umístění:
		WriteToFile.insertCreationComments(path, TITLE_EN_LANGUAGE, locale);
	}
}