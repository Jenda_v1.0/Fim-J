package cz.uhk.fim.fimj.language;

import java.util.Arrays;
import java.util.Properties;

import cz.uhk.fim.fimj.file.ConfigurationFiles;
import cz.uhk.fim.fimj.file.Constants;

/**
 * Tato třída obsahuje pouze statické metody pro vytvoření objektů Properties, které obsahují texty pro aplikaci v
 * požadovaných jazycích.
 * <p>
 * Tato třída byla vytvořena pouze za účelem těchto metod, protože jsem se rozhodl přetížit metody pro zápis souborů s
 * texty kvůli komentářům, tj. zda se maí přidávat nebo ne, jinak by ty texty moly být v těch metodách, ale takto je to
 * alespoň trochu přehlednější, i když trochu "rozsáhledjší / složítější".
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class Languages {

    private Languages() {
    }

    /**
	 * Metoda, která vytvoří objekt Properties, a vloží do něj veškeré texty pro
	 * tuto aplikaci v jazyce českém.
	 * 
	 * @param keysOrder
	 *            - Způsob řazení klíčů - celkově textů v souboru .properties dle
	 *            klíčů.
	 * 
	 * @return výše popsaný objekt Properties, který obsahuje veškeré texty pro tuto
	 *         aplikaci v českém jazyce a s příslušným uspořádáním (keysOSf_Cd_Cdgp_Cd_IncludePrivateMethods_TTrder).
	 */
	public static Properties getLanguage_CZ(final KeysOrder keysOrder) {
		// Nejprve se vymaže mapa:
		ConfigurationFiles.commentsMap.clear();

		/*
		 * Zde by bylo možné využít pouze objekt Properties, ale já chci mít položky
		 * seřazené v souboru .properties dle jedné z možností, tak vužiji třídu
		 * SortedProperties, která obsahuje "přepsané" metody, díky kterým se položky
		 * předa zápisem do souboru seřadí dle potřeby.
		 */
//		final Properties languageCzProperties = new Properties();
				
		final SortedProperties languageCzProperties = new SortedProperties(keysOrder);
				
		

		
		// Texty pro tlačítka typu YES_OPTION a NO_OPTION v dialozích JOptionPane:
		ConfigurationFiles.commentsMap.put("JopTxtYesButton", "Texty pro tlačítka typu 'YES_OPTION' a 'NO_OPTION', která se nachází v dialozích typu JOptionPane. Jedná se pouze o tlačítka pro potvrzení nebo zamítnutí, jiné možnosti se v aplikaci nenacházejí.");
		languageCzProperties.setProperty("JopTxtYesButton", Constants.JOP_TXT_YES_BUTTON);
		languageCzProperties.setProperty("JopTxtNoButton", Constants.JOP_TXT_NO_BUTTON);
		
		
		
		
		
		
		
		
		
		// Texty do Ohraničení class diagramu a Instance diagramu:
		ConfigurationFiles.commentsMap.put("CdBorderTitle", "Titulek ohraníčení diagramu tříd");
		languageCzProperties.setProperty("CdBorderTitle", Constants.CD_BORDER_TITLE);
		ConfigurationFiles.commentsMap.put("IdBorderTitle", "Titulek ohraníčení diagramu instancí");
		languageCzProperties.setProperty("IdBorderTitle", Constants.ID_BORDER_TITLE);

		
		
		
		
		
		
		// Texty do třídy WorkspaceForm:
		ConfigurationFiles.commentsMap.put("Wf_BtnOk", "Texty do dialogu pro výběr adresáře označeného jako workspace.");
		languageCzProperties.setProperty("Wf_BtnOk", Constants.BTN_OK);
		languageCzProperties.setProperty("Wf_Btn_Cancel", Constants.BTN_CANCEL);
		languageCzProperties.setProperty("Wf_BtnBrowse", Constants.BTN_BROWSE);
		languageCzProperties.setProperty("Wf_Title", Constants.TITLE);
		languageCzProperties.setProperty("Wf_Lbl_Select_Workspace", Constants.LBL_SELECT_WORKSPACE);
		languageCzProperties.setProperty("Wf_Lbl_Info", Constants.LBL_INFO);
		languageCzProperties.setProperty("Wf_Lbl_Choose", Constants.LBL_CHOOSE);
		languageCzProperties.setProperty("Wf_Lbl_Workspace", Constants.LBL_WORKSPACE);
		languageCzProperties.setProperty("Wf_Chcb_Remember_Path", Constants.CHCB_REMEMBER_PATH);
		languageCzProperties.setProperty("Wf_Workspace_Path_Empty_Title", Constants.WORKSPACE_PATH_EMPTY_TITLE);
		languageCzProperties.setProperty("Wf_Workspace_Path_Empty_Text", Constants.WORKSPACE_PATH_EMPTY_TEXT);
		languageCzProperties.setProperty("Wf_Workspace_Path_Format_Title", Constants.WORKSPACE_PATH_FORMAT_TITLE);
		languageCzProperties.setProperty("Wf_Workspace_Path_Format_Text", Constants.WORKSPACE_PATH_FORMAT_TEXT);
				
				
		
		
		
		
				// Texty tlačítek do třídy Menu v balíčku jMenu:
		ConfigurationFiles.commentsMap.put("MnTxtProject", "Texty položek v menu v hlavním okně aplikace.");
		languageCzProperties.setProperty("MnTxtProject", Constants.TXT_PROJECT);
		languageCzProperties.setProperty("MnTxtEdit", Constants.TXT_EDIT);
		languageCzProperties.setProperty("MnTxtTools", Constants.TXT_TOOLS);
		languageCzProperties.setProperty("MnTxtHelp", Constants.TXT_HELP);
		languageCzProperties.setProperty("MnTxtNewProject", Constants.TXT_NEW_PROJECT);
		languageCzProperties.setProperty("MnTxtOpenProject", Constants.TXT_OPEN_PROJECT);
		languageCzProperties.setProperty("MnTxtRecentlyOpenedProjects", Constants.TXT_RECENTLY_OPENED_PROJECTS);
		languageCzProperties.setProperty("MnTxtDeleteRecentlyOpenedProjects", Constants.TXT_DELETE_RECENTLY_OPENED_PROJECTS);
        languageCzProperties.setProperty("MnTxtRb_RecentlyOpenedProjectsAsc", Constants.TXT_RB_SORT_RECENTLY_OPENED_PROJECTS_ASC);
        languageCzProperties.setProperty("MnTxtRb_RecentlyOpenedProjectsDes", Constants.TXT_RB_SORT_RECENTLY_OPENED_PROJECTS_DES);
		languageCzProperties.setProperty("MnTxtCloseProject", Constants.TXT_CLOSE_PROJECT);
		languageCzProperties.setProperty("MnTxtSave", Constants.TXT_SAVE);
		languageCzProperties.setProperty("MnTxtSaveAs", Constants.TXT_SAVE_AS);
		languageCzProperties.setProperty("MnTxtSwitchWorkspace", Constants.TXT_SWITCH_WORKSPACE);
		languageCzProperties.setProperty("MnTxtRestartApp", Constants.TXT_RESTART_APP);
		languageCzProperties.setProperty("MnTxtExit", Constants.TXT_EXIT);
		languageCzProperties.setProperty("MnTxtAddClassFromFile", Constants.TXT_ADD_CLASS_FROM_FILE);
		languageCzProperties.setProperty("MnTxtRemove", Constants.TXT_REMOVE);
		languageCzProperties.setProperty("MnTxtSaveImageOfClassDiagram", Constants.TXT_SAVE_IMAGE_OF_CLASS_DIAGRAM);
		languageCzProperties.setProperty("MnTxtSaveImageOfInstanceDiagram", Constants.TXT_SAVE_IMAGE_OF_INSTANCE_DIAGRAM);
		languageCzProperties.setProperty("MnTxtPrintImageOfClassDiagram", Constants.TXT_PRINT_IMAGE_OF_CLASS_DIAGRAM);
		languageCzProperties.setProperty("MnTxtPrintImageOfInstanceDiagram", Constants.TXT_PRINT_IMAGE_OF_INSTANCE_DIAGRAM);
		languageCzProperties.setProperty("MnTxtRefreshClassDiagram", Constants.TXT_REFRESH_CLASS_DIAGRAM);
		languageCzProperties.setProperty("MnTxtRefreshInstanceDiagram", Constants.TXT_REFRESH_INSTANCE_DIAGRAM);
		languageCzProperties.setProperty("MnTxtRemoveAll", Constants.TXT_REMOVE_ALL);
		languageCzProperties.setProperty("MnTxtRemoveInstanceDiagram", Constants.TXT_REMOVE_INSTANCE_DIAGRAM);
		languageCzProperties.setProperty("MnTxtCompile", Constants.TXT_COMPILE);
		languageCzProperties.setProperty("MnTxtOpenClass", Constants.TXT_OPEN_CLASS);		
		languageCzProperties.setProperty("MnTxtOpenInEditor", Constants.TXT_OPEN_IN_EDITOR);
		languageCzProperties.setProperty("MnTxtOpenClassInProjectExplorer", Constants.TXT_OPEN_CLASS_IN_PROJECT_EXPLORER);		
		languageCzProperties.setProperty("MnTxtShowOutputTerminal", Constants.TXT_SHOW_OUTPUT_TERMINAL);
		languageCzProperties.setProperty("MnTxtStart_CallTheMainMethod", Constants.TXT_START_CALL_THE_MAIN_METHOD);
		languageCzProperties.setProperty("MnTxtSettings", Constants.TXT_SETTINGS);
		languageCzProperties.setProperty("MnTxtFontSize", Constants.TXT_FONT_SIZE);
		languageCzProperties.setProperty("MnTxtJava", Constants.TXT_JAVA);
		languageCzProperties.setProperty("MnTxtJavaSettings", Constants.TXT_JAVA_SETTINGS);
		languageCzProperties.setProperty("MnTxtJavaSearch", Constants.TXT_JAVA_SEARCH);
		languageCzProperties.setProperty("MnTxtProjectExplorer", Constants.TXT_PROJECT_EXPLORER);
		languageCzProperties.setProperty("MnTxtFileExplorer", Constants.TXT_OPEN_IN_FILE_EXPLORER);
		languageCzProperties.setProperty("MnTxtProjectOpenProject", Constants.TXT_PROJECT_EXPLORER_PROJECT);
		languageCzProperties.setProperty("MnTxtProjectWorkspace", Constants.TXT_PROJECT_EXPLORER_WORKSPACE);
		languageCzProperties.setProperty("MnTxtFileExplorerWorkspace", Constants.TXT_FILE_EXPLORER_WORKSPACE);
		languageCzProperties.setProperty("MnTxtFileExplorerOpenProject", Constants.TXT_FILE_EXPLORER_PROJECT);
		languageCzProperties.setProperty("MnTxtInfo", Constants.TXT_INFO);
		languageCzProperties.setProperty("MnTxtAboutApp", Constants.TXT_ABOUT_APP);

		
		
		// Popisky k jednotlivým tlačítkům v menu: - Tool tip pro jednotlvé položky v menu:
		ConfigurationFiles.commentsMap.put("MnTtNewProject", "Popisky k položkám v menu v hlavním okně aplikace.");
		languageCzProperties.setProperty("MnTtNewProject", Constants.TT_NEW_PROJECT);
		languageCzProperties.setProperty("MnTtOpenProject", Constants.TT_OPEN_PROJECT);
		languageCzProperties.setProperty("MnTtDeleteRecentlyOpenedProjects", Constants.TT_DELETE_RECENTLY_OPENED_PROJECTS);
        languageCzProperties.setProperty("MnTtRb_SortRecentlyOpenedProjectsAsc", Constants.TT_RB_SORT_RECENTLY_OPENED_PROJECTS_ASC);
        languageCzProperties.setProperty("MnTtRb_SortRecentlyOpenedProjectsDes", Constants.TT_RB_SORT_RECENTLY_OPENED_PROJECTS_DES);
		languageCzProperties.setProperty("MnTtCloseProject", Constants.TT_CLOSE_PROJECT);
		languageCzProperties.setProperty("MnTtSave", Constants.TT_SAVE);
		languageCzProperties.setProperty("MnTtSaveAs", Constants.TT_SAVE_AS);
		languageCzProperties.setProperty("MnTtSwitchWorkspace", Constants.TT_SWITCH_WORKSPACE);
		languageCzProperties.setProperty("MnTtRestartApp", Constants.TT_RESTART_APP);
		languageCzProperties.setProperty("MnTtExit", Constants.TT_EXIT);
		languageCzProperties.setProperty("MnTtAddClassFromFile", Constants.TT_ADD_CLASS_FROM_FILE);
		languageCzProperties.setProperty("MnTtRemove", Constants.TT_REMOVE);
		languageCzProperties.setProperty("MnTtSaveImageOfClassDiagram", Constants.TT_SAVE_IMAGE_OF_CLASS_DIAGRAM);
		languageCzProperties.setProperty("MnTtSaveImageOfInstanceDiagram", Constants.TT_SAVE_IMAGE_OF_INSTANCE_DIAGRAM);
		languageCzProperties.setProperty("MnTtPrintImageOfClassDiagram", Constants.TT_PRINT_IMAGE_OF_CLASS_DIAGRAM);
		languageCzProperties.setProperty("MnTtPrintImageOfInstanceDiagram", Constants.TT_PRINT_IMAGE_OF_INSTANCE_DIAGRAM);
		languageCzProperties.setProperty("MnTtRefreshClassDiagram", Constants.TT_REFRESH_CLASS_DIAGRAM);
		languageCzProperties.setProperty("MnTtRefreshInstanceDiagram", Constants.TT_REFRESH_INSTANCE_DIAGRAM);
		languageCzProperties.setProperty("MnTtRemoveAll", Constants.TT_REMOVE_ALL);
		languageCzProperties.setProperty("MnTtRemoveInstanceDiagram", Constants.TT_REMOVE_INSTANCE_DIAGRAM);
		languageCzProperties.setProperty("MnTtCompile", Constants.TT_COMPILE);
		languageCzProperties.setProperty("MnTtOpenClass", Constants.TT_OPEN_CLASS);
		languageCzProperties.setProperty("MnTtOpenFileInProjectExplorer", Constants.TT_OPEN_FILE_IN_PROJECT_EXPLORER);
		languageCzProperties.setProperty("MnTtShowOutputTerminal", Constants.TT_SHOW_OUTPUT_TERMINAL);
		languageCzProperties.setProperty("MnTtStart_CallTheMainMethod", Constants.TT_START_CALL_THE_MAIN_METHOD);
		languageCzProperties.setProperty("MnTtSettings", Constants.TT_SETTINGS);
		languageCzProperties.setProperty("MnTtFontSize", Constants.TT_FONT_SIZE);
		languageCzProperties.setProperty("MnTtJava", Constants.TT_JAVA);		
		languageCzProperties.setProperty("MnTtJavaSearch", Constants.TT_ITEM_JAVA_SEARCH);
		languageCzProperties.setProperty("MnTtProjectExplorerWorkspace", Constants.TT_PROJECT_EXPLORER_WORKSPACE);
		languageCzProperties.setProperty("MnTtProjectExplorerProject", Constants.TT_PROJECT_EXPLORER_PROJECT);
		languageCzProperties.setProperty("MnTtFileExplorerWorkspace", Constants.TT_FILE_EXPLORER_WORKSPACE);
		languageCzProperties.setProperty("MnTtFileExplorerOpenProject", Constants.TT_FILE_EXPLORER_OPEN_PROJECT);
		languageCzProperties.setProperty("MnTtInfo", Constants.TT_INFO);
		languageCzProperties.setProperty("MnTtAboutApp", Constants.TT_ABOUT_APP);

		// Texty do chybových hlášek v metodě removeselectedObject() v třídě
		// WriteToFile.java v metodě removeSelectdObject:
		ConfigurationFiles.commentsMap.put("MnJopAssociationErrorTitle", "Texty do chybových hlášek, které se mohou zobrazit při pokusu o odebrání objektu z diagramu tříd.");
		languageCzProperties.setProperty("MnJopAssociationErrorTitle", Constants.MN_JOP_ASSOCIATE_ERROR_TITLE);
		languageCzProperties.setProperty("MnJopAssociationErrorText_1", Constants.MN_JOP_ASSOCIATE_ERROR_TEXT_JOP_1);
		languageCzProperties.setProperty("MnJopAssociationErrorText_2", Constants.MN_JOP_ASSOCIATE_ERROR_TEXT_JOP_2);
		languageCzProperties.setProperty("MnJopInheritErrorTitle", Constants.MN_JOP_INHERIT_ERROR_TITLE);
		languageCzProperties.setProperty("MnJopInheritErrorText", Constants.MN_JOP_INHERIT_ERROR_TEXT);
		languageCzProperties.setProperty("MnJopImplementsErrorTitle", Constants.MN_JOP_IMPLEMENTS_ERROR_TITLE);
		languageCzProperties.setProperty("MnJopImplementsErrorText", Constants.MN_JOP_IMPLEMENTS_ERROR_TEXT);
		languageCzProperties.setProperty("MnJopAggregation1_1_1_ErrorTitle", Constants.MN_JOP_AGGREGATION_1_1_ERROR_TITLE);
		languageCzProperties.setProperty("MnJopAggregation1_1_1_ErrorText", Constants.MN_JOP_AGGREGATION_1_1_TEXT);
		languageCzProperties.setProperty("MnJopMnJopAggregation1_1_N_ErrorText", Constants.MN_JOP_AGGREGATION_1_N_ERROR_TEXT);
		languageCzProperties.setProperty("MnJopSelectedObjectErrorTitle", Constants.MN_JOP_SELECTED_OBJ_ERROR_TITLE);
		languageCzProperties.setProperty("MnJopSelectedObjectErrorText", Constants.MN_JOP_SELECTED_OBJ_ERROR_TEXT);
		
		// Chybové hlášky po kliknutí na nějaké tlačítko v menu:
		ConfigurationFiles.commentsMap.put("MnJopMissingDirectoryErrorTitle", "Texty pro chybové hlášky, ke kterým může dojít po kliknutí na nějaké z tlačítek v menu v hlavním okně aplikce.");
		languageCzProperties.setProperty("MnJopMissingDirectoryErrorTitle", Constants.MN_JOP_MISSING_DIRECTORY_ERROR_TITLE);
		languageCzProperties.setProperty("MnJopMissingDirectoryErrorText", Constants.MN_JOP_MISSING_DIRECTORY_ERROR_TEXT);
		languageCzProperties.setProperty("MnJopIdenticalDirectoryErrorTitle", Constants.MN_JOP_IDENTICAL_DIR_ERROR_TITLE);
		languageCzProperties.setProperty("MnJopIdenticalDirectoryErrorText", Constants.MN_JOP_IDENTICAL_DIR_ERROR_TEXT);
		languageCzProperties.setProperty("MnJopMissingClassFileTitle", Constants.MN_JOP_MISSING_CLASS_FILE_TITLE);
		languageCzProperties.setProperty("MnJopMissingClassFileText", Constants.MN_JOP_MISSING_CLASS_FILE_TEXT);
				
		// Texty pro kompilaci nebo pokusu o nalazeni a zavolani metody main v tride v diagramu tříd:
		ConfigurationFiles.commentsMap.put("MnTxtMissingSrcDirText", "Texty pro chybové hlášky, ke kterým může dojít při pokusu o zkompilování třídy, případně při pokusu o spuštění otevřeného projektu.");
		languageCzProperties.setProperty("MnTxtMissingSrcDirText", Constants.MN_TXT_MISSING_SRC_DIR_TEXT);
		languageCzProperties.setProperty("MnTxtMissingSrcDirTitle", Constants.MN_TXT_MISSING_SRC_DIR_TITLE);
		languageCzProperties.setProperty("MnTxtErrorWhileCallTheMainMethod", Constants.MN_TXT_ERROR_WHILE_CALL_THE_MAIN_METHOD);
		languageCzProperties.setProperty("MnTxtClass", Constants.MN_TXT_CLASS);
		languageCzProperties.setProperty("MnTxtError", Constants.MN_TXT_ERROR);
		languageCzProperties.setProperty("MnTxtMethodIsNotAccessible", Constants.MN_TXT_METHOD_IS_NOT_ACCESSIBLE);
		languageCzProperties.setProperty("MnTxtIllegalArgumentExceptionPossibleErrors", Constants.MN_TXT_ILLEGAL_ARGUMENT_EXCEPTION_POSSIBLE_ERRORS);
		languageCzProperties.setProperty("MnTxtErrorWhileCallingMainMethod", Constants.MN_TXT_ERROR_WHILE_CALLING_MAIN_METHOD);
		languageCzProperties.setProperty("MnTxtMainMethodNotFoundText", Constants.MN_TXT_MAIN_METHOD_NOT_FOUND_TEXT);
		languageCzProperties.setProperty("MnTxtMainMethodNotFoundTitle", Constants.MN_TXT_MAIN_METHOD_NOT_FOUND_TITLE);
		languageCzProperties.setProperty("MnTxtChooseClassOrTextDocumentDialogTitle", Constants.MN_TXT_CHOOSE_CLASS_OR_TEXT_DOC_DIALOG_TITLE);

		ConfigurationFiles.commentsMap.put("Mn_TxtErrorWhilePrintingClassDiagramText", "Další texty pro chybové hlášky ke kterým může dojít po kliknutí na jednu z položek v menu v hlavním okně aplikace.");
		languageCzProperties.setProperty("Mn_TxtErrorWhilePrintingClassDiagramText", Constants.MN_TXT_ERROR_WHILE_PRINTING_CLASS_DIAGRAM_TEXT);
		languageCzProperties.setProperty("Mn_TxtErrorWhilePrintingClassDiagramTitle", Constants.MN_TXT_ERROR_WHILE_PRINTING_CLASS_DIAGRAM_TITLE);
		languageCzProperties.setProperty("Mn_TxtFailedToPrintClassDiagramText", Constants.MN_TXT_FAILED_TO_PRINT_CLASS_DIAGRAM_TEXT);
		languageCzProperties.setProperty("Mn_TxtClassDiagramText", Constants.MN_TXT_CLASS_DIAGRAM);
		languageCzProperties.setProperty("Mn_TxtFailedToPrintClassDiagramTitle", Constants.MN_TXT_FAILED_TO_PRINT_CLASS_DIAGRAM_TITLE);
		languageCzProperties.setProperty("Mn_TxtErrorWhilePrintingInstanceDiagramText", Constants.MN_TXT_ERROR_WHILE_PRINTING_INSTANCE_DIAGRAM_TEXT);
		languageCzProperties.setProperty("Mn_TxtErrorWhilePrintingInstanceDiagramTitle", Constants.MN_TXT_ERROR_WHILE_PRINTING_INSTANCE_DIAGRAM_TITLE);
		languageCzProperties.setProperty("Mn_TxtFailedToPrintInstanceDiagramText", Constants.MN_TXT_FAILED_TO_PRINT_INSTANCE_DIAGRAM_TEXT);
		languageCzProperties.setProperty("Mn_TxtInstanceDiagramText", Constants.MN_TXT_INSTANCE_DIAGRAM);
		languageCzProperties.setProperty("Mn_TxtfailedToPrintInstanceDiagramTitle", Constants.MN_TXT_FAILED_TO_PRINT_INSTANCE_DIAGRAM_TITLE);
		languageCzProperties.setProperty("Mn_TxtErrorWhileCopyFilesText", Constants.MN_TXT_ERROR_WHILE_COPY_FILES_TEXT);
		languageCzProperties.setProperty("Mn_TxtPossibleFileIsBeingUsed", Constants.MN_TXT_POSSIBLE_FILE_IS_BEING_USED);
		languageCzProperties.setProperty("Mn_TxtSourceText", Constants.MN_TXT_SOURCE_TEXT);
		languageCzProperties.setProperty("Mn_TxtDestinationText", Constants.MN_TXT_DESTINATION_TEXT);
		languageCzProperties.setProperty("Mn_TxtErrorWhileCopyFilesTitle", Constants.MN_TXT_ERROR_WHILE_COPY_FILES_TITLE);

		// Pro ProjectExplorerDialog - texty v menu:
		ConfigurationFiles.commentsMap.put("Mn_TxtPathToWorkspaceDoNotFoundText", "Texty pro chybové hlášky, ke kterým může dojít při pokusu o otevření dialogu průzkumník projektů.");
		languageCzProperties.setProperty("Mn_TxtPathToWorkspaceDoNotFoundText", Constants.MN_TXT_PATH_TO_WORKSPACE_DO_NOT_FOUND_TEXT);
		languageCzProperties.setProperty("Mn_TxtPathToWorkspaceDoNotFoundTitle", Constants.MN_TXT_PATH_TO_WORKSPACE_DO_NOT_FOUND_TITLE);
		languageCzProperties.setProperty("Mn_TxtDefaultPropInAppDoNotLoadText", Constants.MN_TXT_DEFAULT_PROP_IN_APP_DO_NOT_LOAD_TEXT);
		languageCzProperties.setProperty("Mn_TxtDefaultPropInAppDoNotLoadTitle", Constants.MN_TXT_DEFAULT_PROP_IN_APP_DO_NOT_LOAD_TITLE);

		// Menu: chybové hlášky:
		ConfigurationFiles.commentsMap.put("Mn_Txt_OpenDirDoesNotExistText", "Texty pro chybové hlášky, ke kterým může dojít při pokusu o otevření projektu v aplikaci. Mělo by se jednat o projekt vytvořený právě touto aplikací).");
		languageCzProperties.setProperty("Mn_Txt_OpenDirDoesNotExistText", Constants.MN_TXT_OPEN_DIR_DOES_NOT_EXIST_TEXT);
		languageCzProperties.setProperty("Mn_Txt_OriginalLocation", Constants.MN_TXT_ORIGINAL_LOCATION);
		languageCzProperties.setProperty("Mn_Txt_OpenDirDoesNotExistTitle", Constants.MN_TXT_OPEN_DIR_DOES_NOT_EXIST_TITLE);
		languageCzProperties.setProperty("Mn_Txt_ProjectIsNotOpenText", Constants.MN_TXT_PROJECT_IS_NOT_OPEN_TEXT);
		languageCzProperties.setProperty("Mn_Txt_ProjectIsNotOpenTitle", Constants.MN_TXT_PROJECT_IS_NOT_OPEN_TITLE);
		
		ConfigurationFiles.commentsMap.put("Mn_Txt_ClassDiagramDoesntContainClassTitle", "Další texty pro zkompilování tříd pomocí tlačítka v menu v hlavním okně aplikace.");
		languageCzProperties.setProperty("Mn_Txt_ClassDiagramDoesntContainClassTitle", Constants.MN_TXT_CLASS_DIAGRAM_DOESNT_CONTAIN_CLASS_TITLE);
		languageCzProperties.setProperty("Mn_Txt_ClassDiagramDoesntContainClassText", Constants.MN_TXT_CLASS_DIAGRAM_DOESNT_CONTAIN_CLASS_TEXT);

		// Dotaz na restart při změně workspace:
		ConfigurationFiles.commentsMap.put("Mn_Txt_SwitchWorkspaceJopRestartAppNow_Text", "Texty do dialogového okna pro dotaz na uživatele, zda si přeje aplikaci ihned restartovat.");
		languageCzProperties.setProperty("Mn_Txt_SwitchWorkspaceJopRestartAppNow_Text", Constants.MN_TXT_SWITCH_WORKSPACE_JOP_RESTART_APP_NOW_TEXT);
		languageCzProperties.setProperty("Mn_Txt_SwitchWorkspaceJopRestartAppNow_TT", Constants.MN_TXT_SWITCH_WORKSPACE_JOP_RESTART_APP_NOW_TT);

        // Hlášky pro nenalezené adresáře workspace nebo adresáře otevřeného projektu, když chce uživatel otevřít tyto adresáře v průzkumníku souborů:
        ConfigurationFiles.commentsMap.put("Mn_Txt_PathToWorkspaceWasNotFound_Text", "Texty do dialogového okna pro oznámení uživateli, že nebyl nalezen adresář workspace nebo adresář otevřeného projektu. Tato chyba může nastat při pokusu o otevření těchto adresářů v průzkumníku souborů v OS, ale tyto adresáře neexistují.");
        languageCzProperties.setProperty("Mn_Txt_PathToWorkspaceWasNotFound_Text", Constants.MN_TXT_PATH_TO_WORKSPACE_WAS_NOT_FOUND_TEXT);
        languageCzProperties.setProperty("Mn_Txt_PathToWorkspaceWasNotFound_Title", Constants.MN_TXT_PATH_TO_WORKSPACE_WAS_NOT_FOUND_TITLE);
        // Cesta k otevřenému projektu nebyla nalezena:
        languageCzProperties.setProperty("Mn_Txt_PathToOpenProjectWasNotFound_Text", Constants.MN_TXT_PATH_TO_OPEN_PROJECT_WAS_NOT_FOUND_TEXT);
        languageCzProperties.setProperty("Mn_Txt_PathToOpenProjectWasNotFound_Title", Constants.MN_TXT_PATH_TO_OPEN_PROJECT_WAS_NOT_FOUND_TITLE);

				
				
				
				
				
		// Třída ReadFile:
		
		// Třída ReadFile, metoda: getSelectLanguage(), text do výpisu o nenalezení požadovaného jazyka:
		ConfigurationFiles.commentsMap.put("JopErrorLanguageTitle", "Texty do chybových hlášek ve třídě ReadFile. K těmto chybovým hláškám může dojít při pokusu o čtení souboru .properties s texty pro aplikaci v nějakém jazyce.");
		languageCzProperties.setProperty("JopErrorLanguageTitle", Constants.JOP_ERROR_LANGUAGE_TITLE);
		languageCzProperties.setProperty("JopErrorLanguage1", Constants.JOP_ERROR_LANGUAGE_1);
		languageCzProperties.setProperty("JopErrorLanguage2", Constants.JOP_ERROR_LANGUAGE_2);
		
		
		// Texty pro inforamci pro uživatele, že byly znovu nakopírovány soubory s texty pro tuto aplikaci do configuration ve worspace:
		ConfigurationFiles.commentsMap.put("JopErrorLoadLanguage_Title", "Texty do oznamovací hlášky, která uživatele informuje, že byly soubory s texty pro tuto aplikaci zkopírovány do adresáře language v configuration ve workspace. Toto oznámení může nastat v případě, že uživatel například před spuštěním aplikace smazal potřebný soubor s texty pro tuto aplikaci a bylo tak potřeba je znovu vložit do uvedeného adresáře, aby se znovu načetly.");
		languageCzProperties.setProperty("JopErrorLoadLanguage_Title", Constants.JOP_ERROR_LOAD_LANGUAGE_TITLE);
		languageCzProperties.setProperty("JopErrorLoadLanguage_Text_1", Constants.JOP_ERROR_LOAD_LANGUAGE_TEXT_1);
		languageCzProperties.setProperty("JopErrorLoadLanguage_Text_2", Constants.JOP_ERROR_LOAD_LANGUAGE_TEXT_2);
		languageCzProperties.setProperty("JopErrorLoadLanguage_Text_3", Constants.JOP_ERROR_LOAD_LANGUAGE_TEXT_3);
		
		
		
		
		// Třída readfile, metoda getPathToSrcDirectory
		ConfigurationFiles.commentsMap.put("TacMissingProjectDirectoryText", "Texty pro chybové hlášky, ke kterým může dojít při získávání cesty k adresáři bin nebo src v otevřeném projektu.");
		languageCzProperties.setProperty("TacMissingProjectDirectoryText", Constants.TAC_MISSING_PROJECT_DIRECTORY_TEXT);
		languageCzProperties.setProperty("TacMissingProjectDirectoryTitle", Constants.TAC_MISSING_PROJECT_DIRECTORY_TITLE);
		
		// Třída ReadFile, metode getPathToBinDirectory:		
		// Třída ReadFile, metoda: getProperties()
		ConfigurationFiles.commentsMap.put("JopMissingFileTitle", "Texty pro chybové hlášky, které mohou nastat při čtení souboru .properties - při jeho čtení v metodě getProperties ve třídě ReadFile.");
		languageCzProperties.setProperty("JopMissingFileTitle", Constants.JOP_MISSING_FILE_ERROR_TITLE);
		languageCzProperties.setProperty("JopMissingFileText", Constants.JOP_MISSING_FILE_ERROR_TEXT);
		
		
				
				
		// Třída readFile, metoda getPathToBin:
		ConfigurationFiles.commentsMap.put("Rf_FailedToCreateBinDirText", "Další texty pro chybové hlášky, které mohou nastat při získávání cesty k adresáři bin v otevřeném projektu. třída ReadFile, metoda getPathTobin.");
		languageCzProperties.setProperty("Rf_FailedToCreateBinDirText", Constants.RF_TXT_FAILED_TO_CREATE_BIN_DIR_TEXT);
		languageCzProperties.setProperty("Rf_FailedToCreateBinDirTitle", Constants.RF_TXT_FAILED_TO_CREATE_BIN_DIR_TITLE);
				
				
				
				
				
				
				
				
				
				
				
				
				
				
		// Třída Grah.java v balíčku classDiagram, texty do chybových hlášek:
		ConfigurationFiles.commentsMap.put("CellErrorTitle", "Texty pro diagram tříd. Tyto texty jsou využity pro chyboá hlášení ohledně chybných vztahů mezi třídami, například pokud dojde k chybě při pokusu o vytvoření vztahu asociace mezi třídai, dědičnosti apod. Může se jednat například o chybné označení třídy či komentáře apod.");
		languageCzProperties.setProperty("CellErrorTitle", Constants.CD_CELL_ERROR_TITLE1);
		languageCzProperties.setProperty("CellErrorText", Constants.CD_CELL_ERROR_TEXT1);
		languageCzProperties.setProperty("CellErrorTitle2", Constants.CD_CELL_ERROR_TITLE2);
		languageCzProperties.setProperty("CellErrorText2", Constants.CD_CELL_ERROR_TEXT2);
		languageCzProperties.setProperty("CellErrorTitle3", Constants.CD_CELL_ERROR_TITLE3);
		languageCzProperties.setProperty("CellErrorText3", Constants.CD_CELL_ERROR_TEXT3);
		languageCzProperties.setProperty("CellErrorTitle4", Constants.CD_CELL_ERROR_TITLE4);
		languageCzProperties.setProperty("CellErrorText4", Constants.CD_CELL_ERROR_TEXT4);
		languageCzProperties.setProperty("CellErrorTitle5", Constants.CD_CELL_ERROR_TITLE5);
		languageCzProperties.setProperty("CellErrorText5", Constants.CD_CELL_ERROR_TEXT5);
		languageCzProperties.setProperty("CellErrorTitle6", Constants.CD_CELL_ERROR_TITLE6);
		languageCzProperties.setProperty("CellErrorText6", Constants.CD_CELL_ERROR_TEXT6);
		languageCzProperties.setProperty("CellErrorTitle7", Constants.CD_CELL_ERROR_TITLE7);
		languageCzProperties.setProperty("CellErrorText7", Constants.CD_CELL_ERROR_TEXT7);
		languageCzProperties.setProperty("CellErrorTitle8", Constants.CD_CELL_ERROR_TITLE8);
		languageCzProperties.setProperty("CellErrorText8", Constants.CD_CELL_ERROR_TEXT8);
		languageCzProperties.setProperty("CellErrorTitle9", Constants.CD_CELL_ERROR_TITLE9);
		languageCzProperties.setProperty("CellErrorText9", Constants.CD_CELL_ERROR_TEXT9);
		languageCzProperties.setProperty("CellErrorTitle10", Constants.CD_CELL_ERROR_TITLE10);
		languageCzProperties.setProperty("CellErrorText10", Constants.CD_CELL_ERROR_TEXT10);
		languageCzProperties.setProperty("Cd_EdgeErrorInAssociateWithOneClassTitle", Constants.CD_EDGE_ERROR_TITLE);
		languageCzProperties.setProperty("Cd_EdgeErrorInAssociateWithOneClassText", Constants.CD_EDGE_ERROR_TEXT);
		languageCzProperties.setProperty("Cd_ErrorMissingObjectInGraphTitle", Constants.CD_MISSING_OBJECT_IN_GRAPH_TITLE);
		languageCzProperties.setProperty("Cd_ErrorMissingObjectInGraphText", Constants.CD_MISSING_OBJECT_IN_GRAPH_TEXT);
		
		// Texty pro implementaci rozhraní - chyby:
		languageCzProperties.setProperty("Cd_Txt_ErrorWhileWritingInterfaceMethodsToClass_Text", Constants.CD_TXT_ERROR_WHILE_WRITING_INTERFACE_METHODS_TO_CLASS_TEXT);
		languageCzProperties.setProperty("Cd_Txt_ErrorWhileWritingInterfaceMethodsToClass_TT", Constants.CD_TXT_ERROR_WHILE_WRITING_INTERFACE_METHODS_TO_CLASS_TT);
		languageCzProperties.setProperty("Cd_Txt_Class", Constants.CD_TXT_CLASS);
		languageCzProperties.setProperty("Cd_Txt_AlreadyImplementsInterface", Constants.CD_TXT_ALREADY_IMPLEMENTS_INTERFACE);
		languageCzProperties.setProperty("Cd_Txt_AlreadyImplementsInterface_TT", Constants.CD_TXT_ALREADY_IMPLEMENTS_INTERFACE_TITLE);
		
				
				
				
		// Jazyk pro tlačítka v třídě ButtonsPanel, v balíčku: buttonsPanel:
		ConfigurationFiles.commentsMap.put("ButtonNewClass", "Texty do tlačítek, které se nachází v toolbaru v levé části hlavního okna aplikace. Jedná se o tlačítka pro vytvoření třídy, vztahů mezi nimi a komentáře.");
		languageCzProperties.setProperty("ButtonNewClass", Constants.BP_NEW_CLASS);
		languageCzProperties.setProperty("ButtonAddAssociation", Constants.BP_ADD_ASSOCIATION);
		languageCzProperties.setProperty("ButtonAddExtends", Constants.BP_ADD_EXTENDS);
		languageCzProperties.setProperty("ButtonAddImplements", Constants.BP_ADD_IMPLEMENTS);
		languageCzProperties.setProperty("ButtonComment", Constants.BP_ADD_COMMENT);
		languageCzProperties.setProperty("ButtonAggregation_1_ku_1", Constants.BP_ADD_AGGREGATION_1_KU_1);
		languageCzProperties.setProperty("ButtonAggregation_1_ku_N", Constants.BP_ADD_AGGREGATION_1_KU_N);
		
		// Popisky pro tlačítka v ButtonsPanelu:
		ConfigurationFiles.commentsMap.put("ButtonsNewClassToolTip", "Popisky pro tlačítka, která se nachází v toolbaru vlevé části hlavního okna aplikace. Tyto popisky se zobrazí po najetí kurzorem myši na tyto tlačítka. Jedná se o tlačítka pro vytvoření třídy, vztahů mezi nimi a komentáře.");
		languageCzProperties.setProperty("ButtonsNewClassToolTip", Constants.BP_NEW_CLASS_TOOL_TIP);
		languageCzProperties.setProperty("ButtonAddAssociationToolTip", Constants.BP_ADD_ASSOCIATION_TOOL_TIP);
		languageCzProperties.setProperty("ButtonAddExtendsToolTip", Constants.BP_ADD_EXTENDS_TOOL_TIP);
		languageCzProperties.setProperty("ButtonAddImplementsToolTip", Constants.BP_ADD_IMPLEMENTS_TOOL_TIP);
		languageCzProperties.setProperty("ButtonCommentToolTip", Constants.BP_BTN_COMMENT_TOOL_TIP);
		languageCzProperties.setProperty("ButtonAggregation_1_ku_1ToolTip", Constants.BP_BTN_AGGREGATION_1_KU_1_TOOL_TIP);
		languageCzProperties.setProperty("ButtonAggregation_1_ku_NToolTip", Constants.BP_BTN_AGGREGATION_1_KU_N_TOOL_TIP);
		
		
		
		
		
		
		
		
		// Texty do třídy PopupMenuRelationships, jedná se o textu a popisky
		// jednotlivých tlačítek v tomto kontextovém menu pro označneí vztahu pro
		// vytvoření mezi třídami v diagramu tříd:
		ConfigurationFiles.commentsMap.put("PPR_Item_AddClass_Text", "Texty do jednotlivých položek do kontextového menu, které se zobrazí po kliknutí pravým tlačítkem myši na pozadí diagramu tříd (aby nebyl označen žádný objekt). Jedná se o kontextové menu, které obsahuje stejná tlačítka jako toolbar.");
		languageCzProperties.setProperty("PPR_Item_AddClass_Text", Constants.PMR_ITEM_ADD_CLASS_TEXT);
		languageCzProperties.setProperty("PPR_Item_AddAssociation_Text", Constants.PMR_ITEM_ADD_ASSOCIATION_TEXT);
		languageCzProperties.setProperty("PPR_Item_AddExtends_Text", Constants.PMR_ITEM_ADD_EXTENDS_TEXT);
		languageCzProperties.setProperty("PPR_Item_AddImplements_Text", Constants.PMR_ITEM_ADD_IMPLEMENTS_TEXT);
		languageCzProperties.setProperty("PPR_Item_AddAggregation_1_ku_1_Text", Constants.PMR_ITEM_ADD_AGGREGATION_1_KU_1_TEXT);
		languageCzProperties.setProperty("PPR_Item_AddAggregation_1_ku_N_Text", Constants.PMR_ITEM_ADD_AGGREGATION_1_KU_N_TEXT);
		languageCzProperties.setProperty("PPR_Item_AddComment_Text", Constants.PMR_ITEM_ADD_COMMENT_TEXT);
		
		ConfigurationFiles.commentsMap.put("PPR_Item_AddClass_TT", "Popisky (tooltipy) do jednotlivých položek do kontextového menu, které se zobrazí po kliknutí pravým tlačítkem myši na pozadí diagramu tříd (aby nebyl označen žádný objekt). Jedná se o kontextové menu, které obsahuje stejná tlačítka jako toolbar.");
		languageCzProperties.setProperty("PPR_Item_AddClass_TT", Constants.PMR_ITEM_ADD_CLASS_TT);
		languageCzProperties.setProperty("PPR_Item_AddAssociation_TT", Constants.PMR_ITEM_ADD_ASSOCIATION_TT);
		languageCzProperties.setProperty("PPR_Item_AddExtends_TT", Constants.PMR_ITEM_ADD_EXTENDS_TT);
		languageCzProperties.setProperty("PPR_Item_AddImplements_TT", Constants.PMR_ITEM_ADD_IMPLEMENTS_TT);
		languageCzProperties.setProperty("PPR_Item_AddAggregation_1_ku_1_TT", Constants.PMR_ITEM_ADD_AGGREGATION_1_KU_1_TT);
		languageCzProperties.setProperty("PPR_Item_AddAggregation_1_ku_N_TT", Constants.PMR_ITEM_ADD_AGGREGATION_1_KU_N_TT);
		languageCzProperties.setProperty("PPR_Item_AddComment_TT", Constants.PMR_ITEM_ADD_COMMENT_TT);
		
		
		
		
		
		
		
				
				
				
				
				
				
				// Text pro FileChooser - titulky dialogu a chybové hlášky:
		ConfigurationFiles.commentsMap.put("FcSaveImageTitle", "Texty pro titulky dialogů pro výběr souborů a adresářů. Jedná se o texty pro dialog FileChooser.");
		languageCzProperties.setProperty("FcSaveImageTitle", Constants.FC_SAVE_IMAGE_TITLE);
		languageCzProperties.setProperty("FcFileErrorTitle", Constants.FC_FILE_ERROR_TITLE);
		languageCzProperties.setProperty("FcFileErrorText", Constants.FC_FILE_ERROR_TEXT);
		languageCzProperties.setProperty("FcNewProjectTitle", Constants.FC_NEW_PROJECT_TITLE);
		languageCzProperties.setProperty("FcChooseProjectDirectory", Constants.FC_CHOOSE_PROJECT_DIR);
		languageCzProperties.setProperty("FcChooseJavaClass", Constants.FC_CHOOSE_CLASS);
		languageCzProperties.setProperty("FcChooseFile", Constants.FC_CHOOSE_FILE);
		languageCzProperties.setProperty("FcChooseLocationAndKindOfFile", Constants.FC_CHOOSE_LOCATION_AND_KIND_OF_FILE);

		ConfigurationFiles.commentsMap.put("Fc_ChooseCurrencyData", "Texty pro chybové hlášky do dialogu pro výběr souborů a adresářů. Chybové hlášky mohou nastat v metodých v dialogu FileChooser, například při získávání cesty pro uložení obrázku jednoho s diagramů nebo při výběru cesty k adresáři nějakého projektu aplikace apod.");
		languageCzProperties.setProperty("Fc_ChooseCurrencyData", Constants.FC_CHOOSE_CURRENCY_DATA);
		languageCzProperties.setProperty("Fc_WrongFileTitle", Constants.FC_WRONG_FILE_TITLE);
		languageCzProperties.setProperty("Fc_WrongFileText", Constants.FC_WRONG_FILE_TEXT);
		languageCzProperties.setProperty("Fc_ChooseToolsJar", Constants.FC_CHOOSE_TOOLS_JAR);
		languageCzProperties.setProperty("Fc_ChooseFlavormapProp", Constants.FC_CHOOSE_FLAVORMAP_PROP);
		languageCzProperties.setProperty("Fc_ChooseJavaHomeDir", Constants.FC_CHOOSE_JAVA_HOME_DIR);
		languageCzProperties.setProperty("Fc_SelectWorkspace", Constants.FC_SELECT_WORKSPACE);
				
				
				
				
				
				
				
				
		// Text do třdy CompiledClassLoader - chyba při vytváření ClassLoader, resp.
		// konkrétně při konstrukci URL:
		ConfigurationFiles.commentsMap.put("Mcl_ErrorWhileCreatingURL", "Text pro chybovou hlášku, která se vypíše do editoru výstupů v případě, že při vytváření instance třídy CompiledClassLoader dojde k nějaké chybě.");
		languageCzProperties.setProperty("Mcl_ErrorWhileCreatingURL", Constants.MCL_ERROR_WHILE_CREATING_URL);

		// Chybové texty při načítání souboru .class:
		ConfigurationFiles.commentsMap.put("Mcl_ErrorWhileLoadingClassFile_1", "Texty do chybové hlášky, ke které dojde v případě chyby při pokusu o načtení zkompilované (/ přeložené) třídy do aplikace. Metoda loadClass ve třídě CompiledClassLoader.");
		languageCzProperties.setProperty("Mcl_ErrorWhileLoadingClassFile_1", Constants.MCL_ERROR_WHILE_LOADING_CLASS_FILE_1);
		languageCzProperties.setProperty("Mcl_ErrorWhileLoadingClassFile_2", Constants.MCL_ERROR_WHILE_LOADING_CLASS_FILE_2);
		
		// Texty pro chybové výpis, které mohou nastat při kontroly názvů s case sensitive:
		ConfigurationFiles.commentsMap.put("Mcl_Txt_PackageNotFound_1", "Texty pro chybové výpisy, na které dojde v případě, že se prohledává adresář 'bin' v aktuálně otevřeném projektu za účelem kontroly názvů balíčků a souboru .class (case - sensitive -> rozlišovat velikosti písmen), který se má načíst. Metoda loadClass ve třídě CompiledClassLoader.");
		languageCzProperties.setProperty("Mcl_Txt_PackageNotFound_1", Constants.MCL_TXT_PACKAGE_NOT_FOUND_1);
		languageCzProperties.setProperty("Mcl_Txt_PackageNotFound_2", Constants.MCL_TXT_PACKAGE_NOT_FOUND_2);
		languageCzProperties.setProperty("Mcl_Txt_InPackage", Constants.MCL_TXT_IN_PACKAGE);
		languageCzProperties.setProperty("Mcl_Txt_FileNotFound", Constants.MCL_TXT_FILE_NOT_FOUND);
				
				
				
				
				
				
				
				
				
				
				// Texty do NewProjectForm:
				// Titulek aplikace
		ConfigurationFiles.commentsMap.put("NpfDialogTitle", "Texty pro tlačítka v dialogu pro vytvoření nového projektu.");
		languageCzProperties.setProperty("NpfDialogTitle", Constants.NPF_DIALOG_TITLE);
		// Tlačítka:
		languageCzProperties.setProperty("NpfButtonOk", Constants.NPF_BTN_OK);
		languageCzProperties.setProperty("NpfButtonCancel", Constants.NPF_BTN_CANCEL);
		languageCzProperties.setProperty("NpfButtonBrowse", Constants.NPF_BTN_BROWSE);
		// Popisky:
		ConfigurationFiles.commentsMap.put("NpfLabelNameOfProject", "Texty pro labely v dialogu pro vytvoření nového projektu.");
		languageCzProperties.setProperty("NpfLabelNameOfProject", Constants.NPF_LBL_NAME);
		languageCzProperties.setProperty("NpfLabelWorkspace", Constants.NPF_LBL_WORKSPACE);
		languageCzProperties.setProperty("NpfLabelChangePath", Constants.NPF_LBL_CHANGE_PATH);
		languageCzProperties.setProperty("NpfLabelPlace", Constants.NPF_LBL_PLACE);
		// Chybové hlášky:
		ConfigurationFiles.commentsMap.put("NpfPathErrorTitle", "Texty pro chybové hlášky pro dialog pro vytvoření nového projektu. K těmto chybovým hlášením může dojít například pokud není zadán název projektu, nebo je v chybné syntaxi, jedná se o duplicitní název apod.");
		languageCzProperties.setProperty("NpfPathErrorTitle", Constants.NPF_PATH_ERROR_TITLE);
		languageCzProperties.setProperty("NpfPathErrorText", Constants.NPF_PATH_ERROR_TEXT);
		languageCzProperties.setProperty("NpfEmptyFieldErrorTitle", Constants.NPF_EMPTY_FIELD_ERROR_TITLE);
		languageCzProperties.setProperty("NpfEmptyFieldErrorText", Constants.NPF_EMPTY_FIELD_ERROR_TEXT);
		languageCzProperties.setProperty("NpfProjectFolderExistTitle", Constants.NPF_PROJECT_FOLDER_EXIST_TITLE);
		languageCzProperties.setProperty("NpfProjectFolderExistText", Constants.NPF_PROJECT_FOLDER_EXIST_TEXT);
		// složka projektu již existuje - text:
		languageCzProperties.setProperty("NpfProjectFolderExist", Constants.NPF_PROJECT_FOLDER_NAME_EXIST);
				
				
				
				
		// Text do výchozího souboru cz.uhk.fim.fimj.file.Constants.READ_ME_FILE_NAME_WITH_EXTENSION:
		ConfigurationFiles.commentsMap.put("TextLineInfo", "Výchozí texty pro soubor " + Constants.READ_ME_FILE_NAME_WITH_EXTENSION + ", který se vytvoří do adresáře nového projektu. Tento soubor obsahuje informace o daném projektu. Jedná se o jakousi šablonu pro vyplnění různých informací o projektu.");
		languageCzProperties.setProperty("TextLineInfo", Constants.TEXT_AT_LINE_1);
		languageCzProperties.setProperty("TextLineInfo_2", Constants.TEXT_AT_LINE_2);
		languageCzProperties.setProperty("TextLineAuthor", Constants.TEXT_AT_LINE_AUTHOR);
		languageCzProperties.setProperty("TextLineProjectName", Constants.TEXT_AT_LINE_PROJECT_NAME);
		languageCzProperties.setProperty("TextLineProjectPurpose", Constants.TEXT_AT_LINE_PROJECT_PURPOSE);
        languageCzProperties.setProperty("TextLineHowToRunTheProject", Constants.TEXT_AT_LINE_HOW_TO_RUN_THE_PROJECT);
        languageCzProperties.setProperty("TextLineStartingClasses", Constants.TEXT_AT_LINE_STARTING_CLASSES);
        languageCzProperties.setProperty("TextLineUserInstructions", Constants.TEXT_AT_LINE_USER_INSTRUCTIONS);
        languageCzProperties.setProperty("TextLineUsefulAdditionalInformation", Constants.TEXT_AT_LINE_USEFUL_ADDITIONAL_INFORMATION);
		languageCzProperties.setProperty("TextLineVersion", Constants.TEXT_AT_LINE_VERSION);
		languageCzProperties.setProperty("TextLineDate", Constants.TEXT_AT_LINE_DATE);
				
		
		
				
				// Text pro SettingForm - dialog:
				// Titulek dialogu:
		ConfigurationFiles.commentsMap.put("SfDialogTitle", "Texty pro titulek dialogu Nastavení aplikace a texty do záložek v tomto dialogu.");
		languageCzProperties.setProperty("SfDialogTitle", Constants.SF_TITLE);
		// názvy záložek:
		languageCzProperties.setProperty("SfApplicationTitle", Constants.SF_APP_TITLE);
		languageCzProperties.setProperty("SfClassDiagramTitle", Constants.SF_CLASS_DIAG_TITLE);
		languageCzProperties.setProperty("SfInstanceDiagramTitle", Constants.SF_INSTANC_DIAG_TITLE);
		languageCzProperties.setProperty("SfCommandEditorTitle", Constants.SF_COMMAND_EDIT_TITLE);
		languageCzProperties.setProperty("SfClassEditorTitle", Constants.SF_CODE_EDIT_TITLE);
		languageCzProperties.setProperty("SfOutputEditorTitle", Constants.SF_OUTPUT_EDIT_TITLE);
		
		// Texty do tlačítek:
		ConfigurationFiles.commentsMap.put("SfButtonOk", "Texty pro tlačítka v dialogu Nastavení aplikace. Tlačítka slouží pro uložení změn v dialogu nebo zavření dialogu bez uložení.");
		languageCzProperties.setProperty("SfButtonOk", Constants.SF_BTN_OK);
		languageCzProperties.setProperty("SfButtonCancel", Constants.SF_BTN_CANCEL);
		
		// Texty pro dotaz na restartování aplikace:
		ConfigurationFiles.commentsMap.put("Sf_RestartAppText", "Texty pro dialogové okno, které slouží jako 'dotaz' pro uživatele o tom, zda se má restartovat aplikace. Tento dotaz s těmito texty se zobrazí po kliknutí na tlačítko 'OK' v dialogu 'Nastavení' (v případě, že budou nastavená data v pořádku).");
		languageCzProperties.setProperty("Sf_RestartAppText", Constants.SF_TXT_RESTART_APP_TEXT);
		languageCzProperties.setProperty("Sf_RestartAppTitle", Constants.SF_TXT_RESTART_APP_TITLE);
		
		// Popisky záložek: (Tool tip)
		ConfigurationFiles.commentsMap.put("SfApplicationToolTip", "Texty pro popisky záložek (informace ohledně obsahu daných záložek) v dialogu Nastavení aplikace. Tyto popisky se zobrazí po najetí kurzoru myši na titulek dané záložky.");
		languageCzProperties.setProperty("SfApplicationToolTip", Constants.SF_APP_TOOL_TIP);
		languageCzProperties.setProperty("SfClassDiagramToolTip", Constants.SF_CLASS_DIAG_TOOL_TIP);
		languageCzProperties.setProperty("SfInstanceDiagramToolTip", Constants.SF_INSTANC_DIAG_TOOL_TIP);
		languageCzProperties.setProperty("SfCommandEditorToolTip", Constants.SF_COMMAND_EDIT_TOOL_TIP);
		languageCzProperties.setProperty("SfClassEditorToolTip", Constants.SF_CODE_EDIT_TOOL_TIP);
		languageCzProperties.setProperty("SfOutputEditorToolTip", Constants.SF_OUTPUT_EDIT_TOOL_TIP);
		languageCzProperties.setProperty("SfPnlProjectExpCodeEditTitle", Constants.SF_PNL_PROJECT_EXP_CODE_EDIT_TITLE);
		languageCzProperties.setProperty("SfPnlProjectExpCodeEditText", Constants.SF_PNL_PROJECT_EXP_CODE_EDIT_TEXT);
		languageCzProperties.setProperty("SfPnlOutputFrameTitle", Constants.SF_PNL_OUTPUT_FRAME_TITLE);
		languageCzProperties.setProperty("SfPnlOutputFrameText", Constants.SF_PNL_OUTPUT_FRAME_TEXT);

		
		
				// Texty do ApplicationPanel v SettingsForm.java:		
				// Popisky - labely:
		ConfigurationFiles.commentsMap.put("Sf_App_Sorting_Text_In_Prop_File_Model", "Jednorozměrné pole, které obsahuje texty pro výběr způsobu řazení textů v souborech .properties obsahující texty pro aplikaci. Tyto texty jsou v komponentě JComboBox v dialogu pro Nastavení aplikace.");
		languageCzProperties.setProperty("Sf_App_Sorting_Text_In_Prop_File_Model", createTextFromArray(Constants.SORTING_TEXT_IN_PROP_FILE_MODEL));
		ConfigurationFiles.commentsMap.put("Sf_App_Sorting_Text_In_Prop_File_TT_Model", "Jednorozměrné pole, které obsahuje popisky (tooltipy) pro položky, které slouží pro výběr způsobu řazení textů v souborech .properties obsahující texty pro aplikaci. Tyto texty jsou v komponentě JComboBox v dialogu pro Nastavení aplikace.");
		languageCzProperties.setProperty("Sf_App_Sorting_Text_In_Prop_File_TT_Model", createTextFromArray(Constants.SORTING_TEXT_IN_PROP_FILE_TT_MODEL));
		
		ConfigurationFiles.commentsMap.put("Sf_App_CheckBoxAskForWorkspace", "Texty pro labely a tlačítka v dialogu Nastavení aplikace na záložce Aplikace, ta obsahuje panel ApplicationPanel.");
		languageCzProperties.setProperty("Sf_App_CheckBoxAskForWorkspace", Constants.SF_CHCB_ASK_FOR_WORKSPACE);		
		languageCzProperties.setProperty("Sf_App_BorderLanguage", Constants.SF_BORDER_LANGUAGE);
		languageCzProperties.setProperty("Sf_App_Pnl_CaptureExceptions", Constants.SF_PNL_CAPTURE_EXCEPTION);
		languageCzProperties.setProperty("Sf_App_PanelLanguageBorderTitle", Constants.SF_PNL_LOADING_FRAME_PANEL);
		languageCzProperties.setProperty("Sf_App_BorderCopyDir", Constants.SF_BORDER_COPY_DIR);
		
		// Panel pro chcb pro testování adresáře se soubory potřebnými pro kompilaci:
		ConfigurationFiles.commentsMap.put("Sf_App_Ap_PnlSearchCompileFiles", "Texty pro panel, která obsahuje komponenty, které slouží pro natavení toho, zda se má při spuštění aplikace testovat existence adresáře se soubory, které aplikace potřebnuje pro kompilování Javovských tříd. Dále, zda se má za účelem nalezení těchto souborů prohledat celý disk nebo jen rodičovksé adresáře od adresáře java Home.");
		languageCzProperties.setProperty("Sf_App_Ap_PnlSearchCompileFiles", Constants.SF_AP_PNL_SEARCH_COMPILE_FILES);
		languageCzProperties.setProperty("Sf_App_Ap_ChcbSearchCompileFilesText", Constants.SF_AP_CHCB_SEARCH_COMPILE_FILES_TEXT);
		languageCzProperties.setProperty("Sf_App_Ap_ChcbSearchCompileFilesTt", Constants.SF_AP_CHCB_SEARCH_COMPILE_FILES_TT);
		languageCzProperties.setProperty("Sf_App_Ap_ChcbSearchEntireDiskForCompileFilesText", Constants.SF_AP_CHCB_SEARCH_ENTIRE_DISK_FOR_COMPILE_FILES_TEXT);
		languageCzProperties.setProperty("Sf_App_Ap_ChcbSearchEntireDiskForCompileFilesTt", Constants.SF_AP_CHCB_SEARCH_ENTIRE_DISK_FOR_COMPILE_FILES_TT);
		
		ConfigurationFiles.commentsMap.put("Sf_App_LabelCopyConfigurationDir", "Texty pro komponenty v panelu, který slouží pro vytvoření konfiguračních souborů pro aplikaci.");
		languageCzProperties.setProperty("Sf_App_LabelCopyConfigurationDir", Constants.SF_LBL_COPY_CONFIGURATION_DIR);
		languageCzProperties.setProperty("Sf_App_LabelCopyLanguageDir", Constants.SF_LBL_COPY_LANGUAGE_DIR);
		languageCzProperties.setProperty("Sf_App_LabelCopyDefaultProperties", Constants.SF_LBL_COPY_DEFAULT_PROP);
		languageCzProperties.setProperty("Sf_App_LabelCopyClassDiagramProperties", Constants.SF_LBL_COPY_CD_PROP);
		languageCzProperties.setProperty("Sf_App_LabelCopyInstanceDiagramProperties", Constants.SF_LBL_COPY_ID_PROP);
		languageCzProperties.setProperty("Sf_App_LabelCopyCommandEditorProperties", Constants.SF_LBL_CPY_COMMAND_EDITOR_PROP);
		languageCzProperties.setProperty("Sf_App_LabelCopyClassEditorProperties", Constants.SF_LBL_COPY_CLASS_EDITOR_PROP);
		languageCzProperties.setProperty("Sf_App_LabelCopyOutputEditorProperties", Constants.SF_LBL_COPY_OUTPUT_EDITOR_PROP);
		languageCzProperties.setProperty("Sf_App_LabelCopyOutputFrameProperties", Constants.SF_LBL_COPY_OUTPUT_FRAME_PROP);
		languageCzProperties.setProperty("Sf_App_LabelCopyCodeEditorForInternalFrames", Constants.SF_LBL_COPY_CODE_EDITOR_FOR_INTERNAL_FAMES);
		
		ConfigurationFiles.commentsMap.put("Sf_App_LabelAnimationImage", "Zbývající Texty pro komponenty v panelu na záložce 'Aplikace, která obsahuje možnosti pro nastavení některých vlastností aplikace.");
		languageCzProperties.setProperty("Sf_App_LabelAnimationImage", Constants.SF_LBL_ANIMATION_IMAGE);
		languageCzProperties.setProperty("Sf_App_Chcb_CaptureExceptionsText", Constants.SF_CHCB_CAPTURE_EXCEPTIONS_TEXT);
		languageCzProperties.setProperty("Sf_App_Chcb_CaptureExceptionsText_TT", Constants.SF_CHCB_CAPTURE_EXCEPTIONS_TEXT_TT);
		languageCzProperties.setProperty("Sf_App_LabelResetApplication", Constants.SF_LBL_RESET_APP);
		languageCzProperties.setProperty("Sf_App_ID_Lbl_AttributesAndMethodsPanelTitle", Constants.SF_ID_LBL_ATTRIBUTES_AND_METHODS_PANEL_TITLE);
		languageCzProperties.setProperty("Sf_App_BtnRestoreDefault", Constants.SF_BTN_RESTORE_DEFAULT);
		languageCzProperties.setProperty("Sf_App_ButtonsCopyDirectories", Constants.SF_BUTTONS_FOR_PLACES);


        // Texty do třídy cz.uhk.fim.fimj.settings_form.application_panels.LookAndFeelPanel:
        ConfigurationFiles.commentsMap.put("Lafp_BorderTitle", "Texty do panelu v dialogu nastavení na záložce 'Aplikace', který obsahuje komponenty pro nastavení Look and Feel.");
        languageCzProperties.setProperty("Lafp_BorderTitle", Constants.LAFP_BORDER_TITLE);
        languageCzProperties.setProperty("Lafp_Lbl_LookAndFeel", Constants.LAFP_LBL_LOOK_AND_FEEL);
        languageCzProperties.setProperty("Lafp_Txt_WithoutLookAndFeelItem_TT", Constants.LAFP_TXT_WITHOUT_LOOK_AND_FEEL_ITEM_TT);


		// Texty do třídy: cz.uhk.fim.fimj.settings_form.application_panels.UserNamePanel
		ConfigurationFiles.commentsMap.put("Unp_Pnl_BorderTitle", "Texty do panelu 'Uživatelské jméno' v dialogu nastavení na záložce 'Aplikace'. Panel obsahuje texty pro nastavení uživatelského jména, zde jsou texty pro veškeré komponenty v uvedeném panelu.");
		languageCzProperties.setProperty("Unp_Pnl_BorderTitle", Constants.UNP_PNL_BORDER_TITLE);
		languageCzProperties.setProperty("Unp_Lbl_UserName_Text", Constants.UNP_LBL_USER_NAME_TEXT);
		languageCzProperties.setProperty("Unp_Lbl_UserName_Title", Constants.UNP_LBL_USER_NAME_TITLE);
		languageCzProperties.setProperty("Unp_Txt_UserNameField_1", Constants.UNP_TXT_USER_NAME_FIELD_1);
		languageCzProperties.setProperty("Unp_Txt_UserNameField_2", Constants.UNP_TXT_USER_NAME_FIELD_2);
		languageCzProperties.setProperty("Unp_Btn_Edit", Constants.UNP_BTN_EDIT);
		languageCzProperties.setProperty("Unp_Btn_Save", Constants.UNP_BTN_SAVE);
		languageCzProperties.setProperty("Unp_Btn_Cancel", Constants.UNP_BTN_CANCEL);
		languageCzProperties.setProperty("Unp_Btn_RestoreDefault_Text", Constants.UNP_BTN_RESTORE_DEFAULT_TEXT);
		languageCzProperties.setProperty("Unp_Btn_RestoreDefault_Title", Constants.UNP_BTN_RESTORE_DEFAULT_TITLE);
		// Texty do chybových hlášek:
		languageCzProperties.setProperty("Unp_Txt_FailedSaveUserNameToFile_Text", Constants.UNP_TXT_FAILED_SAVE_USER_NAME_TO_FILE_TEXT);
		languageCzProperties.setProperty("Unp_Txt_FailedSaveUserNameToFile_Title", Constants.UNP_TXT_FAILED_SAVE_USER_NAME_TO_FILE_TITLE);
		languageCzProperties.setProperty("Unp_Txt_InvalidName_Text_1", Constants.UNP_TXT_INVALID_NAME_TEXT_1);
		languageCzProperties.setProperty("Unp_Txt_InvalidName_Text_2", Constants.UNP_TXT_INVALID_NAME_TEXT_2);
		languageCzProperties.setProperty("Unp_Txt_InvalidName_Title", Constants.UNP_TXT_INVALID_NAME_TITLE);


		
		// Texty do LanguagePanel v applicationPanels:
		ConfigurationFiles.commentsMap.put("Sf_App_LabelLanguage", "Texty pro labely v panel LanguagePanel v panelu ApplicationPanel, který tvoří záložku Aplikace v dialogu Nastavení aplikace.");
		languageCzProperties.setProperty("Sf_App_LabelLanguage", Constants.SF_LANGUAGE);
		languageCzProperties.setProperty("Sf_LP_Lbl_Sorting_Text_In_Prop_File", Constants.SF_LP_LBL_SORTING_TEXT_IN_PROP_FILE);
		languageCzProperties.setProperty("Sf_LP_Chcb_Add_Comments", Constants.SF_LP_CHCB_ADD_COMMENTS);
		languageCzProperties.setProperty("Sf_LP_Lbl_Count_Of_Free_Lines_Before_Comment", Constants.SF_LP_LBL_COUNT_OF_FREE_LINES_BEFORE_COMMENT);
		languageCzProperties.setProperty("Sf_LP_Lbl_Locale", Constants.SF_LP_LBL_LOCALE);
		languageCzProperties.setProperty("Sf_LP_Lbl_Locale_Tt", Constants.SF_LP_LBL_LOCALE_TT);

		
		
		
		
		
		
		// Texty do ClassDiagramPanelu v SettingsForm.java:
		// Popisky - labely:
		ConfigurationFiles.commentsMap.put("Sf_Cd_CheckBoxOpaqueClassCell", "Texty pro labely a tlačítka v panelu ClassDiagramPanel, který se nachází v dialogu Nastavení aplikace na záložce diagram tříd.");
		languageCzProperties.setProperty("Sf_Cd_CheckBoxOpaqueClassCell", Constants.SF_CHCB_OPAQUE_CELL);
		languageCzProperties.setProperty("Sf_Cd_LabelFontSizeClassCell", Constants.SF_LBL_FONT_SIZE_CELL);
		languageCzProperties.setProperty("Sf_Cd_LabelFontStyleClassCell", Constants.SF_LBL_FONT_STYLE_CELL);
		languageCzProperties.setProperty("Sf_Cd_LabelTextAlignent_X_ClassCell", Constants.SF_LBL_TEXT_ALIGNMENT_X_CELL);
		languageCzProperties.setProperty("Sf_Cd_LabelTextAlignment_Y_ClassCell", Constants.SF_LBL_TEXT_ALIGNMENT_Y_CELL);
		languageCzProperties.setProperty("Sf_Cd_LabelTextColorClassCell", Constants.SF_LBL_TEXT_COLOR_CELL);
		languageCzProperties.setProperty("Sf_Cd_LabelBackgroundColor_Right_ClassCell", Constants.SF_LBL_BG_COLOR_RIGHT_CELL);
		languageCzProperties.setProperty("Sf_Cd_LabelBackgroundColor_Left_ClassCell", Constants.SF_LBL_BG_COLOR_LEFT_CELL);
		languageCzProperties.setProperty("Sf_Cd_ClassCellBorderTitle", Constants.SF_CD_PNL_CLASS_CELL_BORDER_TITLE);
		languageCzProperties.setProperty("Sf_Cd_CommentCellBorderTitle", Constants.SF_CD_PNL_COMMENT_CELL_BORDER_TITLE);
		languageCzProperties.setProperty("Sf_Cd_ButtonsChooseColor", Constants.SF_CD_BTN_CHOOSE_COLOR);

		
		// Text pro tlačítko obnovit výchozí nastavení:
		ConfigurationFiles.commentsMap.put("Sf_Cd_ButtonRestoreDefaultSettings", "Text pro tlačítko, které slouží pro obnovení výchozího nastavení. Tento text se aplikuje na všechna tlačítka, která slouží pro obnovení výchozího nastavení v panelech, které se nachází v dialogu nastavení aplikace. Tyto tlačítka se nacházejí vždy ve spodní části panelu na každé záložce v dialogu Natavení aplikace.");
		languageCzProperties.setProperty("Sf_Cd_ButtonRestoreDefaultSettings", Constants.SF_BUTTONS_RESTORE_DEFAULT_SETTINGS);


		// Texty do třídy CommandEditorPanel v nastavení. Texty slouží pouze pro chcbs pro nastavení zobrazení a vlastnosti okna pro doplňování příkazů v editoru příkazů:
        ConfigurationFiles.commentsMap.put("CEP_Pnl_AutoCompleteWindow", "Texty pro komponenty v dialogu nastavení pro nastavení vlastností okna s hodnotami pro doplňování příkazů do editoru příkazů.");
        languageCzProperties.setProperty("CEP_Pnl_AutoCompleteWindow", Constants.CEP_PNL_AUTO_COMPLETE_WINDOW);
        languageCzProperties.setProperty("CEP_Chcb_ShowAutoCompleteWindow_Text", Constants.CEP_CHCB_SHOW_AUTO_COMPLETE_WINDOW_TEXT);
        languageCzProperties.setProperty("CEP_Chcb_ShowAutoCompleteWindow_TT", Constants.CEP_CHCB_SHOW_AUTO_COMPLETE_WINDOW_TT);
        languageCzProperties.setProperty("CEP_Chcb_ShowReferenceVariablesInCompleteWindow_Text", Constants.CEP_CHCB_SHOW_REFERENCE_VARIABLES_IN_COMPLETE_WINDOW_TEXT);
        languageCzProperties.setProperty("CEP_Chcb_ShowReferenceVariablesInCompleteWindow_TT", Constants.CEP_CHCB_SHOW_REFERENCE_VARIABLES_IN_COMPLETE_WINDOW_TT);
        languageCzProperties.setProperty("CEP_Chcb_AddSyntaxForFillingTheVariable_Text", Constants.CEP_CHCB_ADD_SYNTAX_FOR_FILLING_THE_VARIABLE_TEXT);
        languageCzProperties.setProperty("CEP_Chcb_AddSyntaxForFillingTheVariable_TT", Constants.CEP_CHCB_ADD_SYNTAX_FOR_FILLING_THE_VARIABLE_TT);
        languageCzProperties.setProperty("CEP_Chcb_AddFinalWordForFillingTheVariable_Text", Constants.CEP_CHCB_ADD_FINAL_WORD_FOR_FILLING_THE_VARIABLE_TEXT);
        languageCzProperties.setProperty("CEP_Chcb_AddFinalWordForFillingTheVariable_TT", Constants.CEP_CHCB_ADD_FINAL_WORD_FOR_FILLING_THE_VARIABLE_TT);
        languageCzProperties.setProperty("CEP_Chcb_ShowExamplesOfSyntax_Text", Constants.CEP_CHCB_SHOW_EXAMPLES_OF_SYNTAX_TEXT);
        languageCzProperties.setProperty("CEP_Chcb_ShowExamplesOfSyntax_TT", Constants.CEP_CHCB_SHOW_EXAMPLES_OF_SYNTAX_TT);
        languageCzProperties.setProperty("CEP_Chcb_ShowDefaultMethods_Text", Constants.CEP_CHCB_SHOW_DEFAULT_METHODS_TEXT);
        languageCzProperties.setProperty("CEP_Chcb_ShowDefaultMethods_TT", Constants.CEP_CHCB_SHOW_DEFAULT_METHODS_TT);

        // Texty do třídy: cz.uhk.fim.fimj.settings_form.command_editor_panels.CompleteHistoryPanel:
        ConfigurationFiles.commentsMap.put("Chp_Txt_Pnl_BorderTitle", "Texty do panelu pro nastavení okna s historií příkazů zadaných do editoru příkazů od spuštění aplikace.");
        languageCzProperties.setProperty("Chp_Txt_Pnl_BorderTitle", Constants.CHP_TXT_PNL_BORDER_TITLE);
        languageCzProperties.setProperty("Chp_Txt_Pnl_Tooltip_Text", Constants.CHP_TXT_PNL_TOOLTIP_TEXT);
        languageCzProperties.setProperty("Chp_Chcb_ShowHistory_Text", Constants.CHP_CHCB_SHOW_HISTORY_TEXT);
        languageCzProperties.setProperty("Chp_Chcb_IndexCommands_Text", Constants.CHP_CHCB_INDEX_COMMANDS_TEXT);
        languageCzProperties.setProperty("Chp_Chcb_IndexCommands_TT", Constants.CHP_CHCB_INDEX_COMMANDS_TT);

        // Texty do třídy: cz.uhk.fim.fimj.settings_form.command_editor_panels.ProjectHistoryPanel:
        ConfigurationFiles.commentsMap.put("Php_Txt_Pnl_BorderTitle", "Texty do panelu pro nastavení okna s historií příkazů zadaných do editoru příkazů v rámci otevřeného projektu.");
        languageCzProperties.setProperty("Php_Txt_Pnl_BorderTitle", Constants.PHP_TXT_PNL_BORDER_TITLE);
        languageCzProperties.setProperty("Php_Txt_PnlTooltip_Text", Constants.PHP_TXT_PNL_TOOLTIP_TEXT);
        languageCzProperties.setProperty("Php_Chcb_ShowHistory_Text", Constants.PHP_CHCB_SHOW_HISTORY_TEXT);
        languageCzProperties.setProperty("Php_Chcb_IndexCommands_Text", Constants.PHP_CHCB_INDEX_COMMANDS_TEXT);
        languageCzProperties.setProperty("Php_Chcb_IndexCommands_TT", Constants.PHP_CHCB_INDEX_COMMANDS_TT);

				
		// Texty do chybových hlášek ve třídě ClassDiagramPanel:
		ConfigurationFiles.commentsMap.put("Sf_Cd_TxtClassAndCommentCellsAreIdenticalText", "Texty pro chybové hlášky, které mohou nastat při ukládání nastavení pro diagram tříd.");
		languageCzProperties.setProperty("Sf_Cd_TxtClassAndCommentCellsAreIdenticalText", Constants.SF_CD_TXT_CLASS_AND_COMMENT_CELLS_ARE_IDENTICAL_TEXT);
		languageCzProperties.setProperty("Sf_Cd_TxtClassAndCommentCellsAreIdenticalTitle", Constants.SF_CD_TXT_CLASS_AND_COMMENT_CELLS_ARE_IDENTICAL_TITLE);
		languageCzProperties.setProperty("Sf_Cd_TxtEdgesAreIdenticalText", Constants.SF_CD_TXT_EDGES_ARE_IDENTICAL_TEXT);
		languageCzProperties.setProperty("Sf_Cd_TxtEdgesAreIdenticalTitle", Constants.SF_CD_TXT_EDGES_ARE_IDENTICAL_TITLE);
				
		ConfigurationFiles.commentsMap.put("Sf_Cd_EdgesPanel", "Popisek pro ohraničení panelu v dialogu nastavení, ve kterém se nachází panely s komponentami pro nastavení vlastnosti hran pro vztahy mezi třídami v diagramu tříd.");
		languageCzProperties.setProperty("Sf_Cd_EdgesPanel", Constants.SF_CD_EDGES_PNL_BORDER_TITLE);
		
		ConfigurationFiles.commentsMap.put("Sf_Id_ClassCellPanelBorderTitle", "Popisek pro panel v dialogu nastavení, který obsahuje komponenty pro nastavení parametrů buňky, která reprezentuje instanci třídy v diagramu instancí.");
		languageCzProperties.setProperty("Sf_Id_ClassCellPanelBorderTitle", Constants.SF_ID_PNL_CLASS_CELL_BORDER_TITLE);
				
		// Texty do panelu v settingForm.java v panelu: ClassDiagramGraphPanel:
		ConfigurationFiles.commentsMap.put("Sf_Cd_Cdgp_BorderTitle", "Texty pro labely v panelech pro nastavení parametrů pro diagram tříd a diagram instancí v dialogu nastavení.");
		languageCzProperties.setProperty("Sf_Cd_Cdgp_BorderTitle", Constants.SF_CD_PNL_CDGP_BORDER_TITLE);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_LabelDisconectableFromEdge", Constants.SF_CD_PNL_CDGP_CHCB_CON_DIS_CON_LABEL_FROM_EDGE);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_ShowRelationsShipsToClassItself", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_RELATION_SHIPS_TO_CLASS_ITSELF);		
		languageCzProperties.setProperty("Sf_Cd_Cdgp_ChcbShowAsociationThroughAgregation", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_ASOCIATION_THROUGH_AGREGATION);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_ChcbShowAsociationThroughAgregation_TT", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_ASOCIATION_THROUGH_AGREGATION_TT);		
		languageCzProperties.setProperty("Sf_Cd_Cdgp_AddInterfaceMethodsToClassText", Constants.SF_CD_PNL_CDGP_CHCB_ADD_INTERFACE_METHODS_TO_CLASS_TEXT);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_AddInterfaceMethodsToClassTT", Constants.SF_CD_PNL_CDGP_CHCB_ADD_INTERFACE_METHODS_TO_CLASS_TT);

		languageCzProperties.setProperty("Sf_Cd_Cdgp_IncludePrivateConstructors", Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PRIVATE_CONSTRUCTORS);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_IncludePrivateConstructors_TT", Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PRIVATE_CONSTRUCTORS_TT);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_IncludeProtectedConstructors", Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PROTECTED_CONSTRUCTORS);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_IncludeProtectedConstructors_TT", Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PROTECTED_CONSTRUCTORS_TT);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_IncludePackagePrivateConstructors", Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PACKAGE_PRIVATE_CONSTRUCTORS);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_IncludePackagePrivateConstructors_TT", Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PACKAGE_PRIVATE_CONSTRUCTORS_TT);

		languageCzProperties.setProperty("Sf_Cd_Cdgp_ShowInheritedClassNameWithPackages", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_ShowInheritedClassNameWithPackages_TT", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES_TT);

		languageCzProperties.setProperty("Sf_Cd_Cdgp_IncludePrivateMethods", Constants.SF_CD_PNL_CDGP_CHCB_INCLUDE_PRIVATE_METHODS);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_Cd_IncludePrivateMethods_TT", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PRIVATE_METHODS_TT);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_Id_IncludePrivateMethods_TT", Constants.SF_CD_PNL_CDGP_CHCB_ID_INCLUDE_PRIVATE_METHODS_TT);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_Id_IncludeProtectedMethods_TT", Constants.SF_CD_PNL_CDGP_CHCB_ID_INCLUDE_PROTECTED_METHODS_TT);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_Id_IncludePackagePrivateMethods_TT", Constants.SF_CD_PNL_CDGP_CHCB_ID_INCLUDE_PACKAGE_PRIVATE_METHODS_TT);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_IncludeProtectedMethods", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PROTECTED_METHODS);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_IncludeProtectedMethods_TT", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PROTECTED_METHODS_TT);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_IncludePackagePrivateMethods", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PACKAGE_PRIVATE_METHODS);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_IncludePackagePrivateMethods_TT", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PACKAGE_PRIVATE_METHODS_TT);

		languageCzProperties.setProperty("Sf_Cd_Cdgp_IncludePrivateInnerStaticClasses", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PRIVATE_INNER_STATIC_CLASSES);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_IncludePrivateInnerStaticClasses_TT", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PRIVATE_INNER_STATIC_CLASSES_TT);

		languageCzProperties.setProperty("Sf_Cd_Cdgp_IncludeProtectedInnerStaticClasses", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PROTECTED_INNER_STATIC_CLASSES);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_IncludeProtectedInnerStaticClasses_TT", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PROTECTED_INNER_STATIC_CLASSES_TT);

		languageCzProperties.setProperty("Sf_Cd_Cdgp_IncludePackagePrivateInnerStaticClasses", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PACKAGE_PRIVATE_INNER_STATIC_CLASSES);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_IncludePackagePrivateInnerStaticClasses_TT", Constants.SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PACKAGE_PRIVATE_INNER_STATIC_CLASSES_TT);

		languageCzProperties.setProperty("Sf_Cd_Cdgp_InnerStaticClassesIncludePrivateStaticMethods", Constants.SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PRIVATE_STATIC_METHODS);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_InnerStaticClassesIncludePrivateStaticMethods_TT", Constants.SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PRIVATE_STATIC_METHODS_TT);

		languageCzProperties.setProperty("Sf_Cd_Cdgp_InnerStaticClassesIncludeProtectedStaticMethods", Constants.SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PROTECTED_STATIC_METHODS);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_InnerStaticClassesIncludeProtectedStaticMethods_TT", Constants.SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PROTECTED_STATIC_METHODS_TT);

		languageCzProperties.setProperty("Sf_Cd_Cdgp_InnerStaticClassesIncludePackagePrivateStaticMethods", Constants.SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PACKAGE_PRIVATE_STATIC_METHODS);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_InnerStaticClassesIncludePackagePrivateStaticMethods_TT", Constants.SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PACKAGE_PRIVATE_STATIC_METHODS_TT);

		languageCzProperties.setProperty("Sf_Cd_Cdgp_Id_ShowInheritedClassNameWithPackages_TT", Constants.SF_ID_PNL_CDGP_CHCB_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES_TT);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_MakeAvailableFieldsForCallConstructor", Constants.SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_CONSTRUCTOR);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_MakeAvailableFieldsForCallConstructor_TT", Constants.SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_CONSTRUCTOR_TT);		
		languageCzProperties.setProperty("Sf_Cd_Cdgp_MakeMethodsAvailableForCallConstructor", Constants.SF_CD_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_CONSTRUCTOR);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_MakeMethodsAvailableForCallConstructor_TT", Constants.SF_CD_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_CONSTRUCTOR_TT);		
		languageCzProperties.setProperty("Sf_Cd_Cdgp_MakeAvailableFieldsForCallMethod", Constants.SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_METHOD);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_MakeAvailableFieldsForCallMethod_TT", Constants.SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_METHOD_TT);		
		languageCzProperties.setProperty("Sf_Id_Cdgp_MakeMethodsAvailableForCallMethod", Constants.SF_ID_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD);
		languageCzProperties.setProperty("Sf_Id_Cdgp_MakeMethodsAvailableForCallMethod_TT", Constants.SF_ID_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD_TT);		
		languageCzProperties.setProperty("Sf_Cd_Cdgp_MakeMethodsAvailableForCallMethod", Constants.SF_CD_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_MakeMethodsAvailableForCallMethod_TT", Constants.SF_CD_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD_TT);		
		languageCzProperties.setProperty("Sf_Cd_Cdgp_ShowCreateNewInstanceOptionInCallConstructor", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_CONSTRUCTOR);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_ShowCreateNewInstanceOptionInCallConstructor_TT", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_CONSTRUCTOR_TT);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_ShowCreateNewInstanceOptionInCallMethod", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_ShowCreateNewInstanceOptionInCallMethod_TT", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD_TT);		
		languageCzProperties.setProperty("Sf_Cd_Cdgp_ShowGetterInSetterMethodInInstance", Constants.SF_ID_PNL_CDGP_CHCB_SHOW_GETTER_IN_SETTER_METHOD_IN_INSTANCE);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_ShowGetterInSetterMethodInInstance_TT", Constants.SF_ID_PNL_CDGP_CHCB_SHOW_GETTER_IN_SETTER_METHOD_IN_INSTANCE_TT);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_WayToRefreshRelationshipsBetweenInstances_Text", Constants.SF_CD_PNL_CDGP_CHCB_WAY_TO_REFRESH_RELATIONSHIPS_BETWEEN_INSTANCES);	
		languageCzProperties.setProperty("Sf_Cd_Cdgp_WayToRefreshRelationshipsBetweenInstances_TT", Constants.SF_CD_PNL_CDGP_CHCB_ID_WAY_TO_REFRESH_RELATIONSHIPS_BETWEEN_INSTANCES_TT);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_ShowRelationshipsInInheritedClasses", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_RELATIONSHIPS_IN_INHERITED_CLASSES);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_ShowRelationshipsInInheritedClasses_TT", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_RELATIONSHIPS_IN_INHERITED_CLASSES_TT);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_Chcb_ShowReferenceVariablesText", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_REFERENCE_VARIABLES_TEXT);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_Chcb_ShowReferenceVariablesTT", Constants.SF_CD_PNL_CDGP_CHCB_SHOW_REFERENCE_VARIABLES_TT);		
		languageCzProperties.setProperty("Sf_Cd_Cdgp_LabelBackgroundColor", Constants.SF_CD_PNL_CDGP_LBL_BG_COLOR);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_LabelScaleOfGraph", Constants.SF_CD_PNL_CDGP_LBL_SCALE);
		//  Texty pro nastavení výchozího editoru zdrojového kodu:
		languageCzProperties.setProperty("Sf_Cd_Cdgp_LabelCodeEditorInfoText", Constants.SF_CD_PNL_CDGP_LBL_CODE_EDITOR_INFO_TEXT);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_LabelCodeEditorInfoTT", Constants.SF_CD_PNL_CDGP_LBL_CODE_EDITOR_INFO_TT);
		// Texty do modelu komponenty JComboBox pro výchozí editor zdrojového kodu:
		languageCzProperties.setProperty("Sf_Cd_Cdgp_TxtSourceCodeEditorText", Constants.SF_CD_PNL_CDGP_TXT_SOURCE_CODE_EDITOR_TEXT);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_TxtSourceCodeEditorTT", Constants.SF_CD_PNL_CDGP_TXT_SOURCE_CODE_EDITOR_TT);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_TxtProjectExplorerText", Constants.SF_CD_PNL_CDGP_TXT_PROJECT_EXPLORER_TEXT);
		languageCzProperties.setProperty("Sf_Cd_Cdgp_TxtProjectExplorerTT", Constants.SF_CD_PNL_CDGP_TXT_PROJECT_EXPLORER_TT);
				
		// Titulky panelů v panelu pro hrany:
		ConfigurationFiles.commentsMap.put("Sf_Cd_ExtendsEdgePanel", "Texty pro titulky do ohraničení panelů s komponentami pro nastavení parametrů pro hrany, které v diagramu tříd reprezentují nějaké vztahy mezi třídami. Dialog nastavení.");
		languageCzProperties.setProperty("Sf_Cd_ExtendsEdgePanel", Constants.SF_CD_PNL_EXTENDS_EDGE_BORDER_TITLE);
		languageCzProperties.setProperty("Sf_Cd_ImplementsEdgePanel", Constants.SF_CD_PNL_IMPLEMENTS_EDGE_BORDER_TITLE);
		languageCzProperties.setProperty("Sf_Cd_Aggregation_1_ku_1_EdgePanel", Constants.SF_CD_PNL_AGGREGATION_1_KU_1_BORDER_TITLE);
		languageCzProperties.setProperty("Sf_Cd_Aggregation_1_ku_N_EdgePanel", Constants.DF_CD_PNL_AGGREGATION_1_KU_N_BORDER_TITLE);
		
		languageCzProperties.setProperty("Sf_Cd_ImplementsEdgePanelLabelLinePattern", Constants.SF_CD_PNL_IMPLEMENTS_EDGE_PANEL_LBL_LINE_PATTERN);
				
				// Text pro dialog pro vyber barvy:
		ConfigurationFiles.commentsMap.put("Sf_Cd_ChooseTextColorDialog", "Texty do labelů v dialogu nastavení, v panelech pro nastavení parametrů buňěk, které slouží jako třída nebo komentář v diagramu tříd.");
		languageCzProperties.setProperty("Sf_Cd_ChooseTextColorDialog", Constants.SF_TXT_CHOOSE_TEXT_COLOR);
		languageCzProperties.setProperty("Sf_Cd_ChooseBackgroundColorDialog", Constants.SF_TXT_CHOOSE_BG_COLOR);
		languageCzProperties.setProperty("Sf_Cd_ChooseEdgeLineColor", Constants.SF_TXT_CHOOSE_LINE_COLOR);
		languageCzProperties.setProperty("Sf_Cd_TxtOneColorForCell", Constants.SF_TXT_ONE_COLOR_FOR_CELL);
		languageCzProperties.setProperty("Sf_Cd_TxtTwoColorForCell", Constants.SF_TXT_TWO_COLOR_FOR_CELL);
		languageCzProperties.setProperty("Sf_Cd_TxtBgColorForCell", Constants.SF_TXT_BG_COLOR_FOR_CELL);
				
				// Texty do panelu ClassDiagramPanel v pnlAssociationEdge:
		ConfigurationFiles.commentsMap.put("Sf_Cd_AsociationEdgePanel", "Texty do dialogu nastavení pro komponenty typu JCheckBox a některé labely v panelu pro nastavení parametrů pro hranu, která reprezentuje vztah typu asociace v diagramu tříd.");
		languageCzProperties.setProperty("Sf_Cd_AsociationEdgePanel", Constants.SF_CD_PNL_ASSOCIATION_EDGE_BORDER_TITLE);
		languageCzProperties.setProperty("Sf_Cd_CheckBoxLabelAlongEdge", Constants.SF_CD_CHCB_EDGE_LABEL_ALONG_EDGE);
		languageCzProperties.setProperty("Sf_Cd_CheckBoxLineBeginFill", Constants.SF_CD_CHCB_LINE_BEGIN_FILL);
		languageCzProperties.setProperty("Sf_Cd_CheckBoxLineEndFill", Constants.SF_CD_CHCB_LINE_EDN_FILL);
		languageCzProperties.setProperty("Sf_Cd_LineColor", Constants.SF_CD_LBL_EDGE_COLOR);
		languageCzProperties.setProperty("Sf_Cd_LineStyle", Constants.SF_CD_LBL_LINE_STYLE);
		languageCzProperties.setProperty("Sf_Cd_LineEnd", Constants.SF_CD_LBL_LINE_END);
		languageCzProperties.setProperty("Sf_Cd_LineBegin", Constants.SF_CD_LBL_LINE_BEGIN);
		languageCzProperties.setProperty("Sf_Cd_LineWidth", Constants.SF_CD_LBL_LINE_WIDTH);

		
				// Texty do InstanceDiagramPanelu  - ClassCellPanel:
		ConfigurationFiles.commentsMap.put("Sf_Id_ShowPackagesInCell", "Texty do dialogu nastavení pro labely v panelu pro nastavení parametrů pro buňku, která reprezentuji instanci třídy v diagramu instancí.");
		languageCzProperties.setProperty("Sf_Id_ShowPackagesInCell", Constants.SF_ID_SHOW_PACKAGES_IN_CELL);
		languageCzProperties.setProperty("Sf_Id_LabelChooseBorderColor", Constants.SF_ID_LBL_BORDER_COLOR);
		languageCzProperties.setProperty("Sf_Id_LabelChooseCellBackgroundColor", Constants.SF_ID_LBL_BG_COLOR);
		languageCzProperties.setProperty("Sf_Id_TextToDialogChooseBorderColor", Constants.SF_ID_DIALOG_CHOOSE_BORDER_COLOR);
		languageCzProperties.setProperty("Sf_Id_TextToDialogChooseBackgroundColor", Constants.SF_ID_DIALOG_CHOOSE_BG_CELL_COLOR);
		languageCzProperties.setProperty("Sf_Id_LabelRoundedBorder", Constants.SF_ID_LBL_RONDED_BORDER);

		
		// Texty do AssociationEdgePanelu:
		ConfigurationFiles.commentsMap.put("Sf_Id_AssociationEdgePanelBorderTitle", "Titulek pro dialog nastavení, pro ohraničení panelu s komponentami pro nastavení hrany, která reprezentuje vztah typu asociace mezi instancemi tříd v diagramu instancí.");
		languageCzProperties.setProperty("Sf_Id_AssociationEdgePanelBorderTitle", Constants.SF_ID_ASSOCIATION_EDGE_PANEL_BORDER_TITLE);
				
		// Titulek panelu AggregationEdgePanel:
		ConfigurationFiles.commentsMap.put("Sf_Id_AggregationEdgePanelBorderTitle", "Titulek pro dialog nastavení, pro ohraničení panelu s komponentami pro nastavení hrany, která reprezentuje vztah typu agregace mezi instancemi tříd v diagramu instancí.");
		languageCzProperties.setProperty("Sf_Id_AggregationEdgePanelBorderTitle", Constants.SF_ID_AGGREGATION_EDGE_PANEL_BORDER_TITLE);
				
		
		// Texty do třídy: HighlightInstancePanel v instanceDigramPanels. Jedná se o
		// třídu s komponentami pro nastavení jak má vypadat zvýrazněná instance a jak
		// dlouho má být zvýrazněná a zda se mají vůbec zvýrazňovat.
		ConfigurationFiles.commentsMap.put("Hip_BorderTitle", "Titulek panelu, který obsahuje komponenty pro nastavení vlastností zvýrazněné buňky / instance. Tento panel se nachází v dialogu Nastavení na záložce diagram instancí v části Zvýraznění instance.");
		languageCzProperties.setProperty("Hip_BorderTitle", Constants.HIP_BODER_TITLE);		
		
		// Zda se mají zvýrazňovat instance:
		ConfigurationFiles.commentsMap.put("Hip_Chcb_HighlightInstancesText", "Texty pro komponentu JCheckBox, která slouží pro nastavení toho, zda semají zvýrazňovat instance v diagramu instancí po například zavolání metody nebo získání hodnoty proměnné a tato hodnota je instance nějaké třídy z diagramu tříd, která je reprezentována v diagramu instancí. Tato komponenta se nachází v dialogu Nastavení na záložce diagram instancí v části Zvýraznění instance.");
		languageCzProperties.setProperty("Hip_Chcb_HighlightInstancesText", Constants.HIP_CHCB_HIGHLIGHT_INSTANCES_TEXT);
		languageCzProperties.setProperty("Hip_Chcb_HighlightInstancesTT", Constants.HIP_CHCB_HIGHLIGHT_INSTANCES_TT);
		
		// Čas, jak dlouho mají být zvýrazněné instance:
		ConfigurationFiles.commentsMap.put("Hip_Lbl_HighlightingTimeText", "Texty pro komponentu JLabel, která informuje uživatele, že na příslušném místě je možné nastavit čas, jak dlouho bude příslušná instance zvýrazněná v diagramu instancí. Například, když se příslušná instance získá z nějaké proměnné v instanci a ta instance je v diagramu instancí, tak jak dlouho má být zvýrazněná. Tato komponenta se nachází v dialogu Nastavení na záložce diagram instancí v části Zvýraznění instance.");
		languageCzProperties.setProperty("Hip_Lbl_HighlightingTimeText", Constants.HIP_LBL_HIGHLIGHTING_TIME_TEXT);
		languageCzProperties.setProperty("Hip_Lbl_HighlightingTimeTT", Constants.HIP_LBL_HIGHLIGHTING_TIME_TT);
		
		// Label pro výběr barvy písma ve zvýrazněné instanci:
		ConfigurationFiles.commentsMap.put("Hip_Lbl_TextColorText", "Texty pro komponentu JLabel, která informuje uživatele, že na příslušném místě je možné nastavit barvu písma pro zvýrazněnou buňku / instanci. Tato komponenta se nachází v dialogu Nastavení na záložce diagram instancí v části Zvýraznění instance.");
		languageCzProperties.setProperty("Hip_Lbl_TextColorText", Constants.HIP_LBL_TEXT_COLOR_TEXT);
		languageCzProperties.setProperty("Hip_Lbl_TextColorTT", Constants.HIP_LBL_TEXT_COLOR_TT);
		
		// Label pro výběr barvy písma atributů ve zvýrazněné instanci:
		ConfigurationFiles.commentsMap.put("Hip_Lbl_AttributeColorText", "Texty pro komponentu JLabel, která informuje uživatele, že na příslušném místě je možné nastavit barvu písma pro atributy ve zvýrazněné buňce / instanci. Tato komponenta se nachází v dialogu Nastavení na záložce diagram instancí v části Zvýraznění instance.");
		languageCzProperties.setProperty("Hip_Lbl_AttributeColorText", Constants.HIP_LBL_ATTRIBUTE_COLOR_TEXT);
		languageCzProperties.setProperty("Hip_Lbl_AttributeColorTT", Constants.HIP_LBL_ATTRIBUTE_COLOR_TT);
		
		// Label pro výběr barvy písma metod ve zvýrazněné instanci:
		ConfigurationFiles.commentsMap.put("Hip_Lbl_MethodColorText", "Texty pro komponentu JLabel, která informuje uživatele, že na příslušném místě je možné nastavit barvu písma pro metody ve zvýrazněné buňce / instanci. Tato komponenta se nachází v dialogu Nastavení na záložce diagram instancí v části Zvýraznění instance.");
		languageCzProperties.setProperty("Hip_Lbl_MethodColorText", Constants.HIP_LBL_METHOD_COLOR_TEXT);
		languageCzProperties.setProperty("Hip_Lbl_MethodColorTT", Constants.HIP_LBL_METHOD_COLOR_TT);
		
		// Label pro výběr barvy ohnraničení zvýrazněné instance:
		ConfigurationFiles.commentsMap.put("Hip_Lbl_BorderColorText", "Texty pro komponentu JLabel, která informuje uživatele, že na příslušném místě je možné nastavit barvu ohraničení pro zvýrazněnou buňku / instanci. Tato komponenta se nachází v dialogu Nastavení na záložce diagram instancí v části Zvýraznění instance.");
		languageCzProperties.setProperty("Hip_Lbl_BorderColorText", Constants.HIP_LBL_BORDER_COLOR_TEXT);
		languageCzProperties.setProperty("Hip_Lbl_BorderColorTT", Constants.HIP_LBL_BORDER_COLOR_TT);
		
		// Label pro výběr barvy pozadí zvýrazněné instance:
		ConfigurationFiles.commentsMap.put("Hip_Lbl_BackgroundColorText", "Texty pro komponentu JLabel, která informuje uživatele, že na příslušném místě je možné nastavit barvu pozadí pro zvýrazněnou buňku / instanci. Tato komponenta se nachází v dialogu Nastavení na záložce diagram instancí v části Zvýraznění instance.");
		languageCzProperties.setProperty("Hip_Lbl_BackgroundColorText", Constants.HIP_LBL_BACKGROUND_COLOR_TEXT);
		languageCzProperties.setProperty("Hip_Lbl_BackgroundColorTT", Constants.HIP_LBL_BACKGROUND_COLOR_TT);
		
		// Tlčítka pro označení barev písma, ohraničení a pozadí instance:
		ConfigurationFiles.commentsMap.put("Hip_Btn_ChooseColor", "Text pro tlačítka, která slouží pro zvolení barvy písma, ohraničení a pozadí buňky zvýrazněné instance. Tato tlačítka se nachází v dialogu Nastavení na záložce diagram instancí v části Zvýraznění instance.");
		languageCzProperties.setProperty("Hip_Btn_ChooseColor", Constants.HIP_BTN_CHOOSE_COLOR);
		
		// Texty pro dialogy pro výběry barev:
		ConfigurationFiles.commentsMap.put("Hip_Txt_ChooseTextColorTitle", "Texty do titulků dialogu pro výběr barvy písma, ohraničení a pozadí buňky zvýrazněné instance.");
		languageCzProperties.setProperty("Hip_Txt_ChooseTextColorTitle", Constants.HIP_TXT_CHOOSE_TEXT_COLOR_TITLE);		
		languageCzProperties.setProperty("Hip_Txt_ChooseAttributeColorTitle", Constants.HIP_TXT_CHOOSE_ATTRIBUTE_COLOR_TITLE);
		languageCzProperties.setProperty("Hip_Txt_ChooseMethodColorTitle", Constants.HIP_TXT_CHOOSE_METHOD_COLOR_TITLE);		
		languageCzProperties.setProperty("Hip_Txt_ChooseBorderColorTitle", Constants.HIP_TXT_CHOOSE_BORDER_COLOR_TITLE);
		languageCzProperties.setProperty("Hip_Txt_ChooseBackgroundColor", Constants.HIP_TXT_CHOOSE_BACKGROUND_COLOR);
		
		
		
		
		
		
		
		
		
		
		// Texty do panelu InstanceCellMethodsPanel v balíčku instnceDiagramPanel, jedná
		// se o texty pro komponenty pro nastavení toho, zda a jak se mají zobrazovat
		// metody v buňce reprezentující instanci v diagramu instancí.
		ConfigurationFiles.commentsMap.put("ICMP_DialogTitle", "Titulek panelu v dialogu nastavení na záložce diagram instanci. Jedná se o titulek panelu pro nastavení metod v buňce v diagramu instancí.");
		languageCzProperties.setProperty("ICMP_DialogTitle", Constants.ICMP_BORDER_TITLE);
		
		ConfigurationFiles.commentsMap.put("ICMP_Chcb_Show_Text", "Texty pro komponenty JCheckBox v panelu pro nastavení metod v buňce reprezentující instanci v diagramu instancí. Jedná se o texty do dialogu nastavení na záložce diagram instancí pro panel s metodami.");
		languageCzProperties.setProperty("ICMP_Chcb_Show_Text", Constants.ICMP_CHCB_SHOW_TEXT);
		languageCzProperties.setProperty("ICMP_Chcb_ShowAll_Text", Constants.ICMP_CHCB_SHOW_ALL_TEXT);
		languageCzProperties.setProperty("ICMP_Chcb_UseSameFontAndColorAsReference_Text", Constants.ICMP_CHCB_USE_SAME_FONT_AND_COLOR_AS_REFERENCE_TEXT);
		
		
		ConfigurationFiles.commentsMap.put("ICMP_Chcb_Show_TT", "Texty do popisků pro komponenty JCheckBox v panelu pro nastavení metod v buňce reprezentující instanci v diagramu instancí. Jedná se o texty do dialogu nastavení na záložce diagram instancí pro panel s metodami.");
		languageCzProperties.setProperty("ICMP_Chcb_Show_TT", Constants.ICMP_CHCB_SHOW_TT);
		languageCzProperties.setProperty("ICMP_Chcb_ShowAll_TT", Constants.ICMP_CHCB_SHOW_ALL_TT);
		languageCzProperties.setProperty("ICMP_Chcb_UseSameFontAndColorAsReference_TT", Constants.ICMP_CHCB_USE_SAME_FONT_AND_COLOR_AS_REFERENCE_TT);
		
		ConfigurationFiles.commentsMap.put("ICMP_Lbl_ShowSpecificCount_Text", "Texty pro label, který informuje uživatele, že si může nastavit určitý počet metod, které se mohou zobrazit v buňce reprezentující instanci v diagramu instancí. Jedná se o texty do dialogu nastavení na záložce diagram instancí pro panel s metodami.");
		languageCzProperties.setProperty("ICMP_Lbl_ShowSpecificCount_Text", Constants.ICMP_LBL_SHOW_SPECIFIC_COUNT_TEXT);
		languageCzProperties.setProperty("ICMP_Lbl_ShowSpecificCount_TT", Constants.ICMP_LBL_SHOW_SPECIFIC_COUNT_TT);
		
		ConfigurationFiles.commentsMap.put("ICMP_Lbl_FontSize", "Texty pro labely, které obsahují informaci o tom, že si může uživatel nastavit velikost font a barvu písma pro metody, které mohou být zobrazeny v buňce reprezentující instanci v diagramu instancí. Jedná se o texty do dialogu nastavení na záložce diagram instancí pro panel s metodami.");
		languageCzProperties.setProperty("ICMP_Lbl_FontSize", Constants.ICMP_LBL_FONT_SIZE);
		languageCzProperties.setProperty("ICMP_Lbl_FontStyle", Constants.ICMP_LBL_FONT_STYLE);
		languageCzProperties.setProperty("ICMP_Lbl_TextColor", Constants.ICMP_LBL_TEXT_COLOR);
		
		ConfigurationFiles.commentsMap.put("ICMP_BtnChooseColor", "Text pro tlačítko 'Vybrat', které slouží pro otevření dialogu pro výběr barvy písma pro metody, které mohou být zobrazeny v buňce, která v diagramu instancí reprezentuje instanci. Jedná se o texty do dialogu nastavení na záložce diagram instancí pro panel s metodami.");
		languageCzProperties.setProperty("ICMP_BtnChooseColor", Constants.ICMP_BTN_CHOOSE_COLOR);
		
		ConfigurationFiles.commentsMap.put("ICMP_Txt_Clr_DialogTitle", "Titulek dialogu pro výběr barvy písma pro metody, které mohou být zobrazeny v buňce reprezentující instanci v diagramu instancí. Jedná se o text do titulku uvedeného dialogu, které je možné otevřít po kliknutí na tlačítko 'Vybrat' barvu pro písmo pro metody. Jedná se o text do dialogu nastavení na záložce diagram instancí pro panel s metodami.");
		languageCzProperties.setProperty("ICMP_Txt_Clr_DialogTitle", Constants.ICMP_TXT_CLR_DIALOG_TITLE);
		
		
		
		
		
		
		
		
		
		
		// Texty do panelu InstanceCellAttributesPanel v balíčku instanceDiagramPanel,
		// jedná se o texty pro komponenty, které slouží pro nastavení toho, zda a jak
		// se mají zobrazovat atributy v buňce, která v diagramu instancí reprezentuje
		// instance.
		ConfigurationFiles.commentsMap.put("ICAP_BorderTitle", "Titulek panelu s komponentami pro nastavení toho, zda se mají a případně 'jak' se mají zobrazovat atributy v buňce reprezentující instanci v diagramu instancí. Jedná se o texty do dialogu nastavení na záložce diagram instancí pro panel s atributy.");
		languageCzProperties.setProperty("ICAP_BorderTitle", Constants.ICAP_BORDER_TITLE);
		
		ConfigurationFiles.commentsMap.put("ICAP_Chcb_Show_Text", "Texty pro komponenty typu JCheckBox v panelu pro nastavení atributů v buňce reprezentující instanci v diagramu instancí. Jedná se o texty do dialogu nastavení na záložce diagram instancí pro panel s atributy.");
		languageCzProperties.setProperty("ICAP_Chcb_Show_Text", Constants.ICAP_CHCB_SHOW_TEXT);
		languageCzProperties.setProperty("ICAP_Chcb_ShowAll_Text", Constants.ICAP_CHCB_SHOW_ALL_TEXT);
		languageCzProperties.setProperty("ICAP_Chcb_UseSameFontAndColorAsReference_Text", Constants.ICAP_CHCB_USE_SAME_FONT_AND_COLOR_AS_REFERENCE_TEXT);
		
		ConfigurationFiles.commentsMap.put("ICAP_Chcb_Show_TT", "Texty pro popisky pro komponenty typu JCheckBox. Jedná se o popisky pro to, zda se například mají zobrazovat atributy v buňce reprezentující instanci, zda jen určitý počet nebo všechny atributy apod. Jedná se o texty do dialogu nastavení na záložce diagram instancí pro panel s atributy.");
		languageCzProperties.setProperty("ICAP_Chcb_Show_TT", Constants.ICAP_CHCB_SHOW_TT);
		languageCzProperties.setProperty("ICAP_Chcb_ShowAll_TT", Constants.ICAP_CHCB_SHOW_ALL_TT);
		languageCzProperties.setProperty("ICAP_Chcb_UseSameFontAndColorAsReference_TT", Constants.ICAP_CHCB_USE_SAME_FONT_AND_COLOR_AS_REFERENCE_TT);
		
		ConfigurationFiles.commentsMap.put("ICAP_Lbl_ShowSpecificCount_Text", "Texty pro label, který informuje uživatele, že si na příslušném místě může nastavit určitý počet atributů, který by se měl zobrazit v buňce reprezentující instanci v diagramu instancí. Jedná se o texty do dialogu nastavení na záložce diagram instancí pro panel s atributy.");
		languageCzProperties.setProperty("ICAP_Lbl_ShowSpecificCount_Text", Constants.ICAP_LBL_SHOW_SPECIFIC_COUNT_TEXT);
		languageCzProperties.setProperty("ICAP_Lbl_ShowSpecificCount_TT", Constants.ICAP_LBL_SHOW_SPECIFIC_COUNT_TT);
		
		ConfigurationFiles.commentsMap.put("ICAP_Lbl_FontSize", "Texty pro labely, které informují uživatele o tom, že si na příslušném místě může nastavit font a barvu písma, kterým budou atributy vykreslovány v příslušné buňce, která v diagramu instanci reprezentuje instanci. Jedná se o texty do dialogu nastavení na záložce diagram instancí pro panel s atributy.");
		languageCzProperties.setProperty("ICAP_Lbl_FontSize", Constants.ICAP_LBL_FONT_SIZE);
		languageCzProperties.setProperty("ICAP_Lbl_FontStyle", Constants.ICAP_LBL_FONT_STYLE);
		languageCzProperties.setProperty("ICAP_Lbl_TextColor", Constants.ICAP_LBL_TEXT_COLOR);
		
		ConfigurationFiles.commentsMap.put("ICAP_Btn_ChooseColor", "Text pro tlačítko 'Vybrat', které slouží pro otevření dialogu pro výběr barvy písma, kterým budou atributy vykreslovány v příslušné buňce, která v diagramu instancí reprezentuje instanci. Jedná se o texty do dialogu nastavení na záložce diagram instancí pro panel s atributy.");
		languageCzProperties.setProperty("ICAP_Btn_ChooseColor", Constants.ICAP_BTN_CHOOSE_COLOR);
		
		ConfigurationFiles.commentsMap.put("ICAP_Txt_Clr_DialogTitle", "Titulek dialogu pro výběr barvy písma pro atributy, které mohou být zobrazeny v buňce reprezentující instanci v diagramu instancí. Jedná se o texty do dialogu nastavení na záložce diagram instancí pro panel s atributy.");
		languageCzProperties.setProperty("ICAP_Txt_Clr_DialogTitle", Constants.ICAP_TXT_CLR_DIALOG_TITLE);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
				
		// Texty do CommandEditor:
		ConfigurationFiles.commentsMap.put("Sf_Ce_PanelBorderTitle", "Některé texty pro panel s komponentami pro nastavení parametrů editoru příkazů v dialogu nastavení.");
		languageCzProperties.setProperty("Sf_Ce_PanelBorderTitle", Constants.SF_CE_BORDER_TITLE);
		languageCzProperties.setProperty("Sf_Ce_CheckBoxJavaHighlightingSyntax", Constants.SF_CE_CHCB_HIGHLIGHTING_CODE);
		languageCzProperties.setProperty("Sf_Ce_LabelBackgroundColor", Constants.SF_CE_LBL_BACKGROUND_COLOR);

		// Text pro ohraničení OutputEditorPanelu:
		ConfigurationFiles.commentsMap.put("Sf_Oep_BorderTitle", "Text do dialogu nastavení, pro ohraničení panelu s komponentami pro nastavení parametrů pro editor výstupů.");
		languageCzProperties.setProperty("Sf_Oep_BorderTitle", Constants.SF_OEP_BORDER_TITLE);
		
		ConfigurationFiles.commentsMap.put("Sf_Oep_Chcb_ShowReferenceVariablesText", "Texty do dialogu nastavení pro komponentu JCheckBox pro nastavení, zda se mají vypisovat do editoru výstupů i reference v případě, že vypsaná hodnota je instance, která se nachází v diagramu instancí.");
		languageCzProperties.setProperty("Sf_Oep_Chcb_ShowReferenceVariablesText", Constants.SF_OEP_CHCB_SHOW_REFERENCE_VARIABLES_TEXT);
		languageCzProperties.setProperty("Sf_Oep_Chcb_ShowReferenceVariableTT", Constants.SF_OEP_CHCB_SHOW_REFERENCE_VARIABLES_TT);
				
		// Text pro ohraničení OutputFramePanelu:
		ConfigurationFiles.commentsMap.put("Sf_Ofp_BorderTitle", "Text do dialogu nastavení, pro ohraníčení panelu s komponentami pro nastavení parametrů pro výstupní terminál.");
		languageCzProperties.setProperty("Sf_Ofp_BorderTitle", Constants.SF_OFP_BORDER_TITLE);
				
				
		// Texty do SettingForm, do panelu ApplicationPanel - texty do dialogu pro umístění konfiguračních souborů:
		ConfigurationFiles.commentsMap.put("Sf_App_Fc_PlaceConfigurationDirText", "Texty do dialogu nastavení, pro záložku aplikace. Jedná se o text pro labely, které popisují, jaký konfigurační souboru (/ soubory) je možné vytvořit.");
		languageCzProperties.setProperty("Sf_App_Fc_PlaceConfigurationDirText", Constants.SF_APP_FC_CONFIGURATION_DIR);
		languageCzProperties.setProperty("Sf_App_Fc_PlaceLanguageDirText", Constants.SF_APP_FC_LANGUAGE_DIR);
		languageCzProperties.setProperty("Sf_App_Fc_PlaceDefaultPropertiesFileText", Constants.SF_APP_FC_DEFAULT_PROP);
		languageCzProperties.setProperty("Sf_App_Fc_PlaceClassDiagramPropertiesFileText", Constants.SF_APP_FC_CLASS_DIAGRAM_PROP);
		languageCzProperties.setProperty("Sf_App_Fc_PlaceInstanceDiagramPropertiesFileText", Constants.SF_APP_FC_INSTANCE_DIAGRAM_PROP);
		languageCzProperties.setProperty("Sf_App_Fc_PlaceCommandEditorPropertiesFileText", Constants.SF_APP_FC_COMMAND_EDITOR_PROP);
		languageCzProperties.setProperty("Sf_App_Fc_PlaceClassEditorPropertiesFileText", Constants.SF_APP_FC_CLASS_EDITOR_PROP);
		languageCzProperties.setProperty("Sf_App_Fc_PlaceOutputEditorPropertiesFileText", Constants.SF_APP_FC_OUTPUT_EDITOR_PROP);
		languageCzProperties.setProperty("Sf_App_Fc_PlaceOutputFramePropertiesFileText", Constants.SF_APP_FC_OUTPUT_FRAME_PROP);
		languageCzProperties.setProperty("Sf_App_Fc_PlaceCodeEditorForInternalFramesFileText", Constants.SF_APP_FC_CODE_EDITOR_FOR_INTERNAL_FRAMES_PROP);
		// Text pro ohraničení panelu CommentEdgePanel - hrana ke komentáři v ClassDiagramPanelu:
		languageCzProperties.setProperty("Sf_Cep_BorderTitle", Constants.SF_CEP_BORDER_TITLE);
				
				
				
		// Texty do CodeEditorPanelu:
		ConfigurationFiles.commentsMap.put("Sf_Cep_PanelBorderTitle", "Texty do dialogu nastavení, pro panel s komponentami pro nastavení parametrů pro editor zdrojového kódu. Jedná se o texty pro některé labely v uvedeném panelu na záložce editor zdrojového kódu.");
		languageCzProperties.setProperty("Sf_Cep_PanelBorderTitle", Constants.SF_CODE_EP_BORDER_TITLE);
		languageCzProperties.setProperty("Sf_Cep_CheckBoxJavaHighlightingSyntax", Constants.SF_CODE_EP_CHCB_HIGHLIGHTING_CODE);
		languageCzProperties.setProperty("Sf_Cep_CheckBoxJavaHighlightingSyntax_TT", Constants.SF_CODE_EP_CHCB_HIGHLIGHTING_CODE_TT);
		languageCzProperties.setProperty("Sf_Cep_CheckBoxHighlightingSyntaxTextByKindOfFile", Constants.SF_CODE_EP_CHCB_HIGHLIGHTING_CODE_BY_KIND_OF_FILE);
		languageCzProperties.setProperty("Sf_Cep_CheckBoxHighlightingSyntaxTextByKindOfFile_TT", Constants.SF_CODE_EP_CHCB_HIGHLIGHTING_CODE_BY_KIND_OF_FILE_TT);
		languageCzProperties.setProperty("Sf_Cep_CheckBoxCompileClassInBackground", Constants.SF_CODE_EP_CHCB_COMPILE_CLASS_IN_BACKGROUND);
		languageCzProperties.setProperty("Sf_Cep_CheckBoxCompileClassInBackground_TT", Constants.SF_CODE_EP_CHCB_COMPILE_CLASS_IN_BACKGROUND_TT);


        // Texty do třídy: cz.uhk.fim.fimj.settings_form.FormattingPropertiesPanel
        ConfigurationFiles.commentsMap.put("Fpp_BorderTitle", "Texty do dialogu nastavení do panelů pro nastavení vlastností pro formátování zdrojového kódu v dialogu editor zdrojového kódu a dialog průzkumník projektů.");
        languageCzProperties.setProperty("Fpp_BorderTitle", Constants.FPP_BORDER_TITLE);
        languageCzProperties.setProperty("Fpp_DeleteEmptyLine_Text", Constants.FPP_DELETE_EMPTY_LINE_TEXT);
        languageCzProperties.setProperty("Fpp_DeleteEmptyLine_TT", Constants.FPP_DELETE_EMPTY_LINE_TT);
        languageCzProperties.setProperty("Fpp_TabSpaceConversion_Text", Constants.FPP_TAB_SPACE_CONVERSION_TEXT);
        languageCzProperties.setProperty("Fpp_TabSpaceConversion_TT", Constants.FPP_TAB_SPACE_CONVERSION_TT);
        languageCzProperties.setProperty("Fpp_BreakElseIf_Text", Constants.FPP_BREAK_ELSE_IF_TEXT);
        languageCzProperties.setProperty("Fpp_BreakElseIf_TT", Constants.FPP_BREAK_ELSE_IF_TT);
        languageCzProperties.setProperty("Fpp_SetSingleStatement_Text", Constants.FPP_SET_SINGLE_STATEMENT_TEXT);
        languageCzProperties.setProperty("Fpp_SetSingleStatement_TT", Constants.FPP_SET_SINGLE_STATEMENT_TT);
        languageCzProperties.setProperty("Fpp_BreakOneLineBlock_Text", Constants.FPP_BREAK_ONE_LINE_BLOCK_TEXT);
        languageCzProperties.setProperty("Fpp_BreakOneLineBlock_TT", Constants.FPP_BREAK_ONE_LINE_BLOCK_TT);
        languageCzProperties.setProperty("Fpp_BreakClosingHeaderBracket_Text", Constants.FPP_BREAK_CLOSING_HEADER_BRACKET_TEXT);
        languageCzProperties.setProperty("Fpp_BreakClosingHeaderBracket_TT", Constants.FPP_BREAK_CLOSING_HEADER_BRACKET_TT);
        languageCzProperties.setProperty("Fpp_BreakBlock_Text", Constants.FPP_BREAK_BLOCK_TEXT);
        languageCzProperties.setProperty("Fpp_BreakBlock_TT", Constants.FPP_BREAK_BLOCK_TT);
        languageCzProperties.setProperty("Fpp_OperatorPaddingMode_Text", Constants.FPP_OPERATOR_PADDING_MODE_TEXT);
        languageCzProperties.setProperty("Fpp_OperatorPaddingMode_TT", Constants.FPP_OPERATOR_PADDING_MODE_TT);

				
				
		// Texty do ProjectExplorerCodeEditorPanelu:
		ConfigurationFiles.commentsMap.put("Sf_Pecep_PanelBorderTitle", "Popisek pro dialog nastavení, pro panel s komponentami pro nastavení parametrů pro editor zdrojového kódu v průzkumníku projektů na záložce průzkumník projektů - editor kódu.");
		languageCzProperties.setProperty("Sf_Pecep_PanelBorderTitle", Constants.SF_PECEP_BORDER_TITLE);
				
				
				
				
				
				
				
		// NewClassForm.java - texty do tohoto dialogu:
		// Titulek dialogu:
		ConfigurationFiles.commentsMap.put("NcfDialogTitle", "Texty do dialogu pro vytvoření nové třídy.");
		languageCzProperties.setProperty("NcfDialogTitle", Constants.NCF_DIALOG_TITLE);
		languageCzProperties.setProperty("NcfLabelClassName", Constants.NCF_LBL_CLASS);
		languageCzProperties.setProperty("NcfBorderTitleClassType", Constants.NCF_CLASS_KIND_BORDER_TITLE);
		languageCzProperties.setProperty("NcfLblPackageName", Constants.NCF_CLASS_LBL_PACKAGE_NAME);
		languageCzProperties.setProperty("NcfRadioButtonClass", Constants.NCF_RB_CLASS);
		languageCzProperties.setProperty("NcfRadioButtonAbstractClass", Constants.NCF_RB_ABSTRACT_CLASS);
		languageCzProperties.setProperty("NcfRadioButtonInterface", Constants.NCF_RB_INTERFACE);
		languageCzProperties.setProperty("NcfRadioButtonApplet", Constants.NCF_APPLET);
		languageCzProperties.setProperty("NcfRadioButtonEnum", Constants.NCF_RB_ENUM);
		languageCzProperties.setProperty("NcfRadioButtonUnitTest", Constants.NCF_RB_UNIT_TEST);
		
		// tlačítka:
		ConfigurationFiles.commentsMap.put("NcfButtonOk", "Texty do tlačítek v dialogu pro vytvoření nové třídy.");
		languageCzProperties.setProperty("NcfButtonOk", Constants.NCF_BTN_OK);
		languageCzProperties.setProperty("NcfButtonCancel", Constants.NCF_BTN_CANCEL);
		
		// Chybové hlášky:
		ConfigurationFiles.commentsMap.put("NcfErrorEmptyClassNameTitle", "Texty pro chybové hlášky, které mohou nastat v dialogu pro vytvoření nové třídy.");
		languageCzProperties.setProperty("NcfErrorEmptyClassNameTitle", Constants.NCF_EMPTY_CLASS_NAME_TITLE);
		languageCzProperties.setProperty("NcfErrorEmptyClassNameText", Constants.NCF_EMPTY_CLASS_NAME_TEXT);
		languageCzProperties.setProperty("NcfErrorFormatTitle", Constants.NCF_FORMAT_ERROR_TITLE);
		languageCzProperties.setProperty("NcfErrorFormatText", Constants.NCF_FORMAT_ERROR_TEXT);
		languageCzProperties.setProperty("NcfErrorClassNameTitle", Constants.NCF_END_CLASS_NAME_ERROR_TITLE);
		languageCzProperties.setProperty("NcfErrorClassNameText", Constants.NCF_END_CLASS_NAME_ERROR_TEXT);
		languageCzProperties.setProperty("NcfErrorKeyWordInClassNameTitle", Constants.NCF_ERROR_KEY_WORD_TITLE);
		languageCzProperties.setProperty("NcfErrorKeyWordInClassNameText", Constants.NCF_ERROR_KEY_WORD_TEXT);
		languageCzProperties.setProperty("NcfDuplicateClassNameErrorTitle", Constants.NCF_DUPLICATE_CLASS_NAME_ERROR_TITLE);
		languageCzProperties.setProperty("NcfDuplicateClassNameErrorText", Constants.NCF_DUPLICATE_CLASS_NAME_ERROR_TEXT);				
		languageCzProperties.setProperty("Ncf_WrongNameOfClassText", Constants.NCF_WRONG_NAME_OF_CLASS_TEXT);
		languageCzProperties.setProperty("NcfWrongNameOfClassTitle", Constants.NCF_WRONG_NAME_OF_CLASS_TITLE);
		languageCzProperties.setProperty("NcfTxtError", Constants.NCF_TXT_ERROR);				
		languageCzProperties.setProperty("Ncf_TextClassAlreadyExistInSamePackageText", Constants.NCF_TXT_CLASS_ALREADY_EXIST_IN_SAME_PACKAGE_TEXT);
		languageCzProperties.setProperty("Ncf_TextClassAlreadyExistInSamePackageTitle", Constants.NCF_TXT_CLASS_ALREADY_EXIST_IN_SAME_PACKAGE_TITLE);
		
		// Texty pro chybovou hlášku - že byl zadán balíček / balíčky v chybné syntaxi:		
		ConfigurationFiles.commentsMap.put("Ncf_TxtPackageIsInWrongSyntaxText_1", "Texty pro chybovou hlášku do dialogu pro vytvoření nové třídy ohledně toho, že uživatel zadal balíček, kam se má umístit třída nová třída ve špatném formátu.");
		languageCzProperties.setProperty("Ncf_TxtPackageIsInWrongSyntaxText_1", Constants.NCF_TXT_PACKAGE_IS_IN_WRONG_SYNTAX_TEXT_1);
		languageCzProperties.setProperty("Ncf_TxtPackageIsInWrongSyntaxText_2", Constants.NCF_TXT_PACKAGE_IS_IN_WRONG_SYNTAX_TEXT_2);
		languageCzProperties.setProperty("Ncf_TxtPackageIsInWrongSyntaxText_3", Constants.NCF_TXT_PACKAGE_IS_IN_WRONG_SYNTAX_TEXT_3);
		languageCzProperties.setProperty("Ncf_TxtPackageIsInWrongSyntaxTitle", Constants.NCF_TXT_PACKAGE_IS_IN_WRONG_SYNTAX_TITLE);
		
				
				
				
				
				
				
				
				
		// Tato třídy chybové hlášky do metoda: existSrcDirectory():
		ConfigurationFiles.commentsMap.put("WtfProjectDirErrorTitle", "Texty pro chybové hlášky, které mohou nastat v metodách ve třídě WriteToFile, konkrétně v metodách existBinDirectory a existSrcDirectory, které slouží pro získání cesty k adresářům src a bin v otevřeném projektu.");
		languageCzProperties.setProperty("WtfProjectDirErrorTitle", Constants.WTF_PROJECT_DIR_ERROR_TITLE);
		languageCzProperties.setProperty("WtfProjectDirErrorText", Constants.WTF_PROJECT_DIR_TEXT);
				
		languageCzProperties.setProperty("WtfProjectSrcDirJopFailedToCreateErrorTitle", Constants.WTF_PROJECT_SRC_DIR_FAILED_TO_CREATE_TITLE);
		languageCzProperties.setProperty("WtfProjectSrcDirJopFailedToCreateErrorText", Constants.WTF_PROJECT_SRC_DIR_FAILED_TO_CREATE_TEXT);
				
				
				
				
		// Třída WriteToFile, metoda: existSrcDirectory:
		ConfigurationFiles.commentsMap.put("WtfProjectBinDirErrorTitle", "Text pro další chybovou hlášku, která může nastat v metodě existBinDirectory ve třídě WriteToFile pro pokusu o vytvoření adresáře.");
		languageCzProperties.setProperty("WtfProjectBinDirErrorTitle", Constants.WTF_BIN_DIR_FAILED_TO_CREATE_ERROR_TITLE);
		languageCzProperties.setProperty("WtfProjectBinDirErrorText", Constants.WTF_BIN_DIR_FAILED_TO_CREATE_ERROR_TEXT);
				
				
				
				
				
				
		// Tato třída texty komentářů v metodě: createClass()
		ConfigurationFiles.commentsMap.put("WtfCommentForClass", "Texty, které slouží jako 'komentáře' v aplikací vytvořené třídě.");
		languageCzProperties.setProperty("WtfCommentForClass", Constants.WTF_TEXT_COMMENT_CLASS);
		languageCzProperties.setProperty("WtfCommentConstructor", Constants.WTF_TEXT_CONSTRUCTOR);
		
		// Texty chybových hláěek:		
		ConfigurationFiles.commentsMap.put("EtfErrorMessage1", "Texty pro chybové hlášky, které mohou nastat při vytváření tříd.");
		languageCzProperties.setProperty("EtfErrorMessage1", Constants.WTF_ERROR_MESSAGE1);
		languageCzProperties.setProperty("EtfErrorMessage2", Constants.WTF_ERROR_MESSAGE2);
		languageCzProperties.setProperty("EtfErrorMessage3", Constants.WTF_ERROR_MESSAGE3);
		languageCzProperties.setProperty("EtfErrorMessageTitle", Constants.WTF_ERROR_MESSAGE_TITLE);
				
				
				
				
				
				
		// Texty do chybových hlášek ve třídě: TestAndCreateEdge.java:
		ConfigurationFiles.commentsMap.put("TacMissingClassText", "Texty do chybových hlášek pro metody ve třídě TestAndCreateEdge. Jedná se o hlášky, které mohou nastat v metodách jako jsou například metody pro přidání hran typu asociace, dědičnost apod. Jak v diagramu tříd, tak ve zdrojových kódechpříslušných tříd.");
		languageCzProperties.setProperty("TacMissingClassText", Constants.TAC_MISSING_CLASS_TEXT);
		languageCzProperties.setProperty("TacMissingClassTitle", Constants.TAC_MISSING_CLASS_TITLE);
		languageCzProperties.setProperty("TacInheritErrorText", Constants.TAC_INHERIT_ERROR_TEXT);
		languageCzProperties.setProperty("TacInheritErrorTitle", Constants.TAC_INHERIT_ERROR_TITLE);
		languageCzProperties.setProperty("TacEnumInheritErrorText", Constants.TAC_ENUM_INHERIT_ERROR_TEXT);
		languageCzProperties.setProperty("TacEnumInheritErrorTitle", Constants.TAC_ENUM_INHERIT_ERROR_TITLE);
		languageCzProperties.setProperty("TacInterfaceErrorText", Constants.TAC_INTERFACE_ERROR_TEXT);
		languageCzProperties.setProperty("TacInterfaceErrorTitle", Constants.TAC_INTERFACE_ERROR_TITLE);
		languageCzProperties.setProperty("TacImplementsErrorText_1", Constants.TAC_IMPLEMENTS_ERROR_TEXT_1);
		languageCzProperties.setProperty("TacImplementsErrorText_2", Constants.TAC_IMPLEMENTS_ERROR_TEXT_2);
		languageCzProperties.setProperty("TacImplementsErrorTitle", Constants.TAC_IMPLEMENTS_ERROR_TITLE);
		languageCzProperties.setProperty("TacSelectedClassErrorText", Constants.TAC_SELECTED_CLASS_ERROR_TEXT);
		languageCzProperties.setProperty("TacSelectedClassErrorTitle", Constants.TAC_SELECTED_CLASS_ERROR_TITLE);
		languageCzProperties.setProperty("TacMissingSrcDirectoryText", Constants.TAC_MISSING_SRC_DIRECTORY_TEXT);
		languageCzProperties.setProperty("TacMissingSrcDirectoryTitle", Constants.TAC_MISSING_SRC_DIRECTORY_TITLE);
		languageCzProperties.setProperty("TacEnumInheritErrorText_2", Constants.TAC_ENUM_INHERIT_ERROR_TEXT_2);
		languageCzProperties.setProperty("TacEnumInheritErrorTitle_2", Constants.TAC_ENUM_INHERIT_ERROR_TITLE_2);
		languageCzProperties.setProperty("Tac_WrongSelectedObjectText", Constants.TAC_WRONG_ELECTED_OBJECT_TEXT);
		languageCzProperties.setProperty("Tac_WrongSelectedObjectTitle", Constants.TAC_WRONG_SELECTED_OBJECT_TITLE);
		languageCzProperties.setProperty("Tac_ChoosedCommentNotClassText", Constants.TAC_CHOOSED_COMMENT_NOT_CLASS_TEXT);
		languageCzProperties.setProperty("Tac_ChoosedCommentNotClassTitle", Constants.TAC_CHOOSED_COMMENT_NOT_CLASS_TITLE);
				
				
				
				
				
				
				
		// Info ohledně nápoědy do třídy OutpuEditor - editor výstupů
		// Pouze základní informace o příkazu s nápovědou:
		ConfigurationFiles.commentsMap.put("Oe_InfoAboutCommands", "Text pro úvodní informaci do editoru výstupů ohledně toho, že je možné vypsat přehled dostupných příkazů pro editor výstupů příslušnou metodou a nějaké klávesové zkratky.");
		languageCzProperties.setProperty("Oe_InfoAboutCommands", Constants.OE_INFO_ABOUT_COMMANDS);
		// Klávesové zkratky pro práci s editorem příkazů, které je vhodné znát, když s aplikací uživatel pracuje poprve:
		languageCzProperties.setProperty("Oe_InfoForCtrl_Enter", Constants.OE_INFO_FOR_CTRL_ENTER);
		languageCzProperties.setProperty("Oe_InfoForEnter_UpDown", Constants.OE_INFO_FOR_ENTER_UP_DOWN);
		languageCzProperties.setProperty("Oe_InfoForShift_Enter", Constants.OE_INFO_FOR_SHIFT_ENTER);
		languageCzProperties.setProperty("Oe_InfoForCtrl_Space", Constants.OE_INFO_FOR_CTRL_SPACE);
		languageCzProperties.setProperty("Oe_InfoForCtrl_H", Constants.OE_INFO_FOR_CTRL_H);
		languageCzProperties.setProperty("Oe_InfoForCtrl_Shift_H", Constants.OE_INFO_FOR_CTRL_SHIFT_H);

				
				
				
				
				
				
		// Texty do chybových hlášek do třídy EditClass.java:
		ConfigurationFiles.commentsMap.put("EcMissingClassTitle", "Texty pro chybové hlášky do třídy EditClass, která obsahuje metody pro editaci zdrojových kódů tříd. Například za účelem smazání či přidání proměnných, dědičnosti, implementace rozhraní apod.");
		languageCzProperties.setProperty("EcMissingClassTitle", Constants.ED_MISSING_CLASS_TITLE);
		languageCzProperties.setProperty("EcMissingClassText_1", Constants.ED_MISSING_CLASS_TEXT_1);
		languageCzProperties.setProperty("EcMissingClassText_2", Constants.ED_MISSING_CLASS_TEXT_2);
				
				
				
				
				// Texty do třídy: ClassFromFileForm.java:
		// tuitulek dialogu:
		ConfigurationFiles.commentsMap.put("Cfff_DialogTitle", "Titulek dialogu pro načtení třídy do otevřeného projektu pomocí tlačítka 'Přidat třídu ze souboru' v menu 'Editovat'.");
		languageCzProperties.setProperty("Cfff_DialogTitle", Constants.CFFF_DIALOG_TITLE);
		
		// texty popisků:
		ConfigurationFiles.commentsMap.put("Cfff_LabelChooseClass", "Texty do labelů v dialogu pro přidání nové třídy pomocí tlačítka 'Přidat třídu ze souboru'.");
		languageCzProperties.setProperty("Cfff_LabelChooseClass", Constants.CFFF_LBL_CHOOSE_CLASS);
		languageCzProperties.setProperty("Cfff_LabelPlace", Constants.CFFF_LBL_PLACE);
		languageCzProperties.setProperty("Cfff_LabelClassName", Constants.CFFF_LBL_CLASS_NAME);
		languageCzProperties.setProperty("Cfff_LabelClassNameTt", Constants.CFFF_LBL_CLASS_NAME_TT);
		
		// Texty tlačítek:
		ConfigurationFiles.commentsMap.put("Cfff_ButtonOk", "Texty do tlačítek v dialogu pro přidání nové třídy pomocí tlačítka 'Přidat třídu ze souboru'.");
		languageCzProperties.setProperty("Cfff_ButtonOk", Constants.CFFF_BTN_OK);
		languageCzProperties.setProperty("Cfff_ButtonCancel", Constants.CFFF_BTN_CANCEL);
		languageCzProperties.setProperty("Cfff_ButtonBrowse", Constants.CFFF_BTN_BROWSE);
		
		// texty chybových hlášek:
		ConfigurationFiles.commentsMap.put("Cfff_Jop_FormatErrorTitle", "Texty do chybových hlášek, které mohou nastat v dialogu pro přidání nové třídy pomocí tlačítka 'Přidat třídu ze souboru'.");
		languageCzProperties.setProperty("Cfff_Jop_FormatErrorTitle", Constants.CFFF_FORMAT_ERROR_TITLE);
		languageCzProperties.setProperty("Cfff_Jop_PathFormatErrorText", Constants.CFFF_PATH_FORMAT_ERROR_TEXT);
		languageCzProperties.setProperty("Cfff_Jop_EmptyFieldErrorTitle", Constants.CFFF_EMPTY_FIELD_ERROR_TITLE);
		languageCzProperties.setProperty("Cfff_Jop_EmptyFieldErrorText", Constants.CFFF_EMPTY_FIELD_ERROR_TEXT);
				
				
				
				
				
		// Texty do PopupMenu v classDiagramu:
		ConfigurationFiles.commentsMap.put("Ppm_ItemRename", "Texty do kontextového menu nad objekty v diagramu tříd.");
		languageCzProperties.setProperty("Ppm_ItemRename", Constants.PPM_ITEM_RENAME);
		languageCzProperties.setProperty("Ppm_ItemRemove", Constants.PPM_ITEM_REMOVE);
		languageCzProperties.setProperty("Ppm_ItemOpenInCodeEditor", Constants.PPM_ITEM_OPEN_IN_CODE_EDITOR);
		languageCzProperties.setProperty("Ppm_ItemOpenInProjectExplorer", Constants.PPM_ITEM_OPEN_IN_PROJECT_EXPLORER);
		languageCzProperties.setProperty("Ppm_ItemOpenInExplorerInOs_Text", Constants.PPM_ITEM_OPEN_IN_EXPLORER_IN_OS_TEXT);
		languageCzProperties.setProperty("Ppm_ItemOpenInExplorerInOs_TT", Constants.PPM_ITEM_OPEN_IN_EXPLORER_IN_OS_TT);
		languageCzProperties.setProperty("Ppm_ItemOpenInDefaultAppInOs_Text", Constants.PPM_ITEM_OPEN_IN_DEFAULT_APP_IN_OS_TEXT);
		languageCzProperties.setProperty("Ppm_ItemOpenInDefaultAppInOs_TT", Constants.PPM_ITEM_OPEN_IN_DEFAULT_APP_IN_OS_TT);
		languageCzProperties.setProperty("Ppm_ItemCopyAbsolutePath", Constants.PPM_ITEM_COPY_ABSOLUTE_PATH);
		languageCzProperties.setProperty("Ppm_ItemCopyRelativePath", Constants.PPM_ITEM_COPY_RELATIVE_PATH);
		languageCzProperties.setProperty("Ppm_ItemCopyReference", Constants.PPM_ITEM_COPY_REFERENCE);
		languageCzProperties.setProperty("Ppm_ItemOpenInEditor", Constants.PPM_ITEM_OPEN_IN_EDITOR);
		languageCzProperties.setProperty("Ppm_MenuCopyReference", Constants.PPM_MENU_COPY_REFERENCE);
		languageCzProperties.setProperty("Ppm_MenuStaticMethods", Constants.PPM_MENU_STATIC_METHODS);
		languageCzProperties.setProperty("Ppm_MenuInheritedStaticMethods", Constants.PPM_MENU_INHERITED_STATIC_METHODS);
		languageCzProperties.setProperty("Ppm_MenuInnerStaticClasses", Constants.PPM_MENU_INNER_CLASSES);
		languageCzProperties.setProperty("Ppm_MenuConstructors", Constants.PPM_MENU_CONSTRUCTORS);
		
		ConfigurationFiles.commentsMap.put("Ppm_ClassIsInRelationShipAlreadyTitle", "Texty do chybových hlášek, které mohou nastat při provádění nějaké z metod v kontextovém menu nad objekty v diagramu tříd.");
		languageCzProperties.setProperty("Ppm_ClassIsInRelationShipAlreadyTitle", Constants.PPM_JOP_CLASS_IS_IN_RELATIONSHIP_ALREADY_TITLE);
		languageCzProperties.setProperty("Ppm_ClassIsInRelationShipAlreadyText", Constants.PPM_JOP_CLASS_IS_IN_RELATIONSHIP_ALREADY_TEXT);
		languageCzProperties.setProperty("Ppm_MissingClassTitle", Constants.PPM_TXT_MISISNG_CLASS_TITLE);
		languageCzProperties.setProperty("Ppm_MissingClassText", Constants.PPM_TXT_MISING_CLASS_TEXT);
		languageCzProperties.setProperty("Ppm_Path", Constants.PPM_TXT_PATH);
		
		// První text do prvně vytvořené buňky, která reprezentuje komentář:
		ConfigurationFiles.commentsMap.put("FirstTextToCommentCell", "Výchozí text, který bude obsahovat buňka, která reprezentuje komentář v diagramu tříd při jejím vytvoření.");
		languageCzProperties.setProperty("FirstTextToCommentCell", Constants.CD_FIRST_TEXT_TO_COMMENT_CELL);
		
		// Texty kvůli spusteni - zavolani metody main:
		ConfigurationFiles.commentsMap.put("Ppm_Txt_FailedToCallTheMainMethod", "Texty do chybových hlášek, které mohou nastat při pokusu o zavolání metody 'main' pomocí kontextového menu nad buňkou reprezentující třídu v diagramu tříd.");
		languageCzProperties.setProperty("Ppm_Txt_FailedToCallTheMainMethod", Constants.PPM_FAILED_TO_CALL_THE_MAIN_METHOD);
		languageCzProperties.setProperty("Ppm_Txt_Error", Constants.PPM_TXT_ERROR);
		languageCzProperties.setProperty("Ppm_Txt_MethodIsInaccessible", Constants.PPM_TXT_METHOD_IS_INACCESSIBLE);
		languageCzProperties.setProperty("Ppm_Txt_IlegalArgumentException", Constants.PPM_TXT_ILLEGAL_ARGUMENT_EXCEPTION);
		languageCzProperties.setProperty("Ppm_Txt_InvocationTargetException", Constants.PPM_TXT_INVOCATION_TARGET_EXCEPTION);
		languageCzProperties.setProperty("Ppm_Txt_Start", Constants.PPM_TXT_START);
		languageCzProperties.setProperty("Ppm_Txt_FailedToCreateInstanceText", Constants.PPM_TXT_FAILED_TO_CREATE_INSTANCE_TEXT);
		languageCzProperties.setProperty("Ppm_Txt_FailedToCreateInstanceTitle", Constants.PPM_TXT_FAILED_TO_CREATE_INSTANCE_TITLE);
		// Hláška 'Úspěch' po zavolání statické metody:
		languageCzProperties.setProperty("Ppm_Txt_Success", Constants.PPM_TXT_SUCCESS);
		// V případě, že se nepodaří naléz cesty pro zkopírování do schránky:
		languageCzProperties.setProperty("Ppm_Txt_NotSpecified", Constants.PPM_TXT_NOT_SPECIFIED);
		// Text: 'Referenční proměnná' info za text proměnné pokud je hodnota instance z
		// ID:
		languageCzProperties.setProperty("Ppm_Txt_ReferenceVariable", Constants.PPM_TXT_REFERENCE_VARIABLE);
				
				
				
				
				
				
				
		// Některé texty do třídy - RenameClassForm:
		ConfigurationFiles.commentsMap.put("Rcf_DialogTitle", "Texty do diaogu pro přejmenování třídy. Dialog je možné zobrazit pomocí kontextového menu nad buňkou reprezentující třídu v diagramu tříd.");
		languageCzProperties.setProperty("Rcf_DialogTitle", Constants.RCF_DIALOG_TITLE);
		languageCzProperties.setProperty("Rcf_LabelNameOfClass", Constants.RCF_LBL_NAME_OF_CLASS);	
				
				
				
				
				
				
				
				
		// Třída: NewInstanceForm - veškeré texty:
		ConfigurationFiles.commentsMap.put("Nif_DialogTitle", "Texty do dialogu, který slouží pro získání dat od uživatele pro vytvoření nové instance třídy z diagramu tříd.");
		languageCzProperties.setProperty("Nif_DialogTitle", Constants.NIF_DIALOG_TITLE);
		languageCzProperties.setProperty("Nif_LabelInfoText", Constants.NIF_LBL_INFO_TEXT);
		languageCzProperties.setProperty("Nif_LabelReferenceName", Constants.NIF_LBL_REFERENCE_NAME);
		// Tlačítka:
		ConfigurationFiles.commentsMap.put("Nif_Button_OK", "Texty pro tlačítka do dialogu, který slouží pro získání dat od uživatele pro vytvoření nové instance třídy z diagramu tříd.");
		languageCzProperties.setProperty("Nif_Button_OK", Constants.NIF_BTN_OK);
		languageCzProperties.setProperty("Nif_Button_Cancel", Constants.NIF_BTN_CANCEL);

		
		// Texty do chybových hlášek:
		ConfigurationFiles.commentsMap.put("Nif_SomeFieldIsEmptyTitle", "Texty pro chybové hlášky, které mohou nastat v dialogu pro vytvoření instance třídy při pokusu o vytvoření instance třídy z diagramu tříd.");
		languageCzProperties.setProperty("Nif_SomeFieldIsEmptyTitle", Constants.NIF_FIELD_IS_EMPTY_ERROR_TITLE);
		languageCzProperties.setProperty("Nif_SomeFieldIsEmptyText", Constants.NIF_FIELD_IS_EMPTY_ERROR_TEXT);
		languageCzProperties.setProperty("Nif_ConstructorParametrFormatErrorTitle", Constants.NIF_PARAMETER_FORMAT_ERROR_TITLE);
		languageCzProperties.setProperty("Nif_ConstructorParametrFormatErrorText", Constants.NIF_PARAMETER_FORMAT_ERROR_TEXT);
		languageCzProperties.setProperty("Nif_NameOfReferenceIsAlreadyTakenTitle", Constants.NIF_NAME_OF_REFERENCE_IS_ALREADY_TAKEN_TITLE);
		languageCzProperties.setProperty("Nif_NameOfReferenceIsAlreadyTakenText", Constants.NIF_NAME_OF_REFERENCE_IS_ALREADY_TAKEN_TEXT);
		languageCzProperties.setProperty("Nif_ReferenceNameFormatErrorTitle", Constants.NIF_FORMAT_ERROR_REFERENCE_NAME_TITLE);
		languageCzProperties.setProperty("Nif_ReferenceNameFormatErrorText", Constants.NIF_FORMAT_ERROR_REFERENCE_NAME_TEXT);
		languageCzProperties.setProperty("Nif_ReferenceFieldIsEmptyTitle", Constants.NIF_EMTY_FIELD_ERROR_TITLE);
		languageCzProperties.setProperty("Nif_ReferenceFieldIsEmptyText", Constants.NIF_EMPTY_FIELD_ERROR_TEXT);
		languageCzProperties.setProperty("Nif_BadFormatSomeParameterTitle", Constants.NIF_BAD_FORMAT_SOME_PARAMETER_TITLE);
		languageCzProperties.setProperty("Nif_BadFormatSomeParameterText", Constants.NIF_BAD_FORMAT_SOME_PARAMETER_TEXT);
		languageCzProperties.setProperty("Nif_BadFormatReferenceNameText", Constants.NIF_BAD_FORMAT_REFERENCE_NAME);
		languageCzProperties.setProperty("Nif_CannotCallTheConstructorTitle", Constants.NIF_CANNOT_CALL_THE_CONSTRUCTOR_TITLE);
				
				
		// Texty jsou ve třídě FlexibleCountOfParameters:
		// Texty pro List:
		ConfigurationFiles.commentsMap.put("Fcop_TextListStringFormatError", "Texty pro chybové hlášky pro dialog pro zavolání metody a vytvoření instance. Jedná se o texty pro zadání parametru metody či konstruktoru s informacemi o hodnotách, které je možné zadat jako zmíněný parametr datového typu List. Tyto hlášky budou zobrazeny v případě zadání chybné syntaxe hodnoty pro parametr.");
		languageCzProperties.setProperty("Fcop_TextListStringFormatError", Constants.FCOP_TXT_LIST_STRING_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_TextListByteFormatError", Constants.FCOP_TXT_LIST_BYTE_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_TextListShortFormatError", Constants.FCOP_TXT_LIST_SHORT_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_TextListIntegerFormatError", Constants.FCOP_TXT_LIST_INTEGER_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_TextListLongFormatError", Constants.FCOP_TXT_LIST_LONG_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_TextListCharacterFormatError", Constants.FCOP_TXT_LIST_CHARACTER_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_TextListBooleanFormatError", Constants.FCOP_TXT_LIST_BOOLEAN_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_TextListFloatFormatError", Constants.FCOP_TXT_LIST_FLOAT_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_TextListDoubleFormatError", Constants.FCOP_TXT_LIST_DOUBLE_FORMAT_ERROR);
		
		// Texty pro kolekce:
		ConfigurationFiles.commentsMap.put("Fcop_TextCollectionByteFormatError", "Texty pro chybové hlášky pro dialog pro zavolání metody a vytvoření instance. Jedná se o texty pro zadání parametru metody či konstruktoru s informacemi o hodnotách, které je možné zadat jako zmíněný parametr typu kolekce. Tyto hlášky budou zobrazeny v případě zadání chybné syntaxe hodnoty pro parametr.");
		languageCzProperties.setProperty("Fcop_TextCollectionByteFormatError", Constants.FCOP_TXT__COLLECTION_BYTE_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_TextCollectionShortFormatError", Constants.FCOP_TXT__COLLECTION_SHORT_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_TextCollectionIntegerFormatError", Constants.FCOP_TXT__COLLECTION_INTEGER_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_TextCollectionLongFormatError", Constants.FCOP_TXT__COLLECTION_LONG_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_TextCollectionCharacterFormatError", Constants.FCOP_TXT__COLLECTION_CHARACTER_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_TextCollectionBooleanFormatError", Constants.FCOP_TXT__COLLECTION_BOOLEAN_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_TextCollectionFloatFormatError", Constants.FCOP_TXT__COLLECTION_FLOAT_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_TextCollectionDoubleFormatError", Constants.FCOP_TXT__COLLECTION_DOUBLE_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_TextCollectionStringFormatError", Constants.FCOP_TXT__COLLECTION_STRING_FORMAT_ERROR);
				
		// Texty pro jednorozměrné pole:
		ConfigurationFiles.commentsMap.put("Fcop_TextArraryFormatErrorForAll_1", "Texty pro chybové hlášky pro dialog pro zavolání metody a vytvoření instance. Jedná se o texty pro zadání parametru metody či konstruktoru s informacemi o hodnotách, které je možné zadat jako zmíněný parametr datového typu pole. Tyto hlášky budou zobrazeny v případě zadání chybné syntaxe hodnoty pro parametr.");
		languageCzProperties.setProperty("Fcop_TextArraryFormatErrorForAll_1", Constants.FCOP_TXT_ARRAY_FORMAT_ERROR_FOR_ALL_1);
		languageCzProperties.setProperty("Fcop_TextArrayFormatErrorAll_2", Constants.FCOP_TXT_ARRAY_FORMAT_ERROR_ALL_2);
		languageCzProperties.setProperty("Fcop_TextArrayCharacterFormatError_2", Constants.FCOP_TXT_ARRAY_CHARACTER_FORMAT_ERROR2);
		languageCzProperties.setProperty("Fcop_TextArrayBooleanFormatError_2", Constants.FCOP_TXT_ARRAY_BOOLEAN_FORMAT_ERROR2);
		languageCzProperties.setProperty("Fcop_TextArrayStringFormatError_2", Constants.FCOP_TXT_ARRAY_STRING_FORMAT_ERROR2);
				
				
		// Výchozí text pro pole a list:
		ConfigurationFiles.commentsMap.put("Fcop_DefaultTextForStringListAndArray_1", "Výchozí texty pro hodnoty datového typu List a pole. Jedná se o výchozí hodnoty, které se zobrazí v dialogu pro zavolní metody či konstruktoru s parametry / parametrem typu List nebo pole.");
		languageCzProperties.setProperty("Fcop_DefaultTextForStringListAndArray_1", Constants.FCOP_DEFAULT_TEXT_FOR_STRING_LIST_AND_ARRAY_1);
		languageCzProperties.setProperty("Fcop_DefaultTextForStringListAndArray_2", Constants.FCOP_DEFAULT_TEXT_FOR_STRING_LIST_AND_ARRAY_2);
		
				
		// Hodnoty pro dvojrozměrné pole - chybové hlášky ohledně správného formátu syntaxe pro dvojrozměrné pole:
		ConfigurationFiles.commentsMap.put("Fcop_Text_2_ArrayFormatErrorAll_1", "Začátek textu do chybové hlášky v případě, že dojde k zadání chybných hodnot pro dvojrozměrné pole.");
		languageCzProperties.setProperty("Fcop_Text_2_ArrayFormatErrorAll_1", Constants.FCOP_TXT_2_ARRAY_FORMAT_ERROR_ALL_1);
			
				
		// Proměnné - resp. texty do chybového oznámení, pokud se jedná o nepodporovaný datový typ parametru:
		ConfigurationFiles.commentsMap.put("Fcop_TextDataTypeOfParameter", "Texty pro chybová oznámení v případě, že datové typy nějakého parametru metody či konstruktoru nebudou podporovány v dialogu pro zvolání metody či konstruktoru.");
		languageCzProperties.setProperty("Fcop_TextDataTypeOfParameter", Constants.FCOP_TXT_DATA_TYPE_OF_PARAMETER);
		languageCzProperties.setProperty("Fcop_TextIsNotSupportedForConstructor", Constants.FCOP_TXT_IS_NOT_SUPPORTED_FOR_CONSTRUCTOR);
		languageCzProperties.setProperty("Fcop_TextIsNotSupportedForMethod", Constants.FCOP_TXT_IS_NOT_SUPPORTED_FOR_METHOD);
		languageCzProperties.setProperty("Fcop_TetxtConstructorCannotCall", Constants.FCOP_TXT_CONSTRUCTOR_CANNOT_CALL);
		languageCzProperties.setProperty("Fcop_TextMethodCannotCall", Constants.FCOP_TXT_METHOD_CANNOT_CALL);
				
		// Chybové hlášky do editoru výstupů v případě, že se nepodaří vytvořit instanci třídy:
		ConfigurationFiles.commentsMap.put("Fcop_TextFailedToCreateInstanceOfClass", "Texty do chybových hlášek do editoru výstupů v případě, že se nepodaří vytvořit intanci tříy.");
		languageCzProperties.setProperty("Fcop_TextFailedToCreateInstanceOfClass", Constants.FCOP_TXT_FAILED_TO_CREATE_INSTANCE_OF_CLASS);
		languageCzProperties.setProperty("Fcop_TextPossibleErrors", Constants.FCOP_TXT_POSSIBLE_ERRORS);
		languageCzProperties.setProperty("Fcop_TextPossibleErrorsWithPrivateType", Constants.FCOP_TXT_POSSIBLE_ERRORS_WITH_PRIVATE_TYPE);
		
		// Texty pro chybové výpisy, že nebyly nalezeny žádné instance v diagramu instancí, které by bylo možné předat do parametru metody nebo konstruktoru, takže jej nepůjde zavolat.
		ConfigurationFiles.commentsMap.put("Fcop_TextInsanceNotFoundMethodCantBeCall", "Texty pro chybové výpisy v případě, že nebyla nalezena žádná instance, kterou by bylo možné předat do parametru metody nebo konstruktoru. Proto jej nelze zavolat.");
		languageCzProperties.setProperty("Fcop_TextInsanceNotFoundMethodCantBeCall", Constants.FCOP_TXT_INSTANCE_NOT_FOUND_METHOD_CANT_BE_CALL);
		languageCzProperties.setProperty("Fcop_TextInsanceNotFoundConstructorCantBeCall", Constants.FCOP_TXT_INSTANCE_NOT_FOUND_CONSTRUCTOR_CANT_BE_CALL);
				
				
				
				
				
				
				
				
				
				
				
		// třída SelectMaimMethodForm.java:
		// název dialogu:
		ConfigurationFiles.commentsMap.put("Smmf_DialogTitle", "Titulek pro dialog, který slouží pro označení metody 'main', která se má zavolat. Tento dialog se zobrazí v případě, že se uživatel rozhodl spustit projekt a byly nalezeny alespoň dvě třídy, které obsahují metodu main 'main'.");
		languageCzProperties.setProperty("Smmf_DialogTitle", Constants.SMMF_DIALOG_TITLE);

		// Labely:
		ConfigurationFiles.commentsMap.put("Smmf_LabelInfo_1", "Texty do labelů s informacemi pro uživatele, že si má vybrat metodu 'main', která bude zavolána pro spuštění projektu. Texty do dialogu pro označení metody 'main' při spuštění projektu, dialog bude zobrazen, pokud se najde více jak jedna třída, která obsahuje metodu 'main'.");
		languageCzProperties.setProperty("Smmf_LabelInfo_1", Constants.SMMF_LBL_INFO_1);
		languageCzProperties.setProperty("Smmf_LabelInfo_2", Constants.SMMF_LBL_INFO_2);

		// Tlačítka:
		ConfigurationFiles.commentsMap.put("Smmf_Button_OK", "Texty do tlačítek v dialogu pro označení metody 'main' pro spuštění projektu.");
		languageCzProperties.setProperty("Smmf_Button_OK", Constants.SMMF_BTN_OK);
		languageCzProperties.setProperty("Smmf_Button_Cancel", Constants.SMMF_BTN_CANCEL);
		
		ConfigurationFiles.commentsMap.put("Smmf_TxtIsNotChoosedClassText", "Texty pro chybovou hlášku do dialogu pro označení metody 'main' pro spuštění projektu. Hláška se objeví v případě, že není označena třída, kde se má zavolat metoda 'main'.");
		languageCzProperties.setProperty("Smmf_TxtIsNotChoosedClassText", Constants.SMMF_TXT_IS_NOT_CHOOSED_CLASS_TEXT);
		languageCzProperties.setProperty("Smmf_TxtIsNotChoosedClassTitle", Constants.SMMF_TXT_IS_NOT_CHOOSED_CLASS_TITLE);
				
				
				
				
				
				
				
				
				
				
				
				
				
		// Třída: GetParametersMethodForm:
		ConfigurationFiles.commentsMap.put("Gpmf_DialogTitle", "Titulek dialogu pro zavolání metody (pomocí grafického rozhraní, ne editoru příkazů).");
		languageCzProperties.setProperty("Gpmf_DialogTitle", Constants.GPMF_DIALOG_TITLE);
		ConfigurationFiles.commentsMap.put("Gpmf_LabelTextInfo", "Text do labelu do dialogu pro zavolání metody.");
		languageCzProperties.setProperty("Gpmf_LabelTextInfo", Constants.GPMF_LBL_TEXT_INFO);
		
		// Tlačítka:
		ConfigurationFiles.commentsMap.put("Gpmf_Button_Ok", "Texty do tlačítek v dialogu pro zavolání metody.");
		languageCzProperties.setProperty("Gpmf_Button_Ok", Constants.GPMF_BTN_OK);
		languageCzProperties.setProperty("Gpmf_Button_Cancel", Constants.GPMF_BTN_CANCEL);
		
		// Tetxty do chybových hlášek:
		ConfigurationFiles.commentsMap.put("Gpmf_JopEmptyFieldParameterTitle", "Texty do chybových hlášek, které mohou nastat v dialogu pro zavolání metody. Například, pokud není vyplněné pole pro zadání hodnoty parametru metody, nebo je ve špatném formátu apod.");
		languageCzProperties.setProperty("Gpmf_JopEmptyFieldParameterTitle", Constants.GPMF_EMPTY_FIELD_PARAMETER_TITLE);
		languageCzProperties.setProperty("Gpmf_JopEmptyFieldParameterText", Constants.GPMF_EMPTY_FIELD_PARAMETER_TEXT);
		languageCzProperties.setProperty("Gpmf_JopFormatErrorParameterValueTitle", Constants.GPMF_FORMAT_ERROR_PARAMETER_VALUE_TITLE);
		languageCzProperties.setProperty("Gpmf_JopFormatErrorParameterValueText", Constants.GPMF_FORMAT_ERROR_PARAMETER_VALUE_TEXT);
		languageCzProperties.setProperty("Gpmf_JopBadFormatSomeParameterTitle", Constants.GPMF_BAD_FORMAT_SOME_PARAMETER_TITLE);
		languageCzProperties.setProperty("Gpmf_JopBadFormatSomeParameterText", Constants.GPMF_BAD_FORMAT_SOME_PARAMETER_TEXT);
		languageCzProperties.setProperty("Gpmf_JopCannotCallTheMethod", Constants.GPMF_CANNOT_CALL_THE_METHOD);
				
				
				
				
				
				
				
				
		// Třída CompileClass.java - texty do hlášek:
		ConfigurationFiles.commentsMap.put("Cp_TextError", "Texty pro výpisy hlášení ohledně kompilace tříd. Jedná se o texty pro úspěšné i neúspěšné výpisy do editoru výstupů.");
		languageCzProperties.setProperty("Cp_TextError", Constants.CP_TXT_ERROR);
		languageCzProperties.setProperty("Cp_TextLine", Constants.CP_TXT_LINE);
		languageCzProperties.setProperty("Cp_TextClass", Constants.CP_TXT_CLASS);
		languageCzProperties.setProperty("Cp_TextSourceNotFound", Constants.CP_TXT_SOURCE_NOT_FOUND);
		languageCzProperties.setProperty("Cp_TextClassNotFound", Constants.CP_TXT_CLASS_NOT_FOUND);
		languageCzProperties.setProperty("Cp_TextPath", Constants.CP_TXT_PATH);
		
		ConfigurationFiles.commentsMap.put("Cp_AllClassesAreCompiled", "Texty pro výpisy hlášení ohledně kompilace tříd. Jedná se o texty s informacemi o konkrétní chybě či úspěchu kompilace. Výpisy jsou vypisovány do editoru výstupů.");
		languageCzProperties.setProperty("Cp_AllClassesAreCompiled", Constants.CP_ALL_CLASSES_ARE_COMPILED);
		languageCzProperties.setProperty("Cp_AllClassesAreNotCompiled", Constants.CP_ALL_CLASSES_ARE_NOT_COMPILED);
		languageCzProperties.setProperty("Cp_ClassesNotFound", Constants.CP_CLASSES_ARE_NOT_FOUND);
		languageCzProperties.setProperty("Cp_ClassDiagramIsEmptyForCompilation", Constants.CP_CLASS_DIAGRAM_IS_EMPTY_FOR_COMPILATION);
				
		// Texty k chybě při vytváření kompilátoru - možné chyby:
		ConfigurationFiles.commentsMap.put("Cp_TextErrorWhileCreatingCompilatortext", "Texty pro chybové hlášky, které mohou nastat při neúspěšném vytvoření kompilátoru pro kompilaci (/ překládání) tříd.");
		languageCzProperties.setProperty("Cp_TextErrorWhileCreatingCompilatortext", Constants.CP_TXT_ERROR_WHILE_CREATEING_COMPILATOR_TEXT);
		languageCzProperties.setProperty("Cp_TextPossibleErrors", Constants.CP_TXT_POSSIBLE_ERRORS);
		languageCzProperties.setProperty("Cp_TextIsNotSetPathToJavaHomeDir", Constants.CP_TXT_IS_NOT_SET_PATH_TO_JAVA_HOME_DIR);
		languageCzProperties.setProperty("Cp_TextMissingToolsJarFile", Constants.CP_TXT_MISSING_TOOLS_JAR_FILE);
		languageCzProperties.setProperty("Cp_TextProbablyMissingFilesInLibTools", Constants.CP_TXT_PROBABLY_MISSING_FILES_IN_LIB_TOOLS);
		languageCzProperties.setProperty("Cp_TextMissingOrDeprecatedFiles", Constants.CP_TXT_MISSING_OR_DEPRECATED_FILES);
		languageCzProperties.setProperty("Cp_TextErrorWhileCreatingCompilatorTitle", Constants.CP_TXT_ERROR_WHILE_CREATING_COMPILATOR_TITLE);		
		languageCzProperties.setProperty("Cp_TextActualPath", Constants.CP_TXT_ACTUAL_PATH);
		languageCzProperties.setProperty("Cp_TextUnknown", Constants.CP_TXT_UNKNOWN);
		languageCzProperties.setProperty("Cp_TextJavaVersionsSupported", Constants.CP_TXT_JAVA_SUPPORTED_VERSIONS);
				
				
				
				
				
				
				
				
				
				
				
				
		// Výchozí texty do třídy: FlexibleCountOfParameters.java:
		ConfigurationFiles.commentsMap.put("Fcop_FormatErrorByte", "Texty pro chybová hlášení, která se mohou zobrazit v labelech u textových polí, kde byla zadána chybná hodnota. Texty obsahují informace o nastalé chybě. Jedná se o texty do dialogu pro zavolání metody či konstruktoru třídy (pomocí grafického rozhraní, ne editoru příkazů).");
		languageCzProperties.setProperty("Fcop_FormatErrorByte", Constants.FCOP_BYTE_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_FormatErrorShort", Constants.FCOP_SHORT_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_FormatErrorInt", Constants.FCOP_INT_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_FormatErrorLong", Constants.FCOP_LONG_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_FormatErrorFloat", Constants.FCOP_FLOAT_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_FormatErrorDouble", Constants.FCOP_DOUBLE_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_FormatErrorChar", Constants.FCOP_CHAR_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_FormatErrorString", Constants.FCOP_STRING_FORMAT_ERROR);
		languageCzProperties.setProperty("Fcop_DefaultValueForStringJtextField",Constants.FCOP_DEFAULT_VALUE_FOR_STRING_FIELD);
		languageCzProperties.setProperty("Fcop_LabelForParameter", Constants.FCOP_LBL_TEXT_INFO_PARAM);
		languageCzProperties.setProperty("Fcop_ErrorText_FieldContainsError",Constants.FCOP_ERROR_TEXT_FIELD_CONTAINS_ERRROR);
		languageCzProperties.setProperty("Fcop_ErrorText_ErrorForErrorList", Constants.FCOP_ERROR_TEXT_FOR_ERROR_LIST);	
		languageCzProperties.setProperty("Fcop_ErrorText_PnlMistakesBorderTitle", Constants.FCOP_PNL_MISTAKES_BORDER_TITLE);
		languageCzProperties.setProperty("Fcop_ErrorText_ChcbWrapLyrics", Constants.FCOP_CHCB_WRAP_LYRICS);
				
				
				
				
				
				
				
				
				
		// Texty do PopupMenu v diagramu instancí:
		ConfigurationFiles.commentsMap.put("Id_Pm_MenuItemInheritedMethods", "Texty do výchozích položek v kontextovém menu nad instancí třídy v diagramu instancí. Položky s těmito texty se v příslušném kontextovém menu budou nacházet vždy.");
		languageCzProperties.setProperty("Id_Pm_MenuItemInheritedMethods", Constants.ID_PM_MENU_ITEM_INHERITED_METHODS);
		languageCzProperties.setProperty("Id_Pm_MenuItemInheritedMethodsFromAncestors", Constants.ID_PM_MENU_ITEM_INHERITED_METHODS_FROM_ANCESTORS);
		languageCzProperties.setProperty("Id_Pm_ItemRemove", Constants.ID_PM_ITEM_REMOVE);
		languageCzProperties.setProperty("Id_Pm_ItemVievVariables", Constants.ID_PM_ITEM_VIEW_VARIABLES);
		languageCzProperties.setProperty("Id_Pm_Txt_Success", Constants.ID_PM_TXT_SUCCESS);
				
				
				
				
				
		// Texty do třídy VariablesInstanceOfClassFrom.java:
		// Texty pro ohraničení:
		ConfigurationFiles.commentsMap.put("Vioc_DialogTitle", "Texty pro popisky oken s hodnotami instance třídy. Tyto texty jsou určeny pro dialog, který zobrazuje přehled proměnných, metod a konstruktorů zvolené instance třídy v diagramu instancí.");
		languageCzProperties.setProperty("Vioc_DialogTitle", Constants.VIOC_DIALOG_TITLE);
		languageCzProperties.setProperty("Vioc_PanelConstructorsBorderTitle", Constants.VIOC_PNL_CONSTUCTORS_BORDER_TITLE);
		languageCzProperties.setProperty("Vioc_PanelMethodsBorderTitle", Constants.VIOC_PNL_METHODS_BORDER_TITLE);
		languageCzProperties.setProperty("Vioc_PanelVariablesBorderTitle", Constants.VIOC_PNL_VARIABLES_BORDER_TITLE);
		languageCzProperties.setProperty("Vioc_ChcbIncludeInheritedValues", Constants.VIOC_CHCB_INCLUDE_INHERITED_VALUES);
		languageCzProperties.setProperty("Vioc_ChcbIncludeInnerClassesValues", Constants.VIOC_CHCB_INCLUDE_INNER_CLASSES_VALUES);
		languageCzProperties.setProperty("Vioc_ChcbWrapValuesInJLists", Constants.VIOC_CHCB_WRAP_VALUES_IN_JLISTS);
		languageCzProperties.setProperty("Vioc_ChcbIncludeInterfaceMethods", Constants.VIOC_CHCB_INCLUDE_INTERFACE_METHODS);
		
		// Texty do okna:
		ConfigurationFiles.commentsMap.put("Vioc_LabelClassInfo_1", "Texty do dialogu pro vyhledávání dle hodnot v instanci třídy. V tomto dialogu lze vyhledávat konstruktory, metody a proměnné dané instance.");
		languageCzProperties.setProperty("Vioc_LabelClassInfo_1", Constants.VIOC_LBL_CLASS_INFO_1);
		languageCzProperties.setProperty("Vioc_LabelClassInfo_2", Constants.VIOC_LBL_CLASS_INFO_2);
		languageCzProperties.setProperty("Vioc_TextConstructor", Constants.VIOC_CONSTRUCTOR_TEXT);
		languageCzProperties.setProperty("Vioc_TextMethod", Constants.VIOC_METHOD_TEXT);
		languageCzProperties.setProperty("Vioc_TextVariable", Constants.VIOC_VARIABLE_TEXT);
		
		// Texty do tlačítek:
		ConfigurationFiles.commentsMap.put("Vioc_ButtonClose", "Texty do tlačítek do dialogu pro prohlížení instance třídy z diagramu tříd. Tento dialog se otevře po kliknutí na položku 'Prohlížet' v kontextovém menu nad buňkou, reprezentující instanci třídy v diagramu instancí.");
		languageCzProperties.setProperty("Vioc_ButtonClose", Constants.VIOC_BTN_CLOSE);
		languageCzProperties.setProperty("Vioc_ButtonSearchConstructor", Constants.VIOC_BTN_SEARCH_CONSTRUCTOR);
		languageCzProperties.setProperty("Vioc_ButtonCallConstructor", Constants.VIOC_BTN_CALL_CONSTRUCTOR);
		languageCzProperties.setProperty("Vioc_ButtonSearchMethod", Constants.VIOC_BTN_SEARCH_METHOD);
		languageCzProperties.setProperty("Vioc_ButtonsSearchVariable", Constants.VIOC_BTN_SEARCH_VARIABLE);
		languageCzProperties.setProperty("Vioc_ButtonsCreateReference", Constants.VIOC_BTN_CREATE_REFERENCE);
		languageCzProperties.setProperty("Vioc_ButtonsCallMethod", Constants.VIOC_BTN_CALL_METHOD);
		languageCzProperties.setProperty("Vioc_TxtSuccess", Constants.VIOC_TXT_SUCCESS);

		// Texty do třídy: cz.uhk.fim.fimj.instance_diagram.overview_of_instance.PopupMenu
        ConfigurationFiles.commentsMap.put("Ooi_Pm_ItemCopy", "Texty do kontextového menu pro operace nad položkami v dialogu s přehledem hodnot o instanci. Například zkopírování reference na konstruktor, proměnnou, metodu apod.");
        languageCzProperties.setProperty("Ooi_Pm_ItemCopy", Constants.OOI_PM_ITEM_COPY);
				
				
				
				
				
				
				
				
		// Texty do třídy: SearchForm.java:
		ConfigurationFiles.commentsMap.put("SForm_DialogTitle", "Texty do dialogu, který slouží pro vyhledání konstruktoru, metody či proměnné v nějaké instanci třídy. Jedná se o dialogu, který se otevře po kliknutí na tlačítko pro vyhledání jedné z uvedencýh položek v dialogu pro prohlížení hodnoty v instancí nějaké třídy. Tento dialog se otevře po kliknutí na položku 'Prohlížet' v kontextovém menu nad instancí třídy v diagramu instancí.");
		languageCzProperties.setProperty("SForm_DialogTitle", Constants.SFORM_DILOG_TITLE);
		languageCzProperties.setProperty("SForm_LabelInfo", Constants.SFORM_LBL_INFO);
		languageCzProperties.setProperty("SForm_ButtonClose", Constants.SFORM_BTN_CLOSE);
		languageCzProperties.setProperty("SForm_ButtonSearch", Constants.SFORM_BTN_SEARCH);
		languageCzProperties.setProperty("SForm_JspSearchedTextBorderTitle", Constants.SFORM_JSP_SEARCHED_TEXT);	
				
				
		
		
		// Texty do chybových hlášek ve třídě GraphInstance.java:
		ConfigurationFiles.commentsMap.put("Id_Jop_MissingObjectInGraphTitle", "Texty pro chybovou hlášku, která může nastat v diagramu instancí při odebírání instance, resp. buňky, která reprezentuje v diagramu instancích instanci nějaké třídy z diagramu tříd.");
		languageCzProperties.setProperty("Id_Jop_MissingObjectInGraphTitle", Constants.ID_MISSING_OBJECT_IN_GRAPH_TITLE);
		languageCzProperties.setProperty("Id_Jop_MissingObjectInGraphText", Constants.ID_MISSING_OBJECT_IN_GRAPH_TEXT);
				
				
				
				
				
				
				
		// Texty do třídy: SaveCodeAfretCloseDialog.java
		// Texty do chybových hlášek:
		ConfigurationFiles.commentsMap.put("Scacd_Jop_TxtSaveChangesText", "Texty pro chybové hlášky, které mohou nastat při ukládání upraveného zdrojového kódu zpět do třídu v editoru kódu.");
		languageCzProperties.setProperty("Scacd_Jop_TxtSaveChangesText", Constants.SCACD_SAVE_CHANGES_TEXT);
		languageCzProperties.setProperty("Scacd_Jop_txtSaveChangesTitle", Constants.SCACD_SAVE_CHANGES_TITLE);
		languageCzProperties.setProperty("Scacd_Jop_TxtPath", Constants.SCACD_PATH);
		languageCzProperties.setProperty("Scacd_Jop_TxtFailedToWriteCodeToClassText", Constants.SCACD_FAILED_TO_WRITE_CODE_TO_CLASS_TEXT);
		languageCzProperties.setProperty("Scacd_Jop_TxtFailedToWriteCodeToClassTitle", Constants.SCACD_FAILED_TO_WRITE_CODE_TO_CLASS_TITLE);
		languageCzProperties.setProperty("Scacd_Jop_TxtClassNotFoundTitle", Constants.SCACD_CLASS_NOT_FOUND_TITLE);
		languageCzProperties.setProperty("Scacd_Jop_TxtClassNotFoundText", Constants.SCACD_CLASS_NOT_FOUND_TEXT);
		languageCzProperties.setProperty("Scacd_Jop_TxtFailedToLoadClassTitle", Constants.SCAD_FAILED_TO_LOAD_CLASS_TITLE);
		languageCzProperties.setProperty("Scacd_Jop_TxtFailedToLoadClassText", Constants.SCAD_FAILED_TO_LOAD_CLASS_TEXT);	
				
				
				
		// Texty do třídy: Editor.java - pro editor příkazů (Command Editor) -
		// akorát text, že nebyl příkaz - syntaxe rozpoznán:
		ConfigurationFiles.commentsMap.put("Ce_ErrorText_SyntaxIsNotRecognized", "Text, který slouží jako chybové oznámení do editoru výstupů. Jedná se o hlášku, která se vypíše po zadání a potvrzení nějakého příkazu v editoru příkazů, ale tento příkaz nebude rozpoznán. Pak se vypíše tato hláška do editoru výstupů ohledně chybné syntaxe příkazu. Dále text 'referenční proměnná' pro zobrazení referencí v okně pro automatické doplňování / dokončování příkazů v editoru příkazů.");
		languageCzProperties.setProperty("Ce_ErrorText_SyntaxIsNotRecognized", Constants.CE_SYNTAX_IS_NOT_RECOGNIZED);
		languageCzProperties.setProperty("Ce_Reference_Variable", Constants.CE_REFERENCE_VARIABLE);

				
				
				
				
				
				
				
				
		// Texty do dialogu pro editoru zdrojového kódu (CodeEditorDialog =
		// CED):

		// Texty do Třídy v dialogu - JMenuForCodeEditor.java:
		// Hlavní menu:
		ConfigurationFiles.commentsMap.put("Ced_Mfce_MenuClass", "Texty do 'hlavních menu' do lišty menu v dialogu editor zdrojového kódu.");
		languageCzProperties.setProperty("Ced_Mfce_MenuClass", Constants.MFCE_MENU_CLASS);
		languageCzProperties.setProperty("Ced_Mfce_MenuEdit", Constants.MFCE_MENU_EDIT);
		languageCzProperties.setProperty("Ced_Mfce_MenuTools", Constants.MFCE_MENU_TOOLS);
		languageCzProperties.setProperty("Ced_Mfce_MenuClass_MenuOpenFileInOs", Constants.MFCE_MENU_CLASS_MENU_OPEN_FILE_IN_OS);

		// Texty do položek v menu MenuClass:
		ConfigurationFiles.commentsMap.put("Ced_Mfce_MenuClassItemSave", "Texty do položek v menu s názvem 'Třída' v liště menu v editoru zdrojového kódu.");
		languageCzProperties.setProperty("Ced_Mfce_MenuClassItemSave", Constants.MFCE_MENU_CLASS_ITEM_SAVE);
		languageCzProperties.setProperty("Ced_Mfce_MenuClassItemReload", Constants.MFCE_MENU_CLASS_ITEM_RELOAD);
		languageCzProperties.setProperty("Ced_Mfce_MenuClassItemPrint", Constants.MFCE_MENU_CLASS_ITEM_PRINT);
		languageCzProperties.setProperty("Ced_Mfce_MenuClassItemClose", Constants.MFCE_MENU_CLASS_ITEM_CLOSE);
        // Texty do položek v menu pro otevření otevřeného souboru v aplikaci OS nebo v průzkumníku souborů v OS:
        languageCzProperties.setProperty("Ced_Mfce_ItemOpenFileInFileExplorer", Constants.MFCE_ITEM_OPEN_FILE_IN_FILE_EXPLORER);
        languageCzProperties.setProperty("Ced_Mfce_ItemOpenFileInDefaultAppInOs", Constants.MFCE_ITEM_OPEN_FILE_IN_DEFAULT_APP_IN_OS);

		
		
		// texty do položek v menu MenuEdit:
		ConfigurationFiles.commentsMap.put("Ced_Mfce_MenuEditItemCut", "Texty do položek v menu s názvem 'Upravit' v liště menu v editoru zdrojového kódu.");
		languageCzProperties.setProperty("Ced_Mfce_MenuEditItemCut", Constants.MFCE_MENU_EDIT_ITEM_CUT);
		languageCzProperties.setProperty("Ced_Mfce_MenuEditItemCopy", Constants.MFCE_MENU_EDIT_ITEM_COPY);
		languageCzProperties.setProperty("Ced_Mfce_MenuEditItemPaste", Constants.MFCE_MENU_EDIT_ITEM_PASTE);
		languageCzProperties.setProperty("Ced_Mfce_MenuEditItemIndentMore", Constants.MFCE_MENU_EDIT_ITEM_INDENT_MORE);
		languageCzProperties.setProperty("Ced_Mfce_MenuEditItemIndentLess", Constants.MFCE_MENU_EDIT_ITEM_INDENT_LESS);
		languageCzProperties.setProperty("Ced_Mfce_MenuEditItemComment", Constants.MFCE_MENU_EDIT_ITEM_COMMENT);
		languageCzProperties.setProperty("Ced_Mfce_MenuEditItemUncomment", Constants.MFCE_MENU_EDIT_ITEM_UNCOMMENT);
		languageCzProperties.setProperty("Ced_Mfce_MenuEditItemFormatCode", Constants.MFCE_MENU_EDIT_ITEM_FORMAT_CODE);
		languageCzProperties.setProperty("Ced_Mfce_MenuEditItemInsertMethod", Constants.MFCE_MENU_EDIT_ITEM_INSERT_METHOD);

		
		
		// texty do položek v menu MenuTools:
		ConfigurationFiles.commentsMap.put("Ced_Mfce_MenuToolsItemReplace", "Texty do položek v menu s názvem 'Nástroje' v liště menu v editoru zdrojového kódu.");
		languageCzProperties.setProperty("Ced_Mfce_MenuToolsItemReplace", Constants.MFCE_MENU_TOOLS_ITEM_REPLACE);
		languageCzProperties.setProperty("Ced_Mfce_MenuToolsItemGoToLine", Constants.MFCE_MENU_TOOLS_ITEM_GO_TO_LINE);
		languageCzProperties.setProperty("Ced_Mfce_MenuToolsItemCompile", Constants.MFCE_MENU_TOOLS_ITEM_COMPILE);
		languageCzProperties.setProperty("Ced_Mfce_MenuToolsRadioButtonWrapText", Constants.MFCE_MENU_TOOLS_RADIO_BUTTON_WRAP_TEXT);
		languageCzProperties.setProperty("Ced_Mfce_MenuToolsRadioButtonShowWhiteSpace", Constants.MFCE_MENU_TOOLS_RADIO_BUTTON_SHOW_WHITE_SPACE);
		languageCzProperties.setProperty("Ced_Mfce_MenuToolsRadioButtonClearWhiteSpaceLine", Constants.MFCE_MENU_TOOLS_RADIO_BUTTON_CLEAR_WHITE_SPACE_LINE);
		languageCzProperties.setProperty("Ced_Mfce_MenuToolsItemAccessMethods", Constants.MFCE_MENU_TOOLS_ITEM_GETTERS_AND_SETTERS);
		languageCzProperties.setProperty("Ced_Mfce_MenuToolsItemConstructors", Constants.MFCE_MENU_TOOLS_ITEM_CONSTRUCTOR);
		languageCzProperties.setProperty("Ced_Mfce_MenuToolsMenuAddSerialID", Constants.MFCE_MENU_TOOLS_MENU_ADD_SERIAL_ID);
		languageCzProperties.setProperty("Ced_Mfce_MenuToolsItemAddDefaultSerialID", Constants.MFCE_MENU_TOOLS_ITEM_ADD_DEFAULT_SERIAL_UID);
		languageCzProperties.setProperty("Ced_Mfce_MenuToolsItemAddGeneratedSerialID", Constants.MFCE_MENU_TOOLS_ITEM_ADD_GENERATED_SERIAL_UID);
				
		// Popisky jednotlivých tlačítek v menu: menuClass:
		ConfigurationFiles.commentsMap.put("Ced_Mfce_MenuClassToolTipItemSave", "Texty s popisky položek v menu s názvem 'Třída' v liště menu v editoru zdrojového kódu.");
		languageCzProperties.setProperty("Ced_Mfce_MenuClassToolTipItemSave", Constants.MFCE_TOOL_TIP_ITEM_SAVE);
		languageCzProperties.setProperty("Ced_Mfce_MenuClassToolTipItemReload", Constants.MFCE_TOOL_TIP_ITEM_RELOAD);
		languageCzProperties.setProperty("Ced_Mfce_MenuClassToolTipItemPrint", Constants.MFCE_TOOL_TIP_ITEM_PRINT);
		languageCzProperties.setProperty("Ced_Mfce_MenuClassToolTipItemClose", Constants.MFCE_TOOL_TIP_ITEM_CLOSE);
        // Popsiky tlačítek v menu: menuOpenFileInAppsOfOs:
		languageCzProperties.setProperty("Ced_Mfce_ToolTipItemOpenFileInFileExplorerInOs", Constants.MFCE_TOOL_TIP_ITEM_OPEN_FILE_IN_FILE_EXPLORER_IN_OS);
		languageCzProperties.setProperty("Ced_Mfce_ToolTipItemOpenFileInDefaultAppInOs", Constants.MFCE_TOOL_TIP_ITEM_OPEN_FILE_IN_DEFAULT_APP_IN_OS);

		
		// Popisky jednotlivých tlačítek v menu: menuEdit:
		ConfigurationFiles.commentsMap.put("Ced_Mfce_MenuEditToolTipItemCut", "Texty s popisky položek v menu s názvem 'Upravit' v liště menu v editoru zdrojového kódu.");
		languageCzProperties.setProperty("Ced_Mfce_MenuEditToolTipItemCut", Constants.MFCE_TOOL_TIP_ITEM_CUT);
		languageCzProperties.setProperty("Ced_Mfce_MenuEditToolTipItemCopy", Constants.MFCE_TOOL_TIP_ITEM_COPY);
		languageCzProperties.setProperty("Ced_Mfce_MenuEditToolTipItemPaste", Constants.MFCE_TOOL_TIP_ITEM_PASTE);
		languageCzProperties.setProperty("Ced_Mfce_MenuEditToolTipItemIndentMore", Constants.MFCE_TOOL_TIP_ITEM_INDENT_MORE);
		languageCzProperties.setProperty("Ced_Mfce_MenuEditToolTipItemIndentLess", Constants.MFCE_TOOL_TIP_ITEM_INDENT_LESS);
		languageCzProperties.setProperty("Ced_Mfce_MenuEditToolTipItemComment", Constants.MFCE_TOOL_TIP_ITEM_COMMENT);
		languageCzProperties.setProperty("Ced_Mfce_MenuEditToolTipItemUncomment", Constants.MFCE_TOOL_TIP_ITEM_UNCOMMENT);
		languageCzProperties.setProperty("Ced_Mfce_MenuEditToolTipItemFormatCode", Constants.MFCE_TOOL_TIP_ITEM_FORMAT_CODE);
		languageCzProperties.setProperty("Ced_Mfce_MenuEditToolTipItemInsertMethod", Constants.MFCE_TOOL_TIP_ITEM_INSERT_METHOD);

		
		
		// Popisky jednotlivých tlačítek v menu: menuTools:
		ConfigurationFiles.commentsMap.put("Ced_Mfce_MenuToolsToolTipItemReplace", "Texty s popisky položek v menu s názvem 'Nástroje' v liště menu v editoru zdrojového kódu.");
		languageCzProperties.setProperty("Ced_Mfce_MenuToolsToolTipItemReplace", Constants.MFCE_TOOL_TIP_ITEM_REPLACE);
		languageCzProperties.setProperty("Ced_Mfce_MenuToolsToolTipItemGoToLine", Constants.MFCE_TOOL_TIP_ITEM_GO_TO_LINE);
		languageCzProperties.setProperty("Ced_Mfce_MenuToolsToolTipItemCompile", Constants.MFCE_TOOL_TIP_ITEM_COMPILE);
		languageCzProperties.setProperty("Ced_Mfce_MenuToolsToolTipItemToString", Constants.MFCE_TOOL_TIP_ITEM_TO_STRING);
		languageCzProperties.setProperty("Ced_Mfce_MenuToolsToolTipItemAccessMethods", Constants.MFCE_TOOL_TIP_ITEM_GETTERS_AND_SETTERS);
		languageCzProperties.setProperty("Ced_Mfce_MenuToolsToolTipItemConstructor", Constants.MFCE_TOOL_TIP_ITEM_CONSTRUCTOR);
		languageCzProperties.setProperty("Ced_Mfce_MenuToolsToolTipItemAddDefaultSerialId", Constants.MFCE_TOOL_TIP_ITEM_ADD_DEFAULT_SERIAL_UID);
		languageCzProperties.setProperty("Ced_Mfce_MenuToolsToolTipItemAddGeneratedSerialId", Constants.MFCE_TOOL_TIP_ITEM_ADD_GENERATED_SERIAL_UID);
				
		
		
		// Texty do chybových hlášek:
		ConfigurationFiles.commentsMap.put("Ced_Mfce_JopClassNotFoundText", "Texty pro chybové hlášky, které mohou nastat v dialogu editor zdrojového kódu. Dále jsou některé hlášky využity ve třídě SharedClassForForMethods, také pro zmíněný dialog editor zdrojového kódu.");
		languageCzProperties.setProperty("Ced_Mfce_JopClassNotFoundText", Constants.NFCE_JOP_CLASS_NOT_FOUND_TEXT);
		languageCzProperties.setProperty("Ced_Mfce_JopClassNotFoundTitle", Constants.NFCE_JOP_CLASS_NOT_FOUND_TITLE);
		languageCzProperties.setProperty("Ced_Mfce_TextPath", Constants.NFCE_TXT_PATH);
		languageCzProperties.setProperty("Ced_Mfce_JopSaveChangesText", Constants.NFCE_JOP_SAVE_CHANGES_TEXT);
		languageCzProperties.setProperty("Ced_Mfce_JopSaveChangesTitle", Constants.NFCE_JOP_SAVE_CHANGES_TITLE);
		languageCzProperties.setProperty("Ced_Mfce_JopErrorWhileSavingChangesToClassText_1", Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TEXT_1);
		languageCzProperties.setProperty("Ced_Mfce_JopErrorWhileSavingChangesToClassText_2", Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TEXT_2);
		languageCzProperties.setProperty("Ced_Mfce_JopErrorWhileSavingChangesToClassTitle", Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TITLE);
		languageCzProperties.setProperty("Ced_Mfce_JopClassDoesNotImplementInterfaceOrErrorText", Constants.NFCE_JOP_CLASS_DOES_NOT_IMPLEMENT_INTERFACE_OR_ERROR_TEXT);
		languageCzProperties.setProperty("Ced_Mfce_JopClassDoesNotImplementInterfaceOrErrorTitle", Constants.NFCE_JOP_CLASS_DOES_NOT_IMPLEMENT_INTERFACE_OR_ERROR_TITLE);
		languageCzProperties.setProperty("Ced_Mfce_TextCommentOnTheMethod", Constants.NFCE_TXT_COMMENT_ON_THE_METHOD);
		languageCzProperties.setProperty("Ced_Mfce_TextParametersDescription", Constants.NFCE_TXT_PARAMETER_DESCRIPTION);
		languageCzProperties.setProperty("Ced_Mfce_TextReturnValue", Constants.NFCE_TXT_RETURN_VALUE);
		languageCzProperties.setProperty("Ced_Mfce_TextPutYoutCodeHere", Constants.NFCE_TXT_PUT_YOUR_CODE_HERE);
		languageCzProperties.setProperty("Ced_Mfce_TextReturnStringValue", Constants.NFCE_TXT_RETURN_STRING_VALUE);
		languageCzProperties.setProperty("Ced_Mfce_TextCouldNotLoadClassFile", Constants.NFCE_TXT_COULD_NOT_LOAD_CLASS_FILE);
		languageCzProperties.setProperty("Ced_Mfce_TextClass", Constants.NFCE_TXT_CLASS);
		languageCzProperties.setProperty("Ced_Mfce_TextFileNotFound", Constants.NFCE_TXT_FILE_NOT_FOUND);
		languageCzProperties.setProperty("Ced_Mfce_FileDoesNotExistInPathText", Constants.MFCE_FILE_DOES_NOT_EXIST_IN_PATH_TEXT);
		languageCzProperties.setProperty("Ced_Mfce_FileDoesNotExistInPathTitle", Constants.MFCE_FILE_DOES_NOT_EXIST_IN_PATH_TITLE);
				
		
		
		// Třída GoToLineForm.java (GTLF)
		// Texty do titulku dialogu ,chybové hlášky a texty v dialogu:
		ConfigurationFiles.commentsMap.put("Ced_Gtlf_DialogTitle", "Texty do dialogu pro nastavení kurzoru na řádek se zadaným číslem. Jedná se o texty jak do titulku dialogu, tak do labelů v něm a chybové hlášky. Tento dialogu se zobrazí po kliknutí na položku 'Jdi na řádek' v menu 'Nástroje' v liště menu v dialogu editor zdrojového kódu.");
		languageCzProperties.setProperty("Ced_Gtlf_DialogTitle", Constants.GTLM_TITLE_DIALOG);
		languageCzProperties.setProperty("Ced_Gtlf_LabelLine", Constants.GTLM_LBL_LINE);
		languageCzProperties.setProperty("Ced_Gtlf_LabelErrorText", Constants.GTLM_LBL_ERROR_TEXT);
		languageCzProperties.setProperty("Ced_Gtlf_ButtonOk", Constants.GTLM_BTN_OK);
		languageCzProperties.setProperty("Ced_Gtlf_ButtonCancel", Constants.GTLM_BTN_CANCEL);
		languageCzProperties.setProperty("Ced_Gtlf_LineNumberIsNotCorrectText", Constants.GTLM_LINE_NUMBER_IS_NOT_CORRECT_TEXT);
		languageCzProperties.setProperty("Ced_Gtlf_LineNumberIsNotCorrectTitle", Constants.GTLM_LINE_NUMBER_IS_NOT_CORRECT_TITLE);
		languageCzProperties.setProperty("Ced_Gtlf_EmptyFieldText", Constants.GTLM_EMPTY_FIELD_TEXT);
		languageCzProperties.setProperty("Ced_Gtlf_EmptyFieldTitle", Constants.GTLM_EMPTY_FIELD_TITLE);
				
				
		
		
				// Třída JtoolBarForCodeEditor.java - texty do tlačítek a jejich popisky:
				// - Budou stejné jako z menu - jak popisky, tak texty z položek
				
				
				
				
		// Texty do třídy: ReplaceTextInCodeEditorForm.java
		ConfigurationFiles.commentsMap.put("Rtice_DialogTitle", "Texty do dialogu editor zdrojového kódu, který slouží pro nahrazení textů v příslušném editoru zdrojového kódu. Jedná se o veškeré texty do uvedeného dialogu, jak text do titulku dialogu, tak pro texty do příslušných labelů a chybových hlášek.");
		languageCzProperties.setProperty("Rtice_DialogTitle", Constants.RTICE_DIALOG_TITLE);
		languageCzProperties.setProperty("Rtice_LabelInfo", Constants.RTICE_LBL_INFO);
		languageCzProperties.setProperty("Rtice_LabelMissingValue", Constants.RTICE_LBL_MISSING_VALUE);
		languageCzProperties.setProperty("Rtice_LabelFind", Constants.RTICE_LBL_FIND);
		languageCzProperties.setProperty("Rtice_LabelReplace", Constants.RTICE_LBL_REPLACE);
		languageCzProperties.setProperty("Rtice_ButtonOk", Constants.RTICE_BTN_OK);
		languageCzProperties.setProperty("Rtice_ButtonCancel", Constants.RTICE_BTN_CANCEL);
		languageCzProperties.setProperty("Rtice_ChcbReplaceFirst", Constants.RTICE_CHCB_REPLACE_FIRST);
		languageCzProperties.setProperty("Rtice_ChcbReplaceAll", Constants.RTICE_CHCB_REPLACE_ALL);
		languageCzProperties.setProperty("Rtice_TextCodeDontContainsString", Constants.RTICE_TXT_CODE_DONT_CONTAINS_STRING);
		languageCzProperties.setProperty("Rtice_TextString", Constants.RTICE_TEXT_STRING);
		languageCzProperties.setProperty("Rtice_MissingValue", Constants.RTICE_MISSING_VALUE);
		languageCzProperties.setProperty("Rtice_EmptyFieldTitle", Constants.RTICE_EMPTY_FIELD_TITLE);
		languageCzProperties.setProperty("Rtice_EmptyFieldText", Constants.RTICE_EMPTY_FIELD_TEXT);
				
				
		// Texty do třídy: GenerateConstructorUsingFieldForm.java:
		ConfigurationFiles.commentsMap.put("Gcuff_DialogTitle", "Texty do dialogu pro vygenerovaní konstruktoru s označenými proměnnými (parametry konstruktoru). Tento dialog je možné otevřít po kliknutí na položku 'Vygenerovat konstruktor' v menu 'Nástroje' v liště menu v dialogu editor zdrojového kódu. jedná se o veškeré texty do uvedeného dialogu, jak titulek dialogu, tak veškeré texty do tlačítek, labelů atd.");
		languageCzProperties.setProperty("Gcuff_DialogTitle", Constants.GCUFF_DIALOG_TITLE);
		languageCzProperties.setProperty("Gcuff_LabelInfo", Constants.GCUFF_LBL_INFO);
		languageCzProperties.setProperty("Gcuff_ButtonOk", Constants.GCUFF_BTN_OK);
		languageCzProperties.setProperty("Gcuff_ButtonCancel", Constants.GCUFF_BTN_CANCEL);
		languageCzProperties.setProperty("Gcuff_ButtonSelectAll", Constants.GCUFF_BTN_SELECT_ALL);
		languageCzProperties.setProperty("Gcuff_ButtonDeselectAll", Constants.GCUFF_BTN_DESELECT_ALL);
		languageCzProperties.setProperty("Gcuff_ChcbGenerateDocDocument", Constants.GCUFF_CHCB_GENERATE_DOC_DOCUMENT);
		languageCzProperties.setProperty("Gcuff_PanelChcbModifierTitle", Constants.GCUFF_PNL_CHCB_MODIFIER);
				
				
				
		// Texty do třídy: GeneraceAccessMethodsform.java:
		ConfigurationFiles.commentsMap.put("Gamf_DialogTitle", "Texty do dialogu, který slouží pro vygenerování přístupových metod (getrů a setrů) proměnné (atributy) příslušné třídy. Tento dialogu lze otevřít po kliknutí na položku 'Generovat getry a setry' v menu 'Nástroje' v liště menu v dialogu editor zdrojového kódu. Jedna se o veškeré texty do uvedeného dialogu, jak titulek dialogu, tak texty do veškerých tlačítek, labelů, položek atd.");
		languageCzProperties.setProperty("Gamf_DialogTitle", Constants.GAMF_DIALOG_TITLE);
		languageCzProperties.setProperty("Gamf_LabelInfo", Constants.GAMF_LBL_INFO);
		languageCzProperties.setProperty("Gamf_ButtonSelectAll", Constants.GAMF_BTN_SELECT_ALL);
		languageCzProperties.setProperty("Gamf_ButtonDeselectAll", Constants.GAMF_BTN_DESELECT_ALL);
		languageCzProperties.setProperty("Gamf_CheckBoxGenerateComment", Constants.GAMF_CHCB_GENERATE_COMMENT);
		languageCzProperties.setProperty("Gamf_ButtonOk", Constants.GAMF_BTN_OK);
		languageCzProperties.setProperty("Gamf_ButtonCancel", Constants.GAMF_BTN_CANCEL);
		languageCzProperties.setProperty("Gamf_PanelModifiersBorderTitle", Constants.GAMF_PNL_MODIFIERS_BORDER_TITLE);
				
				
				
				
				
				
		// Texty do třídy: CodeEditor.java
		ConfigurationFiles.commentsMap.put("CodEdit_LoopForSummary", "Texty do dialogu zdrojového kódu, do samotného editoru zdrojového kódu, konkrétně do okna pro auto - doplňování, které se otevře po stisknutí kombibace kláves Ctrl + Space. Tyto texty slouží pro reprzentaci cyklů, které je možé vložit - pouze jako příklad.");
		languageCzProperties.setProperty("CodEdit_LoopForSummary", Constants.CODE_EDIT_LOOP_FOR_SUMMARY);
		languageCzProperties.setProperty("CodEdit_PreparedLoopForSummary", Constants.CODE_EDIT_PREPARED_LOOP_SUMMARY);
		languageCzProperties.setProperty("CodEdit_LoopWhileSummary", Constants.CODE_EDIT_LOOP_WHILE_SUMMARY);
		languageCzProperties.setProperty("CodEdit_LoopDoWhileSummary", Constants.CODE_EDIT_LOOP_DO_WHILE_SUMMARY);
		languageCzProperties.setProperty("CodEdit_LoopForEachSummary", Constants.CODE_EDIT_LOOP_FOR_REACH);
		languageCzProperties.setProperty("CodEdit_loopUsingLambdaSummary", Constants.CODE_EDIT_LOOP_USING_LAMBDA);
		languageCzProperties.setProperty("CodEdit_loopUsingIntStream", Constants.CODE_EDIT_LOOP_INT_STREAM);
		languageCzProperties.setProperty("CodEdit_InfiniteWhileLoop", Constants.CODE_EDIT_INFINITE_WHILE_LOOP);
		languageCzProperties.setProperty("CodEdit_PutYourCodeHere", Constants.CODE_EDIT_PUT_YOUR_CODE);
		languageCzProperties.setProperty("CodEdit_LoopForInfo", Constants.CODE_EDIT_LOOP_FOR_INFO);
		languageCzProperties.setProperty("CodEdit_LoopWhileInfo", Constants.CODE_EDIT_LOOP_WHILE_INFO);
		languageCzProperties.setProperty("CodEdit_LoopDoWhileInfo", Constants.CODE_EDIT_LOOP_DO_WHILE_INFO);
		languageCzProperties.setProperty("CodEdit_LoopForeachInfo", Constants.CODE_EDIT_LOOP_FOREACH_INFO);
		languageCzProperties.setProperty("CodEdit_LoopWithLambdaExpressionInfo", Constants.CODE_EDIT_LOOP_WITH_LAMBDA_INFO);
		languageCzProperties.setProperty("CodEdit_LoopWithIntStreamInfo", Constants.CODE_EDIT_LOOP_INT_STREAM_INFO);
		languageCzProperties.setProperty("CodEdit_InfiniteWhileLoopInfo", Constants.CODE_EDIT_INFINITE_WHILE_LOOP_INFO);
		// Přístupové metody:
		languageCzProperties.setProperty("CodEdit_GenerateSetter", Constants.CODE_EDIT_GENERATE_SETTER);
		languageCzProperties.setProperty("CodEdit_GenerateGetter", Constants.CODE_EDIT_GENERATE_GETTER);
		// Texty pro popisky cyklů v okně pro doplňování hodnoty:
        languageCzProperties.setProperty("CodEdit_Txt_WithVariable", Constants.CODE_EDIT_TXT_WITH_VARIABLE);
        languageCzProperties.setProperty("CodEdit_Txt_WithoutVariable", Constants.CODE_EDIT_TXT_WITHOUT_VARIABLE);


				
				
				
				
				
				
				
				
		// Veškeré texty do třídy makeOperation.java (Mo) - veškeré výpisy chyb,
		// apod. do editoru výstupů,...
		ConfigurationFiles.commentsMap.put("Mo_Txt_DontShareByZero", "Veškeré texty do třídy MakeOperation, tj. třída, která vykonává veškeré příkazy zadané do editoru příkazů. Tyto texty slouží jako výpisy do editoru výstupů jako hlášení stavu ohledně provední operace, například 'Úspěch' v případě úspěšného provedení operace, případně 'Neúspěch' a další.");
		languageCzProperties.setProperty("Mo_Txt_Success", Constants.MO_TXT_SUCCESS);
		languageCzProperties.setProperty("Mo_Txt_Failure", Constants.MO_TXT_FAILURE);
		languageCzProperties.setProperty("Mo_Txt_InstanceNotFound", Constants.MO_TXT_INSTANCE_NOT_FOUND);
		languageCzProperties.setProperty("Mo_Txt_MethodNotFound", Constants.MO_TXT_METHOD_NOT_FOUND);
		languageCzProperties.setProperty("Mo_Txt_BadNameOfMethodOrParameter", Constants.MO_TXT_BAD_NAME_OF_METHOD_OR_PARAMETER);
		languageCzProperties.setProperty("Mo_Txt_ReferenceNameNotFound", Constants.MO_TXT_REFERENCE_NAME_NOT_FOUND);
		languageCzProperties.setProperty("Mo_Txt_FailedRetypeValue", Constants.MO_TXT_FAILED_RETYPE_VALUE);
		languageCzProperties.setProperty("Mo_Txt_MissingVariableInInstanceOfClass", Constants.MO_TXT_MISSING_VARIABLE_IN_INSTANCE_OF_CLASS);
		languageCzProperties.setProperty("Mo_Txt_ErrorInVariableNameOfVisibilityVariable", Constants.MO_TXT_ERROR_IN_VARIABLE_NAME_OF_VISIBILITY_VARIABLE);

		languageCzProperties.setProperty("Mo_Txt_InstanceOfClassNotFound",Constants.MO_TXT_INSTANCE_OF_CLASS_NOT_FOUND);
		languageCzProperties.setProperty("Mo_Txt_AtLeastOneInstanceOfClassNotFoune", Constants.MO_TXT_AT_LEAST_ONE_INSTANCE_OF_CLASS_NOT_FOUND);
		languageCzProperties.setProperty("Mo_Txt_DataTypesAreNotEqual", Constants.MO_TXT_DATA_TYPES_ARE_NOT_EQUAL);
		languageCzProperties.setProperty("Mo_Txt_ErrorMethodOrParametersOrModifiers", Constants.MO_TXT_ERROR_METHOD_OR_PARAMETERS_OR_MODIFIER);

		languageCzProperties.setProperty("Mo_Txt_VariableNotFoundInReferenceToInstanceOfClass", Constants.MO_TXT_VARIABLE_NOT_FOUND_IN_REFERENCE_TO_INSTANCE_OF_CLASS);
		languageCzProperties.setProperty("Mo_Txt_FailedToFillVariable", Constants.MO_TXT_FAILED_TO_FILL_VARIABLE);
		languageCzProperties.setProperty("Mo_Txt_PossibleErrorsInFillVariable", Constants.MO_TXT_POSSIBLE_ERRORS_IN_FILL_VARIABLE);
		languageCzProperties.setProperty("Mo_Txt_ErrorsImmutableVisibilityPublic", Constants.MO_TXT_ERRORS_IMMUTABLE_VISIBILITY_PUBLIC);
		languageCzProperties.setProperty("Mo_Txt_VariableName", Constants.MO_TXT_VARIABLE_NAME);
		languageCzProperties.setProperty("Mo_Txt_AlreadyExist", Constants.MO_TXT_ALREADY_EXIST);
		languageCzProperties.setProperty("Mo_Txt_SrcDirNotFound", Constants.MO_TXT_SRC_DIR_NOT_FOUND);
		languageCzProperties.setProperty("Mo_Txt_ClassNotFound", Constants.MO_TXT_CLASS_NOT_FOUND);
		languageCzProperties.setProperty("Mo_Txt_FailedToLoadClassFile", Constants.MO_TXT_FILED_TO_LOAD_CLASS_FILE);
		languageCzProperties.setProperty("Mo_Txt_DataTypesOrConstructorNotFound", Constants.MO_TXT_DATA_TYPES_OR_CONSTRUCTOR_NOT_FOUND);
		
		// Texty pro zavolání statické metody:
		languageCzProperties.setProperty("Mo_Txt_ErrorWhileCallingStaticMethod_1", Constants.TXT_ERROR_WHILE_CALLING_STATIC_METHOD_1);
		languageCzProperties.setProperty("Mo_Txt_ErrorWhileCallingStaticMethod_2", Constants.TXT_ERROR_WHILE_CALLING_STATIC_METHOD_2);
		

		// Zbytek texů do této třídy MakeOperation.java - zbylé texty pro výpisy do editoru výstupů:
		ConfigurationFiles.commentsMap.put("Mo_Txt_OverViewOfCommands", "Veškeré texty do třída MakeOperation, tj. třída, která vykonává veškeré příkazy zadané do editoru příkazů. Zde se jedná o veškeré texty do editoru výstupů dle provedení operace (příkazu) z editoru příkazů.");
		languageCzProperties.setProperty("Mo_Txt_OverViewOfCommands", Constants.MO_TXT_OVER_VIEW_OF_COMMANDS);
		languageCzProperties.setProperty("Mo_Txt_OverviewOfCommandsEditorVariables", Constants.MO_TXT_OVERVIEW_OF_COMMANDS_EDITOR_VARIABLES);				
		languageCzProperties.setProperty("Mo_Txt_ErrorInIncrementValue", Constants.MO_TXT_ERROR_IN_INCREMENT_VALUE);
		languageCzProperties.setProperty("Mo_Txt_NumberOutOfInterval", Constants.MO_TXT_NUMBER_OUT_OF_INTERVAL);
		languageCzProperties.setProperty("Mo_Txt_ErrorInDecrementValue", Constants.MO_TXT_ERROR_IN_DECREMENT_VALUE);
		languageCzProperties.setProperty("Mo_Txt_VariableDontContainsNumber", Constants.MO_TXT_VARIABLE_DONT_CONTAINS_NUMBER);
		languageCzProperties.setProperty("Mo_Txt_FailedToCastNumber", Constants.MO_TXT_FAILED_TO_CAST_NUMBER);
		languageCzProperties.setProperty("Mo_Txt_ReferenceDontPointsToInstance", Constants.MO_TXT_REFERENCE_DONT_POINTS_TO_INSTANCE);
		languageCzProperties.setProperty("Mo_Txt_VariableIsNotPublicOrIsFinal", Constants.MO_TXT_VARIABLE_IS_NOT_PUBLIC_OR_IS_FINAL);
		languageCzProperties.setProperty("Mo_Txt_ReferenceDontPointsToInstanceOfClass", Constants.MO_TXT_REFERENCE_DOUNT_POINTS_TO_INSTANCE_OF_CLASS);
		languageCzProperties.setProperty("Mo_Txt_Reference", Constants.MO_TXT_REFERENCE);
		languageCzProperties.setProperty("Mo_Txt_Variable", Constants.MO_TXT_VARIABLE);
		languageCzProperties.setProperty("Mo_Txt_Error", Constants.MO_TXT_ERROR);
		languageCzProperties.setProperty("Mo_Txt_MethodWillNotBeInclude", Constants.MO_TXT_METHOD_WILL_NOT_BE_INCLUDE);
		languageCzProperties.setProperty("Mo_Txt_Method", Constants.MO_TXT_METHOD);
		languageCzProperties.setProperty("Mo_Txt_MethodHasNotBeenCalled", Constants.MO_TXT_METHOD_HAS_NOT_BEEN_CALLED);
		languageCzProperties.setProperty("Mo_Txt_ReferenceDoesNotPointsToInstanceOfClass", Constants.MO_TXT_REFERENCE_DOES_NOT_POINTS_TO_INSTANCE_OF_CLASS);
		languageCzProperties.setProperty("Mo_Txt_ValueWillNotBeCounted", Constants.MO_TXT_VALUE_WILL_NOT_BE_COUNTED);
		languageCzProperties.setProperty("Mo_Txt_ValueOfVariableWillNotBeCounted", Constants.MO_TXT_VALUE_OF_VARIABLE_WILL_NOT_BE_COUNTED);
		languageCzProperties.setProperty("Mo_Txt_DataTypeOfField", Constants.MO_TXT_DATA_TYPE_OF_FIELD);
		languageCzProperties.setProperty("Mo_Txt_DataTypeIsDifferent", Constants.MO_TXT_DATA_TYPE_IS_DIFFERENT);
		languageCzProperties.setProperty("Mo_Txt_Instance", Constants.MO_TXT_INSTANCE);
		languageCzProperties.setProperty("Mo_Txt_ErrorWhileConvertingValues", Constants.MO_TXT_ERROR_WHILE_CONVERTING_VALUES);
		languageCzProperties.setProperty("Mo_Txt_ToType", Constants.MO_TXT_TO_TYPE);
		languageCzProperties.setProperty("Mo_Txt_VariableNameMustBeKeyWork", Constants.MO_TXT_VARIABLE_NAME_MUST_BE_KEY_WORD);
		languageCzProperties.setProperty("Mo_Txt_ValueIsFinalAndCannotBeChanged", Constants.MO_TXT_VALUE_IS_FINAL_AND_CANNOT_BE_CHANGED);
		languageCzProperties.setProperty("Mo_Txt_ErrorInClassLoaderOrAccessToField", Constants.MO_TXT_ERROR_IN_CLASS_LOADER_OR_ACCESS_TO_FIELD);
		languageCzProperties.setProperty("Mo_Txt_DoNotFound", Constants.MO_TXT_DO_NOT_FOUND);
		languageCzProperties.setProperty("Mo_Txt_FailedToCastValue", Constants.MO_TXT_FAILED_TO_CAST_VALUE);
		languageCzProperties.setProperty("Mo_Txt_ToDataType", Constants.MO_TXT_TO_DATA_TYPE);
		languageCzProperties.setProperty("Mo_Txt_ErrorWhileParsingValue", Constants.MO_TXT_ERROR_WHILE_PARSING_VALUE);
		languageCzProperties.setProperty("Mo_Txt_ProbablyIsAbstractClass", Constants.MO_TXT_PROBABLY_IS_ABSTRACT_CLASS);
		languageCzProperties.setProperty("Mo_Txt_NotFoundConstructorToCastingDataType", Constants.MO_TXT_NOT_FOUND_CONSTRUCTOR_TO_CASTING_DATA_TYPE);
		languageCzProperties.setProperty("Mo_Txt_PossibleParametersCountIsDifferent", Constants.MO_TXT_POSSIBLE_PARAMETERS_COUNT_IS_DIFFERENT);
		languageCzProperties.setProperty("Mo_Txt_ExceptionWhileCallingConstructorToCasting", Constants.MO_TXT_EXCEPTION_WHILE_CALLING_CONSTRUCTOR_TO_CASTING);
		languageCzProperties.setProperty("Mo_Txt_VariableFailedToLoad", Constants.MO_TXT_VARIABLE_FAILED_TO_LOAD);
		languageCzProperties.setProperty("Mo_Txt_VariableNotFoundForInsertValue", Constants.MO_TXT_VARIABLE_NOT_FOUND_FOR_INSERT_VALUE);
		languageCzProperties.setProperty("Mo_Txt_VariableNotFound", Constants.MO_TXT_VARIABLE_NOT_FOUND);
		languageCzProperties.setProperty("Mo_Txt_Value", Constants.MO_TXT_VALUE);
		languageCzProperties.setProperty("Mo_Txt_VariableIsFinalIsNeedToFillInCreation", Constants.MO_TXT_VARIABLE_IS_FINAL_IS_NEED_TO_FILL_IN_CREATION);
		languageCzProperties.setProperty("Mo_Txt_MustNotBeFinalAndNull", Constants.MO_TXT_MUST_NOT_BE_FINAL_AND_NULL);
		languageCzProperties.setProperty("Mo_Txt_VariableNameIsNeedToChangeIsAlreadyTaken", Constants.MO_TXT_VARIABLE_NAME_IS_NEED_TO_CHANGE_IS_ALREADY_TAKEN);
		languageCzProperties.setProperty("Mo_Txt_VariableNameNotRecognized", Constants.MO_TXT_VARIABLE_NAME_NOT_RECOGNIZED);
		languageCzProperties.setProperty("Mo_Txt_DataTypeNotFoundOnlyPrimitiveDataType", Constants.MO_TXT_DATA_TYPE_NOT_FOUND_ONLY_PRIMITIVE_DATA_TYPE);
		languageCzProperties.setProperty("Mo_Txt_DataType", Constants.MO_TXT_DATA_TYPE);
		languageCzProperties.setProperty("Mo_Txt_FailedToCreateTemporaryVariable", Constants.MO_TXT_FAILED_TO_CREATE_TEMPORARY_VARIABLE);
		languageCzProperties.setProperty("Mo_Txt_Data", Constants.MO_TXT_DATA);
		languageCzProperties.setProperty("Mo_Txt_Value_2", Constants.MO_TXT_VALUE_2);
		languageCzProperties.setProperty("Mo_Txt_MayBeError", Constants.MO_TXT_MAY_BE_ERROR);
		languageCzProperties.setProperty("Mo_Txt_DataTypeDoNotSupported", Constants.MO_TXT_DATA_TYPE_DO_NOT_SUPPORTED);
		languageCzProperties.setProperty("Mo_Txt_VariableIsNullCannotBeIncrementOrDecrement", Constants.MO_TXT_VARIABLE_IS_NULL_CANNOT_BE_INCREMENT_OR_DECREMENT);
		languageCzProperties.setProperty("Mo_Txt_VariableIsNull", Constants.MO_TXT_VARIABLE_IS_NULL);
		languageCzProperties.setProperty("Mo_Txt_ValueWasNotRecognized", Constants.MO_TXT_VALUE_WAS_NOT_RECOGNIZED);
		languageCzProperties.setProperty("Mo_Txt_FailedToCreateVariable", Constants.MO_TXT_FAILED_TO_CREATE_VARIABLE);
		languageCzProperties.setProperty("Mo_Txt_DoNotRetrieveDataTypeOfVariable", Constants.MO_TXT_DO_NOT_RETRIEVE_DATA_TYPE_OF_VARIABLE);
		languageCzProperties.setProperty("Mo_Txt_FailedToGetDataAboutVariableWhileDeclaration", Constants.MO_TXT_FAILED_TO_GET_DATA_ABOUT_VARIABLE_WHILE_DECLARATION);
		languageCzProperties.setProperty("Mo_Txt_ReferenceDontPointToInstanceOfList", Constants.MO_TXT_REFERENCE_DONT_POINT_TO_INSTANCE_OF_LIST);
		languageCzProperties.setProperty("Mo_Txt_ValueAtIndexIsNull", Constants.MO_TXT_VALUE_AT_INDEX_IS_NULL);
		languageCzProperties.setProperty("Mo_Txt_ConditionsForIndexOfList", Constants.MO_TXT_CONDITIONS_FOR_INDEX_OF_LIST);				
		languageCzProperties.setProperty("Mo_Txt_SpecifiedIndexOfList", Constants.MO_TXT_SPECIFIED_INDEX_OF_LIST);
		languageCzProperties.setProperty("Mo_Txt_InstanceOfListWasNotFoundUnderReference", Constants.MO_TXT_INSTANCE_OF_LIST_WAS_NOT_FOUND_UNDER_REFERENCE);
		languageCzProperties.setProperty("Mo_Txt_SizeOfTheList", Constants.MO_TXT_SIZE_OF_THE_LIST);
		languageCzProperties.setProperty("Mo_Txt_DataTypeForListNotFound", Constants.MO_TXT_DATA_TYPE_FOR_LIST_NOT_FOUND);
		languageCzProperties.setProperty("Mo_Txt_DataTypeForArrayAreNotIdenticaly", Constants.MO_TXT_DATA_TYPE_FOR_ARRAY_ARE_NOT_IDENTICALY);
		languageCzProperties.setProperty("Mo_Txt_DataTypeForArrayNotFound", Constants.MO_TXT_DATA_TYPE_FOR_ARRAY_NOT_FOUND);
		languageCzProperties.setProperty("Mo_Txt_InstanceOfArrayWasNotFoundUnderReference", Constants.MO_TXT_INSTANCE_OF_ARRAY_WAS_NOT_FOUND_UNDER_REFERENCE);
		languageCzProperties.setProperty("Mo_Txt_ArrayIsFinalDontChangeValues", Constants.MO_TXT_ARRAY_IS_FINAL_DONT_CHANGE_VALUES);
		languageCzProperties.setProperty("Mo_Txt_Array", Constants.MO_TXT_ARRAY);
		languageCzProperties.setProperty("Mo_Txt_IndexOutOfRange", Constants.MO_TXT_INDEX_OUT_OF_RANGE);
		languageCzProperties.setProperty("Mo_Txt_ConditionalForIndexInArray", Constants.MO_TXT_CONDITIONAL_FOR_INDEX_IN_ARRAY);
		languageCzProperties.setProperty("Mo_Txt_Index", Constants.MO_TXT_INDEX);
		languageCzProperties.setProperty("Mo_Txt_SizeOfArray", Constants.MO_TXT_SIZE_OF_ARRAY);
		languageCzProperties.setProperty("Mo_Txt_Name", Constants.MO_TXT_NAME);
		languageCzProperties.setProperty("Mo_Txt_DoesNotIncrementDecrement", Constants.MO_TXT_DOES_NOT_INCREMENT_DECREMET);
		languageCzProperties.setProperty("Mo_Txt_IndexOfListIsOutOfRange", Constants.MO_TXT_INDEX_OF_LIST_IS_OUT_OF_RANGE);
		languageCzProperties.setProperty("Mo_Txt_VariableIsFinalDontChangeValue", Constants.MO_TXT_VARIABLE_IS_FINAL_DONT_CHANGE_VALUE);	
		languageCzProperties.setProperty("Mo_Txt_InstanceIsNotCorrectForParameter", Constants.MO_TXT_INSTANCE_IS_NOT_CORRECT_FOR_PARAMETER);
		languageCzProperties.setProperty("Mo_Txt_ArraySizeIsOutOfInteger", Constants.MO_TXT_ARRAY_SIZE_IS_OUT_OF_INTEGER);
		languageCzProperties.setProperty("Mo_Txt_ValueInArrayInIndexIsNull", Constants.MO_TXT_VALUE_IN_ARRAY_IN_INDEX_IS_NULL);
		languageCzProperties.setProperty("Mo_Txt_ValueAtIndexAtArrayCantIncrementDecrement", Constants.MO_TXT_VALUE_AT_INDEX_AT_ARRAY_CANT_INCRE_DECRE);		
		languageCzProperties.setProperty("Mo_Txt_FailedToCalculateMathTerm_1", Constants.MO_TXT_FAILED_TO_CALCULATE_MATH_TERM_1);
		languageCzProperties.setProperty("Mo_Txt_FailedToCalculateMathTerm_2", Constants.MO_TXT_FAILED_TO_CALCULATE_MATH_TERM_2);
		
				
				
		// Texty do metody writeCommands - texty s informacemi pro zacházení s
		// editorem příkazů.
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_MathCalculationTitle", "Texty, které popisují veškeré funkce editoru příkazů (tj. syntaxe pro zavolání metody, vytvoření instance, naplněnní či deklarace proměnných v instanci třídy či 'dočasné' proměnné v editoru příkazů, jejich pravidla apod.). Tyto texty se vypíší do editoru výstupů po zadání příkazu 'printCommands();' do editoru příkazů.");
		// Matematické operace:
		languageCzProperties.setProperty("Mo_Wc_Txt_MathCalculationTitle", Constants.MO_WC_TXT_MATH_CALCULATION_TITLE);
		languageCzProperties.setProperty("Mo_Wc_Txt_OperandsForMathCalculation", Constants.MO_WC_TXT_OPERANDS_FOR_MATH_CALCULATION);
		// Logické operaátory:
		languageCzProperties.setProperty("Mo_Wc_Txt_LogicalOperationsInfo", Constants.MO_WC_TXT_LOGICAL_OPERATIONS_INFO);
		//Konstanty:
		languageCzProperties.setProperty("Mo_Wc_Txt_Constants", Constants.MO_WC_TXT_CONSTANTS);
		//Podporované funkce:
		languageCzProperties.setProperty("Mo_Wc_Txt_SupportedFunctionsTitle", Constants.MO_WC_TXT_SUPPORTED_FUNCTIONS_TITLE);
		languageCzProperties.setProperty("Mo_Wc_Txt_NamesOfFunctionsInfo", Constants.MO_WC_TXT_NAMES_OF_FUNCTIONS_INFO);	
		// Příkaldy:
		languageCzProperties.setProperty("Mo_Wc_Txt_WithoutSemicolonAtEndsOfExpression", Constants.MO_WC_TXT_WITHOUT_SEMICOLON_AT_ENDS_OF_EXPRESSION);
		languageCzProperties.setProperty("Mo_Wc_Txt_IncludeVariablesFromCommandsEditorInfo", Constants.MO_WC_TXT_INCLUDE_VARIABLES_FROM_COMMANDS_EDITOR_INFO);
		languageCzProperties.setProperty("Mo_Wc_Txt_VarNamesCantContainsNumbers", Constants.MO_WC_TXT_VAR_NAMES_CANT_CONTAINS_NUMBERS);
		
		languageCzProperties.setProperty("Mo_Wc_Txt_DeclaredAndFulfilledVars", Constants.MO_WC_TXT_DECLARED_AND_FULFILLED_VARS);
		languageCzProperties.setProperty("Mo_Wc_Txt_DeclaredAndFulfilledVar", Constants.MO_WC_TXT_DECLARED_AND_FULFILLED_VAR);
		
		// Matematické operace:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_UnaryPlus", "Matematické operace.");
		languageCzProperties.setProperty("Mo_Wc_Txt_UnaryPlus", Constants.MO_WC_TXT_UNARY_PLUS);
		languageCzProperties.setProperty("Mo_Wc_Txt_UnaryMinus", Constants.MO_WC_TXT_UNARY_MINUS);
		languageCzProperties.setProperty("Mo_Wc_Txt_MultiplicationOperator", Constants.MO_WC_TXT_MULTIPLICATION_OPERATOR);
		languageCzProperties.setProperty("Mo_Wc_Txt_DivisionOperator", Constants.MO_WC_TXT_DIVISION_OPERATOR);
		languageCzProperties.setProperty("Mo_Wc_Txt_ModuloOperator", Constants.MO_WC_TXT_MODULO_OPERATOR);
		languageCzProperties.setProperty("Mo_Wc_Txt_PoweredOperator", Constants.MO_WC_TXT_POWERED_OPERATOR);
		
		// Logické operátory:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_Equals", "Logické operátory.");
		languageCzProperties.setProperty("Mo_Wc_Txt_Equals", Constants.MO_WC_TXT_EQUALS);
		languageCzProperties.setProperty("Mo_Wc_Txt_NotEquals", Constants.MO_WC_TXT_NOT_EQUALS);
		languageCzProperties.setProperty("Mo_Wc_Txt_LessThan", Constants.MO_WC_TXT_LESS_THAN);
		languageCzProperties.setProperty("Mo_Wc_Txt_LessThanOrEqualTo", Constants.MO_WC_TXT_LESS_THAN_OR_EQUAL_TO);
		languageCzProperties.setProperty("Mo_Wc_Txt_GreaterThan", Constants.MO_WC_TXT_GREATER_THAN);
		languageCzProperties.setProperty("Mo_Wc_Txt_GreaterThanOrEqualTo", Constants.MO_WC_TXT_GREATER_THAN_OR_EQUAL_TO);
		languageCzProperties.setProperty("Mo_Wc_Txt_BooleanAnd", Constants.MO_WC_TXT_BOOLEAN_AND);
		languageCzProperties.setProperty("Mo_Wc_Txt_BooleanOr", Constants.MO_WC_TXT_BOOLEAN_OR);
		
		// Konstanty:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_ValueOfE", "Konstanty.");
		languageCzProperties.setProperty("Mo_Wc_Txt_ValueOfE", Constants.MO_WC_TXT_VALUE_OF_E);
		languageCzProperties.setProperty("Mo_Wc_Txt_ValueOfPI", Constants.MO_WC_TXT_VALUE_OF_PI);
		languageCzProperties.setProperty("Mo_Wc_Txt_ValueOne", Constants.MO_WC_TXT_VALUE_ONE);
		languageCzProperties.setProperty("Mo_Wc_Txt_ValueZero", Constants.MO_WC_TXT_VALUE_ZERO);
		
		// Podporované funkce:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_FceNot", "Podporované funkce.");
		languageCzProperties.setProperty("Mo_Wc_Txt_FceNot", Constants.MO_WC_TXT_FCE_NOT);
		languageCzProperties.setProperty("Mo_Wc_Txt_Condition", Constants.MO_WC_TXT_CONDITION);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceRandom", Constants.MO_WC_TXT_FCE_RANDOM);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceMin", Constants.MO_WC_TXT_FCE_MIN);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceMax", Constants.MO_WC_TXT_FCE_MAX);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceAbs", Constants.MO_WC_TXT_FCE_ABS);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceRound", Constants.MO_WC_TXT_FCE_ROUND);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceFloor", Constants.MO_WC_TXT_FCE_FLOOR);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceCelling", Constants.MO_WC_TXT_FCE_CELLING);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceLog", Constants.MO_WC_TXT_FCE_LOG);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceLog10", Constants.MO_WC_TXT_FCE_LOG_10);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceSqrt", Constants.MO_WC_TXT_FCE_SQRT);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceSin", Constants.MO_WC_TXT_FCE_SIN);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceCos", Constants.MO_WC_TXT_FCE_COS);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceTan", Constants.MO_WC_TXT_FCE_TAN);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceCot", Constants.MO_WC_TXT_FCE_COT);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceAsin", Constants.MO_WC_TXT_FCE_ASIN);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceAcos", Constants.MO_WC_TXT_FCE_ACOS);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceAtan", Constants.MO_WC_TXT_FCE_ATAN);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceAcot", Constants.MO_WC_TXT_FCE_ACOT);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceAtan2", Constants.MO_WC_TXT_FCE_ATAN2);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceSinh", Constants.MO_WC_TXT_FCE_SINH);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceCosh", Constants.MO_WC_TXT_FCE_COSH);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceTah", Constants.MO_WC_TXT_FCE_TAH);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceCoth", Constants.MO_WC_TXT_FCE_COTH);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceSec", Constants.MO_WC_TXT_FCE_SEC);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceCsc", Constants.MO_WC_TXT_FCE_CSC);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceSech", Constants.MO_WC_TXT_FCE_SECH);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceCsch", Constants.MO_WC_TXT_FCE_CSCH);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceAsinh", Constants.MO_WC_TXT_FCE_ASINH);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceAcosh", Constants.MO_WC_TXT_FCE_ACOSH);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceTanh", Constants.MO_WC_TXT_FCE_TANH);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceRad", Constants.MO_WC_TXT_FCE_RAD);
		languageCzProperties.setProperty("Mo_Wc_Txt_FceDeg", Constants.MO_WC_TXT_FCE_DEG);
		
		
		// Nějkaé ukázky pro matematické výrazy ,které je možné zadat (podmínky, funkce, jejich využití, ...)
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_ExamplesOfMathExpressions", "Ukázky matematických výrazů, které je možné vypočítat.");
		languageCzProperties.setProperty("Mo_Wc_Txt_ExamplesOfMathExpressions", Constants.MO_WC_TXT_EXAMPLES_OF_MATH_EXPRESSIONS);		
		languageCzProperties.setProperty("Mo_Wc_Txt_MakeInstance", Constants.MO_WC_TXT_MAKE_INSTANCE);
		languageCzProperties.setProperty("Mo_Wc_Txt_ClassName", Constants.MO_WC_TXT_CLASS_NAME);
		languageCzProperties.setProperty("Mo_Wc_Txt_MakeInstanceParameters_1", Constants.MO_WC_TXT_MAKE_INSTANCE_PARAMETERS_1);
		languageCzProperties.setProperty("Mo_Wc_Txt_MakeInstanceParameters_2", Constants.MO_WC_TXT_MAKE_INSTANCE_PARAMETERS_2);
		languageCzProperties.setProperty("Mo_Wc_Txt_MakeInstanceParameters_3", Constants.MO_WC_TXT_MAKE_INSTANCE_PARAMETERS_3);
		languageCzProperties.setProperty("Mo_Wc_Txt_WithParameters", Constants.MO_WC_TXT_WITH_PARAMETERS);
		languageCzProperties.setProperty("Mo_Wc_Txt_Call_Constructor_Info_1", Constants.MO_WC_TXT_CALL_CONSTRUCTOR_INFO_1);
		languageCzProperties.setProperty("Mo_Wc_Txt_Call_Constructor_Info_2", Constants.MO_WC_TXT_CALL_CONSTRUCTOR_INFO_2);
		languageCzProperties.setProperty("Mo_Wc_Txt_Call_Constructor_Info_3", Constants.MO_WC_TXT_CALL_CONSTRUCTOR_INFO_3);		
		languageCzProperties.setProperty("Mo_Wc_Txt_CallMethodInfo", Constants.MO_WC_TXT_CALL_METHOD_INFO);
		languageCzProperties.setProperty("Mo_Wc_Txt_Call_Static_Method_Info", Constants.MO_WC_TXT_CALL_STATIC_METHOD_INFO);
		languageCzProperties.setProperty("Mo_Wc_Txt_Call_Static_Method_In_Inner_Class_Info", Constants.MO_WC_TXT_CALL_STATIC_METHOD_IN_INNER_CLASS_INFO);
		languageCzProperties.setProperty("Mo_Wc_Txt_CallMethodInfo_1", Constants.MO_WC_TXT_CALL_METHOD_INFO_1);
		languageCzProperties.setProperty("Mo_Wc_Txt_CallMethodInfo_2", Constants.MO_WC_TXT_CALL_METHOD_INFO_2);
		
		// Info ohledně zavolání zděděných metod nad třídou coby potomkem příslušné
		// třídy s danou metodou:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_InfoAboutMethodsTitle", "Informace ohledně zavolání zděděných metod nad třídou, která je potoměk příslušné třídy s metodou.");
		languageCzProperties.setProperty("Mo_Wc_Txt_InfoAboutMethodsTitle", Constants.MO_WC_TXT_INFO_ABOUT_METHODS_TITLE);
		languageCzProperties.setProperty("Mo_Wc_Txt_InfoAboutCallMethodsFromObjectClass", Constants.MO_WC_TXT_INFO_ABOUT_CALL_METHODS_FROM_OBJECT_CLASS);
		languageCzProperties.setProperty("Mo_Wc_Txt_CallMethodFromInheritedClassInfo", Constants.MO_WC_TXT_CALL_METHOD_FROM_INHERITED_CLASS_INFO);
		languageCzProperties.setProperty("Mo_Wc_Txt_CallStaticMethodFromInheritedClassesInfo", Constants.MO_WC_TXT_CALL_STATIC_METHOD_FROM_INHERITED_CLASSES_INFO);
		languageCzProperties.setProperty("Mo_Wc_Txt_IncrementationDecrementation", Constants.MO_WC_TXT_INCREMENTATION_DCREMENTATION);
		languageCzProperties.setProperty("Mo_Wc_Txt_IncrementationDecrementation_1", Constants.MO_WC_TXT_INCREMENTATION_DECREMENTATION_1);
		languageCzProperties.setProperty("Mo_Wc_Txt_VariableIncrementation", Constants.MO_WC_TXT_VARIABLE_INCREMENTATION);
		languageCzProperties.setProperty("Mo_Wc_Txt_VariableDecrementation", Constants.MO_WC_TXT_VARIABLE_DECREMENTATION);
		languageCzProperties.setProperty("Mo_Wc_Txt_IncrementationMyVariable", Constants.MO_WC_TXT_INCREMENTATION_MY_VARIABLE);
		languageCzProperties.setProperty("Mo_Wc_Txt_DecrementationMyVariable", Constants.MO_WC_TXT_DECREMENTATION_MY_VARIABLE);
		languageCzProperties.setProperty("Mo_Wc_Txt_IncrementationDecrementationConstants", Constants.MO_WC_TXT_INCREMENTATION_DECREMENTATION_CONSTANTS);
		languageCzProperties.setProperty("Mo_Wc_Txt_IncrementationConstants", Constants.MO_WC_TXT_INCREMENTATION_CONSTANTS);
		languageCzProperties.setProperty("Mo_Wc_Txt_DecrementationConstants", Constants.MO_WC_TXT_DECREMENTATION_CONSTANTS);
		languageCzProperties.setProperty("Mo_Wc_Txt_DeclarationOfVariable", Constants.MO_WC_TXT_DECLARATION_OF_VARIABLE);
		languageCzProperties.setProperty("Mo_Wc_Txt_DeclarationVariableInfo", Constants.MO_WC_TXT_DECLARATION_VARIABLE_INFO);
		languageCzProperties.setProperty("Mo_Wc_Txt_LikeThis", Constants.MO_WC_TXT_LIKE_THIS);
		languageCzProperties.setProperty("Mo_Wc_Txt_DeclarationVariableInfo_2", Constants.MO_WC_TXT_DECLARATION_VARIABLE_INFO_2);
		languageCzProperties.setProperty("Mo_Wc_Txt_Syntaxe", Constants.MO_WC_TXT_SYNTAXE);
		languageCzProperties.setProperty("Mo_Wc_Txt_SyntaxeForString", Constants.MO_WC_TXT_SYNTAXE_FOR_STRING);
		languageCzProperties.setProperty("Mo_Wc_Txt_SyntaxeForChar", Constants.MO_WC_TXT_SYNTAXE_FOR_CHAR);
		languageCzProperties.setProperty("Mo_Wc_Txt_SyntaxeForDouble", Constants.MO_WC_TXT_SYNTAXE_FOR_DOUBLE);
		languageCzProperties.setProperty("Mo_Wc_Txt_SimilaryDataTypes", Constants.MO_WC_TXT_SIMILARY_DATA_TYPES);
		languageCzProperties.setProperty("Mo_Wc_Txt_FullfilmentVariable", Constants.MO_WC_TXT_FULLFILMENT_VARIABLE);
		languageCzProperties.setProperty("Mo_Wc_Txt_FullfilmentVariableInfo", Constants.MO_WC_TXT_FULLFILMENT_VARIABLE_INFO);
		languageCzProperties.setProperty("Mo_Wc_Txt_FullfilmentVariableSyntaxeInfo", Constants.MO_WC_TXT_FULLFILMENT_VARIABLE_SYNTAXE_INFO);
		languageCzProperties.setProperty("Mo_Wc_Txt_FullfilmentVariableText", Constants.MO_WC_TXT_FULLFILMENT_VARIABLE_TEXT);
		languageCzProperties.setProperty("Mo_Wc_Txt_FullfilmentMyVariable", Constants.MO_WC_TXT_FULLFILMENT_MY_VARIABLE);
		languageCzProperties.setProperty("Mo_Wc_Txt_FullfilmentReplaceInfo", Constants.MO_WC_TXT_FULFILMENT_REPLACE_INFO);
		languageCzProperties.setProperty("Mo_Wc_Txt_FullfilmentReturnValue", Constants.MO_WC_TXT_FULLFILMENT_RETURN_VALUE);
		languageCzProperties.setProperty("Mo_Wc_Txt_Fullfilment_By_Static_Method", Constants.MO_WC_TXT_FULLFILMENT_BY_STATIC_METHOD);
		languageCzProperties.setProperty("Mo_Wc_Txt_Fullfilment_By_Static_Method_In_Inner_Class", Constants.MO_WC_TXT_FULLFILMENT_BY_STATIC_METHOD_IN_INNER_CLASS);
		languageCzProperties.setProperty("Mo_Wc_Txt_FullfilmentVariableSameRules", Constants.MO_WC_TXT_FULLFILMENT_VARIABLE_SAME_RULES);
		languageCzProperties.setProperty("Mo_Wc_Txt_FullfilmentVariableInfo_2", Constants.MO_WC_TXT_FULLFILMENT_VARIABLE_INFO_2);
		languageCzProperties.setProperty("Mo_Wc_Txt_FullfilmentEtc", Constants.MO_WC_TXT_FULLFILMENT_ETC);
		languageCzProperties.setProperty("Mo_Wc_Txt_PrintMethodInfo", Constants.MO_WC_TXT_PRINT_METHOD_INFO);
		languageCzProperties.setProperty("Mo_Wc_Txt_PrintMethodInfo_2", Constants.MO_WC_TXT_PRINT_METHOD_INFO_2);
		languageCzProperties.setProperty("Mo_Wc_Txt_PrintMethodInfo_3", Constants.MO_WC_TXT_PRINT_METHOD_INFO_3);
		languageCzProperties.setProperty("Mo_Wc_Txt_PrintMethodInfo_4", Constants.MO_WC_TXT_PRINT_METHOD_INFO_4);
		languageCzProperties.setProperty("Mo_Wc_Txt_PrintMethodSyntaxeExample", Constants.MO_WC_TXT_PRINT_METHOD_SYNTAXE_EXAMPLE);
		languageCzProperties.setProperty("Mo_Wc_Txt_PrintMethodWriteResult", Constants.MO_WC_TXT_PRINT_METHOD_WRITE_RESULT);
		languageCzProperties.setProperty("Mo_Wc_Txt_PrintMethodReturnValue", Constants.MO_WC_TXT_PRINT_METHOD_RETURN_VALUE);
		languageCzProperties.setProperty("Mo_Wc_Txt_DeclarationListInfo", Constants.MO_WC_TXT_DECLARATION_LIST_INFO);
		languageCzProperties.setProperty("Mo_Wc_Txt_DeclarationListInfo_2", Constants.MO_WC_TXT_DECLARATION_LIST_INFO_2);
		languageCzProperties.setProperty("Mo_Wc_Txt_DeclarationListInfo_3", Constants.MO_WC_TXT_DECLARATION_LIST_INFO_3);
		languageCzProperties.setProperty("Mo_Wc_Txt_Note", Constants.MO_WC_TXT_NOTE);
		languageCzProperties.setProperty("Mo_Wc_Txt_DeclarationListInfo_4", Constants.MO_WC_TXT_DECLARATION_LIST_INFO_4);
		languageCzProperties.setProperty("Mo_Wc_Txt_DeclarationListInfo_5", Constants.MO_WC_TXT_DECLARATION_LIST_INFO_5);
		languageCzProperties.setProperty("Mo_Wc_Txt_ListMethodInfo_1", Constants.MO_WC_TXT_LIST_METHOD_INFO_1);
		languageCzProperties.setProperty("Mo_Wc_Txt_ListMethodInfo_2", Constants.MO_WC_TXT_LIST_METHOD_INFO_2);
		languageCzProperties.setProperty("Mo_Wc_Txt_ListMethodInfo_3", Constants.MO_WC_TXT_LIST_METHOD_INFO_3);
		languageCzProperties.setProperty("Mo_Wc_Txt_ListMethodInfo_4", Constants.MO_WC_TXT_LIST_METHOD_INFO_4);
		languageCzProperties.setProperty("Mo_Wc_Txt_ListMethodInfo_5", Constants.MO_WC_TXT_LIST_METHOD_INFO_5);
		languageCzProperties.setProperty("Mo_Wc_Txt_ListMethodInfo_6", Constants.MO_WC_TXT_LIST_METHOD_INFO_6);
		languageCzProperties.setProperty("Mo_Wc_Txt_ListMethodInfo_7", Constants.MO_WC_TXT_LIST_METHOD_INFO_7);
		languageCzProperties.setProperty("Mo_Wc_Txt_ListMethodInfo_8", Constants.MO_WC_TXT_LIST_METHOD_INFO_8);
		languageCzProperties.setProperty("Mo_Wc_Txt_ListMethodInfo_9", Constants.MO_WC_TXT_LIST_METHOD_INFO_9);
		languageCzProperties.setProperty("Mo_Wc_Txt_ListMethodInfo_10", Constants.MO_WC_TXT_LIST_METHOD_INFO_10);
		languageCzProperties.setProperty("Mo_Wc_Txt_DeclarationOneDimensionalArray", Constants.MO_WC_TXT_DECLARATION_ONE_DIMENSIONAL_ARRAY);
		languageCzProperties.setProperty("Mo_Wc_Txt_OneDimArrayInfo_1", Constants.MO_WC_TXT_ONE_DIM_ARRAY_INFO_1);
		languageCzProperties.setProperty("Mo_Wc_Txt_OneDimArrayInfo_2", Constants.MO_WC_TXT_ONE_DIM_ARRAY_INFO_2);		
		languageCzProperties.setProperty("Mo_Wc_Txt_OneDimArrayInfo_3", Constants.MO_WC_TXT_ONE_DIM_ARRAY_INFO_3);		
		languageCzProperties.setProperty("Mo_Wc_Txt_DataTypeArrayCanBe", Constants.MO_WC_TXT_DATA_TYPE_ARRAY_CAN_BE);
		languageCzProperties.setProperty("Mo_Wc_Txt_OperationsWithArray", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY);
		languageCzProperties.setProperty("Mo_Wc_Txt_OperationsWithArray_1", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_1);
		languageCzProperties.setProperty("Mo_Wc_Txt_OperationsWithArray_2", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_2);
		languageCzProperties.setProperty("Mo_Wc_Txt_OperationsWithArray_3", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_3);
		languageCzProperties.setProperty("Mo_Wc_Txt_OperationsWithArray_4", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_4);
		languageCzProperties.setProperty("Mo_Wc_Txt_OperationsWithArray_5", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_5);
		languageCzProperties.setProperty("Mo_Wc_Txt_OperationsWithArray_6", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_6);
		languageCzProperties.setProperty("Mo_Wc_Txt_OperationsWithArray_7", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_7);
		languageCzProperties.setProperty("Mo_Wc_Txt_TwoDimArrayTitle", Constants.MO_WC_TXT_TWO_DIM_ARRAY_TITLE);
		languageCzProperties.setProperty("Mo_Wc_Txt_WorkWithTwoDimArrayInfo", Constants.MO_WC_TXT_WORK_WITH_TWO_DIM_ARRAY_INFO);
		languageCzProperties.setProperty("Mo_Wc_Txt_CannotCreateTwoDimarray", Constants.MO_WC_TXT_CANNOT_CREATE_TWO_DIM_ARRAY);
		languageCzProperties.setProperty("Mo_Wc_Txt_OperationsWithTwoDimArray", Constants.MO_WC_TXT_OPERATIONS_WITH_TWO_DIM_ARRAY);
		languageCzProperties.setProperty("Mo_Wc_Txt_FullfilmentArrayInfo_1", Constants.MO_WC_TXT_FULLFILMENT_ARRAY_INFO_1);
		languageCzProperties.setProperty("Mo_Wc_Txt_FullfilmentArrayInfo_2", Constants.MO_WC_TXT_FULLFILMENT_ARRAY_INFO_2);
		languageCzProperties.setProperty("Mo_Wc_Txt_FullfilmentArrayInfo_3", Constants.MO_WC_TXT_FULLFILMENT_ARRAY_INFO_3);		
		languageCzProperties.setProperty("Mo_Wc_Txt_OperationsWithArrayInInstanceTitle", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_IN_INSTANCE_TITLE);
		languageCzProperties.setProperty("Mo_Wc_Txt_OperationsWithArrayInInstanceInfo", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_IN_INSTANCE_INFO);
		languageCzProperties.setProperty("Mo_Wc_Txt_OperationsWithArrayInInstanceDifference", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_IN_INSTANCE_DIFFERENCE);		
		languageCzProperties.setProperty("Mo_Wc_Txt_InfoAboutNullValue", Constants.MO_WC_TXT_INFO_ABOUT_NULL_VALUE);
		
		// Info ohledně metod pro restartování a ukončení apliakce pomocí editoru příkazů:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_InfoForMethodsOverApp", "Texty ohledně metod, které lze zavolat v editoru příkazů, ale slouží pro 'ovládání' aplikace.");
		languageCzProperties.setProperty("Mo_Wc_Txt_InfoForMethodsOverApp", Constants.MO_WC_TXT_INFO_FOR_METHODS_OVER_APP);
		
		// Restartování aplikace:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_RestartAppTitle", "Texty pro hlášení do editoru výstupů ohledně metody pro restartování aplikace pomocí příslušných metod, které je možné zadat do editoru příkazů.");
		languageCzProperties.setProperty("Mo_Wc_Txt_RestartAppTitle", Constants.MO_WC_TXT_RESTART_APP_TITLE);
		// Info k restartu - že se uložídiagram tříd apod.
		languageCzProperties.setProperty("Mo_Wc_Txt_InfoAboutCloseOpenFilesBeforeRestartApp", Constants.MO_WC_TXT_INFO_ABOUT_CLOSE_OPEN_FILES_BEFORE_RESTART_APP);
		languageCzProperties.setProperty("Mo_Wc_Txt_InfoAboutLaunchCountdownWithCommandIsEntered", Constants.MO_WC_TXT_INFO_ABOUT_LAUNCH_COUNTDOWN_WHEN_COMMAND_IS_ENTERED);
		languageCzProperties.setProperty("Mo_Wc_Txt_InfoAboutSaveClassDiagramBeforeRestartApp", Constants.MO_WC_TXT_INFO_ABOUT_SAVE_CLASS_DIAGRAM_BEFORE_RESTART_APP);
		languageCzProperties.setProperty("Mo_Wc_Txt_TwoWaysToRestartApp", Constants.MO_WC_TXT_TWO_WAYS_TO_RESTART_APP);
		languageCzProperties.setProperty("Mo_Wc_Txt_FirstWayToRestartApp_1", Constants.MO_WC_TXT_FIRST_WAY_TO_RESTART_APP_1);
		languageCzProperties.setProperty("Mo_Wc_Txt_FirstWayToRestartApp_2", Constants.MO_WC_TXT_FIRST_WAY_TO_RESTART_APP_2);
		languageCzProperties.setProperty("Mo_Wc_Txt_SecondWayToRestartApp", Constants.MO_WC_TXT_SECOND_WAY_TO_RESTART_APP);
		// Ukončení aplikace:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_CloseAppTitle", "Texty pro hlášení do editoru výstupů ohledně metody pro ukončení aplikace pomocí příslušných metod, které je možné zadat do editoru příkazů.");
		languageCzProperties.setProperty("Mo_Wc_Txt_CloseAppTitle", Constants.MO_WC_TXT_CLOSE_APP_TITLE);
		languageCzProperties.setProperty("Mo_Wc_Txt_InfoAboutCloseOpenFilesBeforeCloseApp", Constants.MO_WC_TXT_INFO_ABOUT_CLOSE_OPEN_FILES_BEFORE_CLOSE_APP);
		languageCzProperties.setProperty("Mo_Wc_Txt_InfoAboutLaunchCountdownAfterCommandIsEntered", Constants.MO_WC_TXT_INFO_ABOUT_LAUNCH_COUNTDOWN_AFTER_COMMAND_IS_ENTERED);
		languageCzProperties.setProperty("Mo_Wc_Txt_InfoAboutSaveClassDiagramBeforeCloseApp", Constants.MO_WC_TXT_INFO_ABOUT_SAVE_CLASS_DIAGRAM_BEFORE_CLOSE_APP);
		languageCzProperties.setProperty("Mo_Wc_Txt_ThreeWaysToCloseApp", Constants.MO_WC_TXT_THREE_WAYS_TO_CLOSE_APP);
		languageCzProperties.setProperty("Mo_Wc_Txt_FirstWayToCloseApp_1", Constants.MO_WC_TXT_FIRST_WAY_TO_CLOSE_APP_1);
		languageCzProperties.setProperty("Mo_Wc_Txt_FirstWayToCloseApp_2", Constants.MO_WC_TXT_FIRST_WAY_TO_CLOSE_APP_2);
		languageCzProperties.setProperty("Mo_Wc_Txt_SecondWayToCloseApp", Constants.MO_WC_TXT_SECOND_WAY_TO_CLOSE_APP);
		languageCzProperties.setProperty("Mo_Wc_Txt_ThirdayToCloseApp_1", Constants.MO_WC_TXT_THIRD_WAY_TO_CLOSE_APP_1);
		languageCzProperties.setProperty("Mo_Wc_Txt_ThirdayToCloseApp_2", Constants.MO_WC_TXT_THIRD_WAY_TO_CLOSE_APP_2);
		languageCzProperties.setProperty("Mo_Wc_Txt_InfoAboutThirdWayToCloseApp_1", Constants.MO_WC_TXT_INFO_ABOUT_THIRD_WAY_TO_CLOSE_APP_1);
		languageCzProperties.setProperty("Mo_Wc_Txt_InfoAboutThirdWayToCloseApp_2", Constants.MO_WC_TXT_INFO_ABOUT_THIRD_WAY_TO_CLOSE_APP_2);
		// Zrušení odpočtu pro restartování nebo ukončení aplikace:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_CancelCountdownTitle", "Texty pro hlášení do editoru výstupů ohledně metody pro ukončení odpočítávání pro času pro restart nebo ukončení aplikace.");
		languageCzProperties.setProperty("Mo_Wc_Txt_CancelCountdownTitle", Constants.MO_WC_TXT_CANCEL_COUNTDOWN_TITLE);
		languageCzProperties.setProperty("Mo_Wc_Txt_CancelCountdownInfo", Constants.MO_WC_TXT_CANCEL_COUNTDOWN_INFO);
		
		
		// Texty, že nebyl nalezen soubor .class pro zavolaní staticke metody a nebo konstruktoru:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_File_Dot_Class", "Texty pro chybová oznámení ohledně toho, že se nepodařilo najít zkompilovanou / přeloženou třídu (soubor .class) při pokusu o zavolání statické metody nebo konstruktoru.");
		languageCzProperties.setProperty("Mo_Wc_Txt_File_Dot_Class", Constants.MO_WC_TXT_FILE_DOT_CLASS);
		languageCzProperties.setProperty("Mo_Wc_Txt_File_Dot_Class_Was_Not_Found", Constants.MO_WC_TXT_FILE_DOT_CLASS_WAS_NOT_FOUND);
		languageCzProperties.setProperty("Mo_Wc_Txt_Failed_To_Find_The_Class", Constants.MO_WC_TXT_FAILED_TO_FIND_THE_CLASS);
		languageCzProperties.setProperty("Mo_Wc_Txt_Failed_To_Find_Bin_Dir", Constants.MO_WC_TXT_FAILED_TO_FIND_BIN_DIR);
		languageCzProperties.setProperty("Mo_Wc_Txt_Failed_To_Find_Inner_Class", Constants.MO_WC_TXT_FAILED_TO_FIND_INNER_CLASS);
		languageCzProperties.setProperty("Mo_Wc_Txt_Inner_Class_Is_Not_Static", Constants.MO_WC_TXT_INNER_CLASS_IS_NOT_STATIC);
		languageCzProperties.setProperty("Mo_Wc_Txt_Inner_Class_Is_Not_Public_Either_Protected", Constants.MO_WC_TXT_INNER_CLASS_IS_NOT_PUBLIC_EITHER_PROTECTED);
		languageCzProperties.setProperty("Mo_Wc_Txt_Specific_Method_Was_Not_Found", Constants.MO_WC_TXT_SPECIFIC_METHOD_WAS_NOT_FOUND);
		languageCzProperties.setProperty("Mo_Wc_Txt_Public_Constructor_Was_Not_Found", Constants.MO_WC_TXT_PUBLIC_CONSTRUCTOR_WAS_NOT_FOUND);		
		// Text pro zavolání konstruktoru:
		languageCzProperties.setProperty("Mo_Wc_Txt_Constructor_Was_Not_Found", Constants.MO_WC_TXT_CONSTRUCTOR_WAS_NOT_FOUND);
		// Text že nebyla vytvořena žádná proměnná v editoru příkazů:
		languageCzProperties.setProperty("Mo_Wc_Txt_No_Variable_Was_Created", Constants.MO_WC_TXT_NO_VARIABLE_WAS_CREATED);
		
		// Texty pro operace s jednorozměrným polem v instanci třídy:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_CastingToIntegerFailure_1", "Texty pro chybová oznámení ohledně toho, že se nepodařilo provést nějakou operaci nad jednorozměrným polem v instanci nějaké třídy.");
		languageCzProperties.setProperty("Mo_Wc_Txt_CastingToIntegerFailure_1", Constants.MO_WC_TXT_CASTING_TO_INTEGER_FAILURE_1);
		languageCzProperties.setProperty("Mo_Wc_Txt_CastingToIntegerFailure_2", Constants.MO_WC_TXT_CASTING_TO_INTEGER_FAILURE_2);
		languageCzProperties.setProperty("Mo_Wc_Txt_InstanceOfClassInIdNotFound", Constants.MO_WC_TXT_INSTANCE_OF_CLASS_IN_ID_NOT_FOUND);
		languageCzProperties.setProperty("Mo_Wc_Txt_EnteredVariable_1", Constants.MO_WC_TXT_ENTERED_VARIABLE_1);
		languageCzProperties.setProperty("Mo_Wc_Txt_EnteredVariable_2", Constants.MO_WC_TXT_ENTERED_VARIABLE_2);
		languageCzProperties.setProperty("Mo_Wc_Txt_IsNotOneDimArray", Constants.MO_WC_TXT_IS_NOT_ONE_DIM_ARRAY);
		languageCzProperties.setProperty("Mo_Wc_Txt_IndexValueAtArrayIsNotInInterval", Constants.MO_WC_TXT_INDEX_VALUE_AT_ARRAY_IS_NOT_IN_INTERVAL);
		languageCzProperties.setProperty("Mo_Wc_Txt_ValueInArrayIsNull", Constants.MO_WC_TXT_VALUE_IN_ARRAY_IS_NULL);
		
		// Texty pro operace s dvojrozměrným polem:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_IsNotTwoDimArray", "Texty pro chybová oznámení ohledně toho, že se nepodařilo provést nějakou operaci nad dvojrozměrným polem v instanci nějaké třídy.");
		languageCzProperties.setProperty("Mo_Wc_Txt_IsNotTwoDimArray", Constants.MO_WC_TXT_IS_NOT_TWO_DIM_ARRAY);
		languageCzProperties.setProperty("Mo_Wc_Txt_FailedToSetValueToTwoDimArray", Constants.MO_WC_TXT_FAILED_TO_SET_VALUE_TO_TWO_DIM_ARRAY);
		languageCzProperties.setProperty("Mo_Wc_Txt_Index_1_Text", Constants.MO_WC_TXT_INDEX_1_TEXT);
		languageCzProperties.setProperty("Mo_Wc_Txt_Index_2_Text", Constants.MO_WC_TXT_INDEX_2_TEXT);
		languageCzProperties.setProperty("Mo_Wc_Txt_FailedToGetValueFromTwoDimArray", Constants.MO_WC_TXT_FAILED_TO_GET_VALUE_FROM_TWO_DIM_ARRAY);
		languageCzProperties.setProperty("Mo_Wc_Txt_FailedToIncrementValueAtIndexInTwoDimArrayInIntance", Constants.MO_WC_TXT_FAILED_TO_INCREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE);
		languageCzProperties.setProperty("Mo_Wc_Txt_FailedToDecrementValueAtIndexInTwoDimArrayInIntance", Constants.MO_WC_TXT_FAILED_TO_DECREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE);
		languageCzProperties.setProperty("Mo_Wc_Txt_ValueWasnotFoundAtindexinTwoDimArrayInInstance", Constants.MO_WC_TXT_VALUE_WAS_NOT_FOUND_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE);
		
		
				
		
		// Texty pro ukončení a restart aplikace dle zadaného příkazu v editoru příkazů:
		
		// Texty pro ukončení odpočtu:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_NoCountdownIsRunning", "Texty pro oznámení ohledně ukončení odpočtu pro restartování nebo ukončení aplikace po zadání příslušného příkazu v editoru příkazů.");
		languageCzProperties.setProperty("Mo_Wc_Txt_NoCountdownIsRunning", Constants.MO_WC_TXT_NO_COUNTDOWN_IS_RUNNING);
		languageCzProperties.setProperty("Mo_Wc_Txt_CountdownIsStopped", Constants.MO_WC_TXT_COUNTDOWN_IS_STOPPED);
		
		// Texty pro spuštění timeru pro ukončení aplikace.
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_CountdownForClosAppIsRunning", "Texty pro oznámení ohledně spuštění odpočtu pro ukončení aplikace po zadání příslušného příkazu v editoru příkazů.");
		languageCzProperties.setProperty("Mo_Wc_Txt_CountdownForClosAppIsRunning", Constants.MO_WC_TXT_COUNTDOWN_FOR_CLOSE_APP_IS_RUNNING);
		languageCzProperties.setProperty("Mo_Wc_Txt_CountdownForCloseAppIsNotPossibleToStart", Constants.MO_WC_TXT_COUNTDOWN_FOR_CLOSE_APP_IS_NOT_POSSIBLE_TO_START);
		
		languageCzProperties.setProperty("Mo_Wc_Txt_ToEndOfAppRemains_1", Constants.MO_WC_TXT_TO_END_OF_APP_REMAINS_1);
		languageCzProperties.setProperty("Mo_Wc_Txt_ToEndOfAppRemains_2", Constants.MO_WC_TXT_TO_END_OF_APP_REMAINS_2);
		languageCzProperties.setProperty("Mo_Wc_Txt_ToEndOfAppRemains_3", Constants.MO_WC_TXT_TO_END_OF_APP_REMAINS_3);
		
		languageCzProperties.setProperty("Mo_Wc_Txt_SecondsForClose_1", Constants.MO_WC_TXT_SECONDS_FOR_CLOSE_1);
		languageCzProperties.setProperty("Mo_Wc_Txt_SecondsForClose_2", Constants.MO_WC_TXT_SECONDS_FOR_CLOSE_2);
		languageCzProperties.setProperty("Mo_Wc_Txt_SecondsForClose_3", Constants.MO_WC_TXT_SECONDS_FOR_CLOSE_3);
		
		
		// Texty pro spuštění timeru pro restartování aplikace:		
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_CountdownForRestartAppIsRunning", "Texty pro oznámení ohledně spuštění odpočtu pro restartování aplikace po zadání příslušného příkazu v editoru příkazů.");
		languageCzProperties.setProperty("Mo_Wc_Txt_CountdownForRestartAppIsRunning", Constants.MO_WC_TXT_COUNTDOWN_FOR_RESTART_APP_IS_RUNNING);
		languageCzProperties.setProperty("Mo_Wc_Txt_CountdownForRestartAppIsNotPossibleToStart", Constants.MO_WC_TXT_COUNTDOWN_FOR_RESTART_APP_IS_NOT_POSSIBLE_TO_START);
		

		languageCzProperties.setProperty("Mo_Wc_Txt_ToRestartAppRemains_1", Constants.MO_WC_TXT_TO_RESTART_APP_REMAINS_1);
		languageCzProperties.setProperty("Mo_Wc_Txt_ToRestartAppRemains_2", Constants.MO_WC_TXT_TO_RESTART_APP_REMAINS_2);
		languageCzProperties.setProperty("Mo_Wc_Txt_ToRestartAppRemains_3", Constants.MO_WC_TXT_TO_RESTART_APP_REMAINS_3);
		
		languageCzProperties.setProperty("Mo_Wc_Txt_SecondsForRestart_1", Constants.MO_WC_TXT_SECONDS_FOR_RESTART_1);
		languageCzProperties.setProperty("Mo_Wc_Txt_SecondsForRestart_2", Constants.MO_WC_TXT_SECONDS_FOR_RESTART_2);
		languageCzProperties.setProperty("Mo_Wc_Txt_SecondsForRestart_3", Constants.MO_WC_TXT_SECONDS_FOR_RESTART_3);
		
		
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
		// Texty do RefreshInstance - pouze do chybové hlášky s informací o tom, že došlo k chybě při vytváření nové instance:
		ConfigurationFiles.commentsMap.put("Mo_Txt_FailedToUpdateInstanceOfClass", "Texty do jedné chybové hlášky, která může nastat při vytvoření instance při aktualizaci jednotlivých instancí, které jsou zobrazeny v diagramu instancí. K této hlášce může dojít hlavně v případě, že třída, jejíž instance se má znovu vytvořit (aktualizovat) nebohsahuje výchozí 'nullary' konstruktor.");
		languageCzProperties.setProperty("Mo_Txt_FailedToUpdateInstanceOfClass", Constants.RI_TXT_FAILED_TO_UPDATE_INSTANCE_OF_CLASS);
		languageCzProperties.setProperty("Mo_Txt_FailedToUpdateInstanceOfClass_2", Constants.RI_TXT_FAILED_TO_UPDATE_INSTANCE_OF_CLASS_2);
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
		// Třída JavaHomeForm.java - všechny texty:
		// texty do JradioButtonu:
		ConfigurationFiles.commentsMap.put("Jhf_DialogTitle", "Texty do dialogu, který obsahuje komponenty pro nastavení domovského adresáře Javy pro aplikaci. V tomto dialogu lze nastavit například cestu k adresáři, kde je na příslušném zařízení nainstalována Java, nebo označit potřebné soubory, které se následně překopírují do adresáře označeného jako workspace a nastaví se k němu cesta. Jedná se o texty pro komponety typu JRadioButton v uvedeném dialogu.");
		languageCzProperties.setProperty("Jhf_DialogTitle", Constants.JHF_DIALOG_TITLE);
		languageCzProperties.setProperty("Jhf_RbSetJavaHomeDir", Constants.JHF_RB_SET_JAVA_HOME_DIR);
		languageCzProperties.setProperty("Jhf_RbCopySomeFiles", Constants.JHF_RB_COPY_SOME_FILES);
		languageCzProperties.setProperty("Jhf_RbSetDefault", Constants.JHF_RB_SET_DEFAULT);

		
		// Popisky do labeluů:
		ConfigurationFiles.commentsMap.put("Jhf_LblJavaHomeDirInfo", "Popisky do labelů v dialogu, který obsahuje komponenty pro nastavení domovského adresáře Javy pro aplikaci. V tomto dialogu lze nastavit například cestu k adresáři, kde je na příslušném zařízení nainstalována Java, nebo označit potřebné soubory, které se následně překopírují do adresáře označeného jako workspace a nastaví se k němu cesta. Jedná se o texty pro komponety typu JRadioButton v uvedeném dialogu.");
		languageCzProperties.setProperty("Jhf_LblJavaHomeDirInfo", Constants.JHF_LBL_JAVA_HOME_DIR_INFO);
		languageCzProperties.setProperty("Jhf_LblJavaHomeLibDirInfo", Constants.JHF_LBL_JAVA_HOME_LIB_DIR_INFO);
		languageCzProperties.setProperty("Jhf_LblCopySomeFilesInfo", Constants.JHF_LBL_COPY_SOME_FILES_INFO);

		
		// Tlačítka:
		ConfigurationFiles.commentsMap.put("Jhf_BtnSelectJavaHomeDir", "Texty do tlačítek v dialogu, který obsahuje komponenty pro nastavení domovského adresáře Javy pro aplikaci. V tomto dialogu lze nastavit například cestu k adresáři, kde je na příslušném zařízení nainstalována Java, nebo označit potřebné soubory, které se následně překopírují do adresáře označeného jako workspace a nastaví se k němu cesta. Jedná se o texty pro komponety typu JRadioButton v uvedeném dialogu.");
		languageCzProperties.setProperty("Jhf_BtnSelectJavaHomeDir", Constants.JHF_BTN_SELECT_JAVA_HOME_DIR);
		languageCzProperties.setProperty("Jhf_Btn_OK", Constants.JHF_BTN_OK);
		languageCzProperties.setProperty("Jhf_Btn_Cancel", Constants.JHF_BTN_CANCEL);

		
		// Text do výběru textu:
		ConfigurationFiles.commentsMap.put("Jhf_TextChooseText", "Texty do labelů, které informují uživatele ohledně výběru příslušného souboru nebo cesty k adresáři Javy v dialogu, který obsahuje komponenty pro nastavení domovského adresáře Javy pro aplikaci. V tomto dialogu lze nastavit například cestu k adresáři, kde je na příslušném zařízení nainstalována Java, nebo označit potřebné soubory, které se následně překopírují do adresáře označeného jako workspace a nastaví se k němu cesta. Jedná se o texty pro komponety typu JRadioButton v uvedeném dialogu.");
		languageCzProperties.setProperty("Jhf_TextChooseText", Constants.JHF_TEXT_CHOOSE_TEXT);
		languageCzProperties.setProperty("Jhf_TextChooseFile", Constants.JHF_TEXT_CHOOSE_FILE);

		// texty do hlasek:
		ConfigurationFiles.commentsMap.put("Jhf_TxtFilesNotFoundText", "Texty do chybových hlášek, které mohou nastat v některých případěch označení chybných souborů nebo nastavení chybných cest apod. Vše jsou to texty do chybových hlášek do dialogu, který obsahuje komponenty pro nastavení domovského adresáře Javy pro aplikaci. V tomto dialogu lze nastavit například cestu k adresáři, kde je na příslušném zařízení nainstalována Java, nebo označit potřebné soubory, které se následně překopírují do adresáře označeného jako workspace a nastaví se k němu cesta. Jedná se o texty pro komponety typu JRadioButton v uvedeném dialogu.");
		languageCzProperties.setProperty("Jhf_TxtFilesNotFoundText", Constants.JHF_TXT_FILES_NOT_FOUND_TEXT);
		languageCzProperties.setProperty("Jhf_TxtFilesNotFoundTitle", Constants.JHF_TXT_FILES_NOT_FOUND_TITLE);
		languageCzProperties.setProperty("Jhf_TxtBadFormatClassPathText", Constants.JHF_TXT_BAD_FORMAT_CLASS_PATH_TEXT);
		languageCzProperties.setProperty("Jhf_TxtBadFormatClassPathTitle", Constants.JHF_TXT_BAD_FORMAT_CLASS_PATH_TITLE);
		languageCzProperties.setProperty("Jhf_TxtIsNeededToFillPathText", Constants.JHF_TXT_IS_NEEDED_TO_FILL_PATH_TEXT);
		languageCzProperties.setProperty("Jhf_TxtIsNeededToFillPathTitle", Constants.JHF_TXT_IS_NEEDED_TO_FILL_PATH_TITLE);
		languageCzProperties.setProperty("Jhf_TxtMissingLibOrImportantFilesText", Constants.JHF_TXT_MISSING_LIB_OR_IMPORTANT_FILES_TEXT);
		languageCzProperties.setProperty("Jhf_TxtMissingLibOrImportantFilesTitle", Constants.JHF_TXT_MISSING_LIB_OR_IMPORTANT_FILES_TITLE);
		languageCzProperties.setProperty("Jhf_TxtChoosedDirDoesNotExistText", Constants.JHF_TXT_CHOOSED_DIR_DOES_NOT_EXIST_TEXT);
		languageCzProperties.setProperty("Jhf_TxtChoosedDirDoesNotExistTitle", Constants.JHF_TXT_CHOOSED_DIR_DOES_NOT_EXIST_TITLE);
		languageCzProperties.setProperty("Jhf_TxtPathIsNotInCorrectFormatText", Constants.JHF_TXT_PATH_IS_NOT_IN_CORRECT_FORMAT_TEXT);
		languageCzProperties.setProperty("Jhf_TxtPathIsNotInCorrectFormatTitle", Constants.JHF_TXT_PATH_IS_NOT_IN_CORRECT_FORMAT_TITLE);
		languageCzProperties.setProperty("Jhf_TxtPathToHomeDirIsEmptyText", Constants.JHF_TXT_PATH_TO_HOME_DIR_IS_EMPTY_TEXT);
		languageCzProperties.setProperty("Jhf_TxtPathToHomeDirIsEmptyTitle", Constants.JHF_TXT_PATH_TO_HOME_DIR_IS_EMPTY_TITLE);
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
		// Veškeré texty do třídy: OperationsWithInstances.java z balíčku: instances:
		ConfigurationFiles.commentsMap.put("Owi_Txt_MethodIsNotPublic", "Texty pro chybové výpisy do editoru výstupů, které mohou nastat při provádění operace nad instancemi, například volání metody, vytváření instance apod.");
		languageCzProperties.setProperty("Owi_Txt_MethodIsNotPublic", Constants.OWI_TXT_METHOD_IS_NOT_PUBLIC);
		languageCzProperties.setProperty("Owi_Txt_MethodIsNotInstanceOfCLass", Constants.OWI_TXT_METHOD_IS_NOT_INSTANCE_OF_CLASS);
		languageCzProperties.setProperty("Owi_Txt_ErrorInCallTheMethod", Constants.OWI_TXT_ERROR_IN_CALL_THE_METHOD);
		languageCzProperties.setProperty("Owi_Txt_ErrorDuringCreateInstance", Constants.OWI_TXT_ERROR_DURING_CREATE_INSTANCE);
		languageCzProperties.setProperty("Owi_Txt_PossibleCausesCreateInstanceError", Constants.OWI_TXT_POSSIBLE_CAUSES_CREATE_INSNTANCE_ERROR);
		languageCzProperties.setProperty("Owi_Txt_Class", Constants.OWI_TXT_CLASS);
		languageCzProperties.setProperty("Owi_Txt_CannotCreateInstanceFromAbstractClass", Constants.OWI_TXT_CANNOT_CREATE_INSTANCE_FROM_ABSTRACT_CLASS);
		languageCzProperties.setProperty("Owi_Txt_ConstructorIsNotPublic", Constants.OWI_TXT_CONSTRUCTOR_IS_NOT_PUBLIC);
		languageCzProperties.setProperty("Owi_Txt_ConstructorWantDifferentCountOrTypesOfParameters", Constants.OWI_TXT_CONSTRUCTOR_WANT_DIFFERENT_COUNT_OR_TYPES_OF_PARAMETERS);
		languageCzProperties.setProperty("Owi_Txt_ConstructorWant", Constants.OWI_TXT_CONSTRUCTOR_WANT);		
		languageCzProperties.setProperty("Owi_Txt_ErrorWhileCallingConstructorByThrowAnException", Constants.OWI_TXT_ERROR_WILE_CALLING_CONSTRUCTOR_BY_THROW_AN_EXCEPTION);		
		languageCzProperties.setProperty("Owi_Txt_ButGot", Constants.OWI_TXT_BUT_GOT);
		languageCzProperties.setProperty("Owi_Txt_ErrorDuringCallBaseConstructor", Constants.OWI_TXT_ERROR_DURING_CALL_BASE_CONSTRUCTOR);
		languageCzProperties.setProperty("Owi_Txt_ButDidGot", Constants.OWI_TXT_BUT_DID_GOT);
		languageCzProperties.setProperty("Owi_Txt_ReferenceNameExistMustChange", Constants.OWI_TXT_REFERENCE_NAME_EXIST_MUST_CHANGE);
		languageCzProperties.setProperty("Owi_Txt_Reference", Constants.OWI_TXT_REFERENCE);
		
		// Texty do metody callConstructor:
		ConfigurationFiles.commentsMap.put("Owi_Txt_Failed_To_Call_Constructor_In_Abstract_Class", "Texty pro chybové výpisy do editoru výstupů, které mohou nastat při volání konstruktoru pomocí editoru příkazů. Jedná se o zavolání konstruktoru bez účelu vytvoření reference na příslušnou instanci.");
		languageCzProperties.setProperty("Owi_Txt_Failed_To_Call_Constructor_In_Abstract_Class", Constants.OWI_TXT_FAILED_TO_CALL_CONSTRUCTOR_IN_ABSTRACT_CLASS);
		languageCzProperties.setProperty("Owi_Txt_Inserted_Parameters", Constants.OWI_TXT_INSERTED_PARAMETERS);
		languageCzProperties.setProperty("Owi_Txt_Required_Parameters", Constants.OWI_TXT_REQUIRED_PARAMETERS);
		languageCzProperties.setProperty("Owi_Txt_Failed_To_Call_Constructor", Constants.OWI_TXT_FAILED_TO_CALL_CONSTRUCTOR);
		languageCzProperties.setProperty("Owi_Txt_Constructor_Is_Not_Accessible", Constants.OWI_TXT_CONSTRUCTOR_IS_NOT_ACCESSIBLE);
		languageCzProperties.setProperty("Owi_Txt_Argument_Error", Constants.OWI_TXT_ARGUMENT_ERROR);
		languageCzProperties.setProperty("Owi_Txt_Constructor_Throw_An_Exception", Constants.OWI_TXT_CONSTRUCTOR_THROW_AN_EXCEPTION);
		languageCzProperties.setProperty("Owi_Txt_ReferenceVariable", Constants.OWI_TXT_REFERENCE_VARIABLE);
				
				
				
				
				
				
				
				
				
				
				
				
				
				
		// Texty do tříd v balíčku: projectExplorer:
		// Třída GetIndexOfExcelSheetForm.java:
		ConfigurationFiles.commentsMap.put("Pe_Gioesf_DialogTitle", "Texty do dialogového okna, které slouží pro výběr listu v excelovksém souboru, který se má otevřít (v dialogu průzkumník projektů).");
		languageCzProperties.setProperty("Pe_Gioesf_DialogTitle", Constants.PE_GIESF_DIALOG_TITLE);
		languageCzProperties.setProperty("Pe_Gioesf_LblInfo", Constants.PE_GIESF_LBL_INFO);
		languageCzProperties.setProperty("Pe_Gioesf_BtnOK", Constants.PE_GIESF_BTN_OK);
		languageCzProperties.setProperty("Pe_Gioesf_BtnCancel", Constants.PE_GIESF_BTN_CANCEL);

		// Třída: GetNewFileNameForm.java:
		ConfigurationFiles.commentsMap.put("Pe_Gnfnf_DialogTitle", "Texty do dialogového okna pro zadání nového názvu souboru / adresáře. Toto dialogové okno se otevře po kliknutí na položku přejmenovat nad nějakou položkou ve stromové struktuře v průzkumníku projektů. Jedná se o texty do samotného dialogu pro popisky i chybové hlášky.");
		languageCzProperties.setProperty("Pe_Gnfnf_DialogTitle", Constants.PE_GNFNF_DIALOG_TITLE);
		languageCzProperties.setProperty("Pe_Gnfnf_LblInfo", Constants.PE_GNFNF_LBL_INFO);
		languageCzProperties.setProperty("Pe_Gnfnf_LblErrorInfo", Constants.PE_GNFNF_LBL_ERROR_INFO);
		languageCzProperties.setProperty("Pe_Gnfnf_LblNote", Constants.PE_GNFNF_LBL_NOTE);
		languageCzProperties.setProperty("Pe_Gnfnf_BtnOk", Constants.PE_GNFNF_BTN_OK);
		languageCzProperties.setProperty("Pe_Gnfnf_BtnCalcel", Constants.PE_GNFNF_BTN_CANCEL);
		languageCzProperties.setProperty("Pe_Gnfnf_TxtFileNameAlreadyExistText", Constants.PE_GNFNF_TXT_FILE_NAME_ALREADY_EXIST_TEXT);
		languageCzProperties.setProperty("Pe_Gnfnf_TxtFileNameTextName", Constants.PE_GNFNF_TXT_FILE_NAME_TEXT_NAME);
		languageCzProperties.setProperty("Pe_Gnfnf_TxtFileNameAlreadyExistTitle", Constants.PE_GNFNF_TXT_FILE_NAME_ALREADY_EXIST_TITLE);
		languageCzProperties.setProperty("Pe_Gnfnf_WrongFileNameText", Constants.PE_GNFNF_WRONG_FILE_NAME_TEXT);
		languageCzProperties.setProperty("Pe_Gnfnf_TxtWrongFileNameTitle", Constants.PE_GNFNF_TXT_WRONG_FILE_NAME_TITLE);
		languageCzProperties.setProperty("Pe_Gnfnf_TxtTextFieldIsEmptyText", Constants.PE_GNFNF_TXT_TEXT_FIELD_IS_EMPTY_TEXT);
		languageCzProperties.setProperty("Pe_Gnfnf_TxtTextFieldIsEmptyTitle", Constants.PE_GNFNF_TXT_TEXT_FIELD_IS_EMPTY_TITLE);
		
		// Třída TreeStructure v project Explorer - pouze texty pro položky pro rozšíření a chování větve.
		ConfigurationFiles.commentsMap.put("Pe_Mt_Txt_Expand", "Texty pro kontextové menu pro položku, která slouží pro rozšíření nebo kolaps označené větve, která značí adresář.");
		languageCzProperties.setProperty("Pe_Mt_Txt_Expand", Constants.PE_MT_TXT_EXPAND);
		languageCzProperties.setProperty("Pe_Mt_Txt_Collapse", Constants.PE_MT_TXT_COLLAPSE);
		languageCzProperties.setProperty("Pe_Mt_Txt_ExpandLeaf", Constants.PE_MT_TXT_EXPAND_LEAF);
		languageCzProperties.setProperty("Pe_Mt_Txt_CollapseLeaf", Constants.PE_MT_TXT_COLLAPSE_LEAF);
				
		// Třída: JtreePopupMenu.java:
		// Texty do tlačítek v menu:
		ConfigurationFiles.commentsMap.put("Pe_Jpm_MenuNew", "Texty do kontextového menu nad položkami ve stromové struktuře v průzkumníku projektů. Toto kontextové menu se otevře po kliknutí pravým tlačítkem myši na libovolnou položku ve stromové struktuře v dialogu průzkumník projektů. Jedná se pouze o texty, které slouží jako položky v uvedeném kontextovém menu.");
		languageCzProperties.setProperty("Pe_Jpm_MenuNew", Constants.PE_JPM_MENU_NEW);
		languageCzProperties.setProperty("Pe_Jpm_MenuOpen", Constants.PE_JPM_MENU_OPEN);
		languageCzProperties.setProperty("Pe_Jpm_ItemOpenFileInInternalFrame", Constants.PE_JPM_ITEM_OPEN_FILE_IN_INTERNAL_FRAME);
		languageCzProperties.setProperty("Pe_Jpm_ItemOpenInFileExplorerInOs", Constants.PE_JPM_ITEM_OPEN_IN_FILE_EXPLORER_IN_OS);
		languageCzProperties.setProperty("Pe_Jpm_ItemOpenInDefaultAppInOs", Constants.PE_JPM_ITEM_OPEN_IN_DEFAULT_APP_IN_OS);
		languageCzProperties.setProperty("Pe_Jpm_ItemRefresh", Constants.PE_JPM_ITEM_REFRESH);
		languageCzProperties.setProperty("Pe_Jpm_ItemNewFile", Constants.PE_JPM_ITEM_NEW_FILE);
		languageCzProperties.setProperty("Pe_Jpm_ItemNewFolder", Constants.PE_JPM_ITEM_NEW_FOLDER);
		languageCzProperties.setProperty("Pe_Jpm_ItemNewProject", Constants.PE_JPM_ITEM_NEW_PROJECT);
		languageCzProperties.setProperty("Pe_Jpm_ItemNewExcelList", Constants.PE_JPM_ITEM_NEW_EXCEL_LIST);
		languageCzProperties.setProperty("Pe_Jpm_ItemDelete", Constants.PE_JPM_ITEM_DELETE);
		languageCzProperties.setProperty("Pe_Jpm_ItemRename", Constants.PE_JPM_ITEM_RENAME);
		languageCzProperties.setProperty("Pe_Jpm_ItemPaste", Constants.PE_JPM_ITEM_PASTE);
		languageCzProperties.setProperty("Pe_Jpm_ItemCopy", Constants.PE_JPM_ITEM_COPY);
		languageCzProperties.setProperty("Pe_Jpm_ItemCut", Constants.PE_JPM_ITEM_CUT);
				
		// Texty do hlášek:
		ConfigurationFiles.commentsMap.put("Pe_Jpm_Txt_FileAlreadyExistInDirText", "Texty pro chybové hlášky, které mohou nastat při provádění nějaké operace, které jsou k dispozici v kontextovém menu nad libovolnou položkou ve stromové struktuře v dialogu průzkumník projektů.");
		languageCzProperties.setProperty("Pe_Jpm_Txt_FileAlreadyExistInDirText", Constants.PE_JPM_TXT_FILE_ALREADY_EXIST_IN_DIR_TEXT);
		languageCzProperties.setProperty("Pe_Jpm_Txt_FileAlreadyExistInDirTitle", Constants.PE_JPM_TXT_FILE_ALREADY_EXIST_IN_DIR_TITLE);
		languageCzProperties.setProperty("Pe_Jpm_Txt_CannotCreateFileText", Constants.PE_JPM_TXT_CANT_CREATE_FILE_TEXT);
		languageCzProperties.setProperty("Pe_Jpm_Txt_Location", Constants.PE_JPM_TXT_LOCATION);
		languageCzProperties.setProperty("Pe_Jpm_Txt_CantCreateFileTitle", Constants.PE_JPM_TXT_CANT_CREATE_FILE_TITLE);
		languageCzProperties.setProperty("Pe_Jpm_Txt_DirAlreadyExistText", Constants.PE_JPM_TXT_DIR_ALREADY_EXIST_TEXT);
		languageCzProperties.setProperty("Pe_Jpm_Txt_DirAlreadyExistTitle", Constants.PE_JPM_TXT_DIR_ALREADY_EXIST_TITLE);
		languageCzProperties.setProperty("Pe_Jpm_Txt_ErrorWhileCreatingDirText", Constants.PE_JPM_TXT_ERROR_WHILE_CREATING_DIR_TEXT);
		languageCzProperties.setProperty("Pe_Jpm_Txt_ErrorWhileCreatingDirTitle", Constants.PE_JPM_TXT_EROR_WHILE_CREATING_DIR_TITLE);
		languageCzProperties.setProperty("Pe_Jpm_Txt_CannotDeleteFilesFromOpenProjectText", Constants.PE_JPM_TXT_CANT_DELETE_FILES_FROM_OPEN_PROJECT_TEXT);
		languageCzProperties.setProperty("Pe_Jpm_Txt_CannotDeleteFilesFromOpenProjectTitle", Constants.PE_JPM_TXT_CANT_DELETE_FILES_FROM_OPEN_PROJECT_TITLE);
		languageCzProperties.setProperty("Pe_Jpm_Txt_FileDontExistAlreadyText", Constants.PE_JPM_TXT_FILE_DONT_EXIST_ALREADY_TEXT);
		languageCzProperties.setProperty("Pe_Jpm_Txt_FileDontExistAlreadyTitle", Constants.PE_JPM_TXT_FILE_DONT_EXIST_ALREADY_TITLE);
		languageCzProperties.setProperty("Pe_Jpm_Txt_FailedToRenameFileText", Constants.PE_JPM_TXT_FAILED_TO_RENAME_FILE_TEXT);
		languageCzProperties.setProperty("Pe_Jpm_Txt_OriginalName", Constants.PE_JPM_TXT_ORIGINAL_NAME);
		languageCzProperties.setProperty("Pe_Jpm_Txt_NewName", Constants.PE_JPM_TXT_NEW_NAME);
		languageCzProperties.setProperty("Pe_Jpm_Txt_FailedToRenameFileTitle", Constants.PE_JPM_TXT_FAILED_TO_RENAME_FILE_TITLE);
		languageCzProperties.setProperty("Pe_Jpm_Txt_CannotRenameFileText", Constants.PE_JPM_TXT_CANNOT_RENAME_FILE_TEXT);
		languageCzProperties.setProperty("Pe_Jpm_Txt_CannotRenameFileTitle", Constants.PE_JPM_TXT_CANNOT_RENAME_FILE_TITLE);
		languageCzProperties.setProperty("Pe_Jpm_Txt_FileWithNameAlreadyExistText", Constants.PE_JPM_TXT_FILE_WITH_NAME_ALREADY_EXIST_TEXT);
		languageCzProperties.setProperty("Pe_Jpm_Txt_Target", Constants.PE_JPM_TXT_TARGET);
		languageCzProperties.setProperty("Pe_Jpm_Txt_FileWithNameAlreadyExistTitle", Constants.PE_JPM_TXT_FILE_WITH_NAME_ALREADY_EXIST_TITLE);
		languageCzProperties.setProperty("Pe_Jpm_Txt_ErrorWhileMovingFileText", Constants.PE_JPM_TXT_ERROR_WHILE_MOVING_FILE_TEXT);
		languageCzProperties.setProperty("Pe_Jpm_Txt_Source", Constants.PE_JPM_TXT_SOURCE);
		languageCzProperties.setProperty("Pe_Jpm_Txt_ErrorWhileMovingFileTitle", Constants.PE_JPM_TXT_ERROR_WHILE_MOVING_FILE_TITLE);
		languageCzProperties.setProperty("Pe_Jpm_Txt_FileAlreadyExistInLocationText", Constants.PE_JPM_TXT_FILE_ALREADY_EXIST_IN_LOCATION_TEXT);
		languageCzProperties.setProperty("Pe_Jpm_Txt_FileAlreadyExistInLocationTitle", Constants.PE_JPM_TXT_FILE_ALREADY_EXIST_IN_LOCATION_TITLE);
		languageCzProperties.setProperty("Pe_Jpm_Txt_ErrorWhileMovingFileToLocationText", Constants.PE_JPM_TXT_ERROR_WHILE_MOVING_FILE_TO_LOCATION_TEXT);
		languageCzProperties.setProperty("Pe_Jpm_Txt_ErrorWhileMovingFileToLocationTitle", Constants.PE_JPM_TXT_ERROR_WHILE_MOVING_FILE_TO_LOCATION_TITLE);
		languageCzProperties.setProperty("Pe_Jpm_Txt_TargetPlaceContainFileNameText", Constants.PE_JPM_TXT_TARGET_PLACE_CONTAIN_FILE_NAME_TEXT);
		languageCzProperties.setProperty("Pe_Jpm_Txt_TargetPlaceContainFileNameTitle", Constants.PE_JPM_TXT_TARGET_PLACE_CONTAIN_FILE_NAME_TITLE);
		languageCzProperties.setProperty("Pe_Jpm_Txt_ErrorWhileCopyingDirText", Constants.PE_JPM_TXT_ERROR_WHILE_COPYING_DIR_TEXT);
		languageCzProperties.setProperty("Pe_Jpm_Txt_ErrorWhileCopyingDirTitle", Constants.PE_JPM_TXT_ERROR_WHILE_COPYING_DIR_TITLE);
		languageCzProperties.setProperty("Pe_Jpm_Txt_ErrorWhileCopyingSelectedFileText", Constants.PE_JPM_TXT_ERROR_WHILE_COPYING_SELECTED_FILE_TEXT);
		languageCzProperties.setProperty("Pe_Jpm_Txt_ErrorWhileCopyingSelectedFileTitle", Constants.PE_JPM_TXT_ERROR_WHILE_COPYING_SELECTED_FILE_TITLE);
		languageCzProperties.setProperty("Pe_Jpm_Txt_TargetLocationDontExistText", Constants.PE_JPM_TXT_TARGET_LOCATION_DONT_EXIST_TEXT);
		languageCzProperties.setProperty("Pe_Jpm_Txt_TargetLocationDontExistTitle", Constants.PE_JPM_TXT_TARGET_LOCATION_DONT_EXIST_TITLE);
		languageCzProperties.setProperty("Pe_Jpm_Txt_SelectedFiledoesntExistText", Constants.PE_JPM_TXT_SELECTED_FILE_DOESNT_EXIST_TEXT);
		languageCzProperties.setProperty("Pe_Jpm_Txt_SelectedFileDoesntExistTitle", Constants.PE_JPM_TXT_SELECTED_FILE_DOESNT_EXIST_TITLE);
		languageCzProperties.setProperty("Pe_Jpm_Txt_CannotMoveFileFromOpenProjectText", Constants.PE_JPM_TXT_CANNOT_MOVE_FILE_FROM_OPEN_PROJECT_TEXT);
		languageCzProperties.setProperty("Pe_Jpm_Txt_CannotMoveFileFromOpenProjectTitle", Constants.PE_JPM_TXT_CANNOT_MOVE_FILE_FROM_OPEN_PROJECT_TITLE);
		languageCzProperties.setProperty("Pe_Jpm_Txt_ErrorWhileDeletingDirText", Constants.PE_JPM_TXT_ERROR_WHILE_DLETING_DIR_TEXT);
		languageCzProperties.setProperty("Pe_Jpm_Txt_ErrorWhileDeletingDirTitle", Constants.PE_JPM_TXT_ERROR_WHILE_DELETING_DIR_TITLE);	
		languageCzProperties.setProperty("Pe_Jpm_Txt_ErrorWhileCreatingNewListInExcelFileText_1", Constants.PE_JPM_TXT_ERROR_WHILE_CREATING_NEW_LIST_IN_EXCEL_FILE_TEXT_1);
		languageCzProperties.setProperty("Pe_Jpm_Txt_ErrorWhileCreatingNewListInExcelFileText_2", Constants.PE_JPM_TXT_ERROR_WHILE_CREATING_NEW_LIST_IN_EXCEL_FILE_TEXT_2);
		languageCzProperties.setProperty("Pe_Jpm_Txt_ErrorWhileCreatingNewListInExcelFileText_3", Constants.PE_JPM_TXT_ERROR_WHILE_CREATING_NEW_LIST_IN_EXCEL_FILE_TEXT_3);
		languageCzProperties.setProperty("Pe_Jpm_Txt_ErrorWhileCreatingNewListInExcelFileTitle", Constants.PE_JPM_TXT_ERROR_WHILE_CREATING_NEW_LIST_IN_EXCEL_FILE_TITLE);
				
				
		// Třída Menu:
		// Texty do hlavních menu:
		ConfigurationFiles.commentsMap.put("Pe_M_MenuFile", "Texty do 'hlavních' záložek na liště menu v dialogu průzkumník projektů. Jedná se o texty pro položky, která slouží jako jednotlivá 'menu / záložky' na liště menu v dialogu průzkumník projektů.");
		languageCzProperties.setProperty("Pe_M_MenuFile", Constants.PE_M_MENU_FILE);
		languageCzProperties.setProperty("Pe_M_MenuEdit", Constants.PE_M_MENU_EDIT);
		languageCzProperties.setProperty("Pe_M_MenuTools", Constants.PE_M_MENU_TOOLS);
		languageCzProperties.setProperty("Pe_M_MenuOrderFrames", Constants.PE_M_MENU_ORDER_FRAMES);

		
		// Texty do položek v menuFile:
		ConfigurationFiles.commentsMap.put("Pe_M_ItemOpenFile", "Texty pro položky v záložce 'Soubor', která slouží jako jedna z položek 'menu' v liště menu v dialogu průzkumník projektů.");
		languageCzProperties.setProperty("Pe_M_ItemOpenFile", Constants.PE_M_ITEM_OPEN_FILE);
		languageCzProperties.setProperty("Pe_M_MenuOpenFileInAppsOfOs", Constants.PE_M_MENU_OPEN_FILE_IN_APPS_OF_OS);
		languageCzProperties.setProperty("Pe_M_ItemOpenFileInFileExplorerInOs", Constants.PE_M_ITEM_OPEN_FILE_IN_FILE_EXPLORER_IN_OS);
		languageCzProperties.setProperty("Pe_M_ItemOpenFileInDefaultAppInOs", Constants.PE_M_ITEM_OPEN_FILE_IN_DEFAULT_APP_IN_OS);
		languageCzProperties.setProperty("Pe_M_ItemSave", Constants.PE_M_ITEM_SAVE);
		languageCzProperties.setProperty("Pe_M_ItemSaveAs", Constants.PE_M_ITEM_SAVE_AS);
		languageCzProperties.setProperty("Pe_M_ItemSaveAll", Constants.PE_M_ITEM_SAVE_ALL);
		languageCzProperties.setProperty("Pe_M_Rb_Item_WrapTextInSelectedFrame", Constants.PE_M_RB_ITEM_WRAP_TEXT_IN_SELECTED_FRAME);
		languageCzProperties.setProperty("Pe_M_Rb_Item_WrapTextInAllFrames", Constants.PE_M_RB_ITEM_WRAP_TEXT_IN_ALL_FRAMES);
		languageCzProperties.setProperty("Pe_M_Rb_Item_ShowWhiteSpaceInSelectedFrame", Constants.PE_M_RB_ITEM_SHOW_WHITE_SPACE_IN_SELECTED_FRAME);
		languageCzProperties.setProperty("Pe_M_Rb_Item_ShowWhiteSpaceInAllFrames", Constants.PE_M_RB_ITEM_SHOW_WHITE_SPACE_IN_ALL_FRAMES);
		languageCzProperties.setProperty("Pe_M_Rb_Item_ClearWhiteSpaceLineInSelectedFrame", Constants.PE_M_RB_ITEM_CLEAR_WHITE_SPACE_LINE_IN_SELECTED_FRAME);
		languageCzProperties.setProperty("Pe_M_Rb_Item_ClearWhiteSpaceLineInAllFrames", Constants.PE_M_RB_ITEM_CLEAR_WHITE_SPACE_LINE_IN_ALL_FRAMES);
		languageCzProperties.setProperty("Pe_M_ItemReload", Constants.PE_M_ITEM_RELOAD);
		languageCzProperties.setProperty("Pe_M_ItemReloadAll", Constants.PE_M_ITEM_RELOAD_ALL);
		languageCzProperties.setProperty("Pe_M_ItemPrint", Constants.PE_M_ITEM_PRINT);
		languageCzProperties.setProperty("Pe_M_ItemCloseSelectedWindow", Constants.PE_M_ITEM_CLOSE_SELECTED_WINDOW);
		languageCzProperties.setProperty("Pe_M_ItemCloseAllWindow", Constants.PE_M_ITEM_CLOSE_ALL_WINDOW);
		languageCzProperties.setProperty("Pe_M_ItemClose", Constants.PE_M_ITEM_CLOSE);

		
		// Popisky tlačítek v menuFile:
		ConfigurationFiles.commentsMap.put("Pe_M_TT_ItemOpenFile", "Popisky pro položky v záložce 'Soubor', která slouží jako jedna z položek 'menu' v liště menu v dialogu průzkumník projektů.");
		languageCzProperties.setProperty("Pe_M_TT_ItemOpenFile", Constants.PE_M_TT_ITEM_OPEN_FILE);
		languageCzProperties.setProperty("Pe_M_TT_ItemOpenFileInFileExplorerInOs", Constants.PE_M_TT_ITEM_OPEN_FILE_IN_FILE_EXPLORER_IN_OS);
		languageCzProperties.setProperty("Pe_M_TT_ItemOpenFileInDefaultAppInOs", Constants.PE_M_TT_ITEM_OPEN_FILE_IN_DEFAULT_APP_IN_OS);
		languageCzProperties.setProperty("Pe_M_TT_ItemSave", Constants.PE_M_TT_ITEM_SAVE);
		languageCzProperties.setProperty("Pe_M_TT_ItemSaveAs", Constants.PE_M_TT_ITEM_SAVE_AS);
		languageCzProperties.setProperty("Pe_M_TT_ItemSaveAll", Constants.PE_M_TT_ITEM_SAVE_ALL);
		languageCzProperties.setProperty("Pe_M_TT_Rb_ItemWrapTextInSelectedFrame", Constants.PE_M_TT_RB_ITEM_WRAP_TEXT_IN_SELECTED_FRAME);
		languageCzProperties.setProperty("Pe_M_TT_Rb_ItemWrapTextInAllFrames", Constants.PE_M_TT_RB_ITEM_WRAP_TEXT_IN_ALL_FRAMES);
		languageCzProperties.setProperty("Pe_M_TT_Rb_ItemShowWhiteSpaceInSelectedFrame", Constants.PE_M_TT_RB_ITEM_SHOW_WHITE_SPACE_IN_SELECTED_FRAME);
		languageCzProperties.setProperty("Pe_M_TT_Rb_ItemShowWhiteSpaceInAllFrames", Constants.PE_M_TT_RB_ITEM_SHOW_WHITE_SPACE_IN_ALL_FRAMES);
		languageCzProperties.setProperty("Pe_M_TT_Rb_ItemClearWhiteSpaceLineInSelectedFrame", Constants.PE_M_TT_RB_ITEM_CLEAR_WHITE_SPACE_LINE_IN_SELECTED_FRAME);
		languageCzProperties.setProperty("Pe_M_TT_Rb_ItemClearWhiteSpaceLineInAllFrames", Constants.PE_M_TT_RB_ITEM_CLEAR_WHITE_SPACE_LINE_IN_ALL_FRAMES);
		languageCzProperties.setProperty("Pe_M_TT_ItemReload", Constants.PE_M_TT_ITEM_RELOAD);
		languageCzProperties.setProperty("Pe_M_TT_ItemReloadAll", Constants.PE_M_TT_ITEM_RELOAD_ALL);
		languageCzProperties.setProperty("Pe_M_TT_ItemPrint", Constants.PE_M_TT_ITEM_PRINT);
		languageCzProperties.setProperty("Pe_M_TT_ItemCloseSelectedWindow", Constants.PE_M_TT_ITEM_CLOSE_SELECTED_WINDOW);
		languageCzProperties.setProperty("Pe_M_TT_ItemCloseAllWindow", Constants.PE_M_TT_ITEM_CLOSE_ALL_WINDOW);
		languageCzProperties.setProperty("Pe_M_TT_ItemClose", Constants.PE_M_TT_ITEM_CLOSE);

		// Texty do položek v menEdit:
		ConfigurationFiles.commentsMap.put("Pe_M_ItemIndentMore", "Texty pro položky v záložce 'Upravit', která slouží jako jedna z položek 'menu' v liště menu v dialogu průzkumník projektů.");
		languageCzProperties.setProperty("Pe_M_ItemIndentMore", Constants.PE_M_ITEM_INDENT_MORE);
		languageCzProperties.setProperty("Pe_M_ItemIndentLess", Constants.PE_M_ITEM_INDENT_LESS);
		languageCzProperties.setProperty("Pe_M_ItemComment", Constants.PE_M_ITEM_COMMENT);
		languageCzProperties.setProperty("Pe_M_ItemUncomment", Constants.PE_M_ITEM_UNCOMMENT);
		languageCzProperties.setProperty("Pe_M_ItemFormatCode", Constants.PE_M_ITEM_FORMAT_CODE);
		languageCzProperties.setProperty("Pe_M_ItemInsertMethod", Constants.PE_M_ITEM_INSERT_METHOD);
				
		// Popisky tlačítek v menuEdit:
		ConfigurationFiles.commentsMap.put("Pe_M_TT_ItemIndentMore", "Popisky pro položky v záložce 'Upravit', která slouží jako jedna z položek 'menu' v liště menu v dialogu průzkumník projektů.");
		languageCzProperties.setProperty("Pe_M_TT_ItemIndentMore", Constants.PE_M_TT_ITEM_INDENT_MORE);
		languageCzProperties.setProperty("Pe_M_TT_IndentLess", Constants.PE_M_TT_INDENT_LESS);
		languageCzProperties.setProperty("Pe_M_TT_ItemComment", Constants.PE_M_TT_ITEM_COMMENT);
		languageCzProperties.setProperty("Pe_M_TT_ItemUncomment", Constants.PE_M_TT_ITEM_UNCOMMENT);
		languageCzProperties.setProperty("Pe_M_TT_ItemFormatCode", Constants.PE_M_TT_ITEM_FORMAT_CODE);
		languageCzProperties.setProperty("Pe_M_TT_ItemInsertMethod", Constants.PE_M_TT_ITEM_INSERT_METHOD);

		// Texty do položek v menuTools:
		ConfigurationFiles.commentsMap.put("Pe_M_ItemReplaceText", "Texty pro položky v záložce 'Nástroje', která slouží jako jedna z položek 'menu' v liště menu v dialogu průzkumník projektů.");
		languageCzProperties.setProperty("Pe_M_ItemReplaceText", Constants.PE_M_ITEM_REPLACE_TEXT);
		languageCzProperties.setProperty("Pe_M_ItemGoToLine", Constants.PE_M_ITEM_GO_TO_LINE);
		languageCzProperties.setProperty("Pe_M_ItemGettersAndSetters", Constants.PE_M_ITEM_GETTERS_AND_SETTERS);
		languageCzProperties.setProperty("Pe_M_ItemConstructor", Constants.PE_M_ITEM_CONSTRUCTOR);
		languageCzProperties.setProperty("Pe_M_ItemCompile", Constants.PE_M_ITEM_COMPILE);
		languageCzProperties.setProperty("Pe_M_ItemMenuItemAddSerialVersionId", Constants.PE_M_MENU_ITEM_ADD_SERIAL_VERSION_ID);
		languageCzProperties.setProperty("Pe_M_ItemAddDefaultSerialVersionId", Constants.PE_M_ITEM_ADD_DEFAULT_SERIAL_VERSION_ID);
		languageCzProperties.setProperty("Pe_M_ItemAddGeneratedSerialVersionId", Constants.PE_M_ITEM_ADD_GENERATED_SERIAL_VERSION_ID);

		// Popisky tlačítek v menuTools:
		ConfigurationFiles.commentsMap.put("Pe_M_TT_ItemReplaceText", "Popisky pro položky v záložce 'Nástroje', která slouží jako jedna z položek 'menu' v liště menu v dialogu průzkumník projektů.");
		languageCzProperties.setProperty("Pe_M_TT_ItemReplaceText", Constants.PE_M_TT_ITEM_REPLACE_TEXT);
		languageCzProperties.setProperty("Pe_M_TT_ItemGoToLine", Constants.PE_M_TT_ITEM_GO_TO_LINE);
		languageCzProperties.setProperty("Pe_M_TT_ItemToString", Constants.PE_M_TT_ITEM_TO_STRING);
		languageCzProperties.setProperty("Pe_M_TT_ItemGettersAndSetters", Constants.PE_M_TT_ITEM_GETTERS_AND_SETTERS);
		languageCzProperties.setProperty("Pe_M_TT_ItemConstructor", Constants.PE_M_TT_ITEM_CONSTRUCTOR);
		languageCzProperties.setProperty("Pe_M_TT_ItemCompile", Constants.PE_M_TT_ITEM_COMPILE);
		languageCzProperties.setProperty("Pe_M_TT_ItemAddDefaultSerialVersionId", Constants.PE_M_TT_ITEM_ADD_DEFAULT_SERIAL_VERSION_ID);
		languageCzProperties.setProperty("Pe_M_TT_ItemAddGeneratedSerialVersionId", Constants.PE_M_TT_ITEM_ADD_GENERATED_SERIAL_VERSION_ID);

		// Texty do položek v menuOrderFrames:
		ConfigurationFiles.commentsMap.put("Pe_M_ItemTile", "Texty pro položky v záložce 'Uspořádat', která slouží jako jedna z položek 'menu' v liště menu v dialogu průzkumník projektů.");
		languageCzProperties.setProperty("Pe_M_ItemTile", Constants.PE_M_ITEM_TILE);
		languageCzProperties.setProperty("Pe_M_ItemTileUnderHim", Constants.PE_M_ITEM_TILE_UNDER_HIM);
		languageCzProperties.setProperty("Pe_M_ItemCascade", Constants.PE_M_ITEM_CASCADE);
		languageCzProperties.setProperty("Pe_M_ItemMinimizeAllFrames", Constants.PE_M_ITEM_MINIMIZE_ALL_FRAMES);

		// Popisky tlačítek v menuOrderFrames:
		ConfigurationFiles.commentsMap.put("Pe_M_TT_ItemTile", "Popisky pro položky v záložce 'Uspořádat', která slouží jako jedna z položek 'menu' v liště menu v dialogu průzkumník projektů.");
		languageCzProperties.setProperty("Pe_M_TT_ItemTile", Constants.PE_M_TT_ITEM_TILE);
		languageCzProperties.setProperty("Pe_M_TT_ItemTileUnderHim", Constants.PE_M_TT_ITEM_TILE_UNDER_HIM);
		languageCzProperties.setProperty("Pe_M_TT_ItemCascade", Constants.PE_M_TT_ITEM_CASCADE);
		languageCzProperties.setProperty("Pe_M_TT_ItemMinimizeAllFrames", Constants.PE_M_TT_ITEM_MINIMIZE_ALL_FRAMES);

		// Texty do proměnných - do chybových hlášek:
		ConfigurationFiles.commentsMap.put("Pe_M_Txt_CannotSavePdfFileText", "Texty pro chybové hlášky, které mohou nastat při provádění jedné z operací, které umožňují jednotlivé položky v liště menu v dialogu průzkumník projektů.");
		languageCzProperties.setProperty("Pe_M_Txt_CannotSavePdfFileText", Constants.PE_M_TXT_CANNOT_SAVE_PDF_FILE_TEXT);
		languageCzProperties.setProperty("Pe_M_Txt_Location", Constants.PE_M_TXT_LOCATION);
		languageCzProperties.setProperty("Pe_M_Txt_CannotSavePdfFileTitle", Constants.PE_M_TXT_CANNOT_SAVE_PDF_FILE_TITLE);
		languageCzProperties.setProperty("Pe_M_Txt_FileCouldNotBeSavedText", Constants.PE_M_TXT_FILE_COULD_NOT_BE_SAVED_TEXT);
		languageCzProperties.setProperty("Pe_M_Txt_FileCouldNotBeSavedTitle", Constants.PE_M_TXT_FILE_COULD_NOT_BE_SAVED_TITLE);
		languageCzProperties.setProperty("Pe_M_Txt_ContainsAreDifferentText", Constants.PE_M_TXT_CONTAINS_ARE_DIFFERENT_TEXT);
		languageCzProperties.setProperty("Pe_M_Txt_ContainsAreDifferentTitle", Constants.PE_M_TXT_CONTAINS_ARE_DIFFERENT_TITLE);
		languageCzProperties.setProperty("Pe_M_Txt_FileDoesntExistText", Constants.PE_M_TXT_FILE_DOESNT_EXIST_TEXT);
		languageCzProperties.setProperty("Pe_M_Txt_OriginalLocation", Constants.PE_M_TXT_ORIGINAL_LOCATION);
		languageCzProperties.setProperty("Pe_M_Txt_FileDoesntExistTitle", Constants.PE_M_TXT_FILE_DOESNT_EXIST_TITLE);
		languageCzProperties.setProperty("Pe_M_Txt_ClassFileIsNotloadedText", Constants.PE_M_TXT_CLASS_FILE_IS_NOT_LOADED_TEXT);
		languageCzProperties.setProperty("Pe_M_Txt_ClassFileIsNotLoadedTitle", Constants.PE_M_TXT_CLASS_FILE_IS_NOT_LOADED_TITLE);
		languageCzProperties.setProperty("Pe_M_Txt_CannotGeneratedAccessMethodsText", Constants.PE_M_TXT_CANNOT_GENERATED_ACCESS_METHODS_TEXT);
		languageCzProperties.setProperty("Pe_M_Txt_ClassIsNotInClassDiagram", Constants.PE_M_TXT_CLASS_IS_NOT_IN_CLASS_DIAGRAM);
		languageCzProperties.setProperty("Pe_M_Txt_CannotGenerateConstructor", Constants.PE_M_TXT_CANNOT_GENERATE_CONSTRUCTOR);
		languageCzProperties.setProperty("Pe_M_Txt_ClassDoesntExistAlreadyText", Constants.PE_M_TXT_CLASS_DOESNT_EXIST_ALREADY_TEXT);
		languageCzProperties.setProperty("Pe_M_Txt_ClassDoesntExistAlreadyTitle", Constants.PE_M_TXT_CLASS_DOESNT_EXIST_ALREADY_TITLE);
		languageCzProperties.setProperty("Pe_M_Txt_SrcDirIsMissingText", Constants.PE_M_TXT_SRC_DIR_IS_MISSING_TEXT);
		languageCzProperties.setProperty("Pe_M_Txt_SrcDirIsMissingTitle", Constants.PE_M_TXT_SRC_DIR_IS_MISSING_TITLE);
				
				
				
		// Třída FileInternalFrame.java:
		// texty do textových proměnných do chybových hlášek a nějakéých výpisů:
		ConfigurationFiles.commentsMap.put("Pe_Mif_Txt_SaveChangesText", "Texty pro chybové hlášky, které mohou nastat při provádění nějaké operace nad otevřeným souborem v dialogu průzkumník projektů (například uložení souboru apod.).");
		languageCzProperties.setProperty("Pe_Mif_Txt_SaveChangesText", Constants.PE_MIF_TXT_SAVE_CHANGES_TEXT);
		languageCzProperties.setProperty("Pe_Mif_Txt_SaveChangesTitle", Constants.PE_MIF_TXT_SAVE_CHANGES_TITLE);
		languageCzProperties.setProperty("Pe_Mif_Txt_FileIsNotWrittenText", Constants.PE_MIF_TXT_FILE_IS_NOT_WRITTEN_TEXT);
		languageCzProperties.setProperty("Pe_Mif_Txt_Location", Constants.PE_MIF_TXT_LOCATION);
		languageCzProperties.setProperty("Pe_Mif_Txt_FileIsNotWrittenTitle", Constants.PE_MIF_TXT_FILE_IS_NOT_WRITTEN_TITLE);
		languageCzProperties.setProperty("Pe_Mif_Txt_PdfFileCannotSaveText", Constants.PE_MIF_TXT_PDF_FILE_CANNOT_SAVE_TEXT);
		languageCzProperties.setProperty("Pe_Mif_Txt_PdfFileCannotSaveTitle", Constants.PE_MIF_TXT_PDF_FILE_CANNOT_SAVE_TITLE);
		languageCzProperties.setProperty("Pe_Mif_Txt_ErrorWhileSavingFileText", Constants.PE_MIF_TXT_ERROR_WHILE_SAVING_FILE_TEXT);
		languageCzProperties.setProperty("Pe_Mif_Txt_ErrorWhileSavingFileTitle", Constants.PE_MIF_TXT_ERROR_WHILE_SAVING_FILE_TITLE);
		languageCzProperties.setProperty("Pe_Mif_Txt_ReadOnly", Constants.PE_MIF_TXT_READ_ONLY);
		languageCzProperties.setProperty("Pe_Mif_Txt_FileDoesntExistChooseNewLocationText", Constants.PE_MIF_TXT_FILE_DOESNT_EXIST_CHOOSE_NEW_LOCATION_TEXT);
		languageCzProperties.setProperty("Pe_Mif_Txt_OriginalLocation", Constants.PE_MIF_TXT_ORIGINAL_LOCATION);
		languageCzProperties.setProperty("Pe_Mif_Txt_FileDoesntExistChooseNewLocationTitle", Constants.PE_MIF_TXT_FILE_DOESNT_EXIST_CHOOSE_NEW_LOCATION_TITLE);
				
				
		// Texty do třídy: ProjectExplorerWindowListener.java:
		ConfigurationFiles.commentsMap.put("Pe_Mwl_Txt_SaveChangesText", "Texty pro dialogové okno v dialogu průzkumník projektů, které se zobrazí při zavření dialogu průzkumník projektů s tím, že je otevřen nějaký (alespoň jeden) soubor a došlo k jeho editaci, tj. už není jeho obsah stejný, jak ten uloženy na disku.");
		languageCzProperties.setProperty("Pe_Mwl_Txt_SaveChangesText", Constants.PE_MWL_TXT_SAVE_CHANGES_TEXT);
		languageCzProperties.setProperty("Pe_Mwl_Txt_SaveChangesTitle", Constants.PE_MWL_TXT_SAVE_CHANGES_TITLE);

		// Texty do třídy NewExcelListNameForm.java
		// Texty do titulku dialogu a labelů s informacemi:
		ConfigurationFiles.commentsMap.put("Pe_Nelnf_DialogTitle", "Texty pro dialogové okno, které slouží pro získání názvu nového listu v excelovském souboru. Toto okno je možné otevřít při zvolení vytvoření nového souboru a zadání jedné zpřípon excelovského souboru (v dialogu průzkumník projektů).");
		languageCzProperties.setProperty("Pe_Nelnf_DialogTitle", Constants.PE_NELNM_DIALOG_TITLE);
		languageCzProperties.setProperty("Pe_Nelnf_LblInfo", Constants.PE_NELNM_LBL_INFO);
		languageCzProperties.setProperty("Pe_Nelnf_LblError", Constants.PE_NELNM_LBL_ERROR);
		
		// Texty do tlačítek:
		ConfigurationFiles.commentsMap.put("Pe_Nelnf_BtnOk", "Texty pro tlačítka v dialogovém okně, které slouží pro získání názvu nového listu v excelovském souboru. Okno je možné zobrazit při vytvoření nového excelovského souboru v dialogu průzkumník projektů.");
		languageCzProperties.setProperty("Pe_Nelnf_BtnOk", Constants.PE_NELNM_BTN_OK);
		languageCzProperties.setProperty("Pe_Nelnf_BtnCancel", Constants.PE_NELNM_BTN_CANCEL);
		
		// Texty do textových proměnných s hláškami:
		ConfigurationFiles.commentsMap.put("Pe_Nelnf_TxtErrorFileNameText", "Texty pro chybové hlášky pro dialogové okno, které slouží pro získání názvu nového listu v excelovském souboru (v dialogu průzkumník projektů). Tyto hlášky se zobrazí pokud dojde k nějaké chybě, například nebude zadán validní název pro nový list, nebo nebude zadán název vůbec apod.");
		languageCzProperties.setProperty("Pe_Nelnf_TxtErrorFileNameText", Constants.PE_NELNM_TXT_ERROR_FILE_NAME_TEXT);
		languageCzProperties.setProperty("Pe_Nelnf_TxtErrorFileNameTextSyntaxe", Constants.PE_NELNM_TXT_ERROR_FILE_NAME_TEXT_SYNTAXE);
		languageCzProperties.setProperty("Pe_Nelnf_TxtErrorFileNameTextSyntaxeInfo", Constants.PE_NELNM_TXT_ERROR_FILE_NAME_TEXT_SYNTAXE_INFO);
		languageCzProperties.setProperty("Pe_Nelnf_TxtErrorFileNameTitle", Constants.PE_NELNM_TXT_ERROR_FILE_NAME_TITLE);
		languageCzProperties.setProperty("Pe_Nelnf_TxtIsEmptyText", Constants.PE_NELNM_TXT_IS_EMPTY_TEXT);
		languageCzProperties.setProperty("Pe_Nelnf_TxtIsEmptyTitle", Constants.PE_NELNM_TXT_IS_EMPTY_TITLE);
				
				
				
		// Třída: NewFileForm.java:
		ConfigurationFiles.commentsMap.put("Pe_Nff_DialogTitle", "Texty pro dialogové okno, které slouží pro získání názvu pro nový soubor. Toto okno je možné zobrazit po kliknutí na položku pro vytvoření nového souboru ve stromové struktuře v dialogu průzkumník projektů.");
		languageCzProperties.setProperty("Pe_Nff_DialogTitle", Constants.PE_NFF_DIALOG_TITLE);
		languageCzProperties.setProperty("Pe_Nff_LblInfo", Constants.PE_NFF_LBL_INFO);
				
		// Texty do třídy. NewFolderForm.java:
		// texty do titulku dialogu, tlačítek a popisku - labelů
		ConfigurationFiles.commentsMap.put("Pe_N_Folder_F_DialogTitle", "Texty pro dialogové okno, které slouží pro získání názvu pro nový adresář. Toto okno je možné zobrazit po kliknutí na položku pro vytvoření nového adresáře v dialogu průzkumník projektů.");
		languageCzProperties.setProperty("Pe_N_Folder_F_DialogTitle", Constants.PE_NFOLDER_F_DIALOG_TITLE);
		languageCzProperties.setProperty("Pe_N_Folder_F_BtnOK", Constants.PE_NFOLDER_F_BTN_OK);
		languageCzProperties.setProperty("Pe_N_Folder_F_BtnCancel", Constants.PE_NFOLDER_F_BTN_CANCEL);
		languageCzProperties.setProperty("Pe_N_Folder_F_LblErrorInfo", Constants.PE_NFOLDER_F_LBL_ERROR_INFO);
		languageCzProperties.setProperty("Pe_N_Folder_F_LblInfo", Constants.PE_NFOLDER_F_LBL_INFO);
		
		// Texty do chybovýchhlášek:
		ConfigurationFiles.commentsMap.put("Pe_N_Folder_F_TxtWrongFormatText", "Texty pro chybové hlášky pro dialogové okno pro vytvoření získání názvu pro nový adresář. Toto okno je možné zobrazit po kliknutí na položku pro vytvoření nového adresáře v dialogu průzkumník projektů.");
		languageCzProperties.setProperty("Pe_N_Folder_F_TxtWrongFormatText", Constants.PE_NFOLDER_F_TXT_WRONG_FORMAT_TEXT);
		languageCzProperties.setProperty("Pe_N_Folder_F_TxtWrongFormatTitle", Constants.PE_NFOLDER_F_TXT_WRONG_FORMAT_TITLE);
		languageCzProperties.setProperty("Pe_N_Folder_F_TxtEmptyFieldText", Constants.PE_NFOLDER_F_TXT_EMPTY_FIELD_TEXT);
		languageCzProperties.setProperty("Pe_N_Folder_F_TxtEmptyFieldTitle", Constants.PE_NFOLDER_F_TXT_EMPTY_FIELD_TITLE);
				
		// Texty o třídy. ProjectExplorerDialog.java:
		// text do titulku dialogu:
		ConfigurationFiles.commentsMap.put("Pe_Ped_DialogTitle", "Titulek dialogu průzkumník projektů.");
		languageCzProperties.setProperty("Pe_Ped_DialogTitle", Constants.PE_PED_DIALOG_TITLE);
		
		// texty do proměnných do chybových a informačních hlášek:
		ConfigurationFiles.commentsMap.put("Pe_Ped_Txt_CannotSavePdfFileText", "Texty pro chybové a informační hlášky pro dialog průzkumník projektů. Převážně se jedná o texty pro oznámení například toho, že nelze otevřít nějaký soubor apod.");
		languageCzProperties.setProperty("Pe_Ped_Txt_CannotSavePdfFileText", Constants.PE_PED_TXT_CANNOT_SAVE_PDF_FILE_TEXT);
		languageCzProperties.setProperty("Pe_Ped_Txt_Location", Constants.PE_PED_TXT_LOCATION);
		languageCzProperties.setProperty("Pe_Ped_Txt_CannotSavePdfFileTitle", Constants.PE_PED_TXT_CANNOT_SAVE_PDF_FILE_TITLE);
		languageCzProperties.setProperty("Pe_Ped_Txt_FileIsAlreadyOpenText", Constants.PE_PED_TXT_FILE_IS_ALREADY_OPEN_TEXT);
		languageCzProperties.setProperty("Pe_Ped_Txt_FileIsAlreadyOpenTitle", Constants.PE_PED_TXT_FILE_IS_ALREADY_OPEN_TITLE);
		languageCzProperties.setProperty("Pe_Ped_Txt_ExcelDontContainsListText", Constants.PE_PED_TXT_EXCEL_DONT_CONTAINS_LIST_TEXT);
		languageCzProperties.setProperty("Pe_Ped_Txt_File", Constants.PE_PED_TXT_FILE);
		languageCzProperties.setProperty("Pe_Ped_Txt_ExcelFileDontContainsListTitle", Constants.PE_PED_TXT_EXCEL_FILE_DONT_CONTAINS_LIST_TITLE);
		languageCzProperties.setProperty("Pe_Ped_Txt_FileKind", Constants.PE_PED_TXT_FILE_KIND);
		languageCzProperties.setProperty("Pe_Ped_Txt_DoNotOpen", Constants.PE_PED_TXT_DO_NOT_OPEN);
		languageCzProperties.setProperty("Pe_Ped_Txt_FileDoNotOpen", Constants.PE_PED_TXT_FILE_DO_NOT_OPEN);
		languageCzProperties.setProperty("Pe_Ped_Txt_FileDoNotExistText", Constants.PE_PED_TXT_FILE_DO_NOT_EXIST_TEXT);
		languageCzProperties.setProperty("Pe_Ped_Txt_FileDoNotExistTitle", Constants.PE_PED_TXT_FILE_DO_NOT_EXIST_TITLE);	
				
				
				
				
				
				
				
		// Texty do Výstupního terminálu - balíček redirectSysout:
		// texty do třídy OutputFrame.java:
		ConfigurationFiles.commentsMap.put("Of_FrameTitle", "Text do titulku dialogu 'Výstupní terminál'.");
		languageCzProperties.setProperty("Of_FrameTitle", Constants.OF_FRAME_TITLE);

		// Texty do třídy OutputFrameMenu.java:
		ConfigurationFiles.commentsMap.put("Of_Menu_MenuOptions", "Text pro jedinou záložku v liště menu v dialogu výstupní terminál.");
		languageCzProperties.setProperty("Of_Menu_MenuOptions", Constants.OF_MENU_MENU_OPTIONS);
		// Texty do pložek:
		ConfigurationFiles.commentsMap.put("Of_Menu_Clear", "Texty pro jednotlivé položky na záložce 'Možnosti' v liště menu v dialogu výstupní terminál.");
		languageCzProperties.setProperty("Of_Menu_Clear", Constants.OF_MENU_CLEAR);
		languageCzProperties.setProperty("Of_Menu_ClearSysOutScreen", Constants.OF_MENU_CLEAR_SYS_OUT_SCREEN);
		languageCzProperties.setProperty("Of_Menu_ItemSaveToFile", Constants.OF_MENU_SAVE_TO_FILE);
		languageCzProperties.setProperty("Of_Menu_Print", Constants.OF_MENU_PRINT);
		languageCzProperties.setProperty("Of_Menu_ItemClose", Constants.OF_MENU_ITEM_CLOSE);
		languageCzProperties.setProperty("Of_Menu_ItemClearAll", Constants.OF_MENU_ITEM_CLEAR_ALL);

		// Popisky - tooltipy do položek v menu:
		ConfigurationFiles.commentsMap.put("Of_Menu_TT_ItemClose", "Texty pro popisky jednotlivých položek na záložce 'Možnosti' v liště menu v dialogu výstupní terminál.");
		languageCzProperties.setProperty("Of_Menu_TT_ItemClose", Constants.OF_MENU_TT_ITEM_CLOSE);

		// Texty pro "skládání" vět pro rozdělení textů dle System.out a err:
		ConfigurationFiles.commentsMap.put("Of_Menu_Txt_And", "Texty pro popisky položek pro vymazání, uložení nebo vytisknutí textů vypsaných v dialogu výstupní terminál (zvlášť pro System.out a System.err).");
		languageCzProperties.setProperty("Of_Menu_Txt_And", Constants.OF_MENU_TXT_AND);
		languageCzProperties.setProperty("Of_Menu_Txt_DeleteTextIn", Constants.OF_MENU_TXT_DELETE_TEXT_IN);
		languageCzProperties.setProperty("Of_Menu_Txt_SaveTextIn", Constants.OF_MENU_TXT_SAVE_TEXT_IN);
		languageCzProperties.setProperty("Of_Menu_Txt_PrintTextIn", Constants.OF_MENU_TXT_PRINT_TEXT_IN);

		// Texty do chových hlášek:
		ConfigurationFiles.commentsMap.put("Of_Menu_Txt_FileIsNotWrittenText", "Texty pro chybovou hlášku ohledně toho, že se nepodařilo uložit text vypsaný v editoru v diaogovém okně výstupní terminál do zadaného souboru. Tato hláška může nastat při selhání pokusu o uložení textu vypsaný v editoru v pro dialogu výstupní terminál.");
		languageCzProperties.setProperty("Of_Menu_Txt_FileIsNotWrittenText", Constants.OF_MENU_TXT_FILE_IS_NOT_WRITTEN_TEXT);
		languageCzProperties.setProperty("Of_Menu_Txt_Location", Constants.OF_MENU_TXT_LOCATION);
		languageCzProperties.setProperty("Of_Menu_Txt_FileIsNotWrittenTitle", Constants.OF_MENU_TXT_FILE_IS_NOT_WRITTEN_TITLE);

		// Texty do JChecBoxů pro vypisy hlaviček konstruktorů a metod a mazání textů před vypsáním výpisů z kontruktorů a metod:
		ConfigurationFiles.commentsMap.put("Of_Menu_Chcb_RecordCallConstructors", "Texty do komponenty typu JCheckBox, které se nachází v menu v okně výstupní terminál. Jedná se o komponenty pro nastavení toho, zda se mají vypisovat názvy metod a konstruktorů, případně jejich předané parametry (jsou li) při jejich zavolání. Dále, zda se má vymazat textové pole v terminálu před výpisem ze zavolané metody nebo konstruktoru - výpisy v System.out.");
		languageCzProperties.setProperty("Of_Menu_Chcb_RecordCallConstructors", Constants.OF_MENU_CHCB_RECORD_CALL_CONSTRUCTORS);
		languageCzProperties.setProperty("Of_Menu_Chcb_RecordCallMethods", Constants.OF_MENU_CHCB_RECORD_CALL_METHODS);
		languageCzProperties.setProperty("Of_Menu_Chcb_RecordCallConstructors_TT", Constants.OF_MENU_CHCB_RECORD_CALL_CONSTRUCTORS_TT);
		languageCzProperties.setProperty("Of_Menu_Chcb_RecordCallMethods_TT", Constants.OF_MENU_CHCB_RECORD_CALL_METHODOS_TT);

		languageCzProperties.setProperty("Of_Menu_Chcb_ClearSysOutScreenWhenCallConstructor", Constants.OF_MENU_CHCB_CLEAR_SYS_OUT_SCREEN_WHEN_CALL_CONSTRUCTOR);
		languageCzProperties.setProperty("Of_Menu_Chcb_ClearSysOutScreenWhenCallMethod", Constants.OF_MENU_CHCB_CLEAR_SYS_OUT_SCREEN_WHEN_CALL_METHOD);
		languageCzProperties.setProperty("Of_Menu_Chcb_ClearSysOutScreenWhenCallConstructor_TT", Constants.OF_MENU_CHCB_CLEAR_SYS_OUT_SCREEN_WHEN_CALL_CONSTRUCTOR_TT);
		languageCzProperties.setProperty("Of_Menu_Chcb_ClearSysOutScreenWhenCallMethod_TT", Constants.OF_MENU_CHCB_CLEAR_SYS_OUT_SCREEN_WHEN_CALL_METHOD_TT);

				
				
				
				
				
		// Texty do třídy ExceptionLogger - akorát texty do chybových hlášek:
		ConfigurationFiles.commentsMap.put("Ml_Txt_FailedToLoadOrCreatingFileText", "Texty pro chybové hlášky, které mohou nastat při čtení či zápisu do souboru s příponou '.log'. Jedná se o soubor, kam se zapisují vzniklé výjimky (zachytávané aplikací), dále start a ukončení aplikace.");
		languageCzProperties.setProperty("Ml_Txt_FailedToLoadOrCreatingFileText", Constants.ML_TXT_FAILED_TO_LOAD_OR_CREATING_FILE_TEXT);
		languageCzProperties.setProperty("Ml_Txt_ForWriteException", Constants.ML_TXT_FOR_WRITE_EXCEPTION);
		languageCzProperties.setProperty("Ml_Txt_ErrorInAccessToFile", Constants.ML_TXT_ERROR_IN_ACCESS_TO_FILE);
		languageCzProperties.setProperty("Ml_Txt_FailedToLoadOrCreatingFileTitle", Constants.ML_TXT_FAILED_TO_LOAD_OR_CREATING_FILE_TITLE);
		languageCzProperties.setProperty("Ml_Txt_ErrorWhileOpeningFile", Constants.ML_TXT_ERROR_WHILE_OPENING_FILE);
		languageCzProperties.setProperty("Ml_Txt_ErrorWhileCreatingDirText", Constants.ML_TXT_ERROR_WHILE_CREATING_DIR_TEXT);
		languageCzProperties.setProperty("Ml_Txt_ErrorWhileCreatingDirTitle", Constants.ML_TXT_ERROR_WHILE_CREATING_DIR_TITLE);
		
		
		
		
		
		
		
		
		
		// Texty do třídy / dialogu NameForInstance:		
		ConfigurationFiles.commentsMap.put("NFI_DialogTitle", "Texty do dialogu pro zadání nové reference, která je potřeba v případě, že se zavolá metoda pomocí kontextového menu nad třídou v diagramu tříd nebo nad instancí v diagramu instancí. Tato metoda vrátí instanci třídy, která se nachází v diagramu tříd, ale příslušná vrácená instance ještě není v diagramu instancí reprezentována, tak příslušný dialog slouží pro zadání nové reference.");
		languageCzProperties.setProperty("NFI_DialogTitle", Constants.NFI_DIALOG_TITLE);
		languageCzProperties.setProperty("NFI_LblInstanceClassInfo", Constants.NFI_LBL_INSTANCE_CLASS_INFO);
		languageCzProperties.setProperty("NFI_LblInstance", Constants.NFI_LBL_INSTANCE);
		languageCzProperties.setProperty("NFI_InsertReference", Constants.NFI_INSERT_REFERENCE);
		
		// Texty pro label, který ukazuje, že byla zadána chybná syntaxe nebo duplicitní hodnota:
		ConfigurationFiles.commentsMap.put("NFI_TxtBadSyntaxInReference", "Texty do dialogu pro zadání nové reference při převzetí instance ze zavolané metody, následující texty jsou pro label a obsahují informaci o tom, že byla zadána chybná syntaxe reference nebo duplicitní refrence.");
		languageCzProperties.setProperty("NFI_TxtBadSyntaxInReference", Constants.NFI_TXT_BAD_SYNTAX_IN_REFERENCE);
		languageCzProperties.setProperty("NFI_TxtReferenceAlreadyExist", Constants.NFI_TXT_REFERENCE_ALREADY_EXIST);
		
		// Texty do chybových hlášek:
		ConfigurationFiles.commentsMap.put("NFI_TxtJopEmptyFieldText", "Texty pro chybové hlášky do dialogu pro zadání reference pro instanci, která se vrátila ze zavolané metody.");
		languageCzProperties.setProperty("NFI_TxtJopEmptyFieldText", Constants.NFI_TXT_JOP_EMPTY_FIELD_TEXT);
		languageCzProperties.setProperty("NFI_TxtJopEmptyFieldTitle", Constants.NFI_TXT_JOP_EMPTY_FIELD_TITLE);
		languageCzProperties.setProperty("NFI_TxtjopBadSyntaxText", Constants.NFI_TXT_JOP_BAD_SYNTAX_TEXT);
		languageCzProperties.setProperty("NFI_TxtJopBadSyntaxTitle", Constants.NFI_TXT_JOP_BAD_SYNTAX_TITLE);
		languageCzProperties.setProperty("NFI_TxtJopDupliciteReferenceText", Constants.NFI_TXT_JOP_DUPLICITE_REFERENCE_TEXT);
		languageCzProperties.setProperty("NFI_TxtJopDupliciteReferenceTitle", Constants.NFI_TXT_JOP_DUPLICITE_REFERENCE_TITLE);
		
		// Texty pro tlačítka:
		ConfigurationFiles.commentsMap.put("NFI_BtnOk", "Texty do tlačítek v dialogu, který slouží pro zadání reference na instanci, která se vrátila z nějaké metody a není reprezentována v diagramu instancí.");
		languageCzProperties.setProperty("NFI_BtnOk", Constants.NFI_BTN_OK);
		languageCzProperties.setProperty("NFI_BtnCancel", Constants.NFI_BTN_CANCEL);
		
		
		
		
		
		
				
				
		
		
		
		
		// Texty do dialogu pro informace o autorovi aplikace - dialog, resp. třída AboutAppForm.java:
		// Jendá se o některé texty , které je možné přeložit do více jazyků:
		ConfigurationFiles.commentsMap.put("Aaf_DialogTitle", "Některé texty do dialogového okna s informacemi ohledně autora aplikace " + Constants.APP_TITLE + ".");
		languageCzProperties.setProperty("Aaf_DialogTitle", Constants.AAF_DIALOG_TITLE);
		languageCzProperties.setProperty("Aaf_BtnClose", Constants.AAF_BTN_CLOSE);
		languageCzProperties.setProperty("Aaf_BtnJavaVersion", Constants.AAF_BTN_JAVA_VERSION);
		languageCzProperties.setProperty("Aaf_Txt_Name", Constants.AAF_TXT_NAME);
		languageCzProperties.setProperty("Aaf_Txt_Version", Constants.AAF_TXT_VERSION);
		languageCzProperties.setProperty("Aaf_Txt_Author", Constants.AAF_TXT_AUTHOR);
		languageCzProperties.setProperty("Aaf_Txt_WikiAboutApp", Constants.AAF_TXT_WIKI_ABOUT_APP);
		languageCzProperties.setProperty("Aaf_Txt_Email", Constants.AAF_TXT_EMAIL);
		languageCzProperties.setProperty("Aaf_Txt_Unknown", Constants.AAF_TXT_UNKNOWN);
		languageCzProperties.setProperty("Aaf_Txt_DoesNotExist", Constants.AAF_TXT_DOES_NOT_EXIST);
		languageCzProperties.setProperty("Aaf_Txt_DebugInfo", Constants.AAF_TXT_DEBUG_INFO);
		languageCzProperties.setProperty("Aaf_Txt_ForApp", Constants.AAF_TXT_FOR_APP);

        ConfigurationFiles.commentsMap.put("Aaf_Txt_Path", "Texty pro chybové hlášky, které mohou nastat například v případě, že adresář nebo soubor nebyl nalezen při pokusu o jeho otevření pro kontextové menu. V dialogu s přehledem informací o autorovi ve spodní části s odkazy na cesty k vybraným souborů a adresářům.");
		languageCzProperties.setProperty("Aaf_Txt_Path", Constants.AAF_TXT_PATH);
		languageCzProperties.setProperty("Aaf_Txt_DirDoesNotExist_Text", Constants.AAF_TXT_DIR_DOES_NOT_EXIST_TEXT);
		languageCzProperties.setProperty("Aaf_Txt_DirDoesNotExist_Title", Constants.AAF_TXT_DIR_DOES_NOT_EXIST_TITLE);
		languageCzProperties.setProperty("Aaf_Txt_FileDoesNotExist_Text", Constants.AAF_TXT_FILE_DOES_NOT_EXIST_TEXT);
		languageCzProperties.setProperty("Aaf_Txt_FileDoesNotExist_Title", Constants.AAF_TXT_FILE_DOES_NOT_EXIST_TITLE);




        ConfigurationFiles.commentsMap.put("Aaf_Pm_Item_OpenInFileExplorer_Text", "Texty do kontextového menu, které se otevře kliknutím pravým tlačítkem myši na labely / odkazy ve spodní části dialogu s přehledem informací o autorovi aplikace.");
        // Texty do položek:
        languageCzProperties.setProperty("Aaf_Pm_Item_OpenInFileExplorer_Text", Constants.AAF_PM_ITEM_OPEN_IN_FILE_EXPLORER_TEXT);
        languageCzProperties.setProperty("Aaf_Pm_Item_OpenInProjectExplorer_Text", Constants.AAF_PM_ITEM_OPEN_IN_PROJECT_EXPLORER_TEXT);
        languageCzProperties.setProperty("Aaf_Pm_Item_OpenInCodeEditor_Text", Constants.AAF_PM_ITEM_OPEN_IN_CODE_EDITOR_TEXT);
        languageCzProperties.setProperty("Aaf_Pm_Item_OpenInDefaultApp_Text", Constants.AAF_PM_ITEM_OPEN_IN_DEFAULT_APP);
        // Tooltipy do položek:
        languageCzProperties.setProperty("Aaf_Pm_Item_OpenInFileExplorer_TT", Constants.AAF_PM_ITEM_OPEN_IN_FILE_EXPLORER_TT);
        languageCzProperties.setProperty("Aaf_Pm_Item_OpenInProjectExplorer_TT", Constants.AAF_PM_ITEM_OPEN_IN_PROJECT_EXPLORER_TT);
        languageCzProperties.setProperty("Aaf_Pm_Item_OpenInCodeEditor_TT", Constants.AAF_PM_ITEM_OPEN_IN_CODE_EDITOR_TT);
        languageCzProperties.setProperty("Aaf_Pm_Item_OpenInDefaultApp_TT", Constants.AAF_PM_ITEM_OPEN_IN_DEFAULT_APP_TT);

		
		
		
		
		
		
		
		// Texty do třídy, resp. vlákna CheckCompileFiles v balíčku App. jedná se pouze
		// o texty pro výpisy ohledné výsledku porovední příslušné operace asice
		// nalezení potřebných souborů pro aplikaci, aby mohla kompilovat Javovské
		// třídy.
		ConfigurationFiles.commentsMap.put("Ccf_Jop_Txt_FilesAlreadyExistText", "Texty do vlákna, které slouží pro nalezení a zkopírování souborů, ke kterým potřebuje tato aplikace přístup, aby mohla kompilovat třídy jazyka Java. Jedná se o texty, které budou zobrazeny pouze v případě, že dojde například k tomu, že nebyl nazelen nějaký z potřených souborů apod. Ale tyto hlášky budou zobrazeny pouze v případě, že uživatel spustí toto vlákno kliknutím na položku 'Vyhledat' v menu 'Java' v menu 'Nástoje'. Jinak se toto vlákno spustí po spuštění aplikace (poud je tak nastaveno) ale žádné hlášky se nevypíší, ať už budou soubory nalezeny nebo ne.");
		languageCzProperties.setProperty("Ccf_Jop_Txt_FilesAlreadyExistText", Constants.CCF_JOP_TXT_FILES_ALREADY_EXIST_TEXT);
		languageCzProperties.setProperty("Ccf_Jop_Txt_FilesAlreadyExistTitle", Constants.CCF_JOP_TXT_FILES_ALREADY_EXIST_TITLE);
		languageCzProperties.setProperty("Ccf_Jop_Txt_FilesSuccessfullyCopiedText", Constants.CCF_JOP_TXT_FILES_SUCCESSFULLY_COPIED_TEXT);
		languageCzProperties.setProperty("Ccf_Jop_Txt_FilesSuccessfullyCopiedTitle", Constants.CCF_JOP_TXT_FILES_SUCCESSFULLY_COPIED_TITLE);
		languageCzProperties.setProperty("Ccf_Jop_Txt_FilesNotFoundText", Constants.CCF_JOP_TXT_FILES_NOT_FOUND_TEXT);
		languageCzProperties.setProperty("Ccf_Jop_Txt_FilesNotFoundTitle", Constants.CCF_JOP_TXT_FILES_NOT_FOUND_TITLE);
		
		
		
		
		
		
		
		
		
		
		
		
		// Texty do třídy InfoForm.java - dialog se základními informacemi ohledně ovládání aplikace.
		ConfigurationFiles.commentsMap.put("If_Txt_DialogTitle", "Texty do dialogového okna, které obsahuje informace ohledně ovládání a základních funkcí této aplikace.");
		languageCzProperties.setProperty("If_Txt_DialogTitle", Constants.IF_DIALOG_TITLE);
		languageCzProperties.setProperty("If_Txt_BasicInfo", Constants.IF_TXT_BASIC_INFO);
		languageCzProperties.setProperty("If_Txt_ClassDiagram", Constants.IF_TXT_CLASS_DIAGRAM);
		languageCzProperties.setProperty("If_Txt_CreateClass", Constants.IF_TXT_CREATE_CLASS);
		languageCzProperties.setProperty("If_Txt_InfoHowToCreateClass", Constants.IF_TXT_INFO_HOW_TO_CREATE_CLASS);
		languageCzProperties.setProperty("If_Txt_HowToWriteClassName", Constants.IF_TXT_INFO_HOW_TO_WRITE_CLASS_NAME);
		languageCzProperties.setProperty("If_Txt_InfoForClassNameWithoutPackages", Constants.IF_TXT_INFO_FOR_CLASS_NAME_WITHOUT_PACKAGES);
		languageCzProperties.setProperty("If_Txt_ChooseTemplateForClass", Constants.IF_TXT_CHOOSE_TEMPLATE_FOR_CLASS);
		languageCzProperties.setProperty("If_Txt_AddRelations", Constants.IF_TXT_ADD_RELATIONS);
		languageCzProperties.setProperty("If_Txt_InfoForClickOnButton", Constants.IF_TXT_INFO_FOR_CLICK_ON_BUTTON);
		languageCzProperties.setProperty("If_Txt_ChooseClassesByClick", Constants.IF_TXT_CHOOSE_CLASSES_BY_CLICK);
		languageCzProperties.setProperty("If_Txt_AddedSyntaxeToSourceCode", Constants.IF_TXT_ADDED_SYNTAXE_TO_SOURCE_CODE);
		languageCzProperties.setProperty("If_Txt_InfoForCreateEdge", Constants.IF_TXT_INFO_FOR_CREATE_EDGE);
		languageCzProperties.setProperty("If_Txt_InfoForAddComment", Constants.IF_TXT_INFO_FOR_ADD_COMMENT);
		languageCzProperties.setProperty("If_Txt_RemoveObjectOrRelations", Constants.IF_TXT_REMOVE_OBJECT_OR_RELATIONS);
		languageCzProperties.setProperty("If_Txt_InfoHowToRemoveObject", Constants.IF_TXT_INFO_HOW_TO_REMOVE_OBJECT);
		languageCzProperties.setProperty("If_Txt_InfoForRemoveComment", Constants.IF_TXT_INFO_FOR_REMOVE_COMMENT);
		languageCzProperties.setProperty("If_Txt_InfoHowToRemoveRelations", Constants.IF_TXT_INFO_HOW_TO_REMOVE_RELATIONS);
		languageCzProperties.setProperty("If_Txt_InfoHowToRemoveClass", Constants.IF_TXT_INFO_HOW_TO_REMOVE_CLASS);
		languageCzProperties.setProperty("If_Txt_InstancesDiagram", Constants.IF_TXT_INSTANCES_DIAGRAM);
		languageCzProperties.setProperty("If_Txt_CreateInstanceText", Constants.IF_TXT_CREATE_INSTANCE_TEXT);		
		languageCzProperties.setProperty("If_Txt_InfoHowToCreateInstance", Constants.IF_TXT_INFO_HOW_TO_CREATE_INSTANCE);
		languageCzProperties.setProperty("If_Txt_InfoAboutDialogForReference", Constants.IF_TXT_INFO_ABOUT_DIALOG_FOR_REFERENCE);
		languageCzProperties.setProperty("If_Txt_InfoAboutAddedEdgeToDiagram", Constants.IF_TXT_INFO_ABOUT_ADDED_EDGE_TO_DIAGRAM);
		languageCzProperties.setProperty("If_Txt_RemoveInstanceText", Constants.IF_TXT_REMOVE_INSTANCE_TEXT);
		languageCzProperties.setProperty("If_Txt_InfoHowToRemoveInstance", Constants.IF_TXT_INFO_HOW_TO_REMOVE_INSTANCE);
		languageCzProperties.setProperty("If_Txt_ShowContentMenu", Constants.IF_TXT_SHOW_CONTEXT_MENU);
		languageCzProperties.setProperty("If_Txt_DeleteInstanceByDelKey", Constants.IF_TXT_DELETE_INSTANCE_BY_DEL_KEY);
		languageCzProperties.setProperty("If_Txt_CreateRelationsText", Constants.IF_TXT_CREATE_RELATIONS_TEXT);
		languageCzProperties.setProperty("If_Txt_InfoHowToCreateRelations", Constants.IF_TXT_INFO_HOW_TO_CREATE_RELATIONS);
		languageCzProperties.setProperty("If_Txt_InfoHowToFillVariable", Constants.IF_TXT_INFO_HOW_TO_FILL_VARIABLE);
		languageCzProperties.setProperty("If_Txt_CallingMethodText", Constants.IF_TXT_CALLING_METHOD_TEXT);
		languageCzProperties.setProperty("If_Txt_InfoHowToCallMethod", Constants.IF_TXT_INFO_HOW_TO_CALL_METHOD);
		languageCzProperties.setProperty("If_Txt_InfoCallStaticMethodInContextMenuOverClass", Constants.IF_TXT_INFO_CALL_STATIC_METHOD_IN_CONTEXT_MENU_OVER_CLASS);
		languageCzProperties.setProperty("If_Txt_InfoCallMethodOverInstanceInId", Constants.IF_TXT_INFO_CALL_METHOD_OVER_INSTANCE_IN_ID);		
		languageCzProperties.setProperty("If_Txt_InfoHowToCallMethod_2", Constants.IF_TXT_INFO_HOW_TO_CALL_METHOD_2);
		languageCzProperties.setProperty("If_Txt_CallMethodDialogForParameters", Constants.IF_TXT_CALL_METHOD_DIALOG_FOR_PARAMETERS);
		languageCzProperties.setProperty("If_Txt_InfoOnlyFewSupportedDataTypesInMethodParameters", Constants.IF_TXT_INFO_ONLY_FEW_SUPPORTED_DATA_TYPES_IN_METHOD_PARAMETERS);
		languageCzProperties.setProperty("If_Txt_InfoAboutLoadingVarsFromClasses", Constants.IF_TXT_INFO_ABOUT_LOADING_VARS_FROM_CLASSES);
		languageCzProperties.setProperty("If_Txt_CommandEditorText", Constants.IF_TXT_COMMAND_EDITOR_TEXT);
		languageCzProperties.setProperty("If_Txt_InfoAboutCommandEditor", Constants.IF_TXT_INFO_ABOUT_COMMAND_EDITOR);
		languageCzProperties.setProperty("If_Txt_InfoHowToPrintAllCommands", Constants.IF_TXT_INFO_HOW_TO_PRINT_ALL_COMMANDS);
		languageCzProperties.setProperty("If_Txt_InfoAboutListOfCommands", Constants.IF_TXT_INFO_ABOUT_LIST_OF_COMMANDS);
		languageCzProperties.setProperty("If_Txt_SourceCodeEditorText", Constants.IF_TXT_SOURCE_CODE_EDITOR_TEXT);
		languageCzProperties.setProperty("If_Txt_InfoAboutSourceCodeEditor", Constants.IF_TXT_INFO_ABOUT_SOURCE_CODE_EDITOR);
		languageCzProperties.setProperty("If_Txt_InfoAboutFunctionsOfEditor", Constants.IF_TXT_INFO_ABOUT_FUNCTIONS_OF_EDITOR);
		languageCzProperties.setProperty("If_Txt_InfoAboutGenerateConstructor", Constants.IF_TXT_INFO_ABOUT_GENERATE_CONSTRUCTOR);
		languageCzProperties.setProperty("If_Txt_InfoAboutGenerateAccessMethods", Constants.IF_TXT_INFO_ABOUT_GENERATE_ACCESS_METHODS);
		languageCzProperties.setProperty("If_Txt_InfoAboutContentOfCodeEditor", Constants.IF_TXT_INFO_ABOUT_CONTENT_OF_CODE_EDITOR);
		languageCzProperties.setProperty("If_Txt_InfoHowToOpenCodeEditor", Constants.IF_TXT_INFO_HOW_TO_OPEN_CODE_EDITOR);
		languageCzProperties.setProperty("If_Txt_OpenDeferentFilesInProjectExplorerDialog", Constants.IF_TXT_OPEN_DIFERENT_FILES_IN_PROJECT_EXPLORER_DIALOG);
		languageCzProperties.setProperty("If_Txt_ProjectExplorerText", Constants.IF_TXT_PROJECT_EXPLORER_TEXT);
		languageCzProperties.setProperty("If_Txt_InfoHowToOpenProjectExplorer", Constants.IF_TXT_INFO_HOW_TO_OPEN_PROJECT_EXPLORER);
		languageCzProperties.setProperty("If_Txt_InfoHowToOpenCodeEditor_2", Constants.IF_TXT_INFO_HOW_TO_OPEN_CODE_EDITOR_2);
		languageCzProperties.setProperty("If_Txt_InfoAboutOpennedWorkspaceDir", Constants.IF_TXT_INFO_ABOUT_OPENNED_WORKSPACE_DIR);
		languageCzProperties.setProperty("If_Txt_InfoAboutAccessedmenuField", Constants.IF_TXT_INFO_ABOUT_ACCESSED_MENU_FIELD);
		languageCzProperties.setProperty("If_Txt_InfoAboutOpenMoreKindOfFiles", Constants.IF_TXT_INFO_ABOUT_OPEN_MORE_KIND_OF_FILES);
		languageCzProperties.setProperty("If_Txt_InfoAboutWritingSourceCode", Constants.IF_TXT_INFO_ABOUT_WRITING_SOURCE_CODE);
		languageCzProperties.setProperty("If_Txt_InfoHowToOpenFile", Constants.IF_TXT_INFO_HOW_TO_OPEN_FILE);
		languageCzProperties.setProperty("If_Txt_HowToOpenOtherFile", Constants.IF_TXT_HOW_TO_OPEN_OTHER_FILE);

		
		
		
		
		
		
		
		
		
		
		// Následují texty do dialogu FontSizeForm - dialog pro "rychlé" nastavení velkosti psíma za účelem výuky, třeba při promítání na plátně apod.
		ConfigurationFiles.commentsMap.put("FSF_DialogTitle", "Text do titulku dialogu.");
		languageCzProperties.setProperty("FSF_DialogTitle", Constants.FSF_DIALOG_TITLE);
		
		// Texty pro menu:
		ConfigurationFiles.commentsMap.put("FSF_Menu_MenuDialog", "Texty do menu v dialogu pro 'rychlý' způsob nastavení velikosti písma pro objekty v diagramu tříd a v diagramu instancí.");
		languageCzProperties.setProperty("FSF_Menu_MenuDialog", Constants.FSF_MENU_MENU_DIALOG);
		languageCzProperties.setProperty("FSF_Menu_KindOfForm", Constants.FSF_MENU_KIND_OF_FORM);
		languageCzProperties.setProperty("FSF_Menu_Rb_KindItemOneSameSizeText", Constants.FSF_MENU_RB_KIND_ITEM_ONE_SAME_SIZE_TEXT);
		languageCzProperties.setProperty("FSF_Menu_Rb_KindItemOneSizeForChoosedObjectsText", Constants.FSF_MENU_RB_KIND_ITEM_ONE_SIZE_FOR_CHOOSED_OBJECTS_TEXT);
		languageCzProperties.setProperty("FSF_Menu_Rb_KindItemOneSizeForEachObjectText", Constants.FSF_MENU_RB_KIND_ITEM_ONE_SIZE_FOR_EACH_OBJECT_TEXT);
		languageCzProperties.setProperty("FSF_Menu_Rb_KindItemOneSameSize_TT", Constants.FSF_MENU_RB_KIND_ITEM_ONE_SAME_SIZE_TT);
		languageCzProperties.setProperty("FSF_Menu_Rb_KindItemOneSizeForChoosedObjects_TT", Constants.FSF_MENU_RB_KIND_ITEM_ONE_SIZE_FOR_CHOOSED_OBJECTS_TT);
		languageCzProperties.setProperty("FSF_Menu_Rb_KindItemOneSizeForEachObject_TT", Constants.FSF_MENU_RB_KIND_ITEM_ONE_SIZE_FOR_EACH_OBJECT_TT);
		languageCzProperties.setProperty("FSF_Menu_Rb_SetFontsWhileMovingText", Constants.FSF_MENU_RB_SET_FONTS_WHILE_MOVING_TEXT);
		languageCzProperties.setProperty("FSF_Menu_Rb_SetFontsWhileMoving_TT", Constants.FSF_MENU_RB_SET_FONTS_WHILE_MOVING_TT);
		languageCzProperties.setProperty("FSF_Menu_ItemRestoreDefault_Text", Constants.FSF_MENU_ITEM_RESTORE_DEFAULT_TEXT);
		languageCzProperties.setProperty("FSF_Menu_ItemRestoreDefault_TT", Constants.FSF_MENU_ITEM_RESTORE_DEFAULT_TT);
		languageCzProperties.setProperty("FSF_Menu_ItemClose_Text", Constants.FSF_MENU_ITEM_CLOSE_TEXT);
		languageCzProperties.setProperty("FSF_Menu_ItemClose_TT", Constants.FSF_MENU_ITEM_CLOSE_TT);
		
		
		// Texty do panelu, který obsahuje komponenty pro nastavení velikosti každého objektu zvlášť:
		ConfigurationFiles.commentsMap.put("FSF_OSFEOP_LblInfo", "Texty do panelu, který obsahuje komponenty pro nastavení každého objektu v diagramu tříd a v diagramu instancí tak, že každý objekt lze nastavit individuálně, v dialogu pro nastavení velikosti písma 'rychlým' způsobem.");
		languageCzProperties.setProperty("FSF_OSFEOP_LblInfo", Constants.FSF_OSFEOP_LBL_INFO);
		languageCzProperties.setProperty("FSF_OSFEOP_LblClassDiagram", Constants.FSF_OSFEOP_LBL_CLASS_DIAGRAM);
		languageCzProperties.setProperty("FSF_OSFEOP_LblInstanceDiagram", Constants.FSF_OSFEOP_LBL_INSTANCE_DIAGRAM);
		languageCzProperties.setProperty("FSF_OSFEOP_TxtSize", Constants.FSF_OSFEOP_TXT_SIZE);		
		languageCzProperties.setProperty("FSF_OSFEOP_LblClass", Constants.FSF_OSFEOP_LBL_CLASS);
		languageCzProperties.setProperty("FSF_OSFEOP_LblComment", Constants.FSF_OSFEOP_LBL_COMMENT);
		languageCzProperties.setProperty("FSF_OSFEOP_LblCommentEdge", Constants.FSF_OSFEOP_LBL_COMMENT_EDGE);
		languageCzProperties.setProperty("FSF_OSFEOP_LblAssociation", Constants.FSF_OSFEOP_LBL_ASSOCIATION);
		languageCzProperties.setProperty("FSF_OSFEOP_LblAggregation_1_1", Constants.FSF_OSFEOP_LBL_AGGREGATION_1_1);
		languageCzProperties.setProperty("FSF_OSFEOP_LblExtends", Constants.FSF_OSFEOP_LBL_EXTENDS);
		languageCzProperties.setProperty("FSF_OSFEOP_LblImplements", Constants.FSF_OSFEOP_LBL_IMPLEMENTS);
		languageCzProperties.setProperty("FSF_OSFEOP_LblAggregation_1_N", Constants.FSF_OSFEOP_LBL_AGGREGATION_1_N);
		languageCzProperties.setProperty("FSF_OSFEOP_LblInstance", Constants.FSF_OSFEOP_LBL_INSTANCE);		
		languageCzProperties.setProperty("FSF_OSFEOP_LblInstanceAttributes", Constants.FSF_OSFEOP_LBL_INSTANCE_ATTRIBUTES);
		languageCzProperties.setProperty("FSF_OSFEOP_LblInstanceMethods", Constants.FSF_OSFEOP_LBL_INSTANCE_METHODS);		
		languageCzProperties.setProperty("FSF_OSFEOP_LblInstanceAssociation", Constants.FSF_OSFEOP_LBL_INSTANCE_ASSOCIATION);
		languageCzProperties.setProperty("FSF_OSFEOP_LblInstanceAggregation", Constants.FSF_OSFEOP_LBL_INSTANCE_AGGREGATION);
		
		
		// Texty do panelu, který obsahuje komponety pro nastavení jedné stejné velikosti pro všechny objekty v diagramu tříd a v diagramu instancí:
		ConfigurationFiles.commentsMap.put("FSF_SSP_LblInfo", "Texty do panelu, který obsahuje pouze jeden slider, kterým lze nastavit stejnou velikost pro všechny objekty jak v diagramu tříd, tak v diagramu instancí. Panel se nachází v dialogu pro 'rychlé' nastavení velikosti písma.");
		languageCzProperties.setProperty("FSF_SSP_LblInfo", Constants.FSF_SSP_LBL_INFO);
		languageCzProperties.setProperty("FSF_SSP_TxtSize", Constants.FSF_SSP_TXT_SIZE);
		languageCzProperties.setProperty("FSF_SSP_FontSize", Constants.FSF_SSP_FONT_SIZE);
		
		
		// Texty do panelu, který obsahuje komponenty pro nastavení velikosti písma tak, že si uživatel pouze označí jakým objektům se má nastavit nějaká velikost písma:
		ConfigurationFiles.commentsMap.put("FSF_SSSOP_TxtSize", "Texty do panelu, který obsahuje komponenty pro nastavení velikosti objektům v diagramu tříd a v diagramu instancí, ale tak, že si uživatel označí pouze ty objekty, kterým chce nastavit velikost a pak ji pomocí příslušného posuvníku nastaví. Panel se nachází v dialogu pro 'rychlé' nastavení velikosti písma.");
		languageCzProperties.setProperty("FSF_SSSOP_TxtSize", Constants.FSF_SSSOP_TXT_SIZE);
		languageCzProperties.setProperty("FSF_SSSOP_LblInfo", Constants.FSF_SSSOP_LBL_INFO);
		languageCzProperties.setProperty("FSF_SSSOP_SliderFontSize", Constants.FSF_SSSOP_SLIDER_FONT_SIZE);
		
		
		// Texty do panelu, který obsahuje komponenty pro označení objektů v diagramu tříd, kterým se má nastavit velikost písma:
		ConfigurationFiles.commentsMap.put("FSF_SSOCP_BorderTitle", "Texty do panelu, který obsahuje komponenty pro nastavení toho, jakým objektům se má nastavit velikost písma při pohybu posuvníkem. Tento panel se nachází v panelu pro nastavení velikosti písma dle označených objektů v dialogu pro 'rychlé' nastavení velikosti písma.");
		languageCzProperties.setProperty("FSF_SSOCP_BorderTitle", Constants.FSF_SSOCP_BORDER_TITLE);
		languageCzProperties.setProperty("FSF_SSOCP_Chcb_ClassText", Constants.FSF_SSOCP_CHCB_CLASS_TEXT);
		languageCzProperties.setProperty("FSF_SSOCP_Chcb_CommentText", Constants.FSF_SSOCP_CHCB_COMMENT_TEXT);
		languageCzProperties.setProperty("FSF_SSOCP_Chcb_CommentEdgeText", Constants.FSF_SSOCP_CHCB_COMMENT_EDGE_TEXT);
		languageCzProperties.setProperty("FSF_SSOCP_Chcb_ExtendsText", Constants.FSF_SSOCP_CHCB_EXTENDS_TEXT);
		languageCzProperties.setProperty("FSF_SSOCP_Chcb_ImplementsText", Constants.FSF_SSOCP_CHCB_IMPLEMENTS_TEXT);
		languageCzProperties.setProperty("FSF_SSOCP_Chcb_AssociationText", Constants.FSF_SSOCP_CHCB_ASSOCIATION_TEXT);
		languageCzProperties.setProperty("FSF_SSOCP_Chcb_Aggregation_1_1_Text", Constants.FSF_SSOCP_CHCB_AGGREGATION_1_1_TEXT);
		languageCzProperties.setProperty("FSF_SSOCP_Chcb_Aggregation_1_N_Text", Constants.FSF_SSOCP_CHCB_AGGREGATION_1_N_TEXT);
		languageCzProperties.setProperty("FSF_SSOCP_Chcb_Class_TT", Constants.FSF_SSOCP_CHCB_CLASS_TT);
		languageCzProperties.setProperty("FSF_SSOCP_Chcb_Comment_TT", Constants.FSF_SSOCP_CHCB_COMMENT_TT);
		languageCzProperties.setProperty("FSF_SSOCP_Chcb_CommentEdge_TT", Constants.FSF_SSOCP_CHCB_COMMENT_EDGE_TT);
		languageCzProperties.setProperty("FSF_SSOCP_Chcb_Extends_TT", Constants.FSF_SSOCP_CHCB_EXTENDS_TT);
		languageCzProperties.setProperty("FSF_SSOCP_Chcb_Implements_TT", Constants.FSF_SSOCP_CHCB_IMPLEMENTS_TT);
		languageCzProperties.setProperty("FSF_SSOCP_Chcb_Association_TT", Constants.FSF_SSOCP_CHCB_ASSOCIATION_TT);
		languageCzProperties.setProperty("FSF_SSOCP_Chcb_Aggregation_1_1_TT", Constants.FSF_SSOCP_CHCB_AGGREGATION_1_1_TT);
		languageCzProperties.setProperty("FSF_SSOCP_Chcb_Aggregation_1_N_TT", Constants.FSF_SSOCP_CHCB_AGGREGATION_1_N_TT);
		languageCzProperties.setProperty("FSF_SSOCP_BtnSelectAll", Constants.FSF_SSOCP_BTN_SELECT_ALL);
		languageCzProperties.setProperty("FSF_SSOCP_BtnDeselectAll", Constants.FSF_SSOCP_BTN_DESELECT_ALL);
		
		
		// Texty do panelu, který obsahuje komponenty pro označení objektů v diagramu instancí, kterým se má nastavit velikost písma:
		ConfigurationFiles.commentsMap.put("FSF_SSOIP_BorderTitle", "Texty do panelu, který obsahuje komponenty pro označení objektů v diagramu instancí, kterým se má nastavit velikost písma. Tento panel se nachází v panelu pro nastavení velikosti písma tak, že si uživatel označí objekty, kterým chce nastavit velikost a následně ji pomocí posuvníku nastaví. Tyto panely se nachází v dialogu pro 'rychlé' nastavení velkosti písma objektům v diagramu tříd a v diagramu instancí.");
		languageCzProperties.setProperty("FSF_SSOIP_BorderTitle", Constants.FSF_SSOIP_BORDER_TITLE);
		languageCzProperties.setProperty("FSF_SSOIP_Chcb_Instance_Text", Constants.FSF_SSOIP_CHCB_INSTANCE_TEXT);		
		languageCzProperties.setProperty("FSF_SSOIP_Chcb_InstanceAttributes_Text", Constants.FSF_SSOIP_CHCB_INSTANCE_ATTRIBUTES_TEXT);
		languageCzProperties.setProperty("FSF_SSOIP_Chcb_InstanceMethods_Text", Constants.FSF_SSOIP_CHCB_INSTANCE_METHODS_TEXT);		
		languageCzProperties.setProperty("FSF_SSOIP_Chcb_Association_Text", Constants.FSF_SSOIP_CHCB_ASSOCIATION_TEXT);
		languageCzProperties.setProperty("FSF_SSOIP_Chcb_Aggregation_Text", Constants.FSF_SSOIP_CHCB_AGGREGATION_TEXT);
		languageCzProperties.setProperty("FSF_SSOIP_Chcb_Instance_TT", Constants.FSF_SSOIP_CHCB_INSTANCE_TT);
		languageCzProperties.setProperty("FSF_SSOIP_Chcb_InstanceAttributes_TT", Constants.FSF_SSOIP_CHCB_INSTANCE_ATTRIBUTES_TT);
		languageCzProperties.setProperty("FSF_SSOIP_Chcb_InstanceMethods_TT", Constants.FSF_SSOIP_CHCB_INSTANCE_METHODS_TT);		
		languageCzProperties.setProperty("FSF_SSOIP_Chcb_Association_TT", Constants.FSF_SSOIP_CHCB_ASSOCIATION_TT);
		languageCzProperties.setProperty("FSF_SSOIP_Chcb_Aggregation_TT", Constants.FSF_SSOIP_CHCB_AGGREGATION_TT);
		languageCzProperties.setProperty("FSF_SSOIP_BtnSelectAll", Constants.FSF_SSOIP_BTN_SELECT_ALL);
		languageCzProperties.setProperty("FSF_SSOIP_DeselectAll", Constants.FSF_SSOIP_BTN_DELECT_ALL);
		
		
		ConfigurationFiles.commentsMap.put("FSF_CDID_TXT_JopSameEdgesErrorText", "Texty do chybových hlášek, které mohou nastat, když se po nastavení nějaké velikosti písem / písma hledají duplicitní hodnoty, například duplicitní vlastnosti pro buňky reprezentující třídu a komentář, nebo nějaké hrany apod.");
		languageCzProperties.setProperty("FSF_CDID_TXT_JopSameEdgesErrorText", Constants.FSF_CDID_TXT_JOP_SAME_EDGES_ERROR_TEXT);
		languageCzProperties.setProperty("FSF_CDID_TXT_JopSameEdgesErrorTitle", Constants.FSF_CDID_TXT_JOP_SAME_EDGES_ERROR_TITLE);
		languageCzProperties.setProperty("FSF_CDID_TXT_Aggregation_1_1", Constants.FSF_CDID_TXT_AGGREGATION_1_1);
		languageCzProperties.setProperty("FSF_CDID_TXT_Aggregation_1_N", Constants.FSF_CDID_TXT_AGGREGATION_1_N);
		languageCzProperties.setProperty("FSF_CDID_TXT_Association", Constants.FSF_CDID_TXT_ASSOCIATION);
		languageCzProperties.setProperty("FSF_CDID_TXT_CommentEdge", Constants.FSF_CDID_TXT_COMMENT_EDGE);
		languageCzProperties.setProperty("FSF_CDID_TXT_Inheritance", Constants.FSF_CDID_TXT_INHERITANCE);
		languageCzProperties.setProperty("FSF_CDID_TXT_Unknown", Constants.FSF_CDID_TXT_UNKNOWN);
		
		
		
		ConfigurationFiles.commentsMap.put("FSF_FSPA_Txt_Jop_SameCellsText", "Texty, pro chybové hlášky, které mohou nastat, pokud uživatel nastaví velikost písma v nějakém / nějakých objektech v diagramu tříd nebo v diagramu instancí a příslušné objekty budou mít stejně vlastnosti. Například pokud bude buňka reprezentující komentář identická s buňkou reprezentující třídu v diagramu tříd s výjimkou velikosti písma, tak pokud uživatel nastaví velikost písma na stejné hodnoty, pak by v diagramu byly duplicity a nemusely by se rozeznat jednolitvé objekty, tak je třeba upozornit uživatele a nastavit výchozí hodnoty.");
		// 	// Duplicita bunky a komentare v diagramu tříd:
		languageCzProperties.setProperty("FSF_FSPA_Txt_Jop_SameCellsText", Constants.FSF_FSPA_TXT_JOP_SAME_CELLS_TEXT);
		languageCzProperties.setProperty("FSF_FSPA_Txt_Jop_SameCellsTitle", Constants.FSF_FSPA_TXT_JOP_SAME_CELLS_TITLE);	
		
		// Texty pro duplicitní hrany v diagramu tříd:
		languageCzProperties.setProperty("FSF_FSPA_Txt_Jop_SameEdgesText_1", Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_1);
		languageCzProperties.setProperty("FSF_FSPA_Txt_Jop_SameEdgesText_2", Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_2);		
		languageCzProperties.setProperty("FSF_FSPA_Txt_Jop_SameEdgesText_3", Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_3);
		languageCzProperties.setProperty("FSF_FSPA_Txt_Jop_SameEdgesText_4", Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_4);		
		languageCzProperties.setProperty("FSF_FSPA_Txt_Jop_SameEdgesText_5", Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_5);
		languageCzProperties.setProperty("FSF_FSPA_Txt_Jop_SameEdgesText_6", Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_6);		
		languageCzProperties.setProperty("FSF_FSPA_Txt_Jop_SameEdgesTitle", Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TITLE);
		
		// Texty pro duplicitní hrany v diagramu instancí:
		languageCzProperties.setProperty("FSF_FSPA_Txt_Jop_SameEdgesInIdText_1", Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_IN_ID_TEXT_1);
		languageCzProperties.setProperty("FSF_FSPA_Txt_Jop_SameEdgesInIdText_2", Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_IN_ID_TEXT_2);
		languageCzProperties.setProperty("FSF_FSPA_Txt_Jop_SameEdgesInIdText_3", Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_IN_ID_TEXT_3);
		languageCzProperties.setProperty("FSF_FSPA_Txt_Jop_SameEdgesInIdTitle", Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_IN_ID_TITLE);
		
		
		
		
		
		
		ConfigurationFiles.commentsMap.put("SIFCR_DialogTitle", "Texty do dialogu, který slouží k tomu, aby uživatel označil instance, na které chce vytvořit referenci. Tento dialog je možné zobrazit po kliknutí na tlačítko 'Vytvořit referenci' v dialogu prohlížení instance, když má označeou položku typu pole nebo list, který obsahuje alespoň dvě instance tříd z diagramu tříd, které nejsou reprezentovány v diagramu instancí.");
		languageCzProperties.setProperty("SIFCR_DialogTitle", Constants.SIFCR_DIALOG_TITLE);
		languageCzProperties.setProperty("SIFCR_Lbl_Info", Constants.SIFCR_LBL_INFO);
		languageCzProperties.setProperty("SIFCR_Btn_OK", Constants.SIFCR_BTN_OK);
		languageCzProperties.setProperty("SIFCR_Btn_Cancel", Constants.SIFCR_BTN_CANCEL);
		languageCzProperties.setProperty("SIFCR_Btn_SelectAll", Constants.SIFCR_BTN_SELECT_ALL);
		languageCzProperties.setProperty("SIFCR_Btn_DeselectAll", Constants.SIFCR_BTN_DESELECT_ALL);
		
		
		
		
		
		
		
		
		
		// Texty do třídy InstanceCell v balíčku instanceDiagram. Jedná se pouze
		// otexty potřebné pro vypsání informací o refernční proměnné, nebo o tom, žese
		// v příslušné instanci nenachází atributy nebo metody.
		ConfigurationFiles.commentsMap.put("MIS_Txt_ReferenceVar", "Texty do buňky, které reprezentují instanci třídy v diagramu instancí. Tyto texty jsou potřeba pouze v případě, že se mají zobrazovat proměnné a nebo metody v příslušné buňce / instanci. Jedná se o texty, které v případě potřeby informují uživatele, že se v příslušné instanci (třídě) nenachází žádná proměnná nebo metoda, nebo informace o refernční proměnné (referenci na instanci v diagramu instancí - v naplněné proměnné).");
		languageCzProperties.setProperty("MIS_Txt_ReferenceVar", Constants.MIS_TXT_REFERENCE_VAR);
		languageCzProperties.setProperty("MIS_Txt_NoVariables", Constants.MIS_TXT_NO_VARIABLES);
		languageCzProperties.setProperty("MIS_Txt_NoMethods", Constants.MIS_TXT_NO_METHODS);
		
		
		
		
		
		
		
		
		
		
		
		
		// Texty do třídy PopupMenu v balíčku clearEditor, jedná se pouze o texty do té
		// jediné položky pro vymazání textu z editoru.
		ConfigurationFiles.commentsMap.put("CE_PM_ItemClearText", "Texty pro položku 'Vymazat', která se nachází v kontextovém menu v balíčku clearEditor. Tato položka slouží pro vymazání veškerého obsahuje / textu v příslušném editoru.");
		languageCzProperties.setProperty("CE_PM_ItemClearText", Constants.CE_PM_ITEM_CLEAR_TEXT);
		languageCzProperties.setProperty("CE_PM_ItemClearTT", Constants.CE_PM_ITEM_CLEAR_TT);
		
		

		
		
		
		
		
		ConfigurationFiles.commentsMap.put("EI_Txt_AllowedValues", "Texty pro chybová hlášení, která mohou nastat v případě, že užival zadá úmyslně chybné hodnoty do nějakého konfiguračního souboru, tyto texty mohou být vypsány do příslušného dialogu s hlášením, že uživatel například zadal chybné hodnoty, něbo jsou nějaké duplicity hran, nebo komentáře a třídy apod.");
		languageCzProperties.setProperty("EI_Txt_AllowedValues", Constants.EI_TXT_ALLOWED_VALUES);
		languageCzProperties.setProperty("EI_Txt_MissingKey", Constants.EI_TXT_MISSING_KEY);
		languageCzProperties.setProperty("EI_Txt_CellsDuplicateError", Constants.EI_TXT_CELLS_DUPLICATE_ERROR);
		languageCzProperties.setProperty("EI_Txt_CdEdgesError", Constants.EI_TXT_CD_EDGES_ERROR);
		languageCzProperties.setProperty("EI_Txt_IdEdgesError", Constants.EI_TXT_ID_EDGES_ERROR);
		languageCzProperties.setProperty("EI_Txt_InvalidValues", Constants.EI_TXT_INVALID_VALUE);
		
		
		
		
		
		
		
		
		
		
		// Texty do třídy / dialogu ErrorInConfigFilesForm, jedná se o dialog, který
		// slouží pro zobrazení chyb po uložení změn v nějakém konfiguračním souboru,
		// který bude obsahovat chybné hodnoty nebo duplicity.
		ConfigurationFiles.commentsMap.put("EICFF_DialogTitle", "Texty do dialogu pro přehled chyb, který se zobrazí v případě, že běží aplikace a uživatel provede nějaké změny v konfiguračním souboru a ty změny nebudou validní, pak se zobrazí příslušný dialog, který bude obsahovat přehled chyb a informaci o tom, že se do příslušného souboru zapsalo výchozí nastavení.");
		languageCzProperties.setProperty("EICFF_DialogTitle", Constants.EICFF_DIALOG_TITLE);
		languageCzProperties.setProperty("EICFF_TxtFile", Constants.EICFF_TXT_FILE);
		languageCzProperties.setProperty("EICFF_TxtInfoRest", Constants.EICFF_TXT_INFO_REST);
		languageCzProperties.setProperty("EICFF_JspErrorListBorderTitle", Constants.EICFF_JSP_ERROR_LIST_BORDER_TITLE);
		languageCzProperties.setProperty("EICFF_JspDuplicatesListBorderTitle", Constants.EICFF_JSP_DUPLICATES_LIST_BORDER_TITLE);
		languageCzProperties.setProperty("EICFF_ChcbWrapText", Constants.EICFF_CHCB_WRAP_TEXT);
		languageCzProperties.setProperty("EICFF_BtnOk", Constants.EICFF_BTN_OK);








        // Texty do třídy: cz.uhk.fim.fimj.app.RestartApi
        // Jedná se o texty do dialogů, které informaují uživatele, že z nějakého důvodu nelze restartovat aplikaci apod. (pouze pro linux)
        ConfigurationFiles.commentsMap.put("RA_DirsWereNotFoundTitle", "Texty do třídy pro restartování aplikace. Jedná se o texty do dialogových okne - chybových hlášení, která značí, že došlo k nějaké chybě a z toho důvodu nelze restartovat aplikaci. Texty, resp. ty dialogy v aplikaci mohou nastat pouze v případě, že se jedná o pokus restartovat aplikaci na OS Linux.");
        languageCzProperties.setProperty("RA_DirsWereNotFoundTitle", Constants.RA_TXT_DIRS_WERE_NOT_FOUND_TITLE);
        languageCzProperties.setProperty("RA_DirsWereNotFoundTextPartOne", Constants.RA_TXT_DIRS_WERE_NOT_FOUND_TEXT_PART_ONE);
        languageCzProperties.setProperty("RA_DirsWereNotFoundTextPartTwo", Constants.RA_TXT_DIRS_WERE_NOT_FOUND_TEXT_PART_TWO);

        languageCzProperties.setProperty("RA_RestartIsNotSupportedTitle", Constants.RA_TXT_RESTART_IS_NOT_SUPPORTED_TITLE);
        languageCzProperties.setProperty("RA_RestartIsNotSupportedText", Constants.RA_TXT_RESTART_IS_NOT_SUPPORTED_TEXT);

        languageCzProperties.setProperty("RA_CannotCreateLaunchScriptTitle", Constants.RA_TXT_CANNOT_CREATE_LAUNCH_SCRIPT_TITLE);
        languageCzProperties.setProperty("RA_CannotCreateLaunchScriptText", Constants.RA_TXT_CANNOT_CREATE_LAUNCH_SCRIPT_TEXT);
        languageCzProperties.setProperty("RA_Script", Constants.RA_TXT_SCRIPT);





        // Texty pro chybovou hlášku o tom, že se nepodařilo smazat spouštěcí skript Viz třída: cz.uhk.fim.fimj.app.DeleteLaunchScriptThread
        ConfigurationFiles.commentsMap.put("DLST_DeleteScriptFailedText", "Texty pro chybovou hlášku o tom, že se nepodařilo smazat spouštěcí skript, který byl vygenerován, ale nesmazán při restartování aplikace (na OS Linux). Hláška může nastat v případě, že se po spuštění aplikace nepodaří smazat existující spouštěcí skript, který se nesmazal při restartování apllikace.");
        languageCzProperties.setProperty("DLST_DeleteScriptFailedText", Constants.DLST_TXT_DELETE_SCRIPT_FAILED_TEXT);
        languageCzProperties.setProperty("DLST_DeleteScriptFailedTitle", Constants.DLST_TXT_DELETE_SCRIPT_FAILED_TITLE);
        languageCzProperties.setProperty("DLST_Script", Constants.DLST_TXT_SCRIPT);





        // Texty do třídy DesktopSupport:
        ConfigurationFiles.commentsMap.put("DS_Txt_ErrorInfoText", "Texty pro chybové hlášky do třídy: cz.uhk.fim.fimj.file.DesktopSupport. Jedná se například o chybové hlášky ve smyslu, že se nepodařilo nalézt nějaký soubor, že na používané platformě není podporována akce například pro otevření prohlížeče nebo průzkumníku souborů apod.");
        // Texty ohledně podpory třídy Desktop:
        languageCzProperties.setProperty("DS_Txt_ErrorInfoText", Constants.DS_TXT_ERROR_INFO_TEXT);
        languageCzProperties.setProperty("DS_Txt_ErrorInfoTitle", Constants.DS_TXT_ERROR_INFO_TITLE);
        languageCzProperties.setProperty("DS_Txt_OpenActionIsNotSupportedTitle", Constants.DS_TXT_OPEN_ACTION_IS_NOT_SUPPORTED_TITLE);
        /*
         * Texty chybové hlášky při pokusu oo otevření souboru nebo adresáře v průzkumníku souborů v OS nebo ve výchozí aplikaci pro otevření příslušného typu souboru:
         */
        languageCzProperties.setProperty("DS_Txt_FileDoNotExist_Text", Constants.DS_TXT_FILE_DO_NOT_EXIST_TEXT);
        languageCzProperties.setProperty("DS_Txt_FileDoNotExist_Title", Constants.DS_TXT_FILE_DO_NOT_EXIST_TITLE);
        languageCzProperties.setProperty("DS_Txt_Path", Constants.DS_TXT_PATH);
        languageCzProperties.setProperty("DS_Txt_ParentDirWasNotFound_Text", Constants.DS_TXT_PARENT_DIR_WAS_NOT_FOUND_TEXT);
        languageCzProperties.setProperty("DS_Txt_ParentDirWasNotFound_Title", Constants.DS_TXT_PARENT_DIR_WAS_NOT_FOUND_TITLE);
        /*
         * Texty pro chybové hlášky, že akce pro otevření souboru nebo adresáře není na používané platformě podporováno za účelem otevření souboru nebo adresáře v průzkumníku projektů nebo ve výchozí aplikaci pro otevření příslušného typu souboru:
         */
        languageCzProperties.setProperty("DS_Txt_OpenFileActionIsNotSupported_Text", Constants.DS_TXT_OPEN_FILE_ACTION_IS_NOT_SUPPORTED_TEXT);
        languageCzProperties.setProperty("DS_Txt_OpenFileActionIsNotSupported_Text2", Constants.DS_TXT_OPEN_FILE_ACTION_IS_NOT_SUPPORTED_TEXT2);
        languageCzProperties.setProperty("DS_Txt_OpenDirActionIsNotSupported_Text", Constants.DS_TXT_OPEN_DIR_ACTION_IS_NOT_SUPPORTED_TEXT);
        languageCzProperties.setProperty("DS_Txt_OpenDirActionIsNotSupported_Text2", Constants.DS_TXT_OPEN_DIR_ACTION_IS_NOT_SUPPORTED_TEXT2);
        /*
         * Texty pro chybové hlášky, že třída Desktop není na používané platformě podporována za účelem otevření souboru nebo adresáře v průzkumníku projektů nebo ve výchozí aplikaci pro otevření příslušného typu souboru.
         */
        languageCzProperties.setProperty("DS_Txt_FileCannotBeOpened", Constants.DS_TXT_FILE_CANNOT_BE_OPENED);
        languageCzProperties.setProperty("DS_Txt_DirCannotBeOpened", Constants.DS_TXT_DIR_CANNOT_BE_OPENED);
        /*
         * Text pro chybovou hlášku o tom, že třída Desktop není podporována na používané platformě za účelem odeslání emailu, resp. otevření emailového klienta:
         */
        languageCzProperties.setProperty("DS_Txt_EmailCannotBeSent", Constants.DS_TXT_EMAIL_CANNOT_BE_SENT);
        /*
         * Text pro chybovou hlášku o tom, že akce pro otevření emailového klienta (/ zaslání emailu) není na používané platformě podporována.
         */
        languageCzProperties.setProperty("DS_Txt_EmailActionIsNotSupported", Constants.DS_TXT_EMAIL_ACTION_IS_NOT_SUPPORTED);
        /*
         * Text pro chybovou hlášku o tom, že třída Desktop není na používané platformě podporována za účelem otevření výchozího prohlížeče a v něm příslušnou webovou stránku.
         */
        languageCzProperties.setProperty("DS_Txt_BrowserCannotBeOpened", Constants.DS_TXT_BROWSER_CANNOT_BE_OPENED);
        /*
         * Texty pro chybovou hlášku o tom, že akce pro otevření výchozího prohlížeče a v něm příslušnou webovou stránku není na používané platformě podporována.
         */
        languageCzProperties.setProperty("DS_Txt_BrowseActionIsNotSupported", Constants.DS_TXT_BROWSE_ACTION_IS_NOT_SUPPORTED);



		

		
		
		return languageCzProperties;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří objekt Properties, a vloží do něj veškeré texty pro
	 * tuto aplikaci v jazyce anglickém.
	 * 
	 * @param keysOrder
	 *            - Způsob řazení klíčů - celkově textů v souboru .properties dle
	 *            klíčů.
	 * 
	 * @return výše popsaný objekt Properties, který obsahuje veškeré texty pro tuto
	 *         aplikaci v anglickém jazyce a s příslušným uspořádáním (keysOrder).
	 */
	public static Properties getLanguage_EN(final KeysOrder keysOrder) {
		ConfigurationFiles.commentsMap.clear();
		
//		final Properties languageEnProp = new Properties();
		final SortedProperties languageEnProp = new SortedProperties(keysOrder);
		
		// Doplnění textů:
		
		
		
		
		
		// Texty pro tlačítka typu YES_OPTION a NO_OPTION v dialozích JOptionPane:
		ConfigurationFiles.commentsMap.put("JopTxtYesButton", "Text for 'YES_OPTION' and 'NO_OPTION' button, located in JOptionPane dialogs. These are only buttons for confirmation or rejection, other options are not found in the application.");
		languageEnProp.setProperty("JopTxtYesButton", "Yes");
		languageEnProp.setProperty("JopTxtNoButton", "No");
		
		
		
		
		
		
		
		// Texty do Ohraničení class diagramu a Instance diagramu:
		ConfigurationFiles.commentsMap.put("CdBorderTitle", "Caption of class diagram Bending");
		languageEnProp.setProperty("CdBorderTitle", "Class diagram");
		ConfigurationFiles.commentsMap.put("IdBorderTitle", "Caption of instance diagram Bending");
		languageEnProp.setProperty("IdBorderTitle", "Instances diagram");

		
		
		
		
		
		
		// Texty do třídy WorkspaceForm:
		ConfigurationFiles.commentsMap.put("Wf_BtnOk", "Texts in the dialog for selecting a directory labeled as a workspace.");
		languageEnProp.setProperty("Wf_BtnOk", "OK");
		languageEnProp.setProperty("Wf_Btn_Cancel", "Cancel");
		languageEnProp.setProperty("Wf_BtnBrowse", "Browse");
		languageEnProp.setProperty("Wf_Title", "Workspace");
		languageEnProp.setProperty("Wf_Lbl_Select_Workspace", "Select the workspace");
		languageEnProp.setProperty("Wf_Lbl_Info", "The application stores your projects in a folder called Workspace.");
		languageEnProp.setProperty("Wf_Lbl_Choose", "Select the folder location workspace for this session.");
		languageEnProp.setProperty("Wf_Lbl_Workspace", "Workspace");
		languageEnProp.setProperty("Wf_Chcb_Remember_Path", "Use this as the default path and not ask again.");
		languageEnProp.setProperty("Wf_Workspace_Path_Empty_Title", "Path unlisted");
		languageEnProp.setProperty("Wf_Workspace_Path_Empty_Text", "Workspace path to the folder (the workspace) is not listed!");
		languageEnProp.setProperty("Wf_Workspace_Path_Format_Title", "Wrong path");
		languageEnProp.setProperty("Wf_Workspace_Path_Format_Text", "Workspace path to the folder (the workspace) is in wrong format!");
				
				
		
		
		
		
				// Texty tlačítek do třídy Menu v balíčku jMenu:
		ConfigurationFiles.commentsMap.put("MnTxtProject", "Text items in the menu in the main application window.");
		languageEnProp.setProperty("MnTxtProject", "Project");
		languageEnProp.setProperty("MnTxtEdit", "Edit");
		languageEnProp.setProperty("MnTxtTools", "Tools");
		languageEnProp.setProperty("MnTxtHelp", "Help");
		languageEnProp.setProperty("MnTxtNewProject", "New");
		languageEnProp.setProperty("MnTxtOpenProject", "Open");
		languageEnProp.setProperty("MnTxtRecentlyOpenedProjects", "Recently open");
        languageEnProp.setProperty("MnTxtDeleteRecentlyOpenedProjects", "Clear history");
        languageEnProp.setProperty("MnTxtRb_RecentlyOpenedProjectsAsc", "Ascending order");
        languageEnProp.setProperty("MnTxtRb_RecentlyOpenedProjectsDes", "Descending order");
		languageEnProp.setProperty("MnTxtCloseProject", "Close");
		languageEnProp.setProperty("MnTxtSave", "Save");
		languageEnProp.setProperty("MnTxtSaveAs", "Save as");
		languageEnProp.setProperty("MnTxtSwitchWorkspace", "Change Workspace");
		languageEnProp.setProperty("MnTxtRestartApp", "Restart");
		languageEnProp.setProperty("MnTxtExit", "Exit");
		languageEnProp.setProperty("MnTxtAddClassFromFile", "Add class from file");
		languageEnProp.setProperty("MnTxtRemove", "Remove");
		languageEnProp.setProperty("MnTxtSaveImageOfClassDiagram", "Save image class diagram");
		languageEnProp.setProperty("MnTxtSaveImageOfInstanceDiagram", "Save image instances diagram");
		languageEnProp.setProperty("MnTxtPrintImageOfClassDiagram", "Print image class diagram");
		languageEnProp.setProperty("MnTxtPrintImageOfInstanceDiagram", "Print image instances diagram");
		languageEnProp.setProperty("MnTxtRefreshClassDiagram", "Update class diagram");
		languageEnProp.setProperty("MnTxtRefreshInstanceDiagram", "Update instances diagram");
		languageEnProp.setProperty("MnTxtRemoveAll", "Remove all");
		languageEnProp.setProperty("MnTxtRemoveInstanceDiagram", "Remove instances");
		languageEnProp.setProperty("MnTxtCompile", "Compile");
		languageEnProp.setProperty("MnTxtOpenClass", "In code editor");		
		languageEnProp.setProperty("MnTxtOpenInEditor", "Open file");
		languageEnProp.setProperty("MnTxtOpenClassInProjectExplorer", "In a project explorer");
		languageEnProp.setProperty("MnTxtShowOutputTerminal", "Output terminal");
		languageEnProp.setProperty("MnTxtStart_CallTheMainMethod", "Start");
		languageEnProp.setProperty("MnTxtSettings", "Settings");
		languageEnProp.setProperty("MnTxtFontSize", "Font size");
		languageEnProp.setProperty("MnTxtJava", "Java");
		languageEnProp.setProperty("MnTxtJavaSettings", "Settings");
		languageEnProp.setProperty("MnTxtJavaSearch", "Search");
		languageEnProp.setProperty("MnTxtProjectExplorer", "Project explorer");
        languageEnProp.setProperty("MnTxtFileExplorer", "File explorer");
		languageEnProp.setProperty("MnTxtProjectOpenProject", "Open project");
		languageEnProp.setProperty("MnTxtProjectWorkspace", "Workspace directory");
        languageEnProp.setProperty("MnTxtFileExplorerWorkspace", "Workspace directory");
        languageEnProp.setProperty("MnTxtFileExplorerOpenProject", "Open project");
		languageEnProp.setProperty("MnTxtInfo", "Info");
		languageEnProp.setProperty("MnTxtAboutApp", "About application");

		
		
		// Popisky k jednotlivým tlačítkům v menu: - Tool tip pro jednotlvé položky v menu:
		ConfigurationFiles.commentsMap.put("MnTtNewProject", "Labels for menu items in the main application window.");
		languageEnProp.setProperty("MnTtNewProject", "Opens dialog box for creating a new project.");
		languageEnProp.setProperty("MnTtOpenProject", "A dialog box for selecting the project to open.");
        languageEnProp.setProperty("MnTtDeleteRecentlyOpenedProjects", "Erases the history of previously open projects.");
        languageEnProp.setProperty("MnTtRb_SortRecentlyOpenedProjectsAsc", "Sorts a list of available previously open projects in ascending order by date of opening.");
        languageEnProp.setProperty("MnTtRb_SortRecentlyOpenedProjectsDes", "Sorts a list of available previously open projects in descending order by date of opening.");
		languageEnProp.setProperty("MnTtCloseProject", "Closes the currently open project.");
		languageEnProp.setProperty("MnTtSave", "Saves the changes to the currently open project.");
		languageEnProp.setProperty("MnTtSaveAs", "This opens a dialog box to change the storage location is currently open project.");
		languageEnProp.setProperty("MnTtSwitchWorkspace", "Opens the New or New Selection Window. other folders marked as workspace.");
		languageEnProp.setProperty("MnTtRestartApp", "Restart the application (may not be supported by all JVM).");
		languageEnProp.setProperty("MnTtExit", "End the application.");
		languageEnProp.setProperty("MnTtAddClassFromFile", "A dialog box for selecting the Java classes to be loaded into the currently open project.");
		languageEnProp.setProperty("MnTtRemove", "Withdraw from class diagram of a selected object (only one selected object).");
		languageEnProp.setProperty("MnTtSaveImageOfClassDiagram", "This opens a dialog box for selecting a storage location print screen 'class diagram'.");
		languageEnProp.setProperty("MnTtSaveImageOfInstanceDiagram", "This opens a dialog box for selecting a storage location print screen 'chart instances'.");
		languageEnProp.setProperty("MnTtPrintImageOfClassDiagram", "This opens a dialog window to select properties (printer, number of copies, ...) for printing the print screen 'class diagram'.");
		languageEnProp.setProperty("MnTtPrintImageOfInstanceDiagram", "This opens a dialog window to select properties (printer, number of copies, ...) for printing the print screen 'chart instances'.");
		languageEnProp.setProperty("MnTtRefreshClassDiagram", "Updating the class diagram, ie. Src directory searches the project is open and placed in a class diagram class, which in this directory to find, and not in the diagram, then re-test the relationships between classes.");
		languageEnProp.setProperty("MnTtRefreshInstanceDiagram", "It updates a diagram of instances, ie. They are checked, all relations between the created instances.");
		languageEnProp.setProperty("MnTtRemoveAll", "All objects are collected in a class diagram. (Deletes the contents of the src directory in the project directory.)");
		languageEnProp.setProperty("MnTtRemoveInstanceDiagram", "All instance classes are deleted from the class diagram that was created and the instance diagrams are deleted.");
		languageEnProp.setProperty("MnTtCompile", "Compiles all the classes in the currently open project.");
		languageEnProp.setProperty("MnTtOpenClass", "Opens a dialog to select the text file or Java class that opens the source code editor.");
		languageEnProp.setProperty("MnTtOpenFileInProjectExplorer", "Opens a dialog box for selecting a text file or Java class that will be open in the project explorer dialog with the root directory in workspace.");
		languageEnProp.setProperty("MnTtShowOutputTerminal", "Show the window with the outputs from the source code (output terminal window).");
		languageEnProp.setProperty("MnTtStart_CallTheMainMethod", "Run the project. 'Are searched' source codes of classes in a class diagram in order to find a method main, if this method is started up, is called. Otherwise, the printed message could not find a method main. Note: If the project will have multiple classes with the appropriate method will be called only the first method is found, it can be avoided by right-clicking on the appropriate class in a class diagram with main method you want to run, and click Run. Instances of classes after calling this method will not be shown in the chart instances.");
		languageEnProp.setProperty("MnTtSettings", "The dialog box for editing application settings.");
		languageEnProp.setProperty("MnTtFontSize", "Opens a dialog for setting font size of objects in the class diagram and in the instance diagram.");
		languageEnProp.setProperty("MnTtJava", "A dialog box for setting the 'home' directory for Java and important files to compile.");
		languageEnProp.setProperty("MnTtJavaSearch", "Try to find the necessary files for compiling classes and copy it to the following path");
		languageEnProp.setProperty("MnTtProjectExplorerWorkspace", "This opens a dialog box to work with files in the directory selected as the Workspace.");
		languageEnProp.setProperty("MnTtProjectExplorerProject", "A dialog box for working with files in a directory in an open project opens.");
        languageEnProp.setProperty("MnTtFileExplorerWorkspace", "Opens the directory selected as Workspace in the OS file explorer.");
        languageEnProp.setProperty("MnTtFileExplorerOpenProject", "Opens the open project directory in the file explorer set in the OS.");
		languageEnProp.setProperty("MnTtInfo", "This opens a dialog box with information about the application.");
		languageEnProp.setProperty("MnTtAboutApp", "A dialog box with information about the author of the application.");

		// Texty do chybových hlášek v metodě removeselectedObject() v třídě
		// menu.java:
		ConfigurationFiles.commentsMap.put("MnJopAssociationErrorTitle", "Texts in error messages that may appear when you try to remove an object from a class diagram.");
//		languageEnProp.setProperty("MnJopAssociationErrorTitle", "Error association");
		languageEnProp.setProperty("MnJopAssociationErrorTitle", "Symmetric association error");		
//		languageEnProp.setProperty("MnJopAssociationErrorText_1", "The class designation (cell) is the source of the binding associations can not be deleted!");
		languageEnProp.setProperty("MnJopAssociationErrorText_1", "The designated class (cell) is the source of the symmetric association relationship, it can not be deleted!");
//		languageEnProp.setProperty("MnJopAssociationErrorText_2", "The class designation (cell) is the aim of the association ties, can not be deleted!");
		languageEnProp.setProperty("MnJopAssociationErrorText_2", "The designated class (cell) is the object of the symmetric association relationship, it can not be deleted!");
		languageEnProp.setProperty("MnJopInheritErrorTitle", "Error inheritance");
		languageEnProp.setProperty("MnJopInheritErrorText", "Of the identified class (cell) inherits a class, you can not delete it!");
		languageEnProp.setProperty("MnJopImplementsErrorTitle", "Error implementation");
		languageEnProp.setProperty("MnJopImplementsErrorText", "The class designation (cell) is the interface that a class implements, you can not delete it!");
		languageEnProp.setProperty("MnJopAggregation1_1_1_ErrorTitle", "Error aggregation");
//		languageEnProp.setProperty("MnJopAggregation1_1_1_ErrorText", "The class designation (cell) in relation 'aggregation 1: 1' s another class (cell) (a class has a reference to this class designation.) Can not be deleted!");
		languageEnProp.setProperty("MnJopAggregation1_1_1_ErrorText", "The designated class (cell) is in an 'asymmetric association' relationship with another class (cell) (some class has a reference to this flagged class.), It can not be deleted!");
		languageEnProp.setProperty("MnJopMnJopAggregation1_1_N_ErrorText", "The class designation (cell) in relation 'aggregation 1: N' s another class (cell) (a class has a reference to this class designation.) Can not be deleted!");
		languageEnProp.setProperty("MnJopSelectedObjectErrorTitle", "Error delete operation");
		languageEnProp.setProperty("MnJopSelectedObjectErrorText", "Object is not marked for deletion!");
		
		// Chybové hlášky po kliknutí na nějaké tlačítko v menu:
		ConfigurationFiles.commentsMap.put("MnJopMissingDirectoryErrorTitle", "Text for error messages that can occur when you click on any of the buttons in the menu in the main application window.");
		languageEnProp.setProperty("MnJopMissingDirectoryErrorTitle", "Path error");
		languageEnProp.setProperty("MnJopMissingDirectoryErrorText", "Directory not found! \nPath");
		languageEnProp.setProperty("MnJopIdenticalDirectoryErrorTitle", "Error location");
		languageEnProp.setProperty("MnJopIdenticalDirectoryErrorText", "Directories are identical, please choose another!");
		languageEnProp.setProperty("MnJopMissingClassFileTitle", "Missing file");
		languageEnProp.setProperty("MnJopMissingClassFileText", "The file does not exist!\nPath");
				
		// Texty pro kompilaci nebo pokusu o nalazeni a zavolani metody main v tride v diagramu tříd:
		ConfigurationFiles.commentsMap.put("MnTxtMissingSrcDirText", "Texts for error messages that may occur when trying to compile a class or try to launch an open project.");
		languageEnProp.setProperty("MnTxtMissingSrcDirText", "Src directory was not found or the bin directory in the project directory!");
		languageEnProp.setProperty("MnTxtMissingSrcDirTitle", "Directory not found");
		languageEnProp.setProperty("MnTxtErrorWhileCallTheMainMethod", "When calling the main error occurred!");
		languageEnProp.setProperty("MnTxtClass", "Class");
		languageEnProp.setProperty("MnTxtError", "Error");
		languageEnProp.setProperty("MnTxtMethodIsNotAccessible", "The method is not accessible!");
		languageEnProp.setProperty("MnTxtIllegalArgumentExceptionPossibleErrors", "The object is an instance of the class or interface. They differ in the number of parameters. Failed conversion parameters, and the like.");
		languageEnProp.setProperty("MnTxtErrorWhileCallingMainMethod", "The method raised an exception, unknown error.");
		languageEnProp.setProperty("MnTxtMainMethodNotFoundText", "Main method was not in any of the classes in a class diagram found!");
		languageEnProp.setProperty("MnTxtMainMethodNotFoundTitle", "Method not found");
		languageEnProp.setProperty("MnTxtChooseClassOrTextDocumentDialogTitle", "Select a Java class or a text document");

		ConfigurationFiles.commentsMap.put("Mn_TxtErrorWhilePrintingClassDiagramText", "Additional text for error messages that can occur when you click one of the menu items in the main application window.");
		languageEnProp.setProperty("Mn_TxtErrorWhilePrintingClassDiagramText", "An error occurred while trying to print a class diagram!");
		languageEnProp.setProperty("Mn_TxtErrorWhilePrintingClassDiagramTitle", "Printing Error");
		languageEnProp.setProperty("Mn_TxtFailedToPrintClassDiagramText", "Unable to print print screen class diagram!");
		languageEnProp.setProperty("Mn_TxtClassDiagramText", "Class diagram");
		languageEnProp.setProperty("Mn_TxtFailedToPrintClassDiagramTitle", "Printing error");
		languageEnProp.setProperty("Mn_TxtErrorWhilePrintingInstanceDiagramText", "An error occurred while trying to print a chart instances!");
		languageEnProp.setProperty("Mn_TxtErrorWhilePrintingInstanceDiagramTitle", "Printing error");
		languageEnProp.setProperty("Mn_TxtFailedToPrintInstanceDiagramText", "Unable to print print screen chart instances!");
		languageEnProp.setProperty("Mn_TxtInstanceDiagramText", "Instances diagram");
		languageEnProp.setProperty("Mn_TxtfailedToPrintInstanceDiagramTitle", "Printing error");
		languageEnProp.setProperty("Mn_TxtErrorWhileCopyFilesText", "An error occurred while copying files");
		languageEnProp.setProperty("Mn_TxtPossibleFileIsBeingUsed", "It is possible that the file on the destination already exists and is being used.");
		languageEnProp.setProperty("Mn_TxtSourceText", "Source");
		languageEnProp.setProperty("Mn_TxtDestinationText", "Target");
		languageEnProp.setProperty("Mn_TxtErrorWhileCopyFilesTitle", "Error while copying");

		// Pro ProjectExplorerDialog - texty v menu:
		ConfigurationFiles.commentsMap.put("Mn_TxtPathToWorkspaceDoNotFoundText", "Text for error messages that may occur when you try to open a Project Explorer dialog.");
		languageEnProp.setProperty("Mn_TxtPathToWorkspaceDoNotFoundText", "The path to the workspace directory was not found or the appropriate directory does not exist.");
		languageEnProp.setProperty("Mn_TxtPathToWorkspaceDoNotFoundTitle", "Workspace error");
		languageEnProp.setProperty("Mn_TxtDefaultPropInAppDoNotLoadText", "Failed to load the default file default.properties application directory!");
		languageEnProp.setProperty("Mn_TxtDefaultPropInAppDoNotLoadTitle", "Failed to load");

		// Men: chybové hlášky:
		ConfigurationFiles.commentsMap.put("Mn_Txt_OpenDirDoesNotExistText", "Text for error messages that may occur when you try to open an application project, it should be a project created by this application." );
		languageEnProp.setProperty("Mn_Txt_OpenDirDoesNotExistText", "Open directory project no longer exists!");
		languageEnProp.setProperty("Mn_Txt_OriginalLocation", "Original location");
		languageEnProp.setProperty("Mn_Txt_OpenDirDoesNotExistTitle", "Project directory does not exist");
		languageEnProp.setProperty("Mn_Txt_ProjectIsNotOpenText", "No project is open!");
		languageEnProp.setProperty("Mn_Txt_ProjectIsNotOpenTitle", "The project closed");
		
		ConfigurationFiles.commentsMap.put("Mn_Txt_ClassDiagramDoesntContainClassTitle", "Additional texts for compiling classes using the menu button in the main application window.");
		languageEnProp.setProperty("Mn_Txt_ClassDiagramDoesntContainClassTitle", "Missing class");
		languageEnProp.setProperty("Mn_Txt_ClassDiagramDoesntContainClassText", "The class diagram does not contain any class to compile!");
		
		// Dotaz na restart při změně workspace:
        ConfigurationFiles.commentsMap.put("Mn_Txt_SwitchWorkspaceJopRestartAppNow_Text", "Texts into a dialog box asking if the user wants to restart the application immediately.");
		languageEnProp.setProperty("Mn_Txt_SwitchWorkspaceJopRestartAppNow_Text", "You need to restart the application to make changes, do you want to restart the application now?");
		languageEnProp.setProperty("Mn_Txt_SwitchWorkspaceJopRestartAppNow_TT", "Restart application");

        // Hlášky pro nenalezené adresáře workspace nebo adresáře otevřeného projektu, když chce uživatel otevřít tyto adresáře v průzkumníku souborů:
        ConfigurationFiles.commentsMap.put("Mn_Txt_PathToWorkspaceWasNotFound_Text", "Texts in the notifications dialog box to the user that the workspace directory or open project directory was not found. This error can occur when you try to open these directories in the OS file explorer, but these directories do not exist.");
        languageEnProp.setProperty("Mn_Txt_PathToWorkspaceWasNotFound_Text", "Failed to find the path to workspace directory.");
        languageEnProp.setProperty("Mn_Txt_PathToWorkspaceWasNotFound_Title", "Workspace was not found");
        // Cesta k otevřenému projektu nebyla nalezena:
        languageEnProp.setProperty("Mn_Txt_PathToOpenProjectWasNotFound_Text", "Failed to find the path to an open project.");
        languageEnProp.setProperty("Mn_Txt_PathToOpenProjectWasNotFound_Title", "An open project was not found");
				
				
				
		
		
				
				
		// Třída ReadFile:
		
		// Třída ReadFile, metoda: getSelectLanguage(), text do výpisu o nenalezení požadovaného jazyka:
		ConfigurationFiles.commentsMap.put("JopErrorLanguageTitle", "Texts in ReadFile class error messages These error messages can occur when you try to read a .properties file with text for the application in any language.");
		languageEnProp.setProperty("JopErrorLanguageTitle", "Error language");
		languageEnProp.setProperty("JopErrorLanguage1", "File not found the required language");
		languageEnProp.setProperty("JopErrorLanguage2", "\nThe default application language was selected and all languages ​​were copied to a folder that was selected as Workspace!\nTo make changes, restart the application.");
		
		
		
		// Texty pro inforamci pro uživatele, že byly znovu nakopírovány soubory s texty pro tuto aplikaci do configuration ve worspace:
		ConfigurationFiles.commentsMap.put("JopErrorLoadLanguage_Title", "Texts in a notification message telling the user that the text files for this application were copied to the language in the configuration in the workspace. This notification can occur if the user deletes the required text file for this application before running the application and was so you need to re-paste them into the directory to reload them.");
		languageEnProp.setProperty("JopErrorLoadLanguage_Title", "Language error");
		languageEnProp.setProperty("JopErrorLoadLanguage_Text_1", "The first attempt to load a text file with the text for this application in the selected language failed, so the files");
		languageEnProp.setProperty("JopErrorLoadLanguage_Text_2", "with the texts for this application were copied to the next location and reloaded.");
		languageEnProp.setProperty("JopErrorLoadLanguage_Text_3", "Location");
		
		
		
		// Třída readfile, metoda getPathToSrcDirectory
		ConfigurationFiles.commentsMap.put("TacMissingProjectDirectoryText", "Text for error messages that may occur when you get a path to the bin or src directory in an open project." );
		languageEnProp.setProperty("TacMissingProjectDirectoryText", "Could not find a project folder, please open another project!\nPath");
		languageEnProp.setProperty("TacMissingProjectDirectoryTitle", "Error project");
		
		// Třída ReadFile, metode getPathToBinDirectory:		
		// Třída ReadFile, metoda: getProperties()
		ConfigurationFiles.commentsMap.put("JopMissingFileTitle", "Text for error messages that can occur when reading the .properties file - when read in the getFroperties method in the ReadFile class.");
		languageEnProp.setProperty("JopMissingFileTitle", "File error");
		languageEnProp.setProperty("JopMissingFileText", "File Not Found!\nPath");
				
				
		// Třída readFile, metoda getPathToBin:
		ConfigurationFiles.commentsMap.put("Rf_FailedToCreateBinDirText", "Additional text for error messages that can occur when getting a path to the bin directory in an open project, the ReadFile class, the getPathTobin method.");
		languageEnProp.setProperty("Rf_FailedToCreateBinDirText", "Failed to create bin directory in the directory open project!");
		languageEnProp.setProperty("Rf_FailedToCreateBinDirTitle", "Failed to create directory");
				
				
				
				
				
				
				
				
				
				
				
				
				
				
		// Třída Grah.java v balíčku classDiagram, texty do chybových hlášek:
		ConfigurationFiles.commentsMap.put("CellErrorTitle", "Texts for the class diagram These texts are used for reporting errors about incorrect relationships between classes, for example, if an error occurs when trying to create a relationship between classes, inheritance, etc. This may be, for example, a bad class or comment, etc.");
		languageEnProp.setProperty("CellErrorTitle", "Error class (cell)");
		languageEnProp.setProperty("CellErrorText", "The class designation (cell) can not be found!");
		languageEnProp.setProperty("CellErrorTitle2", "Error class (cell)");
		languageEnProp.setProperty("CellErrorText2", "Class (cell) not marked!");
		languageEnProp.setProperty("CellErrorTitle3", "Error classes (cells)");
		languageEnProp.setProperty("CellErrorText3", "Marked class (cell) or classes (cells) not found!");
		languageEnProp.setProperty("CellErrorTitle4", "Error class (cell)");
		languageEnProp.setProperty("CellErrorText4", "Classes (cells) are not marked!");
		languageEnProp.setProperty("CellErrorTitle5", "Error classes (cells)");
		languageEnProp.setProperty("CellErrorText5", "Marked class (cell) or classes (cells) not found!");
		languageEnProp.setProperty("CellErrorTitle6", "Error class (cell)");
		languageEnProp.setProperty("CellErrorText6", "Classes (cells) are not marked!");
		languageEnProp.setProperty("CellErrorTitle7", "Error classes (cells)");
		languageEnProp.setProperty("CellErrorText7", "Marked class (cell) or classes (cells) not found!");
		languageEnProp.setProperty("CellErrorTitle8", "Error class (cell)");
		languageEnProp.setProperty("CellErrorText8", "Classes (cells) are not marked!");
		languageEnProp.setProperty("CellErrorTitle9", "Error class (cell)");
		languageEnProp.setProperty("CellErrorText9", "Marked class / classes (cells) not found!");
		languageEnProp.setProperty("CellErrorTitle10", "Error class (cell)");
		languageEnProp.setProperty("CellErrorText10", "At least one tagged object is not a class (cell)!");
		languageEnProp.setProperty("Cd_EdgeErrorInAssociateWithOneClassTitle", "Failure relationship");
		languageEnProp.setProperty("Cd_EdgeErrorInAssociateWithOneClassText", "Unable to create a relationship between one class!");
		languageEnProp.setProperty("Cd_ErrorMissingObjectInGraphTitle", "Object not found");
		languageEnProp.setProperty("Cd_ErrorMissingObjectInGraphText", "Object was not found!");
		
		// Texty pro implementaci rozhraní - chyby:
		languageEnProp.setProperty("Cd_Txt_ErrorWhileWritingInterfaceMethodsToClass_Text", "Could not write methods from interface to class");
		languageEnProp.setProperty("Cd_Txt_ErrorWhileWritingInterfaceMethodsToClass_TT", "Write error");
		languageEnProp.setProperty("Cd_Txt_Class", "Class");
		languageEnProp.setProperty("Cd_Txt_AlreadyImplementsInterface", "already implements interface");
		languageEnProp.setProperty("Cd_Txt_AlreadyImplementsInterface_TT", "Interface implemented");
				
				
				
		// Jazyk pro tlačítka v třídě ButtonsPanel, v balíčku: buttonsPanel:
		ConfigurationFiles.commentsMap.put("ButtonNewClass", "Button texts located in the toolbar on the left side of the main application window are buttons for creating a class, relationships between them and comments.");
		languageEnProp.setProperty("ButtonNewClass", "Create class");
//		languageEnProp.setProperty("ButtonAddAssociation", "Association");
		languageEnProp.setProperty("ButtonAddAssociation", "Symmetric association");
		languageEnProp.setProperty("ButtonAddExtends", "Extends");
		languageEnProp.setProperty("ButtonAddImplements", "Implementation");
		languageEnProp.setProperty("ButtonComment", "Comment");
//		languageEnProp.setProperty("ButtonAggregation_1_ku_1", "Aggregation 1 : 1");
		languageEnProp.setProperty("ButtonAggregation_1_ku_1", "Asymmetric association");
		languageEnProp.setProperty("ButtonAggregation_1_ku_N", "Aggregation 1 : N");
		
		// Popisky pro tlačítka v ButtonsPanelu:
		ConfigurationFiles.commentsMap.put("ButtonsNewClassToolTip", "Button labels located on the toolbar on the left side of the main window of the application.This labels are displayed after you move the mouse cursor to these buttons, these are buttons for creating a class, relationships between them and comments.");
		languageEnProp.setProperty("ButtonsNewClassToolTip", "When you click the button, a dialog opens to enter the name of the new class and the type of this new class.");
		languageEnProp.setProperty("ButtonAddAssociationToolTip", "After clicking on this button, you have to mark two classes in the class diagram (by clicking on them with the left mouse button). Both classes will know each other, i.e. the first class will have a second class type variable, and vice versa.");
		languageEnProp.setProperty("ButtonAddExtendsToolTip", "After clicking this button, you have to mark two classes in the class diagram (by clicking on them with the left mouse button). The first marked class will be inherit from the second labeled class.");
		languageEnProp.setProperty("ButtonAddImplementsToolTip", "After clicking this button, you have to mark two classes in the class diagram (by clicking on them with the left mouse button). The first class to be marked will implement the interface, i.e. the second class.");
		languageEnProp.setProperty("ButtonCommentToolTip", "Click this button to mark the class to which a comment is added.");
		languageEnProp.setProperty("ButtonAggregation_1_ku_1ToolTip", "After clicking this button, you have to mark two classes in the class diagram (by clicking on them with the left mouse button). The first marked class will have a type variable of that second class.");
		languageEnProp.setProperty("ButtonAggregation_1_ku_NToolTip", "After clicking this button, you have to mark two classes in the class diagram (by clicking on them with the left mouse button). The first one (the whole) will have the type List variable of the second marked class.");
				
				
				
		
		
		
		
		
		
		
		
		// Texty do třídy PopupMenuRelationships, jedná se o textu a popisky
		// jednotlivých tlačítek v tomto kontextovém menu pro označneí vztahu pro
		// vytvoření mezi třídami v diagramu tříd:
		ConfigurationFiles.commentsMap.put("PPR_Item_AddClass_Text", "Texts for each item in the popup menu that is displayed by right-clicking the background of the class diagram (so that no object is marked) This is a context menu that contains the same buttons as the toolbar.");
		languageEnProp.setProperty("PPR_Item_AddClass_Text", "Create class");
		languageEnProp.setProperty("PPR_Item_AddAssociation_Text", "Symmetric association");
		languageEnProp.setProperty("PPR_Item_AddExtends_Text", "Extends");
		languageEnProp.setProperty("PPR_Item_AddImplements_Text", "Implementation");
		languageEnProp.setProperty("PPR_Item_AddAggregation_1_ku_1_Text", "Asymmetric association");
		languageEnProp.setProperty("PPR_Item_AddAggregation_1_ku_N_Text", "Aggregation 1: N");
		languageEnProp.setProperty("PPR_Item_AddComment_Text", "Comment");
		
		ConfigurationFiles.commentsMap.put("PPR_Item_AddClass_TT", "Tooltips to each item in a popup menu that appears right-click on the class diagram background (no object is specified), a context menu that contains the same buttons as the toolbar.");
		languageEnProp.setProperty("PPR_Item_AddClass_TT", "When you click on this item, a dialog opens to enter the new class name and the type of the new class.");
		languageEnProp.setProperty("PPR_Item_AddAssociation_TT", "When you click on this item, you have to mark two classes in the class diagram (by clicking on it with the left mouse button). Both classes will know each other, i.e. the first class will have the second class type variable, and vice versa.");
		languageEnProp.setProperty("PPR_Item_AddExtends_TT", "When you click on this item, you have to mark two classes in the class diagram (by clicking on it with the left mouse button). The first labeled class will inherit the second marked class.");
		languageEnProp.setProperty("PPR_Item_AddImplements_TT", "When you click on this item, you have to mark two classes in the class diagram (by clicking on it with the left mouse button). The first class to be marked will implement the interface, that is, the second marked class.");
		languageEnProp.setProperty("PPR_Item_AddAggregation_1_ku_1_TT", "When you click on this item, you have to mark two classes in the class diagram (by clicking on it with the left mouse button). The first class will have the type of the second class.");
		languageEnProp.setProperty("PPR_Item_AddAggregation_1_ku_N_TT", "When you click on this item, you have to mark two classes in the class diagram (by clicking on it with the left mouse button). The first one (the whole) will have the type List variable of the second marked class.");
		languageEnProp.setProperty("PPR_Item_AddComment_TT", "When you click on this item, you have to mark the class to which the comment is added.");
		
		
		
		
		
		
		
		
		
		
		
		
		
				
				
		// Text pro FileChooser - titulky dialogu a chybové hlášky:
		ConfigurationFiles.commentsMap.put("FcSaveImageTitle", "Subtitle texts for file and directory selection dialogs are texts for the FileChooser dialog.");
		languageEnProp.setProperty("FcSaveImageTitle", "Save image");
		languageEnProp.setProperty("FcFileErrorTitle", "File error");
		languageEnProp.setProperty("FcFileErrorText", "Not selected file type!");
		languageEnProp.setProperty("FcNewProjectTitle", "New project");
		languageEnProp.setProperty("FcChooseProjectDirectory", "Select the directory project");
		languageEnProp.setProperty("FcChooseJavaClass", "Add class");
		languageEnProp.setProperty("FcChooseFile", "Select the file");
		languageEnProp.setProperty("FcChooseLocationAndKindOfFile", "Select a location and file type");

		ConfigurationFiles.commentsMap.put("Fc_ChooseCurrencyData", "Error messages in the file and directory selection dialogs Error messages can occur in the methods in the FileChooser dialog, for example, when you get a path to save one of the diagrams, or to select a path to the directory of an application project, etc.");
		languageEnProp.setProperty("Fc_ChooseCurrencyData", "Select the file 'currency.data'");
		languageEnProp.setProperty("Fc_WrongFileTitle", "Wrong file");
		languageEnProp.setProperty("Fc_WrongFileText", "You can only select file");
		languageEnProp.setProperty("Fc_ChooseToolsJar", "Choose file 'tools.jar'");
		languageEnProp.setProperty("Fc_ChooseFlavormapProp", "Select the file 'flavormap.properties'");
		languageEnProp.setProperty("Fc_ChooseJavaHomeDir", "Select the Java home directory");
		languageEnProp.setProperty("Fc_SelectWorkspace", "Select Workspace");
				
				
				
				
				
				
				
				
		// Text do třdy CompiledClassLoader - chyba při vytváření ClassLoader, resp.
		// konkrétně při konstrukci URL:
		ConfigurationFiles.commentsMap.put("Mcl_ErrorWhileCreatingURL", "Text for an error message that is printed in the output editor if an error occurs when creating an instance of the CompiledClassLoader class.");
		languageEnProp.setProperty("Mcl_ErrorWhileCreatingURL", "An error occurred while creating a component ClassLoader, you can not load folded - compiled classes. An error may occur during the construction of the URL to the bin directory in the currently open project.");

		// Chybové texty při načítání souboru .class:
		ConfigurationFiles.commentsMap.put("Mcl_ErrorWhileLoadingClassFile_1", "Text in the error message that occurs when an error occurs when trying to load the compiled (/ translated) class into the application. method loadClass in the CompiledClassLoader class.");
		languageEnProp.setProperty("Mcl_ErrorWhileLoadingClassFile_1", "There was an error loading the next folded - compiled classes");
		languageEnProp.setProperty("Mcl_ErrorWhileLoadingClassFile_2", "class was not found.");
		
		// Texty pro chybové výpis, které mohou nastat při kontroly názvů s case sensitive:
		ConfigurationFiles.commentsMap.put("Mcl_Txt_PackageNotFound_1", "Text for error statements that occurs if the bin directory is searched for in the currently open project to check the package names and the .class (case - sensitive -> case - sensitive) file to retrieve. LoadClass in the CompiledClassLoader class.");
		languageEnProp.setProperty("Mcl_Txt_PackageNotFound_1", "No package found");
		languageEnProp.setProperty("Mcl_Txt_PackageNotFound_2", "in the");
		languageEnProp.setProperty("Mcl_Txt_InPackage", "In the package");
		languageEnProp.setProperty("Mcl_Txt_FileNotFound", "No file found");
				
				
				
				
				
				
				
				
				
				
				// Texty do NewProjectForm:
				// Titulek aplikace
		ConfigurationFiles.commentsMap.put("NpfDialogTitle", "Button texts in the dialog for creating a new project.");
		languageEnProp.setProperty("NpfDialogTitle", "New project");
		// Tlačítka:
		languageEnProp.setProperty("NpfButtonOk", "OK");
		languageEnProp.setProperty("NpfButtonCancel", "Cancel");
		languageEnProp.setProperty("NpfButtonBrowse", "Browse");
		// Popisky:
		ConfigurationFiles.commentsMap.put("NpfLabelNameOfProject", "Text labels in the dialog for creating a new project.");
		languageEnProp.setProperty("NpfLabelNameOfProject", "Name of Project");
		languageEnProp.setProperty("NpfLabelWorkspace", "Application stores projects in a folder labeled Workspace.");
		languageEnProp.setProperty("NpfLabelChangePath", "To change the location of this project?");
		languageEnProp.setProperty("NpfLabelPlace", "Location");
		// Chybové hlášky:
		ConfigurationFiles.commentsMap.put("NpfPathErrorTitle", "Error message texts for a new project to create a new project, for example, if a project name is not specified or a syntax is in error, it is a duplicate name, etc.");
		languageEnProp.setProperty("NpfPathErrorTitle", "Path error");
		languageEnProp.setProperty("NpfPathErrorText", "The path to the project directory is in the wrong format or contains illegal characters! \n(permitted characters: a-z, A-Z, 0-9, _)");
		languageEnProp.setProperty("NpfEmptyFieldErrorTitle", "Empty field");
		languageEnProp.setProperty("NpfEmptyFieldErrorText", "Project name or path to the project is not completed!");
		languageEnProp.setProperty("NpfProjectFolderExistTitle", "The project already exists");
		languageEnProp.setProperty("NpfProjectFolderExistText", "The name of the project is already in the selected directory called workspace! Change this name!");
		// složka projektu již existuje - text:
		languageEnProp.setProperty("NpfProjectFolderExist", "A project with that name already exists!");
				
				
				
				
		// Text do výchozího souboru cz.uhk.fim.fimj.file.Constants.READ_ME_FILE_NAME_WITH_EXTENSION:
		ConfigurationFiles.commentsMap.put("TextLineInfo", "The default texts for the " + Constants.READ_ME_FILE_NAME_WITH_EXTENSION + " file that will be created in the new project directory. This file contains information about the project, which is a template for filling in various project information.");
		languageEnProp.setProperty("TextLineInfo", "This is the file" + " " + Constants.READ_ME_FILE_NAME + ".\r\nHere, briefly describe you project.\r\nReaders, who do not know anything about this project, describe everything,\r\nwhat they needs to know to use your app.");
        languageEnProp.setProperty("TextLineInfo_2", "This introductory passage ended with the following dash line is intended\r\nfor authors - remove them before passing on to future users.\r\nLike unnecessary or unused comments.");
		languageEnProp.setProperty("TextLineAuthor", "AUTHOR");
		languageEnProp.setProperty("TextLineProjectName", "PROJECT NAME");
		languageEnProp.setProperty("TextLineProjectPurpose", "PROJECT GOAL");
        languageEnProp.setProperty("TextLineHowToRunTheProject", "HOW TO START THE PROJECT");
        languageEnProp.setProperty("TextLineStartingClasses", "STARTING CLASSES");
        languageEnProp.setProperty("TextLineUserInstructions", "USER INSTRUCTIONS");
        languageEnProp.setProperty("TextLineUsefulAdditionalInformation", "USEFUL / ADDITIONAL INFORMATION");
		languageEnProp.setProperty("TextLineVersion", "VERSION");
		languageEnProp.setProperty("TextLineDate", "DATE");
				
		
		
				
				// Text pro SettingForm - dialog:
				// Titulek dialogu:
		ConfigurationFiles.commentsMap.put("SfDialogTitle", "Text for the title of the Application Settings dialog and the text in bookmarks in this dialog.");
		languageEnProp.setProperty("SfDialogTitle", "Settings");
		// názvy záložek:
		languageEnProp.setProperty("SfApplicationTitle", "Application");
		languageEnProp.setProperty("SfClassDiagramTitle", "Class diagram");
		languageEnProp.setProperty("SfInstanceDiagramTitle", "Instances diagram");
		languageEnProp.setProperty("SfCommandEditorTitle", "Command editor");
		languageEnProp.setProperty("SfClassEditorTitle", "Source code editor");
		languageEnProp.setProperty("SfOutputEditorTitle", "Output editor");
		
		// Texty do tlačítek:
		ConfigurationFiles.commentsMap.put("SfButtonOk", "Button texts in the Application Settings dialog box Use keys to save dialog changes or close a dialog without saving.");
		languageEnProp.setProperty("SfButtonOk", "OK");
		languageEnProp.setProperty("SfButtonCancel", "Cancel");
		
		// Texty pro dotaz na restartování aplikace:
		ConfigurationFiles.commentsMap.put("Sf_RestartAppText", "Text for the dialog box that serves as a 'query' for the user about whether to restart the application.This query with these texts will be displayed when you click 'OK' in the 'Settings' dialog (if the data is set OK).");
		languageEnProp.setProperty("Sf_RestartAppText", "You need to restart the application to make changes, do you want to restart the application now?");
		languageEnProp.setProperty("Sf_RestartAppTitle", "Restart application");
		
		// Popisky záložek: (Tool tip)
		ConfigurationFiles.commentsMap.put("SfApplicationToolTip", "Bookmarks for bookmarks (information about the content of the bookmarks) in the Application Settings dialog box. These labels will appear when you hover over the title of the bookmark.");
		languageEnProp.setProperty("SfApplicationToolTip", "The tab contains settings window.");
		languageEnProp.setProperty("SfClassDiagramToolTip", "The tab contains settings for the chart - the chart classes.");
		languageEnProp.setProperty("SfInstanceDiagramToolTip", "The tab contains settings for the chart - the chart instances.");
		languageEnProp.setProperty("SfCommandEditorToolTip", "Tab contains settings for the option could command editor.");
		languageEnProp.setProperty("SfClassEditorToolTip", "The tab contains settings for the Source Editor.");
		languageEnProp.setProperty("SfOutputEditorToolTip", "Tab features interesting options for editor outputs.");
		languageEnProp.setProperty("SfPnlProjectExpCodeEditTitle", "Project explorer - Code editor");
		languageEnProp.setProperty("SfPnlProjectExpCodeEditText", "The tab contains settings for the source code editor in the project explorer dialog.");
		languageEnProp.setProperty("SfPnlOutputFrameTitle", "Output terminal");
		languageEnProp.setProperty("SfPnlOutputFrameText", "The tab contains settings for the output term window (dialog).");

		
		
		// Texty do ApplicationPanelu v SettingsForm.java:
		// Popisky - labely:
		ConfigurationFiles.commentsMap.put("Sf_App_Sorting_Text_In_Prop_File_Model", "A one-dimensional array that contains texts for selecting the way text is sorted in .properties files that contain text for the application. These texts are in the JComboBox component of the Application Settings dialog.");
		languageEnProp.setProperty("Sf_App_Sorting_Text_In_Prop_File_Model",
				createTextFromArray(new String[] { "Unassigned", "Ascending by adding", "Descending by adding",
						"Ascending by alphabet", "Descending by alphabet" }));
		
		ConfigurationFiles.commentsMap.put("Sf_App_Sorting_Text_In_Prop_File_TT_Model", "A one-dimensional field that contains tooltips for items that are used to select the way text is sorted in .properties files that contain text for the application, these texts are in the JComboBox component of the Application Settings dialog.");
		languageEnProp.setProperty("Sf_App_Sorting_Text_In_Prop_File_TT_Model",
				createTextFromArray(new String[] {
						"The texts in the files will not be sorted, the default sorting will be applied.",
						"The texts will be sorted ascending according to the order of addition to the file.",
						"The texts will be sorted in descending order of the file added.",
						"The texts will be sorted in alphabetical order in ascending order keywords.",
						" The texts will be sorted by alphabetical order in descending order by keywords." }));
		
		ConfigurationFiles.commentsMap.put("Sf_App_CheckBoxAskForWorkspace", "Text labels and buttons in the Application Settings dialog on the Applications tab, which includes the ApplicationPanel panel.");
		languageEnProp.setProperty("Sf_App_CheckBoxAskForWorkspace", "When you run the application, ask for the location of the directory labeled workspace.");
		languageEnProp.setProperty("Sf_App_BorderLanguage", "Set language and text arrangement");
		languageEnProp.setProperty("Sf_App_Pnl_CaptureExceptions", "List exceptions");
		languageEnProp.setProperty("Sf_App_PanelLanguageBorderTitle", "Image into \"Loading panel\"");
		languageEnProp.setProperty("Sf_App_BorderCopyDir", "Creating configuration files");
		
		// Texty pro panel s komponentami pro nastavení, zda se má testovat existence adresáře se soubory potřebnými pro kompilaci tříd a zda se při jejich hledání má prohledat celý disk:
		ConfigurationFiles.commentsMap.put("Sf_App_Ap_PnlSearchCompileFiles", "Texts for a panel that contains components that are used to check whether a directory is to be tested when the application is running, with the files that the application needs to compile the Javascript classes, and whether to search the whole disk or just to search for the files parent directories from the java Home directory.");
		languageEnProp.setProperty("Sf_App_Ap_PnlSearchCompileFiles", "Search for files needed for class compilation");
		languageEnProp.setProperty("Sf_App_Ap_ChcbSearchCompileFilesText", "After running the application, find the files needed for class compilation (Java Home).");
		languageEnProp.setProperty("Sf_App_Ap_ChcbSearchCompileFilesTt", "After starting the application, a thread will start to try to find the files needed for class compilation and copy them to the appropriate directory in the workspace (for more information see Java Home).");
		languageEnProp.setProperty("Sf_App_Ap_ChcbSearchEntireDiskForCompileFilesText", "To search for files, search the entire disk.");
		languageEnProp.setProperty("Sf_App_Ap_ChcbSearchEntireDiskForCompileFilesTt", "In order to find the necessary files, the entire disk will be searched on that device, and there is a risk of finding bad files with the same name.");
		
		ConfigurationFiles.commentsMap.put("Sf_App_LabelCopyConfigurationDir", "Text for components in the panel that is used to create configuration files for the application.");
		languageEnProp.setProperty("Sf_App_LabelCopyConfigurationDir", "Create a directory with default settings (configuration directory) to");
		languageEnProp.setProperty("Sf_App_LabelCopyLanguageDir", "Create a directory with the application languages to");
		languageEnProp.setProperty("Sf_App_LabelCopyDefaultProperties", "Create a file with the default application for Default.properties");
		languageEnProp.setProperty("Sf_App_LabelCopyClassDiagramProperties", "Create a file with the default settings for the class diagram (file ClassDiagram.properties) to");
		languageEnProp.setProperty("Sf_App_LabelCopyInstanceDiagramProperties", "Create a file with the default settings for the diagram instances (file InstanceDiagram.properties) to");
		languageEnProp.setProperty("Sf_App_LabelCopyCommandEditorProperties", "Create a file with the default settings for command editor (CommandEditor.properties) to");
		languageEnProp.setProperty("Sf_App_LabelCopyClassEditorProperties", "Create a file with the default settings for the source editor class (file CodeEditor.properties) to");
		languageEnProp.setProperty("Sf_App_LabelCopyOutputEditorProperties", "Create a file with the default output editor settings (file OutputEditor.properties) to");																			  
		languageEnProp.setProperty("Sf_App_LabelCopyOutputFrameProperties", "Create a file with the default settings for the output terminal (file OutputFrame.properties) to");
		languageEnProp.setProperty("Sf_App_LabelCopyCodeEditorForInternalFrames", "Create a file with the default settings for the code editor project explorer (file CodeEditorInternalFrame.properties) to");
		
		ConfigurationFiles.commentsMap.put("Sf_App_LabelAnimationImage", "Remaining Text for Components in the panel on the 'Applications' tab that contains options for setting some application properties.");
		languageEnProp.setProperty("Sf_App_LabelAnimationImage","image animation");
		languageEnProp.setProperty("Sf_App_Chcb_CaptureExceptionsText", "Write exceptions to the Output terminal.");
		languageEnProp.setProperty("Sf_App_Chcb_CaptureExceptionsText_TT", "If an unexpected exception occurs in" + " " + Constants.APP_TITLE + " " + "application, the relevant information will be printed in the 'Output terminal' dialog. (Only some potential exceptions are being watched when disabling.)");
		languageEnProp.setProperty("Sf_App_LabelResetApplication", "Please make sure you restart the application to make the changes.");
		languageEnProp.setProperty("Sf_App_ID_Lbl_AttributesAndMethodsPanelTitle", "Attributes and methods in class instance");
		languageEnProp.setProperty("Sf_App_BtnRestoreDefault", "Restore defaults");
		languageEnProp.setProperty("Sf_App_ButtonsCopyDirectories", "Place");


        // Texty do třídy cz.uhk.fim.fimj.settings_form.application_panels.LookAndFeelPanel:
        ConfigurationFiles.commentsMap.put("Lafp_BorderTitle", "Panel texts in the settings dialog on the 'Application' tab, which contains the components for the Look and Feel setting.");
        languageEnProp.setProperty("Lafp_BorderTitle", "Graphical User Interface");
        languageEnProp.setProperty("Lafp_Lbl_LookAndFeel", "Appearance of the application");
        languageEnProp.setProperty("Lafp_Txt_WithoutLookAndFeelItem_TT", "No Look and Feel will be applied. The default style common to all platforms is used.");


        // Texty do třídy: cz.uhk.fim.fimj.settings_form.application_panels.UserNamePanel
        ConfigurationFiles.commentsMap.put("Unp_Pnl_BorderTitle", "Texts to the 'User Name' panel in the settings dialog on the 'Application' tab. The panel contains texts for setting the username, here are the texts for all the components in the panel.");
        languageEnProp.setProperty("Unp_Pnl_BorderTitle", "Username");
        languageEnProp.setProperty("Unp_Lbl_UserName_Text", "Name");
        languageEnProp.setProperty("Unp_Lbl_UserName_Title", "The name will be used as the 'author' of created projects, classes, logging, etc.");
        languageEnProp.setProperty("Unp_Txt_UserNameField_1", "Allowed characters: lowercase / uppercase letters, digits, underlings, hyphens, spaces and tabs. The name must begin with a letter.");
        languageEnProp.setProperty("Unp_Txt_UserNameField_2", "characters.");
        languageEnProp.setProperty("Unp_Btn_Edit", "Edit");
        languageEnProp.setProperty("Unp_Btn_Save", "Save");
        languageEnProp.setProperty("Unp_Btn_Cancel", "Cancel");
        languageEnProp.setProperty("Unp_Btn_RestoreDefault_Text", "Restore defaults");
        languageEnProp.setProperty("Unp_Btn_RestoreDefault_Title", "The user name of the logged-in user is set in the OS.");
        // Texty do chybových hlášek:
        languageEnProp.setProperty("Unp_Txt_FailedSaveUserNameToFile_Text", "Unable to save user name to configuration file.");
        languageEnProp.setProperty("Unp_Txt_FailedSaveUserNameToFile_Title", "Failed to save");
        languageEnProp.setProperty("Unp_Txt_InvalidName_Text_1", "The username must begin with a letter, it must contain");
        languageEnProp.setProperty("Unp_Txt_InvalidName_Text_2", "characters and can contain only uppercase and lower case letters with / without diacritics, digits, underscores, hyphens, spaces and tabs.");
        languageEnProp.setProperty("Unp_Txt_InvalidName_Title", "Name is not valid");
		
		
		// Texty do LanguagePanel v applicationPanels:
		ConfigurationFiles.commentsMap.put("Sf_App_LabelLanguage", "Labels for the Labels in the LanguagePanel panel in the ApplicationPanel panel that makes up the Application tab in the Application Settings dialog box.");
		languageEnProp.setProperty("Sf_App_LabelLanguage", "Language");
		languageEnProp.setProperty("Sf_LP_Lbl_Sorting_Text_In_Prop_File", "Sort text in files");
		languageEnProp.setProperty("Sf_LP_Chcb_Add_Comments", "Post comments.");
		languageEnProp.setProperty("Sf_LP_Lbl_Count_Of_Free_Lines_Before_Comment", "Number of free lines above comment");
		languageEnProp.setProperty("Sf_LP_Lbl_Locale", "Global location");
		languageEnProp.setProperty("Sf_LP_Lbl_Locale_Tt", "This is a global location for defining the creation date format in '.properties' files containing application texts in different languages.");


		
		
		
		
		// Texty do ClassDiagramPanelu v SettingsForm.java:
		// Popisky - labely:
		ConfigurationFiles.commentsMap.put("Sf_Cd_CheckBoxOpaqueClassCell", "Text labels and buttons in the ClassDiagramPanel panel, located in the Application Settings dialog box on the Class Diagram tab.");
		languageEnProp.setProperty("Sf_Cd_CheckBoxOpaqueClassCell", "The cell will be transparent.");
		languageEnProp.setProperty("Sf_Cd_LabelFontSizeClassCell", "Font size");
		languageEnProp.setProperty("Sf_Cd_LabelFontStyleClassCell", "Font style");
		languageEnProp.setProperty("Sf_Cd_LabelTextAlignent_X_ClassCell", "Text alignment - horizontally");
		languageEnProp.setProperty("Sf_Cd_LabelTextAlignment_Y_ClassCell", "Text alignment - vertically");
		languageEnProp.setProperty("Sf_Cd_LabelTextColorClassCell", "Font color");
		languageEnProp.setProperty("Sf_Cd_LabelBackgroundColor_Right_ClassCell", "The background color (lower right corner)");
		languageEnProp.setProperty("Sf_Cd_LabelBackgroundColor_Left_ClassCell", "The background color (from the upper left corner)");
		languageEnProp.setProperty("Sf_Cd_ClassCellBorderTitle", "Cell representing the class");
		languageEnProp.setProperty("Sf_Cd_CommentCellBorderTitle", "Cell representing the comment");
		languageEnProp.setProperty("Sf_Cd_ButtonsChooseColor", "Choose");

		
		// Text pro tlačítko obnovit výchozí nastavení:
		ConfigurationFiles.commentsMap.put("Sf_Cd_ButtonRestoreDefaultSettings", "The text for the button to restore the default settings This text is applied to all the buttons that are used to restore the default settings in the panels in the application settings dialog box, which are always located at the bottom of the panel on each tab in the the Application Setup dialog box.");
		languageEnProp.setProperty("Sf_Cd_ButtonRestoreDefaultSettings", "Restore defaults");


        // Texty do třídy CommandEditorPanel v nastavení. Texty slouží pouze pro chcbs pro nastavení zobrazení a vlastnosti okna pro doplňování příkazů v editoru příkazů:
        ConfigurationFiles.commentsMap.put("CEP_Pnl_AutoCompleteWindow", "Component texts in the settings dialog to set the properties of the window with values ​​for adding commands to the command editor.");
        languageEnProp.setProperty("CEP_Pnl_AutoCompleteWindow", "Window for completing commands");
        languageEnProp.setProperty("CEP_Chcb_ShowAutoCompleteWindow_Text", "Make command window available.");
        languageEnProp.setProperty("CEP_Chcb_ShowAutoCompleteWindow_TT", "Press the Ctrl + Space shortcut to display the help with available commands.");
        languageEnProp.setProperty("CEP_Chcb_ShowReferenceVariablesInCompleteWindow_Text", "Show reference variables.");
        languageEnProp.setProperty("CEP_Chcb_ShowReferenceVariablesInCompleteWindow_TT", "Next to the values ​​of variables displayed in the window will be displayed the reference variable to the instance in the instance diagram.");
        languageEnProp.setProperty("CEP_Chcb_AddSyntaxForFillingTheVariable_Text", "Offering the creation and filling of the variable.");
        languageEnProp.setProperty("CEP_Chcb_AddSyntaxForFillingTheVariable_TT", "There will be available options to create or fill variables with values ​​from variables in instances.");
        languageEnProp.setProperty("CEP_Chcb_AddFinalWordForFillingTheVariable_Text", "Declare variables with");
        languageEnProp.setProperty("CEP_Chcb_AddFinalWordForFillingTheVariable_TT", "The generated variable declaration will be implicitly 'final'.");
        languageEnProp.setProperty("CEP_Chcb_ShowExamplesOfSyntax_Text", "Offer sample command syntax.");
        languageEnProp.setProperty("CEP_Chcb_ShowExamplesOfSyntax_TT", "Using the word 'example_', it will be possible to filter and generate one of the available options for declaring a variable, a sheet, a one-dimensional array, etc.");
        languageEnProp.setProperty("CEP_Chcb_ShowDefaultMethods_Text", "Offering methods for the editor.");
        languageEnProp.setProperty("CEP_Chcb_ShowDefaultMethods_TT", "It will be possible to generate methods specific to the command editor (listing of variables, help, values, etc.).");

        // Texty do třídy: cz.uhk.fim.fimj.settings_form.command_editor_panels.CompleteHistoryPanel:
        ConfigurationFiles.commentsMap.put("Chp_Txt_Pnl_BorderTitle", "Text in the panel to set the history window of commands entered into the command editor from application startup.");
        languageEnProp.setProperty("Chp_Txt_Pnl_BorderTitle", "Complete history");
        languageEnProp.setProperty("Chp_Txt_Pnl_Tooltip_Text", "Command history from the start of the application.");
        languageEnProp.setProperty("Chp_Chcb_ShowHistory_Text", "Save history of typed commands.");
        languageEnProp.setProperty("Chp_Chcb_IndexCommands_Text", "Number commands.");
        languageEnProp.setProperty("Chp_Chcb_IndexCommands_TT", "Commands will be numbered according to the input order (it will not be possible to filter by entering parts of the command).");

        // Texty do třídy: cz.uhk.fim.fimj.settings_form.command_editor_panels.ProjectHistoryPanel:
        ConfigurationFiles.commentsMap.put("Php_Txt_Pnl_BorderTitle", "Texts in the panel to set the command history window entered into the command editor within an open project.");
        languageEnProp.setProperty("Php_Txt_Pnl_BorderTitle", "Project history");
        languageEnProp.setProperty("Php_Txt_PnlTooltip_Text", "History of commands entered within an open project.");
        languageEnProp.setProperty("Php_Chcb_ShowHistory_Text","Save history of typed commands.");
        languageEnProp.setProperty("Php_Chcb_IndexCommands_Text", "Number commands.");
        languageEnProp.setProperty("Php_Chcb_IndexCommands_TT", "Commands will be numbered according to the input order (it will not be possible to filter by entering parts of the command).");


		// Texty do chybových hlášek ve třídě ClassDiagramPanel:
		ConfigurationFiles.commentsMap.put("Sf_Cd_TxtClassAndCommentCellsAreIdenticalText", "Text for error messages that can occur when saving settings for a class diagram.");
		languageEnProp.setProperty("Sf_Cd_TxtClassAndCommentCellsAreIdenticalText", "Cells representing the class and comment are identical, please change the parameters!");
		languageEnProp.setProperty("Sf_Cd_TxtClassAndCommentCellsAreIdenticalTitle", "Error duplicity");
		languageEnProp.setProperty("Sf_Cd_TxtEdgesAreIdenticalText", "The following edges are identical, please change the parameters!");
		languageEnProp.setProperty("Sf_Cd_TxtEdgesAreIdenticalTitle", "Error duplicity");
				
				
		ConfigurationFiles.commentsMap.put("Sf_Cd_EdgesPanel", "Border Label Label in the Settings dialog box where edge-editing components are located for class-class relationships in the class diagram.");
		languageEnProp.setProperty("Sf_Cd_EdgesPanel", "Edge properties");
		
		ConfigurationFiles.commentsMap.put("Sf_Id_ClassCellPanelBorderTitle", "A label for the panel in the Settings dialog box that contains the cell parameter setting components that represents the instance of the class in the instance diagram.");
		languageEnProp.setProperty("Sf_Id_ClassCellPanelBorderTitle", "A cell representing a class instance");
				
		// Texty do panelu v settingForm.java v panelu: ClassDiagramGraphPanel:
		ConfigurationFiles.commentsMap.put("Sf_Cd_Cdgp_BorderTitle", "Text labels in panels to set parameters for the class diagram and instances diagram in the setup dialog.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_BorderTitle", "Diagram properties");
		languageEnProp.setProperty("Sf_Cd_Cdgp_LabelDisconectableFromEdge", "Edge labels can be detached from the edge.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_ShowRelationsShipsToClassItself", "Show the relation of the class to itself.");		
		languageEnProp.setProperty("Sf_Cd_Cdgp_ChcbShowAsociationThroughAgregation", "Symmetric association as two asymmetric.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_ChcbShowAsociationThroughAgregation_TT", "The symmetric association type relationship will be displayed as two asymmetric associations.");		
		languageEnProp.setProperty("Sf_Cd_Cdgp_AddInterfaceMethodsToClassText", "Generate methods when create implementing an interface relationship.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_AddInterfaceMethodsToClassTT", "When creating an interface implementation type, the non-static methods from the tagged interface are generated in the appropriate (labeled) class.");

		languageEnProp.setProperty("Sf_Cd_Cdgp_IncludePrivateConstructors", "Access private constructors in the context menu.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_IncludePrivateConstructors_TT", "In the context menu above the designated class, private constructors will be made available (i.e. constructors marked with 'private').");
		languageEnProp.setProperty("Sf_Cd_Cdgp_IncludeProtectedConstructors", "Access protected constructors in the context menu.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_IncludeProtectedConstructors_TT", "In the context menu above the designated class, protected constructors (i.e. constructors marked with 'protected') will be available.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_IncludePackagePrivateConstructors", "Access package-private constructors in the context menu.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_IncludePackagePrivateConstructors_TT", "In the context menu above the designated class, package-private constructors (i.e. constructors without visibility) will be available.");

		languageEnProp.setProperty("Sf_Cd_Cdgp_ShowInheritedClassNameWithPackages", "Names of inherited classes shows with packages.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_ShowInheritedClassNameWithPackages_TT", "In the popup menu above the class in the 'Inherited static methods' menu, the names of the classes with the packages in which they are coming are displayed.");

		languageEnProp.setProperty("Sf_Cd_Cdgp_IncludePrivateMethods", "Access private methods in context menu.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_Cd_IncludePrivateMethods_TT", "In the context menu above the designated class, private methods (i.e. methods marked with 'private') will be available.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_Id_IncludePrivateMethods_TT", "In the context menu above the designated instance, private methods (i.e. methods marked with 'private') will be available.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_Id_IncludeProtectedMethods_TT", "In the context menu above the designated instance, protected methods (i.e., methods marked with 'protected') will be available.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_Id_IncludePackagePrivateMethods_TT", "In the context menu above the designated instance, package-private methods (i.e., methods without a visibility keyword) will be available.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_IncludeProtectedMethods", "Access protected methods in the context menu.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_IncludeProtectedMethods_TT", "In the context menu above the designated class, protected methods (i.e., methods marked with 'protected') will be available.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_IncludePackagePrivateMethods", "Access the package-private methods in the context menu.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_IncludePackagePrivateMethods_TT", "In the context menu above the flagged class, package-private methods will be available (i.e., the methods without a visibility keyword).");

		languageEnProp.setProperty("Sf_Cd_Cdgp_IncludePrivateInnerStaticClasses", "Access static private inner classes in the context menu.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_IncludePrivateInnerStaticClasses_TT", "In the context menu above the marked class, static private inner classes (i.e., inner classes marked with 'private') will be available.");

		languageEnProp.setProperty("Sf_Cd_Cdgp_IncludeProtectedInnerStaticClasses", "Access static protected inner classes in the context menu.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_IncludeProtectedInnerStaticClasses_TT", "In the context menu above the marked class, static protected inner classes (i.e., inner classes marked with 'protected') will be available.");

		languageEnProp.setProperty("Sf_Cd_Cdgp_IncludePackagePrivateInnerStaticClasses", "Access static package-private inner classes in the context menu.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_IncludePackagePrivateInnerStaticClasses_TT", "In the context menu above the marked class, static package-private inner classes (i.e., inner classes without a visibility keyword) will be available.");

		languageEnProp.setProperty("Sf_Cd_Cdgp_InnerStaticClassesIncludePrivateStaticMethods", "Access static private methods in inner static classes in the context menu.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_InnerStaticClassesIncludePrivateStaticMethods_TT", "In the context menu above the marked class, static private methods (i.e. methods marked with 'private') will be available for its static inner classes.");

		languageEnProp.setProperty("Sf_Cd_Cdgp_InnerStaticClassesIncludeProtectedStaticMethods", "Access static protected methods in inner static classes in the context menu.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_InnerStaticClassesIncludeProtectedStaticMethods_TT", "In the context menu above the marked class, static protected methods (i.e. methods marked with 'protected') will be available for its static inner classes.");

		languageEnProp.setProperty("Sf_Cd_Cdgp_InnerStaticClassesIncludePackagePrivateStaticMethods", "Access static package-private methods in inner static classes in the context menu.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_InnerStaticClassesIncludePackagePrivateStaticMethods_TT", "In the context menu above the marked class, static package-private methods (i.e. methods marked with 'private') will be available for its static inner classes.");

		languageEnProp.setProperty("Sf_Cd_Cdgp_Id_ShowInheritedClassNameWithPackages_TT", "In the popup menu above the instance in the 'Inherited methods' menu, the names of the classes with the packages in which they are coming are displayed.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_MakeAvailableFieldsForCallConstructor", "When calling the constructor to make the variables available.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_MakeAvailableFieldsForCallConstructor_TT", "In the constructor call dialog, public and protected (or static) variables from classes, instances of classes and variables generated by the command editor will be available from which it will be possible to pass the value to the parameter.");		
		languageEnProp.setProperty("Sf_Cd_Cdgp_MakeMethodsAvailableForCallConstructor", "When you call the constructor to make the methods available.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_MakeMethodsAvailableForCallConstructor_TT", "In the constructor call dialog, public (or protected) static methods will be accessed from classes in the class diagram and public methods from instances in the instance diagram to pass the return value to the constructor parameter.");		
		languageEnProp.setProperty("Sf_Cd_Cdgp_MakeAvailableFieldsForCallMethod", "When calling the method to make the variables available.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_MakeAvailableFieldsForCallMethod_TT", "In the method call dialog, public and protected (or static) variables from classes, instances of classes and variables generated by the command editor will be available from which it will be possible to pass the value to the parameter.");
		languageEnProp.setProperty("Sf_Id_Cdgp_MakeMethodsAvailableForCallMethod", "When you call the method to make the methods available.");
		languageEnProp.setProperty("Sf_Id_Cdgp_MakeMethodsAvailableForCallMethod_TT", "In the dialog to call the method above the instance in the instance diagram, public (or protected) methods will be accessed from an instance of the instance diagram and public (or protected) static methods from classes in the class diagram to pass their return value to the appropriate method parameter.");		
		languageEnProp.setProperty("Sf_Cd_Cdgp_MakeMethodsAvailableForCallMethod", "When you call the method to make the methods available.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_MakeMethodsAvailableForCallMethod_TT", "In the dialog for calling a static method over a class in the class diagram, public (or protected) static methods will be available from classes in the class diagram and public methods from instances in the instance diagram to pass their return value to the appropriate method parameter.");		
		languageEnProp.setProperty("Sf_Cd_Cdgp_ShowCreateNewInstanceOptionInCallConstructor", "When you call the constructor, make the instantiation available.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_ShowCreateNewInstanceOptionInCallConstructor_TT", "If the constructor parameter is the class that is in the class diagram, it will be possible to create a new instance of it.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_ShowCreateNewInstanceOptionInCallMethod",  "When you call the method, make the instantiation available.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_WayToRefreshRelationshipsBetweenInstances_Text", "Use a quick way to update relationships.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_ShowCreateNewInstanceOptionInCallMethod_TT", "If the method parameter is the class that is in the class diagram, it will be possible to create a new instance of it.");		
		languageEnProp.setProperty("Sf_Cd_Cdgp_ShowGetterInSetterMethodInInstance", "When you call the setter, offering a call getter.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_ShowGetterInSetterMethodInInstance_TT", "If a setter is called above an instance in instance diagram, offer a call getter in the appropriate instance (tested by method names).");
		languageEnProp.setProperty("Sf_Cd_Cdgp_WayToRefreshRelationshipsBetweenInstances_TT", "Relationships between instances will first be erased and then created only by the current ones, the second way is more slower, because the current and necessary number of relationships between the instances is determined and then accordingly the respective relationships are modified (added, removed or retained).");
		languageEnProp.setProperty("Sf_Cd_Cdgp_ShowRelationshipsInInheritedClasses", "Show relationships filled in an ancestral instance.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_ShowRelationshipsInInheritedClasses_TT", "Whether to show relationships between instances in the ancestors of the instance.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_Chcb_ShowReferenceVariablesText", "Show reference variables.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_Chcb_ShowReferenceVariablesTT", "For variables in the instance value overview dialog, references to instances that are represented in the instance diagram will be displayed.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_LabelBackgroundColor", "Background color chart");
		languageEnProp.setProperty("Sf_Cd_Cdgp_LabelScaleOfGraph", "Zoom level graph");
		//  Texty pro nastavení výchozího editoru zdrojového kodu:
		languageEnProp.setProperty("Sf_Cd_Cdgp_LabelCodeEditorInfoText", "Default source code editor");
		languageEnProp.setProperty("Sf_Cd_Cdgp_LabelCodeEditorInfoTT", "The default source code editor in which the class source codes will be primarily opened in the class diagram.");
		// Texty do modelu komponenty JComboBox pro výchozí editor zdrojového kodu:
		languageEnProp.setProperty("Sf_Cd_Cdgp_TxtSourceCodeEditorText", "Source code editor");
		languageEnProp.setProperty("Sf_Cd_Cdgp_TxtSourceCodeEditorTT", "The class source code in the class diagram will be primarily opened in the 'Source code editor' dialog.");
		languageEnProp.setProperty("Sf_Cd_Cdgp_TxtProjectExplorerText", "Project explorer");
		languageEnProp.setProperty("Sf_Cd_Cdgp_TxtProjectExplorerTT", "The class source code in the class diagram will be primarily opened in the 'Project explorer' dialog.");
				
		// Titulky panelů v panelu pro hrany:
		ConfigurationFiles.commentsMap.put("Sf_Cd_ExtendsEdgePanel", "Fonts for subtitles in border of panels with components for parameter settings for edges that represent some class relations in the class diagram.");
		languageEnProp.setProperty("Sf_Cd_ExtendsEdgePanel", "Extends");
		languageEnProp.setProperty("Sf_Cd_ImplementsEdgePanel", "implementation");
//		languageEnProp.setProperty("Sf_Cd_Aggregation_1_ku_1_EdgePanel", "Aggregation 1 : 1");
		languageEnProp.setProperty("Sf_Cd_Aggregation_1_ku_1_EdgePanel", "Asymmetric association");
		languageEnProp.setProperty("Sf_Cd_Aggregation_1_ku_N_EdgePanel", "Aggregation 1 : N");
			
		languageEnProp.setProperty("Sf_Cd_ImplementsEdgePanelLabelLinePattern", "Style dashed edges");
				
				// Text pro dialog pro vyber barvy:
		ConfigurationFiles.commentsMap.put("Sf_Cd_ChooseTextColorDialog", "Text labels in the settings dialog, in cell parameter setting panels that serve as a class or comment in a class diagram.");
		languageEnProp.setProperty("Sf_Cd_ChooseTextColorDialog", "Choose the font color");
		languageEnProp.setProperty("Sf_Cd_ChooseBackgroundColorDialog", "Choose a background color");
		languageEnProp.setProperty("Sf_Cd_ChooseEdgeLineColor", "Select a color edges");
		languageEnProp.setProperty("Sf_Cd_TxtOneColorForCell", "One color for the cell");
		languageEnProp.setProperty("Sf_Cd_TxtTwoColorForCell", "Two colors for cell");
		languageEnProp.setProperty("Sf_Cd_TxtBgColorForCell", "Cell background color");
				
				// Texty do panelu ClassDiagramPanel v pnlAssociationEdge:
		ConfigurationFiles.commentsMap.put("Sf_Cd_AsociationEdgePanel", "Texts in the settings dialog for JCheckBox and some labels in the edge parameter panel for the edge that represents the association type of the association in the class diagram.");
//		languageEnProp.setProperty("Sf_Cd_AsociationEdgePanel", "Association");
		languageEnProp.setProperty("Sf_Cd_AsociationEdgePanel", "Symmetric association");		
		languageEnProp.setProperty("Sf_Cd_CheckBoxLabelAlongEdge", "Text edge is located along the edge.");
		languageEnProp.setProperty("Sf_Cd_CheckBoxLineBeginFill", "Beginning edge is filled.");
		languageEnProp.setProperty("Sf_Cd_CheckBoxLineEndFill", "End of edge is filled.");
		languageEnProp.setProperty("Sf_Cd_LineColor", "Color edge");
		languageEnProp.setProperty("Sf_Cd_LineStyle", "Edge style");
		languageEnProp.setProperty("Sf_Cd_LineEnd", "Type end edges");
		languageEnProp.setProperty("Sf_Cd_LineBegin", "Type the beginning of the edges");
		languageEnProp.setProperty("Sf_Cd_LineWidth", "Width edge");

		
				// Texty do InstanceDiagramPanelu  - ClassCellPanel:
		ConfigurationFiles.commentsMap.put("Sf_Id_ShowPackagesInCell", "Texts in the Label Settings dialog box in the Parameter Settings panel for the cell that represents the instance of the class in the instance diagram.");
		languageEnProp.setProperty("Sf_Id_ShowPackagesInCell", "Show packages class.");
		languageEnProp.setProperty("Sf_Id_LabelChooseBorderColor", "Border Color");
		languageEnProp.setProperty("Sf_Id_LabelChooseCellBackgroundColor", "Color cells");
		languageEnProp.setProperty("Sf_Id_TextToDialogChooseBorderColor", "Select a border color");
		languageEnProp.setProperty("Sf_Id_TextToDialogChooseBackgroundColor", "Select a cell color");
		languageEnProp.setProperty("Sf_Id_LabelRoundedBorder", "Angle edge rounding");

		
		// Texty do AssociationEdgePanelu:
		ConfigurationFiles.commentsMap.put("Sf_Id_AssociationEdgePanelBorderTitle", "Caption for the Settings dialog box for editing edge panel components that represents the association type relationship between class instances in the instance diagram.");
		languageEnProp.setProperty("Sf_Id_AssociationEdgePanelBorderTitle", "Edge representing the association");
				
		// Titulek panelu AggregationEdgePanel:
		ConfigurationFiles.commentsMap.put("Sf_Id_AggregationEdgePanelBorderTitle", "Caption for the Settings dialog box for editing edge panel that represents the aggregation type relationship between class instances in the instance diagram.");
		languageEnProp.setProperty("Sf_Id_AggregationEdgePanelBorderTitle", "Edge representing aggregation");
				
		
		
		// Texty do třídy: HighlightInstancePanel v instanceDigramPanels. Jedná se o
		// třídu s komponentami pro nastavení jak má vypadat zvýrazněná instance a jak
		// dlouho má být zvýrazněná a zda se mají vůbec zvýrazňovat.
		ConfigurationFiles.commentsMap.put("Hip_BorderTitle", "A panel title that contains components for setting the highlighted cell / instance properties. This panel is located in the Setup dialog on the Instance Diagram tab in the Highlight Instance section.");
		languageEnProp.setProperty("Hip_BorderTitle", "Highlight an instance");		
		
		// Zda se mají zvýrazňovat instance:
		ConfigurationFiles.commentsMap.put("Hip_Chcb_HighlightInstancesText", "Texts for the JCheckBox component used to set whether to highlight instances in the instance diagram, for example by calling the method or obtaining a variable value, and this value is an instance of a class from the class diagram that is represented in the instance diagram. of the Settings dialog on the Instance Diagram tab in the Highlighting Instance section.");
		languageEnProp.setProperty("Hip_Chcb_HighlightInstancesText", "Highlight instances");
		languageEnProp.setProperty("Hip_Chcb_HighlightInstancesTT", "If the value obtained (from a method or a variable) of an instance of a class from the class diagram that is in the instance diagram is highlighted.");
		
		// Čas, jak dlouho mají být zvýrazněné instance:
		ConfigurationFiles.commentsMap.put("Hip_Lbl_HighlightingTimeText", "Texts for the Jlabel component that informs the user that at the appropriate location it is possible to set the time for the instance to be highlighted in the instance diagram. For example, when the instance in question is retrieved from a variable in an instance and the instance is in the instance diagram how long it should be highlighted, which is located in the Setup dialog on the Instance Diagram tab in the Highlighting section of the instance. ");
		languageEnProp.setProperty("Hip_Lbl_HighlightingTimeText", "Highlight time in seconds");
		languageEnProp.setProperty("Hip_Lbl_HighlightingTimeTT", "This is the time the instance will be highlighted, for example, the value of '1.0' means that the instance will be highlighted for one second, then returns to its original appearance.");
		
		// Label pro výběr barvy písma ve zvýrazněné instanci:
		ConfigurationFiles.commentsMap.put("Hip_Lbl_TextColorText", "Texts for the Jlabel component that informs users that the font for the highlighted cell / instance can be set at the appropriate location, located in the Setup dialog on the Instance Inventory tab in the Highlighting Instance section.");
		languageEnProp.setProperty("Hip_Lbl_TextColorText", "Font color");
		languageEnProp.setProperty("Hip_Lbl_TextColorTT", "The font color of the highlighted instance.");
		
		// Label pro výběr barvy písma atributů ve zvýrazněné instanci:
		ConfigurationFiles.commentsMap.put("Hip_Lbl_AttributeColorText", "Text for the JLabel component that informs users that the font color for the attributes in the highlighted cell / instance can be set at the appropriate location, and is located in the Setup dialog box on the Instance Inventory tab under Highlighting Instance.");
		languageEnProp.setProperty("Hip_Lbl_AttributeColorText", "Attribute Color");
		languageEnProp.setProperty("Hip_Lbl_AttributeColorTT", "Font color for attributes of the highlighted instance.");
		
		// Label pro výběr barvy písma metod ve zvýrazněné instanci:
		ConfigurationFiles.commentsMap.put("Hip_Lbl_MethodColorText", "Text for the JLabel component that tells the user that the font color for methods in the highlighted cell / instance can be set at the appropriate location, located in the Setup dialog on the Instance Inventory tab in the Highlight Instance section.");
		languageEnProp.setProperty("Hip_Lbl_MethodColorText", "Method color");
		languageEnProp.setProperty("Hip_Lbl_MethodColorTT", "Font color for methods of the highlighted instance.");
		
		// Label pro výběr barvy ohnraničení zvýrazněné instance:
		ConfigurationFiles.commentsMap.put("Hip_Lbl_BorderColorText", "Texts for the Jlabel component that informs users that the border color for the highlighted cell / instance can be set at the appropriate location, located in the Instances Highlighting section of the Setup dialog on the Instance Inventory tab.");
		languageEnProp.setProperty("Hip_Lbl_BorderColorText", "Border Color");
		languageEnProp.setProperty("Hip_Lbl_BorderColorTT", "Border color of highlighted instance.");
		
		// Label pro výběr barvy pozadí zvýrazněné instance:
		ConfigurationFiles.commentsMap.put("Hip_Lbl_BackgroundColorText", "Text for the Jlabel component that tells the user that the background color for the highlighted cell / instance can be set at the appropriate location in the Setup dialog on the Instance Inventory tab in the Highlight Instance section.");
		languageEnProp.setProperty("Hip_Lbl_BackgroundColorText", "Backround colour");
		languageEnProp.setProperty("Hip_Lbl_BackgroundColorTT", "Background color of the highlighted instance.");
		
		// Tlčítka pro označení barev písma, ohraničení a pozadí instance:
		ConfigurationFiles.commentsMap.put("Hip_Btn_ChooseColor", "The button text for selecting the font color, border, and cell background of the highlighted instance. These buttons are located in the Setup dialog on the Instance Inventory tab in the Highlighting Instance section.");
		languageEnProp.setProperty("Hip_Btn_ChooseColor", "Choose");
		
		// Texty pro dialogy pro výběry barev:
		ConfigurationFiles.commentsMap.put("Hip_Txt_ChooseTextColorTitle", "Fonts in the subtitles of the dialog box to select the font color, border, and background of the highlighted instance cell.");
		languageEnProp.setProperty("Hip_Txt_ChooseTextColorTitle", "Select font color");
		languageEnProp.setProperty("Hip_Txt_ChooseAttributeColorTitle", "Select attribute font color");
		languageEnProp.setProperty("Hip_Txt_ChooseMethodColorTitle", "Select method font color");
		languageEnProp.setProperty("Hip_Txt_ChooseBorderColorTitle", "Select Border Color");
		languageEnProp.setProperty("Hip_Txt_ChooseBackgroundColor", "Select background color");
		
		
		
		
		
		
		
		
		// Texty do panelu InstanceCellMethodsPanel v balíčku instnceDiagramPanel, jedná
		// se o texty pro komponenty pro nastavení toho, zda a jak se mají zobrazovat
		// metody v buňce reprezentující instanci v diagramu instancí.
		ConfigurationFiles.commentsMap.put("ICMP_DialogTitle", "The panel title in the Settings dialog on the Instance Diagram tab is a caption of the panel for setting methods in a cell in an instance diagram.");
		languageEnProp.setProperty("ICMP_DialogTitle", "Methods");
		
		ConfigurationFiles.commentsMap.put("ICMP_Chcb_Show_Text", "Texts for JCheckBox components in a tool-setting panel in a cell representing an instance in an instance diagram, which is the text in the setup dialog on the instance diagram tab.");
		languageEnProp.setProperty("ICMP_Chcb_Show_Text", "Show methods.");
		languageEnProp.setProperty("ICMP_Chcb_ShowAll_Text", "Show all methods.");
		languageEnProp.setProperty("ICMP_Chcb_UseSameFontAndColorAsReference_Text", "Apply the same font and font color to the methods.");
		
		
		ConfigurationFiles.commentsMap.put("ICMP_Chcb_Show_TT", "Text labels for JCheckBox components in the panel for setting methods in a cell representing an instance in the instance diagram, these are texts in the setup dialog on the instance diagram tab.");
		languageEnProp.setProperty("ICMP_Chcb_Show_TT", "In the instance cell representing the instances in the instance diagram will be displayed the methods of the instance.");
		languageEnProp.setProperty("ICMP_Chcb_ShowAll_TT", "In the instance cell representing instances in the instance diagram will be displayed all methods of the instance (marked) or only the number of methods selected (unmarked).");
		languageEnProp.setProperty("ICMP_Chcb_UseSameFontAndColorAsReference_TT", "The methods will be rendered with the same font and color that will be set for the cell color (instance).");
		
		ConfigurationFiles.commentsMap.put("ICMP_Lbl_ShowSpecificCount_Text", "Label texts that tell the user that they can set a certain number of methods that can be displayed in the cell representing the instance in the instance diagram, these are the texts in the setup dialog on the instance diagram tab.");
		languageEnProp.setProperty("ICMP_Lbl_ShowSpecificCount_Text", "Show number of methods");
		languageEnProp.setProperty("ICMP_Lbl_ShowSpecificCount_TT", "Only the defined number of methods will be displayed in cells representing instances in the instance diagram.");
		
		ConfigurationFiles.commentsMap.put("ICMP_Lbl_FontSize", "Text labels that contain information that the user can set the font size and font color for methods that can be displayed in a cell representing the instance in the instance diagram, these are the texts in the setup dialog on the instance diagram tab.");
		languageEnProp.setProperty("ICMP_Lbl_FontSize", "Font size");
		languageEnProp.setProperty("ICMP_Lbl_FontStyle", "Font style");
		languageEnProp.setProperty("ICMP_Lbl_TextColor", "Font color");
		
		ConfigurationFiles.commentsMap.put("ICMP_BtnChooseColor", "Text for the 'Select' button that opens the Font Color Selection dialog for methods that can be displayed in a cell that represents an instance in the Instance Diagram - these are the texts in the Setup dialog box on the Instance Diagram tab.");
		languageEnProp.setProperty("ICMP_BtnChooseColor", "Choose");
		
		ConfigurationFiles.commentsMap.put("ICMP_Txt_Clr_DialogTitle", "Text to the Font Color Selection Subtitle Text for methods that can be displayed in a cell representing an instance in an Inventory Diagram This is a text in the caption of the dialog that can be opened by clicking on the 'Select' font for the method font. This is the text in the setup dialog on the instance diagram tab. ");
		languageEnProp.setProperty("ICMP_Txt_Clr_DialogTitle", "Select font color for methods");
		
		
		
		
		
		
		
		
		
		
		
		// Texty do panelu InstanceCellAttributesPanel v balíčku instanceDiagramPanel,
		// jedná se o texty pro komponenty, které slouží pro nastavení toho, zda a jak
		// se mají zobrazovat atributy v buňce, která v diagramu instancí reprezentuje
		// instance.
		ConfigurationFiles.commentsMap.put("ICAP_BorderTitle", "The title of the panel with components for setting whether or not how to display attributes in a cell representing an instance in an instance diagram, these are the texts in the dialog box of the Inventory Diagram tab for the Attributes panel.");
		languageEnProp.setProperty("ICAP_BorderTitle", "Attributes");
		
		ConfigurationFiles.commentsMap.put("ICAP_Chcb_Show_Text", "Texts for JCheckBox components in the Attribute Setting panel in a cell representing an instance in the Inventory Diagram, these are texts in the Setup dialog box on the Inventory Diagram tab for the Attributes panel.");
		languageEnProp.setProperty("ICAP_Chcb_Show_Text", "Show attributes.");
		languageEnProp.setProperty("ICAP_Chcb_ShowAll_Text", "Show all attributes.");
		languageEnProp.setProperty("ICAP_Chcb_UseSameFontAndColorAsReference_Text", "Apply the same font and font color to the attributes.");
		
		ConfigurationFiles.commentsMap.put("ICAP_Chcb_Show_TT", "Text labels for JCheckBox components are labels for example, whether to display attributes in a cell representing an instance, whether only a certain number or all attributes, etc. These are texts in the settings dialog on the Inventory Diagram tab for the attributes. ");
		languageEnProp.setProperty("ICAP_Chcb_Show_TT", "In the instance cell representing the instances in the instance diagram will be displayed the attributes of the instance.");
		languageEnProp.setProperty("ICAP_Chcb_ShowAll_TT", "In the instance cell representing instances in the instance diagram will be displayed all attributes of the instance (marked) or only number of attributes selected (unmarked).");
		languageEnProp.setProperty("ICAP_Chcb_UseSameFontAndColorAsReference_TT", "The attributes will be rendered with the same font and color that will be set for the cell color (instance).");
		
		ConfigurationFiles.commentsMap.put("ICAP_Lbl_ShowSpecificCount_Text", "Text for the label that tells the user that they can set a certain number of attributes in a given location, which should be displayed in a cell representing the instance in the Inventory Diagram. These are texts in the settings dialog on the Attributes Chart tab for the Attributes panel.");
		languageEnProp.setProperty("ICAP_Lbl_ShowSpecificCount_Text", "Show number of attributes");
		languageEnProp.setProperty("ICAP_Lbl_ShowSpecificCount_TT", "Only the defined number of attributes will be displayed in cells representing instances in the instance diagram.");
		
		ConfigurationFiles.commentsMap.put("ICAP_Lbl_FontSize", "Label texts that inform users that they can set the font and color of the font in which the attributes are rendered in the appropriate cell that represents the instance in the instance diagram, which is the text in the settings dialog on the instance diagrams tab for attribute panel. ");
		languageEnProp.setProperty("ICAP_Lbl_FontSize", "Font size");
		languageEnProp.setProperty("ICAP_Lbl_FontStyle", "Font style");
		languageEnProp.setProperty("ICAP_Lbl_TextColor", "Font color");
		
		ConfigurationFiles.commentsMap.put("ICAP_Btn_ChooseColor", "The text for the 'Select' button that opens the Font Color Selection dialog with which the attributes will be rendered in the appropriate cell that represents the instance in the Instance Diagram, which is the text in the Settings dialog box on the Inventory Diagram tab for the Attributes panel.");
		languageEnProp.setProperty("ICAP_Btn_ChooseColor", "Choose");
		
		ConfigurationFiles.commentsMap.put("ICAP_Txt_Clr_DialogTitle", "A font color selection title for attributes that can be displayed in a cell representing an instance in an instance diagram, these are the texts in the settings dialog on the Attributes Chart tab for the Attributes panel.");
		languageEnProp.setProperty("ICAP_Txt_Clr_DialogTitle", "Select font color for attributes");
		
		
		
		
		
		
		
		
		
		
		
		
		
		
				
				
		// Texty do CommandEditor:
		ConfigurationFiles.commentsMap.put("Sf_Ce_PanelBorderTitle", "Some text for component panel to set command editor parameters in setup dialog.");
		languageEnProp.setProperty("Sf_Ce_PanelBorderTitle", "Properties for the command editor");
		languageEnProp.setProperty("Sf_Ce_CheckBoxJavaHighlightingSyntax", "Java syntax highlighting.");
		languageEnProp.setProperty("Sf_Ce_LabelBackgroundColor", "Backround colour");

		// Text pro ohraničení OutputEditorPanelu:
		ConfigurationFiles.commentsMap.put("Sf_Oep_BorderTitle", "Text in the Setup dialog box to fade the panel with parameter settings for the output editor.");
		languageEnProp.setProperty("Sf_Oep_BorderTitle", "Properties for the editor outputs");
		
		ConfigurationFiles.commentsMap.put("Sf_Oep_Chcb_ShowReferenceVariablesText", "Texts in the settings dialog for the JCheckBox component to set whether to write to both the output and reference editor if the listed value is the instance that is in the instance diagram.");
		languageEnProp.setProperty("Sf_Oep_Chcb_ShowReferenceVariablesText", "Write reference variables.");
		languageEnProp.setProperty("Sf_Oep_Chcb_ShowReferenceVariableTT", "When you write an instance from an instance diagram (for example, after calling the method or obtaining an instance from a variable), it will also write its references.");
				
		// Text pro ohraničení OutputFramePanelu:
		ConfigurationFiles.commentsMap.put("Sf_Ofp_BorderTitle", "Text in the setup dialog box to bend the panel with parameter settings for the output terminal.");
		languageEnProp.setProperty("Sf_Ofp_BorderTitle", "Properties for output terminal");
				
				
		// Texty do SettingForm, do panelu ApplicationPanel - texty do dialogu pro umístění konfiguračních souborů:
		ConfigurationFiles.commentsMap.put("Sf_App_Fc_PlaceConfigurationDirText", "Texts in the settings dialog for the bookmark of an application, which is a text for labels that describe what configuration file (s) can be created.");
		languageEnProp.setProperty("Sf_App_Fc_PlaceConfigurationDirText", "Place the configuration directory");
		languageEnProp.setProperty("Sf_App_Fc_PlaceLanguageDirText", "Place a directory language");
		languageEnProp.setProperty("Sf_App_Fc_PlaceDefaultPropertiesFileText", "Place the file " + Constants.DEFAULT_PROPERTIES_NAME);
		languageEnProp.setProperty("Sf_App_Fc_PlaceClassDiagramPropertiesFileText", "Place the file " + Constants.CLASS_DIAGRAM_NAME);
		languageEnProp.setProperty("Sf_App_Fc_PlaceInstanceDiagramPropertiesFileText", "Place the file " + Constants.INSTANCE_DIAGRAM_NAME);
		languageEnProp.setProperty("Sf_App_Fc_PlaceCommandEditorPropertiesFileText", "Place the file " + Constants.COMMAND_EDITOR_NAME);
		languageEnProp.setProperty("Sf_App_Fc_PlaceClassEditorPropertiesFileText", "Place the file " + Constants.CODE_EDITOR_NAME);
		languageEnProp.setProperty("Sf_App_Fc_PlaceOutputEditorPropertiesFileText", "Place the file " + Constants.OUTPUT_EDITOR_NAME);
		languageEnProp.setProperty("Sf_App_Fc_PlaceOutputFramePropertiesFileText", "Place the file " + Constants.OUTPUT_FRAME_NAME);
		languageEnProp.setProperty("Sf_App_Fc_PlaceCodeEditorForInternalFramesFileText", "Place the file " + Constants.CODE_EDITOR_INTERNAL_FRAMES_NAME);
		// Text pro ohraničení panelu CommentEdgePanel - hrana ke komentáři v ClassDiagramPanelu:
		languageEnProp.setProperty("Sf_Cep_BorderTitle", "Edge comments");
				
				
				
		// Texty do CodeEditorPanelu:
		ConfigurationFiles.commentsMap.put("Sf_Cep_PanelBorderTitle", "Texts in the setup dialog for the component parameter panel for the source code editor, these are texts for some labels in that panel on the source code editor tab.");
		languageEnProp.setProperty("Sf_Cep_PanelBorderTitle", "Properties for source code editor");
		languageEnProp.setProperty("Sf_Cep_CheckBoxJavaHighlightingSyntax", "Highlight syntax.");
		languageEnProp.setProperty("Sf_Cep_CheckBoxJavaHighlightingSyntax_TT", "The open source text syntax will / will not be highlighted in the source code editor dialog.");
		languageEnProp.setProperty("Sf_Cep_CheckBoxHighlightingSyntaxTextByKindOfFile", "Highlight syntax by the open file .");
		languageEnProp.setProperty("Sf_Cep_CheckBoxHighlightingSyntaxTextByKindOfFile_TT", "For some file types, syntax highlighting (java, json, properties, xml, html, ...) will be used. For other file types, the Java-specific highlighting (default) will be used.");
		languageEnProp.setProperty("Sf_Cep_CheckBoxCompileClassInBackground", "Compile classes by pressing");
		languageEnProp.setProperty("Sf_Cep_CheckBoxCompileClassInBackground_TT", "Compiles the classes in an open project or a class open in the code editor. In the case of error-free compilation, the resulting values ​​(variables and methods) are added to the automatic code refil window.");


        // Texty do třídy: cz.uhk.fim.fimj.settings_form.FormattingPropertiesPanel
        ConfigurationFiles.commentsMap.put("Fpp_BorderTitle", "Texts in the panel settings dialog for setting the source code formatting properties in the source code editor dialog and the project explorer dialog.");
        languageEnProp.setProperty("Fpp_BorderTitle", "Formatting the source code");
        languageEnProp.setProperty("Fpp_DeleteEmptyLine_Text", "Delete blank lines.");
        languageEnProp.setProperty("Fpp_DeleteEmptyLine_TT", "Blank lines within code methods / blocks will be removed.");
        languageEnProp.setProperty("Fpp_TabSpaceConversion_Text", "Convert tab to spaces.");
        languageEnProp.setProperty("Fpp_TabSpaceConversion_TT", "Tab (white characters) will be converted to spaces.");
        languageEnProp.setProperty("Fpp_BreakElseIf_Text", "Wrap syntax");
        languageEnProp.setProperty("Fpp_BreakElseIf_TT", "'else' headers will be broken from their succeeding 'if' headers.");
        languageEnProp.setProperty("Fpp_SetSingleStatement_Text", "Wrap commands to multiple rows.");
        languageEnProp.setProperty("Fpp_SetSingleStatement_TT", "One command per line. ' If there are multiple commands on one line, for example, two call methods. Commands will be divided into two rows.");
        languageEnProp.setProperty("Fpp_BreakOneLineBlock_Text", "Wrap single line code blocks.");
        languageEnProp.setProperty("Fpp_BreakOneLineBlock_TT", "Blocks of code that can be written to one row will be wrapped in multiple rows.");
        languageEnProp.setProperty("Fpp_BreakClosingHeaderBracket_Text", "Wrap the code behind the closing bracket.");
        languageEnProp.setProperty("Fpp_BreakClosingHeaderBracket_TT", "The code behind the closing bracket will be wrapped in a new line.");
        languageEnProp.setProperty("Fpp_BreakBlock_Text", "Separate unrelated blocks.");
        languageEnProp.setProperty("Fpp_BreakBlock_TT", "Unrelated code blocks will be separated by blank lines. For example, under the closing bracket, a blank line will be left to separate the next block of code.");
        languageEnProp.setProperty("Fpp_OperatorPaddingMode_Text", "Separate operators from operands by spaces.");
        languageEnProp.setProperty("Fpp_OperatorPaddingMode_TT", "Gaps will be added between operators and operands.");
				
				
		// Texty do ProjectExplorerCodeEditorPanelu:
		ConfigurationFiles.commentsMap.put("Sf_Pecep_PanelBorderTitle", "Label for Settings Dialog for Parameter Setting Parameters for Source Code Editor in the Project Explorer on the Project Explorer Tab - Code Editor.");
		languageEnProp.setProperty("Sf_Pecep_PanelBorderTitle", "Properties for code dditor in project explorer");
				
				
				
				
				
				
				
		// NewClassForm.java - texty do tohoto dialogu:
		// Titulek dialogu:
		ConfigurationFiles.commentsMap.put("NcfDialogTitle", "Text to dialog to create a new class.");
		languageEnProp.setProperty("NcfDialogTitle", "Create a new class");
		languageEnProp.setProperty("NcfLabelClassName", "Class name");
		languageEnProp.setProperty("NcfBorderTitleClassType", "Class type");
		languageEnProp.setProperty("NcfLblPackageName", "Package");
		languageEnProp.setProperty("NcfRadioButtonClass", "Class");
		languageEnProp.setProperty("NcfRadioButtonAbstractClass", "Abstract class");
		languageEnProp.setProperty("NcfRadioButtonInterface", "Interface");
		languageEnProp.setProperty("NcfRadioButtonApplet", "Applet");
		languageEnProp.setProperty("NcfRadioButtonEnum", "Enum");
		languageEnProp.setProperty("NcfRadioButtonUnitTest", "Unit test");
		
		// tlačítka:
		ConfigurationFiles.commentsMap.put("NcfButtonOk", "Text to the buttons in the dialog box to create a new class.");
		languageEnProp.setProperty("NcfButtonOk", "OK");
		languageEnProp.setProperty("NcfButtonCancel", "Cancel");
		
		// Chybové hlášky:
		ConfigurationFiles.commentsMap.put("NcfErrorEmptyClassNameTitle", "Text for error messages that may occur in the dialog box to create a new class.");
		languageEnProp.setProperty("NcfErrorEmptyClassNameTitle", "Missing class name");
		languageEnProp.setProperty("NcfErrorEmptyClassNameText", "There is no specified class name!");
		languageEnProp.setProperty("NcfErrorFormatTitle", "Wrong format");
		languageEnProp.setProperty("NcfErrorFormatText", "The format of the class name is not correct! \nThis class must begin with a capital letter, can contain only uppercase and lowercase letters, numbers, dot and '_' (underscore)!");
		languageEnProp.setProperty("NcfErrorClassNameTitle", "Wrong format");
		languageEnProp.setProperty("NcfErrorClassNameText", "The class name must not end with '.' (dot)!");
		languageEnProp.setProperty("NcfErrorKeyWordInClassNameTitle", "Wrong name");
		languageEnProp.setProperty("NcfErrorKeyWordInClassNameText", "The class name or package (for import) must not be the keywords of Java! \nError: Name");
		languageEnProp.setProperty("NcfDuplicateClassNameErrorTitle", "Duplication title");
		languageEnProp.setProperty("NcfDuplicateClassNameErrorText", "New class name is identical to the original, change it!");				
		languageEnProp.setProperty("Ncf_WrongNameOfClassText", "The class name is not entered in the correct syntax, ie, it must begin with a capital letter and can only contain numerals, underscores, uppercase and lowercase without diacritics.");
		languageEnProp.setProperty("NcfWrongNameOfClassTitle", "Wrong class name");
		languageEnProp.setProperty("NcfTxtError", "Error");	
		languageEnProp.setProperty("Ncf_TextClassAlreadyExistInSamePackageText", "The specified class already exists (in the same package), please choose another class name or package.");
		languageEnProp.setProperty("Ncf_TextClassAlreadyExistInSamePackageTitle", "The class already exists");
		
		// Texty pro chybovou hlášku - že byl zadán balíček / balíčky v chybné syntaxi:		
		ConfigurationFiles.commentsMap.put("Ncf_TxtPackageIsInWrongSyntaxText_1", "Text for the error message in the dialog for creating a new class about the user entering a package where the new class class should be placed in the wrong format.");
		languageEnProp.setProperty("Ncf_TxtPackageIsInWrongSyntaxText_1", "The package is not entered in the correct syntax, i.e. it can contain only uppercase and lowercase characters without diacritics, digits, and underscore.");
		languageEnProp.setProperty("Ncf_TxtPackageIsInWrongSyntaxText_2", "The individual directories (packages) can be separated by a decimal point that can not be at the beginning or at the end.");
		languageEnProp.setProperty("Ncf_TxtPackageIsInWrongSyntaxText_3", "Entered");
		languageEnProp.setProperty("Ncf_TxtPackageIsInWrongSyntaxTitle", "Wrong package");
				
				
				
				
				
				
				
				
		// Tato třídy chybové hlášky do metoda: existSrcDirectory():
		ConfigurationFiles.commentsMap.put("WtfProjectDirErrorTitle", "Texts for error messages that can occur in methods in the WriteToFile class, specifically in the existBinDirectory and existenSrcDirectory methods that are used to get the path to the src and bin directories in an open project.");
		languageEnProp.setProperty("WtfProjectDirErrorTitle", "Error project directory");
		languageEnProp.setProperty("WtfProjectDirErrorText", "Project directory not found!");
				
		languageEnProp.setProperty("WtfProjectSrcDirJopFailedToCreateErrorTitle", "Error creating directory");
		languageEnProp.setProperty("WtfProjectSrcDirJopFailedToCreateErrorText", "An error occurred while creating the src directory of the project is open!");
				
				
				
				
		// Třída WriteToFile, metoda: existSrcDirectory:
		ConfigurationFiles.commentsMap.put("WtfProjectBinDirErrorTitle", "Text for the next error message that can occur in the existBinDirectory method in the WriteToFile class for attempting to create a directory.");
		languageEnProp.setProperty("WtfProjectBinDirErrorTitle", "Error while creating directory");
		languageEnProp.setProperty("WtfProjectBinDirErrorText", "An error occurred while creating the bin directory in the directory open project!");
				
				
				
				
				
				
		// Tato třída texty komentářů v metodě: createClass()
		ConfigurationFiles.commentsMap.put("WtfCommentForClass", "Texts that serve as 'comments' in class-created applications.");
		languageEnProp.setProperty("WtfCommentForClass", "Here you can write a comment on this class.");
		languageEnProp.setProperty("WtfCommentConstructor", "Class constructor.");
		
		// Texty chybových hláěek:
		ConfigurationFiles.commentsMap.put("EtfErrorMessage1", "Text for error messages that can occur when creating classes.");
		languageEnProp.setProperty("EtfErrorMessage1", "The class named");
		languageEnProp.setProperty("EtfErrorMessage2", "in the folder");
		languageEnProp.setProperty("EtfErrorMessage3", "already exists, please choose another name!");
		languageEnProp.setProperty("EtfErrorMessageTitle", "Duplicate name");
				
				
				
				
				
				
		// Texty do chybových hlášek ve třídě: TestAndCreateEdge.java:
		ConfigurationFiles.commentsMap.put("TacMissingClassText", "Text for error messages for methods in the TestAndCreateEdge class, such as buzzwords that can occur in methods such as the Add Edge Type, Inheritance, etc. Methods in both the class diagram and the source codes of the appropriate classes.");
		languageEnProp.setProperty("TacMissingClassText", "At least one of the designated classes is missing!");
		languageEnProp.setProperty("TacMissingClassTitle", "Class error");
		languageEnProp.setProperty("TacInheritErrorText", "'Source' class - the cell has inherited from something! \nIn Java there is no multiple inheritance!");
		languageEnProp.setProperty("TacInheritErrorTitle", "Error inheritance");
		languageEnProp.setProperty("TacEnumInheritErrorText", "The Enum can not implement interface!");
		languageEnProp.setProperty("TacEnumInheritErrorTitle", "Error implementation");
		languageEnProp.setProperty("TacInterfaceErrorText", "The interface can not implement interface!");
		languageEnProp.setProperty("TacInterfaceErrorTitle", "Error Interface");
		languageEnProp.setProperty("TacImplementsErrorText_1", "A class can only implement the interface!\nClass");
		languageEnProp.setProperty("TacImplementsErrorText_2", "is not interface!");
		languageEnProp.setProperty("TacImplementsErrorTitle", "Error implementation");
		languageEnProp.setProperty("TacSelectedClassErrorText", "Labeled class was not found! \nPath to class");
		languageEnProp.setProperty("TacSelectedClassErrorTitle", "Error class");
		languageEnProp.setProperty("TacMissingSrcDirectoryText", "src directory was not found in the project directory, please create it or open another project!\n Path");
		languageEnProp.setProperty("TacMissingSrcDirectoryTitle", "Error src directory");
		languageEnProp.setProperty("TacEnumInheritErrorText_2", "Enum can not inherit!");
		languageEnProp.setProperty("TacEnumInheritErrorTitle_2", "Error inheritance");
		languageEnProp.setProperty("Tac_WrongSelectedObjectText", "At least one object is not a class, but comment!");
		languageEnProp.setProperty("Tac_WrongSelectedObjectTitle", "Misnomer objects");
		languageEnProp.setProperty("Tac_ChoosedCommentNotClassText", "Class was not marked, but the commentary, it can not connect another comment!");
		languageEnProp.setProperty("Tac_ChoosedCommentNotClassTitle", "marked comment");
				
				
				
				
				
				
				
		// Info ohledně nápoědy do třídy OutpuEditor - editor výstupů
		// Pouze základní informace o příkazu s nápovědou:
		ConfigurationFiles.commentsMap.put("Oe_InfoAboutCommands", "The text for the input information in the output editor that makes it possible to list the available commands for the output editor by the appropriate method and some keyboard shortcuts.");
		languageEnProp.setProperty("Oe_InfoAboutCommands", "You can view a list of commands by the method");
        // Klávesové zkratky pro práci s editorem příkazů, které je vhodné znát, když s aplikací uživatel pracuje poprve:
        languageEnProp.setProperty("Oe_InfoForCtrl_Enter", "Command confirmation.");
        languageEnProp.setProperty("Oe_InfoForEnter_UpDown", "Browsing the history of confirmed commands.");
        languageEnProp.setProperty("Oe_InfoForShift_Enter", "New line without command confirmation.");
        languageEnProp.setProperty("Oe_InfoForCtrl_Space", "Available commands");
        languageEnProp.setProperty("Oe_InfoForCtrl_H", "Command history in an open project");
        languageEnProp.setProperty("Oe_InfoForCtrl_Shift_H", "Command history from application startup");
				
				
				
				
				
				
		// Texty do chybových hlášek do třídy EditClass.java:
		ConfigurationFiles.commentsMap.put("EcMissingClassTitle", "Text for error messages in the EditClass class, which includes methods for editing class source codes, for example, to delete or add variables, inheritance, interface implementations, etc.");
		languageEnProp.setProperty("EcMissingClassTitle", "Error class");
		languageEnProp.setProperty("EcMissingClassText_1", "Class / Classes were not found!\nClass 1");
		languageEnProp.setProperty("EcMissingClassText_2", "\nClass 2");
				
				
				
				
				// Texty do třídy: ClassFromFileForm.java:
		// tuitulek dialogu:
		ConfigurationFiles.commentsMap.put("Cfff_DialogTitle", "The dialog title for loading a class into an open project using the 'Add a class from a file' button in the 'Edit' menu.");
		languageEnProp.setProperty("Cfff_DialogTitle", "Class load");
		
		// texty popisků:
		ConfigurationFiles.commentsMap.put("Cfff_LabelChooseClass", "Labels in the dialog box to add a new class using the 'Add a class from a file' button.");
		languageEnProp.setProperty("Cfff_LabelChooseClass", "Select the class (file extension '.java')");
		languageEnProp.setProperty("Cfff_LabelPlace", "Location");
		languageEnProp.setProperty("Cfff_LabelClassName", "Enter the name of the class");
		languageEnProp.setProperty("Cfff_LabelClassNameTt", "You can change the class name and place it in a package");
		
		// Texty tlačítek:
		ConfigurationFiles.commentsMap.put("Cfff_ButtonOk", "Add text to the buttons in the dialog box to add a new class using the 'Add class from file' button.");
		languageEnProp.setProperty("Cfff_ButtonOk", "OK");
		languageEnProp.setProperty("Cfff_ButtonCancel", "Cancel");
		languageEnProp.setProperty("Cfff_ButtonBrowse", "Browse");
		
		// texty chybových hlášek:
		ConfigurationFiles.commentsMap.put("Cfff_Jop_FormatErrorTitle", "Text for error messages that can occur in the dialog box to add a new class using the 'Add class from file' button.");
		languageEnProp.setProperty("Cfff_Jop_FormatErrorTitle", "Format error");
		languageEnProp.setProperty("Cfff_Jop_PathFormatErrorText", "The path to the file is in the wrong format!");
		languageEnProp.setProperty("Cfff_Jop_EmptyFieldErrorTitle", "Empty field");
		languageEnProp.setProperty("Cfff_Jop_EmptyFieldErrorText", "Any field is blank!");
				
				
				
				
				
		// Texty do PopupMenu v classDiagramu:
		ConfigurationFiles.commentsMap.put("Ppm_ItemRename", "Text in the context menu above the objects in the class diagram.");
		languageEnProp.setProperty("Ppm_ItemRename", "Rename");
		languageEnProp.setProperty("Ppm_ItemRemove", "Remove");
		languageEnProp.setProperty("Ppm_ItemOpenInCodeEditor", "In the code editor");
		languageEnProp.setProperty("Ppm_ItemOpenInProjectExplorer", "In the project explorer");
        languageEnProp.setProperty("Ppm_ItemOpenInExplorerInOs_Text", "In the file explorer");
        languageEnProp.setProperty("Ppm_ItemOpenInExplorerInOs_TT", "Opens the file explorer and marks the file in it.");
        languageEnProp.setProperty("Ppm_ItemOpenInDefaultAppInOs_Text", "In the default application");
        languageEnProp.setProperty("Ppm_ItemOpenInDefaultAppInOs_TT", "Opens the marked file in an application set up in the OS to open a specific file type.");
        languageEnProp.setProperty("Ppm_ItemCopyAbsolutePath", "Absolute Path");
        languageEnProp.setProperty("Ppm_ItemCopyRelativePath", "Relative path");
        languageEnProp.setProperty("Ppm_ItemCopyReference", "Reference");
		languageEnProp.setProperty("Ppm_ItemOpenInEditor", "Open");
        languageEnProp.setProperty("Ppm_MenuCopyReference", "Copy");
		languageEnProp.setProperty("Ppm_MenuStaticMethods", "Static methods");
		languageEnProp.setProperty("Ppm_MenuInheritedStaticMethods", "Inherited static methods");
		languageEnProp.setProperty("Ppm_MenuInnerStaticClasses", "Inner static classes");
		languageEnProp.setProperty("Ppm_MenuConstructors", "Constructors");
		
		ConfigurationFiles.commentsMap.put("Ppm_ClassIsInRelationShipAlreadyTitle", "Texts into error messages that may occur when you perform one of the methods in the context menu above the objects in the class diagram.");
		languageEnProp.setProperty("Ppm_ClassIsInRelationShipAlreadyTitle", "Error relationship");
		languageEnProp.setProperty("Ppm_ClassIsInRelationShipAlreadyText", "The class is in relation to another class, if you want to rename the marked class, cancel this relationship!");
		languageEnProp.setProperty("Ppm_MissingClassTitle", "Could not find the class in the src directory in open project!");
		languageEnProp.setProperty("Ppm_MissingClassText", "Missing class");
		languageEnProp.setProperty("Ppm_Path", "Path");
		
		// První text do prvně vytvořené bu�?ky, která reprezentuje komentář:
		ConfigurationFiles.commentsMap.put("FirstTextToCommentCell", "Default text that will contain a cell that represents a comment in the class diagram when it is created.");
		languageEnProp.setProperty("FirstTextToCommentCell", "Post a comment");
		
		// Texty kvůli spusteni - zavolani metody main:
		ConfigurationFiles.commentsMap.put("Ppm_Txt_FailedToCallTheMainMethod", "Text to error messages that can occur when trying to call the 'main' method by using the context menu above the cell representing the class in the class diagram.");
		languageEnProp.setProperty("Ppm_Txt_FailedToCallTheMainMethod", "Unable to call the 'main' method.");
		languageEnProp.setProperty("Ppm_Txt_Error", "Error");
		languageEnProp.setProperty("Ppm_Txt_MethodIsInaccessible", "The method is inaccessible");
		languageEnProp.setProperty("Ppm_Txt_IlegalArgumentException", "The object is not an instance of the class or interface. They differ in the number of parameters. Or failed conversion parameters.");
		languageEnProp.setProperty("Ppm_Txt_InvocationTargetException", "The method raised an exception, unknown error.");
		languageEnProp.setProperty("Ppm_Txt_Start", "Start");
		languageEnProp.setProperty("Ppm_Txt_FailedToCreateInstanceText", "Failed to create an instance of the class");
		languageEnProp.setProperty("Ppm_Txt_FailedToCreateInstanceTitle", "Failed to create");
		// Hláška 'Úspěch' po zavolání statické metody:
		languageEnProp.setProperty("Ppm_Txt_Success", "Success");
        // V případě, že se nepodaří naléz cesty pro zkopírování do schránky:
        languageEnProp.setProperty("Ppm_Txt_NotSpecified", "Not specified");
		// Text: 'Referenční proměnná' info za text proměnné pokud je hodnota instance z
		// ID:
		languageEnProp.setProperty("Ppm_Txt_ReferenceVariable", "reference variable");
				
				
				
				
				
				
				
		// Některé texty do třídy - RenameClassForm:
		ConfigurationFiles.commentsMap.put("Rcf_DialogTitle", "Dialog text for renaming a class. The dialog can be displayed using the context menu above the cell representing the class in the class diagram.");
		languageEnProp.setProperty("Rcf_DialogTitle", "Rename class");
		languageEnProp.setProperty("Rcf_LabelNameOfClass", "Class name");	
				
				
				
				
				
				
				
				
				// Třída: NewInstanceForm - veškeré texty::
		ConfigurationFiles.commentsMap.put("Nif_DialogTitle", "Texts in a dialog that is used to retrieve data from a user to create a new class instance from a class diagram.");
		languageEnProp.setProperty("Nif_DialogTitle", "Create an instance of an object");
		languageEnProp.setProperty("Nif_LabelInfoText", "In the following text fields, enter the required values ​​for creating an instance.");
		languageEnProp.setProperty("Nif_LabelReferenceName", "Name reference to an instance of the class");
		
		ConfigurationFiles.commentsMap.put("Nif_Button_OK", "Buttons for dialog buttons that are used to retrieve data from a user to create a new class instance from the class diagram.");
		languageEnProp.setProperty("Nif_Button_OK", "OK");
		languageEnProp.setProperty("Nif_Button_Cancel", "Cancel");

		
		// Texty do chybových hlášek:
		ConfigurationFiles.commentsMap.put("Nif_SomeFieldIsEmptyTitle", "Text for error messages that may occur in the dialog box to create an instance of a class when you try to create a class instance from a class diagram.");
		languageEnProp.setProperty("Nif_SomeFieldIsEmptyTitle", "Empty field");
		languageEnProp.setProperty("Nif_SomeFieldIsEmptyText", "field parameter value is empty!");
		languageEnProp.setProperty("Nif_ConstructorParametrFormatErrorTitle", "Wrong format");
		languageEnProp.setProperty("Nif_ConstructorParametrFormatErrorText", "parameter constructor is not in the correct format!");
		languageEnProp.setProperty("Nif_NameOfReferenceIsAlreadyTakenTitle", "Duplicate name");
		languageEnProp.setProperty("Nif_NameOfReferenceIsAlreadyTakenText", "Name reference to the object instance is busy, please choose another!");
		languageEnProp.setProperty("Nif_ReferenceNameFormatErrorTitle", "Wrong format");
		languageEnProp.setProperty("Nif_ReferenceNameFormatErrorText", "Name reference to an instance of an object is not in the correct format!");
		languageEnProp.setProperty("Nif_ReferenceFieldIsEmptyTitle", "Empty field");
		languageEnProp.setProperty("Nif_ReferenceFieldIsEmptyText", "The reference name for a class class instance is not specified!");
		languageEnProp.setProperty("Nif_BadFormatSomeParameterTitle", "Wrong format");
		languageEnProp.setProperty("Nif_BadFormatSomeParameterText", "The following values are entered in the wrong format!");
		languageEnProp.setProperty("Nif_BadFormatReferenceNameText", "Name reference to an instance of the class can only contain lowercase, uppercase, numbers (not her start), and underscore!");
		languageEnProp.setProperty("Nif_CannotCallTheConstructorTitle", "You can not call the constructor");
				
				
		// Texty jsou ve třídě FlexibleCountOfParameters:
		// Texty pro List:
		ConfigurationFiles.commentsMap.put("Fcop_TextListStringFormatError", "Text for error messages for the method call and instantiation dialogs These are texts for specifying a method or constructor parameter with information about the values ​​that can be entered as a listed data type list These will be displayed when a wrong syntax value is entered for the parameter.");
		languageEnProp.setProperty("Fcop_TextListStringFormatError", "The values in the list of type String can contain any characters in normal (double) quotes - syntax: [\"My text. \" \"More text. \"]");
		languageEnProp.setProperty("Fcop_TextListByteFormatError", "Values in list type Byte can contain only numbers in the interval");
		languageEnProp.setProperty("Fcop_TextListShortFormatError", "Values in list type Short can contain only numbers in the interval");
		languageEnProp.setProperty("Fcop_TextListIntegerFormatError", "Values in list type Integer may contain only numbers in the interval");
		languageEnProp.setProperty("Fcop_TextListLongFormatError", "Values in the list type Long can contain only numbers in the interval");
		languageEnProp.setProperty("Fcop_TextListCharacterFormatError", "Values in list type Character can contain any characters in single quotes ('X'), each single character in quotation mark, besides the actual quotes.");
		languageEnProp.setProperty("Fcop_TextListBooleanFormatError", "Values ​​in a list type Boolean can only contain true or false logical values.");																		
		languageEnProp.setProperty("Fcop_TextListFloatFormatError", "Values in list type Float can contain only numbers in the interval");
		languageEnProp.setProperty("Fcop_TextListDoubleFormatError", "Values in list type Double can contain only numbers in the interval");
		
		// Texty pro kolekce:
		ConfigurationFiles.commentsMap.put("Fcop_TextCollectionByteFormatError", "Text for error messages for dialog for method call and instance creation These are texts for specifying a parameter of a method or constructor with information about values ​​that can be entered as a collection type parameter, which will be displayed if the syntax error is entered for parameter.");
		languageEnProp.setProperty("Fcop_TextCollectionByteFormatError", "Values ​​in the Byte collection can only contain numbers in the interval");
		languageEnProp.setProperty("Fcop_TextCollectionShortFormatError", "Values ​​in the Short Collection can only contain numbers in the interval");
		languageEnProp.setProperty("Fcop_TextCollectionIntegerFormatError", "Values ​​in the Integer collection can only contain numbers in the interval");
		languageEnProp.setProperty("Fcop_TextCollectionLongFormatError", "Values ​​in the Long collection can only contain numbers in the interval");
		languageEnProp.setProperty("Fcop_TextCollectionCharacterFormatError", "The values ​​in the Character collection can contain any characters in single quotation marks ('X'), each one in the quote, except the quotation mark itself.");
		languageEnProp.setProperty("Fcop_TextCollectionBooleanFormatError", "Values ​​in a Boolean collection can only contain logical values ​​true or false.");
		languageEnProp.setProperty("Fcop_TextCollectionFloatFormatError", "Values ​​in the Float collection can only contain numbers in the interval");
		languageEnProp.setProperty("Fcop_TextCollectionDoubleFormatError", "Values ​​in the Double collection can only contain numbers in the interval");
		languageEnProp.setProperty("Fcop_TextCollectionStringFormatError", "Values ​​in a String collection can contain any characters in normal (double) quotes - syntax: [\" My Text. \", \" Additional Text.\"]");
				
		// Texty pro jednorozměrné pole:
		ConfigurationFiles.commentsMap.put("Fcop_TextArraryFormatErrorForAll_1", "Text for error messages for the method call and instantiation dialogs These are texts for specifying a parameter of a method or constructor with information about the values ​​that can be entered as the field data parameter parameter mentioned These will be displayed when a wrong syntax value is entered for the parameter.");
		languageEnProp.setProperty("Fcop_TextArraryFormatErrorForAll_1", "The values in the one-dimensional array type");
		languageEnProp.setProperty("Fcop_TextArrayFormatErrorAll_2", "can contain only numbers in the interval");
		languageEnProp.setProperty("Fcop_TextArrayCharacterFormatError_2", "they can contain any characters in single quotes ('X'), each single character in quotation mark, besides the actual quotes.");
		languageEnProp.setProperty("Fcop_TextArrayBooleanFormatError_2", "they can contain only the logical values like true or false.");
		languageEnProp.setProperty("Fcop_TextArrayStringFormatError_2", "can contain any characters in normal (double) quotes (\"My text.\").");
				
				
		// Výchozí text pro pole a list:
		ConfigurationFiles.commentsMap.put("Fcop_DefaultTextForStringListAndArray_1", "Default Texts for Sheet and Field Data Type Values ​​are the default values ​​that appear in the dialog for Call Methods or Constructor with Parameters / a Type or Field Type Parameter.");
		languageEnProp.setProperty("Fcop_DefaultTextForStringListAndArray_1", "My text.");
		languageEnProp.setProperty("Fcop_DefaultTextForStringListAndArray_2", "My next text.");
		
				
		// Hodnoty pro dvojrozměrné pole - chybové hlášky ohledně správného formátu syntaxe pro dvojrozměrné pole:
		ConfigurationFiles.commentsMap.put("Fcop_Text_2_ArrayFormatErrorAll_1", "Start text in error message if erroneous values ​​are entered for two-dimensional array.");
		languageEnProp.setProperty("Fcop_Text_2_ArrayFormatErrorAll_1", "The values in the two-dimensional array type");
			
				
		// Proměnné - resp. texty do chybového oznámení, pokud se jedná o nepodporovaný datový typ parametru:
		ConfigurationFiles.commentsMap.put("Fcop_TextDataTypeOfParameter", "Texts for error notifications when data types of any method or constructor parameter are not supported in the dialog to call the method or constructor.");
		languageEnProp.setProperty("Fcop_TextDataTypeOfParameter", "Datatype parameter");
		languageEnProp.setProperty("Fcop_TextIsNotSupportedForConstructor", "is not supported, you can not call the constructor!");
		languageEnProp.setProperty("Fcop_TextIsNotSupportedForMethod", "not supported, the method can not call!");
		languageEnProp.setProperty("Fcop_TetxtConstructorCannotCall", "Data types more than one parameter is not supported, you can not call the constructor!");
		languageEnProp.setProperty("Fcop_TextMethodCannotCall", "Data types more than one parameter is not supported, the method can be called!");
				
		// Chybové hlášky do editoru výstupů v případě, že se nepodaří vytvořit instanci třídy:
		ConfigurationFiles.commentsMap.put("Fcop_TextFailedToCreateInstanceOfClass", "Text to error messages in the output editor if you can not create intanci of the three.");
		languageEnProp.setProperty("Fcop_TextFailedToCreateInstanceOfClass", "Failed to create an instance of the class");
		languageEnProp.setProperty("Fcop_TextPossibleErrors", "the most common cause may be following, it is an abstract class or interface, fields, primitive type or void, or missing class constructor or some other reason. Passed a null value.");
		languageEnProp.setProperty("Fcop_TextPossibleErrorsWithPrivateType", "possible cause may be that the class or constructor is not accessible. Passed a null value.");
		
		// Texty pro chybové výpisy, že nebyly nalezeny žádné instance v diagramu instancí, které by bylo možné předat do parametru metody nebo konstruktoru, takže jej nepůjde zavolat.
		ConfigurationFiles.commentsMap.put("Fcop_TextInsanceNotFoundMethodCantBeCall", "Text for error statements if no instance could be found that could be passed to the method or constructor parameter, so it can not be called.");
		languageEnProp.setProperty("Fcop_TextInsanceNotFoundMethodCantBeCall", "No instances for handover found. The method can not be called");
		languageEnProp.setProperty("Fcop_TextInsanceNotFoundConstructorCantBeCall", "No instances for handover were found. The constructor can not be called.");
				
				
				
				
				
				
				
				
				
				
				
		// třída SelectMaimMethodForm.java:
		// název dialogu:
		ConfigurationFiles.commentsMap.put("Smmf_DialogTitle", "The title for the dialog that is used to indicate the 'main' method to be called This dialog will be displayed if the user has chosen to run the project and at least two classes have been found that contain the main 'main' method.");
		languageEnProp.setProperty("Smmf_DialogTitle", "Calling 'main' method");

		// Labely:
		ConfigurationFiles.commentsMap.put("Smmf_LabelInfo_1", "Text labels with information for the user to choose the 'main' method that will be called up to run the project Texts in the dialog to indicate the 'main' method when the project starts, the dialog will be displayed if there is more than one class, which contains the 'main' method.");
		languageEnProp.setProperty("Smmf_LabelInfo_1", "The open project contains several main - startup methods 'main'.");
		languageEnProp.setProperty("Smmf_LabelInfo_2", "Below, mark the class where you want to call the main method.");

		// Tlačítka:
		ConfigurationFiles.commentsMap.put("Smmf_Button_OK", "Button texts in the dialog to indicate the 'main' method to run the project.");
		languageEnProp.setProperty("Smmf_Button_OK", "OK");
		languageEnProp.setProperty("Smmf_Button_Cancel", "Cancel");
		
		ConfigurationFiles.commentsMap.put("Smmf_TxtIsNotChoosedClassText", "Text for the error message in the dialog to indicate the 'main' method to start the project, the message appears if the class is not marked for the 'main' method.");
		languageEnProp.setProperty("Smmf_TxtIsNotChoosedClassText", "There is no indication of the class in which the main method is to call!");
		languageEnProp.setProperty("Smmf_TxtIsNotChoosedClassTitle", "Unmarked item");
				
				
				
				
				
				
				
				
				
				
				
				
				
		// Třída: GetParametersMethodForm:
		ConfigurationFiles.commentsMap.put("Gpmf_DialogTitle", "Dialog title for call method (using graphical interface, not command editor).");
		languageEnProp.setProperty("Gpmf_DialogTitle", "Calling methods");
		ConfigurationFiles.commentsMap.put("Gpmf_LabelTextInfo", "Text to label in dialog to call method.");
		languageEnProp.setProperty("Gpmf_LabelTextInfo", "In these fields, enter the method parameters.");
		
		ConfigurationFiles.commentsMap.put("Gpmf_Button_Ok", "Text to the buttons in the dialog to call the method.");
		languageEnProp.setProperty("Gpmf_Button_Ok", "OK");
		languageEnProp.setProperty("Gpmf_Button_Cancel", "Cancel");
		
		// Tetxty do chybových hlášek:
		ConfigurationFiles.commentsMap.put("Gpmf_JopEmptyFieldParameterTitle", "Text for error messages that can occur in the dialog to call the method, for example, if the field for entering the value of the method parameter is not filled, or it is in the wrong format, etc.");
		languageEnProp.setProperty("Gpmf_JopEmptyFieldParameterTitle", "Empty field");
		languageEnProp.setProperty("Gpmf_JopEmptyFieldParameterText", "Field parameter value is blank!");
		languageEnProp.setProperty("Gpmf_JopFormatErrorParameterValueTitle", "Wrong format");
		languageEnProp.setProperty("Gpmf_JopFormatErrorParameterValueText", "parameter value is in the wrong format!");
		languageEnProp.setProperty("Gpmf_JopBadFormatSomeParameterTitle", "Wrong format");
		languageEnProp.setProperty("Gpmf_JopBadFormatSomeParameterText", "Any parameter value is entered in the wrong format, see the box with error buzz!");
		languageEnProp.setProperty("Gpmf_JopCannotCallTheMethod", "Method cannot be called");
				
				
				
				
				
				
				
				
		// Třída CompileClass.java - texty do hlášek:
		ConfigurationFiles.commentsMap.put("Cp_TextError", "Texts for class compilation report texts, texts for both successful and unsuccessful listings in the output editor.");
		languageEnProp.setProperty("Cp_TextError", "Error");
		languageEnProp.setProperty("Cp_TextLine", "Line");
		languageEnProp.setProperty("Cp_TextClass", "Class");
		languageEnProp.setProperty("Cp_TextSourceNotFound", "Source not found (obtained null).");
		languageEnProp.setProperty("Cp_TextClassNotFound", "Class at the specified path can not be found, not compile!");
		languageEnProp.setProperty("Cp_TextPath", "Path");
		
		ConfigurationFiles.commentsMap.put("Cp_AllClassesAreCompiled", "Texts for reports of class compilation reports are texts with information about a particular compile error or compilation, and listings are written to the output editor.");
		languageEnProp.setProperty("Cp_AllClassesAreCompiled", "All classes in the class diagram are compiled.");
		languageEnProp.setProperty("Cp_AllClassesAreNotCompiled", "At least one class of class diagrams failed to compile, below is an overview of errors.");
		languageEnProp.setProperty("Cp_ClassesNotFound", "It is not transmitted files, respectively. Classes from class diagram, you can not compile!");
		languageEnProp.setProperty("Cp_ClassDiagramIsEmptyForCompilation", "There is no class in the class diagram to compile!");
				
		// Texty k chybě při vytváření kompilátoru - možné chyby:
		ConfigurationFiles.commentsMap.put("Cp_TextErrorWhileCreatingCompilatortext", "Text for error messages that may occur when a compiler compilation (/ translation) class is failed.");
		languageEnProp.setProperty("Cp_TextErrorWhileCreatingCompilatortext", "An error occurred while creating \"compiler\"!");
		languageEnProp.setProperty("Cp_TextPossibleErrors", "This error is most likely caused by one of the following options");
		languageEnProp.setProperty("Cp_TextIsNotSetPathToJavaHomeDir", "There is no set path for the root directory of Java.");
		languageEnProp.setProperty("Cp_TextMissingToolsJarFile", "The aforementioned folder contains a file tools.jar or not the current version.");
		languageEnProp.setProperty("Cp_TextProbablyMissingFilesInLibTools", "If the path to the Java directory is pointing to the libTools directory in the application, it is probably a similar problem, i.e. it is not listed or is not the current version of tools.jar.");
		languageEnProp.setProperty("Cp_TextMissingOrDeprecatedFiles", "On the way to the home directory or a directory of Java libTools in (depending on what is selected) are not placed or outdated files at least: currency.data, flavormap.properties or tools.jar.");
		languageEnProp.setProperty("Cp_TextErrorWhileCreatingCompilatorTitle", "Compiler error");		
		languageEnProp.setProperty("Cp_TextActualPath", "Current path");
		languageEnProp.setProperty("Cp_TextUnknown", "Unknown");
		languageEnProp.setProperty("Cp_TextJavaVersionsSupported", "Supported versions of Java");
				
				
				
				
				
				
				
				
				
				
				
				
		// Výchozí texty do třídy: FlexibleCountOfParameters.java:
		ConfigurationFiles.commentsMap.put("Fcop_FormatErrorByte", "Text for error messages that can be displayed in labels on text fields where the wrong value has been entered.The texts contain information about the error encountered.This is a text in the dialog to call the method or class constructor (using the graphical interface, not the command editor).");
		languageEnProp.setProperty("Fcop_FormatErrorByte", "The byte type number can only contain interval values: -128 to +127.");
		languageEnProp.setProperty("Fcop_FormatErrorShort", "The short type number can only contain interval values: -32768 to +32767.");
		languageEnProp.setProperty("Fcop_FormatErrorInt", "The int type number can only contain interval values: -2147483648 to +2147483647.");
		languageEnProp.setProperty("Fcop_FormatErrorLong", "The long type number can only contain interval values: -9,223,372,036,854,775,808 to +9223372036854775807.");
		languageEnProp.setProperty("Fcop_FormatErrorFloat", "The float type number can only contain interval values: -3.40282e + 38 to + 3.40282e + 38th");
		languageEnProp.setProperty("Fcop_FormatErrorDouble", "The double type number can only contain interval values: -1.79769e + to + 308 1.79769e + 308 thereof.");
		languageEnProp.setProperty("Fcop_FormatErrorChar", "A char character can contain only one character in syntax: 'X'.");
		languageEnProp.setProperty("Fcop_FormatErrorString", "Text String may contain any characters in normal (double) quotes - syntax: \"My text.\".");
		languageEnProp.setProperty("Fcop_DefaultValueForStringJtextField","My text.");
		languageEnProp.setProperty("Fcop_LabelForParameter", "Enter a value type parameter");
		languageEnProp.setProperty("Fcop_ErrorText_FieldContainsError","field contains an error.");
		languageEnProp.setProperty("Fcop_ErrorText_ErrorForErrorList", "Mistakes");
		languageEnProp.setProperty("Fcop_ErrorText_PnlMistakesBorderTitle", "Error information");
		languageEnProp.setProperty("Fcop_ErrorText_ChcbWrapLyrics", "Wrap the lyrics.");
				
				
				
				
				
				
				
				
				
		// Texty do PopupMenu v diagramu instancí:
		ConfigurationFiles.commentsMap.put("Id_Pm_MenuItemInheritedMethods", "Texts in the default items in the context menu above the instance of the class in the instance diagram. Items with these texts will always be in the appropriate context menu.");
		languageEnProp.setProperty("Id_Pm_MenuItemInheritedMethods", "Inherited from");
		languageEnProp.setProperty("Id_Pm_MenuItemInheritedMethodsFromAncestors", "Inherited methods");
		languageEnProp.setProperty("Id_Pm_ItemRemove", "Remove");
		languageEnProp.setProperty("Id_Pm_ItemVievVariables", "View");
		languageEnProp.setProperty("Id_Pm_Txt_Success", "Success");
				
				
				
				
				
		// Texty do třídy VariablesInstanceOfClassFrom.java:
		// Texty pro ohraničení:
		ConfigurationFiles.commentsMap.put("Vioc_DialogTitle", "Text for window labels with class instance values, these texts are for a dialog that displays an overview of the variables, methods, and constructors of the selected class instance in the instance diagram.");
		languageEnProp.setProperty("Vioc_DialogTitle", "Viewing an instance of the class");
		languageEnProp.setProperty("Vioc_PanelConstructorsBorderTitle", "Overview class instance constructors");
		languageEnProp.setProperty("Vioc_PanelMethodsBorderTitle", "Overview class instance methods");
		languageEnProp.setProperty("Vioc_PanelVariablesBorderTitle", "Overview class instance variables");
		languageEnProp.setProperty("Vioc_ChcbIncludeInheritedValues", "Include values ​​from ancestors.");
        languageEnProp.setProperty("Vioc_ChcbIncludeInnerClassesValues", "Include values ​​from inner classes.");
		languageEnProp.setProperty("Vioc_ChcbWrapValuesInJLists", "Wrap long lyrics.");
		languageEnProp.setProperty("Vioc_ChcbIncludeInterfaceMethods", "Include methods from the interface.");
		
		// Texty do okna:
		ConfigurationFiles.commentsMap.put("Vioc_LabelClassInfo_1", "Texts in the search dialog by values ​​in a class instance. In this dialog you can search for constructors, methods, and variables of a given instance.");
		languageEnProp.setProperty("Vioc_LabelClassInfo_1", "Viewing an instance of the class");
		languageEnProp.setProperty("Vioc_LabelClassInfo_2", "titled reference");
		languageEnProp.setProperty("Vioc_TextConstructor", "constructor");
		languageEnProp.setProperty("Vioc_TextMethod", "method (operation)");
		languageEnProp.setProperty("Vioc_TextVariable", "variable (attribute)");
		
		// Texty do tlačítek:
		ConfigurationFiles.commentsMap.put("Vioc_ButtonClose", "Text to the dialog box for viewing a class instance from a class diagram, which opens when you click 'Browse' in the popup menu above the cell representing the instance of the class in the instance diagram.");
		languageEnProp.setProperty("Vioc_ButtonClose", "Close");
		languageEnProp.setProperty("Vioc_ButtonSearchConstructor", "Search constructor");
		languageEnProp.setProperty("Vioc_ButtonCallConstructor", "Call constructor");
		languageEnProp.setProperty("Vioc_ButtonSearchMethod", "Search method");
		languageEnProp.setProperty("Vioc_ButtonsSearchVariable", "Search variable");
		languageEnProp.setProperty("Vioc_ButtonsCreateReference", "Create reference");
		languageEnProp.setProperty("Vioc_ButtonsCallMethod", "Call method");
		languageEnProp.setProperty("Vioc_TxtSuccess", "Success");

        // Texty do třídy: cz.uhk.fim.fimj.instance_diagram.overview_of_instance.PopupMenu
        ConfigurationFiles.commentsMap.put("Ooi_Pm_ItemCopy", "Texts in the context menu for operations over items in the instance values ​​dialog. For example, copying a reference to a constructor, variable, method, etc.");
        languageEnProp.setProperty("Ooi_Pm_ItemCopy", "Copy reference");
				
				
				
				
				
				
				
				
		// Texty do třídy: SearchForm.java:
		ConfigurationFiles.commentsMap.put("SForm_DialogTitle", "Texts in a dialog that is used to find a constructor, method, or variable in an instance of a class This is a dialog that opens when you click a button to find one of the listed items in the value view dialog in the instances of a class. opens when you click 'Browse' in the context menu above the instance of the class in the instance diagram.");
		languageEnProp.setProperty("SForm_DialogTitle", "Dialog to search");
		languageEnProp.setProperty("SForm_LabelInfo", "In the search field below enter the string to search for");
		languageEnProp.setProperty("SForm_ButtonClose", "Close");
		languageEnProp.setProperty("SForm_ButtonSearch", "Search");
		languageEnProp.setProperty("SForm_JspSearchedTextBorderTitle", "Match string");	
				
				
		
		
		// Texty do chybových hlášek ve třídě GraphInstance.java:
		ConfigurationFiles.commentsMap.put("Id_Jop_MissingObjectInGraphTitle", "Text for an error message that can occur in an instance diagram when removing an instance or a cell that represents an instance of a class from the class diagram in the instance diagram.");
		languageEnProp.setProperty("Id_Jop_MissingObjectInGraphTitle", "Error instances diagram");
		languageEnProp.setProperty("Id_Jop_MissingObjectInGraphText", "The labeled class instance cell was not found in the diagram!");
				
				
				
				
				
				
				
		// Texty do třídy: SaveCodeAfretCloseDialog.java
		// Texty do chybových hlášek:
		ConfigurationFiles.commentsMap.put("Scacd_Jop_TxtSaveChangesText", "Text for error messages that can occur when saving the modified source code back to the class in the code editor.");
		languageEnProp.setProperty("Scacd_Jop_TxtSaveChangesText", "Do you want to save changes in the class's source code?");
		languageEnProp.setProperty("Scacd_Jop_txtSaveChangesTitle", "Save changes");
		languageEnProp.setProperty("Scacd_Jop_TxtPath", "Path");
		languageEnProp.setProperty("Scacd_Jop_TxtFailedToWriteCodeToClassText", "Failed to write the dialogue modified source code back to class!");
		languageEnProp.setProperty("Scacd_Jop_TxtFailedToWriteCodeToClassTitle", "Error saving code");
		languageEnProp.setProperty("Scacd_Jop_TxtClassNotFoundTitle", "Class not found");
		languageEnProp.setProperty("Scacd_Jop_TxtClassNotFoundText", "Class was not found!");
		languageEnProp.setProperty("Scacd_Jop_TxtFailedToLoadClassTitle", "Class not found");
		languageEnProp.setProperty("Scacd_Jop_TxtFailedToLoadClassText", "Failed to retrieve the code of the original class!");	
				
				
				
		// Texty do třídy: Editor.java - pro editor příkazů (Command Editor) -
		// akorát text, že nebyl příkaz - syntaxe rozpoznán:
		ConfigurationFiles.commentsMap.put("Ce_ErrorText_SyntaxIsNotRecognized", "A text that serves as an error message in the output editor, a message that will be printed when you type and confirm a command in the command editor, but this command will not be recognized, and then the message will be written to the output syntax output editor. Additionally, the text 'reference variable' for displaying references in the AutoComplete / completion window in the command editor.");
		languageEnProp.setProperty("Ce_ErrorText_SyntaxIsNotRecognized", "The specified syntax is not recognized");
        languageEnProp.setProperty("Ce_Reference_Variable", "reference variable");
				
				
				
				
				
				
				
				
				
		// Texty do dialogu pro editoru zdrojového kódu (CodeEditorDialog =
		// CED):

		// Texty do Třídy v dialogu - JMenuForCodeEditor.java:
		// Hlavní menu:
		ConfigurationFiles.commentsMap.put("Ced_Mfce_MenuClass", "Text to 'main menu' in the menu bar of the source code editor dialog.");
		languageEnProp.setProperty("Ced_Mfce_MenuClass", "Class");
		languageEnProp.setProperty("Ced_Mfce_MenuEdit", "Edit");
		languageEnProp.setProperty("Ced_Mfce_MenuTools", "Tools");
        languageEnProp.setProperty("Ced_Mfce_MenuClass_MenuOpenFileInOs", "Open");

		// Texty do položek v menu MenuClass:
		ConfigurationFiles.commentsMap.put("Ced_Mfce_MenuClassItemSave", "Texts in menu items named 'Class' in the menu bar in the source code editor.");
		languageEnProp.setProperty("Ced_Mfce_MenuClassItemSave", "Save");
		languageEnProp.setProperty("Ced_Mfce_MenuClassItemReload", "Reload");
		languageEnProp.setProperty("Ced_Mfce_MenuClassItemPrint", "Print");
		languageEnProp.setProperty("Ced_Mfce_MenuClassItemClose", "Close");
        // Texty do položek v menu pro otevření otevřeného souboru v aplikaci OS nebo v průzkumníku souborů v OS:
        languageEnProp.setProperty("Ced_Mfce_ItemOpenFileInFileExplorer", "In the file explorer");
        languageEnProp.setProperty("Ced_Mfce_ItemOpenFileInDefaultAppInOs", "In the default application");

		
		
		// texty do položek v menu MenuEdit:
		ConfigurationFiles.commentsMap.put("Ced_Mfce_MenuEditItemCut", "Texts in menu items named 'Edit' in the menu bar in the source code editor.");
		languageEnProp.setProperty("Ced_Mfce_MenuEditItemCut", "Cut");
		languageEnProp.setProperty("Ced_Mfce_MenuEditItemCopy", "Copy");
		languageEnProp.setProperty("Ced_Mfce_MenuEditItemPaste", "Paste");
		languageEnProp.setProperty("Ced_Mfce_MenuEditItemIndentMore", "Indent more");
		languageEnProp.setProperty("Ced_Mfce_MenuEditItemIndentLess", "Indent less");
		languageEnProp.setProperty("Ced_Mfce_MenuEditItemComment", "Comment");
		languageEnProp.setProperty("Ced_Mfce_MenuEditItemUncomment", "Uncomment");
		languageEnProp.setProperty("Ced_Mfce_MenuEditItemFormatCode", "Format the code");
		languageEnProp.setProperty("Ced_Mfce_MenuEditItemInsertMethod", "Insert method");

		
		
		// texty do položek v menu MenuTools:
		ConfigurationFiles.commentsMap.put("Ced_Mfce_MenuToolsItemReplace", "Texts in menu items named 'Tools' in the menu bar in the source code editor.");
		languageEnProp.setProperty("Ced_Mfce_MenuToolsItemReplace", "Replace");
		languageEnProp.setProperty("Ced_Mfce_MenuToolsItemGoToLine", "Go on line");
		languageEnProp.setProperty("Ced_Mfce_MenuToolsItemCompile", "Compile");
        languageEnProp.setProperty("Ced_Mfce_MenuToolsRadioButtonWrapText", "Wrap text");
        languageEnProp.setProperty("Ced_Mfce_MenuToolsRadioButtonShowWhiteSpace", "Show white space");
        languageEnProp.setProperty("Ced_Mfce_MenuToolsRadioButtonClearWhiteSpaceLine", "Remove white space");
		languageEnProp.setProperty("Ced_Mfce_MenuToolsItemAccessMethods", "Generate getters and setters");
		languageEnProp.setProperty("Ced_Mfce_MenuToolsItemConstructors", "Generate constructor");
		languageEnProp.setProperty("Ced_Mfce_MenuToolsMenuAddSerialID", "Add serial version ID");
		languageEnProp.setProperty("Ced_Mfce_MenuToolsItemAddDefaultSerialID", "Add default serial version ID");
		languageEnProp.setProperty("Ced_Mfce_MenuToolsItemAddGeneratedSerialID", "Add generated serial version ID");
				
		// Popisky jednotlivých tlačítek v menu: menuClass:
		ConfigurationFiles.commentsMap.put("Ced_Mfce_MenuClassToolTipItemSave", "Texts with menu item labels named 'Class' in the menu bar in the source code editor.");
		languageEnProp.setProperty("Ced_Mfce_MenuClassToolTipItemSave", "Saves the current source code from the dialogue back to class.");
		languageEnProp.setProperty("Ced_Mfce_MenuClassToolTipItemReload", "Reloads the source code for the class in a dialogue editor source code.");
		languageEnProp.setProperty("Ced_Mfce_MenuClassToolTipItemPrint", "Prints the source code in the source code editor.");
		languageEnProp.setProperty("Ced_Mfce_MenuClassToolTipItemClose", "Closes this dialogue with the source code editor.");
        // Popsiky tlačítek v menu: menuOpenFileInAppsOfOs:
        languageEnProp.setProperty("Ced_Mfce_ToolTipItemOpenFileInFileExplorerInOs", "Display the open file in the file explorer.");
        languageEnProp.setProperty("Ced_Mfce_ToolTipItemOpenFileInDefaultAppInOs", "The open file will open in an OS set application to open the appropriate file type.");

		
		// Popisky jednotlivých tlačítek v menu: menuEdit:
		ConfigurationFiles.commentsMap.put("Ced_Mfce_MenuEditToolTipItemCut", "Texts with menu item titles 'Edit' in the menu bar in the source code editor.");
		languageEnProp.setProperty("Ced_Mfce_MenuEditToolTipItemCut", "Selected portion of the source code and puts it in the mailbox.");
		languageEnProp.setProperty("Ced_Mfce_MenuEditToolTipItemCopy", "Copies the selected portion of the source code and puts it in the mailbox.");
		languageEnProp.setProperty("Ced_Mfce_MenuEditToolTipItemPaste", "Inserts the source code from the clipboard to the cursor position in the source code editor.");
		languageEnProp.setProperty("Ced_Mfce_MenuEditToolTipItemIndentMore", "Offsets the beginning of the line where the cursor is located tabbing.");
		languageEnProp.setProperty("Ced_Mfce_MenuEditToolTipItemIndentLess", "Removes the tab at the beginning of the line where the cursor is located.");
		languageEnProp.setProperty("Ced_Mfce_MenuEditToolTipItemComment", "Inserts line comment on a line where the cursor is located. (\"Comment line where the cursor is located.\")");
		languageEnProp.setProperty("Ced_Mfce_MenuEditToolTipItemUncomment", "Removes line comment from the line on which the cursor is located. (\"Uncomment the line at which the cursor is located.\")");
		languageEnProp.setProperty("Ced_Mfce_MenuEditToolTipItemFormatCode", "Formats source code editor, according to Java syntax.");
		languageEnProp.setProperty("Ced_Mfce_MenuEditToolTipItemInsertMethod", "Inserts \"sample\" method to the editor source code where the cursor is located.");

		
		
		// Popisky jednotlivých tlačítek v menu: menuTools:
		ConfigurationFiles.commentsMap.put("Ced_Mfce_MenuToolsToolTipItemReplace", "Texts with menu item titles 'Tools' in the menu bar in the source code editor.");
		languageEnProp.setProperty("Ced_Mfce_MenuToolsToolTipItemReplace", "You display a dialog box where you can select whether to replace the first occurrence of the text, or all occurrences of the text in the source code editor.");
		languageEnProp.setProperty("Ced_Mfce_MenuToolsToolTipItemGoToLine", "Inserts cursor on the line, a number entered.");
		languageEnProp.setProperty("Ced_Mfce_MenuToolsToolTipItemCompile", "Compiles a class that is in a class diagram.");
		languageEnProp.setProperty("Ced_Mfce_MenuToolsToolTipItemToString", "Put the toString () method on the cursor position.");
		languageEnProp.setProperty("Ced_Mfce_MenuToolsToolTipItemAccessMethods", "Opens a dialog for selecting items (variables) of the class to which it is possible to insert the access method (ie. Getter or setter).");
		languageEnProp.setProperty("Ced_Mfce_MenuToolsToolTipItemConstructor", "Opens a dialog where you can mark items (variables) that can be added to parameter constructor.");
		languageEnProp.setProperty("Ced_Mfce_MenuToolsToolTipItemAddDefaultSerialId", "The default serial version ID will be inserted at the cursor position.");
		languageEnProp.setProperty("Ced_Mfce_MenuToolsToolTipItemAddGeneratedSerialId", "The newly generated serial version ID will be inserted at the cursor position.");
				
		
		
		// Texty do chybových hlášek:
		ConfigurationFiles.commentsMap.put("Ced_Mfce_JopClassNotFoundText", "Text for error messages that may occur in the source code editor dialog, and some buzzwords are used in the SharedClassForForMethods class, also for the source code editor.");
		languageEnProp.setProperty("Ced_Mfce_JopClassNotFoundText", "Not found a class whose code is loaded in the editor!");
		languageEnProp.setProperty("Ced_Mfce_JopClassNotFoundTitle", "Class not found");
		languageEnProp.setProperty("Ced_Mfce_TextPath", "Path");
		languageEnProp.setProperty("Ced_Mfce_JopSaveChangesText", "Save Changes?");
		languageEnProp.setProperty("Ced_Mfce_JopSaveChangesTitle", "Save changes");
		languageEnProp.setProperty("Ced_Mfce_JopErrorWhileSavingChangesToClassText_1", "Failed to write the source code from the code editor back to class!");
		languageEnProp.setProperty("Ced_Mfce_JopErrorWhileSavingChangesToClassText_2", "The code was not written!");
		languageEnProp.setProperty("Ced_Mfce_JopErrorWhileSavingChangesToClassTitle", "Error writing");
		languageEnProp.setProperty("Ced_Mfce_JopClassDoesNotImplementInterfaceOrErrorText", "The class does not implement the Serializable interface or does not inherit the class that implements it or contains an error.");
		languageEnProp.setProperty("Ced_Mfce_JopClassDoesNotImplementInterfaceOrErrorTitle", "The class does not implement Serializable");
		languageEnProp.setProperty("Ced_Mfce_TextCommentOnTheMethod", "Comment on the method, for example, what is its purpose, contents of her body, the process algorithm in the method ...");
		languageEnProp.setProperty("Ced_Mfce_TextParametersDescription", "Brief description of the method parameter.");
		languageEnProp.setProperty("Ced_Mfce_TextReturnValue", "Brief description of the return value method.");
		languageEnProp.setProperty("Ced_Mfce_TextPutYoutCodeHere", "Here you can insert your own code.");
		languageEnProp.setProperty("Ced_Mfce_TextReturnStringValue", "Here you can specify the value of type String has a method to return.");
		languageEnProp.setProperty("Ced_Mfce_TextCouldNotLoadClassFile", "Failed to load translated class, respectively. '.class' file compiled classes!");
		languageEnProp.setProperty("Ced_Mfce_TextClass", "Class");
		languageEnProp.setProperty("Ced_Mfce_TextFileNotFound", "File not found");
		languageEnProp.setProperty("Ced_Mfce_FileDoesNotExistInPathText", "The file on the specified path already do not exists!");
		languageEnProp.setProperty("Ced_Mfce_FileDoesNotExistInPathTitle", "Missing file");
				
		
		
		// Třída GoToLineForm.java (GTLF)
		// Texty do titulku dialogu ,chybové hlášky a texty v dialogu:
		ConfigurationFiles.commentsMap.put("Ced_Gtlf_DialogTitle", "Texts in the cursor setup dialog on the line with the entered number, these are texts both in the dialog title and in the labels in it and in the error messages, which will be displayed by clicking 'Go to Line' in the 'Tools' menu in menu bar in the source code editor dialog.");
		languageEnProp.setProperty("Ced_Gtlf_DialogTitle", "Go on line");
		languageEnProp.setProperty("Ced_Gtlf_LabelLine", "Go to line number");
		languageEnProp.setProperty("Ced_Gtlf_LabelErrorText", "The line number is entered in the wrong size or format!");
		languageEnProp.setProperty("Ced_Gtlf_ButtonOk", "OK");
		languageEnProp.setProperty("Ced_Gtlf_ButtonCancel", "Cancel");
		languageEnProp.setProperty("Ced_Gtlf_LineNumberIsNotCorrectText", "The row number is entered in the correct syntax or range!");
		languageEnProp.setProperty("Ced_Gtlf_LineNumberIsNotCorrectTitle", "Wrong size or scope");
		languageEnProp.setProperty("Ced_Gtlf_EmptyFieldText", "Not specified line number to move the cursor!");
		languageEnProp.setProperty("Ced_Gtlf_EmptyFieldTitle", "Empty field");
				
				
		
		
				// Třída JtoolBarForCodeEditor.java - texty do tlačítek a jejich popisky:
				// - Budou stejné jako z menu - jak popisky, tak texty z položek
				
				
				
				
		// Texty do třídy: ReplaceTextInCodeEditorForm.java
		ConfigurationFiles.commentsMap.put("Rtice_DialogTitle", "Texts in the source code editor dialog to replace the text in the appropriate source code editor, all texts in the dialog, both the text in the dialog title and the texts in the appropriate labels and error messages.");
		languageEnProp.setProperty("Rtice_DialogTitle", "Replace text in code");
		languageEnProp.setProperty("Rtice_LabelInfo", "Below enter the string you want to replace a string with which to replace it.");
		languageEnProp.setProperty("Rtice_LabelMissingValue", "The source code does not contain the expression!");
		languageEnProp.setProperty("Rtice_LabelFind", "Find");
		languageEnProp.setProperty("Rtice_LabelReplace", "Replace");
		languageEnProp.setProperty("Rtice_ButtonOk", "OK");
		languageEnProp.setProperty("Rtice_ButtonCancel", "Cancel");
		languageEnProp.setProperty("Rtice_ChcbReplaceFirst", "Replace the first occurrence of the string.");
		languageEnProp.setProperty("Rtice_ChcbReplaceAll", "Replace all occurrences of the string.");
		languageEnProp.setProperty("Rtice_TextCodeDontContainsString", "The source code does not contain the specified string!");
		languageEnProp.setProperty("Rtice_TextString", "String");
		languageEnProp.setProperty("Rtice_MissingValue", "Missing value");
		languageEnProp.setProperty("Rtice_EmptyFieldTitle", "Empty field");
		languageEnProp.setProperty("Rtice_EmptyFieldText", "For finding the string is empty!");
				
				
		// Texty do třídy: GenerateConstructorUsingFieldForm.java:
		ConfigurationFiles.commentsMap.put("Gcuff_DialogTitle", "Texts in the dialog for generating a constructor with labeled variables (constructor parameters) This dialog can be opened by clicking 'Generate constructor' in the 'Tools' menu in the menu bar of the source code editor dialog, all texts in the dialog , both the title of the dialogue and all the lyrics, buttons, labels, etc.");
		languageEnProp.setProperty("Gcuff_DialogTitle", "Generate constructor with parameters");
		languageEnProp.setProperty("Gcuff_LabelInfo", "Mark the items you want to add a parameter to the constructor.");
		languageEnProp.setProperty("Gcuff_ButtonOk", "OK");
		languageEnProp.setProperty("Gcuff_ButtonCancel", "Cancel");
		languageEnProp.setProperty("Gcuff_ButtonSelectAll", "Select all");
		languageEnProp.setProperty("Gcuff_ButtonDeselectAll", "Deselect all");
		languageEnProp.setProperty("Gcuff_ChcbGenerateDocDocument", "Generate documentation comments marked the constructors.");
		languageEnProp.setProperty("Gcuff_PanelChcbModifierTitle", "Access modifiers");
				
				
				
		// Texty do třídy: GeneraceAccessMethodsform.java:
		ConfigurationFiles.commentsMap.put("Gamf_DialogTitle", "Dialog text to generate access methods (getters and sets) of the variable (attributes) of the appropriate class This dialog can be opened by clicking on 'Generate geters and slicers' in the 'Tools' menu in the source code editor This is all the texts in the dialog, both the title of the dialog and the texts in all the buttons, labels, items, etc.");
		languageEnProp.setProperty("Gamf_DialogTitle", "Generate getters and setters");
		languageEnProp.setProperty("Gamf_LabelInfo", "Labeled \"access methods\" (getters and setters) will be created.");
		languageEnProp.setProperty("Gamf_ButtonSelectAll", "Select all");
		languageEnProp.setProperty("Gamf_ButtonDeselectAll", "Deselect all");
		languageEnProp.setProperty("Gamf_CheckBoxGenerateComment", "Generate documentation comment for allegedly methods.");
		languageEnProp.setProperty("Gamf_ButtonOk", "OK");
		languageEnProp.setProperty("Gamf_ButtonCancel", "Cancel");
		languageEnProp.setProperty("Gamf_PanelModifiersBorderTitle", "Access modifiers");
				
				
				
				
				
				
		// Texty do třídy: CodeEditor.java
		ConfigurationFiles.commentsMap.put("CodEdit_LoopForSummary", "Texts in the source code dialog, into the source code editor itself, specifically in the auto-refresh window that opens when you press the Ctrl + Space keystroke. These texts are used to replay cycles that can be pasted - just as an example.");
		languageEnProp.setProperty("CodEdit_LoopForSummary", "Sample loop for.");
		languageEnProp.setProperty("CodEdit_PreparedLoopForSummary", "Prepared loop for.");
		languageEnProp.setProperty("CodEdit_LoopWhileSummary", "Demonstration of the while loop.");
        languageEnProp.setProperty("CodEdit_LoopDoWhileSummary", "Demonstration of the do-while loop.");
		languageEnProp.setProperty("CodEdit_LoopForEachSummary", "Sample foreach loop.");
		languageEnProp.setProperty("CodEdit_loopUsingLambdaSummary", "Sample loop using lambda expressions.");
        languageEnProp.setProperty("CodEdit_loopUsingIntStream", "Sample loop using IntStream.");
        languageEnProp.setProperty("CodEdit_InfiniteWhileLoop", "Sample infinite while loop");
		languageEnProp.setProperty("CodEdit_PutYourCodeHere", "Here you can insert your code ...");
		languageEnProp.setProperty("CodEdit_LoopForInfo", "Loop for");
		languageEnProp.setProperty("CodEdit_LoopWhileInfo", "while loop");
        languageEnProp.setProperty("CodEdit_LoopDoWhileInfo", "do-while loop");
		languageEnProp.setProperty("CodEdit_LoopForeachInfo", "foreach loop");
		languageEnProp.setProperty("CodEdit_LoopWithLambdaExpressionInfo", "Loop using lambda expressions");
        languageEnProp.setProperty("CodEdit_LoopWithIntStreamInfo", "Loop IntStream");
        languageEnProp.setProperty("CodEdit_InfiniteWhileLoopInfo", "Infinite while loop");
        // Přístupové metody:
        languageEnProp.setProperty("CodEdit_GenerateSetter", "Generate setter");
        languageEnProp.setProperty("CodEdit_GenerateGetter", "Generate getter");
        // Texty pro popisky cyklů v okně pro doplňování hodnoty:
        languageEnProp.setProperty("CodEdit_Txt_WithVariable", "With variable");
        languageEnProp.setProperty("CodEdit_Txt_WithoutVariable", "Without variable");
			
				
				
				
				
				
				
		// Veškeré texty do třídy makeOperation.java (Mo) - veškeré výpysy chyb,
		// apod. do editoru výstupů,...
		ConfigurationFiles.commentsMap.put("Mo_Txt_Success", "All texts in the MakeOperation class, i.e. the class that executes all commands entered in the command editor, these texts serve as statements to the output editor as a status report for the execution of the operation, such as 'Success' in the case of a successful operation or 'Failure' other.");
		languageEnProp.setProperty("Mo_Txt_Success", "Success");
		languageEnProp.setProperty("Mo_Txt_Failure", "Failure");
		languageEnProp.setProperty("Mo_Txt_InstanceNotFound", "Not found an instance of the class under reference");
		languageEnProp.setProperty("Mo_Txt_MethodNotFound", "The method can not be found!");
		languageEnProp.setProperty("Mo_Txt_BadNameOfMethodOrParameter", "Wrong method name or parameter type!");
		languageEnProp.setProperty("Mo_Txt_ReferenceNameNotFound", "Reference to an instance of a class does not exist!");
		languageEnProp.setProperty("Mo_Txt_FailedRetypeValue", "The entered value can not be inserted into a variable of the appropriate type!");
		languageEnProp.setProperty("Mo_Txt_MissingVariableInInstanceOfClass", "Unable to find specified instance variable in the class!");
		languageEnProp.setProperty("Mo_Txt_ErrorInVariableNameOfVisibilityVariable", "Class variable name error or class 'visibility' error!");

		languageEnProp.setProperty("Mo_Txt_InstanceOfClassNotFound", "Instances of classes, which shows the specified variable does not exist!");
		languageEnProp.setProperty("Mo_Txt_AtLeastOneInstanceOfClassNotFoune", "At least one instance of a class, which is indicative of the specified reference does not exist!");
		languageEnProp.setProperty("Mo_Txt_DataTypesAreNotEqual", "Data types are not equal");
		languageEnProp.setProperty("Mo_Txt_ErrorMethodOrParametersOrModifiers", "The specified method does not exist in the given instance of the class, it differs from the number of parameters or is not public! (Ie, it is not marked with public or protected.)");

		languageEnProp.setProperty("Mo_Txt_VariableNotFoundInReferenceToInstanceOfClass", "In the instance of the class, which shows the specified reference variable sought were not found!");
		languageEnProp.setProperty("Mo_Txt_FailedToFillVariable", "Failed to populate a variable!");
		languageEnProp.setProperty("Mo_Txt_PossibleErrorsInFillVariable", "Possible errors: The object is not an instance of the class or subclass interface, etc.");
		languageEnProp.setProperty("Mo_Txt_ErrorsImmutableVisibilityPublic", "Specified variable is either unchangeable - 'final' or not visible, ie. The modifier designated public!");
		languageEnProp.setProperty("Mo_Txt_VariableName", "Variable name");
		languageEnProp.setProperty("Mo_Txt_AlreadyExist", "already exists, please choose another!");
		languageEnProp.setProperty("Mo_Txt_SrcDirNotFound", "Unable to find a path to the src project is open!");
		languageEnProp.setProperty("Mo_Txt_ClassNotFound", "Class was not found!");
		languageEnProp.setProperty("Mo_Txt_FailedToLoadClassFile", "Failed to load the '.class' file!");
		languageEnProp.setProperty("Mo_Txt_DataTypesOrConstructorNotFound", "Unable to find the right type of values or constructor!");

		// Texty pro zavolání statické metody:
		languageEnProp.setProperty("Mo_Txt_ErrorWhileCallingStaticMethod_1", "Unable to call the method. In class");
		languageEnProp.setProperty("Mo_Txt_ErrorWhileCallingStaticMethod_2", "the method with the specified name or parameters has not been found, or it is not a static method.");
		
		// Zbytek texů do této třídy MakeOperation.java - zbylé texty pro výpisy do editoru výstupů:
		ConfigurationFiles.commentsMap.put("Mo_Txt_OverViewOfCommands", "All texts in the MakeOperation class, i.e. the class that executes all commands specified in the command editor, all texts in the output editor are executed by the commands from the command editor.");
		languageEnProp.setProperty("Mo_Txt_OverViewOfCommands", "An overview of commands that the command editor can recognize");
		languageEnProp.setProperty("Mo_Txt_OverviewOfCommandsEditorVariables", "An overview of created variables in the command editor can be listed by the command");
		languageEnProp.setProperty("Mo_Txt_ErrorInIncrementValue", "Could not incremented number");
		languageEnProp.setProperty("Mo_Txt_NumberOutOfInterval", "Probably it is the number out of range");
		languageEnProp.setProperty("Mo_Txt_ErrorInDecrementValue", "Failed to decrement the number");
		languageEnProp.setProperty("Mo_Txt_VariableDontContainsNumber", "Variable does not contain all or a real number!");
		languageEnProp.setProperty("Mo_Txt_FailedToCastNumber", "Variable value failed to cast a number!");
		languageEnProp.setProperty("Mo_Txt_ReferenceDontPointsToInstance", "The reference you specify does not point to a class or interface instance declaring a specified entry (variable), or the specified object is not an instance of the class or interface containing the item (variable)!");
		languageEnProp.setProperty("Mo_Txt_VariableIsNotPublicOrIsFinal", "Item is not publicly available, ie. Not designated public modifier, or 'final' - unchanged!");
		languageEnProp.setProperty("Mo_Txt_ReferenceDontPointsToInstanceOfClass", "The specified reference does not point to any instance of the class!");
		languageEnProp.setProperty("Mo_Txt_Reference", "Reference");
		languageEnProp.setProperty("Mo_Txt_Variable", "Variable");
		languageEnProp.setProperty("Mo_Txt_Error", "Error");
		languageEnProp.setProperty("Mo_Txt_MethodWillNotBeInclude", "The method will not be included in the final variable values!");
		languageEnProp.setProperty("Mo_Txt_Method", "Method");
		languageEnProp.setProperty("Mo_Txt_MethodHasNotBeenCalled", "The method was not called, its value will not be counted!");
		languageEnProp.setProperty("Mo_Txt_ReferenceDoesNotPointsToInstanceOfClass", "The specified reference does not point to any instance of the class!");
		languageEnProp.setProperty("Mo_Txt_ValueWillNotBeCounted", "The value will not be counted!");
		languageEnProp.setProperty("Mo_Txt_ValueOfVariableWillNotBeCounted", "The value of the variable will not be included in the resulting value!");
		languageEnProp.setProperty("Mo_Txt_DataTypeOfField", "Data type Items");
		languageEnProp.setProperty("Mo_Txt_DataTypeIsDifferent", "It is different than the data type of the variable to which the value is to be inserted!");
		languageEnProp.setProperty("Mo_Txt_Instance", "Instance");
		languageEnProp.setProperty("Mo_Txt_ErrorWhileConvertingValues", "An error occurred while converting entered values");
		languageEnProp.setProperty("Mo_Txt_ToType", "to type");
		languageEnProp.setProperty("Mo_Txt_VariableNameMustBeKeyWork", "Variable name must not be a keyword of Java!");
		languageEnProp.setProperty("Mo_Txt_ValueIsFinalAndCannotBeChanged", "the 'final', its value can not be changed!");
		languageEnProp.setProperty("Mo_Txt_ErrorInClassLoaderOrAccessToField", "Possible errors, error ClassLoader, or could not access the item!");
		languageEnProp.setProperty("Mo_Txt_DoNotFound", "was not found!");
		languageEnProp.setProperty("Mo_Txt_FailedToCastValue", "Unable to cast a value");
		languageEnProp.setProperty("Mo_Txt_ToDataType", "data type");
		languageEnProp.setProperty("Mo_Txt_ErrorWhileParsingValue", "An error occurred while parsing values");
		languageEnProp.setProperty("Mo_Txt_ProbablyIsAbstractClass", "Probably it is the abstract class!");
		languageEnProp.setProperty("Mo_Txt_NotFoundConstructorToCastingDataType", "Not found constructor to cast the value to that data type!");
		languageEnProp.setProperty("Mo_Txt_PossibleParametersCountIsDifferent", "Maybe error vary the parameters to count!");
		languageEnProp.setProperty("Mo_Txt_ExceptionWhileCallingConstructorToCasting", "When you call the constructor to cast values to said data type exception occurred!");
		languageEnProp.setProperty("Mo_Txt_VariableFailedToLoad", "Variable failed to load!");
		languageEnProp.setProperty("Mo_Txt_VariableNotFoundForInsertValue", "Was not found in the specified variable in which to insert the value!");
		languageEnProp.setProperty("Mo_Txt_VariableNotFound", "Variable not found");
		languageEnProp.setProperty("Mo_Txt_Value", "Value");
		languageEnProp.setProperty("Mo_Txt_VariableIsFinalIsNeedToFillInCreation", "The variable is 'final', it must be filled when it is created!");
		languageEnProp.setProperty("Mo_Txt_MustNotBeFinalAndNull", "must not be 'final' and null!");
		languageEnProp.setProperty("Mo_Txt_VariableNameIsNeedToChangeIsAlreadyTaken", "Variable name is already taken, you need to change it!");
		languageEnProp.setProperty("Mo_Txt_VariableNameNotRecognized", "Not recognized variable name!");
		languageEnProp.setProperty("Mo_Txt_DataTypeNotFoundOnlyPrimitiveDataType", "Not found the data type of the variable (can only use primitive data types.)!");
		languageEnProp.setProperty("Mo_Txt_DataType", "Data type");
		languageEnProp.setProperty("Mo_Txt_FailedToCreateTemporaryVariable", "Failed to create a temporary variable in the command editor!");
		languageEnProp.setProperty("Mo_Txt_Data", "Data");
		languageEnProp.setProperty("Mo_Txt_Value_2", "value");
		languageEnProp.setProperty("Mo_Txt_MayBeError", "Maybe error");
		languageEnProp.setProperty("Mo_Txt_DataTypeDoNotSupported", "It is not supported by the specified data type variable!");
		languageEnProp.setProperty("Mo_Txt_VariableIsNullCannotBeIncrementOrDecrement", "Variable is null, could not be incremented or decremented!");
		languageEnProp.setProperty("Mo_Txt_VariableIsNull", "Variable is null!");
		languageEnProp.setProperty("Mo_Txt_ValueWasNotRecognized", "The value not recognized!");
		languageEnProp.setProperty("Mo_Txt_FailedToCreateVariable", "Failed to create a variable!");
		languageEnProp.setProperty("Mo_Txt_DoNotRetrieveDataTypeOfVariable", "Unable to retrieve data type of a variable!");
		languageEnProp.setProperty("Mo_Txt_FailedToGetDataAboutVariableWhileDeclaration", "Unable to obtain data on the variables declaration!");
		languageEnProp.setProperty("Mo_Txt_ReferenceDontPointToInstanceOfList", "The specified reference points to the instance list!");
		languageEnProp.setProperty("Mo_Txt_ValueAtIndexIsNull", "The value at the specified index in the array is null!");
		languageEnProp.setProperty("Mo_Txt_ConditionsForIndexOfList", "Index values in List (collection) must be >= 0 while index must be < size List!");				
		languageEnProp.setProperty("Mo_Txt_SpecifiedIndexOfList", "The specified index");
		languageEnProp.setProperty("Mo_Txt_InstanceOfListWasNotFoundUnderReference", "List instance was not found under the specified reference!");
		languageEnProp.setProperty("Mo_Txt_SizeOfTheList", "List size - collection");
		languageEnProp.setProperty("Mo_Txt_DataTypeForListNotFound", "Could not find data type for list!");
		languageEnProp.setProperty("Mo_Txt_DataTypeForArrayAreNotIdenticaly", "Data types for declaring and filling fields are not identical!");
		languageEnProp.setProperty("Mo_Txt_DataTypeForArrayNotFound", "Was not found for a data type field!");
		languageEnProp.setProperty("Mo_Txt_InstanceOfArrayWasNotFoundUnderReference", "Instances of one-dimensional field was not found under the specified reference!");
		languageEnProp.setProperty("Mo_Txt_ArrayIsFinalDontChangeValues", "The field is 'final' (ie. Fixed), not changing its values are read-only!");
		languageEnProp.setProperty("Mo_Txt_Array", "Array");
		languageEnProp.setProperty("Mo_Txt_IndexOutOfRange", "The index position in the array is outside the range of values of type Integer!");
		languageEnProp.setProperty("Mo_Txt_ConditionalForIndexInArray", "Index values in the array must be >= 0 while index must be < array length!");
		languageEnProp.setProperty("Mo_Txt_Index", "Index");
		languageEnProp.setProperty("Mo_Txt_SizeOfArray", "Array length");
		languageEnProp.setProperty("Mo_Txt_Name", "Name");
		languageEnProp.setProperty("Mo_Txt_DoesNotIncrementDecrement", "Value at a given index in a given field can not increment / decrement, does not figure!");
		languageEnProp.setProperty("Mo_Txt_IndexOfListIsOutOfRange", "The index value of the position in the worksheet (collection) where the value is to be inserted is out of range sheet size!");
		languageEnProp.setProperty("Mo_Txt_VariableIsFinalDontChangeValue", "The variable is 'final', it can not change its value!");
		languageEnProp.setProperty("Mo_Txt_InstanceIsNotCorrectForParameter", "Instance of the specified class instance is not required by the parameter!");
		languageEnProp.setProperty("Mo_Txt_ArraySizeIsOutOfInteger", "Field size is outside the range of values of type Integer!");
		languageEnProp.setProperty("Mo_Txt_ValueInArrayInIndexIsNull", "No value for the index in the specified array is null!");
		languageEnProp.setProperty("Mo_Txt_ValueAtIndexAtArrayCantIncrementDecrement", "No value for the index in a given field can not increment / decrement, does not figure!");
		languageEnProp.setProperty("Mo_Txt_FailedToCalculateMathTerm_1", "Failed to calculate entered term");
		languageEnProp.setProperty("Mo_Txt_FailedToCalculateMathTerm_2", "Expression");
				
				
		// Texty do metody writeCommands - texty s informacemi pro zacházení s
		// editorem příkazů.
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_MathCalculationTitle", "Texts that describe all command editor functions (ie, syntax for calling a method, instantiating, filling or declaring variables in a class instance, or 'temporary' variables in the command editor, their rules, etc.) These texts are written to the output editor after the command 'printCommands ();' to the command editor.");
		// Matematické operace:
		languageEnProp.setProperty("Mo_Wc_Txt_MathCalculationTitle", "Mathematical calculations");
		languageEnProp.setProperty("Mo_Wc_Txt_OperandsForMathCalculation", "For mathematical operations, the following mathematical operators can be used");
		// Logické operaátory:
		languageEnProp.setProperty("Mo_Wc_Txt_LogicalOperationsInfo", "The following logical operators can be used for logical operations");
		//Konstanty:
		languageEnProp.setProperty("Mo_Wc_Txt_Constants", "Constants");
		//Podporované funkce:
		languageEnProp.setProperty("Mo_Wc_Txt_SupportedFunctionsTitle", "Supported features");
		languageEnProp.setProperty("Mo_Wc_Txt_NamesOfFunctionsInfo", "The following function names are");	
		// Příkaldy:
		languageEnProp.setProperty("Mo_Wc_Txt_WithoutSemicolonAtEndsOfExpression", "In the case of the mathematical expression for the calculation, no semicolon is given at the end of the expression (not when the result is assigned to the variable).");
		languageEnProp.setProperty("Mo_Wc_Txt_IncludeVariablesFromCommandsEditorInfo", "Mathematical calculations can also include numeric variables created in the command editor, for example in the following syntax");
		languageEnProp.setProperty("Mo_Wc_Txt_VarNamesCantContainsNumbers", "The names of the variables used in the mathematical expression / calculation must not contain digits.");
		
		languageEnProp.setProperty("Mo_Wc_Txt_DeclaredAndFulfilledVars", "Previously declared and filled variables");
		languageEnProp.setProperty("Mo_Wc_Txt_DeclaredAndFulfilledVar", "Previously declared and filled variable");
		
		// Matematické operace:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_UnaryPlus", "Mathematical operations.");
		languageEnProp.setProperty("Mo_Wc_Txt_UnaryPlus", "Additive operator / Unary plus");
		languageEnProp.setProperty("Mo_Wc_Txt_UnaryMinus", "Subtraction operator / Unary minus");
		languageEnProp.setProperty("Mo_Wc_Txt_MultiplicationOperator", "Multiplication operator, can be omitted in front of an open bracket");
		languageEnProp.setProperty("Mo_Wc_Txt_DivisionOperator", "Division operator");
		languageEnProp.setProperty("Mo_Wc_Txt_ModuloOperator", "Remainder operator (Modulo)");
		languageEnProp.setProperty("Mo_Wc_Txt_PoweredOperator", "Power operator");
		
		// Logické operátory:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_Equals", "Logical operators.");
		languageEnProp.setProperty("Mo_Wc_Txt_Equals", "Equals");
		languageEnProp.setProperty("Mo_Wc_Txt_NotEquals", "Not equals");
		languageEnProp.setProperty("Mo_Wc_Txt_LessThan", "Less than");
		languageEnProp.setProperty("Mo_Wc_Txt_LessThanOrEqualTo", "Less than or equal to");
		languageEnProp.setProperty("Mo_Wc_Txt_GreaterThan", "Greater than");
		languageEnProp.setProperty("Mo_Wc_Txt_GreaterThanOrEqualTo", "Greater than or equal to");
		languageEnProp.setProperty("Mo_Wc_Txt_BooleanAnd", "Boolean and");
		languageEnProp.setProperty("Mo_Wc_Txt_BooleanOr", "Boolean or");
		
		// Konstanty:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_ValueOfE", "Constants.");
		languageEnProp.setProperty("Mo_Wc_Txt_ValueOfE", "The value of e, exact to 70 digits");
		languageEnProp.setProperty("Mo_Wc_Txt_ValueOfPI", "The value of PI, exact to 100 digits");
		languageEnProp.setProperty("Mo_Wc_Txt_ValueOne", "The value one");
		languageEnProp.setProperty("Mo_Wc_Txt_ValueZero", "The value zero");
		
		// Podporované funkce:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_FceNot", "Supported features.");
		languageEnProp.setProperty("Mo_Wc_Txt_FceNot", "Boolean negation, 1 (means true) if the expression is not zero");
		languageEnProp.setProperty("Mo_Wc_Txt_Condition", "Returns one value if the condition evaluates to true or the other if it evaluates to false");
		languageEnProp.setProperty("Mo_Wc_Txt_FceRandom", "Produces a random number between 0 and 1");
		languageEnProp.setProperty("Mo_Wc_Txt_FceMin", "Returns the smallest of the given expressions");
		languageEnProp.setProperty("Mo_Wc_Txt_FceMax", "Returns the biggest of the given expressions");
		languageEnProp.setProperty("Mo_Wc_Txt_FceAbs", "Returns the absolute (non-negative) value of the expression");
		languageEnProp.setProperty("Mo_Wc_Txt_FceRound", "Rounds a value to a certain number of digits, uses the current rounding mode");
		languageEnProp.setProperty("Mo_Wc_Txt_FceFloor", "Rounds the value down to the nearest integer");
		languageEnProp.setProperty("Mo_Wc_Txt_FceCelling", "Rounds the value up to the nearest integer");
		languageEnProp.setProperty("Mo_Wc_Txt_FceLog", "Returns the natural logarithm (base e) of an expression");
		languageEnProp.setProperty("Mo_Wc_Txt_FceLog10", "Returns the common logarithm (base 10) of an expression");
		languageEnProp.setProperty("Mo_Wc_Txt_FceSqrt",  "Returns the square root of an expression");
		languageEnProp.setProperty("Mo_Wc_Txt_FceSin", "Returns the trigonometric sine of an angle (in degrees)");
		languageEnProp.setProperty("Mo_Wc_Txt_FceCos",  "Returns the trigonometric cosine of an angle (in degrees)");
		languageEnProp.setProperty("Mo_Wc_Txt_FceTan",  "Returns the trigonometric tangens of an angle (in degrees)");
		languageEnProp.setProperty("Mo_Wc_Txt_FceCot", "Returns the trigonometric cotangens of an angle (in degrees)");
		languageEnProp.setProperty("Mo_Wc_Txt_FceAsin", "Returns the angle of asin (in degrees)");
		languageEnProp.setProperty("Mo_Wc_Txt_FceAcos", "Returns the angle of acos (in degrees)");
		languageEnProp.setProperty("Mo_Wc_Txt_FceAtan", "Returns the angle of atan (in degrees)");
		languageEnProp.setProperty("Mo_Wc_Txt_FceAcot", "Returns the angle of acot (in degrees)");
		languageEnProp.setProperty("Mo_Wc_Txt_FceAtan2", "Returns the angle of atan2 (in degrees)");
		languageEnProp.setProperty("Mo_Wc_Txt_FceSinh", "Returns the hyperbolic sine of a value");
		languageEnProp.setProperty("Mo_Wc_Txt_FceCosh", "Returns the hyperbolic cosine of a value");
		languageEnProp.setProperty("Mo_Wc_Txt_FceTah", "Returns the hyperbolic tangens of a value");
		languageEnProp.setProperty("Mo_Wc_Txt_FceCoth", "Returns the hyperbolic cotangens of a value");
		languageEnProp.setProperty("Mo_Wc_Txt_FceSec", "Returns the secant (in degrees)");
		languageEnProp.setProperty("Mo_Wc_Txt_FceCsc", "Returns the cosecant (in degrees)");
		languageEnProp.setProperty("Mo_Wc_Txt_FceSech", "Returns the hyperbolic secant (in degrees)");
		languageEnProp.setProperty("Mo_Wc_Txt_FceCsch", "Returns the hyperbolic cosecant (in degrees)");
		languageEnProp.setProperty("Mo_Wc_Txt_FceAsinh", "Returns the angle of hyperbolic sine (in degrees)");
		languageEnProp.setProperty("Mo_Wc_Txt_FceAcosh", "Returns the angle of hyperbolic cosine (in degrees)");
		languageEnProp.setProperty("Mo_Wc_Txt_FceTanh", "Returns the angle of hyperbolic tangens of a value");
		languageEnProp.setProperty("Mo_Wc_Txt_FceRad", "Converts an angle measured in degrees to an approximately equivalent angle measured in radians");
		languageEnProp.setProperty("Mo_Wc_Txt_FceDeg", "Converts an angle measured in radians to an approximately equivalent angle measured in degrees");
		
		// Nějkaé ukázky pro matematické výrazy,které je možné zadat (podmínky, funkce, jejich využití, ...)
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_ExamplesOfMathExpressions", "Examples of mathematical expressions that can be calculated.");
		languageEnProp.setProperty("Mo_Wc_Txt_ExamplesOfMathExpressions", "Examples of mathematical expressions for calculating");		
		languageEnProp.setProperty("Mo_Wc_Txt_MakeInstance", "Creating a class instance of a class diagram");
		languageEnProp.setProperty("Mo_Wc_Txt_ClassName", "The class name can be entered with or without packages, depending on whether a given class in any package found");
		languageEnProp.setProperty("Mo_Wc_Txt_MakeInstanceParameters_1", "Furthermore you can enter to create an instance of the class with parameters: character ('x'), text (\"Some text.\"), Boolean (true, false), whole or decimal numbers separated by decimal points,");
		languageEnProp.setProperty("Mo_Wc_Txt_MakeInstanceParameters_2", "call the method with no more than one parameter (referenceName.methodName (ParameterName)), increment and decrement variables in the Command Editor or");
		languageEnProp.setProperty("Mo_Wc_Txt_MakeInstanceParameters_3", "in a class instance (postfix or prefix form), or a mathematical expression (without decimal points)");
		languageEnProp.setProperty("Mo_Wc_Txt_WithParameters", "With the parameters");
		languageEnProp.setProperty("Mo_Wc_Txt_Call_Constructor_Info_1", "Next, you can create a class instance from a class diagram without creating a reference (at the appropriate instance) by calling the constructor.");
		languageEnProp.setProperty("Mo_Wc_Txt_Call_Constructor_Info_2", "Call the constructor in the following syntax");
		languageEnProp.setProperty("Mo_Wc_Txt_Call_Constructor_Info_3", "This option can also be used to create a class instance from a class diagram and then pass it to a method or constructor call parameter, or fill a variable in a class instance.");
		languageEnProp.setProperty("Mo_Wc_Txt_CallMethodInfo", "A method that is not static can only be called over an instance of the class from the class diagram in the following syntax");		
		languageEnProp.setProperty("Mo_Wc_Txt_Call_Static_Method_Info", "You can also call a static method over a class instance or above a class in the class diagram (not above the created instance) in the following syntax");
        languageEnProp.setProperty("Mo_Wc_Txt_Call_Static_Method_In_Inner_Class_Info", "A static method in an static inner class of a class in the class diagram can only be called above this class in the diagram");
		languageEnProp.setProperty("Mo_Wc_Txt_CallMethodInfo_1", "Next, you can add parameters with the same requirements as the above-mentioned constructor, i.e. decimal separator parameters, the method with at most one parameter,");
		languageEnProp.setProperty("Mo_Wc_Txt_CallMethodInfo_2", "as well as texts, characters, variables, numbers, etc.");
		
		// Info ohledně zavolání zděděných metod nad třídou coby potomkem příslušné
		// třídy s danou metodou:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_InfoAboutMethodsTitle", "Learn how to call inherited methods above the class that is the descendant of the appropriate class with the method.");
		languageEnProp.setProperty("Mo_Wc_Txt_InfoAboutMethodsTitle", "Info");
		languageEnProp.setProperty("Mo_Wc_Txt_InfoAboutCallMethodsFromObjectClass", "You can call inherited methods from the Object class above each instance of the class.");
		languageEnProp.setProperty("Mo_Wc_Txt_CallMethodFromInheritedClassInfo", "Methods marked 'public' or 'protected' in any class can be called from all of its descendants.");
		languageEnProp.setProperty("Mo_Wc_Txt_CallStaticMethodFromInheritedClassesInfo", "This also applies to static methods that do not have to be called above class instances.");		
		languageEnProp.setProperty("Mo_Wc_Txt_IncrementationDecrementation", "Increment and decrement variables or constants (prefix and postfix form)");
		languageEnProp.setProperty("Mo_Wc_Txt_IncrementationDecrementation_1", "This operation can be performed over a variable in a class instance, a variable created in the command editor (left) or above the constant (uniquely only in the command editor)");
		languageEnProp.setProperty("Mo_Wc_Txt_VariableIncrementation", "Sample possible syntax increment variable in the instance of the class");
		languageEnProp.setProperty("Mo_Wc_Txt_VariableDecrementation", "Sample possible syntax decrement variable in the instance of the class");
		languageEnProp.setProperty("Mo_Wc_Txt_IncrementationMyVariable", "Sample possible syntax increment variables created in the command editor");
		languageEnProp.setProperty("Mo_Wc_Txt_DecrementationMyVariable", "Sample possible syntax decrement variables created in the command editor");
		languageEnProp.setProperty("Mo_Wc_Txt_IncrementationDecrementationConstants", "Completely unique for the 'command editor' are operations to increment and decrement the constants.");
		languageEnProp.setProperty("Mo_Wc_Txt_IncrementationConstants", "Increment constants in the following syntax");
		languageEnProp.setProperty("Mo_Wc_Txt_DecrementationConstants", "Constant decrement in the following syntax");
		languageEnProp.setProperty("Mo_Wc_Txt_DeclarationOfVariable", "Declaration of variables in the command editor");
		languageEnProp.setProperty("Mo_Wc_Txt_DeclarationVariableInfo", "In the command editor can declare variables only primitive data types or object (Java types)");
		languageEnProp.setProperty("Mo_Wc_Txt_LikeThis", "as");
		languageEnProp.setProperty("Mo_Wc_Txt_DeclarationVariableInfo_2", "Variables can be declared using the following syntax if the variable should be immutable, it is necessary to mark the keyword when declaring a final fill");
		languageEnProp.setProperty("Mo_Wc_Txt_Syntaxe", "Syntax");
		languageEnProp.setProperty("Mo_Wc_Txt_SyntaxeForString", "Syntax for String");
		languageEnProp.setProperty("Mo_Wc_Txt_SyntaxeForChar", "The syntax for the character");
		languageEnProp.setProperty("Mo_Wc_Txt_SyntaxeForDouble", "The syntax for decimal type double");
		languageEnProp.setProperty("Mo_Wc_Txt_SimilaryDataTypes", "and the like. for the remainder of said data types.");
		languageEnProp.setProperty("Mo_Wc_Txt_FullfilmentVariable", "Filling the variables in the instance of the class or variable created in the command editor");
		languageEnProp.setProperty("Mo_Wc_Txt_FullfilmentVariableInfo", "The variable can be filled with a constant (number, text character) - possibly a mathematical operation (see above), a return value of the method or the value of another variable");
		languageEnProp.setProperty("Mo_Wc_Txt_FullfilmentVariableSyntaxeInfo", "Filling variables can be done in the following syntax");
		languageEnProp.setProperty("Mo_Wc_Txt_FullfilmentVariableText", "Filling in the class instance variables");
		languageEnProp.setProperty("Mo_Wc_Txt_FullfilmentMyVariable", "Filling the variables created in the command editor");
		languageEnProp.setProperty("Mo_Wc_Txt_FullfilmentReplaceInfo", "Behind the value can be added the text (\" Some text. \"), The character ('x'), the number, the logical value or the mathematical calculation mentioned at the beginning.");
		languageEnProp.setProperty("Mo_Wc_Txt_FullfilmentReturnValue", "Further, the variable can be filled using the return value of the method, the method must return the same data type as the variable.");		
		languageEnProp.setProperty("Mo_Wc_Txt_Fullfilment_By_Static_Method", "Or, using a return value from a static method");
        languageEnProp.setProperty("Mo_Wc_Txt_Fullfilment_By_Static_Method_In_Inner_Class", "Or by return value from the static inner class static method");
		languageEnProp.setProperty("Mo_Wc_Txt_FullfilmentVariableSameRules", "For the above-mentioned method applies the same rules as for creating a constructor (above).");
		languageEnProp.setProperty("Mo_Wc_Txt_FullfilmentVariableInfo_2", "Filling variables using other variables, it may be both variable from intance class and variable created in the command editor");
		languageEnProp.setProperty("Mo_Wc_Txt_FullfilmentEtc", "etc. in accordance with these rules.");
		languageEnProp.setProperty("Mo_Wc_Txt_PrintMethodInfo", "To output values to the 'output editor' (this window), you can use the command");
		languageEnProp.setProperty("Mo_Wc_Txt_PrintMethodInfo_2", "This method determines the parameters of what is, for instance variables in a class instance, it will attempt to determine the value of this variable and write it.");
		languageEnProp.setProperty("Mo_Wc_Txt_PrintMethodInfo_3", "It is also possible, for example, to dump the return value of the method, a character, text, constants, boolean, etc.");
		languageEnProp.setProperty("Mo_Wc_Txt_PrintMethodInfo_4", "If the method will contain more than one parameter, it must be separated by the '+' (plus) symbol, not counting operations except the exponent.");
		languageEnProp.setProperty("Mo_Wc_Txt_PrintMethodSyntaxeExample", "Sample syntax");
		languageEnProp.setProperty("Mo_Wc_Txt_PrintMethodWriteResult", "lists are");
		languageEnProp.setProperty("Mo_Wc_Txt_PrintMethodReturnValue", "return value method");
		languageEnProp.setProperty("Mo_Wc_Txt_DeclarationListInfo", "To declare List (collection) you can enter the following command, supported only ArrayList");
		languageEnProp.setProperty("Mo_Wc_Txt_DeclarationListInfo_2", "or create a List with parameters");
		languageEnProp.setProperty("Mo_Wc_Txt_DeclarationListInfo_3", "Data type List (collection) may be");
		languageEnProp.setProperty("Mo_Wc_Txt_Note", "Note");
		languageEnProp.setProperty("Mo_Wc_Txt_DeclarationListInfo_4", "The ArrayList constructor parameter requires a collection of values, so it is not in the above command (for simplicity).");
		languageEnProp.setProperty("Mo_Wc_Txt_DeclarationListInfo_5", "In the above syntax with parameters, you do not need to paste the data first into the collection, such as in the syntax: '... = new ArrayList<>(Arrays.asList(value1, value2, ...));'. Just enter the data as parameters without transferring it to the collection.");
		languageEnProp.setProperty("Mo_Wc_Txt_ListMethodInfo_1", "Above the aforementioned sheets (collections), hereinafter list, you can call the following methods");
		languageEnProp.setProperty("Mo_Wc_Txt_ListMethodInfo_2", "The method for adding an item to the end of list");
		languageEnProp.setProperty("Mo_Wc_Txt_ListMethodInfo_3", "The method for adding an item to the index specified in the letter, 0 <= index < size list");
		languageEnProp.setProperty("Mo_Wc_Txt_ListMethodInfo_4", "The method for deleting all of the items from the Letter - the \"flushed \"");
		languageEnProp.setProperty("Mo_Wc_Txt_ListMethodInfo_5", "A method for obtaining (return) list of items on a given index, 0 <= index <size list");
		languageEnProp.setProperty("Mo_Wc_Txt_ListMethodInfo_6", "The method, which sets the entry in the list on a given index, 0 <= index < size list");
		languageEnProp.setProperty("Mo_Wc_Txt_ListMethodInfo_7", "A method that copies items from a worksheet into a one-dimensional array and returns it");
		languageEnProp.setProperty("Mo_Wc_Txt_ListMethodInfo_8", "Method that returns the number of elements in the list, or sheet size. It works just like the length property of the one-dimensional array");
		languageEnProp.setProperty("Mo_Wc_Txt_ListMethodInfo_9", "A method that removes an entry from a specified index sheet, 0 <= index <worksheet size");
		languageEnProp.setProperty("Mo_Wc_Txt_ListMethodInfo_10", "List Index values must be assigned a number, not a variable or method, and the like.");
		languageEnProp.setProperty("Mo_Wc_Txt_DeclarationOneDimensionalArray", "One-dimensional field declaration (four ways)");
		languageEnProp.setProperty("Mo_Wc_Txt_OneDimArrayInfo_1", "It is necessary to define the data type of a field size, field size must be > 0");
		languageEnProp.setProperty("Mo_Wc_Txt_OneDimArrayInfo_2", "It is necessary to define the data type of the field and fill values");
		languageEnProp.setProperty("Mo_Wc_Txt_OneDimArrayInfo_3", "You need to define the data type of values ​​and fill that field with the return value of the");
		languageEnProp.setProperty("Mo_Wc_Txt_DataTypeArrayCanBe", "Array data type can be");
		languageEnProp.setProperty("Mo_Wc_Txt_OperationsWithArray", "Operations with field");
		languageEnProp.setProperty("Mo_Wc_Txt_OperationsWithArray_1", "Adding value to the index in the array, 0 <= index < array size");
		languageEnProp.setProperty("Mo_Wc_Txt_OperationsWithArray_2", "Increment in postfix form");
		languageEnProp.setProperty("Mo_Wc_Txt_OperationsWithArray_3", "Increment in prefix form");
		languageEnProp.setProperty("Mo_Wc_Txt_OperationsWithArray_4", "Decrement in postfix form");
		languageEnProp.setProperty("Mo_Wc_Txt_OperationsWithArray_5", "Decrement in prefix form");
		languageEnProp.setProperty("Mo_Wc_Txt_OperationsWithArray_6", "To obtain values from a field on a given index");
		languageEnProp.setProperty("Mo_Wc_Txt_OperationsWithArray_7", "To get the number of elements in the array or field size");
		languageEnProp.setProperty("Mo_Wc_Txt_TwoDimArrayTitle", "Two-dimensional array");
		languageEnProp.setProperty("Mo_Wc_Txt_WorkWithTwoDimArrayInfo", "Working with a two-dimensional array is only possible over an instance of the class that is represented in the instance diagram.");
		languageEnProp.setProperty("Mo_Wc_Txt_CannotCreateTwoDimarray", "You can not create a two-dimensional array in the command editor.");
		languageEnProp.setProperty("Mo_Wc_Txt_OperationsWithTwoDimArray", "Two-dimensional array opperations");
		languageEnProp.setProperty("Mo_Wc_Txt_FullfilmentArrayInfo_1", "When filling the array variable other fields need to watch the data types of the fields.");
		languageEnProp.setProperty("Mo_Wc_Txt_FullfilmentArrayInfo_2", "If the variable type of a one-dimensional field of a data type is filled with another field of data type, the data type of the field into which");
		languageEnProp.setProperty("Mo_Wc_Txt_FullfilmentArrayInfo_3", "the second field \"overwrites\" is added to the data type of that input field. (Only in this editor - unresolved)");
		languageEnProp.setProperty("Mo_Wc_Txt_OperationsWithArrayInInstanceTitle", "Working with a one-dimensional array in a class instance");
		languageEnProp.setProperty("Mo_Wc_Txt_OperationsWithArrayInInstanceInfo", "For the one-dimensional array operations found in an instance of any class that is represented in the instance diagram, the same rules that are listed above apply.");
		languageEnProp.setProperty("Mo_Wc_Txt_OperationsWithArrayInInstanceDifference", "The only difference is access to the field or field type variables. This is an instance reference.");
		languageEnProp.setProperty("Mo_Wc_Txt_InfoAboutNullValue", "There are no null values ​​in the command editor! It can not be filled with a variable, a method parameter, a constructor,");
		
		// Info ohledně metod pro restartování a ukončení apliakce pomocí editoru příkazů:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_InfoForMethodsOverApp", "Texts about methods that can be called in the command editor but used to 'control' the application.");
		languageEnProp.setProperty("Mo_Wc_Txt_InfoForMethodsOverApp", "The following are the methods that are added exceptionally only for this application.");
		
		// Restartování aplikace:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_RestartAppTitle", "Texts to report to the output editor about the method to restart an application using appropriate methods that can be entered in the command editor.");
		languageEnProp.setProperty("Mo_Wc_Txt_RestartAppTitle", "Restart application");
		// Info k restartu - že se uložídiagram tříd apod.
		languageEnProp.setProperty("Mo_Wc_Txt_InfoAboutCloseOpenFilesBeforeRestartApp", "Before restarting the application, it is best to close any open files in the source code editor or the project explorer, because the changes will not be saved.");
		languageEnProp.setProperty("Mo_Wc_Txt_InfoAboutLaunchCountdownWithCommandIsEntered", "After confirming one of the following commands, the countdown will be started until the application restarts.");
		languageEnProp.setProperty("Mo_Wc_Txt_InfoAboutSaveClassDiagramBeforeRestartApp", "Before restarting the application, the class diagram will be saved in the open project and the necessary application data.");
		languageEnProp.setProperty("Mo_Wc_Txt_TwoWaysToRestartApp", "You can restart the application in two ways (by using the command editor)");
		languageEnProp.setProperty("Mo_Wc_Txt_FirstWayToRestartApp_1", "By calling the following method without parameters, the application will restart after");
		languageEnProp.setProperty("Mo_Wc_Txt_FirstWayToRestartApp_2", "seconds.");
		languageEnProp.setProperty("Mo_Wc_Txt_SecondWayToRestartApp", "By calling the following method with the parameters, the application will restart after the X seconds specified in the method parameter. For X, only the natural number in the");
		// Ukončení aplikace:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_CloseAppTitle", "Texts for Reporting to the Output Editor on the Exit Method using the appropriate methods that can be entered in the Command Editor.");
		languageEnProp.setProperty("Mo_Wc_Txt_CloseAppTitle", "Exit application");
		languageEnProp.setProperty("Mo_Wc_Txt_InfoAboutCloseOpenFilesBeforeCloseApp", "You should close any open files in your source code editor or project explorer before leaving the application, because the changes will not be saved.");
		languageEnProp.setProperty("Mo_Wc_Txt_InfoAboutLaunchCountdownAfterCommandIsEntered", "After confirming one of the following commands, the countdown will be run until the application closes.");
		languageEnProp.setProperty("Mo_Wc_Txt_InfoAboutSaveClassDiagramBeforeCloseApp", "Before the application closes, the class diagram will be saved in the open project and the necessary application data.");
		languageEnProp.setProperty("Mo_Wc_Txt_ThreeWaysToCloseApp", "The application can be closed in three ways (by using the command editor)");
		languageEnProp.setProperty("Mo_Wc_Txt_FirstWayToCloseApp_1", "By calling the following method without parameters, the application will terminate after");
		languageEnProp.setProperty("Mo_Wc_Txt_FirstWayToCloseApp_2", "seconds.");
		languageEnProp.setProperty("Mo_Wc_Txt_SecondWayToCloseApp", "By calling the following method with the parameters, the application will terminate after the X seconds specified in the method parameter. For X can be set the natural number in the");
		languageEnProp.setProperty("Mo_Wc_Txt_ThirdayToCloseApp_1", "By calling the following method with parameter 0 (zero), the application will end after");
		languageEnProp.setProperty("Mo_Wc_Txt_ThirdayToCloseApp_2", "seconds.");
		languageEnProp.setProperty("Mo_Wc_Txt_InfoAboutThirdWayToCloseApp_1", "This method is the same as terminating an application using the Java source code, but you can enter the following method only with parameter 0 (zero),");
		languageEnProp.setProperty("Mo_Wc_Txt_InfoAboutThirdWayToCloseApp_2", "but in Java, other values ​​(indexes) can be entered after the method parameter, which can mark the code of an error. For example, -1 or 1, etc.");
		// Zrušení odpočtu pro restartování nebo ukončení aplikace:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_CancelCountdownTitle", "Texts for Reporting to the Output Editor on the Quit Method for Time to Restart or Exit the Application.");
		languageEnProp.setProperty("Mo_Wc_Txt_CancelCountdownTitle", "Cancel countdown to exit or restart application");
		languageEnProp.setProperty("Mo_Wc_Txt_CancelCountdownInfo", "If the countdown is run to exit or restart the application, you can end it by entering the following command.");
		
		
		// Texty, že nebyl nalezen soubor .class pro zavolaní staticke metody a nebo konstruktoru:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_File_Dot_Class", "Text for error messages about failing to find a compiled / translated class (.class file) when trying to call a static method or a constructor.");
		languageEnProp.setProperty("Mo_Wc_Txt_File_Dot_Class", "The .class file, created after compiling the");
		languageEnProp.setProperty("Mo_Wc_Txt_File_Dot_Class_Was_Not_Found", "was not found in the bin directory of the currently open project.");
        languageEnProp.setProperty("Mo_Wc_Txt_Failed_To_Find_The_Class", "Unable to find the class");
        languageEnProp.setProperty("Mo_Wc_Txt_Failed_To_Find_Bin_Dir", "nor directory 'bin' with compiled classes.");
        languageEnProp.setProperty("Mo_Wc_Txt_Failed_To_Find_Inner_Class", "nor its inner class");
        languageEnProp.setProperty("Mo_Wc_Txt_Inner_Class_Is_Not_Static", "The inner class is not static.");
        languageEnProp.setProperty("Mo_Wc_Txt_Inner_Class_Is_Not_Public_Either_Protected", "Inner class is not accessible. It must be marked with the modifier 'public' or, 'protected'.");
		languageEnProp.setProperty("Mo_Wc_Txt_Specific_Method_Was_Not_Found", "did not find the method with the specified name or parameters, or it is not a static method or the return type data type does not match.");
		languageEnProp.setProperty("Mo_Wc_Txt_Public_Constructor_Was_Not_Found", "Failed to find a public constructor with the specified number or type of parameters, or the class data type (with the constructor) does not match.");
		
		// Text pro zavolání konstruktoru:
		languageEnProp.setProperty("Mo_Wc_Txt_Constructor_Was_Not_Found", "Failed to find public constructor with specified number or type of parameters.");
		
		// Text že nebyla vytvořena žádná proměnná v editoru příkazů:
		languageEnProp.setProperty("Mo_Wc_Txt_No_Variable_Was_Created", "No variables created.");
		
		// Texty pro operace s jednorozměrným polem v instanci třídy:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_CastingToIntegerFailure_1", "Text for error notifications that failed to perform any one-dimensional field operation in a class instance.");
		languageEnProp.setProperty("Mo_Wc_Txt_CastingToIntegerFailure_1", "Failed to cast number");
		languageEnProp.setProperty("Mo_Wc_Txt_CastingToIntegerFailure_2", "to the Integer data type.");
		languageEnProp.setProperty("Mo_Wc_Txt_InstanceOfClassInIdNotFound", "Instance of class not found. Instance");
		languageEnProp.setProperty("Mo_Wc_Txt_EnteredVariable_1", "Entered variable");
		languageEnProp.setProperty("Mo_Wc_Txt_EnteredVariable_2", "is not a array type.");
		languageEnProp.setProperty("Mo_Wc_Txt_IsNotOneDimArray", "is not a one-dimensional array.");
		languageEnProp.setProperty("Mo_Wc_Txt_IndexValueAtArrayIsNotInInterval", "The index value in the array must be >= 0 and smaller than the array size, i.e. the index must be in the range");
		languageEnProp.setProperty("Mo_Wc_Txt_ValueInArrayIsNull", "The value at the specified index in field is null.");
		
		// Texty pro operace s dvojrozměrným polem:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_IsNotTwoDimArray", "Texts for error notifications that failed to perform any operation over a two-dimensional array in an instance of a class.");
		languageEnProp.setProperty("Mo_Wc_Txt_IsNotTwoDimArray", "is not a two-dimensional array.");
		languageEnProp.setProperty("Mo_Wc_Txt_FailedToSetValueToTwoDimArray", "Failed to set value on given index, incorrect indexes were probably entered.");
		languageEnProp.setProperty("Mo_Wc_Txt_Index_1_Text", "Index 1");
		languageEnProp.setProperty("Mo_Wc_Txt_Index_2_Text", "Index 2");
		languageEnProp.setProperty("Mo_Wc_Txt_FailedToGetValueFromTwoDimArray", "Unable to get value on given index, error indexes were probably entered.");
		languageEnProp.setProperty("Mo_Wc_Txt_FailedToIncrementValueAtIndexInTwoDimArrayInIntance", "Failed to increment value on specified index, not a number or a null value.");
		languageEnProp.setProperty("Mo_Wc_Txt_FailedToDecrementValueAtIndexInTwoDimArrayInIntance",  "Failed to decrement value on specified index, not a number or a null value.");
		languageEnProp.setProperty("Mo_Wc_Txt_ValueWasnotFoundAtindexinTwoDimArrayInInstance", "No value found on the specified index.");
		
		// Texty pro ukončení a restart aplikace dle zadaného příkazu v editoru příkazů:
		
		// Texty pro ukončení odpočtu:
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_NoCountdownIsRunning", "Notification texts for terminating the countdown to restart or exit the application after entering the command in the command editor.");
		languageEnProp.setProperty("Mo_Wc_Txt_NoCountdownIsRunning", "No countdown is triggered.");
		languageEnProp.setProperty("Mo_Wc_Txt_CountdownIsStopped", "Countdown stopped.");
		
		// Texty pro spuštění timeru pro ukončení aplikace.
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_CountdownForClosAppIsRunning", "Notification texts for starting a deduction to exit the application after entering the command in the command editor.");
		languageEnProp.setProperty("Mo_Wc_Txt_CountdownForClosAppIsRunning", "The countdown for terminating the application is already running.");
		languageEnProp.setProperty("Mo_Wc_Txt_CountdownForCloseAppIsNotPossibleToStart", "Can not start the countdown to exit the application that is running the reboot to restart the application.");
		
		languageEnProp.setProperty("Mo_Wc_Txt_ToEndOfAppRemains_1", "Terminationg the application remains");
		languageEnProp.setProperty("Mo_Wc_Txt_ToEndOfAppRemains_2", "Terminationg the application remains");
		languageEnProp.setProperty("Mo_Wc_Txt_ToEndOfAppRemains_3", "Terminationg the application remains");
		
		languageEnProp.setProperty("Mo_Wc_Txt_SecondsForClose_1", "seconds.");
		languageEnProp.setProperty("Mo_Wc_Txt_SecondsForClose_2", "seconds.");
		languageEnProp.setProperty("Mo_Wc_Txt_SecondsForClose_3", "second.");
		
		
		// Texty pro spuštění timeru pro restartování aplikace:		
		ConfigurationFiles.commentsMap.put("Mo_Wc_Txt_CountdownForRestartAppIsRunning", "Readout notification texts for restarting the application after entering the command in the command editor.");
		languageEnProp.setProperty("Mo_Wc_Txt_CountdownForRestartAppIsRunning", "The countdown for restarting the application is already running.");
		languageEnProp.setProperty("Mo_Wc_Txt_CountdownForRestartAppIsNotPossibleToStart", "Unable to start the countdown to restart the application that is running the shutdown to terminate the application.");
		

		languageEnProp.setProperty("Mo_Wc_Txt_ToRestartAppRemains_1", "Restarting the application remains");
		languageEnProp.setProperty("Mo_Wc_Txt_ToRestartAppRemains_2", "Restarting the application remains");
		languageEnProp.setProperty("Mo_Wc_Txt_ToRestartAppRemains_3", "Restarting the application remains");
		
		languageEnProp.setProperty("Mo_Wc_Txt_SecondsForRestart_1", "seconds.");
		languageEnProp.setProperty("Mo_Wc_Txt_SecondsForRestart_2", "seconds.");
		languageEnProp.setProperty("Mo_Wc_Txt_SecondsForRestart_3", "second.");
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
		// Texty do RefreshInstance - pouze do chybové hlášky s informací o tom, že došlo k chybě při vytváření nové instance:
		ConfigurationFiles.commentsMap.put("Mo_Txt_FailedToUpdateInstanceOfClass", "Texts into one error message that can occur when creating an instance when updating individual instances that are displayed in the instance diagram. This message can occur especially if the class whose instance is to be rebuilt or has the default 'nullary 'constructor.'");
		languageEnProp.setProperty("Mo_Txt_FailedToUpdateInstanceOfClass", "Failed to update an instance of the class");
		languageEnProp.setProperty("Mo_Txt_FailedToUpdateInstanceOfClass_2", "the most common cause of this error is that the class does not contain the default \"nullary\" constructor, please make sure that the constructor is located in that class, \n  other possible causes: it is an abstract class, interface, field, primitive type or void , or another reason.");
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
		// Třída JavaHomeForm.java - všechny texty:
		// texty do JradioButtonu:
		ConfigurationFiles.commentsMap.put("Jhf_DialogTitle", "Texts in a dialog that contains components for setting up the Java home directory for an application, such as the path to the directory where the Java device is installed on the device, or the necessary files that are then copied to a directory called a workspace and set JRadioButton Components in the dialog.");
		languageEnProp.setProperty("Jhf_DialogTitle", "Settings");
		languageEnProp.setProperty("Jhf_RbSetJavaHomeDir", "Select the location of the home directory of Java.");
		languageEnProp.setProperty("Jhf_RbCopySomeFiles", "Select the files needed to compile.");
		languageEnProp.setProperty("Jhf_RbSetDefault", "Set as the default home directory folder in");

		
		// Popisky do labeluů:
		ConfigurationFiles.commentsMap.put("Jhf_LblJavaHomeDirInfo", "Label labels in a dialog box that contains components for setting up the Java application home directory, such as the directory path where the Java device is installed on the device, or the necessary files that are then copied to a directory called a workspace and the path is set to JRadioButton in the dialog.");
		languageEnProp.setProperty("Jhf_LblJavaHomeDirInfo", "Choose the path of the home directory of Java");
		languageEnProp.setProperty("Jhf_LblJavaHomeLibDirInfo", "The selected directory must contain the 'lib' directory, which must contain at least");
		languageEnProp.setProperty("Jhf_LblCopySomeFilesInfo", "Select the files to be copied to a directory");

		
		// Tlačítka:
		ConfigurationFiles.commentsMap.put("Jhf_BtnSelectJavaHomeDir", "Button text in the dialog box that contains the components for setting up the Java home directory for the application, such as the path to the directory where the Java device is installed on the device, or the necessary files that are then copied to a directory called a workspace and the path is set to JRadioButton in the dialog.");
		languageEnProp.setProperty("Jhf_BtnSelectJavaHomeDir", "Select location");
		languageEnProp.setProperty("Jhf_Btn_OK", "OK");
		languageEnProp.setProperty("Jhf_Btn_Cancel", "Cancel");

		
		// Text do výběru textu:
		ConfigurationFiles.commentsMap.put("Jhf_TextChooseText", "Label texts that tell users how to select the appropriate file or path to the Java directory in a dialog that contains the components for setting up the Java App Home folder, such as the path to the directory where the Java device is installed on the device, or mark the necessary files that are then copied to a directory labeled as a workspace and set to a path that is the text for the JRadioButton components in the dialog.");
		languageEnProp.setProperty("Jhf_TextChooseText", "Choose");
		languageEnProp.setProperty("Jhf_TextChooseFile", "Select the file");

		// texty do hlasek:
		ConfigurationFiles.commentsMap.put("Jhf_TxtFilesNotFoundText", "Texts in error messages that can occur in some cases to indicate bad files or to set wrong paths, etc. These are all texts in error messages in a dialog that contains the components for setting up the Java home directory for an application. the directory where the Java device is installed on the device, or the necessary files that are then copied to the workspace marked as a workspace and the path is set to JRadioButton in the dialog.");
		languageEnProp.setProperty("Jhf_TxtFilesNotFoundText", "The following file (/s) found");
		languageEnProp.setProperty("Jhf_TxtFilesNotFoundTitle", "Files not found");
		languageEnProp.setProperty("Jhf_TxtBadFormatClassPathText", "At least one file path is entered in the wrong format!");
		languageEnProp.setProperty("Jhf_TxtBadFormatClassPathTitle", "Wrong format");
		languageEnProp.setProperty("Jhf_TxtIsNeededToFillPathText", "You must fill out the path to at least one file!");
		languageEnProp.setProperty("Jhf_TxtIsNeededToFillPathTitle", "Empty fields");
		languageEnProp.setProperty("Jhf_TxtMissingLibOrImportantFilesText", "The lib directory has not been found in the selected Java directory, or at least one of the following files has not been found");
		languageEnProp.setProperty("Jhf_TxtMissingLibOrImportantFilesTitle", "Missing file (/s");
		languageEnProp.setProperty("Jhf_TxtChoosedDirDoesNotExistText", "The selected directory already exists, please choose another!");
		languageEnProp.setProperty("Jhf_TxtChoosedDirDoesNotExistTitle", "The directory does not exist");
		languageEnProp.setProperty("Jhf_TxtPathIsNotInCorrectFormatText", "The path is not in the correct format!");
		languageEnProp.setProperty("Jhf_TxtPathIsNotInCorrectFormatTitle", "Wrong format");
		languageEnProp.setProperty("Jhf_TxtPathToHomeDirIsEmptyText", "The path to the home directory for Java is not filled!");
		languageEnProp.setProperty("Jhf_TxtPathToHomeDirIsEmptyTitle", "Empty field");
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
		// Veškeré texty do třídy: OperationsWithInstances.java z balíčku: instances:
		ConfigurationFiles.commentsMap.put("Owi_Txt_MethodIsNotPublic", "Texts for error statements in the output editor that can occur when performing an operation over instances, such as calling a method, creating an instance, etc.");
		languageEnProp.setProperty("Owi_Txt_MethodIsNotPublic", "The method is inaccessible, probably not marked with public (or protected) modifier, (returned null).");
		languageEnProp.setProperty("Owi_Txt_MethodIsNotInstanceOfCLass", "The most common cause of this error is that the method is not in the specified class instance, or different numbers or types of parameters! (Returned null)");
		languageEnProp.setProperty("Owi_Txt_ErrorInCallTheMethod", "Error when calling! (Returned null)");
		languageEnProp.setProperty("Owi_Txt_ErrorDuringCreateInstance", "An error occurred while creating an instance of the class!");
		languageEnProp.setProperty("Owi_Txt_PossibleCausesCreateInstanceError", "Possible causes: This is an abstract class, interface, constructor not found, etc.");
		languageEnProp.setProperty("Owi_Txt_Class", "Class");
		languageEnProp.setProperty("Owi_Txt_CannotCreateInstanceFromAbstractClass", "This is an abstract class, from that you can not create an instance!");
		languageEnProp.setProperty("Owi_Txt_ConstructorIsNotPublic", "The constructor is not accessible. Not marked public modifier.");
		languageEnProp.setProperty("Owi_Txt_ConstructorWantDifferentCountOrTypesOfParameters", "The class constructor expects a different number of parameters or data types than they have been awarded!");
		languageEnProp.setProperty("Owi_Txt_ConstructorWant", "The constructor expects");
		languageEnProp.setProperty("Owi_Txt_ErrorWhileCallingConstructorByThrowAnException", "There was an error to call the constructor, this error can occur if the underlying constructor throws an exception.");
		languageEnProp.setProperty("Owi_Txt_ButGot", "But he got");
		languageEnProp.setProperty("Owi_Txt_ErrorDuringCallBaseConstructor", "There was an exception when calling the default 'nullary' constructor!");
		languageEnProp.setProperty("Owi_Txt_ButDidGot", "But did not get any parameters!");
		languageEnProp.setProperty("Owi_Txt_ReferenceNameExistMustChange", "Reference name already exists, it must be changed!");
		languageEnProp.setProperty("Owi_Txt_Reference", "Reference");
		
		// Texty do metody callConstructor:
		ConfigurationFiles.commentsMap.put("Owi_Txt_Failed_To_Call_Constructor_In_Abstract_Class", "Texts for error output output statements that can occur when calling a constructor using a command editor, such as calling the constructor without the purpose of creating a reference to the instance.");
		languageEnProp.setProperty("Owi_Txt_Failed_To_Call_Constructor_In_Abstract_Class", "Unable to call constructor in abstract class, constructor");
		languageEnProp.setProperty("Owi_Txt_Inserted_Parameters", "Entered parameters");
		languageEnProp.setProperty("Owi_Txt_Required_Parameters", "Constructor requires");
		languageEnProp.setProperty("Owi_Txt_Failed_To_Call_Constructor", "Failed to call constructor");
		languageEnProp.setProperty("Owi_Txt_Constructor_Is_Not_Accessible", "The most common cause of this error is that the constructor is not publicly available.");
		languageEnProp.setProperty("Owi_Txt_Argument_Error", "the number of specified and required parameters may be different, or if a conversion failure for primitive arguments fails, or if the expandable parameter can not be converted to the appropriate formal parameter type by conversion invocation method, or if this constructor concerns the enum type.");
		languageEnProp.setProperty("Owi_Txt_Constructor_Throw_An_Exception", "this error can occur if the constructor drops an exception.");
		languageEnProp.setProperty("Owi_Txt_ReferenceVariable", "reference variable");
				
				
				
				
				
				
				
				
				
				
				
				
				
				
		// Texty do tříd v balíčku: projectExplorer:
		// Třída GetIndexOfExcelSheetForm.java:
		ConfigurationFiles.commentsMap.put("Pe_Gioesf_DialogTitle", "Text to the dialog box used to select the worksheet in the excel file to open (in the Project Explorer dialog).");
		languageEnProp.setProperty("Pe_Gioesf_DialogTitle", "Selection sheet");
		languageEnProp.setProperty("Pe_Gioesf_LblInfo", "Select the sheet to open");
		languageEnProp.setProperty("Pe_Gioesf_BtnOK", "OK");
		languageEnProp.setProperty("Pe_Gioesf_BtnCancel", "Cancel");

		// Třída: GetNewFileNameForm.java:
		ConfigurationFiles.commentsMap.put("Pe_Gnfnf_DialogTitle", "Texts in the dialog box for entering a new file / directory name This dialog opens when you click on the item to rename an item in the tree in the project explorer, the texts in the dialog box for both labels and error messages.");
		languageEnProp.setProperty("Pe_Gnfnf_DialogTitle", "Rename item");
		languageEnProp.setProperty("Pe_Gnfnf_LblInfo", "Enter a new name for the item");
		languageEnProp.setProperty("Pe_Gnfnf_LblErrorInfo", "The name is in the wrong format (only uppercase and lowercase letters, numbers and underscores).");
		languageEnProp.setProperty("Pe_Gnfnf_LblNote", "This is a simple renaming items, no refactoring!");
		languageEnProp.setProperty("Pe_Gnfnf_BtnOk", "OK");
		languageEnProp.setProperty("Pe_Gnfnf_BtnCalcel", "Cancel");
		languageEnProp.setProperty("Pe_Gnfnf_TxtFileNameAlreadyExistText", "The item with the specified name already exists at that location!");
		languageEnProp.setProperty("Pe_Gnfnf_TxtFileNameTextName", "Name");
		languageEnProp.setProperty("Pe_Gnfnf_TxtFileNameAlreadyExistTitle", "Duplicate name");
		languageEnProp.setProperty("Pe_Gnfnf_WrongFileNameText", "The item name does not match the desired format!");
		languageEnProp.setProperty("Pe_Gnfnf_TxtWrongFileNameTitle", "Wrong format");
		languageEnProp.setProperty("Pe_Gnfnf_TxtTextFieldIsEmptyText", "The field for the new entry name is empty!");
		languageEnProp.setProperty("Pe_Gnfnf_TxtTextFieldIsEmptyTitle", "Empty field");
		
		// Třída TreeTreeStructure v project Explorer - pouze texty pro položky pro rozšíření a chování větve.
		ConfigurationFiles.commentsMap.put("Pe_Mt_Txt_Expand", "Text for the context menu for an item that is used to expand or collapse a designated branch that denotes a directory.");
		languageEnProp.setProperty("Pe_Mt_Txt_Expand", "Expand");
		languageEnProp.setProperty("Pe_Mt_Txt_Collapse", "Collapse");
		languageEnProp.setProperty("Pe_Mt_Txt_ExpandLeaf", "Expand branch");
		languageEnProp.setProperty("Pe_Mt_Txt_CollapseLeaf", "Collapse branch");
				
		// Třída: JtreePopupMenu.java:
		// Texty do tlačítek v menu:
		ConfigurationFiles.commentsMap.put("Pe_Jpm_MenuNew", "Texts in the popup menu above the items in the tree in the project explorer This context menu opens by right-clicking on any item in the tree structure of the project explorer dialog, only texts that are used as items in the context menu.");
		languageEnProp.setProperty("Pe_Jpm_MenuNew", "New");
        languageEnProp.setProperty("Pe_Jpm_MenuOpen", "Open");
		languageEnProp.setProperty("Pe_Jpm_ItemOpenFileInInternalFrame", "Open in the internal window");
        languageEnProp.setProperty("Pe_Jpm_ItemOpenInFileExplorerInOs", "In the file explorer");
        languageEnProp.setProperty("Pe_Jpm_ItemOpenInDefaultAppInOs", "In the default application");
		languageEnProp.setProperty("Pe_Jpm_ItemRefresh", "Refresh");
		languageEnProp.setProperty("Pe_Jpm_ItemNewFile", "File");
		languageEnProp.setProperty("Pe_Jpm_ItemNewFolder", "Directory");
		languageEnProp.setProperty("Pe_Jpm_ItemNewProject", "Project");
		languageEnProp.setProperty("Pe_Jpm_ItemNewExcelList", "Sheet");
		languageEnProp.setProperty("Pe_Jpm_ItemDelete", "Delete");
		languageEnProp.setProperty("Pe_Jpm_ItemRename", "Rename");
		languageEnProp.setProperty("Pe_Jpm_ItemPaste", "Insert");
		languageEnProp.setProperty("Pe_Jpm_ItemCopy", "Copy");
		languageEnProp.setProperty("Pe_Jpm_ItemCut", "Cut");
				
		// Texty do hlášek:
		ConfigurationFiles.commentsMap.put("Pe_Jpm_Txt_FileAlreadyExistInDirText", "Text for error messages that can occur when performing any operations that are available in the context menu above any item in the tree structure in the Project Explorer dialog.");
		languageEnProp.setProperty("Pe_Jpm_Txt_FileAlreadyExistInDirText", "The specified file already exists in the selected directory, you can not create it!");
		languageEnProp.setProperty("Pe_Jpm_Txt_FileAlreadyExistInDirTitle", "File already exists");
		languageEnProp.setProperty("Pe_Jpm_Txt_CannotCreateFileText", "Failed to create the file named");
		languageEnProp.setProperty("Pe_Jpm_Txt_Location", "Location");
		languageEnProp.setProperty("Pe_Jpm_Txt_CantCreateFileTitle", "Failed to create file");
		languageEnProp.setProperty("Pe_Jpm_Txt_DirAlreadyExistText", "The specified directory already exists, not create it!");
		languageEnProp.setProperty("Pe_Jpm_Txt_DirAlreadyExistTitle", "Directory already exists");
		languageEnProp.setProperty("Pe_Jpm_Txt_ErrorWhileCreatingDirText", "An error occurred while creating a directory (/ directories)!");
		languageEnProp.setProperty("Pe_Jpm_Txt_ErrorWhileCreatingDirTitle", "Error creating");
		languageEnProp.setProperty("Pe_Jpm_Txt_CannotDeleteFilesFromOpenProjectText", "You can not delete files or directories from an open source project!");
		languageEnProp.setProperty("Pe_Jpm_Txt_CannotDeleteFilesFromOpenProjectTitle", "Error deleting");
		languageEnProp.setProperty("Pe_Jpm_Txt_FileDontExistAlreadyText", "Highlight the target location already does not exists");
		languageEnProp.setProperty("Pe_Jpm_Txt_FileDontExistAlreadyTitle", "item does not exist");
		languageEnProp.setProperty("Pe_Jpm_Txt_FailedToRenameFileText", "Failed to rename an item!");
		languageEnProp.setProperty("Pe_Jpm_Txt_OriginalName", "Original name");
		languageEnProp.setProperty("Pe_Jpm_Txt_NewName", "New name");
		languageEnProp.setProperty("Pe_Jpm_Txt_FailedToRenameFileTitle", "Error Renaming");
		languageEnProp.setProperty("Pe_Jpm_Txt_CannotRenameFileText", "Unable to rename an item from a currently open project!");
		languageEnProp.setProperty("Pe_Jpm_Txt_CannotRenameFileTitle", "Item used");
		languageEnProp.setProperty("Pe_Jpm_Txt_FileWithNameAlreadyExistText", "The target location already contains a folder with the same name!");
		languageEnProp.setProperty("Pe_Jpm_Txt_Target", "Target");
		languageEnProp.setProperty("Pe_Jpm_Txt_FileWithNameAlreadyExistTitle", "Error location");
		languageEnProp.setProperty("Pe_Jpm_Txt_ErrorWhileMovingFileText", "An error occurred while moving the marked directory on the target location!");
		languageEnProp.setProperty("Pe_Jpm_Txt_Source", "Source");
		languageEnProp.setProperty("Pe_Jpm_Txt_ErrorWhileMovingFileTitle", "Error moving directory");
		languageEnProp.setProperty("Pe_Jpm_Txt_FileAlreadyExistInLocationText", "Target location already contains a file with the same name!");
		languageEnProp.setProperty("Pe_Jpm_Txt_FileAlreadyExistInLocationTitle", "Fault location");
		languageEnProp.setProperty("Pe_Jpm_Txt_ErrorWhileMovingFileToLocationText", "An error occurred while moving the tagged file to the destination!");
		languageEnProp.setProperty("Pe_Jpm_Txt_ErrorWhileMovingFileToLocationTitle", "Error moving file");
		languageEnProp.setProperty("Pe_Jpm_Txt_TargetPlaceContainFileNameText", "The target location already contains a folder with the same name!");
		languageEnProp.setProperty("Pe_Jpm_Txt_TargetPlaceContainFileNameTitle", "Error location");
		languageEnProp.setProperty("Pe_Jpm_Txt_ErrorWhileCopyingDirText", "An error occurred while copying designated directories on the destination!");
		languageEnProp.setProperty("Pe_Jpm_Txt_ErrorWhileCopyingDirTitle", "Error copying directories");
		languageEnProp.setProperty("Pe_Jpm_Txt_ErrorWhileCopyingSelectedFileText", "An error occurred while copying the selected file to a target location!");
		languageEnProp.setProperty("Pe_Jpm_Txt_ErrorWhileCopyingSelectedFileTitle", "error copying file");
		languageEnProp.setProperty("Pe_Jpm_Txt_TargetLocationDontExistText", "Destination location no longer exists or is not a directory!");
		languageEnProp.setProperty("Pe_Jpm_Txt_TargetLocationDontExistTitle", "Error location");
		languageEnProp.setProperty("Pe_Jpm_Txt_SelectedFiledoesntExistText", "The previously tagged file or directory no longer exists!");
		languageEnProp.setProperty("Pe_Jpm_Txt_SelectedFileDoesntExistTitle", "Missing file");
		languageEnProp.setProperty("Pe_Jpm_Txt_CannotMoveFileFromOpenProjectText", "You can not move files or folders from an open project!");
		languageEnProp.setProperty("Pe_Jpm_Txt_CannotMoveFileFromOpenProjectTitle", "Error moving");
		languageEnProp.setProperty("Pe_Jpm_Txt_ErrorWhileDeletingDirText", "An error occurred while deleting the directory labeled!");
		languageEnProp.setProperty("Pe_Jpm_Txt_ErrorWhileDeletingDirTitle", "Error deleting");
		languageEnProp.setProperty("Pe_Jpm_Txt_ErrorWhileCreatingNewListInExcelFileText_1", "An error occurred while attempting to create a new worksheet named");
		languageEnProp.setProperty("Pe_Jpm_Txt_ErrorWhileCreatingNewListInExcelFileText_2", "Failed to create sheet.");
		languageEnProp.setProperty("Pe_Jpm_Txt_ErrorWhileCreatingNewListInExcelFileText_3", "File");
		languageEnProp.setProperty("Pe_Jpm_Txt_ErrorWhileCreatingNewListInExcelFileTitle", "Failed to create sheet");
				
				
		// Třída Menu:
		// Texty do hlavních menu:
		ConfigurationFiles.commentsMap.put("Pe_M_MenuFile", "Texts in 'main' tabs in the menu bar in the Project Explorer dialog box. These are text items for items that serve as individual 'menus / bookmarks' in the menu bar of the Project Explorer dialog.");
		languageEnProp.setProperty("Pe_M_MenuFile", "File");
		languageEnProp.setProperty("Pe_M_MenuEdit", "Edit");
		languageEnProp.setProperty("Pe_M_MenuTools", "Tools");
		languageEnProp.setProperty("Pe_M_MenuOrderFrames", "Organize");

		
		// Texty do položek v menuFile:
		ConfigurationFiles.commentsMap.put("Pe_M_ItemOpenFile", "Text for items in the 'File' tab, which serves as one of the 'menu' items in the menu bar of the Project Explorer dialog.");
		languageEnProp.setProperty("Pe_M_ItemOpenFile", "Open file");
        languageEnProp.setProperty("Pe_M_MenuOpenFileInAppsOfOs", "Open");
        languageEnProp.setProperty("Pe_M_ItemOpenFileInFileExplorerInOs", "In the file explorer");
        languageEnProp.setProperty("Pe_M_ItemOpenFileInDefaultAppInOs", "In the default application");
		languageEnProp.setProperty("Pe_M_ItemSave", "Save");
		languageEnProp.setProperty("Pe_M_ItemSaveAs", "Save as");
		languageEnProp.setProperty("Pe_M_ItemSaveAll", "Save all");
        languageEnProp.setProperty("Pe_M_Rb_Item_WrapTextInSelectedFrame", "Wrap text");
        languageEnProp.setProperty("Pe_M_Rb_Item_WrapTextInAllFrames", "Wrap the text everywhere");
        languageEnProp.setProperty("Pe_M_Rb_Item_ShowWhiteSpaceInSelectedFrame", "Show white space");
        languageEnProp.setProperty("Pe_M_Rb_Item_ShowWhiteSpaceInAllFrames", "Show white space everywhere");
        languageEnProp.setProperty("Pe_M_Rb_Item_ClearWhiteSpaceLineInSelectedFrame", "Remove white space");
        languageEnProp.setProperty("Pe_M_Rb_Item_ClearWhiteSpaceLineInAllFrames", "Remove white space everywhere");
		languageEnProp.setProperty("Pe_M_ItemReload", "Reload");
		languageEnProp.setProperty("Pe_M_ItemReloadAll", "Reload all");
		languageEnProp.setProperty("Pe_M_ItemPrint", "Print");
		languageEnProp.setProperty("Pe_M_ItemCloseSelectedWindow", "Close selected frame");
		languageEnProp.setProperty("Pe_M_ItemCloseAllWindow", "Close internal frame");
		languageEnProp.setProperty("Pe_M_ItemClose", "Close");

		
		// Popisky tlačítek v menuFile:
		ConfigurationFiles.commentsMap.put("Pe_M_TT_ItemOpenFile", "Labels for items in the 'File' tab, which serves as one of the 'menu' items in the menu bar of the Project Explorer dialog.");
		languageEnProp.setProperty("Pe_M_TT_ItemOpenFile", "Opens dialog box for selecting the file you want to open.");
        languageEnProp.setProperty("Pe_M_TT_ItemOpenFileInFileExplorerInOs", "Display the file in the marked internal window in the file explorer.");
        languageEnProp.setProperty("Pe_M_TT_ItemOpenFileInDefaultAppInOs", "Opens the file in the marked internal window in the application set to open a specific file type.");
		languageEnProp.setProperty("Pe_M_TT_ItemSave", "Save the text from the editor of the marked internal window back to the original file.");
		languageEnProp.setProperty("Pe_M_TT_ItemSaveAs", "This opens a dialog box for selecting a file where to save the file open in a marked internal window.");
		languageEnProp.setProperty("Pe_M_TT_ItemSaveAll", "All texts opened in the internal windows will be saved back to the original files.");
        languageEnProp.setProperty("Pe_M_TT_Rb_ItemWrapTextInSelectedFrame", "Wrap text in the selected internal frame / file.");
        languageEnProp.setProperty("Pe_M_TT_Rb_ItemWrapTextInAllFrames", "Wrap text in all open files / internal frames.");
        languageEnProp.setProperty("Pe_M_TT_Rb_ItemShowWhiteSpaceInSelectedFrame", "Show white space in the selected internal frame / file.");
        languageEnProp.setProperty("Pe_M_TT_Rb_ItemShowWhiteSpaceInAllFrames", "Show white space in all internal frames / open files.");
        languageEnProp.setProperty("Pe_M_TT_Rb_ItemClearWhiteSpaceLineInSelectedFrame", "Remove white space from a blank line by pressing the Enter key in the selected internal frame / open file.");
        languageEnProp.setProperty("Pe_M_TT_Rb_ItemClearWhiteSpaceLineInAllFrames", "Remove white space from a blank line by pressing the Enter key in all internal frames / open files.");
		languageEnProp.setProperty("Pe_M_TT_ItemReload", "Reloads the text from the file to the editor indicated in an internal window.");
		languageEnProp.setProperty("Pe_M_TT_ItemReloadAll", "Reloads all files opened in internal windows (if it exists).");
		languageEnProp.setProperty("Pe_M_TT_ItemPrint", "The window for the option to print text in a marked internal window.");
		languageEnProp.setProperty("Pe_M_TT_ItemCloseSelectedWindow", "Closes a file that is opened in a designated internal window.");
		languageEnProp.setProperty("Pe_M_TT_ItemCloseAllWindow", "Closes all open internal windows.");
		languageEnProp.setProperty("Pe_M_TT_ItemClose", "Closes the Project explorer.");

		// Texty do položek v menEdit:
		ConfigurationFiles.commentsMap.put("Pe_M_ItemIndentMore", "Text for items in the 'Edit' tab, which serves as one of the 'menu' items in the menu bar of the Project Explorer dialog.");
		languageEnProp.setProperty("Pe_M_ItemIndentMore", "Indent more");
		languageEnProp.setProperty("Pe_M_ItemIndentLess", "Indent less");
		languageEnProp.setProperty("Pe_M_ItemComment", "Comment");
		languageEnProp.setProperty("Pe_M_ItemUncomment", "Uncomment");
		languageEnProp.setProperty("Pe_M_ItemFormatCode", "Format code");
		languageEnProp.setProperty("Pe_M_ItemInsertMethod", "Insert method");
				
		// Popisky tlačítek v menuEdit:
		ConfigurationFiles.commentsMap.put("Pe_M_TT_ItemIndentMore", "Labels for items in the 'Edit' tab, which serves as one of the 'menu' items in the menu bar of the Project Explorer dialog.");
		languageEnProp.setProperty("Pe_M_TT_ItemIndentMore", "Indent the line where the cursor is located using the tab.");
		languageEnProp.setProperty("Pe_M_TT_IndentLess", "Removes the tab from the beginning of the line where the cursor is located.");
		languageEnProp.setProperty("Pe_M_TT_ItemComment", "Inserts line comment on the row where the cursor is located.");
		languageEnProp.setProperty("Pe_M_TT_ItemUncomment", "Removes line comment of the line where the cursor is located.");
		languageEnProp.setProperty("Pe_M_TT_ItemFormatCode", "Reformats source code in the editor at the designated internal window according to the syntax of Java.");
		languageEnProp.setProperty("Pe_M_TT_ItemInsertMethod", "Inserts \"sample\" method on the cursor position.");

		// Texty do položek v menuTools:
		ConfigurationFiles.commentsMap.put("Pe_M_ItemReplaceText", "Text for items in the 'Tools' tab, which serves as one of the 'menu' items in the menu bar of the Project Explorer dialog.");
		languageEnProp.setProperty("Pe_M_ItemReplaceText", "Replace");
		languageEnProp.setProperty("Pe_M_ItemGoToLine", "Go on line");
		languageEnProp.setProperty("Pe_M_ItemGettersAndSetters", "Generate getters and setters");
		languageEnProp.setProperty("Pe_M_ItemConstructor", "Generate constructor");
		languageEnProp.setProperty("Pe_M_ItemCompile", "Compile");				
		languageEnProp.setProperty("Pe_M_ItemMenuItemAddSerialVersionId", "Add serial version ID");
		languageEnProp.setProperty("Pe_M_ItemAddDefaultSerialVersionId", "Add default serial version ID");
		languageEnProp.setProperty("Pe_M_ItemAddGeneratedSerialVersionId", "Add generated serial version ID");

		// Popisky tlačítek v menuTools:
		ConfigurationFiles.commentsMap.put("Pe_M_TT_ItemReplaceText", "Labels for items in the 'Tools' tab, which serves as one of the 'menu' items in the menu bar in the Project Explorer dialog.");
		languageEnProp.setProperty("Pe_M_TT_ItemReplaceText", "Displays a dialog where you can choose to replace the first occurrence of the text or all occurrences of the text in the source code editor.");
		languageEnProp.setProperty("Pe_M_TT_ItemGoToLine", "Inserts cursor on the line with the specified number.");
		languageEnProp.setProperty("Pe_M_TT_ItemToString", "Inserts the toString () method at the cursor position.");
		languageEnProp.setProperty("Pe_M_TT_ItemGettersAndSetters", "Opens a dialog for selecting items (variables) of the class to which it is possible to insert the access method (ie. Getter or setter).");
		languageEnProp.setProperty("Pe_M_TT_ItemConstructor", "Opens a dialog where you can mark items (variables) that can be added to parameter constructor.");
		languageEnProp.setProperty("Pe_M_TT_ItemCompile", "The classes that are in the class diagram are compiled.");
		languageEnProp.setProperty("Pe_M_TT_ItemAddDefaultSerialVersionId", "The default serial version ID will be inserted at the cursor position.");
		languageEnProp.setProperty("Pe_M_TT_ItemAddGeneratedSerialVersionId", "The newly generated serial version ID will be inserted at the cursor position.");

		// Texty do položek v menuOrderFrames:
		ConfigurationFiles.commentsMap.put("Pe_M_ItemTile", "Text for items in the 'Organize' tab, which serves as one of the 'menu' items in the menu bar of the Project Explorer dialog.");
		languageEnProp.setProperty("Pe_M_ItemTile", "Tile");
		languageEnProp.setProperty("Pe_M_ItemTileUnderHim", "Tiles below itselfs");
		languageEnProp.setProperty("Pe_M_ItemCascade", "Cascade");
		languageEnProp.setProperty("Pe_M_ItemMinimizeAllFrames", "Minimize All");

		// Popisky tlačítek v menuOrderFrames:
		ConfigurationFiles.commentsMap.put("Pe_M_TT_ItemTile", "Labels for Items in the 'Arrange' tab, which serves as one of the 'menu' items in the menu bar of the Project Explorer dialog.");
		languageEnProp.setProperty("Pe_M_TT_ItemTile", "Internal windows with open files are sorted into tiles or tables. (For a table, you need to open 4 or more windows.");
		languageEnProp.setProperty("Pe_M_TT_ItemTileUnderHim", "Internal windows with open files are arranged in the form of tiles, but underneath, respectively. single column table.");
		languageEnProp.setProperty("Pe_M_TT_ItemCascade", "Internal windows with open files will be sorted into so-called. Cascades.");
		languageEnProp.setProperty("Pe_M_TT_ItemMinimizeAllFrames", "Minimizes all open internal windows, respectively. open files.");

		// Texty do proměnných - do chybových hlášek:
		ConfigurationFiles.commentsMap.put("Pe_M_Txt_CannotSavePdfFileText", "Text for error messages that may occur when you perform one of the operations that allow individual items in the menu bar in the Project Explorer dialog box.");
		languageEnProp.setProperty("Pe_M_Txt_CannotSavePdfFileText", "This application does not store the file type '.pdf' is read only!");
		languageEnProp.setProperty("Pe_M_Txt_Location", "Location");
		languageEnProp.setProperty("Pe_M_Txt_CannotSavePdfFileTitle", "Unable to save file");
		languageEnProp.setProperty("Pe_M_Txt_FileCouldNotBeSavedText", "The file could not be saved!");
		languageEnProp.setProperty("Pe_M_Txt_FileCouldNotBeSavedTitle", "Error saving");
		languageEnProp.setProperty("Pe_M_Txt_ContainsAreDifferentText", "The contents of the editor and file are different, should the content of the editor be saved?");
		languageEnProp.setProperty("Pe_M_Txt_ContainsAreDifferentTitle", "Save file");
		languageEnProp.setProperty("Pe_M_Txt_FileDoesntExistText", "The file no longer exists, so it can not be printed!");
		languageEnProp.setProperty("Pe_M_Txt_OriginalLocation", "Original location");
		languageEnProp.setProperty("Pe_M_Txt_FileDoesntExistTitle", "File does not exist");
		languageEnProp.setProperty("Pe_M_Txt_ClassFileIsNotloadedText", "Failed to load translated - compiled '.class' file labeled class!");
		languageEnProp.setProperty("Pe_M_Txt_ClassFileIsNotLoadedTitle", "File was not loaded");
		languageEnProp.setProperty("Pe_M_Txt_CannotGeneratedAccessMethodsText", "This is not a class from a class diagram, you can not generate the access method!");
		languageEnProp.setProperty("Pe_M_Txt_ClassIsNotInClassDiagram", "Class is not in a class diagram");
		languageEnProp.setProperty("Pe_M_Txt_CannotGenerateConstructor", "This is not a class from a class diagram, you can not generate the constructor!");
		languageEnProp.setProperty("Pe_M_Txt_ClassDoesntExistAlreadyText", "The class already obtained location does not exist!");
		languageEnProp.setProperty("Pe_M_Txt_ClassDoesntExistAlreadyTitle", "Missing file");
		languageEnProp.setProperty("Pe_M_Txt_SrcDirIsMissingText", "The src directory containing the classes (including packages) was not found in an open project!");
		languageEnProp.setProperty("Pe_M_Txt_SrcDirIsMissingTitle", "Missing the src directory");
				
				
				
		// Třída InternalFrame.java:
		// texty do textových proměnných do chybových hlášek a nějakéých výpisů:
		ConfigurationFiles.commentsMap.put("Pe_Mif_Txt_SaveChangesText", "Text for error messages that can occur when performing an operation over an open file in the Project Explorer dialog (for example, saving a file, etc.).");
		languageEnProp.setProperty("Pe_Mif_Txt_SaveChangesText", "Do you want to save changes?");
		languageEnProp.setProperty("Pe_Mif_Txt_SaveChangesTitle", "Save changes");
		languageEnProp.setProperty("Pe_Mif_Txt_FileIsNotWrittenText", "The file could not be written to the new location!");
		languageEnProp.setProperty("Pe_Mif_Txt_Location", "Location");
		languageEnProp.setProperty("Pe_Mif_Txt_FileIsNotWrittenTitle", "Error saving");
		languageEnProp.setProperty("Pe_Mif_Txt_PdfFileCannotSaveText", "This application does not store the file type '.pdf'!");
		languageEnProp.setProperty("Pe_Mif_Txt_PdfFileCannotSaveTitle", "Unable to save file");
		languageEnProp.setProperty("Pe_Mif_Txt_ErrorWhileSavingFileText", "The file could not be saved!");
		languageEnProp.setProperty("Pe_Mif_Txt_ErrorWhileSavingFileTitle", "Error saving");
		languageEnProp.setProperty("Pe_Mif_Txt_ReadOnly", "Read-only");
		languageEnProp.setProperty("Pe_Mif_Txt_FileDoesntExistChooseNewLocationText", "File on the original location no longer exist, do you want to choose a new location for this file?");
		languageEnProp.setProperty("Pe_Mif_Txt_OriginalLocation", "Original location");
		languageEnProp.setProperty("Pe_Mif_Txt_FileDoesntExistChooseNewLocationTitle", "File does not exist");
				
				
		// Texty do třídy: ProjectExplorerWindowListener.java:
		ConfigurationFiles.commentsMap.put("Pe_Mwl_Txt_SaveChangesText", "Text for the dialog box in the Project Explorer dialog box that appears when the project explorer dialog is closed, with one (at least one) file being opened and edited, that is, the content is no longer the same as the one stored on the disk.");
		languageEnProp.setProperty("Pe_Mwl_Txt_SaveChangesText", "Do you want to save changes?");
		languageEnProp.setProperty("Pe_Mwl_Txt_SaveChangesTitle", "Save changes");

		// Texty do třídy NewExcelListNameForm.java
		// Texty do titulku dialogu a labelů s informacemi:
		ConfigurationFiles.commentsMap.put("Pe_Nelnf_DialogTitle", "Text for the dialog box used to get the name of a new worksheet in an excel file: This window can be opened when you choose to create a new file and specify one Excel Excel file (in the Project Explorer dialog box).");
		languageEnProp.setProperty("Pe_Nelnf_DialogTitle", "A new sheet");
		languageEnProp.setProperty("Pe_Nelnf_LblInfo", "Enter a name for the new worksheet in Excel");
		languageEnProp.setProperty("Pe_Nelnf_LblError", "Only uppercase and lowercase letters (with accents), numbers, and underscores.");
		
		// Texty do tlačítek:
		ConfigurationFiles.commentsMap.put("Pe_Nelnf_BtnOk", "Button texts in the dialog box used to get a new worksheet name in an excel file, which can be displayed when creating a new excel file in the Project Explorer dialog.");
		languageEnProp.setProperty("Pe_Nelnf_BtnOk", "OK");
		languageEnProp.setProperty("Pe_Nelnf_BtnCancel", "Cancel");
		
		// Texty do textových proměnných s hláškami:
		ConfigurationFiles.commentsMap.put("Pe_Nelnf_TxtErrorFileNameText", "Text for error messages for a dialog box that is used to get a new worksheet name in an Excel worksheet (in the Project Explorer dialog box) These messages are displayed if an error occurs, such as a valid new name for a new worksheet, or no name given at all etc.");
		languageEnProp.setProperty("Pe_Nelnf_TxtErrorFileNameText", "The worksheet name does not match the required syntax!");
		languageEnProp.setProperty("Pe_Nelnf_TxtErrorFileNameTextSyntaxe", "Syntax");
		languageEnProp.setProperty("Pe_Nelnf_TxtErrorFileNameTextSyntaxeInfo", "Uppercase and lowercase letters (with accents), numbers, and underscores.");
		languageEnProp.setProperty("Pe_Nelnf_TxtErrorFileNameTitle", "Incorrect syntax");
		languageEnProp.setProperty("Pe_Nelnf_TxtIsEmptyText", "Text field for the name of the worksheet is empty!");
		languageEnProp.setProperty("Pe_Nelnf_TxtIsEmptyTitle", "Empty field");
				
				
				
		// Třída: NewFileForm.java:
		ConfigurationFiles.commentsMap.put("Pe_Nff_DialogTitle", "Text for the dialog box used to get the name for the new file, which can be viewed by clicking on the item to create a new file in the tree in the Project Explorer dialog.");
		languageEnProp.setProperty("Pe_Nff_DialogTitle", "New file");
		languageEnProp.setProperty("Pe_Nff_LblInfo", "Enter a name and select a suffix ('type') file");
				
		// Texty do třídy. NewFolderForm.java:
		// texty do titulku dialogu, tlačítek a popisku - labelů
		ConfigurationFiles.commentsMap.put("Pe_N_Folder_F_DialogTitle", "Text for the dialog box that is used to get the name for the new directory, which can be viewed by clicking on the item to create a new directory in the Project Explorer dialog.");
		languageEnProp.setProperty("Pe_N_Folder_F_DialogTitle", "Create a directory (/ directories)");
		languageEnProp.setProperty("Pe_N_Folder_F_BtnOK", "OK");
		languageEnProp.setProperty("Pe_N_Folder_F_BtnCancel", "Cancel");
		languageEnProp.setProperty("Pe_N_Folder_F_LblErrorInfo", "The directory / directories are entered in the wrong format!");
		languageEnProp.setProperty("Pe_N_Folder_F_LblInfo", "Enter the directory / directories, for creating multiple directories use a decimal point");
		
		// Texty do chybovýchhlášek:
		ConfigurationFiles.commentsMap.put("Pe_N_Folder_F_TxtWrongFormatText", "Text for error messages for a dialog box to create a name for the new directory. This window can be viewed by clicking on an item to create a new directory in the Project Explorer dialog.");
		languageEnProp.setProperty("Pe_N_Folder_F_TxtWrongFormatText", "The directory format does not match the required syntax!");
		languageEnProp.setProperty("Pe_N_Folder_F_TxtWrongFormatTitle", "Incorrect syntax");
		languageEnProp.setProperty("Pe_N_Folder_F_TxtEmptyFieldText", "Field to enter the directory / directories is empty!");
		languageEnProp.setProperty("Pe_N_Folder_F_TxtEmptyFieldTitle", "Empty field");
				
		// Texty o třídy. ProjectExplorerDialog.java:
		// text do titulku dialogu:
		ConfigurationFiles.commentsMap.put("Pe_Ped_DialogTitle", "Title of the Project Explorer Dialog.");
		languageEnProp.setProperty("Pe_Ped_DialogTitle", "Project explorer");
		
		// texty do proměnných do chybových a informačních hlášek:
		ConfigurationFiles.commentsMap.put("Pe_Ped_Txt_CannotSavePdfFileText", "Text for error and information messages for the project explorer dialog. These are mostly texts for announcement, for example, that you can not open a file, etc.");
		languageEnProp.setProperty("Pe_Ped_Txt_CannotSavePdfFileText", "This application does not store the file type '.pdf' is read only!");
		languageEnProp.setProperty("Pe_Ped_Txt_Location", "Location");
		languageEnProp.setProperty("Pe_Ped_Txt_CannotSavePdfFileTitle", "Can not save file");
		languageEnProp.setProperty("Pe_Ped_Txt_FileIsAlreadyOpenText", "Belongs file is already open!");
		languageEnProp.setProperty("Pe_Ped_Txt_FileIsAlreadyOpenTitle", "File opened");
		languageEnProp.setProperty("Pe_Ped_Txt_ExcelDontContainsListText", "The highlighted Excel document does not contain any sheet!");
		languageEnProp.setProperty("Pe_Ped_Txt_File", "File");
		languageEnProp.setProperty("Pe_Ped_Txt_ExcelFileDontContainsListTitle", "The file contains no sheet");
		languageEnProp.setProperty("Pe_Ped_Txt_FileKind", "File Type");
		languageEnProp.setProperty("Pe_Ped_Txt_DoNotOpen", "You can not open in the code editor!");
		languageEnProp.setProperty("Pe_Ped_Txt_FileDoNotOpen", "Can not open file");
		languageEnProp.setProperty("Pe_Ped_Txt_FileDoNotExistText", "Required file does not exist or is a directory!");
		languageEnProp.setProperty("Pe_Ped_Txt_FileDoNotExistTitle", "File error");	
				
				
				
				
				
				
				
		// Texty do Výstupního terminálu - balíček redirectSysout:
		// texty do třídy OutputFrame.java:
		ConfigurationFiles.commentsMap.put("Of_FrameTitle", "Text to the 'Output Terminal 'dialog.'");
		languageEnProp.setProperty("Of_FrameTitle", "Output terminal");

		// Texty do třídy OutputFrameMenu.java:
		ConfigurationFiles.commentsMap.put("Of_Menu_MenuOptions", "Text for a single bookmark in the menu bar in the output terminal dialog.");
		languageEnProp.setProperty("Of_Menu_MenuOptions", "Options");		
		// Texty do pložek:
		ConfigurationFiles.commentsMap.put("Of_Menu_Clear", "Text for individual items on the 'Options' tab in the menu bar in the output terminal dialog.");
		languageEnProp.setProperty("Of_Menu_Clear", "Clear");
        languageEnProp.setProperty("Of_Menu_ClearSysOutScreen", "Clear the screen");
		languageEnProp.setProperty("Of_Menu_ItemSaveToFile", "Save to file");
		languageEnProp.setProperty("Of_Menu_Print", "Print");
		languageEnProp.setProperty("Of_Menu_ItemClose", "Close");
		languageEnProp.setProperty("Of_Menu_ItemClearAll", "All");
		
		// Popisky - tooltipy do položek v menu:
		ConfigurationFiles.commentsMap.put("Of_Menu_TT_ItemClose", "Text for the labels of each item on the 'Options' tab in the menu bar in the output terminal dialog.");
		languageEnProp.setProperty("Of_Menu_TT_ItemClose", "Close this window - Output terminal.");

		// Texty pro "skládání" vět pro rozdělení textů dle System.out a err:
		ConfigurationFiles.commentsMap.put("Of_Menu_Txt_And", "Text for item labels for deleting, storing or printing texts listed in the output terminal dialog (separately for System.out and System.err).");
		languageEnProp.setProperty("Of_Menu_Txt_And", "and");
		languageEnProp.setProperty("Of_Menu_Txt_DeleteTextIn", "Deletes lyrics in");
		languageEnProp.setProperty("Of_Menu_Txt_SaveTextIn", "Opens a dialog to select the location of the text file with text in");
		languageEnProp.setProperty("Of_Menu_Txt_PrintTextIn", "Displays a dialog for setting a printing options text in");

		// Texty do chových hlášek:
		ConfigurationFiles.commentsMap.put("Of_Menu_Txt_FileIsNotWrittenText", "Text for an error message about failing to save the text listed in the Output Terminator Editor window to the specified file This message may occur when the text save attempt in the v editor for the Output Terminal dialog fails.");
		languageEnProp.setProperty("Of_Menu_Txt_FileIsNotWrittenText", "The file could not be written!");
		languageEnProp.setProperty("Of_Menu_Txt_Location", "Location");
		languageEnProp.setProperty("Of_Menu_Txt_FileIsNotWrittenTitle", "Write error");

        // Texty do JChecBoxů pro vypisy hlaviček konstruktorů a metod a mazání textů před vypsáním výpisů z kontruktorů a metod:
        ConfigurationFiles.commentsMap.put("Of_Menu_Chcb_RecordCallConstructors", "Texts to a JCheckBox component that is located in the menu in the output terminal window. These are components for setting whether the names of the methods and constructors are to be written, or the parameters passed (if they are) when they are called. Next, whether the text field in the terminal is to be deleted before dumping from the called method or constructor - statements in System.out.");
        languageEnProp.setProperty("Of_Menu_Chcb_RecordCallConstructors", "Record constructor calls");
        languageEnProp.setProperty("Of_Menu_Chcb_RecordCallMethods", "Record method calls");
        languageEnProp.setProperty("Of_Menu_Chcb_RecordCallConstructors_TT", "Before writing to the console using methods in System.out, the header of the called constructor will be written.");
        languageEnProp.setProperty("Of_Menu_Chcb_RecordCallMethods_TT", "Before writing to the console using methods in System.out, the header of the called method will be written.");

        languageEnProp.setProperty("Of_Menu_Chcb_ClearSysOutScreenWhenCallConstructor", "When calling constructors");
        languageEnProp.setProperty("Of_Menu_Chcb_ClearSysOutScreenWhenCallMethod", "When calling methods");
        languageEnProp.setProperty("Of_Menu_Chcb_ClearSysOutScreenWhenCallConstructor_TT", "Before writing texts using methods in System.out located in the called constructor, the text field for these statements will be cleared.");
        languageEnProp.setProperty("Of_Menu_Chcb_ClearSysOutScreenWhenCallMethod_TT", "Before writing texts using methods in System.out located in the called method, the text field for these statements will be cleared.");
				
				
				
				
				
				
		// Texty do třídy ExceptionLogger - akorát texty do chybových hlášek:
		ConfigurationFiles.commentsMap.put("Ml_Txt_FailedToLoadOrCreatingFileText", "Text for error messages that can occur when reading or writing to a .log file. This is a file where exceptions (apps captured) are written, and the start and end of the application.");
		languageEnProp.setProperty("Ml_Txt_FailedToLoadOrCreatingFileText", "Failed to create or load a file");
		languageEnProp.setProperty("Ml_Txt_ForWriteException", "incurred for entering exceptions.");
		languageEnProp.setProperty("Ml_Txt_ErrorInAccessToFile", "This is an exception type when an error occurred while accessing the file");
		languageEnProp.setProperty("Ml_Txt_FailedToLoadOrCreatingFileTitle", "Unable to load file for logs");
		languageEnProp.setProperty("Ml_Txt_ErrorWhileOpeningFile", "This is an exception type when an error occurred while opening the file");
		languageEnProp.setProperty("Ml_Txt_ErrorWhileCreatingDirText", "An error occurred while trying to create directories on the following path");
		languageEnProp.setProperty("Ml_Txt_ErrorWhileCreatingDirTitle", "Error creating directory");
		
		
		
		
		
		
		
		
		
		// Texty do třídy / dialogu NameForInstance:		
		ConfigurationFiles.commentsMap.put("NFI_DialogTitle", "Texts in the dialog for entering a new reference that is needed when a call is made using the context menu above the class in the class diagram or above the instance in the instance diagram. This method returns a class instance that is in the class diagram, but the corresponding returned instance is not yet represented in the instance diagram, so the appropriate dialog is used to enter a new reference.");
		languageEnProp.setProperty("NFI_DialogTitle", "Create reference");
		languageEnProp.setProperty("NFI_LblInstanceClassInfo", "Class instance");
		languageEnProp.setProperty("NFI_LblInstance", "Instance");
		languageEnProp.setProperty("NFI_InsertReference", "Enter the reference");
		
		// Texty pro label, který ukazuje, že byla zadána chybná syntaxe nebo duplicitní hodnota:
		ConfigurationFiles.commentsMap.put("NFI_TxtBadSyntaxInReference", "Texts in the dialog box for entering a new reference when picking an instance from a called method, the following texts are for the label, and contain information that incorrect reference syntax or duplicate refrence has been entered.");
		languageEnProp.setProperty("NFI_TxtBadSyntaxInReference", "The reference you entered does not match the syntax you want.");
		languageEnProp.setProperty("NFI_TxtReferenceAlreadyExist", "The reference already exists.");
		
		// Texty do chybových hlášek:
		ConfigurationFiles.commentsMap.put("NFI_TxtJopEmptyFieldText", "Text for error messages in the Reference dialog for an instance returned from the called method.");
		languageEnProp.setProperty("NFI_TxtJopEmptyFieldText", "The input field is empty.");
		languageEnProp.setProperty("NFI_TxtJopEmptyFieldTitle", "Empty field");
		languageEnProp.setProperty("NFI_TxtjopBadSyntaxText", "The specified reference does not match the syntax you want, it must be in uppercase or lowercase, and can contain only case-insensitive numbers, digits, and underscores.");
		languageEnProp.setProperty("NFI_TxtJopBadSyntaxTitle", "Bad syntax");
		languageEnProp.setProperty("NFI_TxtJopDupliciteReferenceText", "The reference you have already made is used, please choose another.");
		languageEnProp.setProperty("NFI_TxtJopDupliciteReferenceTitle", "Duplicate references");
		
		// Texty pro tlačítka:
		ConfigurationFiles.commentsMap.put("NFI_BtnOk", "Button texts in a dialog that is used to specify a reference to an instance that has returned from a method and is not represented in the instance diagram.");
		languageEnProp.setProperty("NFI_BtnOk", "OK");
		languageEnProp.setProperty("NFI_BtnCancel", "Cancel");
		
		
		
		
		
				
				
		
		
		
		
		// Texty do dialogu pro informace o autorovi aplikace - dialog, resp. třída AboutAppForm.java:
		// Jendá se o některé texty , které je možné přeložit do více jazyků:
		ConfigurationFiles.commentsMap.put("Aaf_DialogTitle", "Some texts in the " + Constants.APP_TITLE + " author dialog box.");
		languageEnProp.setProperty("Aaf_DialogTitle", "About");
		languageEnProp.setProperty("Aaf_BtnClose", "Close");
		languageEnProp.setProperty("Aaf_BtnJavaVersion", "Java versions");
		languageEnProp.setProperty("Aaf_Txt_Name", "Name");
		languageEnProp.setProperty("Aaf_Txt_Version", "Version");
		languageEnProp.setProperty("Aaf_Txt_Author", "Author");
        languageEnProp.setProperty("Aaf_Txt_WikiAboutApp", "Wiki about the application");
		languageEnProp.setProperty("Aaf_Txt_Email", "Email");
		languageEnProp.setProperty("Aaf_Txt_Unknown", "Unknown");
        languageEnProp.setProperty("Aaf_Txt_DoesNotExist", "It does not exist");
		languageEnProp.setProperty("Aaf_Txt_DebugInfo", "Protocol runtime");
		languageEnProp.setProperty("Aaf_Txt_ForApp", "for application");

        ConfigurationFiles.commentsMap.put("Aaf_Txt_Path", "Text for error messages that can occur, for example, if a directory or file was not found when trying to open it for the context menu. In the author info dialog at the bottom with links to the paths to the selected files and directories.");
        languageEnProp.setProperty("Aaf_Txt_Path", "Path");
        languageEnProp.setProperty("Aaf_Txt_DirDoesNotExist_Text", "The directory on the following path was not found.");
        languageEnProp.setProperty("Aaf_Txt_DirDoesNotExist_Title", "Directory not found");
        languageEnProp.setProperty("Aaf_Txt_FileDoesNotExist_Text", "The file on the following path was not found.");
        languageEnProp.setProperty("Aaf_Txt_FileDoesNotExist_Title", "File not found");




        ConfigurationFiles.commentsMap.put("Aaf_Pm_Item_OpenInFileExplorer_Text", "Texts to the context menu that opens by right-clicking the labels / links at the bottom of the app authoring information summary dialog.");
        // Texty do položek:
        languageEnProp.setProperty("Aaf_Pm_Item_OpenInFileExplorer_Text", "Open in file explorer");
        languageEnProp.setProperty("Aaf_Pm_Item_OpenInProjectExplorer_Text", "Open in project explorer");
        languageEnProp.setProperty("Aaf_Pm_Item_OpenInCodeEditor_Text", "Open in code editor");
        languageEnProp.setProperty("Aaf_Pm_Item_OpenInDefaultApp_Text", "Open in default application");
        // Tooltipy do položek:
        languageEnProp.setProperty("Aaf_Pm_Item_OpenInFileExplorer_TT", "Opens the selected file or directory in the default file explorer (based on the platform used).");
        languageEnProp.setProperty("Aaf_Pm_Item_OpenInProjectExplorer_TT", "Opens the selected file or directory in the project explorer dialog.");
        languageEnProp.setProperty("Aaf_Pm_Item_OpenInCodeEditor_TT", "Opens the selected file or directory in the source code editor dialog.");
        languageEnProp.setProperty("Aaf_Pm_Item_OpenInDefaultApp_TT", "Opens the selected file or directory in the default application on the platform you are using.");
		
		
		
		
		
		
		
		
		
		// Texty do třídy, resp. vlákna CheckCompileFiles v balíčku App. jedná se pouze
		// o texty pro výpisy ohledné výsledku porovední příslušné operace asice
		// nalezení potřebných souborů pro aplikaci, aby mohla kompilovat Javovské
		// třídy.		
		ConfigurationFiles.commentsMap.put("Ccf_Jop_Txt_FilesAlreadyExistText", "Fibers that are used to find and copy files that this application needs to access to compile Java classes, these are texts that will only be displayed if there is, for example, some of the etc. However, these messages will only be displayed if the user starts this thread by clicking 'Search' in the 'Java' menu in the 'Tools' menu, otherwise the thread starts when the application is started (when it is set) but no buzz will be written, whether the files are found or not.");
		languageEnProp.setProperty("Ccf_Jop_Txt_FilesAlreadyExistText", "Required files for class compilation are already located on the given location");
		languageEnProp.setProperty("Ccf_Jop_Txt_FilesAlreadyExistTitle", "Files placed");
		languageEnProp.setProperty("Ccf_Jop_Txt_FilesSuccessfullyCopiedText", "The required files for class compilation have been successfully copied to the");
		languageEnProp.setProperty("Ccf_Jop_Txt_FilesSuccessfullyCopiedTitle", "Files copied");
		languageEnProp.setProperty("Ccf_Jop_Txt_FilesNotFoundText", "The required files for class compilation were not found, please use the appropriate dialog to set these files.");
		languageEnProp.setProperty("Ccf_Jop_Txt_FilesNotFoundTitle", "Files not found");
		
		
		
		
		
		
		
		
		
		
		// Texty do třídy InfoForm.java - dialog se základními informacemi ohledně ovládání aplikace.
		ConfigurationFiles.commentsMap.put("If_Txt_DialogTitle", "Texts in a dialog box that contains information about the control and basic features of this application.");
		languageEnProp.setProperty("If_Txt_DialogTitle", "Info");
		languageEnProp.setProperty("If_Txt_BasicInfo", "Basic application control");
		languageEnProp.setProperty("If_Txt_ClassDiagram", "Class diagram");
		languageEnProp.setProperty("If_Txt_CreateClass", "Create a class");
		languageEnProp.setProperty("If_Txt_InfoHowToCreateClass", "Class can be created by clicking the 'Create class' in the toolbar (left bar).");
		languageEnProp.setProperty("If_Txt_HowToWriteClassName", "You need to enter a class name, or even packages, in the class creation dialog.");
		languageEnProp.setProperty("If_Txt_InfoForClassNameWithoutPackages", "If packages are not specified, the class is inserted into the default 'defaultPackage' package.");
		languageEnProp.setProperty("If_Txt_ChooseTemplateForClass", "Furthermore, you must select one of the predefined templates, Java classes, eventually prepared by the method of 'main'.");
		languageEnProp.setProperty("If_Txt_AddRelations", "Adding relations");
		languageEnProp.setProperty("If_Txt_InfoForClickOnButton", "You need to mark the required relationship by clicking the button in the toolbar.");
		languageEnProp.setProperty("If_Txt_ChooseClassesByClick", "Then you must mark (by left-clicking) two classes in the class diagram, between which the relationship is to be created.");
		languageEnProp.setProperty("If_Txt_AddedSyntaxeToSourceCode", "A syntax will be added to the source code of the selected classes, which represents the selected relationship (the syntax depends on the selected relationship).");
		languageEnProp.setProperty("If_Txt_InfoForCreateEdge", "At the end, an arrow (edge) will be created in the class diagram between the designated classes, which will represent the selected relationship between the classes.");
		languageEnProp.setProperty("If_Txt_InfoForAddComment", "In the case of adding comments (available only to the class) you need to click on the appropriate button in the toolbar and choose the class to which to add a comment.");
		languageEnProp.setProperty("If_Txt_RemoveObjectOrRelations", "Removing objects / relations");
		languageEnProp.setProperty("If_Txt_InfoHowToRemoveObject", "You can remove the object from the class diagram by right-clicking the object and selecting 'Remove' in the context menu.");
		languageEnProp.setProperty("If_Txt_InfoForRemoveComment", "In the case of the comment arrow, the comment itself will be removed.");
		languageEnProp.setProperty("If_Txt_InfoHowToRemoveRelations", "In the case of an arrow that represents one of the possible relationships, the syntax will be removed from the source code of the classes (only within the recognized regular expressions).");
		languageEnProp.setProperty("If_Txt_InfoHowToRemoveClass", "In the case of a class, it is necessary to delete almost all the relations between the classes, then the class can be removed.");
		languageEnProp.setProperty("If_Txt_InstancesDiagram", "Instances diagram");
		languageEnProp.setProperty("If_Txt_CreateInstanceText", "Create an instance");
		languageEnProp.setProperty("If_Txt_InfoHowToCreateInstance", "You can create the instance by right-clicking on the class in the class diagram and choosing the constructor to be called in the context menu.");
		languageEnProp.setProperty("If_Txt_InfoAboutDialogForReference", "Then a dialog box will appear where you need to specify the name of the reference on the instance (or parameters - only some allowed).");
		languageEnProp.setProperty("If_Txt_InfoAboutAddedEdgeToDiagram", "Finally, if a constructor is successfully called (creating an instance), the representation of the constructor will be added to the instance diagram.");
		languageEnProp.setProperty("If_Txt_RemoveInstanceText", "Removing instance");
		languageEnProp.setProperty("If_Txt_InfoHowToRemoveInstance", "Instance can be removed by clicking 'Remove' in the context menu of the instance.");
		languageEnProp.setProperty("If_Txt_ShowContentMenu", "This menu can be displayed by clicking the right mouse button on an instance of the class.");
		languageEnProp.setProperty("If_Txt_DeleteInstanceByDelKey", "Next, you can remove the indicated instance in the instance diagram by pressing the Delete ('Del) key.");
		languageEnProp.setProperty("If_Txt_CreateRelationsText", "Creating relationships");
		languageEnProp.setProperty("If_Txt_InfoHowToCreateRelations", "Relations between instances can be created by filling in the relevant variables in the instance of the class.");
		languageEnProp.setProperty("If_Txt_InfoHowToFillVariable", "For example, you can fill the variables by calling the method or the command in the command editor.");
		languageEnProp.setProperty("If_Txt_CallingMethodText", "Calling methods");
		languageEnProp.setProperty("If_Txt_InfoHowToCallMethod", "You can call the method by using a popup menu above the class in the class diagram or above the instance in the instance diagram.");		
		languageEnProp.setProperty("If_Txt_InfoCallStaticMethodInContextMenuOverClass", "Only static methods can be called above class in the class diagram, just select in the context menu above the class in the 'Static Methods' or 'Inherited Static Methods' tab.");
		languageEnProp.setProperty("If_Txt_InfoCallMethodOverInstanceInId", "Instances in the instance diagram can call methods that are not static, just select the appropriate method in the context menu above the instance, or the 'Inherited methods' tab.");
		languageEnProp.setProperty("If_Txt_InfoHowToCallMethod_2", "When you click on the corresponding item representing the method, this method will, if it does not contain any parameters, be called immediately and the result will be written to the output editor.");
		languageEnProp.setProperty("If_Txt_CallMethodDialogForParameters", "If the method contains some parameters, a dialog will appear where you need to fill it out.");
		languageEnProp.setProperty("If_Txt_InfoOnlyFewSupportedDataTypesInMethodParameters", "Only some types of parameters, such as, for example, 'basic' data types of Java, arrays, map, and worksheets, are supported in the Parameter Call dialog.");
		languageEnProp.setProperty("If_Txt_InfoAboutLoadingVarsFromClasses", "If the method parameter contains a parameter type that is not supported by the input application, it will only be possible to fill it if a class or class instance contains a filled-in variable of the appropriate data type, otherwise it will not be possible to call the");
		languageEnProp.setProperty("If_Txt_CommandEditorText", "Command dditor");
		languageEnProp.setProperty("If_Txt_InfoAboutCommandEditor", "In this editor, you can enter some commands for working with instances (including creation).");
		languageEnProp.setProperty("If_Txt_InfoHowToPrintAllCommands", "Any commands that can be entered in this editor can be listed with the command 'printCommands ();'.");
		languageEnProp.setProperty("If_Txt_InfoAboutListOfCommands", "I.e. commands to create an instance, filling variables, call methods, increment or decrement etc. (see after entering the above command)");
		languageEnProp.setProperty("If_Txt_SourceCodeEditorText", "Source code editor");
		languageEnProp.setProperty("If_Txt_InfoAboutSourceCodeEditor", "In this editor you can edit the source code of the class (or other files that can be opened in this editor).");
		languageEnProp.setProperty("If_Txt_InfoAboutFunctionsOfEditor", "It is also possible to compile the classes, print the source code, generate the sample method etc.");
		languageEnProp.setProperty("If_Txt_InfoAboutGenerateConstructor", "There is added possibility to generate the constructor with variables in the class.");
		languageEnProp.setProperty("If_Txt_InfoAboutGenerateAccessMethods", "There is added possibility to generate the access methods for the variables in the class.");
		languageEnProp.setProperty("If_Txt_InfoAboutContentOfCodeEditor", "The editor contains a text box at the bottom of which compilation messages are printed.");
		languageEnProp.setProperty("If_Txt_InfoHowToOpenCodeEditor", "Editor can be opened via the context menu of the class in the class diagram (using the 'Open' -> 'In the code editor').");
		languageEnProp.setProperty("If_Txt_OpenDeferentFilesInProjectExplorerDialog", "Opening different files can be done by clicking on the menu item Tools -> Open File -> In the Code Editor (Optional in the Project Explorer).");
		languageEnProp.setProperty("If_Txt_ProjectExplorerText", "Project explorer");
		languageEnProp.setProperty("If_Txt_InfoHowToOpenProjectExplorer", "This dialogue can be opened using either one of the items in the menu 'Project explorer' in the 'Tools' menu, or using the context menu of the class in a class diagram 'Open' -> 'Project explorer'.");
		languageEnProp.setProperty("If_Txt_InfoHowToOpenCodeEditor_2", "Or by double clicking on the class in the class diagram or by marking it and pressing the 'Enter' key.");
		languageEnProp.setProperty("If_Txt_InfoAboutOpennedWorkspaceDir", "In the tree structure of this dialog, either the directory selected as a workspace or the open project directory (optional) will be loaded as the root directory.");
		languageEnProp.setProperty("If_Txt_InfoAboutAccessedmenuField", "By this option, you can either access or disable some items.");
		languageEnProp.setProperty("If_Txt_InfoAboutOpenMoreKindOfFiles", "In this dialog, you can open multiple files and different types, such as Pdf, Excel, Word, Txt, properties, html and more.");
		languageEnProp.setProperty("If_Txt_InfoAboutWritingSourceCode", "It contains the same possibility of manipulation of the source code, such as the aforementioned source code editor, including compilations, generating constructors and access methods etc.");
		languageEnProp.setProperty("If_Txt_InfoHowToOpenFile", "Open the file by double-clicking the file in the tree (left) or by right-clicking the file and choosing 'Open'.");
		languageEnProp.setProperty("If_Txt_HowToOpenOtherFile", "Alternatively, you can choose a custom file that is not in the required directory by using 'Open File' in the 'File' menu.");
		
		
		
		
		
		
		
		// Následují texty do dialogu FontSizeForm - dialog pro "rychlé" nastavení velkosti psíma za účelem výuky, třeba při promítání na plátně apod.
		ConfigurationFiles.commentsMap.put("FSF_DialogTitle", "Text to the dialog title.");
		languageEnProp.setProperty("FSF_DialogTitle", "Font size");
		
		// Texty pro menu:
		ConfigurationFiles.commentsMap.put("FSF_Menu_MenuDialog", "Texts in the menu dialog box for a 'quick' way to set the font size for objects in the class diagram and in the instance diagram.");
		languageEnProp.setProperty("FSF_Menu_MenuDialog", "Dialog");
		languageEnProp.setProperty("FSF_Menu_KindOfForm", "Setting options");
		languageEnProp.setProperty("FSF_Menu_Rb_KindItemOneSameSizeText", "One Size");
		languageEnProp.setProperty("FSF_Menu_Rb_KindItemOneSizeForChoosedObjectsText", "Individually by choice");
		languageEnProp.setProperty("FSF_Menu_Rb_KindItemOneSizeForEachObjectText", "Individually");
		languageEnProp.setProperty("FSF_Menu_Rb_KindItemOneSameSize_TT", "Set the same font size for all objects in the class diagram and in the instance diagram.");
		languageEnProp.setProperty("FSF_Menu_Rb_KindItemOneSizeForChoosedObjects_TT", "Set the same font size for the selected objects in the class diagram and in the instance diagram.");
		languageEnProp.setProperty("FSF_Menu_Rb_KindItemOneSizeForEachObject_TT", "Set the font size of each object in the class diagram and in the instance diagram individually.");
		languageEnProp.setProperty("FSF_Menu_Rb_SetFontsWhileMovingText", "Update size");
		languageEnProp.setProperty("FSF_Menu_Rb_SetFontsWhileMoving_TT", "The font size is set each time the slider is moved (marked) or after the slider is set (unmarked).");
		languageEnProp.setProperty("FSF_Menu_ItemRestoreDefault_Text", "Restore default");
		languageEnProp.setProperty("FSF_Menu_ItemRestoreDefault_TT", "Reset the default font sizes of all objects in the class diagram and in the instance diagram.");
		languageEnProp.setProperty("FSF_Menu_ItemClose_Text", "Close");
		languageEnProp.setProperty("FSF_Menu_ItemClose_TT", "Close this dialogue.");
		
		
		// Texty do panelu, který obsahuje komponenty pro nastavení velikosti každého objektu zvlášť:
		ConfigurationFiles.commentsMap.put("FSF_OSFEOP_LblInfo", "Text in a panel that contains the components for setting each object in the class diagram and in the instance diagram so that each object can be set individually in the Font Size dialog box in a 'fast' way.");
		languageEnProp.setProperty("FSF_OSFEOP_LblInfo", "Set the font size for each object in the class diagram and in the instance diagram individually.");
		languageEnProp.setProperty("FSF_OSFEOP_LblClassDiagram", "Class diagram");
		languageEnProp.setProperty("FSF_OSFEOP_LblInstanceDiagram", "Instance diagram");
		languageEnProp.setProperty("FSF_OSFEOP_TxtSize", "Size");		
		languageEnProp.setProperty("FSF_OSFEOP_LblClass", "Class");
		languageEnProp.setProperty("FSF_OSFEOP_LblComment", "Comment");
		languageEnProp.setProperty("FSF_OSFEOP_LblCommentEdge", "Comment edge");
		languageEnProp.setProperty("FSF_OSFEOP_LblAssociation", "Edge of a symmetrical association");
		languageEnProp.setProperty("FSF_OSFEOP_LblAggregation_1_1", "Edge of the asymmetric association");
		languageEnProp.setProperty("FSF_OSFEOP_LblExtends", "Edge of Heredity");
		languageEnProp.setProperty("FSF_OSFEOP_LblImplements", "Interface implementation edge");
		languageEnProp.setProperty("FSF_OSFEOP_LblAggregation_1_N", "Aggregation edge 1: N");
		languageEnProp.setProperty("FSF_OSFEOP_LblInstance", "Instance");
		languageEnProp.setProperty("FSF_OSFEOP_LblInstanceAttributes", "Attributes");
		languageEnProp.setProperty("FSF_OSFEOP_LblInstanceMethods", "Methods");
		languageEnProp.setProperty("FSF_OSFEOP_LblInstanceAssociation", "Association edge");
		languageEnProp.setProperty("FSF_OSFEOP_LblInstanceAggregation", "Aggregate edge");
		
		
		// Texty do panelu, který obsahuje komponety pro nastavení jedné stejné velikosti pro všechny objekty v diagramu tříd a v diagramu instancí:
		ConfigurationFiles.commentsMap.put("FSF_SSP_LblInfo", "Text in a panel that contains only one slider that can be set to the same size for all objects in both the class diagram and the instance diagram. The panel is in the 'fast' font size dialog.");
		languageEnProp.setProperty("FSF_SSP_LblInfo", "Set one font size for all objects in the class diagram and in the instance diagram.");
		languageEnProp.setProperty("FSF_SSP_TxtSize", "Size");
		languageEnProp.setProperty("FSF_SSP_FontSize", "Font size");
		
		
		// Texty do panelu, který obsahuje komponenty pro nastavení velikosti písma tak, že si uživatel pouze označí jakým objektům se má nastavit nějaká velikost písma:
		ConfigurationFiles.commentsMap.put("FSF_SSSOP_TxtSize", "Text in a panel that contains the components for setting the size of the objects in the class diagram and in the instance diagram, but that the user only marks those objects he wants to set the size and then sets it with the appropriate slider.The panel is located in the 'fast' font size setting.");
		languageEnProp.setProperty("FSF_SSSOP_TxtSize", "Size");
		languageEnProp.setProperty("FSF_SSSOP_LblInfo", "Set one font size for labeled objects in the class diagram and in the instance diagram.");
		languageEnProp.setProperty("FSF_SSSOP_SliderFontSize", "Font size");
		
		
		// Texty do panelu, který obsahuje komponenty pro označení objektů v diagramu tříd, kterým se má nastavit velikost písma:
		ConfigurationFiles.commentsMap.put("FSF_SSOCP_BorderTitle", "Panel text that contains the components for setting the object's font size when moving the slider.This panel is located in the font size control panel according to the highlighted objects in the 'fast' font size dialog.");
		languageEnProp.setProperty("FSF_SSOCP_BorderTitle", "Class diagram");
		languageEnProp.setProperty("FSF_SSOCP_Chcb_ClassText", "Class");
		languageEnProp.setProperty("FSF_SSOCP_Chcb_CommentText", "Comment");
		languageEnProp.setProperty("FSF_SSOCP_Chcb_CommentEdgeText", "Edge comment");
		languageEnProp.setProperty("FSF_SSOCP_Chcb_ExtendsText", "Edge heredity");
		languageEnProp.setProperty("FSF_SSOCP_Chcb_ImplementsText", "Interface implementation edge");
		languageEnProp.setProperty("FSF_SSOCP_Chcb_AssociationText", "Edge of a symmetrical association");
		languageEnProp.setProperty("FSF_SSOCP_Chcb_Aggregation_1_1_Text", "Edge asymmetric association");
		languageEnProp.setProperty("FSF_SSOCP_Chcb_Aggregation_1_N_Text", "Aggregation edge 1: N");
		languageEnProp.setProperty("FSF_SSOCP_Chcb_Class_TT", "Set the font size for a cell representing the class in the class diagram.");
		languageEnProp.setProperty("FSF_SSOCP_Chcb_Comment_TT", "Set the font size for the cell representing the comment in the class diagram.");
		languageEnProp.setProperty("FSF_SSOCP_Chcb_CommentEdge_TT", "Set font size for an edge representing a comment link with a class in the class diagram.");
		languageEnProp.setProperty("FSF_SSOCP_Chcb_Extends_TT", "Set font size for an edge representing the inheritance relationship in the class diagram.");
		languageEnProp.setProperty("FSF_SSOCP_Chcb_Implements_TT", "Set the font size for the edge representing the interface implementation type in the class diagram.");
		languageEnProp.setProperty("FSF_SSOCP_Chcb_Association_TT", "Set the font size for the edge representing the symmetric association type relationship in the class diagram.");
		languageEnProp.setProperty("FSF_SSOCP_Chcb_Aggregation_1_1_TT", "Set font size for an edge representing an asymmetric association in the class diagram.");
		languageEnProp.setProperty("FSF_SSOCP_Chcb_Aggregation_1_N_TT", "To set the font size for the edge representing the aggregation type 1: N in the class diagram.");
		languageEnProp.setProperty("FSF_SSOCP_BtnSelectAll", "Select all");
		languageEnProp.setProperty("FSF_SSOCP_BtnDeselectAll", "Deselect all");
		
		
		// Texty do panelu, který obsahuje komponenty pro označení objektů v diagramu instancí, kterým se má nastavit velikost písma:
		ConfigurationFiles.commentsMap.put("FSF_SSOIP_BorderTitle", "Texts in a panel that contains the components for naming objects in the instances diagram to set the font size This panel is in the font size setting panel by the user marking the objects they want to size and then using the slider to set them These panels are in the 'quick' font size setting dialog box for objects in the class diagram and in the instance diagram. ");
		languageEnProp.setProperty("FSF_SSOIP_BorderTitle", "Instance diagram");
		languageEnProp.setProperty("FSF_SSOIP_Chcb_Instance_Text", "Instance");
		languageEnProp.setProperty("FSF_SSOIP_Chcb_InstanceAttributes_Text", "Attributes");
		languageEnProp.setProperty("FSF_SSOIP_Chcb_InstanceMethods_Text", "Methods");
		languageEnProp.setProperty("FSF_SSOIP_Chcb_Association_Text", "Association");
		languageEnProp.setProperty("FSF_SSOIP_Chcb_Aggregation_Text", "Aggregation");
		languageEnProp.setProperty("FSF_SSOIP_Chcb_Instance_TT", "Set font size for a cell representing an instance in an instance diagram.");
		languageEnProp.setProperty("FSF_SSOIP_Chcb_InstanceAttributes_TT", "Set font size for attributes in a cell representing an instance in an instance diagram.");
		languageEnProp.setProperty("FSF_SSOIP_Chcb_InstanceMethods_TT", "Setting font size for methods in a cell representing an instance in an instance diagram.");
		languageEnProp.setProperty("FSF_SSOIP_Chcb_Association_TT", "Set the font size for the edge representing the association type in the instance diagram.");
		languageEnProp.setProperty("FSF_SSOIP_Chcb_Aggregation_TT", "To set the font size for an edge representing an aggregation type relationship in an instance diagram.");
		languageEnProp.setProperty("FSF_SSOIP_BtnSelectAll", "Select all");
		languageEnProp.setProperty("FSF_SSOIP_DeselectAll", "Deselect all");
		
		
		
		ConfigurationFiles.commentsMap.put("FSF_CDID_TXT_JopSameEdgesErrorText", "Texts in error messages that can occur when duplicate values ​​are searched after setting a font / font size, such as duplicate properties for cells representing class and comment, or any edges, etc.");
		languageEnProp.setProperty("FSF_CDID_TXT_JopSameEdgesErrorText", "The following edges have the same settings");
		languageEnProp.setProperty("FSF_CDID_TXT_JopSameEdgesErrorTitle", "Duplicate found");
		languageEnProp.setProperty("FSF_CDID_TXT_Aggregation_1_1", "Asymmetric association");
		languageEnProp.setProperty("FSF_CDID_TXT_Aggregation_1_N", "Aggregation 1: N");
		languageEnProp.setProperty("FSF_CDID_TXT_Association", "Symmetric association");
		languageEnProp.setProperty("FSF_CDID_TXT_CommentEdge", "Comment edge");
		languageEnProp.setProperty("FSF_CDID_TXT_Inheritance", "Heredity");
		languageEnProp.setProperty("FSF_CDID_TXT_Unknown", "Unknown");
		
		
		
		ConfigurationFiles.commentsMap.put("FSF_FSPA_Txt_Jop_SameCellsText", "Texts for error messages that can occur if the user sets the font size in some / some objects in the class diagram or in the instance diagram and the corresponding objects will have the same properties For example, if the cell representing the comment is identical to the cell representing the class in the class diagram except for the font size, so if the user sets the font size to the same value, then there would be duplicates in the diagram and no single objects would have to be distinguished, so users should be warned and set default values.");
		// 	// Duplicita bunky a komentare v diagramu tříd:
		languageEnProp.setProperty("FSF_FSPA_Txt_Jop_SameCellsText", "The cell representing the class and the cell representing the comment in the class diagram had the same properties, they were set to their default font sizes.");
		languageEnProp.setProperty("FSF_FSPA_Txt_Jop_SameCellsTitle", "Duplicate values");	
		
		// Texty pro duplicitní hrany v diagramu tříd:
		languageEnProp.setProperty("FSF_FSPA_Txt_Jop_SameEdgesText_1", "Duplicate properties for at least two edges have been found, the following edges have changed font sizes");
		languageEnProp.setProperty("FSF_FSPA_Txt_Jop_SameEdgesText_2", "The symmetrical association");		
		languageEnProp.setProperty("FSF_FSPA_Txt_Jop_SameEdgesText_3", "Edge asymmetric association");
		languageEnProp.setProperty("FSF_FSPA_Txt_Jop_SameEdgesText_4", "Edge heredity");		
		languageEnProp.setProperty("FSF_FSPA_Txt_Jop_SameEdgesText_5", "Aggregation edge 1: N");
		languageEnProp.setProperty("FSF_FSPA_Txt_Jop_SameEdgesText_6", "Comment edge");		
		languageEnProp.setProperty("FSF_FSPA_Txt_Jop_SameEdgesTitle", "Duplicate values");
		
		// Texty pro duplicitní hrany v diagramu instancí:
		languageEnProp.setProperty("FSF_FSPA_Txt_Jop_SameEdgesInIdText_1", "Edges representing association and aggregation relationships in the instance diagram have the same properties, and font sizes have been changed.");
		languageEnProp.setProperty("FSF_FSPA_Txt_Jop_SameEdgesInIdText_2", "Association");
		languageEnProp.setProperty("FSF_FSPA_Txt_Jop_SameEdgesInIdText_3", "Aggregation");
		languageEnProp.setProperty("FSF_FSPA_Txt_Jop_SameEdgesInIdTitle", "Duplicate values");
		
		
		
		
		
		
		
		
		
		
		ConfigurationFiles.commentsMap.put("SIFCR_DialogTitle","Text in a dialog that is used to indicate the instance to which it wants to create a reference.This dialog can be viewed by clicking on the 'Create Reference' button in the instance view dialog when it has a field or sheet type label that contains at least two instances of classes from a class diagram that are not represented in the instance diagram.");
		languageEnProp.setProperty("SIFCR_DialogTitle", "Instances markup");
		languageEnProp.setProperty("SIFCR_Lbl_Info", "Mark the instance (s) you want to create the reference for.");
		languageEnProp.setProperty("SIFCR_Btn_OK", "OK");
		languageEnProp.setProperty("SIFCR_Btn_Cancel", "Cancel");
		languageEnProp.setProperty("SIFCR_Btn_SelectAll", "Select all");
		languageEnProp.setProperty("SIFCR_Btn_DeselectAll", "Deselect all");
		
		
		
		
		
		
		
		
		// Texty do třídy InstanceCell v balíčku instanceDiagram. Jedná se pouze
		// otexty potřebné pro vypsání informací o refernční proměnné, nebo o tom, žese
		// v příslušné instanci nenachází atributy nebo metody.
		ConfigurationFiles.commentsMap.put("MIS_Txt_ReferenceVar", "Texts into a cell that represent a instance of a class in an instance diagram. These texts are only needed if variables and / or methods are to be displayed in the appropriate cell / instance. These are texts that inform the user if there is no variable or method in the instance (class), or information about the reference variable (instance reference in the instance diagram - in the filled variable).");
		languageEnProp.setProperty("MIS_Txt_ReferenceVar", "reference variable");
		languageEnProp.setProperty("MIS_Txt_NoVariables", "No Variables");
		languageEnProp.setProperty("MIS_Txt_NoMethods", "No Methods");
		
		
		
		
		
		
		
		
		
		
		
		
		// Texty do třídy PopupMenu v balíčku clearEditor, jedná se pouze o texty do té
		// jediné položky pro vymazání textu z editoru.
		ConfigurationFiles.commentsMap.put("CE_PM_ItemClearText", "Text for the 'Clear' item in the context menu of the clearEditor package, which is used to delete all the contents / text in the editor.");
		languageEnProp.setProperty("CE_PM_ItemClearText", "Delete");
		languageEnProp.setProperty("CE_PM_ItemClearTT", "Deletes all text from the editor.");
		
		
		
		
		
		
		
		
		ConfigurationFiles.commentsMap.put("EI_Txt_AllowedValues", "Texts for error messages that can occur if user inputs intentionally wrong values ​​into a configuration file can be typed into the appropriate dialog that states that the user has entered incorrect values, classes, etc.");
		languageEnProp.setProperty("EI_Txt_AllowedValues", "Allowed values");
		languageEnProp.setProperty("EI_Txt_MissingKey", "Missing key");
		languageEnProp.setProperty("EI_Txt_CellsDuplicateError", "Duplicate settings found for cells that represent classes and comments in the class diagram.");
		languageEnProp.setProperty("EI_Txt_CdEdgesError", "Duplicate settings have been found for some edges that represent some relationships between classes or a class comment with a comment in the class diagram.");
		languageEnProp.setProperty("EI_Txt_IdEdgesError", "Duplicate settings found for edges that represent aggregate and association relationships in the instance diagram.");
		languageEnProp.setProperty("EI_Txt_InvalidValues", "Invalid value");
		
		
		
		
		
		
		
		
		// Texty do třídy / dialogu ErrorInConfigFilesForm, jedná se o dialog, který
		// slouží pro zobrazení chyb po uložení změn v nějakém konfiguračním souboru,
		// který bude obsahovat chybné hodnoty nebo duplicity.
		ConfigurationFiles.commentsMap.put("EICFF_DialogTitle", "The texts in the error report dialog that appears when the application runs and the user makes some changes to the configuration file, and those changes will not be valid, then a dialog will appear that will contain an overview of the errors and information that the file has been written to the default settings.");
		languageEnProp.setProperty("EICFF_DialogTitle", "Error Overview");
		languageEnProp.setProperty("EICFF_TxtFile", "File");
		languageEnProp.setProperty("EICFF_TxtInfoRest", "contained the errors listed below: All values ​​in the file were overwritten to the default settings, please restart the application to make changes.");
		languageEnProp.setProperty("EICFF_JspErrorListBorderTitle", "Found errors");
		languageEnProp.setProperty("EICFF_JspDuplicatesListBorderTitle", "Duplicates found");
		languageEnProp.setProperty("EICFF_ChcbWrapText", "Wrap the lyrics.");
		languageEnProp.setProperty("EICFF_BtnOk", "OK");








        // Texty do třídy: cz.uhk.fim.fimj.app.RestartApi
        // Jedná se o texty do dialogů, které informaují uživatele, že z nějakého důvodu nelze restartovat aplikaci apod. (pouze pro linux)
        ConfigurationFiles.commentsMap.put("RA_DirsWereNotFoundTitle", "Text to class for restarting the application. These are texts in the dialog box - error messages that indicate that there was an error and therefore can not restart the application. Texts, resp. those dialogs in the application can only occur if the attempt is to restart the application on Linux.");
        languageEnProp.setProperty("RA_DirsWereNotFoundTitle", "Close the application");
        languageEnProp.setProperty("RA_DirsWereNotFoundTextPartOne", "None of the configuration directories in the workspace, workspace, or home user directory were found to save the boot script.");
        languageEnProp.setProperty("RA_DirsWereNotFoundTextPartTwo", "The configuration directory in the workspace, workspace, or user home directory was not found to save the boot script.");

        languageEnProp.setProperty("RA_RestartIsNotSupportedTitle", "Restart is not supported");
        languageEnProp.setProperty("RA_RestartIsNotSupportedText", "The application does not support restarting on the operating system you are using. Do you want to close the app?");

        languageEnProp.setProperty("RA_CannotCreateLaunchScriptTitle", "Close the application");
        languageEnProp.setProperty("RA_CannotCreateLaunchScriptText", "An attempt to create a startup script failed. Do you want to close the app?");
        languageEnProp.setProperty("RA_Script", "Script");






        // Texty pro chybovou hlášku o tom, že se nepodařilo smazat spouštěcí skript Viz třída: cz.uhk.fim.fimj.app.DeleteLaunchScriptThread
        ConfigurationFiles.commentsMap.put("DLST_DeleteScriptFailedText", "Texts for an error message that failed to delete the boot script that was generated but not deleted when the application was restarted (on Linux OS). The message can occur if an existing startup script that was not deleted when the app is restarted can not be deleted after the application starts.");
        languageEnProp.setProperty("DLST_DeleteScriptFailedText", "The created startup script for the application could not be deleted. Please do so.");
        languageEnProp.setProperty("DLST_DeleteScriptFailedTitle", "Failed to delete boot script");
        languageEnProp.setProperty("DLST_Script", "Script");






        // Texty do třídy cz.uhk.fim.fimj.file.DesktopSupport:
        ConfigurationFiles.commentsMap.put("DS_Txt_ErrorInfoText", "Text for error messages in class: cz.uhk.fim.fimj.file.DesktopSupport. These are, for example, error messages in the sense that a file has not been found that an action is not supported on the platform used to open a browser or file explorer, etc.");
        // Texty ohledně podpory třídy Desktop:
        languageEnProp.setProperty("DS_Txt_ErrorInfoText", "The 'Desktop' class is not supported on the platform you are using.");
        languageEnProp.setProperty("DS_Txt_ErrorInfoTitle", "Desktop is not supported");
        languageEnProp.setProperty("DS_Txt_OpenActionIsNotSupportedTitle", "Action not supported");
        /*
         * Texty chybové hlášky při pokusu oo otevření souboru nebo adresáře v průzkumníku souborů v OS nebo ve
         * výchozí aplikaci pro otevření příslušného typu souboru:
         */
        languageEnProp.setProperty("DS_Txt_FileDoNotExist_Text", "The file or directory does not exist.");
        languageEnProp.setProperty("DS_Txt_FileDoNotExist_Title", "Can not be opened");
        languageEnProp.setProperty("DS_Txt_Path", "Path");
        languageEnProp.setProperty("DS_Txt_ParentDirWasNotFound_Text", "Unable to find the directory of the selected file.");
        languageEnProp.setProperty("DS_Txt_ParentDirWasNotFound_Title", "Directory not found");
        /*
         * Texty pro chybové hlášky, že akce pro otevření souboru nebo adresáře není na používané platformě
         * podporováno za účelem otevření souboru nebo adresáře v průzkumníku projektů nebo ve výchozí aplikaci pro
         * otevření příslušného typu souboru:
         */
        languageEnProp.setProperty("DS_Txt_OpenFileActionIsNotSupported_Text", "The action to open a file is not supported.");
        languageEnProp.setProperty("DS_Txt_OpenFileActionIsNotSupported_Text2", "The file can not be opened.");
        languageEnProp.setProperty("DS_Txt_OpenDirActionIsNotSupported_Text", "The directory opening action is not supported.");
        languageEnProp.setProperty("DS_Txt_OpenDirActionIsNotSupported_Text2", "The directory can not be opened.");
        /*
         * Texty pro chybové hlášky, že třída Desktop není na používané platformě podporována za účelem otevření
         * souboru nebo adresáře v průzkumníku projektů nebo ve výchozí aplikaci pro otevření příslušného typu souboru.
         */
        languageEnProp.setProperty("DS_Txt_FileCannotBeOpened", "The file can not be opened.");
        languageEnProp.setProperty("DS_Txt_DirCannotBeOpened", "The directory can not be opened.");
        /*
         * Text pro chybovou hlášku o tom, že třída Desktop není podporována na používané platformě za účelem
         * odeslání emailu, resp. otevření emailového klienta:
         */
        languageEnProp.setProperty("DS_Txt_EmailCannotBeSent", "Email can not be sent.");
        /*
         * Text pro chybovou hlášku o tom, že akce pro otevření emailového klienta (/ zaslání emailu) není na
         * používané platformě podporována.
         */
        languageEnProp.setProperty("DS_Txt_EmailActionIsNotSupported", "Email sending action is not supported. Email can not be sent.");
        /*
         * Text pro chybovou hlášku o tom, že třída Desktop není na používané platformě podporována za účelem
         * otevření výchozího prohlížeče a v něm příslušnou webovou stránku.
         */
        languageEnProp.setProperty("DS_Txt_BrowserCannotBeOpened", "The browser can not be opened.");
        /*
         * Texty pro chybovou hlášku o tom, že akce pro otevření výchozího prohlížeče a v něm příslušnou webovou
         * stránku není na používané platformě podporována.
         */
        languageEnProp.setProperty("DS_Txt_BrowseActionIsNotSupported", "The action to open the default browser is not supported. Can not display page.");



		
		
		
		
		
		
		return languageEnProp;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která projde pole array a vytvoří jednu proměnnou typu String, která
	 * bude obsahovat veškeré položky v poli oddělené symbolem " |&| ". Tento symbol
	 * jsem zvolil, abych při načtení tohoto textu ze souboru a vložení položek zpět
	 * do polí rozeznal jednotlivé položky a jelikož se dle uvedeného symbolu text
	 * opět "rozděluje", tak jsem potřebval zvolit symbol, který by se v tom textu
	 * neměl nikde nacházet, pouze jako oddělovač položek v poli. Jinak by se
	 * rozdělil i ten text.
	 * 
	 * @param array
	 *            - pole, jehož položky se mají převést na jeduu proměnnou typu
	 *            String.
	 * 
	 * @return proměnnou typu String, která obsahuje veškeré položky v poli array
	 *         oddělené výše uvedeným symbolem.
	 */
	private static String createTextFromArray(final String[] array) {
		/*
		 * Proměnná, do které vložím veěkeré položky v poli array oddělené následujícím
		 * symbolem.
		 */
		String text = "";

		for (int i = 0; i < array.length; i++) {
			text += array[i];

			/*
			 * Oddělovač hodnot v poli přidám pouze v případě, že se nejedná o poslední
			 * prvek v poli:
			 */
			if (i < array.length - 1)
				text += " |&| ";
		}

		return text;
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která rozdělí text v proměnné text dle symbolu |&| s tím, že se do
	 * toho mohou a nemusí počítat mezery kolem tohoto symbolu. Texty oddělené
	 * uvedeným symbolem budou vloženy do jednorozměrného pole, pak se z jenotlivých
	 * položek v tomto poli odeberou bílé znaky na začátku a na konci každého prvku
	 * v poli a toto pole se vrátí.
	 * 
	 * @param text
	 *            - text, který obsahuje prvky pole oddělené výše uvedeným symbolem.
	 * 
	 * @return Jednorozměrné pole, které obsahuje texty odělené dle výše uvedeného
	 *         symbolu.
	 */
	public static String[] createArrayFromText(final String text) {
		// Toto by nastat nemělo:
		if (text != null) {
			/*
			 * Do jednorozměrného pole si vložím texty z proměnné text, které oddělím výše
			 * uvedeným symbolem, akorát do něj nebudu počítat mezery, resp. mohou a nemusí
			 * tam být.
			 */
			final String[] partsArray = text.split("\\s*\\|&\\|\\s*");

			/*
			 * Toto není třeba, ale pro případ, že by v textu byly mezery, jakože budou,
			 * když je tam v metodě createTextFromArray vložím s daným symbolem pro
			 * oddělení, ale uživatel je v souboru může a nemusí odebrat.
			 * 
			 * Toto je pouze pro odebrání mezer na začátku a na konci jednotlivých textů.
			 */
			Arrays.asList(partsArray).forEach(String::trim);

			// Vrátím získané pole:
			return partsArray;
		}

		// Toto by nastat nemělo ale v "případě nouze" vrátím prázdné pole:
		return new String[] {};
	}
}
