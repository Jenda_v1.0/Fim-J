package cz.uhk.fim.fimj.language;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

/**
 * Tato třída slouží pro seřazení "hodnot" coby textů v souboru .properties.
 * <p>
 * Jedná se pouze o účel "zpčehlednění" klíčů v souboru .properties.
 * <p>
 * Jedná se o třídu, který "přepíše" některé metdy pro vkládání a "zapisování / získávání" hodnot pro soubor
 * .properties.
 * <p>
 * Konkrétně je třeba přepsat metody pro vložení klíče (put) a získávání klíčů z mapy (keys). Při vkládání klíče do
 * mapy, tj. v metodě put si "oindexuji" klíč tak, že jej přidám do "vlastní" mapy (ne v objektu properties), ale i to
 * té mé, ze které se pak budou brát hodnoty, aby se mohli seřadit dle potřebných indexů (v případě, že bude zvolené
 * řazení), jinak by to nebylo potřeba, jedná se řazení klíčů v souboru .properties dle možností -> dle abecedy, dle
 * indexu přidání v metodách writeLanguageXX nebo bez řazení, tj. tak jak se to při zápisu do souboru vezme z mapy v
 * objektu Properties, tak se ta data zapíčou, ale toto není moc přehledné.
 * <p>
 * Na následujícím zdroji jsem se dozvěděl o metodě keys v objektu Properties, ale uvedenou tuto metodu jsem již upravil
 * dle sebe, tj. dle možností pro řazení a práce s mapou kvůli indexům pro pořadí, ...
 * <p>
 * Note:
 * <p>
 * Místo mapy pro indexy pořadí přidání a klíče pro text v properties by bylo možné vytvořit novou třídu (/ objekt),
 * která by obsahovala uvedený index (pořadí přidání) a klíč hodnoty pro text v souboru properties, ale napsal jsem tu
 * mapu, protože se s ní pak lépe pracuje v algoritmech pro řazení, jinak bych je musel napsat sám.
 * <p>
 * Zdroj:
 * <p>
 * http://www.java2s.com/Tutorial/Java/0140__Collections/SortPropertieswhensaving.htm
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class SortedProperties extends Properties {

	private static final long serialVersionUID = 1L;

	
	
	/**
	 * Mapa typu <Long, String>, která jako Long obsahuje index vložení do mapy,
	 * resp. pořadí vložení dané hodnoty do mapy. a jako hodnotu String obsahuje
	 * klíč, dle kterého se "ukládají / indexují" textové hodnoty v souboru
	 * .properties, tj. klíč = nějaký text v souboru pro aplikaci.
	 */
	private final Map<Long, String> indexMap;
	
	
	/**
	 * Index, který se při každém vložení do mapy indexMap inkrementuje a slouží tak
	 * jako "informace" o pořadí vložení hodnoty do mapy indexMap.
	 */
	private long index;
	
	
	
	
	/**
	 * Proměnná výčtového typu, dle které se pozná, v jakém pořadí se mají vložit
	 * hodnoty pak do souboru.
	 * 
	 * Dle této hodnoty se seřadí hodnoty v mapě v objektu Properties na zadané
	 * pořadí, například dle abecedy, nebo dle pořadí přidání apod.
	 */
	private final KeysOrder keysOrder;
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy, který vytvoří instanci mapy indexMap a nastaví
	 * proměnnou index na hodnotu -1.
	 * 
	 * @param keysOrder
	 *            - Jedná se o hodnoty výčtového typu, dle které se pozná, jak se
	 *            mají seřadit položky pro zápis do souboru.
	 */
	public SortedProperties(final KeysOrder keysOrder) {
		super();
		
		this.keysOrder = keysOrder;
		
		index = -1L;
		indexMap = new HashMap<>();
	}
	
	
	
	
	
	
	
	
	@Override
	public void store(OutputStream out, String comments) throws IOException {
		/*
		 * Tuto metodu přepisuji pouze proto, že nechi zapisovat první dva řádky daného
		 * souboru .properties.
		 * 
		 * Kdyby se měli zapisovat ty první dva řádky, tj. volitelný komentář a ten
		 * datum vytvoření, pak stačí odkomentvat zakomentovanou metodu výše - původní,
		 * a zakomentovat tuto následující "upravenou" metodu.
		 * 
		 * Nemo stačí smazat Override této metody store, pak se budou zase zapisovat ty
		 * dva zmíněné řádky na začátek souboru.
		 */
		super.store(new StripFirstLineStream(out), null);
	}
	
	
	
	
	
	
	
	
	
	@Override
	public synchronized Object put(Object key, Object value) {		
		/*
		 * Vložím do mapy novou hodnotu key na inkrementovaném indexu (pořadí).
		 * 
		 * Dle této hodnoty index poznám, v jakém pořadí se vložila hodnota key, pak
		 * kdyby se měli hodnoty řadit dle pořadí vložení, tak stačí seřadit hodnoty v
		 * mapě indexMap dle indexů Long a mám hledané pořadí.
		 */
		indexMap.put(++index, (String) key);
		
		/*
		 * Toto je "kalsické" vložení do mapy v objektu Properties, to je třeba, při
		 * zápisu do soubor.
		 */
		return super.put(key, value);
	}
	
	
	
	
	
	
	
	
	@Override
	public synchronized Enumeration<Object> keys() {
		/*
		 * Tuto metodu přepíšu tak, že dle hodnoty ve výčtu KeysOrder seřadí nebo
		 * případně ponechá pořadí vložených klíčů.
		 * 
		 * Dle proměnné keysOrder se pozná, jak se mají seřadit hodnoty, pokud se němají
		 * řadit hodnoty, pak se vrátí výchozí pořadí, které seaktuálně nachází v mapě.
		 */
		
		if (keysOrder == KeysOrder.WITHOUT_SORTING)
			// Vrátím pořadí, jaké se nachází v mapě v objektu Properties, "nic dělat nebudu
			// (dle řazení)"
			return super.keys();

		
		
		else if (keysOrder == KeysOrder.ASCENDING_BY_ADDING)
			/*
			 * Seřadím hodnoty vzestupně dle indexů, které značí pořadí přidání položky do
			 * mapy a vrátím ty klíče pro properties:
			 */
			return getSortedValues(indexMap, true);

		
		
		else if (keysOrder == KeysOrder.DESCENDING_BY_ADDING)
			/*
			 * Seřadím hodnoty sestupně dle indexů, které značí pořadí přidání položky do
			 * mapy a vrátím ty klíče pro properties:
			 */
			return getSortedValues(indexMap, false);

		
		
		else if (keysOrder == KeysOrder.ASCENDING_BY_ALPHABET)
			/*
			 * Seřadím hodnoty z mapy dle abecedy vzestupně a vrátím je. místo hodnot z mapy
			 * indexMap je možné brát hodnoty z super.keys(), ale je třeba je vložit do
			 * objektu Vector.
			 */
			return getSortedValues(new ArrayList<>(indexMap.values()), false);

		
		
		else if (keysOrder == KeysOrder.DESCENDING_BY_ALPHABET)// Tato podmínka není potřeba, jedná se o poslední
																// možnost (pouze pro potenciální rozšíření)
			/*
			 * Seřadím hodnoty z mapy dle abecedy sestupně a vrátím je. místo hodnot z mapy
			 * indexMap je možné brát hodnoty z super.keys(), ale je třeba je vložit do
			 * objektu Vector.
			 */
			return getSortedValues(new ArrayList<>(indexMap.values()), true);

		
		
		// Toto by nastat nemělo pouze jako výchozí hodnota:
		return super.keys();
	}
	
	
	

	
	

	/**
	 * Metoda, která seřadí hodnoty v listu values sestupně nebo vzestupně dle
	 * hodnoty v proměnné ascending a převede je do tvaru Enumeration pomocí
	 * vectoru.
	 * 
	 * @param values
	 *            - List s hodnotami, které se mají seřadit dle abecedy.
	 * 
	 * @param descending
	 *            - logická proměnná, která definuje, zda se mají hodnoty seřadit
	 *            sestupně nebo sestupně. True - vzestupně, false - sestupně.
	 * 
	 * @return seřazené hodnoty z listu values akorát ve tvaru Enumeration.
	 */
	private static Enumeration<Object> getSortedValues(final List<String> values, final boolean descending) {
		/*
		 * Note:
		 * Polaždé seřadím položky nejprve vzestupně, a pouze v případě, že se mají
		 * seřadit položky sestupně, tak zavolám metodu, která položky seřadí sestupně,
		 * je to takto uděláné, protože, když jsem zavolal pouze metodu reverse, tak
		 * neseřadila položky sestupně. Takto to funguje.
		 */
		Collections.sort(values);

		if (descending)
			Collections.reverse(values);

		// Vrátím hodnoty v příslušném pořadí akorát ve tvaru Enumeration:
		return new Vector<Object>(values).elements();
	}
	
	

	
	
	
	
	
	
	/**
	 * Metoda, která si seřadá hodnoty z Properties a vrátí je tak, jak mají být
	 * seřazené, tj. dle indexů, které značí pořadí, v jakém byly přidány, akorát
	 * budou dle těchto indexů seřazeny hodnoty buď vzestupně nebo sestupně.
	 * 
	 * @param map
	 *            - mapa, která obsahuje hodnoty <Long, String> -> Long je index,
	 *            který značí pořadí přidání položky do této mepy a String je klíč
	 *            pro Properties, který obsahuje nějaký text pro aplikaci v
	 *            příslušném jazyce.
	 * 
	 * @param ascending
	 *            - logická hodnota, zda se mají hodnoty seřadit dle indexů sestupně
	 *            nebo vzestupně. True = vzestupně, false = sestupně.
	 * 
	 * @return seřazené hodnoty z indexMap buď vzestupně nebo sestupně dle hodnoty
	 *         ascending.
	 */
	private static Enumeration<Object> getSortedValues(final Map<Long, String> map, final boolean ascending) {
		/*
		 * Vytvořím si List, do kterého vložím veškeré klíče z "mé" mapy -> z mapy
		 * indexMap, kam si na pozici klíče vkládám index, který značí pořadí přidání
		 * položky do mapy.
		 */
		final List<Long> sortedKeys = new ArrayList<>(map.keySet());

		/*
		 * Zjistím si, zda se mají hodnoty seřadit vzestupně nebo sestupně a dle toho je
		 * seřadím:
		 */
		if (ascending)
			// Seřadím vzestupně list sortedKeys
			Collections.sort(sortedKeys);
		// Seřadím vzestupně list sortedKeys
		else
			Collections.reverse(sortedKeys);
		
		
		
		/*
		 * Vytvořím si Vector keys, do kterého postupně vložím hodnoty z mapy indexMap,
		 * na příslušné pozici, teď již seřazených položek v listu, ten obsahuje
		 * seřazené indexy, tak je stačí projít a postupně brát hodnoty z mapy indexMap
		 * na příslušných pozicích.
		 */
		final Vector<Object> keys = new Vector<>();

		/*
		 * Zde projdu výše popsaný list sortedKeys a vložím hodnoty z mapy na každé z
		 * pozic v listu sortedKeys do vecottoru keys, který vrátím v podobě Enumeration
		 */
		sortedKeys.forEach(v -> keys.add(map.get(v)));
		
		// Vrátím seřazené hodnoty dle pořadí, v jakém byly přidány:
		return keys.elements();
	}
}
