package cz.uhk.fim.fimj.language;

import cz.uhk.fim.fimj.file.Constants;

/**
 * Tento výčet obsahuje hodnoty, dle kterých se pozná, jak se mají seřadit
 * hodnoty pro zápis do souboru .properties.
 * 
 * Klíče v souboru .properties lze řadit následovně: - bez řazení -> aplikuje se
 * výchozí pořadí z mapy
 * 
 * - Vzestupně dle přidání -> dle indexu přidání se seřadí hodnoty vzestupně
 * - Sestupně dle přidání -> dle indexu přidání se seřadí hodnoty sestupně
 * - Vzestupně dle abecedy -> Klíče se seřadí dle abecedy vzestupně
 * - Sestupně dle abecedy -> Klíče se seřadí dle abecedy sestupně
 * 
 * 
 * Každý výčtový typ obsahuje nějakou hodnoty typu String, která slouží pro
 * uložení dané hodnoty do souboru Default.properties. Dle této Stringové
 * hodnoty se pak pozná, jaká hodnota je nastavena, aby se dle toho mohlo do
 * aplikace načíst řazení.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public enum KeysOrder {

	/**
	 * Bez řazení -> aplikuje se pořadí, v jakém se bude nacházet v mapě v objektu
	 * Properties, nebudou se přidávat komentáře, protože by se přidaly pouze k
	 * jednomu klíči (slovu v Properties).
	 * 
	 * V tomto případě se nebudou vkládat komentáře, protože nelze dopředu určit, v
	 * jakém pořadí, nebo spíše na jaké umístění v souboru .properteis bude komentář
	 * přidán, tzn. že se nějaký komentář, který patří k jednomu klíči může přidat
	 * úplmě k jinaému klíče apod.
	 */
	WITHOUT_SORTING("Without_Sorting"),
	
	/**
	 * Vzestupně dle přidání, tj. využijí se indexy inkrementované při přidání klíče
	 * do mapy.
	 * 
	 * Nejprve se seřadí hodnoty v mapě dle indexů a pak se v daném (seřazenémú
	 * pořadí zapíšou do souboru .properties.
	 * 
	 * Je možné aplikovat komentáře - vždy k prvnímu klíči pro příslušný "klíč" k
	 * daným slovům.
	 */
	ASCENDING_BY_ADDING("Ascending_By_Adding"), 

	/**
	 * (to samé jako předchozí hodnota - ASCENDING_BY_ADDING, pouze se jedná o
	 * sestupé pořadí).
	 * 
	 * Seřazení hodnot dle indexů - pořadí přidání klíčů do mapy v sestupném pořadí
	 * a následné zapsání do souboru.
	 * 
	 * I v této možností jsem povolil zapsání komentářů, akorát bude komentář zadán
	 * jakoby pod příslušným odstavcem, nad klíšem, ke kterému bude komentář vložen.
	 * (Odstavec který tvoří k sobě patřící slovíčka, například texty do menu
	 * aplikace apod.)
	 * 
	 * Takže je třeba číst pak sobor od konce.
	 * 
	 * (Tato možnost je trochu "nemístná", a nejspíše se nikdy nevyužije, ale pořád
	 * je to možnost.)
	 */
	DESCENDING_BY_ADDING("Descending_By_Adding"),

	/**
	 * Klíče v mapě v objektu Properties se seřadí vzestupně dle abecedy, a v tomto
	 * pořadí se zapíšou hodnoty do souboru .properties.
	 * 
	 * V tomto případě jsem zavrhl komentáře, protože by se přidaly vždy k jednomu
	 * klíči, ale zbytek klíčů, které tvoří texty do nějakého dialogu (například)
	 * může být na druhém konci souboru .properties.
	 * 
	 * Takžeby byl komentář jen u toho jednoho klíše, ale další klíč může být k jiné
	 * části aplikace, proto mi nepříšlo moc vhodné přidávat tam komentáře.
	 */
	ASCENDING_BY_ALPHABET("Ascending_By_Alphabet"),
	
	/**
	 * (to samé jako u předchozí hodnoty - ASCENDING_BY_ALPHABET, pouze se jedná o
	 * sestupně pořadí)
	 * 
	 * V tomto případě se seřadí přidáné klíče do mapy sestupně dle abecedy a v
	 * takovém pořadí se zapíšou do souboru .properties.
	 * 
	 * Opět nelze vkládat komentáře, protože by se příslušný komentář přidal jen k
	 * jednomu klíče, ale zbytek klíčů by mohl být rozházený po celém souboru.
	 */
	DESCENDING_BY_ALPHABET("Descending_By_Alphabet");
	
	
	
	
	
	
	
	
	
	/**
	 * Proměnná, která slouží pro uchování hodnoty, aby bylo možné "uložit tento
	 * výčet do souboru". Dle této hodnoty se při načtení zesouboru poznaá, jaká
	 * výčtová hodnota se má zvolit, jestli řazení dle abecedy, indexů apod.
	 */
	private final String valueForFile;
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param valueForFile
	 *            - hodnota, dle která slouží pro uložení do souboru a následné
	 *            načtení z něj. Dle této hodnoty se pozná, jaký výčtový typ (viz
	 *            výše) se má zvolit.
	 */
	KeysOrder(final String valueForFile) {
		this.valueForFile = valueForFile;
	}
	
	
	/**
	 * Getr na proměnnou valueForFile, která obsahuje text, dle kterého se pozná,
	 * jaký výčtový typ se má zvolit.
	 * 
	 * @return výše popsaná referencena proměnnou valueForFile.
	 */
	public String getValueForFile() {
		return valueForFile;
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která "převede" text v proměnné fileValue na výčtový typ tak, že dle
	 * hodnoty v fileValue se testuje, zda se daná hodnota shoduje s jednou z hodnot
	 * u výčtových typů, pokud ano, vrátí se daný výčtový typ, u kterého se shoduje
	 * příslušná hodnota, pokud ne (nemělo by nastat pokud uživatel schválně nezadá
	 * "nevalidní" hodnotu), pak se vrátí výchozí hodnota.
	 * 
	 * @param fileValue
	 *            - text načtený ze souboru a dle této hodnoty se má najít k ní
	 *            patřící výčtový typ.
	 * 
	 * @return výše popsaný výčtový typ dle hodnoty v fileValue, nebo výchozí
	 *         hodnota, pokud se nenajde shoda s fileValue.
	 */
	public static KeysOrder getKeysOrderByFileValue(final String fileValue) {
		/*
		 * Pro případ, že uživatel nezadá úplně validní název nebudu testovat rovnosti
		 * velikosti písmen.
		 */
		if (fileValue.equalsIgnoreCase(WITHOUT_SORTING.getValueForFile()))
			return WITHOUT_SORTING;

		
		else if (fileValue.equalsIgnoreCase(ASCENDING_BY_ADDING.getValueForFile()))
			return ASCENDING_BY_ADDING;

		
		else if (fileValue.equalsIgnoreCase(DESCENDING_BY_ADDING.getValueForFile()))
			return DESCENDING_BY_ADDING;

		
		else if (fileValue.equalsIgnoreCase(ASCENDING_BY_ALPHABET.getValueForFile()))
			return ASCENDING_BY_ALPHABET;

		
		else if (fileValue.equalsIgnoreCase(DESCENDING_BY_ALPHABET.getValueForFile()))
			return DESCENDING_BY_ALPHABET;

		
		// Vrátím výchozí hodnotu:
		return Constants.DEFAULT_KEYS_ORDER;
	}
}
