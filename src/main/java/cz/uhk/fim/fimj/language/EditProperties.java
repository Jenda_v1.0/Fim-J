package cz.uhk.fim.fimj.language;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.PropertiesConfigurationLayout;
import org.apache.commons.configuration2.ex.ConfigurationException;

import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;

/**
 * Tato třída slouží pro manipulaci se soubory (v rámci aplikace s objekty) typu .properties tak, aby byly zachovány
 * veškeré komentáře v daném souboru. Protože když se manipuluje se souborem .properties pomocí objektu Properties z
 * balíčku java.util, pak se veškeré komentáře smažou (při uložení). Takto jsem tomu předešel.
 * <p>
 * Note:
 * Původně jsem tímto způsobem chtěl napsat i zbytek pro manipulaci se soubory .properties. Pak bych mohl smazat
 * několik metod pro zápis či čtení objektů Properties apod. Ale to jsem zavrhnul, protože chci dát možnost pro řazení
 * klíčů v souboru .properties, což by se pomocí PropertiesConfiguration nebo PropertiesConfigurationLayout nedalo. Dále
 * vkládání či získávání hodnot z objektu .properties je přes "další krok" -> je třeba vytvořit instanci této třídy a
 * volat metody, které teprve mohou manipulovat s hodnotami, navíc, pokud se jedná o vložení nové hodnoty, tak to také v
 * tomto případě nelze, (alespoň to nefunguje), funguje pouze přepisování hodnoty již existující klíče (property).
 * <p>
 * A zápis komentářů bych si zbytečně protahoval, tj. vytvořit tuto instanci, pak volat metodu a pak to zase uložit,
 * pomocí příslušné metody ve třídě WriteToFile je to "snažší" a přehlednější. Ale to je vše, proto je tato třída
 * využita pouze pro editaci objektů Properties (souborů typu .properties) tak, aby se zachovaly veškeré ostatní
 * hodnoty, hlavně, aby se zachovaly komentáře. Protože důvod pro napsání této třídy je ten, že ostaní uvedené objektu
 * pro manipulaci s objekty (soubory) Properties vymažou veškeré komentáře u klíčů. Tato třída ne.
 * <p>
 * - Další možnost by byla zapisovat komentáře zvlášť (po každé editace souboru .properties), ale, pak bych musel
 * zapisovat pořád do kola jedny a ty samé, ale když je například uživatel změní, pak se zase při dalším přepsání
 * souboru .properties přepíšou na originální komentáře, a ty uživatelovy se smažou. To je další důvod využití tohoto
 * objektu, ten zachová veškeré koomentáře v souboru.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class EditProperties {

	/**
	 * Jedná se o loader, který slouží pro načítání souboruů typu .properties
	 * (jednoho či více). Dále umožňuje přepsat již existující hodnoty v tomto
	 * souboru dle příslušného klíče.
	 * 
	 * Note:
	 * Zkoušel jsem i přidat novou hodnotu, ale to z nějakého důvodu nefungovalo,
	 * proto je zde možné pouze přepsat již existující hodnotu, je možné, že jsem
	 * někde udělal nějakou chybu, ale prostě pokud se zadá dosud neexistující klíš
	 * a k němu příslušná hodnota, buď pomocí metody setProperty nebo addPropertey,
	 * tak to novou hodnotu nepřídalo.
	 */
	private final PropertiesConfiguration config = new PropertiesConfiguration();
	
	
	
	/**
	 * Toto je taková "pomocná" třída použitá třídou PropertiesConfiguration a
	 * slouží pro uchování "vlastností" souboru .properties, jako jsou například
	 * komentáře, mezery apod.
	 */
	private final PropertiesConfigurationLayout layout = new PropertiesConfigurationLayout();

	
	
	/**
	 * Cesta k souboru typu .properties, která se má načíst.
	 */
	private final String path;
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * V parametru má cestu k souboru .properties, který se načte do této instance
	 * třídy tak, aby bylo možné manipulovat s jeho daty bez ovlivnění komentářů.
	 * 
	 * @param path
	 *            - cesta k souboru .properties.
	 */
	public EditProperties(final String path) {
		super();

		this.path = path;

		try (final Reader reader = new BufferedReader(new InputStreamReader(
				new FileInputStream(path), StandardCharsets.UTF_8))) {

			layout.load(config, reader);

		} catch (FileNotFoundException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informaci o chybě:
				logger.log(Level.INFO, "Zachycena výjimka při čtení souboru .properties na cestě: " + path
						+ ". Chyba může nastat například z důvodu, že soubor na zadané cestě neexistuje.");

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();
			
		} catch (ConfigurationException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informaci o chybě:
				logger.log(Level.INFO, "Zachycena výjimka při čtení souboru na cestě: " + path
						+ ". Tato chyba může nastat při pokusu o inicializaci objektu Configuration.");

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();

		} catch (IOException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informaci o chybě:
				logger.log(Level.INFO, "Zachycena výjimka při pokusu o čtení souboru na cestě: " + path
						+ ". Chyba může nastat při chybné vstupní / výstupní operaci (I / O operace), v tomto případě " +
						"může nastat při pokusu o zavření readeru.");

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();
		}
	}
	
	
	
	
	/**
	 * Metoda, která slouží pro přepsání hodnoty nějaké vlastnosti (property) neboli
	 * klíče v souboru .properties.
	 * 
	 * @param key
	 *            - klíč, kde se má přepsat hodnota na hodnotu value.
	 * 
	 * @param value
	 *            - nová hodnota, která se má zapsat ke klíči (vlastnosti) key.
	 */
	public final void setProperty(final String key, final String value) {
		config.setProperty(key, value);
	}
	
	
	
	
	
	/**
	 * Metoda, která slouží pro uložení změn zpět do původního souboru.
	 */
	public final void save() {
		try (final Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path),
				StandardCharsets.UTF_8))) {

			layout.save(config, writer);

			writer.flush();

		} catch (ConfigurationException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při zápisu souboru na cestě: " + path
                        + ". Tato chyba může nastat při pokusu o inicializaci objektu Configuration.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
		} catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při pokusu o zápis upraveného objektu zpět do souboru .properties na cestě: "
                                + path
                                + ". Tato chyba může nastat například z toho důvodu, že soubor na uvedené cestě je " +
                                "adresář a ne 'běžný' / potřebný soubor, nebo neexistuje, ale nemůže být vytvořen, " +
                                "nebo další důvody.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
		}
	}
}
