package cz.uhk.fim.fimj.app;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Note: Třída dědí z WindowAdapteru - reakce na události nad oknem konkrétně mně zajímá reakce na zavření aplikace
 * <p>
 * Do hlavního adresáře aplikace, do složky configuration v souboru Defautl.properties si aplikace ukládá cestu k
 * otevřenému projektu, tak ji musím nastavit, na hodnotu 'closed' nebo na nějakou, která indikuje, že není otevřen
 * projekt, jinak by si aplikace myslela (po jejím spoštění), že má otevřený projekt a šli by přidávat třídy, ... což
 * nelze
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class AppWindowListener extends WindowAdapter {

	@Override
	public void windowClosing(WindowEvent e) {
		App.saveDataBeforeCloseApp();
	}
}