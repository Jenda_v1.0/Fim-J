package cz.uhk.fim.fimj.app;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.preferences.PreferencesApi;
import cz.uhk.fim.fimj.redirect_sysout.RedirectApp;

import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.management.ManagementFactory;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Třída, která obsahuje metody, které slouží pro restartování aplikace. Je podporován restart aplikace pouze na
 * operačních systémech Windows a Linux. Testováno pouze na Windows 10 a Linux Mint 19 Mate.
 * <p>
 * Princip je, že se zavolá metoda restart, která dostaně jako parametr (mimo jiné) výčet (značí OS, na kterém běží
 * aplikace). V tomto případě, bych si mohl vzít veřejnou proměnnou z App, ale kdybych to v budoucnu nějak rozšiřoval,
 * abych si ušetřil potenciální předělávky.
 * <p>
 * Metoda restart si získá aktuální VM argumenty, sestaví z ních příkaz. Na OS Linux se uloží vždy do nově vytvořeného
 * souboru s aktuálním datumem a časem, ten se zavolá a na konec se ukončí aktuální instance aplikace. Ale ještě před
 * tím se počká definovaný čas a smaže se vytvořený spouštěcí skript, který už nebude potřeba. Případně, pokud se jej
 * nepovede smazat, uloží se cesta k němu do preferencí a při spuštění aplikace se spustí vlákno
 * cz.uhk.fim.fimj.app.DeleteLaunchScriptThread#DeleteLaunchScriptThread(), aby jej zkusilo smazat.
 * <p>
 * Na OS Windows se pouze sestaví příkaz z výše zmíněných argumentů a ten se vykoná, poté se ukončí aplikace.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 15.07.2018 11:50
 */

public class RestartApi implements LanguageInterface {

    /**
     * Čas v milisekundách, který značí, jak dlouho se bude čekat před ukončením aplikace, aby se smazal vytvořený
     * spouštěcí soubor.
     * <p>
     * Toto se ale využije pouze na OS Linux, kde se vytváří spuštěcí skript.
     * <p>
     * (Zde je třeba si hlídat, jaký čas se zde nastaví. V případě, že se nastaví příliš dlouhé čekání, pak se při
     * spuštění aplikace může stát, že se ve vlákně cz.uhk.fim.fimj.app
     * .DeleteLaunchScriptThread#DeleteLaunchScriptThread()
     * přečte hodnota z preferencí, ale ještě tam nebude uložena cesta k tomu souboru - hlídat s proměnnou:
     * cz.uhk.fim.fimj.app.DeleteLaunchScriptThread#WAIT_TIME.)
     */
    private static final long SLEEP_TIME = 1500;


    // Texty pro výpisy do dialogů:
    private String txtDirsWereNotFoundTitle;
    private String txtDirsWereNotFoundTextPartOne;
    private String txtDirsWereNotFoundTextPartTwo;

    private String txtRestartIsNotSupportedTitle;
    private String txtRestartIsNotSupportedText;

    private String txtCannotCreateLaunchScriptTitle;
    private String txtCannotCreateLaunchScriptText;
    private String txtScript;


    /**
     * Metoda, která dle OS provede kroky pro restartování aplikace.
     *
     * @param kindOfOS
     *         - výčtová hodnota, která značí typ OS, na kterém běží aplikace.
     * @param frame
     *         reference na "hlavní" okno aplikace. Není nezbytné, ale to vhodné, protože v případě restartování
     *         aplikace na Linuxu se chvíli čeká, aby se mohl smazat vygenerovaný spuštěcí skript a je vhodné okno
     *         aplikace "shovat".
     */
    public void restart(final KindOfOS kindOfOS, final JFrame frame) {
        /*
         * Spouštěcí příkaz, který v případě OS Windows stačí spustit / zavolat a v případě OS Linux je třeba jej
         * zapsat do spuštěcího skriptu (.sh souboru) a až ten soubor spustit / zavolat.
         */
        final String startCommand = getStartCommand();

        if (kindOfOS == KindOfOS.WINDOWS)
            restartOnWindows(startCommand);

        else if (kindOfOS == KindOfOS.LINUX_UNIX) {
            final File destinationDir = getDestinationDir();

            if (destinationDir == null) {// Němělo by nastat.
                if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(frame, txtDirsWereNotFoundTextPartOne +
                                "\n" +
                                txtDirsWereNotFoundTextPartTwo, txtDirsWereNotFoundTitle,
                        JOptionPane.YES_NO_OPTION)) {
                    /*
                     * Pokud uživatel zvolí ano, že chce ukončit aplikaci, pak zde musím nejprve uložit otevřený
                     * projekt - je li. Poté mohu ukončit aplikaci.
                     */
                    App.saveDataBeforeCloseApp();

                    System.exit(0);
                }

                // else - Zde uživatel zvolí Ne, že nechce ukončit aplikaci
            }

            // Zde byl nalezen adresář pro umístění skriptu, mohu jej zkusit vytvořit a zavolat:
            else restartOnLinux(frame, destinationDir, startCommand);
        }

        /*
         * V této části je aplikace spuštěna na jiném OS než je Linux nebo Windows. Jelikož jsem aplikaci zaměřil
         * hlavně na Linux a Windows, tak
         * vypíšu pouze oznámení a dotaz, zda se má aplikace ukončit.
         */
        else if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(frame, txtRestartIsNotSupportedText,
                txtRestartIsNotSupportedTitle, JOptionPane.YES_NO_OPTION))
            System.exit(0);
    }


    /**
     * Metoda, která restartuje aplikaci pokud běží na OS Windows.
     * <p>
     * Nejprve se uloží diagram tříd (oevřený projekt - pokud je), poté se spustí / zavolá příkaz pro restartování
     * aplikace.
     *
     * @param startCommand
     *         sestavený příkaz pro restartování aplikace, který se zavolá.
     */
    private static void restartOnWindows(final String startCommand) {
        /*
         * Nejprve (před finálním ukončením aplikace) a spuštěním nové instance aplikace uložím diagram tříd - a
         * obecně otevřený projekt (pokud je nějaký projekt otevřen) a zapíšu do logu informaci
         * ohledně ukončení aplikace.
         *
         * Kdybych toto uložení napsal až po následujícím zavolání, asi by se nic nestalo, ale řeším zde pouze
         * čas, který trvá pro zápis a nové spuštění aplikace.
         * I když při spuštění aplikace úmyslně chvíli čekám než se ta aplikace spustí a oteveře projekt, tak by k
         * ničemu dojít nemělo. Ale kdyby náhodou někde něco, ...
         */
        App.saveDataBeforeCloseApp();

        // execute the command in a shutdown hook, to be sure that all the
        // resources have been disposed before restarting the application
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {

                Runtime.getRuntime().exec(startCommand);

            } catch (IOException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO, "Zachycena výjimka při pokusu o provedení příkazu '" + startCommand + "' " +
                            "za " +
                            "účelem restartování aplikace na OS Windows.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
            }
        }));

        System.exit(0);
    }


    /**
     * Metoda, která slouží pro restartování aplikace na OS Linux.
     * <p>
     * Princip je takový, že se vytvoří název souboru v unikátní syntaxi, ten se vloží buď do adresáře workspace,
     * workspace/configuration nebo adresář uživatele v používaném zařízení - dle toho, jaký adresář bude nalezen. Do
     * toho souboru se zapíše příkaz i s VM argumenty pro spuštění aplikace, pak se uloží aktuálně otevřený projekt v
     * aplikaci - pokud jenějaký otevřen.
     * <p>
     * Pak se ten skrip zavolá / spustí, "schová" se se okno aplikace a počká se definovaný čas, pak se vymaže vytvořený
     * soubor - spouštěcí skript a pak se ukončí aplikace.
     *
     * @param frame
     *         - "hlavní" okno aplikace, které se má "schovat"
     * @param destinationDir
     *         - adresář, kde se má vytvořit spouštěcí soubor - skript (.sh)
     * @param startCommand
     *         - příkaz pro spuštění aplikace, který se se uloží do sktiptu.
     */
    private void restartOnLinux(final JFrame frame, final File destinationDir, final String startCommand) {
        // Získám si název pro vytvoření spuštěcího skriptu:
        final String scriptName = getFileName();

        // Vytvořím si instanci pro soubor, který bude reprezentovat spuštěcí skript:
        final File scriptFile = new File(destinationDir.getAbsolutePath() + File.separator + scriptName);

        // Vytvořím spuštěcí skript - samotný soubor na disku:
        if (!createShellLaunchScript(scriptFile, startCommand)) {
            /*
             * Zde ještě otestuji, zda se ten soubor podařilo alespoň vytvořit - například jestli třeba neselhal
             * pouze zápis apod. Pokud soubor samotný existuje,
             * pak jej zkustím i tak smazat.
             */
            deleteAndCheckLaunchScript(scriptFile);

            /*
             * V této podmínce selhalo vytvoření spuštěcího skriptu. Oznámím to uživateli a zeptám se ho, zda si
             * přeje i tak aplikaci ukončit, pokud ano, pak ji rovnou ukončtím,
             * pokud ne, pak ukončím běh této metody a tím v podstatě pokus o restart aplikace končí.
             */
            if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(frame, txtCannotCreateLaunchScriptText + "\n"
                    + txtScript + ": " + scriptFile.getAbsolutePath(), txtCannotCreateLaunchScriptTitle, JOptionPane
                    .YES_NO_OPTION)) {
                App.saveDataBeforeCloseApp();

                System.exit(0);
            }

            return;
        }

        /*
         * Nejprve (před finálním ukončením aplikace) a spuštěním nové instance aplikace uložím diagram tříd - a
         * obecně otevřený projekt (pokud je nějaký projekt otevřen) a zapíšu do logu informaci
         * ohledně ukončení aplikace.
         *
         * Kdybych toto uložení napsal až po následujícím zavolání, asi by se nic nestalo, ale řeším zde pouze
         * čas, který trvá pro zápis a nové spuštění aplikace.
         * I když při spuštění aplikace úmyslně chvíli čekám než se ta aplikace spustí a oteveře projekt, tak by k
         * ničemu dojít nemělo. Ale kdyby náhodou někde něco, ...
         */
        App.saveDataBeforeCloseApp();

        // Spustím / Zavolám skript pro spuštění nové instance aplikace:
        runScriptOnLinux(scriptFile);

        // "Schovám" okno výstupního terminálu - je li zobrazeno:
        RedirectApp.disposeOutputFrame();

        // "Schovám" okno aplikace:
        frame.dispose();

        // Počkám definovaný čas, abych mohl smazat vytvořený spouštěcí skript:
        waitSpecificTime();

        /*
         * Zkusím smazat výše vytvořený skript, pokud se to nepovede, uložím si cestu k souboru do preferencí, aby se
         * po spuštění aplikace mohl zkusit smazat.
         */
        deleteAndCheckLaunchScript(scriptFile);

        // Ukončení aplikace:
        System.exit(0);
    }


    /**
     * Metoda, která zkusí zavolat metodu, která smaže příslušný soubor file (spouštěcí skript), pokud se tento soubor
     * nepodaři smazat, uloží se cesta k němu do preferencí, aby se mohl smazat při dalším spuštění aplikace.
     *
     * @param file
     *         - cesta k vytvořenému spouštěcímu souboru, který se má smazat.
     */
    private static void deleteAndCheckLaunchScript(final File file) {
        if (!deleteLaunchScript(file))
            saveScriptFilePathToPreferences(file);
        else saveScriptFilePathToPreferences(null);
    }


    /**
     * Metoda, která v případě, že parametr file není null uloží cestu k příslušnému souboru do preferencí. Pokud bude
     * file null, pak se do preferencí uloží hodnota v Constants.FILE_NOT_SPECIFIED, která značí, že není třeba smazat
     * žádný soubor.
     *
     * @param file
     *         - cesta k spouštěcímu skriptu, která se má uložit do preferencí, aby se příslušný skript smazal v případě
     *         nouze při dalším spuštění aplikace nebo null, pokud se má uložit do preferencí hodnota, která značí, že
     *         není třeba nic mazat (není třeba smazat žádný soubor).
     */
    private static void saveScriptFilePathToPreferences(final File file) {
        if (file != null)
            PreferencesApi.setPreferencesValue(PreferencesApi.LAUNCH_SCRIPT, file.getAbsolutePath());
        else PreferencesApi.setPreferencesValue(PreferencesApi.LAUNCH_SCRIPT, Constants.FILE_NOT_SPECIFIED);
    }


    /**
     * Metoda, která slouží pro smazání spouštěcího skriptu (file).
     *
     * @param file
     *         - soubor (/ spouštěcí skript), který se má smazat.
     * @return true, pokud se povede soubor file úspěšně smazat, jinak false.
     */
    private static boolean deleteLaunchScript(final File file) {
        try {

            return Files.deleteIfExists(Paths.get(file.getAbsolutePath()));

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při pokusu o smazání souboru: " + file.getAbsolutePath() +
                        ". Tato výjimka značí chybu nějaké I/O operace. Jedná se o pokus o smazání spuštěcího skriptu" +
                        " v rámci restartování aplikace.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }

        return false;
    }


    /**
     * Metoda, která získá VM argumenty (pro aktuálně běžící instanci aplikace). Ze získaných argumentů bude sestaven
     * příkaz, kterým je možné spustit novou instanci aplikace.
     * <p>
     * Zdroj: http://lewisleo.blogspot.cz/2012/08/programmatically-restart-java.html
     *
     * @return VM Argumenty v podobě spuštěcího příkazu, který se může v případě OS Windows ihned zavolat, čímž se
     * spustí nová instance aplikace nebo se může uložit do spuštěcího skriptu (.sh souboru) v případě OS Linux a
     * zavoláním tohoto skriptu / souboru se spustí nová instance aplikace a předají se jí aktuální argumenty.
     */
    private static String getStartCommand() {
        /*
         * Je zde potřeba vždy cesta k adresáři, kde je nainstalována Java, ale v
         * aplikaci se kvůli kompilátoru načítá z preferencí uložená cesta, kde si
         * uživatel uložil ty 3 soubory nebo cesta opravdu k domovskému adresáři. Ať je
         * to tak či tak, mám v proměnné pathToJavaHome vždy uloženou cestu k adresáři,
         * kde je na daném zařízení opravdu nainstalována java.
         */
        final String java = App.PATH_TO_JAVA_HOME_DIR + File.separator + "bin" + File.separator + "java";

        // vm arguments
        final List<String> vmArguments = ManagementFactory.getRuntimeMXBean().getInputArguments();

        final StringBuilder vmArgsOneLine = new StringBuilder();

        vmArguments.forEach(arg -> {
            // if it's the agent argument : we ignore it otherwise the
            // address of the old application and the new one will be in conflict
            if (!arg.contains("-agentlib")) {
                vmArgsOneLine.append(arg);
                vmArgsOneLine.append(" ");
            }
        });


        // init the command to execute, add the vm args
        final StringBuilder cmd = new StringBuilder("\"" + java + "\" " + vmArgsOneLine);

        // program main and program arguments (be careful a sun property. might not be supported by all JVM)
        final String[] mainCommand = System.getProperty("sun.java.command").split(" ");

        // program main is a jar
        if (mainCommand[0].endsWith(".jar"))
            // if it's a jar, add -jar mainJar
            cmd.append("-jar ").append(new File(mainCommand[0]).getPath());
        else
            // else it's a .class, add the classpath and mainClass, nebo místo -cp dát -classpath
            cmd.append("-cp \"").append(System.getProperty("java.class.path")).append("\" ").append(mainCommand[0]);


        // finally add program arguments
        for (int i = 1; i < mainCommand.length; i++) {
            cmd.append(" ");
            cmd.append(mainCommand[i]);
        }

        return cmd.toString();
    }


    /**
     * Metoda, která slouží pro vytvoření skriptu / spouštěcího souboru (.sh).
     * <p>
     * Skript slouží pro spuštění aplikace na OS Linux. Do souboru se zapíše text startCommand.
     *
     * @param scriptFile
     *         - název a umístění souboru, který se má vytvořit.
     * @param startCommand
     *         - příkaz pro spuštění aplikace, který se má zapsat do souboru.
     * @return true v případě, že se soubor úspěšně vytvoří a příkaz do něj zapíše, jinak false.
     */
    private static boolean createShellLaunchScript(final File scriptFile, final String startCommand) {
        try (final OutputStreamWriter writer =
                     new OutputStreamWriter(new FileOutputStream(scriptFile), StandardCharsets.UTF_8)) {

            writer.write(startCommand);

            writer.flush();

            /*
             * Zde si uložím cestu k vytvořenému souboru do preferencí a to proto, že kdyby uživatel například
             * ukončil úmyslně aplikaci (ukončil její proces apod.) Pak
             * by ten vytvořený soubor mohl zůstat vytvořen, proto po spuštění aplikace na Linuxu se spustí vlákno,
             * které se z preferencí tento soubor načte a smaže jej.
             *
             * Toto je už ale taková "pojistka", která se nemusí využít, ale pokud se aplikace ukončí ve chvíli, kdy
             * se tento soubor vytvoří a nevyužije, tj. nepokusí se smazat po jeho spuštění, pak
             * by se ani nesmazal.
             *
             * Díky tomuto uložení do preferencí se v případě potřeby / nouze smaže po příštím spuštění aplikace.
             *
             * Zde je ještě vhodné dodat, že se tato hodnota v případě "běžného" pokrčování postupu v restartování
             * aplikace
             * hodnota přepíše nejčastěji na výchozí hodnotu, že byl soubor úspěšně smazán, nebo ještě jednou na
             * stejnou cestu k souboru.
             */
            saveScriptFilePathToPreferences(scriptFile);

            return true;

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při pokusu o vytvoření souboru - spuštěcího skriptu pro " +
                        "spuštění aplikace na Linuxu (v rámci restartování aplikace).");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }

        return false;
    }


    /**
     * Metoda, která slouží pro spuštění / zavolání skriptu na OS Linux.
     * <p>
     * Princip je takový, že se spustí nejprve příkaz, který zvýší / nastaví práva skriptu / souboru scriptFile na to,
     * aby bylo možné jej spustit. Poté se zavolá příkaz, který jej (/ skript) spustí.
     * <p>
     * Info:
     * <p>
     * https://askubuntu.com/questions/38661/how-do-i-run-sh-files
     *
     * @param scriptFile
     *         soubor, kterému se mají nastavit práva na spuštění a soubor / skript, který se spustí, aby se spustila
     *         aplikace (v rámci restartování aplikace).
     */
    private static void runScriptOnLinux(final File scriptFile) {
        /*
         * Z nějakého důvodu musím ten příkaz "exec"... spustit takto "u sebe", když bych to samotné volání dal zvláště
         * do metody a jako parametr jen ten text (/ příkaz), který se má zavolat,
         * tak to nefunguje ???
         *
         * Note:
         *
         * Také zde nefugnuje spuštění přes addShutdownHook - podobně jako u restartu na OS Windows.
         */
        try {
            // Navýším práva, aby bylo možné soubor / skript spustit / zavolat:
            Runtime.getRuntime().exec("chmod +x " + scriptFile.getAbsolutePath());

            // Spustím / zavolám skript:
            Runtime.getRuntime().exec(scriptFile.getAbsolutePath());

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při pokusu o spuštění / zavolání skriptu pro spuštění " +
                        "aplikace na OS Linux (v rámci restartování aplikace). Výjimka mohla nastat buď při provádění" +
                        " příkazu pro nastavení práv pro spuštění souboru: '" + "chmod +x " + scriptFile
                        .getAbsolutePath() + "' nebo při spuštění skriptu: '" + scriptFile.getAbsolutePath() + "'.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
    }


    /**
     * Metoda, která vrátí adresář, kam se má umístit skript pro spuštění - toto je potřeba pouze v případě OS Linux,
     * kde je třeba si vytvořit spuštěcí skript pro spuštění aplikace.
     * <p>
     * Princip je takový, že si nejrpve zkusím získat cestu k adresáři configuration ve workspace, pokud k němu nezískám
     * cestu, v tomto případě nebudu nic dělat (znovu jej vytvářet nebo oznámit, že neexistuje apod.). Pokud tento
     * adresář configration neexistuje - nebude nalezen, pak zkusím získat cestu k adresáři workspace, pokud ani ten
     * nebude nalezen, pak si získám cestu k domovskému adresáři uživatele. Ten už by měl v každém případě existovat. A
     * do tohoto nebo jednoho z výše uvedených - nalezených adresářů se vrátí cesta, kde se má vytvořit ten spuštěcí
     * skript pro aplikaci.
     * <p>
     * Výše zmíněné "nic nedělání". Když neexistuje nějaký adresář není potřeba z důvodu, že se jedná o restart aplikace
     * -> po jejím spuštění se zobrazí dotaz na workspace, případně se vytvoří adresář configuration apod. Viz třída
     * cz.uhk.fim.fimj.app.Start
     *
     * @return Jeden z výše zmíněných adresářů - cestu k němu.
     */
    private static File getDestinationDir() {
        // Cesta ke zvolenému workspace pro aplikaci:
        final String pathToWorkspace = ReadFile.getPathToWorkspace();

        if (pathToWorkspace != null) {
            final File workspaceDir = new File(pathToWorkspace);

            /*
             * Pokud existuje workspace, zkusím zjistit, zda v něm existuje adresář configuration, pokud ano, vrátím
             * k němu cestu, pokud ne,
             * pak vrátím cestu k workspace - to už budu vědět, že workspace existuje.
             */
            if (ReadFile.existsDirectory(workspaceDir.getAbsolutePath())) {
                final File configDir = new File(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME);

                if (configDir.exists() && configDir.isDirectory())
                    return configDir;

                return workspaceDir;
            }
        }

        /*
         * Sem se dostanu v případě, že neexistuje adresář zvolený jako workspace, tak si načtu cestu k adresáři,
         * který slouží jako
         * domovský adresář uživatele v používaném zařízení. Pokud bude adresář existovat, vrátím k němu cestu, jinak
         * null (toto by nastat nemělo).
         */
        final String pathToUserHome = System.getProperty("user.home");

        final File userHome = new File(pathToUserHome);

        if (ReadFile.existsDirectory(userHome.getAbsolutePath()))
            return userHome;

        return null;
    }


    /**
     * Metoda, která sestaví název spouštěcího skriptu v případě, že se má aplikace restartovat na OS Linux.
     * <p>
     * Název souboru bude v syntaxi: ".start_Fim-J_2018.mesic.den_hodina.minuta.sekunda.sh"
     *
     * @return výše zmíněný název souboru.
     */
    private static String getFileName() {
        // https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss_a");

        // (tečku před příponou souboru vygeneruje atribut "a" v simpleDateFormat)
        return ".start_Fim-J_" + simpleDateFormat.format(new Date()) + "sh";
    }


    /**
     * Metoda, která "uspí / zastaví" aplikací na definovaný čas.
     */
    private static void waitSpecificTime() {
        try {

            Thread.sleep(SLEEP_TIME);

        } catch (InterruptedException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při volání metody 'Thread.sleep' pro uspání vlákna aplikace" +
                        " " +
                        "při pokusu o restartování aplikace na OS Linux.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
    }


    @Override
    public void setLanguage(final Properties properties) {
        if (properties != null) {
            txtDirsWereNotFoundTitle = properties.getProperty("RA_DirsWereNotFoundTitle", Constants
                    .RA_TXT_DIRS_WERE_NOT_FOUND_TITLE);
            txtDirsWereNotFoundTextPartOne = properties.getProperty("RA_DirsWereNotFoundTextPartOne", Constants
                    .RA_TXT_DIRS_WERE_NOT_FOUND_TEXT_PART_ONE);
            txtDirsWereNotFoundTextPartTwo = properties.getProperty("RA_DirsWereNotFoundTextPartTwo", Constants
                    .RA_TXT_DIRS_WERE_NOT_FOUND_TEXT_PART_TWO);

            txtRestartIsNotSupportedTitle = properties.getProperty("RA_RestartIsNotSupportedTitle", Constants
                    .RA_TXT_RESTART_IS_NOT_SUPPORTED_TITLE);
            txtRestartIsNotSupportedText = properties.getProperty("RA_RestartIsNotSupportedText", Constants
                    .RA_TXT_RESTART_IS_NOT_SUPPORTED_TEXT);

            txtCannotCreateLaunchScriptTitle = properties.getProperty("RA_CannotCreateLaunchScriptTitle", Constants
                    .RA_TXT_CANNOT_CREATE_LAUNCH_SCRIPT_TITLE);
            txtCannotCreateLaunchScriptText = properties.getProperty("RA_CannotCreateLaunchScriptText", Constants
                    .RA_TXT_CANNOT_CREATE_LAUNCH_SCRIPT_TEXT);
            txtScript = properties.getProperty("RA_Script", Constants.RA_TXT_SCRIPT);
        } else {
            txtDirsWereNotFoundTitle = Constants.RA_TXT_DIRS_WERE_NOT_FOUND_TITLE;
            txtDirsWereNotFoundTextPartOne = Constants.RA_TXT_DIRS_WERE_NOT_FOUND_TEXT_PART_ONE;
            txtDirsWereNotFoundTextPartTwo = Constants.RA_TXT_DIRS_WERE_NOT_FOUND_TEXT_PART_TWO;

            txtRestartIsNotSupportedTitle = Constants.RA_TXT_RESTART_IS_NOT_SUPPORTED_TITLE;
            txtRestartIsNotSupportedText = Constants.RA_TXT_RESTART_IS_NOT_SUPPORTED_TEXT;

            txtCannotCreateLaunchScriptTitle = Constants.RA_TXT_CANNOT_CREATE_LAUNCH_SCRIPT_TITLE;
            txtCannotCreateLaunchScriptText = Constants.RA_TXT_CANNOT_CREATE_LAUNCH_SCRIPT_TEXT;
            txtScript = Constants.RA_TXT_SCRIPT;
        }
    }
}
