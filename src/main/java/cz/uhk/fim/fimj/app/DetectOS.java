package cz.uhk.fim.fimj.app;

/**
 * Tato třída slouží pro zjištění typu OS, který je aktuálně využíván.
 * 
 * Pro zjištění OS existuje více způsobů, například na zdrojích:
 * 
 * https://www.mkyong.com/java/how-to-detect-os-in-java-systemgetpropertyosname/
 * 
 * https://www.code4copy.com/java/detecting-os-type-in-java/
 * 
 * https://alvinalexander.com/blog/post/java/how-determine-application-running-mac-os-x-osx-version
 * 
 * a další. Dále je to možné například zjistit tak, že se zjistím pomocí
 * vlastnosti os.name přílušný OS a pak jen testovat, zda získaná hodnota
 * například obsahuje text Windows, Linux, případně jejich části apod.
 * 
 * Dále tato aplikace využívá knihovnu Apache commons, která také podporuje
 * získání typu OS, například:
 * 
 * System.out.println(SystemUtils.IS_OS_LINUX);
 * System.out.println(SystemUtils.IS_OS_WINDOWS);
 * System.out.println(SystemUtils.IS_OS_MAC);
 * System.out.println(SystemUtils.IS_OS_MAC_OSX);
 * System.out.println(SystemUtils.IS_OS_SOLARIS);
 * System.out.println(SystemUtils.IS_OS_UNIX);
 * System.out.println(SystemUtils.IS_OS_SUN_OS);
 * 
 * tyto hodnoty stačí dát do podmínky a dle podmínky, která bude splněna (true),
 * pak naplnit příslušný typu OS (například výčtovou hodnotu apod.).
 * 
 * Já využiji zdroj na prvním uvedeném odkaze, protože převážně plánuji využívat
 * pouze OS Windows a Linux kvůli rozlišení syntaxe cest k souborům, ale alespoň
 * nějaké další (kdyby náhodou) také uvedu, ale kdybych potřeboval více OS, asi
 * bude nejlepší využít zmíněnou knihovnu Apache commons a dle těch hodnot si
 * vytvořit Enum pro typy OS, tato knihovna obsahuje i další možností,
 * například, zda se jedná o Windows 7, Windows XP a mnohá další.
 * 
 * Dalším důvodem, proč nevyužiji výše zmíněnou knihovu pro zjištění OS je ten,
 * že by mohlo, s důrazem na to "mohlo" dojít k nějaké chybě (absolutně nevím
 * jaké) a příslušná knihovna by nemusela fungovat, nebo si někdo bude tuto
 * aplikaci nějak upravovat a třeba tu knihovnu odebere, nebo později bude nová
 * verze, kde se to bude zjišťovat jinak apod. Tak se na tu knihovnu nechi
 * vázat, názvy OS se hned tak měnit nebudou.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class DetectOS {

	private static final String OS = System.getProperty("os.name").toLowerCase();


	private DetectOS() {
	}



	/**
	 * Metoda, která zjistí typ OS, který je aktuálně využíván.
	 * 
	 * @return typ OS v podobě výčtové hodnoty.
	 */
	static KindOfOS getKindOfOs() {
		if (isWindows())
			return KindOfOS.WINDOWS;

		else if (isMac())
			return KindOfOS.MAC;

		else if (isUnix())
			return KindOfOS.LINUX_UNIX;

		else if (isSolaris())
			return KindOfOS.SOLARIS;

		return KindOfOS.OTHER;
	}
	
	
	
	/**
	 * Metoda, která zjistí, zda je aktuálně využíván OS typu Windows.
	 * 
	 * @return true, pokud je využíván OS typu windows, jinak false.
	 */
	private static boolean isWindows() {
		return (OS.contains("win"));
	}

	/**
	 * Metoda, která zjistí, zda je aktuálně využíván OS typu Mac.
	 * 
	 * @return true, pokud je využíván OS typu Mac, jinak false.
	 */
	private static boolean isMac() {
		return (OS.contains("mac"));
	}

	/**
	 * Metoda, která zjistí, zda je aktuálně využíván OS typu Unix.
	 * 
	 * @return true, pokud je využíván OS typu Unix, jinak false.
	 */
	private static boolean isUnix() {
		return (OS.contains("nix") || OS.contains("nux") || OS.indexOf("aix") > 0);
	}

	/**
	 * Metoda, která zjistí, zda je aktuálně využíván OS typu Solaris.
	 * 
	 * @return true, pokud je využíván OS typu Solaris, jinak false.
	 */
	private static boolean isSolaris() {
		return (OS.contains("sunos"));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která dle OS, na kterém je spuštěna tato aplikace se vrátí regulární
	 * výraz pro rozpoznání cesty k nějakému souboru či adresáři pro rozpoznání
	 * syntaxe pro aktuální OS.
	 * 
	 * Vím, že Windowsy používají pro cestu zpětná lomítka a linuxy normální
	 * lomítka. Tyto normální lomítka pouzžívá v podstatě i většina ostatních OS,
	 * včetně Mac, ale některé verze Mac, například Classic Mac OS používá dvojtečky
	 * (pokud je mi známo).
	 * 
	 * Proto jsem zatím napsal cesty k souborů pouze pro syntaxe, které využívá
	 * Windows a Linux, ale do budoucna je možné to rozšíčit tak, že dle potřeby pro
	 * konkrétní cestu k nějakému konkrétnímu souboru či adresáři nebo jen cestě
	 * apod. (záleží na aktuální potřebě) lze tuto metodu doplnit o další parametr,
	 * který bude obsahovat regulární výraz pro další OS, resp. s dalším
	 * rozpoznáváním syntaxe, třeba s tou dvojtečkou a dle toho doplnit možnosti ve
	 * switchi pro vrácení.
	 * 
	 * Zatím je rozpoznávána cesta puze pro windows, pokud je aktuální OS Windows,
	 * pak se vrátí regExForWindows, jinak se vrátí regExForLinux ve všech ostatních
	 * případech.
	 * 
	 * @param regExForWindows
	 *            - reguálární výraz pro nějakou cestu k adresáři souboru apod. V
	 *            syntaxi pro OS typu Windows
	 * 
	 * @param regExForLinux
	 *            - reguálární výraz pro nějakou cestu k adresáři souboru apod. V
	 *            syntaxi pro OS typu Linux
	 * 
	 * @return dle typu OS, na kterém je spuštěna tato aplikace se vrátí regulární
	 *         výraz z parametru metody, který je definován pro daný OS.
	 */
	public static String getRegExByKindOfOs(final String regExForWindows, final String regExForLinux) {
		switch (App.KIND_OF_OS) {
		case WINDOWS:
			return regExForWindows;

		case LINUX_UNIX:
			return regExForLinux;

		case MAC:
			return regExForLinux;

		case SOLARIS:
			return regExForLinux;

		case OTHER:
			return regExForLinux;

		default:
			return regExForLinux;
		}
	}
}
