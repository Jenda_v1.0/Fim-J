package cz.uhk.fim.fimj.app;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.preferences.PreferencesApi;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Třída slouží jako vlákno, které se spustí po spuštění aplikace v případě, že je aplikace spuštěna na OS Linux. Vždy
 * se z preferencí načtě cesta k vytvořenému souboru (spouštěcímu skriptu - .sh), který byl vytvořen za účelem spuštění
 * aplikace s předanými VM argumenty.
 * <p>
 * V případě, že je aplikace spuštěna na OS Linux a uživatel bude chtít restartovat aplikaci, pak se vytvoří skript, do
 * kterého se vloží příkaz pro spuštění aplikace s aktálními VM argumenty. Tento soubor je ale potřeba po využití
 * smazat. Resp. je pouze na jedno použití, poté již není potřeba.
 * <p>
 * (Soubor se zkouší odebrat vícekrát - pro případ, že by jej využíval nějaký proces apod.)
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 16.07.2018 21:23
 */

public class DeleteLaunchScriptThread extends Thread implements LanguageInterface {

    /**
     * Čas, který značí, jak dlouho se bude čekat, než se načte z preferencí soubor, který se má smazat.
     */
    private static final long WAIT_TIME = 10000;

    /**
     * Čas, který značí interval opakování timeru.
     */
    private static final long PERIOD = 2000;

    /**
     * Hodnota, která značí počet opakování / pokusů pro smazání příslušného souboru / skriptu.
     */
    private static final int NUMBER_OF_QUERIES = 5;

    /**
     * Proměnná pro uložení počtu neúspěšných pokusů - až se dojde k nule, vlákno se ukončí (s chybovým oznámením).
     */
    private int actualNumberOfQueries;


    private final Timer timer;


    private String txtDeleteScriptFailedText;
    private String txtDeleteScriptFailedTitle;
    private String txtScript;


    DeleteLaunchScriptThread() {
        super();

        timer = new Timer();

        actualNumberOfQueries = NUMBER_OF_QUERIES;
    }


    @Override
    public void run() {
        /*
         * Nejprve se počká definovaný čas, aby se případně stihla zapsat cesta do preferencí při ukončování nějaké
         * (/ bývalé) instance aplikace.
         */
        waitSpecificTime();


        /*
         * Načtení hodnoty uložené v preferencích, kam se ukládá cesta ke spouštěcímu skriptu - je li potřeba.
         */
        final String pathToScript = PreferencesApi.getPreferencesValue(PreferencesApi.LAUNCH_SCRIPT, null);

        if (pathToScript == null || pathToScript.equals(Constants.FILE_NOT_SPECIFIED))
            return;

        final File file = new File(pathToScript);


        /*
         * Každých PERIOD ms se pokusím NUMBER_OF_QUERIES - krát smazat soubor file - spouštěcí skript.
         */
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                // Test, zda soubor existuje, pokud ne, pak končíme:
                if (!file.exists() || !file.isFile()) {

                    timer.cancel();

                    closeThread();

                    // Ukončím úlohu:
                    cancel();

                    /*
                     * Zde již soubor neexistuje, mohu do preferncí uložit výchozí hodnotu, aby se nespouštělo vlákno
                     * při dalším spuštění aplikace - už to nebude potřeba, když ten soubor již neexistuje.
                     */
                    saveFileNotSpecifiedToPreferences();

                    // Zde mohu ukončit metodu (není nezbytné):
                    return;
                }

                // Zkusím smazat soubor:
                deleteFile(file);


                // Pokud jsem nevyčerpal veškeré pokusy, pak snížím počet zbývajících pokusů:
                if (actualNumberOfQueries > 0)
                    actualNumberOfQueries--;

                    // Pokud jsem vyčerpal pokusy a soubor stále existuje, pak skončím a oznámím uživateli, že se
                    // nepodařilo soubor smazat.
                else if (file.exists() && file.isFile()) {
                    JOptionPane.showMessageDialog(null, txtDeleteScriptFailedText + "\n" + txtScript + ": " + file
                            .getAbsolutePath(), txtDeleteScriptFailedTitle, JOptionPane.ERROR_MESSAGE);

                    closeThread();

                    // Ukončím úlohu:
                    cancel();
                }
            }
        }, PERIOD, PERIOD);
    }


    /**
     * Metoda,  která ukončí časovač a vlákno.
     */
    private void closeThread() {
        timer.cancel();
        currentThread().interrupt();
    }


    /**
     * Metoda, která zkusí vymazat soubor file.
     *
     * @param file
     *         - soubor, který se má smazat.
     */
    private static void deleteFile(final File file) {
        try {

            Files.delete(Paths.get(file.getAbsolutePath()));

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při pokusu o smazání souboru: " + file.getAbsolutePath() +
                        ". (V rámci restartování aplikace.) Soubor se nepodařilo smazat při ukončení poslední " +
                        "instance aplikace - před jejím ukončením při restartování.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
    }


    /**
     * Metoda, která uloží do preferencí hodnotu o tom, že není třeba mazat spouštěcí skript na OS Linux, tedy do
     * preferencí uloží výchozí hodnotu, která značí, že není třeba mazat soubor - k žádnému není nastavena cesta.
     */
    private static void saveFileNotSpecifiedToPreferences() {
        PreferencesApi.setPreferencesValue(PreferencesApi.LAUNCH_SCRIPT, Constants.FILE_NOT_SPECIFIED);
    }


    /**
     * Metoda, která uspí vlákno na WAIT_TIME čas. (Aby se potenciálně stihla zapsat cesta ke spouštěcímu sktiptu do
     * preferencí z poslední instance aplikace.)
     */
    private static void waitSpecificTime() {
        try {

            Thread.sleep(WAIT_TIME);

        } catch (InterruptedException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při pokusu o uspání vlákna, než se pokusí získat cesta ke " +
                        "spouštěcímu skriptu z preferencí a případně smazat příslušný soubor.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
    }

    @Override
    public void setLanguage(final Properties properties) {
        if (properties != null) {
            txtDeleteScriptFailedText = properties.getProperty("DLST_DeleteScriptFailedText", Constants
                    .DLST_TXT_DELETE_SCRIPT_FAILED_TEXT);
            txtDeleteScriptFailedTitle = properties.getProperty("DLST_DeleteScriptFailedTitle", Constants
                    .DLST_TXT_DELETE_SCRIPT_FAILED_TITLE);
            txtScript = properties.getProperty("DLST_Script", Constants.DLST_TXT_SCRIPT);
        } else {
            txtDeleteScriptFailedText = Constants.DLST_TXT_DELETE_SCRIPT_FAILED_TEXT;
            txtDeleteScriptFailedTitle = Constants.DLST_TXT_DELETE_SCRIPT_FAILED_TITLE;
            txtScript = Constants.DLST_TXT_SCRIPT;
        }
    }
}
