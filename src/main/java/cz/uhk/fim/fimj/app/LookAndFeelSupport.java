package cz.uhk.fim.fimj.app;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.logger.ExtractEnum;

import javax.swing.*;
import java.util.Arrays;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Třída obsahuje metody pro "práci" s Look and Feel.
 * <p>
 * Jedná se například o nastavení designu aplikace, konstrola, zda je příslušný "design" dostupný apod.
 * <p>
 * <p>
 * Zdroj:
 * <p>
 * https://www.geeksforgeeks.org/java-swing-look-feel/
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 28.08.2018 14:35
 */

public final class LookAndFeelSupport {

    private LookAndFeelSupport() {
    }

    /**
     * Nastavení "designu" aplikace (Look and Feel aplikace).
     *
     * <i>Vezme se ze souboru defaultProp hodnota, která značí uživatelem nastavné lookAndFeel, zjistí se, zda je k
     * dispozici pro konkrétní / aktuálně využívanou platformu a pokud ano, nastaví se. Pokud není k dispozici nebo se
     * nenačte požadovaná čí výchozí hodnota apod. "Nic se nestane." Tj. bude využito výchozí nastavení.</i>
     *
     * @param defaultProp
     *         - načtený soubor .properties, obsahující uživatelem nastavenou hodnotu pro Look and Feel.
     */
    static void setLookAndFeel(final Properties defaultProp) {
        final String lookAndFeel;

        if (defaultProp != null)
            lookAndFeel = defaultProp.getProperty("LookAndFeel", Constants.DEFAULT_LOOK_AND_FEEL);
        else lookAndFeel = Constants.DEFAULT_LOOK_AND_FEEL;

        // Nemělo by nastat:
        if (lookAndFeel == null)
            return;

        /*
         * Zde se zjistí, zda se má vůbec nějaký styl aplikovat. Uživatel nemusí chtít aplikovat žádný styl, resp.
         * chce využít výchozí styl pro desktopové aplikace (platformy), který je pro veškeré platformy stejný.
         *
         * <i>V případě, že je podmínka splněna a nemá se aplikovat žádný styl, je možné skončit a ani by se nemusel
         * nastavovat výchozí styl ("getCrossPlatformLookAndFeelClassName"). Ale je pro jistotu nastaven pro případ,
         * že by se naříklad v nějakém jiném kroku nastavil jiný Look and Feel (nemělo by nastat), ale kdyby tu
         * aplikaci někde předělával apod.</i>
         */
        if (lookAndFeel.equalsIgnoreCase(Constants.WITHOUT_SPECIFIC_LOOK_AND_FEEL))
            setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());

            /*
             * Nejprve se zjistí, zda je výše získané lookAndFeel k dispozici pro aktuálně využívanou platformu, pokud
             * ano, nastaví se, v opačném případě se nic neprovede, takže se ponechá výchozí nastavení.
             */
        else if (checkExistLookAndFeel(lookAndFeel))
            setLookAndFeel(lookAndFeel);
    }


    /**
     * Kontrola, zda je LookAndFeelSupport k dispozici pro aktuálně využívanou platformu.
     *
     * @param lookAndFeel
     *         - className lookAndFeel, o kterém se má zjistit, zda je dostupné pro aktuálně využívanou platformu.
     *
     * @return true v případě, že je lookAndFeel k dispozici pro aktuálně využívanou platformu, jinak false.
     */
    public static boolean checkExistLookAndFeel(final String lookAndFeel) {
        final UIManager.LookAndFeelInfo[] installedLookAndFeels = UIManager.getInstalledLookAndFeels();

        return Arrays.stream(installedLookAndFeels).anyMatch(laf -> laf.getClassName().equals(lookAndFeel));
    }


    /**
     * Nastavení Look and Feel.
     *
     * @param lookAndFeel
     *         - index Look and Feel, které se má nastavit.
     */
    private static void setLookAndFeel(final String lookAndFeel) {
        try {

            UIManager.setLookAndFeel(lookAndFeel);

        } catch (final ClassNotFoundException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při pokusu o nastavení Look and Feel. Výjimka může nastat v" +
                        " případě, že zadaný identifikátor: '" + lookAndFeel + "' není identifikátorem třídy, která " +
                        "dědí z LookAndFeelSupport.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();

        } catch (final InstantiationException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při pokusu o nastavení Look and Feel. Výjimka může nastat v" +
                        " případě, že se nepodařilo vytvořit instanci třídy pro nastavení LookAndFeelSupport.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();

        } catch (final IllegalAccessException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při pokusu o nastavení Look and Feel. Výjimka může nastat v" +
                        " případě, že se třída nebo initializer není přístupný pro nastavení LookAndFeelSupport.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();

        } catch (final UnsupportedLookAndFeelException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při pokusu o nastavení Look and Feel. Výjimka může nastat v" +
                        " případě, že 'lnf.isSupportedLookAndFeel() is false'");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
    }


    /**
     * Získání indexů (názvů tříd) podporovaných Look and Feel pro aktuálně využívanou platformu - kde běží aplikace.
     * <p>
     * Hodnoty / index budou oddělené desetinnou čárkou.
     *
     * @return text, který bude obshaovat indexy Look and Feel oddělené desetinnou čárkou, které jsou podporovány pro
     * platformu, kde běží aplikace.
     */
    public static String getSupportedLookAndFeelsInText() {
        final UIManager.LookAndFeelInfo[] installedLookAndFeels = UIManager.getInstalledLookAndFeels();

        final StringBuilder lookAndFeelBuilder = new StringBuilder();

        Arrays.stream(installedLookAndFeels).forEach(lookAndFeelInfo -> lookAndFeelBuilder.append(lookAndFeelInfo.getClassName()).append(", "));

        // Odebrání poslední destinné čárky:
        if (lookAndFeelBuilder.toString().endsWith(", "))
            lookAndFeelBuilder.setLength(lookAndFeelBuilder.length() - 2);

        return lookAndFeelBuilder.toString();
    }


    /**
     * Získání Look and Feeld dostupných pro aktuálně používanou platformu.
     *
     * @return pole dostupných Look and Feel pro využívanou platformu.
     */
    public static UIManager.LookAndFeelInfo[] getInstalledLookAndFeels() {
        return UIManager.getInstalledLookAndFeels();
    }
}
