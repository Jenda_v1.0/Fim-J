package cz.uhk.fim.fimj.app;

import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.GetIconInterface;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;

/**
 * Tato třída slouží pouze jako "SplashScreen" - má verze po spuštění apliakce se otestuje Workspace, a pokud se vybere,
 * nebo je již označen tak by se mělo spustit hlavní okno aplikace pro práci s projekty, ale ještě před tím, než se
 * spustí toto hlavní okno aplikace jsem nastavil, podobně jako u eclipsu tuto "úvodní" obrazovku s obrázkem
 * znázovrňující aplikaci a JprogressBarem, který simuluje načítání
 * <p>
 * Info:
 * <p>
 * Vytvoření / nastavení transparentnosti obrázku - aby byl obrázek transparentní (Stačí přes Upload nahrát obrázek):
 * https://www167.lunapic.com/editor/
 * <p>
 * (Pokud chceme nastavit, aby byl obrázek transparentní, na tom webu stačí nahrát obrázek a kliknout na "Crop", pak se
 * nám zpřístupní tlačítka pro nastavení "kde" má být obrázek transparentní. Ale pokud obrázek, který na ten web
 * nahrajeme dosud nebyl upravován - například v malování apod. Tak by se nám měli zpřístupňit možnosti pro nastavení
 * transparentnosti ihned po nahrání obrázku)
 * <p>
 * - akorát se vždy uloží jako gif obrázky, ale to nevadí, ty lze zobrazit, pouze se nebudou animovat, pokud se
 * nezobrazí jako nemodální okno ve SwingUtilities vlákně - viz to řešení loading dialogu v Fim-J.
 * <p>
 * Jinak princip tohoto okna je takový, že se jedná o "JWindow" s průhledným pozadím. Jako pozadí má nastavený ten
 * obrázek a do spodní části okna je přidán toolbar, který ja odsazen (vertikálně).
 *
 * @author Jan Krunčík
 * @version 1.0
 */

class SplashScreen extends JWindow {

	
	private static final long serialVersionUID = 1L;


	/**
	 * Proměnná, která značí počet, který bude dekrementován do nuly. Až se tak stane, aplikace bude spuštěna.
	 *
	 * K této proměnné je důležité nastavit proměnnou DELAY_IN_MS, protože tato proměnná značí jak "často / rychle"
	 * bude
	 * proměnná COUNT dekrementována.
	 *
	 * Například když nastavíme COUNT na 5 a proměnnou DELAY_IN_MS na 1000, pak se COUNT každou sekundu dekrementuje,
	 * tedy po pěti vteřinách bude možné spustit aplikaci. Nebo COUNT bude 10 a DELAY_IN_MS bude 500. Pak se dvakrát za
	 * sekundu sníží COUNT. Celkový doba bude tedy také 5 vteřin.
	 *
	 * (Nyní pro COUNT = 12 a DELAY_IN_MS = 500 je celková doba trvání 6 vteřin)
	 */
	private static final int COUNT = 12;

	/**
	 * Proměnná, která značí jak často provede timer příslušnou akci. Viz komentář u proměnné COUNT.
	 */
	private static final int DELAY_IN_MS = 500;

	/**
	 * Proměnná, která slouží pouze pro odkládání hodnoty při dekrementaci proměnné COUNT.
	 */
	private int temp;




	/**
	 * timer, který bude "zdržovat" a zvyšovat hodnotu progressBaru
	 */
	private final Timer timer;
	
	
	
	
	
	/**
	 * Reference na App - coby Hlavní okno Aplikace, které se spustí 
	 * ve externém vlákně, aby se stihlo vykonat vše co je potřeba dřívě než 
	 * doběhne progressBar:
	 */
	private App app;
	
	
	
	
	
	/**
	 * Vlákno, které se spustí,aby vytvořilo hlavní okno aplikace
	 * během doby, co se bude zobrazovat SplassScreen, během té doby  
	 * se vše na pozadí přípraví a až doběhne SplashScreen, tak se p
	 * ouze již připravené hlavní okno zobrazí:
	 */
	private final Thread prepareAppThread;
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 */
	SplashScreen() {
		super();

		// Je nezbytně nutné nastavit transparentnost okna, bez toho to nebude fungovat:
		setBackground(new Color(0, 0, 0, 0));

		// Nejprve nastavím obrázek pozadí:
		setContentPane(new JLabel(GetIconInterface.getMyIcon(Constants.PATH_TO_APP_ICON, false)));
		
		
		// Získám si ten kontejner už i s obrázkem, a "na něj" se budou přidávat komponenty,
		// aby byl obrázek na pozadí
		final Container container = getContentPane();
		container.setLayout(new BorderLayout());


		/*
		 * Zde si definuji, jak rychle se ta "přebíhající" část bude pohybovat po té liště tam a zpět (cycleTime) ->
		 * tím definuji, jak dlouho potrvá,
		 * než se dostane jednou tam a zpět, tedy vždy za půlku nastaveného cycleTimu přeběhne z jedné strany na
		 * druhou, za celý čas cycleTime se dostane tam a zpět.
		 *
		 * repaintInterval značí, jak "rychle" se ta "přebíhající" část bude překreslovat, tím se v podstatě dá
		 * nastavit jak moc se bude "sekat" - překreslovat, resp.
		 * jak se bude posouvat, že se třeba zobrazí a o chvíli později se zobrazí o kus dál, nebo pojede plynule apod.
		 */
		UIManager.put("ProgressBar.repaintInterval", 10);
		UIManager.put("ProgressBar.cycleTime", 2000);

		// Vytvořím progressBar, který bude simulovat načítání:
		final JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		
		
		// Vytvořím panel, do kterého vložím progressBar, abych ho mohl zarovnat do pravého dolního
		// rohu a vložím tento panel do okna coby SplashScreen
		final JPanel pnlProgressBar = new JPanel();
		pnlProgressBar.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 40));
		// Aby byl ten panel průhledný
		pnlProgressBar.setOpaque(false);
		pnlProgressBar.add(progressBar);
		
		container.add(pnlProgressBar, BorderLayout.SOUTH);
		
		
		// Vytvořím a spustím vlákno na pozadí, aby zatím připravilo hlavní okno aplikace,
		// než doběhne SplashScreen
		prepareAppThread = new PrepareAppThread();
		prepareAppThread.start();



		// Naplním proměnnou, pro dekrementaci:
		temp = COUNT;
		// Vytvořím timer a nastavím mu zpoždění, resp. že se má opakovat každých 50 ms:
		timer = new Timer(DELAY_IN_MS, event -> checkTime());
		// Pro rychlejší spuštění - pouze v rámci testování:
		timer.start();
		
		
		// Předpřipravená velikost SplashScreenu:
		setPreferredSize(new Dimension(510, 510));

		
		
		pack();
		setLocationRelativeTo(null);
		
		setVisible(true);	
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která dekrementuje proměnnou COUNT a otestuje, zda je proměnná nula. Pokud ano, provede kroky pro
	 * spuštění hlavního okna aplikace.
	 */
	private void checkTime() {
		temp--;
		
		if (temp <= 0) {
			// Zde již doběhl ProgressBar, tak toto okno mohu zavřít a zobrazit hlavní okno
			// aplikace:

			// Nejprve ukončím timer:
			timer.stop();

			// Zneviditelním okno:
			setVisible(false);					
			
			
			// Otestuji, zda již vlákno doběhlo - resp. otestuji, zda je již přípravené hlavní okno aplikace,
			// pokud není, resp. vlákno ještě běží, tak zavolám metodu, join, která počká než vlákno doběhne - ukončí
			// se a pak již bude připravené hlavní okno aplikace a je možné jej zobrazit
			if (prepareAppThread.isAlive()) {
				try {
					prepareAppThread.join();
				} catch (InterruptedException e) {
					/*
					 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
					 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
					 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
					 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
					 */
					final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

					// Otestuji, zda se podařilo načíst logger:
					if (logger != null) {
						// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

						// Nejprve mou informaci o chybě:
						logger.log(Level.INFO,
								"Zachycena výjimka při volání metody join pro počkání na dokončení vlákna, třída " +
										"MySplashScreen"
										+ ". Třída MySplashScreen, metoda increaseValueOfProgressBar.");

						/*
						 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
						 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
						 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
						 */
						logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
					}
					/*
					 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
					 */
					ExceptionLogger.closeFileHandler();
				}
			}
			
			// Zde se již přípravilo hlavní okno aplikace - doběhl konstruktor ve třídě App:
			
			// Nejprve otestuji, zda je nějaká projekt otevřen:
			final boolean isOpenedProject = App.checkOpenProject();
			
			// otestuji, dle výsledku, zda je oteřený projekt
			if (!isOpenedProject)
				//  Zde je nějaký projekt otevřen - nebyl zavřen a ještě existuje:
				// Znepřístupním tlačítka, která nejsou potřeba,
				// pokud není otevřený projekt:
				App.enableButtons(false);
			
			
			// nyní doběhl progresbar, tak mohu zobrazit hlavní okno aplikac
			app.setVisible(true);
			
			// Zavřu toto okno coby načítání - SplashScreen - toto okno
			dispose();
		}
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Tato třída slouží jako vlákno, které se spustí během zobrazování
	 * SplachScreenu, a na pozadí přípraví hlavní okno aplikace, aby si načetlo
	 * všechny potřené konfigurační soubory, vytvořili potřebné komponenty, ... atd.
	 * prostě aby se udělalo vše co je potřeba pro spuštění hlavního okna aplikace
	 * pro práci s projekty. Až toto vlákno doběhne, už stači pouze toto již
	 * připravené okno pouze zobrazit (metoda setVisible(true)) nad připraveným
	 * oknem, což se sudělá, až doběhne připravený progressBar.
	 * 
	 * @author Jan Krunčík
	 * @version 1.0
	 */
	private final class PrepareAppThread extends Thread {
		@Override
		public void run() {
			SwingUtilities.invokeLater(() -> app = new App());
		}
	}
}