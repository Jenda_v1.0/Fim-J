package cz.uhk.fim.fimj.app;

import java.io.File;
import java.util.Properties;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.file.UserNameSupport;
import cz.uhk.fim.fimj.file.WriteToFile;
import cz.uhk.fim.fimj.forms.WorkspaceForm;

/**
 * Spuštěcí třída
 * pak se zobrazí dialog pro zadání místa workspacu,
 * Pokud Workspace neexistuje, vytvoři se na zvoleném místěi
 *
 *
 * Postup:
 * Načtu si výchozí Default.properties v adresáři aplikace
 * Načtu si z něj cestu k složce coby Workspace
 * Pokud existuje
 * 		tak se ujistím, že existuje a je to opravdu "Workspace" - služka
 * 		dále jestli obsahuje složku configuration
 * 			Pokud ano: tak se kouknu do Default.properties v Workspace na výchozí nastavení
 * 				V tomto příápadě, zda se mám dotázat na Workspace nebo ne:
 * 						Pokud ano, tak se dtáži zavoláním metody selectWorkspace
 * 						Pokud ne, tak nezbývá než spustit aplikaci (mám Workspace i konfigurační soubory)
 * 			Pokud ne: tak je vytvořím a spustím aplikaci
 * Pokud neexistuje, tak otevřu okno pro výběr workspacu (metoda: selectWorkspace)
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public final class Start {

    /**
     * Při převedení Java projektu na Maven se změnily cesty ke zdrojovým adresářům na jeden zdrojový adresář
     * "resources". V této proměnné je k němu cesta, aby se vědělo, kam se mají zapisovat konfigurační soubory - výchozí
     * hodnoty, které jsou uvedeny ve vygenerované aplikaci (jako výchozí hodnoty).
     */
    private static final String PATH_TO_RESOURCES_FOLDER = "src/main/resources/";


    public static void main(final String[] args) {
        /*
         * Zde je vhodné doplnit / ponechat, že v rámci využití knihovny (sada knihoven
         * "ApachePOI") pro otevření misrosoftích dokumentů (word, excel, ...) se
         * občas, ne pokaždé, jen někdy z nějakého důvodu vypsalo následující varování:
         *
         * log4j:WARN No appenders could be found for logger (org.apache.pdfbox.util.PDFStreamEngine).
         * log4j:WARN Please initialize the log4j system properly.
         * log4j:WARN See http://logging.apache.org/log4j/1.2/faq.html#noconfig for more info.
         *
         * Nedošlo k žádné chybě nebo tak něco, vše fungovalo, jak mělo, potřebné
         * soubory se otevřely atd. Pouze se vypsalo výše uvedené varování. Jedná se o to, že neni soubor log4j.xml
         * nebo log4.xml nebo tak něco v
         * cestě classPath.
         *
         * Ale toto by měla řešit jiná kniovna, jejíž název si nepamatuji, ale na
         * odkaze:
         *
         * https://stackoverflow.com/questions/1140358/how-to-initialize-log4j-properly
         *
         * jsem se dohledal nějaké metody (ne zrovna nejlepší řešení), která to potlačí,
         * ale jelikož to nastalo cca 1 - 2 krát, tak ta metoda může být zakomentována, ale aby to "nerušilo"
         * uživatele, tak je odkomentována.
         */
        org.apache.log4j.BasicConfigurator.configure();


        // Otestuji, zda již existuje Workspace nebo ne, pokud ano, tak zda se na něj mám dotázat:
        // Pokud Workspace neexistuje, nebo se na něj mám dotázat, budu pokračovat


        /*
         * Získám si cestu k adresáři workspace (pokud je zadána).
         */
        final String pathToWorkspace = ReadFile.getPathToWorkspace();


		/*
		 * Toto zde nechám zakomentované, kdyby si někdo hrál ze zdrojovými kódy
		 * aplikace, konkrétně, když by bylo potřeba přenačíst konfigurační soubory pro
		 * nastavení některých komponent v apliikaci, tyto metody slouží přávě k tomu,
		 * že se nově zapíšou konfigurační soubory do adresáře v aplikaci.
		 */

		// Jazyky:
//		WriteToFile.writeLanguages(new File(PATH_TO_RESOURCES_FOLDER + Constants.CONFIGURATION_NAME));
//
//      // Vytvoření adresáře configuration i s konfiguračními soubory a texty:
//        WriteToFile.createConfigureFilesInPath(PATH_TO_RESOURCES_FOLDER);
//
//		// Konfiguracni soubory pro komponenty v aplikaci:
//		ConfigurationFiles.writeDefaultPropertiesAllNew(PATH_TO_RESOURCES_FOLDER + Constants.PATH_TO_DEFAULT_PROPERTIES);
//		ConfigurationFiles.writeClassDiagramProperties(PATH_TO_RESOURCES_FOLDER + Constants.PATH_TO_CLASS_DIAGRAM_PROPERTIES);
//		ConfigurationFiles.writeInstanceDiagramProperties(PATH_TO_RESOURCES_FOLDER + Constants.PATH_TO_INSTANCE_DIAGRAM_PROPERTIES);
//		ConfigurationFiles.writeCommandEditorProperties(PATH_TO_RESOURCES_FOLDER + Constants.PATH_TO_COMMAND_EDITOR_PROPERTIES);
//		ConfigurationFiles.writeCodeEditorProperties(PATH_TO_RESOURCES_FOLDER + Constants.PATH_TO_CODE_EDITOR_PROPERTIES);
//		ConfigurationFiles.writeOutputEditorProperties(PATH_TO_RESOURCES_FOLDER + Constants.PATH_TO_OUTPUT_EDITOR_PROPERTIES);
//		ConfigurationFiles.writeCodeEditorPropertiesForInternalFrames(PATH_TO_RESOURCES_FOLDER + Constants.PATH_TO_CODE_EDITOR_INTERNAL_FRAMES_PROPERTIES);
//		ConfigurationFiles.writeOutputFrameProperties(PATH_TO_RESOURCES_FOLDER + Constants.PATH_TO_OUTPUT_FRAME_PROPERTIES);


        if (pathToWorkspace == null) {
            /*
             * Zde neexistuje adresář workspace, tak nejprve se nastaví výchozí Look and Feel pro aplikaci, aby byl
             * dialog pro nastavení adresáře workspace ve správném "stylu".
             */
            LookAndFeelSupport.setLookAndFeel(null);

            // Zde Workspace neexistuje, je třeba ho načíst od uživatele:
            selectWorkspace();

            return;
        }


        // Otestuji, jestli na získané cestě (uložené) ještě složka
        // existuje:
        final File fileWorkspace = new File(pathToWorkspace);

        // Zde si načtu pole všech názvů, jejich název = configuration
        final String[] listOfFiles = fileWorkspace.list((dir, name) -> name.equals(Constants.CONFIGURATION_NAME));

        // Zde si pomocí cyklu prohledám pole názvů ve složce Workspace
        // abych zjistil, zda se tam nachází složka configuration:
        if (listOfFiles == null || listOfFiles.length <= 0) {
            /*
             * Tato část by neměla nastat, ale může se stát, že uživatel například vymaže adresář configuration před
             * spuštěním aplikace, pak je třeba jej vytvořit a dotázat se uživatele na workspace - výchozí nastavení
             * pro nově vytvořené konfigurační soubory.
             */

            // Zde se složka configuration nenašla, je třeba ji vytvořím
            WriteToFile.createConfigureFilesInPath(pathToWorkspace);

            // Uložím cestu k workspace do soubouru:
            App.WRITE_TO_FILE.changePathToWorkspace(pathToWorkspace);

            /*
             * Výše se vytvořily konfigurační soubory a nastavila cesta k workspace, tak je třeba nastavit
             * uživatelské jméno do aplikace, aby se do logu zapsalo správné uživatelské jméno.
             *
             * (Nejspíše bude v souboru uloženo výchozí jméno příhlášeného uživatele v OS. Protože při vytvoření
             * konfiguračních souborů se bere výchozí jméno.)
             */
            UserNameSupport.loadAndSetUserNameFromDefaultProp();

            // Zapíšu do logu spustění aplikace:
            App.WRITE_TO_FILE.writeStartAppInfoToLog();

            /*
             * Výše se nově vytvořily konfigurační soubory a uložil workspace. Níže následuje dotaz na adresář
             * workspace, takže je zde třeba nastavit Look and Feel, aby se dialog s výběrem adresáře workspace
             * zobrazil alespoň ve výchozím zobrazení.
             *
             * Pokud se níže vybere jiný adresář workspace, pak se posléze aplikuji nastavení z nově zvoleného
             * workspace.
             */
            LookAndFeelSupport.setLookAndFeel(ReadFile.getDefaultPropAnyway());

            // V této častí se výše vytvořili nové konfigurační soubory, takže v
            // Default.properties bude, že se aplikace má dotázat na workspace, tak se
            // dotážu:
            selectWorkspace();

            return;
        }


        // Zde ve zvoleneé složce jako Workspace existuje nějaký soubor / adresář s název confiuration, zjistí se,
        // zda se jedná o složku:
        for (final String s : listOfFiles) {
            final File fileConfigurationDir = new File(pathToWorkspace + File.separator + s);

            // otestuji, zda je to složka:
            if (ReadFile.existsDirectory(fileConfigurationDir.getAbsolutePath())) {
                /*
                 * V této část už bych měl mít jistotu, že existuje workspace a v něm adresář
                 * configuration, tak zavolám metodu, která otestuje, zda se v adresáři
                 * configuration vyskytují veškeré potřebné konfigurační soubory, pokud se
                 * nějaký z nich v příslušném adresáři nevyskytuje, pak se vytvoří s výchozím
                 * nastavením.
                 *
                 * Dále je třeba ještě otestovat, zda ty soubory obsahují validní hodnoty,
                 * protože i když ty soubory existují, uživatel mohl úmyslně zadat nějaké
                 * hodnoty, které nejsou validní, tak v případě, že ty hodnoty nejsou validní,
                 * pak se vytvoří ten soubor s výchozím nastavením.
                 */
                WriteToFile.checkConfigFiles();

                /*
                 * Výše se vytvořily (pokud neexistovaly) konfigurační soubory, tak je třeba nastavit uživatelské
                 * jméno do aplikace, aby se do logu zapsalo správné uživatelské jméno.
                 *
                 * (Nejspíše bude v souboru uloženo výchozí jméno příhlášeného uživatele v OS. Protože při vytvoření
                 * konfiguračních souborů se bere výchozí jméno (pokud se vytvořil nový konfigurační soubor s
                 * uživatelským jménem).)
                 */
                UserNameSupport.loadAndSetUserNameFromDefaultProp();

                // Zde jsem ve složce configuraion, tak si načtu
                // výchozí soubor: Default.properties
                // a zjistím si, zda se mám dotázat na workspace
                // nebo ne: (pokud tensoubor existuje:)

                final File fileDefaultPropertiesInWorkspace = new File(
                        fileConfigurationDir.getPath() + File.separator + Constants.DEFAULT_PROPERTIES_NAME);

                if (fileDefaultPropertiesInWorkspace.exists()) {
                    // NOTE: Zde to nemohu načíst přes metodu:
                    // readFile.getDefaultPropertiesInWorkspace(),
                    // protože nemám jistotu, že je to správná cesta
                    final Properties defaultProperties = App.READ_FILE
                            .getProperties(fileDefaultPropertiesInWorkspace.getPath());

                    if (defaultProperties != null) {
                        final boolean ask = Boolean
                                .parseBoolean(defaultProperties.getProperty("Display WorkspaceChooser",
                                        String.valueOf(Constants.DEFAULT_DISPLAY_WORKSPACE)));
                        /*
                         * Pokud bude ask true, pak se má zobrazit okno pro zadání adresáře workspace,
                         * jinak ne:
                         */
                        if (ask) {
                            /*
                             * Výše se mohl vytvořit konfigurační soubor s výchozími hodnotami, tak je třeba načíst a
                              * nastavit Look and Feel z příslušného souboru. Pokud si uživatel niže vybere
                              * workspace, kde je nastavena jiná hodnota, posléze se aplikuje příslušná hodnota z
                              * nově zvoleného workspace.
                             */
                            LookAndFeelSupport.setLookAndFeel(ReadFile.getDefaultPropAnyway());

                            // Nechám uživatele vybrat nebo potvrdit adresář workspace:
                            selectWorkspace();
                        }


                        // Zde se nemá zobrazit dotaz na workspace, tak se spustí aplikace:
                        else {
                            App.WRITE_TO_FILE.writeStartAppInfoToLog();

                            startMainApp();
                        }
                    }
                }

                else {
                    /*
                     * Tato část by také nikdy nastat neměla, protože soubor Default.properties by
                     * měl vždy v této části kódu existovat, protože pokud existuje adresář
                     * configuration, pak se zapíše do adresáře logs v confiruration záznam o
                     * spuštění aplikace, ale před samotným zápisem se aplikace zeptá na jazyk
                     * aplikace - pro texty aplikace, a při získávání textů v požadovaném jazyce se
                     * případně (pokud neexistuje), tak se vytvoří soubor Default.properties a
                     * využije se výchozí jazyk.
                     *
                     * V této části se řeší pouze to, že neexistuje výchozí soubor
                     * Default.properties, teorietikcy ba tato část nikdy neměla nastat, až na
                     * případy, kdy uživatel úmyslně z nějakého důvodu vymaže soubor
                     * Default.properties z adresáře configuration ve workspace, ale tak rychle, že
                     * se zapíšou logy a nestihne se otestovat ta existence Default.properties výše
                     * v kódu.
                     *
                     * Zde je možnost, vytvořit puze daný soubor Default.properties pomocí metody:
                     * 'App.writeToFile.writeDefaultPropertiesAllNew(Zde cesta do configuration a
                     * název Default.propertis);'.
                     *
                     * Ale v této části nemám jistotu, že je to jediný soubor, který chybí, tak pro
                     * jistotu vytvořím celý adresář configuration v adresáři označeném jako
                     * workspace, pokud již tento adresář (configration) existuje, pak bude přepsán
                     * se všemi soubory.
                     */

                    // Zde došlo k chybě - neexistuje ve workspace
                    // výchozí soubor s nastaveím, znovu to načtu:

                    // Ale nevím, zda je to jedniný soubor, který chybí,
                    // tak je zkopíruji raději všechny (abych je nemusel postupně
                    // zjišťovat)
                    WriteToFile.createConfigureFilesInPath(pathToWorkspace);

                    // Uložím cestu k workspace do soubouru:
                    App.WRITE_TO_FILE.changePathToWorkspace(pathToWorkspace);

                    /*
                     * Výše se vytvořily konfigurační soubory a nastavila cesta k workspace, tak je třeba nastavit
                     * uživatelské jméno do aplikace, aby se do logu zapsalo správné uživatelské jméno.
                     *
                     * (Nejspíše bude v souboru uloženo výchozí jméno příhlášeného uživatele v OS. Protože při
                     * vytvoření konfiguračních souborů se bere výchozí jméno.)
                     */
                    UserNameSupport.loadAndSetUserNameFromDefaultProp();

                    // Zapíšu start aplikace:
                    App.WRITE_TO_FILE.writeStartAppInfoToLog();

                    /*
                     * Výše se vytvořily konfigurační soubory, nebo alespoň "Default.properties", tak je třeba
                     * nastavit výchozí Look and Feel - buď výchozí nastavený v aplikaci nebo ve výše nastaveném
                     * workspace apod. Aby se zobrazil dialog pro nastavení workspace v požadovaném designu.
                     */
                    LookAndFeelSupport.setLookAndFeel(ReadFile.getDefaultPropAnyway());

                    // Dotaz na workspace:
                    selectWorkspace();
                }
            }

            else {
                // Tato část by nastat neměla! - nikdy nennastala, ale kdyby náhodou

                // Tato část by také neměla nastat, ze stejného důvodu jako výše,
                // když se načáti instance třídy ReadFile, tak se načte jazyk,
                // a ten testuje, zda existuje configration adresář ve Workspace a pokud ne,
                // tak jej vytvoří, takže zde by již měl exitsovat, proto se k této části
                // nedostanu,
                // ale kdyby se uživateli náhodou podařilo vymazat během té chvíle zae ten
                // adresář configuration
                // tak tato část zde být musí, protože je třeba jej znovu spustit, jinak by se
                // aplikace ukončila - nenačetla by se

                // Už by tam tedy měl existovat soubor Default.protpeties, který obsahuje
                // proměnnou,
                // že se má zeptat na workspace, takže bude podmínka výše splněna,
                // u dotazu na workspace při čtení tohoto souboru

                // Zde se v workspace nachází soubor configuration
                // ale nejedná se o složku, tak ji musím vytvořit:
                WriteToFile.createConfigureFilesInPath(pathToWorkspace);

                // Uložím cestu k workspace do soubouru:
                App.WRITE_TO_FILE.changePathToWorkspace(pathToWorkspace);

                /*
                 * Výše se vytvořily konfigurační soubory a nastavila cesta k workspace, tak je třeba nastavit
                 * uživatelské jméno do aplikace, aby se do logu zapsalo správné uživatelské jméno.
                 *
                 * (Nejspíše bude v souboru uloženo výchozí jméno příhlášeného uživatele v OS. Protože při vytvoření
                 * konfiguračních souborů se bere výchozí jméno.)
                 */
                UserNameSupport.loadAndSetUserNameFromDefaultProp();

                App.WRITE_TO_FILE.writeStartAppInfoToLog();

                /*
                 * Výše se vytvořily konfigurační soubory, tak je třeba nastavit výchozí Look and Feel. Buď to
                 * nastavené v aplikaci nebo načtené z workspace apod. Aby se níže zobrazil dialog pro nastavení
                 * adresáře workspace v aktuálním designu.
                 */
                LookAndFeelSupport.setLookAndFeel(ReadFile.getDefaultPropAnyway());

                /*
                 * V této častí se výše vytvořili nové konfigurační soubory, takže v Default.properties bude, že se
                 * aplikace má dotázat na workspace, tak se dotážu:
                 */
                selectWorkspace();
            }
        }
    }


    /**
     * Metoda, která zobrazí úvodní obrazovku s načítáním - SplassScreen a až doběhně progressBar, neboli v tomto
     * případě konkrétně, až se pomocí timeru pro spoždění napočítá do 100, tento SplashScreen se zavře a spustí se
     * hlavní okno aplikace pro práci.
     * <p>
     * Mezi tím se spustí vlákno, které otestuje, zda jsou k dispozici soubory, které aplikace potřebuji, aby mohla
     * kompilovat Javovské třídy. Pokud ne, pak je dle nastavených parametrů zkusí najít v adresáři, kde je
     * nainstalována Java nebo na celém disku.
     */
    private static void startMainApp() {
        /*
         * Zjistím si, zda se má při spuštění aplikace otestovat, zda je nastaven
         * adresář, kde mají být soubory, které aplikace potřebuji pro kompilování tříd
         * a zda ty soubory jsou obsaženy, když ne, pak se zkusí někde na disku (dle
         * nastavených možností) najít a nastavit do workspace.
         */
        if (getValueFromDefaultProp("Search compile files", Constants.SEARCH_COMPILE_FILES_AFTER_START_UP_APP))
            new CheckCompileFiles(App.PATH_TO_JAVA_HOME_DIR, false);

        /*
         * Načte se konfigurační soubor s infromacemi pro výchozí nastavení aplikace. Načtený soubor se předá do
         * metody pro nastavení "Look and Feel" aplikace.
         *
         * Navíc v případě, že se zobrazilo okno pro dotaz na změnu adresáře workspace a uživatel jej změnil, tak je
         * třeba načíst hodnotu nově zvoleného workspace. Jinak by se aplikovala ta původní výše načtená při nalezení
          * adresáře workspace.
         */
        LookAndFeelSupport.setLookAndFeel(ReadFile.getDefaultPropAnyway());

        // Spuštění "loading dialogu" a pak i načtení apikace:
        new SplashScreen();

        // Samotné spuštění aplikace bez úvodní obrazovky coby SplashScreen, ale nebude se hlidat otevreny
        // projekt, tak je třeba mít zavřený projekt pří zavření aplikace!
//        SwingUtilities.invokeLater(() -> new App().setVisible(true));
    }


    /**
     * Metoda, která si načte soubor Default.properties buď z adresáře workspace a pokud nebude načten, pak se načte z
     * aplikace ten výchozí soubor.
     * <p>
     * Pak se z něj vytáhne hodnota pod klíčem key, případně se využije výchozí hodnota - defaultValue.
     * <p>
     * Note: Jedná se pouze o logické hodnoty, které slouží buď pro zjištění, zda se má spustit vlákno, které otestuje,
     * zda je nastaven adresář se soubory, které aplikace potřebuji pro kompilaci tříd a ta druhá hodnota je pro načtení
     * toho, zda se má v případě potřeby nalezení těch souborů potřebných pro kompilování tříd prohledat celý disk
     * daného zařízení nebo jen rodičovksé soubory od adresáře, kde je nainstalována Java.
     *
     * @param key
     *         - klíč hodnoty v souboru .properties.
     * @param defaultValue
     *         - výchozí hodnota, která se využije, pokud nebude hodnota pod key načtena, například není v souboru
     *         uvedena apod.
     *
     * @return logickou hodnotu dle klíče key nebo výchozí hodnotu.
     */
    static boolean getValueFromDefaultProp(final String key, final boolean defaultValue) {
        final Properties prop = ReadFile.getDefaultPropAnyway();

        return Boolean.parseBoolean(prop.getProperty(key, String.valueOf(defaultValue)));
    }


    /**
	 * Metoda, která otevře dialog pro vyběr složky coby Workspace
	 * NOTE: pokud uživatel vybere, že nechce zobrazovat dialog pro vyber Workspacu a workspace se zmení nebo ještě není vytvoren
	 * nebude to uloženo a při dalším spuštění aplikace se na workspace äplikace znovu dotaže
	 *
	 * Postup:
	 * Otevře se dialog a vyberu si cestu, ta se nacte do promenne (pathToWorkspace) - (pokud uživatel nezruší okno):
	 * 		Jestliže Workspace existuje
	 * 			Ujistím se, že je to složka
	 * 				Zde je to adresář, tak změním cestu k Workspacu v Default.properties v adresáři aplikace
	 * 				Zjistím, zda se ve Workspacu vykytuje složka configuration:
	 * 					Pokud Ano, tak dále soubory již netestuji, počítám s tím, že uživatel soubory mazat nebude, a kdyby ano, lze je z koše obnovit
	 * 							nebo stačí smazat složku configuration a při spuštění aplikace se znovu vytvoří
	 * 					Pokud ne, tak ji vytvořím
	 * 		Jinak ho vytvořím a spustím aplikaci
	 * Pokud uživatel tento dialog zavře, tak se ukončí i aplikace.
	 */
    private static void selectWorkspace() {
        final WorkspaceForm wf = new WorkspaceForm();

        // Nastavení jazyka pro dialog pro výběr workspacu::
        wf.setLanguage(App.READ_FILE.getSelectLanguage());

        final InfoAboutWorkspace workspaceInfo = wf.chooseWorkspace();


        // Zde uživatel buzď zadal cestu nebo zavřel dialog, takže v tomto konkrétním
        // případě zavřu celou aplikaci:
        if (workspaceInfo == null)
            System.exit(0);


        // Zjistím si cestu k Workspacu - zadanou v dialogu:

        // Zde je zadána cesta do složky, kterou uživatel zvolil jako pracovní prostor -
        // Workspace (dále jen Workspace)
        // Otestuji, zda vůbec exitsuje, a jedná se o adresář:
        if (ReadFile.existsDirectory(workspaceInfo.getPathToWorkspace())) {

            // Zde vím, že uživatel vybral složku jako Workspace, tak uložím cestu k této složce pro další využití:
            App.WRITE_TO_FILE.changePathToWorkspace(workspaceInfo.getPathToWorkspace());

            // dále potřebuji zjistit, zda Workspace obsahuje složku configuration nebo ne,
            // Pokud ji obsahuje, tak již nebudu testovat ostatní soubory ve složce, počítám
            // s tím, že je uživatel nebude mazat
            // nebo stačí smazat celý adresář configuration, a při spuštění aplikace se do
            // složky Workspace znovu nakopírují
            final File fileConfiguration = new File(
                    workspaceInfo.getPathToWorkspace() + File.separator + Constants.CONFIGURATION_NAME);


            if (fileConfiguration.exists() && fileConfiguration.isDirectory()) {
                /*
                 * Zde si uživatel zvolil existující adresář pro workspace, tak je třeba zapsat do zápis spuštění
                 * aplikace do logu a nastavit Look and Feel protože se mohl otevřít dialog z důvodu, že workspace
                 * neexistuje a uživatel zvolil existující adresář. Nebo je jen nastaven dotaz, tak se zde zapíše
                 * zápis do logu, nastaví znovu ten samý Look and Feel a pokračuje se spuštěním aplikace.
                 *
                 * Zde se zvolil již existující workspace a konfigurační soubory existují, tak je třeba načíst jméno
                 * z konfiguračního souboru.
                 */
                UserNameSupport.loadAndSetUserNameFromDefaultProp();

                App.WRITE_TO_FILE.writeStartAppInfoToLog();

                /*
                 * Zde je třeba nastavit Look and Feel pro případy, když se zobrazil dialg kvůli neexistujícímu
                 * adresáři workspace. V takovém případě je třeba načíst Look and Feel z existujícího konfiguračního
                 * souboru, který právě uživatel zvolil v existujícím workspace.
                 */
                LookAndFeelSupport.setLookAndFeel(ReadFile.getDefaultPropAnyway());

                // Před tím než ji spustím, zda zapíšu, zda se má aplikace zeptat na workspace
                // při dalším spuštění
                App.WRITE_TO_FILE.setAskForWorkspaceValue(workspaceInfo.isAskForWorkspace());

                startMainApp();
            }

            else {
                // Zde složky configuration neexistuje, tak ji z hlavního adresářa aplikace
                // nakopíruji do složky zvolené jako Workspace:
                WriteToFile.createConfigureFilesInPath(workspaceInfo.getPathToWorkspace());

                /*
                 * Výše se vytvořily konfigurační soubory, tak je třeba nastavit uživatelské jméno do aplikace, aby
                 * se do logu zapsalo správné uživatelské jméno.
                 *
                 * (Nejspíše bude v souboru uloženo výchozí jméno příhlášeného uživatele v OS. Protože při vytvoření
                 * konfiguračních souborů se bere výchozí jméno.)
                 */
                UserNameSupport.loadAndSetUserNameFromDefaultProp();

                App.WRITE_TO_FILE.writeStartAppInfoToLog();

                // Jelikož jsem nyní pouze zkopíroval adresář, tak mu musím nastavit aktuální
                // hodnotu - zda se má při dalším
                // spuštění aplikace dotázat na workspace:
                App.WRITE_TO_FILE.setAskForWorkspaceValue(workspaceInfo.isAskForWorkspace());

                startMainApp();
            }
        }

        else {
            /*
             * Zde zvolený adresář neexistuje, tak je třeba ho vytvořit. Stejně tak složku configuration s výchozími
             * soubory, a pokud nenastane chyba, tak je "základ" hotový, tak je možné spustit aplikaci
             */
            WriteToFile.createConfigureFilesInPath(workspaceInfo.getPathToWorkspace());

            // Uložím cestu k workspace do soubouru:
            App.WRITE_TO_FILE.changePathToWorkspace(workspaceInfo.getPathToWorkspace());

            /*
             * Výše se vytvořily konfigurační soubory a nastavila cesta k workspace, tak je třeba nastavit
             * uživatelské jméno do aplikace, aby se do logu zapsalo správné uživatelské jméno.
             *
             * (Nejspíše bude v souboru uloženo výchozí jméno příhlášeného uživatele v OS. Protože při vytvoření
             * konfiguračních souborů se bere výchozí jméno.)
             */
            UserNameSupport.loadAndSetUserNameFromDefaultProp();

            App.WRITE_TO_FILE.writeStartAppInfoToLog();

            // Nastavím dotaz na workspace:
            App.WRITE_TO_FILE.setAskForWorkspaceValue(workspaceInfo.isAskForWorkspace());

            startMainApp();
        }
    }
}