package cz.uhk.fim.fimj.app;

import java.awt.BorderLayout;

import java.awt.Dimension;
import java.io.File;
import java.util.Properties;

import javax.swing.*;

import cz.uhk.fim.fimj.file.*;
import cz.uhk.fim.fimj.preferences.OpenedProjectPreferencesHelper;
import cz.uhk.fim.fimj.redirect_sysout.RedirectApp;
import org.fife.ui.rtextarea.RTextScrollPane;

import cz.uhk.fim.fimj.buttons_panel.ButtonsPanel;
import cz.uhk.fim.fimj.check_configuration_files.CheckingConfigurationFilesThread;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.commands_editor.Editor;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;
import cz.uhk.fim.fimj.instances.Instances;
import cz.uhk.fim.fimj.menu.Menu;
import cz.uhk.fim.fimj.output_editor.OutputEditor;
import cz.uhk.fim.fimj.preferences.PreferencesApi;

/**
 * Tato třída slouží jako hlavní okno aplikace, jsou zde inicializován většiny komponent důležitých pro aplikaci, jako
 * jsou například diagramy tříd a instancí, dále editory pro zadávánání Javovských příkazů (vybraných) a dále editor
 * výstupůu, dále menu a tlačítka pro zacházení s diagramem tříd, apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class App extends JFrame {

	private static final long serialVersionUID = 1902400361639885826L;

	
	
	/**
	 * Proměnná výčtového typu, která obsahuje typ aktuálně využívaného OS.
	 */
	public static final KindOfOS KIND_OF_OS = DetectOS.getKindOfOs();


	
	
	
	
	/**
	 * Komponenta pro čtení dat ze souborů a kompilaci tříd.
	 * 
	 * Tato komponenta či proměnná by mohla být umístěna asi v kterékoli třídě,
	 * stačí její instanci vytvořit pouze jednou a pak pouze volat její metody,
	 * proto jsem ji deklaroval v této části, název této třídy "App" je krátký a
	 * snadno se k této proměnné přistupuje, navíc se vytvoří téměř ihned po startu
	 * aplikace, takže bude pro ostatní části aplikace sodstupná.
	 * 
	 * Navíc tato třída musí existovat, protože se jedná o "hlavní" okno aplikace.
	 */
	public static final ReadFileInterface READ_FILE = new ReadFile();
	
	/**
	 * Komponenta pro zápis dat do souborů.
	 * 
	 * Tato komponenta či proměnná by mohla být umístěna asi v kterékoli třídě,
	 * stačí její instanci vytvořit pouze jednou a pak pouze volat její metody,
	 * proto jsem ji deklaroval v této části, název této třídy "App" je krátký a
	 * snadno se k této proměnné přistupuje, navíc se vytvoří téměř ihned po startu
	 * aplikace, takže bude pro ostatní části aplikace sodstupná.
	 * 
	 * Navíc tato třída musí existovat, protože se jedná o "hlavní" okno aplikace.
	 */
	public static final WriteToFilesInterface WRITE_TO_FILE = new WriteToFile();
	
	
	
	
	/**
	 * Proměnná, do které si při spuštění aplikace uložím cestu k adresáři
	 * (java.home), kde je nainstalována Java. (Jedná se o adresář na OS Win to je
	 * například 'C:/Program Files/Java/jre1.8.0_151', apod. pro ostatní OS.)
	 * 
	 * Tato cesta je potřeba, protože když chci restartovat aplikaci, pak musím znát
	 * tuto cestu, kterou potřebuje metoda restartApp, aby mohla aplikaci znovu
	 * spustit.
	 * 
	 * Tuto cestu bych mohl použít i pro kompilaci tříd ve třídě CompileClass pro
	 * kompilátor, ale uvedená cesta by nemusela obsahovat potřebné soubory (alespoň
	 * ne vššchny) a když je uživatel někde nastaví, pak si to aplikace bude
	 * paamtovat díky uložení do Preferencí, kdybych pokaždé nastavoval tuto cestu,
	 * pak by se aplikace pokaždé ptala na ty soubory, když by v tom adresáři
	 * nebyli.
	 */
	public static final String PATH_TO_JAVA_HOME_DIR = System.getProperty("java.home");
	
	
	
	
	/**
	 * Tato proměnná slouží pouze pro "dočasné" uložení hodnoty, resp. pozice
	 * posuvníku mezi diagramem tříd a diagramemm instancí, pro výpočty nové hodnoty
	 * při minimalizaci z maximalizace apod.
	 */
	private static int diagramsDividerLocation;
	
	
	
	
	
	
	/**
	 * Toto vlákno slouží pro monitorování změn v adresáři configuration ve
	 * workspace. Jde o to, že aby uživatel nezadal nějaká chybaná data do
	 * konfiguračních souborů apod. Prostě aby ty konfigurační soubory obsahovaly
	 * vždy potřebná validní data, tak to potřebuji nějak "hlídat", k tomu slouží
	 * toto vlákno.
	 */
	private static Thread monitoringConfigDirThread;
	
	
	
	
	
	
	
	
	
	/**
	 * Proměnná coby diagram tříd, kde se pracuje s třídami
	 */
	private static GraphClass classDiagram;

	/**
	 * Proměnná coby diagram instancí, kde jsou reprezentovány instance tříd z
	 * diagramu tříd a lze nad nimi volat metody a zobrazují se zde mezi instancemi
	 * různé vztahy, apod.
	 */
	private static GraphInstance instanceDiagram;

	/**
	 * Hlavní menu v hlavním okně aplikace - v tomto "okně"
	 */
	private static Menu menu;

	/**
	 * Panel tlačítek - pro manipulaci s třídami a vztahy mezi třídami v diagramu
	 * tříd
	 */
	private static ButtonsPanel pnlButtons;

	/**
	 * Editor příkazů pro zadávání různých příkazů, například pro vytváření instancí
	 * tříd z diagramu tříd a voláneí metod na instancí třídy, apod.
	 */
	private static Editor commandEditor;

	/**
	 * Editor pro výstupy z commandEditoru a kompilace
	 */
	private static OutputEditor outputEditor;

	/**
	 * Panel, která se vloží do "horní" části JsplitPane, ve kterém budou tlačítka a
	 * oba diagramy:
	 */
	private static JPanel pnlCenter;
	
	
	
	/**
	 * Tento JSplitPane je jako globální neboli instanční proměnná pouze pro to, že
	 * potřebuji nastavit při spuštění aplikace posuvník na střed a to se dělá až po
	 * metodě pack, protože ta resetuje nějak to nastavení JSplitPane. Takže abych
	 * mohl nastavit ten posuvník na požadouvanou pozici musím jej zavolat po té
	 * metodě pack, ale jen, když je tato promnná instanční / globální, pak k ní
	 * bude po metodě pack přístup.
	 */
	private static JSplitPane jspDiagrams;


    /**
     * Objekt properties, do kterého se uloží načtený soubor s texty pro aplikaci.
     */
	private final Properties languageProperties;

	
	
	
	
	/**
	 * Konstruktor této třídy.
	 */
	public App() {
		initGui();


		// Načtení jazyka, která se dále nastaví do komponent v aplikaci:
		languageProperties = READ_FILE.getSelectLanguage();

        /*
         * Nastavení textů pro chybové hlášky do třídy, kde jsou metoda pro práci s třídou "Desktop" - Otevření
         * průzkumníku souborů, otevření souboru ve výchozí aplikaci dle využívané platformy apod.
         */
        DesktopSupport.setLanguage(languageProperties);


        /*
         * V případě, že je aplikace spuštěna na OS Linux, spustí se vlákno, které otestuje, zda byla aplikace
         * restartována a nepodařilo e při restartu smazat vytvořený spouštěcí skript.
         */
        if (KIND_OF_OS == KindOfOS.LINUX_UNIX) {
            final DeleteLaunchScriptThread deleteLaunchScriptThread = new DeleteLaunchScriptThread();
            deleteLaunchScriptThread.setLanguage(languageProperties);

            deleteLaunchScriptThread.start();
        }


		
		
		
		// Nastavím texty pro tlačítka s Ano a ne v dialozícj JoptionPane (pro celou
		// aplikaci):
		setButtonsTextToJOptionPane(languageProperties);







		// OutputEditor pro výstupy kompilace a Command editoru:
		outputEditor = new OutputEditor(languageProperties);
		final JScrollPane jspOutputEditor = new JScrollPane(outputEditor);


		/*
		 * Vytvoření panelu s levým menu a JsplitPane s grafy: Následující metodu musím zavolat na tomto místeě,
		 * protože v té metodě je třeba reference na instanci editoru výstupů a pro editor příkazů je třeba reference
		 * na instanci diagramu instanci:
		 */
		createPnlCenter(languageProperties);
		
		
		Instances.setInstanceDiagram(instanceDiagram);
		
		
		
		// Instance Command editoru:
		commandEditor = new Editor(outputEditor, instanceDiagram, languageProperties, this);
		final RTextScrollPane rspEditor = new RTextScrollPane(commandEditor);

        /*
         * Nastavení výchozího textu v editoru výstupů je třeba provést zde, protože se výše v editoru příkazů
         * načetli hodnoty z konfiguračních souborů. Proto si editor výstupů může zjistit, zda li je povolena jedna z
         * historií příkazů a dle toho se vypíší příslušné zkratky pro uživatele.
         */
        outputEditor.clearEditor();



		// vytvořím panel, která bude obsahovat JsplitPane, kde budou command editor a
		// OutputEditor vedle sebe:
		final JPanel pnlEditors = new JPanel(new BorderLayout());

		final JSplitPane jspEditor = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, rspEditor, jspOutputEditor);
		jspEditor.setOneTouchExpandable(true);
		pnlEditors.add(jspEditor, BorderLayout.CENTER);
	
		
		
		
		

		

		
		
		
		
		
		// Jsplit pane s panelem, ve kterém jsou diagramy a leve menu se základními tlačítky a na druhé straně Jsplitpane 
		// je Command editor pro zadávání příkazů:
		final JSplitPane jspEditorAndDiagrams = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true, pnlCenter, pnlEditors);
		jspEditorAndDiagrams.setOneTouchExpandable(true);
		jspEditorAndDiagrams.setResizeWeight(0.75d);

		
		// Přidání hlavního JsplitPane do okna aplikace:
		add(jspEditorAndDiagrams, BorderLayout.CENTER);








        if (!OpenedProjectPreferencesHelper.warOpenedProjectsCreatedInPreferences()) {
            /*
             * Zde potřebuji otestovat, zda se v preferencích již nachází objekt (jednorozměrné pole) s objekty pro
             * otevřené projekty v aplikaci. Pokud ne, tj. jsem v této podmínce, pak je třeba vytvořit
             * alespoň úvodní - prázdné pole do preferencí.
             *
             * Protože kdyby se tak nestalo, tak když by se přistupovalo k tomu objektu (projektům) v preferencích a
             * ještě by neexistovaly, nastala vy výjimka, kterou
             * nelze zachytit.
             */

            // Nastavím / Vložím do preferencí prázdné pole pro otevřené objekty, aby se k nim již mohlo přistupovat.
            OpenedProjectPreferencesHelper.setEmptyOpenedProjectsToPreferences();

            // Nastavím si index pro to, žejiž mohu přistupovat k objektům - otevřeným projektům v preferencích.
            OpenedProjectPreferencesHelper.setOpenedProjectsCreatedToPreferences();
        }





		
		
		
		// Přidání menu do aplikace:
		menu = new Menu(classDiagram, instanceDiagram, this, commandEditor, outputEditor);
		setJMenuBar(menu);
		
		
		
		
		

		
		
		// Nastavení Jazyků komponent:
		// Jazyk do hlavního menu aplikace:
		menu.setLanguage(languageProperties);
		pnlButtons.setLanguage(languageProperties);
				
		
		
		
		
		
		
		// Přidám reakce na okno, konkrétně na zavřeni aplikace:
		addWindowListener(new AppWindowListener());






        /*
         * Nastavení přesměrování výpisů pomocí metod v System.out a System.err do okna výstupní terminál:
         */
        RedirectApp.prepareRedirects(languageProperties);






        // Události na posuvník mezi diagramy:
		addFrameStateListener();
		
		
		
		
		
		// Spuštění vlákna pro hlídání editace konfiguračních souborů:
		launchCheckingMonitoringConfigFiles(languageProperties);
		
		
		
		
		// Přizpůsobení okna komponentám, a zarovnání okna na střed obrazovky:
		pack();
		setLocationRelativeTo(null);

		/*
		 * Po tom, co se zavolá metoda pack je třeba znovu nastavit posuvník takm, kam
		 * chci, protože ta metoda pack s tím něco provede, asi nějaký restart či co,
		 * tak jej musím znovu nastavit.
		 */
		jspDiagrams.setDividerLocation(0.5d);
		jspEditor.setDividerLocation(0.5d);
		// Tento posuvník mezi diagramy se nemá zvětšovat dle velikosti okna:
//		jspEditorAndDiagrams.setDividerLocation(0.8d);
	}
	
	
	

	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří instanci panlu "pnlCenter", ve kterém bude panl s
	 * tlačítka - levé menu, dále JsplitPane, kde budou oba grafy classDiagram na
	 * jedné a InstanceDiagram na druhé
	 * 
	 * Metoda dále vytvoří i instance pnlButtons - "leve menu s tlačítky" a zrovna
	 * ho přidá do tohoto panelu
	 * 
	 * Dále vytvoři instance obou grafů a Jsplit pane, do ní tyto grafy přidá, a
	 * JsplitPane přidá do panelu
	 * 
	 * @param languageProperties
	 *            - objekt Properties, který obsahuje texty pro aplikace ve zvoleném
	 *            jazyce.
	 */
	private static void createPnlCenter(final Properties languageProperties) {
		pnlCenter = new JPanel();
		
		pnlCenter.setLayout(new BorderLayout());		
		
		// Vytvoření instancí grafů				
		instanceDiagram = new GraphInstance(outputEditor);
		classDiagram = new GraphClass(outputEditor, instanceDiagram);
		
		
		classDiagram.setLanguage(languageProperties);
		instanceDiagram.setLanguage(languageProperties);
		
		
		final JScrollPane jspClassDiagram = new JScrollPane(classDiagram);
		final JScrollPane jspInstanceDiagram = new JScrollPane(instanceDiagram);
		
		
		
		
		// Nastaveni ohraničení s titulky:
		if (languageProperties != null) {
			jspClassDiagram.setBorder(BorderFactory.createTitledBorder(languageProperties.getProperty("CdBorderTitle", Constants.CD_BORDER_TITLE)));
			jspInstanceDiagram.setBorder(BorderFactory.createTitledBorder(languageProperties.getProperty("IdBorderTitle", Constants.ID_BORDER_TITLE)));
		}
		
		else {
			jspClassDiagram.setBorder(BorderFactory.createTitledBorder(Constants.CD_BORDER_TITLE));
			jspInstanceDiagram.setBorder(BorderFactory.createTitledBorder(Constants.ID_BORDER_TITLE));
		}
		
		// Instance "levého" panelu s tlačítky pro základni ovládání class diagramu:
		pnlButtons = new ButtonsPanel(classDiagram);
		
		
		// Výše je vytvořena instance třída ButtonsPanel, tak tuto referenci mohu
		// "nasetovat" do třídy GraphClass, coby
		// diagram tříd, aby se po vytvoření vztahu mohli odznačit tlačitka, tj. aby
		// nevypadaly jako stisknuté:
		GraphClass.setButtonsPanel(pnlButtons);

		
		// Přidání do pnlCenter - okna aplikace:
		pnlCenter.add(pnlButtons, BorderLayout.WEST);

		// Instance JsplitPane s grafy:
		jspDiagrams = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, jspClassDiagram, jspInstanceDiagram);
		
		// přidám šipky pro plné roztažení:
		jspDiagrams.setOneTouchExpandable(true);

		/*
		 * Při změne posuvníku se uloží jeho aktuálí pozice v okně, aby se při přechodu
		 * z maximalizované velikosti okna mohl nastavit posuvník do "původní" pozice:
		 */
		jspDiagrams.addPropertyChangeListener(event -> diagramsDividerLocation = jspDiagrams.getDividerLocation());

		// Přidání grafů do panelu:
		pnlCenter.add(jspDiagrams, BorderLayout.CENTER);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda je otevřen nějaký projekt, resp. zda se tato
	 * aplikace zavřela, uživatel jí zavřel ale projekt nějaký projekt zůstal
	 * otevřený, tj. projekt v aplikaci uživatel nezavřel, a pokud tomu tak je, tak
	 * si pomocí preferencí načtu cestu k otevřenému projektu, tak akorát otestuji,
	 * zda tento adresář, resp. cesta k otevřenému projektu ještě existuje a pokud
	 * ano, tak ho rovnou otevřu,k a pokud již neexistuje, tak senic neděje, jen
	 * nastavím místo té cesty k projektu, tak nastavím na zavřený projekt v
	 * preferencích, a aplikace bude dále pokračovat v načtení akorát bez otevřeného
	 * projektu.
	 * 
	 * @return metoda vrátí logickou hodnotu dle toho, zda se buď projekt otevřel
	 *         nebo ne, true, pokud se projekt otevřel, pak sje třeba "zpřístupnit"
	 *         některá tlačítka, jinak false, pokud se projekt neotevřel (důvody v
	 *         tělě metody)
	 */
	static boolean checkOpenProject() {
		final String openProject = PreferencesApi.getPreferencesValue(PreferencesApi.OPEN_PROJECT, null);
		
		if (openProject != null) {
			// Zde se podařilo načíst nějakou hodnotu, tak otestuji, zda je to closed, a pokud ne, tak otestuji,
			// zda je to cesta k adresáři projektu a ještě ten projekt existuje:
			if (!openProject.equals(Constants.CLOSED_PROJECT)) {
				// Zde se nejedná o zavřený projekt, a není null, tak už zbývá pouze otestovat,
				// zda je to cesta k adresáři a ten existuje:
				
				if (ReadFile.existsDirectory(openProject)) {
					// zde je to adresář a exituje, tak mohu otevřít příslušný projekt:
					menu.openProjectInApp(openProject);
					
					// Zde se otevřel projekt, tak mohu vrátit true, jako, že je projekt otevřen,
					// a mohou ze zpřístupnit tlačítka:
					return true;
				}
				// else - zde se buď nejedná o cestu k adresáři nebo soubor na zadané cestě již neexistuje,
				// ať tak či tak, nastaví se hodnota v preferencích na zavřeno, protože už žádný projekt
				// otevřít nelze - není jaký
			}
			// else - zde se v preferencích vyskytuje hodnota zavřeno, tak není co otevírat
		}
		// Zde se vrátila null hodnota, to by ale neměla, tak nastavím projekt jako zavřený:
		PreferencesApi.setPreferencesValue(PreferencesApi.OPEN_PROJECT, Constants.CLOSED_PROJECT);
		
		// vrátím false, jako, že se žádný projekt neotevřel
		return false;
	}
	
	
	

	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro přidání listeneru pro změnu velikosti okna.
	 * 
	 * Pokud se okno maximalizuje, pak se nastaví posuvník na střed diagramů.
	 * 
	 * 
	 * Pokud se okno z maximalizované velikosti vrátí do původní pozice, kterou mělo
	 * před maximalizací okna, pak:
	 * 
	 * - Pokud byl posuvník úplně vlevo nebo v pravo, pak se nastaví na střed, jinak
	 * pokud byl někde mezi, pak se přepočítá na procentuální hodnotu mezi 0 a 1 a
	 * ta se nastaví jako aktuální pozice posuvníku v okně.
	 */
	private void addFrameStateListener() {
		/*
		 * Přidání listeneru, který "hlídá", když se okno maximalizuje, pak je vhodné
		 * nastavit posuvník na střed okna - tedy na střed mezi diagramem tříd a
		 * diagramem instancí.
		 * 
		 * Zdroj:
		 * http://www.java2s.com/Code/Java/Swing-JFC/DeterminingWhenaFrameorWindowIsIconizedorMaximized.htm
		 */
		addWindowStateListener(e -> {
			final int oldState = e.getOldState();
			final int newState = e.getNewState();

			if ((oldState & MAXIMIZED_BOTH) == 0 && (newState & MAXIMIZED_BOTH) != 0) {
				/*
				 * Z nějakého důvodu je potřeba nejprve "aktualizovat" komponenty a pak až mohu
				 * nastavit ten posuvník na požadovanou pozici.
				 */
				revalidate();

				// Nastavím posuvník na střed:
				jspDiagrams.setDividerLocation(0.5d);

				// Uložím si pozici:
				diagramsDividerLocation = jspDiagrams.getDividerLocation();
			}






			/*
			 * Zde testuji, zda se okno změnilo z maximalizované velikosti zpět do té
			 * původní.
			 *
			 * Pokud ano, pak provedu následující nastavení posuvníku mezi diagramu tříd a
			 * instancí:
			 *
			 * - Pokud je posuvník úplně vlevo nebo úplně vpravo, pak jej nastavím na střed.
			 *
			 * - Jinak, pokud je posuvník někde mezi, tak se vypočítá procetuální umístění
			 * toho posuvníku s aktuální šířkou té komponenty JSplitPane od 0 do 1 a tato
			 * hodnota se nastaví jako umístění posuvníku.
			 */
			if (newState == NORMAL) {
				// Pokud je nastavení posuvníku úplně vlevo nebo úplně vpravo, pak jej nastavím
				// na střed.
				if (diagramsDividerLocation == jspDiagrams.getMinimumDividerLocation()
						|| diagramsDividerLocation == jspDiagrams.getMaximumDividerLocation()) {
					/*
					 * revalidate kvůil tomu, aby se brali ty aktuální hodnoty - dle aktuální již ne
					 * maximalizované ale té zmenšené velikosti okna a dle toho se nastavi posuvník
					 * na střed.
					 */
					revalidate();

					// Nastavení posuvníku na střed:
					jspDiagrams.setDividerLocation(0.5d);
				}


				else {
					/*
					 * Zde je posuvník někde mezi, tj. není úplně vpravo, ani úplně vlevo. Tak
					 * otestuji, zda je pozice posuvníku větší než 1 (to by mělo nastat vždy, jen
					 * kdyby náhodou), pak vypočítám procentuální pozici tak, že zjistím aktuální
					 * umístění posuvníku a vydělím jej aktuální šířkou komponenty JSplitPane (berou
					 * se hodnoty ještě ze zvětšeného okna), pak zavolám metodu revalidate, která
					 * aktualizuje velikosti komponent a pak nastavím posuvník na tu vypočtenou
					 * procentuální pozici již ve zmenšeném okně.
					 */
					if (diagramsDividerLocation > 1) {
						/*
						 * Procentuální umístění posuvníku v okně:
						 */
						final double dob = (double) diagramsDividerLocation
								/ jspDiagrams.getMaximumDividerLocation();

						/*
						 * Aktualizuji komponentu JSplitPane, aby se aplikovalo nastavení umístění
						 * posuvníku na "aktuální rozměrny změnšeného okna.
						 */
						revalidate();

						/*
						 * Zde potřebuji ještě otestovat, , zda je nově vypočtená pozice větší než 1,
						 * proto to může nastat například pokud uživatel využije šipky pro "plné
						 * zvětšení" jednoho z oken v JSplitPane, pak musím nastavi tnějakou výchozí
						 * hodnotu, protože je třeba nastavovat pouze hodnoty mezi 0 a jedničkou
						 * (včetně).
						 */
						if (dob > 1d)
							jspDiagrams.setDividerLocation(0.5d);

						// Zde není hodnota větší než 1, tak mohu nastavit posuuvník na vypočítanou
						// pozici.
						else
							jspDiagrams.setDividerLocation(dob);
					}

					else {
						/*
						 * Tato část může nastat v případě, že se klikne na šipku doleva, která ten
						 * posuvník posune úplně doleva, pak bude hodnota 1 v proměnné
						 * diagramsDividerLocation, tak nastavím posuvník na půku - do středu okna
						 * diagramů.
						 */
						revalidate();
						jspDiagrams.setDividerLocation(0.5d);
					}
				}
			}
		});
	}
	
	
	
	

	

	
	
	/**
	 * Metoda, která zpřístupní / znepřístupní některá tlačítka - dle toho, zda je
	 * nějaký projekt otevřen nebo ne:
	 * 
	 * @param enable
	 *            - Logická proměnná, do které vložím true nebo false, dle toho, zda
	 *            chci tlačítka zpřístupnit nebo ne
	 */
	public static void enableButtons(final boolean enable) {
		// Povolí / Zakážou se tlačítka v menu:
		Menu.enableMenuItems(enable);
		
		// Povolí / Zakážou se tlačítka v "levém menu" (toolbaru)
		pnlButtons.enableButtons(enable);
		
		classDiagram.setEnabled(enable);
		
		instanceDiagram.setEnabled(enable);
		
		commandEditor.setEnabled(enable);
		
		// Editor výstupů bych mohl nechat zpřístupněn, to je otázka?
		outputEditor.setEnabled(enable);
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která se volá před ("řádným") ukončením aplikaci, tj. například po
	 * kliknutí na křížek vpravo nahoře v okně aplikace, nebo například pomocí
	 * klávesy Alt + F4, nebo pomocí tlačítka "Zavřít" v menu v okně aplikace apod.
	 * 
	 * Tato metoda uloží aktuální diagram tříd(pokud je otevřen projekt), dále
	 * otestuje, zda je otevřen projekt a zda ještě existuje tak jej ponechá v
	 * Preferences, jinak jej nastaví jako zavřený a na konec zapíše do logu
	 * informaci ohledně ukončení aplikace (tato část již není nijak extra potřebná,
	 * spíše pro předhlednost apod.).
	 */
	public static void saveDataBeforeCloseApp() {
		// Uložím aktuální diagram tříd:
		WRITE_TO_FILE.saveClassDiagram(GraphClass.getCellsList());
		
		// Zavolám metodu, která otestuje, zda se má zavřít projekt nebo uložit, resp. ponechat
		// otevřený projekt - pokud to lze:
		Menu.setOpenProjectInPreferences();
		
		// Zapíšu do příslušného souboru v logs v configuration ve Workspace,
		// ukončení aplikace - toto už není potřebna, ale pro přehlednost:
		WRITE_TO_FILE.writeExitAppInfoToLog();
	}








    /**
     * Metoda, která slouží pro vykonání procesu restartování aplikace.
     */
    public void restartApp() {
        final RestartApi restartApi = new RestartApi();
        restartApi.setLanguage(languageProperties);

        restartApi.restart(KIND_OF_OS, this);
    }



	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro nastavení toho, že se budou ve všech dialozích
	 * JOptionPane, kde budou tlačítka YES_OPTION a nebo NO_OPTION, tak pro tyto
	 * tlačítka se aplikují předdefinované texty.
	 * 
	 * Zdroj:
	 * https://stackoverflow.com/questions/14407804/how-to-change-the-default-text-of-buttons-in-joptionpane-showinputdialog
	 * 
	 * Info pro tlačítka:
	 * https://stackoverflow.com/questions/1951558/list-of-java-swing-ui-properties
	 * 
	 * @param languageProperties
	 *            - objekt Properties, který obsahuje texty ve zvoleném jazyce.
	 */
    private static void setButtonsTextToJOptionPane(final Properties languageProperties) {
        /*
         * Texty, do kterých nastaví texty pro tlačítka s Ano a Ne v příslušném jazyce.
         */
        final String txtYesButton;
        final String txtNoButton;

        if (languageProperties != null) {
            txtYesButton = languageProperties.getProperty("JopTxtYesButton", Constants.JOP_TXT_YES_BUTTON);
            txtNoButton = languageProperties.getProperty("JopTxtNoButton", Constants.JOP_TXT_NO_BUTTON);
        }

        else {
            txtYesButton = Constants.JOP_TXT_YES_BUTTON;
            txtNoButton = Constants.JOP_TXT_NO_BUTTON;
        }

        /*
         * Nastavím pro veškeré komponenty JOptionPane pro příslušná tlačítka výše
         * získané texty.
         */
        UIManager.put("OptionPane.yesButtonText", txtYesButton);
        UIManager.put("OptionPane.noButtonText", txtNoButton);
    }
	
	
	
	

	
	
	
	/**
	 * Metoda, která slouží pro spuštění vlákna, které bude neusále monitorovat
	 * změny v příslušném adresáři configuration ve workspace, aby uživatel nezadal
	 * nějaké chybné hodnoty do konfiguračních souborů.
	 * 
	 * @param languageProperties
	 *            - objekt Properties, který obsahuje texty v uživatelem zvoleném
	 *            jazyce.
	 */
	public static void launchCheckingMonitoringConfigFiles(final Properties languageProperties) {
		final String path = ReadFile.getPathToWorkspace();

		if (path == null)
			return;


		/*
		 * V případě, že to vlákno již bylo vytvořeno a běží, pak jej zkusím ukončit,
		 * ale snad pokaždé nastavení příslušná výjimky InterruptedException (kvůli
		 * Thread.Sleep), takže jej zachytím, v té výjimce opravdu ukončím a pak mohu
		 * spustit to nové vlákno nad aktuálním adresářem.
		 */
		if (monitoringConfigDirThread != null && monitoringConfigDirThread.isAlive())
			monitoringConfigDirThread.interrupt();
			
		
		/*
		 * Konfigurační soubory se nachází v adresáři configuration ve workspace, tak si
		 * zjistím, zda ten adresář configuration existuje, ale vytvářet jej zde nebudu,
		 * protože už by měl být vytvořený.
		 */
		final String pathToConfigDir = path + File.separator + Constants.CONFIGURATION_NAME;

		if (ReadFile.existsDirectory(pathToConfigDir)) {
			monitoringConfigDirThread = new CheckingConfigurationFilesThread(new File(pathToConfigDir),
					languageProperties);
			
			monitoringConfigDirThread.start();
		}
	}
	
	

	
	

	/**
	 * Metoda, která se zavolá při vytvoření instance této třídy Metoda, nastaveí
	 * tomuto oknu - hlavní okno aplikace výchozí "design" - velikost, ikona,
	 * layout, ...
	 */
	private void initGui() {
		setTitle(Constants.APP_TITLE);
		setPreferredSize(new Dimension(1100, 700));
		setLayout(new BorderLayout());
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setIconImage(GetIconInterface.getMyIcon(Constants.PATH_TO_APP_ICON, true).getImage());
	}







	/**
	 * Getr na editor příkazů.
	 *
	 * @return referenci na komponentu editor příkazů.
	 */
	public static Editor getCommandEditor() {
		return commandEditor;
	}
}