package cz.uhk.fim.fimj.app;

/**
 * Tato třída slouží pouze pro uložení informací o Workspacu, konkrétně cesta k adresáři, který bude pro aplikaci jako
 * Workspace - pracovní prostor, kde budou ukládány projekty a logická hodnota o tom, zda se má při příštím spuštění
 * aplikace dotázat na umístění Workspacu (v pricipu podobné jako u Eclipse)
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class InfoAboutWorkspace {

    /**
     * Logická hodnota o tom, zda se má aplikace při příštím spuštění dotázat na umístění Workspacu nebo ne:
     */
    private final boolean askForWorkspace;


    /**
     * Proměná coby cesta k adresáři, který slouží jako Workspace:
     */
    private final String pathToWorkspace;


    /**
     * Tento konstruktor slouží pro vytvoření instance této třídy, ale také hlavně pro naplnění výše zmíněných hodnot.
     *
     * @param pathToWorkspace
     *         - cesta k Workspace
     * @param askForWorkspace
     *         - logická hodnota o tom, zda se má dotázat na Workspace při příštím spučtění
     */
    public InfoAboutWorkspace(final String pathToWorkspace, final boolean askForWorkspace) {
        super();

        this.askForWorkspace = askForWorkspace;

        this.pathToWorkspace = pathToWorkspace;
    }


    public final boolean isAskForWorkspace() {
        return askForWorkspace;
    }


    public final String getPathToWorkspace() {
        return pathToWorkspace;
    }
}