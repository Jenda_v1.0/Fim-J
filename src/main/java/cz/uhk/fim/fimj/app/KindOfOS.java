package cz.uhk.fim.fimj.app;

/**
 * Tento výčet slouží pro "definování" typu OS, na kterém běží tato aplikace.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public enum KindOfOS {

	WINDOWS, MAC, LINUX_UNIX, SOLARIS, OTHER
}
