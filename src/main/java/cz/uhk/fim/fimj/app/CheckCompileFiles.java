package cz.uhk.fim.fimj.app;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.file.WriteToFile;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.preferences.PreferencesApi;
import cz.uhk.fim.fimj.swing_worker.SwingWorkerInterface;

/**
 * Tato třída slouží pouze jako vlákno, které slouží pro testování existence
 * souborů, které jsou potřeba pro to, aby bylo možné pomocí aplikace, resp. aby
 * aplikace mohla kompilovat Javovské třídy v projekterch této aplikace.
 * 
 * Toto vlákno v principu slouží pro otestování, zda je zadán adresář, kde se
 * nachází soubory, potřebné pro kompilaci.
 * 
 * Pokud ještě není nastavena cesta k adresáři, kde mají být ty soubory, pak se
 * zkusí najít v adresáři s naintalovanou Javou na daném zařízení a zkopírovat
 * do adresáře workspace/configuration/libTools/lib/Zde budou ty soubory.
 * 
 * Pro zjednodušení jsem toto vlákno napsal tak, že akorát stačí vytvořit
 * instanci a v kontruktoru se již vlákno spustí, a začne vše testovat.
 * 
 * 
 * 
 * Note: Doplnil jsem zde možnost, že se může prohledat celý disk, aby se našli
 * potřebné soubory, ta ale není doporučena, protože se může stát, že se najde
 * soubor s potřebnám názvem dle listu COMPILE_FILES_LIST ale bude to jiný
 * soubor, než který je potřebna pro kompilování tříd v aplikaci.
 * 
 * Ale to může nastat pouze v případě, že tak uživatel nastaví v dialogu
 * nastavení, jinak ve výchozím nastavení je tato funkce vypnuta. Proto se
 * prohledávají pouze od získaného adresáře směrem nahoru, vždy se v získaném
 * adreáři zkusí najít adresář lib a v něm ty soubory ve zmíněném listu.
 * 
 * Například:
 * 
 * Toto bude výchozí nalezená cesta k adresáři Java.home: C:\Program
 * Files\Java\jdk1.8.0_144\
 * 
 * C:\Program Files\Java\jdk1.8.0_144\lib -> toto se k tomu pripoji a v něm se
 * zkusí najít ty soubory v listu výše.
 * 
 * Pak: C:\Program Files\Java\lib -> se jde o úroveň výše a v něm se hledá
 * adresář lib a v něm ty soubory v listu výše, a takto to pokračuje dokud to
 * jde, akorát při úrovni výše se prohledávají všechny adresáře ve snaze k nim
 * připojit adresář lib, ne pouze ten jeden jak je výše uvedeno na příkladu
 * (pouze pro zjednodušení ukázky).
 * 
 * 
 * Note:
 * Tato třída ještě implementuje rozhraní MySwingWorkerInterface, jen kvůli
 * tomu, protože jsem do menu v hlavním okně aplikace doplnil možnost, že si
 * uživatel může sám zvolit, že chce vyhledat ty soubory, aby je nemusel
 * nastavovat, třeba, když nějaké soubory doplnil apod. Tak aby si nedával práci
 * s nastavováním. Jinak toto rozhraní MySwingWorkerInterface je třeba
 * implementovat, protože pokud tak uživatel zvolí v menu aplikace, že se to
 * vlákno spustí tam, tak neběží jako "běžné vlákno", ale běží ve swingWorkeru,
 * aby se zobrazil i loading dialogu.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class CheckCompileFiles extends Thread implements SwingWorkerInterface, LanguageInterface {

	/**
	 * List, který obsahuje názvy souborů, které jsou potřebné ke kompilaci a které
	 * "hledá" tato třída.
	 * 
	 * Stačí pouze doplnit názvy souborů, které by se měly nacházet v adresářích lib
	 * v adresářich s naintalovanou Javou, které se nachází v adresáři Java.
	 */
	private static final List<String> COMPILE_FILES_LIST = Collections
			.unmodifiableList(Arrays.asList(Constants.COMPILER_FILE_TOOLS_JAR, Constants.COMPILER_FILE_CURRENCY_DATA,
					Constants.COMPILER_FILE_FLAVORMAP_PROPERTIES));
	
	
	/**
	 * Adresář, kde je nainstalovaná Java, která se aktuálně používá /jre
	 * (nejpsíše).
	 */
	private final String javaHome;
	
	
	
	/**
	 * List, do kterého se budou vkládat cesty k těm třem souborům, až se najdou.
	 * Mohlo by to být pole nebo 3 proměnné, resp. jedna proměnná pro každý z těch 3
	 * souborů.
	 * 
	 * Hlavně by to mohly být ty 3 proměnné - jedna pro každý soubor, pak by stačilo
	 * jen testovat, zda není příslušná proměnná null, místo toho, abych procházel
	 * celý list, ale pro případ, že bych v budoucnu potřeboval více souborů, pak je
	 * jen stačí doplnit do listu COMPILE_FILES_LIST, tak nechám tento list.
	 */
	private final List<File> filesList;
	
	
	
	/**
	 * Proměnná typu Semaphore, jedná se o tzv. Mutext, která je implementován jako
	 * Semaphore, akorát s jedním vláknem, které může přistupovat k danému souboru.
	 * 
	 * Pouzžívám jej zde, protože je možné, že budu kopírovat soubory, které se
	 * možná najdou na zařízení, kde běží tato aplikace, tak pokud se najdou
	 * soubory, které aplikace potřebuje, aby mohla kompilovat Javovské třídy, tak
	 * se zkopírují do adresáře workspace/configuration/libTools/lib, tak aby je
	 * nikdo jiný nevyužíval - jiné vlákno, dokud se potenciální kopírování
	 * nedokončí.
	 */
	private final Semaphore semaphoreMutex;
	
	
	
	
	/**
	 * Proměnná o tom, zda se má po doběhnutí "algoritmů" pro testování existence
	 * souborů vypsat oznámení ohledně výsledku, například, že se nepodařilo nalézt
	 * soubory, nebo že se to naopak podařilo, nebo že už je to OK a není co řešit
	 * apod.
	 * 
	 * Ale v podstatě v tomto vlákně značí pouze to, zda se mají načíst texty do
	 * proměnných ve zvoleném jazyce, aby se ten postup "trochu urychlil", protože
	 * to uživatel uvidí, jako že se něco načítá. Hlášky se pak vypíšou v metodě
	 * showResults nehledě na tuto proměnnou.
	 */
	private final boolean showMessage;
	
	
	
	
	
	/**
	 * Logické proměnné, které jsou potřeba pouze pro "informační výpisy" v případě,
	 * že toto vlákno volá uživatel pomocí aplikace, aby se našli příslušné soubory
	 * pro kompilování tříd.
	 * 
	 * Proměnná listed slouží pro to, aby se zjistílo, v jaké část se má vypsat
	 * hláška, tj. zda se má vypsat hláška, že není třeba soubory hledat, už jsou
	 * nastaveny nebo, pokud je false, pak se ještě testuje, zda se má nějaká hláška
	 * vypsat a dle proměnné result se vypíše buď že se soubory našly a zkopírovaly
	 * na příslušné místo ve workspace nebo, že nebyly soubory (alespoň jeden
	 * soubor) nalezen.
	 */
	private boolean listed = false, result = false;
	
	
	
	/**
	 * Tato proměnná je zde jako globální, resp. instanční proměnná pouze z toho
	 * důvodu, aby se v případě potřeby vypsala cesta uživateli, že se do
	 * příslušného adresáře zkopírovaly potřebné soubory apod. Jinak zde není
	 * potřeba.
	 */
	private String pathToCompilerDir;
	
	
	
	
	
	/**
	 * Proměnné pro texty pro hlášky pro oznámení ohledně výsledků kopírování či nalezení souborů ve zvoleném jazyce.
	 * 
	 * Naplní se pouze v případě, že se na konec mají hlášky vypsat.
	 */
	private static String txtFilesAlreadyExistText, txtFilesAlreadyExistTitle, txtFilesSuccessfullyCopiedText,
			txtFilesSuccessfullyCopiedTitle, txtFilesNotFoundText, txtFilesNotFoundTitle;
	
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy, který spustí toto vlákno.
	 * 
	 * @param pathToJavahome
	 *            - cesta k adresáři, kde je nainstalovaná Java na zařízení, kde
	 *            běží tato aplikace, raději je zde předána tato cesta k adresáři v
	 *            tomto konstruktoru, protože, kdybych se v budoucnu rozhodl, že se
	 *            to vlákno bude spouštět ještě někde jinde než po staru aplikace,
	 *            pak si tuto cestu k adresáři raději předám, protože je možné, že
	 *            se u kompilátoru nastaví cesta někam jinam, tak si ve třídě App
	 *            držím tu původní cestu, než si jej aplikace za běhu změní.
	 * 
	 * @param showMessage
	 *            - logická proměnná o tom, zda se má po proběhnutí vlákna vypsat
	 *            hlášení ohledně výsledku vlákna, tj. zda se podařilo soubory najít
	 *            a zkoprírovat do workspace nebo ne Tato proměnná je zde pouze pro
	 *            to, aby se případně načetly "dopředu" texty pro výsledné oznámení.
	 */
	public CheckCompileFiles(final String pathToJavahome, final boolean showMessage) {
		super();
		
		this.showMessage = showMessage;
		
		javaHome = pathToJavahome;
		filesList = new ArrayList<>(COMPILE_FILES_LIST.size());
		
		semaphoreMutex = new Semaphore(1);
		
		start();
	}
	
	
	
	
	
	
	
	
	@Override
	public void run() {
		/*
		 * Otestuji, zda se mají vypsat nějaká hlášení a pokud ano, naplním si proměnné
		 * příslušnými texty ve zvoleném jazyce.
		 * 
		 * Note:
		 * Toto načtení souborů by ohlo být vmetodě showResults, ale aby to pak
		 * nevypadalo, že se to seklo apod. Tak si je načtu zde "na pozadí" a pak se
		 * rovnou zobrazí hláška.
		 */
		if (showMessage)
			setLanguage(App.READ_FILE.getSelectLanguage());
			
		
		/*
		 * Postup:
		 * 
		 * Načtu si z Preferencí adresář, který označil uživatel jako adresář, kde se
		 * nachází soubory pro kompilaci, pokud adresář existuje a obsahuje všechny
		 * soubory, potřebné pro kompilaci, pak mohu skončit vlákno. Vše je OK. Ale
		 * pokud adresář neexistuje, nebo alespoň jeden z těch souborů chybí, pak musím
		 * pokračovat:
		 * 
		 * 
		 * - Takže zavolám metodu createCompilerDir, která zjistí, zda existuje
		 * workspace, jinak mohu skončit, pokud existuje, pak i když v něm neexistuje
		 * adresář configuration, pak jej vytvořím a v něm adresář libTools, ten by se
		 * vytvořil pokaždé, už nebudu testovat, zda existuje nebo ne, protože se mohlo
		 * jednat o adresář, který již neobsahuje alespoň jeden z těch souborů.
		 * 
		 */
		pathToCompilerDir = PreferencesApi.getPreferencesValue(PreferencesApi.COMPILER_DIR, null);

		/*
		 * Pokud jsem nenačetl cestu k adresáři, kde se mají nacházet ty soubory potřené
		 * pro kompilaci tříd v aplikaci, pak pokračuji metodou, createCompilerDir,
		 * která ty soubory zkusí najít a zkopírovat je do workspace.
		 */
		if (pathToCompilerDir == null)
			result = createCompilerDir();

		/*
		 * Pokud neexistuje adresář adresář, kde se mají nacházet ty soubory potřené pro
		 * kompilaci tříd v aplikaci, pak pokračuji metodou, createCompilerDir, která ty
		 * soubory zkusí najít a zkopírovat je do workspace.
		 */
		else if (!ReadFile.existsDirectory(pathToCompilerDir))
			result = createCompilerDir();
		
		
		/*
		 * Všude, kde se nastavuje cesta k nějakému adresáři, který má sloužít jako
		 * "adresář", který obsahuje soubory, které aplikace potřebuji pro kompilaci
		 * tříd, tak se ta cesta nastavuje k "nějakému adresáři" a ten musí obsahovat
		 * podadresář "lib", tak nyní otestuji, zda existuje a v něm případně budu
		 * hledat ty soubory:
		 */
		else if (!ReadFile.existsDirectory(pathToCompilerDir + File.separator + Constants.COMPILER_DIR_LIB))
			result = createCompilerDir();
		
		
		/*
		 * Zde existuje získaný adresář, pak otestuji, zda obsahuje všechny soubory,
		 * potřebné pro kompilaci tříd v aplikaci.
		 */
		else if (!containsDirCompileFiles(new File(pathToCompilerDir + File.separator + Constants.COMPILER_DIR_LIB)))
			result = createCompilerDir();
		
		
		/*
		 * Sem se dostanu v případě, že se provedla jedna z podmínek nebo žádná a
		 * soubory v načteném adresáři existují. Nebo se provedla jedna z podmínek a
		 * soubory ani tak nebyly nalezeny, ať tak či tak, zde již mohu ukončit vlákno,
		 * tak mohu zavolat metodu pro ukončení vlákna ("interrupt"), nebo to "nechat
		 * doběhnout" protože je to konec metody a vlákno se "ukončí samo".
		 */
		
		
		
		
		/*
		 * Tuto část jsem doplnil pouze za účelem výpisů, když toto vlákno zavolá
		 * uživatel pomocí menu v hlavním okně aplikace. Tam je to sice zbytečné, ale
		 * pokud například omylem či úmyslně smazal nějaký z potřebných souborů, pak je
		 * toto možnost, jak jej "znovu obnovit".
		 * 
		 * Jde o to, že tato proměnná se naplní pouze v případě, že se všechny potřebné
		 * soubory již nachází v požadovaném adresáři, pak není třeba nic kopírovat
		 * apod. Pak stačí otestovat, zda se má vypsat nějaké oznámení a případně jej
		 * vypsat.
		 */
		else listed = true;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro výpis výsledků ohledně zkopírování souborů
	 * potřebných pro kompilaci.
	 */
	public final void showResult() {
		/*
		 * Následující část jsem doplnil pouze protože, jsem doplnil uživateli možnost v
		 * aplikaci aby si ty soubory mohl vyhledat pomocí tohoto vlákna pomocí položky
		 * v hlavním menu v hlavním okně aplikace.
		 * 
		 * Následující část slouží pouze pro "informační" výpis pro uživatele v případě,
		 * že se bude toto vlákno volat pomocí toho menu, tak aby uživatel věděl, "jak
		 * to dopadlo", jinak pokud se toto vlákno spustí po startu aplikace, pak není
		 * třeba nic vypisovat, při pokusu o zkompilování tříd se vše uživatel dozví
		 * (zda se nachází potřebné soubory pro kompilaci nebo ne).
		 */
						
		if (listed)
			/*
			 * Sem se dostanu, pokud nebyla ani jedna podmínka splněna, pak se v poslední
			 * části 'else' nastavila proměnná listed na true, takže na cestě:
			 * pathToCompilerDir + File.separator + Constants.COMPILER_DIR_LIB se nachází
			 * potřebné soubory, které aplikace potřebuje ke kompilaci, tak akorát zjistím,
			 * zda se má vypsat hlášení, že je to OK. Toto může nastat například v případě,
			 * že se toto vlákno volá po kliknutí na příslušnou položku v menu v hlavním
			 * okně aplikace. Jinak se to nemá důvod vypisovat.
			 */
			JOptionPane.showMessageDialog(null,
					txtFilesAlreadyExistText + ": " + pathToCompilerDir + File.separator + Constants.COMPILER_DIR_LIB,
					txtFilesAlreadyExistTitle, JOptionPane.INFORMATION_MESSAGE);
				
		
		/*
		 * Zde otestuji, jestli se má vypsat nějaké oznámení ohledně zkopírování souborů
		 * apod. (dle hodnoty showMessage - true značí, že se má vypsat oznámení ohledně
		 * výsledku operace)
		 * 
		 * Dále zjistím, zda se ještě nějaká zpráva nevypsala (dle hodnoty listed),
		 * protože pokud jsou již všechny soubory pro kompilování nastaveny na příslušné
		 * cestě, pak stačí pouze upozornit uživatele, že je vše v pořádku.
		 * 
		 * Dále pokud jsou tyto dvě podmínky výše splněny, tj. ještě nebylo vypsáno
		 * hlášení, že jsou soubory v pořádku, protože nejsou, a má se vypsat hlášení,
		 * pak se dále otestuje proměnná result, pokud bude true, pak se zavolala metoda
		 * createCompilerDir, a podařilo se úspěšně najít potřebné soubory a zkopírovat
		 * je do příslušného adresáře. Pokud je ale proměnná result false, pak se
		 * zavolala metoda createCompilerDir, ale nepodařilo se najít potřebné soubory,
		 * tak je musí uživatel nastavit sám pomocí příslušného dialogu.
		 */
		else if (result)
			JOptionPane.showMessageDialog(null,
					txtFilesSuccessfullyCopiedText + ": " + pathToCompilerDir + File.separator
							+ Constants.COMPILER_DIR_LIB,
					txtFilesSuccessfullyCopiedTitle, JOptionPane.INFORMATION_MESSAGE);
		
		else
			JOptionPane.showMessageDialog(null, txtFilesNotFoundText, txtFilesNotFoundTitle,
					JOptionPane.INFORMATION_MESSAGE);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se v adresáři, který byl načten z Preferencí (file)
	 * obsahuje všechny soubory potřebné pro kompilaci tříd pomocí aplikace.
	 * 
	 * To se zjistí tak, že projdu list COMPILE_FILES_LIST, který obsahuje názvy
	 * všech souborů, které se mají načíst, aby bylo možné kompilovat třídy v
	 * aplikaci a pro každá název zavolám metodu containsDirCompileFile, která
	 * zjistí, zda se v načteném adresáři (file) nachází soubor s příslušným názvem.
	 * Pokud alespoň jeden nebude nalezen, pak mohu vrátit false.
	 * 
	 * @param file
	 *            - cesta k adresáři, který byl načten z Preferencí -> tj. který byl
	 *            dříve nastaven jako adresář, který obsahuje soubory potřebné pro
	 *            kompilaci.
	 * 
	 * @return true, pokud adrsář file obsahuje všechny soubory potřebné pro
	 *         kompilaci, jinak false.
	 */
	private static boolean containsDirCompileFiles(final File file) {
		for (final String s : COMPILE_FILES_LIST)
			if (!containsDirCompileFile(s, file))
				return false;

		return true;
	}

	
	/**
	 * Metoda, která zjistí, zda adesář fileDir obsahuje soubor s názvem
	 * compileFileName. Pokud jej obsahuje, pak se vrátí true, jinak false.
	 * 
	 * @param compileFileName
	 *            - název souboru (jeden z nich), který je potřebný pro kompilaci
	 *            tříd v aplikaci.
	 * 
	 * @param fileDir
	 *            - adresář, o kterém se má zjistit, zda obsahuje soubor s názvem
	 *            compileFileName.
	 * 
	 * @return true, pokud fileDir obsahuje soubor s názvem compileFileName, jinak
	 *         false.
	 */
	private static boolean containsDirCompileFile(final String compileFileName, final File fileDir) {
		return Arrays.stream(fileDir.listFiles()).anyMatch(f -> f.isFile() && f.getName().equals(compileFileName));
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která se pokusí vytvořit adresář libTools/lib v adresáři
	 * workspace/configuration. A do adresáře lib se pokusí nakopírovat soubory s
	 * názvy v listu COMPILE_FILES_LIST, které se nachází v adresáři, kde je
	 * nainstalovaná Java na daném zařízení, na kterém přávě běží tato aplikace,
	 * vždy v adresářích lib.
	 * 
	 * Tj. Prohledá se adresář Java (liší se dle OS, například na linuxu, distribuce
	 * Ubuntu je to adresář jvm v usr/lib/jvm/ Zde), kde jsou nainstalovány veškeré
	 * "verze" Javy na daném zařízení a vždy se v každém adresáři s "verzí" Javy
	 * najde adresář lib a v něm se hledají soubory v listu COMPILE_FILES_LIST.
	 * 
	 * Pokud se veškeré soubory najdou, pak se jejich cesty vloží do listu
	 * filesList a později se zkopírují do výše uvedeného adresáře lib.
	 * 
	 * Ještě se nejprve otestuje, zda vůbec existuje workspace, jinak by nemělo
	 * smysl nějaké soubory hledat, protože by je nebylo kam zkopírovat.
	 * 
	 * @return logická proměnná o tom, že se podařilo nebo nepodařilo najít a
	 *         zkopírovat soubory do příslušného adresáře ve workspace (uvedeno
	 *         výše). True se vrátí v případě, že existuje workspace, a byly
	 *         nalezeny potřebné soubory pro kompilaci a byly zkopírování do
	 *         příslušného adresáře ve workspace, jinak se vrátí false.
	 */
	private boolean createCompilerDir() {
		/*
		 * Nejprve otestuji, zda existuje adresář workspace, protože pokud ne, pak ani
		 * nemá smysl pokračovat, protože by se neměli kam zkopírovat ty nalezené
		 * soubory potřebné pro kompilaci, protože se kopírují do adresáře
		 * workspace/configuration/libTools/lib/Zde ty soubory.
		 */
		final String pathToWorkspace = ReadFile.getPathToWorkspace();
		
		// Pokud nebyl nalezen workspace, pak nemá smysl pokračovat:
		if (pathToWorkspace == null)
			return false;
		
		/*
		 * Nyní zjistím, ve workspace existuje adresář configuration, protože pokud ne
		 * (to by nastat nemělo), pak jej znovu vytvořím i s výchozími konfiguračními
		 * soubory.
		 */
		if (!ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
			WriteToFile.createConfigureFilesInPath(pathToWorkspace);
		
		
		
		/*
		 * Nyní vím, že již existuje adresář workspace a v něm adresář configuration,
		 * tak mohu začít hledat soubory potřebné pro kompilaci v adresáři, kde má
		 * uživatel na dané mzařízení nainstalovanou Javu.
		 */
		
		
		/*
		 * Pokud nebyla zjišťěna cesta, tak mohu také skončit, protože nevím, kde ty
		 * soubory hledat.
		 */
		if (javaHome == null)
			return false;
			
		final File javaHomeFile = new File(javaHome);
		
		
		/*
		 * Zde mám v proměnné javaFile cestu k adesáři, kde by měly být nainstalovány
		 * veškeré verze Javy na příslušném zařízení.
		 * 
		 * Tak si projdu všechny adresáře v tomto adresáři Java (případně jiném dle OS)
		 * v proměné javaFile, vždy zjistím, zda je to adresář a zda obsahuje podadresář
		 * lib, resp. Vždy k té cestě pouze připojím název adresáře lib, abych si
		 * ušetřil podmínku, kdyby to nebyl adresář, tak nemá smysl připojovat lib.
		 * 
		 * V tom adresáři lib vždy budu hledat ty 3 soubory (v listu
		 * COMPILE_FILES_LIST), pokud se nějaký z nich najde, přidá se do listu.
		 * 
		 * Až projde celý adresář "Java" (případně jiný dle OS) v proměnné javaFile, tak
		 * zjistím, zda jsem našel všechny 3 potřebné soubory, pokud ano, pak je mohu
		 * zkopírovat do adresáře workspace/configuration/libTools/lib/ty soubory zde.
		 * 
		 * Pokud alespoň jeden z těch 3 souborů nebude nalezen, pak jsem skončil a je
		 * třeba využít dialog v aplikaci pro nastavení těch 3 souborů nebo cestu k
		 * adresáři, kde se alespoň ty 3 soubory nachází.
		 */
		
		
		
		/*
		 * Proměnná, dle které se pozná, zda se má prohledat celý disk nebo jen
		 * rodičovksé adresáře, za účelem toho, aby se našli soubory, jejichž názvy jsou
		 * v listu COMPILE_FILES_LIST a které aplikace potřebuje pro to, aby mohla
		 * kompilovat Javovské třídy.
		 */
		final boolean searchWholeDisk = Start.getValueFromDefaultProp("Search whole disk for compile files",
				Constants.SEARCH_ENTIRE_DISK_FOR_COMPILE_FILES);
		
		
		/*
		 * Projdu všechny adresáře (případně celý disk) od adresáře javaHomeFile směrem
		 * "vzhůru" ke kořenovému adresáři disku a vždy v každém hledám adresář lib a v
		 * něm ty soubory v listu COMPILE_FILES_LIST.
		 */
		final boolean foundAllFilesForCompile = getDirWithJavaFiles(javaHomeFile, searchWholeDisk);
		
		/*
		 * Zde otestuji, zda byly všechny potřebné soubory nalezeny, pokud ne, pak nemá
		 * smysl pokračovat, pokud ano, pak mohu pokračovat zkopírováním tech souborů na
		 * výše uvedenou pozici ve workspace.
		 */
		if (!foundAllFilesForCompile)
			return false;
		
		
		

		/*
		 * Zde spustím mutext, resp. semaphore, protože budu přepisovat soubory, tak aby
		 * k ním žádné jiné vlákno nemohlo, napřílad aby je nikdo nikde nesmazal nebo
		 * nijak s nimi nemanipuloval apod.
		 */
		setSemaphore(true);

		
		
		
		
		/*
		 * Cesta k adresáři, kam se mají zkopírovat všechny soubory potřebné pro
		 * kompilaci.
		 * 
		 * Mělo by se jednat o adresář v: workspace/configuration/libTools/lib
		 */
		final File fileDesLibDir = new File(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME
				+ File.separator + Constants.COMPILER_LIB_DIR);

		
		/*
		 * Pokud alespoň jeden z adresářů na zadané cestě neexistuje, pak je vytvořím:
		 */
		if (!fileDesLibDir.exists())
			fileDesLibDir.mkdirs();

		
		
		/*
		 * Zde už by měl existovat cílový adresář, tak stačí zkopírovat všechny soubory.
		 */
		filesList.forEach(f -> {
			try {
				Files.copy(Paths.get(f.getAbsolutePath()), Paths.get(fileDesLibDir.getAbsolutePath(), f.getName()),
						StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				/*
				 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
				 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
				 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
				 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
				 */
				final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

				// Otestuji, zda se podařilo načíst logger:
				if (logger != null) {
					// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

					// Nejprve mou informaci o chybě:
					logger.log(Level.INFO,
							"Zachycena výjimka při kopírování souborů, které aplikace potřebuji pro kompilování " +
									"Javovských tříd z nalezených umístění do výchozího adresáře " +
									"workspace/configuration/libTools/lib. Soubor: "
									+ f.getAbsolutePath() + "\nna umístění: " + fileDesLibDir.getAbsolutePath()
									+ File.separator + f.getName());

					/*
					 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
					 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
					 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
					 */
					logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
				}
				/*
				 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
				 */
				ExceptionLogger.closeFileHandler();
			}
		});

		/*
		 * Zde jsou již soubory přespsané, resp. zkopírované na cílové umístění, tak
		 * mohu blokování uvolnit.
		 */
		setSemaphore(false);

		/*
		 * Zde byly zkopírovány všechny nalezene potřebné soubory do
		 * workspace/configuration/libTools/lib, tak mohu nastavit tuto novou cestu do
		 * Preferences, aby si pak kompilator nasel tuto cestu, kde můze vyuzit ty
		 * soubory.
		 */
		PreferencesApi.setPreferencesValue(PreferencesApi.COMPILER_DIR, pathToWorkspace + File.separator
				+ Constants.CONFIGURATION_NAME + File.separator + Constants.COMPILER_DIR);
		
		/*
		 * Sem se dostanu pouze v případě, že se podařilo najít a zkopírovat soubory,
		 * tak vrátím true, jako úspěšné "splnění" potřebné operace:
		 */
		return true;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro postupné nalezení všech souborů s příslušným názvem
	 * v listu COMPILE_FILES_LIST.
	 * 
	 * @param file
	 *            - Adresář, který byl získán jako adresář, kde je nainstalovaná
	 *            Java.
	 * 
	 * @param searchWholeDisk
	 *            - logická proměnná o tom, zda se má prohledat celý disk pro
	 *            získání potřebných souborů (hodnota true) nebo jen některé
	 *            adresáře cestou ke kořenovému adresáři disku od adresáře file ->
	 *            vždy se půjde pouze o úroveň výše a hledá se v něm adresář lib a v
	 *            něm ty soubory (hodnota false - doporučeno, jinak se mohou najít
	 *            chybné soubory se stejným názvem).
	 * 
	 * @return true, pokud se podaří najít všechny potřebné soubory ve výše uvedeném
	 *         listu, jinak false.
	 */
	private boolean getDirWithJavaFiles(final File file, final boolean searchWholeDisk) {
		/*
		 * Proměnná, do které si vždy uložím adresář, který se má "projít" a hledat v
		 * něm adresář lib a v něm příslušné soubory.
		 */
		File temp = file;
		
		
		/*
		 * Proměnná, dle které se pozná, zda se má pořád iterovat cyklem a hledat
		 * příslušné soubory nebo ne.
		 */
		boolean search = true;
		
		
		/*
		 * Cyklus, který vždy otestuje, zda se v příslušném adresáři nachází adresář lib
		 * a v něm potřebné soubory pro kompilaci tříd nebo se postupně prohledá celý
		 * disk.
		 */
		while (search) {
			/*
			 * Toto může nastat například v případě, že již adresář nemá předka, pak mohu
			 * nastavit proměnnou search na false a při další iterace se zjistí, že cyklus
			 * může skončit.
			 */
			if (temp == null) {
				search = false;
				continue;
			}
				
			
			// Toto by nastat nemělo:
			if (!temp.isDirectory()) {
				temp = temp.getParentFile();
				continue;
			}
				
			
			
			/*
			 * Zkusím v aktuálním adresáři najít adresář lib a v něm ty soubory, jelikož
			 * nevím, kde přesně se vrátí cesta k adresáři s aktuálně nainstalovanou Javou,
			 * tak budu postupně prohledávat adresáře od získaného místa v proměnné file a
			 * půjdu postupně ke kořenu disku a vždy v předkoví otestuji, zda obsahuje
			 * adresář lib a v něm ty soubory.
			 * 
			 * Nebo Pokud bude proměnná searchWholeDisk true, pak se bude pmocí rekurze
			 * prohledávat postupně celý disk, ale zde hrozí riziko, že se najdou chybné
			 * soubory, pouze se stejným názvem, ale pak už nezbývá než, aby uživatel zvolil
			 * potřebné soubory nebo adresář ručně pomocí příslušného dialogu v aplikaci.
			 */
			checkFilesInJava(temp, searchWholeDisk);
			
			/*
			 * Nyní otestuji, zda jsem našel všechny potřebné soubory, pokud ano, pak zde
			 * mohu vrátit hodnotu true a zkopírovat je do workspace/...
			 */
			if (foundAllFiles())
				return true;

			/*
			 * Sem se dostanu v případě, že jsem dosud nenašel všechny potřebné soubory, pak
			 * si vezmu předka z aktuálního adresáře a opakuji postup.
			 */
			temp = temp.getParentFile();			
		}
		
		/*
		 * Toto se vrátí puze v případě, že se prohledají (dle příslušných postupů)
		 * některé adresáře cestou ke kořenovému adresáři na disku nebo pomocí rekurze
		 * všechny soubory na daném disku a alespoň jeden z požadovaných souborů nebude
		 * nalezen. Pak se vrátí false a uživatel může nastavit příslušné soubory sám
		 * pomocí příslušného dialogu.
		 */
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	

	/**
	 * Metoda, která projde všechny soubory v adresáři "Java" (různé dle OS), kde
	 * jsou nainstalovány všechny verze Javy, JDK, JRE, ...
	 * 
	 * V každém takovém adresáři se zjistí, zda obsahuje adresář lib, pokud ano, pak
	 * se zavolá další metoda, která zjistí, zda obsahuje nějaký z těch 3
	 * požadovaných souborů, resp. souborů v listu COMPILE_FILES_LIST.
	 * 
	 * Note:
	 * Pokud bude proměnná searchWholeDisk true, pak se bude prohledávat postupně
	 * celý disk pomocí rekurze, ne jen "nějaké" soubory od adresáře file směrem
	 * "vzhůru" ke kořenovému adresáři disku a v něm vždy adresář lib a v něm
	 * požadované soubor (searchWholeDisk = true). Pokud bude proměnná
	 * searchWholeDisk true, pak se bude pomocí rekurze prohledávat celý disk a
	 * hrozí riziko, že se najde chybný soubor se stejným názvem.
	 * 
	 * @param file
	 *            - cesta k adresáři Java (různé dle OS), kde jsou nainstalované
	 *            verze Javy na příslušném zařízení, kde běží tato aplikace.
	 * 
	 * @param searchWholeDisk
	 *            - logická proměnná o tom, zda se má prohledat celý disk, aby se
	 *            našli potřebné soubory (true) nebo se budou postupně prohledávat
	 *            pouze adresáře od získaného adresáře směrem "vzhůru" (false -
	 *            doporučeno).
	 */
	private void checkFilesInJava(final File file, final boolean searchWholeDisk) {
		/*
		 * Aby byly vůbec nějaké soubory v daném adresáři:
		 */
		if (file.listFiles() == null)
			return;
		
		Arrays.stream(file.listFiles()).forEach(f -> {
			/*
			 * Pokud se nepodařilo "načíst soubor" (nemělo by nastat) nebo se nejedná o
			 * adresář, pak budu pokračovat další iterací. Protože pokud se nejedná o
			 * adresář, pak nemá smysl k němu připojovat podadresář lib.
			 */
			if (f == null || !f.isDirectory())
				return;
			
			
			/*
			 * Zde otestuji, zda se má prohledávat postupně celý disk od adresáře, který byl
			 * získán jako adresář, kde je nainstalována Java, nebo ne.
			 */
			if (searchWholeDisk)
				checkFilesInJava(f, searchWholeDisk);
			
			/*
			 * Vytvořím cestu k adresáři lib v daném adresáři (možná Javy):
			 */
			final File fileLib = new File(f, File.separator + Constants.COMPILER_DIR_LIB);
			
			/*
			 * Zjistím, zda se adresář lib nachází v daném adresáři:
			 */
			if (!ReadFile.existsDirectory(fileLib.getAbsolutePath()))
				return;
			
			/*
			 * Zde stačí zjistít, zda existují ty soubory v tom adresáři lib:
			 */
			checkCompilationFilesInLib(fileLib);
		});
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která projde všechny soubory v adresáři lib a u každého souboru
	 * otestuje, zda je to soubor (ne adresář) a zda se jedná o jeden z těch
	 * požadovaných souborů pro kompilaci, pokud ano, pak se přidá do listu s těmi
	 * soubory (v případě, že tam ještě není).
	 * 
	 * @param lib
	 *            - cesta k adresáři lib v jedné z adresářů s nainstalovanou Javou.
	 */
	private void checkCompilationFilesInLib(final File lib) {
		Arrays.stream(lib.listFiles()).forEach(f -> {
			/*
			 * Zda soubor existuje by mělo být vždy, ale že je adresář už pokaždé být
			 * nemusí, pokud bude jedna z podmínek splněna, pak pokračuji další iterací.
			 */
			if (!ReadFile.existsFile(f.getAbsolutePath()))
				return;

			
			/*
			 * Nyní otestuji názvy souborů, zda se jedná o soubor s příslušným názvem. Pokud
			 * ano, pak jej přidám do listu s příslušnými soubory (pokud v něm ještě není).
			 */
			COMPILE_FILES_LIST.forEach(cf -> {
				if (f.getName().equals(cf) && !containsListFileWithName(f.getName()))
					filesList.add(f);
			});
		});
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda byly nalezeny veškeré soubory potřebné ke
	 * kompilaci, tj. zda byly nalezeny všechny soubory, které se nachází v listu
	 * COMPILE_FILES_LIST. To poznám tak, že proiteruji list COMPILE_FILES_LIST a
	 * zjistím, zda se pro každý název příslušného souboru nachází v listu
	 * filesList cesta k souboru se steným názvem.
	 * 
	 * 
	 * @return true, pokud list filesList obsahuje cesty ke všem souborům s názvy
	 *         souborů, které se nachází v listu COMPILE_FILES_LIST, jinak false.
	 */
	private boolean foundAllFiles() {
		for (final String s : COMPILE_FILES_LIST)
			if (!containsListFileWithName(s))
				return false;

		return true;
	}
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se v listu filesList nachází soubor s názvem v
	 * proměnné fileName.
	 * 
	 * @param fileName
	 *            - název souboru, o kterém se má zjistit, zda sen achází v listu
	 *            filesList
	 * 
	 * @return true, pokud se v listu filesList nachází soubor se stejným názvem
	 *         jako je v proměné fileName, jinak false.
	 */
	private boolean containsListFileWithName(final String fileName) {
		return filesList.stream().anyMatch(f -> f.getName().equals(fileName));
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která "spustí / získá" nebo "uvolní" mutext, resp. semaphore.
	 * 
	 * @param setSemaphore
	 *            - logická hodnota, dle které se pozná, zda se má semaphore
	 *            "spustit / získat" nebo uvolnit. True - získat, false - uvolnit.
	 */
	private void setSemaphore(final boolean setSemaphore) {
		if (setSemaphore) {
			try {
				semaphoreMutex.acquire();
            } catch (InterruptedException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při 'získávání / nastavování či spouštění mutextu, resp. semaphore s " +
                                    "jedním vláknem, "
                                    + "které může přistupovat k souboru. Tato výjimka může nastat například v " +
                                    "případě, že je aktuální vlákno přerušeno.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
            }
		}

		else
			semaphoreMutex.release();
	}



	
	
	
	
	
	





	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			txtFilesAlreadyExistText = properties.getProperty("Ccf_Jop_Txt_FilesAlreadyExistText", Constants.CCF_JOP_TXT_FILES_ALREADY_EXIST_TEXT);
			txtFilesAlreadyExistTitle = properties.getProperty("Ccf_Jop_Txt_FilesAlreadyExistTitle", Constants.CCF_JOP_TXT_FILES_ALREADY_EXIST_TITLE);
			
			txtFilesSuccessfullyCopiedText = properties.getProperty("Ccf_Jop_Txt_FilesSuccessfullyCopiedText", Constants.CCF_JOP_TXT_FILES_SUCCESSFULLY_COPIED_TEXT);
			txtFilesSuccessfullyCopiedTitle = properties.getProperty("Ccf_Jop_Txt_FilesSuccessfullyCopiedTitle", Constants.CCF_JOP_TXT_FILES_SUCCESSFULLY_COPIED_TITLE);
			
			txtFilesNotFoundText = properties.getProperty("Ccf_Jop_Txt_FilesNotFoundText", Constants.CCF_JOP_TXT_FILES_NOT_FOUND_TEXT);
			txtFilesNotFoundTitle = properties.getProperty("Ccf_Jop_Txt_FilesNotFoundTitle", Constants.CCF_JOP_TXT_FILES_NOT_FOUND_TITLE);
		}
		
		
		else {
			txtFilesAlreadyExistText = Constants.CCF_JOP_TXT_FILES_ALREADY_EXIST_TEXT;
			txtFilesAlreadyExistTitle = Constants.CCF_JOP_TXT_FILES_ALREADY_EXIST_TITLE;

			txtFilesSuccessfullyCopiedText = Constants.CCF_JOP_TXT_FILES_SUCCESSFULLY_COPIED_TEXT;
			txtFilesSuccessfullyCopiedTitle = Constants.CCF_JOP_TXT_FILES_SUCCESSFULLY_COPIED_TITLE;

			txtFilesNotFoundText = Constants.CCF_JOP_TXT_FILES_NOT_FOUND_TEXT;
			txtFilesNotFoundTitle = Constants.CCF_JOP_TXT_FILES_NOT_FOUND_TITLE;
		}
	}
}
