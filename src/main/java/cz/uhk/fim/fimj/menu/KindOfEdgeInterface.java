package cz.uhk.fim.fimj.menu;

import java.util.Properties;

import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;

/**
 * Deklarace metod pro třídu KindOfEdge
 * <p>
 * Rozhraní, které obsahuje metody, které rozpoznavají o jaký objekt v diagramu tříd se jedná, například, zda je označen
 * komentář, třída, hrana typu dědičnost, apod. Viz jednotlivé metody
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface KindOfEdgeInterface {

    /**
     * Metodda, která toestuje, zda hrana předaná v parametru reprezentuje vztah - asociace Zjístí se to tak, že se ze
     * souboru přečtou všechny potřené - nastavené hodnoty pro danou hranu, a když nějaká vlastnost neopovídá
     * nejedná se
     * o danou hranu
     * <p>
     * Pokud nebude nalezen soubor s hodnotami, museli se použít výchozí hodnoty, tak to také otestuji:
     *
     * @param edge
     *         - hrana u které chci zjistit, zda reprezentuje asociace
     * @return true, pokud hrana reprezentuje asociace, jinak false
     */
    boolean isAssociationEdge(final DefaultEdge edge);


    /**
     * Metoda, která zjistí, zda se jedná o hranu, která reprezentuje dědičnost
     *
     * @param edge
     *         = hrana, u které chci otestovat, zda se jedná o dědičnost
     * @return true, pokud hrana reprezentuje dědišnost, jinak false
     */
    boolean isInheritEdge(final DefaultEdge edge);


    /**
     * Metoda, která zjistí, zda se jedná o hranu, která reprezentuje Implementaci rozhrani
     *
     * @param edge
     *         - hrana, u které chci otestovat, zda se jedná o Implementaci rozhraní
     * @return true, pokud hrana reprezentuje implementaci rozhraní, jinak false
     */
    boolean isImplementsEdge(final DefaultEdge edge);


    /**
     * Metoda, která zjistí, zda se jedná o hranu, která reprezentuje agregace s multipliticou 1 : 1
     *
     * @param edge
     *         - hrana, u které chci otestovat, zda se jedná o agregaci
     * @return true, pokud hrana reprezentuje agregaci, jinak false
     */
    boolean isAggregation_1_ku_1_Edge(final DefaultEdge edge);


    /**
     * Metoda, která zjistí, zda se jedná o hranu, která reprezentuje agregaci s multipliticou 1 : N
     *
     * @param edge
     *         - hrana, u které chci otestovat, zda se jedná o agregaci
     * @return true, pokud hrana reprezentuje agregaci, jinak false
     */
    boolean isAggregation_1_ku_N_Edge(final DefaultEdge edge);


    /**
     * Metoda, která zjistí, zda se jedná o buňku v diagramu tříd, která reprezentuje třídu
     *
     * @param cell
     *         - označená buňka, resp. třída v diagramu tříd
     * @return true, pokud bunka reprezentuje třídu, jinak false
     */
    boolean isClassCell(final DefaultGraphCell cell);


    /**
     * Metoda, která zjistí, zda se jedná o buňku, která v diagramu tříd reprezentuje komentář
     *
     * @param cell
     *         - označená buňka v diagramu tříd
     * @return true, pokud se jedná o buňku reprezentující komentář, jinak false
     */
    boolean isCommentCell(final DefaultGraphCell cell);


    /**
     * Metoda, která zjistí, zda se jedná o hranu, která v diagramu tříd reprezentuje hranu komentáře, resp. hranu mezi
     * třídou a komentářem
     *
     * @param edge
     *         - hrana označena v grafu
     * @return true, pokud se jedná o hranu, která v grafu reprezentuje hranu mezi buňkou coby komentar a bunkou coby
     * třída, jinak false
     */
    boolean isCommentEdge(final DefaultEdge edge);


    /**
     * Metoda, která vloží do objektu typu Properties, ve kterém je načten ClassDiagramProperties nové hodnoty, resp.
     * přepíše hodnotu aktuáně existujícího klíše, toto je využito pouze při "rychlé" změné velikost písma.
     * <p>
     * Proto je potřeba doplnit ty hodnoty do uvedeného objektu Properties, prootže kdybych to neudělal, tak by se
     * nerozpoznaly objekty v diagramu, protože by měli jiné (například) velikosti písma, než by mít měli pdle
     * příslušné
     * objektu Properties apod.
     *
     * @param key
     *         - Klíč v mapě v příslušné Properties.
     * @param value
     *         - nová hodnota pro příslušný objekt.
     */
    void editValueInProperties(final String key, final String value);


    /**
     * Metoda, která vrátí objekt Properties, který obsahuje nastavení pro diagram tříd (ClassDiagram.properties).
     * <p>
     * Jedná se o to, že v případě, že se využije "rychlý" způsob pro manipulaci s velikostí písma v diagramu tříd, tak
     * je potřeba změnit příslušnou hodnotu i v objektu Properties, který se využívá pro otestování / zjištění
     * objektů v
     * diagramu tříd. Je potřeba změněné hodnoty uložit ihned do příslušného souboru v configuration ve workspace,
     * protože jinak, kdyby uživatel zavřel aplikaci, tak při otevření by se zjistily chybné hodnoty.
     *
     * @return objekt Properties, který se využívá v aplikaci pro zjištění objektů v diagramu tříd.
     */
    Properties getProperties();


    /**
     * Metoda, která slouží pro unovu načtení objektu Properties, který si příslušný objekt drží u sebe, toto je
     * potřeba
     * pouze proto, že když se při spuštění aplikace nachází v nějakých konfiguračních souborech chyby, pak se znovu
     * vytvoří, ale příslušný objekt se musí také přenačíst, jinak by mohl obsahovat chybná data.
     */
    void reloadProperties();
}