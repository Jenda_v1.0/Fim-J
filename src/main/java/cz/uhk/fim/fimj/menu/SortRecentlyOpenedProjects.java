package cz.uhk.fim.fimj.menu;

import cz.uhk.fim.fimj.file.Constants;

/**
 * Výčet obsahuje pouze hodnoty pro způsob řazení, které je možné nastavit pro řazení položek v rozevíracím menu:
 * cz.uhk.fim.fimj.menu.Menu#menuRecentlyOpenedProjects.
 * <p>
 * I když jsou řazení pouze sestupně a vzestupně, tak kdybych v budoucnu náhodou tyto možnosti rozšířil o řazení
 * například dle názvu projektu (vzestupně / sestupně), dle cesty k projektu (sestupně / vzestupně) nebo přidal ještě
 * nějaké další možnosti apod. Tak abych nemusel přdělávat "kde co", ale pouze doplnit komponenty, hodnoty zde ve výčtu
 * a informace pro uživatele v konfiguračních souborech, případně testování validních hodnot.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 22.07.2018 17:51
 */

public enum SortRecentlyOpenedProjects {
    ASC("ASC"), DES("DES");

    /**
     * Proměnná pro text, který slouží jako "identifikátor" způsobu řazení při ukládání hodnoty do konfiguračního
     * souboru.
     */
    private final String value;

    /**
     * Konstruktor třídy.
     *
     * @param value
     *         - identifikátor konkrétní výčtové hodnoty při ukládání do konfiguračního souboru.
     */
    SortRecentlyOpenedProjects(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }


    /**
     * Metoda, která zjistí, zda je parametr "v" hodnota / identifikátor jedné z výčtových hodnot.
     * <p>
     * (Vím, že by bylo možné využít valueOf, ale nechci řešit výjimku v případě, nebude odpovídat konkrétní hodnota -
     * nemělo by nastat)
     *
     * @param v
     *         - text, o kterém se zjstí, zda se jedná o jednu z výčtových hodnot.
     *
     * @return Konkrétní výčtovou hodnotu v případě, že v bude odpovídat konkrétní výčtové hodnotě, jinak výchozí
     * hodnotu definovanou v Constants.
     */
    public static SortRecentlyOpenedProjects checkValue(final String v) {
        for (SortRecentlyOpenedProjects s : values())
            if (s.getValue().equals(v))
                return s;

        return Constants.DEFAULT_SORT_RECENTLY_OPENED_PROJECTS;
    }
}
