package cz.uhk.fim.fimj.menu;

/**
 * Toto rozhraní slouží pro deklaraci - signatury a kontrakty pro metod, které v Javovské třídě budou odebírat některé
 * vztahy z diagramu tříd. Vztahy jako jsou například dědičnost nějaké třídy, implemntace reozhraní, nebo pro odebrání
 * nějakého atributu z třídy. Atributu, který vznikl například jako vztah agregace mezi třídami v diagramu tříd apod.
 * viz konkrétní metody
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface EditClassInterface {

    /**
     * Metoda, která vymaže reference - asociaci v obou třídách
     * <p>
     * Postup: Cestu k adresáři src si předávám, abych ji nemusel znovu zjišťovat, v tuto cvhíli vím, že ten adresář
     * existuje, ale nemohu totéž říct o označených třídách tak se jejich existenci otestuji POkud existují, tak si
     * najdu atribut, reprezentující asociace: ve zdrojové třídě najdu private|public|protected destinationClassName
     * soumeName;; v cílové tříděš naopak a tento atriut smažu, pokud do dopane dobře, vrátím true, jinak false false -
     * pokud dojde k nějaké chybě - kdy se atribut nesmazal pokud by v té třídě nebyl vrátím true - neexistuje tam
     * vazba
     * <p>
     * Note: Tato metoda odebere všechny atributy - reference na danou třídu - vztah typu asociace jinak by mezi nimí
     * pořád existoval nějaký vztah - typu asociace
     * <p>
     * Postup algoritmu: Vezmu si text třídy, (pokud existuje) prohledám ho po řádcích pokud bude nalezen odpovídající
     * syntaxe pro daný atribut bude odstranen po probehnuti tohoto cyklu uložím provedené zmeny zpět do třídy a vrátím
     * true nebo false - dle úspěšného nebo neúspěšného dokončení
     *
     * @param pathToSrc
     *         - cesta k adresáři src
     * @param cellValue1
     *         - náze buňky - třídy - zdrojová třída
     * @param cellValue2
     *         - název druhé - cílové buňky - třídy
     * @param countOfVars
     *         - proměnná, která značí počet proměnných, který by se měl smazat. Jedná se o kardinalitu (multiplicitu)
     *         na příslušné hraně typu (symetrická) asociace, která se má smazat, takže pokud bude například na hraně
     *         (typu sym asociace) kardinalita 2 : 2, tak se z obou tříd musí smazat dvě proměnné typu té druhé třídy.
     * @return zda se smazala hrana.
     */
    boolean deleteAssociation(final String pathToSrc, final String cellValue1, final String cellValue2,
                              final int countOfVars);


    /**
     * metoda, která vymaže dědičnost z třídy v parametru
     * <p>
     * V podstaě jen zapíše novou hlavičku třídy Najde v ní klíčove slovo extends, po toto slovi (index -1) se hlavička
     * zkopíruje, pak index +2 se pokracuje a otestuje se, zda se tam nachází další např rozhraní, apod.
     *
     * @param pathToSrc
     *         - cesta k adresáři src v projektu
     * @param cellValue
     *         - název buňky - třídy - pro zjištění existence třídy
     * @return true, pokud se operace provede úspěšně, jinak false
     */
    boolean deleteInherit(final String pathToSrc, final String cellValue);


    /**
     * Metoda, která vymaže ze zdrojové třídy (cellValue1) implementaci rozhraní (cellValue2) *
     * <p>
     * Metoda, zda obě třídy existjí - i když by zde stačilo otestovat pouze první třídu - s tou druhou nic nedělám - s
     * rozhraním Pokud obě třídy existují, tak si zjistím z názvu druhé třídy její opravodovy nazev - bez balíčků -
     * čistý název rozhraní a odeberu ho ze zdrojové třídy Pokud je ve zrdojové třídě pouze jedno rozhraní edebere se i
     * klíčové slovo implements, jinak se to se zbytekm nechaá
     *
     * @param pathToSrc
     *         - cesta do adresáře src v projektu
     * @param cellValue1
     *         - název první třídy i s balíčky - z této třídy se má odebreat implementace rozhraní - druhá třída
     * @param cellValue2
     *         - název druhé třídy - rozhraní, jehož implementace se má z první třídy odebrat
     * @return true, pokud operace dopadně úspěšně, jinak false
     */
    boolean deleteImplements(final String pathToSrc, final String cellValue1, String cellValue2);


    /**
     * Metoda, která vymže ze zdrojové třídy (cellValue1) odkaz - referenci na cílovou třídu (cellValu2)
     * <p>
     * Metoda vymaže všechny podobné atributy, jinak by mezi třídami pořád existovala vazba, což nechci
     *
     * @param pathToSrc
     *         - cesta k adresáři src, abych mohl otestovat existenci tříd
     * @param cellValue1
     *         - název první - zdrojoví buňky - třdy, ve které je třeba smazat odkaz na druhou třídu
     * @param cellValue2
     *         - 'cílová' třída, na kterou v první třídě je třeba smazat odkaz
     * @param countOfVarsInSrc
     *         - proměnná, která značí kolik atributů v té první třídě (cellValue1) typu té druhé třídy (cellValue2) se
     *         má smazat.
     * @param countOfVarsInDes
     *         - proměnná, která značí kolik atributů v té druhé třídě (cellValue2) typu té první třídy (cellValue1) se
     *         má smazat.
     * @param ascIsDisplayedAsAgr
     *         - logická proměnná, která značí, zda jsou v diagramu tříd zobrazeny vztahy typu (symetrická) asociace
     *         jako dvě (asymetrické) asociace (původně agregace 1 : 1). Toto je potřeba, aby se vědělo, kolik se má
     *         smazat proměnných z jakých tříd.
     * @return true, pokud se operace úspěšně provede, jinak false
     */
    boolean deleteAggregation_1_ku_1(final String pathToSrc, final String cellValue1, final String cellValue2,
                                     final int countOfVarsInSrc, final int countOfVarsInDes, final boolean
											 ascIsDisplayedAsAgr);


    /**
     * Metoda, která ze zdrojové třídy vymaže odkaz - List<DestinationClass> cílových tříd
     * <p>
     * Metoda vymaže všechny podobné atributy, jinak by existovala počád vazba
     *
     * @param pathToSrc
     *         - cesta k adresáři src v projektu
     * @param cellValue1
     *         - název 'Zdrojové' třídy i s balíčky
     * @param cellValue2
     *         - název 'cílové' třídy i s balíčky
     * @return true, pokud operace úspěšně dopadne, jinak false
     */
    boolean deleteAggregation_1_ku_N(final String pathToSrc, final String cellValue1, final String cellValue2);
}