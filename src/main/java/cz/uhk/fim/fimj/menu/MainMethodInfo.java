package cz.uhk.fim.fimj.menu;

import java.lang.reflect.Method;

import javax.swing.JRadioButton;

/**
 * Tato třída slouží pro "uchování" hlavní - spouštěcí metody main, názvu třídy, a JradioButtonu, pomocí kterého bude
 * main metoda reprezentována v dialogu pro označení main metody, která se má zavolat
 * <p>
 * Instance této třídy - objektu se vytvoří pouze pokud se v aktuálně otevřeném projjektu aplikace nachází více tříd,
 * které obsahují metodu main
 * <p>
 * Všechny takto vytvořené instance této třídy budou vloženy do kolekce a ta bude dále předána dialogu:
 * SelectMainMethod.java, kde se označí příslušná metoda main, která se má zavolat, ale to je již jiná část aplikace
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class MainMethodInfo {

    /**
     * JradioButton, který půjde označit v dialogu pro označení metody main:
     */
    private final JRadioButton rbMain;


    /**
     * Main metoda, která se zavolá po označení uživatelem:
     */
    private final Method mainMethod;


    /**
     * Konstruktor této třídy, který se zavolá pro vytvoření instance této třídy. Konstruktor vytvoří instanci
     * radiobuttonu a uloží si metodu pro spuštění
     *
     * @param className
     *         - název třídy, ve které se nacháze hlavní - spouštěc metoda main
     * @param mainMethod
     *         - main metoda, která se zavolá, po označení uživatelem
     */
    MainMethodInfo(final String className, final Method mainMethod) {
        super();

        this.mainMethod = mainMethod;

        rbMain = new JRadioButton(className);
    }


    /**
     * Metoda - getr, který vrátí hlavní metodu, kterou tato třída reprezentuje a která se má zavolat po označení
     * oživatelem
     *
     * @return hlavní metodu main
     */
    public final Method getMainMethod() {
        return mainMethod;
    }


    /**
     * Metoda, která zjistí, zda je příslušný JradioButton oznčený nebo ne
     *
     * @return vrátí true, pokud je příslušný JradionButton označený, jinak false
     */
    public final boolean isRbMainSelected() {
        return rbMain.isSelected();
    }


    public final void setRbMainSelected(final boolean selected) {
        rbMain.setSelected(selected);
    }


    /**
     * Metoda - getr, který vrátí reference na insanci radioButtonu, abych ho mohl přídat do dialogu pro označení tak,
     * aby šel označit vždy jen jeden - ButtonGroup
     *
     * @return referenci na instanci komponenta radioButtonu v této třídě
     */
    public final JRadioButton getRbMain() {
        return rbMain;
    }
}