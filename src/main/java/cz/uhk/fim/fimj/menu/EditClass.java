package cz.uhk.fim.fimj.menu;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.JOptionPane;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.project_explorer.ReadWriteFile;
import cz.uhk.fim.fimj.project_explorer.ReadWriteFileInterface;

/**
 * Třída slouží pro editaci Javovských tříd, konkrétně pro edebrání vztahů jako předchůdce odebrání hrany, například
 * odebrání atributů či dědičnosti nebo implementace rozhraní, apod. reprezentující nejaký vztah mezí třídami v grafu
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class EditClass implements EditClassInterface, LanguageInterface {
	
	/**
	 * Třída pro získání a zapsání zdrojového kódu třídy ze a do souboru (+ nějaké další manipulace)
	 */
	private static final ReadWriteFileInterface editCode = new ReadWriteFile();
	
	
	// Proměnné, do kterých vložím text ve správném jazyce - text do chybových hlášek:
	private static String missingClassText1, missingClassTitle, missingClassText2;
	
	
	
	/**
	 * Konstruktor této třídy.
	 */
	public EditClass() {
		super();
		
		setLanguage(App.READ_FILE.getSelectLanguage());
	}
	
	
	
	
	
	@Override
	public boolean deleteAssociation(final String pathToSrc, final String cellValue1, final String cellValue2,
			final int countOfVars) {
		
		final String classNameWithPackage1 = cellValue1.replace(".", File.separator);
		final String classNameWithPackage2 = cellValue2.replace(".", File.separator);
		
		
		final File fileClass1 = new File(pathToSrc + File.separator + classNameWithPackage1 + ".java");
		final File fileClass2 = new File(pathToSrc + File.separator + classNameWithPackage2 + ".java");
		
		if (fileClass1.exists() && fileClass1.isFile() && fileClass2.exists() && fileClass2.isFile()) {
			// Zde obě třídy existují, mohu získat jejich kód:
			List<String> textOfClass1 = editCode.getTextOfFile(fileClass1.getPath());
			List<String> textOfClass2 = editCode.getTextOfFile(fileClass2.getPath());
			
			
			final String className1 = fileClass1.getName().substring(0, fileClass1.getName().lastIndexOf('.'));
			final String className2 = fileClass2.getName().substring(0, fileClass2.getName().lastIndexOf('.'));
			
			
			/*
			 * Nyni projdu obě kolekce - zdrojové kódy a pokud najdu odpovídající atribut,
			 * smažu ho.
			 * 
			 * Ale je to potřeba provést tak, že tento postup pro mazání (že projdu kód
			 * třídy, najdu v něm referenci na druhou třídu a smažu jej) budu muset opakovat
			 * tolikrát, kolikrát je kardinalita (proměnná countOfVars).
			 */
			for (int i = 0; i < countOfVars; i++) {
				// Z PRVNÍ TŘÍDY MUSÍM SMAZAT REFERENCI NA DRUHOU TŘÍDU A NAOPAK!!!:
				textOfClass1 = deleteAttributeIfExist(textOfClass1, className2, false);
				textOfClass2 = deleteAttributeIfExist(textOfClass2, className1, false);				
			}

			
			
			
			// Zde proběhlo vše v pořádku, mohu vložir kód zpět do tříd a vrátit hodnotu true:

			return editCode.setTextToFile(fileClass1.getPath(), textOfClass1)
					&& editCode.setTextToFile(fileClass2.getPath(), textOfClass2);
		}
		else
			JOptionPane.showMessageDialog(null,
					missingClassText1 + fileClass1.getPath() + missingClassText2 + fileClass2.getPath(),
					missingClassTitle, JOptionPane.ERROR_MESSAGE);
		
		// sem se dostanu, pokud nastane nějaký problém:
		return false;
	}
	
	
	
	
	
	
	@Override
	public boolean deleteInherit(final String pathToSrc, final String cellValue) {
		final String classNameWithPackage1 = cellValue.replace(".", File.separator);				
		
		final File fileClass1 = new File(pathToSrc + File.separator + classNameWithPackage1 + ".java");
		
		if (fileClass1.exists() && fileClass1.isFile()) {
			// Zde obě třídy existují, mohu získat jejich kód:
			List<String> textOfClass1 = editCode.getTextOfFile(fileClass1.getPath());
			
			
			final String className1 = fileClass1.getName().substring(0, fileClass1.getName().lastIndexOf('.'));
			
			
			textOfClass1 = deleteExtendsFromHeader(textOfClass1, className1);
			
			
			// Zde proběhlo vše v pořádku, mohu vložir kód zpět do tříd a vrátit hodnotu true:

			return editCode.setTextToFile(fileClass1.getPath(), textOfClass1);
		}
		else
			JOptionPane.showMessageDialog(null, missingClassText1 + fileClass1.getPath(), missingClassTitle,
					JOptionPane.ERROR_MESSAGE);
		
		// sem se dostanu, pokud nastane nějaký problém:
		return false;
	}






	@Override
	public boolean deleteImplements(final String pathToSrc, final String cellValue1, final String cellValue2) {
		final String classNameWithPackage1 = cellValue1.replace(".", File.separator);
		final String classNameWithPackage2 = cellValue2.replace(".", File.separator);
				
		
		final File fileClass1 = new File(pathToSrc + File.separator + classNameWithPackage1 + ".java");
		final File fileClass2 = new File(pathToSrc + File.separator + classNameWithPackage2 + ".java");
		
		if (fileClass1.exists() && fileClass1.isFile() && fileClass2.exists() && fileClass2.isFile()) {
			// Zde obě třídy existují, mohu získat jejich kód:
			List<String> textOfClass1 = editCode.getTextOfFile(fileClass1.getPath());
			
			
			final String className1 = fileClass1.getName().substring(0, fileClass1.getName().lastIndexOf('.'));
			final String className2 = fileClass2.getName().substring(0, fileClass2.getName().lastIndexOf('.'));
			
			
			// smažu implementaci z zdrojové třídy:
			textOfClass1 = deleteImplementsFromHeader(textOfClass1, className1, className2);
			
			
			
			// Zde proběhlo vše v pořádku, mohu vložir kód zpět do tříd a vrátit hodnotu true:

			return editCode.setTextToFile(fileClass1.getPath(), textOfClass1);
		}
		else
			JOptionPane.showMessageDialog(null,
					missingClassText1 + fileClass1.getPath() + missingClassText2 + fileClass2.getPath(),
					missingClassTitle, JOptionPane.ERROR_MESSAGE);
		
		// sem se dostanu, pokud nastane nějaký problém:
		return false;
	}






	@Override
	public boolean deleteAggregation_1_ku_1(final String pathToSrc, final String cellValue1, final String cellValue2,
			final int countOfVarsInSrc, final int countOfVarsInDes, final boolean ascIsDisplayedAsAgr) {

		final String classNameWithPackage1 = cellValue1.replace(".", File.separator);
		final String classNameWithPackage2 = cellValue2.replace(".", File.separator);
				
		
		final File fileClass1 = new File(pathToSrc + File.separator + classNameWithPackage1 + ".java");
		final File fileClass2 = new File(pathToSrc + File.separator + classNameWithPackage2 + ".java");
		
		if (fileClass1.exists() && fileClass1.isFile() && fileClass2.exists() && fileClass2.isFile()) {
			// Zde obě třídy existují, mohu získat jejich kód:
			List<String> textOfClass1 = editCode.getTextOfFile(fileClass1.getPath());
			List<String> textOfClass2 = editCode.getTextOfFile(fileClass2.getPath());
			
			
			final String className1 = fileClass1.getName().substring(0, fileClass1.getName().lastIndexOf('.'));
			final String className2 = fileClass2.getName().substring(0, fileClass2.getName().lastIndexOf('.'));
			
			
			
			
			
			// PŮVODNÍ:
			// Naný projdu obě kolekce - zdrojové kódy a pokud najdu odpovídající atribut, smažu ho:
			// Z PRVNÍ TŘÍDY MUSÍM SMAZAT REFERENCI NA DRUHOU TŘÍDU !!!:			
//			textOfClass1 = deleteAttributeIfExist(textOfClass1, className2, false);
			
			/*
			 * Zde potřebuji nejprve otestovat, jestli jsou (symetrické) asociace zobrazeny
			 * jako dvě (asymetrické) asociace, resp. původně agregace 1 : 1. Pokud ano, pak
			 * jsou na hranách pouze vztahy 1 : 1, tak musím smazat vždy jen z té první
			 * třídy atribut typu té druhé třídy, nic víc.
			 * 
			 * Poud se mají zobrazovat vztahy (symetrické) asociace jak dvě (asymetrické)
			 * asociace (agregace 1 : 1), pak jsou úplně všechny vztahy typu sym a asym
			 * asociace zobrazeny jako hrana s šipkou s kardinalitou 1 : 1, takže musím vždy
			 * smazat jen z té zdrojové třídy atribut typu té cílové třídy.
			 */
			if (ascIsDisplayedAsAgr) {
				textOfClass1 = deleteAttributeIfExist(textOfClass1, className2, false);
				
				// Zde proběhlo vše v pořádku, mohu vložit kód zpět do tříd a vrátit hodnotu
				// true:
				if (editCode.setTextToFile(fileClass1.getPath(), textOfClass1))
					return true;
			}				
			
			
			/*
			 * Zde jsem nově doplnil, resp. upravil vztah typu agregace 1 : 1 na vztah
			 * asymetrické asociace, takže může být hrana s multiplicitou například 1 : 1
			 * nebo 5 : 3 apod. Takže musím dle této kardinality / (multiplicity) "projet"
			 * kód první třídy a smazat z ní například 5 atributů typu té druhé třídy a z
			 * kódu té druhé třídy musím smazat například 3 atributy typu té první třídy.
			 */
			for (int i = 0; i < countOfVarsInSrc; i++)
				textOfClass1 = deleteAttributeIfExist(textOfClass1, className2, false);

			for (int i = 0; i < countOfVarsInDes; i++)
				textOfClass2 = deleteAttributeIfExist(textOfClass2, className1, false);
			
			
			
			// Zde proběhlo vše v pořádku, mohu vložit kód zpět do tříd a vrátit hodnotu
			// true:
			return editCode.setTextToFile(fileClass1.getPath(), textOfClass1)
					&& editCode.setTextToFile(fileClass2.getPath(), textOfClass2);
		}
		else
			JOptionPane.showMessageDialog(null,
					missingClassText1 + fileClass1.getPath() + missingClassText2 + fileClass2.getPath(),
					missingClassTitle, JOptionPane.ERROR_MESSAGE);
		
		// sem se dostanu, pokud nastane nějaký problém:
		return false;
	}






	@Override
	public boolean deleteAggregation_1_ku_N(final String pathToSrc, final String cellValue1, final String cellValue2) {
		final String classNameWithPackage1 = cellValue1.replace(".", File.separator);
		final String classNameWithPackage2 = cellValue2.replace(".", File.separator);
		
		
		
		final File fileClass1 = new File(pathToSrc + File.separator + classNameWithPackage1 + ".java");
		final File fileClass2 = new File(pathToSrc + File.separator + classNameWithPackage2 + ".java");
		
		if (fileClass1.exists() && fileClass1.isFile() && fileClass2.exists() && fileClass2.isFile()) {
			// Zde obě třídy existují, mohu získat jejich kód:
			List<String> textOfClass1 = editCode.getTextOfFile(fileClass1.getPath());
			
			// Zjistím si pouze název třídy pro zjištění atributu:
			final String className2 = fileClass2.getName().substring(0, fileClass2.getName().lastIndexOf('.'));
			
			
			// Naný projdu obě kolekce - zdrojové kódy a pokud najdu odpovídající atribut, smažu ho:
			// Z PRVNÍ TŘÍDY MUSÍM SMAZAT REFERENCI NA DRUHOU TŘÍDU !!!:
			
			
			// List, do kterého uložím řádky, které se mají smazat
			// Kdybych si je neodkládal bokem, a mazal bych je přímo, tak kdyby byly dva listy hned pod sebou, smazal by se
			// první - OK, ale druhý by se přeskočil, protože by se změnil počet řádků
			List<String> listForDelete = new ArrayList<>();
			
			for (final String s : textOfClass1) {
				// Note - zde si druhou částí podmínky: isRequiredAttribute pomoho pro případ, že by se nerozponal
				// výraz z první části podmínky - isAttributeList
				if (isAttributeList(s, className2) || isRequiredAttribute(s, className2, true))
					listForDelete.add(s);
			}
			
			// Vymžu kolekci označených  řádků
			listForDelete.forEach(textOfClass1::remove);
			
			
			
			// Zde proběhlo vše v pořádku, mohu vložir kód zpět do tříd a vrátit hodnotu true:

			return editCode.setTextToFile(fileClass1.getPath(), textOfClass1);
		}
		else
			JOptionPane.showMessageDialog(null,
					missingClassText1 + fileClass1.getPath() + missingClassText2 + fileClass2.getPath(),
					missingClassTitle, JOptionPane.ERROR_MESSAGE);
		
		// sem se dostanu, pokud nastane nějaký problém:
		return false;
	}
	
	
	
	
	
	/**
	 * Metoda, která vymaže z hlavičky třídy implementaci rozhraní v paramtetru
	 * metody
	 * 
	 * Metoda najde hlavičku třídy, která implementuje rozhraní, pokud takovou
	 * hlavičku nenajde, jen vrátí původní text - třída již neimplementuje rozhraní,
	 * tak není co řešit
	 * 
	 * Pokud tuto hlavičku najde, uloží se její index Hlavičku si pak "rozkouskuji"
	 * po klíčové slovo implements (pokud existuje), za ním najdu název potřebného
	 * rozhraní a vymažu, zbytek ponechám, pokud to bylo jediné rozhraní, vymaže se
	 * i slovo implements
	 * 
	 * @param textOfClass
	 *            - text - zdrojový kód třídy, ze kterého se má vymazat implementace
	 *            rozhraní
	 * 
	 * @param className
	 *            - název třídy, ze kterého se má smazat rozhraní - abych našel
	 *            správnou hlavičku třídy
	 * 
	 * @param interfaceName
	 *            - název rozhraní pro smazání
	 * 
	 * @return kód třídy, bez implementace rozhraní
	 */
	private static  List<String> deleteImplementsFromHeader(final List<String> textOfClass, final String className,
			final String interfaceName) {
		// MUSÍ BÝT NÁZEV TŘÍDY, ABY BYLOMOŽNÉ NAJÍT HLAVIČKU !!
		
		
		// Zde si nechaám nějaké komentáře a původní verze výrazů, pro případ, že by se musely předělávat:
		
		// Najdu index hlavičky:
		final String regexHeaderClass = "^\\s*public\\s+((static\\s+)?(final\\s+)?|(final\\s+)?(static\\s+)?)?(abstract\\s+)?class\\s+" + className + "(\\s+extends\\s+\\w+)?\\s+implements\\s+[\\w,\\s]*\\{?\\s*$";
		
		
		
		final String regexHeaderInterface = "^\\s*public\\s+((static\\s+)?(final\\s+)?|(final\\s+)?(static\\s+)?)?(abstract\\s+)?interface\\s+" + className + "(\\s+extends\\s+\\w+)?\\s+implements\\s+[\\w,\\s]*\\{?\\s*$";
		
		
		final String regexHeaderAbstractClass = "^\\s*public\\s+((static\\s+)?(final\\s+)?|(final\\s+)?(static\\s+)?)?abstract\\s+class\\s+" + className + "(\\s+extends\\s+\\w+)?\\s+implements\\s+[\\w,\\s]*\\{?\\s*$";
		
		
		// enum neimplementuje rozhrani, tak není třeba testovat
		
		
		int headerIndex = -1;
		for (int i = 0; i < textOfClass.size(); i++) {
			if (textOfClass.get(i).matches(regexHeaderClass)) {
				headerIndex = i;
				break;
			}
			else if (textOfClass.get(i).matches(regexHeaderInterface)) {
				headerIndex = i;
				break;
			}
			else if (textOfClass.get(i).matches(regexHeaderAbstractClass)) {
				headerIndex = i;
				break;
			}
		}
		
		if (headerIndex > -1) {
			// Zde jsem našel hlavičku:
			
			// Nyní si zjistím, zda se jedná pouze o jedno rozhraní, abych odebral klíčové slovo implements - pokud je to 
			// jedno rozhraní to, které je třeba odebrat
			
			// Test, zda je hlavička ve tvaru:
			// dostupnost typ nazev implements X {		// Zde klíčové slovo odeberu, pokud se jedná o dané rozhraní k odebrání
			// nebo:
			// dostupnost typ nazev implements X, Y, S, ... { 	// zde odeberu pouze rozhraní, a zbytek nechám
			
			// Nejprve otestuji, zda se jedná o první variantu -  třída implementuje pouze jedno rozhran:
			final String regexImplements1 = "^[\\s*\\w+\\s+]+\\s*implements\\s+\\w+\\s*[{]?\\s*$";
			final String regexImplementsMore = "^[\\s*\\w+\\s+]+\\s*implements\\s+\\w+[\\w+,\\s*]*\\s*[{]?\\s*$";
			
			
			if (textOfClass.get(headerIndex).matches(regexImplements1)) {
				// Zde je za implements jedno rozhraní, tak ho otestuji:
				final String[] partsOfHeader = textOfClass.get(headerIndex).split("\\s");
				
				int implementsIndex = -1;
				for (int i = 0; i < partsOfHeader.length; i++) {
					if (partsOfHeader[i].replaceAll("\\s", "").equalsIgnoreCase("implements")) {
						implementsIndex = i;
						break;
					}
				}
				
				String newHeader = "";
				
				if (implementsIndex > -1) {
					// Načel jsem klíčové slovo implements, otestuji poslední rozhraní, zda je to které se má smazat:
					// Musím to projít posupně, kvůli mezerám:
					
					for (int q = implementsIndex; q < partsOfHeader.length; q++) {						

						String implementedInterface = partsOfHeader[q].replaceAll("\\s", "");
						
						// jestli končí se závorkou, tak ji odeberu:
						if (implementedInterface.endsWith("{"))
							implementedInterface = implementedInterface.substring(0, implementedInterface.length() - 1);
						
						// Otestuji, zda se jedná o dané rozhraní
						if (implementedInterface.equalsIgnoreCase(interfaceName)) {
						// Zde se jedná o interface, který mám smaszat:
						
						// tak vytvořím novou hlavičku bez interfacu.
						for (int i = 0; i < implementsIndex; i++)
							newHeader += partsOfHeader[i] + " ";
						
						newHeader += "{";
						}
						// else - zde se nejedná o potřebné rozhraní, tak ho nechám být						
					}
					
					// Zde mohu nastavit novou hlavičku:
					if (!newHeader.equals(""))
						textOfClass.set(headerIndex, newHeader);
				}
			}
			// Note: Zde bych druhou možnost testovat nemusel, - když to není první je to druhá, ale pro jistotu:
			else if (textOfClass.get(headerIndex).matches(regexImplementsMore)) {
				// Zde tedy třída implementuje více rozhraní, tak potřebuji odebrat, akorát to jedno:
				
				
				final String[] partsOfHeader = textOfClass.get(headerIndex).split(",");
				
				
				// Rozkouskuji první hodnotu v poli dle mezer:
				final String[] partsOfFirstIndex = partsOfHeader[0].split(" ");
				
				// Zkusím najít hledaný interface pro smazání:
				int interfaceIndex = -1;
				
				for (int i = 0; i < partsOfFirstIndex.length; i++) {
					if (partsOfFirstIndex[i].replaceAll("\\s", "").equalsIgnoreCase(interfaceName)) {
						interfaceIndex = i;
						break;
					}
				}
				
				// dle proměnné poznám, kdy jsem na konci edibral závorku , když ji odeberu, tak ji musím znvu přidat
				boolean addBracket = false;
				
				String newHeader = "";
				
				if (interfaceIndex > -1) {
					// Zde jsem našel hledaný interface, tak si vezmu text do tohoto interfacu a pripojím k nemu zbytek z pole celé hlavicky, protože v
					// tomto případě může být hledaný interface na posledním místě - když nepočítám mezery
					
					// Zde si zkopíruji celý začátek hlavičky:
					for (int i = 0; i < interfaceIndex; i++)
						newHeader += partsOfFirstIndex[i] + " ";
					
					// Zde připojím zbytek:
					for (int i = 1; i < partsOfHeader.length; i++)
						newHeader += partsOfHeader[i] + ", ";
					
					// Pdeberu poslední dva znaky = mezera a čárka
					newHeader = newHeader.substring(0, newHeader.length() - 2);		
				}
				
				// Zde jsem nenašel hledaný interface:
				// - nenachází se jako první v pořadí, tak musím prohledat zbytek:
				
				// Note: musím si hlídat i konec slova - čárky tam nebudou ale mohou tam být mezery nebo spojená závorka se slovem:
				else {
					// Prohledám zbytek pole (od jedničky), pokažde odeberu mezeru, bíle znaky a otestuji, zda nekončí závorkou
					// Pkud budou podmínky splněny, tak otestuji, zda se jedná o daný interface:
					
					for (int i = 1; i < partsOfHeader.length; i++) {
						String interfaceText = partsOfHeader[i].replaceAll("\\s", "");
						
						if(interfaceText.endsWith("{")) {
							// název interfacu bez závorky:
							interfaceText = interfaceText.substring(0, interfaceText.length() - 1);
							addBracket = true;
						}

						
						// nyní otestuji, zda se jedná o hledaný interface:
						if (interfaceText.equalsIgnoreCase(interfaceName)) {
							// pokud jsem ho našel, uložím si jeho index a mohu ukončit cyklus:
							interfaceIndex = i;
							break;
						}
					}
					
					// Zde jsem prošel celé pole s interfacy a otestuji, zda jsem našel hledaný interface či nikoliv:
					if (interfaceIndex > -1) {
						// Zde jsem našel hledaný interface:
						
						// Vytvořím novou hlavičku se začátekm až po daný index interfacu, a přidám zbytek - interface + 1:
						
						for (int i = 0; i < interfaceIndex; i++)
							newHeader += partsOfHeader[i] + ", ";
						
						// nyní zbytek:
						if (interfaceIndex + 1 < partsOfHeader.length) {
							for (int i = interfaceIndex + 1; i < partsOfHeader.length; i++)
								newHeader += partsOfHeader[i] + ", ";
						}
						
						// Odebrat poslední čárku a mezeru za ní:
						newHeader = newHeader.substring(0, newHeader.length() - 2);
						
						if (addBracket)
							newHeader += " {";
					}
					// else - zde jsem hledaný interface nenašel není třba dělat nic
				}

				// jestliže se hlavička upravila, tak ji změním
				if (!newHeader.equals(""))
					textOfClass.set(headerIndex, newHeader);
			}
			// jinak je to nějaká "vadná hlavička", tak vrátím původní text - kód
		}
		// Zde jsem hlavičku nenašel, tak vrátím původní text:
		
		
		return textOfClass;
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která z hlavičky třídy vymaže dědičnost Nejprve otestuje, zda se v
	 * hlavičce nachází klíčové slovo extends, pokud ne, nic nedělá, prostě vrátí
	 * původní text třídy - kód Pokud se slovo najde, tak podle mezer se rozkouskuje
	 * hlavicka, a sestavi se nova bude obsahovat kousky po slovo extends -1 (bez
	 * něj) a slova za extends +2 - třída ze ktere dědila se také odebere, dále muže
	 * a nemusí obsahovat implementaci, coz ponechám
	 * 
	 * @param textOfClass
	 *            - zdrojoý kód třídy
	 * 
	 * @return upravenoý zdrojový kód bez dědičnosti
	 */
	private static List<String> deleteExtendsFromHeader(final List<String> textOfClass, final String className) {
		// Výraz pro nalezení hlavičky třídy, která musí dědit, jinak se ve třídě
		// žádná hlavička třídy s dědičností nenachází, tak mohu vrátit původní koleic - dědičnost zrušena, resp. nenalezena:
		
		final String regexHeaderClass = "^\\s*public\\s+((static\\s+)?(final\\s+)?|(final\\s+)?(static\\s+)?)?(abstract\\s+)?class\\s+" + className + "\\s+extends\\s+\\w+[\\s\\w+,]*\\s*[{]?\\s*$";
		
		
		final String regexHeaderInterface = "^\\s*public\\s+((static\\s+)?(final\\s+)?|(final\\s+)?(static\\s+)?)?(abstract\\s+)?interface\\s+" + className + "\\s+extends\\s+\\w+[\\s\\w+,]*\\s*[{]?\\s*$";
		
		final String regexHeaderAbstractClass = "^\\s*public\\s+((static\\s+)?(final\\s+)?|(final\\s+)?(static\\s+)?)?abstract\\s+class\\s+" + className + "\\s+extends\\s+\\w+[\\s\\w+,]*\\s*[{]?\\s*$";
		
		

		// enum dědit nemůže, ten zde ani netestuji
		
		// Zjistím si index dědící hlavičky třídy - pokud existuje
		int indexOfHeader = -1;
		for (int i = 0; i < textOfClass.size(); i++) {
			if (textOfClass.get(i).matches(regexHeaderClass)) {
				indexOfHeader = i;
				break;
			}	
			else if (textOfClass.get(i).matches(regexHeaderInterface)) {
				indexOfHeader = i;
				break;
			}	
			else if (textOfClass.get(i).matches(regexHeaderAbstractClass)) {
				indexOfHeader = i;
				break;
			}	
		}	
		
		if (indexOfHeader > -1) {
			final String[] partsOfHeader = textOfClass.get(indexOfHeader).split(" ");			
			
			// Zjistím si index slova extends:
			int extendsIndex = -1;
			for (int i = 0; i < partsOfHeader.length; i++) {
				if (partsOfHeader[i].equalsIgnoreCase("extends")) {
					extendsIndex = i;
					break;
				}				
			}
			
			
			// Note:
			// Následující kód selže, pokud za slovem extends bude různá kombinace mezer tabulátorů - kombinace prázdných znaků
			// V takovém případě, se smaže slovo extends, ale třída, že které původně dědila, tam pořád bude
			String newHeader = "";
			
			if (extendsIndex > -1) {
				for (int i = 0; i < extendsIndex; i++)
					newHeader += partsOfHeader[i] + " ";
				
				if (extendsIndex + 2 < partsOfHeader.length) {
					for (int i = extendsIndex + 2; i < partsOfHeader.length; i++) {
						if (!partsOfHeader[i].matches("\\s"))
							newHeader += partsOfHeader[i] + " ";
					}
						
				}
				
				// Nastavím novou hlavičku:
				textOfClass.set(indexOfHeader, newHeader);
			}
		}
		
		return textOfClass;
	}
	
	
	
		
	
	
	
	private static boolean isAttributeList(final String text, final String className) {
		// List může bát v syntaxi:
		// dostupnost = private, public, protected (static <-> final)
		
		// dostupnost (packages.)List< (package.) ClassName> variable;
		// nebo:
		// dostupnost (package.)List< (package.) ClassName> variable, variable2, ...;
		
		// Dále List s deklaraci: ta ale neni "dokonalá" nemohl jsem vypsat všechny mozne instance listu (arraylist, linkedlist, vectory, ...)
		// tak jsem to "nechal pouze na mozných vyskytech znaku v dane syntaxi"
		
		
		// a pole at uz jednorozmerne nebo vicerozmerne pole, jak deklarace, tak i s inicializaci:
		// dostupnost ClassName []([] [] ...) someName  (, var2, ...);
		// nebo s inicializace:
		// dostupnost (package.)ClassName [] ([], ...) someName = new ClassName [size] ([size2], ...);
		// nebo:
		// dostupnost (package.)ClassName [] ([], ...) someName = new ClassName [] ([], ...) {};
		// nebo:
		// dostupnost (package.)ClassName [] ([], ...) someName = new ClassName [] ([], ...) {new ClassName(parameters), new package.ClassName(), ...};
		
		
				
		final String regExForList = "^\\s*(private\\s+|public\\s+|protected\\s+)?((static\\s+)?(final\\s+)|final\\s+)?(static\\s+)?(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?List\\s*<\\s*" + "(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?" + className + "\\s*>\\s*(\\w*\\s*|\\w+\\s*\\,\\s*\\w+\\s*[\\w\\,\\s]*)\\s*;?\\s*$";
		
		
		final String regExForDeclarationList = "^\\s*(private\\s+|public\\s+|protected\\s+)?((static\\s+)?(final\\s+)|final\\s+)?(static\\s+)?(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?List\\s*<\\s*" + "(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?" + className + "\\s*>\\s*\\w+\\s*(=\\s*new\\s+(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?\\w+\\s*<\\s*\\w*\\s*>\\s*[(]\\s*('.'|\".*\"|true|false|null|[<>\\w\\s(),\\.'\"\\+-])*\\s*[)])\\s*;?\\s*$";
		
		
		
		
		
		// dostupnost (packages.) ClassName [] ([]...) variableName  (, var2, var3, ..); - OK:
		final String regExForArrayDeclaration = "^\\s*(private\\s+|public\\s+|protected\\s+)?((static\\s+)?(final\\s+)|final\\s+)?(static\\s+)?(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?" + className + "\\s*(\\s*\\[\\s*\\]\\s*)+\\s*(\\w+\\s*|\\w+\\s*\\,\\s*\\w+\\s*(\\,\\s*\\w+\\s*)*)\\s*;?\\s*$";
		
		
		
		// dostupnost (package.)ClassName [] ([], ...) someName (, someName2) = new (package.)ClassName [size] ([size2], ...);
		final String regExForArrayInitWithNumber = "^\\s*(private\\s+|public\\s+|protected\\s+)?((static\\s+)?(final\\s+)|final\\s+)?(static\\s+)?(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?" + className + "\\s*(\\s*\\[\\s*\\]\\s*)+\\s*\\w+(\\s*,\\s*\\w+\\s*)*\\s*\\=\\s*new\\s+(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?\\w+\\s*(\\s*\\[\\s*\\d+\\s*\\]\\s*)+\\s*;?\\s*$";
		
		
		
		
		// dostupnost (package.)ClassName [] ([], ...) someName (, someName2) = new  (packages.) ClassName [] ([], ...) { zde prazdno nebo s parametry};
		final String regExForArrayInitWithFullfillment = "^\\s*(private\\s+|public\\s+|protected\\s+)?((static\\s+)?(final\\s+)|final\\s+)?(static\\s+)?(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?" + className + "\\s*(\\s*\\[\\s*\\]\\s*)+\\s*\\w+(\\s*,\\s*\\w+\\s*)*\\s*\\=\\s*new\\s+(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?\\w+\\s*(\\s*\\[\\s*\\]\\s*)+\\s*(\\s*\\{\\s*.*\\s*\\}\\s*)\\s*;?\\s*$";
		

		
		
		
		// dostupnost (package.)ClassName [] ([], ...) someName  (, someName2) = {  zde parametry nebo prazdno  };
		final String regExForArrayInitWithFullfillment2 = "^\\s*(private\\s+|public\\s+|protected\\s+)?((static\\s+)?(final\\s+)|final\\s+)?(static\\s+)?(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?" + className + "\\s*(\\s*\\[\\s*\\]\\s*)+\\s*\\w+(\\s*,\\s*\\w+\\s*)*\\s*\\=\\s*(\\s*\\{\\s*.*\\s*\\}\\s*)\\s*;?\\s*$";



		return text.matches(regExForList) || text.matches(regExForDeclarationList)
				|| text.matches(regExForArrayDeclaration) || text.matches(regExForArrayInitWithNumber)
				|| text.matches(regExForArrayInitWithFullfillment) || text.matches(regExForArrayInitWithFullfillment2);
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vymaže z listu - zdrojový kód třídy příslušný atribut
	 * 
	 * @param textOfClass
	 *            - Zdrojový kód třídy - jedna hodnota v kolekci = jeden řádek ve
	 *            třídě
	 * 
	 * @param className
	 *            - název třídy - čistý - bez balíčků
	 * 
	 * @param isAggregation_1_N
	 *            = logická proměnná o tom, zda se jedná o proměnnou pro smazání
	 *            typu agregace 1 : N, tj. pole nebo list (true), jinak bude tato
	 *            proměnná false a tj "normální" proměnná
	 * 
	 * @return upravenou kolekci, resp. kolekci - zdrojový kód bez příslušných
	 *         atributů
	 */
	private static List<String> deleteAttributeIfExist(final List<String> textOfClass, final String className,
			final boolean isAggregation_1_N) {
		// Musím použít tento typ cyklu, protože mažu testovaný objekt, v ostatních cykles for toto nelze
		// nastala byvyjímka  ConcurrentModificationException
		
		// Musím odkládat řádky, které chci smazat do  jiné kolekce, protože kdyby byly dva atributy pod sebou
		// tak by se první smazal v pohodě, ale druhý by se přeskočil
		List<String> listForDelete = new ArrayList<>();

		// Cyklus prohledá celý zdrojový kód třídy, a pokud najde požadovaný atribut pro smazání,
		// tak ho přidá do kolekce, a cylus skončí - jedná se zde o smazání pouze jednoho atributu, který
		// reprezentuje asociaci nebo agregaci 1 : 1, takže smazat pouze jeden atribut a zbytek nechat, protože tam
		// těchto vztahů může být více
		
		for (final String s : textOfClass) {
			if (isRequiredAttribute(s, className, isAggregation_1_N)) {
				listForDelete.add(s);
				// Zde jsem našel syntaxi atributu, který odpovídá regulárnímu výrazu pro typ proměnné coby reference na 
				// nějaku třídu, tak tento řádek přidám do kolekce a skončím cyklus:
				break;
			}				
		}
		
		
		// nyní mohu smaazt označené řádky:
		listForDelete.forEach(textOfClass::remove);
		
		
		return textOfClass;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se zadaný text - hodnota nachází ve správné
	 * syntaxi atrinutu třídy pro smazání Konkrétně: dostupnost název třídy
	 * NázevPromenne; apod, viz syntaxe - regulární výrazy v metodě
	 * 
	 * @param textAtRow
	 *            - řádek zdrojového kódu
	 * 
	 * @param className
	 *            - název třídy - čistý bez balíčků
	 * 
	 * @param isAggregation_1_N
	 *            - logická proměnná o tom, zda se jedná o proměnnou typu Agregace
	 *            1:n, protože v takovém případě je třeba si hlídat pole nebo list,
	 *            jinak jsou to klasické proměnné
	 * 
	 * @return true, pokud se na daném řádku nachází zvolená syntaxe znaků - hledaný
	 *         atribut třídy, jinak false
	 */
	private static boolean isRequiredAttribute(final String textAtRow, final String className,
			final boolean isAggregation_1_N) {
		// Note:
		// Jsou zde napsány regulární výrazy, které rozpoznávají pouze základní syntaxe,
		// nejsou zde žádné porobné syntaxe, jako je například naplnění u deklarace listu třeba nějakou metodou,
		// apod. - opravdu jen ty nejčastější případy
		
		
		// Hodnoty mohou být v syntaxi:
		// dostupnost ClassName variable;
		// nebo: dostupnost ClassName variable1, variable2, ... ;
		
		// Dále instanci: dostupnost ClassName variableName = new xxx();
		// Zde nemohu za xxx dosadit konkrétní název, protoze se muze jednat o :
		// Interface var = new ClassName(); - trida ClassName implementuje dané rozhraní
		
		
		
		
		// Note: středník může být i na následujícím řádku, proto není povinný, ale neměl by tam být, měl by být hne za výrazem
		// Ale regulární výraz by to již nerozpoznal:
			
		// Note:
		// Dále výraz předpokládá, že klíčové slovo pro dostupnost bude na začátku, tj. 
		// private neco nebo public neco ...
		// pokud bude až třba na druhém místě ,například:
		// static private int i; -> apod. tak tato syntaxe opět výrazem neprojde - už se mi nechtělo vypisovat všechny možné varianty
		// pro rozpoznávání syntaxí a navíc bych je ani nepokryl všechny, tak jsem nechal jěn pár základních, viz níže
		
		
		// Pro: dostupnost (package.)ClassName var1, var2, ... (;)
		final String regExForDeclarationVar = "^\\s*(private\\s+|public\\s+|protected\\s+)?((static\\s+)?(final\\s+)|final\\s+)?(static\\s+)?(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?" + className + "\\s+(\\w*\\s*|\\w+\\s*\\,\\s*\\w+\\s*[\\w\\,\\s]*)\\s*;?\\s*$";
		
		
		// Pro dostupnost (package.)ClassName var = new (package.) xxx(mohou byt i parametry);
		final String regExForDeclarationVarWithIntance = "^\\s*(private\\s+|public\\s+|protected\\s+)?((static\\s+)?(final\\s+)|final\\s+)?(static\\s+)?(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?" + className + "\\s+\\w+\\s*=\\s*new\\s+(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?\\w+\\s*[(]\\s*('.'|\".*\"|true|false|null|[<>\\w\\s(),\\.'\"\\+-])*\\s*[)]\\s*;?\\s*$";
		
		
		
		// pro výšet 
		// naplnení například jinou proměnnou apod.
		// dostupnost ClassName var = variable;
		// dostupnost ClassName var = variable.var;
		// dostupnost ClassName var = model.data.Vycet.Value;
		final String regExForEnumEtc = "^\\s*(private\\s+|public\\s+|protected\\s+)?((static\\s+)?(final\\s+)|final\\s+)?(static\\s+)?(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?" + className + "\\s+\\w+\\s*=\\s*\\w+(\\s*\\.\\s*\\w+)*\\s*;?\\s*$";
		
		
		
		
		
		// Pro 1 : N
		
		
		
		// instanciace listu:
		// Dále například na instanci ArrayListu pro vztah agregace 1 : N, syntaxe:
		// public static final List<ClassName> classNamesList = new java.util.ArrayList<>(java.util.Arrays.asList(new ClassNames[] {new ClassName()}));
		// apod.
		
		// Note - na konci výrazu je tečka coby libovolné znaky a je to proto, že se může lišti syntaxe pro Arraylist, Linked list, ...
		// stejně tak parametry v jejich konstroktorech a nevím co vše by tam uživateli mohl napsat, tak jsem si to takto zjednodušil
		final String regExForList = "^\\s*(private\\s+|public\\s+|protected\\s+)?((static\\s+)?(final\\s+)|final\\s+)?(static\\s+)?List\\s*<\\s*(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?" + className + "\\s*>\\s*\\w+\\s*=\\s*new\\s+(\\s*\\w+(\\s*\\.\\s*\\w+)*)*\\w+(\\s*<\\s*>\\s*)?[(].*[)]\\s*;?\\s*$";
		
		
		// Deklarace listu:
		// pubilc ... List<ClassName> xxx, YYY, ... (;)
		// apod.
		final String regExForListDeclaration = "^\\s*(private\\s+|public\\s+|protected\\s+)?((static\\s+)?(final\\s+)|final\\s+)?(static\\s+)?List\\s*<\\s*(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?" + className + "\\s*>\\s*\\w+(\\s*,\\s*\\w+)*\\s*;?\\s*$";
		
		
		// pole
		
		// Deklarace pole
		// public ClassName[][][]... x (, y, z, ...);
		final String regExForArrayDeclaration = "^\\s*(private\\s+|public\\s+|protected\\s+)?((static\\s+)?(final\\s+)|final\\s+)?(static\\s+)?(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?" + className + "\\s*\\[\\s*\\](\\s*\\[\\s*\\]\\s*)*\\s*\\w+(\\s*,\\s*\\w+)*\\s*;?\\s*$";
		
		
		
		
		// Inicializace pole:
		// ClassName[][] var = new ClassName [size] ;
		final String regExForArrayInicialization = "^\\s*(private\\s+|public\\s+|protected\\s+)?((static\\s+)?(final\\s+)|final\\s+)?(static\\s+)?(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?" + className + "\\s*\\[\\s*\\](\\s*\\[\\s*\\]\\s*)*\\s*\\w+\\s*=\\s*new\\s+\\w+(\\s*\\.\\s*\\w+)*\\s*\\[\\s*\\d+\\s*\\]\\s*;?\\s*$";
		
		
		
		
		// private ClassName[] var = {...};
		// Note:
		// Zde opět nevím co se zadá v špičatých závorkách, proto tečka coby libovolné znaky
		final String regExForArrayInicialization_2 = "^\\s*(private\\s+|public\\s+|protected\\s+)?((static\\s+)?(final\\s+)|final\\s+)?(static\\s+)?(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?" + className + "\\s*\\[\\s*\\](\\s*\\[\\s*\\]\\s*)*\\s*\\w+\\s*=\\s*\\{.*\\}\\s*;?\\s*$";
		
		
		
		
		// protected ClassName[][] var = new ClassName[] {...};
		// Note> 
		// Na konci v poslední špičaté závorce může být opět cokoliv, tak tečka coby libovolná znak,
		// a ten druhý název třídy "new CLassName" - před ním je třeba dát opět balíčky a navíc se může jednat o nějakou verzi polymorfismu,
		// tak ani nebudu dávat ten samá název
		final String regExForArrayInicialization_3 = "^\\s*(private\\s+|public\\s+|protected\\s+)?((static\\s+)?(final\\s+)|final\\s+)?(static\\s+)?(\\w+\\s*\\.\\s*(\\w+\\s*\\.\\s*)*)?" + className + "\\s*\\[\\s*\\](\\s*\\[\\s*\\]\\s*)*\\s*\\w+\\s*=\\s*new\\s+(\\w+\\s*\\.\\s*)*\\w+\\s*\\[\\s*\\]\\s*\\{.*\\}\\s*;?\\s*$";
		
		
		
		
			
		if (!isAggregation_1_N) {
			// Zde se testuji "normální" proměnné
			if (textAtRow.matches(regExForDeclarationVar))
				return true;
			
			else if (textAtRow.matches(regExForDeclarationVarWithIntance))
				return true;
			
			else if (textAtRow.matches(regExForEnumEtc))
				return true;
		}
		
		
		else {
			// Zde je řeba otestovat list nebo pole
			if (textAtRow.matches(regExForList))
				return true;
			
			else if (textAtRow.matches(regExForListDeclaration))
				return true;
			
			else if (textAtRow.matches(regExForArrayDeclaration))
				return true;
			
			else if (textAtRow.matches(regExForArrayInicialization))
				return true;
			
			else if (textAtRow.matches(regExForArrayInicialization_2))
				return true;
			
			else if (textAtRow.matches(regExForArrayInicialization_3))
				return true;
		}
				
		
		return false;
	}






	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			missingClassTitle = properties.getProperty("EcMissingClassTitle", Constants.ED_MISSING_CLASS_TITLE);
			missingClassText1 = properties.getProperty("EcMissingClassText_1", Constants.ED_MISSING_CLASS_TEXT_1) + ": ";
			missingClassText2 = properties.getProperty("EcMissingClassText_2", Constants.ED_MISSING_CLASS_TEXT_2) + ": ";
		}
		
		
		else {
			missingClassTitle = Constants.ED_MISSING_CLASS_TITLE;
			missingClassText1 = Constants.ED_MISSING_CLASS_TEXT_1 + ": ";
			missingClassText2 = Constants.ED_MISSING_CLASS_TEXT_2 + ": ";
		}
	}
}