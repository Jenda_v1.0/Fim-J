package cz.uhk.fim.fimj.menu;

import java.awt.event.*;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;

import cz.uhk.fim.fimj.file.*;
import cz.uhk.fim.fimj.language.EditProperties;
import cz.uhk.fim.fimj.popup_menu.PopupMenuScroller;
import cz.uhk.fim.fimj.preferences.OpenedProject;
import cz.uhk.fim.fimj.preferences.OpenedProjectPreferencesHelper;
import cz.uhk.fim.fimj.redirect_sysout.RedirectApp;
import cz.uhk.fim.fimj.swing_worker.ThreadSwingWorker;
import org.apache.commons.io.FileUtils;
import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.app.CheckCompileFiles;
import cz.uhk.fim.fimj.app.InfoAboutWorkspace;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.class_diagram.RetrieveNotLoadedClassesThread;
import cz.uhk.fim.fimj.class_diagram.CheckRelationShipsBetweenClassesThread;
import cz.uhk.fim.fimj.code_editor.CodeEditorDialog;
import cz.uhk.fim.fimj.commands_editor.Editor;
import cz.uhk.fim.fimj.font_size_form.FontSizeForm;
import cz.uhk.fim.fimj.font_size_form.FontSizeFormType;
import cz.uhk.fim.fimj.forms.about_app_form.AboutAppForm;
import cz.uhk.fim.fimj.forms.ClassFromFileForm;
import cz.uhk.fim.fimj.forms.InfoForm;
import cz.uhk.fim.fimj.forms.JavaHomeForm;
import cz.uhk.fim.fimj.forms.NewProjectForm;
import cz.uhk.fim.fimj.forms.SelectMainMethodForm;
import cz.uhk.fim.fimj.forms.WorkspaceForm;
import cz.uhk.fim.fimj.instance_diagram.CheckRelationShipsBetweenInstances;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;
import cz.uhk.fim.fimj.instances.Instances;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.output_editor.OutputEditor;
import cz.uhk.fim.fimj.preferences.PreferencesApi;
import cz.uhk.fim.fimj.print.PrintGraph;
import cz.uhk.fim.fimj.project_explorer.ProjectExplorerDialog;
import cz.uhk.fim.fimj.settings_form.SettingsForm;

/**
 * Třída slouží jako menu v hlavním okně aplikace
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class Menu extends JMenuBar implements ActionListener, LanguageInterface {

    private static final long serialVersionUID = 2742347121810091697L;

    /**
     * Počet položek (JMenuItem), které budeou zobrazeny jako, že se mají scrolovat.
     * <p>
     * Například,když jich bude v menu celkem 20, a tato proměnná bude nastavena na 5, tak jich bude na začátku a na
     * konci definovaný počet, a těch 5 položek bude vždy vidět tak, že se budou posouvat.
     * <p>
     * Konkrétní příkaz u proměnné: cz.uhk.fim.fimj.popup_menu.PopupMenuAbstract#SCROLL_COUNT
     */
    private static final int SCROLL_COUNT = 15;

    /**
     * Interval ve scrolování mezi položkami v příslušném JMenu. V podstatě se jedná o rychlost scrolování mezi
     * položkami, když myši najedem na šipku pro posunutí položek v menu.
     */
    private static final int SCROLL_INTERVAL = 150;
    /**
     * Jedná se o počet položek, které by byly pevně umístěny v příslušném menu v horní části. Například by byly třeba 3
     * pevně dané položky, poté SCROLL_COUNT položek, mezi kterými se bude srcolovat při najetí myši na šipku a pak zase
     * nějaký definovaný počet, který bude jako pevný počet položek, mezi kterými se nebude srcolovat.
     */
    private static final int TOP_FIXED_COUNT = 0;
    /**
     * Počet pevně definovaných položek ve spodní části příslušného menu. Nepůjde mezi nimi scrolovat.
     */
    private static final int BOTTOM_FIXED_COUNT = 4;



    /**
     * Výčtová proměnná, která značí pořadí, v jakém se mají řadit položky (projekty) v rozevíracím menu pro dříve
     * otevřené projekty.
     */
    private static SortRecentlyOpenedProjects sortRecentlyOpenedProjects;







    // Jednotlivá menu:
	private static JMenu menuProject, menuEdit, menuTools, menuHelp;
	
	// Položky do menuProject:
    private static JMenuItem itemNewProject, itemOpenProject, itemCloseProject, itemSave, itemSaveAs,
            itemSwitchWorkspace, itemRestart, itemExit;
    // Menu pro dříve otevřené projekty.
    private static JMenu menuRecentlyOpenedProjects;
    /**
     * Vymazání z preferencí dříve otevřené projekty:
     */
    private static JMenuItem itemDeleteRecentlyProjects;
    /**
     * Komponenta pro nastavení řazení položek v menu pro dříve otevřené projekty sestupně dle datumu otevření
     * projektu.
     */
    private static JRadioButtonMenuItem rbItemSortRecentlyProjectsAsc;
    /**
     * Komponenta pro nastavení řazení položek v menu pro dříve otevřené projekty vzestupně dle datumu otevření
     * projektu.
     */
    private static JRadioButtonMenuItem rbItemSortRecentlyProjectsDes;

	
	// Položky do menu Edit:
	private static JMenuItem itemAddClassFromFile, itemRemove, itemSaveImageOfClassDiagram,
			itemSaveImageOfInstanceDiagram, itemPrintClassDiagram, itemPrintInstanceDiagram, itemRefreshClassDiagram,
			itemRefreshInstanceDiagram, itemRemoveAll, itemRemoveInstanceDiagram;
	
	
	// Položky do menu Tools:
	private static JMenuItem itemCompile, itemShowOutputFrame, itemStart, itemSettings, itemFontSize, itemJavaSettings,
			itemJavaSearch;
	
	
	/**
	 * Rozervírací menu, které obsahuje dvě položky a sice pro otevření nějakého
	 * souboru, který mí uživatel kdekoliv na disku v průzkumníku projektů nebo v
	 * editoru kódu.
	 */
	private static JMenu menuOpenInEditor;

    /**
     * Otevření vybraného souboru v dialogu editor zdrojového kódu.
     */
    private static JMenuItem itemOpenFileInCodeEditor;
    /**
     * Otevření vybraného souboru v dialogu průzkumník projektů.
     */
    private static JMenuItem itemOpenFileInProjectExplorer;
	
	
	/**
	 * Položke, která slouží jako "rozevírací menu" v menu Tools.
	 */
	private static JMenu menuItemJava;
	
	
	// Zde bude vnořené menu, pro označení, zda se má otevřít bud dialog coby průzkumník projěktu s kořenovým 
	// adresářem ve Workspace nebo s kořenovým adresářem coby adresář otevřeného projektu:
	private static JMenu menuProjectExplorer;
	
	// Položky do menu: menuProjectExplorer:
	private static JMenuItem itemPeWorkspace, itemPeOpenProject;


    /**
     * Rozevírací menu pro položky, které slouží pro otevření průzkumníku souborů definovaného v OS. V tom průzkumníku
     * se otevře buď adresář workspace nebo otevřený projekt.
     */
    private static JMenu menuOpenInFileExplorer;

    /**
     * Otevření adresáře workspace v průzkumníku souborů nastaveného v OS.
     */
    private static JMenuItem itemFeWorkspace;
    /**
     * Otevření adresáře otevřeného projektu v aplikaci v průzkumníku souborů nastaveného v OS.
     */
    private static JMenuItem itemFeOpenProject;
	
	
	// Položky do menu Help:
	private static JMenuItem itemInfo, itemAboutApp;
	
	private final GraphClass classDiagram;
	
	private final GraphInstance instanceDiagram;
	
	
	/**
	 * Reference na hlavní okno aplikace je zde potřeba, abych aplikace věděla, kdy
	 * se mají "zpřístupnit" položky v levém menu, v menu aplikace a grafy a kdy se
	 * maji naopak tyto komponenty znepřístupnit, proto se pomocí této reference
	 * zavolá metoda: enableButtons pro zpřístupnění / znepřístupnění těchto
	 * komponent
	 */
	private final App app;
	
	
	/**
	 * Do následující proměnné se uloži načtený jazyk zesoubour sem se uloži po
	 * zavolání metoda setLanguage() na menu tento jazyk se dále předává do
	 * ostatních oken kde jsou nějaké text
	 */
	private Properties languageProperties;
	
	
	
	
	// Proměnné do chybových hlášek po kliknutí na polozku v menu:
    private static String missingDirErrTitle, missingDirErrText, identicalDirErrTitle, identicalDirErrText,
            missingClassFileTitle, missingClassFileText, missingSrcDirText, missingSrcDirTitle,
            errorWhileCallTheMainMethod, txtClass, txtError, methodIsNotaccessible,
            txtIllegalArgumentExceptionPossibleErrors, errorWhileCallingMainMethod, txtMainMethodNotFoundText,
            txtMainMethodNotFoundTitle, txtChooseClassOrTxtDialogTitle, txtErrorWhilePrintingsClassDiagramText,
            txtErrorWhilePrintingsClassDiagramTitle, txtFailedToPrintClassDiagramText, txtClassDiagram,
            txtFailedToPrintClassDiagramTitle, txtErrorWhilePrintingInstanceDiagramText,
            txtErrorWhilePrintingInstanceDiagramTitle, txtFailedToPrintInstanceDiagramText, txtInstanceDiagram,
            txtFailedToPrintInstanceDiagramTitle, txtErrorWhileCopyFilesText, txtPossibleFileIsBeingUsed, txtSource,
            txtDestination, txtErrorWhileCopyFilesTitle,
    // Texty pro menu pro ProectExplorer
    txtPathToWorkspaceDoNotFoundText, txtPathToWorkspaceDoNotFoundTitle,

    txtOpenDirDoesNotExistText, txtOriginalLocation, txtOpenDirDoesNotExistTitle, txtProjectIsNotOpenText,
            txtProjectIsNotOpenTitle, txtClassDiagramDoesntContainClassText, txtClassDiagramDoesntContainClassTitle,

    // Dotaz na restart aplikace:
    txtSwitchWorkspaceJopRestartAppNowText, txtSwitchWorkspaceJopRestartAppNowTT,
    // Chyba při získávání adresáře workspace nebo otevřeného projektu pro otevření průzkumníku souborů:
    txtPathToWorkspaceWasNotFoundText, txtPathToWorkspaceWasNotFoundTitle,
            txtPathToOpenProjectWasNotFoundText, txtPathToOpenProjectWasNotFoundTitle;
			
	
	
	
	/**
	 * Promenná pro uchování reference na command editor - když změním nastavení v
	 * dialogu pro nastavení, tak aby se změn projevily bez restartovani aplikace
	 */
	private final Editor commandEditor;
	
	
	/**
	 * Proměnná, ve které uchovám referenci na OutputEditor, abych mohl při změné
	 * nastavení změnit to nastavní
	 */
	private final OutputEditor outputEditor;
	
	
	
	
	public Menu(final GraphClass classDiagram, final GraphInstance instanceDiagram, final App app,
			final Editor commandEditor, final OutputEditor outputEditor) {
		
		this.outputEditor = outputEditor;
		this.commandEditor = commandEditor;
		this.classDiagram = classDiagram;
		this.instanceDiagram = instanceDiagram;
		this.app = app;
		
		
		createMenu();
				
		createMenuItems();

        // Označím si radiobuttony, které definují způsob řazení položek reprezentující dříve otevřené projekty:
        prepareRbsForSortingRecentlyProjects();

		// Přidám do menu dříve otevřené projekty:
        createRecentlyOpenedProjectsItems();
		
		createShortcuts();
		
		// Přidání ikon tlačítkům v menu:
		addIconToItems();
	}
	
	
	
	
	
	
	/**
	 * Metoda, která každému tlačítku v menu přidá ikonu
	 */
	private static void addIconToItems() {
		itemNewProject.setIcon(GetIconInterface.getMyIcon("/icons/app/NewProjectIcon.png", true));
		
		itemOpenProject.setIcon(GetIconInterface.getMyIcon("/icons/app/OpenProjectIcon.png", true));

        menuRecentlyOpenedProjects.setIcon(GetIconInterface.getMyIcon("/icons/app/RecentlyOpenedProjectsIcon.png", true));

        itemDeleteRecentlyProjects.setIcon(GetIconInterface.getMyIcon("/icons/app/CleanHistoryIcon.png", true));

		itemCloseProject.setIcon(GetIconInterface.getMyIcon("/icons/app/CloseProjectIcon.png", true));
		
		itemSave.setIcon(GetIconInterface.getMyIcon("/icons/app/SaveIcon.png", true));
		
		itemSaveAs.setIcon(GetIconInterface.getMyIcon("/icons/app/SaveAsIcon.png", true));
		
		itemSwitchWorkspace.setIcon(GetIconInterface.getMyIcon("/icons/app/SwitchWorkspaceIcon.png", true));
		
		itemRestart.setIcon(GetIconInterface.getMyIcon("/icons/app/RestartAppIcon.png", true));
		
		itemExit.setIcon(GetIconInterface.getMyIcon("/icons/app/CloseIcon.png", true));
		
		itemAddClassFromFile.setIcon(GetIconInterface.getMyIcon("/icons/app/AddJavaClassIcon.png", true));
		
		itemRemove.setIcon(GetIconInterface.getMyIcon("/icons/app/RemoveIcon.png", true));
		
		itemSaveImageOfClassDiagram.setIcon(GetIconInterface.getMyIcon("/icons/app/SaveImageOfDiagram.png", true));
		
		itemSaveImageOfInstanceDiagram.setIcon(GetIconInterface.getMyIcon("/icons/app/SaveImageOfDiagram.png", true));
		
		itemPrintClassDiagram.setIcon(GetIconInterface.getMyIcon("/icons/app/PrintDiagramIcon.jpg", true));
		
		itemPrintInstanceDiagram.setIcon(GetIconInterface.getMyIcon("/icons/app/PrintDiagramIcon.jpg", true));
		
		itemRefreshClassDiagram.setIcon(GetIconInterface.getMyIcon("/icons/app/RefreshDiagramIcon.jpg", true));
		
		itemRefreshInstanceDiagram.setIcon(GetIconInterface.getMyIcon("/icons/app/RefreshDiagramIcon.jpg", true));
		
		itemRemoveAll.setIcon(GetIconInterface.getMyIcon("/icons/app/RemoveAllIcon.jpg", true));
		
		itemRemoveInstanceDiagram.setIcon(GetIconInterface.getMyIcon("/icons/app/RemoveIcon.png", true));
		
		itemOpenFileInCodeEditor.setIcon(GetIconInterface.getMyIcon("/icons/app/CodeEditorIcon.png", true));

		itemOpenFileInProjectExplorer.setIcon(GetIconInterface.getMyIcon("/icons/projectExplorerIcons/ProjectExplorerDialogIcon.gif", true));

		itemShowOutputFrame.setIcon(GetIconInterface.getMyIcon("/icons/outputFrameIcons/OutputFrameIcon.png", true));
		
		itemCompile.setIcon(GetIconInterface.getMyIcon("/icons/app/CompileIcon.png", true));
		
		itemSettings.setIcon(GetIconInterface.getMyIcon("/icons/app/SettingsIcon.png", true));
		
		itemFontSize.setIcon(GetIconInterface.getMyIcon("/icons/app/FontSizeIcon.png", true));
		
		itemJavaSettings.setIcon(GetIconInterface.getMyIcon("/icons/app/JavaSettingsIcon.png", true));
		
		itemJavaSearch.setIcon(GetIconInterface.getMyIcon("/icons/app/JavaSearchCompileFilesIcon.png", true));
		
		itemPeOpenProject.setIcon(GetIconInterface.getMyIcon("/icons/projectExplorerIcons/ProjectExplorerDialogIcon.gif", true));
		
		itemPeWorkspace.setIcon(GetIconInterface.getMyIcon("/icons/projectExplorerIcons/ProjectExplorerDialogIcon.gif", true));

        itemFeWorkspace.setIcon(GetIconInterface.getMyIcon("/icons/app/FileExplorerIcon.png", true));
        itemFeOpenProject.setIcon(GetIconInterface.getMyIcon("/icons/app/FileExplorerIcon.png", true));

        itemStart.setIcon(GetIconInterface.getMyIcon("/icons/app/StartIcon.png", true));
		
		itemInfo.setIcon(GetIconInterface.getMyIcon("/icons/app/InfoIcon.png", true));
		
		itemAboutApp.setIcon(GetIconInterface.getMyIcon("/icons/app/AuthorIcon.png", true));
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá ke všem položka v Jmenu klávesové zkratky
	 */
	private static void createShortcuts() {
		// CTRL + N:
		itemNewProject.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
		
		// CTRL + O:
		itemOpenProject.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));
		
		// CTRL + W:
		itemCloseProject.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_DOWN_MASK));
		
		// CTRL + S:
		itemSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
		
		// CTRL + SHIFT + S:
		itemSaveAs.setAccelerator(KeyStroke.getKeyStroke("control shift S"));
		
		// CTRL + ALT + S
		itemSwitchWorkspace.setAccelerator(KeyStroke.getKeyStroke("control alt S"));
		
		// CTRL + Q
		itemRestart.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_DOWN_MASK));
		
		// CTRL + E:
		itemExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_DOWN_MASK));		
		
		// CTRL + SHIFT + A:
		itemAddClassFromFile.setAccelerator(KeyStroke.getKeyStroke("control shift A"));
		
		// CTRL + R:
		itemRemove.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_DOWN_MASK));
		
		// CTRL + I:
		itemSaveImageOfClassDiagram.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_DOWN_MASK));
		
		// CTRL + SHIFT + I:
		itemSaveImageOfInstanceDiagram.setAccelerator(KeyStroke.getKeyStroke("control shift I"));
		
		// CTRL + P
		itemPrintClassDiagram.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_DOWN_MASK));
		
		// CTRL + SHIFT + P
		itemPrintInstanceDiagram.setAccelerator(KeyStroke.getKeyStroke("control shift P"));
		
		// CTRL + shift + L
		itemRefreshClassDiagram.setAccelerator(KeyStroke.getKeyStroke("control shift L"));
		
		// CTRL + shift + K
		itemRefreshInstanceDiagram.setAccelerator(KeyStroke.getKeyStroke("control shift K"));
		
		// CTRL + SHIFT + R:
		itemRemoveAll.setAccelerator(KeyStroke.getKeyStroke("control shift R"));
		
		// CTRL + M
		itemRemoveInstanceDiagram.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, InputEvent.CTRL_DOWN_MASK));
		
		// CTRL + SHIFT + O
		itemOpenFileInCodeEditor.setAccelerator(KeyStroke.getKeyStroke("control shift O"));
		
		// CTRL + ALT + O
		itemOpenFileInProjectExplorer.setAccelerator(KeyStroke.getKeyStroke("control alt O"));
		
		// CTRL + SHIFT + V
		itemShowOutputFrame.setAccelerator(KeyStroke.getKeyStroke("control shift V"));
		
		// CTRL + B
		itemStart.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_DOWN_MASK));
		
		// CTRL + SHIFT + C:
//		itemCompile.setAccelerator(KeyStroke.getKeyStroke("control shift C"));
		// Shift + F6
		itemCompile.setAccelerator(KeyStroke.getKeyStroke("shift F6"));
		
		// CTRL + T:
		itemSettings.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_DOWN_MASK));
		
		// CTRL + F
//		itemFontSize.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_DOWN_MASK));
		itemFontSize.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_DOWN_MASK));
		
		// CTRL + J
		itemJavaSettings.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_J, InputEvent.CTRL_DOWN_MASK));
		
		// CTRL + F
		itemJavaSearch.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_DOWN_MASK));
		
		// CTRL + SHIFT + E
		itemPeOpenProject.setAccelerator(KeyStroke.getKeyStroke("control shift E"));
		
		// CTRL + SHIFT + W
		itemPeWorkspace.setAccelerator(KeyStroke.getKeyStroke("control shift W")); 
		
		// CTRL + U:
		itemInfo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, InputEvent.CTRL_DOWN_MASK));
		
		// CTRL + Z:
		itemAboutApp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_DOWN_MASK));
	}

	
	
	

	/**
	 * Metoda, která vytvoří instanci jednotlivých tlačítek do Jmenu, přidá jim události na jejích kliknutí
	 * myší, a přidá je do příslušného menu v Jmenu
	 */
	private void createMenuItems() {
		// Menu Project:
		// Instance položek v menu Project:
		itemNewProject = new JMenuItem();
		itemOpenProject = new JMenuItem();
        menuRecentlyOpenedProjects = new JMenu();
		itemCloseProject = new JMenuItem();
		itemSave = new JMenuItem();
		itemSaveAs = new JMenuItem();
		itemSwitchWorkspace = new JMenuItem();
		itemRestart = new JMenuItem();
		itemExit = new JMenuItem();

		
		// Přidání událostí:
		itemNewProject.addActionListener(this);
		itemOpenProject.addActionListener(this);
		itemCloseProject.addActionListener(this);
		itemSave.addActionListener(this);
		itemSaveAs.addActionListener(this);
		itemSwitchWorkspace.addActionListener(this);
		itemRestart.addActionListener(this);
		itemExit.addActionListener(this);
		
		// Přidání položek do MenuProject:
		menuProject.add(itemNewProject);
		menuProject.add(itemOpenProject);
		menuProject.add(menuRecentlyOpenedProjects);
		menuProject.add(itemCloseProject);
		menuProject.addSeparator();
		menuProject.add(itemSave);
		menuProject.add(itemSaveAs);
		menuProject.addSeparator();
		menuProject.add(itemSwitchWorkspace);
		menuProject.addSeparator();
		menuProject.add(itemRestart);
		menuProject.add(itemExit);
		



		// Rozevírací menu pro dříve otevřené projekty:
        /*
         *  Aby se při určitém množství projektů (/ položek v menu) zobrazily šipky a scrolovalo se mezi položkami,
         *  aby nebyl vždy zobrazen pevný počet těch položek v menu:
         */
        new PopupMenuScroller(menuRecentlyOpenedProjects, SCROLL_COUNT, SCROLL_INTERVAL, TOP_FIXED_COUNT,
                BOTTOM_FIXED_COUNT);
        // Položky do menu pro dříve otevřené projekty:
        itemDeleteRecentlyProjects = new JMenuItem();// Přidání do menu až při generování položek (projektů)
        itemDeleteRecentlyProjects.addActionListener(this);

        // (označení se řeší zvlášť po vytovření položek v konstuktoru:)
        rbItemSortRecentlyProjectsAsc = new JRadioButtonMenuItem();
        rbItemSortRecentlyProjectsDes = new JRadioButtonMenuItem();

        rbItemSortRecentlyProjectsAsc.addActionListener(this);
        rbItemSortRecentlyProjectsDes.addActionListener(this);

        final ButtonGroup bgRbForSorring = new ButtonGroup();
        bgRbForSorring.add(rbItemSortRecentlyProjectsAsc);
        bgRbForSorring.add(rbItemSortRecentlyProjectsDes);



		
		
		
		// Menu Edit:
		// Instance položek v menu Edit:
		itemAddClassFromFile = new JMenuItem();
		itemRemove = new JMenuItem();
		itemSaveImageOfClassDiagram = new JMenuItem();
		itemSaveImageOfInstanceDiagram = new JMenuItem();
		itemPrintClassDiagram = new JMenuItem();
		itemPrintInstanceDiagram = new JMenuItem();
		itemRefreshClassDiagram = new JMenuItem();
		itemRefreshInstanceDiagram = new JMenuItem();
		itemRemoveAll = new JMenuItem();
		itemRemoveInstanceDiagram = new JMenuItem();
		
		// Přidání událostí na jednotlivé položky:
		itemAddClassFromFile.addActionListener(this);
		itemRemove.addActionListener(this);
		itemSaveImageOfClassDiagram.addActionListener(this);
		itemSaveImageOfInstanceDiagram.addActionListener(this);
		itemPrintClassDiagram.addActionListener(this);
		itemPrintInstanceDiagram.addActionListener(this);
		itemRefreshClassDiagram.addActionListener(this);
		itemRefreshInstanceDiagram.addActionListener(this);
		itemRemoveAll.addActionListener(this);
		itemRemoveInstanceDiagram.addActionListener(this);
		
		// Přidání položek do menu Edit:
		menuEdit.add(itemAddClassFromFile);
		menuEdit.add(itemRemove);
		menuEdit.addSeparator();
		menuEdit.add(itemSaveImageOfClassDiagram);
		menuEdit.add(itemSaveImageOfInstanceDiagram);
		menuEdit.addSeparator();
		menuEdit.add(itemPrintClassDiagram);
		menuEdit.add(itemPrintInstanceDiagram);
		menuEdit.addSeparator();
		menuEdit.add(itemRefreshClassDiagram);
		menuEdit.add(itemRefreshInstanceDiagram);
		menuEdit.addSeparator();
		menuEdit.add(itemRemoveInstanceDiagram);
		menuEdit.add(itemRemoveAll);
		
		
		
		
		
		// Menu Tools:
		itemCompile = new JMenuItem();
		menuOpenInEditor = new JMenu();
		itemShowOutputFrame = new JMenuItem();
		itemStart = new JMenuItem();
		itemSettings = new JMenuItem();
		itemFontSize = new JMenuItem();
		menuItemJava = new JMenu();
		menuProjectExplorer = new JMenu();
        menuOpenInFileExplorer = new JMenu();
		
		// Přidání událostí na jednotlivé položky:
		itemCompile.addActionListener(this);		
		itemShowOutputFrame.addActionListener(this);
		itemStart.addActionListener(this);
		itemSettings.addActionListener(this);
		itemFontSize.addActionListener(this);
		
		// Přidání položek do menu Tools:
		menuTools.add(itemCompile);
		menuTools.addSeparator();
		menuTools.add(menuOpenInEditor);
		menuTools.addSeparator();
		menuTools.add(itemShowOutputFrame);
		menuTools.addSeparator();
		menuTools.add(itemStart);
		menuTools.addSeparator();
		menuTools.add(itemSettings);
		menuTools.addSeparator();
		menuTools.add(itemFontSize);
		menuTools.addSeparator();
		menuTools.add(menuItemJava);
		menuTools.addSeparator();
		menuTools.add(menuProjectExplorer);
		menuTools.add(menuOpenInFileExplorer);

		
		
		/*
		 * Položky do menu menuOpenInEditor - pro otevření nějakého souboru, který má
		 * uživatel na disku v editoru zdrojového kódu nebo v průzkumníku projektu (s
		 * kořenovým adresářem ve workspace).
		 */
		itemOpenFileInCodeEditor = new JMenuItem();
		itemOpenFileInProjectExplorer = new JMenuItem();
		
		itemOpenFileInCodeEditor.addActionListener(this);
		itemOpenFileInProjectExplorer.addActionListener(this);
		
		menuOpenInEditor.add(itemOpenFileInCodeEditor);
		menuOpenInEditor.addSeparator();
		menuOpenInEditor.add(itemOpenFileInProjectExplorer);
		
		
		
		
		/*
		 * Položky do menu menuItemJava - pro otevření dialogu pro nastavení adresáře
		 * Java home nebo potřebných souborů pro kompilaci a nebo spuštění algoritmu pro
		 * vyhledání těchto souborů
		 */
		itemJavaSettings = new JMenuItem();
		itemJavaSearch = new JMenuItem();
		
		itemJavaSettings.addActionListener(this);
		itemJavaSearch.addActionListener(this);
		
		menuItemJava.add(itemJavaSettings);
		menuItemJava.addSeparator();
		menuItemJava.add(itemJavaSearch);
		
		
		
		
		
		
		
		// Položky do menuProjectExplorer - pro otevření bud workspace nebo adresare projektu:
		itemPeOpenProject = new JMenuItem();
		itemPeWorkspace = new JMenuItem();
		
		itemPeOpenProject.addActionListener(this);
		itemPeWorkspace.addActionListener(this);
		
		menuProjectExplorer.add(itemPeOpenProject);
		menuProjectExplorer.addSeparator();
		menuProjectExplorer.add(itemPeWorkspace);


        /*
         * Položky do menuOpenInFileExplorer - pro otevření adresáře workspace nebo adresáře otevřeného projektu v
         * průzkumníku souborů nastaveném v OS:
         */
        itemFeWorkspace = new JMenuItem();
        itemFeOpenProject = new JMenuItem();

        itemFeWorkspace.addActionListener(this);
        itemFeOpenProject.addActionListener(this);

        menuOpenInFileExplorer.add(itemFeOpenProject);
        menuOpenInFileExplorer.addSeparator();
        menuOpenInFileExplorer.add(itemFeWorkspace);



		
		
		
		
		// Položky do menu Help:
		itemInfo = new JMenuItem();
		itemAboutApp = new JMenuItem();
		
		// Přidání událostí na jednotlivé položky:
		itemInfo.addActionListener(this);
		itemAboutApp.addActionListener(this);
		
		// Přidání položek do menu Help:
		menuHelp.add(itemInfo);
		menuHelp.addSeparator();
		menuHelp.add(itemAboutApp);
	}


	
	
	
	
	/**
	 * Metoda, která vytvoří instancí Jednotlivých "hlavních - prvních" menu v Jmenu
	 * komponentě a přidá je do Jmenu
	 */
	private void createMenu() {
		menuProject = new JMenu();
		menuEdit = new JMenu();
		menuTools = new JMenu();
		menuHelp = new JMenu();
		
		add(menuProject);
		add(menuEdit);
		add(menuTools);
		add(menuHelp);
	}





    /**
     * Metoda, která vytvoří položky do menu pro nedávno / dříve otevřené projekty.
     * <p>
     * Princip je takový, že se načtou veškeré dříve otevřené projekty z preferencí, pro všechny se otestuje, zda ještě
     * existují, seřadí se dle nastavení buď vzestupně nebo sestupně a postupně se v definovaném pořadí přidají do menu
     * v okně aplikace.
     */
    private void createRecentlyOpenedProjectsItems() {
        // Získám si veškeré dříve otevřené projekty z preferencí (s výjimkou toho aktuálně otevřeného - je li):
        final List<OpenedProject> openedProjectList = getOpenedProjects();

        // Odeberu neexistující projekty:
        openedProjectList.removeIf(openedProject -> !openedProject.existProject());

        // Seřazení projektů:
        sortRecentlyOpenedProjectsItems(openedProjectList);



        // Získám si položky do menu:
        final List<JMenuItem> menuItemList = getItemsForOpenedProjects(openedProjectList);

        // Nejprve odeberu existující položky v rozevíracím menu - může se jednat o existují projekty před otevřním
        // nového projektu:
        menuRecentlyOpenedProjects.removeAll();

        // Přidám položky do rozevíracího menu v okně aplikace:
        menuItemList.forEach(item -> menuRecentlyOpenedProjects.add(item));

        /*
         * Na konec do menu přidám položky, které se v něm měli nacházet vždy.
         *
         * Note: separátor by bylo možné přidávat, jestli se výše přidala alespoň jedna položka reprezentující dříve
         * otevřený projekt, ale
         * takto to bude "vypadat lépe" v tom smyslu, že uživatel uvidí, že dříve nemá žádné otevřené projekty.
         *
         *
         *
         * !!! Important!!!
         * Zde je třeba si hlídat počet položek, které se v tom menu budou vždy nacházet !!!
         *
         * V tomto případě se jedná o položku pro vymazání historie a komponenty pro nastavení sestupného a
         * vzestupného řazení
         * otevření projektů. Ale na toto menu pro položky pro dříve otevřené projekty je nastaveno scrolování.
         *
         * Tedy, že se při určitém počtu položek v menu nastaví scrolvání mezi položkami, ale úplně na konci toho menu
         * chci, aby byly pevně definované výše zmíněné 3 položky, proto musím nastavit proměnnou: cz.uhk.fim.fimj
         * .menu.Menu.BOTTOM_FIXED_COUNT
         * na hodnotu 3 (3 ukotvené položky ve spodní části rozevíracího menu). Kdybych tyto položky doplnil nebo
         * odebral, musím
         * spolu s těmi položkami i upravit to číslo (hodnotu proměnné).
         */
        menuRecentlyOpenedProjects.addSeparator();
        menuRecentlyOpenedProjects.add(itemDeleteRecentlyProjects);
        menuRecentlyOpenedProjects.addSeparator();
        menuRecentlyOpenedProjects.add(rbItemSortRecentlyProjectsAsc);
        menuRecentlyOpenedProjects.add(rbItemSortRecentlyProjectsDes);
    }


    /**
     * Metoda, která seřadí položky v listu openedProjectList sestupně nebo vzestupně dle datumu otevření projektu.
     *
     * @param openedProjectList
     *         - list s položkami, který se má seřadit sestupně nebo vzstupně dle datumu otevření příslušných projektů.
     */
    private static void sortRecentlyOpenedProjectsItems(final List<OpenedProject> openedProjectList) {
        if (sortRecentlyOpenedProjects == SortRecentlyOpenedProjects.ASC)
            // Metoda sort je seřazení položek vzestupně:
            openedProjectList.sort(Comparator.comparing(OpenedProject::getDate));

        else if (sortRecentlyOpenedProjects == SortRecentlyOpenedProjects.DES) {// Stačí else
            // Metoda sort je seřazení položek vzestupně:
            openedProjectList.sort(Comparator.comparing(OpenedProject::getDate));

            // Potřebuji položky v kolekci "otočit", aby se jednalo o sestupné pořadí:
            Collections.reverse(openedProjectList);
        }
    }


    /**
     * Metoda pro získání dříve otevřených projektů. Pokud je aktuálně otevřen existují projekt, tak se vrátí dříve
     * otevřené projekty s výjimkou toho aktuálně otevřeného. Pokud nebude nalezen otevřený projekt, tj. není žádný
     * otevřen, neexistuje apod. Tak se vrátí veškeré dříve otevřené projekty, které jsou uloženy v preferencích.
     *
     * @return výše popsané dříve otevřené projekty uložené v (/ načtené z) preferencích.
     */
    private static List<OpenedProject> getOpenedProjects() {
        // Cesta k otevřenému projektu:
        final String openProject = PreferencesApi.getPreferencesValue(PreferencesApi.OPEN_PROJECT, null);

        // Zda byla cesta k existujícímu projektu získána a nejedná se o zavřený projekt:
        if (openProject != null && !openProject.equals(Constants.CLOSED_PROJECT) && ReadFile.existsDirectory
                (openProject))
            // Vrátím dříve otevřené projekty, akorát bez toho aktuálně otevřeného:
            return convertOpenedProjectToList(OpenedProjectPreferencesHelper.getOpenedProjectsExceptOpenedProject(new
                    File(openProject)));

        // Zde není otevřen žádný nebo existující projekt, tak vrátím veškeré získané dříve otevřené projekty:
        return convertOpenedProjectToList(OpenedProjectPreferencesHelper.getOpenedProjectsExceptOpenedProject(null));
    }


    /**
     * Metoda, která převede jednorozměrné pole s otevřenýmy projekty (OpenedProject) do daotvé struktury typu List.
     *
     * @param openedProjects
     *         - jednorozměrné pole typu OpenedProject, které se má převést do Listu.
     * @return list z openedProjects
     */
    private static List<OpenedProject> convertOpenedProjectToList(final OpenedProject[] openedProjects) {
        return new ArrayList<>(Arrays.asList(openedProjects));
    }


    /**
     * Metoda, která vytvoří jednotlivé položky (JMenuItems) do rozevíracího menu v aplikaci. Položky reprezentují dříve
     * otevřené projekty v aplikaci.
     *
     * @param openedProjectList
     *         - list otevřených projektů, ze kterých se mají vytovřit položky do menu
     * @return list položek JMenuItem, které se mohou vložit do rozevíracího menu v aplikaci.
     */
    private List<JMenuItem> getItemsForOpenedProjects(final List<OpenedProject> openedProjectList) {
        final List<JMenuItem> menuItemList = new ArrayList<>();

        openedProjectList.forEach(openedProject -> {
            final JMenuItem item = new JMenuItem(openedProject.getFileName());

            item.setToolTipText(openedProject.getTextForTooltip());

            item.addActionListener(e -> openProject(openedProject.getPath()));

            menuItemList.add(item);
        });

        return menuItemList;
    }







	

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JMenuItem) {
			// Položky v Menu Project:
			if (e.getSource() == itemNewProject) {
				
				final NewProjectForm projectForm = new NewProjectForm();
				projectForm.setLanguage(languageProperties);
				
				final String pathToProjectFolder = projectForm.getPathToProject();

                if (pathToProjectFolder != null && App.WRITE_TO_FILE.createNewProjectFolder(pathToProjectFolder)) {

                    // Nejprve uložím aktuální class diagram - pokud je nějaký projekt otevřený:
                    App.WRITE_TO_FILE.saveClassDiagram(GraphClass.getCellsList());


                    // Do příslušné promenné v defaul.properties - 'Open project' zapíši cestu ke zvolenému projektu
                    // Aby aplikace poznala, že má otevřený projekt, a mohla zprovoznit příslušná tlačítka, apod.

                    PreferencesApi.setPreferencesValue(PreferencesApi.OPEN_PROJECT, pathToProjectFolder);
                    /*
                     * Zde je potřeba přidat nový projekt do preferencí a přefiltrovat hodnoty v rozevíracím menu
                     * pro dříve otevřené projekty, protože
                     * pokud byl otevřen projekt před tím, než se tento nově vytvořený projekt oevřel, tak se má
                     * přidat do nabídky pro dříve otevřené
                     * projekty.
                     */
                    OpenedProjectPreferencesHelper.checkOpenedProject(new File(pathToProjectFolder));
                    /*
                     * Výše se mohl uložit nějaký nový projekt do preferencí a případně se může zpřístupnit
                     * předchozí otevřený projekt, tak přenačtu položky
                     * pro otevřní nových projektů:
                     */
                    createRecentlyOpenedProjectsItems();


                    // Dále vymažu diagram instancí a všechny instance - pokud jsou nějaké vytvořené:
                    instanceDiagram.clearGraph(GraphInstance.getCellsList());

                    // vymažu vytvořené instance:
                    Instances.deleteAllInstancesFromMap();

                    // zruším označené, ale nevytvořené vztahy
                    GraphClass.deselectChoosedEdge();

                    // Vymažu class diagram - graf - pokud by tam náhodou něco bylo:
                    classDiagram.clearGraph(GraphClass.getCellsList());

                    // vymažu obsah editoru příkazů, aby nemátly příkazy zadané v jiném projektu:
                    commandEditor.clearEditor();

                    // vymažu obsah editoru výstupů, aby byly rozeznat výstupy z nového projektu:
                    outputEditor.clearEditor();

                    // Zde proběhlo vše v pohodě a je otevřen projekt, tak zpřístupnim veškerá tlačítka / komponenty
                    App.enableButtons(true);

                    // Nastavím do titulku okna aplikace cestu k aktuálně otevřenému projektu
                    app.setTitle(Constants.APP_TITLE + " - " + pathToProjectFolder);
                }
			}
			
			
			
			
			
			else if (e.getSource() == itemOpenProject) {
				// Otevřu dialog pro výběr projektu, otestuje, zda se jedná o projekt tét aplikace
				// a pokud ano, tak ho načtu do aplikace - "otevřu projekt":
				

                final String pathToWorkspace = ReadFile.getPathToWorkspace();


                final String pathToProjectDir = new FileChooser().getPathToProjectDirectory(pathToWorkspace, true);

                // Postup:
                // Nejprve otestuji, zda je nějaký projektu otevřený, pokud ano, tak uložím aktuální class diagram
                // Otevře se dialogové okno pro výber projektu
                // Otestuje se, zda existuje adresář src v adresáři proektu - případně se vytvoří
                // Otestuje se, zda existuje soubor: ClassDiagramProperties.xml, pokud ne nic se neděje, jen se nenačte class diagram
                // Zapíše se cesta k nově otevrenemu projektu do default.properties
                // Uvolní se tlačítka
                // změní se titulek aplikace:
                openProject(pathToProjectDir);
            }






            else if (e.getSource() == itemDeleteRecentlyProjects) {
                // Nastavím do preferencí k příslušné hodnotě prázdné pole s dříve otevřenými projekty:
                OpenedProjectPreferencesHelper.setEmptyOpenedProjectsToPreferences();

                /*
                 * Jelikož se vymazaly veškeré dříve otevřené projekty - pokud existovaly, tak zde musím přenačíst
                 * položky v menu, protože už by v něm neměli být žádné dříve otevřené položky (v případě úspěšného
                 * vymazání "historie").
                 */
                createRecentlyOpenedProjectsItems();
            }






            else if (e.getSource() == rbItemSortRecentlyProjectsAsc) {
                // Nastavím proměnnou pro způsob řazení:
                sortRecentlyOpenedProjects = SortRecentlyOpenedProjects.ASC;

                // Znovu potřebuji aktualizovat položky v příslušném rozevíracím menu, aby se
                createRecentlyOpenedProjectsItems();

                // Uložím nový způsob řazení do konfiguračního souboru:
                writeChangeToDefaultProp(sortRecentlyOpenedProjects);
            }

            else if (e.getSource() == rbItemSortRecentlyProjectsDes) {
                // Nastavím proměnnou pro způsob řazení:
                sortRecentlyOpenedProjects = SortRecentlyOpenedProjects.DES;

                // Znovu potřebuji aktualizovat položky v příslušném rozevíracím menu, aby se
                createRecentlyOpenedProjectsItems();

                // Uložím nový způsob řazení do konfiguračního souboru:
                writeChangeToDefaultProp(sortRecentlyOpenedProjects);
            }



			
			
			
			
			
			else if (e.getSource() == itemCloseProject) {
				// Postup:
				// Ulozi se aktualní class diagram - pokud je otevreny nejaky projekt:
				// vymaže se class diagram:
				// znepřístupnit tlačítka, která nejsou potřeba, pokud se "nepracuje na projektu"
				
				
				
				// Ulozi se class diagram so souboru - pokue je otevreny projekt:
				App.WRITE_TO_FILE.saveClassDiagram(GraphClass.getCellsList());
				
				// vymažu všechny instance - pokud jsou nějaké vytvořené:
				Instances.deleteAllInstancesFromMap();
				
				// vymyzat class idagram:
				classDiagram.clearGraph(GraphClass.getCellsList());
				
				// vymazu diagram instanci - pokud jsou nějaké vytvořené:
				instanceDiagram.clearGraph(GraphInstance.getCellsList());
				
				// zruším označené, ale nevytvořené vztahy
				GraphClass.deselectChoosedEdge();

                // vymažu obsah editoru příkazů, byly specifické pro konkrétní projekt:
                commandEditor.clearEditor();

                // vymažu obsah editoru výstupů, pro jiný projekt budou jiné výstupy, mohlo by mást:
                outputEditor.clearEditor();

				// Zde proběhlo v pořádku zavření projektu, tak některá tlačítka znepřístupním, protože nejsou potřeba 
				// a ani by nereagovala správně:
				App.enableButtons(false);
				
				// Zavru projekt v default.properites:
				App.WRITE_TO_FILE.setCloseProject(Constants.CLOSED_PROJECT);

                /*
                 * Zde je třeba přenačíst položky v rozevíracím menu s dříve otevřenými projekty, protože zde se zavřel
                 * otevřený projekt, tudíž je by i ten měl být přístupný jako dříve otevřený a mělo by být možné jej
                 * otevřit.
                 */
                createRecentlyOpenedProjectsItems();

				// Nastavím titulek na normální název aplikace:
				app.setTitle(Constants.APP_TITLE);

				// Vytvořím si svůj ClassLoader, pomocí kterého mohu načítat přeložené třídy z
				// adresáře bin:
				CompiledClassLoader.setClassLoaderToNull();

				// Změním titulek dialogu, aby nebyl zobrazen nějaký otevřený projekt:
				RedirectApp.changeOutputFrameTitle();
			}
			
			
			
			
			
			
			else if (e.getSource() == itemSave)
				// Uloží se do adresáře aktuálně otevřeného projektu class diagram
				// Více v podstatě nepotřebuji, editor kódu uloží změny po zavření jeho dialogu, instance se nikam neukládají
				// třídy se uloži hned při zápisu či manipulací v grafu, takže zbývá uložit akorát class diagram:
				App.WRITE_TO_FILE.saveClassDiagram(GraphClass.getCellsList());		
			
			
			
			
			else if (e.getSource() == itemSaveAs) {
				// Otevřu dialogové okno, kde se zadá umístění, kam se má aktuální projekt uložit, a do te složky se nakopíruje dany projekt								
								
				final String pathToWorkspace = ReadFile.getPathToWorkspace();
				
				final String pathToProjectFolder = new FileChooser().getPathToProjectDirectory(pathToWorkspace, false);
				
				// Pokud to nezrušil:
				if (pathToProjectFolder != null) {
					// Nejprve otestuji, zda adresář existuje, pokud ne, vytvořím ho:
					final File fileProjectDir = new File(pathToProjectFolder);
					
					// Zde již vím, že je to adresář (v dialogu jiného vybrat nejde), tak otestuji, zda již existuje nebo ne
					// Pokud ne, tak ho vytvořím
					if (!fileProjectDir.exists()) {
						try {
							Files.createDirectories(Paths.get(pathToProjectFolder));
						} catch (IOException e1) {
							/*
							 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
							 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
							 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
							 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
							 */
							final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

							// Otestuji, zda se podařilo načíst logger:
							if (logger != null) {
								// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

								// Nejprve mou informaci o chybě:
								logger.log(Level.INFO,
										"Zachycena výjimka při vytváření adresáře pro projekt, který vybral uživatel " +
												"jako adresář pro uložení projektu z menu Save As, cesta: "
												+ pathToProjectFolder
												+ ". Třída v aplikaci: Menu.java, událost po kliknutí na tlačítko Save" +
												" As.");

								/*
								 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
								 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
								 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
								 */
								logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
							}
							/*
							 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
							 */
							ExceptionLogger.closeFileHandler();
						}
					}
					// Zde už adresář existuje, tak do něj nakopíruji soubory z adresáře aktuálně otevřeného projektu:
					// Ale před tím uložím aktuální class diagram:
					App.WRITE_TO_FILE.saveClassDiagram(GraphClass.getCellsList());

					// Nyní mohu nakopírovat obsah aktuálně otevřeného projektu
					// do nové zvolené složky:
					final String pathToOpenProject = PreferencesApi.getPreferencesValue(PreferencesApi.OPEN_PROJECT,
							null);

					if (pathToOpenProject != null) {

						final File srcFile = new File(pathToOpenProject);

						if (srcFile.exists() && srcFile.isDirectory()) {
							try {
								// Nejprve ale odstraním adresář src , protože by v něm mohli existovat nejaké tridy, které
								// by do aktuálního projektu  nepatřily, což nechci, ostatní soubory -
								// ClassDiagramProperties a soubor s informacemi se přepíšou, zbytek obsahu adresáře se
								// zachová
								final File fileSrcAlready = new File(pathToProjectFolder + File.separator + "src");
								if (fileSrcAlready.exists() && fileSrcAlready.isDirectory())
									FileUtils.deleteDirectory(fileSrcAlready);

								// Kopírování adresáře - otestuji, zda jejich cesty, resp. umístění nejsou identické
								if (!srcFile.getAbsolutePath().equals(fileProjectDir.getAbsolutePath())) {
									// Přepíše soubory v daném adresáři (pokud
									// tam nějaké budou)
									FileUtils.copyDirectory(srcFile, fileProjectDir, false);

									// Změním cestu k novému projektu:
									PreferencesApi.setPreferencesValue(PreferencesApi.OPEN_PROJECT,
											pathToProjectFolder);

									// Nastavím do titulku okna aplikace cestu k  aktuálně otevřenému projektu
									app.setTitle(Constants.APP_TITLE + " - " + pathToProjectFolder);
								} 
								else
									JOptionPane.showMessageDialog(this, identicalDirErrText, identicalDirErrTitle,
											JOptionPane.ERROR_MESSAGE);

							} catch (IOException e1) {
                                /*
                                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                                 */
                                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                                // Otestuji, zda se podařilo načíst logger:
                                if (logger != null) {
                                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                                    // Nejprve mou informaci o chybě:
                                    logger.log(Level.INFO,
                                            "Zachycena výjimka při kopírování aktuálně otevřeného projektu: "
                                                    + pathToOpenProject + " do nově zvoleného adresáře: "
                                                    + fileProjectDir.getPath()
                                                    + "třída menu.java metoda - po kliknutí na SaveAs v menu.");

                                    /*
                                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                                     */
                                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
                                }
                                /*
                                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                                 */
                                ExceptionLogger.closeFileHandler();
                            }
						}
					}
				}	
			}
			
			
			
			
			
			else if (e.getSource() == itemSwitchWorkspace) {
				// Otevře se okno pro výběr složky coby Workspace
				// pouze se změní tento adresář, jinak nic
				
				//Postup:
				// Otevře se dialogové okno pro výběr složky coby Workspace
				// Jakmile ji uživatle vybere, tak se otestuje, zda zvolený - napsaný adresář existuje, případně se vytvoří
				//	Pokud se vytvoří nakopíruje se do něj složka confuguration a uloží si hodnota dotazu na Workspace
				// 	Pokud se nevytvoří, tak se otestuje, zda v něm složka configuration existuje, případně ji vytvořím - nakopíruji
				
				
				// Instance Workspace form pro vyber cesty do nového workspace
				// a nastavení jazyka:
				final WorkspaceForm wf = new WorkspaceForm();
				wf.setLanguage(languageProperties);
				
				final InfoAboutWorkspace workspaceInfo = wf.chooseWorkspace();
				
				// Zjistím, zda to uživatel nezrušil:
				if (workspaceInfo != null) {
					// Zde zadal nějakou cestu, tak ji zjistím:
					
					// otestuji, zda existuje:
					final File fileWorkspace = new File(workspaceInfo.getPathToWorkspace());
					
					if (!fileWorkspace.exists() || !fileWorkspace.isDirectory()) {
						// Zde Adresář Workspace neexistuje, nebo se nejedná o adresář, tak ho vytvořím - nakopíruji:
						try {
							Files.createDirectories(Paths.get(workspaceInfo.getPathToWorkspace()));
							
							// Nyní do ní nakopíruji soubory z hlavního adresáře aplikace:
							WriteToFile.createConfigureFilesInPath(workspaceInfo.getPathToWorkspace());

                        } catch (IOException e1) {
                            /*
                             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                             */
                            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                            // Otestuji, zda se podařilo načíst logger:
                            if (logger != null) {
                                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                                // Nejprve mou informaci o chybě:
                                logger.log(Level.INFO,
                                        "Zachycena výjimka při vytváření adresáře zvoleného jako Workspace, Třída " +
                                                "Menu.java, metoda - po kliknutí na položku změnit Workspace.");

                                /*
                                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                                 */
                                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
                            }
                            /*
                             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                             */
                            ExceptionLogger.closeFileHandler();
						}
					}
					
					// Zde by měl již adresář zvolený jako Workspace existovat (pokud neexistovatl)
					// Tak otestuji, zda v něm existuje složka configuration, případně ji nakopíruji:
					final File fileConfiguration = new File(
							workspaceInfo.getPathToWorkspace() + File.separator + Constants.CONFIGURATION_NAME);
					
					
					if (!fileConfiguration.exists() || !fileConfiguration.isDirectory())
						// Zde složka buď neexistuje, nebo se nejedná o adresář, tak či tak ji musím
						// vytvořit, tak ji nakopíruje z hlavního adresáře aplikace:
						WriteToFile.createConfigureFilesInPath(workspaceInfo.getPathToWorkspace());
					
					// Zde již adresář configuration existuje, ale na soubory v něm se již neptám, nevidím důvod, proč by uživatel 
					// mazal jeho obsah, akorát editovat, pokud bude chybět, stačí smazat adresář configuration a restartovat aplikaci
					// a adresář se znovu nakopíruje, případně lze z menu, nastavení vytvořit nový
					
					
					// Nastavím novou cestu do Workspacu:
					App.WRITE_TO_FILE.changePathToWorkspace(workspaceInfo.getPathToWorkspace());
												
					// Nastavím nový dotaz na Workspace - pokud ho uživatel zmenil:
					App.WRITE_TO_FILE.setAskForWorkspaceValue(workspaceInfo.isAskForWorkspace());
					
					
					/*
					 * Ukončím aktuální vlákno, které monitoruje soubory v teď už předchozím
					 * adresáři workspace/configuration a spustím to vlákno znovu na teď již novém
					 * umístění adresáře workspace.
					 * 
					 * Toto je trochu zbytečné, ale uživatel níženemusí ihned restartovat aplikaci,
					 * aby se projevily změny například v nastavení apod. Tak pak bybylo třeba ty
					 * nové soubory hlídat.
					 */
					App.launchCheckingMonitoringConfigFiles(languageProperties);


                    /*
                     * Zde je ale potřeba restartovat aplikaci aby se změny projevily, protože nově
                     * zvolený adresář workspace může například obsahovat jiné designy buněk pro
                     * třídy a komentáře nebo instance, jinak vypadájící hrany atd.
                     *
                     * Tak aby to nastavení v nově zvoleném adresáři workspace projevily, je třeba
                     * tu aplikaci restartovat.
                     */

                    // Zeptám se uživatele, zda si opravdu přeje aplikaci ukončit.
                    if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(this,
                            txtSwitchWorkspaceJopRestartAppNowText, txtSwitchWorkspaceJopRestartAppNowTT,
                            JOptionPane.YES_NO_OPTION))
                        app.restartApp();
                }
			}






            // Tlačítko pro restartování aplikace:
            else if (e.getSource() == itemRestart)
                app.restartApp();
				
			
			
			
			
			
			
			
			else if (e.getSource() == itemExit) {
				// Uložít rozpracovaný projekt a zavřít aplikaci
				// Note - zde již nemusím vymazat diagramy nebo instance, apod, stejné se ukončí aplikace a příslušné instance
				// apod. s ní

				App.saveDataBeforeCloseApp();
				
				
				
				// Zavřu aplikaci:
				System.exit(0);
			}
			
			
			
			
			
			// Položky v menu Edit:
			else if (e.getSource() == itemAddClassFromFile) {
				// Otevře se dialog pro označení třídy, která se má přidat do projektu
				// ta se pak zkopíruje do příslušného adresáře
				
				
				final ClassFromFileForm cfff = new ClassFromFileForm();
				cfff.setLanguage(languageProperties);
				
				// Note:
				// mohl jsem použít více objektový přístup - vice viz komentář metody getSelectedClass()
				final Object[] newClassInfo = cfff.getSelectedClass();
				
				if (newClassInfo.length > 0) {
					// Naní mám název třídy a její umístění na disku
					
					// Pro začátek vytvořím příslušné balíčky - pokud je třeba:
					String className = String.valueOf(newClassInfo[0]);
					String packageText = "";
					
					final String pathToSrc = App.READ_FILE.getPathToSrc();
					
					final String pathToBin = App.READ_FILE.getPathToBin();
					
					
					if (pathToSrc != null && pathToBin != null) {
						// Musím přidat umístění třídy do výchozího balíčku, pokud uživatel nezadá žádný balíček:

						// Vytvořím příslušné balíčky v adresáři src:
						// A do promenné si uložím cestu, kam mám nakopírovat
						// zvolenou třídu:
						final String pathToClassDir = App.WRITE_TO_FILE.createDirectoriesToClass(pathToSrc, pathToBin,
								className);

						final String[] partsOfPackage = className.split("\\.");

						for (int i = 0; i < partsOfPackage.length - 1; i++)
							packageText += partsOfPackage[i] + ".";

						// odeberu tecku:
						packageText = packageText.substring(0, packageText.length() - 1);

						// Cesta, kam se má třída nakopírovat:
						final String pathToClass = pathToClassDir + File.separator
								+ partsOfPackage[partsOfPackage.length - 1] + ".java";

						// Nyní mohu zkopirovat třídu na do vytvořených balíčků
						// (i s přejmenováním):
						final File fileSrc = new File(String.valueOf(newClassInfo[1]));

						if (fileSrc.exists() && fileSrc.isFile()) {
							try {
								Files.copy(Paths.get(String.valueOf(newClassInfo[1])), Paths.get(pathToClass),
										StandardCopyOption.REPLACE_EXISTING);
                            } catch (IOException e1) {
                                /*
                                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                                 */
                                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                                // Otestuji, zda se podařilo načíst logger:
                                if (logger != null) {
                                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                                    // Nejprve mou informaci o chybě:
                                    logger.log(Level.INFO, "Zachycena výjimka při kopirování označené třídy: "
                                            + newClassInfo[1] + ", do adresáře: " + pathToClassDir);

                                    /*
                                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                                     */
                                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
                                }
                                /*
                                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                                 */
                                ExceptionLogger.closeFileHandler();
							}

							// Nyní do zkopírované třídy musím zapsat balíček,
							// ve kterém se nachází:
							// Nejprve ho smažu - pokud existuje, poté zapíšu
							// nový:
							App.WRITE_TO_FILE.deletePackageIfExist(pathToClass);
							App.WRITE_TO_FILE.addPackageToClass(pathToClass, packageText);

							// Přejmenovat třídu - pokud je potřeba:
							final String oldName = fileSrc.getName().substring(0, fileSrc.getName().lastIndexOf('.'));
							final String newName = String.valueOf(partsOfPackage[partsOfPackage.length - 1]);

							App.WRITE_TO_FILE.renameClass(pathToClass, oldName, newName);

							// Pokud se vše v pořádku povedlo, tak se přidá reprezentace třídy do diagramu tříd:
							classDiagram.addClassCell(className);

                            /*
                             * Zde se zkompilují třídy v diagramu tříd a aktualizuje se okno pro doplňování příkazů
                             * do editoru příkazů, aby se v něm nabízela nově přidaná třída apod.
                             */
                            compileClassesInClassDiagram();
						} 
						else
							JOptionPane.showMessageDialog(this, missingClassFileText + fileSrc.getPath(),
									missingClassFileTitle, JOptionPane.ERROR_MESSAGE);
					}
				}
			}
			
			
			
			else if (e.getSource() == itemRemove) {
				// Odebere se označená položka v grafu
				
				// Nejprve si zjistím, zda se jedná o třídu, a pokud ano, tak zda ještě existuje,
				// a pokud je to třída a už neexistuje, tak  v posledním paraemtru metody bude true - jako že
				// se mají smazat všechny hrany vs tou buňku, jinak false - pouze jeden objekt - případně hlášky:
				
				boolean deleteAllEdge = false;
				
				final DefaultGraphCell selectedObject = (DefaultGraphCell) classDiagram.getSelectionCell();
				
				if (selectedObject != null) {
					// Pokud označený objekt neexistuje, tak se mají smazat všchny
					final boolean result = GraphClass.existClassInSrc(selectedObject);	
					
					deleteAllEdge = !result;
				}
			

				// Metoda, která smaže označený objekt:
				App.WRITE_TO_FILE.removeSelectedObject(classDiagram, languageProperties, deleteAllEdge);
			}
			
			
			
			
			
			
			else if (e.getSource() == itemSaveImageOfClassDiagram) {
				// Otevře se dialogvé okn opro býběr umístění a názvu PrintScreenu grafu

				final String pathToImage = new FileChooser().getPathToSaveImage();
				
				if (pathToImage != null)
					// Uložení obrázku do souboru:
					App.WRITE_TO_FILE.saveImageOfGraph(pathToImage, classDiagram);
			}
			
			
			
			
			
			else if (e.getSource() == itemSaveImageOfInstanceDiagram) {
				// Otevře se dialogvé okn opro býběr umístění a názvu PrintScreenu grafu
				final String pathToImage = new FileChooser().getPathToSaveImage();
				
				if (pathToImage != null)
					App.WRITE_TO_FILE.saveImageOfGraph(pathToImage, instanceDiagram);
			}
			
			
			
			else if (e.getSource() == itemPrintClassDiagram) {
				// Zde chce uživatel vytisknout PrintScreen diagramu tříd, tak otestuji,
				// zda neni null - nemělo by nastat nikdy, takže bych to ani nemusel testovat, ale kdyby náhodou:
				
				if (classDiagram != null) {
					final PrinterJob printJob = PrinterJob.getPrinterJob();
					printJob.setPrintable(new PrintGraph(classDiagram));
					
					if (printJob.printDialog())
						try {
							printJob.print();
                        } catch (PrinterException e1) {
                            /*
                             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                             */
                            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                            // Otestuji, zda se podařilo načíst logger:
                            if (logger != null) {
                                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                                // Nejprve mou informaci o chybě:
                                logger.log(Level.INFO,
                                        "Zachycena výjimka pří pokusu o vytisknutí Print Screenu diagramu tříd, třída" +
                                                " Menu, metoda - po "
                                                + "kliknutí na položku v menu: itemSaveImageOfInstanceDiagram.");

                                /*
                                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                                 */
                                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
                            }
                            /*
                             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                             */
                            ExceptionLogger.closeFileHandler();
							
							
							JOptionPane.showMessageDialog(this, txtErrorWhilePrintingsClassDiagramText,
									txtErrorWhilePrintingsClassDiagramTitle, JOptionPane.ERROR_MESSAGE);							
						}
				}
				else
					JOptionPane.showMessageDialog(this,
							txtFailedToPrintClassDiagramText + "\n" + txtError + ": " + txtClassDiagram + " = null",
							txtFailedToPrintClassDiagramTitle, JOptionPane.ERROR_MESSAGE);
			}
			
			
			
			
			
			else if (e.getSource() == itemPrintInstanceDiagram) {
				// Zde se jedná v podstatě o to samé jako u výše uvedeného tisku diagramu tříd,
				// akorát, zde se jedná o vytisknutí PrintScreenu diagramu instancí:
				if (instanceDiagram != null) {
					final PrinterJob printJob = PrinterJob.getPrinterJob();
					printJob.setPrintable(new PrintGraph(instanceDiagram));
					
					if (printJob.printDialog())
						try {
							printJob.print();
                        } catch (PrinterException e1) {
                            /*
                             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                             */
                            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                            // Otestuji, zda se podařilo načíst logger:
                            if (logger != null) {
                                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                                // Nejprve mou informaci o chybě:
                                logger.log(Level.INFO,
                                        "Zachycena výjimka pří pokusu o vytisknutí PrintScreenu diagramu instancí, " +
                                                "třída Menu, metoda - po kliknutí na"
                                                + " položku v menu: itemPrintInstanceDiagram.");

                                /*
                                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                                 */
                                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
                            }
                            /*
                             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                             */
                            ExceptionLogger.closeFileHandler();
							
							JOptionPane.showMessageDialog(this, txtErrorWhilePrintingInstanceDiagramText,
									txtErrorWhilePrintingInstanceDiagramTitle, JOptionPane.ERROR_MESSAGE);							
						}
				}
				else
					JOptionPane
							.showMessageDialog(this,
									txtFailedToPrintInstanceDiagramText + "\n" + txtError + ": " + txtInstanceDiagram
											+ " = null",
									txtFailedToPrintInstanceDiagramTitle, JOptionPane.ERROR_MESSAGE);
			}
			
			
			
			
			
			
			
			
			else if (e.getSource() == itemRefreshClassDiagram) {
				// Kliknutím na toto tlačítko se přenačte diagram tříd tak, že se spustí vlákno, které
				// si najde adresář src v aktuálně otevřeném projektu a od tohoto adresáře se otestují, zda
				// jsou všechny třídy v idagramu tříd, které jsou (budou) nalezeny v adresáři src na disku:
				
				// Potřebuji si akorát zjistit cestu k adresáři src a tu před do vlákna, jako adresář, kde se má začít
				// prohledávat:
				final String pathToSrc = App.READ_FILE.getPathToSrc();
				
				if (pathToSrc != null)
					ThreadSwingWorker.runMyThread(new RetrieveNotLoadedClassesThread(new File(pathToSrc), classDiagram));
				

				
				// Po tom, co se počká až doto vlákno doběhne, spustí se další, které otestuje jednotlivé vztahy mezi třídamy
				// v diagramu tříd - pro případ, že by se přidala třída, která má nějaké vztahy na ostatní, tak aby se to zjistilo,
				// a v diagramu tříd také znázornilo:

				ThreadSwingWorker.runMyThread((new CheckRelationShipsBetweenClassesThread(classDiagram, outputEditor,
						classDiagram.isShowRelationShipsToItself(), classDiagram.isShowAssociationThroughAggregation())));
			}
			
			
			
			
			
			
			
			
			// Zde se jedná akorát o přenačtení vztahů mezi vytvořenými instancemi v diagramu instanci,
			// takže akorát spustím příslušné vlákno, které tyto vztahy otestuje:
			else if (e.getSource() == itemRefreshInstanceDiagram) {
                ThreadSwingWorker.runMyThread(new CheckRelationShipsBetweenInstances(instanceDiagram,
                        instanceDiagram.isShowRelationShipsToItself(), GraphInstance.isUseFastWay(),
                        GraphInstance.isShowRelationshipsInInheritedClasses(),
                        instanceDiagram.isShowAssociationThroughAggregation()));

                /*
                 * Výše se aktualizovaly vztahy mezi instancemi v diagramu instancí. I když by se v podstatě nic
                 * nemělo změnit, protože veškeré provedné operace by mezi instancemi měly být vždy správně
                 * zobrazeny, tak kdyby náhodou, je zde "pro jistotu" toto tlačítko pro aktualizaci vztahů, například
                 * kdyby se neprojevilo třeba naplnění nějaké proměnné apod. (nemělo by nastat), tak aby se to
                 * projevilo i v okně pro doplňování příkazů v editoru příkazů, je třeba aktualizovat i toto okno,
                 * aby se v něm například zobrazovaly správné hodnoty proměnných apod. Ale v tomto případě by se
                 * také nemělo nic změnit. Toto tlačítko je jen jako taková pojistka.
                 */
                App.getCommandEditor().launchThreadForUpdateValueCompletion();
            }












            else if (e.getSource() == itemRemoveAll) {
				// Postup:
				// Vymazat obsah adresaře src v projektu
				// vymazat class diagram
				// vymazat instance diagram
				// vymazat všechny vytvořené instance - pokud jsou
				
												
				
				// Vymazání adresáře src v adresáři projektu, lze to udělat několika způsoby, např napsat algoritmus, 
				// ktery si do pole načte list souboru - metoda list nad File, a cyklem projit tento list a vše smaazt 
				// nebo:
				// Smažu adresář src a novu ho vytvořím, to je asi nejjednodušši varianta a v podstatě nemůže dojít k chybám, jako že by se 
				// nějaky soubor nesmazal, - došlo by k nejake chybě apod.
				
				
				final String pathToProject = PreferencesApi.getPreferencesValue(PreferencesApi.OPEN_PROJECT, null);

				// Otestuji existenci adresáře projektu:
				if (pathToProject != null && ReadFile.existsDirectory(pathToProject)) {

					final File fileSrcDir = new File(pathToProject + File.separator + "src");
					final File fileBinDir = new File(pathToProject + File.separator + "bin");

					// Tuto podmínku nemohu dát moc konkrétní, protže kdyby napřílad adresář bin
					// neexistoval, tak by nebyla podmínka splněna a nevymazaly by se
					if (fileSrcDir.isDirectory() || fileBinDir.isDirectory()) {
						try {
							// Smažu původní adresář src:
							if (fileSrcDir.exists())
								FileUtils.deleteDirectory(fileSrcDir);


							// smažů původní adresář bin:
							if (fileBinDir.exists())
								FileUtils.deleteDirectory(fileBinDir);


							// Nyní vytvořím nový adresář src:
							Files.createDirectory(Paths.get(fileSrcDir.getPath()));


							// vytvořím nový adresář bin:
							Files.createDirectory(Paths.get(fileBinDir.getPath()));

						} catch (IOException e1) {
                            /*
                             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                             */
                            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                            // Otestuji, zda se podařilo načíst logger:
                            if (logger != null) {
                                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                                // Nejprve mou informaci o chybě:
                                logger.log(Level.INFO,
                                        "Zachycena výjimka při mazání a následně nového vytvoření adresářů 'src' a " +
                                                "'bin' v adresáři projektu\n"
                                                + "Adresář src: " + fileSrcDir.getAbsolutePath() + "\n"
                                                + "Adresář bin: " + fileBinDir.getAbsolutePath() + "\n"
                                                + "Třída menu.java, metoda - po kliknutí na tlačítko itemRemoveAll.");

                                /*
                                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                                 */
                                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
                            }
                            /*
                             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                             */
                            ExceptionLogger.closeFileHandler();
                        }
					}
				}
				
				// Nyní je vytvořem nový - prázdný adresář src v adresáři projektu:
				// Dále vymažu class diagram,.. - dle postupu:
				classDiagram.clearGraph(GraphClass.getCellsList());
				
				App.WRITE_TO_FILE.saveClassDiagram(GraphClass.getCellsList());
				
				instanceDiagram.clearGraph(GraphInstance.getCellsList());
				
				// vymazat všechny vytvořené instance - pokud jsou
				Instances.deleteAllInstancesFromMap();

                /*
                 * Výše se odstranili veškeré třídy a instance (z obou diagramů). Zde je tedy potřeba znovu načíst
                 * hodnoty do okna pro automatické doplňování / dokončování příkazů v editoru příkazů, aby se v tom
                 * okně například nenabízely již neexistující třídy či instance apod.
                 */
                App.getCommandEditor().launchThreadForUpdateValueCompletion();
			}









			// vymažu diagram instancí a všechny již vytvořené instance tříd z diagramu tříd:
			else if (e.getSource() == itemRemoveInstanceDiagram) {
				instanceDiagram.clearGraph(GraphInstance.getCellsList());
				
				Instances.removeAllClassInstances();

                /*
                 * Výše se odebrali instance tříd z diagramu tříd, které se nacházeli v diagramu instancí. Resp.
                 * uživatel odebral veškeré vytvořené instance. Zde je třeba aktualizovat hodnoty nabízené v okně pro
                 * autmatické doplňování / dokončování příkazů v editoru příkazů, aby se v tom okně nenabízeli již
                 * neexistující instance apod.
                 */
                App.getCommandEditor().launchThreadForUpdateValueCompletion();
			}
			
			
			
			
			
			
			
			
			// Otevřeni souboru v editoru zdrojového kódu:
			else if (e.getSource() == itemOpenFileInCodeEditor) {
				// Zobrazím dialog pro výběr cesty k dané třídě:
				final String pathToClass = new FileChooser().getPathToClassOrTxtFile(txtChooseClassOrTxtDialogTitle);
								
				if (pathToClass != null) {
					// Zde si uživatel označil třídu pro otevření kódu v editoru:
					
					// Zde mám pro dialog připravené vše, co v této chvíli potřebuji, tak mohu vytvořit jeho instanci,
					// nastavitm mu uživatelem vzolený jazyk pro texty do dialogu a zviditelnit ho:
					final CodeEditorDialog ced = new CodeEditorDialog(pathToClass, languageProperties);
					ced.setLanguage(languageProperties);
					ced.setVisible(true);
				}
			}
			
			
			
			
			
			
			/*
			 * Otevření nějakého souboru v průzkumníku projektů s kořenovým adresářem v
			 * adresáři označeným jako workspace.
			 */
			else if (e.getSource() == itemOpenFileInProjectExplorer) {
				/*
				 * Načtu si soubor, který chce uživatel otevřít v průzkumníku projektu.
				 */
				final String pathToFile = new FileChooser().getPathToNewFileForOpen();

				// Pokud uživatel nevybral soubor pro otevření, pak mohu skončit:
				if (pathToFile == null)
					return;

				
				/*
				 * Do průzkumníku projektů si potřebuji načíst i adresář, který je označen jako
				 * workspace.
				 */
                final String pathToWorkspace = ReadFile.getPathToWorkspace();

                if (pathToWorkspace != null)
                    new ProjectExplorerDialog(new File(pathToWorkspace), languageProperties, new File(pathToFile), null, false);

                else
                    JOptionPane.showMessageDialog(this, txtPathToWorkspaceDoNotFoundText,
							txtPathToWorkspaceDoNotFoundTitle, JOptionPane.ERROR_MESSAGE);
			}
			
			
			
			
			
			
			
			
			
			
			// po kliknutí na toto tlačítko se zobrazí okno coby Výstupní terminál, kam se vypisuji
			// metody v System.out a err:
			else if (e.getSource() == itemShowOutputFrame)
				RedirectApp.markOutputFrame();
			
			
			
			
			
			
			
			
			
			// Položky v menu Tools:
            else if (e.getSource() == itemCompile) {
                // Po kliknutí na tuto položku se zkompilují veškeré třídy v aktuálně otevřeném projektu:
                // Postup:
                // Načtu si do kolekce všechny objekty
                // Tuto kolekci prohledám a zjistím si které z nich jsou třídy, pokud existuje, tak budu znát i její
                // path
                // a zkompiluji jí:

                /*
                 * Zkompilování tříd v diagramu tříd a aktualizace okna s příkazu pro doplňování / dokončování
                 * příkazů v editoru příkazů:
                 */
                compileClassesInClassDiagram();
			}
			
			
			
			
			
			else if (e.getSource() == itemStart) {
				// Postup:
				// Načtu si všechny třídy z diagramu tříd, otestuje, zda existuji, pak je zkompiluji a načtu si
				// soubor .Class, a z v něm zkusím najít hlavní - spouštěcí metodu main, pokud ji najdu hledání ukončím a metodu zavolám
				// čímž "spustím projektu", v takovém případě již aplikace není schopná zobrazovat vytvořené instance v diagramu instncí
								
				
				
				
				// Kolekce, do které budu ukládat základní informace o třídách, ve kterých se nachází metoda
				// main, abych ji mohl zavolat, více viz příslušný objekt - třída MainMethodInfo
				final List<MainMethodInfo> mainMethodList = new ArrayList<>();	
				
				// Zjistím si cestu k adresáři src ve workspace - pokud existuje:
				final String pathToSrc = App.READ_FILE.getPathToSrc();
				
				// Cesta k adresáři bin pro načtení zkompilované třídy:
				if (pathToSrc != null) {
					// Načtu si všechny třídy - objekty v diagramu tříd:
					final List<DefaultGraphCell> cellsList = GraphClass.getCellsList();
					
					// načtu si list souborů pro předání "mému kompilátoru", aby je mohl všechny třídy v diagramu 
					// tříd zkompilovat:
					final List<File> filesList = App.READ_FILE.getFileFromCells(pathToSrc, cellsList, outputEditor,
							languageProperties);
					
					// Otestuji, zda byl úspěšně předán vytvořen a získán list:
					if (filesList != null) {
						// vytvořím si instnaci pro kompiltor a zkompiluji všechny třídy:
						new CompileClass(languageProperties);
						final boolean compiledWithoutError = CompileClass.compileClass(outputEditor, filesList, true, false);

						// Otestuji, zda byly zkompilovány všechny třídy bez chyby:
						if (compiledWithoutError) {
							// Zde jsou všechny třídy zkompilovány bez chyby tak mohu postupně projít všechny třídy a získat
							// si jejich přeloženou verzi:

							// To udělám tak, že si projdu celou kolekci objektů
							// v diagramu tříd a otestuji zda se jedná o  třídu a zkusím si získat její zkompilovanou verzi
							// - .class:
							for (final DefaultGraphCell d : cellsList) {
								// Jedná se o tridu a ne nic jineho: Note stačila by pouze podínka pro zjistění
								// bunky, v takovém případě to nic jiného již být nemůže,
								// - pouze prevence:
								if (!(d instanceof DefaultEdge) && GraphClass.KIND_OF_EDGE.isClassCell(d)) {
									// Zkusím zde načíst zkompilovanou třídu:
									// final Class<?> loadedClass =
									// readFile.loadCompiledClass(d.toString(),
									// outputEditor, pathToSrc);
									final Class<?> loadedClass = App.READ_FILE.loadCompiledClass(d.toString(),
											outputEditor);
									
									// Otestuji, zda se v pořdku načetla:
									if (loadedClass != null) {
										// zde se v pořádku načetla, tak mohu zkusit zjistit, zda obsahuje hlavni
										// metodu main: Zde se mi podařilo načíst danou třídu
										// z diagramu tříd: Zkusím najít metodu main:
										
										final Method mainMethod = getMainMethodFromClass(loadedClass);

										// sem se dostanu, pokud se podařilo
										// najít metodu main a nevyskočila
										// vyjímka tak ji vytvořím do kolekce
										// nový objekt:
										if (mainMethod != null)
											mainMethodList.add(new MainMethodInfo(loadedClass.getName(), mainMethod));
										
									}
									// else - Zde nebyla načtena zkompilovaná třída, tak nic nedělám, a jdu na další
									// iteraci - hledat další přeloženou třídu
								}
								// else - zde se jedná o hranu, tak nic nedělám,
								// ta mně nezajímá
							}
						}
					}
					
					
					
					// Nyní otetuji, kolik bylo nalezených tříd, které obsahují hlavní - spuštěcí metodu main
					// Pokud existuje jedna třída, která ji obsahuje, tak zavolám danou metodu v dané třídě,
					// Pokud neexistuje žádná třída, která obsahuje metodu main, tak vypíšu oznámení uživateli 
					// a na konec, pokud bylo nalezeno 2 a více tříd, které obsahují metodu main, zobrazím dialog
					// ve kterém si uživatel vybere třídu, ve které se má zavolat daná metoda main:

					if (mainMethodList.size() == 1) {
						// Zde byla nalezena pouze jedna třída, ve které se
						// nachází metoda main, tak ji zavolám:
						final String className = mainMethodList.get(0).getRbMain().getText();
						final Method main = mainMethodList.get(0).getMainMethod();
						callTheMainMethod(main, className);
					}
					
					else if (mainMethodList.size() > 1) {
						// Zde existují minimálně dvě třída, které obsahují metodu main, tak zobrazím dialog, ve kterém si uživatel 
						// zvolí třídu, ve které se má zavolat daná metoda:
						final SelectMainMethodForm smmf = new SelectMainMethodForm(mainMethodList);
						smmf.setLanguage(languageProperties);
						
						// Zavolám metodu, která zviditelní dialog a vrátí označenou metodu:
						final Object[] selectMainMethodInfo = smmf.getSelectedMainMethodInfo();
						
						
						if (selectMainMethodInfo != null) {
							// Zde uživatel klikl na OK s označenou třídou, resp. třídou, která obsahuje danou metodu main:
							// tak ji zavolám:
							final Method mainMethod = (Method) selectMainMethodInfo[0];
							final String className = String.valueOf(selectMainMethodInfo[1]);
							
							// Mohuu se pokusit zavolat metodu:
							callTheMainMethod(mainMethod, className);
						}
					}
					// Zde nebyla nalazena žádná třída, která by obsahovala metodu main, tak vypíšu oznámení uživateli o nenalezené metodě main:
					else
						JOptionPane.showMessageDialog(this, txtMainMethodNotFoundText, txtMainMethodNotFoundTitle,
								JOptionPane.ERROR_MESSAGE);
				}
				else
					JOptionPane.showMessageDialog(this, missingSrcDirText, missingSrcDirTitle,
							JOptionPane.ERROR_MESSAGE);						
			}
			
			
			
			
			
			
			else if (e.getSource() == itemSettings) {
				// Otevře se dialogové okno s nastavením "v podstatě celé aplikace" fonty, grafy, písmo, editor, ...
				final SettingsForm sf = new SettingsForm(commandEditor, outputEditor, app);
				sf.setLanguage(languageProperties);
				sf.setVisible(true);
			}
			
			
			
			
			
			
			
			
			
			
			else if (e.getSource() == itemFontSize) {
				/*
				 * Načtu si Default.properties z adresáře ve workspace nebo výchozí soubor z
				 * adresáře aplikace:
				 */
				final Properties defaultProp = ReadFile.getDefaultPropAnyway();
				
				
				 final FontSizeFormType formType;
				 final boolean valueForMoving;
				 
				 if (defaultProp != null) {					 
					valueForMoving = Boolean
							.parseBoolean(defaultProp.getProperty("FontSizeForm_UpdateDiagramsWhileMovingWithSlider",
									String.valueOf(Constants.UPDATE_DIAGRAMS_WHILE_MOVING_WITH_SLIDER)));

					/*
					 * Získám si textovou hodnotu ze souboru, popřípadě z Constants:
					 */
					final String tempFormType = defaultProp.getProperty("FontSizeForm_SelectedPanel",
							Constants.FONT_SIZE_FORM_TYPE_SELECTED_PANEL.getValue());

					if (tempFormType.equals(FontSizeFormType.ONE_SAME_SIZE.getValue()))
						formType = FontSizeFormType.ONE_SAME_SIZE;

					else if (tempFormType.equals(FontSizeFormType.ONE_SIZE_FOR_CHOOSED_OBJECTS.getValue()))
						formType = FontSizeFormType.ONE_SIZE_FOR_CHOOSED_OBJECTS;

					else if (tempFormType.equals(FontSizeFormType.ONE_SIZE_FOR_EACH_OBJECT.getValue()))
						formType = FontSizeFormType.ONE_SIZE_FOR_EACH_OBJECT;

					// Výchozí hodnota:
					else
						formType = Constants.FONT_SIZE_FORM_TYPE_SELECTED_PANEL;
				}

				else {

					valueForMoving = Constants.UPDATE_DIAGRAMS_WHILE_MOVING_WITH_SLIDER;

					formType = Constants.FONT_SIZE_FORM_TYPE_SELECTED_PANEL;
				}
				
				 
				 
				 

				/*
				 * Vytvořím dialog:
				 */
				final FontSizeForm fsf = new FontSizeForm(languageProperties, formType, classDiagram, instanceDiagram,
						valueForMoving);

				fsf.setLanguage(languageProperties);

				fsf.setVisible(true);
			}
			
			
			
			
			
			
			
			
			
			
			
			
			/*
			 * Položka pro otevření dialogového okna pro nastavení domovského adresáře Javy
			 * nebo potřebných souborů pro kompilaci Javovských tříd pomocí aplikace.
			 */
			else if (e.getSource() == itemJavaSettings) {
				// Postup:
				// zobrazí se dialog odkud se načtou cesty k adresáři "libTools", tj. adresář, kde se nachází adresář
				// lib a ten obsahuje ty tří soubory - níže
				// POkud se vybrala výchozí cesta v adresáři apliace, mělo by to být bez problému,
				// pokud se vybrala cesta někde na disku, tak by to mělo být také bez probělému, když bude příslušný
				// adresář obsahovat ty tří soubory,
				// a pokud se vybere to kopírování do workspace//configuration//libTools, tak jde jen o to,
				// zda se podaří vytvořit adresáře a nakopírovat do nich soubory, pak by to mělo také fungovat
				
				
				// Zde se má otevřít dialog pro nastavení domovského adresáře pro Javu nebo 
				// cesty k vybraným souborům potřebné pro kompilaci, aby se zkopírovali do adresáře libTools
				// v aplikaci:				
				
				final JavaHomeForm jhf = new JavaHomeForm();
				jhf.setLanguage(languageProperties);
				
				final JavaHomeInfo jhi = jhf.getJavaHomeInfo();
				
				if (jhi != null) {
					// Zde se kliklo na OK, neboli nezavřel se dialog:
					// tak mohu nastavit příslušná data:
					
					// Nejprve otestuji pomocí proměnné: isPathToJavaHomeDir, zda se má nastavit domovský adresář 
					// nebo nakopírovat soubory:
					if (jhi.isPathToJavaHomeDir())
						// Zde se jedná o nastavení domovského adresáře pro Javu																	
						PreferencesApi.setPreferencesValue(PreferencesApi.COMPILER_DIR, jhi.getPathToJavaHomeDir());
						
					
					
					else {
						// Zde se mají nakopírovat nějaké soubory do adresáře libTools v configuration ve Workspace
						
						// Nejprve otestuji, zda adresář libTools\\lib  vůbec existuje, pokud ano, tak mohu přesouvat soubory.
						final String pathToWorkspace = ReadFile.getPathToWorkspace();
						
						if (pathToWorkspace != null) {
							// Zde existuje workspace, tak otestuji existenci adresáře configuration:
							if (!ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
								// Zde neexistuje configuration adresář ve Workspace, tak ho nakopíruji:
								WriteToFile.createConfigureFilesInPath(pathToWorkspace);
							
							
							// Zde pro jistotu znovu otestuje, zda existuje adresář configuration ve Workspace:
							if (ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME)) {
								// Zde existuje adresář configuration, tak otestuji existenci adresáře libTools a lib,
								// a pokud existuje, tak je vytvořím:
								final String pathToLib = pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME + File.separator
										+ Constants.COMPILER_LIB_DIR;
								
								
								if (!ReadFile.existsDirectory(pathToLib)) {
									// Zde adresář libTools a lib neexistuje, tak jej vytvořím:
									try {
										Files.createDirectories(Paths.get(pathToLib));
                                    } catch (IOException e1) {
                                        /*
                                         * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                                         * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                                         * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                                         * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                                         */
                                        final Logger logger = ExceptionLogger.getLogger(ExtractEnum
                                                .EXTRACT_EXCEPTION);

                                        // Otestuji, zda se podařilo načíst logger:
                                        if (logger != null) {
                                            // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                                            // Nejprve mou informaci o chybě:
                                            logger.log(Level.INFO,
                                                    "Zachycena výjimka při vytváření adresářů na cestě: " + pathToLib);

                                            /*
                                             * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                                             * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v
                                             * jaké metodě apod. Proto vypisuji celou "informaci" o výjimce.
                                             */
                                            logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
                                        }
                                        /*
                                         * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                                         */
                                        ExceptionLogger.closeFileHandler();
                                    }
                                }
								
								
								
								// Zde opět otestuji, zda se podařilo vytvořit adresář libTools, prootže pokud došlo
								// k chybě, tak zde by ten kód také selhal - když by se pokračovalo po zachycení vyjímky:
								if (ReadFile.existsDirectory(pathToLib)) {
									// zde tedy již existují adresáře libTools a lib, tak do něj mohu nakopírovat označené soubory
									// a nastavit cestu k této složce libTools, do adresáře lib se již bude dívat kompilátor:									
									
									// Note:
									// zde již nebudu zjišťovat, zda soubor ještě existuje, běhěm té chvíle by se soubor smazat něměl:
									// Popř. to zachytí vyjímka při přesouvání souborů
									
									// Note: názvy se nejspíše měnit nebudou a měly by být pořád stejné (názvy těch tří souborů), tak
									// je zde mohu zadat na pevno, a navíc jen tyto názvy projdou výběrem v dialogu:
									if (jhi.getPathToToolsJar() != null)
										copyFiles(jhi.getPathToToolsJar(), pathToLib + File.separator + Constants.COMPILER_FILE_TOOLS_JAR);
									
									if (jhi.getPathToCurrencyData() != null)
										copyFiles(jhi.getPathToCurrencyData(),pathToLib + File.separator + Constants.COMPILER_FILE_CURRENCY_DATA);
									
									if (jhi.getPathToFlavormapPro() != null)
										copyFiles(jhi.getPathToFlavormapPro(), pathToLib + File.separator + Constants.COMPILER_FILE_FLAVORMAP_PROPERTIES);
									
									
									// sen se dojde, po zkopírování potřebných souborů, tak mohu nastavit cestu k adresáři libTools
									// ve workspace pro kompilátor:
									PreferencesApi.setPreferencesValue(PreferencesApi.COMPILER_DIR, pathToWorkspace
											+ File.separator + Constants.CONFIGURATION_NAME + File.separator + Constants.COMPILER_DIR);
								}
							}
						}
					}
				}
			}
			
			
			
			
			
			
			
			
			/*
			 * Položka pro spuštění vlákna, které slouží pro vyhledání potřebných souborů,
			 * které aplikace potřebuji, aby mohla kompilovat Javovské třídy, tyto soubory,
			 * pokud je najde, pak je zkopíruje do workspace/configuration/libTools/lib/zde.
			 * Pokud je nenajde, pak není co řešit, uživatel musí využit dialog nebo se
			 * nikde na disku ani nenachází.
			 */
			else if (e.getSource() == itemJavaSearch) {

				/*
				 * Vytvořím si vlákno, které se má vykonat ve swingworkeru:
				 */
				final CheckCompileFiles ccf = new CheckCompileFiles(App.PATH_TO_JAVA_HOME_DIR, true);
				
				ThreadSwingWorker.runMyThread(ccf);
				
				
				try {
					/*
					 * Tato část je doplněna pouze pro tyto účely. Jedná se o to, že kdybych
					 * zobrazil JOptionPane ve vlákně cff, pak by pořád bylo zobrazeno okno s
					 * načítáním (loading dialog), což nechci. Tak si počkám, něž příslušné vlákno
					 * doběhne (pokud ještě nedoběhlo) a teprve potom zobrazím výsledky ohledně
					 * toho, jak dopadlo to vlákno, například, že všechny soubory jsou již nalezeny,
					 * nebo naopak se je nepodařilo naléžt, ať jej nastaví v dialogu apod.
					 * 
					 * Další možností by bylo doplnit například do rozhraní: MySwingWorkerInterface
					 * další metodu, například 'done' apod. a da by se zavolala v metodě 'done' ve
					 * třídě MySwingWorker, ale pak by bylo třeba tuto metodu implementovat v každé
					 * třídě, jejiž nějaká metoda se vykonává pomocí uvedeného MySwingWorkeru, resp.
					 * bylo by potřeba tuto metodu implementovat v každé třídě, která implemetnuje
					 * uvedené rozhraní.
					 * 
					 * Pak stačilo akorát otestovat v metodě 'done' v MySwingWorker jestli se jedná
					 * o příslušnou třídu, tak zavolat tu metodu done, nebo ji zavolat pokaždé a to
					 * tělo vyplnit jen někde apod. Jelikož toto používám pouze jednou, jinde žádné
					 * výpisy ohledně provedení či neprovedení vlákna apod. nepoužívám, napsal jsem
					 * tuto variantu, pokud by bylo potřeba doplnit někde jinde ty výpisy po
					 * provedené vlákna pro oznámení výsledků apod. Pak by doplnil tu metodu do
					 * uvedeného rozhraní a zavolal ji pokaždé, když to vlákno doběhne.
					 */
					if (ccf.isAlive())
						ccf.join();
					
					// Zde vlákno doběhlo, tak zobrazím výsledky:
                    ccf.showResult();

                } catch (InterruptedException e1) {
                    /*
                     * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                     * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                     * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                     * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                     */
                    final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                    // Otestuji, zda se podařilo načíst logger:
                    if (logger != null) {
                        // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                        // Nejprve mou informaci o chybě:
                        logger.log(Level.INFO,
                                "Zachycena výjimka při volání metody join nad vláknem, které prohledávalo disk " +
                                        "zařízení za účelem najít potřebné "
                                        + "soubory pro kompilování tříd v aplikaci. Tato výjimka může nastat pokud " +
                                        "nějaký podproces přerušil aktuální "
                                        + "vlákno. Stav přerušení aktuálního podprocesu se vymaže, když je tato " +
                                        "výjimka vyhodena.");

                        /*
                         * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                         * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                         * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                         */
                        logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
                    }
					
					/*
					 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
					 */
					ExceptionLogger.closeFileHandler();
					
				}
			}
			
			
			
			
			
			
			
			
			
			
			// Otevře se dialog průzkumníku projektů s kořenem v adresáři označeným jako
			// Wroskapce
			else if (e.getSource() == itemPeWorkspace) {
				final String pathToWorkspace = ReadFile.getPathToWorkspace();

				if (pathToWorkspace != null)
					new ProjectExplorerDialog(new File(pathToWorkspace), languageProperties);

				else
					JOptionPane.showMessageDialog(this, txtPathToWorkspaceDoNotFoundText,
							txtPathToWorkspaceDoNotFoundTitle, JOptionPane.ERROR_MESSAGE);
				
				
				
				// MOŽNÁ DOPSAT I OSTATNÍ HLÁŠKY - ŽE NEBYL NALEZEN SOUBOR
				// .PROPERTIES A ZE NEBYL NALEZENA TA HODNOTA ????
			}
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
 
			// Otevře se dialog průzkumníku projektů s kořenem coby adresář otevřeného projektu:
			else if (e.getSource() == itemPeOpenProject) {
				// Otestuji, zda je otevřený nějaký projekt
				// Načtu si cestu k otevřenému projektu, otetuji, zda příslušný adresář existuje a pokud ano,
				// tak víc ani řešit nemusím, pokud tento adresář projektu neexistuje, vypíšu uživateli hlášku,
				// a pokud existuje, tak otevřu příslušný dialog s kořeném v příslušném adresáři,
				// pokud je prázdný, tak to se zde neřeší:
				
				final String pathToOpenProject = PreferencesApi.getPreferencesValue(PreferencesApi.OPEN_PROJECT, null);
				
				
				if (pathToOpenProject != null) {
					
					// Otestuji, zda je vůbec nějaký projekt otevřený:
					if (!pathToOpenProject.equals("Closed")) {
						
						// // Zde nebyla načtena hodnota null, ani Closed, takže by měl být nějaký projektu otevřen,
						// tak otestuji, zda existuje, a případně mohu otevřít dialog, pokud neexistuje, vypíšu oznámení:
						final File fileProject = new File(pathToOpenProject);
						
						if (fileProject.exists() && fileProject.isDirectory())
							// Zde příslušný adresář existuje a je to adresář, tak mohu otevřít dialog
							// coby průzkumník projektů s příslušným adresářem coby kořen:
							new ProjectExplorerDialog(fileProject, languageProperties, classDiagram);
						
						else
							JOptionPane.showMessageDialog(this,
									txtOpenDirDoesNotExistText + "\n" + txtOriginalLocation + ": " + pathToOpenProject,
									txtOpenDirDoesNotExistTitle, JOptionPane.ERROR_MESSAGE);
					}
					else
						JOptionPane.showMessageDialog(this, txtProjectIsNotOpenText, txtProjectIsNotOpenTitle,
								JOptionPane.ERROR_MESSAGE);
				}				
			}





			else if (e.getSource() == itemFeWorkspace) {
                final String pathToWorkspace = ReadFile.getPathToWorkspace();

                if (pathToWorkspace == null) {
                    JOptionPane.showMessageDialog(this, txtPathToWorkspaceWasNotFoundText,
                            txtPathToWorkspaceWasNotFoundTitle, JOptionPane.ERROR_MESSAGE);

                    return;
                }

                DesktopSupport.openFileExplorer(new File(pathToWorkspace), true);
            }



            else if (e.getSource() == itemFeOpenProject) {
                final String openProject = PreferencesApi.getPreferencesValue(PreferencesApi.OPEN_PROJECT, null);

                if (openProject == null) {
                    JOptionPane.showMessageDialog(this, txtPathToOpenProjectWasNotFoundText,
                            txtPathToOpenProjectWasNotFoundTitle, JOptionPane.ERROR_MESSAGE);

                    return;
                }

                // Otevření průzkumníku:
                DesktopSupport.openFileExplorer(new File(openProject), true);
            }


			
			
			
			
			
			
			
			
			// Položky v menu Help:
			else if (e.getSource() == itemInfo) {
				// Zobrazí se dialog s informace o aplikaci
				final InfoForm infoForm = new InfoForm();
				
				infoForm.setLanguage(languageProperties);
				infoForm.setVisible(true);
			}
			
			
			
			
			else if (e.getSource() == itemAboutApp) {
				// Zobrazí se dialog s informaceni o autorovi aplikace
				final AboutAppForm aboutAppForm = new AboutAppForm();
				
				aboutAppForm.setLanguage(languageProperties);
				aboutAppForm.setValueToConfigurationInfoLabels();

				aboutAppForm.setVisible(true);
			}
		}
	}









    /**
     * Metoda, která si načte konfigurační soubor Default.properties a z něj si získá nastavený způsob řazení položek
     * reprezentující dříve otevřené projekty. Dle toho pak označí radiobuttony, které "říkají" uživateli, jaký je
     * aplikován způsob řazení.
     */
    private static void prepareRbsForSortingRecentlyProjects() {
        // Získání konfiguračního souboru:
        final Properties defaultProperties = ReadFile.getDefaultPropAnyway();

        // Získání hodnoty z konfiguračního souboru:
        final String sortValue = defaultProperties.getProperty("Sorting of recently opened projects", Constants
                .DEFAULT_SORT_RECENTLY_OPENED_PROJECTS.getValue());


        // Získání nastaveného způsobu řazení položek reprezentující dříve otevřené projekty:
        final SortRecentlyOpenedProjects sortRecentlyOpenedProjects = SortRecentlyOpenedProjects.checkValue(sortValue);


        /*
         * Nastavím označených radiobuttonů, který definuje příslušný způsob řazení a získanou hodnotu nastavím i do
         * proměnné, aby se dle toho seřadily položky v příslušné rozevíracím menu.
         */
        if (sortRecentlyOpenedProjects == SortRecentlyOpenedProjects.ASC) {
            Menu.sortRecentlyOpenedProjects = SortRecentlyOpenedProjects.ASC;
            rbItemSortRecentlyProjectsAsc.setSelected(true);
        }

        else if (sortRecentlyOpenedProjects == SortRecentlyOpenedProjects.DES) {
            Menu.sortRecentlyOpenedProjects = SortRecentlyOpenedProjects.DES;
            rbItemSortRecentlyProjectsDes.setSelected(true);
        }
    }







    /**
     * Metoda pro zkompilování veškerých tříd v diagramu tříd.
     * <p>
     * Jedná se o zkompilování tříd například po kliknutí na tlačítko zkompilovat, když chce uživatel zkompilovat
     * veškeré třídy v diagramu tříd nebo po přidání nové (již existující) třídy do diagramu, tak aby se například
     * hodnoty doplnily do okna pro autmatické doplnňování příkazů do editoru příkazů - aby se v něm zobraila nově
     * přidaná třída apod.
     * <p>
     * (Metoda by mohla být například někde v diagramu tříd apod. Ale toto "specifické" využití (nastavené parametry a
     * hlášky) má pouze v této třídě.)
     */
    private void compileClassesInClassDiagram() {
        // Note: Zde si ještě mohu zjistit, jaké třídy se nachází v adresáři src, a všechny je zkompilovat ,jenže
        // některé třídy nemusí být načteny v diagramu, proto by se k nim nemohlo pomocí aplikace přistupovat
        // - až na vyjímky, jako je například otevření kodu třídy, apod.
        // "V apliakci by se s ní nedalo pracovat"


        // Získám si cestu k adresáři src - pokud existuje:
        final String pathToSrc = App.READ_FILE.getPathToSrc();

        // Otrestuji, zda existuje adresář src a mám k němu cestu:
        if (pathToSrc != null) {

            // Nejprve je třeba otestovat, aby se v Diagramu tříd vůbec nějaké třídy nacházely,
            // jinak by se vypsala hlášky, že se nepodařilo předat soubory - třídy, ale to se tyka .class souboru:
            if (GraphClass.areInDiagramAtLeastOneClass()) {
                // Zde se v Diagramu tříd nachází alespoň jena trida

                // získám si převedené třídy z diagramu tříd tak jejich cestu na disku
                final List<File> filesList = App.READ_FILE.getFileFromCells(pathToSrc,
                        GraphClass.getCellsList(), outputEditor, languageProperties);

                // všechny třídy zkompiluji:
                new CompileClass(languageProperties);
                CompileClass.compileClass(outputEditor, filesList, true, true);
            }

            // Zde se v Diagramu nenachází žadný objekt, který reprezentuje tridu:
            else
                JOptionPane.showMessageDialog(this, txtClassDiagramDoesntContainClassText,
                        txtClassDiagramDoesntContainClassTitle, JOptionPane.INFORMATION_MESSAGE);
        }
    }









    /**
     * Metoda, která zapíše změněnou hodnotou pro způsob řazení do konfiguračního souboru.
     * <p>
     * (Note: Jedná se pouze o zápis do konfiguračního souboru, který se najde ve workspace, pokud se ve workspace
     * soubor nenajde, nic se neprovede.)
     *
     * @param sortRecentlyOpenedProjects
     *         - nově uživatelem nastavený způsob řazení, který se zapíše do konfiguračního souboru.
     */
    private static void writeChangeToDefaultProp(SortRecentlyOpenedProjects sortRecentlyOpenedProjects) {
        // Zkusím si z workspace načíst soubor Default.properties:
        final EditProperties editProperties = App.READ_FILE.getPropertiesInWorkspacePersistComments(Constants
                .DEFAULT_PROPERTIES_NAME);

        /*
         * Pokud se nepodaří načíst soubor z workspace, tak nebudu nic dělat, jedná se pouze o to, že uživatel změnil
         * způsob řazení projektů (/ položek) v rozevíracím menu, což nepovažuji za "důležité" v rámci funkce aplikace.
         *
         * Proto, když se zde nepodaří soubor načíst, nebudu zde řešit příkapdy, kdy budu testovat, zda workspace
         * existuje,
         * zda v něm existuje adresář configuration apod.
         *
         * Způsob řazení, který uživatel nastaví se dále ukládá ("runtimově") do proměnné cz.uhk.fim.fimj.menu
         * .Menu#sortRecentlyOpenedProjects,
         * takže se způsob řazení při běhu aplikace vždy nastaví správně, ale po restartu aplikace bude jako výchozí
         * načtený způsob
         * řazení ten, který se načte z konfiguračního souboru.
         *
         * Případy, kdy konfigurační soubor neexistuje apod. se řeší při restartu aplikace nebo v nastavení apod. Zde
         * to mohu vynechat.
         */
        if (editProperties == null)
            return;

        editProperties.setProperty("Sorting of recently opened projects", sortRecentlyOpenedProjects.getValue());

        // Uložím provedené změny:
        editProperties.save();
    }



	
	
	
	
	
	
	
	
	/**
	 * Metoda, která se pokusí získat hlavní - spuštěcí main metodu z dané třídy.
	 * buď vrátí tu metodu main - statickou, veřejnou, bez návratové hodnoty a s
	 * parametrem jednorozměrného pole typu String
	 * 
	 * Note:
	 * Tuto metodu jsem přepsal oproti té co je pod touto metodou zakomentovaná, jen
	 * kvůli tomu, že když jsem se pokoušel získat tu hlavní main metodu, tak
	 * nastala vyjímka, když ta metoda v dané třídě nebyla a akorát by se pořad
	 * psali tyto vyjímky do logu, tak jsem napsal tuto metodu znovu ale zmenil jsem
	 * způsob získávání hlavní - spoustěci metody main poocí cyklu, kdy se projdou
	 * vseechny metody v dane tříde a otestuje se, zda je daná metoda verejna,
	 * staticka, navratovy typ hodnoty je void a jako parametr obsahuje
	 * jednorozměrné pole typu String, takto by měla být hlavní - spousteci metody
	 * main - ostatní ne
	 * 
	 * @param clazz
	 *            - přeložená - zkompilovaná třída z diagramu tříd - soubor .class,
	 *            ze které se má metoda pokusit získat metodu main
	 * 
	 * @return hlavní metodu main, pokud bude v dané třídě nalezena, jinak null
	 */
	public static Method getMainMethodFromClass(final Class<?> clazz) {
        // hlavní - spouštěcí main metoda nebude u výčtu - z těch se nevytváří instance
        if (clazz == null || clazz.isEnum())
            return null;

        try {
            final Method[] methodsArray = clazz.getDeclaredMethods();

            for (final Method m : methodsArray) {
                if (Modifier.isStatic(m.getModifiers()) && m.getName().equals("main")
                        && m.getReturnType().equals(Void.TYPE) && Modifier.isPublic(m.getModifiers())) {

                    final Type[] typesArray = m.getParameterTypes();
                    final Type[] stringType = {String[].class};

                    if (Arrays.deepEquals(typesArray, stringType))
                        return m;
                }
            }

        } catch (SecurityException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka pří získávání metody 'main' z třídy: " + clazz.getName()
                        + " v diagramu tříd, třída Menu.java, metoda getMainMethodFromClass(). Tato chyba může " +
                        "nastat například v případě nějaké chyba Class loaderu, "
                        + "například Class loader volajícího není stejný jako předchůdce třídy loader pro " +
                        "aktuální třídu a další.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }

        return null;
    }









    /**
     * Metoda, k terá slouží pro "úplné" otevření projektu v aplikaci. V tomto případě se jedná o to, že se nejprve
     * uloží otevřený projekt - je li, odeberou se veškeré objekty z diagraů, nastaví se titulky oken atd.
     * <p>
     * (Mimo jiné se zavolá i metoda openProjectInApp, která se stará pouze o "otevření" samotného projektu, ne "věci"
     * popsané výše.)
     * <p>
     * Jedná se o uložení a vymazání diagramu tříd, smazání oznčených ale nevytvořených vztahů mezi třídami, odebrání
     * instancí, změnu titulků apod. Viz tělíčko metody.
     *
     * @param pathToProjectDir
     *         - cesta k projektu, který se má otevřít - může být null, ale v takovém případě se nic neprovede.
     */
    private void openProject(final String pathToProjectDir) {
        // Otestuji, zda okno bylo zavreno nebo byl vybran nejaky soubor:
        if (pathToProjectDir == null)
            return;

        // Pokud je otevřený projekt, tak do něj uložím class diagram pro pozdější
        // načtení:
        App.WRITE_TO_FILE.saveClassDiagram(GraphClass.getCellsList());

        // Dále vymažu diagram instancí a všechny instance - pokud jsou nějaké
        // vytvořené:
        instanceDiagram.clearGraph(GraphInstance.getCellsList());

        // vymažu instance:
        Instances.deleteAllInstancesFromMap();

        // vymažu graf:
        classDiagram.clearGraph(GraphClass.getCellsList());

        // zruším označené, ale nevytvořené vztahy
        GraphClass.deselectChoosedEdge();

        // Vymazat obsah editoru příkazů a jeho historii:
        commandEditor.clearEditor();

        // Vymazat obsah editoru výstupů a nastavit výchozí hodnoty:
        outputEditor.clearEditor();

        // Metoda, která otevře zvolený projekt v aplikaci:
        openProjectInApp(pathToProjectDir);

        // Zde se otevřel nový projekt, změním titulek výstupního terminálu, kde je zobrazen otevřený projekt:
        RedirectApp.changeOutputFrameTitle();
    }










    /**
     * Metoda, která otevře zvolený projekt v aplikaci.
     * <p>
     * Otestuji, zda existuje adresář toho zvoleného, případně se vněm vytvoří adresář src, a zkusí se načíst data do
     * diagramu tříd - pokud jsou v daném adresáři uložena, a pak se prohledá adresář src - pokud existuje a otestuje
     * se, zda se v idagramu tříd opravdu vyskytují všechny třídy, které se nacházejí v adresáři src v otevřeném
     * projektu
     *
     * @param pathToProjectDir
     *         - cesta k projektu, který se má otevřít
     */
    public final void openProjectInApp(final String pathToProjectDir) {
        if (pathToProjectDir == null)
            return;

        final File fileProjectDir = new File(pathToProjectDir);

        if (!fileProjectDir.exists()) {
            JOptionPane.showMessageDialog(this, missingDirErrText + fileProjectDir.getPath(), missingDirErrTitle,
                    JOptionPane.ERROR_MESSAGE);

            /*
             * Zde se našel nějaký adresář / projekt, který již neexistuje, uživatel jej musel smazat, přejmenovat apod.
             *
             * Tak musím přefiltrovat seznam dostupných dříve otevřených projektů, které ještě existují:
             */
            createRecentlyOpenedProjectsItems();

            return;
        }


        // Otestuji adresář src, případně ho vytvořím:
        final File fileSrcDir = new File(pathToProjectDir + File.separator + "src");

        if (!fileSrcDir.exists() || !fileSrcDir.isDirectory()) {
            // Zde adresář ve složce zvoleného projektu buď neexistuje, nebo není adresář, tak jej vytvořím:
            try {

                Files.createDirectories(Paths.get(fileSrcDir.getPath()));

            } catch (IOException e1) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při vytváření adresáře 'src' ve složce zvolené pro otevření " +
                                    "projektu, třída menu.java, metoda - po kliknutí na OpenProject !\nCesta: "
                                    + fileSrcDir.getPath());

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
            }
        }


        // Zde již adresář src existuje (pokud neexistoval)
        // Mohu se pokusit načíst kolekci do ClassDiagramu:
        final File fileClassDiagramProperties = new File(pathToProjectDir + File.separator + Constants
                .NAME_OF_CLASS_DIAGRAM_FILE);

        if (fileClassDiagramProperties.exists() && fileClassDiagramProperties.isFile()) {
            final List<DefaultGraphCell> cellsList = App.READ_FILE
                    .openClassDiagramProperties(fileClassDiagramProperties.getPath());

            classDiagram.setCellsList(cellsList);
        }


        // Uložím do preferencí otevřený projekt:
        PreferencesApi.setPreferencesValue(PreferencesApi.OPEN_PROJECT, pathToProjectDir);


        /*
         * Zde je potřeba zkontrolovat, připadně přidat otevřený projekt do preferencí do nedávno otevřených projektů.
         *
         * Aby se v položce pro nedávno otevřené projekty v menu zobrazovaly na výběr dříve otevřené projekty, tak
         * pro tento aktuálně
         * otevíraný projekt otestuji, zda byl již otevřen, pokud ano, aktualizuje se datum otevření, pokud nebyl
         * otevřen, bude přidán do preferencí
         * pro pozdější otevření.
         */
        OpenedProjectPreferencesHelper.checkOpenedProject(fileProjectDir);

        /*
         *  Výše jsem se mohl uložit nový projekt do preferencí, něbo nějaký smazat, nebo se změnilo otevření jiného
         *  projektu, tak ten předchozí mohu zobrazit apod. Proto potřebuji znovu přenačist položky v rozevíracím
         *  menu pro dříve otevřené projekty:
         */
        createRecentlyOpenedProjectsItems();

        /*
         * Zde je třeba zrušit označená tlačítka v toolbaru a nastavit proměnné, které slouží pro ukládání hodnot pro
         * vytvoření vztahů na null.
         */
        GraphClass.deselectChoosedEdge();

        // Zde proběhlo otevření projektu v pohodě, projekt je otevřen, tak zpřístupnit veškerá tlačítka / komponenty:
        App.enableButtons(true);
        // Nastavit oknu aplikace text na cestu k otevřenému projektu:
        app.setTitle(Constants.APP_TITLE + " - " + pathToProjectDir);


        // Prohledat adresář src a zjistit, zda se v diagramu tříd nachází veškeré třídy co v tom adresáři, případně
        // je přidat:
        ThreadSwingWorker.runMyThread(new RetrieveNotLoadedClassesThread(fileSrcDir, classDiagram));


        // kontrola vztahů mezi třídami v diagramu tříd:
        ThreadSwingWorker.runMyThread(new CheckRelationShipsBetweenClassesThread(classDiagram, outputEditor,
                classDiagram.isShowRelationShipsToItself(), classDiagram.isShowAssociationThroughAggregation()));
    }

	
	
	

	
	
	
	

	
	
	
	
	/**
	 * Metoda, která zavolá hlavní - spouštění metodu načtenou z nějaké třídy
	 * 
	 * @param main
	 *            - hlavní - spouštěcí metoda main, která se má zavolat
	 * 
	 * @param className
	 *            - název třídy, ve které se tato metoda main nachází
	 */
	private void callTheMainMethod(final Method main, final String className) {
		final String[] parameters = null;
		
		try {
			// Pokusím ze zavolat metodu main, buď nastane vyjímka, nebo se zavolá:
			main.invoke(null,  (Object) parameters);
			
		} catch (IllegalAccessException e1) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při pokusu o zavolání metody 'main' v třídě: " + className
                        + ", třída v aplikaci: Menu.java, metoda - po kliknutí na tlačítko v menu spustit. Výjimka " +
                        "nastala v metodě: "
                        + "callTheMainMethod. Tato výjimka mohla nastat například v případě, že metoda 'main' není " +
                        "přístupná.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			
			// Přepsáno na zarovnání:
			outputEditor.addResult(errorWhileCallTheMainMethod + OutputEditor.NEW_LINE_TWO_GAPS + txtClass + ": "
					+ className + OutputEditor.NEW_LINE_TWO_GAPS + txtError + ": " + methodIsNotaccessible);

        } catch (IllegalArgumentException e1) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při pokusu o zavolání metody 'main' v označené třídě: "
                        + className
                        + ", třída v aplikaci: Menu.java, metoda - po kliknutí na tlačítko v menu spustit. Tato " +
                        "výjimka může nastat například v případě - je-li metodou metoda "
                        + "instance a zadaný objektový argument není instancí třídy nebo rozhraní, které deklaruje " +
                        "podkladovou metodu (nebo její podtřídy nebo implementátor); pokud se počet "
                        + "skutečných a formálních parametrů liší; pokud selhání konverze pro primitivní argumenty " +
                        "selže; nebo pokud po případném rozbalení nelze hodnotu parametru převést na "
                        + "odpovídající typ formálního parametru pomocí konverze vyvolání metody apod.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			// Přepsáno na zarovnání:
			outputEditor.addResult(errorWhileCallTheMainMethod + OutputEditor.NEW_LINE_TWO_GAPS + txtClass + ": "
					+ className + OutputEditor.NEW_LINE_TWO_GAPS + txtError + ": "
					+ txtIllegalArgumentExceptionPossibleErrors);

        } catch (InvocationTargetException e1) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při pokusu o zavolání metody 'main' v označené třídě: "
                        + className
                        + ", třída v aplikaci: Menu.java, metoda - po kliknutí na tlačítko v menu spustit. Tato " +
                        "výjimka může nastat například v případě, že podkladová metoda vyhodí výjimku.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			// Přepsáno na zarovnání:
			outputEditor.addResult(errorWhileCallTheMainMethod + OutputEditor.NEW_LINE_TWO_GAPS + txtClass + ": "
					+ className + OutputEditor.NEW_LINE_TWO_GAPS + txtError + ": " + errorWhileCallingMainMethod);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda je nějaký projekt otevřen v aplikaci, a pokud ano,
	 * tak se otestuje, zda ještě existuje, pak se cesta k tomuto projektu necha v příslušné hodnotě
	 * a v podstatě se nic neděje, ale pokud ten otevřený projekt již neexistuje, tak se nastaví na zavření
	 * - hodnota Closed v constatns a pokud již je zavřen, tak není co řešit
	 */
	public static void setOpenProjectInPreferences() {
		// Postup:
		// Vzít si hodnotu z preferenci pro otevřený projekt,
		// otstovat, zda neni null, pokud ano, tak ji nastavit na Closed
		// Pak otestovat, zda to není closed, a pokud ne, jde se dále, pokud ano, není co řešit - projekt je již zavřen
		// Pokud se odstanu až sem, tak mám cestu k nějakém otevřenému projektu, tak otestuji, zda ještě existuje,
		// a pokud ano, tak nic nedělám, protože je v tom uložená hodnota k otevřenému projektu a tu tam nechám,
		// aby šla otevřit,
		// pokud ale ten projekt - adresář už neexistuje, tak nastavím tuto hodnotu na Closed
		
		final String openProject = PreferencesApi.getPreferencesValue(PreferencesApi.OPEN_PROJECT, null);
		
		if (openProject != null) {
			if (!openProject.equals(Constants.CLOSED_PROJECT)) {
				// Zde v této podmínce už vím, že mám nějakou cestu k projetku, tak jen zjistím, zda existuje,
				// a je to adresář, pokud ne, tak nastavím cestu na zavřeno, protože daný projekt již neexistuje, nebo
				// byl minimálně přejmenován, nebo se nejedná o adresář, apod.:
				final File fileProject = new File(openProject);
				
				if (!fileProject.exists() || !fileProject.isDirectory())
					// Zde projekt již neexistuje, nebo o není adresář (už), tak nastavím projekt,
					// jako na zavřený, aby ho nešlo znovu otevřít - už neexistuje...
					App.WRITE_TO_FILE.setCloseProject(Constants.CLOSED_PROJECT);
			}
			// Projekt je již uzavřen
		}
		// Zde se nepodařilo ani načíst cestu k projektu, tak ji nastavím na Closed:
		else
			App.WRITE_TO_FILE.setCloseProject(Constants.CLOSED_PROJECT);
	}
	
	
	
	
	
	
	
	

	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		final String compilesPathExample = ": workspace/configuration/libTools/lib";
		
		languageProperties = properties;
		
		if (properties != null) {
			menuProject.setText(properties.getProperty("MnTxtProject", Constants.TXT_PROJECT));
			menuEdit.setText(properties.getProperty("MnTxtEdit", Constants.TXT_EDIT));
			menuTools.setText(properties.getProperty("MnTxtTools", Constants.TXT_TOOLS));
			menuHelp.setText(properties.getProperty("MnTxtHelp", Constants.TXT_HELP));
			
			itemNewProject.setText(properties.getProperty("MnTxtNewProject", Constants.TXT_NEW_PROJECT));
			itemOpenProject.setText(properties.getProperty("MnTxtOpenProject", Constants.TXT_OPEN_PROJECT));
            menuRecentlyOpenedProjects.setText(properties.getProperty("MnTxtRecentlyOpenedProjects", Constants.TXT_RECENTLY_OPENED_PROJECTS));
            itemDeleteRecentlyProjects.setText(properties.getProperty("MnTxtDeleteRecentlyOpenedProjects", Constants.TXT_DELETE_RECENTLY_OPENED_PROJECTS));
            rbItemSortRecentlyProjectsAsc.setText(properties.getProperty("MnTxtRb_RecentlyOpenedProjectsAsc", Constants.TXT_RB_SORT_RECENTLY_OPENED_PROJECTS_ASC));
            rbItemSortRecentlyProjectsDes.setText(properties.getProperty("MnTxtRb_RecentlyOpenedProjectsDes", Constants.TXT_RB_SORT_RECENTLY_OPENED_PROJECTS_DES));
			itemCloseProject.setText(properties.getProperty("MnTxtCloseProject", Constants.TXT_CLOSE_PROJECT));
			itemSave.setText(properties.getProperty("MnTxtSave", Constants.TXT_SAVE));
			itemSaveAs.setText(properties.getProperty("MnTxtSaveAs", Constants.TXT_SAVE_AS));
			itemSwitchWorkspace.setText(properties.getProperty("MnTxtSwitchWorkspace", Constants.TXT_SWITCH_WORKSPACE));			
			itemRestart.setText(properties.getProperty("MnTxtRestartApp", Constants.TXT_RESTART_APP));
			itemExit.setText(properties.getProperty("MnTxtExit", Constants.TXT_EXIT));
			itemAddClassFromFile.setText(properties.getProperty("MnTxtAddClassFromFile", Constants.TXT_ADD_CLASS_FROM_FILE));
			itemRemove.setText(properties.getProperty("MnTxtRemove", Constants.TXT_REMOVE));
			itemSaveImageOfClassDiagram.setText(properties.getProperty("MnTxtSaveImageOfClassDiagram", Constants.TXT_SAVE_IMAGE_OF_CLASS_DIAGRAM));
			itemSaveImageOfInstanceDiagram.setText(properties.getProperty("MnTxtSaveImageOfInstanceDiagram", Constants.TXT_SAVE_IMAGE_OF_INSTANCE_DIAGRAM));
			itemPrintClassDiagram.setText(properties.getProperty("MnTxtPrintImageOfClassDiagram", Constants.TXT_PRINT_IMAGE_OF_CLASS_DIAGRAM));
			itemPrintInstanceDiagram.setText(properties.getProperty("MnTxtPrintImageOfInstanceDiagram", Constants.TXT_PRINT_IMAGE_OF_INSTANCE_DIAGRAM));
			itemRefreshClassDiagram.setText(properties.getProperty("MnTxtRefreshClassDiagram", Constants.TXT_REFRESH_CLASS_DIAGRAM));
			itemRefreshInstanceDiagram.setText(properties.getProperty("MnTxtRefreshInstanceDiagram", Constants.TXT_REFRESH_INSTANCE_DIAGRAM));
			itemRemoveAll.setText(properties.getProperty("MnTxtRemoveAll", Constants.TXT_REMOVE_ALL));
			itemRemoveInstanceDiagram.setText(properties.getProperty("MnTxtRemoveInstanceDiagram", Constants.TXT_REMOVE_INSTANCE_DIAGRAM));							
			menuOpenInEditor.setText(properties.getProperty("MnTxtOpenInEditor", Constants.TXT_OPEN_IN_EDITOR));
			itemOpenFileInProjectExplorer.setText(properties.getProperty("MnTxtOpenClassInProjectExplorer", Constants.TXT_OPEN_CLASS_IN_PROJECT_EXPLORER));			
			itemOpenFileInCodeEditor.setText(properties.getProperty("MnTxtOpenClass", Constants.TXT_OPEN_CLASS));
			itemShowOutputFrame.setText(properties.getProperty("MnTxtShowOutputTerminal", Constants.TXT_SHOW_OUTPUT_TERMINAL));
			itemCompile.setText(properties.getProperty("MnTxtCompile", Constants.TXT_COMPILE));
			itemStart.setText(properties.getProperty("MnTxtStart_CallTheMainMethod", Constants.TXT_START_CALL_THE_MAIN_METHOD));
			itemSettings.setText(properties.getProperty("MnTxtSettings", Constants.TXT_SETTINGS));
			itemFontSize.setText(properties.getProperty("MnTxtFontSize", Constants.TXT_FONT_SIZE));
			menuItemJava.setText(properties.getProperty("MnTxtJava", Constants.TXT_JAVA));
			itemJavaSettings.setText(properties.getProperty("MnTxtJavaSettings", Constants.TXT_JAVA_SETTINGS));
			itemJavaSearch.setText(properties.getProperty("MnTxtJavaSearch", Constants.TXT_JAVA_SEARCH));
			menuProjectExplorer.setText(properties.getProperty("MnTxtProjectExplorer", Constants.TXT_PROJECT_EXPLORER));
            menuOpenInFileExplorer.setText(properties.getProperty("MnTxtFileExplorer", Constants.TXT_OPEN_IN_FILE_EXPLORER));
			itemPeOpenProject.setText(properties.getProperty("MnTxtProjectOpenProject", Constants.TXT_PROJECT_EXPLORER_PROJECT));
			itemPeWorkspace.setText(properties.getProperty("MnTxtProjectWorkspace", Constants.TXT_PROJECT_EXPLORER_WORKSPACE));
            itemFeWorkspace.setText(properties.getProperty("MnTxtFileExplorerWorkspace", Constants.TXT_FILE_EXPLORER_WORKSPACE));
            itemFeOpenProject.setText(properties.getProperty("MnTxtFileExplorerOpenProject", Constants.TXT_FILE_EXPLORER_PROJECT));
			itemInfo.setText(properties.getProperty("MnTxtInfo", Constants.TXT_INFO));
			itemAboutApp.setText(properties.getProperty("MnTxtAboutApp", Constants.TXT_ABOUT_APP));
			
			// Popisky - po najětí myší:
			itemNewProject.setToolTipText(properties.getProperty("MnTtNewProject", Constants.TT_NEW_PROJECT));
			itemOpenProject.setToolTipText(properties.getProperty("MnTtOpenProject", Constants.TT_OPEN_PROJECT));
            itemDeleteRecentlyProjects.setToolTipText(properties.getProperty("MnTtDeleteRecentlyOpenedProjects", Constants.TT_DELETE_RECENTLY_OPENED_PROJECTS));
            rbItemSortRecentlyProjectsAsc.setToolTipText(properties.getProperty("MnTtRb_SortRecentlyOpenedProjectsAsc", Constants.TT_RB_SORT_RECENTLY_OPENED_PROJECTS_ASC));
            rbItemSortRecentlyProjectsDes.setToolTipText(properties.getProperty("MnTtRb_SortRecentlyOpenedProjectsDes", Constants.TT_RB_SORT_RECENTLY_OPENED_PROJECTS_DES));
			itemCloseProject.setToolTipText(properties.getProperty("MnTtCloseProject", Constants.TT_CLOSE_PROJECT));
			itemSave.setToolTipText(properties.getProperty("MnTtSave", Constants.TT_SAVE));
			itemSaveAs.setToolTipText(properties.getProperty("MnTtSaveAs", Constants.TT_SAVE_AS));
			itemSwitchWorkspace.setToolTipText(properties.getProperty("MnTtSwitchWorkspace", Constants.TT_SWITCH_WORKSPACE));
			itemRestart.setToolTipText(properties.getProperty("MnTtRestartApp", Constants.TT_RESTART_APP));
			itemExit.setToolTipText(properties.getProperty("MnTtExit", Constants.TT_EXIT));
			itemAddClassFromFile.setToolTipText(properties.getProperty("MnTtAddClassFromFile", Constants.TT_ADD_CLASS_FROM_FILE));
			itemRemove.setToolTipText(properties.getProperty("MnTtRemove", Constants.TT_REMOVE));
			itemSaveImageOfClassDiagram.setToolTipText(properties.getProperty("MnTtSaveImageOfClassDiagram", Constants.TT_SAVE_IMAGE_OF_CLASS_DIAGRAM));
			itemSaveImageOfInstanceDiagram.setToolTipText(properties.getProperty("MnTtSaveImageOfInstanceDiagram", Constants.TT_SAVE_IMAGE_OF_INSTANCE_DIAGRAM));			
			itemPrintClassDiagram.setToolTipText(properties.getProperty("MnTtPrintImageOfClassDiagram", Constants.TT_PRINT_IMAGE_OF_CLASS_DIAGRAM));
			itemPrintInstanceDiagram.setToolTipText(properties.getProperty("MnTtPrintImageOfInstanceDiagram", Constants.TT_PRINT_IMAGE_OF_INSTANCE_DIAGRAM));
			itemRefreshClassDiagram.setToolTipText(properties.getProperty("MnTtRefreshClassDiagram", Constants.TT_REFRESH_CLASS_DIAGRAM));
			itemRefreshInstanceDiagram.setToolTipText(properties.getProperty("MnTtRefreshInstanceDiagram", Constants.TT_REFRESH_INSTANCE_DIAGRAM));
			itemRemoveAll.setToolTipText(properties.getProperty("MnTtRemoveAll", Constants.TT_REMOVE_ALL));			
			itemRemoveInstanceDiagram.setToolTipText(properties.getProperty("MnTtRemoveInstanceDiagram", Constants.TT_REMOVE_INSTANCE_DIAGRAM));
			itemOpenFileInProjectExplorer.setToolTipText(properties.getProperty("MnTtOpenFileInProjectExplorer", Constants.TT_OPEN_FILE_IN_PROJECT_EXPLORER));			
			itemOpenFileInCodeEditor.setToolTipText(properties.getProperty("MnTtOpenClass", Constants.TT_OPEN_CLASS));
			itemShowOutputFrame.setToolTipText(properties.getProperty("MnTtShowOutputTerminal", Constants.TT_SHOW_OUTPUT_TERMINAL));
			itemCompile.setToolTipText(properties.getProperty("MnTtCompile", Constants.TT_COMPILE));
			itemStart.setToolTipText(properties.getProperty("MnTtStart_CallTheMainMethod", Constants.TT_START_CALL_THE_MAIN_METHOD));
			itemSettings.setToolTipText(properties.getProperty("MnTtSettings", Constants.TT_SETTINGS));
			itemFontSize.setToolTipText(properties.getProperty("MnTtFontSize", Constants.TT_FONT_SIZE));
			itemJavaSettings.setToolTipText(properties.getProperty("MnTtJava", Constants.TT_JAVA));
			itemJavaSearch.setToolTipText(properties.getProperty("MnTtJavaSearch", Constants.TT_ITEM_JAVA_SEARCH) + compilesPathExample);			
			itemPeOpenProject.setToolTipText(properties.getProperty("MnTtProjectExplorerProject", Constants.TT_PROJECT_EXPLORER_PROJECT));
			itemPeWorkspace.setToolTipText(properties.getProperty("MnTtProjectExplorerWorkspace", Constants.TT_PROJECT_EXPLORER_WORKSPACE));
            itemFeWorkspace.setToolTipText(properties.getProperty("MnTtFileExplorerWorkspace", Constants.TT_FILE_EXPLORER_WORKSPACE));
            itemFeOpenProject.setToolTipText(properties.getProperty("MnTtFileExplorerOpenProject", Constants.TT_FILE_EXPLORER_OPEN_PROJECT));
			itemInfo.setToolTipText(properties.getProperty("MnTtInfo", Constants.TT_INFO));
			itemAboutApp.setToolTipText(properties.getProperty("MnTtAboutApp", Constants.TT_ABOUT_APP));
			
			
			
			// Chybové hlášky po kliknutí na tlačítko v menu:
			missingDirErrTitle = properties.getProperty("MnJopMissingDirectoryErrorTitle", Constants.MN_JOP_MISSING_DIRECTORY_ERROR_TITLE);
			missingDirErrText = properties.getProperty("MnJopMissingDirectoryErrorText", Constants.MN_JOP_MISSING_DIRECTORY_ERROR_TEXT) + ": ";
			identicalDirErrTitle = properties.getProperty("MnJopIdenticalDirectoryErrorTitle", Constants.MN_JOP_IDENTICAL_DIR_ERROR_TITLE);
			identicalDirErrText = properties.getProperty("MnJopIdenticalDirectoryErrorText", Constants.MN_JOP_IDENTICAL_DIR_ERROR_TEXT);
			missingClassFileTitle = properties.getProperty("MnJopMissingClassFileTitle", Constants.MN_JOP_MISSING_CLASS_FILE_TITLE);
			missingClassFileText = properties.getProperty("MnJopMissingClassFileText", Constants.MN_JOP_MISSING_CLASS_FILE_TEXT) + ": ";
			
			// texty pro kompilaci, nalezeni a zavolani metody main:
			missingSrcDirText = properties.getProperty("MnTxtMissingSrcDirText", Constants.MN_TXT_MISSING_SRC_DIR_TEXT);
			missingSrcDirTitle = properties.getProperty("MnTxtMissingSrcDirTitle", Constants.MN_TXT_MISSING_SRC_DIR_TITLE);
			errorWhileCallTheMainMethod = properties.getProperty("MnTxtErrorWhileCallTheMainMethod", Constants.MN_TXT_ERROR_WHILE_CALL_THE_MAIN_METHOD);
			txtClass = properties.getProperty("MnTxtClass", Constants.MN_TXT_CLASS);
			txtError = properties.getProperty("MnTxtError", Constants.MN_TXT_ERROR);
			methodIsNotaccessible = properties.getProperty("MnTxtMethodIsNotAccessible", Constants.MN_TXT_METHOD_IS_NOT_ACCESSIBLE);
			txtIllegalArgumentExceptionPossibleErrors = properties.getProperty("MnTxtIllegalArgumentExceptionPossibleErrors", Constants.MN_TXT_ILLEGAL_ARGUMENT_EXCEPTION_POSSIBLE_ERRORS);
			errorWhileCallingMainMethod = properties.getProperty("MnTxtErrorWhileCallingMainMethod", Constants.MN_TXT_ERROR_WHILE_CALLING_MAIN_METHOD);
			
			txtMainMethodNotFoundText = properties.getProperty("MnTxtMainMethodNotFoundText", Constants.MN_TXT_MAIN_METHOD_NOT_FOUND_TEXT);
			txtMainMethodNotFoundTitle = properties.getProperty("MnTxtMainMethodNotFoundTitle", Constants.MN_TXT_MAIN_METHOD_NOT_FOUND_TITLE);
			
			txtChooseClassOrTxtDialogTitle = properties.getProperty("MnTxtChooseClassOrTextDocumentDialogTitle", Constants.MN_TXT_CHOOSE_CLASS_OR_TEXT_DOC_DIALOG_TITLE);						
			
			
			txtErrorWhilePrintingsClassDiagramText = properties.getProperty("Mn_TxtErrorWhilePrintingClassDiagramText", Constants.MN_TXT_ERROR_WHILE_PRINTING_CLASS_DIAGRAM_TEXT);
			txtErrorWhilePrintingsClassDiagramTitle = properties.getProperty("Mn_TxtErrorWhilePrintingClassDiagramTitle", Constants.MN_TXT_ERROR_WHILE_PRINTING_CLASS_DIAGRAM_TITLE);
			txtFailedToPrintClassDiagramText = properties.getProperty("Mn_TxtFailedToPrintClassDiagramText", Constants.MN_TXT_FAILED_TO_PRINT_CLASS_DIAGRAM_TEXT);
			txtClassDiagram = properties.getProperty("Mn_TxtClassDiagramText", Constants.MN_TXT_CLASS_DIAGRAM);
			txtFailedToPrintClassDiagramTitle = properties.getProperty("Mn_TxtFailedToPrintClassDiagramTitle", Constants.MN_TXT_FAILED_TO_PRINT_CLASS_DIAGRAM_TITLE);
			txtErrorWhilePrintingInstanceDiagramText = properties.getProperty("Mn_TxtErrorWhilePrintingInstanceDiagramText", Constants.MN_TXT_ERROR_WHILE_PRINTING_INSTANCE_DIAGRAM_TEXT);
			txtErrorWhilePrintingInstanceDiagramTitle = properties.getProperty("Mn_TxtErrorWhilePrintingInstanceDiagramTitle", Constants.MN_TXT_ERROR_WHILE_PRINTING_INSTANCE_DIAGRAM_TITLE);
			txtFailedToPrintInstanceDiagramText = properties.getProperty("Mn_TxtFailedToPrintInstanceDiagramText", Constants.MN_TXT_FAILED_TO_PRINT_INSTANCE_DIAGRAM_TEXT);
			txtInstanceDiagram = properties.getProperty("Mn_TxtInstanceDiagramText", Constants.MN_TXT_INSTANCE_DIAGRAM);
			txtFailedToPrintInstanceDiagramTitle = properties.getProperty("Mn_TxtfailedToPrintInstanceDiagramTitle", Constants.MN_TXT_FAILED_TO_PRINT_INSTANCE_DIAGRAM_TITLE);
			txtErrorWhileCopyFilesText = properties.getProperty("Mn_TxtErrorWhileCopyFilesText", Constants.MN_TXT_ERROR_WHILE_COPY_FILES_TEXT);
			txtPossibleFileIsBeingUsed = properties.getProperty("Mn_TxtPossibleFileIsBeingUsed", Constants.MN_TXT_POSSIBLE_FILE_IS_BEING_USED);
			txtSource = properties.getProperty("Mn_TxtSourceText", Constants.MN_TXT_SOURCE_TEXT);
			txtDestination = properties.getProperty("Mn_TxtDestinationText", Constants.MN_TXT_DESTINATION_TEXT);
			txtErrorWhileCopyFilesTitle = properties.getProperty("Mn_TxtErrorWhileCopyFilesTitle", Constants.MN_TXT_ERROR_WHILE_COPY_FILES_TITLE);
			
			// Pro ProjectExplorer 
			txtPathToWorkspaceDoNotFoundText = properties.getProperty("Mn_TxtPathToWorkspaceDoNotFoundText", Constants.MN_TXT_PATH_TO_WORKSPACE_DO_NOT_FOUND_TEXT);
			txtPathToWorkspaceDoNotFoundTitle = properties.getProperty("Mn_TxtPathToWorkspaceDoNotFoundTitle", Constants.MN_TXT_PATH_TO_WORKSPACE_DO_NOT_FOUND_TITLE);
			
			txtOpenDirDoesNotExistText = properties.getProperty("Mn_Txt_OpenDirDoesNotExistText", Constants.MN_TXT_OPEN_DIR_DOES_NOT_EXIST_TEXT);
			txtOriginalLocation = properties.getProperty("Mn_Txt_OriginalLocation", Constants.MN_TXT_ORIGINAL_LOCATION);
			txtOpenDirDoesNotExistTitle = properties.getProperty("Mn_Txt_OpenDirDoesNotExistTitle", Constants.MN_TXT_OPEN_DIR_DOES_NOT_EXIST_TITLE);
			txtProjectIsNotOpenText = properties.getProperty("Mn_Txt_ProjectIsNotOpenText", Constants.MN_TXT_PROJECT_IS_NOT_OPEN_TEXT);
			txtProjectIsNotOpenTitle = properties.getProperty("Mn_Txt_ProjectIsNotOpenTitle", Constants.MN_TXT_PROJECT_IS_NOT_OPEN_TITLE);
			txtClassDiagramDoesntContainClassText = properties.getProperty("Mn_Txt_ClassDiagramDoesntContainClassText", Constants.MN_TXT_CLASS_DIAGRAM_DOESNT_CONTAIN_CLASS_TEXT);
			txtClassDiagramDoesntContainClassTitle = properties.getProperty("Mn_Txt_ClassDiagramDoesntContainClassTitle", Constants.MN_TXT_CLASS_DIAGRAM_DOESNT_CONTAIN_CLASS_TITLE);

			// Dotaz na restart aplikace při změně workspace:
			txtSwitchWorkspaceJopRestartAppNowText = properties.getProperty("Mn_Txt_SwitchWorkspaceJopRestartAppNow_Text", Constants.MN_TXT_SWITCH_WORKSPACE_JOP_RESTART_APP_NOW_TEXT);
			txtSwitchWorkspaceJopRestartAppNowTT = properties.getProperty("Mn_Txt_SwitchWorkspaceJopRestartAppNow_TT", Constants.MN_TXT_SWITCH_WORKSPACE_JOP_RESTART_APP_NOW_TT);

			// Chyba při získávání workspace a adresáře otevřeného projektu pro otevření v průzkumníku souboru:
            txtPathToWorkspaceWasNotFoundText = properties.getProperty("Mn_Txt_PathToWorkspaceWasNotFound_Text", Constants.MN_TXT_PATH_TO_WORKSPACE_WAS_NOT_FOUND_TEXT);
            txtPathToWorkspaceWasNotFoundTitle = properties.getProperty("Mn_Txt_PathToWorkspaceWasNotFound_Title", Constants.MN_TXT_PATH_TO_WORKSPACE_WAS_NOT_FOUND_TITLE);
            txtPathToOpenProjectWasNotFoundText = properties.getProperty("Mn_Txt_PathToOpenProjectWasNotFound_Text", Constants.MN_TXT_PATH_TO_OPEN_PROJECT_WAS_NOT_FOUND_TEXT);
            txtPathToOpenProjectWasNotFoundTitle = properties.getProperty("Mn_Txt_PathToOpenProjectWasNotFound_Title", Constants.MN_TXT_PATH_TO_OPEN_PROJECT_WAS_NOT_FOUND_TITLE);
		}
		
		
		else {
			// Zde nebyl načten soubor, tak použiji výchozí texty z konstant:
			menuProject.setText(Constants.TXT_PROJECT);
			menuEdit.setText(Constants.TXT_EDIT);
			menuTools.setText(Constants.TXT_TOOLS);
			menuHelp.setText(Constants.TXT_HELP);
			
			itemNewProject.setText(Constants.TXT_NEW_PROJECT);
			itemOpenProject.setText(Constants.TXT_OPEN_PROJECT);
            menuRecentlyOpenedProjects.setText(Constants.TXT_RECENTLY_OPENED_PROJECTS);
            itemDeleteRecentlyProjects.setText(Constants.TXT_DELETE_RECENTLY_OPENED_PROJECTS);
            rbItemSortRecentlyProjectsAsc.setText(Constants.TXT_RB_SORT_RECENTLY_OPENED_PROJECTS_ASC);
            rbItemSortRecentlyProjectsDes.setText(Constants.TXT_RB_SORT_RECENTLY_OPENED_PROJECTS_DES);
			itemCloseProject.setText(Constants.TXT_CLOSE_PROJECT);
			itemSave.setText(Constants.TXT_SAVE);
			itemSaveAs.setText(Constants.TXT_SAVE_AS);
			itemSwitchWorkspace.setText(Constants.TXT_SWITCH_WORKSPACE);
			itemRestart.setText(Constants.TXT_RESTART_APP);
			itemExit.setText(Constants.TXT_EXIT);
			itemAddClassFromFile.setText(Constants.TXT_ADD_CLASS_FROM_FILE);
			itemRemove.setText(Constants.TXT_REMOVE);
			itemSaveImageOfClassDiagram.setText(Constants.TXT_SAVE_IMAGE_OF_CLASS_DIAGRAM);
			itemSaveImageOfInstanceDiagram.setText(Constants.TXT_SAVE_IMAGE_OF_INSTANCE_DIAGRAM);
			itemPrintClassDiagram.setText(Constants.TXT_PRINT_IMAGE_OF_CLASS_DIAGRAM);
			itemPrintInstanceDiagram.setText(Constants.TXT_PRINT_IMAGE_OF_INSTANCE_DIAGRAM);
			itemRefreshClassDiagram.setText(Constants.TXT_REFRESH_CLASS_DIAGRAM);
			itemRefreshInstanceDiagram.setText(Constants.TXT_REFRESH_INSTANCE_DIAGRAM);
			itemRemoveAll.setText(Constants.TXT_REMOVE_ALL);
			itemRemoveInstanceDiagram.setText(Constants.TXT_REMOVE_INSTANCE_DIAGRAM);
			menuOpenInEditor.setText(Constants.TXT_OPEN_IN_EDITOR);
			itemOpenFileInProjectExplorer.setText(Constants.TXT_OPEN_CLASS_IN_PROJECT_EXPLORER);
			itemOpenFileInCodeEditor.setText(Constants.TXT_OPEN_CLASS);
			itemShowOutputFrame.setText(Constants.TXT_SHOW_OUTPUT_TERMINAL);
			itemCompile.setText(Constants.TXT_COMPILE);
			itemStart.setText(Constants.TXT_START_CALL_THE_MAIN_METHOD);
			itemSettings.setText(Constants.TXT_SETTINGS);
			itemFontSize.setText(Constants.TXT_FONT_SIZE);
			menuItemJava.setText(Constants.TXT_JAVA);
			itemJavaSettings.setText(Constants.TXT_JAVA_SETTINGS);
			itemJavaSearch.setText(Constants.TXT_JAVA_SEARCH);
			menuProjectExplorer.setText(Constants.TXT_PROJECT_EXPLORER);
            menuOpenInFileExplorer.setText(Constants.TXT_OPEN_IN_FILE_EXPLORER);
			itemPeOpenProject.setText(Constants.TXT_PROJECT_EXPLORER_PROJECT);
			itemPeWorkspace.setText(Constants.TXT_PROJECT_EXPLORER_WORKSPACE);
            itemFeWorkspace.setText(Constants.TXT_FILE_EXPLORER_WORKSPACE);
            itemFeOpenProject.setText(Constants.TXT_FILE_EXPLORER_PROJECT);
			itemInfo.setText(Constants.TXT_INFO);
			itemAboutApp.setText(Constants.TXT_ABOUT_APP);
			
			// Popisky - po najětí myší:
			itemNewProject.setToolTipText(Constants.TT_NEW_PROJECT);
			itemOpenProject.setToolTipText(Constants.TT_OPEN_PROJECT);
            itemDeleteRecentlyProjects.setToolTipText(Constants.TT_DELETE_RECENTLY_OPENED_PROJECTS);
            rbItemSortRecentlyProjectsAsc.setToolTipText(Constants.TT_RB_SORT_RECENTLY_OPENED_PROJECTS_ASC);
            rbItemSortRecentlyProjectsDes.setToolTipText(Constants.TT_RB_SORT_RECENTLY_OPENED_PROJECTS_DES);
			itemCloseProject.setToolTipText(Constants.TT_CLOSE_PROJECT);
			itemSave.setToolTipText(Constants.TT_SAVE);
			itemSaveAs.setToolTipText(Constants.TT_SAVE_AS);
			itemRestart.setToolTipText(Constants.TT_RESTART_APP);
			itemExit.setToolTipText(Constants.TT_EXIT);
			itemAddClassFromFile.setToolTipText(Constants.TT_ADD_CLASS_FROM_FILE);
			itemRemove.setToolTipText(Constants.TT_REMOVE);
			itemSaveImageOfClassDiagram.setToolTipText(Constants.TT_SAVE_IMAGE_OF_CLASS_DIAGRAM);
			itemSaveImageOfInstanceDiagram.setToolTipText(Constants.TT_SAVE_IMAGE_OF_INSTANCE_DIAGRAM);
			itemPrintClassDiagram.setToolTipText(Constants.TT_PRINT_IMAGE_OF_CLASS_DIAGRAM);
			itemPrintInstanceDiagram.setToolTipText(Constants.TT_PRINT_IMAGE_OF_INSTANCE_DIAGRAM);
			itemRefreshClassDiagram.setToolTipText(Constants.TT_REFRESH_CLASS_DIAGRAM);
			itemRefreshInstanceDiagram.setToolTipText(Constants.TT_REFRESH_INSTANCE_DIAGRAM);
			itemRemoveAll.setToolTipText(Constants.TT_REMOVE_ALL);
			itemRemoveInstanceDiagram.setToolTipText(Constants.TT_REMOVE_INSTANCE_DIAGRAM);
			itemOpenFileInProjectExplorer.setToolTipText(Constants.TT_OPEN_FILE_IN_PROJECT_EXPLORER);
			itemOpenFileInCodeEditor.setToolTipText(Constants.TT_OPEN_CLASS);
			itemShowOutputFrame.setToolTipText(Constants.TT_SHOW_OUTPUT_TERMINAL);
			itemCompile.setToolTipText(Constants.TT_COMPILE);
			itemStart.setToolTipText(Constants.TT_START_CALL_THE_MAIN_METHOD);
			itemSettings.setToolTipText(Constants.TT_SETTINGS);
			itemFontSize.setToolTipText(Constants.TT_FONT_SIZE);
			itemJavaSettings.setToolTipText(Constants.TT_JAVA);
			itemJavaSearch.setToolTipText(Constants.TT_ITEM_JAVA_SEARCH + compilesPathExample);
			itemPeOpenProject.setToolTipText(Constants.TT_PROJECT_EXPLORER_PROJECT);
			itemPeWorkspace.setToolTipText(Constants.TT_PROJECT_EXPLORER_WORKSPACE);
            itemFeWorkspace.setToolTipText(Constants.TT_FILE_EXPLORER_WORKSPACE);
            itemFeOpenProject.setToolTipText(Constants.TT_FILE_EXPLORER_OPEN_PROJECT);
			itemInfo.setToolTipText(Constants.TT_INFO);
			itemAboutApp.setToolTipText(Constants.TT_ABOUT_APP);
			
			
			
			// Hlášky na tlačítka v menu:
			missingDirErrTitle = Constants.MN_JOP_MISSING_DIRECTORY_ERROR_TITLE;
			missingDirErrText = Constants.MN_JOP_MISSING_DIRECTORY_ERROR_TEXT + ": ";
			identicalDirErrTitle = Constants.MN_JOP_IDENTICAL_DIR_ERROR_TITLE;
			identicalDirErrText = Constants.MN_JOP_IDENTICAL_DIR_ERROR_TEXT;
			missingClassFileTitle = Constants.MN_JOP_MISSING_CLASS_FILE_TITLE;
			missingClassFileText = Constants.MN_JOP_MISSING_CLASS_FILE_TEXT + ": ";
			
			// texty pro kompilaci, nalezeni a zavolani metody main:
			missingSrcDirText = Constants.MN_TXT_MISSING_SRC_DIR_TEXT;
			missingSrcDirTitle = Constants.MN_TXT_MISSING_SRC_DIR_TITLE;
			errorWhileCallTheMainMethod = Constants.MN_TXT_ERROR_WHILE_CALL_THE_MAIN_METHOD;
			txtClass = Constants.MN_TXT_CLASS;
			txtError = Constants.MN_TXT_ERROR;
			methodIsNotaccessible = Constants.MN_TXT_METHOD_IS_NOT_ACCESSIBLE;
			txtIllegalArgumentExceptionPossibleErrors = Constants.MN_TXT_ILLEGAL_ARGUMENT_EXCEPTION_POSSIBLE_ERRORS;
			errorWhileCallingMainMethod = Constants.MN_TXT_ERROR_WHILE_CALLING_MAIN_METHOD;
			
			txtMainMethodNotFoundText = Constants.MN_TXT_MAIN_METHOD_NOT_FOUND_TEXT;
			txtMainMethodNotFoundTitle = Constants.MN_TXT_MAIN_METHOD_NOT_FOUND_TITLE;
			
			txtChooseClassOrTxtDialogTitle = Constants.MN_TXT_CHOOSE_CLASS_OR_TEXT_DOC_DIALOG_TITLE;	
			
			
			txtErrorWhilePrintingsClassDiagramText = Constants.MN_TXT_ERROR_WHILE_PRINTING_CLASS_DIAGRAM_TEXT;
			txtErrorWhilePrintingsClassDiagramTitle = Constants.MN_TXT_ERROR_WHILE_PRINTING_CLASS_DIAGRAM_TITLE;
			txtFailedToPrintClassDiagramText =  Constants.MN_TXT_FAILED_TO_PRINT_CLASS_DIAGRAM_TEXT;
			txtClassDiagram = Constants.MN_TXT_CLASS_DIAGRAM;
			txtFailedToPrintClassDiagramTitle = Constants.MN_TXT_FAILED_TO_PRINT_CLASS_DIAGRAM_TITLE;
			txtErrorWhilePrintingInstanceDiagramText = Constants.MN_TXT_ERROR_WHILE_PRINTING_INSTANCE_DIAGRAM_TEXT;
			txtErrorWhilePrintingInstanceDiagramTitle = Constants.MN_TXT_ERROR_WHILE_PRINTING_INSTANCE_DIAGRAM_TITLE;
			txtFailedToPrintInstanceDiagramText = Constants.MN_TXT_FAILED_TO_PRINT_INSTANCE_DIAGRAM_TEXT;
			txtInstanceDiagram = Constants.MN_TXT_INSTANCE_DIAGRAM;
			txtFailedToPrintInstanceDiagramTitle =  Constants.MN_TXT_FAILED_TO_PRINT_INSTANCE_DIAGRAM_TITLE;
			txtErrorWhileCopyFilesText = Constants.MN_TXT_ERROR_WHILE_COPY_FILES_TEXT;
			txtPossibleFileIsBeingUsed = Constants.MN_TXT_POSSIBLE_FILE_IS_BEING_USED;
			txtSource = Constants.MN_TXT_SOURCE_TEXT;
			txtDestination =Constants.MN_TXT_DESTINATION_TEXT;
			txtErrorWhileCopyFilesTitle = Constants.MN_TXT_ERROR_WHILE_COPY_FILES_TITLE;
			
			// Pro ProjectExplorer 
			txtPathToWorkspaceDoNotFoundText = Constants.MN_TXT_PATH_TO_WORKSPACE_DO_NOT_FOUND_TEXT;
			txtPathToWorkspaceDoNotFoundTitle = Constants.MN_TXT_PATH_TO_WORKSPACE_DO_NOT_FOUND_TITLE;
			
			txtOpenDirDoesNotExistText = Constants.MN_TXT_OPEN_DIR_DOES_NOT_EXIST_TEXT;
			txtOriginalLocation = Constants.MN_TXT_ORIGINAL_LOCATION;
			txtOpenDirDoesNotExistTitle = Constants.MN_TXT_OPEN_DIR_DOES_NOT_EXIST_TITLE;
			txtProjectIsNotOpenText = Constants.MN_TXT_PROJECT_IS_NOT_OPEN_TEXT;
			txtProjectIsNotOpenTitle = Constants.MN_TXT_PROJECT_IS_NOT_OPEN_TITLE;
			txtClassDiagramDoesntContainClassText = Constants.MN_TXT_CLASS_DIAGRAM_DOESNT_CONTAIN_CLASS_TEXT;
			txtClassDiagramDoesntContainClassTitle = Constants.MN_TXT_CLASS_DIAGRAM_DOESNT_CONTAIN_CLASS_TITLE;

			
			// Dotaz na restart aplikace při změně workspace:
			txtSwitchWorkspaceJopRestartAppNowText = Constants.MN_TXT_SWITCH_WORKSPACE_JOP_RESTART_APP_NOW_TEXT;
			txtSwitchWorkspaceJopRestartAppNowTT = Constants.MN_TXT_SWITCH_WORKSPACE_JOP_RESTART_APP_NOW_TT;

            // Chyba při získávání workspace a adresáře otevřeného projektu pro otevření v průzkumníku souboru:
            txtPathToWorkspaceWasNotFoundText = Constants.MN_TXT_PATH_TO_WORKSPACE_WAS_NOT_FOUND_TEXT;
            txtPathToWorkspaceWasNotFoundTitle = Constants.MN_TXT_PATH_TO_WORKSPACE_WAS_NOT_FOUND_TITLE;
            txtPathToOpenProjectWasNotFoundText = Constants.MN_TXT_PATH_TO_OPEN_PROJECT_WAS_NOT_FOUND_TEXT;
            txtPathToOpenProjectWasNotFoundTitle = Constants.MN_TXT_PATH_TO_OPEN_PROJECT_WAS_NOT_FOUND_TITLE;
		}
	}
	
	
	
	/**
	 * Metoda, která znepřístupní / zpřístupní nekterá tlačítka, která by neměla /
	 * měla být použita, pokud není / je otevřen projekt
	 * 
	 * Pokud nebude otevřen projekt, tak se zakážou položky, jinak se povolí:
	 * 
	 * @param enabled
	 *            - Logická proměnná, do které se vloží buď true nebo false, dle
	 *            toho, zda se mají tlačítka - komponenty povolit nebo zakázet
	 * 
	 *            Open project, save, save as, closeProject, všechny položky vmenu
	 *            edit, takže celé menu Edit, dále Compile v menu Tools
	 */
    public static void enableMenuItems(final boolean enabled) {
        itemCloseProject.setEnabled(enabled);
        itemSave.setEnabled(enabled);
        itemSaveAs.setEnabled(enabled);
        menuEdit.setEnabled(enabled);
        itemCompile.setEnabled(enabled);
        itemStart.setEnabled(enabled);
        itemPeOpenProject.setEnabled(enabled);
        itemFeOpenProject.setEnabled(enabled);
    }
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zkopíruje nějaký soubor ze zdroje (srcPath) do cíle (desPath) s
	 * "nastavením", že pokud již cíl existuje, tak se přepíše.
	 * 
	 * @param srcPath
	 *            - zdroj, resp. cesta k souboru, který se má zkopírovat
	 * 
	 * @param desPath
	 *            - cíl, resp. cesta k cíli, coby cílové umístění, kam se má
	 *            zdrojový soubor (srcPath) zkopírovat
	 */
	private static void copyFiles(final String srcPath, final String desPath) {
		try {
			/*
			 * V tomto konkrétním přípíadě by zde měli být mutexy k ničemu, jedná se o
			 * "hlavní" swingové vlákno, navíc poud se soubor, který se má zkopírovat na
			 * cílové umístění již na cílovém umístění existuje, a je zrovna využíván
			 * aplikací, stejně se soubor nezkopíruje, ale kdyby náhodou došlo k nějakým
			 * nedomyšleným okolnostem...
			 */
			final Semaphore mutex = new Semaphore(1);

			// "Spustím" mutex:
			mutex.acquire();

			// Zkopíruji soubor:
			Files.copy(Paths.get(srcPath), Paths.get(desPath), StandardCopyOption.REPLACE_EXISTING);

			// "Uvolním" mutex:
			mutex.release();

		} catch (IOException e) {
			/*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při kopírovní souborů, zdroj: " + srcPath + ", cíl: " + desPath);

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			
			JOptionPane.showMessageDialog(null,
					txtErrorWhileCopyFilesText + ":\n" + txtSource + ": " + srcPath + "\n" + txtDestination + ": "
							+ desPath + "\n" + txtPossibleFileIsBeingUsed,
					txtErrorWhileCopyFilesTitle, JOptionPane.ERROR_MESSAGE);

		} catch (InterruptedException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při kopírovní souborů, zdroj: " + srcPath + ", cíl: " + desPath
                                + ". Tato výjimka může nastat například v případě,"
                                + " že bylo aktuální vlákno přerušeno. Jedná se o případ využití mutexů.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
	}
}