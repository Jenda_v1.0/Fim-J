package cz.uhk.fim.fimj.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.Constants;

/**
 * Tato třída slouží ke zjištění, typu hrany: KOnkrétně obsahuje metody pro zjíštění, zda hrana reprezentuje asociace,
 * dědičnost, implementaci, agregaci, či hranu komentáře
 * <p>
 * Postup: Každá metoda, se zjistí konkrétní parametry hrany a na zákldě těchto vlastností reozhodne zda je daná hrana
 * požadovaný typ či nikoliv
 * <p>
 * <p>
 * Note: u každé metody se načítají data ze souboru typu .properties pokaždé, protože uživatel může v podstatě kdykoli
 * za běhu aplikace změnit v příslušném souboru data a mohlo by dojí; k nějaké chybě pri porovnávání. Tato třída je
 * vytvořena pouze jednou na místěch, kde je potřeba, pak se pouze volají metody, proto si vždy načítají soubor. Využívá
 * to převážně menu nebo i diagram tříd, akorát je nevhoda při cyklu kdyz se testuje více typů hran v jednom cyklu -
 * akorát se to několkrát během chvíle načte ale to nelze dopředu říci, kdy má uživatel v plánu něco upravit a kdy ne.
 * <p>
 * <p>
 * Note: Třída předpokládá, že žádné dvě libovolné hrany v grafu nebudou stejné pak by metody selhaly, odpověděla by
 * první v pořadí i když by se nemuselo jednat o tu správnou Např: Hrany pro reprezentaci dědičnosti a implementace
 * budou mít identické vlastnost Uživatel bude chtít smazat implementaci ale jako prvně se otestuje dědičnost a jelikož
 * jsou obě hrany stejně tak se vrátí true u dědičnost a bude se chtít smazat dědičnost, což je "chyba"
 * <p>
 * <p>
 * Note: Není vyzkoušeno, ale toto porovnávání by mělo selhat, pokud si aplikace načte data z příslušného souboru ve
 * Workspace, vytvoří se v diagramu nějaké obejkty, pak se ve Workspace přislušný soubor s nastavením pro diagram tříd
 * smaže a jak mile dojde k porovnávání tak si aplikace načte bud výchozí data z "své" složk, kterou jsem pro to
 * definoval, nebo si načte data z třídy Constant coby výchozí hodnoty, ať tak či tak, pokud budou načtena data ze
 * souboru z Workspace a ten se smaže budou se neustále porovnávat chyná data. (Za předpokladu, že se v souboru z
 * Workspace nacházelo odlišné nstavení od výchozího)
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class KindOfEdge implements KindOfEdgeInterface {

	/**
	 * Proměnná, která obsahuje referenci na objekt s informacemi ohledně nastavení
	 * diagramu tříd.
	 */
	private static Properties classDiagramProperties;
	
	

	/**
	 * Konstruktor této třídy.
	 */
	public KindOfEdge() {
		super();
		
		// Načtu si soubor, kter jsou uloženy všechny hodnoty pro diagram tříd, abych mohl jednoznačně
		// určit, o jaký objekt v diagramu se jedná.
		classDiagramProperties = App.READ_FILE.getClassDiagramProperties();
	}
	
	
	
	

	
	@Override
	public boolean isAssociationEdge(final DefaultEdge edge) {
		final Map<?, ?> attributes = edge.getAttributes();
		
		if (classDiagramProperties != null) {
			final String lineColor = classDiagramProperties.getProperty("AscEdgeLineColor", Integer.toString(Constants.CD_ASC_EDGE_COLOR.getRGB()));
			
			if (GraphConstants.getLineColor(attributes).getRGB() != Integer.parseInt(lineColor))
				return false;
			
			
			final boolean labelsAlongEdge = Boolean.parseBoolean(classDiagramProperties.getProperty("AscEdgeLabelAlongEdge", String.valueOf(Constants.CD_ASC_LABEL_ALONG_EDGE)));
			if (GraphConstants.isLabelAlongEdge(attributes) != labelsAlongEdge)
				return false;
			
			final int lineStyle = Integer.parseInt(classDiagramProperties.getProperty("AscEdgeLineStyle", Integer.toString(Constants.CD_ASC_EDGE_LINE_STYLE)));
			if (GraphConstants.getLineStyle(attributes) != lineStyle)
				return false;
			
			final int lineEnd = Integer.parseInt(classDiagramProperties.getProperty("AscEdgeLineEnd", Integer.toString(Constants.CD_ASC_EDGE_LINE_END)));
			if (GraphConstants.getLineEnd(attributes) != lineEnd)
				return false;
			
			final int lineBegin = Integer.parseInt(classDiagramProperties.getProperty("AscEdgeLineBegin", Integer.toString(Constants.CD_ASC_EDGE_LINE_BEGIN)));
			if (GraphConstants.getLineBegin(attributes) != lineBegin)
				return false;
			
			final float lineWidth = Float.parseFloat(classDiagramProperties.getProperty("AscEdgeLineWidth", String.valueOf(Constants.CD_ASC_EDGE_LINE_WIDTH)));
			if (GraphConstants.getLineWidth(attributes) != lineWidth)
				return false;
			
			final boolean endFill = Boolean.parseBoolean(classDiagramProperties.getProperty("AscEdgeEndFill", String.valueOf(Constants.CD_ASC_EDGE_END_FILL)));
			if (GraphConstants.isEndFill(attributes) != endFill)
				return false;
			
			final boolean beginFill = Boolean.parseBoolean(classDiagramProperties.getProperty("AscEdgeBeginFill", String.valueOf(Constants.CD_ASC_EDGE_BEGIN_FILL)));
			if (GraphConstants.isBeginFill(attributes) != beginFill)
				return false;
			
			// Barva textu:
			final String fontColor = classDiagramProperties.getProperty("AscEdgeLineTextColor", Integer.toString(Constants.CD_ASC_EDGE_FONT_COLOR.getRGB()));
			if (GraphConstants.getForeground(attributes).getRGB() != Integer.parseInt(fontColor))
				return false;
			
			// Font poznámky na hraně:
			final int fontSize = Integer.parseInt(classDiagramProperties.getProperty("AscEdgeLineFontSize", Integer.toString(Constants.CD_ASC_EDGE_LINE_FONT.getSize())));
			final int fontStyle = Integer.parseInt(classDiagramProperties.getProperty("AscEdgeLineFontStyle", Integer.toString(Constants.CD_ASC_EDGE_LINE_FONT.getStyle())));
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.CD_ASC_EDGE_LINE_FONT.getName(), fontStyle, fontSize)))
				return false;
		}
		
		
		else {			
			if (GraphConstants.getLineColor(attributes).getRGB() != Constants.CD_ASC_EDGE_COLOR.getRGB())
				return false;
			
			if (GraphConstants.isLabelAlongEdge(attributes) != Constants.CD_ASC_LABEL_ALONG_EDGE)
				return false;
			
			if (GraphConstants.getLineStyle(attributes) != Constants.CD_ASC_EDGE_LINE_STYLE)
				return false;
			
			if (GraphConstants.getLineEnd(attributes) != Constants.CD_ASC_EDGE_LINE_END)
				return false;
			
			if (GraphConstants.getLineBegin(attributes) != Constants.CD_ASC_EDGE_LINE_BEGIN)
				return false;
			
			if (GraphConstants.getLineWidth(attributes) != Constants.CD_ASC_EDGE_LINE_WIDTH)
				return false;
			
			if (GraphConstants.isEndFill(attributes) != Constants.CD_ASC_EDGE_END_FILL)
				return false;
			
			if (GraphConstants.isBeginFill(attributes) != Constants.CD_ASC_EDGE_BEGIN_FILL)
				return false;		
			
			// Barva textu:
			if (GraphConstants.getForeground(attributes).getRGB() != Constants.CD_ASC_EDGE_FONT_COLOR.getRGB())
				return false;
			
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.CD_ASC_EDGE_LINE_FONT.getName(),
					Constants.CD_ASC_EDGE_LINE_FONT.getStyle(), Constants.CD_ASC_EDGE_LINE_FONT.getSize())))
				return false;
		}
		
		// Kromě hrany reprezentující implementaci rozhraní nemá žádný jiná hrana čerchovaný styl čáry - bude null:
		final float[] pattern = GraphConstants.getDashPattern(attributes);
		
		if (!Arrays.equals(pattern, null))
			return false;		
		
		
		
		/*
		 * Kardinalita / (multiplicita) na hranách musí být v případě tohoto vztahu
		 * (symetrická) asociace na obou stranách stejná.
		 */
		final Object[] labels = GraphConstants.getExtraLabels(attributes);
		
		if (labels.length != 2)
			return false;
		
		// Porovnám hodnoty:
		if (labels[0] != labels[1])
			return false;
		
		
		final Point2D[] points = { new Point2D.Double(GraphConstants.PERMILLE * 7 / 8, -20),
				new Point2D.Double(GraphConstants.PERMILLE / 8, -20) };
		final Point2D[] points2 = GraphConstants.getExtraLabelPositions(attributes);

		// Pro true: Sem se dostanu, pokud veškeré hodnoty odpovídají, tudíž se jedná o daný typ hrany:
		return Arrays.equals(points, points2);
	}

	
	
	
	
	
	
	
	
	
	@Override
	public boolean isInheritEdge(final DefaultEdge edge) {
		final Map<?, ?> attributes = edge.getAttributes();
		
		if (classDiagramProperties != null) {
			final String colorLine = classDiagramProperties.getProperty("ExtEdgeLineColor", Integer.toString(Constants.CD_EXT_EDGE_COLOR.getRGB()));
			if (GraphConstants.getLineColor(attributes).getRGB() != Integer.parseInt(colorLine))
				return false;
			
			final boolean labelAlongEdge = Boolean.parseBoolean(classDiagramProperties.getProperty("ExtEdgeLabelAlongEdge", String.valueOf(Constants.CD_EXT_LABEL_ALONG_EDGE)));
			if (GraphConstants.isLabelAlongEdge(attributes) != labelAlongEdge)
				return false;
			
			final int lineStyle = Integer.parseInt(classDiagramProperties.getProperty("ExtEdgeLineStyle", Integer.toString(Constants.CD_EXT_EDGE_LINE_STYLE)));
			if (GraphConstants.getLineStyle(attributes) != lineStyle)
				return false;
						
			final int lineEnd = Integer.parseInt(classDiagramProperties.getProperty("ExtEdgeLineEnd", Integer.toString(Constants.CD_EXT_EDGE_LINE_END)));
			if (GraphConstants.getLineEnd(attributes) != lineEnd)
				return false;
			
			final int lineBegin = Integer.parseInt(classDiagramProperties.getProperty("ExtEdgeLineBegin", Integer.toString(Constants.CD_EXT_EDGE_LINE_BEGIN)));
			if (GraphConstants.getLineBegin(attributes) != lineBegin)
				return false;
			
			final float lineWidth = Float.parseFloat(classDiagramProperties.getProperty("ExtEdgeLineWidth", String.valueOf(Constants.CD_EXT_EDGE_LINE_WIDTH)));
			if (GraphConstants.getLineWidth(attributes) != lineWidth)
				return false;
				
			final boolean endFill = Boolean.parseBoolean(classDiagramProperties.getProperty("ExtEdgeEndLineFill", String.valueOf(Constants.CD_EXT_EDGE_END_FILL)));
			if (GraphConstants.isEndFill(attributes) != endFill)
				return false;
			
			final boolean beginFill = Boolean.parseBoolean(classDiagramProperties.getProperty("ExtEdgeBeginLineFill", String.valueOf(Constants.CD_EXT_EDGE_BEGIN_FILL)));
			if (GraphConstants.isBeginFill(attributes) != beginFill)
				return false;
			
			final String fontColor = classDiagramProperties.getProperty("ExtEdgeLineTextColor", Integer.toString(Constants.CD_EXT_EDGE_FONT_COLOR.getRGB()));
			if (GraphConstants.getForeground(attributes).getRGB() != Integer.parseInt(fontColor))
				return false;
			
			// Font poznámky na hraně:
			final int fontSize = Integer.parseInt(classDiagramProperties.getProperty("ExtEdgeLineFontSize", Integer.toString(Constants.CD_EXT_EDGE_LINE_FONT.getSize())));
			final int fontStyle = Integer.parseInt(classDiagramProperties.getProperty("ExtEdgeLineFontStyle", Integer.toString(Constants.CD_EXT_EDGE_LINE_FONT.getStyle())));
			
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.CD_EXT_EDGE_LINE_FONT.getName(), fontStyle, fontSize)))
				return false;
		}
		
		
		else {
			if (GraphConstants.getLineColor(attributes).getRGB() != Constants.CD_EXT_EDGE_COLOR.getRGB())
				return false;
			
			if (GraphConstants.isLabelAlongEdge(attributes) != Constants.CD_EXT_LABEL_ALONG_EDGE)
				return false;
			
			if (GraphConstants.getLineStyle(attributes) != Constants.CD_EXT_EDGE_LINE_STYLE)
				return false;
						
			if (GraphConstants.getLineEnd(attributes) != Constants.CD_EXT_EDGE_LINE_END)
				return false;
			
			if (GraphConstants.getLineBegin(attributes) != Constants.CD_EXT_EDGE_LINE_BEGIN)
				return false;
			
			if (GraphConstants.getLineWidth(attributes) != Constants.CD_EXT_EDGE_LINE_WIDTH)
				return false;
			
			if (GraphConstants.isEndFill(attributes) != Constants.CD_EXT_EDGE_END_FILL)
				return false;
						
			if (GraphConstants.isBeginFill(attributes) != Constants.CD_EXT_EDGE_BEGIN_FILL)
				return false;
			
			if (GraphConstants.getForeground(attributes).getRGB() != Constants.CD_EXT_EDGE_FONT_COLOR.getRGB())
				return false;
			
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.CD_EXT_EDGE_LINE_FONT.getName(),
					Constants.CD_EXT_EDGE_LINE_FONT.getStyle(), Constants.CD_EXT_EDGE_LINE_FONT.getSize())))
				return false;
		}
		
		// Kromě hrany reprezentující implementaci rozhraní nemá žádný jiná hrana čerchovaný styl čáry - bude null:
		final float[] pattern = GraphConstants.getDashPattern(attributes);

		// Zde byly všechny vlastnosti splněny, tak se jedná o danou hranu - vrátím true, jinak false
		return Arrays.equals(pattern, null);
	}

	
	
	
	
	
	
	
	
	
	
	
	@Override
	public boolean isImplementsEdge(final DefaultEdge edge) {
		final Map<?, ?> attributes = edge.getAttributes();
		
		if (classDiagramProperties != null) {
			final String colorLine = classDiagramProperties.getProperty("ImpEdgeLineColor", Integer.toString(Constants.CD_IMP_EDGE_COLOR.getRGB()));
			if (GraphConstants.getLineColor(attributes).getRGB() != Integer.parseInt(colorLine))
				return false;
			
			final boolean labelAlongEdge = Boolean.parseBoolean(classDiagramProperties.getProperty("ImpEdgeLabelAlongEdge", String.valueOf(Constants.CD_IMP_LABEL_ALONG_EDGE)));
			if (GraphConstants.isLabelAlongEdge(attributes) != labelAlongEdge)
				return false;
			
			final int lineStyle = Integer.parseInt(classDiagramProperties.getProperty("ImpEdgeLineStyle", Integer.toString(Constants.CD_IMP_EDGE_LINE_STYLE)));
			if (GraphConstants.getLineStyle(attributes) != lineStyle)
				return false;
						
			final int lineEnd = Integer.parseInt(classDiagramProperties.getProperty("ImpEdgeLineEnd", Integer.toString(Constants.CD_IMP_EDGE_LINE_END)));
			if (GraphConstants.getLineEnd(attributes) != lineEnd)
				return false;
			
			final int lineBegin = Integer.parseInt(classDiagramProperties.getProperty("ImpEdgeLineBegin", Integer.toString(Constants.CD_IMP_EDGE_LINE_BEGIN)));
			if (GraphConstants.getLineBegin(attributes) != lineBegin)
				return false;
			
			final float lineWidth = Float.parseFloat(classDiagramProperties.getProperty("ImpEdgeLineWidth", String.valueOf(Constants.CD_IMP_EDGE_LINE_WIDTH)));
			if (GraphConstants.getLineWidth(attributes) != lineWidth)
				return false;
				
			final boolean endFill = Boolean.parseBoolean(classDiagramProperties.getProperty("ImpEdgeEndLineFill", String.valueOf(Constants.CD_IMP_EDGE_END_FILL)));
			if (GraphConstants.isEndFill(attributes) != endFill)
				return false;
			
			final boolean beginFill = Boolean.parseBoolean(classDiagramProperties.getProperty("ImpEdgeBeginLineFill", String.valueOf(Constants.CD_IMP_EDGE_BEGIN_FILL)));
			if (GraphConstants.isBeginFill(attributes) != beginFill)
				return false;
			
			final String fontColor = classDiagramProperties.getProperty("ImpEdgeLineTextColor", Integer.toString(Constants.CD_IMP_EDGE_FONT_COLOR.getRGB()));
			if (GraphConstants.getForeground(attributes).getRGB() != Integer.parseInt(fontColor))
				return false;
			
			// Font poznámky na hraně:
			final int fontSize = Integer.parseInt(classDiagramProperties.getProperty("ImpEdgeLineFontSize", Integer.toString(Constants.CD_IMP_EDGE_LINE_FONT.getSize())));
			final int fontStyle = Integer.parseInt(classDiagramProperties.getProperty("ImpEdgeLineFontStyle", Integer.toString(Constants.CD_IMP_EDGE_LINE_FONT.getStyle())));
			
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.CD_IMP_EDGE_LINE_FONT.getName(), fontStyle, fontSize)))
				return false;
			
			// Čerchovaná hrana - čára:
			// Ukládá se ve formátu: [X, X, X, ...]
			// tak si akorát z textu odstraním závorky (substring), dáel odeberu mezery a předám do do jednorozměrného pole typu float:
			String patternText = classDiagramProperties.getProperty("ImpEdgeDashPattern", Arrays.toString(Constants.CD_IMP_PATTERN));
			// text bez závorek:
			patternText = patternText.substring(1, patternText.length() - 1);
			
			
			// pole čísel typu string:
			final String[] partsOfPattern = patternText.trim().replace("\\s", "").split(",");
			
			final float[] patternArray = new float[partsOfPattern.length];
			
			for (int i = 0; i < partsOfPattern.length; i++)
				patternArray[i] = Float.parseFloat(partsOfPattern[i]);
			
			final float[] patternArray2 = GraphConstants.getDashPattern(attributes);

			return Arrays.equals(patternArray, patternArray2);
		}
		
		
		else {
			if (GraphConstants.getLineColor(attributes).getRGB() != Constants.CD_IMP_EDGE_COLOR.getRGB())
				return false;
			
			if (GraphConstants.isLabelAlongEdge(attributes) != Constants.CD_IMP_LABEL_ALONG_EDGE)
				return false;
			
			if (GraphConstants.getLineStyle(attributes) != Constants.CD_IMP_EDGE_LINE_STYLE)
				return false;
						
			if (GraphConstants.getLineEnd(attributes) != Constants.CD_IMP_EDGE_LINE_END)
				return false;
			
			if (GraphConstants.getLineBegin(attributes) != Constants.CD_IMP_EDGE_LINE_BEGIN)
				return false;
			
			if (GraphConstants.getLineWidth(attributes) != Constants.CD_IMP_EDGE_LINE_WIDTH)
				return false;
			
			if (GraphConstants.isEndFill(attributes) != Constants.CD_IMP_EDGE_END_FILL)
				return false;
						
			if (GraphConstants.isBeginFill(attributes) != Constants.CD_IMP_EDGE_BEGIN_FILL)
				return false;
			
			if (GraphConstants.getForeground(attributes).getRGB() != Constants.CD_IMP_EDGE_FONT_COLOR.getRGB())
				return false;
			
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.CD_IMP_EDGE_LINE_FONT.getName(),
					Constants.CD_IMP_EDGE_LINE_FONT.getStyle(), Constants.CD_IMP_EDGE_LINE_FONT.getSize())))
				return false;
			
			final float[] patternArrays = Constants.CD_IMP_PATTERN;
			final float[] patternArrays2 = GraphConstants.getDashPattern(attributes);

			return Arrays.equals(patternArrays, patternArrays2);
		}
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public boolean isAggregation_1_ku_1_Edge(final DefaultEdge edge) {
		final Map<?, ?> attributes = edge.getAttributes();
		
		if (classDiagramProperties != null) {
			final String lineColor = classDiagramProperties.getProperty("AgrEdgeLineColor", Integer.toString(Constants.CD_AGR_EDGE_COLOR.getRGB()));			
			if (GraphConstants.getLineColor(attributes).getRGB() != Integer.parseInt(lineColor))
				return false;
						
			final boolean labelsAlongEdge = Boolean.parseBoolean(classDiagramProperties.getProperty("AgrEdgeLabelAlongEdge", String.valueOf(Constants.CD_AGR_LABEL_ALONG_EDGE)));
			if (GraphConstants.isLabelAlongEdge(attributes) != labelsAlongEdge)
				return false;
			
			final int lineStyle = Integer.parseInt(classDiagramProperties.getProperty("AgrEdgeLineStyle", Integer.toString(Constants.CD_AGR_EDGE_LINE_STYLE)));
			if (GraphConstants.getLineStyle(attributes) != lineStyle)
				return false;
			
			final int lineEnd = Integer.parseInt(classDiagramProperties.getProperty("AgrEdgeLineEnd", Integer.toString(Constants.CD_AGR_EDGE_LINE_END)));
			if (GraphConstants.getLineEnd(attributes) != lineEnd)
				return false;
			
			final int lineBegin = Integer.parseInt(classDiagramProperties.getProperty("AgrEdgeLineBegin", Integer.toString(Constants.CD_AGR_EDGE_LINE_BEGIN)));
			if (GraphConstants.getLineBegin(attributes) != lineBegin)
				return false;
			
			final float lineWidth = Float.parseFloat(classDiagramProperties.getProperty("AgrEdgeLineWidth", String.valueOf(Constants.CD_AGR_EDGE_LINE_WIDTH)));
			if (GraphConstants.getLineWidth(attributes) != lineWidth)
				return false;
			
			final boolean endFill = Boolean.parseBoolean(classDiagramProperties.getProperty("AgrEdgeEndLineFill", String.valueOf(Constants.CD_AGR_EDGE_END_FILL)));
			if (GraphConstants.isEndFill(attributes) != endFill)
				return false;
			
			final boolean beginFill = Boolean.parseBoolean(classDiagramProperties.getProperty("AgrEdgeBeginLineFill", String.valueOf(Constants.CD_AGR_EDGE_BEGIN_FILL)));
			if (GraphConstants.isBeginFill(attributes) != beginFill)
				return false;
			
			final String fontColor = classDiagramProperties.getProperty("AgrEdgeLineTextColor", Integer.toString(Constants.CD_AGR_EDGE_FONT_COLOR.getRGB()));
			if (GraphConstants.getForeground(attributes).getRGB() != Integer.parseInt(fontColor))
				return false;
			
			// Font poznámky na hraně:
			final int fontSize = Integer.parseInt(classDiagramProperties.getProperty("AgrEdgeLineFontSize", Integer.toString(Constants.CD_AGR_EDGE_LINE_FONT.getSize())));
			final int fontStyle = Integer.parseInt(classDiagramProperties.getProperty("AgrEdgeLineFontStyle", Integer.toString(Constants.CD_AGR_EDGE_LINE_FONT.getStyle())));
			
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.CD_AGR_EDGE_LINE_FONT.getName(), fontStyle, fontSize)))
				return false;
		}
		
		
		else {
			if (GraphConstants.getLineColor(attributes).getRGB() != Constants.CD_AGR_EDGE_COLOR.getRGB())
				return false;
			
			if (GraphConstants.isLabelAlongEdge(attributes) != Constants.CD_AGR_LABEL_ALONG_EDGE)
				return false;
			
			if (GraphConstants.getLineStyle(attributes) != Constants.CD_AGR_EDGE_LINE_STYLE)
				return false;
						
			if (GraphConstants.getLineEnd(attributes) != Constants.CD_AGR_EDGE_LINE_END)
				return false;
			
			if (GraphConstants.getLineBegin(attributes) != Constants.CD_AGR_EDGE_LINE_BEGIN)
				return false;
			
			if (GraphConstants.getLineWidth(attributes) != Constants.CD_AGR_EDGE_LINE_WIDTH)
				return false;
			
			if (GraphConstants.isEndFill(attributes) != Constants.CD_AGR_EDGE_END_FILL)
				return false;
						
			if (GraphConstants.isBeginFill(attributes) != Constants.CD_AGR_EDGE_BEGIN_FILL)
				return false;
			
			if (GraphConstants.getForeground(attributes).getRGB() != Constants.CD_AGR_EDGE_FONT_COLOR.getRGB())
				return false;
			
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.CD_AGR_EDGE_LINE_FONT.getName(),
					Constants.CD_AGR_EDGE_LINE_FONT.getStyle(), Constants.CD_AGR_EDGE_LINE_FONT.getSize())))
				return false;
		}
		
		
		
		
		/*
		 * Zde by bylo vhodné otestovat ještě kardinalitu / (multiplicitu) na obou
		 * koncích hrany, jenže to není tak úplně možné, protože v případě tohoto vztahu
		 * mohou být na obou koncích hrany 1 : 1 (jednička na obou koncích), protože by
		 * se jednalo o "klasický" vztahy typu (asymetrická) asociace (agregace 1 : 1),
		 * tento případ také může nastat v případě, že se vztahy (symetrická) asociace
		 * má zobrazovat jako agregace 1 : 1 (asymetrická asociace), ale když se bude
		 * jednat o "klasickou" agregaci 1 : 1, resp. teď už (asymetrickou) asociaci,
		 * pak na obou koncích hran mohou být různá čísla.
		 * 
		 * Proto zde ty čísla na obou koncích hrany testovat nebudu, dopředu nevím, zda
		 * by se jednalo o agregaci 1 : 1 (asymetrickou asociaci) nebo (symetrickou)
		 * asociaci.
		 */
		
		
		
		final Point2D[] points = { new Point2D.Double(GraphConstants.PERMILLE * 7 / 8, -20),
				new Point2D.Double(GraphConstants.PERMILLE / 8, -20) };
		final Point2D[] points2 = GraphConstants.getExtraLabelPositions(attributes);
		
		if (!Arrays.equals(points, points2))
			return false;
		
		// Kromě hrany reprezentující implementaci rozhraní nemá žádný jiná hrana čerchovaný styl čáry - bude null:
		final float[] pattern = GraphConstants.getDashPattern(attributes);

		return Arrays.equals(pattern, null);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

	@Override
	public boolean isAggregation_1_ku_N_Edge(final DefaultEdge edge) {
		final Map<?, ?> attributes = edge.getAttributes();
		
		if (classDiagramProperties != null) {
			final String lineColor = classDiagramProperties.getProperty("Agr_1_N_EdgeLineColor", Integer.toString(Constants.CD_AGR_N_EDGE_COLOR.getRGB()));			
			if (GraphConstants.getLineColor(attributes).getRGB() != Integer.parseInt(lineColor))
				return false;
						
			final boolean labelsAlongEdge = Boolean.parseBoolean(classDiagramProperties.getProperty("Agr_1_N_EdgeLabelAlongEdge", String.valueOf(Constants.CD_AGR_N_LABEL_ALONG_EDGE)));
			if (GraphConstants.isLabelAlongEdge(attributes) != labelsAlongEdge)
				return false;
			
			final int lineStyle = Integer.parseInt(classDiagramProperties.getProperty("Agr_1_N_EdgeLineStyle", Integer.toString(Constants.CD_AGR_EDGE_N_LINE_STYLE)));
			if (GraphConstants.getLineStyle(attributes) != lineStyle)
				return false;
			
			final int lineEnd = Integer.parseInt(classDiagramProperties.getProperty("Agr_1_N_EdgeLineEnd", Integer.toString(Constants.CD_AGR_N_EDGE_LINE_END)));
			if (GraphConstants.getLineEnd(attributes) != lineEnd)
				return false;
			
			final int lineBegin = Integer.parseInt(classDiagramProperties.getProperty("Agr_1_N_EdgeLineBegin", Integer.toString(Constants.CD_AGR_N_EDGE_LINE_BEGIN)));
			if (GraphConstants.getLineBegin(attributes) != lineBegin)
				return false;
			
			final float lineWidth = Float.parseFloat(classDiagramProperties.getProperty("Agr_1_N_EdgeLineWidth", String.valueOf(Constants.CD_AGR_EDGE_N_LINE_WIDTH)));
			if (GraphConstants.getLineWidth(attributes) != lineWidth)
				return false;
			
			final boolean endFill = Boolean.parseBoolean(classDiagramProperties.getProperty("Agr_1_N_EdgeEndLineFill", String.valueOf(Constants.CD_AGR_N_EDGE_END_FILL)));
			if (GraphConstants.isEndFill(attributes) != endFill)
				return false;
			
			final boolean beginFill = Boolean.parseBoolean(classDiagramProperties.getProperty("Agr_1_N_EdgeBeginLineFill", String.valueOf(Constants.CD_AGR_N_EDGE_BEGIN_FILL)));
			if (GraphConstants.isBeginFill(attributes) != beginFill)
				return false;
			
			final String fontColor = classDiagramProperties.getProperty("Agr_1_N_EdgeLineTextColor", Integer.toString(Constants.CD_AGR_N_EDGE_FONT_COLOR.getRGB()));
			if (GraphConstants.getForeground(attributes).getRGB() != Integer.parseInt(fontColor))
				return false;
			
			// Font poznámky na hraně:
			final int fontSize = Integer.parseInt(classDiagramProperties.getProperty("Agr_1_N_EdgeLineFontSize", Integer.toString(Constants.CD_AGR_EDGE_N_LINE_FONT.getSize())));
			final int fontStyle = Integer.parseInt(classDiagramProperties.getProperty("Agr_1_N_EdgeLineFontStyle", Integer.toString(Constants.CD_AGR_EDGE_N_LINE_FONT.getStyle())));
			
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.CD_AGR_EDGE_N_LINE_FONT.getName(), fontStyle, fontSize)))
				return false;
		}
		
		
		else {
			if (GraphConstants.getLineColor(attributes).getRGB() != Constants.CD_AGR_N_EDGE_COLOR.getRGB())
				return false;
			
			if (GraphConstants.isLabelAlongEdge(attributes) != Constants.CD_AGR_N_LABEL_ALONG_EDGE)
				return false;
			
			if (GraphConstants.getLineStyle(attributes) != Constants.CD_AGR_EDGE_N_LINE_STYLE)
				return false;
						
			if (GraphConstants.getLineEnd(attributes) != Constants.CD_AGR_N_EDGE_LINE_END)
				return false;
			
			if (GraphConstants.getLineBegin(attributes) != Constants.CD_AGR_N_EDGE_LINE_BEGIN)
				return false;
			
			if (GraphConstants.getLineWidth(attributes) != Constants.CD_AGR_EDGE_N_LINE_WIDTH)
				return false;
			
			if (GraphConstants.isEndFill(attributes) != Constants.CD_AGR_N_EDGE_END_FILL)
				return false;
						
			if (GraphConstants.isBeginFill(attributes) != Constants.CD_AGR_N_EDGE_BEGIN_FILL)
				return false;
			
			if (GraphConstants.getForeground(attributes).getRGB() != Constants.CD_AGR_N_EDGE_FONT_COLOR.getRGB())
				return false;
						
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.CD_AGR_EDGE_N_LINE_FONT.getName(),
					Constants.CD_AGR_EDGE_N_LINE_FONT.getStyle(), Constants.CD_AGR_EDGE_N_LINE_FONT.getSize())))
				return false;
		}
		
		
		final Object[] labels = {"N", "1"};
		final Object[] labels2 = GraphConstants.getExtraLabels(attributes);
		
		// Porovnám pole:
		if (!Arrays.equals(labels, labels2))
			return false;
		
		
		final Point2D[] points = { new Point2D.Double(GraphConstants.PERMILLE * 7 / 8, -20),
				new Point2D.Double(GraphConstants.PERMILLE / 8, -20) };
		final Point2D[] points2 = GraphConstants.getExtraLabelPositions(attributes);
		
		if (!Arrays.equals(points, points2))
			return false;
		
		
		// Kromě hrany reprezentující implementaci rozhraní nemá žádný jiná hrana čerchovaný styl čáry - bude null:
		float[] pattern = GraphConstants.getDashPattern(attributes);

		return Arrays.equals(pattern, null);
	}










	@Override
	public boolean isClassCell(final DefaultGraphCell cell) {
		final Map<?, ?> attributes = cell.getAttributes();
		
		if (classDiagramProperties != null) {
			// Font pro název třídy:
			final int fontSize = Integer.parseInt(classDiagramProperties.getProperty("ClassFontSize", Integer.toString(Constants.CD_FONT_CLASS.getSize())));
			final int fontStyle = Integer.parseInt(classDiagramProperties.getProperty("ClassFontType", Integer.toString(Constants.CD_FONT_CLASS.getStyle())));
			
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.CD_FONT_CLASS.getName(), fontStyle, fontSize)))
				return false;
			
			
			// Zarovnání pro písmo a obrázky:
			final int verticalAlignment = Integer.parseInt(classDiagramProperties.getProperty("VerticalAlignment", String.valueOf(Constants.CD_VERTICAL_ALIGNMENT)));
			if (GraphConstants.getVerticalAlignment(attributes) != verticalAlignment)
				return false;
			
			
			final int horizontalAlignment = Integer.parseInt(classDiagramProperties.getProperty("HorizontalAlignment", String.valueOf(Constants.CD_HORIZONTAL_ALIGNMENT)));
			if (GraphConstants.getHorizontalAlignment(attributes) != horizontalAlignment)
				return false;
			
			
			// Barva písma
			final String textColor = classDiagramProperties.getProperty("ForegroundColor", Integer.toString(Constants.CD_TEXT_COLOR.getRGB()));
			if (GraphConstants.getForeground(attributes).getRGB() != Integer.parseInt(textColor))
				return false;
			
			
			
			// Nyní si pro nastavení barvy pozadí musím načíst logickou proměnnou, dle které poznám, zda se má obarvit
			// pozadí buňky  jednou barvou nebo dvěmí:
			final boolean oneColor = Boolean.parseBoolean(classDiagramProperties.getProperty("ClassOneBgColorBoolean", String.valueOf(Constants.CD_ONE_BG_COLOR_BOOLEAN)));
			
			
			if (oneColor) {
				final String bgColor = classDiagramProperties.getProperty("ClassOneBackgroundColor", Integer.toString(Constants.CD_ONE_BG_COLOR.getRGB()));
				
				// Barva pozadí - levý horní roh:
				if (GraphConstants.getBackground(attributes).getRGB() != Integer.parseInt(bgColor))
					return false;
				
				
				// Barva pozadí - pravý dolní roh:
				if (GraphConstants.getGradientColor(attributes).getRGB() != Integer.parseInt(bgColor))
					return false;
				
			}
			
			else {
				// Barva pozadí - levý horní roh:
				final String backgroundColor = classDiagramProperties.getProperty("ClassBackgroundColor", Integer.toString(Constants.CD_CLASS_BACKGROUND_COLOR.getRGB()));
				if (GraphConstants.getBackground(attributes).getRGB() != Integer.parseInt(backgroundColor))
					return false;
				
				
				// Barva pozadí - pravý dolní roh:
				final String gradientColor = classDiagramProperties.getProperty("GradientColor", Integer.toString(Constants.CD_GRADIENT_COLOR.getRGB()));
				if (GraphConstants.getGradientColor(attributes).getRGB() != Integer.parseInt(gradientColor))
					return false;
			}
			
			
			

			
			// Ohraničení:
			// Note: - Ohraničení jsem v kódu pevně definovat a nelze změnit, takže ho ani nemusím testovat, jedná se pouze o barvy pro 
			// levý horní a pravý dolní roh, ale ty jsou testovány výše.
			
			/*
			 * Note:
			 * Otočil jsem logickou hodnotu pro průhlednost, knihovna pro grafy chce jako
			 * hodnotu pro to, aby byla buňka průhledná hodnotu false, ale do souboru
			 * zapisuji pro průhlednou buňku hodnotu true, aby to bylo pro uživatele
			 * "přehlednější" a nemusel si otáčet logickou hodnotu, proto tuto hodnotu
			 * raději "otočím" zde v kódu, takže když v následující podmínce testuji, jestli
			 * je buňka průhledná, tak musím načtenou hodnotu ze souboru "otočit", aby se
			 * mohla shodovat s hodnotou dle knihovny.
			 */
			
			// Nastavení neprůhlednosti buňky - aby nebyla průhledná
			final boolean opaque = !Boolean.parseBoolean(classDiagramProperties.getProperty("ClassCellOpaque", String.valueOf(Constants.CD_OPAQUE_CLASS_CELL)));


			return GraphConstants.isOpaque(attributes) == opaque;
		}
		
		
		// Zde se předpokládá výchozí nastavení:
		else {
			// Odebral jsem hodnotu pro nazev fontu ze souboru:
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.CD_FONT_CLASS.getName(),
					Constants.CD_FONT_CLASS.getStyle(), Constants.CD_FONT_CLASS.getSize())))
				return false;
			
			
			if (GraphConstants.getVerticalAlignment(attributes) != Constants.CD_VERTICAL_ALIGNMENT)
				return false;
			
			
			if (GraphConstants.getHorizontalAlignment(attributes) != Constants.CD_HORIZONTAL_ALIGNMENT)
				return false;
			
			
			if (GraphConstants.getForeground(attributes).getRGB() != Constants.CD_TEXT_COLOR.getRGB())
				return false;
			
			
			// Nyní si pro nastavení barvy pozadí musím načíst logickou proměnnou, dle které poznám, zda se má obarvit
			// pozadí buňky  jednou barvou nebo dvěmí:
			if (Constants.CD_ONE_BG_COLOR_BOOLEAN) {
				final Color color = Constants.CD_ONE_BG_COLOR;
				
				if (GraphConstants.getBackground(attributes).getRGB() != color.getRGB())
					return false;
				
				
				if (GraphConstants.getGradientColor(attributes).getRGB() != color.getRGB())
					return false;
			}
			
			else {
				if (GraphConstants.getBackground(attributes).getRGB() != Constants.CD_CLASS_BACKGROUND_COLOR.getRGB())
					return false;
				
				
				if (GraphConstants.getGradientColor(attributes).getRGB() != Constants.CD_GRADIENT_COLOR.getRGB())
					return false;
			}

			return GraphConstants.isOpaque(attributes) == !Constants.CD_OPAQUE_CLASS_CELL;
		}
	}










	@Override
	public boolean isCommentCell(final DefaultGraphCell cell) {
		final Map<?, ?> attributes = cell.getAttributes();
		
		if (classDiagramProperties != null) {
			// Font pro název třídy:
			final int fontSize = Integer.parseInt(classDiagramProperties.getProperty("CommentFontSize", Integer.toString(Constants.CD_FONT_COMMENT.getSize())));
			final int fontStyle = Integer.parseInt(classDiagramProperties.getProperty("CommentFontType", Integer.toString(Constants.CD_FONT_COMMENT.getStyle())));
			
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.CD_FONT_COMMENT.getName(), fontStyle, fontSize)))
				return false;
			
			
			// Zarovnání pro písmo a obrázky:
			final int verticalAlignment = Integer.parseInt(classDiagramProperties.getProperty("CommentVerticalAlignment", String.valueOf(Constants.CD_COM_VERTICAL_ALIGNMENT)));
			if (GraphConstants.getVerticalAlignment(attributes) != verticalAlignment)
				return false;
			
			
			final int horizontalAlignment = Integer.parseInt(classDiagramProperties.getProperty("CommentHorizontalAlignment", String.valueOf(Constants.CD_COM_HORIZONTAL_ALIGNMENT)));
			if (GraphConstants.getHorizontalAlignment(attributes) != horizontalAlignment)
				return false;
			
			
			// Barva písma
			final String textColor = classDiagramProperties.getProperty("CommentForegroundColor", Integer.toString(Constants.CD_COM_FOREGROUND_COLOR.getRGB()));
			if (GraphConstants.getForeground(attributes).getRGB() != Integer.parseInt(textColor))
				return false;
			
			
			// načtu si logickouk proměnnou, dle které zjistím kolik barev se má nastavit pro pozadí buňky:
			final boolean oneBgColor = Boolean.parseBoolean(classDiagramProperties.getProperty("CommentOneBgColorBoolean", String.valueOf(Constants.CD_COM_ONE_BG_COLOR_BOOLEAN)));
			
			if (oneBgColor) {
				final String bgColor = classDiagramProperties.getProperty("CommentOneBackgroundColor", Integer.toString(Constants.CD_COM_ONE_BG_COLOR.getRGB()));
				
				// Barva pozadí - levý horní roh:
				if (GraphConstants.getBackground(attributes).getRGB() != Integer.parseInt(bgColor))
					return false;
				
				
				// Barva pozadí - pravý dolní roh:
				if (GraphConstants.getGradientColor(attributes).getRGB() != Integer.parseInt(bgColor))
					return false;
				
			}
			
			else {
				// Barva pozadí - levý horní roh:
				final String backgroundColor = classDiagramProperties.getProperty("CommentBackgroundColor", Integer.toString(Constants.CD_COM_BACKGROUND_COLOR.getRGB()));
				if (GraphConstants.getBackground(attributes).getRGB() != Integer.parseInt(backgroundColor))
					return false;
				
				
				// Barva pozadí - pravý dolní roh:
				final String gradientColor = classDiagramProperties.getProperty("CommentGradientColor", Integer.toString(Constants.CD_COM_GRADIENT_COLOR.getRGB()));
				if (GraphConstants.getGradientColor(attributes).getRGB() != Integer.parseInt(gradientColor))
					return false;
			}
			
			
			

			
			// Ohraničení:
			// Note: - Ohraničení jsem v kódu pevně definovat a nelze změnit, takže ho ani nemusím testovat, jedná se pouze o barvy pro 
			// levý horní a pravý dolní roh, ale ty jsou testovány výše.
			
			
			/*
			 * Note:
			 * Otočil jsem logickou hodnotu pro průhlednost, knihovna pro grafy chce jako
			 * hodnotu pro to, aby byla buňka průhledná hodnotu false, ale do souboru
			 * zapisuji pro průhlednou buňku hodnotu true, aby to bylo pro uživatele
			 * "přehlednější" a nemusel si otáčet logickou hodnotu, proto tuto hodnotu
			 * raději "otočím" zde v kódu, takže když v následující podmínce testuji, jestli
			 * je buňka průhledná, tak musím načtenou hodnotu ze souboru "otočit", aby se
			 * mohla shodovat s hodnotou dle knihovny.
			 */
			
			// Nastavení neprůhlednésti buňky - aby nebyla průhledná
			final boolean opaque = !Boolean.parseBoolean(classDiagramProperties.getProperty("CommentCellOpaque", String.valueOf(Constants.CD_OPAQUE_COMMENT_CELL)));

			return GraphConstants.isOpaque(attributes) == opaque;
		}
		
		
		else {			
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.CD_FONT_COMMENT.getName(),
					Constants.CD_FONT_COMMENT.getStyle(), Constants.CD_FONT_COMMENT.getSize())))
				return false;
			
			if (GraphConstants.getVerticalAlignment(attributes) != Constants.CD_COM_VERTICAL_ALIGNMENT)
				return false;
			
			
			if (GraphConstants.getHorizontalAlignment(attributes) != Constants.CD_COM_HORIZONTAL_ALIGNMENT)
				return false;
			
			
			if (GraphConstants.getForeground(attributes).getRGB() != Constants.CD_COM_FOREGROUND_COLOR.getRGB())
				return false;
			
			
			if (Constants.CD_COM_ONE_BG_COLOR_BOOLEAN) {
				final Color color = Constants.CD_COM_ONE_BG_COLOR;
				if (GraphConstants.getBackground(attributes).getRGB() != color.getRGB())
					return false;
				
				
				if (GraphConstants.getGradientColor(attributes).getRGB() != color.getRGB())
					return false;
			}
			
			else {
				if (GraphConstants.getBackground(attributes).getRGB() != Constants.CD_COM_BACKGROUND_COLOR.getRGB())
					return false;
				
				
				if (GraphConstants.getGradientColor(attributes).getRGB() != Constants.CD_COM_GRADIENT_COLOR.getRGB())
					return false;
			}

			return GraphConstants.isOpaque(attributes) == !Constants.CD_OPAQUE_COMMENT_CELL;
		}
	}










	@Override
	public boolean isCommentEdge(final DefaultEdge edge) {
		final Map<?, ?> attributes = edge.getAttributes();
		
		if (classDiagramProperties != null) {
			final String lineColor = classDiagramProperties.getProperty("CommentEdgeColor", Integer.toString(Constants.CD_COM_EDGE_COLOR.getRGB()));
			
			if (GraphConstants.getLineColor(attributes).getRGB() != Integer.parseInt(lineColor))
				return false;
			
			
			final boolean labelsAlongEdge = Boolean.parseBoolean(classDiagramProperties.getProperty("CommentEdgeLabelAlongEdge", String.valueOf(Constants.CD_COM_LABEL_ALONG_EDGE)));
			if (GraphConstants.isLabelAlongEdge(attributes) != labelsAlongEdge)
				return false;
			
			final int lineStyle = Integer.parseInt(classDiagramProperties.getProperty("CommentEdgeLineStyle", Integer.toString(Constants.CD_COM_EDGE_LINE_STYLE)));
			if (GraphConstants.getLineStyle(attributes) != lineStyle)
				return false;
			
			final int lineEnd = Integer.parseInt(classDiagramProperties.getProperty("CommentEdgeLineEnd", Integer.toString(Constants.CD_COM_LINE_END)));
			if (GraphConstants.getLineEnd(attributes) != lineEnd)
				return false;
			
			final int lineBegin = Integer.parseInt(classDiagramProperties.getProperty("CommentEdgeLineBegin", Integer.toString(Constants.CD_COM_LINE_BEGIN)));
			if (GraphConstants.getLineBegin(attributes) != lineBegin)
				return false;
			
			final float lineWidth = Float.parseFloat(classDiagramProperties.getProperty("CommentEdgeLineWidth", String.valueOf(Constants.CD_COM_LINE_WIDTH)));
			if (GraphConstants.getLineWidth(attributes) != lineWidth)
				return false;
			
			final boolean endFill = Boolean.parseBoolean(classDiagramProperties.getProperty("CommentEdgeEndFill", String.valueOf(Constants.CD_COM_LINE_END_FILL)));
			if (GraphConstants.isEndFill(attributes) != endFill)
				return false;
			
			final boolean beginFill = Boolean.parseBoolean(classDiagramProperties.getProperty("CommentEdgeBeginFill", String.valueOf(Constants.CD_COM_LINE_BEGIN_FILL)));
			if (GraphConstants.isBeginFill(attributes) != beginFill)
				return false;
			
			// Barva textu:
			final String fontColor = classDiagramProperties.getProperty("CommentEdgeTextColor", Integer.toString(Constants.CD_COM_EDGE_FONT_COLOR.getRGB()));
			if (GraphConstants.getForeground(attributes).getRGB() != Integer.parseInt(fontColor))
				return false;
			
			// Font poznámky na hraně:
			final int fontSize = Integer.parseInt(classDiagramProperties.getProperty("CommentEdgeLineFontSize", Integer.toString(Constants.CD_COM_EDGE_FONT.getSize())));
			final int fontStyle = Integer.parseInt(classDiagramProperties.getProperty("CommentEdgeLineFontStyle", Integer.toString(Constants.CD_COM_EDGE_FONT.getStyle())));
			
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.CD_COM_EDGE_FONT.getName(), fontStyle, fontSize)))
				return false;
		}
		
		
		else {			
			if (GraphConstants.getLineColor(attributes).getRGB() != Constants.CD_COM_EDGE_COLOR.getRGB())
				return false;
			
			if (GraphConstants.isLabelAlongEdge(attributes) != Constants.CD_COM_LABEL_ALONG_EDGE)
				return false;
			
			if (GraphConstants.getLineStyle(attributes) != Constants.CD_COM_EDGE_LINE_STYLE)
				return false;
			
			if (GraphConstants.getLineEnd(attributes) != Constants.CD_COM_LINE_END)
				return false;
			
			if (GraphConstants.getLineBegin(attributes) != Constants.CD_COM_LINE_BEGIN)
				return false;
			
			if (GraphConstants.getLineWidth(attributes) != Constants.CD_COM_LINE_WIDTH)
				return false;
			
			if (GraphConstants.isEndFill(attributes) != Constants.CD_COM_LINE_END_FILL)
				return false;
			
			if (GraphConstants.isBeginFill(attributes) != Constants.CD_COM_LINE_BEGIN_FILL)
				return false;		
			
			// Barva textu:
			if (GraphConstants.getForeground(attributes).getRGB() != Constants.CD_COM_EDGE_FONT_COLOR.getRGB())
				return false;
			
			// Text na hraně:
			if (!GraphConstants.getFont(attributes).equals(new Font(Constants.CD_COM_EDGE_FONT.getName(),
					Constants.CD_COM_EDGE_FONT.getStyle(), Constants.CD_COM_EDGE_FONT.getSize())))
				return false;
		}
		
		
		
		// Kromě hrany reprezentující implementaci rozhraní nemá žádný jiná hrana čerchovaný styl čáry - bude null:
		final float[] pattern = GraphConstants.getDashPattern(attributes);


		/*
		 * Sem se dostanu, pokud všechnyhodnoty odpovídají, tudíž se jedná o daný typ hrany, resp. v tomto případně
		 * hranu pro komentář - vrátí se true, jinak false.
		 */
		return Arrays.equals(pattern, null);
	}


	@Override
	public void editValueInProperties(final String key, final String value) {
		classDiagramProperties.setProperty(key, value);
	}


	@Override
	public Properties getProperties() {
		return classDiagramProperties;
	}


	@Override
	public void reloadProperties() {
		classDiagramProperties = App.READ_FILE.getClassDiagramProperties();
	}
}