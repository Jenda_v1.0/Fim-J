package cz.uhk.fim.fimj.class_diagram;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

import cz.uhk.fim.fimj.file.*;
import cz.uhk.fim.fimj.reflection_support.ParameterToText;
import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;
import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.swing_worker.SwingWorkerInterface;

/**
 * Tato třída slouží jako vlákno, které se spustí například po zavření dialogu editor zdrojového kódu, nebo při otevření
 * projektu apod. Vlákno slouží pro otestování vztahů mezi třídami v diagramu tříd.
 * <p>
 * <p>
 * Toto vlákno má za úkol zkompilovat zavřenou třídu (případně všechny - dle vztahů) a zjistit, zda uživatel ručně
 * doplnil nějaký vztah s jinou třídou, například, zda do zavřené třídy doplnil děičnost z jiné třídy z diagramu tříd,
 * zda doplnil nějakou asociace - resp. referenci na jinou třídu: viditelnost DifferentClassInClassDiagram someName;
 * apod. (v případě zavření editoru kodu)
 * <p>
 * Dle potřeby se daná hrana coby vazba mezi dvěma třídami (jedna z nich bude ta, jejiž kód byl zavřen) může smazat,
 * nebo vytvořit nová s nějakým vztahem na ostatní třídy (asociace, dědičnost, implementace, agregace), případně obojí,
 * pokud se například přepíše dědičnost z jendé třídy na jinou, apod. s implementací a agregací, ...
 * <p>
 * <p>
 * <p>
 * Postup: Princip je takový, že se nejprve vymažou veškeré hrany mezi třídami v diagramu tříd s vyýjimkou těch hran,
 * které reprezentují spojeni třídy s komentářem. Pak se projdou všechny třídy v diagramu tříd a pro každou se zjistí,
 * jaké vztahy má na ostatní třídy - dědičnost, implementace rozhraní, agregace 1 : 1 (přepsáno na asymetrická
 * asociace), (symetrická) asociace a agregace 2 : N. Každá "klasická" proměnné (ne pole nebo list) se při každé iteraci
 * vloží do kolekce, aby se zjistilo, zda se příslušný typ proměnné již testoval nebo ne.
 * <p>
 * Pro každou proměnnou se zjistí, zda se jedná o proměnnou typu jiné třídy v diagramu tříd, pokud ano, pak se zjistí,
 * kolik má ta třída proměnných typu té právě testované (iterované) třídy a dle toho se přidá buď jedna hrana s
 * příslušnou multiplicitou nebo příslušný počet hran typu agregace1 : 1 (asymetrická asociace).
 * <p>
 * <p>
 * <p>
 * Note: Toto vlákno je napsané zvlášť od toho, které doplňuje proměnné do dialogu, protože to vlákno se zavolá pouze po
 * sisknutí nějaké kláesy, ale když uživatel například dialog zavře nebo před jeho zavřením nestiskne nějaké testované
 * tlačítko, tak se nic nezjistí. Proto je třeba tyto vztahy otestovat až po uzavření dialogu - pokud je třída validní -
 * tj. bez chyby ve zdrojovém kódu, jinak se nepodaří třída zkompilovat a nelze nic zjistit
 * <p>
 * <p>
 * <p>
 * Note: Pro otestovani jednotlicych vztahu by bylo dobre jeste testovat HashMapu, pripadne jeji jednotlive "potomky"
 * TreeMap, dále HashSet, TreeSet, apod. ale tato aplikace ma slouzit hlavne pro zacatecniky, tak se touto casti nebudu
 * omentalne zdrzovat a odlozim to na dalsi verzi - pro testovani ostatnich kolekci. Navíc v případě implementace pro
 * testování mapy by to bylo celkem časové náročné, protože bych musel zvláště testovat její oba typy a jejich hodnoty.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class CheckRelationShipsBetweenClassesThread extends AbstractForRelationShipThreads
		implements SwingWorkerInterface {


	
	
	
	/**
	 * Mapa, kteerá slouží pro "dočasné uložení" již testovaných tříd a jejich
	 * proměnných.
	 * 
	 * Jedná se o to, že když se prochází "klasické" proměnné ve třídách, tak se
	 * zjišťují vztahy typu agregace 1 : 1 (dále jako asymetrická asociace) a
	 * (symetrická) asociace.
	 * 
	 * Funguje to tak, že se vždy zjistí ve třídě nějaká proměnná a když je tato
	 * proměnná typu jiné třídy v diagramu tříd tak se zjistí, kolik proměnných má
	 * ta aktuálně iterovaná třída typu to druhé třídy a naopak, kolik proměnných má
	 * ta cílové třída typu té právě testovné (iterované), dle tohoto se vypočítá
	 * kardinalita (multiplicita) na hranách.
	 * 
	 * Ale musím vědět, v jakých třídách se ty proměnné již testovaly a v jakých ne,
	 * protože kdybych to nevěděl, pak bych nemohl ty typy proměnných testovat
	 * vícejak jednou, protože by mohly vznikat duplicity, například pri sym
	 * asociace, kdy by e iterovala první třída, zjistí se ta sym asociace, pak se
	 * bude iterovat druhá třída a zjistí se také sym asociace, to je špatně. Proto
	 * díky tomu, že budu mít mapu testovaných tříd a jejich proměnných, tak budu
	 * vědět, v jaké třídě jsem již testoval ty proměnné a v jaké ne.
	 * 
	 * 
	 * 
	 * Important: V jedné třídě (key v mapě) mohu testovat pouze jednou jeden typ
	 * proměnné.
	 * 
	 * Vždy v té třídě otestuji proměnnou a zjistím si počet těch promnných
	 * příslušného typu v dané třídě a to samé v té cílové třídě (typ proměnné),
	 * takže tu proměnnou v jedné třídě mohu testovat pouze jednou. Jinak by
	 * vznikaly duplicity.
	 * 
	 * 
	 * 
	 * 
	 * Příklad procházení s mapou:
	 * 
	 * Třídy: Osoba, Byt a Adresa.
	 * 
	 * Vztahy:
	 * 
	 * Osoba -> Byt
	 * Byt -> Adresa
	 * Adresa -> Osoba
	 * 
	 * 
	 * První se prochází třeba Osoba,
	 * 
	 * Takže do mapy se postupně přidjí hodnoty: Osoba -> (Byt). Podmínkami tato
	 * hodnota projde, protože Byt není již testovaná třída, a je v té třídě Osoba
	 * jen jednou, takže se ještě netestoval.
	 * 
	 * Jako druhá třída se testuje třeba Byt. Pak se u Bytu zjistí, že má proměnnou
	 * Adresa, Adresa se ještě netestovala, takže není v mapě jako klíč a ani není v
	 * Bytu jako hodnota, takže se vše také vytvoří v pořádku a do mapy k Bytu se
	 * přidá Adresa: Byt -> {Adresa}.
	 * 
	 * Na konec se testuje Adresa. Ta má atribut Osoba, u té se zjistí, že už je v
	 * mapě jako klíč, takže se musí využít část "nebo" v první části podmínky a
	 * zjistí se, že ve třídě Osoba není atribut Adresa, takže první část podmínky
	 * se splní a pak se zjistí, že v té třídě Adresa se Osoba testuje poprvne, tak
	 * se přidá do mapy, vytvoří se vztah z Adresy na Osobu a je vše v pořádku:
	 * Adresa -> {Osoba.
	 * 
	 * 
	 * 
	 * Celkově v pořadí pro vkládání do mapy: Osoba -> {Byt}
	 * Byt -> {Adresa}
	 * Adresa -> {Osoba}
	 */
	private final Map<Class<?>, List<Class<?>>> map;
	
	
	
	
	
	
	
	/**
	 * Proměnná, do které se uloží reference na diagram tříd, ze kterého si při
	 * testování vytáhnu označenou buňku coby třídu, jejíž kod byl načten v editoru
	 * zdrojového kódu, dále se zjistí všechny objekty v diagramu tříd, aby bylo
	 * ožné mezi nimi vytvořit vztahy, případně nějaké odebrat, apod.
	 */
	private final GraphClass classDiagram;
	
	
	
	
	private final OutputEditorInterface outputEditor;
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy - vlákna
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd, ze kterého si při tetování vytáhnu
	 *            potřebné prvky, jako jsou označené všechny prvky, označený prvek,
	 *            ...
	 * 
	 * @param outputEditor
	 *            - reference na instnaci editoru výstupů, která se předá dále do
	 *            kompilace třídy, pro případně výpisy, apod.
	 * 
	 * @param showRelationShipsToClassItself
	 *            - logická hodnota, která značí, zda se mají v diagramu tříd nebo
	 *            instancí zobrazovat vztahy třídy nebo instance na sebe sama.
	 * 
	 * @param showAssociationThroughAggregation
	 *            - logická proměnná, která značí, zda se má vztah typu asociace
	 *            znázornit normálně, tj. jedna hrana s šipkami na obou koncích
	 *            (hodnota false). Nebo zda se má vztah typu asociace znázornit jako
	 *            dvě agregace (hodnota true).
	 */
	public CheckRelationShipsBetweenClassesThread(final GraphClass classDiagram,
			final OutputEditorInterface outputEditor, final boolean showRelationShipsToClassItself,
			final boolean showAssociationThroughAggregation) {

		super(showRelationShipsToClassItself, showAssociationThroughAggregation);

		map = new HashMap<>();
		
		this.classDiagram = classDiagram;
		this.outputEditor = outputEditor;
	}
	
	
	
	
	
	
	
	
	@Override
	public void run() {
		// Postup:
		
		/*
		 * Nejprve si zkompiluji danou třídu a pokud se NEpodaří načíst přeložený soubor
		 * .class ,tak v podstatě něci co dělat, třída obsahuje chybu, apod. vlákno
		 * skončí.
		 * 
		 * Jinak
		 * 
		 * Zde se podařilo načíst danou třídu, tak pokračuji tak, že vymažu veškeré
		 * hrany s výjimkou hran reprezentující spojení komentáře s třídou, projde
		 * veškeré třídy a z každé si vytáhnu třídu, ze které dědí - pouze jednu první a
		 * otestuji, zda se tato třída nachází v diagramu tříd a pokud ano, pak se mezi
		 * těmi třídami v diagramu tříd vytvoří. vztahy, který reprezentuje dědičnost.
		 * 
		 * Dále se zjistí, zda třída implementuje nějaké rozhraní a toto rozhraní je v
		 * diagramu tříd pak opět vytvořím příslušné hrany.
		 * 
		 * Dále
		 * 
		 * Otestuji vztah agregaci, tj. akorát otestuji všechny položky, a pokud je
		 * nějaká položky typu jiné třídy z diagramu tříd, tak se mezi nimi vytvoří
		 * vztah, reprezentující daný typ agregace, pokud se jedná o typ 1 : 1 tak je to
		 * klasický zápis atributu: viditelno DataType someName; ale pokud je to typ 1 :
		 * N, tak se musi jednat o list typu jedné z tříd z diagramu tříd: například
		 * List<ClassFromClassDiagram> someName, nebo pole;
		 * 
		 * Ze zjištěného počtu vztahů agragace se vypočítá i kolik vhtahů typu asociace
		 * má existovat, výpočty jsou popsány u konkrétních částí, kde se to vypočítává.
		 */
					
		
		
		/*
		 * Objekt s texty pro aplikaci ve zvoleném jazyce.
		 */
		final Properties languageProperties = App.READ_FILE.getSelectLanguage();
		
		
		final String pathToSrc = App.READ_FILE.getPathToSrc();

		if (pathToSrc != null) {
			new CompileClass(languageProperties);

			/*
			 * Zkompilují se třídy v diagramu tříd a výsledek o kompilaci se uloží do
			 * proměnné result, kde bude true, pokud se třídy zkompilovaly bez chyby, pak se
			 * mohou otestovat i vztahy mezi třídami v diagramu tříd, jinak false.
			 */
			final boolean result = CompileClass.compileClass(outputEditor, App.READ_FILE.getFileFromCells(pathToSrc,
					GraphClass.getCellsList(), outputEditor, languageProperties), false, true);

			/*
			 * Pokud třídy obsahují chyby, mohu skončit, nemá smysl testovat vztahy, protože
			 * by nebyly "platné / aktuální", vždy by se načetly pouze poslední validní
			 * soubory .class tříd z diagramu tříd a pokud aktuálně nějaká třída obsahuje
			 * chybu, tak ty vztahy nebudou aktuální, nebo se nenačtouvůbec.
			 */
			if (!result)
				return;
		}
		

		
		
		// Zde se mají otestovat všechny třídy v diagramu tříd aby se mezi nimi
		// vytvořily vztahy - pokud nejsou

		// Projdu všechny objekty v diagramu tříd a otestuji, zda se jedná o
		// buňku, která reprezentuje třídu, pokud ano,
		// tak tuto buňku předám do metody, která otestuje vztahy mezi ní a
		// všemi ostatními třídami v diagramu tříd:

		// Zde musím použít tento "klasický" cyklu for, protože kdybych použil v
		// podstatě kterýkoliv jiný
		// tak by mohla vyskočit java.util.ConcurrentModificationException -
		// protože bych si přepisoval kolekci
		// zrovna když jí procházím, tento cyklus for si je při každém cyklu
		// nenačítá znovu

		
		
		
		
		/*
		 * Jelikož použitá knihovna JGraph má problém, když se s ní pracuje ve vláknech,
		 * musí běžět ve swingu, pak musím veškeré volání pro manipulaci s knihovnou
		 * JGraph obalit ještě do SwingUtilities.invokeAndWait, díky čemuž poběží sice v
		 * jiném vlákně, ale pořád ve swingu, navíc díky metodě invokeAndWait se počká,
		 * než se příslušné vztahy znázorní a až pak se bude pokračovat původní
		 * Swingovou aplikací - hlavní aplikace Fim-J.
		 * 
		 * Note:
		 * dle návodu by bylo vhodné ještě tento blok sw SwingUtilitis vložit například
		 * do vlákna, ale jelikož již toto všech se děde ve vlákně, tak to vynechám a
		 * pouze dám do bloku try - catch.
		 */
		try {
			SwingUtilities.invokeAndWait(() -> {
                /*
                 * Pro rychlejší testování vztahů vymažu všechny aktuální vztahy mezi třídami s
                 * výjimkou hran pro komentáře.
                 */
                classDiagram.deleteAllEdgesExceptCommentEdges();

                /*
                 * Tímto cyklem projdu veškeré objekty v diagramu tříd a vždy testuji, zda se
                 * jedná o třídu, pokud ano, tak pro každou třídu zavolám metodu, která již
                 * testouje a případně vytvoří či smaže nebo ponechá znázorněné vztahy mezi
                 * třídami v diagramu tříd. Resp. vždy mezi právě testovanou / iterovanou třídou
                 * a všemi ostatními v diagramu tříd.
                 */
                for (int i = 0; i < GraphClass.getCellsList().size(); i++) {
                    final DefaultGraphCell cell = GraphClass.getCellsList().get(i);

                    if (!(cell instanceof DefaultEdge) && GraphClass.KIND_OF_EDGE.isClassCell(cell))
                        checkRelationShipsAroundCell(cell);
                }
            });

		} catch (InvocationTargetException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informaci o chybě:
				logger.log(Level.INFO,
						"Zachycena výjimka při pokusu o zavolání SwingUtilities.invokeAndWait pro volání metod pro " +
								"znázornění vztahů mezi třídami v diagramu tříd. "
								+ "Tato chyba může nastat například v případě, že je vyhozena výjimka pri pokusu o " +
								"spuštění programu doRun.");

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();

        } catch (InterruptedException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informace i chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při pokusu o zavolání SwingUtilities.invokeAndWait pro volání metod pro " +
                                "znázornění vztahů mezi třídami v diagramu tříd. "
                                + "Tato chyba může nastat například v případě, že pokud budeme přerušeni při čekaní " +
                                "na událost dispečování vlákna k dokončení spuštění příkazu doRun.run ().");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
		
		
		
		
			
		
		// NÁSLEDUJÍCÍ KOD FUNGOVAL, AKORÁT DOCHÁZELO K TÉ CHYBĚ V KNIHOVNĚ, přepsáno výše na SwingUtilities.
//		for (int i = 0; i < GraphClass.getCellsList().size(); i++) {
//			final DefaultGraphCell cell = GraphClass.getCellsList().get(i);
//
//			if (!(cell instanceof DefaultEdge) && kindOfEdge.isClassCell(cell))
//				checkRelationShipsAroundCell(cell);
//		}
	}



	
	
	
	
	
	
	/**
	 * Metoda, která provede postup, který je uveden v mětodě run - zakomentován.
	 * Metoda tento postup provede vždy pro jednu buňku coby nějakou třídu - vždy
	 * otestuje vztahy mezi ní a ostatními třídami v diagramu tříd.
	 * 
	 * @param selectedCell
	 *            - buňka, která reprezentuje třídu v diagramu tříd, a budou se u
	 *            této buňky testovat všechny vztahy k ostatním třídám z
	 *            diagramu tříd.
	 */
	private void checkRelationShipsAroundCell(final DefaultGraphCell selectedCell) {
		// Do následující proměnné vložím text z označené buňky v diagramu tříd, a testovat, zda není null, apod.
		// již nemusím, protože je ta označená třída z diagramu tříd.
		final String cellTextWithDot = selectedCell.toString();
				
		// Zkusím načíst tuto třídu:
		final Class<?> clazz = App.READ_FILE.loadCompiledClass(cellTextWithDot, outputEditor);

		if (clazz != null) {
			// 1) Testování dědičnosti:

			/*
			 * Načtu si třídu, ze které právě testovaná třída (selectedCell) dědí a zda se
			 * nachází v diagramu tříd, prířpadně mezi nimi znázorním vztah typu dědičnost.
			 */
			final Class<?> inheritClass = clazz.getSuperclass();

			// Note: Zde nemsuím testovat zda se typy tříd shodují, třída nemůže dědit z té
			// samé třídy.

			if (inheritClass != null) {
				final String packageName;

				if (inheritClass.getPackage() == null)
					packageName = "";
				else
					packageName = inheritClass.getPackage().getName();

				
				// Název třídy, ze která ta zavřená (testovaná) dědí, sestavím její název i s
				// balíčky a otestuji, zda je v diagramu tříd,
				final String inheritClassNameWithPackages = packageName + "." + inheritClass.getSimpleName();

				
				/*
				 * V tomto případě byly již všechny vztahy mezi třídami vymazány, tak stačí
				 * pouze přidat ten jeden vztah (pokud se předek třídy nachází v diagramu
				 * tříd.):
				 */
				final DefaultGraphCell desCell = GraphClass.getClassCellWithText(inheritClassNameWithPackages);

				if (desCell != null)
					classDiagram.addExtendsEdge(selectedCell, desCell);
			}
					
					
					
					
					
					
					
					
					
			// 2) Testování implementace rozhraní:

			// Načtu si všechna implementovaná rozhraní
			// zjistím si jejich název ve složení: balíčky . název třídy
			// zjisím si ji načíst v diagramu tříd
			// pokud existuje:

			// otestuji, zda jsou příslušné třídy spojeny vztahem
			// pokud nejsou, tak ho vytvořím, pokud jsou, tak nic nedělám - je to v pořádku

			// pokud v diagramu tříd dané rozhranní není coby třída v diagramu tříd,
			final Class<?>[] interfacesArray = clazz.getInterfaces();

			/*
			 * Vyvořím si list a předám mu názvy tříd, resp. rozhraních i s balíčky, ve
			 * kterém se nacházejí:
			 * 
			 * Toto není povinné, mohl bych tuto kolekci přeskočit a rovnou vnásledujícím
			 * cyklu testovat existenci těch rozhraní, ale pro přehlednost mi to přijde
			 * lepší.
			 */
			final List<String> classNamesWithPackagesList = new ArrayList<>();

			/*
			 * Note:
			 * 
			 * Zde nemusím testovat, zda se jedná o rozhraní typu třídy sebe sama - třída
			 * může implementovat pouze rozhraní a rozhraní nemůže implementovat jiná
			 * rozhraí a třída je zkompilovaná, takže ani nemůže být chyba:
			 * 
			 * Dále je dobré poznamenat, že bych ani nemusel dávat balíčky s názvy tříd do
			 * kolekce, ale rovnou bych v tom cyklu mohl testovat to rozhraní, ale takto mi
			 * to přijde přehlednější.
			 */
			for (final Class<?> c : interfacesArray) {
				final String interfacePackageName;

				if (c.getPackage() == null)
					interfacePackageName = "";
				else
					interfacePackageName = c.getPackage().getName();

				classNamesWithPackagesList.add(interfacePackageName + "." + c.getSimpleName());
			}

			
			classNamesWithPackagesList.forEach(i -> {
				/*
				 * V tomto případě byly již všechny vztahy mezi třídami vymazány, tak stačí
				 * pouze přidat ten jeden vztah (pokud se rozhraní nachází v diagramu tříd.):
				 */
				final DefaultGraphCell desCell = GraphClass.getClassCellWithText(i);

				if (desCell != null)
					classDiagram.addImplementsEdge(selectedCell, desCell);
			});
					
					
					
					
					
					
					
					
					
					
				
			
			
					
					
					
					
			// 3) Testování agregace 1 : N:
			// a agregace 1 : 1 (asymetrická asociace)

			// Postup:
			// -Musím si načíst všechny proměnné dané třídy
			// - Projdu pole všech proměnných a pro každé zjistím, zda se v diagramu tříd
			// vyskytuje
			// buňka, reprezentující nějakou třídu, pokud ano, jedná se o vztah agregace 1 :
			// 1
			// a text teto tridy (včetně balíčků) vložím do kolekce typu text, kterou dále
			// předám metodě
			// pro otestování hran v diagramu tříd:
			final Field[] fields = clazz.getDeclaredFields();

			// List, do kterého budu vkládat texty tříd pro ty agregace 1 : 1

			// Hrany, které by měly existovat mezi příslušnými třídami - touto testovanou a
			// osatními
			final List<InfoForNewEdge> newEdgesList = new ArrayList<>();

			// List, do kterého budu vkládat texty tříd pro ty agregace 1 : N
			final List<String> fieldsTextListForAgr_1_N = new ArrayList<>();

			
			
			
			
			
			
			
			for (final Field f : fields) {
				// Otestuji, zda se jedná o kolekci:
				if (ReflectionHelper.isDataTypeOfList(f.getType())) {
					// Zde se jedná o kolekci, tak musím zjistit i její typ
					final DataTypeOfList dataTypeOfList = ReflectionHelper.getDataTypeOfList(f);

					/*
					 * V případě, že aktuálně iterovaný list nemá parametr nebo se jedná o generický
					 * datový typ, pak mohu pokračovat další iterecí, protože se nejedná o nějaký
					 * vztah na jinou třídu v diagramu tříd.
					 */
					if (dataTypeOfList.getDataTypeOfListEnum() != DataTypeOfListEnum.SPECIFIC_DATA_TYPE)
						continue;

					// Otestuji, zda není třída typem této třídy - proměnna třídy sebe sama:
					// PŮVODNÍ:
					// if (!dataTypeOfList.toString().equals(clazz.toString())) {
					// Opraveno do nastavení:
					/*
					 * Je to to, abych nějak nastavil, zda se mají zobrazovat vztahy tříd na sebe
					 * sama. Takže pokud má třída vztah na sebe sama a ty vztahy se v diagramu tříd
					 * nemají zobrazit, pak tu proměnnou vynechám, v ostaních případech vztah dané
					 * proměnné přidám k testování pro zobrazení v diagramu tříd.
					 * 
					 * Pokud je proměnna (atribut) typu své vlastní třídy, pak zjistím, zda se má
					 * tento vztah zobrazit v diagramu, pokud ano, pak jej zobrazím, pokud ne, pak
					 * pokračuji další iterací.
					 */
					// if (dataTypeOfList.toString().equals(clazz.toString()) &&
					// !showRelationShipToClassItself)
					if (!showRelationShipsToClassItself || clazz.isEnum())
						continue;

					// načtu si text, který by mela buňka obsahovat:
					final String packagesOfClass = getTextOfClassCell(dataTypeOfList.getDataType());

					// pokud byl načten (není null), tak ho přidám do kolece pro pozdější testování:
					if (packagesOfClass != null)
						fieldsTextListForAgr_1_N.add(packagesOfClass);
				}
						
						
						
						
						
						
				else if (f.getType().isArray()) {
					// Načtu si datový typ pole, abych mohl otestovat, zda se jedná o třídu z
					// diagramu tříd:

					// Note:
					// Zde musím ještě otestovat, o kolika dimenzionální pole se jedná, pokud více
					// než jedna,
					// tak toho pole musím celé projít a vzít si datový typ pole až toho posledního
					// - viz metoda:

					final Class<?> dataTypeOfArray = ReflectionHelper.getDataTypeOfArray(f.getType());

					// Otestuji, zda se nejedná o proměnnou typu své vlastní třídy - sebe sama třídy
					// if (!dataTypeOfArray.toString().equals(clazz.toString())) {
					/*
					 * Je to to, abych nějak nastavil, zda se mají zobrazovat vztahy tříd na sebe
					 * sama. Takže pokud má třída vztah na sebe sama a ty vztahy se v diagramu tříd
					 * nemají zobrazit, pak tu proměnnou vynechám, v ostaních případech vztah dané
					 * proměnné přidám k testování pro zobrazení v diagramu tříd.
					 * 
					 * Pokud je proměnna (atribut) typu své vlastní třídy, pak zjistím, zda se má
					 * tento vztah zobrazit v diagramu, pokud ano, pak jej zobrazím, pokud ne, pak
					 * pokračuji další iterací.
					 */
					if ((dataTypeOfArray.toString().equals(clazz.toString()) && !showRelationShipsToClassItself)
							|| clazz.isEnum())
						continue;

					// načtu si text, který by mela buňka obsahovat:
					final String packagesOfClass = getTextOfClassCell(dataTypeOfArray);

					// pokud byl načten (není null), tak ho přidám do koelce pro pozdější testování:
					if (packagesOfClass != null)
						fieldsTextListForAgr_1_N.add(packagesOfClass);
				}
						
						
						
						
				
				
				/*
				 * Postup:
				 * 
				 * Zde zbývá pouze "klasická" proměnná nebo netestovaný typ proměnné (mapa
				 * apod.) Otestuji, zda je to promenná typu jine tridy v diagramu tříd, pokud
				 * ano, tak si načtu její třídu - resp. soubor .class příslušné třídy, zjistím
				 * si počet proměnných, které jsou typu této třídy, dále si zjistím počet
				 * proměnných v této třídě, které jsou typu té druhé třídy (výše)
				 * 
				 * vypočet - toho výše:
				 * 
				 * třída A = clazz - tato právě testovaná třída, hodnota A = počet proměnných
				 * typu B
				 * 
				 * třída B = načtená třída coby typ právě testované proměnné, hodnota B = počet
				 * proměnných v dané třídě typu třídy A
				 * 
				 * Výpočet, pokud se jedná o zobrazování vztahu (symetrické) asociace pomocí
				 * agregace 1 : 1 (asymetrické asiciace):
				 * 
				 * 
				 * result = A - B
				 * 
				 * if (result <= 0) pokud jsou sym asociace jako asym asociace, pak přidím z A
				 * do B počet A asym asociaci (agregace 1 : 1), a z B do A počet B asym asociaci
				 * (agregace 1 : 1), jinak přidám jednu hranu s příslušnou hardinalitou ve směru
				 * z B do A.
				 * 
				 * if (result > 0) pokud jsou sym asociace jako asym asociace, pak z A do B to
				 * budou počet hran A a z B do A to bude počet B, jinak pokud jsou klasické
				 * vztahy, pak jednu hranu ve směru z A do B s příslušnou kardinalitou /
				 * multiplicitou.
				 * 
				 * 
				 * Výpočet, pokud se jedná o zobrazování vztahu (symetrické) asociace "normálně"
				 * -> jako hrana s kardinalitou / (multiplicitou) a agregace 1 : 1 (asymetrická
				 * asociace) také zvlášť:
				 * 
				 * Výpočet je stejný jako výše, ale dle proměnné result se pozná, jaká se má na
				 * příslušných hranách vytvořit kardinalita / (multiplicita), pokud je result ==
				 * 0, pak (symetrická) asociace bude mít kardinalitu jako countA nebo countB -
				 * jsou stejné, jinak se bude jednat o agregaci 1 : 1 (asymetrická asociace),
				 * pak bude kardinalita různá.
				 * 
				 * result > 0, pak směr bude z A (právě testovaná) do B, u třídy B bude countA a
				 * u třídy A budou countA proměnných (kardinalita na konci hrany)
				 * 
				 * result < 0, pak směr šipky bude z B do A (právě testovaná), a kardinalita
				 * bude také opačně (jako výše), tj. u A bude countB a u B bude countA.
				 */

				
				
				

				
				/*
				 * Zde potřebuji neprve otestovat, zda se v mapě ještě právě iterovaná třída
				 * nenachází, protože pokud ne, pak ji musím přidat a jako její hodnotu vytvořím
				 * prázdný list, do kterého se níže mohou vložit hodnoty:
				 */
				if (!map.containsKey(clazz))
					map.put(clazz, new ArrayList<>());
				
				
				
				
				/*
				 * Tato podmínka slouží hlavně pro otestování vztahů tříd sama na sebe.
				 * 
				 * To se hlavně zjistí tak, že právě testovaná proměnná (f) se již nachází v
				 * mapě jako klíč, takže typ proměnné f je nějaká třída z diagramu tříd, a tato
				 * proměnná se v právě iterované třídě testuje poprve.
				 * 
				 * Toto není nezbytná podmínka pro to, aby se jednalo o vztah třídy sama na
				 * sebe, ale je to jedný případ, který zde testuji, pak v těle této podmínky už
				 * se zjistí, zda je to opravdu vztah třídy sama na sebe a zda se má zobrazit
				 * nebo ne.
				 * 
				 * Dále zde v této podmínce nesmí přidávat aktuálně iterovaný typ proměnné (f),
				 * protože kdybych to udělal, tak jak jsem uedl výše, když by se nejednalo o
				 * vztah třídy sama na sebe, pak by se už v další podmínce nezjistil opravdový
				 * vztah, protože už by neprošla podmínka, protože bych přidal proměnou do mapy
				 * jako již testovanou, což by v tomto případě bylo špatně.
				 */
				if (map.containsKey(f.getType()) && !map.get(clazz).contains(f.getType())) {
					/*
					 * Načtu si text, který by měla buňka obsahovat.
					 */
					final String packagesOfClass = getTextOfClassCell(f.getGenericType());

					// Pokud jsem nezískal balíčky, mohu pokračovat další iterací:
					if (packagesOfClass == null)
						continue;
					
					
					
					/*
					 * Načtu si buňku, ke které by měla být nějaká hrana - buď asociace nebo
					 * agregace a otestuji
					 */
					final DefaultGraphCell desCell = GraphClass.getClassCellWithText(packagesOfClass);

					// Note: Už zde nemusím testovat, zda není null, zde už vím, že není - díky
					// metodě getTextOfClassCell.

					/*
					 * Načtu si tedy soubor .class té druhé třídy a prohledám její proměnné (mohu
					 * testovat právě iterovanou třídu, protože je to typ té samé třídy).
					 */
					final Class<?> desClass = App.READ_FILE.loadCompiledClass(packagesOfClass, outputEditor);
					
					
					/*
					 * Zde vím, že už bych tuto podmínku nemusel testovat. Výše už vím, žeje to OK,
					 * ale pořád mi to nedalo i když by měla být zbytečná, ale kdyby náhodou k tomu
					 * došlo, tak předejdu výjimkám při využítání těch proměnných níže.
					 */
					if (desClass == null || desCell == null)
						continue;

					
					
					/*
					 * Je to to, abych nějak nastavil, zda se mají zobrazovat vztahy tříd na sebe
					 * sama. Takže pokud má třída vztah na sebe sama a ty vztahy se v diagramu tříd
					 * nemají zobrazit, pak tu proměnnou vynechám, v ostaních případech vztah dané
					 * proměnné přidám k testování pro zobrazení v diagramu tříd.
					 * 
					 * Pokud je proměnna (atribut) typu své vlastní třídy, pak zjistím, zda se má
					 * tento vztah zobrazit v diagramu, pokud ano, pak jej zobrazím, pokud ne, pak
					 * pokračuji další iterací.
					 */
					if (desCell == selectedCell && !showRelationShipsToClassItself)
						continue;

					else if (desCell == selectedCell && showRelationShipsToClassItself) {
						/*
						 * V případě, vztahu třídy sama na sebe mohu rovnou vložit vztah typu agregace 1
						 * : 1 na příslušnou třídu, resp. buňku v diagramu tříd. V tomto případě to bude
						 * 1 : 1 (asymetrická asociace), protože 1 : N je například u pole nebo listu,
						 * toto je "klasická" proměnná.
						 */

						/*
						 * Zde si zjistím počet proměnných, které jsou typu třídy sama na sebe, abych
						 * mohl znázornit příslušnou kardinaltiu (multipilcitu).
						 */
						final int count = getCountOfVariables(Arrays.asList(fields), f.getType());

						// Přidám příslušný počet hran.
						newEdgesList.add(
								new InfoForNewEdge(selectedCell, desCell, count, 1, EdgeTypeEnum.AGGREGATION_1_1));

						// Mohu pokračovat další iterací.
						continue;
					}
				}
				
				
				
				
				
				
				
				
				
				
				
				/*
				 * Zde potřebuji zjistit, zda se jedná o proměnnou, která je typu, který se
				 * ještě netestoval jako třída, tedy ta proměnná (f) buď není typu nějaké třídy
				 * z diagramu tříd, nebo je typu nějaké třídy z diagramu tříd, ale tato třída se
				 * ještě netestovala NEBO se ta třída již testovala, tj. je to třída z diagramu
				 * tříd, ale ta proměnná (f) se netestovala v té dříve testované třídě, která je
				 * v mapě jako klíč, toto znamená že se jedná jen o vztah t "této" třídy clazz,
				 * konkrétně proměnná f je jediný vztah na tu druhou cílovou třídu.
				 * 
				 * (Kdybych tuto druhou část podmínky vynechal, tak by se testovala každá třída
				 * jen jednou, ale to je špatně, pak by se nevytvořily všechny vztahy.)
				 * 
				 * Dále v této aktuálně iterované třídě se ta právě iterovaná proměnná také
				 * nesmí nacházet v mapě u příslušné třídy, protože pokud ano, pak by to
				 * znamenalo, že v příslušné třídě (právě iterované) jsem již příslušnou
				 * promě+nnou (f) již jednou testoval, takže vztahy jsou již vytvořen, tak už do
				 * podmínky nejdu.
				 * 
				 * Tímto jsem si zajistil, že se každý typ proměnné v každé třídě bude testovat
				 * pouze jednou (tj. když například bude více proměnných stejného datového typu,
				 * pak se otestují pouze jednou - ten typ proměnné). Dále jsem tímto zajistil,
				 * že se z úplně z každé třídy projdou všechny typy proměnných tak, že nebudou
				 * duplicity u obou verzí asociací a vždy se vytvoří ve správném počtu a směru,
				 * případně s kardinalitou.
				 * 
				 * 
				 * Př:
				 * Takže Například u sym asociace, se zde zjistí ten vztah, a až bude testovat
				 * tu druhou třídu na tuto, tak v mapě jako klíč už ta třída bude, takže se
				 * neotestuje, a nevzniknou tak duplicity.
				 * 
				 * Dále například pro asym asociace se při první iteraci té první třídy zjistí
				 * celý vztah se správnou kardinalitou / multiplicitou a při další iteraci se
				 * již zjistí, že se ta třída dříve testovala nebo, pokud je to jen jednosměrná
				 * asym asociace (pouze z A do B), pak se zjisí i, že se v té první třídě
				 * netestoval tento druhý typ promněnné (třídy), resp. že se v té první třídě
				 * nenachází proměnná té druhé třídy.
				 */
				if ((!map.containsKey(f.getType()) || !map.get(f.getType()).contains(clazz))
						&& !map.get(clazz).contains(f.getType())) {

					/*
					 * Zde do mapy přidám prměnnou k právě iterované třídě, aby se tato proměnná v
					 * právě iterované třídě již netestovala, díky tomuto jsem si zajistil, že se
					 * proměnná bude v jedné třídě iterovat pouze jednou, takže se vztahy níže
					 * vytvoří také pouze jednou.
					 */
					addValueToMap(clazz, f.getType());
					
					
					

					/*
					 * Načtu si text, který by mela buňka obsahovat.
					 */
					final String packagesOfClass = getTextOfClassCell(f.getGenericType());

					/*
					 * Otestuji, zda neni null, pokud ne, tak se jedná o proměnnou, typu nějaké
					 * třídy, která se nachzází v diagramu tříd.
					 */
					if (packagesOfClass == null)
						continue;

					
					/*
					 * Načtu si buňku, ke které by měla být nějaká hrana - buď asociace nebo
					 * agregace a otestuji.
					 */
					final DefaultGraphCell desCell = GraphClass.getClassCellWithText(packagesOfClass);

					// Note: Už zde nemusím testovat, zda není null, zde už vím, že není - díky
					// metodě getTextOfClassCell.

					/*
					 * Načtu si tedy soubor .class té druhé třídy a prohledám její proměnné.
					 */
					final Class<?> desClass = App.READ_FILE.loadCompiledClass(packagesOfClass, outputEditor);

						
						
					/*
					 * Zde vím, že už bych tuto podmínku nemusel testovat. Výše už vím, žeje to OK,
					 * ale pořád mi to nedalo i když by měla být zbytečná, ale kdyby náhodou k tomu
					 * došlo, tak předejdu výjimkám při využítání těch proměnných níže.
					 */
					if (desClass == null || desCell == null)
						continue;

						
						
						
					/*
					 * Dále si zjistím počet proměnných v této testované třídě - clazz kolik
					 * obsahuje proměnných typu té druhé třídy, resp. třídy desClass:
					 */
					final int countA = getCountOfVariables(Arrays.asList(fields), f.getType());

					/*
					 * Zde byla načtena přeložená třída, tak si získám její proměnné a otestuji,
					 * kolik obsahuje proměnných typu této třídy - clazz, abych mohl určit počet
					 * asociací a agregací, které se mají vytvořit:
					 */
					final int countB = getCountOfVariables(Arrays.asList(desClass.getDeclaredFields()), clazz);

					final int result = countA - countB;
						
						
						
						
						
						
						
						
					/*
					 * Zde zjistím, zda se má vztah typu asociace zobrazovat jako dva vztahy typu
					 * agregace, pokud ano, tak stačí pouze v této části přidat počet vztahů typu
					 * agregace 1 : 1 (asymetrická asociace) na obě třídy zárověň a pokračovat další
					 * iterací, protože není třeba dále řešit vztah typu asociace. Pak až se bude
					 * iterovat ta druhá třída, tak se u ní (u příslušné proměnné) zjistí že už se
					 * nachází v kolekci, tak se nebude přidávat i vztah z druhé strny.
					 * 
					 * V podmínce musí být i, zda je result nula, protože pokud je result nula, pak
					 * to značí, že se jedná o vztah typu asociace, protože v obou třídách je uveden
					 * stejný počet proměnných, takže symetrická asociace.
					 */
					if (showAssociationThroughAggregation && result == 0) {
						for (int i = 0; i < countA; i++) {
							newEdgesList.add(
									new InfoForNewEdge(selectedCell, desCell, 1, 1, EdgeTypeEnum.AGGREGATION_1_1));

							newEdgesList.add(
									new InfoForNewEdge(desCell, selectedCell, 1, 1, EdgeTypeEnum.AGGREGATION_1_1));
						}

						continue;
					}


						

					
					
					
						
						
					/*
					 * V případě, že se má využít "rychlý" postup, tak zde musím hlídat počet vztahů
					 * typu (symetrická) asociace, protože například při tomto testování se přijde
					 * na vztah (symetrická) asociace mezi třídami srcCell a desCell, a při dalším
					 * testování, až se bude testovat třída desCell, pak se přijde na vztah asociace
					 * s třídou srcCell.
					 * 
					 * Takže zde musím vytvořit příslušnou hranu s příslušným počtem proměnných pro
					 * kardinalitu (multiplicitu) a po skončení cyklu je vytvořit, protože se již
					 * výše přidali obe proměnné do listu, takže už se testovat nebudou, tak rovnou
					 * zjistím počty a přidám do kolekce vztah s příslušnou kardinalitou a tím ta
					 * konkrétní proměnná / proměnné v obou třídách končí.
					 */
					if (result == 0)
						/*
						 * Zde vím, že je zvolen rychlý způsob pro doplňování vztahů typu asociace,
						 * takže stačí vytvořit jen jednu hranu s příslušnou kardinalitou
						 * (multiplicitou), protože vím, že v této části ta hrana ještě neexistuje, když
						 * se úplně na začátku vymazaly veškeré hrany.
						 * 
						 * Note: Kardinalita / multiplicita na hraně může být z countA nebo z countB, je
						 * to jedno, v tomto případě jsou v obou proměnných stejné hodnoty (po odečtení
						 * jsou nul).
						 */
						newEdgesList.add(new InfoForNewEdge(selectedCell, desCell, countB, EdgeTypeEnum.ASSOCIATION));

					
					// Může být jen '<':
					else if (result <= 0) {
						/*
						 * Zde potřebuji otestovat, zda se mají vztahy typu (symetrická) asociace
						 * zobrazit jako dvě (asymetrické) asociace, resp. agregace 1 : 1, protože pokud
						 * ano, pak zde musím přidat příslušný počet hran, kolik je příslušných
						 * proměnných, takže z selectedCell do desCell musím přidat countA hran typu
						 * agregace 1 : 1 (pro reprezentaci asymetrické asociace) a z desCell do
						 * selectedCell musím přidat countB hran (pro reprezentaci asymetrické
						 * asociace).
						 * 
						 * Jinak by se zde zobrazila hrana s příslušnou kardinalitou.
						 * 
						 * Toto je vše potřeba provést zde, protože jinak by se příslušný počet hran z
						 * druhého pohledu, tedy z desCell do selectedCell ty hrany nevytvořily, protože
						 * už by se netestovala příslušná proměnná.
						 */
						if (showAssociationThroughAggregation) {
							for (int i = 0; i < countA; i++)
								newEdgesList.add(
										new InfoForNewEdge(selectedCell, desCell, 1, 1, EdgeTypeEnum.AGGREGATION_1_1));

							for (int i = 0; i < countB; i++)
								newEdgesList.add(
										new InfoForNewEdge(desCell, selectedCell, 1, 1, EdgeTypeEnum.AGGREGATION_1_1));
						}

						
						/*
						 * Zde se přidá pouze vztah agragace 1 : 1 (asymetrická asociace), která bude
						 * mít multiplicitu, která značí příslušný počet proměnných na každé straně.
						 * 
						 * 
						 * Výpočet:
						 * 
						 * V tomto případě, vím, že rozdíl hodnot countA a countB je záporný, takže
						 * countB musí být větší než countA. Takže ve třídě desCell je větší počet
						 * proměnných než ve třídě selectedCell, takže šipka povede z desCell do
						 * selectedCell a kardinalita bude jakoby obráceně. countA bude u cílové třídy a
						 * countB bude u zdroje (selectedCell).
						 * 
						 * Například: Auto, Motor – Auto má dva atributy typu Motor, pak dvojka bude u
						 * Motor a u Auto bude jednička, a to i tehdy, když Motor nebude mít atribut
						 * typu Auto).
						 */
						else
							newEdgesList.add(new InfoForNewEdge(desCell, selectedCell, countA, countB,
									EdgeTypeEnum.AGGREGATION_1_1));
					}

					
					
					// else if (result > 0)
					else {
						/*
						 * Zde potřebuji otestovat, zda se mají vztahy typu (symetrická) asociace
						 * zobrazit jako dvě (asymetrické) asociace, resp. agregace 1 : 1, protože pokud
						 * ano, pak zde musím přidat příslušný počet hran, kolik je příslušných
						 * proměnných, takže z selectedCell do desCell musím přidat countA hran typu
						 * agregace 1 : 1 (pro reprezentaci asymetrické asociace) a z desCEll do
						 * selectedCell musím přidat countB hran (pro reprezentaci asymetrické
						 * asociace).
						 * 
						 * Jinak by se zde zobrazila hrana s příslušnou kardinalitou.
						 * 
						 * Toto je vše potřeba provést zde, protože jinak by se příslušný počet hran z
						 * druhého pohledu, tedy z desCell do selectedCell ty hrany nevytvořily, protože
						 * už by se netestovala příslušná proměnná.
						 */
						if (showAssociationThroughAggregation) {
							for (int i = 0; i < countA; i++)
								newEdgesList.add(
										new InfoForNewEdge(selectedCell, desCell, 1, 1, EdgeTypeEnum.AGGREGATION_1_1));

							for (int i = 0; i < countB; i++)
								newEdgesList.add(
										new InfoForNewEdge(desCell, selectedCell, 1, 1, EdgeTypeEnum.AGGREGATION_1_1));
						}

						
						/*
						 * Zde se přidá pouze vztah agragace 1 : 1 (asymetrická asociace), která bude
						 * mít kardinalitu (multiplicitu), která značí příslušný počet proměnných na
						 * každé straně.
						 * 
						 * Zde se jedná o "kladný výsledek", takže vím, že ve třídě selectedCell je více
						 * proměnných typu třídy desCell než ve třídě desCell typu třídy selectedCell.
						 * Takže šipka pro agregace 1 : 1 (asymetrickou asociace) dám ve směru
						 * selectedCell do desCell, ale kardinalita se uvádí obráceně, že u cíle mude
						 * kardinalita od zdroje a naopak.
						 * 
						 * Například: Auto, Motor – Auto má dva atributy typu Motor, pak dvojka bude u
						 * Motor a u Auto bude jednička, a to i tehdy, když Motor nebude mít atribut
						 * typu Auto).
						 */
						else
							newEdgesList.add(new InfoForNewEdge(selectedCell, desCell, countB, countA,
									EdgeTypeEnum.AGGREGATION_1_1));
					}
				}
			}
					
				
			
			
			
			/*
			 * Zde stačí vytvořit výše hrany pro agregaci 1 : N (získáno v cyklu výše).
			 */
			fieldsTextListForAgr_1_N.forEach(e -> {
				final DefaultGraphCell cell = GraphClass.getClassCellWithText(e);

				// Podmínka by neměla být potřebná v této části vždy vím, že příslušná buňka /
				// třída v diagramu tříd existuje:
				if (cell != null)
					classDiagram.addAggregation_1_ku_N_Edge(selectedCell, cell);
			});

			
			/*
			 * Vytvořím pouze ty hrany / vztahy mezi třídami, které by měly existovat, tj.
			 * ty, co se nachází v kolekcích výše (newEdgesList obsahuje pouze hrany typu
			 * (symetrická) asociace a agregace 1 : 1 nebo předělaná (asymetrická)
			 * asociace).
			 */
			for (final InfoForNewEdge i : newEdgesList)
				classDiagram.addEdge(i);
		}
		
		// else - zde neni co řešit, nepodařilo se zkompilovat a nebo načíst datnou
		// třídu, tak nic nedělám
	}


    /**
     * Metoda, která slouží pro přidání nové / nových hodnot do mapy.
     * <p>
     * Pokud se keyClass ještě v mapě nenachází, pak se do ni přidá a jako value v mapě se vytvoří nový list a přidá se
     * do něj valueClass. Tato varianta by nikdy neměla nastat, je to pouze jako také ošetření potenciální chyby.
     * <p>
     * Pokud se keyClass již v mapě nachází (tuto by vždy mělo být), pak jen do příslušného listu (value) u keyClass
     * přidám hodnotu valueClass.
     *
     * @param keyClass
     *         - třída, která slouží jako klíč v mapě, pokud v mapě ještě není , pak se do mapy i s value_Class jako
     *         položkou v listu na pozici value v listu přidá.
     * @param valueClass
     *         - hodnota, která se má přidat do listu v mapě na pozici key_Class.
     */
    private void addValueToMap(final Class<?> keyClass, final Class<?> valueClass) {
        // Tato podmínka by měla být zbytečná:
        if (!map.containsKey(keyClass))
            map.put(keyClass, new ArrayList<>(Collections.singletonList(valueClass)));

        else if (!map.get(keyClass).contains(valueClass))
            map.get(keyClass).add(valueClass);
    }


    /**
     * Metoda, která zjistí, kolik proměnných typu proměnné (field) se nachází v listu (filedsList), a tento počet
     * vrátí.
     *
     * @param fieldsList
     *         - list / kolekce, ve které se nachází položky nějaké třídy, a chci zjistit, kolik položek typu proměnné
     *         field se v této kolekci - listu nachází
     * @param fieldType
     *         - z listu výše chci znát, kolik obsahuje položek typu této proměnné - field
     *
     * @return počet proměnných typu proměnné field, které se nachází v listu
     */
    private static int getCountOfVariables(final List<Field> fieldsList, final Class<?> fieldType) {
        int countOfField = 0;

        for (final Field f : fieldsList)
            if (f.getType().equals(fieldType))
                countOfField++;

        return countOfField;
    }


    /**
     * Metoda, která zjistí, zda se položka nachází v nějakém balíčku a příadpně si z položky vatáhne tento balíček a
     * název, a otestuje, zda se v diagramu tříd nachází nějaká buňka, reprezentující třídu, pokud ano, mohlo by se
     * jednat o vztah pro typu agregaci, tak bych příslušné hrany musel v diagramu tříd nějak změnit (smazat, vytvořit
     * přepsat, ...)
     *
     * @param field
     *         - položka, o které chci zjistit, zda je to reference na třídu v idagramu tříd
     *
     * @return text coby název třídy i s balíčky, který by se měl být v diagrau tříd, jinak null, pokud nebude něco z
     * toho nalezeno
     */
    private static String getTextOfClassCell(final Object field) {
        /*
         * Zkusím získat balíček, ve kterém se proměnná nachází (pokud není null). Pokud je balíček null, může se
         * jednat například o "klasicku" proměnnou - int, char, ... ale ne o referenci na jiný objekt, resp. třídu v
         * diagramu tříd, takže vrátím null
         */


        // Zjistím si text, který by měla buňka v diagramu tříd obsahovat:

        // Získání názvu třídy s balíčky:
        final String classNameWithDot = ParameterToText.getDataTypeInText(field, true);


        // Otestuji, zda v diagramu tříd je nějaká buňka s takovým názvem a jedná se o třídu:
        final boolean isClassInClassDiagram = GraphClass.isClassWithTextInClassDiagram(classNameWithDot);

        /*
         * Otestuji, zda se v diagramu tříd nachízí buňka s daným textem, pokud ano, tak tento text vrátím a přídám
         * do kolekce a dále otestuji, zda v daném diagramu, jsou vazby ohledně agregace správně vypsané (jiná část)
         */
        if (isClassInClassDiagram)
            /*
             * Zde se v diagramu tříd nachází buňka s daným textem, tak ho mohu vrátit a přidat text do kolekce pro
             * pozdější testování hran mezi třídami:
             */
            return classNameWithDot;

        return null;
    }
}