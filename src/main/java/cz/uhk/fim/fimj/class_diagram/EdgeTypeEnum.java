package cz.uhk.fim.fimj.class_diagram;

/**
 * Tento výčet slouží pro "definování" různých typů hran, které se mohou vytvořit v diagramu tříd,
 * <p>
 * Jsou zde definovány "typy" hran, jako agregace, asociace, dědíčnost, implementace, komentář, apod. viz vkládání do
 * diagramu tříd - třída GraphClass.java, které lze do diagramu tříd vložit
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public enum EdgeTypeEnum {

    // Hrany, které lze vložit do diagramu tříd:
    ASSOCIATION, EXTENDS, IMPLEMENTATION, AGGREGATION_1_1, AGGREGATION_1_N, COMMENT
}
