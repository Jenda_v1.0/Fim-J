package cz.uhk.fim.fimj.class_diagram;

/**
 * Tato třída obsahuje pouzte typy editorů zdrojového kódu, ve kterých může uživatel otevřit třídy v diagramu tříd.
 * <p>
 * Jde o to, že uživatel má mít možnost si sám zvolit výchozí editor zdrojového kódu, tak tent výčet obsahuje možnosti
 * editorů.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public enum KindOfCodeEditor {

    /**
     * Tato hodnota značít dialog editor zdrojového kódu.
     */
    SOURCE_CODE_EDITOR("Source_Code_Editor"),

    /**
     * Tato hodnota značít dialog průzkumník projektů.
     */
    PROJECT_EXPLORER("Project_Explorer");


    /**
     * Tato hodnota značí pouze nějaco jako identifikátor zvoleného editoru. Je to zde potřeba kvůli uložení do
     * souboru,
     * abych nemusel jen kopírovat ty výčtové hodnoty do textu.
     */
    private final String value;


    /**
     * Konstruktor této třídy.
     *
     * @param value
     *         - "identifikátor" výčtového hodnoty kvůli čtení a zápisu příslušné výčtové hodnoty z a do souboru.
     */
    KindOfCodeEditor(final String value) {
        this.value = value;
    }


    /**
     * Getr na hodnotu, která "identifikuje" příslušný editor kódu.
     *
     * @return - identifikátor zdrojového kódu / výčtové hodnoty.
     */
    public String getValue() {
        return value;
    }
}
