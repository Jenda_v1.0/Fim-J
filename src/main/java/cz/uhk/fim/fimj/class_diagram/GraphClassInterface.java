package cz.uhk.fim.fimj.class_diagram;

/**
 * Tato třída coby rozhraní slouží pro deklaraci signatur metod, které jsou implementovány v třídě GraphClass coby
 * diagram tříd a tyto metody slouží pro základní "zacházení - ovládaní" diagramu tříd, resp. manipulaci s objekty v
 * něm, jako je třeba přidání nově reprezentace třídy, komentáře, různých vztahů mezi třídami, apod. viz konkrétní
 * metody
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface GraphClassInterface {

	/**
	 * Metoda, která do grafu přidá "buňku", která reprezentuje třídu
	 * 
	 * @param name
	 *            = Název buňky, resp. nové třídy
	 */
	void addClassCell(final String name);
	
	
	/**
	 * Metoda, která přidá komentář k označené třídě (buňce, co reprezentuje třídu)
	 * do grafu přidá bunku, která reprezentuje komentář, spulu s hranou, která
	 * tento komentář spojuje s zvolenou třídou
	 * 
	 * @param classCell
	 *            - označená "třída" buňka, ke které se ma připojit komentář
	 * 
	 * @param textToCell
	 *            - text, který se má přidat do buňky coby nový komentář, nebo null
	 *            hodnota, pokud se má vložit výchozí text: "Vložte komentář ...",
	 *            možnost s nějakým textem může nastat v případě, že se přenačte
	 *            diagramu tříd a tím, že se změnily styly buněk v nastavení,
	 *            například jiné barvy pozadí, písma, ... pak se přenačte tento
	 *            diagram a vytvoří se nové reprezentace příslušnýchobjektů s novým
	 *            designem a tím pádem se musí ty původní zase smazat, ale komentáře
	 *            se musí zachovat, proto tento parametr
	 */
	void addComment(final Object classCell, final String textToCell);
	
	
	
	
	/**
	 * Metoda, která přidá hranu ("vazbu") do grafu, která spojí dvě třídy Jedná se
	 * o hranu, která reprezentuje asociace mezi zvolenými třídami
	 * 
	 * Postup:
	 * otestuji, zda se v kolekci nachází příslušné buňky - třídy Pokud ano,
	 * vytvořím hranu, její design a přidám ji do kolekce a přenačtu graf kvůli nově
	 * přidané hraně pokud ne, oznámím to uživateli - Nemělo by nastat
	 * 
	 * @param srcCell
	 *            - Zrojová buňka - třída
	 * 
	 * @param desCell
	 *            - cílová buňka - třída
	 * 
	 * @param cardinalityAsc
	 *            - hodnota, která značí kardinalitu (multiplicitu), která se bude
	 *            nacházet na obou koncích hrany.
	 * 
	 */
	void addAssociationEdge(final Object srcCell, final Object desCell, final int cardinalityAsc);
	
	
	
	
	
	/**
	 * Metoda, která do grafu přidá hranu, která reprezentuje dědičnost mezi
	 * zvolenými dvěma třídami
	 * 
	 * Postup:
	 * Otestuji, zda se v kolekci - grafu nachází zvolené třídy - buňky, Pokud ano,
	 * vytvořím příslušnou hranu, její design, výchozí nastavení, ... a přidám ji do
	 * grafu pokud ne, oznámím to uživateli - Nemělo by nastat
	 * 
	 * @param srcCell
	 *            - Zdrojová buňka - třída, která bude dědit z cílové buňky - třídy
	 * 
	 * @param desCell
	 *            - cílová buňka, ze které bude zdrojová buňka - třída dědit
	 */
	void addExtendsEdge(final Object srcCell, final Object desCell);
	
	
	
	
	
	/**
	 * Metoda, která do grafu přidá hranu, která reprezentuje implementaci rozhraní
	 * 
	 * Postup:
	 * Otestuje, zda se označené třídy - buňky nacházejí v grafu, pokud ano,
	 * vytvořím příslušnou hranu, její výchozí nastavení, ... a přidám ji do grafu
	 * pokud ne, oznámím to uživateli - Nemělo by nastat
	 * 
	 * @param srcCell
	 *            - zrojová třída, která bude implementovat rozhraní = cílová buňka
	 *            - třída
	 * 
	 * @param desCell
	 *            - cílová buňka, třída, = Rozhraní
	 */
	void addImplementsEdge(final Object srcCell, final Object desCell);
	
	
	
	
	/**
	 * Metoda, která do grafu přidá hranu, která reprezentuje vztah - agregaci mezi
	 * 2 třídami
	 * 
	 * Postup:
	 * Otestuje, zda se označené třídy - buňky nacházejí v grafu, pokud ano,
	 * vytvořím příslušnou hranu, její výchozí nastavení, ... a přidám ji do grafu
	 * pokud ne, oznámím to uživateli - Nemělo by nastat
	 * 
	 * @param srcCell
	 *            -zdrojová třída - buňka, která bude mít referenci na zdrojovou
	 *            buňku bud list zdrojových objektu nebo odkaz na jeden cil - jako
	 *            jeden objekt zdroj 1 : List<Cil> nebo : zdroj 1 : cil 1
	 * 
	 * @param desCell
	 *            - cílová třída - buňka na kterou bude mít zdrojová třída - buňka
	 *            reference
	 * 
	 * @param cardinalitySrc
	 *            - hodnota, která značí kardinalitu (multiplicitu), která se bude
	 *            nacházet na straně srcCell - u zdrojové buňky / třídy.
	 * 
	 * @param cardinalityDes
	 *            - hodnota, která značí kardinalitu (multiplicitu), která se bude
	 *            nacházet na straně desCell - u cílové buňky / třídy.
	 */
	void addAggregation_1_1ku_1_Edge(final Object srcCell, final Object desCell, final int cardinalitySrc,
			final int cardinalityDes);
	
	
	
	
	/**
	 * Metoda, která do grafu přidá agregaci mezi dvěma buňkami typu 1 : N Zdrojová
	 * třídy - srcCell bude mít List<desCell>
	 * 
	 * @param srcCell
	 *            - zdrojová třída
	 * 
	 * @param desCell
	 *            - cílová třída
	 */
	void addAggregation_1_ku_N_Edge(Object srcCell, Object desCell);
}
