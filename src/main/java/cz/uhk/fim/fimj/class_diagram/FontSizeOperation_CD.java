package cz.uhk.fim.fimj.class_diagram;

import java.awt.Font;
import java.io.File;
import java.util.Map;
import java.util.Properties;

import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.GraphConstants;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.ConfigurationFiles;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.file.WriteToFile;
import cz.uhk.fim.fimj.language.EditProperties;

/**
 * Tato třída obsahuje metody pro práci s velikostí písma / fontu objektů v diagramu tříd.
 * <p>
 * Jedná se o metody pro tzn. "rychlé" nastavení velikosti písma / fontu pro objekty v dagramu tříd.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class FontSizeOperation_CD implements FontSizeOperation_CD_Interface {
	
	@Override
	public void quickSetupOfFontSize(final int size, final GraphClass classDiagram) {
		/*
		 * Získám si objekt Properties, který je "držen" v aplikaci pro zjišťování typů
		 * objektů v diagramu tříd. Dle tohoto objektu s příslušnými hodnotam ise
		 * rozeznávají veškeré objekty v diagramu tříd
		 */
		final Properties prop = GraphClass.KIND_OF_EDGE.getProperties();
		
		
		/*
		 * Tato podmínka by neměla nikdy nastat a ještě nikdy nenastala, pouze pro
		 * případ nouze, resp. něnajké chyby, ke které by nikdy nemělo dojít.
		 */
		if (prop == null)
			return;
		
		
		
		// Zjistím si typ písem / fontů všech objektů v diagramu tříd.
		final int fontStyleClass = Integer.parseInt(prop.getProperty("ClassFontType", Integer.toString(Constants.CD_FONT_CLASS.getStyle())));
		final int fontStyleCommentCell = Integer.parseInt(prop.getProperty("CommentFontType", Integer.toString(Constants.CD_FONT_COMMENT.getStyle())));
		
		final int fontStyleAsociation = Integer.parseInt(prop.getProperty("AscEdgeLineFontStyle", Integer.toString(Constants.CD_ASC_EDGE_LINE_FONT.getStyle())));
		final int fontStyleInherit = Integer.parseInt(prop.getProperty("ExtEdgeLineFontStyle", Integer.toString(Constants.CD_EXT_EDGE_LINE_FONT.getStyle())));
		final int fontStyleImplements = Integer.parseInt(prop.getProperty("ImpEdgeLineFontStyle", Integer.toString(Constants.CD_IMP_EDGE_LINE_FONT.getStyle())));
		final int fontStyleAgreagation_1_ku_1 = Integer.parseInt(prop.getProperty("AgrEdgeLineFontStyle", Integer.toString(Constants.CD_AGR_EDGE_LINE_FONT.getStyle())));
		final int fontStyleAgreagation_1_ku_N = Integer.parseInt(prop.getProperty("Agr_1_N_EdgeLineFontStyle", Integer.toString(Constants.CD_AGR_EDGE_N_LINE_FONT.getStyle())));
		final int fontStyleCommentEdge = Integer.parseInt(prop.getProperty("CommentEdgeLineFontStyle", Integer.toString(Constants.CD_COM_EDGE_FONT.getStyle())));
		
		
		
		
		/*
		 * Projdu veškeré objekty v diagramu tříd a všem nastavím nové velikosti fontů /
		 * písma:
		 */
		GraphClass.getCellsList().forEach(c -> {
			final Map<?, ?> attributes = c.getAttributes();

			if (c instanceof DefaultEdge) {
				final DefaultEdge edge = (DefaultEdge) c;

				if (GraphClass.KIND_OF_EDGE.isAssociationEdge(edge))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_ASC_EDGE_LINE_FONT.getName(), fontStyleAsociation, size));

				else if (GraphClass.KIND_OF_EDGE.isInheritEdge(edge))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_EXT_EDGE_LINE_FONT.getName(), fontStyleInherit, size));

				else if (GraphClass.KIND_OF_EDGE.isImplementsEdge(edge))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_IMP_EDGE_LINE_FONT.getName(), fontStyleImplements, size));

				else if (GraphClass.KIND_OF_EDGE.isAggregation_1_ku_1_Edge(edge))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_AGR_EDGE_LINE_FONT.getName(), fontStyleAgreagation_1_ku_1, size));

				else if (GraphClass.KIND_OF_EDGE.isAggregation_1_ku_N_Edge(edge))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_AGR_EDGE_N_LINE_FONT.getName(), fontStyleAgreagation_1_ku_N, size));

				else if (GraphClass.KIND_OF_EDGE.isCommentEdge(edge))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_COM_EDGE_FONT.getName(), fontStyleCommentEdge, size));
			}

			else {
				if (GraphClass.KIND_OF_EDGE.isClassCell(c))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_FONT_CLASS.getName(), fontStyleClass, size));

				else if (GraphClass.KIND_OF_EDGE.isCommentCell(c))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_FONT_COMMENT.getName(), fontStyleCommentCell, size));
			}
		});

		
		/*
		 * Je třeba aktualizovat diagram tříd, aby se změny velikosti písmen / fontů
		 * projevily v okně aplikace.
		 */
		classDiagram.updateDiagram(GraphClass.getCellsList());
		
		
		
		/*
		 * Nyní je třeba uložit veškeré změněné hodnoty do objektu Properties, který je
		 * držen v aplikaci, aby se mohli nadále zjišťovat objekty v diagramu tříd, teď
		 * pouze s novými daty.
		 */		
		GraphClass.KIND_OF_EDGE.editValueInProperties("ClassFontSize", String.valueOf(size));
		GraphClass.KIND_OF_EDGE.editValueInProperties("CommentFontSize", String.valueOf(size));
		
		GraphClass.KIND_OF_EDGE.editValueInProperties("AscEdgeLineFontSize", String.valueOf(size));		
		GraphClass.KIND_OF_EDGE.editValueInProperties("ExtEdgeLineFontSize", String.valueOf(size));		
		GraphClass.KIND_OF_EDGE.editValueInProperties("ImpEdgeLineFontSize", String.valueOf(size));
		GraphClass.KIND_OF_EDGE.editValueInProperties("AgrEdgeLineFontSize", String.valueOf(size));
		GraphClass.KIND_OF_EDGE.editValueInProperties("Agr_1_N_EdgeLineFontSize", String.valueOf(size));
		GraphClass.KIND_OF_EDGE.editValueInProperties("CommentEdgeLineFontSize", String.valueOf(size));
		
		

		
		
		/*
		 * Zde je třeba aktualizovat objekt Properties, který je držen v tomto diagramu
		 * instancí kvůli vytváření nových objektů, kdybych toto neudělal, tak by se
		 * vytvářeli vždy nové objekty, třeba třídy apod. s původními velikostmi fotnů /
		 * písma, ale takové objekty by se nerozpoznaly, protože jsou drženy již jiné
		 * velikost písem.
		 * 
		 * Note:
		 * Bylo by možné využít jen jeden objekt Properties, ale původně se ty hodnoty
		 * pro testování a vytváření načítali vždy nové aktuální, pak se to ale
		 * předělalo na restart aplikace, pak byla doplněna tato možnost pro "rychlou"
		 * změnu velikostí písem apod. Tak jsem to již nechal takto, kdybych se v
		 * budoucnu ještě rozhodl někde něco upravit.
		 */
		classDiagram.setDefaultProperties(GraphClass.KIND_OF_EDGE.getProperties());
		
		
		
		
		
		
		// Uložím data do souboru:
		
		
		/*
		 * Note:
		 * Zde pouze zmíním, že to načítání s komentáři zde dělám proto, že když bych to
		 * takto načítal již při "prvotní" fází a držel ten soubor v aplikaci a uživatel
		 * za běhu aplikace ten soubor upravil, resp. upravil nějaký komentář, tak v
		 * této fází by se ten komentář přepsal na ty co byly načteny a drženy v
		 * aplikaci, takže by to musel uživatle přepsat znovu, nebo by si toho ani
		 * nevšiml, proto si výše načítám a v aplikaci držím pouze soubor s nastavín a
		 * ne ten pro ukládání.
		 */
		
		
		/*
		 * Zde si načtu soubour pro uložení změn pouze z adresáře Workspace, jinak nic
		 * ten z aplikace je pouze pro výchozí nastavení.
		 * 
		 * Toto musím udělat, protože potřebuji načíst příslušný soubor i s komentáři,
		 * vložit do něj nově nastavné velikost (viz výše) a zase jej uložit, protože
		 * když uživatel změní velikost písma a zde se to neuloží, pak se při restartu
		 * aplikace změny nerozeznají.
		 */
		final EditProperties classDiagramProperties = getClassDiagramPropWithComments();
		
		
		
		// Zapíšu data do souboru výše
		if (classDiagramProperties != null) {
			classDiagramProperties.setProperty("ClassFontSize", String.valueOf(size));
			classDiagramProperties.setProperty("CommentFontSize", String.valueOf(size));

			classDiagramProperties.setProperty("AscEdgeLineFontSize", String.valueOf(size));
			classDiagramProperties.setProperty("ExtEdgeLineFontSize", String.valueOf(size));
			classDiagramProperties.setProperty("ImpEdgeLineFontSize", String.valueOf(size));
			classDiagramProperties.setProperty("AgrEdgeLineFontSize", String.valueOf(size));
			classDiagramProperties.setProperty("Agr_1_N_EdgeLineFontSize", String.valueOf(size));
			classDiagramProperties.setProperty("CommentEdgeLineFontSize", String.valueOf(size));

			// Uložení do souboru:
			classDiagramProperties.save();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	

	@Override
	public void quickSetupDefaultFontSizes(final GraphClass classDiagram) {
		/*
		 * Získám si objekt Properties, který je "držen" v aplikaci pro zjišťování typů
		 * objektů v diagramu tříd. Dle tohoto objektu s příslušnými hodnotam ise
		 * rozeznávají veškeré objekty v diagramu tříd
		 */
		final Properties prop = GraphClass.KIND_OF_EDGE.getProperties();
		
		
		/*
		 * Tato podmínka by neměla nikdy nastat a ještě nikdy nenastala, pouze pro
		 * případ nouze, resp. něnajké chyby, ke které by nikdy nemělo dojít.
		 */
		if (prop == null)
			return;
		
		
		
		// Zjistím si typ písem / fontů všech objektů v diagramu tříd.
		final int fontStyleClass = Integer
				.parseInt(prop.getProperty("ClassFontType", Integer.toString(Constants.CD_FONT_CLASS.getStyle())));
		final int fontStyleCommentCell = Integer
				.parseInt(prop.getProperty("CommentFontType", Integer.toString(Constants.CD_FONT_COMMENT.getStyle())));

		final int fontStyleAsociation = Integer.parseInt(
				prop.getProperty("AscEdgeLineFontStyle", Integer.toString(Constants.CD_ASC_EDGE_LINE_FONT.getStyle())));
		final int fontStyleInherit = Integer.parseInt(
				prop.getProperty("ExtEdgeLineFontStyle", Integer.toString(Constants.CD_EXT_EDGE_LINE_FONT.getStyle())));
		final int fontStyleImplements = Integer.parseInt(
				prop.getProperty("ImpEdgeLineFontStyle", Integer.toString(Constants.CD_IMP_EDGE_LINE_FONT.getStyle())));
		final int fontStyleAgreagation_1_ku_1 = Integer.parseInt(
				prop.getProperty("AgrEdgeLineFontStyle", Integer.toString(Constants.CD_AGR_EDGE_LINE_FONT.getStyle())));
		final int fontStyleAgreagation_1_ku_N = Integer.parseInt(prop.getProperty("Agr_1_N_EdgeLineFontStyle",
				Integer.toString(Constants.CD_AGR_EDGE_N_LINE_FONT.getStyle())));
		final int fontStyleCommentEdge = Integer.parseInt(
				prop.getProperty("CommentEdgeLineFontStyle", Integer.toString(Constants.CD_COM_EDGE_FONT.getStyle())));
		
		
		
		/*
		 * Projdu veškeré objekty v diagramu tříd a všem nastavím nové velikosti fontů /
		 * písma:
		 */
		GraphClass.getCellsList().forEach(c -> {
			final Map<?, ?> attributes = c.getAttributes();

			if (c instanceof DefaultEdge) {
				final DefaultEdge edge = (DefaultEdge) c;

				if (GraphClass.KIND_OF_EDGE.isAssociationEdge(edge))
					GraphConstants.setFont(attributes, new Font(Constants.CD_ASC_EDGE_LINE_FONT.getName(),
							fontStyleAsociation, Constants.CD_ASC_EDGE_LINE_FONT.getSize()));

				else if (GraphClass.KIND_OF_EDGE.isInheritEdge(edge))
					GraphConstants.setFont(attributes, new Font(Constants.CD_EXT_EDGE_LINE_FONT.getName(),
							fontStyleInherit, Constants.CD_EXT_EDGE_LINE_FONT.getSize()));

				else if (GraphClass.KIND_OF_EDGE.isImplementsEdge(edge))
					GraphConstants.setFont(attributes, new Font(Constants.CD_IMP_EDGE_LINE_FONT.getName(),
							fontStyleImplements, Constants.CD_IMP_EDGE_LINE_FONT.getSize()));

				else if (GraphClass.KIND_OF_EDGE.isAggregation_1_ku_1_Edge(edge))
					GraphConstants.setFont(attributes, new Font(Constants.CD_AGR_EDGE_LINE_FONT.getName(),
							fontStyleAgreagation_1_ku_1, Constants.CD_AGR_EDGE_LINE_FONT.getSize()));

				else if (GraphClass.KIND_OF_EDGE.isAggregation_1_ku_N_Edge(edge))
					GraphConstants.setFont(attributes, new Font(Constants.CD_AGR_EDGE_N_LINE_FONT.getName(),
							fontStyleAgreagation_1_ku_N, Constants.CD_AGR_EDGE_N_LINE_FONT.getSize()));

				else if (GraphClass.KIND_OF_EDGE.isCommentEdge(edge))
					GraphConstants.setFont(attributes, new Font(Constants.CD_COM_EDGE_FONT.getName(),
							fontStyleCommentEdge, Constants.CD_COM_EDGE_FONT.getSize()));
			}

			else {
				if (GraphClass.KIND_OF_EDGE.isClassCell(c))
					GraphConstants.setFont(attributes, new Font(Constants.CD_FONT_CLASS.getName(), fontStyleClass,
							Constants.CD_FONT_CLASS.getSize()));

				else if (GraphClass.KIND_OF_EDGE.isCommentCell(c))
					GraphConstants.setFont(attributes, new Font(Constants.CD_FONT_COMMENT.getName(),
							fontStyleCommentCell, Constants.CD_FONT_COMMENT.getSize()));
			}
		});

		
		/*
		 * Je třeba aktualizovat diagram tříd, aby se změny velikosti písmen / fontů
		 * projevily v okně aplikace.
		 */
		classDiagram.updateDiagram(GraphClass.getCellsList());
		
		
		
		/*
		 * Nyní je třeba uložit veškeré změněné hodnoty do objektu Properties, který je
		 * držen v aplikaci, aby se mohli nadále zjišťovat objekty v diagramu tříd, teď
		 * pouze s novými daty.
		 */		
		GraphClass.KIND_OF_EDGE.editValueInProperties("ClassFontSize",
				String.valueOf(Constants.CD_FONT_CLASS.getSize()));
		GraphClass.KIND_OF_EDGE.editValueInProperties("CommentFontSize",
				String.valueOf(Constants.CD_FONT_COMMENT.getSize()));

		GraphClass.KIND_OF_EDGE.editValueInProperties("AscEdgeLineFontSize",
				String.valueOf(Constants.CD_ASC_EDGE_LINE_FONT.getSize()));
		GraphClass.KIND_OF_EDGE.editValueInProperties("ExtEdgeLineFontSize",
				String.valueOf(Constants.CD_EXT_EDGE_LINE_FONT.getSize()));
		GraphClass.KIND_OF_EDGE.editValueInProperties("ImpEdgeLineFontSize",
				String.valueOf(Constants.CD_IMP_EDGE_LINE_FONT.getSize()));
		GraphClass.KIND_OF_EDGE.editValueInProperties("AgrEdgeLineFontSize",
				String.valueOf(Constants.CD_AGR_EDGE_LINE_FONT.getSize()));
		GraphClass.KIND_OF_EDGE.editValueInProperties("Agr_1_N_EdgeLineFontSize",
				String.valueOf(Constants.CD_AGR_EDGE_N_LINE_FONT.getSize()));
		GraphClass.KIND_OF_EDGE.editValueInProperties("CommentEdgeLineFontSize",
				String.valueOf(Constants.CD_COM_EDGE_FONT.getSize()));
		

		
		
		/*
		 * Zde je třeba aktualizovat objekt Properties, který je držen v tomto diagramu
		 * instancí kvůli vytváření nových objektů, kdybych toto neudělal, tak by se
		 * vytvářeli vždy nové objekty, třeba třídy apod. s původními velikostmi fotnů /
		 * písma, ale takové objekty by se nerozpoznaly, protože jsou drženy již jiné
		 * velikost písem.
		 * 
		 * Note:
		 * Bylo by možné využít jen jeden objekt Properties, ale původně se ty hodnoty
		 * pro testování a vytváření načítali vždy nové aktuální, pak se to ale
		 * předělalo na restart aplikace, pak byla doplněna tato možnost pro "rychlou"
		 * změnu velikostí písem apod. Tak jsem to již nechal takto, kdybych se v
		 * budoucnu ještě rozhodl někde něco upravit.
		 */
		classDiagram.setDefaultProperties(GraphClass.KIND_OF_EDGE.getProperties());
		
		
		
		
		
		
		// Uložím data do souboru:
		
		
		/*
		 * Note:
		 * Zde pouze zmíním, že to načítání s komentáři zde dělám proto, že když bych to
		 * takto načítal již při "prvotní" fází a držel ten soubor v aplikaci a uživatel
		 * za běhu aplikace ten soubor upravil, resp. upravil nějaký komentář, tak v
		 * této fází by se ten komentář přepsal na ty co byly načteny a drženy v
		 * aplikaci, takže by to musel uživatle přepsat znovu, nebo by si toho ani
		 * nevšiml, proto si výše načítám a v aplikaci držím pouze soubor s nastavín a
		 * ne ten pro ukládání.
		 */
		
		
		/*
		 * Zde si načtu soubour pro uložení změn pouze z adresáře Workspace, jinak nic
		 * ten z aplikace je pouze pro výchozí nastavení.
		 * 
		 * Toto musím udělat, protože potřebuji načíst příslušný soubor i s komentáři,
		 * vložit do něj nově nastavné velikost (viz výše) a zase jej uložit, protože
		 * když uživatel změní velikost písma a zde se to neuloží, pak se při restartu
		 * aplikace změny nerozeznají.
		 */
		final EditProperties classDiagramProperties = getClassDiagramPropWithComments();
		
		
		
		// Zapíšu data do souboru výše
		if (classDiagramProperties != null) {
			classDiagramProperties.setProperty("ClassFontSize", String.valueOf(Constants.CD_FONT_CLASS.getSize()));
			classDiagramProperties.setProperty("CommentFontSize", String.valueOf(Constants.CD_FONT_COMMENT.getSize()));

			classDiagramProperties.setProperty("AscEdgeLineFontSize",
					String.valueOf(Constants.CD_ASC_EDGE_LINE_FONT.getSize()));
			classDiagramProperties.setProperty("ExtEdgeLineFontSize",
					String.valueOf(Constants.CD_EXT_EDGE_LINE_FONT.getSize()));
			classDiagramProperties.setProperty("ImpEdgeLineFontSize",
					String.valueOf(Constants.CD_IMP_EDGE_LINE_FONT.getSize()));
			classDiagramProperties.setProperty("AgrEdgeLineFontSize",
					String.valueOf(Constants.CD_AGR_EDGE_LINE_FONT.getSize()));
			classDiagramProperties.setProperty("Agr_1_N_EdgeLineFontSize",
					String.valueOf(Constants.CD_AGR_EDGE_N_LINE_FONT.getSize()));
			classDiagramProperties.setProperty("CommentEdgeLineFontSize",
					String.valueOf(Constants.CD_COM_EDGE_FONT.getSize()));

			// Uložení do souboru:
			classDiagramProperties.save();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public void quickSetupOfFontSize(final int size, final boolean isAssociation, final boolean isExtends,
			final boolean isimplements, final boolean isClass, final boolean isComment, final boolean isCommentEdge,
			final boolean isAggregation_1_1, final boolean isAggregation_1_N, final GraphClass classDiagram) {
		
		/*
		 * Získám si objekt Properties, který je "držen" v aplikaci pro zjišťování typů
		 * objektů v diagramu tříd. Dle tohoto objektu s příslušnými hodnotam ise
		 * rozeznávají veškeré objekty v diagramu tříd
		 */
		final Properties prop = GraphClass.KIND_OF_EDGE.getProperties();
		
		
		/*
		 * Tato podmínka by neměla nikdy nastat a ještě nikdy nenastala, pouze pro
		 * případ nouze, resp. něnajké chyby, ke které by nikdy nemělo dojít.
		 */
		if (prop == null)
			return;
		
		
		
		// Zjistím si typ písem / fontů všech objektů v diagramu tříd.
		final int fontStyleClass = Integer.parseInt(prop.getProperty("ClassFontType", Integer.toString(Constants.CD_FONT_CLASS.getStyle())));
		final int fontStyleCommentCell = Integer.parseInt(prop.getProperty("CommentFontType", Integer.toString(Constants.CD_FONT_COMMENT.getStyle())));
		
		final int fontStyleAsociation = Integer.parseInt(prop.getProperty("AscEdgeLineFontStyle", Integer.toString(Constants.CD_ASC_EDGE_LINE_FONT.getStyle())));
		final int fontStyleInherit = Integer.parseInt(prop.getProperty("ExtEdgeLineFontStyle", Integer.toString(Constants.CD_EXT_EDGE_LINE_FONT.getStyle())));
		final int fontStyleImplements = Integer.parseInt(prop.getProperty("ImpEdgeLineFontStyle", Integer.toString(Constants.CD_IMP_EDGE_LINE_FONT.getStyle())));
		final int fontStyleAgreagation_1_ku_1 = Integer.parseInt(prop.getProperty("AgrEdgeLineFontStyle", Integer.toString(Constants.CD_AGR_EDGE_LINE_FONT.getStyle())));
		final int fontStyleAgreagation_1_ku_N = Integer.parseInt(prop.getProperty("Agr_1_N_EdgeLineFontStyle", Integer.toString(Constants.CD_AGR_EDGE_N_LINE_FONT.getStyle())));
		final int fontStyleCommentEdge = Integer.parseInt(prop.getProperty("CommentEdgeLineFontStyle", Integer.toString(Constants.CD_COM_EDGE_FONT.getStyle())));
		
		
		
		
		/*
		 * Projdu veškeré objekty v diagramu tříd a všem nastavím nové velikosti fontů /
		 * písma:
		 */
		GraphClass.getCellsList().forEach(c -> {
			final Map<?, ?> attributes = c.getAttributes();

			if (c instanceof DefaultEdge) {
				final DefaultEdge edge = (DefaultEdge) c;

				if (isAssociation && GraphClass.KIND_OF_EDGE.isAssociationEdge(edge))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_ASC_EDGE_LINE_FONT.getName(), fontStyleAsociation, size));

				else if (isExtends && GraphClass.KIND_OF_EDGE.isInheritEdge(edge))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_EXT_EDGE_LINE_FONT.getName(), fontStyleInherit, size));

				else if (isimplements && GraphClass.KIND_OF_EDGE.isImplementsEdge(edge))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_IMP_EDGE_LINE_FONT.getName(), fontStyleImplements, size));

				else if (isAggregation_1_1 && GraphClass.KIND_OF_EDGE.isAggregation_1_ku_1_Edge(edge))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_AGR_EDGE_LINE_FONT.getName(), fontStyleAgreagation_1_ku_1, size));

				else if (isAggregation_1_N && GraphClass.KIND_OF_EDGE.isAggregation_1_ku_N_Edge(edge))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_AGR_EDGE_N_LINE_FONT.getName(), fontStyleAgreagation_1_ku_N, size));

				else if (isCommentEdge && GraphClass.KIND_OF_EDGE.isCommentEdge(edge))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_COM_EDGE_FONT.getName(), fontStyleCommentEdge, size));
			}

			else {
				if (isClass && GraphClass.KIND_OF_EDGE.isClassCell(c))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_FONT_CLASS.getName(), fontStyleClass, size));

				else if (isComment && GraphClass.KIND_OF_EDGE.isCommentCell(c))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_FONT_COMMENT.getName(), fontStyleCommentCell, size));
			}
		});

		
		/*
		 * Je třeba aktualizovat diagram tříd, aby se změny velikosti písmen / fontů
		 * projevily v okně aplikace.
		 */
		classDiagram.updateDiagram(GraphClass.getCellsList());
		
		
		
		/*
		 * Nyní je třeba uložit veškeré změněné hodnoty do objektu Properties, který je
		 * držen v aplikaci, aby se mohli nadále zjišťovat objekty v diagramu tříd, teď
		 * pouze s novými daty.
		 */
		if (isClass)
			GraphClass.KIND_OF_EDGE.editValueInProperties("ClassFontSize", String.valueOf(size));

		if (isComment)
			GraphClass.KIND_OF_EDGE.editValueInProperties("CommentFontSize", String.valueOf(size));

		if (isAssociation)
			GraphClass.KIND_OF_EDGE.editValueInProperties("AscEdgeLineFontSize", String.valueOf(size));

		if (isExtends)
			GraphClass.KIND_OF_EDGE.editValueInProperties("ExtEdgeLineFontSize", String.valueOf(size));

		if (isimplements)
			GraphClass.KIND_OF_EDGE.editValueInProperties("ImpEdgeLineFontSize", String.valueOf(size));

		if (isAggregation_1_1)
			GraphClass.KIND_OF_EDGE.editValueInProperties("AgrEdgeLineFontSize", String.valueOf(size));

		if (isAggregation_1_N)
			GraphClass.KIND_OF_EDGE.editValueInProperties("Agr_1_N_EdgeLineFontSize", String.valueOf(size));

		if (isCommentEdge)
			GraphClass.KIND_OF_EDGE.editValueInProperties("CommentEdgeLineFontSize", String.valueOf(size));
		
		

		
		
		/*
		 * Zde je třeba aktualizovat objekt Properties, který je držen v tomto diagramu
		 * instancí kvůli vytváření nových objektů, kdybych toto neudělal, tak by se
		 * vytvářeli vždy nové objekty, třeba třídy apod. s původními velikostmi fotnů /
		 * písma, ale takové objekty by se nerozpoznaly, protože jsou drženy již jiné
		 * velikost písem.
		 * 
		 * Note:
		 * Bylo by možné využít jen jeden objekt Properties, ale původně se ty hodnoty
		 * pro testování a vytváření načítali vždy nové aktuální, pak se to ale
		 * předělalo na restart aplikace, pak byla doplněna tato možnost pro "rychlou"
		 * změnu velikostí písem apod. Tak jsem to již nechal takto, kdybych se v
		 * budoucnu ještě rozhodl někde něco upravit.
		 */
		classDiagram.setDefaultProperties(GraphClass.KIND_OF_EDGE.getProperties());
		
		
		
		
		
		
		// Uložím data do souboru:
		
		
		/*
		 * Note:
		 * Zde pouze zmíním, že to načítání s komentáři zde dělám proto, že když bych to
		 * takto načítal již při "prvotní" fází a držel ten soubor v aplikaci a uživatel
		 * za běhu aplikace ten soubor upravil, resp. upravil nějaký komentář, tak v
		 * této fází by se ten komentář přepsal na ty co byly načteny a drženy v
		 * aplikaci, takže by to musel uživatle přepsat znovu, nebo by si toho ani
		 * nevšiml, proto si výše načítám a v aplikaci držím pouze soubor s nastavín a
		 * ne ten pro ukládání.
		 */
		
		
		/*
		 * Zde si načtu soubour pro uložení změn pouze z adresáře Workspace, jinak nic
		 * ten z aplikace je pouze pro výchozí nastavení.
		 * 
		 * Toto musím udělat, protože potřebuji načíst příslušný soubor i s komentáři,
		 * vložit do něj nově nastavné velikost (viz výše) a zase jej uložit, protože
		 * když uživatel změní velikost písma a zde se to neuloží, pak se při restartu
		 * aplikace změny nerozeznají.
		 */
		final EditProperties classDiagramProperties = getClassDiagramPropWithComments();
		
		
		
		// Zapíšu data do souboru výše
		if (classDiagramProperties != null) {
			if (isClass)
				classDiagramProperties.setProperty("ClassFontSize", String.valueOf(size));

			if (isComment)
				classDiagramProperties.setProperty("CommentFontSize", String.valueOf(size));

			if (isAssociation)
				classDiagramProperties.setProperty("AscEdgeLineFontSize", String.valueOf(size));

			if (isExtends)
				classDiagramProperties.setProperty("ExtEdgeLineFontSize", String.valueOf(size));

			if (isimplements)
				classDiagramProperties.setProperty("ImpEdgeLineFontSize", String.valueOf(size));

			if (isAggregation_1_1)
				classDiagramProperties.setProperty("AgrEdgeLineFontSize", String.valueOf(size));

			if (isAggregation_1_N)
				classDiagramProperties.setProperty("Agr_1_N_EdgeLineFontSize", String.valueOf(size));

			if (isCommentEdge)
				classDiagramProperties.setProperty("CommentEdgeLineFontSize", String.valueOf(size));

			
			// Uložení do souboru:
			classDiagramProperties.save();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public void quickSetupOfClassFontSize(final int size, final GraphClass classDiagram) {
		/*
		 * Získám si objekt Properties, který je "držen" v aplikaci pro zjišťování typů
		 * objektů v diagramu tříd. Dle tohoto objektu s příslušnými hodnotam ise
		 * rozeznávají veškeré objekty v diagramu tříd
		 */
		final Properties prop = GraphClass.KIND_OF_EDGE.getProperties();
		
		
		/*
		 * Tato podmínka by neměla nikdy nastat a ještě nikdy nenastala, pouze pro
		 * případ nouze, resp. něnajké chyby, ke které by nikdy nemělo dojít.
		 */
		if (prop == null)
			return;
		
		
		
		/*
		 * Typ fontu buňky reprezentující komentář v diagramu tříd.
		 */
		final int fontStyleClass = Integer
				.parseInt(prop.getProperty("ClassFontType", Integer.toString(Constants.CD_FONT_CLASS.getStyle())));
		
		
		
		
		/*
		 * Projdu veškeré objekty v diagramu tříd a všem nastavím nové velikosti fontů /
		 * písma:
		 */
		GraphClass.getCellsList().forEach(c -> {
			final Map<?, ?> attributes = c.getAttributes();

			if (!(c instanceof DefaultEdge) && GraphClass.KIND_OF_EDGE.isClassCell(c))
				GraphConstants.setFont(attributes, new Font(Constants.CD_FONT_CLASS.getName(), fontStyleClass, size));
		});

		
		/*
		 * Je třeba aktualizovat diagram tříd, aby se změny velikosti písmen / fontů
		 * projevily v okně aplikace.
		 */
		classDiagram.updateDiagram(GraphClass.getCellsList());
		
		
		
		/*
		 * Nyní je třeba uložit veškeré změněné hodnoty do objektu Properties, který je
		 * držen v aplikaci, aby se mohli nadále zjišťovat objekty v diagramu tříd, teď
		 * pouze s novými daty.
		 */		
		GraphClass.KIND_OF_EDGE.editValueInProperties("ClassFontSize", String.valueOf(size));
		
		

		
		
		/*
		 * Zde je třeba aktualizovat objekt Properties, který je držen v tomto diagramu
		 * instancí kvůli vytváření nových objektů, kdybych toto neudělal, tak by se
		 * vytvářeli vždy nové objekty, třeba třídy apod. s původními velikostmi fotnů /
		 * písma, ale takové objekty by se nerozpoznaly, protože jsou drženy již jiné
		 * velikost písem.
		 * 
		 * Note:
		 * Bylo by možné využít jen jeden objekt Properties, ale původně se ty hodnoty
		 * pro testování a vytváření načítali vždy nové aktuální, pak se to ale
		 * předělalo na restart aplikace, pak byla doplněna tato možnost pro "rychlou"
		 * změnu velikostí písem apod. Tak jsem to již nechal takto, kdybych se v
		 * budoucnu ještě rozhodl někde něco upravit.
		 */
		classDiagram.setDefaultProperties(GraphClass.KIND_OF_EDGE.getProperties());
		
		
		
		
		
		
		// Uložím data do souboru:
		
		
		/*
		 * Note:
		 * Zde pouze zmíním, že to načítání s komentáři zde dělám proto, že když bych to
		 * takto načítal již při "prvotní" fází a držel ten soubor v aplikaci a uživatel
		 * za běhu aplikace ten soubor upravil, resp. upravil nějaký komentář, tak v
		 * této fází by se ten komentář přepsal na ty co byly načteny a drženy v
		 * aplikaci, takže by to musel uživatle přepsat znovu, nebo by si toho ani
		 * nevšiml, proto si výše načítám a v aplikaci držím pouze soubor s nastavín a
		 * ne ten pro ukládání.
		 */
		
		
		/*
		 * Zde si načtu soubour pro uložení změn pouze z adresáře Workspace, jinak nic
		 * ten z aplikace je pouze pro výchozí nastavení.
		 * 
		 * Toto musím udělat, protože potřebuji načíst příslušný soubor i s komentáři,
		 * vložit do něj nově nastavné velikost (viz výše) a zase jej uložit, protože
		 * když uživatel změní velikost písma a zde se to neuloží, pak se při restartu
		 * aplikace změny nerozeznají.
		 */
		final EditProperties classDiagramProperties = getClassDiagramPropWithComments();
		
		
		
		// Zapíšu data do souboru výše
		if (classDiagramProperties != null) {
			classDiagramProperties.setProperty("ClassFontSize", String.valueOf(size));

			// Uložení do souboru:
			classDiagramProperties.save();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public void quickSetupOfCommentFontSize(final int size, final GraphClass classDiagram) {
		/*
		 * Získám si objekt Properties, který je "držen" v aplikaci pro zjišťování typů
		 * objektů v diagramu tříd. Dle tohoto objektu s příslušnými hodnotam ise
		 * rozeznávají veškeré objekty v diagramu tříd
		 */
		final Properties prop = GraphClass.KIND_OF_EDGE.getProperties();
		
		
		/*
		 * Tato podmínka by neměla nikdy nastat a ještě nikdy nenastala, pouze pro
		 * případ nouze, resp. něnajké chyby, ke které by nikdy nemělo dojít.
		 */
		if (prop == null)
			return;
		
		
		
		/*
		 * Zjistím si typ fontu pro buňku reprezentující komentář v diagramu tříd.
		 */
		final int fontStyleCommentCell = Integer
				.parseInt(prop.getProperty("CommentFontType", Integer.toString(Constants.CD_FONT_COMMENT.getStyle())));
		
		
		
		
		/*
		 * Projdu veškeré objekty v diagramu tříd a všem nastavím nové velikosti fontů /
		 * písma:
		 */
		GraphClass.getCellsList().forEach(c -> {
			final Map<?, ?> attributes = c.getAttributes();

			if (!(c instanceof DefaultEdge) && GraphClass.KIND_OF_EDGE.isCommentCell(c))
				GraphConstants.setFont(attributes,
						new Font(Constants.CD_FONT_COMMENT.getName(), fontStyleCommentCell, size));
		});
		
		/*
		 * Je třeba aktualizovat diagram tříd, aby se změny velikosti písmen / fontů
		 * projevily v okně aplikace.
		 */
		classDiagram.updateDiagram(GraphClass.getCellsList());
		
		
		
		/*
		 * Nyní je třeba uložit veškeré změněné hodnoty do objektu Properties, který je
		 * držen v aplikaci, aby se mohli nadále zjišťovat objekty v diagramu tříd, teď
		 * pouze s novými daty.
		 */		
		GraphClass.KIND_OF_EDGE.editValueInProperties("CommentFontSize", String.valueOf(size));
		
		

		
		
		/*
		 * Zde je třeba aktualizovat objekt Properties, který je držen v tomto diagramu
		 * instancí kvůli vytváření nových objektů, kdybych toto neudělal, tak by se
		 * vytvářeli vždy nové objekty, třeba třídy apod. s původními velikostmi fotnů /
		 * písma, ale takové objekty by se nerozpoznaly, protože jsou drženy již jiné
		 * velikost písem.
		 * 
		 * Note:
		 * Bylo by možné využít jen jeden objekt Properties, ale původně se ty hodnoty
		 * pro testování a vytváření načítali vždy nové aktuální, pak se to ale
		 * předělalo na restart aplikace, pak byla doplněna tato možnost pro "rychlou"
		 * změnu velikostí písem apod. Tak jsem to již nechal takto, kdybych se v
		 * budoucnu ještě rozhodl někde něco upravit.
		 */
		classDiagram.setDefaultProperties(GraphClass.KIND_OF_EDGE.getProperties());
		
		
		
		
		
		
		// Uložím data do souboru:
		
		
		/*
		 * Note:
		 * Zde pouze zmíním, že to načítání s komentáři zde dělám proto, že když bych to
		 * takto načítal již při "prvotní" fází a držel ten soubor v aplikaci a uživatel
		 * za běhu aplikace ten soubor upravil, resp. upravil nějaký komentář, tak v
		 * této fází by se ten komentář přepsal na ty co byly načteny a drženy v
		 * aplikaci, takže by to musel uživatle přepsat znovu, nebo by si toho ani
		 * nevšiml, proto si výše načítám a v aplikaci držím pouze soubor s nastavín a
		 * ne ten pro ukládání.
		 */
		
		
		/*
		 * Zde si načtu soubour pro uložení změn pouze z adresáře Workspace, jinak nic
		 * ten z aplikace je pouze pro výchozí nastavení.
		 * 
		 * Toto musím udělat, protože potřebuji načíst příslušný soubor i s komentáři,
		 * vložit do něj nově nastavné velikost (viz výše) a zase jej uložit, protože
		 * když uživatel změní velikost písma a zde se to neuloží, pak se při restartu
		 * aplikace změny nerozeznají.
		 */
		final EditProperties classDiagramProperties = getClassDiagramPropWithComments();
		
		
		
		// Zapíšu data do souboru výše
		if (classDiagramProperties != null) {
			classDiagramProperties.setProperty("CommentFontSize", String.valueOf(size));

			// Uložení do souboru:
			classDiagramProperties.save();
		}
	}
	
	
	
	
	
	
	
	
	
	
	


	
	@Override
	public void quickSetupOfCommentEdgeFontSize(final int size, final GraphClass classDiagram) {
		/*
		 * Získám si objekt Properties, který je "držen" v aplikaci pro zjišťování typů
		 * objektů v diagramu tříd. Dle tohoto objektu s příslušnými hodnotam ise
		 * rozeznávají veškeré objekty v diagramu tříd
		 */
		final Properties prop = GraphClass.KIND_OF_EDGE.getProperties();
		
		
		/*
		 * Tato podmínka by neměla nikdy nastat a ještě nikdy nenastala, pouze pro
		 * případ nouze, resp. něnajké chyby, ke které by nikdy nemělo dojít.
		 */
		if (prop == null)
			return;
		
		
		
		/*
		 * Zjistím si typ fontu pro hranu reprezentující spojení třídy s komentářem.
		 */
		final int fontStyleCommentEdge = Integer.parseInt(
				prop.getProperty("CommentEdgeLineFontStyle", Integer.toString(Constants.CD_COM_EDGE_FONT.getStyle())));
		
		
		
		
		/*
		 * Projdu veškeré objekty v diagramu tříd a všem nastavím nové velikosti fontů /
		 * písma:
		 */
		GraphClass.getCellsList().forEach(c -> {
			final Map<?, ?> attributes = c.getAttributes();

			if (c instanceof DefaultEdge) {
				final DefaultEdge edge = (DefaultEdge) c;

				if (GraphClass.KIND_OF_EDGE.isCommentEdge(edge))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_COM_EDGE_FONT.getName(), fontStyleCommentEdge, size));
			}
		});

		
		/*
		 * Je třeba aktualizovat diagram tříd, aby se změny velikosti písmen / fontů
		 * projevily v okně aplikace.
		 */
		classDiagram.updateDiagram(GraphClass.getCellsList());
		
		
		
		/*
		 * Nyní je třeba uložit veškeré změněné hodnoty do objektu Properties, který je
		 * držen v aplikaci, aby se mohli nadále zjišťovat objekty v diagramu tříd, teď
		 * pouze s novými daty.
		 */		
		GraphClass.KIND_OF_EDGE.editValueInProperties("CommentEdgeLineFontSize", String.valueOf(size));
		
		

		
		
		/*
		 * Zde je třeba aktualizovat objekt Properties, který je držen v tomto diagramu
		 * instancí kvůli vytváření nových objektů, kdybych toto neudělal, tak by se
		 * vytvářeli vždy nové objekty, třeba třídy apod. s původními velikostmi fotnů /
		 * písma, ale takové objekty by se nerozpoznaly, protože jsou drženy již jiné
		 * velikost písem.
		 * 
		 * Note:
		 * Bylo by možné využít jen jeden objekt Properties, ale původně se ty hodnoty
		 * pro testování a vytváření načítali vždy nové aktuální, pak se to ale
		 * předělalo na restart aplikace, pak byla doplněna tato možnost pro "rychlou"
		 * změnu velikostí písem apod. Tak jsem to již nechal takto, kdybych se v
		 * budoucnu ještě rozhodl někde něco upravit.
		 */
		classDiagram.setDefaultProperties(GraphClass.KIND_OF_EDGE.getProperties());
		
		
		
		
		
		
		// Uložím data do souboru:
		
		
		/*
		 * Note:
		 * Zde pouze zmíním, že to načítání s komentáři zde dělám proto, že když bych to
		 * takto načítal již při "prvotní" fází a držel ten soubor v aplikaci a uživatel
		 * za běhu aplikace ten soubor upravil, resp. upravil nějaký komentář, tak v
		 * této fází by se ten komentář přepsal na ty co byly načteny a drženy v
		 * aplikaci, takže by to musel uživatle přepsat znovu, nebo by si toho ani
		 * nevšiml, proto si výše načítám a v aplikaci držím pouze soubor s nastavín a
		 * ne ten pro ukládání.
		 */
		
		
		/*
		 * Zde si načtu soubour pro uložení změn pouze z adresáře Workspace, jinak nic
		 * ten z aplikace je pouze pro výchozí nastavení.
		 * 
		 * Toto musím udělat, protože potřebuji načíst příslušný soubor i s komentáři,
		 * vložit do něj nově nastavné velikost (viz výše) a zase jej uložit, protože
		 * když uživatel změní velikost písma a zde se to neuloží, pak se při restartu
		 * aplikace změny nerozeznají.
		 */
		final EditProperties classDiagramProperties = getClassDiagramPropWithComments();
		
		
		
		// Zapíšu data do souboru výše
		if (classDiagramProperties != null) {
			classDiagramProperties.setProperty("CommentEdgeLineFontSize", String.valueOf(size));

			// Uložení do souboru:
			classDiagramProperties.save();
		}
	}
	
	
	
	
	
	
	
	
	
	
	

	
	@Override
	public void quickSetupOfAssociationEdgeFontSize(final int size, final GraphClass classDiagram) {
		/*
		 * Získám si objekt Properties, který je "držen" v aplikaci pro zjišťování typů
		 * objektů v diagramu tříd. Dle tohoto objektu s příslušnými hodnotam ise
		 * rozeznávají veškeré objekty v diagramu tříd
		 */
		final Properties prop = GraphClass.KIND_OF_EDGE.getProperties();
		
		
		/*
		 * Tato podmínka by neměla nikdy nastat a ještě nikdy nenastala, pouze pro
		 * případ nouze, resp. něnajké chyby, ke které by nikdy nemělo dojít.
		 */
		if (prop == null)
			return;
		
		

		/*
		 * Získám si typ fontu pro hranu, která reprezentuje vztah asocaice v diagramu
		 * tříd.
		 */
		final int fontStyleAsociation = Integer.parseInt(
				prop.getProperty("AscEdgeLineFontStyle", Integer.toString(Constants.CD_ASC_EDGE_LINE_FONT.getStyle())));
		
		
		
		
		
		/*
		 * Projdu veškeré objekty v diagramu tříd a všem nastavím nové velikosti fontů /
		 * písma:
		 */
		GraphClass.getCellsList().forEach(c -> {
			final Map<?, ?> attributes = c.getAttributes();

			if (c instanceof DefaultEdge) {
				final DefaultEdge edge = (DefaultEdge) c;

				if (GraphClass.KIND_OF_EDGE.isAssociationEdge(edge))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_ASC_EDGE_LINE_FONT.getName(), fontStyleAsociation, size));
			}
		});

		
		/*
		 * Je třeba aktualizovat diagram tříd, aby se změny velikosti písmen / fontů
		 * projevily v okně aplikace.
		 */
		classDiagram.updateDiagram(GraphClass.getCellsList());
		
		
		
		/*
		 * Nyní je třeba uložit veškeré změněné hodnoty do objektu Properties, který je
		 * držen v aplikaci, aby se mohli nadále zjišťovat objekty v diagramu tříd, teď
		 * pouze s novými daty.
		 */		
		GraphClass.KIND_OF_EDGE.editValueInProperties("AscEdgeLineFontSize", String.valueOf(size));				
		
		

		
		
		/*
		 * Zde je třeba aktualizovat objekt Properties, který je držen v tomto diagramu
		 * instancí kvůli vytváření nových objektů, kdybych toto neudělal, tak by se
		 * vytvářeli vždy nové objekty, třeba třídy apod. s původními velikostmi fotnů /
		 * písma, ale takové objekty by se nerozpoznaly, protože jsou drženy již jiné
		 * velikost písem.
		 * 
		 * Note:
		 * Bylo by možné využít jen jeden objekt Properties, ale původně se ty hodnoty
		 * pro testování a vytváření načítali vždy nové aktuální, pak se to ale
		 * předělalo na restart aplikace, pak byla doplněna tato možnost pro "rychlou"
		 * změnu velikostí písem apod. Tak jsem to již nechal takto, kdybych se v
		 * budoucnu ještě rozhodl někde něco upravit.
		 */
		classDiagram.setDefaultProperties(GraphClass.KIND_OF_EDGE.getProperties());
		
		
		
		
		
		
		// Uložím data do souboru:
		
		
		/*
		 * Note:
		 * Zde pouze zmíním, že to načítání s komentáři zde dělám proto, že když bych to
		 * takto načítal již při "prvotní" fází a držel ten soubor v aplikaci a uživatel
		 * za běhu aplikace ten soubor upravil, resp. upravil nějaký komentář, tak v
		 * této fází by se ten komentář přepsal na ty co byly načteny a drženy v
		 * aplikaci, takže by to musel uživatle přepsat znovu, nebo by si toho ani
		 * nevšiml, proto si výše načítám a v aplikaci držím pouze soubor s nastavín a
		 * ne ten pro ukládání.
		 */
		
		
		/*
		 * Zde si načtu soubour pro uložení změn pouze z adresáře Workspace, jinak nic
		 * ten z aplikace je pouze pro výchozí nastavení.
		 * 
		 * Toto musím udělat, protože potřebuji načíst příslušný soubor i s komentáři,
		 * vložit do něj nově nastavné velikost (viz výše) a zase jej uložit, protože
		 * když uživatel změní velikost písma a zde se to neuloží, pak se při restartu
		 * aplikace změny nerozeznají.
		 */
		final EditProperties classDiagramProperties = getClassDiagramPropWithComments();
		
		
		
		// Zapíšu data do souboru výše
		if (classDiagramProperties != null) {
			classDiagramProperties.setProperty("AscEdgeLineFontSize", String.valueOf(size));

			// Uložení do souboru:
			classDiagramProperties.save();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	

	
	@Override
	public void quickSetupOfImplementsEdgeFontSize(final int size, final GraphClass classDiagram) {
		/*
		 * Získám si objekt Properties, který je "držen" v aplikaci pro zjišťování typů
		 * objektů v diagramu tříd. Dle tohoto objektu s příslušnými hodnotam ise
		 * rozeznávají veškeré objekty v diagramu tříd
		 */
		final Properties prop = GraphClass.KIND_OF_EDGE.getProperties();
		
		
		/*
		 * Tato podmínka by neměla nikdy nastat a ještě nikdy nenastala, pouze pro
		 * případ nouze, resp. něnajké chyby, ke které by nikdy nemělo dojít.
		 */
		if (prop == null)
			return;
		
		
		
		/*
		 * Zjistím si typ fontu / písma pro hranu, která v diagramu tříd reprezentuje
		 * vztah typu implementace rozhraní.
		 */
		final int fontStyleImplements = Integer.parseInt(
				prop.getProperty("ImpEdgeLineFontStyle", Integer.toString(Constants.CD_IMP_EDGE_LINE_FONT.getStyle())));
		
		
		
		
		/*
		 * Projdu veškeré objekty v diagramu tříd a všem nastavím nové velikosti fontů /
		 * písma:
		 */
		GraphClass.getCellsList().forEach(c -> {
			final Map<?, ?> attributes = c.getAttributes();

			if (c instanceof DefaultEdge) {
				final DefaultEdge edge = (DefaultEdge) c;

				if (GraphClass.KIND_OF_EDGE.isImplementsEdge(edge))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_IMP_EDGE_LINE_FONT.getName(), fontStyleImplements, size));
			}
		});

		
		/*
		 * Je třeba aktualizovat diagram tříd, aby se změny velikosti písmen / fontů
		 * projevily v okně aplikace.
		 */
		classDiagram.updateDiagram(GraphClass.getCellsList());
		
		
		
		/*
		 * Nyní je třeba uložit veškeré změněné hodnoty do objektu Properties, který je
		 * držen v aplikaci, aby se mohli nadále zjišťovat objekty v diagramu tříd, teď
		 * pouze s novými daty.
		 */		
		GraphClass.KIND_OF_EDGE.editValueInProperties("ImpEdgeLineFontSize", String.valueOf(size));
		
		

		
		
		/*
		 * Zde je třeba aktualizovat objekt Properties, který je držen v tomto diagramu
		 * instancí kvůli vytváření nových objektů, kdybych toto neudělal, tak by se
		 * vytvářeli vždy nové objekty, třeba třídy apod. s původními velikostmi fotnů /
		 * písma, ale takové objekty by se nerozpoznaly, protože jsou drženy již jiné
		 * velikost písem.
		 * 
		 * Note:
		 * Bylo by možné využít jen jeden objekt Properties, ale původně se ty hodnoty
		 * pro testování a vytváření načítali vždy nové aktuální, pak se to ale
		 * předělalo na restart aplikace, pak byla doplněna tato možnost pro "rychlou"
		 * změnu velikostí písem apod. Tak jsem to již nechal takto, kdybych se v
		 * budoucnu ještě rozhodl někde něco upravit.
		 */
		classDiagram.setDefaultProperties(GraphClass.KIND_OF_EDGE.getProperties());
		
		
		
		
		
		
		// Uložím data do souboru:
		
		
		/*
		 * Note:
		 * Zde pouze zmíním, že to načítání s komentáři zde dělám proto, že když bych to
		 * takto načítal již při "prvotní" fází a držel ten soubor v aplikaci a uživatel
		 * za běhu aplikace ten soubor upravil, resp. upravil nějaký komentář, tak v
		 * této fází by se ten komentář přepsal na ty co byly načteny a drženy v
		 * aplikaci, takže by to musel uživatle přepsat znovu, nebo by si toho ani
		 * nevšiml, proto si výše načítám a v aplikaci držím pouze soubor s nastavín a
		 * ne ten pro ukládání.
		 */
		
		
		/*
		 * Zde si načtu soubour pro uložení změn pouze z adresáře Workspace, jinak nic
		 * ten z aplikace je pouze pro výchozí nastavení.
		 * 
		 * Toto musím udělat, protože potřebuji načíst příslušný soubor i s komentáři,
		 * vložit do něj nově nastavné velikost (viz výše) a zase jej uložit, protože
		 * když uživatel změní velikost písma a zde se to neuloží, pak se při restartu
		 * aplikace změny nerozeznají.
		 */
		final EditProperties classDiagramProperties = getClassDiagramPropWithComments();
		
		
		
		// Zapíšu data do souboru výše
		if (classDiagramProperties != null) {
			classDiagramProperties.setProperty("ImpEdgeLineFontSize", String.valueOf(size));

			// Uložení do souboru:
			classDiagramProperties.save();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	


	
	@Override
	public void quickSetupOfExtendsEdgeFontSize(final int size, final GraphClass classDiagram) {
		/*
		 * Získám si objekt Properties, který je "držen" v aplikaci pro zjišťování typů
		 * objektů v diagramu tříd. Dle tohoto objektu s příslušnými hodnotam ise
		 * rozeznávají veškeré objekty v diagramu tříd
		 */
		final Properties prop = GraphClass.KIND_OF_EDGE.getProperties();
		
		
		/*
		 * Tato podmínka by neměla nikdy nastat a ještě nikdy nenastala, pouze pro
		 * případ nouze, resp. něnajké chyby, ke které by nikdy nemělo dojít.
		 */
		if (prop == null)
			return;
		
		
		
		
		/*
		 * Zjistím si typ fontu pro hranu, která v diagramu tříd reprezentuje dědičnost.
		 */
		final int fontStyleInherit = Integer.parseInt(
				prop.getProperty("ExtEdgeLineFontStyle", Integer.toString(Constants.CD_EXT_EDGE_LINE_FONT.getStyle())));
		
		
		
		
		/*
		 * Projdu veškeré objekty v diagramu tříd a všem nastavím nové velikosti fontů /
		 * písma:
		 */
		GraphClass.getCellsList().forEach(c -> {
			final Map<?, ?> attributes = c.getAttributes();

			if (c instanceof DefaultEdge) {
				final DefaultEdge edge = (DefaultEdge) c;

				if (GraphClass.KIND_OF_EDGE.isInheritEdge(edge))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_EXT_EDGE_LINE_FONT.getName(), fontStyleInherit, size));
			}
		});


		/*
		 * Je třeba aktualizovat diagram tříd, aby se změny velikosti písmen / fontů
		 * projevily v okně aplikace.
		 */
		classDiagram.updateDiagram(GraphClass.getCellsList());
		
		
		
		/*
		 * Nyní je třeba uložit veškeré změněné hodnoty do objektu Properties, který je
		 * držen v aplikaci, aby se mohli nadále zjišťovat objekty v diagramu tříd, teď
		 * pouze s novými daty.
		 */
		GraphClass.KIND_OF_EDGE.editValueInProperties("ExtEdgeLineFontSize", String.valueOf(size));
		

		
		
		/*
		 * Zde je třeba aktualizovat objekt Properties, který je držen v tomto diagramu
		 * instancí kvůli vytváření nových objektů, kdybych toto neudělal, tak by se
		 * vytvářeli vždy nové objekty, třeba třídy apod. s původními velikostmi fotnů /
		 * písma, ale takové objekty by se nerozpoznaly, protože jsou drženy již jiné
		 * velikost písem.
		 * 
		 * Note:
		 * Bylo by možné využít jen jeden objekt Properties, ale původně se ty hodnoty
		 * pro testování a vytváření načítali vždy nové aktuální, pak se to ale
		 * předělalo na restart aplikace, pak byla doplněna tato možnost pro "rychlou"
		 * změnu velikostí písem apod. Tak jsem to již nechal takto, kdybych se v
		 * budoucnu ještě rozhodl někde něco upravit.
		 */
		classDiagram.setDefaultProperties(GraphClass.KIND_OF_EDGE.getProperties());
		
		
		
		
		
		
		// Uložím data do souboru:
		
		
		/*
		 * Note:
		 * Zde pouze zmíním, že to načítání s komentáři zde dělám proto, že když bych to
		 * takto načítal již při "prvotní" fází a držel ten soubor v aplikaci a uživatel
		 * za běhu aplikace ten soubor upravil, resp. upravil nějaký komentář, tak v
		 * této fází by se ten komentář přepsal na ty co byly načteny a drženy v
		 * aplikaci, takže by to musel uživatle přepsat znovu, nebo by si toho ani
		 * nevšiml, proto si výše načítám a v aplikaci držím pouze soubor s nastavín a
		 * ne ten pro ukládání.
		 */
		
		
		/*
		 * Zde si načtu soubour pro uložení změn pouze z adresáře Workspace, jinak nic
		 * ten z aplikace je pouze pro výchozí nastavení.
		 * 
		 * Toto musím udělat, protože potřebuji načíst příslušný soubor i s komentáři,
		 * vložit do něj nově nastavné velikost (viz výše) a zase jej uložit, protože
		 * když uživatel změní velikost písma a zde se to neuloží, pak se při restartu
		 * aplikace změny nerozeznají.
		 */
		final EditProperties classDiagramProperties = getClassDiagramPropWithComments();
		
		
		
		// Zapíšu data do souboru výše
		if (classDiagramProperties != null) {
			classDiagramProperties.setProperty("ExtEdgeLineFontSize", String.valueOf(size));

			// Uložení do souboru:
			classDiagramProperties.save();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	

	
	@Override
	public void quickSetupOfAggregation_1_1_EdgeFontSize(final int size, final GraphClass classDiagram) {
		/*
		 * Získám si objekt Properties, který je "držen" v aplikaci pro zjišťování typů
		 * objektů v diagramu tříd. Dle tohoto objektu s příslušnými hodnotam ise
		 * rozeznávají veškeré objekty v diagramu tříd
		 */
		final Properties prop = GraphClass.KIND_OF_EDGE.getProperties();
		
		
		/*
		 * Tato podmínka by neměla nikdy nastat a ještě nikdy nenastala, pouze pro
		 * případ nouze, resp. něnajké chyby, ke které by nikdy nemělo dojít.
		 */
		if (prop == null)
			return;
		
		
		
		/*
		 * Zjistím si typ písma / fontu pro hranu, která reprezentuje vztah typu
		 * agregace 1 : 1 v diagramu tříd.
		 */
		final int fontStyleAgreagation_1_ku_1 = Integer.parseInt(
				prop.getProperty("AgrEdgeLineFontStyle", Integer.toString(Constants.CD_AGR_EDGE_LINE_FONT.getStyle())));
		
		
		
		
		/*
		 * Projdu veškeré objekty v diagramu tříd a všem nastavím nové velikosti fontů /
		 * písma:
		 */
		GraphClass.getCellsList().forEach(c -> {
			final Map<?, ?> attributes = c.getAttributes();

			if (c instanceof DefaultEdge) {
				final DefaultEdge edge = (DefaultEdge) c;

				if (GraphClass.KIND_OF_EDGE.isAggregation_1_ku_1_Edge(edge))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_AGR_EDGE_LINE_FONT.getName(), fontStyleAgreagation_1_ku_1, size));
			}
		});

		
		/*
		 * Je třeba aktualizovat diagram tříd, aby se změny velikosti písmen / fontů
		 * projevily v okně aplikace.
		 */
		classDiagram.updateDiagram(GraphClass.getCellsList());
		
		
		
		/*
		 * Nyní je třeba uložit veškeré změněné hodnoty do objektu Properties, který je
		 * držen v aplikaci, aby se mohli nadále zjišťovat objekty v diagramu tříd, teď
		 * pouze s novými daty.
		 */		
		GraphClass.KIND_OF_EDGE.editValueInProperties("AgrEdgeLineFontSize", String.valueOf(size));
		
		

		
		
		/*
		 * Zde je třeba aktualizovat objekt Properties, který je držen v tomto diagramu
		 * instancí kvůli vytváření nových objektů, kdybych toto neudělal, tak by se
		 * vytvářeli vždy nové objekty, třeba třídy apod. s původními velikostmi fotnů /
		 * písma, ale takové objekty by se nerozpoznaly, protože jsou drženy již jiné
		 * velikost písem.
		 * 
		 * Note:
		 * Bylo by možné využít jen jeden objekt Properties, ale původně se ty hodnoty
		 * pro testování a vytváření načítali vždy nové aktuální, pak se to ale
		 * předělalo na restart aplikace, pak byla doplněna tato možnost pro "rychlou"
		 * změnu velikostí písem apod. Tak jsem to již nechal takto, kdybych se v
		 * budoucnu ještě rozhodl někde něco upravit.
		 */
		classDiagram.setDefaultProperties(GraphClass.KIND_OF_EDGE.getProperties());
		
		
		
		
		
		
		// Uložím data do souboru:
		
		
		/*
		 * Note:
		 * Zde pouze zmíním, že to načítání s komentáři zde dělám proto, že když bych to
		 * takto načítal již při "prvotní" fází a držel ten soubor v aplikaci a uživatel
		 * za běhu aplikace ten soubor upravil, resp. upravil nějaký komentář, tak v
		 * této fází by se ten komentář přepsal na ty co byly načteny a drženy v
		 * aplikaci, takže by to musel uživatle přepsat znovu, nebo by si toho ani
		 * nevšiml, proto si výše načítám a v aplikaci držím pouze soubor s nastavín a
		 * ne ten pro ukládání.
		 */
		
		
		/*
		 * Zde si načtu soubour pro uložení změn pouze z adresáře Workspace, jinak nic
		 * ten z aplikace je pouze pro výchozí nastavení.
		 * 
		 * Toto musím udělat, protože potřebuji načíst příslušný soubor i s komentáři,
		 * vložit do něj nově nastavné velikost (viz výše) a zase jej uložit, protože
		 * když uživatel změní velikost písma a zde se to neuloží, pak se při restartu
		 * aplikace změny nerozeznají.
		 */
		final EditProperties classDiagramProperties = getClassDiagramPropWithComments();
		
		
		
		// Zapíšu data do souboru výše
		if (classDiagramProperties != null) {
			classDiagramProperties.setProperty("AgrEdgeLineFontSize", String.valueOf(size));

			// Uložení do souboru:
			classDiagramProperties.save();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	


	
	@Override
	public void quickSetupOfAggregation_1_N_EdgeFontSize(final int size, final GraphClass classDiagram) {
		/*
		 * Získám si objekt Properties, který je "držen" v aplikaci pro zjišťování typů
		 * objektů v diagramu tříd. Dle tohoto objektu s příslušnými hodnotam ise
		 * rozeznávají veškeré objekty v diagramu tříd
		 */
		final Properties prop = GraphClass.KIND_OF_EDGE.getProperties();
		
		
		/*
		 * Tato podmínka by neměla nikdy nastat a ještě nikdy nenastala, pouze pro
		 * případ nouze, resp. něnajké chyby, ke které by nikdy nemělo dojít.
		 */
		if (prop == null)
			return;
		
		
		
		/*
		 * Zjistím si typ fontu / písma pro hranu, která v diagramu tříd reprezentuje
		 * vztah typu agregace 1 : N.
		 */
		final int fontStyleAgreagation_1_ku_N = Integer.parseInt(prop.getProperty("Agr_1_N_EdgeLineFontStyle",
				Integer.toString(Constants.CD_AGR_EDGE_N_LINE_FONT.getStyle())));
		
		
		
		
		/*
		 * Projdu veškeré objekty v diagramu tříd a všem nastavím nové velikosti fontů /
		 * písma:
		 */
		GraphClass.getCellsList().forEach(c -> {
			final Map<?, ?> attributes = c.getAttributes();

			if (c instanceof DefaultEdge) {
				final DefaultEdge edge = (DefaultEdge) c;

				if (GraphClass.KIND_OF_EDGE.isAggregation_1_ku_N_Edge(edge))
					GraphConstants.setFont(attributes,
							new Font(Constants.CD_AGR_EDGE_N_LINE_FONT.getName(), fontStyleAgreagation_1_ku_N, size));
			}
		});

		
		/*
		 * Je třeba aktualizovat diagram tříd, aby se změny velikosti písmen / fontů
		 * projevily v okně aplikace.
		 */
		classDiagram.updateDiagram(GraphClass.getCellsList());
		
		
		
		/*
		 * Nyní je třeba uložit veškeré změněné hodnoty do objektu Properties, který je
		 * držen v aplikaci, aby se mohli nadále zjišťovat objekty v diagramu tříd, teď
		 * pouze s novými daty.
		 */		
		GraphClass.KIND_OF_EDGE.editValueInProperties("Agr_1_N_EdgeLineFontSize", String.valueOf(size));
		
		

		
		
		/*
		 * Zde je třeba aktualizovat objekt Properties, který je držen v tomto diagramu
		 * instancí kvůli vytváření nových objektů, kdybych toto neudělal, tak by se
		 * vytvářeli vždy nové objekty, třeba třídy apod. s původními velikostmi fotnů /
		 * písma, ale takové objekty by se nerozpoznaly, protože jsou drženy již jiné
		 * velikost písem.
		 * 
		 * Note:
		 * Bylo by možné využít jen jeden objekt Properties, ale původně se ty hodnoty
		 * pro testování a vytváření načítali vždy nové aktuální, pak se to ale
		 * předělalo na restart aplikace, pak byla doplněna tato možnost pro "rychlou"
		 * změnu velikostí písem apod. Tak jsem to již nechal takto, kdybych se v
		 * budoucnu ještě rozhodl někde něco upravit.
		 */
		classDiagram.setDefaultProperties(GraphClass.KIND_OF_EDGE.getProperties());
		
		
		
		
		
		
		// Uložím data do souboru:
		
		
		/*
		 * Note:
		 * Zde pouze zmíním, že to načítání s komentáři zde dělám proto, že když bych to
		 * takto načítal již při "prvotní" fází a držel ten soubor v aplikaci a uživatel
		 * za běhu aplikace ten soubor upravil, resp. upravil nějaký komentář, tak v
		 * této fází by se ten komentář přepsal na ty co byly načteny a drženy v
		 * aplikaci, takže by to musel uživatle přepsat znovu, nebo by si toho ani
		 * nevšiml, proto si výše načítám a v aplikaci držím pouze soubor s nastavín a
		 * ne ten pro ukládání.
		 */
		
		
		/*
		 * Zde si načtu soubour pro uložení změn pouze z adresáře Workspace, jinak nic
		 * ten z aplikace je pouze pro výchozí nastavení.
		 * 
		 * Toto musím udělat, protože potřebuji načíst příslušný soubor i s komentáři,
		 * vložit do něj nově nastavné velikost (viz výše) a zase jej uložit, protože
		 * když uživatel změní velikost písma a zde se to neuloží, pak se při restartu
		 * aplikace změny nerozeznají.
		 */
		final EditProperties classDiagramProperties = getClassDiagramPropWithComments();
		
		
		
		// Zapíšu data do souboru výše
		if (classDiagramProperties != null) {
			classDiagramProperties.setProperty("Agr_1_N_EdgeLineFontSize", String.valueOf(size));

			// Uložení do souboru:
			classDiagramProperties.save();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro získání objektu Properties, který obsahuje nastavení
	 * pro diagram tříd, ale tento objekt Properties se načte tak, aby se zachovaly
	 * komentáře.
	 * 
	 * @return objekt Properties obsahující nastavení pro diagram tříd se
	 *         zachovalými komentáři pro soubor Properties.
	 */
	private static EditProperties getClassDiagramPropWithComments() {
		EditProperties classDiagramProperties = App.READ_FILE
				.getPropertiesInWorkspacePersistComments(Constants.CLASS_DIAGRAM_NAME);
		
		if (classDiagramProperties == null) {			
			final String pathToWorkspace = ReadFile.getPathToWorkspace();

			/*
			 * Zde pouze zjisšťuji, zda existují veškeré adresáře k cestě pro soubor
			 * ClassDiagram.properties v adresáři configuration ve workspace. Pokud ne, pak v
			 * rámci možností nekteré adresáře mohu vytvořit, jinak není kam uložit soubory,
			 * pokud například byl smazan workspace apod. Pak už zbývá jedině znovu vytvořit
			 * adresář workspace s projekty apod. Ale tuto část zde vynechávám, protože o
			 * tom uživatel musí vědět - že si smazal projekty případně workspace apod. Tak
			 * snad musí vědět, že nelze pracovat s projekty / soubory, které byly smazány.
			 * 
			 * Otestuji, zda existuje workspace, pak configuration, pak příslušný soubor,
			 * případně některé z nich mohu vytvořit:
			 */
			if (pathToWorkspace != null) {
				/*
				 * Pokud neexsistuje adresář configuration ve workspace, bude vytvořen i s
				 * potřebnými soubory s výchozím nastavením.
				 * 
				 * Pokud adresář configuration ve workspace existuje, pak stačí vytvořit jen
				 * soubor ClassDiagram.properties, protože vím, že neexistuje, když se jej
				 * nepodařilo načíst.
				 */
				if (ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
					ConfigurationFiles.writeClassDiagramProperties(
							pathToWorkspace + File.separator + Constants.PATH_TO_CLASS_DIAGRAM_PROPERTIES);

				else
					WriteToFile.createConfigureFilesInPath(pathToWorkspace);

				// Znovu načtu soubor Default.properties z workspace z configuration, protože
				// výše byl znovu vytvořn:
				classDiagramProperties = App.READ_FILE
						.getPropertiesInWorkspacePersistComments(Constants.CLASS_DIAGRAM_NAME);
			}
			/*
			 * část else:
			 * 
			 * Zde by bylo možné vytvořit dříve zadaný adresář workspace - pokud byl již
			 * zadán, ale byl smazán, ale to by v rámci aplikace nemělo nastat, proto tuto
			 * část vynechám, uživatel jej musí smazat sám, pak o tom musí vědět.
			 */
		}
		
		return classDiagramProperties;
	}
}
