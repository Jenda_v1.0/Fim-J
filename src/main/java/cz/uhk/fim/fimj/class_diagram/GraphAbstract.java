package cz.uhk.fim.fimj.class_diagram;

import java.awt.*;
import java.util.List;
import java.util.Properties;

import javax.swing.JOptionPane;

import org.jgraph.JGraph;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphLayoutCache;
import org.jgraph.graph.GraphModel;

import cz.uhk.fim.fimj.font_size_form.CheckDuplicatesInDiagramsInterface;
import cz.uhk.fim.fimj.output_editor.OutputEditor;

/**
 * Tato třída obsahuje pouze některé "společné" metody, jak pro diagram tříd, tak i pro diagram intancí, ale nejsou zde
 * již příslušné kolekce pro objekty, atd. protože jsou statické a final, to by pak by jeden list pro obsa diagramy, což
 * je špatně, alespoň pro tentokonkrétní případ, protože si pomocí statického přístupu přistupujii k této kolekce, a
 * některým dalším věděm, ale pro obsa diagramy zvlášt, proto nemohou být společné! - statické
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class GraphAbstract extends JGraph {

    private static final long serialVersionUID = -1583042460150553006L;


    /**
     * "Komponenta", která obsahuje metody pro zjištění duplicitních hodnot v diagramu tříd a v diagramu instnací,
     * například když uživatel změní velikosti písma / fontu pomocí příslušných klávesových zkratek (Ctrl a + nebo -),
     * tak se ve všech objektech v příslušném diagramu změní velikosti písma, ale dále je třeba testovat, jestli nemají
     * ty objekty stejné nastavení, protože by je jinak nebylo možné rozeznat. Tak se jim nastaví "nouzové" výchozí
     * hodnoty a oznámí se to uživateli.
     */
    protected static CheckDuplicatesInDiagramsInterface checkDuplicatesInDiagrams;


    /**
     * Proměnná, do které se bude vkládat výchozí nastavení pro příslušný diagram, - nepredstavuje to default.proeprteis
     * ale Class nebo Instance diagram.properties, tento název byl zvolet pouze defaultně pro oba diagram, kde se tato
     * proměná používá
     */
    protected Properties defaultProperties;


    /**
     * Proměnná, která bude držet informace s texty pro aplikaci v uživatelem zvoleném jazyce
     */
    protected Properties languageProperties;


    /**
     * Pro výpis informací, například o kompilaci v diagramu tříd, nebo pro vypsání informací o výsledku provedění
     * metody v diagramu instancí, apod.
     */
    protected OutputEditor outputEditor;


    // Proměnné pro výpis při smazání objektů - na to by nemělo dojít,
    // ale je dobré, když už by k tomu došlo, aby o tom uživatel věděl:
    protected String missingObjectInGraphText;
    protected String missingObjectInGraphTitle;


    /**
     * Proměnná, která značí, zda se budou zobrazovat v diagramu tříd nebo instancí vztahy na sebe sama.
     * <p>
     * Tj. když bude třeba ve třídě proměnná typu té samé třídy (třída má vztah na sebe sama), tak, zda se tento vztah
     * má zobrazovat v diagramu tříd. Pokud se jedná o naplnění proměnné v instanci dané třídy, tak zda příslušná
     * proměnná naplněná tou samou instancí třídy, která je v dané proměnné.
     */
    protected boolean showRelationShipsToItself;


    /**
     * Logická proměnná, dle které se pozná, zda se v kontextovém menu nad třídou v diagramu tříd a nad instancí v
     * diagramu instancí mají zpřístupnit i metody, které jsou privátní (označené klíčovým slouvem private). True,
     * zpřístupní se i privátní metody, false -> budou zpřístupněny pouze veřejné popř. jiné povolené metody.
     */
    protected boolean includePrivateMethods;


    /**
     * Logická proměnná, dle které se pzná, zda se v kontextovém menu nad třídou v diagramu tříd a nebo nad instancí v
     * diagramu instancí mají zpřístupnit i metody, které jsou chráněné (označené klíčovým slovem protected). True,
     * zpřístupní se i chráněné metody, false -> budou zppřístupněny, pouze veřejné, popř. jiné nastavené metody
     * (private nebo package-private)
     */
    protected boolean includeProtectedMethods;


    /**
     * Logická proměnná, dle které se pozná, zda se v kontextovém menu nad třídou v diagramu tříd nebo nad instancí v
     * diagramu instancí mají zobrazit metody, které jsou označené viditelností package-private (pouze v rámci balíčku).
     * True, zpřístupní se i package-private metody, jinak když bude tato proměnná false, budou zpřístupněny, pouze
     * veřejné, popř. jiné povolené metody (private a protected).
     */
    protected boolean includePackagePrivateMethods;


    /**
     * Logická proměnné, dle které se pozná, zda se mají v dialogu pro zavolání metody zpřístupnít veřejné a chráněné
     * proměnné pro předání do příslušného parametru metody. Jednalo by se pouze o veřejné a chráněné (popř. staticé)
     * proměnné, které se nachází v třídách v diagramu tříd, v instancích v diagramu instancí a proměnné vytvořené
     * uživatelem v editoru příkazů. A vždy by se jedna o proměnné, které se vyfiltrovaly ze všech nalezených
     * proměnných, které jsou stejného datového typu, který potřebuje příslušný parametr metody.
     */
    protected boolean makeVariablesAvailableForCallMethod;


    /**
     * Logická proměnná, která značí, zda se mají při zavolání metody nad instancí třídy v diagramu instancí nebo nad
     * třídou v diagramu tříd pomocí kontextového menu zpřístupnit možnost pro vytvoření nové instance. V případě, že se
     * jedná o parametr metody typu nějaké třídy, která se nachází v diagramu tříd, tak aby bylo na výběr, že se může
     * vytvořit nová instance nebo předat jedna z existujících instancí v diagramu instancí.
     */
    protected boolean showCreateNewInstanceOptionInCallMethod;


    /**
     * Logická proměnná, která značí, zda se mají v dialogu pro zavolání metody zpřístupnit metody, jejíž návratová
     * hodnota se předá do příslušného parametru metody.
     * <p>
     * Jde o to, že uživatel má možnost pro předání do parametru metody návratovou hodnotu z příslušné metody která
     * vrací stejný datový typ, který potřebuje příslušný parametr.
     */
    protected boolean makeMethodsAvailableForCallMethod;


    /**
     * Logická proměnná, která značí, zda se má vztah typu asociace v diagramu tříd nebo v diagramu instancí zobrazit
     * jako klasická asociace, tj. jednou hranou s šipkami na obou koncích nebo pomocí dvou agregací 1 : 1.
     */
    protected boolean showAssociationThroughAggregation;


    /**
     * Proměnná, která značí, zda se v kontextovém menu nad třídou v položce "Zděděné statické metody" v diagramu tříd
     * nebo nad instancí v kontextovém menu "Zděděné metody" v diagramu instnací zobrazit ty názvy tříd i s balíčky nebo
     * jen samotný název třídy (bez balíčků).
     */
    protected boolean showInheritedClassNameWithPackages;


    /**
     * Konstruktor, který pouze zavolá konstruktor z třídy, ze které tato dědí - z grafu
     * <p>
     * Note: pro proměnné výše (v parametru tohoto konstruktoru) lze najít více informací v dokumntaci na straně 51
     *
     * @param model
     *         - "model grafu"
     * @param view
     *         - "model ro zorbazení"
     */
    public GraphAbstract(final GraphModel model, final GraphLayoutCache view) {
        super(model, view);

        /*
         * Zdroj:
         *
         * https://www.databaseadm.com/article/11381780/JList+KeyListener+throws+exception+on+VK_DELETE
         *
         * (Jedná se o poslední chybu "null XORColor exception")
         *
         * Tuto hodnotu je třeba nastavit kvůli tomu, že se do aplikace přidala možnost / funkce pro nastavení Look
         * and Feel.
         *
         * Využitá knihovna pro diagramy tříd a instanci vyhazuje výjimku v případě, že se aplikuje jeden z
         * dostupných Look and Feel pro využívanou platformu.
         *
         * Když se v diagramu tříd nebo instancí pokusí uživatel označit něco myši, tedy například stisknout a držet
         * levé tlačítko myši a bude posouvat kurzor přes diagramy, tak bude vyskakovat výjimka popsaná ve výše
         * uvedeném zdroji. Tam je také informace k tomu, že se zde musí nastavit barva "rámečku pro označení objektů
         * v diagramech", aby nevyskakovala ta výjimka.
         */
        setMarqueeColor(Color.GRAY);
    }


    /**
     * Metoda, která do grafu přidá mouse wheel listener, konkrétní použití je na "Scalování" - přibližování grafu - Aby
     * když uživatel drří klávesy CTRL a kolečkem myši roluje nahoru nebo dolů, tak se graf bude přibližovat nebo
     * oddalovat
     */
    protected void addMouseWheelListener() {
        addMouseWheelListener(e -> {

            if (e.getWheelRotation() < 0 && e.isControlDown()) { // uživatel roluje kolečkem myši nahoru
                double scale = getScale();

                if (scale <= 20) {
                    scale += 0.1;
                    setScale(scale);
                }
            }

            else if (e.getWheelRotation() > 0 && e.isControlDown()) { // Uživatel roluje kolečkem myši dolů
                double scale = getScale();

                if (scale > 0.5) {
                    scale -= 0.1;
                    setScale(scale);
                }
            }
        });
    }


    /**
     * Metoda, která aktualizuje diagram.
     *
     * @param cellsList
     *         - kolekce s objekty, které se nachází v diagramu, například se přidala nějaká nová buňka (objekt) nebo
     *         naopak odebral, apod. Tak je třeba aktualizovat příslšný diagram
     */
    public final void updateDiagram(final List<DefaultGraphCell> cellsList) {
        getGraphLayoutCache().insert(cellsList.toArray());

        refresh();
    }


    /**
     * Metoda, která vymažu příslušný diagram (tříd nebo instancí) a aktualizuje jej
     *
     * @param cellsList
     *         - kolekce, coby list objektů jednoho ze zmíněných diagramů, která se ma vymazat
     */
    public final void clearGraph(final List<DefaultGraphCell> cellsList) {
        getModel().remove(cellsList.toArray());

        cellsList.clear();

        updateDiagram(cellsList);
    }


    /**
     * Metoda, která odebere z příslušné kolekce coby diagramu (tříd nebo instancí) příslušný objekt (pokud ještě
     * existuje - vždy by mělo) a aktualizuje příslušný diagram
     *
     * @param object
     *         - objekt z příslušného diagramu, který se má smazat
     * @param cellsList
     *         - kolekce - list s objekty z příslušného diagramu, ze které se má objekt (object) smazat
     */
    public final void deleteObjectFromGraph(final Object object, final List<DefaultGraphCell> cellsList) {
        if (cellsList.contains(object)) {
            // odeberu objekt z kolekce objektů
            cellsList.remove(object);

            // Odeberu objekt z modelu grafu
            if (getModel().contains(object))
                getModel().remove(new Object[]{object});

            updateDiagram(cellsList);
        }

        // Toto by nastat nemělo, leda by uživatel smazal z adresáře src v projektu
        // třídu a graf se nepřenačetl, nebo je parametr metody
        // objekt, který nemá s grafem nic společnohé, např nějaké tlačítko, či jiná
        // komponenta aplikace, ale to by nastat nemělo
        else
            JOptionPane.showMessageDialog(this, missingObjectInGraphText, missingObjectInGraphTitle,
                    JOptionPane.ERROR_MESSAGE);
    }


    /**
     * Metoda, která odebere z modelu objektů vykreslených v diagramu tříd nebo instanci příslušný objekt a aktualizuje
     * diagram.
     *
     * @param object
     *         - objekt z příslušného diagramu, který se má smazat
     * @param cellsList
     *         - kolekce - list s objekty z příslušného diagramu, ze které se má objekt (object) smazat
     */
    public final void deleteObjectFromModelOfGraph(final Object object, final List<DefaultGraphCell> cellsList) {
        // Odeberu objekt z modelu grafu
        if (getModel().contains(object))
            getModel().remove(new Object[]{object});

        updateDiagram(cellsList);
    }


    /**
     * Getr na proměnnou, která značí, zda semají v tomto diagramu (tříd nebo instancí) zobrazovat vztahy třídy nebo
     * instance na sebe sama.
     *
     * @return true nebo false, dle hodnoty proměnné, True značí, že se mají zobrazovat vztahy třídy nebo instance na
     * sebe sama, false že se nebudou zobrazovat vztahy třídy nebo instance na sebe sama.
     */
    public final boolean isShowRelationShipsToItself() {
        return showRelationShipsToItself;
    }


    /**
     * Getr na logickou proměnnou, která značí, zda se má v dialogu pro zavolání metody zpřístupnit možnost pro
     * vytvoření nové instance příslušné třídy. Pokud je v parametru příslušné metody parametr typu nějaké třídy, která
     * se nachází v diagramu tříd.
     *
     * @return popsáno výše
     */
    public boolean isShowCreateNewInstanceOptionInCallMethod() {
        return showCreateNewInstanceOptionInCallMethod;
    }


    /**
     * Getr na proměnou, která značí, zda se má v dialogu pro zavolání metody nebo konstruktoru zobrazit možnost pro
     * zavolání metody, aby se její návratová hodnota předala do příslušného parametru.
     *
     * @return true, pokud se mají v dialogu pro zavolání metody nebo konstruktoru zobrazit možnosti pro zavolání
     * metody, aby se její návratová hodnota předala do příslušného parametru metody nebo konstruktoru, jinak false,
     * pokud semetody nemají dávat na výběr jako hodnota pro předání do parametru přslušné metody nebo konstruktoru.
     */
    public boolean isMakeMethodsAvailable() {
        return makeMethodsAvailableForCallMethod;
    }


    /**
     * Getr na proměnnou, která značí, zda se má vztah typu asociace znázornit jako klasická asociace pomocí jedné hrany
     * s šipkami na obou koncích nebo pomocí dvou agregací.
     *
     * @return - true pro znázornění asociace pomocí dvou agregací, jinak false.
     */
    public boolean isShowAssociationThroughAggregation() {
        return showAssociationThroughAggregation;
    }


    /**
     * Getr na proměnnou, která obsahuje referenci na objekt, který obsahuje veškeré nastavení pro diagram tříd nebo
     * diagram instancí.
     *
     * @return objekt Properties s aktuálním nastavením pro diagram tříd nebo diagram instancí.
     */
    public Properties getDefaultProperties() {
        return defaultProperties;
    }


    /**
     * Setr na proměnnou, která obsahuje referenci na objekt, který obsahuje nastavení pro diagram tříd.
     *
     * @param defaultProperties
     *         - objekt Properties, který obsahuje nastavení pro diagram tříd.
     */
    public void setDefaultProperties(final Properties defaultProperties) {
        this.defaultProperties = defaultProperties;
    }


    /**
     * Zda se mají v kontextovém menu v položce "Zděděné metody" nebo "Zděděné statické metody" zobrazit ty třídy
     * (předci) jako názvy tříd s balíčky nebo bez nich.
     *
     * @return true, pokud se mají ty třídy (předci) zobrazit s balíčky, jinak false.
     */
    public boolean isShowInheritedClassNameWithPackages() {
        return showInheritedClassNameWithPackages;
    }
}