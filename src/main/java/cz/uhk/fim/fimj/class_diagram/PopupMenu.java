package cz.uhk.fim.fimj.class_diagram;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import cz.uhk.fim.fimj.file.*;
import cz.uhk.fim.fimj.popup_menu.MethodInfo;
import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;
import org.jgraph.graph.DefaultGraphCell;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.forms.GetParametersMethodForm;
import cz.uhk.fim.fimj.forms.NewInstanceForm;
import cz.uhk.fim.fimj.forms.RenameClassForm;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;
import cz.uhk.fim.fimj.instances.Called;
import cz.uhk.fim.fimj.instances.OperationsWithInstances;
import cz.uhk.fim.fimj.menu.Menu;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.output_editor.OutputEditor;
import cz.uhk.fim.fimj.popup_menu.PopupMenuAbstract;
import cz.uhk.fim.fimj.popup_menu.PopupMenuScroller;

/**
 * Třída slouží jako "menu", které se otevře po kliknutí na třídu v class diagramu
 * <p>
 * toto menu bude umožňovat zmenu názvu třídy - pokud na ní neukazuje žádná reference a příslušná třída také nemá žádné
 * reference na ostatní třídy v projektu - v grafu pak ji lze přejmenovat, protože bych jinak musel přejmenovat veškeré
 * balíčky ve všech třídách s referencemi
 * <p>
 * dále lze zkompilovat kód třídy nebo ji odebrat či otevřít její kód v editoru
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class PopupMenu extends PopupMenuAbstract implements ActionListener, LanguageInterface {

	private static final long serialVersionUID = 2278253098845302793L;


	/**
	 * Proměnná, která značí počet konstruktorů, kolik musí třída obsahovat, aby se ten seznam konstruktorů vkládal do
	 * rozevíracího menu.
	 */
	private static final int COUNT_OF_CONSTRUCTOR_FOR_MENU = 3;


	// Texty do výpisových hlášek:
    private String txtClassIsInRelationShipAlreadyTitle, txtClassIsInRelationShipAlreadyText,
            txtFailedToCallMainMethod, txtError, txtMethodIsInaccessible, txtIllegalArgumentException,
            txtInvocationTargetException, txtStart, txtFailedToCreateInstanceText, txtFailedToCreateInstanceTitle,
            txtSuccess, txtNotSpecified;
	
	
	// Položky v menu:
	private JMenuItem itemRename;
    private final JMenuItem itemRemove;


	/**
	 * Otevření příslušné třídy v editoru zdrojového kódu nebo průzkumníku projektů, průzkumníku souborů apod.
	 */
	private JMenu menuOpen;
    /**
     * Menu pro položky, které slouží pro zkopírování cesty nebo reference na třídu.
     */
    private JMenu menuCopyReference;
	/**
	 * Seznam statických metod v příslušné třídě
	 */
	private JMenu menuStaticMethods;
	/**
	 * Seznam statických metod (veřejných a chráněncýh) ve všech předcích
	 */
	private JMenu menuInheritedStaticMethods;
	/**
	 * Seznam konstruktorů, ten se ale využije pouze v případě, že byly nalezeny více jak COUNT_OF_CONSTRUCTOR_FOR_MENU
	 * konstruktorů:
	 */
	private JMenu menuConstructors;

	/**
	 * Seznam vnitřních tříd, které se nachází v označené třídě v diagramu tříd.
	 */
	private JMenu menuInnerStaticClasses;



    /**
     * Položka pro otevření označené třídy v dialogu průzkumník projektů.
     */
    private JMenuItem itemOpenInProjectExplorer;
    /**
     * Položka pro otevření označené třídy v dialogu editor zdrojového kódu.
     */
    private JMenuItem itemOpenInCodeEditor;


    /**
     * Položka pro otevření označené třídy ve výchozím průzkummíku souborů dle používané platformy.
     */
    private JMenuItem itemOpenInExplorerInOs;
    /**
     * Položka pro otevření označené třídy ve výchozí aplikaci používané platformy, napříkad textový soubor v notepadu
     * apod.
     */
    private JMenuItem itemOpenInDefaultAppInOs;


    /**
     * Položka pro vložení do schránky absolutní cesty označené třídě.
     */
    private JMenuItem itemCopyAbsolutePath;
    /**
     * Položka pro vložení do schránky relativní cesty k označené třídě.
     */
    private JMenuItem itemCopyRelativePath;
    /**
     * Položka pro vložení do schránky reference na označenou třídu (název třídy s balíčky).
     */
    private JMenuItem itemCopyReference;


	
	
	// Označená třída:
	private DefaultGraphCell cell;

	private final GraphClass classDiagram;

	/**
	 * Proměnná, do které vložím načtený soubor .class po kompilaci označené třídy v
	 * diagramu tříd (pokud) se kompilace povede, jinak tam bude null:
	 */
	private final Class<?> selectedClass;
	
	
	
	/**
	 * Logická proměnná, se nastaví na true nebo false podle konstruktoru, když se
	 * zobrazí konstruktor, po kliknutí na objekt v idagramu tříd, se otestuje, zda
	 * je to trida a pokud ano, tak jestli existuje a jestli ne, tak už ji lze
	 * jedině smazat, pak se ale musí smazat všechny hrany k ní pripojene, a proto
	 * bude tato proměnna true, ale pokud se klikne na tridu pravym a ta jeste
	 * existuje, tak se nejprve
	 */
	private final boolean deleteAllEdges;


    /**
     * Absolutní cesta k označené třídě v diagramu tříd.
     */
    private String absolutePathToClass;
    /**
     * Relativní cesta k označené třídě v diagramu tříd.
     * <p>
     * Jedná se o cestu od adresáře src (bez něj) až k příslušné třídě s příponou .java
     */
    private String relativePathToClass;
    /**
     * Reference na třídu, v podstatě se jedná pouze o název třídy s balíčky. Balíčky jsou oddělené desetinnou tečkou.
     */
    private String referenceToClass;




    /**
     * Tento konstruktor se volá pro běžné zobrazení možností nad třídou
     *
     * @param cell
     *         - označená buňka v diagramu tříd reprezentující třídu
     * @param classDiagram
     *         - reference na diagram tříd
     * @param outputEditor
     *         - reference na editor výstupů v hlavním okně aplikace
     * @param instanceDiagram
     *         - reference na diagram instancí v hlavním okně aplikace
     * @param languageProperties
     *         - reference na soubor s texty ve zvoleném jazyce
     * @param includePrivateMethod
     *         - logická proměnné o tom, zda se mají přidat i privátní metody nebo ne. Hodnota true, značí, že se budou
     *         přidávat i privátní metody na zavolání, hodnota false, že se nebudou přidávat i privátní metody. Jde o
     *         to, že nad třídou by neměly jít volat privátní metody , ale uživateli zde tuto možnost zpřístupním pouze
     *         za účelem testování, když by chtěl nějakou metodu vyzkouše apod. Ale musí tuto funkci povolit v
     *         nastavení.
     * @param includeProtectedMethods
     *         - logická proměnná, která značí, zda se mají do kontextového menu přidat i chráněné metody. True - mají,
     *         false - nemají.
     * @param includePackagePrivateMethods
     *         - logická proměnná, která značí, zda se mají do kontextového menu přidat i packageprivate metody. True -
     *         mají se do menu přidat metody s viditelností pouze v rámci balíčku, false - nemají se do menu přidat
     *         takové metody s touto viditelností.
     * @param makeVariablesAvailableForCallMethod
     *         - logická proměnné, dle které se pozná, zda se mají zpřístupnit veřejné a chráněné proměnné v třídách v
     *         diagramu tříd, v instancích v diagramu instancí a vytvořených proměnných pomocí editorupříkazů -> tyto
     *         hodnoty ze zpřístupní pro předání do parametru pro zavolání metody. Bude se jednat o hodnoty puze
     *         stejného datového typu,které bude možné předat do příslušného parametru metody.
     */
    public PopupMenu(final DefaultGraphCell cell, final GraphClass classDiagram, final OutputEditor outputEditor,
                     final GraphInstance instanceDiagram, final Properties languageProperties,
                     final boolean includePrivateMethod, final boolean includeProtectedMethods, final boolean
                             includePackagePrivateMethods, final boolean makeVariablesAvailableForCallMethod) {

        super();

        this.classDiagram = classDiagram;
        this.instanceDiagram = instanceDiagram;
        this.cell = cell;
        this.outputEditor = outputEditor;
        deleteAllEdges = false;
        this.makeVariablesAvailableForCallMethod = makeVariablesAvailableForCallMethod;


        operationWithInstances = new OperationsWithInstances(outputEditor, languageProperties, instanceDiagram);


        // Vytvořím menu pro konstruktory (pokaždé, ale ne vždy se implementuje):
        menuConstructors = new JMenu();


        menuStaticMethods = new JMenu();
        new PopupMenuScroller(menuStaticMethods, SCROLL_COUNT, SCROLL_INTERVAL, TOP_FIXED_COUNT, BOTTOM_FIXED_COUNT);


        menuInheritedStaticMethods = new JMenu();
        new PopupMenuScroller(menuInheritedStaticMethods, SCROLL_COUNT, SCROLL_INTERVAL, TOP_FIXED_COUNT,
                BOTTOM_FIXED_COUNT);


        menuInnerStaticClasses = new JMenu();
        new PopupMenuScroller(menuInnerStaticClasses, SCROLL_COUNT, SCROLL_INTERVAL, TOP_FIXED_COUNT, BOTTOM_FIXED_COUNT);


        itemRename = new JMenuItem();
        itemRename.addActionListener(this);


        itemRemove = new JMenuItem();
        // Červená barva písma:
        itemRemove.setForeground(REMOVE_ITEM_FOREGROUND_COLOR);
        itemRemove.addActionListener(this);


        menuOpen = new JMenu();

        itemOpenInCodeEditor = new JMenuItem();
        itemOpenInProjectExplorer = new JMenuItem();
        itemOpenInExplorerInOs = new JMenuItem();
        itemOpenInDefaultAppInOs = new JMenuItem();

        itemOpenInCodeEditor.addActionListener(this);
        itemOpenInProjectExplorer.addActionListener(this);
        itemOpenInExplorerInOs.addActionListener(this);
        itemOpenInDefaultAppInOs.addActionListener(this);

        menuOpen.add(itemOpenInCodeEditor);
        menuOpen.addSeparator();
        menuOpen.add(itemOpenInProjectExplorer);
        menuOpen.addSeparator();
        menuOpen.add(itemOpenInExplorerInOs);
        menuOpen.addSeparator();
        menuOpen.add(itemOpenInDefaultAppInOs);


        menuCopyReference = new JMenu();

        itemCopyAbsolutePath = new JMenuItem();
        itemCopyRelativePath = new JMenuItem();
        itemCopyReference = new JMenuItem();

        itemCopyAbsolutePath.addActionListener(this);
        itemCopyRelativePath.addActionListener(this);
        itemCopyReference.addActionListener(this);

        menuCopyReference.add(itemCopyAbsolutePath);
        menuCopyReference.addSeparator();
        menuCopyReference.add(itemCopyRelativePath);
        menuCopyReference.addSeparator();
        menuCopyReference.add(itemCopyReference);




        // Nejprve přidám všechny konstruktory, pak zbytek z menu:

        // Před tím potřebuji ale získat nějaké texty, pttože pokud by v následující metodě
        // pro hledání konstruktoru došlo k chybě, texty by nebyly k dispozici
        setLanguage(languageProperties);


        // Pokusím se zkompilovat a načíst označenou třídy v diagramu tříd a vzít si soubor .class:
        selectedClass = App.READ_FILE.getClassFromFile(cell.toString(), outputEditor, GraphClass.getCellsList(),
                false, false);


		/*
         * Načtu si veškeré konstruktory, které se v té třídě nachází a ty vyfiltruji dle nastavených parametrů.
         */
        final List<Constructor<?>> availableConstructors = getAvailableConstructors(selectedClass);


        /*
         * Pokud je počet konstruktorů větší než definovaný počet, tak se budou veškeré
         * konstrukty vkládat do rozevíracího menu.
         */
        if (availableConstructors.size() > COUNT_OF_CONSTRUCTOR_FOR_MENU) {
            /*
             * Nastavení toho, aby šel seznam konstruktorů rolovat v případě, že jich bude
             * načteno více než SCROLL_COUNT_CONSTRUCTORS
             */
            new PopupMenuScroller(menuConstructors, SCROLL_COUNT_CONSTRUCTORS, SCROLL_INTERVAL, TOP_FIXED_COUNT,
                    BOTTOM_FIXED_COUNT);

            // Přidám jej do okna:
            add(menuConstructors);
            addSeparator();


            // Přidám do něj konstruktory:
            addConstructorsItem(menuConstructors, availableConstructors);
        }

        // Nalezení konstruktorů, které se budou přidávat do "tohoto" popupMenu, ne do
        // rozevíracího menu:
        else {
            addConstructorsItem(null, availableConstructors);

            addSeparator();
        }




        // Nalezení všech statických metod a jejich přidání do tohoto popupMenu
        addStaticMethodItems(includePrivateMethod, includeProtectedMethods, includePackagePrivateMethods);


        // Vložení statických zděděných metod do menu menuInheritedStaticMethods
        addInheritedStaticMethods();


        // Vložení statických vnitřních tříd do menu menuInnerStaticClasses:
        addStaticInnerClasses();


        /*
         * Přidání položek do okna (tohoto kontextového menu).
         *
         * Nabídky Statické metdy, Zděděné statické metody a Vnitřní statické třídy budou přidány do menu (/ okna)
         * pouze v případě, že obsahují alespoň jednu položku. Jinak je nemá smysl zobrazovat.
         *
         * Note:
         * Bylo by možné to pořešit i tak, že by se nemuseli ty metody výše procházet "celé", vždy bych si mohl
         * nejprvenačíst, zda se například
         * v té třídě nějaké statické metody vůbec nachází, pokud ano, tak pokračovat apod. Ale takto je to méně kódu
         * a trochu přehlednější než nějaké další testování, i když už je to spíše takový "detail".
         */
        if (menuStaticMethods.getItemCount() > 0) {
            add(menuStaticMethods);
            addSeparator();
        }

        if (menuInheritedStaticMethods.getItemCount() > 0) {
            add(menuInheritedStaticMethods);
            addSeparator();
        }

        if (menuInnerStaticClasses.getItemCount() > 0) {
            add(menuInnerStaticClasses);
            addSeparator();
        }

        add(itemRename);
        addSeparator();
        add(menuOpen);
        addSeparator();
        add(menuCopyReference);
        addSeparator();
        add(itemRemove);


        addStartItem();
    }







    /**
     * Metoda, která získá veškeré konstruktory, které se mohou načíst. Tzn. že projde veškeré konstruktory v uživatelem
     * označené třídě, a pro každý konstruktor otestuje, zda se má načíst nebo ne. Dle nastavených hodnot se zjistí, zda
     * je soukromý, chráněný, package-private, jinak e veřejný, ty se načtou vždy. Pro každou z těchto uvedených
     * podmínek se otestuje, zda se mají takové konstruktory započítat, pokud ano, pak se započítají.
     *
     * @param clazz
     *         - Třída, ze které se mají načíst konstruktory. (Uživatelem označená třída v diagramu tříd)
     *
     * @return List s nalezenými a povolenými konstruktory, které se vloží do kontextového menu.
     */
    private static List<Constructor<?>> getAvailableConstructors(final Class<?> clazz) {
        final List<Constructor<?>> availableConstructors = new ArrayList<>();

        if (clazz == null || clazz.isEnum() || Modifier.isInterface(clazz.getModifiers())
                || Modifier.isAbstract(clazz.getModifiers()))
            return availableConstructors;

        Arrays.stream(clazz.getDeclaredConstructors()).forEach(c -> {
            /*
             * Zde otestuji, zda se mají vkládat jak soukromé, chráněné nebo package-private, pokud se některé z nich
             * nemají zobrazovat a jedná se o takový konstruktor, pak nebude započítán.
             */
            if (!modifiersCorrespond(c.getModifiers(), GraphClass.isIncludePrivateConstructors(), GraphClass
                            .isIncludeProtectedConstructors(),
                    GraphClass.isIncludePackagePrivateConstructors()))
                return;

            /*
             * Zde je konstruktor veřejný, nebo jeden z výše uvedených viditelností, které jsou povolené, pak jej mohu
             * přidat do výběru.
             */
            availableConstructors.add(c);
        });

        return availableConstructors;
    }







    /**
     * Tento konstruktor se zavolá pouze v případě, že uživatel klikl v diagramu tříd pravým tlačítkem myší na buňku
     * reprezentující třídu a ta neexistue v adresáři src v projektu aplikace
     * <p>
     * Tento konstruktor zobrazí menu pouze s jedním tlačítekm, asice Odebrat - takže lze danou buňku vymyzat z diagramu
     * tříd - stejně už tam nebude potřeba
     *
     * @param classDiagram
     *         - reference na diagram tříd
     * @param languageProperties
     *         - reference na soubor s uživatelem zvoleným jazykem pro aplikace
     */
    public PopupMenu(final GraphClass classDiagram, final Properties languageProperties) {
        super();

        selectedClass = null;

        this.classDiagram = classDiagram;
        this.languageProperties = languageProperties;
        deleteAllEdges = true;


        itemRemove = new JMenuItem();
        itemRemove.addActionListener(this);


        if (languageProperties != null)
            itemRemove.setText(languageProperties.getProperty("Ppm_ItemRemove", Constants.PPM_ITEM_REMOVE));

        else
            itemRemove.setText(Constants.PPM_ITEM_REMOVE);

        add(itemRemove);
    }
	
	
	
	
	
	
	
	
	
	

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JMenuItem) {
			if (e.getSource() == itemRename) {
				// Otestuji, zda třída není v nějakém vztahu s jinou třída, pak nepůjdu přejmenovat, protože bych musel přepsat balíčky 
				// ve všechn třídách se kterými je ve vztahu, proto je třeba nejprve smazat všechny vztahy s danou třídou, pak půjde přejmenovat
				if (!GraphClass.isClassInRelationship(cell)) {
					final RenameClassForm rcf = new RenameClassForm();
					rcf.setLanguage(languageProperties);
					
					final String newClassName = rcf.getNewNameOfClass(cell.toString());
					
					if (newClassName != null) {
						// Nyní prošel nový název třídy:, tak ji přejmenuji, případně vytvořím balíčky, apod.:
						
						// Postup:
						// Nakopíruji si označenou třídu do nově zadaného adresáře - pokud je zadaný
						// přepíše balíčrk, ve kterém se nachází
						// Přemenují třídu a konstuktory - pokud existují
						// V grafu přepíšu název třídy, resp. původní buňka se z grafu vymaže a založí so nová pod novým jménem
						
						
						// Nejprve musím balíčky vytvořit - pokud je má obsahovat a neexistují:
						final String pathToSrc = App.READ_FILE.getPathToSrc();
						
						final String pathToBin = App.READ_FILE.getPathToBin();
						
						if (pathToSrc != null && pathToBin != null) {
							// Otestuji, zda zadaný nýzev třídy obsahuje balíček nebo be, pokud ne, přidám tam výchozí:
							
							// Zde je třeba pokaždé testovat balíčky a přepisování tříd - vždy se bude v nějakém nacházet,
							// ať už je výchozí či jiný zadaný:

							final String[] partsOfPackage = newClassName.split("\\.");

                            final StringBuilder packageText = new StringBuilder();

                            for (int i = 0; i < partsOfPackage.length - 1; i++)
                                packageText.append(partsOfPackage[i]).append(".");

                            // Odeberu tečku:
                            packageText.setLength(packageText.length() - 1);

							// Nyní mohu vytvořit balíčky do kterých má být
							// třída umístěna - pokud neexistují:
							final String pathToDesDirForClass = App.WRITE_TO_FILE.createDirectoriesToClass(pathToSrc,
									pathToBin, newClassName);

							if (pathToDesDirForClass != null) {
								// Zde se vytvořily balíčky, mohu nakopírovat
								// třídu:
								final String srcClass = pathToSrc + File.separator
										+ cell.toString().replace(".", File.separator) + ".java";
								final String desClass = pathToDesDirForClass + File.separator
										+ partsOfPackage[partsOfPackage.length - 1] + ".java";
								
								try {
									Files.copy(Paths.get(srcClass), Paths.get(desClass),
											StandardCopyOption.REPLACE_EXISTING);

									// Note: zde se jedná přesun třídy do jiného
									// balíčku, tak je třeba původní třídu
									// smazat,
									// a ještě před tím ji nakopírovat do nově
									// zvoleného.

									// Nejprve z grafu smažu původní třídu a z
									// adresáře projektu také:
									App.WRITE_TO_FILE.deleteClassFromSrcDirectory(cell.toString());

									// Před tím než smažu objekt musím smazat
									// potenciální komentář k dané třídě -
									// buňce:
									classDiagram.deleteCommentClass(cell);

									classDiagram.deleteObjectFromGraph(cell, GraphClass.getCellsList());

									// Zde se třída zkopírovala, tak mohu
									// upravit, případně přidat balíčky:
									App.WRITE_TO_FILE.deletePackageIfExist(desClass);
									App.WRITE_TO_FILE.addPackageToClass(desClass, packageText.toString());

									// Přejmenování třídy - hlaviček a
									// konstukrotů
									final String oldName = cell.toString()
											.substring(cell.toString().lastIndexOf('.') + 1);
									
									App.WRITE_TO_FILE.renameClass(desClass, oldName,
											partsOfPackage[partsOfPackage.length - 1]);

									// Na konec mohu v grafu vytvořit novou
									// buňku reprezentující danou třídu:
									classDiagram.addClassCell(newClassName);

								} catch (IOException e1) {
                                    /*
                                     * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                                     * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                                     * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                                     * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                                     */
                                    final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                                    // Otestuji, zda se podařilo načíst logger:
                                    if (logger != null) {
                                        // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                                        // Nejprve mou informaci o chybě:
                                        logger.log(Level.INFO, "Zachycena výjimka při kopírování třídy, zdroj: "
                                                + srcClass + ", cíl: " + desClass
                                                + ", třída PopupMenu v balíčku classDiagram! K této chybě došlo při " +
                                                "pokusu o přejmenování třídy.");

                                        /*
                                         * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                                         * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                                         * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                                         */
                                        logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
                                    }
                                    /*
                                     * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                                     */
                                    ExceptionLogger.closeFileHandler();
                                }
							}
						}
					}
				}

				else
					JOptionPane.showMessageDialog(this, txtClassIsInRelationShipAlreadyText,
							txtClassIsInRelationShipAlreadyTitle, JOptionPane.ERROR_MESSAGE);
			}




			else if (e.getSource() == itemRemove)
				App.WRITE_TO_FILE.removeSelectedObject(classDiagram, languageProperties, deleteAllEdges);






                // Vloží do schránky absolutní cestu k označené třídě v diagramu tříd:
            else if (e.getSource() == itemCopyAbsolutePath)
                ClipboardHelper.copyToClipboard(absolutePathToClass);


                // Vloží do schránky relativní cestu k označené třídě v diagramu tříd:
            else if (e.getSource() == itemCopyRelativePath)
                ClipboardHelper.copyToClipboard(relativePathToClass);


                // Vloží do schránky referenci na označenou třídu v diagramu tříd (název třídy s balíčky):
            else if (e.getSource() == itemCopyReference)
                ClipboardHelper.copyToClipboard(referenceToClass);




			
			
			
			
			
			else if (e.getSource() == itemOpenInCodeEditor)
				// Otevřu označenou třídu - buňku v "klasickém" editoru kódu
				classDiagram.openClassInCodeEditor(cell, selectedClass);
			
			
			
			
			
			
			else if (e.getSource() == itemOpenInProjectExplorer)
				// Otevře se označená třída, resp. buňku v průzkumníku projektů:
				classDiagram.openClassInProjectExplorer(cell, selectedClass);




                // Otevření třídy v průzkumníku projektů nastaveního v OS:
            else if (e.getSource() == itemOpenInExplorerInOs)
                DesktopSupport.openFileExplorer(createFilePathFromClass(selectedClass), true);

                // Otevření třídy ve výchozí aplikaci nastavené v OS ro otevření příslušného typu souboru:
            else if (e.getSource() == itemOpenInDefaultAppInOs)
                DesktopSupport.openFileExplorer(createFilePathFromClass(selectedClass), false);
        }
	}








    /**
     * Sestavení cesty k třídě clazz. Jedná se o sestavení cesty k adresáři src v otevřeném projektu a k tomu se přidá
     * název třídy (i s balíčky).
     *
     * @param clazz
     *         - třída, která senachází v diagramu tříd. K ní se má sestavit absolutní cesta.
     *
     * @return absolutní cestu k třídě clazz v diagramu tříd. Jinak null.
     */
    private static File createFilePathFromClass(final Class<?> clazz) {
        final String pathToSrc = App.READ_FILE.getPathToSrc();

        if (pathToSrc == null)
            return null;

        final String pathToClass = pathToSrc + File.separator + clazz.getName().replace(".", File.separator) + ".java";

        return new File(pathToClass);
    }








	/**
	 * Metoda, která přidá do menu menuInnerStaticClasses veškeré vnitřní třídy a jejich povolené metody, které se v nich
	 * nachází v označené třídě v diagramu tříd.
	 */
	private void addStaticInnerClasses() {
		if (selectedClass == null)
			return;

		/*
		 * Získám si veškeré vnitřní třídy, které se nachází v označené třídě v diagramu tříd.
		 */
		final Class<?>[] classes = selectedClass.getDeclaredClasses();

		/*
		 * Získám si veškeré povolené (/ viditelné) vnitřní třídy z uživatelem označené třídy v diagramu tříd:
		 */
		final List<Class<?>> allowedClasses = getStaticInnerClasses(classes, new ArrayList<>());

		/*
		 * Zde vyfiltruji pouze ty metody, kterou jsou statické a veřejné, popř. jiné viditelnosti, pokud jsou
		 * povolené:
		 */
		final List<MethodInfo> allowedMethods = getAllowedMethods(allowedClasses);


		// Zde projdu získané metody i s třídami a vytvořím z nich položky do menu a přidám je do menu vnitřních tříd:
		allowedMethods.forEach(info -> {
			/*
			 * Vytvořím si rozevírací položku (menu), do kterého níže vložím příslušné metody.
			 */
			final JMenu menu = getMenuForClass(info.getClazz(), false, info.getTooltip
					());

			info.getMethods().forEach(m -> {
				/*
				 * Položka, která bude reprezentovat konkrétní (aktuálně iterovanou) metodu. Po kliknutí na toto
				 * tlačítko se metoda zkusí zavolat.
				 */
				final JMenuItem itemMethod = getMethodMenuItem(m);

				// Přidám událost na kliknutí na tu položku, aby se zavolala příslušná metoda:
				addListenerToMethodItem(itemMethod, m);

				// Přidám položku do rozevíracího menu reprezentující metody z konréní třídy:
				menu.add(itemMethod);
			});

			// Přidám rozevírací menu reprezentující třídu (předka) a jeho metody do kontextového menu nad třídou:
			menuInnerStaticClasses.add(menu);
		});
	}















	/**
	 * Metoda, která vyfiltruje pouze povolené vnitřní třídy (classes). Ve výsledku budou pouze všechny statické
	 * veřejné
	 * vnitřní třídy. Dále, pokud je nějaký z parametrů pro viditelnost private, protected nebo package-private true,
	 * pak i vnitřní třídy s příslušnou viditelností budou také načteny / zpřístupněny.
	 *
	 * @param classes
	 *         - pole vnitřních tříd, které se mají "vyfiltrovat" a případně vložit do listu allowedClasses. Vyfiltrují
	 *         se takové vnitřní třídy, které jsou statické a mají příslušnou viditelnost - private, protected, ...
	 * @param allowedClasses
	 *         - list, do kterého se postupně vkládají nalezené vnitřní třídy, které jsou statické a splňují
	 *         viditelnost
	 *         (private, protected, ...).
	 * @return list allowedClasses, který obsahuje veškeré povolené / vyfiltrované třídy, které jsou statické a
	 * přísulšné viditelnosti - buď public nebo jedna z výše povolených.
     */
    private static List<Class<?>> getStaticInnerClasses(final Class<?>[] classes, final List<Class<?>>
            allowedClasses) {

        if (classes == null)
            return allowedClasses;

        Arrays.stream(classes).forEach(c -> {
            // Veškeré vnitřní třídy musí být statické
            if (!Modifier.isStatic(c.getModifiers()))
                return;

            if (!modifiersCorrespond(c.getModifiers(), GraphClass.isIncludePrivateInnerStaticClasses(), GraphClass
                    .isIncludeProtectedInnerStaticClasses(), GraphClass.isIncludePackagePrivateInnerStaticClasses()))
                return;

            allowedClasses.add(c);

            getStaticInnerClasses(c.getDeclaredClasses(), allowedClasses);
        });

        return allowedClasses;
    }









	/**
	 * Metoda, která z vnitřních tříd, které vyfiltrovala metoda getStaticInnerClasses vybere pouze takové metody,
	 * které
	 * jsou veřejné statické. Dále ty, které jsou statické a mají jednu z povolených viditelností, kterou si uživatel
	 * může nastavit v nastavení. Například se mohou vyfiltrovat i metody, které jsou statické s viditelností private,
	 * protected apod.
	 *
	 * @param allowedClasses
	 *         - list vnitřních tříd, ve kterých se mají projít jejich metody a vybrat statické veřejné a s niže
	 *         uvedenou viditelností.
	 * @return list, který bude obsahovat výše zmíněné povolené hodnoty dle povolených viditelností.
	 */
	private static List<MethodInfo> getAllowedMethods(final List<Class<?>> allowedClasses) {
		/*
		 * List, do kterého se vloží veškeré povolené metody (dle nastavení). Tyto metody patří do příslušné třídy.
		 */
		final List<MethodInfo> allowedMethods = new ArrayList<>();

		/*
		 * Projdu si všechny (vnitřní) třídy, pro každou třídu se vezmu všechny její metody a zjistím, zda jsou
		 * statické a mají povolenou viditelnost, pokod ano, vloží se do listu allowedMethods.
		 */
		allowedClasses.forEach(c -> {
			/*
			 * Metody z příslušné vnitřní třídy.
			 */
			final Method[] methods = c.getDeclaredMethods();

			/*
			 * Dočasný list pro získání / vyfiltrování metod z pole methods. Vyfiltrují se takové metody,
			 * které jsou statické a mají jednu z povolených viditelností.
			 */
			final List<Method> tempMethodsList = new ArrayList<>();

			Arrays.stream(methods).forEach(m -> {
				if (!Modifier.isStatic(m.getModifiers()))
					return;

				if (!modifiersCorrespond(m.getModifiers(), GraphClass.isInnerStaticClassesIncludePrivateStaticMethods
						(), GraphClass.isInnerStaticClassesIncludeProtectedStaticMethods(), GraphClass
						.isInnerStaticClassesIncludePackagePrivateStaticMethods()))
					return;

				tempMethodsList.add(m);
			});

			/*
			 * Zde přidám list tempMethodsList (list s povolenými metodami) do listu allowedMethods, který
			 * obsahuje veškeré povolené metody, které budemoci uživatel zavolat.
			 */
			allowedMethods.add(new MethodInfo(c, tempMethodsList));
		});

		return allowedMethods;
	}





	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuji, zda označená třída obsahuje main metodu, pokud ano,
	 * tak do menu přidá položku s názvem Spustit (v češtině) a po kliknutí na tuto
	 * položku se zavolá daná main metoda v dané třídě, a "spustí" se tím daný
	 * projěkt, ovšem v takovém případě již nelze zjišťvat, zda je nějaká instance
	 * třídy v diagramu tříd vytvořena nebo ne, aby se případně přidala do diagramu
	 * instancí a šlo nad ní prováět metody, apod.
	 */
	private void addStartItem() {

		// pokudsím se získat main metodu z dané třídy:
		final Method mainMethod = Menu.getMainMethodFromClass(selectedClass);

		// Otestuji, zda se ji podařilo získat:
		if (mainMethod != null) {
			final JMenuItem itemMainMethod = new JMenuItem(txtStart);
			
			// Zelená barva písma:
			itemMainMethod.setForeground(new Color(0,100,0));

			// Přidám událost po kliknutí na danou pložku:
			addActionListenerForMainMethod(itemMainMethod, mainMethod);

			addSeparator();
			add(itemMainMethod);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá událost na položku v parametru této metody a sice, přidá
	 * událost po kliknutí na danou položku , po kliknutí na tuto položku se pokusí
	 * zavolat metu main v označené třídě, případn vypíše nějaké chybové oznámení do
	 * editoru výstupů v hlavním okně aplikacee
	 * 
	 * @param item
	 *            - položka pro přidání do menu
	 * 
	 * @param mainMethod
	 *            - main metoda, která se zavolá po kilknutí na danou položku v menu
	 */
	private void addActionListenerForMainMethod(final JMenuItem item, final Method mainMethod) {
		item.addActionListener(event -> {

			final String[] parameters = null;

			try {
				// Pokusím se zavolat metodu main:
				mainMethod.invoke(null, (Object) parameters);

			} catch (IllegalAccessException e) {
				outputEditor.addResult(txtFailedToCallMainMethod + OutputEditor.NEW_LINE_TWO_GAPS + txtError + ": "
						+ txtMethodIsInaccessible);

				/*
				 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
				 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
				 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
				 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
				 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při pokusu o zavolání metody main v označené třídě v diagramu tříd, " +
                                    "třída PopumMenu.java,"
                                    + " metoda addStartItem(). Metoda 'main' je nepřístupná pro zavolání.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
				
			} catch (IllegalArgumentException e) {
				outputEditor.addResult(txtFailedToCallMainMethod + OutputEditor.NEW_LINE_TWO_GAPS + txtError + ": "
						+ txtIllegalArgumentException);

                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při pokusu o zavolání metody main v označené třídě v diagramu tříd, " +
                                    "třída PopumMenu.java, metoda addStartItem()."
                                    + " Tato chyba může nastat například pokud je zadán chybný počet nebo typ " +
                                    "parametrů metody 'main'.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();

			} catch (InvocationTargetException e) {
				outputEditor.addResult(txtFailedToCallMainMethod + OutputEditor.NEW_LINE_TWO_GAPS + txtError + ": "
						+ txtInvocationTargetException);

				/*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při pokusu o zavolání metody main v označené třídě v diagramu tříd, " +
                                    "třída PopumMenu.java,"
                                    + " metoda addStartItem(). Jedna ze 'základních' metod vyvolala výjimku.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
            }
        });
	}






    /**
     * Metoda, která do této třídy přidá na "začátek tohoto menu" nebo do položky menuConstructors všechny dostupné
     * konstruktory v označené třídě.
     * <p>
     * Pokud bude parametr menuConstructors null, pak se budou přidávat veškeré nalezené konstruktory "klasicky do
     * tohoto popupMenu", ale pokud parametr menuConstructors nebude null, pak se budou veškeré nalezené konstruktory
     * přidávat do tohoto rozevíracího menu.
     *
     * @param menuConstructors
     *         - položka / rozevírací menu, kam se mají přidávat nalezené konstruktory, ale jen v případě, že se těch
     *         konstruktorů nachází více než COUNT_OF_CONSTRUCTOR_FOR_MENU.
     * @param constructors
     *         - list, který obsahuje získané / povolené konstruktory, tj. už vyfiltrované soukromé, chráněné, případně
     *         package-private apod. Takže se jedná pouze o ty konstruktory, které se mají přidat do kontextového menu.
     */
    private void addConstructorsItem(final JMenuItem menuConstructors, final List<Constructor<?>> constructors) {
        /*
         * Projdu si veškeré konstruktory a pro každý sestavím text položky do menu a příslušnou položku vytvořím.
         */
        for (final Constructor<?> c : constructors) {
            // Zjistím si název konstruktoru bez balíčku:
            final String constructorName = selectedClass.getSimpleName();

            // Proměnná do které bude skládat výsledný text, který přidám do menu:
            String textToMenu = "new ";

            // Nyní si zjistím jeho parametry:
            textToMenu += ReflectionHelper.getMethodNameAndParametersInText(constructorName, c.getParameters());

            // Přidám uzavírací závorku a středník:
            textToMenu += ";";


            // Nyní mohu přidat získanou syntaxi konstruktoru do menu:
            final JMenuItem itemConstructor = new JMenuItem(textToMenu);

            addListenerToItem(itemConstructor, c);

            if (menuConstructors != null)
                menuConstructors.add(itemConstructor);
            else
                add(itemConstructor);
        }
    }
	
	
	
	
	
	
	/**
	 * Metoda, která přidá událost na kliknutí na dané tlačítko - položka v menu pro
	 * vytvoření nové instance
	 * 
	 * Postup:
	 * Otevře si deialogové okno, tam se zadané potřebné údaje - viz dialog ten
	 * vrátí tyto údaje zpět a zavolá se vytvoření příslušné instance
	 * 
	 * @param item
	 *            položka v menu - konstruktor, který se má zavolat po klinutí na
	 *            tuto pložku
	 * 
	 * @param constructor
	 *            - kontruktor, zde ho potřebuji předat, protože odtud budu volat
	 *            vytvoření nové instance, tak potřebuji předat konstruktor, který
	 *            se má zavolat pro vytvoření nové instance označené třídy
	 * 
	 */
	private void addListenerToItem(final JMenuItem item, final Constructor<?> constructor) {
		
		item.addActionListener(e -> {
			// Získám hodnoty z dialogu
			final NewInstanceForm nif = new NewInstanceForm(constructor, languageProperties, outputEditor,
					GraphClass.isMakeVariablesAvailable(),
					GraphClass.isShowCreateNewInstanceOptionInCallConstructor(), instanceDiagram, selectedClass,
					GraphClass.isMakeMethodsAvailableForCallConstructor());

			final List<Object> valuesForInstance = nif.getValuesForNewInstance(cell.toString());

			// Ještě si zjistím balíček (/ balíčky), ve kterých se třída nachází:
			final String packageName = getPackageOfClass();


			if (valuesForInstance != null && !valuesForInstance.isEmpty()) {
				// Zde uživatel klikl na OK - nezavřel dialog

				// Otestuji, jakou mám zavolat metodu pro vytvoření instance - s parametry nebo bez
				if (valuesForInstance.size() == 1) {

					// Zde se zavolat konstruktor bez parametrů:
					final boolean isNewInstanceCreated = operationWithInstances.createInstance(
							selectedClass.getSimpleName(), packageName, valuesForInstance.get(0).toString(),
							constructor);

					if (!isNewInstanceCreated)
						JOptionPane.showMessageDialog(PopupMenu.this,
								txtFailedToCreateInstanceText + ": " + selectedClass.getSimpleName() + "!",
								txtFailedToCreateInstanceTitle, JOptionPane.ERROR_MESSAGE);
				}


				else {
					// Zde se jedná i o parametry:

					// Zjistím si název reference
					final String referenceName = valuesForInstance.get(0).toString();

					// Odeberu z kolekce název reference, aby zuůstaly pouze parametry, referenci
					// mám uloženou výše
					valuesForInstance.remove(0);

					final boolean isNewInstanceCreated = operationWithInstances.createInstanceWithParameters(
							selectedClass.getSimpleName(), packageName, referenceName, constructor, true,
							valuesForInstance.toArray());

					if (!isNewInstanceCreated)
						JOptionPane.showMessageDialog(PopupMenu.this,
								txtFailedToCreateInstanceText + ": " + selectedClass.getSimpleName() + "!",
								txtFailedToCreateInstanceTitle, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}












	/**
	 * Metoda, která slouží pro přidání položek do tohoto kontextového menu, které budou reprezentovat statické
	 * metody v
	 * dané (označené) třídě - selectedClass. Statické metody lze zavolat i bez vytvoření instance příslušné třídy.
	 *
	 * Note:
	 *
	 * Nebude se takto přidávat hlavní / souštěcí metoda 'main' -> tu reprezentuje položka 'Spustit', která je jako
	 * poslední v kontextovém menu nad označneou třídou v diagramu tříd v případě, že tuto metodu 'main' příslušná -
	 * označená třída obsahuje.
	 *
	 * @param includePrivateMethods
	 *         - logická proměnná o tom, zda se mají načíst i privátní metody, true ano, false ne.
	 * @param includeProtectedMethods
	 *         - logická proměnná, která značí, zda se mají započítat i chráněné metody, které obsahuje ta třída.
	 *         true -
	 *         započítají se chráněné metody, false, nezapočítají se chráněné metody.
	 * @param includePackagePrivateMethods
	 *         - logická proměnná, která značí, zda se mají započítat i package-private metody, které senachází v té
	 *         třídě, true - mají se započítat, false - nemají se započítat.
	 */
	private void addStaticMethodItems(final boolean includePrivateMethods, final boolean includeProtectedMethods,
									  final boolean includePackagePrivateMethods) {
		/*
		 * Pokud byla třída načtena, pak si zjistím její statické metody - zda nějaké
		 * má, to udělám tak, že si načtu všechny metody z dané třídy, všechny je
		 * proiteruji a pokud budou statické, pak je přidám do tohoto kontextového menu.
		 */
		if (selectedClass == null)
			return;

		Arrays.stream(selectedClass.getDeclaredMethods()).forEach(m -> {
			if (!Modifier.isStatic(m.getModifiers()))
				return;

			/*
			 * Pokud se mají přidat i privání metody a metoda je privátní, tak se bude
			 * pokračovat, ale zde stačí otestovat, poud se nemají přidávat privátní metody
			 * a metoda je privátní, pak pouze přeskočím aktuální iterace - aktuálně
			 * iterovanou privátní metodu, která se nemá přidat.
			 */
			if (!modifiersCorrespond(m.getModifiers(), includePrivateMethods, includeProtectedMethods,
					includePackagePrivateMethods))
				return;


			/*
			 * Zde je metoda statická, tak zjistím, zda se nejedná o metodu main, protože,
			 * ta se přidává pro tlačítko 'spustit'.
			 *
			 * Pokud se jedná o metodu 'main', pak přejdu do další iterace, nebudu ji zde
			 * přidávat.
			 *
			 * Note:
			 *
			 * Toto jsem na konec povolil, tedy i zde ve statických metodách bude možné
			 * zavolat metodu main a to z toho důvodu, aby bylo možné jí předat příslušný
			 * parametr. Jde o to, že jsou k dispozici tlačítka start v menu nebo jako
			 * poslední položka v tomto kontexotévm menu v případě, že třída selectedClass
			 * obsahuje metodu main, ale pomocí těchto tlačítek se spustí, resp. zavolá
			 * metoda main bez parametrů, takže zde to povolím, kdyby chtěl uživatel předat
			 * parametry té metody.
			 */
//			if (isMainMethod(m))
//				return;


			/*
			 * Nyní mohu vytvořit položku s příslušným textem metody a přidat k ní událost
			 * po kliknutí - aby se příslušná metoda zavolala.
			 */
			final JMenuItem itemMethod = getMethodMenuItem(m);

			addListenerToMethodItem(itemMethod, m);

			menuStaticMethods.add(itemMethod);
		});
	}








	/**
	 * Metoda, která přidá do menu menu menuInheritedStaticMethods veškeré metody, které jsou statické chráněné nebo
	 * veřejné, tj. jsou zděděné z předka, resp. ze všech předků.
	 */
	private void addInheritedStaticMethods() {
		/*
		 * Pokud nebyla načten, resp. zkompilována třída (bez chyby), pak mohu skončit:
		 */
		if (selectedClass == null)
			return;


		/*
		 * Zavolám metodu, která proiteruje veškeré předky dané třídy až dokud se
		 * nenarazí na třídu Object a vloží do listu inheritedMethodsList všechny veřejné a
		 * chráněné metody ze všech předků, ty metody musí být statické.
		 */
		final List<MethodInfo> inheritedMethodsList = getInheritedMethods(selectedClass.getSuperclass(),
				new ArrayList<>(), true, selectedClass.getSimpleName());

		/*
		 * Projdu získaný list s informacemi o zděděných metodách. Pro každou získanou třídu (předka) vytvořím
		 * rozevírací
		 * menu, které bude reprezentovat toho předka. Toto rozevícací menu bude obsahovat tooltip, který "ukazuje"
		 * postup dědičnosti
		 * od označené třídy v diagramu tříd. To menu dále bude obshovat jako jednotlivé položky statické veřejné a
		 * chráněné metody, které
		 * se v té třídě (reprezentující rozevírací menu - předka) nachází.
		 */
		inheritedMethodsList.forEach(info -> {
			/*
			 * Vytvořím si rozevírací položku (menu), do kterého níže vložím příslušné metody.
			 */
			final JMenu menu = getMenuForClass(info.getClazz(), classDiagram.isShowInheritedClassNameWithPackages(),
					info.getTooltip());

			info.getMethods().forEach(m -> {
				/*
				 * Položka, která bude reprezentovat konkrétní (aktuálně ierovanou) metodu. Po kliknutí na toto
				 * tlačítko se metoda zkusí zavolat.
				 */
				final JMenuItem itemMethod = getMethodMenuItem(m);

				// Přidám událost na kliknutí na tu položku, aby se zavolala příslušná metoda:
				addListenerToMethodItem(itemMethod, m);

				// Přidám položku do rozevíracího menu reprezentující metody z konréní třídy:
				menu.add(itemMethod);
			});

			// Přidám rozevírací menu reprezentující třídu (předka) a jeho metody do kontextového menu nad třídou:
			menuInheritedStaticMethods.add(menu);
		});
	}





    /**
     * Sestavení cest pro jejich zkopírování do schránky a nastavení těchto cest do tooltipů k příslušným položkám.
     */
    void preparePathsForInsert() {
        if (selectedClass != null) {
            final String packagesAndClassName = selectedClass.getName();

            relativePathToClass = packagesAndClassName.replace(".", File.separator) + ".java";

            referenceToClass = packagesAndClassName;
        }

        else {
            // Tato část by nikdy neměla nastat, ale kdyby náhodou...
            relativePathToClass = txtNotSpecified;
            referenceToClass = txtNotSpecified;
        }

        itemCopyAbsolutePath.setToolTipText(absolutePathToClass);
        itemCopyRelativePath.setToolTipText(relativePathToClass);
        itemCopyReference.setToolTipText(referenceToClass);
    }


    /**
     * Setr na proměnnou, která obsahuje absolutní cestu o označené třídě v diagramu tříd.
     *
     * @param absolutePathToClass - absolutní cesta k třídě v diagramu tříd.
     */
    void setAbsolutePathToClass(String absolutePathToClass) {
        this.absolutePathToClass = absolutePathToClass;
    }

    /**
	 * Metoda, která přidá na danou položku v menu událost po kliknutí po kliknutí
	 * se zavolá metoda, kterou daná položka reprezentuje
	 * 
	 * @param item
	 *            - pložka v menu pro přidání události
	 * @param method
	 *            - metoda, která se provede po kliknutí na danou položku
	 */
	private void addListenerToMethodItem(final JMenuItem item, final Method method) {
		item.addActionListener(e -> {
			if (method.getParameterCount() == 0) {
				// Metoda neobsahuje žádný parametr, tak ji mohu zavolat:
				final Called returnValue = operationWithInstances.callMethodWithoutParameters(method, null, true);

				// Vypís výsledku do editoru:

				if (returnValue != null && returnValue.isWasCalled()) {
					// Zde byla metoda zavolána, tak zjistím, zda metoda má něco vracet:
					if (method.getReturnType().equals(Void.TYPE))
						// zde metoda nemá nic vracet:
						outputEditor.addResult(txtSuccess);
					else {
						// Zde metoda má něco vrátit, tak to vypíšu:
						printReturnedValueToEditor(txtSuccess + ": ",
								returnValue.getObjReturnValueWithTestedReference(), outputEditor);


						// Zda se mají v diagramu instancí zvýraznit příslušné reference - jsou li:
						instanceDiagram.checkHighlightOfInstances(returnValue.getObjReturnValue());


						// Otestuji, zda se vrátila instance, případně se zeptám, zda se má vytvořit
						// reprezentace v diagramu instancí.
						checkReturnedValue(returnValue.getObjReturnValue(), instanceDiagram,
								languageProperties);
					}
				}
				/*
				 * else - Zde se metodu nepodařilo zavolat, chyby byly již vypsány, tak bych
				 * mohl vypsat třeba neúspěch apod. Ale už nic dělat nebudu. Uživatel už o chybě
				 * ví.
				 *
				 * else - Zde by se do editoru výstupů vypsali informace o nastalé chybě pri zavolání metody:
				 * callMethodWithoutParameters
				 */
			}



			else {
				// Zde metoda obsahuje alespoň jeden parametr, tak ho musím načíst od uživatele:
				final GetParametersMethodForm gpmf = new GetParametersMethodForm(method, languageProperties,
						outputEditor, makeVariablesAvailableForCallMethod,
						classDiagram.isShowCreateNewInstanceOptionInCallMethod(), instanceDiagram, selectedClass,
						null, classDiagram.isMakeMethodsAvailable());

				final List<Object> parametersOfMethod = gpmf.getParameters(method.getName());

				if (parametersOfMethod != null && !parametersOfMethod.isEmpty()) {
					// Zjistím si návratovou hodnotu a případně ji vypíšu do editoru
					final Called returnValue = operationWithInstances.callMethodWithParameters(method, null, true,
							parametersOfMethod.toArray());

					// Otestuji, zda se něco vrátilo a pokud ano, vypíši to do editoru výstupů
					if (returnValue != null && returnValue.isWasCalled()) {
						// Zde byla metoda zavolána, tak zjistím, zda metoda má něco vracet:
						if (method.getReturnType().equals(Void.TYPE))
							// zde metoda nemá nic vracet:
							outputEditor.addResult(txtSuccess);
						else {
							// Zde metoda má něco vrátit, tak to vypíšu:
							printReturnedValueToEditor(txtSuccess + ": ",
									returnValue.getObjReturnValueWithTestedReference(), outputEditor);


							// Zda se mají v diagramu instancí zvýraznit příslušné reference - jsou li:
							instanceDiagram.checkHighlightOfInstances(returnValue.getObjReturnValue());


							// Otestuji, zda se vrátila instance, případně se zeptám, zda se má vytvořit
							// reprezentace v diagramu instancí.
							checkReturnedValue(returnValue.getObjReturnValue(), instanceDiagram,
									languageProperties);
						}
					}
					/*
					 * else - Zde se metodu nepodařilo zavolat, chyby byly již vypsány, tak bych
					 * mohl vypsat třeba neúspěch apod. Ale už nic dělat nebudu. Uživatel už o chybě
					 * ví.
					 *
					 * else - Zde by se do editoru výstupů vypsali informace o nastalé chybě pri zavolání metody:
					 * callMethodWithoutParameters
					 */
				}
			}
		});
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zkusí zjistit, zda se daná třída nachází v nějakém balíčku,
	 * poukd ano, tak v jakém (vždy by se třída měla ncházet v nějakém balíčku, v
	 * případě této aplikace by jinak nešla ani zkompilovat - proto defaultPackage).
	 * 
	 * 
	 * @return buď název balíčků, ve kterých (/ kterém) se daná třída nachází nebo
	 *         null, pokud se to z nějakého důvodu nepovede - viz kroky metody
	 */
	private String getPackageOfClass() {
		// Otestuji, zda je nějaká třída načtená:
		if (selectedClass != null) {
			// Zde je nějaká třída načtená, tak se pokusím zjístit její balíček:
			final Package packageName = selectedClass.getPackage();
			
			// Otestji, zda byl balíček nalezen a pokud ano, tak vrátím jeho text:
			if (packageName != null)
				return packageName.getName();
		}
		
		// Zde se balíček nepodařilo získat, důvody výše
		return null;
	}
	
	
	
	
	
	
	
	
	

	
	
	
	@Override
	public void setLanguage(Properties properties) {
		languageProperties = properties;
		
		if (properties != null) {
			menuOpen.setText(properties.getProperty("Ppm_ItemOpenInEditor", Constants.PPM_ITEM_OPEN_IN_EDITOR));
            menuCopyReference.setText(properties.getProperty("Ppm_MenuCopyReference", Constants.PPM_MENU_COPY_REFERENCE));
			menuStaticMethods.setText(properties.getProperty("Ppm_MenuStaticMethods", Constants.PPM_MENU_STATIC_METHODS));
			menuInheritedStaticMethods.setText(properties.getProperty("Ppm_MenuInheritedStaticMethods", Constants.PPM_MENU_INHERITED_STATIC_METHODS));
			menuInnerStaticClasses.setText(properties.getProperty("Ppm_MenuInnerStaticClasses", Constants.PPM_MENU_INNER_CLASSES));
			menuConstructors.setText(properties.getProperty("Ppm_MenuConstructors", Constants.PPM_MENU_CONSTRUCTORS));
			
			itemRename.setText(properties.getProperty("Ppm_ItemRename", Constants.PPM_ITEM_RENAME));
			itemRemove.setText(properties.getProperty("Ppm_ItemRemove", Constants.PPM_ITEM_REMOVE));

			itemOpenInCodeEditor.setText(properties.getProperty("Ppm_ItemOpenInCodeEditor", Constants.PPM_ITEM_OPEN_IN_CODE_EDITOR));
			itemOpenInProjectExplorer.setText(properties.getProperty("Ppm_ItemOpenInProjectExplorer", Constants.PPM_ITEM_OPEN_IN_PROJECT_EXPLORER));


            itemOpenInExplorerInOs.setText(properties.getProperty("Ppm_ItemOpenInExplorerInOs_Text", Constants.PPM_ITEM_OPEN_IN_EXPLORER_IN_OS_TEXT));
            itemOpenInDefaultAppInOs.setText(properties.getProperty("Ppm_ItemOpenInDefaultAppInOs_Text", Constants.PPM_ITEM_OPEN_IN_DEFAULT_APP_IN_OS_TEXT));

            itemOpenInExplorerInOs.setToolTipText(properties.getProperty("Ppm_ItemOpenInExplorerInOs_TT", Constants.PPM_ITEM_OPEN_IN_EXPLORER_IN_OS_TT));
            itemOpenInDefaultAppInOs.setToolTipText(properties.getProperty("Ppm_ItemOpenInDefaultAppInOs_TT", Constants.PPM_ITEM_OPEN_IN_DEFAULT_APP_IN_OS_TT));

			
			txtClassIsInRelationShipAlreadyTitle = properties.getProperty("Ppm_ClassIsInRelationShipAlreadyTitle", Constants.PPM_JOP_CLASS_IS_IN_RELATIONSHIP_ALREADY_TITLE);
			txtClassIsInRelationShipAlreadyText = properties.getProperty("Ppm_ClassIsInRelationShipAlreadyText", Constants.PPM_JOP_CLASS_IS_IN_RELATIONSHIP_ALREADY_TEXT);


			// Vložení cest a referencí do schránky:
            itemCopyAbsolutePath.setText(properties.getProperty("Ppm_ItemCopyAbsolutePath", Constants.PPM_ITEM_COPY_ABSOLUTE_PATH));
            itemCopyRelativePath.setText(properties.getProperty("Ppm_ItemCopyRelativePath", Constants.PPM_ITEM_COPY_RELATIVE_PATH));
            itemCopyReference.setText(properties.getProperty("Ppm_ItemCopyReference", Constants.PPM_ITEM_COPY_REFERENCE));

			
			// texty pro spuštění - pomocí metody main:
			txtFailedToCallMainMethod = properties.getProperty("Ppm_Txt_FailedToCallTheMainMethod", Constants.PPM_FAILED_TO_CALL_THE_MAIN_METHOD);
			txtError = properties.getProperty("Ppm_Txt_Error", Constants.PPM_TXT_ERROR);
			txtMethodIsInaccessible = properties.getProperty("Ppm_Txt_MethodIsInaccessible", Constants.PPM_TXT_METHOD_IS_INACCESSIBLE);
			txtIllegalArgumentException = properties.getProperty("Ppm_Txt_IlegalArgumentException", Constants.PPM_TXT_ILLEGAL_ARGUMENT_EXCEPTION);
			txtInvocationTargetException = properties.getProperty("Ppm_Txt_InvocationTargetException", Constants.PPM_TXT_INVOCATION_TARGET_EXCEPTION);
			txtStart = properties.getProperty("Ppm_Txt_Start", Constants.PPM_TXT_START);
			txtFailedToCreateInstanceText = properties.getProperty("Ppm_Txt_FailedToCreateInstanceText", Constants.PPM_TXT_FAILED_TO_CREATE_INSTANCE_TEXT);
			txtFailedToCreateInstanceTitle = properties.getProperty("Ppm_Txt_FailedToCreateInstanceTitle", Constants.PPM_TXT_FAILED_TO_CREATE_INSTANCE_TITLE);
			
			// Hláška 'Úspěch' po zavolání statické metody:
			txtSuccess = properties.getProperty("Ppm_Txt_Success", Constants.PPM_TXT_SUCCESS);
            txtNotSpecified = properties.getProperty("Ppm_Txt_NotSpecified", Constants.PPM_TXT_NOT_SPECIFIED);

		}

		else {
            menuOpen.setText(Constants.PPM_ITEM_OPEN_IN_EDITOR);
            menuCopyReference.setText(Constants.PPM_MENU_COPY_REFERENCE);
            menuStaticMethods.setText(Constants.PPM_MENU_STATIC_METHODS);
            menuInheritedStaticMethods.setText(Constants.PPM_MENU_INHERITED_STATIC_METHODS);
            menuInnerStaticClasses.setText(Constants.PPM_MENU_INNER_CLASSES);
            menuConstructors.setText(Constants.PPM_MENU_CONSTRUCTORS);

            itemRename.setText(Constants.PPM_ITEM_RENAME);
            itemRemove.setText(Constants.PPM_ITEM_REMOVE);

            itemOpenInCodeEditor.setText(Constants.PPM_ITEM_OPEN_IN_CODE_EDITOR);
            itemOpenInProjectExplorer.setText(Constants.PPM_ITEM_OPEN_IN_PROJECT_EXPLORER);

            itemOpenInExplorerInOs.setText(Constants.PPM_ITEM_OPEN_IN_EXPLORER_IN_OS_TEXT);
            itemOpenInDefaultAppInOs.setText(Constants.PPM_ITEM_OPEN_IN_DEFAULT_APP_IN_OS_TEXT);

            itemOpenInExplorerInOs.setToolTipText(Constants.PPM_ITEM_OPEN_IN_EXPLORER_IN_OS_TT);
            itemOpenInDefaultAppInOs.setToolTipText(Constants.PPM_ITEM_OPEN_IN_DEFAULT_APP_IN_OS_TT);
			
			txtClassIsInRelationShipAlreadyTitle = Constants.PPM_JOP_CLASS_IS_IN_RELATIONSHIP_ALREADY_TITLE;
			txtClassIsInRelationShipAlreadyText = Constants.PPM_JOP_CLASS_IS_IN_RELATIONSHIP_ALREADY_TEXT;


            itemCopyAbsolutePath.setText(Constants.PPM_ITEM_COPY_ABSOLUTE_PATH);
            itemCopyRelativePath.setText(Constants.PPM_ITEM_COPY_RELATIVE_PATH);
            itemCopyReference.setText(Constants.PPM_ITEM_COPY_REFERENCE);


            // texty pro spuštění - pomocí metody main:
            txtFailedToCallMainMethod = Constants.PPM_FAILED_TO_CALL_THE_MAIN_METHOD;
            txtError = Constants.PPM_TXT_ERROR;
            txtMethodIsInaccessible = Constants.PPM_TXT_METHOD_IS_INACCESSIBLE;
            txtIllegalArgumentException = Constants.PPM_TXT_ILLEGAL_ARGUMENT_EXCEPTION;
            txtInvocationTargetException = Constants.PPM_TXT_INVOCATION_TARGET_EXCEPTION;
            txtStart = Constants.PPM_TXT_START;
            txtFailedToCreateInstanceText = Constants.PPM_TXT_FAILED_TO_CREATE_INSTANCE_TEXT;
            txtFailedToCreateInstanceTitle = Constants.PPM_TXT_FAILED_TO_CREATE_INSTANCE_TITLE;

            // Hláška 'Úspěch' po zavolání statické metody:
            txtSuccess = Constants.PPM_TXT_SUCCESS;
            txtNotSpecified = Constants.PPM_TXT_NOT_SPECIFIED;
        }
    }
}