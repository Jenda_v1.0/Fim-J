package cz.uhk.fim.fimj.class_diagram;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.border.BevelBorder;

import cz.uhk.fim.fimj.swing_worker.ThreadSwingWorker;
import org.jgraph.graph.DefaultCellViewFactory;
import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.DefaultGraphModel;
import org.jgraph.graph.GraphConstants;
import org.jgraph.graph.GraphLayoutCache;
import org.jgraph.graph.GraphModel;
import org.jgraph.util.ParallelEdgeRouter;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.buttons_panel.ButtonsPanel;
import cz.uhk.fim.fimj.buttons_panel.PopupMenuRelationships;
import cz.uhk.fim.fimj.buttons_panel.TestAndCreateEdge;
import cz.uhk.fim.fimj.buttons_panel.TestAndCreateEdgeInterface;
import cz.uhk.fim.fimj.code_editor.CodeEditorDialog;
import cz.uhk.fim.fimj.file.CompileClass;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.font_size_form.CheckDuplicatesInDiagrams;
import cz.uhk.fim.fimj.font_size_form.FontSizePanelAbstract;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;
import cz.uhk.fim.fimj.menu.KindOfEdge;
import cz.uhk.fim.fimj.menu.KindOfEdgeInterface;
import cz.uhk.fim.fimj.output_editor.OutputEditor;
import cz.uhk.fim.fimj.preferences.PreferencesApi;
import cz.uhk.fim.fimj.project_explorer.ProjectExplorerDialog;

/**
 * Tato třída slouží jako diagram tříd, ve kterém jsou repreznetovány třídy v adresáři src v příslušném otevřeném
 * projektu a jejich vztahy - agregace, asociace, dědičnost, implementace rozhraí, ... případně komentáře k jednotlivým
 * třídám
 * <p>
 * Note: pro konkrétní třídu jsem zde zkoušel podobný přístup jako u diagramu instancí, a sice, že by byl nějaký objekt
 * coby třída, která obsahovala atributy pro název tridy, balíčky a promennou pro to, co vse se ma zobrazit atd. ale
 * nepodarilo se mi tuto tridu serializovat do souboru typu .xml, do ktereho se uklada graf pri ulozeni - zavreni
 * projektu, apod. aby sel znovu otevrit, Ale opět se jedná pouze o trochu více objektový přístup podobně jako u
 * poznamky k HashMape s instancemi tříd ve třídě Instances.java v balíčku instances
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class GraphClass extends GraphAbstract implements LanguageInterface, GraphClassInterface {

	private static final long serialVersionUID = 1L;


	/**
	 * "Komponenta", která obsahuje metody pro nastavení velikosti písem objektům v
	 * diagramu tříd.
	 */
	private static final FontSizeOperation_CD_Interface FONT_SIZE_OPERATION = new FontSizeOperation_CD();





	/**
	 * Proměnná, která vždy obsahuje aktuální velikost písma / fontu pro buňku,
	 * která reprezentuje v třídu diagramu tříd.
	 *
	 * Tato proměnná je zde potřeba, protože uživatel má možnost si pomocí
	 * klávesových zkratek Ctrl + nebo - nastavit velikost písma v objektech v
	 * diagramu tříd, tak v této proměné vždy bude velikost písma ve třídě.
	 *
	 * Je celkem jedno, která hodnota by zde byla, zda hodnota pro velikost písma v
	 * komentáři, nebo text hrany apod. Velikost písma ve třídě jsem vybral,
	 * protože je to taková nevíce vyskytující se část v diagramu tříd, taková, na
	 * kterou se každý nejvíce dívá.
	 */
	private static int classFontSize;


	/**
	 * Kolekce, do které se ukládají veškeré objekty v tomto diagram (Class
	 * diagramu)
	 */
	private static List<DefaultGraphCell> cellsList = new ArrayList<>();

	/**
	 * V podstatě model grafu - do něj se předávaí komponenty pro zobrazení v grafu:
	 */
	private static final GraphModel MODEL = new DefaultGraphModel();


	/**
	 * View, které zobrazí model objektu do grafu:
	 */
	private static final GraphLayoutCache VIEW = new GraphLayoutCache(MODEL, new DefaultCellViewFactory());



	/**
	 * Proměnná pro zjišťování typů označených objektů v diagramu tříd testuje, zda
	 * je označena hrana, pro asociaci, agregaci, komentar, trida, ...
	 */
	public static final KindOfEdgeInterface KIND_OF_EDGE = new KindOfEdge();





	// proměnné, do kterých se uloží hodnoty při změné nastavení grafu:
	private static String cellErrorText1, cellErrorTitle1, cellErrorText2, cellErrorTitle2, cellErrorTitle3,
			cellErrorText3, cellErrorText4, cellErrorTitle4, cellErrorTitle5, cellErrorText5, cellErrorTitle6,
			cellErrorText6, cellErrorTitle7, cellErrorText7, cellErrorTitle8, cellErrorText8, cellErrorText9,
			cellErrorTitle9, cellErrorText10, cellErrorTitle10, txtEdgeErrorTitle, txtEdgeErrorText,
			txtMissingClassText, txtMissingClassTitle, txtPath, txtInsertComment,
			// Texty pro implementaci rozhraní - chybové texty (některé)
			txtErrorWhileWritingInterfaceMethodsToClassText, txtErrorWhileWritingInterfaceMethodsToClassTT, txtClass,
			txtAlreadyImplementesInterface, txtAlreadyImplementesInterfaceTitle;


	/**
	 * Proměnná, ve které bude uložena nějaká hodnota ohledně toho, jaký tpp hrany
	 * se má vytvořit mezi nějakými třídami
	 */
	private static EdgeTypeEnum edgeType = null;



	// Proměné, do kterých budu vkládat označené buňky, pro jejich propojení hranou:
    private static DefaultGraphCell cell1 = null;
    private static DefaultGraphCell cell2 = null;




	private static TestAndCreateEdgeInterface testAndCreate;



	private final GraphInstance instanceDiagram;



	/**
	 * Proměnná, která bude držet referenci na instanci třídy ButtonsPanel, což je
	 * třída pro "levé menu" s tlačítky pro vytvoření nové třídy a zadávání vztahů
	 * mezi třídami, je to třeba, abych moh lvšechna tlačítka odznačit, po vytvoření
	 * příslušného vztahu mezi třídami, což se provede metodou v následující třídě:
	 */
	private static ButtonsPanel buttonsPanel;



	/**
	 * Logická proměnná, dle které se pozná, zda se mají v dialogu pro zavolání
	 * konstruktoru s parametry zpřístupnit veřejné a chráněné proměnné pro předání
	 * do příslušného parametru konstruktoru. Zpřístpnily by se uvedené proměnné v
	 * třídách v diagramu tříd, v instancích v diagramu instancí a proměnné
	 * vytvořené pomocí editoru příkazů.
	 */
	private static boolean makeVariablesAvailable;



	/**
	 * Logická proměnná, která značí, zda se má zpřístupnit možnost pro vytvoření
	 * nové instance příslušné třídy v případě, že se v parametru zavolaného
	 * konstruktoru pomocí kontextového menu nad třídou v diagramu tříd nachází
	 * parametr, který je typu nějaké třídy, která se nachází v diagramu tříd, tak
	 * do parametru toho konstruktoru budemožnost předat instance příslušné třídy,
	 * ale taky zda se má zpřístupnit položka pro vytvoření nové instance příslušné
	 * třídy pro předání do parametru příslušného konstruktoru.
	 */
	private static boolean showCreateNewInstanceOptionInCallConstructor;


	/**
	 * Proměnná, která značí, zda se v kontextovém menu nad třídou v diagramu tříd mají zpřístupnit i její soukromé
	 * konstruktory.
	 */
	private static boolean includePrivateConstructors;
	/**
	 * Proměnná, která značí, zda se v kontextovém menu nad třídou v diagramu tříd mají zpřístupnit i její chráněné
	 * konstruktory.
	 */
	private static boolean includeProtectedConstructors;
	/**
	 * Proměnná, která značí, zda se v kontextovém menu nad třídou v diagramu tříd mají zpřístupnit i její
	 * konstruktory,
	 * které jsou viditelné pouze v rámci balíču (tj. package-private).
	 */
	private static boolean includePackagePrivateConstructors;




	/**
	 * Proměnná, která značí, zda se v kontextovém menu nad třídou v diagramu tříd mají v rozevírací nabídce "Vnitřní
	 * statické třídy" zpřístupnit i soukromé statické třídy, které se nachází v označené třídě v diagramu.
	 */
	private static boolean includePrivateInnerStaticClasses;
	/**
	 * Proměnná, která značí, zda se v kontextovém menu nad třídou v diagramu tříd mají v rozevírací nabídce "Vnitřní
	 * statické třídy" zpřístupnit i chráněné statické třídy, které se nachází v označené třídě v diagramu.
	 */
	private static boolean includeProtectedInnerStaticClasses;
	/**
	 * Proměnná, která značí, zda se v kontextovém menu nad třídou v diagramu tříd mají v rozevírací nabídce "Vnitřní
	 * statické třídy" zpřístupnit i package-private statické třídy, které se nachází v označené třídě v diagramu.
	 */
	private static boolean includePackagePrivateInnerStaticClasses;


	/**
	 * Proměnná, která značí, zda se v kontextovém menu nad třídou v diagramu tříd mají v rozevírací nabídce "Vnitřní
	 * statické třídy" u jednotlivých vnitřních tříd zpřístupnit i jejich statické soukromé metody.
	 */
	private static boolean innerStaticClassesIncludePrivateStaticMethods;
	/**
	 * Proměnná, která značí, zda se v kontextovém menu nad třídou v diagramu tříd mají v rozevírací nabídce "Vnitřní
	 * statické třídy" u jednotlivých vnitřních tříd zpřístupnit i jejich statické chráněné metody.
	 */
	private static boolean innerStaticClassesIncludeProtectedStaticMethods;
	/**
	 * Proměnná, která značí, zda se v kontextovém menu nad třídou v diagramu tříd mají v rozevírací nabídce "Vnitřní
	 * statické třídy" u jednotlivých vnitřních tříd zpřístupnit i jejich statické package-private metody.
	 */
	private static boolean innerStaticClassesIncludePackagePrivateStaticMethods;











	/**
	 * Proměnná, která obsahuje výčtovou hodnotu, která značí jaký editor kódu se má
	 * použít při otevření třídy pomocí dvojkliku levým tlačítkem myši nebo kláesou
	 * Enter.
	 */
	private static KindOfCodeEditor kindOfCodeEditor;



	/**
	 * Logická proměnná, která značí, zda se mají v dialogu pro zavolání
	 * konstruktoru zobrazit na výběr i metody, jejichž návratové hodnoty lze předat
	 * do parametru příslušného konstruktoru.
	 */
	private static boolean makeMethodsAvailableForCallConstructor;




	/**
	 * Proměnná, která značí, zda se mají při vytovření vztahu implementace rozhraní
	 * mezi příslušnými třídami v diagramu tříd rovnou naimplementovat i příslušné
	 * metody v rozhraní.
	 */
	private static boolean addInterfaceMethods;
















	public GraphClass(final OutputEditor outputEditor, final GraphInstance instanceDiagram) {
		super(MODEL, VIEW);

		this.outputEditor = outputEditor;
		this.instanceDiagram = instanceDiagram;

		// Výchozí nastavení grafu:
		chartSettings();

		// Načtu si výchozí nastavení pro graf:
		setClassDiagramProperties();

		addMouseListener();

		addMouseWheelListener();

		myKeyListener();

		testAndCreate = new TestAndCreateEdge(this);
	}







	/**
	 * Metoda, která přidá na do tohoto diagramu tříd událost na stisknutí klávesy
	 * Del (Delete), která odstraní vždy jeden označený objekt v diagramu
	 */
	private void myKeyListener() {
		addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DELETE) {
					// Otestuji, zda je označený objekt bunka a ta bunka rerezentuje trida a ta
					// jeste existuje,
					// pokud ne, maji se s ni smazat vsechny hrany, pokud ano, jen se testuje ta
					// označený objekt
					final boolean result = existClassInSrc((DefaultGraphCell) getSelectionCell());

                    App.WRITE_TO_FILE.removeSelectedObject(GraphClass.this, languageProperties, !result);
				}

				// Zde se jedná o stisknutí enteru, a pokud je označená buňka třída, tak se
				// otevře
				// tak se otevře v průzkumníkovi projektů
				else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					// Uložím si označený objekt v diagramu tříd:
					final Object selectedObj = getSelectionCell();

					// Otestuji, zda je něco označeno a to něco je buňka reprezentující třídu:
					if (selectedObj != null && KIND_OF_EDGE.isClassCell((DefaultGraphCell) selectedObj))
						// Uživatel klikl dvakrát na nějakou třídu, tak se ji pokusím otevřít ve
						// zvoleném editoru kódu:
						openClassInEditor(kindOfCodeEditor, (DefaultGraphCell) selectedObj);
				}





				// Kombinace Ctrl a + (plus) pro zvětšení písma.

				// Toto nefungovalo ???
//				else if (e.isControlDown() && e.getKeyChar() == KeyEvent.VK_PLUS)
//				else if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_PLUS)
				else if (e.isControlDown() && e.getKeyChar() == '+') {
					if (classFontSize < Constants.FONT_SIZE_MAX) {
						FONT_SIZE_OPERATION.quickSetupOfFontSize(++classFontSize, GraphClass.this);

						/*
						 * Výše se nastavila nějaká velikost písma pro všechny komponenty v diagramu
						 * tříd, tak zde potřebuji otestovat, zda v diagramu tříd neexistují nějaké
						 * duplicitní hodnoty, například buňka pro třídu a komentář jsou stejné, nebo
						 * některé hrany (s výjimkou implementace rozhraní) apod.
						 */

						/*
						 * V tomto případě ještě potřebuji otestovat, jestli jsou buňky pro komentář a
						 * třídu stejné, pokud ano, pak se jim nastavily výchozí vlastnosti, tak musím
						 * ještě příslušnou proměnnou nastavit na stejnou velikosti, jinak by se při
						 * každém zvětšní / změnšení velikosti písma ta velikost zvětšila / změnšila o
						 * větší množství než o 1 z toho výchozího nastavení.
						 *
						 * Note:
						 * Také by zde stačiloo jen odečíst jedničku neboli dekrementovat tu hodnotu
						 * classFontSize.
						 */
						if (FontSizePanelAbstract.checkDuplicateClassAndCommentProperties(GraphClass.this,
                                checkDuplicatesInDiagrams))
							classFontSize = Constants.CD_FONT_CLASS.getSize();

						FontSizePanelAbstract.checkDuplicateEdgesInCd(GraphClass.this, checkDuplicatesInDiagrams);
					}
				}



				// Kombinace Ctrl a - (mínus) pro změnšení písma.
				else if (e.isControlDown() && e.getKeyChar() == KeyEvent.VK_MINUS) {
					if (classFontSize > Constants.FONT_SIZE_MIN) {
						FONT_SIZE_OPERATION.quickSetupOfFontSize(--classFontSize, GraphClass.this);

						/*
						 * Výše se nastavila nějaká velikost písma pro všechny komponenty v diagramu
						 * tříd, tak zde potřebuji otestovat, zda v diagramu tříd neexistují nějaké
						 * duplicitní hodnoty, například buňka pro třídu a komentář jsou stejné, nebo
						 * některé hrany (s výjimkou implementace rozhraní) apod.
						 */

						/*
						 * V tomto případě ještě potřebuji otestovat, jestli jsou buňky pro komentář a
						 * třídu stejné, pokud ano, pak se jim nastavily výchozí vlastnosti, tak musím
						 * ještě příslušnou proměnnou nastavit na stejnou velikosti, jinak by se při
						 * každém zvětšní / změnšení velikosti písma ta velikost zvětšila / změnšila o
						 * větší množství než o 1 z toho výchozího nastavení.
						 *
						 * Note:
						 * Také by zde stačiloo jen přičíst jedničku neboli inkrementovat tu hodnotu
						 * classFontSize.
						 */
						if (FontSizePanelAbstract.checkDuplicateClassAndCommentProperties(GraphClass.this,
                                checkDuplicatesInDiagrams))
							classFontSize = Constants.CD_FONT_CLASS.getSize();

						FontSizePanelAbstract.checkDuplicateEdgesInCd(GraphClass.this, checkDuplicatesInDiagrams);
					}
				}
			}
		});
	}




















	/**
	 * Metoda, která do grafu přidá Mouse listener, který bude sloužit k tomu, abych
	 * poznal jaké objekty uživatel označil, a dle toho posoudil přidání hrany, na
	 * otevření "menu" pravým tlačítkem, apod.
	 */
	private void addMouseListener() {
		addMouseListener(new MouseAdapter() {

			// Tlačítka musí reagovat až na uvolnění tlačítka mysši, protože kdyby reagovaly na stisknutí, tak by se
			// takto označila první třídy - buňka, a kdyby uživatel klikl třeba do "prázdného místa v grafu", tak by byla ve chvíli stisknutí
			// tlačítka myši pořád ještě označena původní buňka - třídy, což by nemělo, proto reakce na puštění tlačítka myši

			@Override
			public void mouseReleased(final MouseEvent e) {
                // Bylo stisknuto levé tlačítko myši
                if (e.getButton() == MouseEvent.BUTTON1) {
                    // Pokud se má přidat nějaká hrana:
                    if (edgeType == null)
                        return;

                    // tak si uložím označené buňky:
                    // Nejprve otestuji, zda se nejedná o hranu ale o buňku::
                    if (getSelectionCell() instanceof DefaultEdge || getSelectionCell() == null)
                        return;

                    if (cell1 == null)
                        cell1 = (DefaultGraphCell) getSelectionCell();

                    else if (cell2 == null)
                        cell2 = (DefaultGraphCell) getSelectionCell();


                    // Komentář - stačí první označená buňka:
                    if (cell1 != null && edgeType.equals(EdgeTypeEnum.COMMENT)) {
                        testAndCreate.addComment(cell1);
                        edgeType = null;
                        cell1 = null;
                        // Odznačím tlačítka
                        buttonsPanel.setSelectedMenuButtons(null);
                    }


                    // Pokud budou obě označené (ani jedna se != null), tak stačí zjistit,
                    // kterou hranu mám mezi těmito dvěma buňkami - třídami vytvořit:
                    if (cell1 == null || cell2 == null)
                        return;

                    if (cell1 != cell2) {
                        if (edgeType.equals(EdgeTypeEnum.ASSOCIATION)) {
                            /*
                             * Zde potřebuji otestovat, zda se má vztah asociace reprezentovat jako jedna
                             * hrana s šipkami na obou koncích nebo jako dvě agregace a dle toho vytvořit
                             * buď klasický vztah asociace, nebo mezi označenými buňkami vytvořit dvě
                             * agregace
                             */

                            if (showAssociationThroughAggregation) {
                                testAndCreate.addAggregation_1_ku_1_Edge(
                                        new DefaultGraphCell[]{cell1, cell2});
                                testAndCreate.addAggregation_1_ku_1_Edge(
                                        new DefaultGraphCell[]{cell2, cell1});
                            }

                            else
                                testAndCreate.addAssociationEdge(new DefaultGraphCell[] { cell1, cell2 });


                            /*
                             * Na konec potřebuji spustit vlákno, které zkontroluje nově vytvořený vztah
                             * typu (symetrická) asociace, protože se jako výchozí kardinalita
                             * (multiplicita) nastavila na 1 : 1, ale už může existovat nějaký vztah, tak je
                             * třeba aby se nastavila, resp. v tomto případě nově vypočítala příslušná
                             * kardianalita a správně nastavila:
                             */
                            ThreadSwingWorker.runMyThread((new CheckRelationShipsBetweenClassesThread(
                                    GraphClass.this, outputEditor, showRelationShipsToItself,
                                    showAssociationThroughAggregation)));

                            /*
                             * Zde je třeba aktualizovat hodnoty, které jsou (/ mohou být) zobrazeny v jednotlivých
                             * instancích. Například zobrazené určité proměnné apod. Jelikož se přidal nový vtah a
                             * tím nová proměnná, je třeba zobrazené hodnoty v instancích aktualizovat, jinak by
                             * mohli zobrazovat chybné hodnoty nebo by se vůbec nezobrazily apod.
                             */
                            instanceDiagram.updateInstanceCellValues();

                            /*
                             * Note:
                             *
                             * Okno pro doplňování příkazů v editoru příkazů se aktualizuje v kompilaci tříd
                             * ve vlákně pro aktualzaci vztahů mezi třídami (pokud se třídy zkompilují bez chyby)
                             */
                        }


                        else if (edgeType.equals(EdgeTypeEnum.EXTENDS))
                            testAndCreate.addExtendsEdge(new DefaultGraphCell[] {cell1, cell2});


                        else if (edgeType.equals(EdgeTypeEnum.IMPLEMENTATION)) {
                            /*
                             * Zde se jedná o přidání vztahu pro implementaci rozhraní v nějaké třídě.
                             *
                             * Pokud je ale povoleno, že se mají naimportovat metody, které se v příslušném
                             * rozhraní nacházejí, pak je postup následující:
                             *
                             * zkompiluji se veškeré třídy, abych zjistil, zda si mohu načíst rozhraní a
                             * přidat do té třídy ty metody z rozhraní, dále si pak načtu obě ty třídy, z
                             * rozhraní si vezmu veškeré metody a ty implementuji do příslušné třídy.
                             *
                             * Note:
                             *
                             * Vím, že bych mohl některé podmínky nedávat tak vnořené, například otestovat,
                             * zda existuje adresář src, jinak skončit apod. Ale potřebuji, aby se celá
                             * metoda vykonala až do konce, aby se na konci odznačil tlačtka a nastavily
                             * příslušné komponenty na příslušné hodnoty, i když vím, že bych to mohl dát do
                             * metody.
                             */

                            /*
                             * Získám zkusím si načíst cestu k adresáři src v aktuálně otevřeném projektu.
                             */
                            final String pathToSrc = App.READ_FILE.getPathToSrc();

                            /*
                             * Pokud se nepodařilo načíst cestu k aktuálně otevřenému projektu - k adresáři
                             * src v něm, pak mohu skončit, akorát zkusímzavolat ještě metodu pro
                             * implementaci rozhraní, a ta by uživateli ohládsila, že tenadresář neexistuje,
                             * případně nějaké podrobnější informace.
                             */
                            if (pathToSrc != null) {
                                /*
                                 * Načtu si list souborů, resp. list tříd, které se nachází v adresáři src,
                                 * resp. v diagramu tříd, jedná se o list tříd, které se mají zkompilovat.
                                 */
                                final List<File> classesForCompile = App.READ_FILE
                                        .getFileFromCells(pathToSrc, cellsList, null, languageProperties);

                                new CompileClass(languageProperties);

                                /*
                                 * V případě, že se mají vygenerovávat metody z rozhraní, a jsou třídy
                                 * validní, pak se ty metody z rozhraní vygenerují do příslušné třídy.
                                 */
                                if (addInterfaceMethods && CompileClass.compileClass(outputEditor,
                                        classesForCompile, false, false)) {
                                    /*
                                     * Načtu si třídu, kam se maí vygenerovat příslušné metody.
                                     */
                                    final Class<?> clazzClazz = App.READ_FILE
                                            .loadCompiledClass(cell1.toString(), outputEditor);

                                    /*
                                     * Načtu si rozhraní, které obsahuje metody, které se mají implementovat
                                     * do třídy clazzClazz.
                                     */
                                    final Class<?> clazzInterface = App.READ_FILE
                                            .loadCompiledClass(cell2.toString(), outputEditor);

                                    /*
                                     * Otestuji, zda se podařilo načíst obě třídy a ta první je "klasická"
                                     * třídy a ta druhá načtená třída je rozhraní.
                                     */
                                    if (clazzClazz != null && clazzInterface != null
                                            && !clazzClazz.isInterface() && clazzInterface.isInterface()) {

                                        /*
                                         * Otestuji, zda příslušná třída ještě neimplementuje příslušné
                                         * rozhraní, pokud ano, pak vyhodím oznámení.
                                         */
                                        if (!Arrays.asList(clazzClazz.getInterfaces())
                                                .contains(clazzInterface)) {
                                            final Method[] interfaceMethods = clazzInterface
                                                    .getDeclaredMethods();

                                            if (testAndCreate.addImplementedMethods(cell1,
                                                    interfaceMethods)) {
                                                // Zde přidám do hlavičky implementaci příslušného rozhraní:
                                                testAndCreate.addImplementsEdge(
                                                        new DefaultGraphCell[] { cell1, cell2 });

                                                /*
                                                 * Získám si cestu k adresáři bin v aktuálně otevřeném
                                                 * projektu, abych ji mohl předat do metody pro aktualizaci
                                                 * diagramu
                                                 */
                                                final String pathToBinDir = App.READ_FILE.getPathToBin();

                                                if (pathToBinDir != null)
                                                    /*
                                                     * Zkompiluji veškeré třídy v diagramu tříd a rovnou
                                                     * aktualizuji instance v diagramu instancí a buňky s
                                                     * instancemi, aby se projevily nové metody.
                                                     *
                                                     * Dále je to potřeba, aby uživatel viděl, že se
                                                     * podařilo úspěšně / neúspěšně zapsat nové metody.
                                                     */
                                                    CompileClass.compileClass(outputEditor,
                                                            classesForCompile, true, true);
                                            }

                                            else
                                                JOptionPane.showMessageDialog(GraphClass.this,
                                                        txtErrorWhileWritingInterfaceMethodsToClassText
                                                                + ": " + cell1.toString(),
                                                        txtErrorWhileWritingInterfaceMethodsToClassTT,
                                                        JOptionPane.ERROR_MESSAGE);

                                        }

                                        else
                                            JOptionPane.showMessageDialog(GraphClass.this,
                                                    txtClass + " '" + cell1.toString() + "' "
                                                            + txtAlreadyImplementesInterface + " '"
                                                            + cell2.toString() + "'",
                                                    txtAlreadyImplementesInterfaceTitle,
                                                    JOptionPane.ERROR_MESSAGE);

                                    }

                                    else
                                        testAndCreate
                                                .addImplementsEdge(new DefaultGraphCell[] { cell1, cell2 });
                                }

                                else
                                    testAndCreate
                                            .addImplementsEdge(new DefaultGraphCell[] { cell1, cell2 });
                            }

                            else
                                testAndCreate.addImplementsEdge(new DefaultGraphCell[] { cell1, cell2 });
                        }






                        else if (edgeType.equals(EdgeTypeEnum.AGGREGATION_1_1)) {
                            // Zde si mohu vybrat, jaka trida ma byt celek a jaka cast,
                            // V tomto priade bude prvni oznacena trida celek a druha cast:
                            testAndCreate.addAggregation_1_ku_1_Edge(new DefaultGraphCell[] {cell1, cell2});

                            // a  v tomto pripade bude prvni oznacena trida cast a druha celek
//										testAndCreate.addAggregation_1_ku_1_Edge(new DefaultGraphCell[] {cell2, cell1});

                            // Zde spustím vlákno, aby otestovalo, v tomto konkrétním případě, zda se nemá náhodou někde
                            // vytvořit asociace nebo agregace mezi třídami - když zde uživatel vytvořil mezi nějakými třídami agregaci 1 : 1
                            // tak je možné, že už je to vztah asociace, toto vlákno to otestuje, ale musím nad ním zavolat metodu join,
                            // aby se počkalo, než doběhne, jinak mohou nastat vyjímky.

                            // vytvořím si ThreadSwingWorker a předám mu bývalé vlákno, aby věděl, co má
                            // prověst - metodu run
                            ThreadSwingWorker.runMyThread((new CheckRelationShipsBetweenClassesThread(
                                    GraphClass.this, outputEditor, showRelationShipsToItself,
                                    showAssociationThroughAggregation)));

                            /*
                             * Zde je třeba aktualizovat hodnoty, které jsou (/ mohou být) vypsány v instancích v
                             * diagramu instancí.
                             *
                             * Npříklad pokud se v jednotlivých buňkách (/ instancích) budou vypisovat metody, tak
                             * výše mohla přidat nová metoda, tak je třeba, aby se v té buňce také zobrazila.
                             */
                            instanceDiagram.updateInstanceCellValues();

                            /*
                             * Note:
                             *
                             * Okno s hodnotami pro automatické doplňování / dokončování příkazů v editoru příkazů se
                             * autmaticky aktualizuje výše ve vlákné pro aktualizaci vztahů mezi třídami v případě,
                             * že se třídy zkompilují bez chyby. Takže nově přidané metody z rozhraní v tom okně pro
                             * doplňování příkazů se v podstatě pro uživatele ihned projeví.
                             */
                        }


                        else if (edgeType.equals(EdgeTypeEnum.AGGREGATION_1_N)) {
                            // Zde si mohu vybrat, jaka trida ma byt celek a jaka cast,
                            // V tomto případě bude první označena trida celek a druha cast:
                            testAndCreate.addAggregation_1_ku_N_Edge(new DefaultGraphCell[]{cell1, cell2});

                            // V tomto případe bude první oznaceni trida cast a druha celek:
                            // testAndCreate.addAggregation_1_ku_N_Edge(new DefaultGraphCell[] {cell2, cell1});


                            /*
                             * Výše se přidala nová proměnná - List. Tato aktualizace vzthů mezi třídami by neměla
                             * být potřeba, protože se přidal pouze vztah
                             * agregace 1 : N, což nijak neovlivní ostatní vztahy. Zde se jedná pouze o to, že se
                             * přidala nová proměnná, tak je třeba zkompilovat třídy
                             * a přenačíst buňky / instance v diagramu instancí, aby se v nich projevila nově přidaná
                             * proměnná. Proto se zde místo
                             * "pouhé" kompilace tříd rovnou i aktualizují vztahy mezi třídami.
                             */
                            ThreadSwingWorker.runMyThread((new CheckRelationShipsBetweenClassesThread(
                                    GraphClass.this, outputEditor, showRelationShipsToItself,
                                    showAssociationThroughAggregation)));

                            /*
                             * Zde je vhodné aktualizovat hodnoty, které se nachází v instancích v diagramu instancí.
                             * Výše se totiž přidal nový atribut pro List. Proto je vhodné aktualizovat buňky /
                             * instance v diagramu instancí, aby se projevily změny - tedy doplněný atribut. Protože
                             * jestli má uživatel nastaveno, že se mají zobrazovat atrbuty tridy, tak by se změny
                             * neprojevily, kdyby zde nebyla tato aktualizace.
                             */
                            instanceDiagram.updateInstanceCellValues();

                            /*
                             * Note:
                             *
                             * Okno pro automatické doplňování / dokončování příkazů v editoru příkazů bylo
                             * aktualizováno výše ve vlákně pro aktualizaci vztahů mezi třídami. Ale to není úplně
                             * nezbytné, protože
                             * se nově vytvořená proměnná (List) vytvořila s viditelnéstí "private", takže v tom okně
                             * stejně nebude na výběr. Ale pro případ, že by to někdo změnil, tak aby se i tak
                             * změny projevily to zde raději nechám povolené (aktualizaci okna pro doplňování příkazů
                             * v editoru příkazů).
                             */
                        }
                    }

                    else
                        JOptionPane.showMessageDialog(GraphClass.this, txtEdgeErrorText, txtEdgeErrorTitle,
                                JOptionPane.ERROR_MESSAGE);

                    /*
                     * Výše se vytvořil nějaký vztah mezi třídami, tak je třeba nastavit veškeré proměnné obsahující
                     * hodnoty pro vytvoření vztahu na null a odznačit příslušné tlačítko v toolbaru, aby si uživatel
                     * nemyslel, že se pořád nějaký vztah vytváří.
                     */
                    deselectChoosedEdge();
                }




				// Bylo stisknuto pravé tlačítko
				else if (e.getButton() == MouseEvent.BUTTON3) {
					// Ověřím si, zda bylo kliknut na třídu a né na komentář, hranu nebo "pozadí" - aby byla označena pouze třída
					// To zjistím tak, že si zjistím text třídy a dle toho, zda v adresáři src v projektu příslušná třídä existuje
					// pokud ano, tak otevřu "dialogové okno" - takové menu s možnostma - viz toto okno - menu

					final DefaultGraphCell selectedCell = (DefaultGraphCell) getSelectionCell();

					// Otestuji, zda bylo vůbec něco označeno, takže nebylo kliknuto myši do "prostoru" grafu - mimo objekty - prostě na pozadí
					if (selectedCell != null) {
						final String cellText = selectedCell.toString().replace(".", File.separator);


						final String pathToSrcDir = App.READ_FILE.getPathToSrc();


						if (pathToSrcDir != null) {
							final String pathToMarkedClass = pathToSrcDir + File.separator + cellText + ".java";


							// Otestuji, zda se jedná o buňku, která reprezentuje třídu a tato třída se stále nachází na disku, kde má:
							if (KIND_OF_EDGE.isClassCell(selectedCell) && ReadFile.existsFile(pathToMarkedClass)) {
                                // Zde se hledaný soubor - označená třída našla, a neobsahuje žádné vazby s třídamí v
                                // grafu, tak mohu zobrazit menu:
                                final PopupMenu pMenu = new PopupMenu(selectedCell, GraphClass.this, outputEditor,
                                        instanceDiagram, languageProperties, includePrivateMethods,
                                        includeProtectedMethods, includePackagePrivateMethods,
                                        makeVariablesAvailableForCallMethod);

                                // Nastavím absolutní cestu k třídě:
                                pMenu.setAbsolutePathToClass(pathToMarkedClass);
                                /*
                                 * Zde se nastaví veškeré cesty / hondoty, aby je bylo možné zkopírovat / vložit do
                                 * schránky.
                                 *
                                 * Zde už je třeba mát nastavenou absolutní cestu (výše), protože se veškeré cesty
                                 * nastaví do popisků - tooltipů k příslušným položkám, které vkládají text do schránky.
                                 */
                                pMenu.preparePathsForInsert();

                                pMenu.show(GraphClass.this, e.getX(), e.getY());
                            }

							else {
								// Zde uživatel klikl pravým tlačítkem myši na hranu nebo na buňku reprezentující komentář nebo na buňku reprezentující
								// třídu, jenže ta už na zadaném místě neexistuje, například byla smazána, přejmenována, přesunuta, apod.

								// tak zobrazím menu pouze s jedním tlačítkem - odebrat:
								final PopupMenu pMenu = new PopupMenu(GraphClass.this, languageProperties);
								pMenu.show(GraphClass.this, e.getX(), e.getY());
							}
						}
					}


					else {
						/*
						 * Zde je selectdCell null, takže není označen žádný objekt v diagramu tříd,
						 * uživatel tedy klikl na pozadí plátna / diagramu, tak otevřu kontextové menu,
						 * které bude obsahovat položky pro označení jednoho ze vztahů pro vytvoření
						 * mezi třídami nebo komentáře.
						 */
						final PopupMenuRelationships popupMenuRelationships = new PopupMenuRelationships(buttonsPanel,
								GraphClass.this);
						popupMenuRelationships.setLanguage(languageProperties);
						popupMenuRelationships.show(GraphClass.this, e.getX(), e.getY());
					}
				}
			}










			// Následuje posluchač pro dvojnásobné kliknutí na nějký objekt v diagramu tříd:
			// Pokud uživatel klikne na nějakou buňku reprezentující třídu v diagramu tříd,
			// tak se zobrazí v editoru kódu:
			@Override
			public void mouseClicked(MouseEvent e) {
				// Otestuji, zda uživatel klikl dvakrát levým tlačítkem myši:
				if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON1) {
					// Uložím si označený objekt v diagramu tříd:
					final Object selectedObj = getSelectionCell();

					// Otestuji, zda je něco označeno a to něco je buňka reprezentující třídu:
					if (selectedObj != null && KIND_OF_EDGE.isClassCell((DefaultGraphCell) selectedObj))
						// Uživatel klikl dvakrát na nějakou třídu, tak se ji pokusím otevřít ve
						// zvoleném výchozím editoru kódu:
						openClassInEditor(kindOfCodeEditor, (DefaultGraphCell) selectedObj);
				}
			}
		});
	}













    /**
	 * Metoda, která slouží pro otevření třídy (cell) v jednom z editoru zdrojového
	 * kódu.
	 *
	 * @param kindOfCodeEditor
     *            - výčtová hodnota, která značí konkrétní editor zdrojového kódu.
	 *
	 * @param cell
	 *            označená třída - buňka v diagramu tříd.
	 */
	private void openClassInEditor(final KindOfCodeEditor kindOfCodeEditor, final DefaultGraphCell cell) {
		/*
		 * Proměnná, kterou jsem zde uvedl pouze jako potenciílní "chybu", když by
		 * náhodou byla hodnota kindOfCodeEditor null, ale to by nikdy nastat nemělo.
		 */
		final KindOfCodeEditor temp;

		if (kindOfCodeEditor == null)
			temp = Constants.CD_KIND_OF_DEFAULT_CODE_EDITOR;
		else
			temp = kindOfCodeEditor;



		switch (temp) {
		case SOURCE_CODE_EDITOR:
			openClassInCodeEditor(cell, null);
			break;

		case PROJECT_EXPLORER:
            openClassInProjectExplorer(cell, null);
			break;

		default:
			openClassInCodeEditor(cell, null);
			break;
		}
	}














	/**
	 * Metoda, pro "Výchozí" nastavení grafu metoda obsahuje prvky, díky kterým
	 * uživatel bude mít povolené jen "nějaké" operace třeba aby mohl přemísťovat
	 * buňky v grafu, nešlo hýbar s textem na hraně, apod
	 */
	private void chartSettings() {
		// Půjde hýbat s Buňkami:
		setMoveable(true);

		// Aby se buňky nedostaly za okraj grafu
		// Půjdou posunout doprava a dolů - "zvětší" se okno grafu, ale nepůjdou posunout doleva a nahoru
		setMoveBelowZero(false);

		// Hrana se nesmí při pohybu odpojit:
		setDisconnectOnMove(false);

		// Povolení antialisingu:
		// zaoblené hrany - přechody, ...
        setAntiAliased(true);

		// Poklepáním na buňku půjde změnit její název - to přes dialog, abych věděl to mám kde změnit
		// Resp. jakou třídu v jakém balíčku, ...
		// Zde nastaavím, že půjde vše editovat, pak si jen zakážu co nemá jít editovat
		setEditable(true);

		// Při změné názvu se velikost buňky nepřizpůsobní, to si uživatel musí "buňku roztáhnout" sám
		// Já nastavím pouze výchozí velikst buňky, zbytek - ohledně velikosti je na uživateli
		getGraphLayoutCache().setAutoSizeOnValueChange(false);

		// Nepůjdu hýbat s textem na hranách
		setEdgeLabelsMovable(Constants.CD_COM_LABELS_MOVABLE);

		// Barva pozadí:
		setBackground(Constants.CD_BACKGROUND_COLOR);
	}










	@Override
	public void addClassCell(final String name) {
		// Instance nové buňky:
		final DefaultGraphCell classCell = new DefaultGraphCell(name);

		// Nastavím její atributy:
		final Map<?, ?> attributes = classCell.getAttributes();

		// Parametry, vlastnosti a design:
		// Velikost a umístění obdélníku:
		GraphConstants.setBounds(attributes, new Rectangle2D.Double(10d, 10d, 150d, 50d));


		// Zakážu editaci názvu:
		GraphConstants.setEditable(attributes, false);



		// Pro následující nastavení otestuje jestil exituje soubor s výchozím nastavením pro graf (defaultproperites)
		// POkud ano, načtu z něj hodnoty, pokud ne, použiji výchozí:
		if (defaultProperties != null) {
			// Font:
			final int fontType = Integer.parseInt(defaultProperties.getProperty("ClassFontType", Integer.toString(Constants.CD_FONT_CLASS.getStyle())));
			final int fontSize = Integer.parseInt(defaultProperties.getProperty("ClassFontSize", Integer.toString(Constants.CD_FONT_CLASS.getSize())));
			GraphConstants.setFont(attributes, new Font(Constants.CD_FONT_CLASS.getName(), fontType, fontSize));


			// Zarovnání pro písmo a obrázky:
			final String verticalAlignment = defaultProperties.getProperty("VerticalAlignment", String.valueOf(Constants.CD_VERTICAL_ALIGNMENT));
			GraphConstants.setVerticalAlignment(attributes, Integer.parseInt(verticalAlignment));

			final String horizontalAlignment = defaultProperties.getProperty("HorizontalAlignment", String.valueOf(Constants.CD_HORIZONTAL_ALIGNMENT));
			GraphConstants.setHorizontalAlignment(attributes, Integer.parseInt(horizontalAlignment));


			// Barva písma
			final String textColor = defaultProperties.getProperty("ForegroundColor", Integer.toString(Constants.CD_TEXT_COLOR.getRGB()));
			GraphConstants.setForeground(attributes, new Color(Integer.parseInt(textColor)));


			// Nyní si pro nastavení barvy pozadí musím načíst logickou proměnnou, dle které poznám, zda se má obarvit
			// pozadí buňky  jednou barvou nebo dvěmí:
			final boolean oneColor = Boolean.parseBoolean(defaultProperties.getProperty("ClassOneBgColorBoolean", String.valueOf(Constants.CD_ONE_BG_COLOR_BOOLEAN)));

			if (oneColor) {
				// Zde se nastaví jedna barva pro celé pozadí buňky:
				final String bgColor = defaultProperties.getProperty("ClassOneBackgroundColor", Integer.toString(Constants.CD_ONE_BG_COLOR.getRGB()));
				GraphConstants.setBackground(attributes, new Color(Integer.parseInt(bgColor)));
				GraphConstants.setGradientColor(attributes, new Color(Integer.parseInt(bgColor)));

                // Ohraničení:
				GraphConstants.setBorder(attributes, BorderFactory.createBevelBorder(BevelBorder.RAISED, new Color(Integer.parseInt(bgColor)), new Color(Integer.parseInt(bgColor))));
			}

			else {
				// Zde se naství různe barvy pro obě strany buňky:

				// Barva pozadí - levý horní roh:
                final String backgroundColor = defaultProperties.getProperty("ClassBackgroundColor", Integer.toString(Constants.CD_CLASS_BACKGROUND_COLOR.getRGB()));
				GraphConstants.setBackground(attributes, new Color(Integer.parseInt(backgroundColor)));

				// Barva pozadí - pravý dolní roh:
				final String gradientColor = defaultProperties.getProperty("GradientColor", Integer.toString(Constants.CD_GRADIENT_COLOR.getRGB()));
				GraphConstants.setGradientColor(attributes, new Color(Integer.parseInt(gradientColor)));

				// Ohraničení:
				GraphConstants.setBorder(attributes, BorderFactory.createBevelBorder(BevelBorder.RAISED, new Color(Integer.parseInt(backgroundColor)), new Color(Integer.parseInt(gradientColor))));
			}


			/*
			 * Nastavení (ne) průhlednésti buňky - aby (ne) byla průhledná
			 *
			 * - Pokud bude do metody setOpaque nastavena hodnota true, pak buňka nebude
			 * průhledná.
			 *
			 * - Pokud bude do metody setOpaque nastavena hodnota false, pak buňka bude
			 * průhledná.
			 *
			 * Proto, když uživatel v souboru změní hodnotu, tak aby správně "pochopil" ten
			 * zápis hodnoty dám, že es v souboru zapíše hodnota true pro půrhlednost a
			 * false pro neprůhlednost buňky, ale zde v metodě setOpaque musím příslušnou
			 * hodnotu znegovat, nechtěl jsem to udělat v souboru, abych tím uživatele
			 * "nemátl", protože je v nastavení nastaven text "Buňka bude průhledná" a pokud
			 * je to označeno, pak je to jako true, že má být průhledná.
			 *
			 * Jinak bych do nastavení musel dát text například v syntaxi: "Buňka nebude
			 * průhledná", ale nechtělo se mi v nastavení takto "otáček" hodnotu, proto to
			 * raději otočím zde v kódu:
			 */
			GraphConstants.setOpaque(attributes, !Boolean.parseBoolean(defaultProperties.getProperty("ClassCellOpaque", String.valueOf(Constants.CD_OPAQUE_CLASS_CELL))));
		}


		// Zde nebyl načten soubor z workspace ani z adresáře aplikace, tak použiji výchozí nastavení z constants:
		else {
			// Font:
			GraphConstants.setFont(attributes, Constants.CD_FONT_CLASS);

            // Zarovnání pro písmo a obrázky:
			GraphConstants.setVerticalAlignment(attributes, Constants.CD_VERTICAL_ALIGNMENT);
			GraphConstants.setHorizontalAlignment(attributes, Constants.CD_HORIZONTAL_ALIGNMENT);


			// Barva písma
			GraphConstants.setForeground(attributes, Constants.CD_TEXT_COLOR);



			if (Constants.CD_ONE_BG_COLOR_BOOLEAN) {
				final Color color = Constants.CD_ONE_BG_COLOR;

				// Barva pozadí - levý horní roh:
				GraphConstants.setBackground(attributes, color);

				// Barva pozadí - pravý dolní roh:
				GraphConstants.setGradientColor(attributes, color);

				// Ohraničení:
				GraphConstants.setBorder(attributes, BorderFactory.createBevelBorder(BevelBorder.RAISED, color, color));

			}

			else {

				// Barva pozadí - levý horní roh:
				GraphConstants.setBackground(attributes, Constants.CD_CLASS_BACKGROUND_COLOR);

				// Barva pozadí - pravý dolní roh:
				GraphConstants.setGradientColor(attributes, Constants.CD_GRADIENT_COLOR);

				// Ohraničení:
				GraphConstants.setBorder(attributes, BorderFactory.createBevelBorder(BevelBorder.RAISED, Constants.CD_CLASS_BACKGROUND_COLOR, Constants.CD_GRADIENT_COLOR));

			}



			// Nastavení neprůhlednésti buňky - aby nebyla průhledná (info výše u této metody)
			GraphConstants.setOpaque(attributes, !Constants.CD_OPAQUE_CLASS_CELL);
		}



		// Buňce přidám tzv. Default Port = aby se automaticky vypočítalo, kde se má konec hrany spojit s buňkou
		// na jaké části buňky se hrana připojí - uprostřed, na straně, dole, něco mezi, ...
		classCell.addPort();

		// Nyní je připraven design buňky, tak ji přidám do kolekce všech objektů v grafu:
		cellsList.add(classCell);

		updateDiagram(cellsList);
	}











	@Override
	public void addComment(final Object classCell, final String textToCell) {
		if (classCell instanceof DefaultGraphCell) {
            if (cellsList.contains(classCell)) {
				// Zde vím, žebuňka existuje :
                // Instance bunky pro komentář: - připravená
				final DefaultGraphCell commentCell = prepareCommentCell(textToCell);

				final DefaultEdge edge = new DefaultEdge();
				// Zdroj hrany bude komentář a cíl třída, ke které patří
				edge.setSource(commentCell.getChildAt(0));
                edge.setTarget(((DefaultGraphCell) classCell).getChildAt(0));



                int lineStyle = -1;

				// Nastavení parametry hrany pro komentář:
				final Map<?, ?> attributes = edge.getAttributes();

				if (defaultProperties != null) {
                    // Barva hrany:
					final String edgeColor = defaultProperties.getProperty("CommentEdgeColor", Integer.toString(Constants.CD_COM_EDGE_COLOR.getRGB()));
					GraphConstants.setLineColor(attributes, new Color(Integer.parseInt(edgeColor)));

					// Popis hrany bude poděl čáry:
					GraphConstants.setLabelAlongEdge(attributes, Boolean.parseBoolean(defaultProperties.getProperty("CommentEdgeLabelAlongEdge", String.valueOf(Constants.CD_COM_LABEL_ALONG_EDGE))));

					// Styl hrany:
					GraphConstants.setLineStyle(attributes, Integer.parseInt(defaultProperties.getProperty("CommentEdgeLineStyle", String.valueOf(Constants.CD_COM_EDGE_LINE_STYLE))));
					lineStyle = GraphConstants.getLineStyle(attributes);

                    // konec hrany:
					GraphConstants.setLineEnd(attributes, Integer.parseInt(defaultProperties.getProperty("CommentEdgeLineEnd", String.valueOf(Constants.CD_COM_LINE_END))));

					// začátek hrany:
					GraphConstants.setLineBegin(attributes, Integer.parseInt(defaultProperties.getProperty("CommentEdgeLineBegin", String.valueOf(Constants.CD_COM_LINE_BEGIN))));

					// Šířka hrany:
					GraphConstants.setLineWidth(attributes, Float.parseFloat(defaultProperties.getProperty("CommentEdgeLineWidth", String.valueOf(Constants.CD_COM_LINE_WIDTH))));

					// Výplň na konci hrany:
                    GraphConstants.setEndFill(attributes, Boolean.parseBoolean(defaultProperties.getProperty("CommentEdgeEndFill", String.valueOf(Constants.CD_COM_LINE_END_FILL))));

					// Výplň hrany na jejím začátku:
					GraphConstants.setBeginFill(attributes, Boolean.parseBoolean(defaultProperties.getProperty("CommentEdgeBeginFill", String.valueOf(Constants.CD_COM_LINE_BEGIN_FILL))));

                    // Barva písma pro poznámky na hraně:
					final String textColor = defaultProperties.getProperty("CommentEdgeTextColor", Integer.toString(Constants.CD_COM_EDGE_FONT_COLOR.getRGB()));
					GraphConstants.setForeground(attributes, new Color(Integer.parseInt(textColor)));


                    // Font písma pro poznámky - text na hrane:
					final int fontSize = Integer.parseInt(defaultProperties.getProperty("CommentEdgeLineFontSize", Integer.toString(Constants.CD_COM_EDGE_FONT.getSize())));
					final int fontStyle = Integer.parseInt(defaultProperties.getProperty("CommentEdgeLineFontStyle", Integer.toString(Constants.CD_COM_EDGE_FONT.getStyle())));
					GraphConstants.setFont(attributes, new Font(Constants.CD_COM_EDGE_FONT.getName(), fontStyle, fontSize));
				}


				else {
					// Barva hrany:
					GraphConstants.setLineColor(attributes, Constants.CD_COM_EDGE_COLOR);

					// Popis hrany bude poděl čáry:
					GraphConstants.setLabelAlongEdge(attributes, Constants.CD_COM_LABEL_ALONG_EDGE);

					// Styl hrany:
					GraphConstants.setLineStyle(attributes, Constants.CD_COM_EDGE_LINE_STYLE);
					lineStyle = GraphConstants.getLineStyle(attributes);

					// konec hrany:
					GraphConstants.setLineEnd(attributes, Constants.CD_COM_LINE_END);

					// začátek hrany:
					GraphConstants.setLineBegin(attributes, Constants.CD_COM_LINE_BEGIN);

					// Šířka hrany:
					GraphConstants.setLineWidth(attributes, Constants.CD_COM_LINE_WIDTH);

					// Výplň na konci hrany:
					GraphConstants.setEndFill(attributes, Constants.CD_COM_LINE_END_FILL);

					// Výplň na začátku hrany:
					GraphConstants.setBeginFill(attributes, Constants.CD_COM_LINE_BEGIN_FILL);

					// Barva písma pro poznámky na hraně:
					GraphConstants.setForeground(attributes, Constants.CD_COM_EDGE_FONT_COLOR);

					// Font pro text na hreně:
					GraphConstants.setFont(attributes, Constants.CD_COM_EDGE_FONT);
				}



				/*
				 * pokud je hodnout routingu -1, pak je nastavena na normal, tak musím dát typ routingu Default, jinak
				 * se používá jeden z typů - bezier, spline nebo orthogonal, a proto aby tyto typy hrany - čáry fungovaly,
				 * tak je třeba nastavit routing na simple, jinak se změna neprojeví
				 */
				if (lineStyle > -1)
					GraphConstants.setRouting(attributes, GraphConstants.ROUTING_SIMPLE);
				else GraphConstants.setRouting(attributes, GraphConstants.ROUTING_DEFAULT);



				// Pro styl hrany je třeba jistý typ Routingu, pokud má být Orthogonal, ...
				GraphConstants.setRouting(attributes, Constants.CD_COM_EDGE_ROUTING);

				// Hrana nepůjde odpojit:
				GraphConstants.setDisconnectable(attributes, false);

				// Hrany nepůjdou spojit:
				GraphConstants.setConnectable(attributes, false);


				// Přídám hranu a komentář do kolekce pro vykreslení do grafu:
				cellsList.add(commentCell);
				cellsList.add(edge);

				updateDiagram(cellsList);
            }
			else JOptionPane.showMessageDialog(this, cellErrorText1, cellErrorTitle1, JOptionPane.ERROR_MESSAGE);
		}
		else JOptionPane.showMessageDialog(this, cellErrorText2, cellErrorTitle2, JOptionPane.ERROR_MESSAGE);
	}








	/**
	 * Metoda, která vrátí připravenou - "naformátovanou - hotový design" instanci
	 * buňky, která slouží v grafu jako komentář
	 *
	 * @param textToCell
	 *            - text, který se má vložit do buňky coby nového komentáře, nebo
	 *            null, pokud se má použít výchozí text
	 *
	 * @return přednastavenou buňku, připravenou k zobrazení
	 */
	private DefaultGraphCell prepareCommentCell(final String textToCell) {
		// Instance nové buňky:

		final DefaultGraphCell commentCell;
		// Otestuji, zda se nachází nějaký text v parametru, resp. text, který semá přidat do buňky pro
		// vytvoření komentáře - výchozí text nebo text z komentáře, který byl připojen k původní
		// buňce před změnou designu tříd, apod.
		if (textToCell != null)
			commentCell = new DefaultGraphCell(textToCell);

		// Zde se má použít výchozí text - nebyl předán komentář:
		else commentCell = new DefaultGraphCell(txtInsertComment);



        // Nastavím její atributy:
		final Map<?, ?> attributes = commentCell.getAttributes();

		// Parametry, vlastnosti a design:
		// Velikost a umístění obdélníku:
		GraphConstants.setBounds(attributes, new Rectangle2D.Double(10d, 10d, 180d, 70d));


		// Zde půjde editovat hodnota buňky:
		GraphConstants.setEditable(attributes, true);


		// Pro následující nastavení otestuje jestil exituje soubor s výchozím nastavením pro graf (defaultproperites)
		// POkud ano, načtu z něj hodnoty, pokud ne, použiji výchozí:
		if (defaultProperties != null) {
			// Font:
			final int fontType = Integer.parseInt(defaultProperties.getProperty("CommentFontType", Integer.toString(Constants.CD_FONT_COMMENT.getStyle())));
			final int fontSize = Integer.parseInt(defaultProperties.getProperty("CommentFontSize", Integer.toString(Constants.CD_FONT_COMMENT.getSize())));
			GraphConstants.setFont(attributes, new Font(Constants.CD_FONT_COMMENT.getName(), fontType, fontSize));


			// Zarovnání pro písmo a obrázky:
			final String verticalAlignment = defaultProperties.getProperty("CommentVerticalAlignment", String.valueOf(Constants.CD_COM_VERTICAL_ALIGNMENT));
			GraphConstants.setVerticalAlignment(attributes, Integer.parseInt(verticalAlignment));

			final String horizontalAlignment = defaultProperties.getProperty("CommentHorizontalAlignment", String.valueOf(Constants.CD_COM_HORIZONTAL_ALIGNMENT));
			GraphConstants.setHorizontalAlignment(attributes, Integer.parseInt(horizontalAlignment));


			// Barva písma
			final String textColor = defaultProperties.getProperty("CommentForegroundColor", Integer.toString(Constants.CD_COM_FOREGROUND_COLOR.getRGB()));
			GraphConstants.setForeground(attributes, new Color(Integer.parseInt(textColor)));



			// načtu si logickouk proměnnou, dle které zjistím kolik barev se má nastavit pro pozadí buňky:
			final boolean oneBgColor = Boolean.parseBoolean(defaultProperties.getProperty("CommentOneBgColorBoolean", String.valueOf(Constants.CD_COM_ONE_BG_COLOR_BOOLEAN)));


            if (oneBgColor) {
				final String bgColor = defaultProperties.getProperty("CommentOneBackgroundColor", Integer.toString(Constants.CD_COM_ONE_BG_COLOR.getRGB()));

				// Barva pozadí - levý horní roh:
                GraphConstants.setBackground(attributes, new Color(Integer.parseInt(bgColor)));

				// Barva pozadí - pravý dolní roh:
				GraphConstants.setGradientColor(attributes, new Color(Integer.parseInt(bgColor)));

				// Ohraničení:
				GraphConstants.setBorder(attributes, BorderFactory.createBevelBorder(BevelBorder.RAISED, new Color(Integer.parseInt(bgColor)), new Color(Integer.parseInt(bgColor))));

			}

			else {
				// Barva pozadí - levý horní roh:
				final String backgroundColor = defaultProperties.getProperty("CommentBackgroundColor", Integer.toString(Constants.CD_COM_BACKGROUND_COLOR.getRGB()));
				GraphConstants.setBackground(attributes, new Color(Integer.parseInt(backgroundColor)));

				// Barva pozadí - pravý dolní roh:
				final String gradientColor = defaultProperties.getProperty("CommentGradientColor", Integer.toString(Constants.CD_COM_GRADIENT_COLOR.getRGB()));
				GraphConstants.setGradientColor(attributes, new Color(Integer.parseInt(gradientColor)));

				// Ohraničení:
				GraphConstants.setBorder(attributes, BorderFactory.createBevelBorder(BevelBorder.RAISED, new Color(Integer.parseInt(backgroundColor)), new Color(Integer.parseInt(gradientColor))));

			}



			/*
			 * Nastavení (ne) průhlednésti buňky - aby (ne) byla průhledná
			 *
			 * - Pokud bude do metody setOpaque nastavena hodnota true, pak buňka nebude
			 * průhledná.
			 *
			 * - Pokud bude do metody setOpaque nastavena hodnota false, pak buňka bude
			 * průhledná.
			 *
			 * Proto, když uživatel v souboru změní hodnotu, tak aby správně "pochopil" ten
			 * zápis hodnoty dám, že es v souboru zapíše hodnota true pro půrhlednost a
			 * false pro neprůhlednost buňky, ale zde v metodě setOpaque musím příslušnou
			 * hodnotu znegovat, nechtěl jsem to udělat v souboru, abych tím uživatele
			 * "nemátl", protože je v nastavení nastaven text "Buňka bude průhledná" a pokud
			 * je to označeno, pak je to jako true, že má být průhledná.
			 *
			 * Jinak bych do nastavení musel dát text například v syntaxi: "Buňka nebude
			 * průhledná", ale nechtělo se mi v nastavení takto "otáček" hodnotu, proto to
			 * raději otočím zde v kódu:
			 */
			GraphConstants.setOpaque(attributes, !Boolean.parseBoolean(defaultProperties.getProperty("CommentCellOpaque", String.valueOf(Constants.CD_OPAQUE_COMMENT_CELL))));
		}


		// Zde nebyl načten soubor z workspace ani z adresáře aplikace, tak použiji výchozí nastavení z constants:
		else {
			// Font:
			GraphConstants.setFont(attributes, Constants.CD_FONT_COMMENT);

			// Zarovnání pro písmo a obrázky:
			GraphConstants.setVerticalAlignment(attributes, Constants.CD_COM_VERTICAL_ALIGNMENT);
			GraphConstants.setHorizontalAlignment(attributes, Constants.CD_COM_HORIZONTAL_ALIGNMENT);

			// Barva písma
			GraphConstants.setForeground(attributes, Constants.CD_COM_FOREGROUND_COLOR);

			if (Constants.CD_COM_ONE_BG_COLOR_BOOLEAN) {
				final Color color = Constants.CD_COM_ONE_BG_COLOR;
				// Barva pozadí - levý horní roh:
				GraphConstants.setBackground(attributes, color);

				// Barva pozadí - pravý dolní roh:
				GraphConstants.setGradientColor(attributes, color);

				// Ohraničení:
				GraphConstants.setBorder(attributes, BorderFactory.createBevelBorder(BevelBorder.RAISED, color, color));

			}

			else {
				// Barva pozadí - levý horní roh:
				GraphConstants.setBackground(attributes, Constants.CD_COM_BACKGROUND_COLOR);

				// Barva pozadí - pravý dolní roh:
				GraphConstants.setGradientColor(attributes, Constants.CD_COM_GRADIENT_COLOR);

				// Ohraničení:
				GraphConstants.setBorder(attributes, BorderFactory.createBevelBorder(BevelBorder.RAISED, Constants.CD_COM_BACKGROUND_COLOR, Constants.CD_COM_GRADIENT_COLOR));

			}


			// Nastavení neprůhlednésti buňky - aby nebyla průhledná (info výše)
            GraphConstants.setOpaque(attributes, !Constants.CD_OPAQUE_COMMENT_CELL);
		}


		// Buňce přidám tzv. Default Port = aby se automaticky vypočítalo, kde se má konec hrany spojit s buňkou
		// na jaké části buňky se hrana připojí - uprostřed, na straně, dole, něco mezi, ...
		commentCell.addPort();

		// vrátím upravenou buňku:
		return commentCell;
	}










	@Override
	public void addAssociationEdge(final Object srcCell, final Object desCell, final int cardinalityAsc) {
		if (srcCell instanceof DefaultGraphCell && desCell instanceof DefaultGraphCell) {
			if (cellsList.contains(srcCell) && cellsList.contains(desCell)) {
				final DefaultEdge edge = new DefaultEdge();

				// K hraně přidám její konec a začátek - připojení třídy:
				edge.setSource(((DefaultGraphCell) srcCell).getChildAt(0));
				edge.setTarget(((DefaultGraphCell) desCell).getChildAt(0));


				int lineStyle = -1;

                // Nastavím parametry hrany - design, funkce, ...
				final Map<?, ?> attributes = edge.getAttributes();

				if (defaultProperties != null) {
					// Barva hrany:
                    final String colorLine = defaultProperties.getProperty("AscEdgeLineColor", Integer.toString(Constants.CD_ASC_EDGE_COLOR.getRGB()));
					GraphConstants.setLineColor(attributes, new Color(Integer.parseInt(colorLine)));

					// Popis hrany bude poděl čáry:
                    GraphConstants.setLabelAlongEdge(attributes, Boolean.parseBoolean(defaultProperties.getProperty("AscEdgeLabelAlongEdge", String.valueOf(Constants.CD_ASC_LABEL_ALONG_EDGE))));

					// Styl hrany:
					GraphConstants.setLineStyle(attributes, Integer.parseInt(defaultProperties.getProperty("AscEdgeLineStyle", String.valueOf(Constants.CD_ASC_EDGE_LINE_STYLE))));
					lineStyle = GraphConstants.getLineStyle(attributes);

					// konec hrany:
					GraphConstants.setLineEnd(attributes, Integer.parseInt(defaultProperties.getProperty("AscEdgeLineEnd", Integer.toString(Constants.CD_ASC_EDGE_LINE_END))));

					// začátek hrany:
					GraphConstants.setLineBegin(attributes, Integer.parseInt(defaultProperties.getProperty("AscEdgeLineBegin", Integer.toString(Constants.CD_ASC_EDGE_LINE_BEGIN))));

					// Šířka hrany:
					GraphConstants.setLineWidth(attributes, Float.parseFloat(defaultProperties.getProperty("AscEdgeLineWidth", String.valueOf(Constants.CD_ASC_EDGE_LINE_WIDTH))));

					// Výplň na konci hrany:
					GraphConstants.setEndFill(attributes, Boolean.parseBoolean(defaultProperties.getProperty("AscEdgeEndFill", String.valueOf(Constants.CD_ASC_EDGE_END_FILL))));

					// Výplň na začátku hrany:
					GraphConstants.setBeginFill(attributes, Boolean.parseBoolean(defaultProperties.getProperty("AscEdgeBeginFill", String.valueOf(Constants.CD_ASC_EDGE_BEGIN_FILL))));

					// Barva písma pro poznámky na hraně:
					final String textColor = defaultProperties.getProperty("AscEdgeLineTextColor", Integer.toString(Constants.CD_ASC_EDGE_FONT_COLOR.getRGB()));
					GraphConstants.setForeground(attributes, new Color(Integer.parseInt(textColor)));

					// Font písma pro poznámky - text na hrane:
					final int fontSize = Integer.parseInt(defaultProperties.getProperty("AscEdgeLineFontSize", Integer.toString(Constants.CD_ASC_EDGE_LINE_FONT.getSize())));
					final int fontStyle = Integer.parseInt(defaultProperties.getProperty("AscEdgeLineFontStyle", Integer.toString(Constants.CD_ASC_EDGE_LINE_FONT.getStyle())));
					GraphConstants.setFont(attributes, new Font(Constants.CD_ASC_EDGE_LINE_FONT.getName(), fontStyle, fontSize));
				}


				else {
					// Barva hrany:
					GraphConstants.setLineColor(attributes, Constants.CD_ASC_EDGE_COLOR);

					// Popis hrany bude poděl čáry:
					GraphConstants.setLabelAlongEdge(attributes, Constants.CD_ASC_LABEL_ALONG_EDGE);

					// Styl hrany:
					GraphConstants.setLineStyle(attributes, Constants.CD_ASC_EDGE_LINE_STYLE);
					lineStyle = GraphConstants.getLineStyle(attributes);

					// konec hrany:
					GraphConstants.setLineEnd(attributes, Constants.CD_ASC_EDGE_LINE_END);

					// začátek hrany:
					GraphConstants.setLineBegin(attributes, Constants.CD_ASC_EDGE_LINE_BEGIN);

					// Šířka hrany:
					GraphConstants.setLineWidth(attributes, Constants.CD_ASC_EDGE_LINE_WIDTH);

					// Výplň na konci hrany:
					GraphConstants.setEndFill(attributes, Constants.CD_ASC_EDGE_END_FILL);

					// Výplň na začátku hrany:
					GraphConstants.setBeginFill(attributes, Constants.CD_ASC_EDGE_BEGIN_FILL);

					// Barva písma pro poznámky na hraně:
					GraphConstants.setForeground(attributes, Constants.CD_ASC_EDGE_FONT_COLOR);

					// Font písma pro poznámky - text na hrane:
					GraphConstants.setFont(attributes, Constants.CD_ASC_EDGE_LINE_FONT);
				}


				/*
				 * pokud je hodnout routingu -1, pak je nastavena na normal, tak musím dát typ routingu Default, jinak
				 * se používá jeden z typů - bezier, spline nebo orthogonal, a proto aby tyto typy hrany - čáry fungovaly,
				 * tak je třeba nastavit routing na simple, jinak se změna neprojeví
				 */
				if (lineStyle > -1)
					GraphConstants.setRouting(attributes, GraphConstants.ROUTING_SIMPLE);
				else GraphConstants.setRouting(attributes, GraphConstants.ROUTING_DEFAULT);


				// Aby hrana nešla odpojit ani připojit:
				GraphConstants.setDisconnectable(attributes, false);
				GraphConstants.setConnectable(attributes, false);



				// Přidám k hraně multiplicitu - 1 : 1:
				final Object[] labels = {cardinalityAsc, cardinalityAsc};
				final Point2D[] labelPositions = { new Point2D.Double(GraphConstants.PERMILLE * 7 / 8, -20),
						new Point2D.Double(GraphConstants.PERMILLE / 8, -20) };

				GraphConstants.setExtraLabelPositions(attributes, labelPositions);
				GraphConstants.setExtraLabels(attributes, labels);




				// Přidám hranu do kolekce pro vykreslení.
				cellsList.add(edge);
				//
				updateDiagram(cellsList);
			}
			else JOptionPane.showMessageDialog(this, cellErrorText3, cellErrorTitle3, JOptionPane.ERROR_MESSAGE);
		}
		else JOptionPane.showMessageDialog(this, cellErrorText4, cellErrorTitle4, JOptionPane.ERROR_MESSAGE);
	}









	@Override
	public void addExtendsEdge(final Object srcCell, final Object desCell) {
		if (srcCell instanceof DefaultGraphCell && desCell instanceof DefaultGraphCell) {
			if (cellsList.contains(srcCell) && cellsList.contains(desCell)) {
                final DefaultEdge edge = new DefaultEdge();

				// K hraně přidám její konec a začátek - připojení třídy:
				edge.setSource(((DefaultGraphCell) srcCell).getChildAt(0));
				edge.setTarget(((DefaultGraphCell) desCell).getChildAt(0));



				int lineStyle = -1;


				// Nastavím parametry hrany - design, funkce, ...
				final Map<?, ?> attributes = edge.getAttributes();

				if (defaultProperties != null) {
                    // Barva hrany:
					final String colorLine = defaultProperties.getProperty("ExtEdgeLineColor", Integer.toString(Constants.CD_EXT_EDGE_COLOR.getRGB()));
                    GraphConstants.setLineColor(attributes, new Color(Integer.parseInt(colorLine)));

					// Popis hrany bude poděl čáry:
					GraphConstants.setLabelAlongEdge(attributes, Boolean.parseBoolean(defaultProperties.getProperty("ExtEdgeLabelAlongEdge", String.valueOf(Constants.CD_EXT_LABEL_ALONG_EDGE))));

					// Styl hrany:
					GraphConstants.setLineStyle(attributes, Integer.parseInt(defaultProperties.getProperty("ExtEdgeLineStyle", String.valueOf(Constants.CD_EXT_EDGE_LINE_STYLE))));
					lineStyle = GraphConstants.getLineStyle(attributes);

					// konec hrany:
					GraphConstants.setLineEnd(attributes, Integer.parseInt(defaultProperties.getProperty("ExtEdgeLineEnd", Integer.toString(Constants.CD_EXT_EDGE_LINE_END))));

					// začátek hrany:
					GraphConstants.setLineBegin(attributes, Integer.parseInt(defaultProperties.getProperty("ExtEdgeLineBegin", Integer.toString(Constants.CD_EXT_EDGE_LINE_BEGIN))));

					// Šířka hrany:
					GraphConstants.setLineWidth(attributes, Float.parseFloat(defaultProperties.getProperty("ExtEdgeLineWidth", String.valueOf(Constants.CD_EXT_EDGE_LINE_WIDTH))));

					// Výplň na konci hrany:
					GraphConstants.setEndFill(attributes, Boolean.parseBoolean(defaultProperties.getProperty("ExtEdgeEndLineFill", String.valueOf(Constants.CD_EXT_EDGE_END_FILL))));

					// Výplň na začátku hrany:
					GraphConstants.setBeginFill(attributes, Boolean.parseBoolean(defaultProperties.getProperty("ExtEdgeBeginLineFill", String.valueOf(Constants.CD_EXT_EDGE_BEGIN_FILL))));

					// Barva písma pro poznámky na hraně:
					final String textColor = defaultProperties.getProperty("ExtEdgeLineTextColor", Integer.toString(Constants.CD_EXT_EDGE_FONT_COLOR.getRGB()));
					GraphConstants.setForeground(attributes, new Color(Integer.parseInt(textColor)));

					// Font písma pro poznámky - text na hrane:
					final int fontSize = Integer.parseInt(defaultProperties.getProperty("ExtEdgeLineFontSize", Integer.toString(Constants.CD_EXT_EDGE_LINE_FONT.getSize())));
					final int fontStyle = Integer.parseInt(defaultProperties.getProperty("ExtEdgeLineFontStyle", Integer.toString(Constants.CD_EXT_EDGE_LINE_FONT.getStyle())));
					GraphConstants.setFont(attributes, new Font(Constants.CD_EXT_EDGE_LINE_FONT.getName(), fontStyle, fontSize));
				}


				else {
					// Barva hrany:
					GraphConstants.setLineColor(attributes, Constants.CD_EXT_EDGE_COLOR);

					// Popis hrany bude poděl čáry:
					GraphConstants.setLabelAlongEdge(attributes, Constants.CD_EXT_LABEL_ALONG_EDGE);

					// Styl hrany:
					GraphConstants.setLineStyle(attributes, Constants.CD_EXT_EDGE_LINE_STYLE);
					lineStyle = GraphConstants.getLineStyle(attributes);

					// konec hrany:
					GraphConstants.setLineEnd(attributes, Constants.CD_EXT_EDGE_LINE_END);

					// začátek hrany:
					GraphConstants.setLineBegin(attributes, Constants.CD_EXT_EDGE_LINE_BEGIN);

                    // Šířka hrany:
					GraphConstants.setLineWidth(attributes, Constants.CD_EXT_EDGE_LINE_WIDTH);

					// Výplň na konci hrany:
					GraphConstants.setEndFill(attributes, Constants.CD_EXT_EDGE_END_FILL);

					// Výplň na začátku hrany:
					GraphConstants.setBeginFill(attributes, Constants.CD_EXT_EDGE_BEGIN_FILL);

					// Barva písma pro poznámky na hraně:
					GraphConstants.setForeground(attributes, Constants.CD_EXT_EDGE_FONT_COLOR);

					// Font písma pro poznámky - text na hrane:
					GraphConstants.setFont(attributes, Constants.CD_EXT_EDGE_LINE_FONT);
				}


				/*
				 * pokud je hodnout routingu -1, pak je nastavena na normal, tak musím dát typ routingu Default, jinak
				 * se používá jeden z typů - bezier, spline nebo orthogonal, a proto aby tyto typy hrany - čáry fungovaly,
				 * tak je třeba nastavit routing na simple, jinak se změna neprojeví
				 */
				if (lineStyle > -1)
					GraphConstants.setRouting(attributes, GraphConstants.ROUTING_SIMPLE);
				else GraphConstants.setRouting(attributes, GraphConstants.ROUTING_DEFAULT);



				// Aby hrana nešla odpojit ani připojit:
				GraphConstants.setDisconnectable(attributes, false);
				GraphConstants.setConnectable(attributes, false);


				// Přidám hranu do kolekce pro vykreslení.
				cellsList.add(edge);

				updateDiagram(cellsList);
			}
			else JOptionPane.showMessageDialog(this, cellErrorText5, cellErrorTitle5, JOptionPane.ERROR_MESSAGE);
		}
		else JOptionPane.showMessageDialog(this, cellErrorText6, cellErrorTitle6, JOptionPane.ERROR_MESSAGE);
    }










	@Override
	public void addImplementsEdge(final Object srcCell, final Object desCell) {
		if (srcCell instanceof DefaultGraphCell && desCell instanceof DefaultGraphCell) {
			if (cellsList.contains(srcCell) && cellsList.contains(desCell)) {
				final DefaultEdge edge = new DefaultEdge();

				// K hraně přidám její konec a začátek - připojení třídy:
				edge.setSource(((DefaultGraphCell) srcCell).getChildAt(0));
				edge.setTarget(((DefaultGraphCell) desCell).getChildAt(0));


				int lineStyle = -1;



				// Nastavím parametry hrany - design, funkce, ...
				final Map<?, ?> attributes = edge.getAttributes();

				if (defaultProperties != null) {
					// Barva hrany:
					final String colorLine = defaultProperties.getProperty("ImpEdgeLineColor", Integer.toString(Constants.CD_IMP_EDGE_COLOR.getRGB()));
					GraphConstants.setLineColor(attributes, new Color(Integer.parseInt(colorLine)));

                    // Popis hrany bude poděl čáry:
					GraphConstants.setLabelAlongEdge(attributes, Boolean.parseBoolean(defaultProperties.getProperty("ImpEdgeLabelAlongEdge", String.valueOf(Constants.CD_IMP_LABEL_ALONG_EDGE))));

					// Styl hrany:
					GraphConstants.setLineStyle(attributes, Integer.parseInt(defaultProperties.getProperty("ImpEdgeLineStyle", String.valueOf(Constants.CD_IMP_EDGE_LINE_STYLE))));
                    lineStyle = GraphConstants.getLineStyle(attributes);

                    // konec hrany:
					GraphConstants.setLineEnd(attributes, Integer.parseInt(defaultProperties.getProperty("ImpEdgeLineEnd", Integer.toString(Constants.CD_IMP_EDGE_LINE_END))));

					// začátek hrany:
					GraphConstants.setLineBegin(attributes, Integer.parseInt(defaultProperties.getProperty("ImpEdgeLineBegin", Integer.toString(Constants.CD_IMP_EDGE_LINE_BEGIN))));

					// Šířka hrany:
					GraphConstants.setLineWidth(attributes, Float.parseFloat(defaultProperties.getProperty("ImpEdgeLineWidth", String.valueOf(Constants.CD_IMP_EDGE_LINE_WIDTH))));

					// Výplň na konci hrany:
					GraphConstants.setEndFill(attributes, Boolean.parseBoolean(defaultProperties.getProperty("ImpEdgeEndLineFill", String.valueOf(Constants.CD_IMP_EDGE_END_FILL))));

					// Výplň na začátku hrany:
					GraphConstants.setBeginFill(attributes, Boolean.parseBoolean(defaultProperties.getProperty("ImpEdgeBeginLineFill", String.valueOf(Constants.CD_IMP_EDGE_BEGIN_FILL))));


					// Barva písma pro poznámky na hraně:
					final String textColor = defaultProperties.getProperty("ImpEdgeLineTextColor", Integer.toString(Constants.CD_IMP_EDGE_FONT_COLOR.getRGB()));
					GraphConstants.setForeground(attributes, new Color(Integer.parseInt(textColor)));

					// Font písma pro poznámky - text na hrane:
					final int fontSize = Integer.parseInt(defaultProperties.getProperty("ImpEdgeLineFontSize", Integer.toString(Constants.CD_IMP_EDGE_LINE_FONT.getSize())));
					final int fontStyle = Integer.parseInt(defaultProperties.getProperty("ImpEdgeLineFontStyle", Integer.toString(Constants.CD_IMP_EDGE_LINE_FONT.getStyle())));
					GraphConstants.setFont(attributes, new Font(Constants.CD_IMP_EDGE_LINE_FONT.getName(), fontStyle, fontSize));



					// Čerchovaná hrana - čára:
					// Ukládá se ve formátu: [X, X, X, ...]
					// tak si akorát z textu odstraním závorky (substring), dále odeberu mezery a předám je do jednorozměrného pole typu float:
					String patternText = defaultProperties.getProperty("ImpEdgeDashPattern", Arrays.toString(Constants.CD_IMP_PATTERN));
					// text bez závorek:
					patternText = patternText.trim().substring(1, patternText.length() - 1);
					// pole čísel typu string:
					final String[] partsOfPattern = patternText.trim().replace("\\s", "").split(",");

					float[] patternArray = new float[partsOfPattern.length];

					for (int i = 0; i < partsOfPattern.length; i++)
						patternArray[i] = Float.parseFloat(partsOfPattern[i]);

                    // Nastavení hrany - čerchovaně:
					GraphConstants.setDashPattern(attributes, patternArray);
				}


				else {
					// Barva hrany:
					GraphConstants.setLineColor(attributes, Constants.CD_IMP_EDGE_COLOR);

					// Popis hrany bude poděl čáry:
					GraphConstants.setLabelAlongEdge(attributes, Constants.CD_IMP_LABEL_ALONG_EDGE);

					// Styl hrany:
					GraphConstants.setLineStyle(attributes, Constants.CD_IMP_EDGE_LINE_STYLE);
					lineStyle = GraphConstants.getLineStyle(attributes);

					// konec hrany:
					GraphConstants.setLineEnd(attributes, Constants.CD_IMP_EDGE_LINE_END);

					// začátek hrany:
					GraphConstants.setLineBegin(attributes, Constants.CD_IMP_EDGE_LINE_BEGIN);

					// Šířka hrany:
					GraphConstants.setLineWidth(attributes, Constants.CD_IMP_EDGE_LINE_WIDTH);

					// Výplň na konci hrany:
					GraphConstants.setEndFill(attributes, Constants.CD_IMP_EDGE_END_FILL);

					// Výplň na začátku hrany:
					GraphConstants.setBeginFill(attributes, Constants.CD_IMP_EDGE_BEGIN_FILL);

					// Čerchovaná hrana - čára:
					GraphConstants.setDashPattern(attributes, Constants.CD_IMP_PATTERN);

					// Barva písma pro poznámky na hraně:
					GraphConstants.setForeground(attributes, Constants.CD_IMP_EDGE_FONT_COLOR);

					// Font písma pro poznámky - text na hrane:
					GraphConstants.setFont(attributes, Constants.CD_IMP_EDGE_LINE_FONT);
				}

				/*
				 * pokud je hodnout routingu -1, pak je nastavena na normal, tak musím dát typ routingu Default, jinak
				 * se používá jeden z typů - bezier, spline nebo orthogonal, a proto aby tyto typy hrany - čáry fungovaly,
				 * tak je třeba nastavit routing na simple, jinak se změna neprojeví
				 */
				if (lineStyle > -1)
					GraphConstants.setRouting(attributes, GraphConstants.ROUTING_SIMPLE);
				else GraphConstants.setRouting(attributes, GraphConstants.ROUTING_DEFAULT);



				// Aby hrana nešla odpojit ani připojit:
				GraphConstants.setDisconnectable(attributes, false);
				GraphConstants.setConnectable(attributes, false);


				// Přidám hranu do kolekce pro vykreslení.
				cellsList.add(edge);

				updateDiagram(cellsList);
			}
			else JOptionPane.showMessageDialog(this, cellErrorText7, cellErrorTitle7, JOptionPane.ERROR_MESSAGE);
		}
		else JOptionPane.showMessageDialog(this, cellErrorText8, cellErrorTitle8, JOptionPane.ERROR_MESSAGE);
	}







	@Override
	public void addAggregation_1_1ku_1_Edge(final Object srcCell, final Object desCell, final int cardinalitySrc,
			final int cardinalityDes) {
		if (srcCell instanceof DefaultGraphCell && desCell instanceof DefaultGraphCell) {
			if (cellsList.contains(srcCell) && cellsList.contains(desCell)) {
				final DefaultEdge edge = new DefaultEdge();

				edge.setSource(((DefaultGraphCell) srcCell).getChildAt(0));
				edge.setTarget(((DefaultGraphCell) desCell).getChildAt(0));


				int lineStyle = -1;

				// Design:
				final Map<?, ?> attributes = edge.getAttributes();

				if (defaultProperties != null) {
					// Barva hrany:
					final String colorLine = defaultProperties.getProperty("AgrEdgeLineColor", Integer.toString(Constants.CD_AGR_EDGE_COLOR.getRGB()));
					GraphConstants.setLineColor(attributes, new Color(Integer.parseInt(colorLine)));

					// Popis hrany bude poděl čáry:
					GraphConstants.setLabelAlongEdge(attributes, Boolean.parseBoolean(defaultProperties.getProperty("AgrEdgeLabelAlongEdge", String.valueOf(Constants.CD_AGR_LABEL_ALONG_EDGE))));

					// Styl hrany:
					GraphConstants.setLineStyle(attributes, Integer.parseInt(defaultProperties.getProperty("AgrEdgeLineStyle", Integer.toString(Constants.CD_AGR_EDGE_LINE_STYLE))));
                    lineStyle = GraphConstants.getLineStyle(attributes);

					// konec hrany:
					GraphConstants.setLineEnd(attributes, Integer.parseInt(defaultProperties.getProperty("AgrEdgeLineEnd", Integer.toString(Constants.CD_AGR_EDGE_LINE_END))));

					// začátek hrany:
					GraphConstants.setLineBegin(attributes, Integer.parseInt(defaultProperties.getProperty("AgrEdgeLineBegin", Integer.toString(Constants.CD_AGR_EDGE_LINE_BEGIN))));

					// Šířka hrany:
					GraphConstants.setLineWidth(attributes, Float.parseFloat(defaultProperties.getProperty("AgrEdgeLineWidth", String.valueOf(Constants.CD_AGR_EDGE_LINE_WIDTH))));

					// Výplň na konci hrany:
					GraphConstants.setEndFill(attributes, Boolean.parseBoolean(defaultProperties.getProperty("AgrEdgeEndLineFill", String.valueOf(Constants.CD_AGR_EDGE_END_FILL))));

					// Výplň na začátku hrany:
					GraphConstants.setBeginFill(attributes, Boolean.parseBoolean(defaultProperties.getProperty("AgrEdgeBeginLineFill", String.valueOf(Constants.CD_AGR_EDGE_BEGIN_FILL))));

					// Barva písma pro poznámky na hraně:
					final String textColor = defaultProperties.getProperty("AgrEdgeLineTextColor", Integer.toString(Constants.CD_AGR_EDGE_FONT_COLOR.getRGB()));
					GraphConstants.setForeground(attributes, new Color(Integer.parseInt(textColor)));

					// Font písma pro poznámky - text na hrane:
					final int fontSize = Integer.parseInt(defaultProperties.getProperty("AgrEdgeLineFontSize", Integer.toString(Constants.CD_AGR_EDGE_LINE_FONT.getSize())));
					final int fontStyle = Integer.parseInt(defaultProperties.getProperty("AgrEdgeLineFontStyle", Integer.toString(Constants.CD_AGR_EDGE_LINE_FONT.getStyle())));
					GraphConstants.setFont(attributes, new Font(Constants.CD_AGR_EDGE_LINE_FONT.getName(), fontStyle, fontSize));
				}


				else {
					// Barva hrany:
					GraphConstants.setLineColor(attributes, Constants.CD_AGR_EDGE_COLOR);

					// Popis hrany bude poděl čáry:
					GraphConstants.setLabelAlongEdge(attributes, Constants.CD_AGR_LABEL_ALONG_EDGE);

					// Styl hrany:
					GraphConstants.setLineStyle(attributes, Constants.CD_AGR_EDGE_LINE_STYLE);
					lineStyle = GraphConstants.getLineStyle(attributes);

                    // konec hrany:
					GraphConstants.setLineEnd(attributes, Constants.CD_AGR_EDGE_LINE_END);

					// začátek hrany:
                    GraphConstants.setLineBegin(attributes, Constants.CD_AGR_EDGE_LINE_BEGIN);

					// Šířka hrany:
					GraphConstants.setLineWidth(attributes, Constants.CD_AGR_EDGE_LINE_WIDTH);

					// Výplň na konci hrany:
					GraphConstants.setEndFill(attributes, Constants.CD_AGR_EDGE_END_FILL);

					// Výplň na začátku hrany:
					GraphConstants.setBeginFill(attributes, Constants.CD_AGR_EDGE_BEGIN_FILL);

					// Barva písma pro poznámky na hraně:
					GraphConstants.setForeground(attributes, Constants.CD_AGR_EDGE_FONT_COLOR);

					// Font písma pro poznámky - text na hrane:
					GraphConstants.setFont(attributes, Constants.CD_AGR_EDGE_LINE_FONT);
				}



				/*
				 * pokud je hodnout routingu -1, pak je nastavena na normal, tak musím dát typ routingu Default, jinak
				 * se používá jeden z typů - bezier, spline nebo orthogonal, a proto aby tyto typy hrany - čáry fungovaly,
				 * tak je třeba nastavit routing na simple, jinak se změna neprojeví
				 */
				if (lineStyle > -1)
					GraphConstants.setRouting(attributes, GraphConstants.ROUTING_SIMPLE);
				else
					GraphConstants.setRouting(attributes, GraphConstants.ROUTING_DEFAULT);


				/*
				 * Zde je vhodné využít tento typ "routování" v případě, že je nastaveno, že se
				 * má v diagramu tříd zobrazovat vztah asociace jen jako agregace, protože
				 * kdybych toto neudělal, tak by se ty hrany překrývali a nebyl by viděl rozdíl,
				 * v podstatě by v tom diagramu tříd byly správně zobrazeny vztahy, tj. vztah
				 * asociace jako dvě šipky typu agregace, jen by to nebylo vidět, protože by
				 * byly překrytě, tak by to pořád vypadalo jako asociace.
				 */
                if (showAssociationThroughAggregation)
					GraphConstants.setRouting(attributes, ParallelEdgeRouter.getSharedInstance());





				/*
				 * Jelikož nebude nikde nula, tj. když je například vztah pouze z třídy A do
				 * třídy B a ne naopak, tj. pouze třída A má proměnnou typu třídy B, pak bude i
				 * tak kardinalita / (multiplicita) 1 : 1, tj. na obou koncích hrany bude 1.
				 *
				 * Note:
				 * toto řeším až zde při přidání, hrany, tj. když náhodou je jedena z těch
				 * kardinalit nula, pak se nastaví na 1. Nechtěl jsem to řešit ve vlákně, bylo
				 * by to řekl bych trochu nepřehledné testování, navíc je možné, že bych to
				 * musel psát na více místech.
				 */

				// Proměnné pro vložení kardinality:
				final int cardinalitySrcTemp, cardinalityDesTemp;

				/*
				 * V případě, že bude cardinalitySrc nebo cardinalityDes nula, pak se do výše
				 * uvedených proměnných vloží místo nuly jednička, jinak se do těch výše
				 * uvedených proměnných vloží přímo požaované hodnoty.
				 */
				if (cardinalitySrc == 0)
					cardinalitySrcTemp = 1;
				else
					cardinalitySrcTemp = cardinalitySrc;

				if (cardinalityDes == 0)
					cardinalityDesTemp = 1;
				else
					cardinalityDesTemp = cardinalityDes;


				// Jaká se má přidat kardinalita:
				// (Cíl a zdroj)
				final Object[] labels_1_ku_1 = { cardinalityDesTemp, cardinalitySrcTemp };

				final Point2D[] labelPositions = { new Point2D.Double(GraphConstants.PERMILLE * 7 / 8, -20),
						new Point2D.Double(GraphConstants.PERMILLE / 8, -20) };
				GraphConstants.setExtraLabelPositions(attributes, labelPositions);

				GraphConstants.setExtraLabels(attributes, labels_1_ku_1);




				// Aby hrana nešla odpojit ani připojit:
				GraphConstants.setDisconnectable(attributes, false);
				GraphConstants.setConnectable(attributes, false);


				// Přídáni hrany do kolekce pro vykreslení
				cellsList.add(edge);

				updateDiagram(cellsList);
			}
			else JOptionPane.showMessageDialog(this, cellErrorText9, cellErrorTitle9, JOptionPane.ERROR_MESSAGE);
        }
		else JOptionPane.showMessageDialog(this, cellErrorText10, cellErrorTitle10, JOptionPane.ERROR_MESSAGE);
	}










	@Override
	public void addAggregation_1_ku_N_Edge(final Object srcCell, final Object desCell) {
		if (srcCell instanceof DefaultGraphCell && desCell instanceof DefaultGraphCell) {
            if (cellsList.contains(srcCell) && cellsList.contains(desCell)) {
				final DefaultEdge edge = new DefaultEdge();

                edge.setSource(((DefaultGraphCell) srcCell).getChildAt(0));
				edge.setTarget(((DefaultGraphCell) desCell).getChildAt(0));



				int lineStyle = -1;


				// Design:
				final Map<?, ?> attributes = edge.getAttributes();

				if (defaultProperties != null) {
					// Barva hrany:
					final String colorLine = defaultProperties.getProperty("Agr_1_N_EdgeLineColor", Integer.toString(Constants.CD_AGR_N_EDGE_COLOR.getRGB()));
                    GraphConstants.setLineColor(attributes, new Color(Integer.parseInt(colorLine)));

					// Popis hrany bude poděl čáry:
					GraphConstants.setLabelAlongEdge(attributes, Boolean.parseBoolean(defaultProperties.getProperty("Agr_1_N_EdgeLabelAlongEdge", String.valueOf(Constants.CD_AGR_N_LABEL_ALONG_EDGE))));

                    // Styl hrany:
					GraphConstants.setLineStyle(attributes, Integer.parseInt(defaultProperties.getProperty("Agr_1_N_EdgeLineStyle", Integer.toString(Constants.CD_AGR_EDGE_N_LINE_STYLE))));
					lineStyle = GraphConstants.getLineStyle(attributes);

                    // konec hrany:
					GraphConstants.setLineEnd(attributes, Integer.parseInt(defaultProperties.getProperty("Agr_1_N_EdgeLineEnd", Integer.toString(Constants.CD_AGR_N_EDGE_LINE_END))));

					// začátek hrany:
					GraphConstants.setLineBegin(attributes, Integer.parseInt(defaultProperties.getProperty("Agr_1_N_EdgeLineBegin", Integer.toString(Constants.CD_AGR_N_EDGE_LINE_BEGIN))));

					// Šířka hrany:
					GraphConstants.setLineWidth(attributes, Float.parseFloat(defaultProperties.getProperty("Agr_1_N_EdgeLineWidth", String.valueOf(Constants.CD_AGR_EDGE_N_LINE_WIDTH))));

					// Výplň na konci hrany:
                    GraphConstants.setEndFill(attributes, Boolean.parseBoolean(defaultProperties.getProperty("Agr_1_N_EdgeEndLineFill", String.valueOf(Constants.CD_AGR_N_EDGE_END_FILL))));

					// Výplň na začátku hrany:
					GraphConstants.setBeginFill(attributes, Boolean.parseBoolean(defaultProperties.getProperty("Agr_1_N_EdgeBeginLineFill", String.valueOf(Constants.CD_AGR_N_EDGE_BEGIN_FILL))));

                    // Barva písma pro poznámky na hraně:
					final String textColor = defaultProperties.getProperty("Agr_1_N_EdgeLineTextColor", Integer.toString(Constants.CD_AGR_N_EDGE_FONT_COLOR.getRGB()));
					GraphConstants.setForeground(attributes, new Color(Integer.parseInt(textColor)));

					// Font písma pro poznámky - text na hrane:
                    final int fontSize = Integer.parseInt(defaultProperties.getProperty("Agr_1_N_EdgeLineFontSize", Integer.toString(Constants.CD_AGR_EDGE_N_LINE_FONT.getSize())));
					final int fontStyle = Integer.parseInt(defaultProperties.getProperty("Agr_1_N_EdgeLineFontStyle", Integer.toString(Constants.CD_AGR_EDGE_N_LINE_FONT.getStyle())));
					GraphConstants.setFont(attributes, new Font(Constants.CD_AGR_EDGE_N_LINE_FONT.getName(), fontStyle, fontSize));
				}


				else {
                    // Barva hrany:
					GraphConstants.setLineColor(attributes, Constants.CD_AGR_N_EDGE_COLOR);

                    // Popis hrany bude poděl čáry:
					GraphConstants.setLabelAlongEdge(attributes, Constants.CD_AGR_N_LABEL_ALONG_EDGE);

                    // Styl hrany:
					GraphConstants.setLineStyle(attributes, Constants.CD_AGR_EDGE_N_LINE_STYLE);
					lineStyle = GraphConstants.getLineStyle(attributes);

					// konec hrany:
                    GraphConstants.setLineEnd(attributes, Constants.CD_AGR_N_EDGE_LINE_END);

					// začátek hrany:
					GraphConstants.setLineBegin(attributes, Constants.CD_AGR_N_EDGE_LINE_BEGIN);

					// Šířka hrany:
					GraphConstants.setLineWidth(attributes, Constants.CD_AGR_EDGE_N_LINE_WIDTH);

					// Výplň na konci hrany:
					GraphConstants.setEndFill(attributes, Constants.CD_AGR_N_EDGE_END_FILL);

					// Výplň na začátku hrany:
					GraphConstants.setBeginFill(attributes, Constants.CD_AGR_N_EDGE_BEGIN_FILL);

					// Barva písma pro poznámky na hraně:
					GraphConstants.setForeground(attributes, Constants.CD_AGR_N_EDGE_FONT_COLOR);

                    // Font písma pro poznámky - text na hrane:
					GraphConstants.setFont(attributes, Constants.CD_AGR_EDGE_N_LINE_FONT);
				}



				/*
				 * pokud je hodnout routingu -1, pak je nastavena na normal, tak musím dát typ routingu Default, jinak
				 * se používá jeden z typů - bezier, spline nebo orthogonal, a proto aby tyto typy hrany - čáry fungovaly,
				 * tak je třeba nastavit routing na simple, jinak se změna neprojeví
				 */
				if (lineStyle > -1)
					GraphConstants.setRouting(attributes, GraphConstants.ROUTING_SIMPLE);

                else GraphConstants.setRouting(attributes, GraphConstants.ROUTING_DEFAULT);



				// Zda se má zapsat multiplicita 1 : 1 nebo 1 : N:
				// (Cíl a zdroj)
				final Object[] labels_1_ku_N = {"N", "1"};

				final Point2D[] labelPocitions = {new Point2D.Double(GraphConstants.PERMILLE * 7 / 8, -20), new Point2D.Double(GraphConstants.PERMILLE / 8, -20)};
                GraphConstants.setExtraLabelPositions(attributes, labelPocitions);

				GraphConstants.setExtraLabels(attributes, labels_1_ku_N);


				// Aby hrana nešla odpojit ani připojit:
                GraphConstants.setDisconnectable(attributes, false);
				GraphConstants.setConnectable(attributes, false);


				// Přídáni hrany do kolekce pro vykreslení
				cellsList.add(edge);

				updateDiagram(cellsList);
			}
            else JOptionPane.showMessageDialog(this, cellErrorText9, cellErrorTitle9, JOptionPane.ERROR_MESSAGE);
		}
		else JOptionPane.showMessageDialog(this, cellErrorText10, cellErrorTitle10, JOptionPane.ERROR_MESSAGE);
	}








	/**
	 * Metoda, která si ze souboru načte nastavení diagramu - fonty, barvy, písmo,
	 * ... Pokud soubor ve Workspace nebude nalazen, použije se výchozí nastavení z
	 * adresáře aplikace
	 */
	private void setClassDiagramProperties() {
		defaultProperties = App.READ_FILE.getClassDiagramProperties();

		/*
		 * Zde je potřeba znovu přenačíst objekt Properties v objektu kindOfEdge,
		 * protože je to statický objekt, takžeje možné, že pokud se při spuštění
		 * aplikace nacházeli například nějaké duplicity v příslušném konfiguračním
		 * souboru, pak může obsahovat chybně načtené hodnoty, proto jej přenačtu, aby
		 * obsahoval ty správné.
		 */
		KIND_OF_EDGE.reloadProperties();




		if (defaultProperties != null) {
			// Proměnná pro velikost písma buňky:
			classFontSize = Integer.parseInt(defaultProperties.getProperty("ClassFontSize", Integer.toString(Constants.CD_FONT_CLASS.getSize())));

			// Odpojitelnost popisku hrany:
			setEdgeLabelsMovable(Boolean.parseBoolean(defaultProperties.getProperty("EdgeLabelMovable", String.valueOf(Constants.CD_COM_LABELS_MOVABLE))));

			// Barva pozadí:
			final String colorText = defaultProperties.getProperty("BackgroundColor", Integer.toString(Constants.CD_BACKGROUND_COLOR.getRGB()));
			setBackground(new Color(Integer.parseInt(colorText)));


			// Přiblížení:
			setScale(Double.parseDouble(defaultProperties.getProperty("ClassDiagramScale", String.valueOf(Constants.CLASS_DIAGRAM_SCALE))));


			// Nastavení, zda se mají zobrazovat vztahy třídy na sebe sama:
			showRelationShipsToItself = Boolean.parseBoolean(defaultProperties.getProperty("ShowRelationsShipsToClassItself", String.valueOf(Constants.CD_SHOW_RELATION_SHIPS_TO_CLASS_ITSELF)));


            // Zda se mají zobrazovat názvy tříd s balíčky nebo bez nich (v položce "Zděděné statické metody" v kontextovém menu nad třídou)
			showInheritedClassNameWithPackages = Boolean.parseBoolean(defaultProperties.getProperty("ShowStaticInheritedClassNameWithPackages", String.valueOf(Constants.CD_SHOW_STATIC_INHERITED_CLASS_NAME_WITH_PACKAGES)));


			// Zda se mají v kontextovém menu zobrazovat i soukromé konstruktory:
            includePrivateConstructors = Boolean.parseBoolean(defaultProperties.getProperty("IncludePrivateConstructors", String.valueOf(Constants.CD_INCLUDE_PRIVATE_CONSTRUCTORS)));

			// Zda se mají v kontextovém menu zobrazovat i chráněné konstruktory:
            includeProtectedConstructors = Boolean.parseBoolean(defaultProperties.getProperty("IncludeProtectedConstructors", String.valueOf(Constants.CD_INCLUDE_PROTECTED_CONSTRUCTORS)));

            // Zda se mají v kontextovém menu zobrazovat i package-private konstruktory:
			includePackagePrivateConstructors = Boolean.parseBoolean(defaultProperties.getProperty("IncludePackagePrivateConstructors", String.valueOf(Constants.CD_INCLUDE_PACKAGE_PRIVATE_CONSTRUCTORS)));


			// Zda se mají v kontextovém menu zobrazovat i privátní metody:
			includePrivateMethods = Boolean.parseBoolean(defaultProperties.getProperty("IncludePrivateMethods", String.valueOf(Constants.CD_INCLUDE_PRIVATE_METHODS)));

			// Zda se mají v kontextovém menu zobrazovat i chráněné metody:
			includeProtectedMethods = Boolean.parseBoolean(defaultProperties.getProperty("IncludeProtectedMethods", String.valueOf(Constants.CD_INCLUDE_PROTECTED_METHODS)));

			// Zda se mají v kontextovém menu zobrazovat i package-private metody:
			includePackagePrivateMethods = Boolean.parseBoolean(defaultProperties.getProperty("IncludePackagePrivateMethods", String.valueOf(Constants.CD_INCLUDE_PACKAGE_PRIVATE_METHODS)));


			// Zda se mají v kontextovém menu v "Vnitřní statické třídy" zobrazovat i privátní třídy:
            includePrivateInnerStaticClasses = Boolean.parseBoolean(defaultProperties.getProperty("IncludePrivateInnerStaticClasses", String.valueOf(Constants.CD_INCLUDE_PRIVATE_INNER_STATIC_CLASSES)));

			// Zda se mají v kontextovém menu v "Vnitřní statické třídy" zobrazovat i chráněné třídy:
            includeProtectedInnerStaticClasses = Boolean.parseBoolean(defaultProperties.getProperty("IncludeProtectedInnerStaticClasses", String.valueOf(Constants.CD_INCLUDE_PROTECTED_INNER_STATIC_CLASSES)));

			// Zda se mají v kontextovém menu v "Vnitřní statické třídy" zobrazovat i package-private třídy:
			includePackagePrivateInnerStaticClasses = Boolean.parseBoolean(defaultProperties.getProperty("IncludePackagePrivateInnerStaticClasses", String.valueOf(Constants.CD_INCLUDE_PACKAGE_PRIVATE_INNER_STATIC_CLASSES)));


			// Zda se mají v kontextovém menu v nabídce "Vnitřní statické třídy" u jednotlivých interních tříd zobrazovat i jejich statické soukromé metody:
			innerStaticClassesIncludePrivateStaticMethods = Boolean.parseBoolean(defaultProperties.getProperty("InnerStaticClassesIncludePrivateStaticMethods", String.valueOf(Constants.CD_INNER_STATIC_CLASSES_INCLUDE_PRIVATE_STATIC_METHODS)));

			// Zda se mají v kontextovém menu v nabídce "Vnitřní statické třídy" u jednotlivých interních tříd zobrazovat i jejich statické chráněné metody:
			innerStaticClassesIncludeProtectedStaticMethods = Boolean.parseBoolean(defaultProperties.getProperty("InnerStaticClassesIncludeProtectedStaticMethods", String.valueOf(Constants.CD_INNER_STATIC_CLASSES_INCLUDE_PROTECTED_STATIC_METHODS)));

			// Zda se mají v kontextovém menu v nabídce "Vnitřní statické třídy" u jednotlivých interních tříd zobrazovat i jejich statické package-private metody:
			innerStaticClassesIncludePackagePrivateStaticMethods = Boolean.parseBoolean(defaultProperties.getProperty("InnerStaticClassesIncludePackagePrivateStaticMethods", String.valueOf(Constants.CD_INNER_STATIC_CLASSES_INCLUDE_PACKAGE_PRIVATE_STATIC_METHODS)));



			// Zda se mají zpřístupnit proměnné pro předání do konstruktoru:
			makeVariablesAvailable = Boolean.parseBoolean(defaultProperties.getProperty("MakeFieldsAvailableForConstructor", String.valueOf(Constants.CD_MAKE_FIELDS_AVAILABLE_FOR_CALL_CONSTRUCTOR)));


			// Zda se mají zpřístupnít proměnné pro předání do parametru metody:
			makeVariablesAvailableForCallMethod = Boolean.parseBoolean(defaultProperties.getProperty("MakeFieldsAvailableForMethod", String.valueOf(Constants.CD_MAKE_FIELDS_AVAILABLE_FOR_CALL_METHOD)));


			// Zda se má zobrazit možnost pro vytvoření nové instance v parametru zavolané metody, pokud se jedná o parametr typu třídy z diagramu tříd:
			showCreateNewInstanceOptionInCallMethod = Boolean.parseBoolean(defaultProperties.getProperty("ShowCreateNewInstanceOptionInCallMethod", String.valueOf(Constants.ID_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD)));


			// Zda se má při zavolání konstruktoru zpřístupnit možnost pro vytvoření nové instaci v případě, že je v parametru konstruktoru typ třídy z diagramu tříd:
			showCreateNewInstanceOptionInCallConstructor = Boolean.parseBoolean(defaultProperties.getProperty("ShowCreateNewInstanceOptionInCallConstructor", String.valueOf(Constants.CD_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_CONSTRUCTOR)));


			// Zda se má vztah typu asociace znázornit jako jedna hrana s šipkami na obou koncích nebo pomocí dvou agregací:
			showAssociationThroughAggregation = Boolean.parseBoolean(defaultProperties.getProperty("ShowAssociationThroughAggregation", String.valueOf(Constants.CD_SHOW_ASSOCIATION_THROUGH_AGGREGATION)));

			/*
			 * Získání "textové" hodnoty, která značí typ editoru kódu pro otevření třídy.
			 */
			final String kindOfCodeEditorTemp = defaultProperties.getProperty("DefaultSourceCodeEditor", Constants.CD_KIND_OF_DEFAULT_CODE_EDITOR.getValue());
			// Typ editoru kódu, ve kterém se mají primárně otevírat třídy v diagramu tříd:
			kindOfCodeEditor = getKindOfEditorByTextValue(kindOfCodeEditorTemp);


			// Zda se mají v dialogu pro zavolání statické metody nad třídou zobrazit metody pro předání jejich návratového hodnoty do parametru metody:
			makeMethodsAvailableForCallMethod = Boolean.parseBoolean(defaultProperties.getProperty("MakeMethodsAvailableForCallMethod", String.valueOf(Constants.CD_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD)));

			// Zda se mají v dialogu pro zavolání konstruktoru zobrazit metody pro předání jejich návratové hodnoty do parametru konstruktoru:
			makeMethodsAvailableForCallConstructor = Boolean.parseBoolean(defaultProperties.getProperty("MakeMethodsAvailableForCallConstructor", String.valueOf(Constants.CD_MAKE_METHODS_AVAILABLE_FOR_CALL_CONSTRUCTOR)));

			// Zda se mají při implementaci rozhraní i vygenerovat příslušné metody:
			addInterfaceMethods = Boolean.parseBoolean(defaultProperties.getProperty("AddInterfaceMethodsToClass", String.valueOf(Constants.ADD_INTERFACE_METHODS_TO_CLASS)));
		}




		else {
            // Proměnná pro velikost písma buňky:
			classFontSize = Constants.CD_FONT_CLASS.getSize();

			setBackground(Constants.CD_BACKGROUND_COLOR);

            setEdgeLabelsMovable(Constants.CD_COM_LABELS_MOVABLE);

			setScale(Constants.CLASS_DIAGRAM_SCALE);

			// Nastavení, zda se mají zobrazovat vztahy třídy na sebe sama:
			showRelationShipsToItself = Constants.CD_SHOW_RELATION_SHIPS_TO_CLASS_ITSELF;

			// Zda se mají zobrazovat názvy tříd s balíčky nebo bez nich (v položce "Zděděné statické metody" v kontextovém menu nad třídou)
			showInheritedClassNameWithPackages = Constants.CD_SHOW_STATIC_INHERITED_CLASS_NAME_WITH_PACKAGES;

			// Zda se mají v kontextovém menu zobrazovat i soukromé konstruktory:
			includePrivateConstructors = Constants.CD_INCLUDE_PRIVATE_CONSTRUCTORS;

			// Zda se mají v kontextovém menu zobrazovat i chráněné konstruktory:
			includeProtectedConstructors = Constants.CD_INCLUDE_PROTECTED_CONSTRUCTORS;

			// Zda se mají v kontextovém menu zobrazovat i package-private konstruktory:
			includePackagePrivateConstructors = Constants.CD_INCLUDE_PACKAGE_PRIVATE_CONSTRUCTORS;

			// Zda se mají v kontextovém menu zobrazovat i privátní metody:
			includePrivateMethods = Constants.CD_INCLUDE_PRIVATE_METHODS;

			// Zda se mají v kontextovém menu zobrazovat i chráněné metody:
			includeProtectedMethods = Constants.CD_INCLUDE_PROTECTED_METHODS;

			// Zda se mají v kontextovém menu zobrazovat i package-private metody:
			includePackagePrivateMethods = Constants.CD_INCLUDE_PACKAGE_PRIVATE_METHODS;

			// Zda se mají v kontextovém menu v "Vnitřní statické třídy" zobrazovat i privátní třídy:
			includePrivateInnerStaticClasses = Constants.CD_INCLUDE_PRIVATE_INNER_STATIC_CLASSES;

			// Zda se mají v kontextovém menu v "Vnitřní statické třídy" zobrazovat i chráněné třídy:
			includeProtectedInnerStaticClasses = Constants.CD_INCLUDE_PROTECTED_INNER_STATIC_CLASSES;

			// Zda se mají v kontextovém menu v "Vnitřní statické třídy" zobrazovat i pacakage-private třídy:
            includePackagePrivateInnerStaticClasses = Constants.CD_INCLUDE_PACKAGE_PRIVATE_INNER_STATIC_CLASSES;

            // Zda se mají v kontextovém menu v nabídce "Vnitřní statické třídy" u jednotlivých interních tříd zobrazovat i jejich statické soukromé metody:
			innerStaticClassesIncludePrivateStaticMethods = Constants.CD_INNER_STATIC_CLASSES_INCLUDE_PRIVATE_STATIC_METHODS;

            // Zda se mají v kontextovém menu v nabídce "Vnitřní statické třídy" u jednotlivých interních tříd zobrazovat i jejich statické chráněné metody:
			innerStaticClassesIncludeProtectedStaticMethods = Constants.CD_INNER_STATIC_CLASSES_INCLUDE_PROTECTED_STATIC_METHODS;

			// Zda se mají v kontextovém menu v nabídce "Vnitřní statické třídy" u jednotlivých interních tříd zobrazovat i jejich statické package-private metody:
			innerStaticClassesIncludePackagePrivateStaticMethods = Constants.CD_INNER_STATIC_CLASSES_INCLUDE_PACKAGE_PRIVATE_STATIC_METHODS;

            // Zda se mají zpřístupnit proměnné pro předání do konstruktoru:
			makeVariablesAvailable = Constants.CD_MAKE_FIELDS_AVAILABLE_FOR_CALL_CONSTRUCTOR;

            // Zda se mají zpřístupnít proměnné pro předání do parametru metody:
			makeVariablesAvailableForCallMethod = Constants.CD_MAKE_FIELDS_AVAILABLE_FOR_CALL_METHOD;

            // Zda se má zobrazit možnost pro vytvoření nové instance v parametru zavolané metody, pokud se jedná o parametr typu třídy z diagramu tříd:
			showCreateNewInstanceOptionInCallMethod = Constants.CD_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD;

			// Zda se má při zavolání konstruktoru zpřístupnit možnost pro vytvoření nové instaci v případě, že je v parametru konstruktoru typ třídy z diagramu tříd:
			showCreateNewInstanceOptionInCallConstructor = Constants.CD_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_CONSTRUCTOR;

			// Zda se má vztah typu asociace znázornit jako jedna hrana s šipkami na obou koncích nebo pomocí dvou agregací:
			showAssociationThroughAggregation = Constants.CD_SHOW_ASSOCIATION_THROUGH_AGGREGATION;

			// Typ editoru kódu, ve kterém se mají primárně otevírat třídy v diagramu tříd:
			kindOfCodeEditor = Constants.CD_KIND_OF_DEFAULT_CODE_EDITOR;

			// Zda se mají v dialogu pro zavolání statické metody nad třídou zobrazit metody
			// pro předání jejich návratového hodnoty do parametru metody:
			makeMethodsAvailableForCallMethod = Constants.CD_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD;

			// Zda se mají v dialogu pro zavolání konstruktoru zobrazit metody pro předání
			// jejich návratové hodnoty do parametru konstruktoru:
			makeMethodsAvailableForCallConstructor = Constants.CD_MAKE_METHODS_AVAILABLE_FOR_CALL_CONSTRUCTOR;

			// Zda se mají při implementaci rozhraní i vygenerovat příslušné metody:
			addInterfaceMethods = Constants.ADD_INTERFACE_METHODS_TO_CLASS;
		}

		updateDiagram(cellsList);
	}











	/**
	 * Metoda, která ze zadané "textové" proměnné, která značí "identifikátor"
	 * příslušného editoru zdrojového kódu vrátí výčtovou hodnotu, která značí
	 * editor zdrojového kódu, ve kterém se mají primárně otevírat třídy.
	 *
	 * @param value
	 *            - hodnota načtená z příslušného souboru ClassDiagram.properties,
	 *            která značí index příslušného editoru kódu.
	 *
	 * @return výčtovou hodnotu, která značí konkrétní editor zdrojového kódu, který
	 *         se má primárně využít pro otevření tříd v diagramu tříd.
     */
	public static KindOfCodeEditor getKindOfEditorByTextValue(final String value) {
        if (value.equalsIgnoreCase(KindOfCodeEditor.SOURCE_CODE_EDITOR.getValue()))
			return KindOfCodeEditor.SOURCE_CODE_EDITOR;

		else if (value.equalsIgnoreCase(KindOfCodeEditor.PROJECT_EXPLORER.getValue()))
			return KindOfCodeEditor.PROJECT_EXPLORER;

		return Constants.CD_KIND_OF_DEFAULT_CODE_EDITOR;
	}















	@Override
	public void setLanguage(Properties properties) {
		FontSizePanelAbstract.setLanguageForErrorText(properties);

		// Zde bych to mohl instaciovat v konstruktoru, ale je třeba předat texty, proto
		// je to zde.
		checkDuplicatesInDiagrams = new CheckDuplicatesInDiagrams(properties);


		if (properties != null) {
			languageProperties = properties;

			// První komentář pro buňku reprezentující komentář, že se má přidatkomentáře:
			txtInsertComment = properties.getProperty("FirstTextToCommentCell", Constants.CD_FIRST_TEXT_TO_COMMENT_CELL) + " ...";

			// Texty do chybové hlášky pro otevření kodu třídy v editoru kodu - v metodě openClassInCodeEditor:
			txtMissingClassText = properties.getProperty("Ppm_MissingClassText", Constants.PPM_TXT_MISING_CLASS_TEXT);
			txtMissingClassTitle = properties.getProperty("Ppm_MissingClassTitle", Constants.PPM_TXT_MISISNG_CLASS_TITLE);
			txtPath = properties.getProperty("Ppm_Path", Constants.PPM_TXT_PATH);


            cellErrorText1 = properties.getProperty("CellErrorTitle", Constants.CD_CELL_ERROR_TEXT1);
			cellErrorTitle1 = properties.getProperty("CellErrorText", Constants.CD_CELL_ERROR_TITLE1);

			cellErrorTitle2 = properties.getProperty("CellErrorTitle2", Constants.CD_CELL_ERROR_TITLE2);
			cellErrorText2 = properties.getProperty("CellErrorText2", Constants.CD_CELL_ERROR_TEXT2);

			cellErrorTitle3 = properties.getProperty("CellErrorTitle3", Constants.CD_CELL_ERROR_TITLE3);
			cellErrorText3 = properties.getProperty("CellErrorText3", Constants.CD_CELL_ERROR_TEXT3);

			cellErrorTitle4 = properties.getProperty("CellErrorTitle4", Constants.CD_CELL_ERROR_TITLE4);
			cellErrorText4 = properties.getProperty("CellErrorText4", Constants.CD_CELL_ERROR_TEXT4);

			cellErrorTitle5 = properties.getProperty("CellErrorTitle5", Constants.CD_CELL_ERROR_TITLE5);
			cellErrorText5 = properties.getProperty("CellErrorText5", Constants.CD_CELL_ERROR_TEXT5);

			cellErrorTitle6 = properties.getProperty("CellErrorTitle6", Constants.CD_CELL_ERROR_TITLE6);
			cellErrorText6 = properties.getProperty("CellErrorText6", Constants.CD_CELL_ERROR_TEXT6);

			cellErrorTitle7 = properties.getProperty("CellErrorTitle7", Constants.CD_CELL_ERROR_TITLE7);
			cellErrorText7 = properties.getProperty("CellErrorText7", Constants.CD_CELL_ERROR_TEXT7);

			cellErrorTitle8 = properties.getProperty("CellErrorTitle8", Constants.CD_CELL_ERROR_TITLE8);
			cellErrorText8 = properties.getProperty("CellErrorText8", Constants.CD_CELL_ERROR_TEXT8);

			cellErrorTitle9 = properties.getProperty("CellErrorTitle9", Constants.CD_CELL_ERROR_TITLE9);
			cellErrorText9 = properties.getProperty("CellErrorText9", Constants.CD_CELL_ERROR_TEXT9);

			cellErrorTitle10 = properties.getProperty("CellErrorTitle10", Constants.CD_CELL_ERROR_TITLE10);
			cellErrorText10 = properties.getProperty("CellErrorText10", Constants.CD_CELL_ERROR_TEXT10);

			txtEdgeErrorTitle = properties.getProperty("Cd_EdgeErrorInAssociateWithOneClassTitle", Constants.CD_EDGE_ERROR_TITLE);
			txtEdgeErrorText = properties.getProperty("Cd_EdgeErrorInAssociateWithOneClassText", Constants.CD_EDGE_ERROR_TEXT);

			missingObjectInGraphTitle = properties.getProperty("Cd_ErrorMissingObjectInGraphTitle", Constants.CD_MISSING_OBJECT_IN_GRAPH_TITLE);
			missingObjectInGraphText = properties.getProperty("Cd_ErrorMissingObjectInGraphText", Constants.CD_MISSING_OBJECT_IN_GRAPH_TEXT);

			txtErrorWhileWritingInterfaceMethodsToClassText = properties.getProperty("Cd_Txt_ErrorWhileWritingInterfaceMethodsToClass_Text", Constants.CD_TXT_ERROR_WHILE_WRITING_INTERFACE_METHODS_TO_CLASS_TEXT);
			txtErrorWhileWritingInterfaceMethodsToClassTT = properties.getProperty("Cd_Txt_ErrorWhileWritingInterfaceMethodsToClass_TT", Constants.CD_TXT_ERROR_WHILE_WRITING_INTERFACE_METHODS_TO_CLASS_TT);
			txtClass = properties.getProperty("Cd_Txt_Class", Constants.CD_TXT_CLASS);
			txtAlreadyImplementesInterface = properties.getProperty("Cd_Txt_AlreadyImplementsInterface", Constants.CD_TXT_ALREADY_IMPLEMENTS_INTERFACE);
			txtAlreadyImplementesInterfaceTitle = properties.getProperty("Cd_Txt_AlreadyImplementsInterface_TT", Constants.CD_TXT_ALREADY_IMPLEMENTS_INTERFACE_TITLE);
		}

		else {
			txtInsertComment = Constants.CD_FIRST_TEXT_TO_COMMENT_CELL + " ...";

			txtMissingClassText = Constants.PPM_TXT_MISING_CLASS_TEXT;
			txtMissingClassTitle = Constants.PPM_TXT_MISISNG_CLASS_TITLE;
			txtPath = Constants.PPM_TXT_PATH;



			cellErrorText1 = Constants.CD_CELL_ERROR_TEXT1;
			cellErrorTitle1 = Constants.CD_CELL_ERROR_TITLE1;

			cellErrorTitle2 = Constants.CD_CELL_ERROR_TITLE2;
			cellErrorText2 = Constants.CD_CELL_ERROR_TEXT2;

			cellErrorTitle3 = Constants.CD_CELL_ERROR_TITLE3;
			cellErrorText3 = Constants.CD_CELL_ERROR_TEXT3;

			cellErrorTitle4 = Constants.CD_CELL_ERROR_TITLE4;
			cellErrorText4 = Constants.CD_CELL_ERROR_TEXT4;

			cellErrorTitle5 = Constants.CD_CELL_ERROR_TITLE5;
			cellErrorText5 = Constants.CD_CELL_ERROR_TEXT5;

			cellErrorTitle6 = Constants.CD_CELL_ERROR_TITLE6;
			cellErrorText6 = Constants.CD_CELL_ERROR_TEXT6;

			cellErrorTitle7 = Constants.CD_CELL_ERROR_TITLE7;
			cellErrorText7 = Constants.CD_CELL_ERROR_TEXT7;

			cellErrorTitle8 = Constants.CD_CELL_ERROR_TITLE8;
			cellErrorText8 = Constants.CD_CELL_ERROR_TEXT8;

			cellErrorTitle9 = Constants.CD_CELL_ERROR_TITLE9;
			cellErrorText9 = Constants.CD_CELL_ERROR_TEXT9;

			cellErrorTitle10 = Constants.CD_CELL_ERROR_TITLE10;
			cellErrorText10 = Constants.CD_CELL_ERROR_TEXT10;

			txtEdgeErrorTitle = Constants.CD_EDGE_ERROR_TITLE;
			txtEdgeErrorText = Constants.CD_EDGE_ERROR_TEXT;

			missingObjectInGraphTitle = Constants.CD_MISSING_OBJECT_IN_GRAPH_TITLE;
			missingObjectInGraphText = Constants.CD_MISSING_OBJECT_IN_GRAPH_TEXT;

			txtErrorWhileWritingInterfaceMethodsToClassText =Constants.CD_TXT_ERROR_WHILE_WRITING_INTERFACE_METHODS_TO_CLASS_TEXT;
			txtErrorWhileWritingInterfaceMethodsToClassTT = Constants.CD_TXT_ERROR_WHILE_WRITING_INTERFACE_METHODS_TO_CLASS_TT;
			txtClass = Constants.CD_TXT_CLASS;
			txtAlreadyImplementesInterface = Constants.CD_TXT_ALREADY_IMPLEMENTS_INTERFACE;
			txtAlreadyImplementesInterfaceTitle = Constants.CD_TXT_ALREADY_IMPLEMENTS_INTERFACE_TITLE;
		}
	}










    /**
	 * Obyčejný setr, která nastaví proměnnou edgeType, dle které poznám, zda se má
	 * vytvořit hrane nebo ne, záleží pak na jějí hodnotuě, zda bude null - hrana
	 * byla vytvořena nebo se nenmá vytvořit a nebo dle hodnoty výčtu se má vytvořit
     * příslušná hrana
	 *
	 * @param edgeType
	 *            - výčetová hodnota, která určí, jaký typ hodnoty se má vytvořit
	 */
	public static void setEdgeType(final EdgeTypeEnum edgeType) {
		GraphClass.edgeType = edgeType;
	}
















	/**
	 * Metoda, která vymaže z tohoto diagramu tříd všedhny hrany, které mají jako zdroj nebo cíl
	 * buňku v parametru metody.
	 *
	 * @param cell = buňka, u které se testuje, zda je zroj nebo cíl hrany, pokud tomu tak je, tak se
	 * příslušná hrana smaže  a pokud se jedná o komentář, tak ten se smaže taky
	 */
	public final void deleteAllEdgesByCell(final DefaultGraphCell cell) {
		// Postup:
		// proiteruji všechny objekty v diagramu tříd,
		// otestuji, zda je to hrana, pokud ano, tak si zjistím její zdroj a cíl
		// a provnám ho s buňku v parametru metody, pokud je buňka v parametru metody
        // zdroj nebo cíl hrany tak otestuji, zda se jedná o hrany komentáře, pokud ano,
		// tak se smaže i ten komentář, jinak jen ta hrana


		for (final Iterator<DefaultGraphCell> it = cellsList.iterator(); it.hasNext();) {
			final DefaultGraphCell c = it.next();

			if (c instanceof DefaultEdge) {
				// Zjistím si zdroj a cíl hrany:
				final DefaultGraphCell srcCell = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) c).getSource()).getParent();
				final DefaultGraphCell desCell = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) c).getTarget()).getParent();



				// Nyní mám zdroj a cál hrany, tak otestuji, zda je jednoa z těch buňek označená
				// buňka v parametru metody, protože pokud ano, tak se ta hrana má smazat:
				if (srcCell == cell || desCell == cell) {
					// Zde se našel zdroj nebo cíl hrany - nějaká buňka, která je tou co se má
					// smazat,
					// zakže s tou buňkou, co se má smazat je k ní připojen nějaká hrana, tak ji
					// přidám do kolekce
					// pro smazání:

					/*
					 * Nejprve odeberu příslušnou hranu z kolekce se všemi objekty, protože, kdybych
					 * to udělal až po následující metodě deleteObjectFromModelOfGraph, tak by se
					 * stejně do modulu ta hrana přidala, protože by v tu chvíli ještě v kolekci
					 * byla, ale pak se by se smazala z kolekce, takže by byla zobrazena v diagramu,
					 * ale už by nikde neexistovala, a při přenačtění diagramu by se to opravilo.
					 */
					it.remove();

					deleteObjectFromModelOfGraph(c, cellsList);
				}
			}
		}
	}














	/**
     * Metoda, která vrátí hranu, jejíž zdroj je buňka předaná v parametru metody,
	 * ta buňka je komentář Metoda vrátí null, pokud nebude hrana nalezena, pak
	 * došlo buď k nějaké chybě - zde mně žádná chyba nenapada, k jaké by mohlo
     * dojít nebo hrana v grafu, rasp. kolekci objektů v grafu neexistuje, což by
	 * nastat nemělo
	 *
	 * Postup:
	 * Potřebuji prohledat celou kolekci objektů v grafu a zjišťovat, jestli je
	 * nějaký objekt hrana Pokud ano, tak jestli je její zdroj buňka v parametru
	 *
	 * @param commentCell
	 *            - Buńka v grafu,
	 * @return null, pokud nebude hrana nalezena, jinak vrátí nalezenou hranu k
	 *         buňce
	 */
	public static DefaultEdge getEdgeOfTheCommentCell(final DefaultGraphCell commentCell) {
		for (DefaultGraphCell d : cellsList) {
			if (d instanceof DefaultEdge) {
				// Zde se jedná o hranu, tak si zjistím její zdroj:
				final DefaultGraphCell srcCell = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) d).getSource())
						.getParent();
				// Nyní otestuji zdroj té hrany, zda se jedná o komentář nebo ne, případně ho
				// vrátím
				if (srcCell == commentCell)
					return (DefaultEdge) d;
			}
		}
		return null;
	}






	/**
	 * Klasický getr, který vrátí veškeré objekty z diagramu tříd
	 *
	 * @return objekty z diagramu tříd v podobně kolekce - Listu typu
	 *         DefaultGraphCell coby obejkty v grafu - diagramu
	 */
	public static List<DefaultGraphCell> getCellsList() {
		return cellsList;
	}







	/**
	 * Metoda, která nastaví diagram tříd dle parametru
	 *
	 * @param cellsList
	 *            hodnoty pro diagram tříd, resp. bunky coby třídy
	 */
	public final void setCellsList(final List<DefaultGraphCell> cellsList) {
        // Vymžu to, co je v grafu teď:
		getModel().remove(GraphClass.cellsList.toArray());

        // Vložím nové objekty:
		GraphClass.cellsList = cellsList;

		// Aktualizuji graf:
		getGraphLayoutCache().insert(cellsList.toArray());
	}














	/**
	 * Metoda, která prohledá kolekci objektů v grafu a zjistí, zda je třída - buňka
	 * v parametru spojena - je ve vztahu s nějakou jinou třídou v grafu, pokud ano,
     * vrátí true, pokud není vrátí false
	 *
	 * @param cell
	 *            - třída - buňka v grafu u které chci zjistit, zda se nachází ve
	 *            vztahu s jinou třídou - buňkou v grafu
	 *
	 * @return true pokud ano, jinak false
     */
	static boolean isClassInRelationship(final DefaultGraphCell cell) {
		final String pathToSrc = App.READ_FILE.getPathToSrc();

		for (DefaultGraphCell cells : cellsList) {
			if (cells instanceof DefaultEdge) {
				// Zjístím si začátek a konec hrany:
				final DefaultGraphCell srcCell = ((DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) cells).getSource()).getParent());
				final DefaultGraphCell desCell = ((DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) cells).getTarget()).getParent());


				// Pokud označená třída - buňka bude zdroj hrany, pak mohu vrátit true a skončit tím cyklus - třída je ve vztahu
				if (cell == srcCell)
					return true;

				else if (cell == desCell) {
					// Zde je označená třída cílem aktuálně testované hrany v grafu
					// Musím otestovat, zda její zdroj existuje v adresáři projektu v src, pokud ne
					// jedná se o komentář - což nevadí
					// nebo uživatel smazal třídu v adresáři projektu a v grafu zůstala její
					// reprezentace
					if (ReadFile.existsFile(
							pathToSrc + File.separator + srcCell.toString().replace(".", File.separator) + ".java"))
						// Pokud je podmínka splněna a třída na konci hrany existuje, pak je označená
						// třída ve vztahu - vrátím true
						return true;
				}
			}
		}
		return false;
	}








	/**
	 * Metoda, která vymaže komentář k dané buňce - třídě
	 *
	 * @param cell
	 *            - buňka - třída u které se má smazat komentář
	 */
	final void deleteCommentClass(final DefaultGraphCell cell) {
		final List<DefaultGraphCell> listForDelete = new ArrayList<>();

		final String pathToSrc = App.READ_FILE.getPathToSrc();

		for (DefaultGraphCell c : cellsList) {
			if (c instanceof DefaultEdge) {
				final DefaultGraphCell srcCell = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) c).getSource()).getParent();
				final DefaultGraphCell desCell = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) c).getTarget()).getParent();

				if (cell == desCell) {
					// Zde je označená buňka - třída v parametru cíl této testované hrany

					// Otestuji, zda její zdroj neexistuje - je to komentář, pokud ano, tak ho smažu

					if (pathToSrc != null) {
						// Mohlo dojít například ke změně stylů, pak by se hrana nebo buňka s původním
						// designem nemusela
						// rozpoznat, pokud byla změněna, tak si musím zjistit, zda se text v dané buňce
						// není náhodou třída,
						// která existuje někde v adresáři src, pokud ne, jedna se o nějaký text... - je
						// to tedy komentář
						if (KIND_OF_EDGE.isCommentCell(desCell) || !ReadFile.existsFile(pathToSrc + File.separator
								+ srcCell.toString().replace(".", File.separator) + ".java")) {
							// Zde zdroj neexistuje, tak se jedná o komentář nebo omylem smazanou třídu v
							// src v projektu,
							// mohu hranu i cíl smazat:

							// Objekty pro smazání dám do nové kolekce, kterou následně smažu:
							listForDelete.add(c);
							listForDelete.add(srcCell);
						}
					}
				}
			}
		}


		listForDelete.forEach(c -> deleteObjectFromGraph(c, cellsList));

		updateDiagram(cellsList);
	}


















	/**
	 * Metoda, která v editoru kódu zobrazí zdrojový kód označené třídy v diagramu
	 * tříd.
	 *
	 * Bud se daná označená třída zkompiluje a předá do dialogu nebo se předá v
	 * parametru metody (pokud není proměnná choosedClass null) - tj. předá se z
	 * popupMenu, kde už bude zkompilovaná
	 *
	 * @param selectedCell
	 *            - označená třída z diagramu tříd
	 *
	 * @param choosedClass
	 *            - zkompilovaná tříd z popupMenu, nebo null, pokud se kód má načíst
	 *            drojkliknutím na třídu v diagramu tříd
	 */
	final void openClassInCodeEditor(final DefaultGraphCell selectedCell, final Class<?> choosedClass) {
		// Proměnná, do které načtu bud nové zkompilovanou třídu - pokud se uživatel rozhodně otevřít třídu v editoru kodu
		// dvoj - kliknutím na třídu v diagramu tříd,
		// Nebo si ji zde předám v parametru této metody z popupMenu, kde se již dopředu zkompiluje - aby se nemusela dvakrát zbytečně
		// kompilovat ta samá třídy - kdy se v podstatě nedá nic změnit - během chvíle s otevřenou nápovědou:
		final Class<?> loadedCompiledClass;

		if (choosedClass != null)
			loadedCompiledClass = choosedClass;
		// Zde je třeba třídu zkompilovat a načíst (První případ):
		else
			loadedCompiledClass = App.READ_FILE.getClassFromFile(selectedCell.toString(), outputEditor, cellsList,
					true, false);


		// Note: Zde nemusím testovat, zda je získaná třída - zkompilovaná null nebo ne, to je již pořešeno dáel v kódu,
		// a navíc se konkrétně v tomto případě jedná pouze o doplnění proměnných do nápovědy v editoru kodu.

		// Nejprve potřebuji načíst cestu k označené třídě, a zda vůbec existuje:
		final String pathToSrc = App.READ_FILE.getPathToSrc();

		if (pathToSrc != null) {
			// Zde jsem našel konečnou cestu k dané třídě, tak otestuji, zda vůbec daný soubor - třída existuje
			// a pokud ano, tak ji mohu předat do editoru kódu, který načte její zdrojový kód, který lze dále
			// editovat, ... - viz editor kódu:
			final String pathToClass = pathToSrc + File.separator + selectedCell.toString().replace(".", File.separator)
					+ ".java";

			final File fileClass = new File(pathToClass);

			if (fileClass.exists() && fileClass.isFile()) {
				// Zde mám pro dialog připravené vše, co v této chvíli potřebuji, tak mohu vytvořit jeho instanci,
				// nastavitm mu uživatelem vzolený jazyk pro texty do dialogu a zviditelnit ho:
				final CodeEditorDialog ced = new CodeEditorDialog(pathToSrc,
						selectedCell.toString().replace(".", File.separator), loadedCompiledClass, languageProperties,
						GraphClass.this);
				ced.setVisible(true);
			}

			else
				JOptionPane.showMessageDialog(this, txtMissingClassText + "\n" + txtPath + ": " + pathToClass,
						txtMissingClassTitle, JOptionPane.ERROR_MESSAGE);
		}
	}










	/**
	 * Metoda, která v průzkumníku projektů zobrazí zdrojový kód označené třídy v
	 * diagramu tříd.
	 *
	 * Bud se daná označená třída zkompiluje a předá do dialogu nebo se předá v
	 * parametru metody (pokud není proměnná choosedClass null) - tj. předá se z
	 * popupMenu, kde už bude zkompilovaná
	 *
	 * @param selectedCell
	 *            - označená třída z diagramu tříd
	 *
	 * @param choosedClass
	 *            - zkompilovaná tříd z popupMenu, nebo null, pokud se kód má načíst
	 *            drojkliknutím na třídu v diagramu tříd
	 */
	final void openClassInProjectExplorer(final DefaultGraphCell selectedCell, final Class<?> choosedClass) {
		// Proměnná, do které načtu bud nové zkompilovanou třídu - pokud se uživatel rozhodně otevřít třídu v editoru kodu
		// dvoj - kliknutím na třídu v diagramu tříd,
		// Nebo si ji zde předám v parametru této metody z popupMenu, kde se již dopředu zkompiluje - aby se nemusela dvakrát zbytečně
		// kompilovat ta samá třídy - kdy se v podstatě nedá nic změnit - během chvíle s otevřenou nápovědou:
		final Class<?> loadedCompiledClass;

		if (choosedClass != null)
			loadedCompiledClass = choosedClass;
		// Zde je třeba třídu zkompilovat a načíst (První případ):
		else
			loadedCompiledClass = App.READ_FILE.getClassFromFile(selectedCell.toString(), outputEditor, cellsList,
					true, false);


		// Note: Zde nemusím testovat, zda je získaná třída - zkompilovaná null nebo ne, to je již pořešeno dáel v kódu,
		// a navíc se konkrétně v tomto případě jedná pouze o doplnění proměnných do nápovědy v editoru kodu.

		// Nejprve potřebuji načíst cestu k označené třídě, a zda vůbec existuje:
		final String pathToSrc = App.READ_FILE.getPathToSrc();

		if (pathToSrc != null) {
			// Zde jsem našel konečnou cestu k dané třídě, tak otestuji, zda vůbec daný soubor - třída existuje
			// a pokud ano, tak ji mohu předat do editoru kódu, který načte její zdrojový kód, který lze dále
			// editovat, ... - viz editor kódu:
			final String pathToClass = pathToSrc + File.separator + selectedCell.toString().replace(".", File.separator) + ".java";

			final File fileClass = new File(pathToClass);

			if (fileClass.exists() && fileClass.isFile()) {
				// Nyní si ještě potřebuji načíst cestu k otevřenému projektu kvůli průzkumníkovu projektů (dialogu),
				// aby veděl, jaký kořenévý adresář má proheldat, tuto testu k otevřenému projektu si vezmu z preferencí,
				// kam se měla tato cesta uložit, když se příslušný projekt otevřen a to že existuje již vím, protože
				// existuje třída, která se má otevřít a ta je pouze v otevřeném projektu, tak takorát otestuji, zda není null,
				// - pro případ, že by se ji nepodařilo z preferencí získat:
				final String pathToOpenProject = PreferencesApi.getPreferencesValue(PreferencesApi.OPEN_PROJECT, null);

				if (pathToOpenProject != null)
					new ProjectExplorerDialog(new File(pathToOpenProject), fileClass, languageProperties, this,
							loadedCompiledClass);
			}

			else
				JOptionPane.showMessageDialog(this, txtMissingClassText + "\n" + txtPath + ": " + pathToClass,
						txtMissingClassTitle, JOptionPane.ERROR_MESSAGE);
		}
	}





    /**
     * Metoda, která "zruší" vztah, který zvolil uživatel a měl se vytvořit mezi třídami,
     * <p>
     * Tato metoda se zavolá například, když se zavře projěkt, otevře jiný, apod.
     */
    public static void deselectChoosedEdge() {
        clearVariablesForChooseEdge();

        buttonsPanel.setSelectedMenuButtons(null);
    }


    /**
     * Nastavení hodnoty null proměnným, které značí označení / vytvoření vztahů mezi třídami.
     */
    public static void clearVariablesForChooseEdge() {
        edgeType = null;
        cell1 = null;
        cell2 = null;
    }








	/**
	 * Metoda, která "nasetuje" referenci na buttonsPanel, aby se mohli po vytvoření
	 * vztahu mezi třídami nastavit tlačítka tak, aby nevypadaly, že jsou stisknuté
	 *
	 * @param buttonsPanel
	 *            - reference na instanci třídy ButtonsPanel
	 */
	public static void setButtonsPanel(final ButtonsPanel buttonsPanel) {
		GraphClass.buttonsPanel = buttonsPanel;
	}



















	/**
	 * Metoda, která otestuje, zda se v diagramu tříd nachází buňka, která
	 * reprezentuje třídu v diagramu tříd, která obsahuje text v parametru metody.
	 *
	 * @param text
	 *            - text, který by měla obsahovat třída, resp. buňka reprezentující
	 *            třídu, o které se má posoudit, zda je v diagramu tříd.
	 *
	 * @return true, pokud se v diagramu tříd nachází buňka, která reprezentuje
	 *         třídu a obsahuje text v parametru metody (text)
	 */
	public static boolean isClassWithTextInClassDiagram(final String text) {
		for (final DefaultGraphCell d : cellsList) {
			// Nesmí to být hrana, ale třída a musí obsahovat text v parametru:
			if (!(d instanceof DefaultEdge) && KIND_OF_EDGE.isClassCell(d) && d.toString().equals(text))
				return true;
		}

		return false;
	}












	/**
	 * Metoda, která otestuje, zda öbjekt v parametru metody je třída a pokud ano,
	 * tak zda ještě existuje v adresáři src v otevřeném projektu.
	 *
	 * @param cell
	 *            = označený objekt v diagramu tříd
	 *
	 * @return true, pokud je to třída a ještě v adresáři src v příslušném balíčku
	 *         existuje, jinak false
	 */
	public static boolean existClassInSrc(final DefaultGraphCell cell) {
		if (cell != null && KIND_OF_EDGE.isClassCell(cell)) {
			final String pathToSrc = App.READ_FILE.getPathToSrc();

			if (pathToSrc != null) {
				final String classText = cell.toString().replace(".", File.separator) + ".java";

				final File fileClass = new File(pathToSrc + File.separator + classText);

				return ReadFile.existsFile(fileClass.getAbsolutePath());
			}
		}

		return false;
	}














	/**
	 * Metoda, která do diaggramu tříd přidá hranu mezi příslušné dvě třídy - resp.
	 * buňky, reprezentující nějaké třídy.
	 *
	 * @param edgeInfo
	 *            - objekt, který obsahuje informace o tom, jaká hrana, resp. hrana
	 *            reprezentující jaký typ vztahu mezi třídami a i ty třídy (buňky,
	 *            mezi kterými se má vztah vytvořit), případně i kardinalitu
	 *            (multiplicitu), která se má vytvořit vložit na příslušné hrany.
	 */
	final void addEdge(final InfoForNewEdge edgeInfo) {
		if (edgeInfo.getEdgeType().equals(EdgeTypeEnum.EXTENDS))
			addExtendsEdge(edgeInfo.getSrcCell(), edgeInfo.getDesCell());

		else if (edgeInfo.getEdgeType().equals(EdgeTypeEnum.IMPLEMENTATION))
			addImplementsEdge(edgeInfo.getSrcCell(), edgeInfo.getDesCell());

		else if (edgeInfo.getEdgeType().equals(EdgeTypeEnum.AGGREGATION_1_1))
			addAggregation_1_1ku_1_Edge(edgeInfo.getSrcCell(), edgeInfo.getDesCell(), edgeInfo.getCardinalitySrc(),
					edgeInfo.getCardinalityDes());

		else if (edgeInfo.getEdgeType().equals(EdgeTypeEnum.AGGREGATION_1_N))
			addAggregation_1_ku_N_Edge(edgeInfo.getSrcCell(), edgeInfo.getDesCell());

		else if (edgeInfo.getEdgeType().equals(EdgeTypeEnum.ASSOCIATION))
			addAssociationEdge(edgeInfo.getSrcCell(), edgeInfo.getDesCell(), edgeInfo.getCardinalityAsc());
	}

















	/**
	 * Metoda, která prohledá celou kolekci s objekty v tomto diagramu tříd, a
	 * otestuje, zda se jedná o komentář, pokud ano, tak si jeho textu uloží do
	 * kolekce, kterou na konec metoda vrátí, a příslušný komentář (/ře) smaže.
	 *
	 * Tato metoda se volá pouze v případě prohledávání adresáře src pro aktualizaci
	 * tohoto diagramu tříd, takže se komentář musí testovat jednak před kindOfEdge,
	 * ale také, si musím vzít text z dané buňky, která by mohla být komentář a
	 * otestovat, zda opravdu neexistuje třída, s daným textem v adresáři src a
	 * přslušném balíčku, protože pokud došlo napřílad ke změně stylů buňěk či hran,
	 * apod. tak by se již nerozpoznaly objekty s původním designem, proto je třeba
	 * to testovat i s existencí na disku (v případě komentáře)
	 *
	 * @param cell
	 *            - buňka ocby třída v tomto diagramu tříd, u které chci smazat
	 *            všechny komentáře a jejich text si uložit do koelkce a tu vrátit.
	 *
	 * @return kolekci - List typu String, který obsauje získané texty z komentářů,
	 *         které byly připojeny k buňce v parametru metody - cell
	 */
	private List<String> getCommentsTextOnCell(final DefaultGraphCell cell) {
		// Kolekce pro texty z koemntářů
		final List<String> commentsTextList = new ArrayList<>();

		// kolekce pro objekty, které se mají smazat:
		final List<DefaultGraphCell> objectsForDeleteList = new ArrayList<>();

		// cesta k adresáři src, abych mohl otestovat, zda vůbec třídy existují:
		final String pathToSrc = App.READ_FILE.getPathToSrc();

		if (pathToSrc != null)

		for (final DefaultGraphCell c : cellsList) {
			if (c instanceof DefaultEdge) {
				final DefaultGraphCell srcCell = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) c).getSource()).getParent();
				final DefaultGraphCell desCell = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) c).getTarget()).getParent();

				if (cell == desCell) {
					// Zde je označená buňka - třída v parametru cíl této testované hrany

					// Otestuji, zda její zdroj neexistuje - je to komentář, pokud ano, tak si uložím jeho text pro pozdější přidání,
					// k nové buňce - třídě

					// Zde musím otestovat, jednak, zda je to komentář - pokud se styl příslušné buňky, která reprezentuje komentář
					// nezměnil a pokud se styl změnil, tak tato metod vrátí false, pak musím otestovat, zda opravdu text z dané buňky není
					// náhodou text nějaké třídy a pokud ani to ne, tak se jedná o komentář
						if (KIND_OF_EDGE.isCommentCell(desCell) || !ReadFile.existsFile(pathToSrc + File.separator
								+ srcCell.toString().replace(".", File.separator) + ".java")) {
							// přidám si do kolekce text komentáře:
							commentsTextList.add(srcCell.toString());

							// Objekty pro smazání dám do nové kolekce, kterou
							// následně smažu:
							objectsForDeleteList.add(c);
							objectsForDeleteList.add(srcCell);

						}
					}
				}
			}



		// Nejprve smažu všechny objekty, coby komentáře a aktualizuji diagram:
		if (!objectsForDeleteList.isEmpty()) {
			objectsForDeleteList.forEach(c -> deleteObjectFromGraph(c, cellsList));
			updateDiagram(cellsList);
		}


		// nyní mohu vrátit kolekce s texty pro komentáře:
		return commentsTextList;
	}














	/**
	 * Metoda, která projede všechny objekty v tomto diagramu tříd, a otestuje, zda
	 * es jedná o buňku s daným textem a není to komentář, pokud se taková najde,
	 * pak se odstraní i se všemi hranami.
	 *
	 * jedná se o metodu, která se používá hlavně pro "přenačtění" objektů v domto
	 * diagramu, pokud se změní nějaké styly buňěk, například barva pozadí, ... v
	 * takovém případě se při otevření projektu musí znovu přenašít a ty původní,
	 * reps. ty s původním designem se musí odstranit, jinak by tam byly všechny
	 * dvakraát.
	 *
	 * @param text
	 *            - text, který by měla obsahovat buňka pro smazání
	 *
	 * @return kolekci - List, který obsahuje texty komentářů, které byly u původní
	 *         buňky (nalezené) a které je třeba připojit k nové buňce coby třídě
	 */
	final List<String> deleteCellWithText(final String text) {
		// List - kolekce, do které si vložím získané texty z komentářů, které se
		// mohli nacházet u nalezené buňky (pokud byla nalezena)
		List<String> commentsList = new ArrayList<>();

		DefaultGraphCell cellForDelete = null;

		for (final DefaultGraphCell c : cellsList) {
			// Otestuji, zda to není hrana, obsahuje zadaný text a není to komentář,
			// Note:
			// komentář, také nemusí projít při změné stylů, ale to ani třída a navíc se to vše aktualizuje
			if (!(c instanceof DefaultEdge) && c.toString().equals(text) && !KIND_OF_EDGE.isCommentCell(c)) {
				// Zde jsem našel shodu a jelikož je už dané v aplikaci, že nelze vytvořit dvě třídy, které
				// obsaují stejné texty, resp. dvě třídy ve stejném balíčku se stejným názvem,
				// tak zde mohu skončit
				cellForDelete = c;
				break;
			}
		}


		// Otestuji, zda byla nějaká buňka s daným názvem nalezena a pokud ano,
		// tak ji smažu a přenačtu diagram:
		if (cellForDelete != null) {
			// Nyní mám tedy buňku coby třídu s původním designem, která se má smazat, jenže než se smaže
			// tato buňka, tak musím smazat všechny hrany, které z ní vychzájeí nebo s ní jsou spojeny

			// Takže prohledám znovu celý diagram ale tentokrát budu testovat všechny hrany, které buď vyházejí
			// nebo "vcházejí" - jsou cílem nalezené buňky, a všechny hrany, které takto najdu a jsou spojeny s tou
			// nalezenou buňkou smažu a až pak smažu samotnou původní třidu - s původním deisgnem:

			// Nejprve vymažu všechny komentáře k dané buňce - původní třídě:
			commentsList = getCommentsTextOnCell(cellForDelete);

			final List<DefaultEdge> edgesForDelete = new ArrayList<>();

			for (final DefaultGraphCell c : cellsList) {
				if (c instanceof DefaultEdge) {
					// Zde vím, že je to hrana
					// Zjistím si její zdroj a cíl
					final DefaultGraphCell srcCellOfEdge = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) c).getSource()).getParent();
					final DefaultGraphCell desCellOfEdge = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) c).getTarget()).getParent();

					if (cellForDelete == srcCellOfEdge || cellForDelete == desCellOfEdge)
						edgesForDelete.add((DefaultEdge) c);
				}
			}


			// Nyní mohu vymazat všechny nalezené hrany, které jsou spojeny s nalezenou
			// buňkou:
			edgesForDelete.forEach(c -> deleteObjectFromGraph(c, cellsList));

			// Nyní mohu vymazat ten nalezeý objekt - třídu:
			deleteObjectFromGraph(cellForDelete, cellsList);

			// aktualizuji diagram:
			updateDiagram(cellsList);
		}

		return commentsList;
	}










	/**
	 * Tato metoda zde ani nemusí být, je použit v menu, akorát pro to, že, když se
	 * uživatel rozhodne zkompilovat třídy, tak aby v tomto Diagramu tříd, alespoň
	 * jedna třída byla, jinak nemá smysl něco kompilovat, takže tato metoda zjistí,
	 * zda je v tomto Diagramu tříd alespon jedna trida
	 *
	 * @return true, pokud je v diagramu alespon jedna trida, tj. cyklus v metode
	 *         narazi na prvni tridu, jinak false, pokud diagram neobsahuje zadnou
	 *         tridu, tj. projdou se vsechny objekty v tomto diagramu a zadny z nich
	 *         neni bunka, ktera reprezentuje tridu
	 */
	public static boolean areInDiagramAtLeastOneClass() {
		// Projdu cyklem všechny obejkty v tomto diagrau trid a pro kazdy objekt otestuji, zda
		// se jedna o bunkku, ktera reprezentuje tridu:
		for (final DefaultGraphCell c : cellsList)
			if (KIND_OF_EDGE.isClassCell(c))
				// Zde se jedná o bunku, která reprezentuje tridu, tak mohu vratit true,
				// diagram obsahuje alespon jednu tridu:
				return true;

		return false;
	}












	/**
	 * Metoda, která zkusí najít v diagramu tříd buňku - třídu, která obsahuje
	 * zadaný text (text).
	 *
	 * @param text
	 *            - text, který by měla obsahovat buňka - třída v diagramu tříd
	 *
	 * @return null, pokud se nenajde buňka - třída v diagramu tříd, která obsahuje
	 *         text v parametr (text), pokud se tato buňka najde, tak se vrátí
	 */
	public static DefaultGraphCell getClassCellWithText(final String text) {
		for (final DefaultGraphCell d : cellsList) {
			if (!(d instanceof DefaultEdge) && KIND_OF_EDGE.isClassCell(d) && d.toString().equals(text))
				return d;
		}

		return null;
	}














	/**
	 * Metoda, která slouží pro vymazání všech vztahů mezi třídami v diagramu tříd s
	 * výjimkou hran, které značí spojení třídy s komentářem.
	 *
	 * "Vymažou se veškeré hrany / šipky mezi třídami s výjimkou hran pro
	 * komentáře".
	 */
	public final void deleteAllEdgesExceptCommentEdges() {
		for (final Iterator<DefaultGraphCell> it = cellsList.iterator(); it.hasNext();) {
			final DefaultGraphCell cell = it.next();

			if (cell instanceof DefaultEdge && !KIND_OF_EDGE.isCommentEdge((DefaultEdge) cell)) {
				it.remove();

				// Odeberu objekt z modelu grafu
				if (getModel().contains(cell))
					getModel().remove(new Object[] { cell });
			}
		}
		updateDiagram(cellsList);
	}














	/**
	 * Getr na logickou proměnnou, která značí, zda se má v dialogu pro zavolání
	 * konstruktoru zpřístupnit možnost pro vytvoření nové instance. V případě, že
	 * je parametr konstruktoru parametr typu nějaké třídy z diagramu tříd, tak do
	 * tohoto parametru lze předat pouze instance příslušné třídy, které se aktuálně
	 * nachází v diagramu instancí, ale pokud v diagramu instancí žádní instance
	 * příslušné třídy - parametr konstruktoru) není, pak nepůjde zavolat příslušný
	 * konstruktoru (jedině vytvořit tu novou instanci).
	 *
	 * @return - true, pokud se má zpřístupnit možnost pro vytvoření nové instance v
	 *         dialogu pro zavolání konstruktoru inak false.
	 */
	public static boolean isShowCreateNewInstanceOptionInCallConstructor() {
		return showCreateNewInstanceOptionInCallConstructor;
	}






	/**
	 * Setr pro proměnnou, která obsahuje velikost textu v buňce reprezentující
	 * třídu v diagramu tříd.
	 *
	 * @param classFontSize
	 *            - velikost písma v buňce reprezentující třídu.
	 */
	public static void setClassFontSize(int classFontSize) {
		GraphClass.classFontSize = classFontSize;
	}







	/**
	 * Getr na proměnnou, která značí, zda se mají při zavolání konstruktoru
	 * zpřístupnit veřejné a chráněné proměnné z tříd v diagramu tříd a instancí v
	 * diagramu instancí a z proměnných vytvořených pomocí editoru příkazů.
	 *
	 * @return výše popsanou logickou hodnotu.
	 */
	public static boolean isMakeVariablesAvailable() {
		return makeVariablesAvailable;
	}






	/**
	 * Getr na proměnnou, která značí, zda se mají v dialogu pro zavolání
	 * konstruktoru zobrazovat i metody, jejíž návratové hodnoty je možné předat do
	 * příslušného parametru konstruktoru.
	 *
	 * @return je popsáné výše, true, mají se zobrazovatmetody na výběr, false
	 *         nemají.
	 */
	public static boolean isMakeMethodsAvailableForCallConstructor() {
		return makeMethodsAvailableForCallConstructor;
	}


	/**
	 * Getr na proměnnou, která značí, zda se v kontextovém menu nad třídou v diagramu tříd mají zobrazovat soukromé
	 * konstruktory.
	 *
	 * @return viz výše.
	 */
	public static boolean isIncludePrivateConstructors() {
		return includePrivateConstructors;
	}

	/**
	 * Getr na proměnnou, která značí, zda se v kontextovém menu nad třídou v diagramu tříd mají zobrazovat chráněné
	 * konstruktory.
	 *
	 * @return viz výše.
	 */
	public static boolean isIncludeProtectedConstructors() {
		return includeProtectedConstructors;
	}

	/**
	 * Getr na proměnnou, která značí, zda se v kontextovém menu nad třídou v diagramu tříd mají zobrazovat
	 * package-private konstruktory.
	 *
	 * @return viz výše.
	 */
	public static boolean isIncludePackagePrivateConstructors() {
		return includePackagePrivateConstructors;
	}


	/**
	 * Getr na proměnnou, která značí, zda se v kontextovém menu nad třídou v diagramu tříd mají zpřístupnit i statické
	 * soukromé vnitřní třídy.
	 *
	 * @return true, když se mají zpřístupnit soukromé statické vnitřní třídy, jinak false.
	 */
	public static boolean isIncludePrivateInnerStaticClasses() {
		return includePrivateInnerStaticClasses;
	}

	/**
	 * Getr na proměnnou, která značí, zda se v kontextovém menu nad třídou v diagramu tříd mají zpřístupnit i statické
	 * chráněné vnitřní třídy.
	 *
	 * @return true, když se mají zpřístupnit chráněné statické vnitřní třídy, jinak false.
	 */
	public static boolean isIncludeProtectedInnerStaticClasses() {
		return includeProtectedInnerStaticClasses;
	}

	/**
	 * Getr na proměnnou, která značí, zda se v kontextovém menu nad třídou v diagramu tříd mají zpřístupnit i statické
	 * package-private vnitřní třídy.
	 *
	 * @return true, když se mají zpřístupnit package-private statické vnitřní třídy, jinak false.
	 */
	public static boolean isIncludePackagePrivateInnerStaticClasses() {
		return includePackagePrivateInnerStaticClasses;
	}


	/**
	 * Getr na proměnnou, která značí, zda se v kontxtovém menu nad třídou v diagramu tříd mají zpřístupnit v nabídce
	 * "Vnitřní statické třídy" pro jednotlivé vnitřní třídy i jejich statické soukromé metody.
	 *
	 * @return true, pokud se mají zpřístupnit pro jednotlivé vnitřní třídy i jejich statické soukromé metody. Jinak
	 * false.
	 */
	public static boolean isInnerStaticClassesIncludePrivateStaticMethods() {
		return innerStaticClassesIncludePrivateStaticMethods;
	}

	/**
	 * Getr na proměnnou, která značí, zda se v kontxtovém menu nad třídou v diagramu tříd mají zpřístupnit v nabídce
	 * "Vnitřní statické třídy" pro jednotlivé vnitřní třídy i jejich statické chráněné metody.
	 *
	 * @return true, pokud se mají zpřístupnit pro jednotlivé vnitřní třídy i jejich statické chráněné metody. Jinak
	 * false.
	 */
	public static boolean isInnerStaticClassesIncludeProtectedStaticMethods() {
		return innerStaticClassesIncludeProtectedStaticMethods;
	}

	/**
	 * Getr na proměnnou, která značí, zda se v kontxtovém menu nad třídou v diagramu tříd mají zpřístupnit v nabídce
	 * "Vnitřní statické třídy" pro jednotlivé vnitřní třídy i jejich statické package-private metody.
	 *
	 * @return true, pokud se mají zpřístupnit pro jednotlivé vnitřní třídy i jejich statické package-private metody.
	 * Jinak false.
	 */
	public static boolean isInnerStaticClassesIncludePackagePrivateStaticMethods() {
		return innerStaticClassesIncludePackagePrivateStaticMethods;
	}
}