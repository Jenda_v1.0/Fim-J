package cz.uhk.fim.fimj.class_diagram;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

import org.apache.commons.io.FileUtils;
import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.swing_worker.SwingWorkerInterface;

/**
 * Tato třída slouží jako vlákno, které začne prohledávat adresář src v aktuálně otevřeném projektu a od tohoto adresáře
 * bude testovat všechny nalezené soubory, každý otestuje, zda se jedná o Javovskou třídu, a pokud ano, tak se otestuje,
 * zda se nachází v diagramu tříd, pokud ne, tak se tam přidá a pokud se tam již nachází, tak není co řešit.
 * <p>
 * Zádrhel ale trochu bude, že se musí pokaždé vytvořit výchozí balíček, pokud se třída nachází hned na první úrovní
 * adresáře src - tj. src/Class.java a to být nesmí kvůli kompilátoru, ten vyžaduje, aby všechny třídy byly alespoň v
 * nějakém balíčku, proto se musí vložit do výchozího a přepsat se její balíček na začátku kódu dané třídy.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class RetrieveNotLoadedClassesThread implements SwingWorkerInterface {

	private final File fileSrc;

	private final GraphClass classDiagram;
	
	
	
	/**
	 * Kokntruktor této třídy, slouží pro naplnění proměnných referencema.
	 * 
	 * @param fileSrc
	 *            - reference typu File, která směřuje k adresáři src v aktuálně
	 *            otevřeném projektu v aplikaci
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd, aby se do něj případně mohla přidat
	 *            třída a nebo se mohlo zjistit, zda již v diagramu tříd nalezena
	 *            třída na disku existuje
	 */
	public RetrieveNotLoadedClassesThread(final File fileSrc, final GraphClass classDiagram) {
		super();

		this.classDiagram = classDiagram;

		this.fileSrc = fileSrc;
	}
	
	
	
	
	
	@Override
	public void run() {
		/*
		 * Note:
		 * Zde musí využít následující vlákno SwingUtilities.invokeAndWait, protože
		 * následují některé metody, které volají metody nad diagramem tříd, již je v
		 * příslušné třídě i některých vláknech popsáno, že pokud se pomocí vláken
		 * manipuluje s diagramem tříd nebo instancí, resp. s použitou knihovnou JGraph,
		 * tak se příslušné operace s touto knihovnou musí provádět v "halvním"
		 * swingovém vlákně, jinak vypadává výjimka.
		 */
		try {
			SwingUtilities.invokeAndWait(() -> {
				// Otestuji, zda adresář src existuje a je to opravdu adresář:
				if (fileSrc != null && fileSrc.exists() && fileSrc.isDirectory())
					checkClasses(fileSrc);
			});
			
		} catch (InvocationTargetException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informaci o chybě:
				logger.log(Level.INFO,
						"Zachycena výjimka při pokusu o zavolání SwingUtilities.invokeAndWait pro volání metody pro " +
								"načtení tříd z adresáře src"
								+ " do diagramu tříd, které se dosud nenachází v diagramu tříd (při otevření " +
								"projektu). "
								+ "Tato chyba může nastat například v případě, že je vyhozena výjimka pri pokusu o " +
								"spuštění programu doRun.");

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();
			
		} catch (InterruptedException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při pokusu o zavolání SwingUtilities.invokeAndWait pro volání metody, " +
                                "která zkusí načíst třídy z adresáře src a aktuálně otevřeném projektu do diagramu " +
                                "tříd, "
                                + "které se v diagramu tříd dosud nenachází - při otevření příslušného projektu. "
                                + "Tato chyba může nastat například v případě, že pokud budeme přerušeni při čekaní " +
                                "na událost dispečování vlákna k dokončení spuštění příkazu doRun.run ().");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
		
		
		
		
		// Původně v této metodě run bylo pouze následující:
		// (Výše oprava kvůli výjimce v JGraph)
		
		// Otestuji, zda adresář src existuje a je to opravdu adresář:
//		if (fileSrc != null && fileSrc.exists() && fileSrc.isDirectory())
//			checkClasses(fileSrc);
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která pomocí rekurze prohledá všechny soubory a adresáře od adresáře
	 * src v otevřeném projektu a pokaždé otestuje, zda se jedná o Javovskou třídu,
	 * která se nachází v diagramu tříd, pokud ne, tak se otestuje, zda se nachází
	 * alespoň ve výchozím balíčku, pokud ne, tj. je hned na první úrovní od
	 * adresáře src, tak se přidá do výchozího, jinak se jen přidá do diagramu tříd
	 * - pokud tam není.
	 * 
	 * @param file
	 *            - cesta k souboru nebo adresáři, který se otestuje na Javovskou
	 *            třídu
	 */
	private void checkClasses(final File file) {
		// Načtu si všechny soubory na daném umístění:
		final File[] filesArray = file.listFiles();

		if (filesArray == null)
			return;

		for (final File f : filesArray) {
			if (f == null)
				continue;

			// Otestuji, zda je to adresář, pokud ano, tak prohledávám dál
			if (f.isDirectory())
				checkClasses(f);
					
			// Zde to není adresář, ale nejaký soubor, tak otestuji, zda je to třída jazyka
			// Java:
			else if (f.getName().endsWith(".java")) {
				// Zde se jedná o třídu, tak otestuji, zda se nachází v diagramu tříd:

				// Otestuji, zda není hned na první úrovní adresáře src, v takovém případě by
				// ani neměla být
				// v diagramu:
				final File filePath = f.getParentFile();

				if (filePath != null && filePath.getAbsolutePath().equals(fileSrc.getAbsolutePath())) {
					// Zde je "rodičem" třídy adrsář src, tak je třeba jí přidat do balíčku
					// defaultPackage:
					// ale nejprve otestovat, zda se tam ještě nenachází

					// Otestuji, zda existuje výchozí balíček případně ho vytvořím

					try {
						// vytvořím si adresář defaultPackage - pokud neexistuje:
						Files.createDirectories(Paths.get(fileSrc.getAbsolutePath(), Constants.DEFAULT_PACKAGE));
						
					} catch (IOException e) {
                        /*
                         * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                         * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                         * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                         * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                         */
                        final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                        // Otestuji, zda se podařilo načíst logger:
                        if (logger != null) {
                            // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                            // Nejprve mou informaci o chybě:
                            logger.log(Level.INFO,
                                    "Zachycena výjimka pří vytváření adresářů, třída RetrieveNotloadedClassesThread, " +
                                            "na vytvoření: "
                                            + fileSrc.getAbsolutePath() + File.pathSeparator
                                            + Constants.DEFAULT_PACKAGE);

                            /*
                             * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                             * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                             * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                             */
                            logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                        }
                        /*
                         * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                         */
                        ExceptionLogger.closeFileHandler();
                    }

					// Zde by již měl existovat výchozí adresář, tak zjistím, zda se v něm nachází
					// požadovaná třída, pokud ne, tak ji tam přesunu, pokud ano, tak se později
					// načte do diagramu
					// tříd - pokud tam ještě není:
					final File desFile = new File(fileSrc.getAbsolutePath() + File.separator + Constants.DEFAULT_PACKAGE
							+ File.separator + f.getName());

					// Přesunu si třídu do defaultPackage:
					try {
						if (!desFile.exists()) {
							// Pokud v daném adresáři ještě neexistuje, tak ji tam přesunu:
							FileUtils.moveFile(f, desFile);

							// Zde, pokud neexistovala daná třída ve výchozím balíčku, tak ted už by tam měl
							// být,
							// tak si opět zjistím její balíčkty - od adresáře src (bez něj až po příponu)
							final String classNameWithPackages = desFile.getAbsolutePath()
									.substring(fileSrc.getAbsolutePath().length() + 1,
											desFile.getAbsolutePath().lastIndexOf('.'))
									.replace(File.separator, ".");

							replaceTheOriginalCell(classNameWithPackages);

							// Balíčky třídy jsou od začátku po poslední tečku , za ní je již název třídy:
							final String packageText = classNameWithPackages.substring(0,
									classNameWithPackages.lastIndexOf('.'));

							App.WRITE_TO_FILE.deletePackageIfExist(desFile.getAbsolutePath());
							App.WRITE_TO_FILE.addPackageToClass(desFile.getAbsolutePath(), packageText);
						}

					} catch (IOException e) {
                        /*
                         * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                         * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                         * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                         * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                         */
                        final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                        // Otestuji, zda se podařilo načíst logger:
                        if (logger != null) {
                            // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                            // Nejprve mou informaci o chybě:
                            logger.log(Level.INFO,
                                    "Zachycena výjimka při přesouvání souboru, třída RetrieveNotLoadedClassesThread" +
                                            ".java, zdroj: "
                                            + f.getAbsolutePath() + ", cíl: " + desFile.getAbsolutePath());

                            /*
                             * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                             * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                             * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                             */
                            logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                        }
                        /*
                         * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                         */
                        ExceptionLogger.closeFileHandler();
                    }
				}

				else {
					// Zde není rodičem té třídy adresář src, takže se již v nějakém "balíčku" -
					// adresáři nahází,
					// tak si ho jen zjistím, a otestuji, zda je v diagramu tříd, případně ho tam
					// vložím
					final String classNameWithPackages = f.getAbsolutePath()
							.substring(fileSrc.getAbsolutePath().length() + 1, f.getAbsolutePath().lastIndexOf('.'))
							.replace(File.separator, ".");

					// Zde akorát otestuji, zda se již třída s příslušným názvem nachází v diagramu
					// tříd nebo ne,
					// a pokud ne, tak ji tam přidám:
					replaceTheOriginalCell(classNameWithPackages);
				}
			}
		}
	}




	
	
	
	
	
	
	
	
	private void replaceTheOriginalCell(final String classNameWithPackages) {
		if (GraphClass.getClassCellWithText(classNameWithPackages) == null) {
			// Zde se v diagramu tříd nenachazi bunka, coby trida s danym textem,
			// tak ji musím přidat, ale musím také otestovat, zda se v danem diagramu
			// nachází nějaka bunka, ktera neni komentar a obsahuje dany text, pokud ano, tak 
			// ji musím smazat, protoze je možné, ze se zmenily styly bunek, pak nebudou rozpoznány aktuální
			// buňky, takže toto slouží zároveň jako takové přenačtení diagramu pro nové styly:
			
			// Získám si i kolekce s komentáři, které mohli být připojené k původní buňce - třídě
			final List<String> commentsTextList = classDiagram.deleteCellWithText(classNameWithPackages);
			
			// Nyní mohu přidat novou buňku s novým designem (pokud se změnil design),
			// jinak se prostě přidá nová třída do diagramu tříd, která byla nalezena na disku a ještě
			// není v diagramu tříd:
			classDiagram.addClassCell(classNameWithPackages);
			
			
			// Nyní - pokud se našly nějako koentáře, tak je třeba jej opět připojit k původní třídě,
			// v tomto případě se jedná o nově vytvořenou třídu, která se nachází na posledním místě v kolekci,
			// tak projdu kolekci získaných komentářů a případně je přidám k nové nově vytvořené třídě:
			if (!commentsTextList.isEmpty()) {
				// získám si buňku coby nově vytvořenou třídu, která je na psledním místě v kolekci:
				final DefaultGraphCell lastCell = GraphClass.getCellsList().get(GraphClass.getCellsList().size() - 1);
				
				// Pro jistotu otestuji, zda je to opravdu nově vytvořená tříd a obsahuje zadaný text - 
				// - ale toto bych již testovat nemusel, protože metoda pro přidání třídy ji přidá na konec kolekce,
				// a jinak s ní v tomto případě nic nedělám, takže by to ani nemělo být jiank - ale kdyby náhodou:
				if (!(lastCell instanceof DefaultEdge) && lastCell.toString().equals(classNameWithPackages))
					for (final String s : commentsTextList)
						classDiagram.addComment(lastCell, s);
			}											
		}
		// else - zde již třída existuje v diagramu trid
	}
}