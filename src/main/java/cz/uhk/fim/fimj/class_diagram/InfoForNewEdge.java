package cz.uhk.fim.fimj.class_diagram;

import org.jgraph.graph.DefaultGraphCell;

/**
 * Tato třída slouží pro vytvoření a uchování informací o dvou buňkách - resp. třídách v diagramu tříd, mezi kterými se
 * má vytvořit hrana a informaci o typu hrany, která se má vytvořit.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

class InfoForNewEdge {

    /**
     * Zdroj hrany.
     */
    private final DefaultGraphCell srcCell;

    /**
     * Cíl hrany.
     */
    private final DefaultGraphCell desCell;

    /**
     * Typ hrany reprezentující nějaký vztah.
     */
    private final EdgeTypeEnum edgeType;

    /**
     * Kardinalita (multiplicita), která se bude nacházet na straně začátku hrany.
     */
    private final int cardinalitySrc;

    /**
     * Kardinalita (multiplicita), která se bude nacházet na straně konci hrany.
     */
    private final int cardinalityDes;


    /**
     * Tato proměnná slouží jako číslo (počet proměnných) pro kardinalitu (multiplicitu), která bude "znázorněna" na
     * obou koncích přslušné hrany (symetrické) asociace.
     * <p>
     * Note: Mohl bych využít jednu z proměnných cardinalitySrc nebo cardinalityDes, ale pro přehlednost, jsem sem
     * doplnil další proměnnouk, abych se nepletl jejich účely. V této třídě jich tolik není a i tak je přehledná. Tak
     * jedna proměnná nic moc nezmění.
     */
    private final int cardinalityAsc;


    /**
     * Konstruktor, který vytvoří instanci této třídy, a zárovveň do ní vloží potřebná data pro vytvoření nové hrany
     * mezi dvěma buňkami (třídami) v diagramu tříd.
     * <p>
     * Tento konstruktor slouží hlavně pro naplnění níže uvedených proměnných potřebných pro vytvoření vztahu typu
     * (asymetrická) asociace, kde se udává různá kardinalita (multiplicita) na obou koncích hrany.
     *
     * @param srcCell
     *         - zbuňka, která má být zdrojem hrany tyup edgeType
     * @param desCell
     *         - buňka, která má být cílem hrany typu edgeType
     * @param cardinalitySrc
     *         - číslo typu int, které značí kardinalitu (multiplicitu) na začátku hrany typu (asymetrická) asociace
     *         (agregace 1 : 1).
     * @param cardinalityDes
     *         - číslo typu int, které značí kardinalitu (multiplicitu) na konci hrany typu (asymetrická) asociace
     *         (agregace 1 : 1).
     * @param edgeType
     *         - informace o typu hrany, která se má vytvořit mezi bunkami výše (srcCell a desCell)
     */
    InfoForNewEdge(final DefaultGraphCell srcCell, final DefaultGraphCell desCell, final int cardinalitySrc,
                   final int cardinalityDes, final EdgeTypeEnum edgeType) {

        this.srcCell = srcCell;
        this.desCell = desCell;
        this.cardinalitySrc = cardinalitySrc;
        this.cardinalityDes = cardinalityDes;
        this.edgeType = edgeType;

        cardinalityAsc = -1;
    }


    /**
     * Konstruktor, který vytvoří instanci této třídy, a zároveň do ní vloží potřebná data pro vytvoření nové hrany mezi
     * dvěma buňkami (třídami) v diagramu tříd.
     * <p>
     * Tento konstruktor slouží hlavně pro naplnění níže uvedených proměnných potřebných pro vytvoření vztahu typu
     * (symetrická) asociace, kde se udává stejná multiplicita na obou koncích příslušné hrahny.
     *
     * @param srcCell
     *         - zbuňka, která má být zdrojem hrany tyup edgeType
     * @param desCell
     *         - buňka, která má být cílem hrany typu edgeType
     * @param cardinalityAsc
     *         - číslo typu int, které značí kardinalitu (multiplicitu) pro oba konce hrany pro (symetrickou) asociace.
     * @param edgeType
     *         - informace o typu hrany, která se má vytvořit mezi bunkami výše (srcCell a desCell)
     */
    InfoForNewEdge(final DefaultGraphCell srcCell, final DefaultGraphCell desCell, final int cardinalityAsc,
                   final EdgeTypeEnum edgeType) {

        this.srcCell = srcCell;
        this.desCell = desCell;
        this.cardinalityAsc = cardinalityAsc;
        this.edgeType = edgeType;

        cardinalitySrc = -1;
        cardinalityDes = -1;
    }


    final DefaultGraphCell getSrcCell() {
        return srcCell;
    }

    final DefaultGraphCell getDesCell() {
        return desCell;
    }

    final int getCardinalityAsc() {
        return cardinalityAsc;
    }

    final int getCardinalityDes() {
        return cardinalityDes;
    }

    final int getCardinalitySrc() {
        return cardinalitySrc;
    }

    final EdgeTypeEnum getEdgeType() {
        return edgeType;
    }
}