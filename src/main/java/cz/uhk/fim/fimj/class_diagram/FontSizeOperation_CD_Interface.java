package cz.uhk.fim.fimj.class_diagram;

/**
 * Toto rozhraní slouží pro deklaraci metod, které slouží pro nastavení velikosti písma pomocí dialogu pro "rychlé"
 * nastavení velikosti písma všem objektům v diagramu tříd.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface FontSizeOperation_CD_Interface {

	/**
	 * Metoda, která slouží pro změnu velikosti písem / fontu v podstatě ve všech
	 * objektech (označené v nastavení), které se mohou vyskytovat v diagramu tříd.
	 * 
	 * Metoda vždy nastaví příslušným objektů velikost písma / fontu na velikost
	 * size, ale pokud budou označené všechny komponenty, pak by mohlo déjít k té
	 * chybě, že pokud bude v nastavení hran nebo třídy a komentáře nastavené uplně
	 * stejné vlastnosti, pak v případě, že se zde změní velikost písma, nastaví se
	 * u všech označených objektů příslušná velikost písma, ale už se netestuje, zda
	 * se nějak ty objekty liší, například třída od komentáře apod. Pokud budou
	 * stejně, pak se může stát, že nebude příslušný objekt / objekty v diagramu
	 * tříd rozpoznány.
	 * 
	 * Note:
	 * Šel by využít jen jeden objektu Propeties a ten pak předávat do metod pro
	 * zjišˇˇtování vztahů, ale původně jsem měl vždy aktuální načítání ze souboru a
	 * pak jsem to takto ponechal na držení v aplkaci a pohou úpravu hodnot, tak
	 * jsem to tak i nadále nechal, kdybych se rozhodl ještě někdy v budoucnu jinak.
	 * 
	 * @param size
	 *            - nová velikost, na kterou se má nastavit velikost písma / fontu v
	 *            příslušných / nastavených objektech v diagramu tříd.
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd.
	 */
	void quickSetupOfFontSize(final int size, final GraphClass classDiagram);

	
	
	
	/**
	 * Metoda, která slouží pro nastavení výchozí velikosti písma pro veškeré
	 * objekty v diagramu tříd.
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd.
	 */
	void quickSetupDefaultFontSizes(final GraphClass classDiagram);

	
	
	
	/**
	 * Metoda, která slouží pro "rychlé" nastavení velikosti písma / fontu pro
	 * veškeré objekty v diagramu tříd. Ale nastaví se příslušná velikost size pouze
	 * těm objektům, které jsou označeny / zvoleny, tj. u těch, u kterých bude
	 * proměnná v parametru této metody true. Zbytek se ponechá.
	 * 
	 * @param size
	 *            - nová velikost pro písma pro objekty v diagramu tříd.
	 * 
	 * @param isAssociation
	 *            - logická proměnná, která značí, zda se má nastavit nová velikost
	 *            písma pro hranu reprezentující vztah typu asociace v diagramu
	 *            tříd.
	 * 
	 * @param isExtends
	 *            - logická proměnná, která značí, zda se má nastavit nová velikost
	 *            písma pro hranu reprezentující vztah typu dědičnost v diagramu
	 *            tříd.
	 * 
	 * @param isimplements
	 *            - logická proměnná, která značí, zda se má nastavit nová velikost
	 *            písma pro hranu reprezentující vztah typu implementace rozhraní v
	 *            diagramu tříd.
	 * 
	 * @param isClass
	 *            - logická proměnná, která značí, zda se má nastavit nová velikost
	 *            písma pro buňku reprezentující třídu v diagramu tříd.
	 * 
	 * @param isComment
	 *            - logická proměnná, která značí, zda se má nastavit nová velikost
	 *            písma pro buňku reprezentující komentář v diagramu tříd.
	 * 
	 * @param isCommentEdge
	 *            - logická proměnná, která značí, zda se má nastavit nová velikost
	 *            písma pro hranu reprezentující spojení třídy s komentářem v
	 *            diagramu tříd.
	 * 
	 * @param isAggregation_1_1
	 *            - logická proměnná, která značí, zda se má nastavit nová velikost
	 *            písma pro hranu reprezentující vztahy typu agregace 1 : 1 v
	 *            diagramu tříd.
	 * 
	 * @param isAggregation_1_N
	 *            - logická proměnná, která značí, zda se má nastavit nová velikost
	 *            písma pro hranu reprezentující vztahy typu agregace 1 : N v
	 *            diagramu tříd.
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd.
	 */
	void quickSetupOfFontSize(final int size, final boolean isAssociation, final boolean isExtends,
			final boolean isimplements, final boolean isClass, final boolean isComment, final boolean isCommentEdge,
			final boolean isAggregation_1_1, final boolean isAggregation_1_N, final GraphClass classDiagram);

	
	
	
	/**
	 * Metoda, která slouží pro nastavení velikosti fontu / písma pro buňku, která v
	 * diagramu tříd reperzentuje třídu.
	 * 
	 * @param size
	 *            - nová velikost písma / fontu pro buňku reprezentující třídu v
	 *            diagramu tříd.
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd.
	 */
	void quickSetupOfClassFontSize(final int size, final GraphClass classDiagram);

	
	
	
	/**
	 * Metoda, která slouží pro nastavení velikosti písma / fontu pro buňku, která v
	 * diagramu tříd reprezentuje komentář.
	 * 
	 * @param size
	 *            - nová velikost pro písmo / font pro buňku, která v diagramu tříd
	 *            reprezentuje komentář.
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd.
	 */
	void quickSetupOfCommentFontSize(final int size, final GraphClass classDiagram);

	
	
	
	/**
	 * Metoda, která slouží pro "rychlé" nastavení velikosti písma / fontu pro
	 * hrane, která v diagramu tříd reprezentuje spojení třídy s komentářem.
	 * 
	 * @param size
	 *            - velikost písma / fontu pro hranu reprezentující spojení třídy s
	 *            komentářem.
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd.
	 */
	void quickSetupOfCommentEdgeFontSize(final int size, final GraphClass classDiagram);

	
	
	
	/**
	 * Metoda, která slouží pro "rychlé" nastavení velikosti fontu / písma pro
	 * hranu, která reprezentuje vztah asociace v diagramu tříd.
	 * 
	 * @param size
	 *            - velikosti písma pro hranu, která reprezentuje vztah asociace v
	 *            diagramu tříd.
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd.
	 */
	void quickSetupOfAssociationEdgeFontSize(final int size, final GraphClass classDiagram);

	
	
	
	/**
	 * Metoda, která slouží pro nastavení velikosti písma / fontu pro hranu, která v
	 * diagramu tříd reprezentuje vztahy typu implementace rozhraní.
	 * 
	 * @param size
	 *            - nová velikost písma / fontu pro hranu, která v diagramu tříd
	 *            reprezentuje vztah typu implementace rozhraní.
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd
	 */
	void quickSetupOfImplementsEdgeFontSize(final int size, final GraphClass classDiagram);

	
	
	
	/**
	 * Metoda, která slouží pro nastavení velikosti písma / fontu pro hranu, která v
	 * diagramu tříd reprezentuje vztah typu dědičnost.
	 * 
	 * @param size
	 *            - nová velikost písma / fontu pro hranu, která v diagramu tříd
	 *            reprezentuje dědičnost.
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd.
	 */
	void quickSetupOfExtendsEdgeFontSize(final int size, final GraphClass classDiagram);

	
	
	
	/**
	 * Metoda, která slouží pro "rychlé" nastavení velikosti písma / fontu pro
	 * hranu, která reprezentuje vztah typu agregace 1 : 1 neboli vztah typu
	 * asymetrická asociace v diagramu tříd.
	 * 
	 * @param size
	 *            - nová velikost pro výše popsanou hranu typu agregace 1 : 1
	 *            (asymetrická asociace) v diagramu tříd.
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd.
	 */
	void quickSetupOfAggregation_1_1_EdgeFontSize(final int size, final GraphClass classDiagram);

	
	
	
	/**
	 * Metoda, která slouží pro nastavení velikosti písma / fontu pro hranu, která v
	 * diagramu tříd reprezentuje vztah typu agregace 1 : N.
	 * 
	 * @param size
	 *            - nová velikosti písma / fontu pro hranu, která v diagramu tříf
	 *            reprezentuje vztah typu agregace 1 : N.
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd.
	 */
	void quickSetupOfAggregation_1_N_EdgeFontSize(final int size, final GraphClass classDiagram);
}
