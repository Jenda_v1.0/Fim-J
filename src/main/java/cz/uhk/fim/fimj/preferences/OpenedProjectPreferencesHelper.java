package cz.uhk.fim.fimj.preferences;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Třída, která obsahuje metody pro práci s preferencemi v rámci manipulace s daty pro nedávno otevřené projekty.
 * <p>
 * Tzn. jsou zde metody pro načítání a ukládání otevřených projektů v aplikaci. Dále nějaká manipulace s nimi, například
 * aktualizace datumu, kontrola existence adresářů / projektů, přípdné odstranění, vymazání otevřených projektů apod.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 22.07.2018 10:04
 */

public class OpenedProjectPreferencesHelper {

    /**
     * Index / Klíč, ke kterému se budou ukládat otevřené projekty. Dle tohodo klíče pak mohu načítat nebo ukládat pole
     * s otevřenými projekty do preferencí.
     */
    private static final String RECENLTY_OPENED_PROJECTS = "Recently opened projects";

    /**
     * Index / Klíč, který značí, zda byl klíč v RECENLTY_OPENED_PROJECTS vytvořen nebo ne. Jedná se o ukládání hodnoty
     * true do preferencí v případě, že byla v preferencích vytvořena položka pro RECENLTY_OPENED_PROJECTS, jinak
     * false.
     * <p>
     * Jde o to, že se ukládají objekty OpenedProject do preferencí jako jednorozměrné pole těchto objektů, ale když
     * chci nejprvenačíst z preferncí nějaký objekt, tak k tomu objektu přistupuji přes jeho klíč, ale pokud se v
     * preferencích nenachází hodnota s tímto klíčem, tak vypadne vždy nějaká výjimka, kterou není možné zachytit.
     * <p>
     * Proto ptřebuji tuto proměnnou RECENLTY_OPENED_PROJECTS_CREATED, která bude obsahovat hodnotu typu boolean (v
     * preferencích). Při každém spuštění aplikace se získá tato hodnota z preferencí a zjstí se, zda je true nebo ne.
     * Pokud je true, pak byla již v preferencích vytvořena položka pro pole otevřených objektů, pokud je false, pak
     * musím do preferencí uložit alespoň prázdné pole. Jde o to, že se v preferencích musí alespoň ta položka s
     * příslušným klíčem nacházet, na hodnotě ke klíči nezáleží.
     */
    private static final String RECENLTY_OPENED_PROJECTS_CREATED = "Recently opened projects created";


    private OpenedProjectPreferencesHelper() {
    }


    /**
     * Metoda se zavolá například při otevření projektu v aplikaci.
     * <p>
     * Otestuje, otevřená projekt (project), zda se již nachází v preferencích. Pokud ano, aktualizuje mu datum otevření
     * na aktuální datum. Pokud se v preferncích dosud nenachází, přidá se do nich.
     *
     * @param project
     *         - otevřený projekt v aplikaci (cesta k němu).
     */
    public static void checkOpenedProject(final File project) {
        final OpenedProject[] openedProjects = getOpenedProjectsFromPreferences();

        // Nemělo by nikdy nastat:
        if (openedProjects == null)
            return;

        final OpenedProject recentlyOpenedProject = getOpenedProjectByFile(project, openedProjects);

        if (recentlyOpenedProject != null) {
            /*
             * Zde byl nalezen projekt, který se již nachází v preferencích, takže byl již dříve otevřen. V takovém
             * případě pouze aktualizuji datum k příslušnému otevřené projektu
             * na aktuální datum a uložím zpět do preferencí.
             */

            // Aktualizuji datum na aktuální, abych mohl později správně řadit dle historie otevření projektu:
            recentlyOpenedProject.setDate(new Date());

            // Uložím změny zpět do preferencí:
            setOpenedProjectToPreferences(openedProjects);
        }

        else {
            /*
             * Zde je recentlyOpenedProject null, což znamená, že projekt nebyl nalezen v uložených dříve otevřených
             * projektech v preferencích.
             *
             * Takže projekt nebyl dosud otevřen v aplikaci (nebo byly vymazány - jiná záležitost).
             *
             * V takovém případě je třeba vytvořit novou položku (nový otevřený projekt) a uložit jej do preferencí.
             */

            // Převedu získané otevřené projekty do listu přo přidání nové položky:
            final List<OpenedProject> openedProjectList = new ArrayList<>(Arrays.asList(openedProjects));

            // Přidání nově otevřeného projektu:
            openedProjectList.add(new OpenedProject(new Date(), project));

            // Uložím změny (přidaný otevřený projekt) do preferencí:
            setOpenedProjectToPreferences(openedProjectList.toArray(new OpenedProject[]{}));
        }
    }


    /**
     * Získání projektu s cestou v project.
     * <p>
     * Jde o to, že se projdou veškeré projekty z preferencí, ty se projdou a hledá se ten, který obsahuje cestu k
     * projektu stejnou jako je v parametru project.
     *
     * @param project
     *         - cesta k projektu, o kterém se má zjistit, zda se nachází v openedProjects, pokud ano, tak se tent
     *         OpeneProject vrátí.
     * @param openedProjects
     *         - načtené projekty z preferencí, které se prohledají za účelem nalzení projektu se stejnou cestou jako je
     *         v project.
     *
     * @return OpenedProject z pole openedProjects, který obsahuje stejnou cestu k projektu jako je v project, jinak
     * null.
     */
    private static OpenedProject getOpenedProjectByFile(final File project, final OpenedProject[] openedProjects) {
        for (final OpenedProject openedProject : openedProjects)
            if (openedProject.getFile().equals(project))
                return openedProject;

        return null;
    }


    /**
     * Metoda pro získání veškerých otevřených projektů uložených v preferencích.
     * <p>
     * Princip je takový, že se načtou veškeré otevřené projekty z preferencí a odebere se aktuálně otevřený projekt,
     * tedy openedProject.
     *
     * @param openedProject
     *         aktuálně otevřený projekt nebo null v případě, že žádný projekt není aktuálně otevřen v aplikaci.
     *
     * @return jednorozměrné pole dříve otevřených projektů načtených z preferencí. Případně vyfiltrovány o
     * openedProject (pokud není null).
     */
    public static OpenedProject[] getOpenedProjectsExceptOpenedProject(final File openedProject) {
        final OpenedProject[] openedProjects = getOpenedProjectsFromPreferences();

        // Nikdy by nemělo nastat:
        if (openedProjects == null)
            return new OpenedProject[]{};

        /*
         * Pokud je openedProject null, tzn. že není otevřen žádný projekt v aplikaci, tak mohu rovnou vrátit veškeré
         * získané projekty z preferencí, protože není třeba žádný projekt odebrat.
         */
        if (openedProject == null)
            return openedProjects;

        final List<OpenedProject> openedProjectList = new ArrayList<>(Arrays.asList(openedProjects));

        openedProjectList.removeIf(op -> op.getFile().equals(openedProject));

        return openedProjectList.toArray(new OpenedProject[]{});
    }


    /**
     * Získání jednorozměrného pole z preferencí. Pole obsahuje seznam všech otevřených projektů v aplikaci.
     *
     * @return jednorozměrné pole otevřených projektů v aplikaci.
     */
    private static OpenedProject[] getOpenedProjectsFromPreferences() {
        return (OpenedProject[]) PreferencesApi.getPreferencesObject(RECENLTY_OPENED_PROJECTS);
    }


    /**
     * Uložení pole otevřených projektů v aplikaci do preferencí.
     * <p>
     * (Před uložením se celé pole projde a odstraní se ty projekty, které již neexistují.)
     *
     * @param openedProjectToPreferences
     *         - Jednorozměrné pole obsahující veškeré otevřené projekty v aplikaci.
     */
    private static void setOpenedProjectToPreferences(final OpenedProject[] openedProjectToPreferences) {
        final OpenedProject[] openedProjectsOnlyExisting = checkExistenceOfProjects(openedProjectToPreferences);

        PreferencesApi.setPreferencesObject(RECENLTY_OPENED_PROJECTS, openedProjectsOnlyExisting);
    }

    /**
     * Metoda, která projde předané dříve otevřené projekty v aplikaci (openedProjects) a pro každý otestuje, zda ještě
     * existuje. Poukud nějaký projekt již neexistuje, odstraní se.
     * <p>
     * Na konec se vrátí pole s dříve otevřenými projekty, ale vyfiltrovanými o ty projekty, které již neexistují.
     *
     * @param openedProjects
     *         - pole s dříve otevřenými projekty, ze kterého se mají odebrat ty projekty, které již neexistují.
     *
     * @return pole s predanými dříve otevřenými projekty (openedProjects), ale už se v tomto poli nebudou nacházet již
     * neexistující projekty.
     */
    private static OpenedProject[] checkExistenceOfProjects(final OpenedProject[] openedProjects) {
        final List<OpenedProject> openedProjectList = new ArrayList<>(Arrays.asList(openedProjects));

        openedProjectList.removeIf(openedProject -> !openedProject.existProject());

        return openedProjectList.toArray(new OpenedProject[]{});
    }


    /**
     * Nastavení prázdného pole do preferencí. Prázdné pole reprezentuje, že nebyly otevřeny žádné projekty v aplikaci.
     * Nebo se mají vymazat. Popř. se jedná o vložení "výchozí" hodnoty do preferencí v případě, že se v preferencích
     * dosud nenachází tato hodnoty pro otevřené projekty, tak aby se k nim mohlo přistupovat, aby nenastala výjimka,
     * když by se v preferencích nenacházel objekt, ke kterému se přistupuje.
     */
    public static void setEmptyOpenedProjectsToPreferences() {
        PreferencesApi.setPreferencesObject(RECENLTY_OPENED_PROJECTS, new OpenedProject[]{});
    }


    /**
     * Zjištění, zda byla vytvořena položka pro otevřené projekty v preferencích.
     * <p>
     * To se zjistí tak, že se načte hodnota pro RECENLTY_OPENED_PROJECTS_CREATED z preferencí.
     * <p>
     * (Note: bylo by možné využít metodu putBoolean v preferencích, ale metody by měla pouze toto jedno využití, proto
     * ponechán String)
     *
     * @return Pokud výše zmíněná získaná hodnota bude true, pak je to v pořádku, do preferencí bylo již vytvořeno
     * alespoň prázdné pole pro projekty. Jde o to, že se v preferencích již hodnota nachází, tak je možné jej načíst.
     * Pokud se vrátí false, pak dosud nebyla vytvořena položka pro otevřené projekty v preferencích. Je třeba jej
     * vytvořit.
     */
    public static boolean warOpenedProjectsCreatedInPreferences() {
        final String created = PreferencesApi.getPreferencesValue(RECENLTY_OPENED_PROJECTS_CREATED, null);

        return Boolean.parseBoolean(created);
    }

    /**
     * Vložení logické hodnoty "true" do preferencí k indexu / klíči: RECENLTY_OPENED_PROJECTS_CREATED.
     * <p>
     * Jedná se o to, že je třeba si nějak "hlídat", zda se v preferencích již nachází pole s otevřenými projekty, pokud
     * ne, je třeba vytvořit alespoň prázdé pole (uložit do preferncí), jinak by nastala chyba.
     * <p>
     * Tato metoda nastaví výše zmněnou hodnotu v preferencích na "true", což značí, že se někde již do preferencí
     * zapsalo pole s otevřenými projekty (alespoň výchozí), proto se k těmto objektům (otevřeným projektům) v
     * preferencích může přistupovat.
     */
    public static void setOpenedProjectsCreatedToPreferences() {
        PreferencesApi.setPreferencesValue(RECENLTY_OPENED_PROJECTS_CREATED, "true");
    }
}
