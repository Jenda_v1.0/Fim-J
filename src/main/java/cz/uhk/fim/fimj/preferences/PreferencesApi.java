package cz.uhk.fim.fimj.preferences;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;

/**
 * Jelikož nelze ukládat, resp. zapisovat data do souborů, které jsou ve vygenerovaném projektu coby soubor .jar, tak
 * musím použt Preferences API v této třídě, aby si aplikace mohla pamatovat potřená data, v tomto případě si aplikace
 * musí pamatovat, akorát následující parametry:
 * <p>
 * Cestu k otevřenému projektu, cestku k adresáři coby workspace zvolený uživatelem a cestu k adresáři, kde se nachází
 * soubory potřebné pro kompilaci tříd (označováno jako libTools adresář). Dále oevřené soubory v průzkumníku projektů a
 * potřebné hodnoty pro "funkci" nedávno otevřené projekty.
 * <p>
 * Takže stručně řešeno, tato třída používá Preferences API pouze za účelem pamatování si některých potřebných hodnot,
 * které jsou potřebna si držet i po ukončení aplikace.
 * <p>
 * <p>
 * Zdroj: http://www.ibm.com/developerworks/library/j-prefapi/
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class PreferencesApi {

    /**
     * Proměnná slouží pro definici názvu proměnné pro preferenci, kde bude uložena cesta k adresáři označený jako
     * Workspace, který zvolí uživatel.
     */
    public static final String PATH_TO_WORKSPACE = "Path to Workspace";

    /**
     * Proměnná, která drží název proměnné, ve které bude ulolžena cesta k otevřenému projektu v aplikaci.
     */
    public static final String OPEN_PROJECT = "Open project";

    /**
     * Proměnná, ve které bude uložen název proměnné, která slouží pro uchování cesty k adresáři, ve kterém budou
     * soubory potřebné pro kompilaci tříd.
     */
    public static final String COMPILER_DIR = "Compiler dir";



    /**
     * Proměnná, ve které je uložen název proměnné, která slouží pro uchování cesty ke spouštěcímu skriptu (soubor s
     * příponou .sh). Cesta k tomuto souboru se uložit pouze v případě, že aplikace běží na OS Linux, uživatel zkusí
     * restartovat aplikaci, vytvoří se příslušný skript s příkazem pro spuštění aplikace, ale ten skript / soubor se
     * nepodaří smazat. V takovém případě se po spuštění aplikace spustí vlákno, které otstuje, zda byl skript smazán,
     * případně jej zkusí smazat.
     */
    public static final String LAUNCH_SCRIPT = "Launch script file";




    /**
     * Tato proměnná slouží pro uložení do preferencí jednorozměrné pole a toto pole obsahuje soubory, které jsou
     * otevřeny v dialogu coby Průzkumník projektů.
     * <p>
     * Pokaždé, když se tento dialog zavře, tak se do preferencí uloží otevřené soubory, resp. otevřená okna (pokud
     * existují příslušné soubory) a při opetovném otevření tohoto dialogu s kořenovým adresářem ve Workspace se z této
     * hodnoty v Preferencích vezme toto jednorozměrné pole a prohelají se příslušné soubory a pokud existují, tak se
     * znovu otevřou v tomto dialogu.
     */
    public static final String OPEN_FILES_ARRAY = "Open files array";




    /**
     * Proměnná v podstatě definuje Preferences, přes které je možné přistupovat k výše zmíněným hodnotám.
     */
    private static Preferences preferences;


    private PreferencesApi() {
    }


    /**
     * Metoda slouží pro získání hodnoty nějaké proměnné z Preferences, kde jsou uloženy hodnoty i po zavření aplikace.
     * <p>
     * Metoda, která získá hodnotu z proměnné variable, pokud proměnná nebude nalezena, vrátí se hodnota defaultValue.
     *
     * @param variable
     *         - název proměnné, ze které se má získat hodnota.
     * @param defaultValue
     *         - hodnota, která bude vrácena, pokud nebude příslušná proměnná nalezena
     * @return buď hodnotu z proměnné - poukd bude daná proměnná nalezena, jinak pokud nebude název proměnné nalezen,
     * tak se vrátí zadaná výchozí hodnota - defaultValue
     */
    public static String getPreferencesValue(final String variable, final String defaultValue) {
        // pomocí metody userNodeForPackage , která vrátí uzel, který je specifický pro danou aplikaci, resp.
        // pro tuto aplikaci, je to potřebna místo metody userRoot, protože, kdybych použil tuto metodu userRoot
        // tak by mohlo dojít ke kolizi s proměnými, například kdyby dva uživatele uložili různé hodnoty,
        // do stejné proměnné, tak by aplikac enevěděla, kterou hodnotu má vrátit, tak díky použité metodě
        // userNodeForPackage
        // se vytvoří uzel, který je jak jsem již zmínil specifický pro tuto aplikaci, resp. konkrétně pro tuto třídu.

        preferences = Preferences.userNodeForPackage(PreferencesApi.class);

        return preferences.get(variable, defaultValue);
    }


    /**
     * Metoda, která nastaví hodnotu nějakké proměnné pomocí Preferences API, tak aby bylo možné pro tuto aplikaci si
     * tuto hodnotu opět získat i po zavření a opětovnému otevření této aplikace.
     * <p>
     * Metoda, která nastaví proměnnou variable na hodnotu value.
     *
     * @param variable
     *         - název proměnné, která se má nastavit na hodnotu value
     * @param value
     *         - hodnota, která se má "vložit" do proměnné variable
     */
    public static void setPreferencesValue(final String variable, final String value) {
        // Zde může být stejný komentář - poznámka jako výše, ohledně toho, že budou hodnoty
        // pomocí preferences přístupně v rámci této třídy, více informací ohledně funkce Preferences API,
        // lze získat například na uvedeném zdroji.
        preferences = Preferences.userNodeForPackage(PreferencesApi.class);

        preferences.put(variable, value);
    }


    /**
     * Metoda, která nastaví hodntu proměnné variable v Preferenes API na hodnotu value. Do této proměnné se ukládají
     * hodnoty potřebné pro otevření souborů v průzkumníku projektů
     *
     * @param variable
     *         = název proměnné, pod kterou lze k hodnotě přistupovat
     * @param value
     *         = hodnota, která se má vložit do proměnné variable v Preferences API
     */
    public static void setPreferencesObject(final String variable, final Object value) {
        preferences = Preferences.userNodeForPackage(PreferencesApi.class);

        try {
            // Pomocí třídy PrefObj lze vkládat do preferencí i objekty, tak vložím příslušný objekt
            // do preferencí pro pozdější načtení:
            PrefObj.putObject(preferences, variable, value);

        } catch (IOException | BackingStoreException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při vkládání objektu do Preferences, pro opětovné načtení, třída " +
                                "MyPreferencesApi, metoda: setPreferencesObject.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
    }


    /**
     * Metoda, která získá objekt z preferencí pod proměnou variable a vrátí ho
     *
     * @param variable
     *         - název proměnné, která se vyskytuje v Preferences API a obsahuje nějakou hodnotu
     * @return objekt z Preferences API získaný pod proměnnou variable
     */
    public static Object getPreferencesObject(final String variable) {
        preferences = Preferences.userNodeForPackage(PreferencesApi.class);

        try {

            return PrefObj.getObject(preferences, variable);

        } catch (ClassNotFoundException | IOException | BackingStoreException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při získávání objektu z Preferences, třída MyPreferencesApi, metoda: " +
                                "getPreferencesObject.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }

        return null;
    }
}