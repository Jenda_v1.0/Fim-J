package cz.uhk.fim.fimj.preferences;

import java.io.*;
import java.util.prefs.*;


/**
 * Zdroj této třídy, resp. tato třída:
 * <p>
 * https://sourceforge.net/p/j-po/git/ci/112cc8d99196da57eb02ffc69264412f8e33e118/tree/src/java/jpo/dataModel/PrefObj
 * .java#l3
 * <p>
 * Info jsem načel zde: http://www.ibm.com/developerworks/library/j-prefapi/
 * <p>
 * <p>
 * Tato třída slouží pro to, aby šlo pmocí preferencí ukládat kroměně definovaných typů (String, boolean, float, ...)
 * také objekty, v mém případě se jedná pouze o to, aby šli do těchto preferencí ukládat i datové struktury, opět v mém
 * případě se jedá pouze o jednorozměrné pole, které obsahuje soubory, které jsou, resp. byly otevřeny v průzkumníku
 * projektů, a nedávno otevřené projekty.
 * <p>
 * Tuto třídu jsem použil například, abych mohl ukládat do preferencí jednorozměrné pole typu
 * InfoForSaveFileToPreference (mimo jiné), které obsahuje cesty k souborům (mimo jiné), které jsou otevřeny, nebo byly
 * otevřeny v průzkumníku projektů - dialogu, když byl zavřen. chtěl jsem zmíněný dialog - Průzkumník projektů doplnit
 * podobně jako Eclipse umí při otevření znovu otevřít třídy, které byly otevřeny, tak tento stejný princip, akorát jde
 * to pouze pro Průzkumník projektů, když je otevřen adresář Workspace, rozlišovat jednotlivé projekty už by bylo trochu
 * řekl bych zbytečné a v momenté, kdy by se nějaký smazal by bylo třeba smazat, i příslušnou reference v preferencích,
 * která by měla na starosti příslušný projekt, ale bylo by potřeba hlídat si i smazání / existenci projektů. Například
 * při spuštění aplikace nebo při otevření dialogu průzkumnik projektů by bylo potřeba oveřit zda veškeré projekty ještě
 * existují, případně je smazat.
 * <p>
 * Proto si uložítm pomocí těchto PrefObj cesty k souborům, které byly otevřeny v Průzkumníku projektů, a pokaždé, když
 * se tento průzkumník s kořenovým adresářem coby Workspace otevře, tak se zkusí znovu otevřít soubory, které již byly
 * otevřeny (pokud ještě existují)
 *
 * @author Jan Krunčík
 * @version 1.0
 */

class PrefObj {
    // Max byte count is 3/4 max string length (see Preferences
    // documentation).
    private static final int PIECE_LENGTH = ((3 * Preferences.MAX_VALUE_LENGTH) / 4);

    private PrefObj() {
    }

    private static byte[] object2Bytes(final Object o) throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(o);

        return baos.toByteArray();
    }

    private static byte[][] breakIntoPieces(final byte[] raw) {
        final int numPieces = (raw.length + PIECE_LENGTH - 1) / PIECE_LENGTH;
        final byte[][] pieces = new byte[numPieces][];

        for (int i = 0; i < numPieces; ++i) {
            final int startByte = i * PIECE_LENGTH;
            int endByte = startByte + PIECE_LENGTH;
            if (endByte > raw.length)
                endByte = raw.length;
            final int length = endByte - startByte;
            pieces[i] = new byte[length];
            System.arraycopy(raw, startByte, pieces[i], 0, length);
        }

        return pieces;
    }


    private static void writePieces(final Preferences prefs, final String key, final byte[][] pieces)
            throws BackingStoreException {
        final Preferences node = prefs.node(key);
        node.clear();

        for (int i = 0; i < pieces.length; ++i)
            node.putByteArray("" + i, pieces[i]);
    }


    private static byte[][] readPieces(final Preferences prefs, final String key) throws BackingStoreException {
        final Preferences node = prefs.node(key);
        final String[] keys = node.keys();
        final int numPieces = keys.length;
        final byte[][] pieces = new byte[numPieces][];

        for (int i = 0; i < numPieces; ++i)
            pieces[i] = node.getByteArray("" + i, null);

        return pieces;
    }

    private static byte[] combinePieces(final byte[][] pieces) {
        int length = 0;

        for (final byte[] piece : pieces) length += piece.length;

        final byte[] raw = new byte[length];
        int cursor = 0;

        for (final byte[] piece : pieces) {
            System.arraycopy(piece, 0, raw, cursor, piece.length);
            cursor += piece.length;
        }

        return raw;
    }

    private static Object bytes2Object(final byte[] raw) throws IOException, ClassNotFoundException {
        final ByteArrayInputStream bais = new ByteArrayInputStream(raw);
        final ObjectInputStream ois = new ObjectInputStream(bais);

        return ois.readObject();
    }


    static void putObject(final Preferences prefs, final String key, final Object o)
            throws IOException, BackingStoreException {
        final byte[] raw = object2Bytes(o);
        final byte[][] pieces = breakIntoPieces(raw);

        writePieces(prefs, key, pieces);
    }


    static Object getObject(final Preferences prefs, final String key)
            throws IOException, BackingStoreException, ClassNotFoundException {
        final byte[][] pieces = readPieces(prefs, key);
        final byte[] raw = combinePieces(pieces);
        return bytes2Object(raw);
    }
}