package cz.uhk.fim.fimj.preferences;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Třída slouží pro reprezentaci "otevřeného projektu" pro vkládání do preferencí.
 * <p>
 * Jde o to, že se v menu nachází položka, kde se zobrazí veškeré otevřené projekty, a ty se řadí dle datumu a času, kdy
 * byly otevřeny, dále je k tomu potřeba přidat cestu k adresáři, resp. k projektu, který byl otevřen.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 22.07.2018 9:54
 */

public class OpenedProject implements Serializable {

    private static final long serialVersionUID = 1699621381139060827L;

    /**
     * Formát data a času, který se aplikuje pro toolbar nad konkrétním dříve otevřeným projektem.
     * <p>
     * <p>
     * (Note: když byla tato proměnná jako instancční proměnná - ne statická, vždy byla null ?)
     */
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");

    /**
     * Datum (a čas), kdy byl projekt otevřen v aplikaci. Tato položka se aktuailzuji pokaždé, když uživatel otevře
     * projekt.
     * <p>
     * Když uživatel otevře například projekt "A", pak projekt "B", oba projekty zde budou mít nějaký čas, čas "B" bude
     * "aktálnější". Pokud ale uživatel otevře znovu projekt "A", pak se zde aktuaiuji tento datum a čas na aktuální
     * hodnotu, aby bylo možné řadit otevřené projekty dle toho, kdy byly otevřeny - historie.
     */
    private Date date;

    /**
     * Cesta k adresáři / projektu, který byl otevřen v aplikaci.
     */
    private final File file;

    /**
     * Konstruktor třídy.
     *
     * @param date
     *         - datum (a čas), kdy byly projekt otevřen v aplikaci.
     * @param file
     *         - cesta k adresáři projektu, který byl v aplikaci otevřen.
     */
    OpenedProject(final Date date, final File file) {
        super();

        this.date = date;
        this.file = file;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public File getFile() {
        return file;
    }

    public boolean existProject() {
        return file.exists();
    }

    public String getPath() {
        return file.getAbsolutePath();
    }

    public String getFileName() {
        return file.getName();
    }

    /**
     * Metoda, která vrátí text pro tooltip, který se zobrazí nad položkou v menu (Projekt -> Nedávné projekty ->
     * projekt)
     *
     * @return - text, který slouží jako tooltip nad konkrétní položkou ve výše zmíněném menu nad konkrétním dříve
     * otevřeným projektem.
     */
    public String getTextForTooltip() {
        return "(" + SIMPLE_DATE_FORMAT.format(date) + ") - " + file.getAbsolutePath();
    }
}
