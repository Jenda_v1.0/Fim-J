package cz.uhk.fim.fimj.instances;

import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.file.*;
import cz.uhk.fim.fimj.instance_diagram.CheckRelationShipsBetweenInstances;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;
import cz.uhk.fim.fimj.swing_worker.ThreadSwingWorker;

/**
 * Tato třída slouží pouze pro deklaraci metod, které přenačtou již vytvořené instance v HashMapě (instance tříd z
 * diagramu tříd).
 * <p>
 * Postup je uveden u příslušné metody. Ale jedná se akorát o to, že se vytvoří nové instance stejné třídy, vloží se do
 * nich stejné hodnoty proměnných, akorát se přenačtou instance za nové a tyto nové instance se vloží zpět do HashMapy s
 * instancemi a přenačte se diagram instancí
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class RefreshInstances {

	private RefreshInstances() {
	}


	/**
	 * Komponenta,která slouží pouze pro vytvoření instanci pro aktualizaci hodnoty,
	 * ale pouze zavoláním výchozího konstruktoru bez parametr a v případě, že by
	 * náhodou došlo k nějaké chybě (nemělo by), tak se v tomto případě stejně
	 * nebude nic vypisovat apod. Proto nepotřebui předávat žádné hodnoty jako jsou
	 * texty a reference na editor výstupů apod.
	 * 
	 * Při této aktualizaci intancí a jejich hodnot stejně nepořetbuji nic
	 * vypisovat, navíc by nemělo být co, vše už by mělo být v rámci možností v
	 * pořádku.
	 */
    private static final OperationsWithInstances OPERATIONS_WITH_INSTANCE = new OperationsWithInstances(null, null, null);
	
	
	

	
	/**
	 * Proměnná, která slouží vždy pro "dočasné" uložení velikosti pole.
	 * 
	 * Tato proměnná je potřeba, protože, když se zjišťuje velikost největší dimenze
	 * v nějakém poli, tak se to pole prochází pomocí rekurze, tak abych si vždy
	 * nějak "pamatoval" tu nějvětší velikost, budu si jí "odkládvat" do této
	 * proměnné, díky tomu vždy budu mít tu největši velikost.
	 */
	private static int longestArray;
	
	
	

	
	
	
	
	/**
	 * Tato metoda přenačte všechny instance tříd z diagramu tříd, které uživatel vytvořil a jejich reprezentace
	 * se nacházejí v diagramu instancí.
	 * 
	 * Tato metoda je potřeba z toho důvodu, protože si ClassLoader drží veškeré třídy (soubory .class), které načetl
	 * takže pokud se pomocí tohoto loaderu načte třída, tak lze tak učinit pouze jednou, pokud se tato načtená třída 
	 * jakkoliv upraví, znovu zkompiluje a má se načíst pomocí tohoto ClassLoaderu, tak se načte pouze ta původní, resp. 
	 * nenačte, protože si je ClassLoader drží v paměti, takže je spíše "vrátí", což je jeden problém, ale ten další je ten,
	 * že pro vyřešení toho předchozí, je možné vyřešit to tak, že se pokaždé pro načtení třídy použíje nový ClassLoader,
	 * ale pokud se to tak udělá, tak se mezi vytvořenými instancemi nepůjde předávat reference na instance tříd, protože to
	 * vždy vyhodí vyjímku illegalArgument exteption - Argument type Mismathch, i když je vše správně, 
	 * protoo jsem napsal tuto metodu, která pokaždé, když se třídy zkompilují, tak se vytvoří nový ClassLoader,
	 * a přenačtou se všecny instance, resp načtou se nově zkompilované a vzniklé soubory .class a ty třídy, 
	 * ze kterých byly vytvořeny instance se znovu vytvoří a naplní se i jejich proměnné, tak jako původní s tím, že se 
	 * nahradí jejich reference na instance
	 * 
	 * 
	 * (z časově náročných důvodů jsou testované jsou pouze proměnné typu pole, list a "klasické" proměnné, ne mapy apod.)
	 * 
	 * 
	 * @param pathToBinDir = cesta k adresáři bin, v aktuálně otevřeném projektu, mělo by se otestovat, že již existuje pří
	 * předávání cesty do teéto metody, metoda s tím již počítá
	 * 
	 * @param outputEditor = reference na editor výstupů, pro předání do metody pro načtení přeložené - zkompilované třídy,
	 * aby o tom uživatel věděl, že například daná třída neexistuje v adresáři bin, apod. ale o zbytku chyb - které by mohly ale 
	 * neměly nastat již uživatele informovat není třeba, protože by se tak dělo při každé kompilaci
	 * 
	 * @param languageProperties = soubor s texty ve zvoleném jazyce
	 * 
	 * @param instanceDiagram = reference na diagram instancí, abych ho mohl předat vláknu, které se postará o aktualizaci diagramu
	 * instancí ohledě vazeb mezi instancemi.
	 * 
	 * @param refreshInstanceDiagram - logická proměnná o tom, zda se mají přenačíst vztahy mezi instnacemi v diagramu instancí.
	 * 
	 * 
	 * 
	 * 
	 * Postup:
	 *- vytvoreni noveho toho meho puvodniho classloaderu (nove instance)
	 *
	 *
	- vytvorim si novou hashmapu, do které si vlozim vsechny existujici instance,

 	-vytvorm si kolekci nejakych objektu InfoAboutInstances a tyto objekty budou obsahovat referenci a původní instanci
	a novou instanci - ta co ma nahradit tu puvodní

	Cyklus 1:
	- pak projdu tu hashmapu nactenych instanci a pokazde si nactu tu instanci a:
		- zjistím si balicek a nazev tridy, tuto tridu (ted uz nove prelozenou) si nactu pomoci noveho ClassLoaderu
		- vytvorim si jeji instanci
		- a obe tyto instance vlozim do kolekce objeku po puvodni a novou instanci a k tomu i nazev reference na instanci

	Cyklus 2:
	- projdu kolekci s nove vytvorenymi instancemi
	- v kazde iteraci si vezmu puvodni instanci, nactu si vsechny polozky a ty iteruji:
		- vzdy si nactu hodnotu a otestuji, zda to neni pole nebo kolekce
		- pokud je to klasicka promenna, tak si vezmu jeji hodnotu a otestuji, zda neni null, pokud ne,
			tak otestuji, zda je to instance, ktera je obsazena v teto kolekci jako puvodni instance, pokud
			ano, bude nahrazena za tu novou (pod polozkou pro nove vytvorenou instanci)
			pokud to neni instance, tak ta hodnota co byla z promenne nactena, bude vlozena do te v nove instanci

		- pro pole nebo kolekce - ty opet proiteruji a otestuji to same jako vyse - akorat pro kazdou polozku pole ci kolekce

	- toto je treba udelat v dalsim cyklu protoze, kdybych to udelal v tom prvnim, tak nektere instance jeste nemuseli byt 
		znovu vytvoreny a byly by tam porad ty puvodni - coz je spatne


	Cyklus 3:
	zde akorat projdu tuto novou kolekci, ted uz i s predanymi hodnotami, tak ji proiteruji a dle nazvu reference z objektů
	v kolekci nahradim touto novou instanci v objektech v kolekci tu v HashMape, kde jsou ulozeny veskere instance,
	ktere uzivatel vytvoril.

	- jeste je dobre prenacist graf, ale pouze pro pripad, ze v nove instanci jiz napriklad chybi nejaka promenna, apod.
	- jinak by to ani nebylo treba
	
				
	 */
	static void refreshClassInstances(final String pathToBinDir, final OutputEditorInterface outputEditor,
			final Properties languageProperties, final GraphInstance instanceDiagram,
			final boolean refreshInstanceDiagram) {
		
		/*
		 * Vytvořím si nový ClassLoader, pro načítání všech tříd, dokud se zase
		 * nezkompilují
		 */
		CompiledClassLoader.createClassLoader(pathToBinDir, outputEditor, languageProperties);
		
		
		/*
		 * Načtu si všechny již uživatelem vytvořené instance tříd, které se nacházejí v
		 * diagramu tříd:
		 */
		final Map<String, Object> classInstancesMap = Instances.getAllInstancesOfClassInCd();
		
		
		
		/*
		 * Vytvořím si kolekci objektů, do které vložím informace o referenci, původní
		 * instanci a nové instanci - nahrazení původní instance, za novou, aby
		 * fungovalo předávání referencí na instance v diagramu instancí a vůbec v
		 * Aplikaci
		 */
		final List<InfoAboutInstances> infoAboutInstancesList = new ArrayList<>();
		
		
		
		
		
		
		// Cyklus 1 - info výše v komentáři metody:
		for (final Map.Entry<String, Object> map : classInstancesMap.entrySet()) {
			final Object objOldInstance = map.getValue();
			
			final String packageAndClassName;
			
			// Otestuji, zda se instancce nachází v nějakém balíčku (měla by vždy, takže to ani není třeba testovat,
			// ale opravdu kdyby náhodou nebyl - nemělo by nastat, tak by vyskočilo vyjímka NullPointerException)
            if (objOldInstance.getClass().getPackage() != null)
                packageAndClassName = objOldInstance.getClass().getName();

                // Zde se tedy nenachází v balíčku, tak vložím pouze název třídy (nemělo by
                // nastat, třída by vždy měla být v nějakém balíčku):
            else
                packageAndClassName = objOldInstance.getClass().getSimpleName();
			
			
			// Načtu si novou nově zkompilovanou třídu:
			final Class<?> clazz = App.READ_FILE.loadCompiledClass(packageAndClassName, outputEditor);
			
			
			// Otestuji, zda se ji podařilo načíst:
			if (clazz != null) {
				// zde se tedy podařilo načíst nově zkompilovanou verzi nějaké třídy, tak vytvořím její instanci,
				// až na to, ze je to třba udělat zde a ne poomcí metod v třídě OperationWithInstances, protože by se do diagramu
				// instancí přidala další reprezentaci této instance, což nechci, takže budu muset zde napsat novou metodu pro vytvoření,
				// abych si kód zde moc nezpřehledňoval
				final Object newInstance = createInstance(clazz, languageProperties, outputEditor);
				
				if (newInstance != null)
					// Zde se podařilo vytvořit novou instanci, kterou se může nahradit ta původní, tak ji mohu
					// přidat do kolekce pro pozdější doplnění hodnot do proměnných a nahrazení jí původní instanci:
					infoAboutInstancesList.add(new InfoAboutInstances(objOldInstance, newInstance, map.getKey()));
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		// Cyklus 2, vysvětlení v komentáři metody:
		for (final InfoAboutInstances i : infoAboutInstancesList) {
			/*
			 * List, do kterého si "načtu" veškeré proměnné, které jsou deklarovány ve třídě
			 * i.getOriginalInstance().getClass() a které se nachází ve všech předcích
			 * příslušné třídy až po třídu Object - bez ní, z třídy Object už se proměnné
			 * neberou.
			 */
			final List<Field> allFieldsFromInstanceList = getAllField(i.getOriginalInstance().getClass(), new ArrayList<>());
			
			// Vezmu si všechny položky z původní instance a její hodnotu vložím do nové instance:
			
			// Včetně procházení proměnných v předích třídy: i.getOriginalInstance().getClass():
			for (final Field f : allFieldsFromInstanceList) {
				// Získám si hodnotu z právě testované proměnné z původní instance:
				final Object fieldValue = ReflectionHelper.getValueFromField(f, i.getOriginalInstance());
				
				
				// Zkusím si načíst položku z té nové instance, abych mohl otestovat, zda tam ta proměnné
				// ještě existuje:
				final Field fieldForInsert = getFieldFromInstance(i.getNewInstance(), f.getName());
				
				
				
				
				// Logická proměnná, do které vložím hodnotu o tom, zda se má pokračovat nebo ne, 
				// pokračuje se v případě, že se shodují datové typy proměnných a proměnná není null - toto by 
				// nastat nemělo
				boolean compare = false;
				
				// Následující podmínky jsem musel připsat, protože mi neprošly podmínky v syntaxi:
//				if (fieldForInsert != null && f.getType().equals(fieldForInsert.getType())) {
				
				// pokud se jednalo o "klasickou" proměnnou, ale u pole a kolekce to prošlo,
				// proto jsem porovnávání kolekce a polí nechal takto - v případě, že se jedná o proměnnou,
				// typu kolekce nebo pole:
				if (ReflectionHelper.isDataTypeOfList(f.getType()) /*|| f.getType().isArray()*/) {
					if (fieldForInsert != null && f.getType().equals(fieldForInsert.getType()))
						compare = true;
				}
				
				// a na tuto podmínku se dostane, pouze v případě, že se jedná o "klasickou" proměnnou,
				// a ta projde v případě, že se porovnávají datové typy proměmných, ale převedené do Stringu:
				else if (fieldForInsert != null && f.getType().toString().equals(fieldForInsert.getType().toString()))
					compare = true;
			
				
				
				
				
				
				// Otestuji, zda byla položka nalezena a shodují se datové typy:
				// datové typy se již nemusí shodovat pro novou instanci, takže by ani nemuselo být možné
				// je nahradit - jejich hodnoty
				
				
				// Nelze porovnat následující podmínkou: - viz výše
//				if (fieldForInsert != null && f.getType().equals(fieldForInsert.getType())) {
//				if (fieldForInsert != null && f.getType().toString().equals(fieldForInsert.getType().toString())) {
					
				
				// Zde otestuji, zda jsou podmínky výše splněný nebo ne:
				if (compare) {
					// Zde byla položka - proměnná nalezena i v té nové instanci třídy, tak otestuji, zda není hodnota
					// pro vložení null, pokud ano, tak vložím do té proměnné také null,
					// pokud ne, tak otestuji, zda se jedná o "klasickou" proměnnou, kolekci nebo pole:
					
					if (fieldValue != null) {
						// Otestuji, zda se jedná o nějakou kolekci:
						if (ReflectionHelper.isDataTypeOfList(f.getType())) {
							// Zde se jedná o nějakou kolekci, tak si ji načtu a musím jí celou projít
							// abych otestoval, zda se jedná o nějakou instanci, která se již nachází v 
							// kolekci pod proměnnou pro nahrazení a případně jí nahradit a pak tuto upravenou kolekci
							// vložit do stejné proměnné v nové instanci třídy
							
							// Note:
							// Zde nemá smysl zjišťvat typ kolekce a dle toho se rozhodnout, protože tento typ
							// bude bud Object, nebo typ nějaké třídy z diagramu tříd, ale kolekci s instancemi bych
							// musel projít stejně, kvůli otestování, zda existuje instance příslušné třídy,
							// takže by to byl spíše kód navíc a pokud by se zjistilo, že to tak je,
							// tak by se ta kolekce procházela znovu kvůli nahrazení instancí.
							
							@SuppressWarnings("unchecked")
							final List<Object> valuesFromField = (List<Object>) fieldValue;
							
							// Zde proiteruji celou získanou kolekci a otestuji, zda je příslušná položka v kolekci
							// instance, která se má nahradit, pkud ano, tak se v kolekci nahradí, pokud ne,
							// tak se nic nedělá
							for (int q = 0; q < valuesFromField.size(); q++) {
								// Otestuji, zda se má nahradit instance za novou:
								
								// Musím otestovat, zda není hodnota v poli null, aby nedošlo k chybě:
								final Object objvalueForTest = valuesFromField.get(q);
								
								if (objvalueForTest != null) {
									final Object objNewValue = isValueReferenceToOldInstance(objvalueForTest, infoAboutInstancesList);
									
									if (objNewValue != null)
										// zde se má nahradit získaná instance za novou, tak ji nahradím v kolekci:
										valuesFromField.set(q, objNewValue);
								}
							}
							
							// zde se v kolekci nahradily původní instance za nové (pokud bylo třeba) tak tuto kolekci vložím do té samé
							// proměnné v nové instanci:
							setFinalStaticVar(fieldForInsert, i.getNewInstance(), valuesFromField);
						}
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						// Zde otestuji, zda se jedná o pole:
						else if (f.getType().isArray()) {
							/*
							 * Dříve jsem zkoušel celé pole nejprve zkopírovat, a pak pouze nahradit prvky v
							 * tom poli, ale nikdy mi to nevyšlo, je možné, že jsem někde chybně postupoval,
							 * ale vždy jsem dostal buď nějakou chybu, nebo se to pole nenastavilo do
							 * příslušné proměnné v instanci třídy.
							 */
							
							
							/*
							 * Postup:
							 * - Tato část si zjistí, zda se jedná o jednorozměrné pole nebo více - rozměrné
							 * pole.
							 * 
							 * Pokud se jedá o jednorozměné pole, tak se pouze zjistí jeho délka, vytvoří se
							 * stejné velké pole stejného datového typu, pak se projdou veškeré hodnoty v
							 * tom poli (projde se celé pole) a vždy se načtu příslušná hodnota na
							 * příslušném indexu v poli. Pokud je to null, pak se do nového pole na stejný
							 * index vloží také null hodnota, pokud je to hodnota, která představuje původní
							 * instance, pak se nahradí za novou instanci a vloží se do nového pole na
							 * stejný index, jinak pokud se jedná o nějakou jinou hodnotu, pak se prostě
							 * vloží do nového pole na příslušný index.
							 * 
							 * 
							 * Pokud se jedná o více rozměrné pole:
							 * Zjistím si datový typ a velikost / délku nejdelší dimenze, protože kdybych
							 * bral například vždy velikost té prví dimenze a pak ta další byla větší,
							 * modhlo by dojít k chybě při vkládání hodnoty do prvku v poli, který již v
							 * poli není, resp. pole by nebylo tak velké (IndexOutOf...Ex).
							 * 
							 * Pak si vytvořím nové pole, které bude značit počet dimezí a toto pole naplním
							 * vždy tou nejveší hodnotu, která byla získána výše a značí velikost nejdelší
							 * dimenze. Nenapadl mě lepší způsob, hlavně při nedostatku času. Tak jsem to
							 * bohužel udělal takto, vždy nebo většinou se při atualizaci budou nebo mohou
							 * nacházet o nějaké prvky v poli více s výchozími hodnotami právě kvůli tomuto
							 * zjišťování, nepodařilo se mi najít způsob, při kterém by se dalo vytvořit
							 * vždy stejné pole se stejnými velikost dimenzí, je možné si zjistit velikost
							 * všech dimenzí, ale nepodařilo se mi přijít na způsob, jak vytvořit stejné
							 * pole pomocí reflexe, aby mělo naprosto stejné velikosti těch dimenzí.
							 * 
							 * Když už mám velikost a počet dimezí a datový typ, tak mohu vytvořit nové pole
							 * s těmito hodnotami a pak akorát projdu to původní, a každý prvek z toho
							 * původního vložím na stejný index v tom novém poli, akorát stačí otestovat,
							 * zda se jedná o původní instanci nějaké třídy, který byla nahrazena, pokud
							 * ano, tak vložit do nového pole tu novou instanci (nahradit jí tu původní
							 * instanci), jinak pro všechny ostatní prvky to neplatí, ty se prostě vezmou z
							 * původního pold z nějakého indexu a vloží se na stejný index v novém poli.
							 */

							/*
							 * Zjistím si počet dimenzí příslušného pole.
							 */
							final int countOfDims = getCountOfDimensionsOfArray(fieldValue);

							// Otestuji, zda se jedná má pole jednu dimenzi:
							if (countOfDims == 1) {
								/*
								 * Zjistím si velikost příslušné pole.
								 */
								final int lengthOfArray = Array.getLength(fieldValue);

								/*
								 * Vytořím si nové pole, které bude stejného datového typu a stejné délky.
								 */
								final Object oneDimArray = Array
										.newInstance(fieldForInsert.getType().getComponentType(), lengthOfArray);

								/*
								 * Zde si projdu původní pole, načtu si hodnotu, otestuji, zda je to původní
								 * instance, pokud ano, tak ji nahradím za novou. Pokud to není původní
								 * instance, pak už je mi jedno, jaká je to hodnota, null, číslo, jiný objekt,
								 * ... Prostě vložím tuto hodnotu na stejnou pozici v novém poli.
								 */
								for (int q = 0; q < lengthOfArray; q++) {
									/*
									 * Získám si objekt / hodnotu z pole:
									 */
									final Object value = Array.get(fieldValue, q);

									if (value != null) {
										/*
										 * Zjistím si, zda se jedná o původní instanci, pokud ano, pak se do této
										 * proměnné objNewValue vloží nová instance.
										 */
										final Object objNewValue = isValueReferenceToOldInstance(value,
												infoAboutInstancesList);

										// Otestuji, zda se jedná o původní instanci a byla nalezena nová:
										if (objNewValue != null)
											// Zde se nahradí původní instance za novou na stejném indexu v poli:
											Array.set(oneDimArray, q, objNewValue);
										
										// Zde se jedná o jakoukoli jinou hodnotu než původní instance nebo nebyla
										// nalezena, tak jako tak, tu hodnotu vložím do nového pole na stejný index.
										else {
											/*
											 * Zde nejprve potřebuji otestovat, zda je ta položka nějaká instance z
											 * diagramu tříd, která není znázorněna v diagramu instancí, pokud ano, pak
											 * z té třídy potřebji také vytvořit novou instanci a vložit jej na stejnou
											 * položku v poli, akorát novou instanci, ale už zde není dořešeno
											 * přenačtení hodnot i z té instance - možná v další verzi.
											 */
											if (isInstanceOfClassFromCd(value)) {
												/*
												 * Zde si vytvořím novou instanci té samé třídy, která se právě našla v
												 * poli, ale už se nepřenčtou její hodnoty, pouze se vytvoří nová
												 * instance zavolání výchozího konstruktoru.
												 */
												final Object refreshInstance = getNewInstanceOfSameClass(value);

												// Vložím je do pole:
												Array.set(oneDimArray, q, refreshInstance);
											}

											// Zde se jedná o jakoukoliv jinou hodnotu:
											else
												Array.set(oneDimArray, q, value);
										}
											
									}
									// Zde se jedná o null hodnotu, tak ji rostě vložím na stejný index, ale toto už
									// není povinné, pokud se jedná o null hodnotu, jedná se o pole objektového typu
									// a null hodnota tam již je jako výchozí.
									else
										Array.set(oneDimArray, q, null);
								}

								// Vložím nové vytvořené pole do stejné proměnné v nové instanci:
								setFinalStaticVar(fieldForInsert, i.getNewInstance(), oneDimArray);
							}
							
							
							
							// Zde má pole více dimenzí, tak je třeba jej projít trochu jinak:

							// Stačilo by pouze 'else'
							else if (countOfDims > 1) {
								/*
								 * Zjistím si datový typ toho pole, zde se jedná o X - dimezí (konkrétně o
								 * countOfDimes dimenzí), tak je musím všechny projít, abych zjistil datový typ
								 * toho pole, který semá vytvořit.
                                 */
                                final Class<?> dataTypeOfArray = ReflectionHelper.getDataTypeOfArray(fieldForInsert.getType());

								/*
								 * Zde si zjistím velikost toho nejvetšího pole. Toto je potřeba, abych náhodou
								 * nevytvořil třeba dvojrozměrné pole a nějaký "vnitřní" index byl třeba 3, ale
								 * nově vytvořené pole končilo u dvou (kdybych bral vždy velikost toho prvního
								 * pole).
								 * 
								 * Proto projdu úplně celé pole a najdu si to nejvetší, a na tuto velikost se
								 * vytvoří všechny "vnitřní pole".
								 */
								// Toto jsem původně zkoušel:
//								final int longestDimsInArray = findLonestArray(fieldValue, 0);

								/*
								 * Zde je ale potřeba nejprve zjistit délku toho "prvního" pole - celkově, kolik
								 * má prvků, proto, kdyby bylo třeba pole 2x3, pak by se nové pole vytvořilo jen
								 * 2x2 a to by bylo spatně, pak by přetekly indexy (indexBounds...), proto si
								 * zde zjistím délku toho prvního pole, a s tou budu porovnávat pak všechny
								 * "vnitřní" pole, pak se správně vytvoří i to nové pole.
								 *
								 * Níže se zjistí velikost té největší dimenze, počinaje úplně tou první - polem s
								 * dimenzemi samotnými, až po všechny vnitřní.
								 */
								longestArray = Array.getLength(fieldValue);
								final int longestDimsInArray = findLongestArray(fieldValue);
								
								/*
								 * Vytvořím si pole, které bude značit veškeré dimeze a jejich velikosti.
								 * 
								 * Každá dimenze bude stejně velká a sice tak, jak jsem zjistil v proměnné
								 * longestDimsInArray, aby nedošlo k indexBoutOfEx...
								 */
								final int[] dimensionsInArray = new int[countOfDims];
								

								// Naplním pole s dimenzemi příslušnou hodnottou (velikostí dimeze):
								IntStream.range(0, dimensionsInArray.length)
										.forEach(q -> dimensionsInArray[q] = longestDimsInArray);

								/*
								 * Zde mohu vytvořit nové pole, které bude stejného datového typu a stejného
								 * počtu dimenzí, pouze se budou lišit počty hodnot v těch dimenzích, protože
								 * jsem nepřišel na způsob jak to pole doslova zkopírovat a jen zaměnit prvky
								 * (pokud by se jednalo o instance).
								 */
								final Object multiDimArray = Array.newInstance(dataTypeOfArray, dimensionsInArray);

								/*
								 * Zde si naplním pole výše vytvořené pole multiDimArray hodnotami z původního
								 * pole v proměnné fieldValue, akorát budou problémy s prvkami, v tom novém jich
								 * bude vždy více, popsáno výše.
								 */
								final Object newArray = copyStuffToNewArray(multiDimArray, fieldValue,
										infoAboutInstancesList);

								// Na konec nastavím nově získané vytvořené a naplněné pole (newArray) do
								// příslušné proměnné v nové instanci:
								setFinalStaticVar(fieldForInsert, i.getNewInstance(), newArray);
							}
						}
						
						
						
						
						
						
						
						
						
						
						// Zde se jedná o klasickou proměnnou:
						else {
							// Potřebuji tedy zjistit, zda je hodnota této proměnné reference na instanci původní
							// třídy, pokud ano, tak se prohodí za novou instanci, pokud ne, tak se prostě
							// vloží tato získaná hodnota z původní instance do nové instance do stejné proměnné:
							final Object objNewValue = isValueReferenceToOldInstance(fieldValue, infoAboutInstancesList);
							
							// Otestuji, zda se vrátila nová instance, která má nahradit v proměnné tu původní,
							// pokud ne, tak se do proměnné v té nové instanci vloží ta samá hodnota, jako je v 
							// původní instanci:
							if (objNewValue != null)
								// Zde byla se v položce fieldValue nachází instance na nějakou třídu, která
								// se má nahradit novou instancí, a ta byla vrácena, tak ji vložím do proměnné:
								setFinalStaticVar(fieldForInsert, i.getNewInstance(), objNewValue);
							
							// zde se nejedná o instanci z diagramu instancí, tak tu hodnotu prostě vložím do té samé
							// proměnné v nové instanci třídy:
							else setFinalStaticVar(fieldForInsert, i.getNewInstance(), fieldValue);
						}
					}
					
					
					// zde je hodnota té proměnné null, tak těžko řící, zda došlo k chybě
					// nebo je ta hodnota null, ať tak čí tak, nastaví se ta proměnná na null:
					else setFinalStaticVar(fieldForInsert, i.getNewInstance(), null);
				}		
				
				// else - zde se již v nové instanci tato položka nenachází, tak není co řešit a jde se dál
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		// Cyklus 3, vysvětlení v komentáří metody:
		// Pouze nahradím starou instanci třídy za novou na aktuální pozici reference:
		// Note:
		// Metoda put nad mapou bu dnahradí objekt pod daným názvem nebo vytvoří novy objekt 
		// daným názvem, pokud daný název ještě neexistuje
		infoAboutInstancesList.forEach(i -> Instances.addInstanceToMap(i.getReference(), i.getNewInstance()));
			
		
		
		
		
		
		// a na konec spustím vlákno, které ro jistot přenačte diagram instancí kvůli vztahům mezi nimi:
		if (refreshInstanceDiagram) {
			ThreadSwingWorker.runMyThread(new CheckRelationShipsBetweenInstances(instanceDiagram,
					instanceDiagram.isShowRelationShipsToItself(), GraphInstance.isUseFastWay(),
					GraphInstance.isShowRelationshipsInInheritedClasses(),
					instanceDiagram.isShowAssociationThroughAggregation()));
		}
	}












    /**
     * Metoda, která zjistí, zda je parametr value instance nějaké třídy, která se nachází v diagramu tříd.
     *
     * @param value
     *         - nějaký objekt, který byl získán z pole a má se o něm zjistit, zda je to instance nějaké třídy, která se
     *         nachází v diagramu tříd.
     *
     * @return true, pokud je v objektu value reference na nějakou instanci nějaké třídy, která se nachází v diagramu
     * tříd, jinak false.
     */
    private static boolean isInstanceOfClassFromCd(final Object value) {
        if (value == null)
            return false;

        /*
         * Text, který by měla obshaovat buňka / třída v diagramu tříd (balíčky oddělené desetinnou tečkou a za
         * poslední tečkou je název třídy.).
         */
        final String textInClassCellInCd = value.getClass().getName();

        // zjistím zda se v diagramu tříd nachází příslušná třída, resp. buňka s
        // příslušným textem:
        return GraphClass.isClassWithTextInClassDiagram(textInClassCellInCd);
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zkusí vytvořit novou instanci třídy, která je v parametru
	 * metody - v hodnotě value.
	 * 
	 * Zjistí se příslušné balíčky a název třídy a načte se ta třída a její výchozí
	 * nullary konstruktor a zkusí se vytvořit, pokud například neexistuje nebo
	 * někde při postupu pro inicializaci třídy dojde k chybě, metoda vrátí null a
	 * instance se nevytvoří.
	 * 
	 * @param value
	 *            - nějaká instance třídy, která se má nahradit za novou instanci
	 *            stejné třídy.
	 * 
	 * @return nová instance stejné třídy jako je v objektu value, jinak null, pokud
	 *         se něco nepovede nebo třída již neexistuje apod.
	 */
    private static Object getNewInstanceOfSameClass(final Object value) {
        final String classWithPackages = value.getClass().getName();

		/*
		 * Načtu si cestu k adresáři bin, v aktuálně otevřeném projektu, toto zjištění
		 * cesty by mohlo být někde jako proměnná, aby se načetla pouze jednou, ale
		 * kdyby náhodou to třeba kdykoliv mezi aktualizacemi uživatel smazal došlo by k
		 * chybě když by se to již netestovalo, proto je to zde.
		 * 
		 * Navíc by k tomu moc často dojít nemělo protože při této aktualizaci vztahů
		 * již jsou třídy zkompilované, takže by měl být i vytvořen adresář bin s
		 * přeloženými třídami, ale opět, kdyby náhodou.
		 */
		final String pathToBin = App.READ_FILE.getPathToBin();

		
		if (pathToBin == null)
			return null;
			

		
		/*
		 * Otestuji, zda existuje třída -> přeložená verze - soubor .class, jejíž
		 * konstruktor se má zavolat.
		 * 
		 * Ještě by bylo možná vhodné upozornit uživatele, ale jelikož v této části vím,
		 * že se třída již nachází v diagramu tříd, tak by již měla být i přeložená a v
		 * příslušném diagramu, proto zde případně vrátím null, ale nemělo by to nikdy
		 * nastat.
		 */
		final String pathToJavaClassFile = pathToBin + File.separator + classWithPackages.replace(".", File.separator)
				+ ".class";

		if (!ReadFile.existsFile(pathToJavaClassFile))
			return null;

		
		/*
		 * Zde si načtu přeloženoý / zkompilovaný soubor .class příslušné třídy, jejiž
		 * konstruktor se má zavolat.
		 */
		final Class<?> clazz = App.READ_FILE.loadCompiledClass(classWithPackages, null);
		
		if (clazz == null)
			return null;
		
		
		try {
			/*
			 * Zkusím najít výchozí nullary konstruktor, ten by se měl nacházet v každé
			 * třídě, a kdyby ne, tak pouze vrátím null a instance se nepřenačte.
			 */
			final Constructor<?> defaultConstructor = Arrays.stream(clazz.getDeclaredConstructors())
					.filter(c -> Modifier.isPublic(c.getModifiers()) && c.getParameterCount() == 0).findFirst().get();

			// Vrátím null nebo vytvořenou instanci:
			return OPERATIONS_WITH_INSTANCE.callConstructor(defaultConstructor);

		} catch (NoSuchElementException e) {
			/*
			 * Tato výjimka nastave v případě, že nebyl nalezen požadovaný konstruktor, tak
			 * vrátím null.
			 */
			return null;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která projde veškeré proměnné (některých vybraných typů) a vymaže z
	 * nich objekt referenceName (pokud jej proměnné obsahují, pak se místo toho
	 * objektu reference nataví na null).
	 * 
	 * Metoda se volá pouze v případě, že uživatel smazal nějakou instanci v
	 * diagramu instancí, tak je potřeba projít veškeré proměnné (v tomto případě
	 * pouze vybrané) a zjistit, zda neobsahují příslušný objekt - referenci na
	 * smazanou insanci, pokud ano, pak se místo reference nastaví na null.
	 * 
	 * @param reference
	 *            - reference, resp. nějaký bývalý objekt, který se má smazat ze
	 *            všech výskytů
	 */
	public static void deleteAllReferencesFromAllFields(final Object reference) {
		/*
		 * Získám si mapu, která bude obsahovat veškeré instance z diagramu instancí.
		 * Tuto mapu projdu a z každé testované proměnné (pouze list, pole a "klasická
		 * proměnná" - časová náročnost) odeberu referenci na již smazaný objekt
		 * reference (pokud se v nějakých proměnných vyskytuje).
		 */
		final Map<String, Object> allInstancesInCd = Instances.getAllInstancesOfClassInCd();
		
		
		
		
		
		
		/*
		 * Projdu získané instance, otestuji, zda je to list nebo pole nebo "klasická"
		 * proměnná a pokud ano, pak v případě, že je to list jej projdu a vymažu
		 * veškeré výskyty objektu reference, to samé pro pole, akorát pole se musí nově
		 * vytvořit a pro "klasickou" proměnnnou, akorát si získám její hodnotu a pokud
		 * je to objekt reference, pak jej nastavím na null.
		 */
		allInstancesInCd.values().forEach(v -> {
			/*
			 * List, do kterého si "načtu" veškeré proměnné, které jsou deklarovány ve třídě
			 * 'v' a které se nachází ve všech předcích příslušné třídy až po třídu Object -
			 * bez ní, z třídy Object už se proměnné neberou.
			 */
			final List<Field> allFieldsFromInstanceList = getAllField(v.getClass(), new ArrayList<>());
			
			
			// Projdu si veškeré výše načtené proměnné:
			for (final Field f : allFieldsFromInstanceList) {
				/*
				 * Získám si hodnotu z přávě testované proměnné
				 */
				final Object fieldValue = ReflectionHelper.getValueFromField(f, v);
				
				
				// ppkud je hodnta proměnné null, pak mohu ji mohu přeskočit, zcela určitě nemá
				// referenci:
				if (fieldValue == null)
					continue;
				
				

				
				

				

				// Otestuji, zda se jedná o nějakou kolekci:
				if (ReflectionHelper.isDataTypeOfList(f.getType())) {
					/*
					 * V tomto případě sejedná o nějakou kolekci, tak si načtu její hodnoty, všechny
					 * je projdu a pokud se jedná o objekt reference, pak místo tohoto objektu
					 * nastavím null hodnotu, protože tenobjekt byl již smazán
					 */
					@SuppressWarnings("unchecked")
					final List<Object> valuesFromField = (List<Object>) fieldValue;

					for (int q = 0; q < valuesFromField.size(); q++) {
						// Musím otestovat, zda není hodnota v poli null, aby nedošlo k chybě:
						final Object objvalueForTest = valuesFromField.get(q);

						if (objvalueForTest != null && objvalueForTest.equals(reference))
							// Zde se jedná o objekt reference, pak jej vymažu (nastavím na null:)
							valuesFromField.set(q, null);
					}

					// Zde se již prošla kolekce a možná nahradily objekty reference za null, tak
					// tuto kolekci vložím zpět do proměnné.
					setFinalStaticVar(f, v, valuesFromField);
				}
						
						
						
						
						
						
						
						
						
						
						
						
						
				// Zde otestuji, zda se jedná o pole:
				else if (f.getType().isArray()) {
					/*
					 * Note:
					 * 
					 * postup je v podstatě úplně stejný, jako u aktualizace instancí, v tomto
					 * případě je jediná změna ta, že místo změny instancí se testuji, zda je
					 * položka v poli objekt reference, pokud ano, pak se pouze nahradí tenobjekt za
					 * null hodnotu. Protože objekt reference značí, že se jedná o smazaný objekt,
					 * tak jej musím smazat ze všech proměnných, ale jinak je to úplně stejné.
					 */

					/*
					 * Zjistím si počet dimenzí příslušného pole.
					 */
					final int countOfDims = getCountOfDimensionsOfArray(fieldValue);

					// Otestuji, zda se jedná má pole jednu dimenzi:
					if (countOfDims == 1) {
						/*
						 * Zjistím si velikost příslušného pole.
						 */
						final int lengthOfArray = Array.getLength(fieldValue);

						/*
						 * Vytořím si nové pole, které bude stejného datového typu a stejné délky.
						 */
						final Object oneDimArray = Array.newInstance(f.getType().getComponentType(), lengthOfArray);

						/*
						 * Zde si projdu původní pole, načtu si hodnotu, otestuji, zda je to objekt
						 * reference, pokud ano, tak ji nahradím za null hodnotu.
						 */
						for (int q = 0; q < lengthOfArray; q++) {
							/*
							 * Získám si objekt / hodnotu z pole:
							 */
							final Object value = Array.get(fieldValue, q);

							if (value != null) {
								// Zde otestuji, zda je to objekt reference, který se má vymazat (nastavit na
								// null)
								if (value.equals(reference))
									// Nastavím null hodnotu místo objektu reference (smazaného objektu):
									Array.set(oneDimArray, q, null);
								// Zde se jedná o jakoukoli jinou hodnotu než objekt reference
								// tak jako tak, tu hodnotu vložím do nového pole na stejný index.
								else
									Array.set(oneDimArray, q, value);
							}
							// Zde se jedná o null hodnotu, tak ji prostě vložím na stejný index, ale toto
							// už
							// není povinné, pokud se jedná o null hodnotu, jedná se o pole objektového typu
							// a null hodnota tam již je jako výchozí.
							else
								Array.set(oneDimArray, q, null);
						}

						// Vložím nové vytvořené pole s potenciláně smazanými položkami (reference) do
						// stejné proměnné v nové instanci:
						setFinalStaticVar(f, v, oneDimArray);
					}

					
					
					
					// Zde má pole více dimenzí, tak je třeba jej projít trochu jinak:

					// Stačilo by pouze 'else'
					else if (countOfDims > 1) {
						/*
						 * Zjistím si datový typ toho pole, zde se jedná o X - dimezí (konkrétně o
						 * countOfDimes dimenzí), tak je musím všechny projít, abych zjistil datový typ
						 * toho pole, který se má vytvořit.
						 */
                        final Class<?> dataTypeOfArray = ReflectionHelper.getDataTypeOfArray(f.getType());

						/*
						 * Zde si zjistím velikost toho nejvetšího pole. Toto je potřeba, abych náhodou
						 * nevytvořil třeba dvojrozměrné pole a nějaký "vnitřní" index byl třeba 3, ale
						 * nově vytvořené pole končilo u dvou (kdybych bral vždy velikost toho prvního
						 * pole).
						 * 
						 * Proto projdu úplně celé pole a najdu si to nejvetší, a na tuto velikost se
						 * vytvoří všechny "vnitřní pole".
						 */
						longestArray = 0;
						final int longestDimsInArray = findLongestArray(fieldValue);

						/*
						 * Vytvořím si pole, které bude značit veškeré dimeze a jejich velikosti.
						 * 
						 * Každá dimenze bude stejně velká a sice tak, jak jsem zjistil v proměnné
						 * longestDimsInArray, aby nedošlo k indexBoutOfEx...
						 */
						final int[] dimensionsInArray = new int[countOfDims];

						// Naplním pole s dimenzemi příslušnou hodnottou (velikostí dimeze):
						IntStream.range(0, dimensionsInArray.length)
								.forEach(q -> dimensionsInArray[q] = longestDimsInArray);

						/*
						 * Zde mohu vytvořit nové pole, které bude stejného datového typu a stejného
						 * počtu dimenzí, pouze se budou lišit počty hodnot v těch dimenzích, protože
						 * jsem nepřišel na způsob jak to pole doslova zkopírovat a jen zaměnit prvky
						 * (pokud by se jednalo o instance).
						 */
						final Object multiDimArray = Array.newInstance(dataTypeOfArray, dimensionsInArray);

						/*
						 * Zde si naplním pole výše vytvořené pole multiDimArray hodnotami z původního
						 * pole v proměnné f, akorát budou problémy s prvkami, v tom novém jich bude
						 * vždy více, popsáno výše.
						 */
						final Object newArray = copyStuffToNewArray(multiDimArray, fieldValue, reference);

						// Na konec nastavím nově získané vytvořené a naplněné pole (newArray) do
						// příslušné proměnné v nové instanci:
						setFinalStaticVar(f, v, newArray);
					}
				}
						
						
						
						
						
						
						
						
						
						
				// Zde se jedná o klasickou proměnnou (případně nějakou z těch netestovaných -
				// mapa apod.):
				else {
					/*
					 * Třeba u mapy by to muselo být jinak, ale to je na déle, tak to pouze beru
					 * jako "klasickou" proměnnou, takže akorát otestuji, zda se jedná o hodnotu
					 * reference, pokud ano, pak do té proměnné vložím null hodnotu, jinak tam
					 * ponechám tu původní hodnotu.
					 */
					if (fieldValue.equals(reference))
						setFinalStaticVar(f, v, null);

					// zde se nejedná o objekt reference z diagramu instancí, tak tu hodnotu prostě
					// vložím do té samé
					// proměnné v nové instanci třídy:
					else
						setFinalStaticVar(f, v, fieldValue);
				}
			}
		});
		
		
		
		
		
		// Vložím do mapy s intancemi aktuální hodnoty, resp. proměnné, které by již
		// neměli obsahovat reference (referenci na smazanou instanci):
		allInstancesInCd.forEach(Instances::addInstanceToMap);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí tu velikost největší dimenze v poli obj. jedná se o
	 * velikost nějaké dimenze v poli (vnitřního pole).
	 * 
	 * "Projde se celé pole obj a pokud je ta položka v poli další pole, velikost
	 * těchto vnitřních polí se testují a najde se ta největší."
	 * 
	 * @param obj
	 *            - pole, ve kterém se má zjistit ta nejveší délka vnitřního pole
	 *            (vnitřní dimeenze).
	 * 
	 * @return - hodnotu v longestArray, která značí velikost nejvetší dimenze v
	 *         poli obj.
	 */
	private static int findLongestArray(final Object obj) {
		/*
		 * Délka aktuálního pole obj.
		 */
		final int size = Array.getLength(obj);
		
		/*
		 * Zde úplně při prvním volání toto metody (ne při rekuzi) bude v proměnné
		 * longestArray hodnota 0 (nula), ale musím započítat i velikosti "celkové" toho
		 * pole, tedy jakoby asi první dimeze, tak zde již potřebuji otestovat ty
		 * velikosti a pokud je velikost větší než nula, tak už musím "uložit" i tu
		 * proměnnou a dále s ní počítat.
		 */
		if (size > longestArray)
			longestArray = size;

		
		// Projdu pole:
		for (int i = 0; i < size; i++) {
			/*
			 * Získám si hodnotu v poli, pokud je to další pole, tak otestuji velikost toho
			 * pole.
			 */
			final Object value = Array.get(obj, i);

			if (value != null && value.getClass().isArray()) {
				/*
				 * Zde je to další pole, tak si zjistím jeho délku.
				 */
				final int length = Array.getLength(value);

				// Pokud je vetší než doposud nalezená, aktualizuji to maximální velikost:
				if (length > longestArray)
					longestArray = length;

				// rekurzí pokračuji:
				findLongestArray(value);
			}
		}

		// Vrátím největčí nalezenou velikost nějaké dimeze:
		return longestArray;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro zkopírování položek v poli originalArray do nového
	 * pole newArray.
	 * 
	 * Metoda projde původní pole originalArray a pro každou pložku otestuji, zda
	 * sejedná o další pole, pokud ano, pak se rekurzí pokračuje, pokud ano, pak se
	 * testuji, zda se jedná o nějakou hodnotu, která byla původní instance nějaé
	 * třídy v diagramu instancí. Pokud ano, pak se tato reference nahradí za novou
	 * referenci na příslušnou instanci v diagramu instancí. Pro ostaní hodnoty, ty
	 * už mohou být jakékoliv se prostě vloží na stejný index v novém poli newArray,
	 * to už mohou být například null hodnoty nebo primitivní hodnoty apod. To už
	 * záleží na datovém typu toho pole (obou polí).
	 * 
	 * @param newArray
	 *            - nové pole stejného datového typu a dimenzí, jako pole
	 *            originalArray. Do tohoto pole se postupně vloží veškeré položky z
	 *            pole originalArray. Pole bude obsahovat stejné položky jako pole
	 *            originalArray. Pouze pokud pole originalArray bude obsahovat
	 *            nějakou referenci na nějakou instanci třídy z diagramu tříd, který
	 *            byla nahrazena, pak se do nového pole newArray vloží i nově
	 *            vytvořená (nahrazená) reference na novou instanci příslušné třídy.
	 * 
	 * @param originalArray
	 *            - původní pole, jehož položky se mají vložit do pole newArray
	 *            (pokud se bude jednat o referenci, tak sezamění za novou).
	 */
	private static Object copyStuffToNewArray(final Object newArray, final Object originalArray,
			final List<InfoAboutInstances> infoAboutInstancesList) {

		/*
		 * Zjistím si velikost pole, které mám projít.
		 */
		final int size = Array.getLength(originalArray);

		for (int i = 0; i < size; i++) {
			/*
			 * Ziskám si položku na příslušném indexu v původním poli (procházeném).
			 */
			final Object value = Array.get(originalArray, i);
			/*
			 * Dále je třeba si získat i hodnotu na stejném indexu akorát v novém poli, aby
			 * se případně poslali do rekurze stejné hodnoty a ně například hodnota v jiné
			 * dimezi pole apod.
			 */
			final Object value2 = Array.get(newArray, i);

			// Zde otestuji, zda se jedná o další pole, pokud ano, rekurzí se pokračuje:
			if (value != null && value.getClass().isArray())
				copyStuffToNewArray(value2, value, infoAboutInstancesList);

			else {
				/*
				 * Zde otestuji, zda se jedná o hodnotu, která značí původní instanci, pokud
				 * ano, pak si získám její náhradu (novou instanci) a tu vložím do nového pole
				 * na příslušný index.
				 */
				final Object objNewValue = isValueReferenceToOldInstance(value, infoAboutInstancesList);

				if (objNewValue != null)
					Array.set(newArray, i, objNewValue);

				// Zde se může jedna v podstatě o jakoukoliv hodnotu, protstě jí vložím do pole
				// na příslušný index:
				else {
					/*
					 * Zde nejprve potřebuji otestovat, zda je ta položka nějaká instance z diagramu
					 * tříd, která není znázorněna v diagramu instancí, pokud ano, pak z té třídy
					 * potřebji také vytvořit novou instanci a vložit jej na stejnou položku v poli,
					 * akorát novou instanci, ale už zde není dořešeno přenačtení hodnot i z té
					 * instance - možná v další verzi.
					 */
					if (isInstanceOfClassFromCd(value)) {
						/*
						 * Zde si vytvořím novou instanci příslušné třídy.
						 */
						final Object refreshInstance = getNewInstanceOfSameClass(value);

						// Nastavím jej do pole na příslušnou pozici:
						Array.set(newArray, i, refreshInstance);
					}

					// Zde se jedná o jakoukoliv jinou hodnotu, tak ji vložím do pole:
					else
						Array.set(newArray, i, value);
				}

			}
		}

		// Vrátím nově naplněné pole:
		return newArray;
	}
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro zkopírování položek v poli originalArray do nového
	 * pole newArray s tím, že pokud je příslušná iterovaná položka v poli
	 * originalArray reference, pak se vymaže, tj. do nového pole se na příslušnou
	 * pozici nastaví null hodnota..
	 * 
	 * Metoda projde původní pole originalArray a pro každou položku otestuje, zda
	 * se jedná o další pole, pokud ano, pak se rekurzí pokračuje, pokud ne, pak se
	 * testuje, zda se jedná o hodnotu reference (hodnota, která se má smazat),
	 * která byla původní instance třídy v diagramu instancí. Pokud ano, pak se na
	 * příslušnou pozici místo reference nastaví null hodnota, protože se ta
	 * reference již smazala. Pro ostaní hodnoty, ty už mohou být jakékoliv se
	 * prostě vloží na stejný index v novém poli newArray, to už mohou být například
	 * null hodnoty nebo primitivní hodnoty apod. To už záleží na datovém typu toho
	 * pole (obou polí).
	 * 
	 * @param newArray
	 *            - nové pole stejného datového typu a dimenzí, jako pole
	 *            originalArray. Do tohoto pole se postupně vloží veškeré položky z
	 *            pole originalArray. Pole bude obsahovat stejné položky jako pole
	 *            originalArray. Pouze pokud pole originalArray bude obsahovat
	 *            položku shodnou s reference, pak se na do nového pole newArray
	 *            vloží na příslušný index místo reference null hodnota, protože
	 *            reference byla smazána.
	 * 
	 * @param originalArray
	 *            - původní pole, jehož položky se mají vložit do pole newArray.
	 * 
	 * @param reference
	 *            - původní reference na nějakou instanci v diagramu instancí, která
	 *            byla smazána, tak je třeba ji vymazat i ze všech položek v poli.
	 */
	private static Object copyStuffToNewArray(final Object newArray, final Object originalArray,
			final Object reference) {

		/*
		 * Zjistím si velikost pole, které mám projít.
		 */
		final int size = Array.getLength(originalArray);

		for (int i = 0; i < size; i++) {
			/*
			 * Ziskám si položku na příslušném indexu v původním poli (procházeném).
			 */
			final Object value = Array.get(originalArray, i);
			/*
			 * Dále je třeba si získat i hodnotu na stejném indexu akorát v novém poli, aby
			 * se případně poslali do rekurze stejné hodnoty a ně například hodnota v jiné
			 * dimezi pole apod.
			 */
			final Object value2 = Array.get(newArray, i);

			// Zde otestuji, zda se jedná o další pole, pokud ano, rekurzí se pokračuje:
			if (value != null && value.getClass().isArray())
				copyStuffToNewArray(value2, value, reference);

			else {
				/*
				 * Potřebuji otestovat, zda se nejedná o null hodnotu a zda je to reference,
				 * která semá smazal, pokud jsou tyto podmínky splněny, pak se na příslušný
				 * index v poli nastaví null hodnota, pokud podmínky nejsou splněny, pak se
				 * nastaví na příslušný index původní hodnota.
				 */
				if (value != null && value.equals(reference))
					Array.set(newArray, i, null);

				// Zde se může jedna v podstatě o jakoukoliv hodnotu, protstě jí vložím do pole
				// na příslušný index:
				else
					Array.set(newArray, i, value);
			}
		}

		// Vrátím nově naplněné pole:
		return newArray;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro zjištění kolik dimenzí má pole v parametru této
	 * metody ("array").
	 * 
	 * @param array
	 *            - X - rozměrné pole o kterém se má zjistit, kolika rozměrné to
	 *            pole je, tj. kolik má dimehí.
	 * 
	 * @return hodnotu typu int,která značí počet dimenzí v poli array, nebo likol
	 *         má pole array dimenzí (kolika rozměrné je).
	 */
	private static int getCountOfDimensionsOfArray(final Object array) {
		/*
		 * Proměnná, do které se budou postupně ukládat počet zjištěných dimenzí.
		 */
		int count = 0;

		Class<?> clazz = array.getClass();

		while (clazz.isArray()) {
			count++;
			clazz = clazz.getComponentType();
		}

		return count;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která získá veškeré proěmné z třídy clazz a ze všech předků třídy
	 * clazz až po třídu Object, z té třídy Object už se proměnné brát nebudou.
	 * 
	 * @param clazz
	 *            - třída, ze které se mají začíst brát veškeré proměnné, až se
	 *            všechny najdou a "převezmou", tak se pokračuje předkem této třídy,
	 *            dokud se nejedná o třídu Object (popř. null), z třídy Object už se
	 *            proměnné "brát" nebudou.
	 * 
	 * @param fieldsList
	 *            - Instancen ového listu, kam se budou vkládat veškeré nalezené
	 *            položky z třídy clazz a ze všech jejich předků až po třídu Object.
	 * 
	 * @return list, který obsahuje veškeré proměnné, deklarované v třídě clazz a
	 *         všech jejich předků.
	 */
	private static List<Field> getAllField(final Class<?> clazz, final List<Field> fieldsList) {
		/*
		 * Jelikož uživatel může mít třídu v diagramu tříd, která dědí například z JFrame, pak by se zde načetli i
		 * proměnné z toho okna, což nechci, proto bude hledání proměnných omezeno
		 * pouze na třídy v diagramu tříd.
		 */
        if (clazz == null || !GraphClass.isClassWithTextInClassDiagram(clazz.getName()) || clazz == Object.class)
            return fieldsList;

		/*
		 * Získám si veškeré proměnné z aktuálně iterované třídy.
		 */
		if (clazz.getDeclaredFields() != null)
			fieldsList.addAll(Arrays.asList(clazz.getDeclaredFields()));

		/*
		 * Zde otestuji, zda předek třídy clazz není třída Object nebo null, vždy by
		 * semělo dojít nejprve k třídě Object, ale kdby náhodou se někde něco pokazilo.
		 * Jinak, budu načítat všechny předky tříd clazz a brát si z nich všechny
		 * proměnné, dokud nedojdu k třídě Object (nebo null), a pokud ano, tak skončím
		 * tak, že vrátím list získaných proměnných, protože z třídy Object už brát
		 * žádné proměnné nebudu.
		 */
		if (clazz.getSuperclass() != null && clazz.getSuperclass() != Object.class)
			getAllField(clazz.getSuperclass(), fieldsList);

		return fieldsList;
	}



	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda je hodnota v parametru - fieldValue reference na
	 * instanci nějaké z původních instancí, které se mají nahradit tou novou
	 * instancí, pokud ano, tak se vrátí nová instance, kterou se tato původní
	 * nahradní, pokud ne, tak metoda vrátí null.
	 * 
	 * @param fieldValue
	 *            - hodnota o které chci zjistit, zda je to reference na původní
	 *            instanci, resp. původní instance
	 * 
	 * @param infoAboutInstancesList
	 *            - kolekce hodnot s informacemi o původních instancích a nových
	 *            instancích, kterými se mají nahradit ty původní a z tohoto listu
	 *            se má zjistit, zda hodnota fieldValue je, resp. se shoduje s
	 *            nějakou z původních instancí v listu, pokud ano, tak se vrátí její
	 *            náhražka - resp. nová instance, která má nahradit tu původní,
	 *            pokud se taková nenajde metoda vrátí null.
	 * 
	 * @return null, pokud se nenajde shoda s instancí fieldValue v listu v
	 *         původních instancích, jinak se vrátí nová instance, která má nahradit
	 *         tu původní
	 */
	private static Object isValueReferenceToOldInstance(final Object fieldValue,
			final List<InfoAboutInstances> infoAboutInstancesList) {

		for (final InfoAboutInstances i : infoAboutInstancesList) {
			if (i.getOriginalInstance().equals(fieldValue))
				return i.getNewInstance();
		}

		return null;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která se pokusí vrátit položku - proměnnou z instance třídy v
	 * parametru metody.
	 * 
	 * @param objInstance
	 *            - instance třídy, ze které se má vzít položka fieldName
	 * 
	 * @param fieldName
	 *            - název položky, která by se měla vrátit v instance objInstance
	 * 
	 * @return referenci na položku fieldName nebo null, pokud nebu položka v
	 *         instanci třídy nalezena
	 */
	private static Field getFieldFromInstance(final Object objInstance, final String fieldName) {	
		/*
		 * Po dalších výzkumu jsem dospěl k tomu, že budu načítat veškeré proměnné z
		 * dané třídy objInstance, a zároveň si načtu veškeré proměnné ze všech jejich
		 * předků až po třídu Object, ale z třídy Object už se proměnné načítat nebudou.
		 * 
		 * Ze všech načtených proměnných budu pouze hledat název proměnné, ale zde by
		 * mohlo dojít k tomu problému, že se najde název nějaké proměnné v například v
		 * nějakém předkovi a bude třeba jiného datového typu apod. To ale tak úplně
		 * nevím, jak pořešit, jestli to vůbecjde, protože v nově zkompilované třídě se
		 * již nemusí nacházet příslušná proměnná ani v jejichp ředcích, nebo může být
		 * změněn datový typ příslušné proměnné apod. Proto už tyto informace předávat
		 * nebudu. Vždy se najde první výskyt názvu proměnné fieldname, aˇž už se jedná
		 * o požadovaný typ nebo ne, testuje se pouze název proměnné.
		 */
		
		/*
		 * List, který obsahuje veškeré proměnné získané z třídy objInstance a všech
		 * jejich předků až po třídu Object - z ní už se proměnné brát nebudou.
		 */
		final List<Field> fieldsList = getAllField(objInstance.getClass(), new ArrayList<>());

		/*
		 * Nyní projdu veškeré získané proměnné a vrátím tu, kde se shoduje název
		 * proměnné, případně null, pokud proměnná spříslušným název nebude nalezena.
		 */
		try {
			return fieldsList.stream().filter(f -> f != null && f.getName().equals(fieldName)).findFirst().get();
		} catch (NoSuchElementException e) {
			/*
			 * Tato výjimka (NoSuchElementException) může nastat / vypadnout v případě, že
			 * se vlistu fieldsList nevyskytuje proměná s názvem fieldName, a výskyt této
			 * výjimky tedy značí, že proměnná s požadovaným názvem nebyla nalezena, pak
			 * vrátím null.
			 */
			return null;
		}
	}
	
	
	
	

	
	
	
	
	
	
	/**
	 * Metoda, která vloží hodnotu objvalue do proměnné typu Field v instanci třídy
	 * objOwner.
	 * 
	 * Tato metoda jednak zpřehledňuje kód než kdybych jí psal v cyklu ale také
	 * zpřístupňuje položku resp. umožní nastavit položku i když budenáhodou final,
	 * protože mohl uživatel třeba nastavit nějakou proměnnou v konstruktoru, ale
	 * ten zde nezavlám, tak to musím udělat tímto způsobem - viz metoda.
	 * 
	 * @param field
	 *            - položka, do které se vloží hodnoty objValue
	 * 
	 * @param objOwner
	 *            - vlastník proměnné nebli instance nějaké třídy, kde se proměnná
	 *            field nachází
	 * 
	 * @param objvalue
	 *            - hodnota, která se má vložit do proměnné field
	 */
	private static void setFinalStaticVar(final Field field, final Object objOwner, final Object objvalue) {
		field.setAccessible(true);
		
		try {
			// Otestuji, zda náhodnou není proměnná final, pokud ano, musím použít kód v podmínce, abych 
			// mohl její hodnotu nastvit
			if (Modifier.isFinal(field.getModifiers())) {
				final Field modifiersField = Field.class.getDeclaredField("modifiers");
				
				modifiersField.setAccessible(true);
				
				modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
			}

			// Vložím hodnotu do proměnné:
			field.set(objOwner, objvalue);

		} catch (NoSuchFieldException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka získávání proměnné: " + field.getName()
                        + " z instance třídy: " + objOwner
                        + ", třída v aplikaci: Instances, metoda : setFinalStaticVar. Jedna z možných příčin této " +
                        "chyby může být například to, že uvedená proměnná se nenachází v instanci třídy.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
		}
		
		catch (SecurityException e2) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka získávání proměnné: " + field.getName()
                        + " z instance třídy: " + objOwner
                        + ", třída v aplikaci: Instances, metoda : setFinalStaticVar. Jedna z možných příčin této " +
                        "chyby může být například chyba Class loaderu. "
                        + "Například Class loader volajícího není stejný jako předchůdce třídy loader pro aktuální " +
                        "třídu apod.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e2));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
		}
		
		catch (IllegalArgumentException e2) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka pří vkládání hodnoty: " + objvalue + " do proměnné: "
                        + field.getName() + ", v instanci: " + objOwner
                        + ", třída v aplikaci: Instances, metoda : setFinalStaticVar. Jedna z možných příčin této " +
                        "chyby může být například to, pokud zadaný objekt není"
                        + " instancí třídy nebo rozhraní, které deklaruje podkladové pole (nebo jeho podtřídu nebo " +
                        "implementátor) nebo pokud selže konverze rozbalení.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e2));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			
			
			
			/*
			 * Pokud nastane tato výjimka (IllegalArgumentException), tak se nepodařilo
			 * naplnit proměnnou field příslušnou hodnotou v objValue.
			 * 
			 * Informace o nstalé chybě byla výše zapsána do logu, ale nyní je vhodné
			 * provést následující kód.
			 * 
			 * 
			 * Jedná se o to, že tato výjimka nejčastěji nastane v případě, že je v nějaké
			 * instanci třídy proměnná typu nějaké třídy v diagramu tříd. Tato proměnná je
			 * naplněna příslušnou instancí příslušné třídy, ale tato instance, kterou je
			 * naplněna ta proměnná se nenachází v diagramu instancí, tj. není na ní
			 * reference.
			 * 
			 * Například následující kód v nějaké třídě v diagramu tříd, například v konstruktoru:
			 * className1.className = new model.data.ClassNameB();
			 * 
			 * Pak v případě, že se přenačtou instance tříd, tak se vezme ta hodnota v
			 * proměnné, tj. instance nějaké třídy, která není znázorněna v diagramu
			 * instancí. a zkusí se vložit do té samé proměnné akorát v nové instanci, ale
			 * to již nepůjde, popsáno na začátku této třídy -> (RefreshInstances). Proto
			 * zkusím vzít tu hodnotu a načíst si teď již zkompilovanou příslušnou třídu a
			 * znovu vytvořit její instanci a vložit jí do té proměnné.
			 * 
			 * 
			 * 
			 * Ještě by se dále hodilo projít i tuto novou instanci a projít i její proměnné
			 * s tou původní instancí a také přenačíst hodnoty i v tech proměnnných, ale to
			 * už je trochu na déle, tak možná v další verzi (už bych věděl s čím počítat).
			 * Dále by zde možná mohlo dojít k zacyklení, ale v žádném z testů se mi to
			 * nestalo, vždy se tato část vykonávání kódu zastavila buď na tom, že se
			 * nenačetla požadovaná třída nebo chyba balíčků (při úmyslném zaměnení hodnot),
			 * jinak nic, to by ale stačilo vyřešit například tak, že by se do této metody
			 * 'setFinalStaticVar' dala ještě například logická proměnná 'repeat' a zde,
			 * když nastane tato výjimka 'IllegalArgumentException' testovat, zda je tato
			 * proměnná true, pak by se již tento kód pro načtení neopakoval, jinak tato
			 * metoda setFinalStaticVar by se pak volala s tím logickým parametrem true zde
			 * a false všude jinde, pak by se nemělo nic stát, ale jak jsem již uvedl, ještě
			 * k tomu nedošlo, tak to zde neimplementuji.
			 */
            if (objvalue.getClass().getPackage() != null) {
                final String packageAndClassName = objvalue.getClass().getName();

				final Class<?> clazz = App.READ_FILE.loadCompiledClass(packageAndClassName, null);

				// Otestuji, zda se ji podařilo načíst:
				if (clazz != null) {
					// zde se tedy podařilo načíst nově zkompilovanou verzi nějaké třídy, tak
					// vytvořím její instanci,
					final Object newInstance = createInstance(clazz, null, null);

					if (newInstance != null)
						setFinalStaticVar(field, objOwner, newInstance);
				}
			}
		}
		
		catch (IllegalAccessException e2) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka pří vkládání hodnoty: " + objvalue + " do proměnné: "
                        + field.getName() + ", v instanci: " + objOwner
                        + ", třída v aplikaci: Instances, metoda : setFinalStaticVar. Jedna z možných příčin této " +
                        "chyby může být například to, že uvedená proměnná je nepřístupná nebo final.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e2));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
	}
	
	

	
	
	
	
	
	
	
	
	/**
	 * Tato metodaq slouží pouze pro vytvoření nové instance, abych si výše
	 * nezpřehledňoval kód, a nezastavil se kvůli vyjímce
	 * 
	 * @param clazz
	 *            - třída - Class, jejíž instance se má vytvořit
	 * 
	 * @param languageProperties
	 *            - načtený soubor typu .properties, která obsahuje texty aplikace v
	 *            uživatelem zvoleném jazyce, je zde potřeba, protože když se
	 *            nepovede vytvořit instanci třídy, pravděpodobně je to tím, že se
	 *            nenachází výchozí "nullary" konstruktor v dané třídě
	 * 
	 * @param outputEditor
	 *            - editor výstupů - komponenta, kam se vypíše chybové hlášení o
	 *            vzniklé situaci s informací o tom, že se nepodařilo vytvořit
	 *            instancí nějkaé třídy a že se tam má případně doplnit výchozí
	 *            "nullary" konstruktor.
	 * 
	 * @return nově vytvořenou instance třídy v parametru metody - clazz nebo null,
	 *         pokud dojde k vyjímce
	 */
	private static Object createInstance(final Class<?> clazz, final Properties languageProperties,
			final OutputEditorInterface outputEditor) {
		try {
			return clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jakodruhou informace text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při vytváření instance pro nahrazení té původní po zkompilování tříd kvůli" +
                                " ClassLoaderu, třída Instances, mtoda createInstance.\n"
                                + "V případě chyby InstantiationException se může jednat o jednu z následujících " +
                                "příčin: třída představuje abstraktní třídu, rozhraní, pole, primitivní datový typ, "
                                + "nebo není platná, nebo pokud třída nemá výchozí nullary konstruktor, nebo pokud " +
                                "instanciace selže z nějakého jiného důvodu."
                                + "\n"
                                + "Chyba IllegalAccessException může vzniknout napříkld v případě, kdy třída nebo " +
                                "její výchozí nullary konstruktor není přístupný.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();


			if (languageProperties != null) {
				final String txtFailedToUpdateInstanceOfClass = languageProperties.getProperty("Mo_Txt_FailedToUpdateInstanceOfClass", Constants.RI_TXT_FAILED_TO_UPDATE_INSTANCE_OF_CLASS);
				final String txtFailedToUpdateInstanceOfClass_2 = languageProperties.getProperty("Mo_Txt_FailedToUpdateInstanceOfClass_2", Constants.RI_TXT_FAILED_TO_UPDATE_INSTANCE_OF_CLASS_2);

				if (outputEditor != null)
				    outputEditor.addResult(txtFailedToUpdateInstanceOfClass + ": " + clazz.getName() + ", " + txtFailedToUpdateInstanceOfClass_2);
			}

			else if (outputEditor != null)
                outputEditor.addResult(Constants.RI_TXT_FAILED_TO_UPDATE_INSTANCE_OF_CLASS + ": " + clazz.getName() + ", " + Constants.RI_TXT_FAILED_TO_UPDATE_INSTANCE_OF_CLASS_2);

            return null;
        }
    }
}