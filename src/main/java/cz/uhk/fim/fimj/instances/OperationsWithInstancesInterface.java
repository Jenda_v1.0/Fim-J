package cz.uhk.fim.fimj.instances;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * Tento Interface obsahuje hlavičky metod pro manipulaci s instancemi tříd vytvořený uživatelem pomocí této aplikace
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface OperationsWithInstancesInterface {

    /**
     * Metoda, která slouží pouze k zavolání konstruktoru.
     * <p>
     * Jedná se o to, že jsem doplnil do editoruů příkazů možnost pro rozpoznávání syntaxe pro vytvoření nové instance,
     * ale pouze vytvoření instance, tj. že se nebude tato nově vytvořená instance nějaké třídy (pokud se povede
     * vytvořit instance) nikam přidávat, do žádné mapy apod. Nebude nikde "zachovávána" apod. Pouze pokud se například
     * zavolá metoda s takovým parametrem, pak se předá jako parametr metody apod. Ale to je vše. nebude se
     * zobrazovat v
     * diagramu instancí apod.
     *
     * @param constructor
     *         - konstruktor nějaké třídy, který se má zavolat, měl by být veřejný a měl by se nacházet v nějaké třídě,
     *         i když se jedná o výchozí nullary konstruktor.
     * @param parameters
     *         - parametry, které se předají do constructor při jeho zavolání.
     * @return vytvořenou instanci třídy, poud se úspěšně povede zavolat příslušný konstruktor a nedojde k žádné
     * výjimce, jinak null.
     */
    Object callConstructor(final Constructor<?> constructor, final Object... parameters);


    /**
     * Metoda, vytvoří instanci třídy pod daným názvem proměnné, která na ní "bude ukazovat", a "tento objekt" -
     * proměnnou a její instanci přidá do HashMapy s instancemi
     * <p>
     * Instance z této třídy by měla jít vytvořit vždy - neni třeba zpřístupňovat knstruktor
     *
     * @param instanceName
     *         - název proměnné, která bude ukazovat na vytvořenou instanci nějaké třídy
     * @param clazz
     *         - třída, jejíž instanci je třeba vytvořit
     * @return true, pokud vse dopadne v pohodě, jinak false
     */
    boolean createInstance(final String instanceName, final Class<?> clazz);


    /**
     * Metoda, která vytvoří instanci třídy pod daným názvem Zavolá konstruktor s daným počtem a typy parametrů
     * <p>
     * Dále se nejprve otestuje, zda se má "zpřístupnit" - může se jedna o privátní konstuktor
     *
     * @param className
     *         - název třídy, ze které se má vytvořit intance - zavolatkonstruktoru
     * @param packageName
     *         - název balíčků, ve kterých se třída nachází
     * @param classNameVariable
     *         - proměnná odkazující na danou instanci třídy
     * @param constructor
     *         - konstruktor třídy, který se má zavolat pro vytvoření instance
     * @param disclose
     *         - logická proměnná (true / false) - pokud se má konstruktor zpřístupnit nebo ne pokud je volán z
     *         diagramu
     *         tříd, ten je udělán tak, že půjdou zavolat veškeré konstuktory, ale pokud se jedná o vytvoření
     *         instance z
     *         editoru příkaů, tak tam se snažím dodržovat "klasickou Javovskou syntaxi" zde se tedy zpřístupňovat
     *         nebude
     * @param parameters
     *         - parametry konstruktoru třídy
     * @return true, pokud vse dopadne v pohodě, jinak false
     */
    boolean createInstanceWithParameters(final String className, final String packageName,
                                         final String classNameVariable, final Constructor<?> constructor, final
										 boolean disclose,
                                         final Object... parameters);


    /**
     * Metoda, která vytvoří instanci třídy zaoláním příslušného kontruktoru, nově vytvořenou instanci vloží do mapy,
     * kde jsou umístěny všechny instance vytvořené uživatelem a přidá do diagramuinstancí příslušnou buňku, která bude
     * reprezentovat tuto nově vytvořenou instancí nějaké třídy z diagramu tříd
     *
     * @param className
     *         - nzev třídy, ze které byla vytvořena instance
     * @param packageName
     *         - název balíčků, ve kterých (/ kterém) se daná třída nachází
     * @param classVariable
     *         - název proměnné, odkazující na instanci dané třídy
     * @param constructor
     *         - konstruktor, která se má zavolat
     * @return true, pokud vse dopadne v pohodě, jinak false
     */
    boolean createInstance(final String className, final String packageName, final String classVariable,
                           final Constructor<?> constructor);


    /**
     * Metoda, která z kolekce - HashMapy vytvořených instancí získá objekt, na který se odkazuje nějaká proměnná, která
     * zastupuje danou instanci třídy
     *
     * @param classVariable
     *         - název - proměnná, která odkazuje na instanci třídy
     * @return Objekt dané třídy nebo null, pokud dojde k chybe nebo nebude nalazena požadovaná hodnota
     */
    Object getClassFromMap(final String classVariable);


    /**
     * metoda, která zavolá pomocí reflexe metodu zvolenou uživatelem z instance třídy v diagramu tříd konkrétně zavolá
     * metodu bez parametrů
     *
     * @param method
     *         - metoda, kterou chce uživatel zavolat
     * @param ownerMethod
     *         - "vlastník metody" - konkrétně se jedná o konkrétní instance třídy, ve které se má daná metoda zavola
     * @param disclose
     *         - logická hodnota - true / false, dle které poznám, zda mám nebo nemám "zpřístupnit" metodu pomocí
     *         metody
     *         setAccessible(true);, je to proto, abych zrealizoval svou myšlenku, že když uživatel zavolá metodu z
     *         diagramu instancí - nad konkrétní instancí, tak metoda půjde zavolat vždy - i když je např privátní čí
     *         protected, apod. kdežto, editoru příkazů jsem napsal už jako "klasickou" Javovovský editor pro zadávání
     *         příkazů, fungují v něm klasická pravidla - nejde zavolat privátní metoda, apod.
     *         <p>
     *         Takže pomocí této proměnno (discClose) buď zpřístupním nebo ne danou metodu, - v editoru příkazů ji
     *         nebudu zpřístupňovat v diagramu instancí ano
     * @return objekt Called, který obsahuje hodnotu, kterou metoda vrátila a zda se metoda zavolala nebo došlo k
     * nějaké
     * chybě.
     */
    Called callMethodWithoutParameters(final Method method, final Object ownerMethod, final boolean disclose);


    /**
     * Metoda, která zavolá zvolenou metodu uživatelem nad instancí nějaké třídy z diagramu tříd, metody s alespoň
     * jedním parametrem
     *
     * @param method
     *         - metoda, která se má zavolat
     * @param ownerMethod
     *         - vlastník metody - konkrétně instance třídy, kde se má metoda zavolat
     * @param disclose
     *         - logická hodnota, dle které buď zpřístupním nebo ne, zadanou metodu, která se má zavoalt pokud má být
     *         metoda zavolána z diagramu instancí, tak ji zpřístupním, jinak ne, z diagramu instancí půjdou zavolat
     *         všechny metody bez ohledu na jejich viditelnost mezi třídami - private, protocted,...*
     * @param parameters
     *         - parametry metody, která se má zavolat
     * @return instance objektu Called, který obsahuje návratovou hodnotu metody, případně null. A logickou hodnotu o
     * tom, zda se metodu podařio zavolat nebo ne.
     */
    Called callMethodWithParameters(final Method method, final Object ownerMethod, final boolean disclose,
                                    final Object... parameters);
}