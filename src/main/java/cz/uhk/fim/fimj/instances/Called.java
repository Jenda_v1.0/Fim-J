package cz.uhk.fim.fimj.instances;

import cz.uhk.fim.fimj.output_editor.OutputEditor;

/**
 * Tato třída slouží pro "předání" výsledku po zavolání metody (statické nebo nad instancí nějaké třídy).
 * <p>
 * Tato třída (objekt) obsahuje pouze dvě proměnné (atributy), první je objekt, který obsahuje hodnotu, která se
 * vrátila, může to být hodnota null, která byla vrácena, nebo to můžebýt i výchozí hodnoty.
 * <p>
 * Druhá proměnná je logická hodnota true nebo false, true bude v případě, že se metodu úspěšně podařilo zavolat, false
 * bude v případě, že se metodu z nějakého důvodu napodaří zavolat, pak ani nezáleží, jaká hodnota bude v první popsané
 * proměnné s návratovou hodnotu, tam může být null, ale je to jedno, protože se stejně nevyužije (pokud bude druhá
 * proměnná false).
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class Called {

    /**
     * Proměnná, která bude obsahovat vrácenou hodnotu z metody, popř. null hodnotu, pokud se metodu nepodaří zavolat,
     * nebo to může být i vrácená hodnota apod.
     */
    private final Object objReturnValue;


    /**
     * Logická proměnná, která značí, zda se metodu podařilo zavolat nebo ne a dle toho se buď bude nebo nebude
     * přistupovat k hodnotě v proměnné objReturnValue. Pokud bude hodnota v této logické proměnné true, pak se metoda
     * zavolala a je možné, že něco vrátila, pak se může vzít hodnota z objReturnValue, jinak pokud bude tato logická
     * proměnná false, pak se metoda nezavolala, pak nezáleží na tom, co je v proměnné ojReturnValue.
     */
    private final boolean wasCalled;


    /**
     * Tato proměnná obsahuje stejnou hodnotu, jako proměnné objReturnValue, tj. obsahuje návratovou hodnotu nějaké
     * zavolané metody.
     * <p>
     * Ale tato proměnné na rozdíl od proměnné objReturnValue může obsahovat navíc i referenční proměnnou, která
     * obsahuje zadanou referenci na příslušnou instanci, která se nachází v diagramu instancí.
     * <p>
     * Jde o to, že když se zavolá metoda a ta vrátí nějakou hodnotu / vrátí instanci nějaké třídy, která se nachází v
     * diagramu instancí, tak by bylo vhodné, zobrazit tu referenci, aby uživatel věděl, o jakou instanci jde.
     * <p>
     * Hodnota této proměnné se nevyužije vždy, nekdy je potřeba pouze zavolat metodu a vzít si její hodnotu, například
     * při naplnění nějkaé proměnné apod. Ale pokud se bude jednat o "pouzhé" zavolání metody bez předání návratovhé
     * hodnoty do proměnné nebo parametru metody či konstruktoru adpod. Tak se využije tato hodnota aby to uživatel mohl
     * vidět - o jakou referenci, resp. instanci jde.
     */
    private final Object objReturnValueWithTestedReference;


    /**
     * Konstruktor této třídy.
     *
     * @param objReturnValue
     *         - hodnota, kterou metoda vrátila, případně null.
     * @param wasCalled
     *         - logická proměnná, která značí, zda se metoda zavolala v pořádku nebo došlo k nějaké "chybě".
     */
    public Called(final Object objReturnValue, final boolean wasCalled) {
        super();

        this.objReturnValue = objReturnValue;
        this.wasCalled = wasCalled;

        /*
         * Zde otestuji, zda se mají vypisovat referenční proměnné (reference), pokud
         * ne, pak do té proměnné, která slouží pro uložení a vypsání i té reference
         * vložím tu samou hodnotu, jako v objReturnValue, tj. bez reference na konci.
         *
         * Když se mají vypisovat, tak se otestuji, zda se metoda zavolala v pořádku a
         * pokud ano, tak otestuji, zda je to instance nebo pole nebo list s instnacemi
         * v diagramu instancí a případně k nim doplním reference, když ano to neplatí,
         * tak si do proměnné pro vypsání hodnoty / instance s referencí vložím null,
         * protože se nepodařilo zavolat metodu, tak se ani nic nevrátilo (také bych tam
         * mohl vložit stejnou hodnotu jako v objReturnValue - ale obě hodnoty by měli
         * být null, takže je to jedno).
         */
        if (!OutputEditor.isShowReferenceVariables())
            objReturnValueWithTestedReference = objReturnValue;

        else if (wasCalled)
            objReturnValueWithTestedReference = OperationsWithInstances
                    .getReturnValueWithReferenceVariableForPrint(objReturnValue);
        else
            objReturnValueWithTestedReference = null;
    }


    /**
     * Metoda, která získá hodnotu, kterou metoda vrátila.
     *
     * @return hodnotu v proměnné objReturnValue - vrácená hodnota z metody nebo null.
     */
    public Object getObjReturnValue() {
        return objReturnValue;
    }


    /**
     * Metoda, která získá logickou hodnotu z proměnné wasCalled, která značí, zda se metoda zavolala nebo ne. True
     * značí, že semetoda zavolala, false, že se metoda nezavolala - došlo k nějaké chybě apod.
     *
     * @return výše popsanou proměnné wasCalled.
     */
    public boolean isWasCalled() {
        return wasCalled;
    }


    /**
     * Metoda, k terá vrátí hodnotu, kterou vrátila nějaká metoda, akorát, v případě, že se jedná o hodnotu coby
     * instance nějaké třídy, která se nachází v diagramu instancí, tak za touto hodnotou, kterou vrátí tato metoda bude
     * i doplněna referenční proměnná.
     *
     * @return výše popsanou návratovou hodnotu nějaké metody. Pokud je to instance nějaké třídy z diagramu tříd, a
     * nachází se v diagramu instancí, tak bude za návratovou hodnotou doplněna ještě referenční proměnná na příslušnou
     * instanci z diagramu instnací.
     */
    public Object getObjReturnValueWithTestedReference() {
        return objReturnValueWithTestedReference;
    }
}
