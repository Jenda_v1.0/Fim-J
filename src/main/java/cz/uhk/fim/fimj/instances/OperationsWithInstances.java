package cz.uhk.fim.fimj.instances;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;
import cz.uhk.fim.fimj.instance_diagram.CheckRelationShipsBetweenInstances;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.output_editor.OutputEditor;
import cz.uhk.fim.fimj.redirect_sysout.CalledMethod;
import cz.uhk.fim.fimj.redirect_sysout.RedirectApp;
import cz.uhk.fim.fimj.swing_worker.ThreadSwingWorker;

/**
 * Tato třída obsahuje implementace metod pro manipulaci s instancemi vytvořené uživatelem ze tříd vytvořené touto
 * aplikací
 * <p>
 * konkrétně se jedná o instance vytvořené z grafu nebo editoru příkazů aplikace
 * <p>
 * Jedná se o operace například jako vytvoření instance, zavolání metody, přířazení hodnoty do preoměnné, apod viz
 * interface
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class OperationsWithInstances implements OperationsWithInstancesInterface, LanguageInterface {

	private static String txtMethodIsNotPublic, txtMethodIsNotInInstanceOfClass, txtErrorInCallTheMethod,
			txtErrorDuringCreateInstance, txtPossibleCousesCreateInstanceError, txtClass,
			txtCannotCreateInstanceFromAbstractClass, txtConstructorIsNotPublic,
			txtConstructorWantDifferentCountOrTypesOfParameters, txtConstructorWant, txtButGot,
			txtErrorDuringCallBaseConstructor, txtButDidGot, txtReferenceNameExistChange, txtReference,
			txtErrorWhileCallingConstructorByThrowAnException,

			// Texty do metody callConstructor:
			txtFailedToCallConstructorInAbstractClass, txtInsertedParameters, txtRequiredParameters,
			txtFailedToCallConstructor, txtConstructorIsNotAccessible, txtArgumentsError,
			txtConstructorThrowAnException,

			// Text pro objekt Called - kvůli vypísu referencí v případě instance z diagramu
			// instancí:
			txtReferenceVariable;
	
	
	
	
	
	
	
	/**
	 * Statická proměnná, která slouží pro "sestavení" X - rozměrného pole v
	 * případě, že se má vypsat do editoru výstupů, například po zavolání metody
	 * nebo získání hodnoty z proměnné.
	 * 
	 * Do této proměnné si postupně vkládám nalezené hodnoty a na konec ten text
	 * vrátím (pro vypsání).
	 */
	private static String tempArray;
	
	
	
	
	
	/**
	 * pro výpis chybových hlášek - pokud nastanou vyjímky:
	 */
	private final OutputEditor outputEditor;
	
	
	
	
	/**
	 * Reference na diagram instancí je zde potřeba, abych ho mohl před vlákně,
	 * které mezi reprezentacemi instancí v tomto diagramu vytvoří příslušné hrany
	 * coby vztahy:
	 */
	private final GraphInstance instanceDiagram;

	
	
	
	
	public OperationsWithInstances(final OutputEditor outputEditor,
			final Properties languageProperties, final GraphInstance instanceDiagram) {
		super();
		
		this.outputEditor = outputEditor;
		this.instanceDiagram = instanceDiagram;
		
		
		setLanguage(languageProperties);
	}

	
	
	
	
	
	
	
	
	@Override
	public Object callConstructor(final Constructor<?> constructor, final Object... parameters) {

		/*
		 * Nastavím proměnnou v RedirectApp, kdyby uživatel měl povoleno, že se maí vypisovat i metody a konstruktory
		 * k System.out, tak aby se vypsaly i ty hodnoty.
		 */
		RedirectApp.setCalledMethod(new CalledMethod(constructor, parameters));

		try {
			/*
			 * Pokud nejsou zadány žádné parametry, pak jej rovnou zavolám a pokud nedojde k žádné chybě, pak 
			 * rovnou vrátím true.
			 */
			if (parameters.length == 0)
				return constructor.newInstance();

			
			// Otestuji, zda metoda změnila v nějaké vztahy mezi instancemi:
			checkRelationsBetweenInstances();

			/*
			 * Zde byly zadány nějaké parametry, pak je předám a zavolám konsturktorm s
			 * příslušnými parametry, pokud nedojde k žádné chybě, pak se vrátí true.
			 */
			return constructor.newInstance(parameters);
			
		} catch (InstantiationException e) {

			if (parameters.length == 0) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO, "Zachycena výjimka při volání konstruktoru: " + constructor
                            + ". Tato chyba může nastat v případě, že se jedná o konstruktor, který je deklarován v " +
                            "abstraktní třídě.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();

				if (outputEditor != null)
					outputEditor.addResult(txtFailedToCallConstructorInAbstractClass + ": " + constructor);
			}

			else {
				/*
				 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
				 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO, "Zachycena výjimka při volání konstruktoru: " + constructor
                            + ". Tato chyba může nastat v případě, že se jedná o konstruktor, který je deklarován v " +
                            "abstraktní třídě.\nZadané parametry: "
                            + Arrays.toString(parameters) + "\nKonstruktor vyžaduje: "
                            + Arrays.toString(constructor.getParameterTypes()));

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
				
				
				// Přepsáno na zarovnání mezer:
				if (outputEditor != null)
					outputEditor.addResult(txtFailedToCallConstructorInAbstractClass + ": " + constructor
							+ OutputEditor.NEW_LINE_TWO_GAPS + txtInsertedParameters + ": "
							+ Arrays.toString(parameters) + OutputEditor.NEW_LINE_TWO_GAPS + txtRequiredParameters
							+ ": " + Arrays.toString(constructor.getParameterTypes()));
			}
			
		} catch (IllegalAccessException e) {
			
			if (parameters.length == 0) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO, "Zachycena výjimka při volání konstruktoru: " + constructor
                            + ". Tato chyba může nastat například v případě, že zadaný konstruktor není přístupný.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
				
				
				if (outputEditor != null)
					outputEditor.addResult(
							txtFailedToCallConstructor + ": " + constructor + ", " + txtConstructorIsNotAccessible);
			}

			else {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO, "Zachycena výjimka při volání konstruktoru: " + constructor
                            + ". Tato chyba může nastat například v případě, že zadaný konstruktor není přístupný" +
                            ".\nZadané parametry: "
                            + Arrays.toString(parameters) + "\nKonstruktor vyžaduje: "
                            + Arrays.toString(constructor.getParameterTypes()));

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
				 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
				 */
				ExceptionLogger.closeFileHandler();
				
				
				
				// Přepsáno na zarovnání mezer:
				if (outputEditor != null)
					outputEditor.addResult(txtFailedToCallConstructor + ": " + constructor + ", "
							+ txtConstructorIsNotAccessible + OutputEditor.NEW_LINE_TWO_GAPS + txtInsertedParameters
							+ ": " + Arrays.toString(parameters) + OutputEditor.NEW_LINE_TWO_GAPS
							+ txtRequiredParameters + ": " + Arrays.toString(constructor.getParameterTypes()));
			}		

		} catch (IllegalArgumentException e) {
			/*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při volání konstruktoru: " + constructor
                        + ". Tato chyba může nastat například v případě, že se liší počet vyžadovaných a zadaných " +
                        "parametrů, nebo pokud selhání konverze pro primitivní argumenty selže, "
                        + "nebo pokud po případném rozbalení nemůže být hodnota parametru převedena na odpovídající " +
                        "typ formálního parametru pomocí konverze vyvolání metody, nebo pokud se tento konstruktor " +
                        "týká typu enum.\nZadané parametry: "
                        + Arrays.toString(parameters) + "\nKonstruktor vyžaduje: "
                        + Arrays.toString(constructor.getParameterTypes()));

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			
			
			// Přepsáno na zarovnání mezer:
			if (outputEditor != null)
				outputEditor.addResult(txtFailedToCallConstructor + ": " + constructor + ", " + txtArgumentsError
						+ OutputEditor.NEW_LINE_TWO_GAPS + txtInsertedParameters + ": " + Arrays.toString(parameters)
						+ OutputEditor.NEW_LINE_TWO_GAPS + txtRequiredParameters + ": "
						+ Arrays.toString(constructor.getParameterTypes()));

		} catch (InvocationTargetException e) {

            if (parameters.length == 0) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO, "Zachycena výjimka při volání konstruktoru: " + constructor
                            + ". Tato chyba může nastat například v případě, že podkladový konstruktor vyhodí výjimku" +
                            ".");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
				
				
				if (outputEditor != null)
					outputEditor.addResult(
							txtFailedToCallConstructor + ": " + constructor + ", " + txtConstructorThrowAnException);
			}

			else {				
				/*
				 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
				 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
				 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO, "Zachycena výjimka při volání konstruktoru: " + constructor
                            + ". Tato chyba může nastat například v případě, že podkladový konstruktor vyhodí výjimku" +
                            ".\nZadané parametry: "
                            + Arrays.toString(parameters) + "\nKonstruktor vyžaduje: "
                            + Arrays.toString(constructor.getParameterTypes()));

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
				
				
				
				// Přepsáno na zarovnání mezer:
				if (outputEditor != null)
					outputEditor.addResult(txtFailedToCallConstructor + ": " + constructor + ", "
							+ txtConstructorThrowAnException + OutputEditor.NEW_LINE_TWO_GAPS + txtInsertedParameters
							+ ": " + Arrays.toString(parameters) + OutputEditor.NEW_LINE_TWO_GAPS
							+ txtRequiredParameters + ": " + Arrays.toString(constructor.getParameterTypes()));
			}
		}

		/*
		 * Zde se nepodařilo konstruktor zavolat, tak vrátím null.
		 */
		return null;
	}
	

	
	
	
	
	
	
	
	
	
	@Override
	public boolean createInstance(final String instanceName, final Class<?> clazz) {

		/*
		 * Nastavím proměnnou v RedirectApp, kdyby uživatel měl povoleno, že se maí vypisovat i metody a konstruktory
		 * k System.out, tak aby se vypsaly i ty hodnoty.
		 */
		RedirectApp.setCalledMethod(new CalledMethod(getDeclaredConstructor(clazz)));

		try {
			/*
			 * Vytvořím si instanci příslušné třídy (clazz).
			 */
			final Object myObject = clazz.newInstance();
			
			if (!Instances.isReferenceNameInMap(instanceName)) {
				Instances.addInstanceToMap(instanceName, myObject);
				
				// Zkusím si zjistit balíček třídy:
				final Package packageName = clazz.getPackage();
				
				// Otestuji, zda se v nějakém balíčku třída nachází:
				if (packageName != null)
					// Zde se v nějakém balíčku třída nachází, tak si ho zjistím:
					instanceDiagram.addInstanceCell(clazz.getSimpleName(), packageName.getName(), instanceName);

				// Zde se třída v žádném balíčku nenachází (nemělo by nastat)
				else
					instanceDiagram.addInstanceCell(clazz.getSimpleName(), null, instanceName);
				
				
				// Otestuji, zda metoda změnila v nějaké vztahy mezi instancemi:
				checkRelationsBetweenInstances();

                /*
                 * Zde se vytvořila nová instance a už by měla být v diagramu instancí, tak aktualizuji okno pro
                 * doplnění hodnot do editoru příkazů, aby byla na výběr i nová instance:
                 */
                App.getCommandEditor().launchThreadForUpdateValueCompletion();

				return true;
			}
			
			else {
				outputEditor.addResult(txtReferenceNameExistChange + OutputEditor.NEW_LINE_TWO_GAPS + txtReference
						+ ": " + instanceName);

				return false;
			}						
			
		} catch (InstantiationException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při vytváření instance třídy pod referencí: " + instanceName
                        + ", třída OperationWithInstances.java, metoda: createInstance. Tato chyba může nastat " +
                        "například v případě pokud třída představuje abstraktní třídu, rozhraní, pole, primitivní " +
                        "datový typ nebo void nebo pokud "
                        + "třída nemá výchozí nullary konstruktor, nebo pokud instanciace selže z nějakého jiného " +
                        "důvodu.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			

			
			outputEditor.addResult(
					txtErrorDuringCreateInstance + OutputEditor.NEW_LINE_TWO_GAPS + txtPossibleCousesCreateInstanceError
							+ OutputEditor.NEW_LINE_TWO_GAPS + txtClass + ": " + clazz.getName());
		}
		catch (IllegalAccessException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při vytváření instance třídy pod referecí: " + instanceName
                        + ", třída OperationWithInstances.java, metoda: createInstance. Tato chyba může nastat " +
                        "například pokud není třída nebo její výchozí nullary konstruktor přístupný.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			
			outputEditor.addResult(
					txtConstructorIsNotPublic + OutputEditor.NEW_LINE_TWO_GAPS + txtClass + ": " + clazz.getName());
		}
		return false;
	}
	
	
	
	
	


	@Override
	public boolean createInstanceWithParameters(final String className, final String packageName,
			final String classNameVariable, final Constructor<?> constructor, final boolean disclose,
			final Object... parameters) {

		/*
		 * Nastavím proměnnou v RedirectApp, kdyby uživatel měl povoleno, že se maí vypisovat i metody a konstruktory
		 * k System.out, tak aby se vypsaly i ty hodnoty.
		 */
		RedirectApp.setCalledMethod(new CalledMethod(constructor, parameters));

		try {
			if (disclose)
				// Zpřístupním konstuktor - pokud je privátní nebo chráněný,
				// pokud je veřejný, ta se nic neděje - ušetřím dotaz na
				// IsAccessible
				constructor.setAccessible(true);

			// Zavolám příslušný konstruktor - vytvořím instanci dané třídy a
			// předám mu parametry

			// Zde je konstuktor vešejný nebo je zpřístupněn, tak ho mohu
			// zavolat:
			final Object myObject = constructor.newInstance(parameters);

			// Otestuji, zda ještě neexistuje název reference:
			if (!Instances.isReferenceNameInMap(classNameVariable)) {
				// Vytvořenou instanci coby objekt vložím do mapy s instancemi
				// pro další manimpulaci
				Instances.addInstanceToMap(classNameVariable, myObject);

				// přidám do diagramu instancí buňku, která bude reprezentovat
				// tuto nově vytvořenou instanci
				instanceDiagram.addInstanceCell(className, packageName, classNameVariable);

				// Otestuji, zda metoda změnila v nějaké vztahy mezi instancemi:
				checkRelationsBetweenInstances();

                /*
                 * Zde se vytvořila nová instance a už by měla být v diagramu instancí, tak aktualizuji okno pro
                 * doplnění hodnot do editoru příkazů, aby byla na výběr i nová instance:
                 */
                App.getCommandEditor().launchThreadForUpdateValueCompletion();

				// Pokud se dostanu sem, tak se instance v pořádku vytvořila, a
				// mohu vrátit true, coby potvrzení vytvoření instance dané
				// třídy
				return true;
				}
			
			else {
				outputEditor.addResult(txtReferenceNameExistChange + OutputEditor.NEW_LINE_TWO_GAPS + txtReference
						+ ": " + classNameVariable);

				return false;
			}

		} catch (InstantiationException e) {
			/*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při volání konstruktoru : " + constructor
                        + ", třída v aplikaci OperationsWithInstances, metoda: createInstanceWithParameters s " +
                        "parametry - název promwnné instance, konstruktor a neurčený počet parametrů typu String!\n " +
                        "Chyba: parametry: "
                        + Arrays.toString(parameters) + "\nKonstruktor chce parametry: "
                        + Arrays.toString(constructor.getParameterTypes())
                        + "\nDalší možná příčina je, že se jedná o abstraktní třídu.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			
			outputEditor.addResult(txtCannotCreateInstanceFromAbstractClass);
			
		} catch (IllegalAccessException e) {
			/*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při volání konstruktoru ve třídě: " + constructor
                        + ", třída v aplikaci: OperationsWithInstances, metoda: createInstanceWithParameters s " +
                        "parametry - název promwnné instance, konstruktor a neurčený počet parametrů typu String!\n " +
                        "Chyba: parametry: "
                        + Arrays.toString(parameters) + "\nKonstruktor chce parametry: "
                        + Arrays.toString(constructor.getParameterTypes())
                        + "\nDalší možný vznik této chyby je, že konstruktor třídy není přístupný.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();
			
			outputEditor.addResult(txtConstructorIsNotPublic + OutputEditor.NEW_LINE_TWO_GAPS + constructor);
			
		} catch (IllegalArgumentException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při volání konstruktoru ve třídě: " + constructor
                        + ", třída v aplikaci: OperationsWithInstances, metoda: createInstanceWithParameters s " +
                        "parametry - název promwnné instance, konstruktor a neurčený počet parametrů typu String " +
                        "!\nChyba: parametry: "
                        + Arrays.toString(parameters) + "\nKonstruktor chce parametry: "
                        + Arrays.toString(constructor.getParameterTypes())
                        + "\nDalší možné příčiny vzniku této chyby mohou být například když se skutečný a formální " +
                        "počet parametrů liší, pokud selhala konverze primitivních datových typů, pokud se " +
                        "konstruktor týká typ Enum a další.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			
			outputEditor.addResult(txtConstructorWantDifferentCountOrTypesOfParameters + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtConstructorWant + ": " + Arrays.toString(constructor.getParameterTypes())
					+ OutputEditor.NEW_LINE_TWO_GAPS + txtButGot + ": " + Arrays.toString(parameters));
			
		} catch (InvocationTargetException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při volání konstruktoru ve třídě: " + constructor
                        + ", třída v aplikaci: OperationsWithInstances, metoda: createInstanceWithParameters s " +
                        "parametry - název promwnné instance, konstruktor a neurčený počet parametrů typu String " +
                        "!\nChyba: parametry: "
                        + Arrays.toString(parameters) + "\nKonstruktor chce parametry: "
                        + Arrays.toString(constructor.getParameterTypes())
                        + "\nTato chyba může nastat v případě, že podkladový konstruktor vyhodí výjimku.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			outputEditor.addResult(txtErrorWhileCallingConstructorByThrowAnException);
		}

		return false;
	}




	@Override
	public boolean createInstance(final String className, final String packageName, final String classVariable,
			final Constructor<?> constructor) {

		/*
		 * Nastavím proměnnou v RedirectApp, kdyby uživatel měl povoleno, že se maí vypisovat i metody a konstruktory
		 * k System.out, tak aby se vypsaly i ty hodnoty.
		 */
		RedirectApp.setCalledMethod(new CalledMethod(constructor));

		try {
			// Zpřístupním konstruktor, aby bylo možné jej zavolat )v případě, že není
			// veřejný)
			constructor.setAccessible(true);

			/*
			 * Vytvořím si instanci příslušné třídy (zavoláním konstruktoru).
			 */
			final Object myObject = constructor.newInstance();

			if (!Instances.isReferenceNameInMap(classVariable)) {
				Instances.addInstanceToMap(classVariable, myObject);

                instanceDiagram.addInstanceCell(className, packageName, classVariable);

                // Otestuji, zda metoda změnila v nějaké vztahy mezi instancemi:
                checkRelationsBetweenInstances();

                /*
                 * Zde se vytvořila nová instance a už by měla být v diagramu instancí, tak aktualizuji okno pro
                 * doplnění hodnot do editoru příkazů, aby byla na výběr i nová instance:
                 */
                App.getCommandEditor().launchThreadForUpdateValueCompletion();

                return true;
			}

			else {
				outputEditor.addResult(txtReferenceNameExistChange + OutputEditor.NEW_LINE_TWO_GAPS + txtReference
						+ ": " + classVariable);

				return false;
			}				

		} catch (InstantiationException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při volání konstruktoru" + constructor
                        + ", třída OperationsWithInstances, metoda: createInstance s parametry - název promwnné " +
                        "instance, a konstruktor. Tato chyba může nastat například v případě, že se jedná o " +
                        "abstraktní třídu.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			outputEditor.addResult(txtCannotCreateInstanceFromAbstractClass);

        } catch (IllegalAccessException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při volání konstruktoru" + constructor
                        + ", třída OperationsWithInstances, metoda: createInstance s parametry - název promwnné " +
                        "instance, a konstruktor. Tato chyba"
                        + " může nastat například v případě, že příslušný konstruktor v dané třídě není přístupný.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			
			outputEditor.addResult(txtConstructorIsNotPublic + OutputEditor.NEW_LINE_TWO_GAPS + constructor);
			
		} catch (IllegalArgumentException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při volání konstruktoru" + constructor
                        + ", třída OperationsWithInstances, metoda: createInstance s parametry - název promwnné " +
                        "instance, a konstruktor. Tato chyba může nastat například v případě, se liší "
                        + "počet skutečných a formálních parametrů, selhala konverze primitivních datových typů nebo " +
                        "pokud se konstruktor týká typu Enum a další.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			outputEditor.addResult(txtConstructorWantDifferentCountOrTypesOfParameters + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtConstructorWant + ": " + Arrays.toString(constructor.getParameterTypes())
					+ OutputEditor.NEW_LINE_TWO_GAPS + txtButDidGot);
			
		} catch (InvocationTargetException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při volání konstruktoru" + constructor
                        + ", třída OperationsWithInstances, metoda: createInstance s parametry - název promwnné " +
                        "instance, a konstruktor. Tato chyba může nastat například v případě, že podkladový " +
                        "konstruktor hodí výjimku.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			outputEditor.addResult(txtErrorDuringCallBaseConstructor);
		}
		
		return false;
	}




	
	
	
	@Override
	public Object getClassFromMap(final String classVariable) {
		return Instances.getInstanceFromMap(classVariable);
	}
	
	


	
	
	
	
	
	@Override
	public Called callMethodWithoutParameters(final Method method, final Object ownerMethod, final boolean disclose) {

		/*
		 * Nastavím proměnnou v RedirectApp, kdyby uživatel měl povoleno, že se maí vypisovat i metody a konstruktory
		 * k System.out, tak aby se vypsaly i ty hodnoty.
		 */
		RedirectApp.setCalledMethod(new CalledMethod(method));

		try {
			if (disclose)
				method.setAccessible(true);

			/*
			 * Zde potřebuji ještě otestovat, zda se náhodou nejedná o zděděnou metodu s
			 * modifikátorem "protected", pokud ano, tak ji zde musím pomocí reflexe
			 * "zpřístupnít", tj. zavolat nad ní metodu setAccessible, abych ji mohl
			 * zavolat. Jde o to, že když se volá metoda, která je zděděná, tak se zde musí
			 * i tak metoda zpřístupnit než se zavolá, jinak vypadne
			 * výjimka.IllegalAccessException.
			 */
			else if (Modifier.isProtected(method.getModifiers()))
				method.setAccessible(true);
			
			
			// zavolám metodu a uložím si její návratovou hodnotu:
			final Object returnValue = method.invoke(ownerMethod);
			
			// Otestuji, zda metoda změnila v nějaké vztahy mezi instancemi:
			checkRelationsBetweenInstances();

            /*
             * Zde se zavolala nějaká metoda, tak aktualizuji okno pro doplnění hodnot do editoru příkazů, protože se
             * mohla například naplnit nebo změnit hodnota v nějaké proměnné apod. Tak, aby se nabízeli aktuální
             * hodnoty.
             */
            App.getCommandEditor().launchThreadForUpdateValueCompletion();
			
			// vrátím návratovou hodnotu:
			return new Called(returnValue, true);
			
		} catch (IllegalAccessException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při zavolání metody bez parametrů, mtetoda: "
                        + method.getName()
                        + ", třída v aplikaci: OperationsWithInstances, metoda: callMethodWithoutParameters. Tato " +
                        "chyba může nastat například v případě, že metoda není přístupná.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			outputEditor.addResult(method.getName() + " - " + txtMethodIsNotPublic);
		}
		catch (IllegalArgumentException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při zavolání metody bez parametrů, mtetoda: "
                        + method.getName()
                        + ", třída v aplikaci: OperationsWithInstances, metoda: callMethodWithoutParameters. Tato " +
                        "chyba může nastat například v případě, je-li metodou metoda "
                        + "instance a zadaný objektový argument není instancí třídy nebo rozhraní, které deklaruje " +
                        "podkladovou metodu (nebo její podtřídy nebo implementátor); "
                        + "pokud se počet skutečných a formálních parametrů liší; pokud selhání konverze pro " +
                        "primitivní argumenty selže; nebo pokud po případném rozbalení "
                        + "nelze hodnotu parametru převést na odpovídající typ formálního parametru pomocí konverze " +
                        "vyvolání metody");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			outputEditor.addResult(method.getName() + " - " + txtMethodIsNotInInstanceOfClass);
		}
		catch (InvocationTargetException e) {
			/*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informace i chybě:
                logger.log(Level.INFO, "Zachycena výjimka při zavolání metody bez parametrů, mtetoda: "
                        + method.getName()
                        + ", třída v aplikaci: OperationsWithInstances, metoda: callMethodWithoutParameters. Tato " +
                        "chyba může nastat například v případě, že podkladová metody vyhodí výjimku.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			outputEditor.addResult(method.getName() + " - " + txtErrorInCallTheMethod);
		}
		return new Called(null, false);
	}


	
	
	

	
	

	@Override
	public Called callMethodWithParameters(final Method method, final Object ownerMethod, final boolean disclose,
			final Object... parameters) {

		/*
		 * Nastavím proměnnou v RedirectApp, kdyby uživatel měl povoleno, že se maí vypisovat i metody a konstruktory
		 * k System.out, tak aby se vypsaly i ty hodnoty.
		 */
		RedirectApp.setCalledMethod(new CalledMethod(method, parameters));

		try {
			if (disclose)
				method.setAccessible(true);

			/*
			 * Zde potřebuji ještě otestovat, zda se náhodou nejedná o zděděnou metodu s
			 * modifikátorem "protected", pokud ano, tak ji zde musím pomocí reflexe
			 * "zpřístupnít", tj. zavolat nad ní metodu setAccessible, abych ji mohl
			 * zavolat. Jde o to, že když se volá metoda, která je zděděná, tak se zde musí
			 * i tak metoda zpřístupnit než se zavolá, jinak vypadne
			 * výjimka.IllegalAccessException.
			 */
			else if (Modifier.isProtected(method.getModifiers()))
				method.setAccessible(true);

			
			
			final Object returnValue = method.invoke(ownerMethod, parameters);
			
			// otestuji, zda se naplnili nějaké vztahy mezi instancemi, které mohla  provést metoda,
			// například setr na nějakou promennou, apod.
			checkRelationsBetweenInstances();

            /*
             * Zde se zavolala nějaká metoda, tak aktualizuji okno pro doplnění hodnot do editoru příkazů, protože se
             * mohla například naplnit nebo změnit hodnota v nějaké proměnné apod. Tak, aby se nabízeli aktuální
             * hodnoty.
             */
            App.getCommandEditor().launchThreadForUpdateValueCompletion();
			
			// vrátím výslednou hodnotu metody:
			return new Called(returnValue, true);
		} 
		catch (IllegalAccessException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachyena výjimka při volání metody: " + method.getName()
                        + ", třída v aplikaci: callMethodWithParameters(Method method, Object ownreMethod, final " +
                        "Object... parameters), ve třídě: OperationsWithInstances.java. "
                        + "Tato chyba může nastat například v případě, že metoda není přístupná.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			
			outputEditor.addResult(method.getName() + " - " + txtMethodIsNotPublic);

		} catch (IllegalArgumentException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při volání metody: " + method.getName()
                        + ", va aplikaci callMethodWithParameters(Method method, Object ownreMethod, final Object... " +
                        "parameters), ve třídě: OperationsWithInstances.java \n"
                        + "metoda chce argumenty: " + Arrays.toString(method.getParameterTypes()) + ", ale dostala: "
                        + Arrays.toString(parameters)
                        + ".\nJedním z příčin této chyby může být například je-li metodou metoda instance a zadaný " +
                        "objektový argument není instancí třídy nebo rozhraní, které deklaruje podkladovou metodu (nebo"
                        + " její podtřídy nebo implementátor); pokud se počet skutečných a formálních parametrů liší;" +
                        " pokud selhání konverze pro primitivní argumenty selže; nebo pokud po případném rozbalení "
                        + "nelze hodnotu parametru převést na odpovídající typ formálního parametru pomocí konverze " +
                        "vyvolání metody.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			
			outputEditor.addResult(method.getName() + " - " + txtMethodIsNotInInstanceOfClass);
		}
		catch (InvocationTargetException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při volání metody: " + method.getName()
                        + ", v aplikaci: callMethodWithParameters(Method method, Object ownreMethod, final Object... " +
                        "parameters), ve třídě: OperationsWithInstances.java."
                        + " K této chybě může dojít například tím, že podkladová vyhodí výjimku.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			outputEditor.addResult(method.getName() + " - " + txtErrorInCallTheMethod);
		}
		
		return new Called(null, false);
	}


	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro převod objektu objReturnValue do textové podoby a
	 * případné přidání referencí za instance - pokud jsou to instance z diagramu
	 * instancí.
	 * 
	 * Metoda zjistí, zda je objekt objReturnValue null, list, pole nebo "klasická"
	 * proměnné a dle toho získá příslušnou / příslušné hodnoty a o kždé hodnotě
	 * zjistí, zda se jedná o instanci, která se nachází v diagramu instancí. Pokud
	 * ano, tak za ten objekt (instanci) doplní referenci na tu instanci, která se
	 * nachází v diagramu instancí.
	 * 
	 * Metoda vrací veškeré objekty převedené do takové podoby, aby bylo možné je
	 * vypsat do jednoho z výstupních editorů pro uživatele.
	 * 
	 * @param objReturnValue
	 *            - nějaká návratová hodnota metody / hodnota z proměnné, o které se
	 *            má zjistit, zda je to instance třídy, která se nachází v diagramu
	 *            tříd a tato instance má svou reprezentaci v diagramu instancí,
	 *            pokud ano, pak se na za příslušný objekt / instanci přidá ještě
	 *            text té referenční proměnné - reference na isntanci v diagramu
	 *            instancí.
	 * 
	 * @return objekt returnValue převedený do podobny pro vypsání do editoru s
	 *         potenciálně doplněnými referenčními hodnotami - referencemi na
	 *         instanci v diagramu instancí.
	 */
	@SuppressWarnings("unchecked")
	public static Object getReturnValueWithReferenceVariableForPrint(final Object objReturnValue) {
		/*
		 * Otestuje se, zda je to list, pole nebo klasická proměnné, pak pokud je to
		 * list nebo pole, tak se projde celé pole nebo kolekce a ka každé hodnotě se
		 * přidá ten název reference na instanci v diagramu instnací (pokud ta hodnota v
		 * poli nebo listu je instance z diagramu instnací), jinak se nic nedělá.
		 * 
		 * Note:
		 * else u podmínek níže již není třeba.
		 */
		
		// pokud je to null, tak není co řešit:
		if (objReturnValue == null)
			return objReturnValue;
		// Nebo: return null (to samé)	
				
		
		
		
		/*
		 * Pokud je to pole, tak postupuji rekurzivně pro každou dimenzi příslušného
		 * pole.
		 * 
		 * Postupně projde celé pole a jednotlivé položky si bude postupně vkládat do
		 * textové proměnné (jelikož se jedná pouze o výpis), a jednotlivé hodnoty budou
		 * oddělené desetinnou čárkou, popřípadě hranatou závorkou reprezentující pole
		 * nebo jeho dimeze.
		 */
		else if (objReturnValue.getClass().isArray()) {
			// Na začátek naplním proměnnou otevírací hranatou závorkou, která značí
			// "začátek" pole
			tempArray = "[";

			// Rekurzivně projdu celé pole a do proměnné tempArray si vložím jeho jednotlivé
			// položky:
			getArrayWithReferenceVariables(objReturnValue);

			/*
			 * Pokud to pole není prázdné, tj. přidala se tam alespoň jedna položka, tak
			 * takto bude končit ta proměnné po proiterování celého pole, tak odeberu
			 * poslední dva znaky a přidám uzavírací hranatou závorku:
			 */
			if (tempArray.endsWith(", "))
				tempArray = tempArray.substring(0, tempArray.length() - 2);

			tempArray += "]";

			// Vrátím získané pole převedené do textové podoby potenciálně doplněny
			// referenční hodnoty:
			return tempArray;
		}
		
		
		
		
		
		
		/*
		 * Zde se jedná o kolekci / list.
		 * 
		 * Převedu si ten listu (objekt) do listu a projdu jej, ale všechny hodnoty
		 * musím dávat do jiného listu, abych si nepřepisoval reference, na konec vrátím
		 * úplně stejný list, bude obsahovat ty samé položky, akorát tam, kde se jedná o
		 * instanci z diagramu instancí bude doplněna referenční proměnná (za příslušnou
		 * instanci v listu).
		 */
		else if (ReflectionHelper.isDataTypeOfList(objReturnValue.getClass())) {

			/*
			 * Přetypuji si získaný list na list objektů:
			 */
			final List<Object> valuesList = (List<Object>) objReturnValue;

			/*
			 * Toto je list, do kterého se překopírují veškeré objekty z listu valuesList,
			 * protože, když to neudělám, tak se bude držet reference na příslušné objekty a
			 * v momentě, kdy by je uživatel vypsal, tak se "změní" i v té proměnné, takže
			 * se za ty instance připíšou ještě reference a to budou jako hodnoty v tom
			 * listu, což je špatně.
			 */
			final List<Object> copyValues = new ArrayList<>(valuesList.size());


			for (final Object o : valuesList) {
				/*
				 * Získám si aktuálně iterovaný objekt:
				 */
				/*
				 * Pokud je to null, tak prostě přidám null hodnotu do výsledného listu a
				 * pokračuji další iterací, nemá smysl testovat reference.
				 */
				if (o == null) {
					copyValues.add(null);
					continue;
				}

				/*
				 * Zjistím, zda se jedná o instanci nějaké třídy z diagramu tříd a ta instance
				 * je reprezentována v diagramu instancí, takže pokud se mi naplní proměnná
				 * reference, pak ji přidám za ten objekt, resp. hodnotu v listu.
				 */
				final String reference = Instances.getReferenceToInstanceFromCdInMap(o);

				if (reference != null) {
					copyValues.add(o + " (" + txtReferenceVariable + ": " + reference + ")");
				}

				// Zde se nejedná o instanci z diagramu instnací, tak jej prostě vložím do
				// výsledného listu:
				else
					copyValues.add(o);
			}

			return copyValues;
		}
		
		
		
		
		// Zde se jedná o "klasickou" proměnnou, nebo nějaku, která není "hlídaná" /
		// testovaná pro instance:
		else {
			/*
			 * Zde si zjistím, zda je návratová hodnota metody nějaká instance třídy z
			 * diagramu tříd, která se nachází v diagramu instancí.
			 */
			final String reference = Instances.getReferenceToInstanceFromCdInMap(objReturnValue);

			/*
			 * pokud reference není null, pak je objReturnValue nějaká instance nějaké třídy
			 * z diagramu tříd a tato instance (objReturnValue) se nachází v diagramu
			 * instancí, tak za tu instanci (do vrácení) přidám referenci na tuto instancí,
			 * aby uživatel věděl, o jaký objekt se jedná.
			 */
			if (reference != null)
				return objReturnValue + " (" + txtReferenceVariable + ": " + reference + ")";

			/*
			 * Zde se nejedná o žádnou instanci z diagramu instancí, tak na konec za ten
			 * objekt (návratovou hodnotu metody) přidávat nebudu:
			 */
			return objReturnValue;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která do staticé proměnné tempArray postupně vloží X - rozměrné pole
	 * objArray, tj. objArray převede do textové podoby a u každé položky otestuje,
	 * zda se jedná o instanci nějaké třídy z diagramu tříd, která se aktuálně
	 * nachází v diagramu instancí, pokud ano, pak se za tu položku v poli (za
	 * instanci) vloží / doplní ještě reference na tu instnaci v diagramu instnací,
	 * kterou uživatel zadal při jejím vytváření.
	 * 
	 * @param objArray
	 *            - X - rozměrné pole, které se má převést do textové podoby.
	 */
	private static void getArrayWithReferenceVariables(final Object objArray) {
		/*
		 * Získám si délku pole.
		 */
		final int length = Array.getLength(objArray);
		
		
		// proiteruji celé pole:
		for (int i = 0; i < length; i++) {
			/*
			 * Získám si objekt / položku v poli na aktuálně iterovaném indexu - pozici v
			 * poli.
			 */
			final Object objValue = Array.get(objArray, i);
			
			/*
			 * pokud je to null hodnota, pak nemá smysl pokračovat, přidám jej do textu pro
			 * výslednou podobu pole v textové podobně a pokračuji další iterací.
			 * 
			 * Zde by nemělo smysl dále pokračovat, protože je jasné, že to instance nebude.
			 */
			if (objValue == null)
				tempArray += "null, ";
				
			
			
			/*
			 * pokud se jedná o další pole, resp. další dimezi pole objArray, tak si do
			 * příslušného textu vložím příslušné závorky a pokračuji rekurzivně.
			 */
			else if (objValue.getClass().isArray()) {
				tempArray += "[";

				getArrayWithReferenceVariables(objValue);

				if (tempArray.endsWith(", "))
					tempArray = tempArray.substring(0, tempArray.length() - 2);

				tempArray += "], ";
			}
				
			
			
			else {
				/*
				 * Zde se jedná o "nějakou" položku v poli, tj. není to null hodnota ani další
				 * pole / dimeze, tak otestuji, zda se nejedná o instanci, která se nachází v
				 * diagramu instancí, pokud ano, pak přidám příslušnou referenci za tu instanci.
				 */
				final String reference = Instances.getReferenceToInstanceFromCdInMap(objValue);

				/*
				 * Otestuji, zda byla získána reference, pokud ano, je to instnace z diagramu
				 * instnací, tak ji na konec přidám, pokud bude null, tak to není instnace,
				 * která se nachází v diagramu instnací, tak přidám jen tu hodnotu.
				 */
				if (reference != null)
					tempArray += objValue + " (" + txtReferenceVariable + ": " + reference + "), ";

				else
					tempArray += objValue + ", ";
			}
		}
	}








    /**
     * Metoda, která vrátí výchozí konstruktor. Toto je "nouzové / výchozí" řešení pro případ, když uživatel zavolá
     * výchozí konstruktor pomocí editoru příkazů, tak kdyby v něm měl nějaké System.out metody, tak aby se v okně
     * výstupního terminálu vypsaly tyto hodnoty případně i s třídou - konstruktorem.
     *
     * @param clazz
     *         - třída, ze které se má získat výchozí konstruktor.
     * @return výchozí konstruktor nebo null.
     */
	private static Constructor<?> getDeclaredConstructor(final Class<?> clazz) {
		try {

			return clazz.getDeclaredConstructor();

		} catch (NoSuchMethodException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informaci o chybě:
				logger.log(Level.INFO, "Zachycena výjimka při získávání konstruktoru třídy: " + clazz.getName());

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();
		}

		return null;
	}
	
	
	
	
	
	

	
	/**
	 * Metoda, která spustí vlákno, které otestuje vztahy mezi instancemi
	 */
	private void checkRelationsBetweenInstances() {
		ThreadSwingWorker.runMyThread(
				new CheckRelationShipsBetweenInstances(instanceDiagram, instanceDiagram.isShowRelationShipsToItself(),
						GraphInstance.isUseFastWay(), GraphInstance.isShowRelationshipsInInheritedClasses(),
						instanceDiagram.isShowAssociationThroughAggregation()));
	}
	
	

	
	

	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			txtMethodIsNotPublic = properties.getProperty("Owi_Txt_MethodIsNotPublic", Constants.OWI_TXT_METHOD_IS_NOT_PUBLIC);
			txtMethodIsNotInInstanceOfClass = properties.getProperty("Owi_Txt_MethodIsNotInstanceOfCLass", Constants.OWI_TXT_METHOD_IS_NOT_INSTANCE_OF_CLASS);
			txtErrorInCallTheMethod = properties.getProperty("Owi_Txt_ErrorInCallTheMethod", Constants.OWI_TXT_ERROR_IN_CALL_THE_METHOD);
			txtErrorDuringCreateInstance = properties.getProperty("Owi_Txt_ErrorDuringCreateInstance", Constants.OWI_TXT_ERROR_DURING_CREATE_INSTANCE);
			txtPossibleCousesCreateInstanceError = properties.getProperty("Owi_Txt_PossibleCausesCreateInstanceError", Constants.OWI_TXT_POSSIBLE_CAUSES_CREATE_INSNTANCE_ERROR);
			txtClass = properties.getProperty("Owi_Txt_Class", Constants.OWI_TXT_CLASS);
			txtCannotCreateInstanceFromAbstractClass = properties.getProperty("Owi_Txt_CannotCreateInstanceFromAbstractClass", Constants.OWI_TXT_CANNOT_CREATE_INSTANCE_FROM_ABSTRACT_CLASS);
			txtConstructorIsNotPublic = properties.getProperty("Owi_Txt_ConstructorIsNotPublic", Constants.OWI_TXT_CONSTRUCTOR_IS_NOT_PUBLIC);
			txtConstructorWantDifferentCountOrTypesOfParameters = properties.getProperty("Owi_Txt_ConstructorWantDifferentCountOrTypesOfParameters", Constants.OWI_TXT_CONSTRUCTOR_WANT_DIFFERENT_COUNT_OR_TYPES_OF_PARAMETERS);
			txtConstructorWant = properties.getProperty("Owi_Txt_ConstructorWant", Constants.OWI_TXT_CONSTRUCTOR_WANT);			
			txtErrorWhileCallingConstructorByThrowAnException = properties.getProperty("Owi_Txt_ErrorWhileCallingConstructorByThrowAnException", Constants.OWI_TXT_ERROR_WILE_CALLING_CONSTRUCTOR_BY_THROW_AN_EXCEPTION);			
			txtButGot = properties.getProperty("Owi_Txt_ButGot", Constants.OWI_TXT_BUT_GOT);
			txtErrorDuringCallBaseConstructor = properties.getProperty("Owi_Txt_ErrorDuringCallBaseConstructor", Constants.OWI_TXT_ERROR_DURING_CALL_BASE_CONSTRUCTOR);
			txtButDidGot = properties.getProperty("Owi_Txt_ButDidGot", Constants.OWI_TXT_BUT_DID_GOT);
			txtReferenceNameExistChange = properties.getProperty("Owi_Txt_ReferenceNameExistMustChange", Constants.OWI_TXT_REFERENCE_NAME_EXIST_MUST_CHANGE);
			txtReference = properties.getProperty("Owi_Txt_Reference", Constants.OWI_TXT_REFERENCE);
			
			// Texty do metody callConstructor:						
			txtFailedToCallConstructorInAbstractClass = properties.getProperty("Owi_Txt_Failed_To_Call_Constructor_In_Abstract_Class", Constants.OWI_TXT_FAILED_TO_CALL_CONSTRUCTOR_IN_ABSTRACT_CLASS);
			txtInsertedParameters = properties.getProperty("Owi_Txt_Inserted_Parameters", Constants.OWI_TXT_INSERTED_PARAMETERS);
			txtRequiredParameters = properties.getProperty("Owi_Txt_Required_Parameters", Constants.OWI_TXT_REQUIRED_PARAMETERS);
			txtFailedToCallConstructor = properties.getProperty("Owi_Txt_Failed_To_Call_Constructor", Constants.OWI_TXT_FAILED_TO_CALL_CONSTRUCTOR);
			txtConstructorIsNotAccessible = properties.getProperty("Owi_Txt_Constructor_Is_Not_Accessible", Constants.OWI_TXT_CONSTRUCTOR_IS_NOT_ACCESSIBLE);
			txtArgumentsError = properties.getProperty("Owi_Txt_Argument_Error", Constants.OWI_TXT_ARGUMENT_ERROR);
			txtConstructorThrowAnException = properties.getProperty("Owi_Txt_Constructor_Throw_An_Exception", Constants.OWI_TXT_CONSTRUCTOR_THROW_AN_EXCEPTION);
			
			// Text pro objekt Called - kvůli výpísu referencí v případě instance z diagramu
			// instancí:
			txtReferenceVariable = properties.getProperty("Owi_Txt_ReferenceVariable", Constants.OWI_TXT_REFERENCE_VARIABLE);
		}
		
		else {
			txtMethodIsNotPublic = Constants.OWI_TXT_METHOD_IS_NOT_PUBLIC;
			txtMethodIsNotInInstanceOfClass = Constants.OWI_TXT_METHOD_IS_NOT_INSTANCE_OF_CLASS;
			txtErrorInCallTheMethod = Constants.OWI_TXT_ERROR_IN_CALL_THE_METHOD;
			txtErrorDuringCreateInstance = Constants.OWI_TXT_ERROR_DURING_CREATE_INSTANCE;
			txtPossibleCousesCreateInstanceError = Constants.OWI_TXT_POSSIBLE_CAUSES_CREATE_INSNTANCE_ERROR;
			txtClass = Constants.OWI_TXT_CLASS;
			txtCannotCreateInstanceFromAbstractClass = Constants.OWI_TXT_CANNOT_CREATE_INSTANCE_FROM_ABSTRACT_CLASS;
			txtConstructorIsNotPublic = Constants.OWI_TXT_CONSTRUCTOR_IS_NOT_PUBLIC;
			txtConstructorWantDifferentCountOrTypesOfParameters = Constants.OWI_TXT_CONSTRUCTOR_WANT_DIFFERENT_COUNT_OR_TYPES_OF_PARAMETERS;
			txtConstructorWant = Constants.OWI_TXT_CONSTRUCTOR_WANT;
			txtErrorWhileCallingConstructorByThrowAnException = Constants.OWI_TXT_ERROR_WILE_CALLING_CONSTRUCTOR_BY_THROW_AN_EXCEPTION;
			txtButGot = Constants.OWI_TXT_BUT_GOT;
			txtErrorDuringCallBaseConstructor = Constants.OWI_TXT_ERROR_DURING_CALL_BASE_CONSTRUCTOR;
			txtButDidGot = Constants.OWI_TXT_BUT_DID_GOT;
			txtReferenceNameExistChange = Constants.OWI_TXT_REFERENCE_NAME_EXIST_MUST_CHANGE;
			txtReference = Constants.OWI_TXT_REFERENCE;
			
			// Texty do metody callConstructor:						
			txtFailedToCallConstructorInAbstractClass = Constants.OWI_TXT_FAILED_TO_CALL_CONSTRUCTOR_IN_ABSTRACT_CLASS;
			txtInsertedParameters = Constants.OWI_TXT_INSERTED_PARAMETERS;
			txtRequiredParameters = Constants.OWI_TXT_REQUIRED_PARAMETERS;
			txtFailedToCallConstructor = Constants.OWI_TXT_FAILED_TO_CALL_CONSTRUCTOR;
			txtConstructorIsNotAccessible = Constants.OWI_TXT_CONSTRUCTOR_IS_NOT_ACCESSIBLE;
			txtArgumentsError = Constants.OWI_TXT_ARGUMENT_ERROR;
			txtConstructorThrowAnException = Constants.OWI_TXT_CONSTRUCTOR_THROW_AN_EXCEPTION;
			
			// Text pro objekt Called - kvůli výpísu referencí v případě instance z diagramu
			// instancí:
			txtReferenceVariable = Constants.OWI_TXT_REFERENCE_VARIABLE;
		}
	}
}