package cz.uhk.fim.fimj.instances;

import java.util.*;
import java.util.stream.Collectors;

import cz.uhk.fim.fimj.commands_editor.Variable;
import cz.uhk.fim.fimj.commands_editor.VariableAbstract;
import cz.uhk.fim.fimj.commands_editor.VariableArray;
import cz.uhk.fim.fimj.commands_editor.VariableList;
import cz.uhk.fim.fimj.file.OutputEditorInterface;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;

/**
 * Tato třída obsahuje hasMapu s veškerými uživatelem vytvořenými instancemi vytvořených tříd v aplikace
 * <p>
 * Třída se vytvoři se spuštěním aplikace a do ostatních tříd - kde je potřeba přiístup k těmto intancím se předává
 * pouze reference na tuto třídu
 * <p>
 * Po zavření projektu či aplikace se tato mapa vymaže
 * <p>
 * Třída obashuje metody pro přidání, odebrání, smazání jedné instance a smazání všech instancí s hasmapy
 * <p>
 * <p>
 * Do hashMapy jsou zadávány hodnoty typu text a objekt První hodnota - klíč je název proměnné odkazující na instancí
 * nějaké třídy Druhá hodnota - objekt je konkrétní vytvoření instance dané třídy
 * <p>
 * Note:
 * Pro více objektové řešení tu je možnost, že by nebyla hashMapa s objekty a názvy referencí ale byl by nějaký
 * objekt (třída), která by osahovala atribnuty pro název reference, samotnou instanci a pro pdejiti testovani podminek
 * by jeste mohla mit promennou vyctoveho typu, která by definovala o jaky typ se jedna (instance tčridy z diagramu
 * trid, nebo promenna v editoru prikazu typu pole, list, ...), ale takto jsem si ušetřil dve tridy (objekt a vycet) a
 * pouze si definuji jednoznačné podminky, ale jded jen o pristup.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class Instances {

	private Instances() {
	}

	/**
	 * Proměnná, ve které budou uloženy veškeré uživatelem vytvořené instance tříd z
	 * diagramu tříd: a i instance tříd, které reprezentují proměnné vytvoření v
	 * editoru příkazů.
	 * 
	 * Původně jsem chtěl ty vytvořené instance tříd z diagramu tříd dávat třeba do
	 * této mapy a na ostatní proměnné, které uživatel vytvořil v editoru příkazů do
	 * jiné mapy. Třeba pro každý typ proměnné jednu mapu apod. Pak by ale bylo
	 * složitější testování, například, kdybych měl třeba 10 typů proměnných, pak
	 * bych musel zvláště psát deset metod, které testují nějaké reference, protože
	 * dostanu třeba název reference a měl bych zjistit, na jaký objekt ukazuje -
	 * jestli na instanci nebo na proměnnou z editoru příkazů, na to by bylo možné
	 * napsat metodu, ale pak by metoda vrátila jen ten objekt a ten bych pak musel
	 * prohnat další metodou na otestování čeho je ten objekt instancí (instanceof),
	 * to už se mi takto nechtělo "kompikovat", tak je vše na jednom místě - v této
	 * mapě a jen při získání potřebné hodnoty testuji hodnotu, kterou chci získat.
	 */
	private static final HashMap<String, Object> mapInstances = new HashMap<>();
	
	

	
	/**
	 * Referenci na diagram instncí, konkrétně na instanci třídy GraphInstance.
	 */
	private static GraphInstance instanceDiagram;
	
	
	

	
	/**
	 * Metoda, která přidá novou instanci do mapy:
	 * 
	 * @param classNameVariable
	 *            - název proměnné odkazující na instanci dané třídy
	 * 
	 * @param instance
	 *            - instance dané třídy
	 */
	public static void addInstanceToMap(final String classNameVariable, final Object instance) {
		mapInstances.put(classNameVariable, instance);
	}
	
	
	

	
	
	/**
	 * Metoda, která zjistí, zda se v hashmapě nachází název reference s daným
	 * názvem
	 * 
	 * @param referenceName
	 *            - název reference na instanci třídy nebo na prměnnou
	 * 
	 * @return true, pokud se v mapě již nachází reference s daným názvem, jinal
	 *         false
	 */
	public static boolean isReferenceNameInMap(final String referenceName) {
		return mapInstances.containsKey(referenceName);		
	}
	
	
	

	
	/**
	 * Metoda, která z mapy instancí vytáhne tu, pod názvem dané reference třídy:
	 * 
	 * @param classNameVariable
	 *            - název proměnné (reference), které odkazuje na instanci příslušné
	 *            třídy
	 * 
	 * @return instanci příslušné třídy, na kterou odkazuje text v parametru
	 */
	public static Object getInstanceFromMap(final String classNameVariable) {
		// Otestuji, zda se v mapě nachází reference s daným názvem a zda se nejedná o
		// instance "proměnné" v editoru příkazů (tj. Variable, VariableLIst,
		// a VariableArray), ale instanci nějaké třidy z diagramu tříd:

		// Note - nemusím hasMapu procházet cyklem, každá hodnota coby reference by se v
		// ní měla nacházet pouze jednou!

		if (mapInstances.containsKey(classNameVariable) && isValueInstanceFromCd(mapInstances.get(classNameVariable)))
			return mapInstances.get(classNameVariable);

		return null;
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda zadaný název je název ukazující na proměnnou v
	 * vytvořenou v editoru příkazů (instance třídy Variable - moje vytvoření, né
	 * "Javyovksá"), pokud v mapě najde takovou hodnotu, na kterou ukazuje zadaný
	 * text a jedná se o instanci třídy Variable, tak ho vrátí
	 * 
	 * @param editorVariableName
	 *            - název reference na proměnnou
	 * 
	 * @return instanci třídy Variable, na kterou ukazuje zadaný text -reference
	 *         nebo null - nebude li proměnná nalezena
	 */
	public static Object getVariableFromMap(final String editorVariableName) {
		// Otestuji, zda mapa obshuje název prměnné a jedná se o instanci proměnné a ne instanci třídy:
		if (mapInstances.containsKey(editorVariableName) && mapInstances.get(editorVariableName) instanceof Variable)
			return mapInstances.get(editorVariableName);

		return null;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda zadaný název, resp. reference ukazuje na instanci
	 * třídy VariableList, který reprezentuje instanci kolekce List, konkrétně
	 * instanci ArrayListu.
	 * 
	 * Pokud se najde v mapě objekt, pod daným názvem a zároveň je daný objekt
	 * instancí třídy VariableList, tak ho vrátí, jinak vrátí null - nenašla se
	 * taková instnace
	 * 
	 * @param listVariableName
	 *            = název reference na list
	 * 
	 * @return daný list - instnace třídy VariableLIst nebo null, pokud nebude
	 *         instnace nalezena
	 */
	public static Object getListFromMap(final String listVariableName) {
		if (mapInstances.containsKey(listVariableName) && mapInstances.get(listVariableName) instanceof VariableList<?>)
			return mapInstances.get(listVariableName);

		// Nemělo by nastat:
		return null;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která se pokusí vrátit intancí třídy VariableArray, na kterou má
	 * ukazovat nějaká reference, neboli pole pod nějakým názvem
	 * 
	 * Metoda si nejprve otestuje, zda je v mapě vůbec nejaký objekt pod daným
	 * názvem reference, pak otestuje, zda se jedná o instanci třídy VariabelArray
	 * (pokud bude objekt pod daným názvem nalezen) a pokud jsou tyto podmínky
	 * splněny, vrátí tento objekt (instanci)
	 * 
	 * @param arrayVariableName
	 *            - název proměnné, která by měla ukazovat na nějaké pole (instancí
	 *            třídy VariableArray)
	 * 
	 * @return null nebo instanci třídy VariableArray - pokud bude nalezena dle
	 *         podmínek uvedených výše
	 */
	public static Object getArrayFromMap(final String arrayVariableName) {
		if (mapInstances.containsKey(arrayVariableName)
				&& mapInstances.get(arrayVariableName) instanceof VariableArray<?>)
			return mapInstances.get(arrayVariableName);

		// Nemělo by nastat:
		return null;
	}
	
	
	
	
	
	

	
	
	/**
	 * Metoda, která z mapy vymaže objekt s příslušným klíčem - v tomto případě se
	 * jedná o název proměné odkazující na instanci třídy
	 * 
	 * @param classNameVariable
	 *            - název promenné odkazující na nějakou instanci nějaké třídy
	 */
	public static void deleteInstanceFromMap(final String classNameVariable) {
		mapInstances.remove(classNameVariable);
	}
	
	
	
	/**
	 * Metoda, která vymaže z mapy veškeré instance - veškeré objekty z mapy
	 */
	public static void deleteAllInstancesFromMap() {
		mapInstances.clear();
		
	}







	/**
	 * Metoda, která vrátí list typu String, ve kterém budou názvy všech instancí
	 * resp, názvy referencí na instance tříd vytvořených v diagramu instancí nebo v
	 * editoru příkaů (dočasné proměnné v editoru příkazů)
	 *
	 * @return list referencí na instance tříd v diagramu instancí
	 */
	public static List<String> getListOfVariablesOfClassInstances() {
		/*
		 * V podstatě jen převedu všechny hodnoty v mapě coby klíče v mapě do listu a
		 * vrátím jej:
		 */
		return new ArrayList<>(mapInstances.keySet());
	}
	
	
	
	
	
	

	
	/**
	 * Metoda, která vrátí veškeré vytvořené instance, resp. celou mapu s nimi
	 * 
	 * @return hashMapu s instancemi
	 */
	public static Map<String, Object> getAllInstances() {
		return mapInstances;
	}
	

	
	
	
	/**
	 * Metoda, která vrátí všechny instance tříd, které se nachází v diagramu tříd
	 * 
	 * @return HashMapu, která obsahuje instance tříd, které se nachází v diagramu
	 *         tříd
	 */
	public static Map<String, Object> getAllInstancesOfClassInCd() {
		return mapInstances.entrySet().stream().filter(f -> isValueInstanceFromCd(f.getValue()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}
	
	
	

	
	
	
	/**
	 * Metoda, vrátí mapu, která obsahuje veškeré proměnné vytvořené v editoru
	 * příkazů, tzn. že se vrátí všechny hodnota, které nejsou instance tříd z
	 * diagramu tříd. resp. jsou to instance tříd Variable, VariableArray a
	 * VariableList.
	 * 
	 * @return výše popsanou mapu se zmíněnými hodnotami.
	 */
	public static Map<String, Object> getAllVariablesFromCommandEditor() {
		/*
		 * Note:
		 * Jelikož instance tříd z diagramu tříd jsou pouze "obejkty", pak nelze určit,
		 * čeho konkrétně je to instance, tak musím otestovat, zda to je instance tříd
		 * Variable, VariableArray nebo VariableList, a pokud nějakou doplním, pak je
		 * třeba jej doplnit i zde.
		 */
		
		return mapInstances.entrySet().stream().filter(f -> !isValueInstanceFromCd(f.getValue()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}



    /**
     * Získání veškerých proměnných, které uživatel vytvořil v editoru příkazů a jsou datového typu "klasické" proměnné
     * - nejedná se o datovou strukturu.
     * <p>
     * Například int, String, boolean, double, ...
     *
     * @return mapu, která bude obsahovat proměnné, které uživatel vytvořil v editoru příkazů a nejedná se o instance
     * nebo datovou strukturu - "klasické" proměnné. Klíč je reference na proměnnou, hodnota je objekt s proměnnou.
     */
    public static Map<String, Variable> getClassicVariablesFromCommandEditor() {
        return mapInstances.entrySet().stream().filter(i -> i.getValue() instanceof Variable).collect(Collectors
                .toMap(Map.Entry::getKey, v -> (Variable) v.getValue()));
    }


    /**
     * Získání veškerých proměnných datového typu List, které uživatel vytvořil pomocí editoru příkazů.
     *
     * @return mapu, která bude obsahovat uživatelem vytvořené proměnné v editoru příkazů typu List. Klíč je reference,
     * hodnota je konkrétní objekt obsahující uživatelem vytvořený list.
     */
    public static Map<String, VariableList<?>> getListVariablesFromCommandEditor() {
        return mapInstances.entrySet().stream().filter(i -> i.getValue() instanceof VariableList<?>).collect
                (Collectors.toMap(Map.Entry::getKey, v -> (VariableList<?>) v.getValue()));
    }


    /**
     * Získání veškerých proměnných, které uživatel vytvořil v editoru příkazů a jsou datového typu jednorozměrné pole.
     *
     * @return mapu, která bude obshaovat uživatelem vytvořené jednorozměrné pole v editoru příkazů. Klíč je reference
     * na proměnnou, hodnota je konkrétní objekt obsahující uživatelem vytvořené pole.
     */
    public static Map<String, VariableArray<?>> getArrayVariablesFromCommandEditor() {
        return mapInstances.entrySet().stream().filter(i -> i.getValue() instanceof VariableArray<?>).collect
                (Collectors.toMap(Map.Entry::getKey, v -> (VariableArray<?>) v.getValue()));
    }
	
	
	
	

	/**
	 * Klasický setr na nastavení reference na diagm instancí pro předání do metody
	 * na přenačtení instancí v mapě po zkompilování tříd
	 * 
	 * @param instanceDiagram
	 *            - reference na diagram instancí
	 */
	public static void setInstanceDiagram(final GraphInstance instanceDiagram) {
		Instances.instanceDiagram = instanceDiagram;
	}
	

	
	
	
	
	/**
	 * Metoda, která zavolá metodu pro přenačtení instancí tříd z diagramu tříd v
	 * mapě, vysvětlení u konkrétní metody.
	 * 
	 * @param pathToBinDir
	 *            - cesta k adresáři bin v otevřeném projěktu - musí existovat
	 * 
	 * @param outputEditor
	 *            - reference na editor výstupů pro oznámení nějaké chyby
	 * 
	 * @param languageProperties
	 *            - soubor s načtenými texty
	 * 
	 * @param refreshInstanceDiagram
	 *            - logická proměnná o tom, zda se mají přenačist vztahy mezi
	 *            instancemi v diagramu tříd.
	 */
	public static void refreshClassInstances(final String pathToBinDir, final OutputEditorInterface outputEditor,
			final Properties languageProperties, final boolean refreshInstanceDiagram) {
		RefreshInstances.refreshClassInstances(pathToBinDir, outputEditor, languageProperties, instanceDiagram,
				refreshInstanceDiagram);
	}
	
	
	
	

	
	
	/**
	 * Metoda, která z HashMapy vymaže všechny objekty coby instance tříd z diagramu
	 * tříd.
	 * 
	 * "vymažou se z mapy všechny vytvořené instance tříd z diagramu tříd"
	 */
	public static void removeAllClassInstances() {
		/*
		 * Projdu celou mapu a odeberu vse, co je instance, resp. není to instance
		 * VariableList, Variable nebo VariableArray
		 */
		mapInstances.entrySet().removeIf(Instances::isValueInstanceFromCd);
	}
	

	
	
	
	/**
	 * Metoda, která zjistí, zda objekt value je instance nějaké třídy z diagramu
	 * tříd, to se pozná tak, že se zjistí, zda to není instance třídy Variable,
	 * VariableList a VariableArray.
	 * 
	 * @param value
	 *            - intance nějaké třídy viz výše.
	 * 
	 * @return true, pokud se jedná o instanci nějaké třídy z diagramu tříd
	 *         (zjištění v popisu výše), jinak false.
	 */
	private static boolean isValueInstanceFromCd(final Object value) {
		return !(value instanceof Variable) && !(value instanceof VariableList<?>)
				&& !(value instanceof VariableArray<?>);
	}
	
	
	
	
	
	
	

	
		
	
	/**
	 * Metoda, která otestuje, zda je objekt objInstance instance nějaké třídy,
	 * která se aktuálně nachází v diagramu instnací, resp. je v mapě jako jedna z
	 * hodnot pro instanci.
	 * 
	 * @param objInstance
	 *            - nějaká hodnota, o které se má zjistit, zda je to instance nějaké
	 *            třídy, která je reprezentována v diagramu instací, neboli, je to
	 *            instance, která se nachází v mapě.
	 * 
	 * @return true, pokud je objIntance instance nějaké třídy, která se nachází v
	 *         mapě, jinak false.
	 */
	public static boolean isInstanceInMap(final Object objInstance) {
		final Map<String, Object> map = getAllInstancesOfClassInCd();

		return map.values().stream().anyMatch(i -> i.equals(objInstance));
	}
	
	
	
	

	
	
	
	/**
	 * Metoda, která otestuje, zda je objekt objInstance instance nějaké třídy,
	 * která se aktuálně nachází v diagramu instnací, resp. je v mapě jako jedna z
	 * hodnot pro instanci.
	 * 
	 * A pokud se takový objekt najde, tj. objInstance je instance nějaké třídy,
	 * který se nachází v mapě jako instance z diagramu instnací, tak se vrátí
	 * reference na tuto instanci.
	 * 
	 * @param objInstance
	 *            - nějaká hodnota, o které se má zjistit, zda je to instance nějaké
	 *            třídy, která je reprezentována v diagramu instací, neboli, je to
	 *            instance, která se nachází v mapě.
	 * 
	 * @return reference na instanci objInstance nebo null, pokud objInstance nebude
	 *         v mapě nalezena ebo to není instnace třídy, která se nachází v
	 *         diagramu instancí.
	 */
	public static String getReferenceToInstanceFromCdInMap(final Object objInstance) {
		final Map<String, Object> map = getAllInstancesOfClassInCd();
		
		for (final Map.Entry<String, Object> m : map.entrySet())
			if (m.getValue().equals(objInstance))
				return m.getKey();

        return null;
    }


    /**
     * Získání veškerých proměnných, které uživatel vytvořil v editoru příkazů a jedná se vždy o typ proměnné variable.
     * Buď je to "klasická" proměnná, list nebo jednorozměrné pole základních datových typů jazyka Java.
     *
     * @param variable
     *         - proměnná, která reprezentuje list, pole nebo "klasickou" proměnnou. Dle datového typu této proměnné,
     *         listu nebo pole se vrátí existující proměnné, které nejsou final.
     * @param mustNotBeFinal
     *         - zda nesmí být proměnná final. True, pokud nesmí být proměnná final. False když na tom nezáleží -
     *         proměnná může a nemusí být final
     *
     * @return mapu, která bude obsahovat existující proměnné, které jsou stejného datového typu, jako je proměnná
     * variable (list, pole nebo "klasická" proměnná)
     */
    public static Map<String, VariableAbstract> getVariablesWithSameDataType(final VariableAbstract variable,
                                                                             final boolean mustNotBeFinal) {
        final HashMap<String, VariableAbstract> values = new HashMap<>();

        final Map<String, Object> variables = getAllVariablesFromCommandEditor();

        if (variable instanceof Variable) {
            // Přetypování pro snažší a přehlednější přístup:
            final Variable var = (Variable) variable;

            /*
             * Vyberu veškeré uživatelem vytvořené proměnné v editoru příkazů, které jsou stejného datového typu a
             * nejsou final.
             */
            variables.entrySet().stream().filter(i -> {
                if (!(i.getValue() instanceof Variable))
                    return false;

                final Variable v = (Variable) i.getValue();

                if (mustNotBeFinal && v.isFinal())
                    return false;

                return v.getDataTypeOfVariable().equals(var.getDataTypeOfVariable());

            }).forEach(i -> values.put(i.getKey(), (Variable) i.getValue()));
        }


        // Získání proměnných typu jednorozměrné pole:
        else if (variable instanceof VariableArray<?>) {
            final VariableArray<?> var = (VariableArray<?>) variable;

            variables.entrySet().stream().filter(i -> {
                if (!(i.getValue() instanceof VariableArray<?>))
                    return false;

                final VariableArray<?> v = (VariableArray<?>) i.getValue();

                if (mustNotBeFinal && v.isFinal())
                    return false;

                return v.getTypeArray().equals(var.getTypeArray());

            }).forEach(i -> values.put(i.getKey(), (VariableArray<?>) i.getValue()));
        }

        // Získání proměnných typu List:
        else if (variable instanceof VariableList<?>) {
            final VariableList<?> var = (VariableList<?>) variable;

            variables.entrySet().stream().filter(i -> {
                if (!(i.getValue() instanceof VariableList<?>))
                    return false;

                final VariableList<?> v = (VariableList<?>) i.getValue();

                if (mustNotBeFinal && v.isFinal())
                    return false;

                return v.getClassType().equals(var.getClassType());

            }).forEach(i -> values.put(i.getKey(), (VariableList<?>) i.getValue()));
        }

        return values;
    }
}