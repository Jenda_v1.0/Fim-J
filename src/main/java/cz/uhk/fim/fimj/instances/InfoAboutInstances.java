package cz.uhk.fim.fimj.instances;

/**
 * Tato třída slouží pouze pro uchování informací o instanci, která se má nahradit, dále o instanci, kterou se má ta
 * předchozí (původní) nahradit a na konec informaci o referenci na tyto instance, abych věděl, jakou instanci mám
 * nahradit.
 * <p>
 * Note: informaci o názvu reference by ani nebyla potřeba stačilo by testovat pak v HashMape u nahrazení původních
 * referencí, tak by stačilo hledat v mapě objekty které se shodují s původní referencí v této třídě.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class InfoAboutInstances {

    /**
     * Uložení reference na instanci, která se má nahradit. Resp. Jedná se o původní instanci, která se má narahdit za
     * "newInstance"
     */
    private final Object originalInstance;

    /**
     * Uložní reference na instanci, která má nrahdit tu původní - originalInstance.
     */
    private final Object newInstance;


    /**
     * následující proměnná slouží pro uchování reference na instance výše ale je tou pouze pro přehlednější / snadnější
     * vyhledání a následné nahrazení původní instance za novou v mapě se všemi instancemi, které uživatel vytvoříl:
     */
    private final String reference;


    /**
     * Konstruktor této třídy, který služí pro naplnění výše zmíněných proměnuných
     *
     * @param originalInstance
     *         - původní instance, která se má nahradit
     * @param newInstance
     *         - nová instance, kterou se má ta původní (originalInstance) nahradit
     * @param reference
     *         - String coby reference na výše uvedené instance, v tomto případě slouží pouze pro lepší vyhledání a
     *         následné nahrazení
     */
    public InfoAboutInstances(final Object originalInstance, final Object newInstance, final String reference) {
        super();

        this.originalInstance = originalInstance;
        this.newInstance = newInstance;
        this.reference = reference;
    }


    final Object getOriginalInstance() {
        return originalInstance;
    }


    public final Object getNewInstance() {
        return newInstance;
    }


    public final String getReference() {
        return reference;
    }
}