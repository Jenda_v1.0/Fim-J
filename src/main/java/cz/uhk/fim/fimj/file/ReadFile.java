package cz.uhk.fim.fimj.file;

import java.beans.XMLDecoder;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.language.EditProperties;
import cz.uhk.fim.fimj.language.KeysOrder;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.output_editor.OutputEditor;
import cz.uhk.fim.fimj.preferences.PreferencesApi;

/**
 * Tato třída obsahuje metody, které slouží převžně pro čtení či záskání nějakých dat z nějakých souborů na disku a dále
 * metody pro kompilaci tříd a získání přeložené třídy (soubor .class), apod. viz jednotlivé metody.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ReadFile implements ReadFileInterface {

	/**
	 * Při načtení jazyka pro aplikace se uloží do následující proměnné, tato
	 * proměnná slouží pouze pro získání textu do chybových hlášek v aktuálním
	 * jazyce když uživatel zvolí jazyk = OK, pak uživatel při běhu aplikace změní
	 * jazyk, a dojde k chybě, proto si z následující proměnné "vytýhnu" text v
	 * aktuálním jazyce
	 */
	private static Properties selectedLanguage;
	


	
	
	
	/**
	 * Metoda, která zjistí cestu k adresáři označný jako workspace a zda ještě
	 * existuje, pokud ano, vrátí se cestak tomuto adresáři workspace, pokud
	 * neexistuje, nebo nebude cesta získána vrátí se null.
	 * 
	 * @return cesta k adresáři označený jako workspace, pokud existuje, jinak null.
	 */
	public static String getPathToWorkspace() {
		final String pathToWorkspace = PreferencesApi.getPreferencesValue(PreferencesApi.PATH_TO_WORKSPACE, null);

		if (pathToWorkspace != null && existsDirectory(pathToWorkspace))
			return pathToWorkspace;

		return null;
	}
	
	
	


	
	
	
	
	/**
	 * Metoda, načte do proměnné typu Properties výchozí nastavení - výchozí soubor
	 * 'Default.properties' který je umístěň ve složce Workspace zvolené uživatelem
	 * 
	 * Metoda si zjistí z Default.properites v adresáři aplikace kde se nachází
	 * složka zvolená jako Workspase a vrátí ten soubor Default.properties, který se
	 * nachází v příslušném Workspace
	 * 
	 * Note:
	 * je staticka je kvůli tomu, ze tyto objekty "properties" je treba mit moznostu
	 * nacist "v kazdém" případě, protoze, například u vytvoreni instance teto třídy
	 * (ReadFile) se nacitani jazyky pro chybova hlasky, a kdyz neni ve workspace
	 * adresar z texty nebo alespon ten jeden soubor se zvolenymi texy ve zvolenem
	 * jazyce, tak potrebuji nacist ty z aplikace, jenze kdyz v te dobe neni
	 * vytvorena instance teto tridy, tak nemohu zavolat tuto metodu, proto je
	 * staticka, abych ji mohl zavolat i v konstruktoru teto tridy apod.!
	 * 
	 * @return Default.properties nebo null, pokud soubor nebude nalazen
	 */
	public static Properties getDefaultPropertiesInWorkspace() {
		final String pathToWorkspace = getPathToWorkspace();

		if (pathToWorkspace == null)
			return null;

		final String path = pathToWorkspace + File.separator + Constants.PATH_TO_DEFAULT_PROPERTIES;

		// Ujistím se, že potřebný soubor existuje, jinak vrátím null
		if (existsFile(path))
			return App.READ_FILE.getProperties(path);

		return null;
	}

	
	
	
	
	
	
	
	

	@Override
	public EditProperties getPropertiesInWorkspacePersistComments(final String fileName) {
		final String pathToWorkspace = getPathToWorkspace();

		if (pathToWorkspace == null)
			return null;

		final String path = pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME + File.separator + fileName;

		// Ujistím se, že potřebný soubor existuje, jinak vrátím null
		if (existsFile(path))
			return new EditProperties(path);

		return null;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public Properties getSelectLanguage() {
        final String pathToWorkspace = getPathToWorkspace();

        if (pathToWorkspace == null)
            // Zde nejspíše neexistuje konfigurační soubory, tak použiji ty z
            // adresáře aplikace: a Použiji výchozí jazyk, což je čeština:

            // Zde je ještě možné zobrazit dialog pro zvolení adresáře coby
            // Workspace od uživatele:
            return getDefaultLanguageFromApp();


        if (!existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
            // Zde neexistuje adresář configuration, tak se vytvoří:
            WriteToFile.createConfigureFilesInPath(pathToWorkspace);

        else {
            // Ujistím se, že existuje konfigurační soubor:
            final String fileDefaultProperties = pathToWorkspace + File.separator
                    + Constants.PATH_TO_DEFAULT_PROPERTIES;

            if (existsFile(fileDefaultProperties)) {
                final Properties defaultPropertiesInWorkspace = getProperties(fileDefaultProperties);

                if (defaultPropertiesInWorkspace != null) {
                    // Výchozí jazyk bude vždy čeština:

                    // Note:
                    // Zde bych ještě mohl načíst soubor
                    // Default.propterties z aplikace, pro případ, že by
                    // byl jiný nastavený jazyk než čeština, ale tento
                    // jazyk jsem zvolil "na
                    // pevno" v souboru, a ve vygenerovaném Jar souboru
                    // nelze změnit,
                    // akorát si ho může změnit ve Workspace v
                    // configuration, takže vždy když se nenajde,
                    // použije se čeština, jako výchozí jazyk:

                    final String language = defaultPropertiesInWorkspace.getProperty("Language",
                            Constants.DEFAULT_LANGUAGE_NAME);

                    // Zde mám výchozí jazyk, tak načtu soubor typu
                    // .properties s příslušným jazykem (pokud existuje)

                    final File fileLanguage = new File(
                            pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME + File.separator
                                    + Constants.LANGUAGE_NAME + File.separator + language + ".properties");

                    if (fileLanguage.exists()) {
                        final Properties propertiesLanguage = getProperties(fileLanguage.getAbsolutePath());

                        /*
                         * Tento krok je zcela zbytečný, stačílo by nastavit proměnnou selectedLanguage
                         * jen jednou nejlépe ve třídě App, kde se načítá jazyk pro aplikaci, ale dal
                         * jsem jej sem, protože se může stát, že se vícekrát vytvoří instance této
                         * třídy, pak by se mohla zavolat nějaká metody, kde vyskočí hláška a v takovém
                         * případě by se využily výchozí hodnoty z třídy Constants. Proto si vždy při
                         * získání jazyka naplním příslušnou proměnnou, která je statická, pak se naplní
                         * ve všech instancích této třídy (kdyby někde nebyla). Toto se mi právě že
                         * stalo, tak jsem to raději dal na toto místo, ale je také možné jej umístit do
                         * konstruktoru této třídy. Pak by ale mohlo občas dojít k chybě při startu
                         * aplikace, kdy ve workspace není adresář configuration (například).
                         *
                         * Ale stačí jej naplnit pouze jednou, pokud uživatel v nastavení změní jazyk
                         * aplikace, pak se můsí aplikace stejně restartovat, pokud dojde k nějaké
                         * chybě, pak se zde proměnná stejně nenaplní a pokud ano, pak jej stačí naplnit
                         * jen jednou.
                         */
                        if (selectedLanguage == null)
                            selectedLanguage = propertiesLanguage;

                        return propertiesLanguage;
                    }

                    else {
                        // Nejprve překopíruji soubory:
                        writeLanguagesToWorkspace();

                        /*
                         * Znovu otestji, zda se nachází příslušný soubor s texty pro tuto aplikaci na
                         * požadovaném umístění, pokud ano, pak jej načtu a informuji uživatele, že byly
                         * překopírovány příslušné konfigurační soubory do adresáře configuration ve
                         * workapce, pokud se ale ani po překopírování souborů (metoda
                         * writeLanguagesToWorkspace) nenachází příslušný soubor s texty pro tuto
                         * aplikaci na požadovaném umístění, tak zvolím výchozí hodnoty v adresíři
                         * aplikace a oznámí to uživateli.
                         */
                        if (fileLanguage.exists()) {
                            selectedLanguage = getProperties(fileLanguage.getAbsolutePath());

                            // Získám si hlášku v příslušném jazyce:
                            final String jopErrorLanguageTitle;
                            final String jopErrorLanguage1;
                            final String jopErrorLanguage2;
                            final String jopErrorLanguage3;

                            if (selectedLanguage != null) {
                                jopErrorLanguageTitle = selectedLanguage.getProperty("JopErrorLoadLanguage_Title",
                                        Constants.JOP_ERROR_LOAD_LANGUAGE_TITLE);
                                jopErrorLanguage1 = selectedLanguage.getProperty("JopErrorLoadLanguage_Text_1",
                                        Constants.JOP_ERROR_LOAD_LANGUAGE_TEXT_1);
                                jopErrorLanguage2 = selectedLanguage.getProperty("JopErrorLoadLanguage_Text_2",
                                        Constants.JOP_ERROR_LOAD_LANGUAGE_TEXT_2);
                                jopErrorLanguage3 = selectedLanguage.getProperty("JopErrorLoadLanguage_Text_3",
                                        Constants.JOP_ERROR_LOAD_LANGUAGE_TEXT_3);
                            }

                            else {
                                jopErrorLanguageTitle = Constants.JOP_ERROR_LOAD_LANGUAGE_TITLE;
                                jopErrorLanguage1 = Constants.JOP_ERROR_LOAD_LANGUAGE_TEXT_1;
                                jopErrorLanguage2 = Constants.JOP_ERROR_LOAD_LANGUAGE_TEXT_2;
                                jopErrorLanguage3 = Constants.JOP_ERROR_LOAD_LANGUAGE_TEXT_3;
                            }


                            JOptionPane.showMessageDialog(null,
                                    jopErrorLanguage1 + "\n" + jopErrorLanguage2 + "\n" + jopErrorLanguage3 + ": "
                                            + fileLanguage.getParentFile().getAbsolutePath(),
                                    jopErrorLanguageTitle, JOptionPane.INFORMATION_MESSAGE);

                            // Vrátím nově vytvořený soubor s texty pro tuto aplikaci:
                            return selectedLanguage;
                        }



                        /*
                         * Pokud se dostanu do této části kódu, pak se ani po zkopírování souborů s
                         * texty pro tuto aplikaci nepodařilo načíst potřebný soubor s texty pro tuto
                         * aplikaci, tak to oznámím uživatel a vrátím soubor, který se načte z adresáře
                         * aplikace - výchozí soubor.
                         *
                         * Tato část by nikdy neměla nastat.
                         */
                        // Získám si hlášku v příslušném jazyce:
                        final String jopErrorLanguageTitle;
                        final String jopErrorLanguage1;
                        final String jopErrorLanguage2;

                        if (selectedLanguage != null) {
                            jopErrorLanguageTitle = selectedLanguage.getProperty("JopErrorLanguageTitle",
                                    Constants.JOP_ERROR_LANGUAGE_TITLE);
                            jopErrorLanguage1 = selectedLanguage.getProperty("JopErrorLanguage1",
                                    Constants.JOP_ERROR_LANGUAGE_1 + ": ");
                            jopErrorLanguage2 = selectedLanguage.getProperty("JopErrorLanguage2",
                                    Constants.JOP_ERROR_LANGUAGE_2);
                        }

                        else {
                            jopErrorLanguageTitle = Constants.JOP_ERROR_LANGUAGE_TITLE;
                            jopErrorLanguage1 = Constants.JOP_ERROR_LANGUAGE_1 + ": ";
                            jopErrorLanguage2 = Constants.JOP_ERROR_LANGUAGE_2;
                        }


                        JOptionPane.showMessageDialog(null,
                                jopErrorLanguage1 + fileLanguage.getPath() + jopErrorLanguage2,
                                jopErrorLanguageTitle, JOptionPane.ERROR_MESSAGE);


                        // Přečtu si výchozí soubor z texty pro aplikaci:
                        return getDefaultLanguageFromApp();
                    }
                }
            }

            // Zde by se také hodilo možna načíst nový workspace nebo
            // znovu nakopíroat soubory z app
            // (copyConfigureFilesToWorkspace)
            // ZDE NEEXISTUJE VÝCHOZÍ SOUBOR - DEFAULT.PROPERTIES - tak
            // ho nakopíruji:
            else {
                /*
                 * Zde vím, že existuje adresář configuration v adresáři označným jako
                 * workspace, pouze neexistuje soubor Default.properties, tak jej vytvořím s
                 * výchozím nastavením:
                 */
                ConfigurationFiles.writeDefaultPropertiesAllNew(
                        pathToWorkspace + File.separator + Constants.PATH_TO_DEFAULT_PROPERTIES);

                return getDefaultLanguageFromApp();
            }
        }

        return null;
    }
	
	
	
	
	
	
	
	
	
	
	

	/**
	 * Metoda se používá, resp. zavolá, pokud nebude nalezen výchozí nastavený
	 * jazyk ve Workspace zvoleným uživatelem
	 * 
	 * Metoda, která z výchozího adresáře aplikace, dále v configuration,
	 * language "vytáhne" výchozí jazyk (Čeština), ten načte dou souboru typu
	 * .properties a vrátí ho
	 * 
	 * @return soubor typu .properties načtený z výchozího adresáře aplikace z
	 *         configuration, language s výchozím jazykem (Češtinou)
	 */
	private Properties getDefaultLanguageFromApp() {
        try (final Reader reader =
                     new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/" + Constants.CONFIGURATION_NAME + "/" + Constants.LANGUAGE_NAME + "/" + Constants.DEFAULT_LANGUAGE_NAME + ".properties"), StandardCharsets.UTF_8))) {

            final Properties defaultPropertiesLanguage = new Properties();

            defaultPropertiesLanguage.load(reader);

            return defaultPropertiesLanguage;

		} catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při získávání souboru s texty pro aplikaci '.Properties' z hlavního " +
                                "adresáře aplikace,"
                                + " z language!");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
		}
		return null;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která do adresáře zvoleného jako workspace (pokud existuje) zapíše
	 * adresář language s texty pro aplikaci v příslušných jazycích (pokud tedy
	 * tento adresář ještě neexistuje), pokud již existuje adresář language, pak se
	 * nic nestane , pokud neexistuje adresář configuration, vytvoří se.
	 */
	private static void writeLanguagesToWorkspace() {

		// Zjistím si cestu do Workspace:
		final String pathToWorkspace = getPathToWorkspace();

		// Otrestuje, zda workspace existuje:
		if (pathToWorkspace != null) {
			// Otestuji si složku configuration, případně, pokud neexistuje,
			// tak ji zkopíruji:
			final String fileConfiguration = pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME;

			if (existsDirectory(fileConfiguration))
				// Zde existuje adresář configuration, tak vytvořím,
				// případně přepíšu
				// adresář language a vložím do něj jazyky:
				WriteToFile.writeLanguages(new File(fileConfiguration));

			
			// ZDE NEEXISTUJE SLOŽKA CONFIGURATION VE WORKSPACU, TAK JI
			// VYTVOŘÍM:
			else
				WriteToFile.createConfigureFilesInPath(pathToWorkspace);
		}
	}

	
	
	
	
	
	
	
	
	
	@Override
    public Properties getProperties(final String path) {
        final Properties properties = new Properties();

        if (!existsFile(path)) {
            JOptionPane.showMessageDialog(null,
                    selectedLanguage.getProperty("JopMissingFileText", Constants.JOP_MISSING_FILE_ERROR_TEXT) + ": " + path,
                    selectedLanguage.getProperty("JopMissingFileTitle", Constants.JOP_MISSING_FILE_ERROR_TITLE),
                    JOptionPane.ERROR_MESSAGE);

            return null;
        }


        try (final Reader reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(path), StandardCharsets.UTF_8))) {

            properties.load(reader);

            return properties;

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při čtení souboru typu .Properties, třída ReadFile, metoda " +
                                "getProperties.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }

        return null;
    }

	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí Default.properties v adresáři aplikace
	 * 
	 * Note:
	 * Metoda je staticka je kvůli tomu, ze tyto objekty "properties" je treba mit
	 * moznostu nacist "v kazdém" případě, protoze, například u vytvoreni instance
	 * teto třídy (ReadFile) se nacitani jazyky pro chybova hlasky, a kdyz neni ve
	 * workspace adresar z texty nebo alespon ten jeden soubor se zvolenymi texy ve
	 * zvolenem jazyce, tak potrebuji nacist ty z aplikace, jenze kdyz v te dobe
	 * neni vytvorena instance teto tridy, tak nemohu zavolat tuto metodu, proto je
	 * staticka, abych ji mohl zavolat i v konstruktoru teto tridy apod.!
	 * 
	 * @return Default.properties v adresáři aplikace nebo null, pokud nebude
	 *         nalezen
	 */
	
	public static Properties getDefaultPropertiesInApp() {
		return getPropertiesFromAppOnly(Constants.DEFAULT_PROPERTIES_NAME);
	}

	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o adresář, a zda tento adresář vůbec
	 * existuje
	 * 
	 * @param path
	 *            = cesta k adresáři
	 * 
	 * @return true, pokud existuje, jinak false
	 */
	public static boolean existsDirectory(final String path) {
		final File fileDirectory = new File(path);

        return fileDirectory.exists() && fileDirectory.isDirectory();
    }





    /**
     * Zjištění, zda je soubor nebo adresář na cestě path ve workspace.
     * <p>
     * Porovnávají se pouze cesty k workspace, takže pokud path obsahuje ze začátku stejnou cestu k workspace, pak se
     * vrátí true.
     *
     * @param path
     *         - cesta k souboru nebo adresáři, o které se má zjistit, zda je to samotný adresář workspace nebo jeden z
     *         jeho podadresářů / podsouborů.
     *
     * @return true v případě, že cesta path je cesta k souboru nebo adresáři, který se nachází ve workspace, nebo se
     * jedná o workspace samotný.
     */
    public static boolean isFileInWorkspace(final String path) {
        // Cesta k workspace:
        final String workspace = getPathToWorkspace();

        /*
         * V případě, že nebyl načten workspace nebo neexistuje, pak se soubor nebo adresář na cestě path nenachází
         * ve workspace. (Neuvažují se případy, kdy byl jen například přejmenován, přesunut apod.)
         */
        if (workspace == null)
            return false;

        /*
         * V případě, že je cesta k workspace delší než cesta k souboru nebo adresáři na cestě path, pak se soubor
         * nebo adreář na cestě path určitě nenachází ve workspace.
         */
        if (workspace.length() > path.length())
            return false;

        /*
         * Zde se získá stejný řetězec / cesta k adresáři workspace na cestě path a měla by být stejná jako výše
         * získaná cesta k workspace, poku je tomu tak, by je příslušný soubor nebo adresář na cestě path ve workspace.
         */
        final String sub = path.substring(0, workspace.length());

        return sub.equals(workspace);
    }

	
	
	
	
	
	
	
	

	/**
	 * Metoda, která zjistí, zda existuje soubor na zadané cestě
	 * 
	 * @param pathToFile
	 *            - cesta k souboru
	 * 
	 * @return true, pokud soubor exsituje, jinak false
	 */
	public static boolean existsFile(final String pathToFile) {
		final File file = new File(pathToFile);

        return file.exists() && file.isFile();
    }

	
	
	
	
	
	
	
	
	
	
	@Override
	public Properties getClassDiagramProperties() {
		final String pathToWorkspace = getPathToWorkspace();

		if (pathToWorkspace != null) {
			if (existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME)) {
				if (existsFile(pathToWorkspace + File.separator + Constants.PATH_TO_CLASS_DIAGRAM_PROPERTIES))
					// Zde existuje soubor ClassDiagram.properites, tak ho načtu
					// a vrátím:
					return getProperties(pathToWorkspace + File.separator + Constants.PATH_TO_CLASS_DIAGRAM_PROPERTIES);

				// Zde neexistuje soubor ClassDiagram.properties, tak použiji
				// výchozí z adresáře aplikace:
				// pro načtení z vygenerovane aplikace:
				else
					return getPropertiesFromAppOnly(Constants.CLASS_DIAGRAM_NAME);
			}
			// Zde nexistuje adresář configuration ve Workspace, použiji výchozí
			// nastavení
			else
				return getPropertiesFromAppOnly(Constants.CLASS_DIAGRAM_NAME);
		}
		// Zde neexistuje adresář Workspace, použiji výchozí nastavení class
		// diagramu z adresáře aplikace:
		else
			return getPropertiesFromAppOnly(Constants.CLASS_DIAGRAM_NAME);
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která načte nějaký soubor typu .properties z adresáře configuration v
	 * aplikaci - je zde potřeba, aby načítala tyto soubory speciálně kvůli načítání
	 * souborů z vygenerovaného souboru .jar - coby tato aplikace.
	 * 
	 * @param filePathFromSourceFile
	 *            - pouze cesta od adresáře configuration - bez něj, ten se jako
	 *            zdroj již neuvádí
	 * 
	 * @return příslušný soubor typu .properties nebo null, pokud se nepodaří
	 *         příslušný soubor načíst.
	 */
	private static Properties getPropertiesFromAppOnly(final String filePathFromSourceFile) {
		// vytvořím si promennou typu File, která bude ukazovat na
		// classDiagram.properties v adresáři configuration
		// v adresáři aplikace, abych mohl otestovat, zda vůbec existuje, abych
		// předešel chybě:

		// Jelikož se už při vygenerovaní aplikace postarám o to, aby byly
		// potřebné konfigurační soubory
		// v adresáři configuration v aplikaci, tak zde již nemusím testovat,
		// zda soubory v aplikaci
		// existuje, proste jen jen načtu, protože vím, že tam jsou - pokud bych
		// je z toho adresáře v aplikaci,
		// úmyslně nesmazal, ale proč bych to dělal?, zde je prostě načtu:
		
		// A navíc by ani v té aplikaci být nemusely a mohly by se použít rovnou výchozí hodnoty
		// z třídy Constatnts, pak bych si tím ulehčil i trochu prácí, ale lěpe se mi pracuje 
		//se soubory na disku než s přepisováním ve třídě, navíc ve vygenerované aplikaci

		final Properties prop = new Properties();

		// Zde příslušný soubor existuje, tak ho mohu načíst (pomocí streamů) a
		// vrátit
        try (final Reader reader = new BufferedReader(new InputStreamReader(
                ReadFile.class.getResourceAsStream("/" + Constants.CONFIGURATION_NAME + "/" + filePathFromSourceFile)
                , StandardCharsets.UTF_8))) {

            prop.load(reader);

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při načítání souboru .properties z adresáře configuration ve vygenerované " +
                                "aplikaci: "
                                + filePathFromSourceFile
                                + ", nebo při zavírání InputStreamu, třída ReadFile, metoda: getPropertiesFromAppOnly" +
                                ".");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
		}

		return prop;
	}


		@Override
	public Properties getInstanceDiagramProperties() {
		final String pathToWorkspace = getPathToWorkspace();

		if (pathToWorkspace != null) {
			if (existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME)) {
				if (existsFile(pathToWorkspace + File.separator + Constants.PATH_TO_INSTANCE_DIAGRAM_PROPERTIES))
					// Zde existuje požadovaný soubor -
					// InstanceDiagram.properties, tak ho vrátím:
					return getProperties(
							pathToWorkspace + File.separator + Constants.PATH_TO_INSTANCE_DIAGRAM_PROPERTIES);

				// Zde neexistuje InstanceDiagram.prop.. ve Workspace, tak
				// vrátím ten z adresáře app - výchozí soubor
				else
					return getPropertiesFromAppOnly(Constants.INSTANCE_DIAGRAM_NAME);
			}
			// Zde nextistuje adresář configuration, použiji výchozí nastavení
			else
				return getPropertiesFromAppOnly(Constants.INSTANCE_DIAGRAM_NAME);
		}
		// Zde neexistuje Workspace, použiji výchozí nastavení:
		else
			return getPropertiesFromAppOnly(Constants.INSTANCE_DIAGRAM_NAME);
	}

	
	
	
	
	
	
	
	
	
	
	@Override
	public String getPathToSrc() {
        final String missingSrcDirectoryText;
        final String missingSrcDirectoryTitle;
        final String missingProjectDirectoryText;
        final String missingProjectDirectoryTitle;

		if (selectedLanguage != null) {
			missingProjectDirectoryText = selectedLanguage.getProperty("TacMissingProjectDirectoryText",
					Constants.TAC_MISSING_PROJECT_DIRECTORY_TEXT) + ": ";
			missingProjectDirectoryTitle = selectedLanguage.getProperty("TacMissingProjectDirectoryTitle",
					Constants.TAC_MISSING_PROJECT_DIRECTORY_TITLE);
			missingSrcDirectoryText = selectedLanguage.getProperty("TacMissingSrcDirectoryText",
					Constants.TAC_MISSING_SRC_DIRECTORY_TEXT);
			missingSrcDirectoryTitle = selectedLanguage.getProperty("TacMissingSrcDirectoryTitle",
					Constants.TAC_MISSING_SRC_DIRECTORY_TITLE);
		}

		else {
			missingProjectDirectoryText = Constants.TAC_MISSING_PROJECT_DIRECTORY_TEXT + ": ";
			missingProjectDirectoryTitle = Constants.TAC_MISSING_PROJECT_DIRECTORY_TITLE;
			missingSrcDirectoryText = Constants.TAC_MISSING_SRC_DIRECTORY_TEXT;
			missingSrcDirectoryTitle = Constants.TAC_MISSING_SRC_DIRECTORY_TITLE;
		}

		final String pathToProject = PreferencesApi.getPreferencesValue(PreferencesApi.OPEN_PROJECT, null);

		// Zde byla nalezena nějaká cesta, tak otestuji, zda se nevrátilo null a
		// je to opravdu cesta k adresář, který ještě existuje:
		if (pathToProject != null && existsDirectory(pathToProject)) {

			// Otestuji existenci adresáře src
			final String pathToSrc = pathToProject + File.separator + "src";

			if (existsDirectory(pathToSrc))
				return pathToSrc;

			// Note:
			// Zde vím, že existuje adresář projektu, tak jsem mohl vytvořit
			// příslušný adresář zde, ale nevím, jaké
			// má uživatel úmysly, třeba si chtěl jen vyzkoušet jiný adresář
			// src, například, aby nevytvářel dva jiné projekty,
			// tak jen prohodil adresáře s třídami a aktualizoval diagram, apod.
			// nebo src adresř jen omylem přejmenoval, apod.
			// nebo jej úmyslně nebo omyslem smazal, apod. je těžké toto
			// předpovědět, tak to pouze oznámím uživateli,
			// a ten může buď otevřít jiný projekt nebo znovu vytviřt adresář
			// src, a přenačíst diagram, apod. možností je mnoho...
			else
				JOptionPane.showMessageDialog(null, missingSrcDirectoryText + pathToSrc, missingSrcDirectoryTitle,
						JOptionPane.ERROR_MESSAGE);
		}

		// Note:
		// zde je možné příslušný adresář znovu vytvořit, ale nevím jaké má
		// uživatel úmysly, smazal ho náhodou, úmyslně,
		// zapměl, že je příslušný projekt otevřen v aplikaci, apod. Tak to puze
		// oznámím uživateli, a ten si už může
		// příslušný adresář vytvořit znovu - název mu to úkaže, nebo stačít
		// příslušný projekt zavřít a otevřít jiný, nebo
		// rovnou otevřít jiný, apod. ...
		else
			JOptionPane.showMessageDialog(null, missingProjectDirectoryText + pathToProject,
					missingProjectDirectoryTitle, JOptionPane.ERROR_MESSAGE);

		return null;
	}

	
	
	
	
	
	
	
	
	
	@Override
	public String getPathToBin() {
		final String missingProjectDirectoryText;
        final String missingProjectDirectoryTitle;
        final String txtFailedToCreateBinDirText;
        final String txtFailedToCreateBinDirTitle;

		if (selectedLanguage != null) {
			missingProjectDirectoryText = selectedLanguage.getProperty("TacMissingProjectDirectoryText",
					Constants.TAC_MISSING_PROJECT_DIRECTORY_TEXT) + ": ";
			missingProjectDirectoryTitle = selectedLanguage.getProperty("TacMissingProjectDirectoryTitle",
					Constants.TAC_MISSING_PROJECT_DIRECTORY_TITLE);
			txtFailedToCreateBinDirText = selectedLanguage.getProperty("Rf_FailedToCreateBinDirText",
					Constants.RF_TXT_FAILED_TO_CREATE_BIN_DIR_TEXT);
			txtFailedToCreateBinDirTitle = selectedLanguage.getProperty("Rf_FailedToCreateBinDirTitle",
					Constants.RF_TXT_FAILED_TO_CREATE_BIN_DIR_TITLE);
		} else {
			missingProjectDirectoryText = Constants.TAC_MISSING_PROJECT_DIRECTORY_TEXT + ": ";
			missingProjectDirectoryTitle = Constants.TAC_MISSING_PROJECT_DIRECTORY_TITLE;
			txtFailedToCreateBinDirText = Constants.RF_TXT_FAILED_TO_CREATE_BIN_DIR_TEXT;
			txtFailedToCreateBinDirTitle = Constants.RF_TXT_FAILED_TO_CREATE_BIN_DIR_TITLE;
		}

		final String pathToProject = PreferencesApi.getPreferencesValue(PreferencesApi.OPEN_PROJECT, null);

		if (pathToProject != null) {

			// Otestuji existenci adresáře bin
			final String pathToBin = pathToProject + File.separator + "bin";

			if (existsDirectory(pathToBin))
				return pathToBin;

			else {
				// Zde adresář bin neexistue v adresáři projektu, tak ho znovu
				// vytvořím:
				try {

					Files.createDirectories(Paths.get(pathToBin));

					return pathToBin;

				} catch (IOException e) {
                    /*
                     * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                     * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                     * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                     * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                     */
                    final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                    // Otestuji, zda se podařilo načíst logger:
                    if (logger != null) {
                        // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                        // Nejprve mou informaci o chybě:
                        logger.log(Level.INFO,
                                "Zachyceny výjimka při vytváření adresáře bin v aktuálně otevřeném projektu, třída " +
                                        "ReadFile, metoda: getPathToBin. Cesta: "
                                        + pathToBin);

                        /*
                         * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                         * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                         * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                         */
                        logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                    }
                    /*
                     * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                     */
                    ExceptionLogger.closeFileHandler();

					JOptionPane.showMessageDialog(null, txtFailedToCreateBinDirText, txtFailedToCreateBinDirTitle,
							JOptionPane.ERROR_MESSAGE);
				}
			}

		}

		else
			JOptionPane.showMessageDialog(null, missingProjectDirectoryText + pathToProject,
					missingProjectDirectoryTitle, JOptionPane.ERROR_MESSAGE);

		return null;
	}

	
	
	
	
	
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DefaultGraphCell> openClassDiagramProperties(final String path) {
		List<DefaultGraphCell> cellsList = new ArrayList<>();

		try (final InputStream in = new FileInputStream(path)) {

			final XMLDecoder dec = new XMLDecoder(in);
				final Object obj = dec.readObject();

				dec.close();

				cellsList = (ArrayList<DefaultGraphCell>) obj;

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při načítání souboru classDiagrampProperties.xml, třída ReadFile.java, " +
                                "metoda openClassDiagramProperties(), cesta: "
                                + path);

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
		}

		return cellsList;
	}

	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	@Override
	public Properties getClassDiagramFromWorkspaceOnly() {
		final String pathToWorkspace = getPathToWorkspace();

		if (pathToWorkspace != null && existsDirectory(pathToWorkspace + File.separator + Constants
				.CONFIGURATION_NAME) && existsFile
				(pathToWorkspace + File.separator + Constants.PATH_TO_CLASS_DIAGRAM_PROPERTIES))
			return getProperties(pathToWorkspace + File.separator + Constants.PATH_TO_CLASS_DIAGRAM_PROPERTIES);

		// Nějaký adresář nebo ClassDiagram.properties nebyl nalezen, tak vrátím
		// null:
		return null;
	}
	
	
	
	
	

	@Override
	public Properties getInstanceDiagramFromWorkspaceOnly() {
		final String pathToWorkspace = getPathToWorkspace();

		if (pathToWorkspace != null && existsDirectory(pathToWorkspace + File.separator + Constants
				.CONFIGURATION_NAME) && existsFile
				(pathToWorkspace + File.separator + Constants.PATH_TO_INSTANCE_DIAGRAM_PROPERTIES))
			return getProperties(
					pathToWorkspace + File.separator + Constants.PATH_TO_INSTANCE_DIAGRAM_PROPERTIES);
		return null;
	}

	
	
	
	
	
	@Override
	public Properties getCommandEditorProperties() {
		final String pathToWorkspace = getPathToWorkspace();

		if (pathToWorkspace != null) {
			if (existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME)) {
				final File fileCommandEditorProp = new File(
						pathToWorkspace + File.separator + Constants.PATH_TO_COMMAND_EDITOR_PROPERTIES);

				if (existsFile(pathToWorkspace + File.separator + Constants.PATH_TO_COMMAND_EDITOR_PROPERTIES))
					// Zde požadovaný soubor existuje, tak ho vrátím:
					return getProperties(fileCommandEditorProp.getPath());

				// Zde požadovaný soubor neexistuje, vrátím výchozí:
				else
					return getPropertiesFromAppOnly(Constants.COMMAND_EDITOR_NAME);
			}
			// Zde neexistuje adresář configuration, vrátím výchozí soubor z
			// adresáře App:
			else
				return getPropertiesFromAppOnly(Constants.COMMAND_EDITOR_NAME);
		}
		// Zde neexistuje Workspace, vrátím z výchozí adresáře:
		else
			return getPropertiesFromAppOnly(Constants.COMMAND_EDITOR_NAME);
	}

	
	
	
	
	
	
	
	@Override
	public Properties getCommandEditorPropertiesFromWorkspaceOnly() {
		final String pathToWorkspace = getPathToWorkspace();

        if (pathToWorkspace != null && existsDirectory(pathToWorkspace + File.separator + Constants
                .CONFIGURATION_NAME)) {

            final File fileCommandEditorProp = new File(
                    pathToWorkspace + File.separator + Constants.PATH_TO_COMMAND_EDITOR_PROPERTIES);

            if (existsFile(pathToWorkspace + File.separator + Constants.PATH_TO_COMMAND_EDITOR_PROPERTIES))
                // Zde požadovaný soubor existuje, tak ho vrátím:
                return getProperties(fileCommandEditorProp.getPath());
        }
		return null;
	}

	
	
	
	
	
	
	
	
	
	
	@Override
	public Properties getCodeEditorProperties() {
		final String pathToWorkspace = getPathToWorkspace();

		if (pathToWorkspace != null) {
			if (existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME)) {
				final File fileClassEditorProp = new File(
						pathToWorkspace + File.separator + Constants.PATH_TO_CODE_EDITOR_PROPERTIES);

				if (existsFile(pathToWorkspace + File.separator + Constants.PATH_TO_CODE_EDITOR_PROPERTIES))
					// Zde požadovaný soubor existuje, tak ho vrátím:
					return getProperties(fileClassEditorProp.getPath());

				// Zde požadovaný soubor neexistuje, vrátím výchozí:
				else
					return getPropertiesFromAppOnly(Constants.CODE_EDITOR_NAME);
			}
			// Zde neexistuje adresář configuration, vrátím výchozí soubor z
			// adresáře App:
			else
				return getPropertiesFromAppOnly(Constants.CODE_EDITOR_NAME);
		}
		// Zde neexistuje Workspace, vrátím z výchozí adresáře:
		else
			return getPropertiesFromAppOnly(Constants.CODE_EDITOR_NAME);
	}
	
	
	
	
	
	
	
	
	

	@Override
	public Properties getCodeEditorPropertiesFromWorkspaceOnly() {
		final String pathToWorkspace = getPathToWorkspace();

        if (pathToWorkspace != null && existsDirectory(pathToWorkspace + File.separator + Constants
                .CONFIGURATION_NAME)) {
            final File fileClassEditorProp = new File(
                    pathToWorkspace + File.separator + Constants.PATH_TO_CODE_EDITOR_PROPERTIES);

            if (fileClassEditorProp.exists() && fileClassEditorProp.isFile())
                // Zde požadovaný soubor existuje, tak ho vrátím:
                return getProperties(fileClassEditorProp.getPath());
        }
		return null;
	}
	
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	

	@Override
	public Properties getCodeEditorPropertiesForInternalFrames() {
		final String pathToWorkspace = getPathToWorkspace();

		if (pathToWorkspace != null) {
			if (existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME)) {
				final File fileCodeEditorProp = new File(
						pathToWorkspace + File.separator + Constants.PATH_TO_CODE_EDITOR_INTERNAL_FRAMES_PROPERTIES);

				if (fileCodeEditorProp.exists() && fileCodeEditorProp.isFile())
					// Zde požadovaný soubor existuje, tak ho vrátím:
					return getProperties(fileCodeEditorProp.getPath());

				// Zde požadovaný soubor neexistuje, vrátím výchozí:
				else
					return getPropertiesFromAppOnly(Constants.CODE_EDITOR_INTERNAL_FRAMES_NAME);
			}
			// Zde neexistuje adresář configuration, vrátím výchozí soubor z
			// adresáře App:
			else
				return getPropertiesFromAppOnly(Constants.CODE_EDITOR_INTERNAL_FRAMES_NAME);
		}
		// Zde neexistuje Workspace, vrátím z výchozí adresáře:
		else
			return getPropertiesFromAppOnly(Constants.CODE_EDITOR_INTERNAL_FRAMES_NAME);
	}

	
	
	
	
	
	
	
	
	
	@Override
	public Properties getCodeEditorPropertiesForInternalFramesFromWorkspaceOnly() {
		final String pathToWorkspace = getPathToWorkspace();

        if (pathToWorkspace != null && existsDirectory(pathToWorkspace + File.separator + Constants
                .CONFIGURATION_NAME)) {
            final File fileClassEditorProp = new File(
                    pathToWorkspace + File.separator + Constants.PATH_TO_CODE_EDITOR_INTERNAL_FRAMES_PROPERTIES);

            if (fileClassEditorProp.exists() && fileClassEditorProp.isFile())
                // Zde požadovaný soubor existuje, tak ho vrátím:
                return getProperties(fileClassEditorProp.getPath());
        }

		return null;
	}
	
	
	
	
	
	
	
	
	

	@Override
	public Properties getOutputEditorProperties() {
		final String pathToWorkspace = getPathToWorkspace();

		if (pathToWorkspace != null) {
			if (existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME)) {
				if (existsFile(pathToWorkspace + File.separator + Constants.PATH_TO_OUTPUT_EDITOR_PROPERTIES))
					// Zde existuje požadovaný soubor -
					// InstanceDiagram.properties, tak ho vrátím:
					return getProperties(pathToWorkspace + File.separator + Constants.PATH_TO_OUTPUT_EDITOR_PROPERTIES);

				// Zde neexistuje OutputEditor.prop.. ve Workspace, tak vrátím
				// ten z adresáře app - výchozí soubor
				else
					return getPropertiesFromAppOnly(Constants.OUTPUT_EDITOR_NAME);
			}
			// Zde nextistuje adresář configuration, použiji výchozí nastavení
			else

				return getPropertiesFromAppOnly(Constants.OUTPUT_EDITOR_NAME);
		}
		// Zde neexistuje Workspace, použiji výchozí nastavení:
		else
			return getPropertiesFromAppOnly(Constants.OUTPUT_EDITOR_NAME);
	}
	
	
	
	
	
	
	
	

	@Override
	public Properties getOutputEditorPropertiesFromWorkspaceOnly() {
		final String pathToWorkspace = getPathToWorkspace();

        if (pathToWorkspace != null && existsDirectory(pathToWorkspace + File.separator + Constants
                .CONFIGURATION_NAME)) {
            final File fileOutputEditorProp = new File(
                    pathToWorkspace + File.separator + Constants.PATH_TO_OUTPUT_EDITOR_PROPERTIES);

            if (fileOutputEditorProp.exists() && fileOutputEditorProp.isFile())
                // Zde požadovaný soubor existuje, tak ho vrátím:
                return getProperties(fileOutputEditorProp.getPath());
        }
		return null;
	}

	
	
	
	
	
	
	
	
	
	
	
	@Override
	public Properties getOutputFrameProperties() {
		final String pathToWorkspace = getPathToWorkspace();

		if (pathToWorkspace != null) {
			if (existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME)) {
				if (existsFile(pathToWorkspace + File.separator + Constants.PATH_TO_OUTPUT_FRAME_PROPERTIES))
					// Zde existuje požadovaný soubor -
					// InstanceDiagram.properties, tak ho vrátím:
					return getProperties(pathToWorkspace + File.separator + Constants.PATH_TO_OUTPUT_FRAME_PROPERTIES);

				// Zde neexistuje OutputEditor.prop.. ve Workspace, tak vrátím
				// ten z adresáře app - výchozí soubor
				else
					return getPropertiesFromAppOnly(Constants.OUTPUT_FRAME_NAME);
			}
			// Zde nextistuje adresář configuration, použiji výchozí nastavení
			else
				return getPropertiesFromAppOnly(Constants.OUTPUT_FRAME_NAME);
		}
		// Zde neexistuje Workspace, použiji výchozí nastavení:
		else
			return getPropertiesFromAppOnly(Constants.OUTPUT_FRAME_NAME);
	}

	
	
	
	
	
	
	
	
	
	@Override
	public Properties getOutputFramePropertiesFromWorkspaceOnly() {
		final String pathToWorkspace = getPathToWorkspace();

        if (pathToWorkspace != null && existsDirectory(pathToWorkspace + File.separator + Constants
                .CONFIGURATION_NAME)) {
            final File fileOutputFrameProp = new File(
                    pathToWorkspace + File.separator + Constants.PATH_TO_OUTPUT_FRAME_PROPERTIES);

            if (fileOutputFrameProp.exists() && fileOutputFrameProp.isFile())
                // Zde požadovaný soubor existuje, tak ho vrátím:
                return getProperties(fileOutputFrameProp.getPath());
        }
		return null;
	}

	
	
	
	
	
	
	
	
	
	@Override
	public Class<?> getClassFromFile(final String cellTextWithDot, final OutputEditorInterface outputEditor,
									 final List<DefaultGraphCell> cellsList, final boolean refreshInstanceDiagram,
									 final boolean updateCommandEditorValueCompletion) {
		// Zde zavolat metodu na zkomilování třídy, aby se vytvořil soubor s
		// příponou .class, a z něj mohu získat Class:

		// Získám si cestu k adresáři src a bin, pokud existuje:
		final String pathToSrc = getPathToSrc();

		final String pathToBin = getPathToBin();

		// Otestuji, zda byl adresář src nalezen a třída, která se má
		// zkompilovat a jeji přeložený - zkompilovaný soubor .class načíst,
		// tak její cesta či název a cesta skrze balíčky z adrsáře src byla
		// zadána
		if (pathToSrc != null && pathToBin != null && cellTextWithDot != null) {
			// byl nalezen adresář src, tak mohu pokračovat pro Class:

			// Vytvořím instanci třídy pro kompilaci:
			new CompileClass(selectedLanguage);

			// Jelikož je třeba zkompilovat veškeré vztahy mezi zadanými třídami
			// a bylo by z hlediska kódu poměrné náročné zjistit,
			// jaká třída má jaké vztahy s ostatními třídami z ClassDiagramu,
			// tak je získám všechny a zkompiluji je všechny najednou:
			// pomocí následující metody si vložím do proměnné
			// filesToCompileList cesty k třídám v class diagramu - pokud
			// existují:
			final List<File> filesToCompileList = getFileFromCells(pathToSrc, cellsList, outputEditor,
					selectedLanguage);

			// Zde se nepodařilo zkompilovat požadovanou třídu, možná příčina
			// jsou zmíněné vztahy mezi třídami,
			// tak je zkusím zkompilovat všechny a na základě výsledku rozhodnu,
			// zda se má načíst přeložená třída:
			// Nejprve otestuji, zda se nějaký list vrátil - zda se mají třídy
			// vůbec zkompilovat:
            if (filesToCompileList != null && CompileClass.compileClass(outputEditor, filesToCompileList,
                    refreshInstanceDiagram, updateCommandEditorValueCompletion))
                return loadCompiledClass(cellTextWithDot, outputEditor);
		}
		return null;
	}

	
	
	
	
	
	
	
	
	@Override
	public Class<?> loadCompiledClass(final String cellTextWithDot, final OutputEditorInterface outputEditor) {

		// V této metodě jsem měl dříve konkrétní postup, jenže jsem přišel na
		// to, že kvůli ClassLoderu,
		// nelze načítat třídy "pořád", protože by mezi sebou jednotlivé
		// instance tříd nemohli "komunikovat",
		// tak jsem na to napsal následující metodu pro jeden ClassLoader:
		return CompiledClassLoader.loadClass(cellTextWithDot, outputEditor);
	}
	
	
	
	
	
	
	
	
	
	
	

	@Override
    public List<File> getFileFromCells(final String pathToSrc, final List<DefaultGraphCell> cellsList,
                                       final OutputEditorInterface outputEditor, final Properties languageProperties) {
        // Musím otestovat, zda není cellsList null, protože k tomu může dojít
        // v případě, že si uživatel otevře v dialogu třídu mimo
        // diagram tříd - někde z disku, apod. - prostě "vlastní třídu", v
        // takovém případě se nebude kompilovat

        final List<File> filesList = new ArrayList<>();

        if (cellsList == null)
            return filesList;

        final String txtClassNotFound;
        final String txtPath;

        if (languageProperties != null) {
            txtClassNotFound = languageProperties.getProperty("Cp_TextClassNotFound",
                    Constants.CP_TXT_CLASS_NOT_FOUND);
            txtPath = languageProperties.getProperty("Cp_TextPath", Constants.CP_TXT_PATH);
        } else {
            txtClassNotFound = Constants.CP_TXT_CLASS_NOT_FOUND;
            txtPath = Constants.CP_TXT_PATH;
        }

        for (final DefaultGraphCell d : cellsList) {
            // Nesmí to být hrana a musí to být bunka, která v diagramu tříd
            // reprezentuje třídu:
            // Note: stačílo by testovat pouze podmínku pro bunku - ta druhá
            // je pouze prevence:
            if (!(d instanceof DefaultEdge) && GraphClass.KIND_OF_EDGE.isClassCell(d)) {
                // ZDe se nenedná o hranu
                final File file = new File(
                        pathToSrc + File.separator + d.toString().replace(".", File.separator) + ".java");

                if (existsFile(file.getPath()))
                    filesList.add(file);

                else {
                    if (outputEditor != null)
                        outputEditor.addResult(txtClassNotFound + OutputEditor.NEW_LINE_TWO_GAPS + txtPath + ": "
                                + file.getAbsolutePath());
                }
            }
        }

        return filesList;
    }
	
	

	
	
	
	
	/**
	 * Metoda, která si načte soubor Default.properties buď z workspace nebo z
	 * aplikace, a získá si hodnotu pro řazení textů v souborech .properties.
	 * Konkrétně výčtovou hodnotu KeysOrder.
	 * 
	 * Note:
	 * Tato metoda je statická, protože se volá (mimo jiné) při vytvářaní souborů s
	 * texty pro aplikaci, což se může zavolat i při vytváření instance této třídy.
	 * Takže, když se při vytvoření této třídy zavolá tato metoda, došo by k chybě,
	 * protože by ji nebylo možné zavlat, instance této třídy by byla null! Proto je
	 * tato metoda statická, aby nepotřebovala vytvoření instance této třídy.
	 * Ostatně k čemu? Nepotřebuje žádné jiné metody,které nejsou statické apod.
	 * 
	 * @return výše popsanou hodnotu KeysOrder.
	 */
    public static KeysOrder getKeysOrder() {
        // Načtu si default.properties: z workspacu (/ popř. z aplikace:)
        final Properties defaultProp = getDefaultPropAnyway();


        /*
         * Do této proměnné keysOrder si uložím hodnotu získanou z Default.properties
         * převedou na výčet a předám ji dále pro řazení jazyků v souboru .properties.
         */
        return KeysOrder.getKeysOrderByFileValue(
                defaultProp.getProperty("Default keys order", Constants.DEFAULT_KEYS_ORDER.getValueForFile()));
    }







	/**
	 * Metoda, která načte v každém případě konfigurační soubor Default.properties.
	 * <p>
	 * Nejprve se tento soubor pokusí získat z workspace, pokud se to nepovede, načte se z adresáře v aplikaci.
	 *
	 * @return načtený konfigurační soubor Default.properties buď z adresáře workspace nebo z adresáře aplikace.
	 */
	public static Properties getDefaultPropAnyway() {
		/*
		 * Načtu si Default.properties z adresáře ve workspace nebo výchozí soubor z
		 * adresáře aplikace:
		 */
		Properties defaultProp = ReadFile.getDefaultPropertiesInWorkspace();

		if (defaultProp == null)
			defaultProp = ReadFile.getDefaultPropertiesInApp();

		return defaultProp;
	}
}