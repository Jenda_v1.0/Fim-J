package cz.uhk.fim.fimj.file;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.app.KindOfOS;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.logger.ExtractEnum;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Jsou zde metody, které slouží pro různé operace s komponentami v používáním OS.
 * <p>
 * Jedná se o metody například pro otevření průzkumníku souborů, otevření prohlížece a v něm nějakou specifickou stránku
 * apod.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 08.08.2018 14:41
 */

public class DesktopSupport {


    // Texty ohledně podpory třídy Desktop:
    private static String txtErrorInfoText;
    private static String txtErrorInfoTitle;
    private static String txtOpenActionIsNotSupportedTitle;

    /*
     * Texty chybové hlášky při pokusu oo otevření souboru nebo adresáře v průzkumníku souborů v OS nebo ve výchozí
     * aplikaci pro otevření příslušného typu souboru:
     */
    private static String txtFileDoNotExistText;
    private static String txtFileDoNotExistTitle;
    private static String txtPath;
    private static String txtParentDirWasNotFoundText;
    private static String txtParentDirWasNotFoundTitle;

    /*
     * Texty pro chybové hlášky, že akce pro otevření souboru nebo adresáře není na používané platformě podporováno
     * za účelem otevření souboru nebo adresáře v průzkumníku projektů nebo ve výchozí aplikaci pro otevření
     * příslušného typu souboru:
     */
    private static String txtOpenFileActionIsNotSupportedText;
    private static String txtOpenFileActionIsNotSupportedText2;

    private static String txtOpenDirActionIsNotSupportedText;
    private static String txtOpenDirActionIsNotSupportedText2;

    /*
     * Texty pro chybové hlášky, že třída Desktop není na používané platformě podporována za účelem otevření souboru
     * nebo adresáře v průzkumníku projektů nebo ve výchozí aplikaci pro otevření příslušného typu souboru.
     */
    private static String txtFileCannotBeOpened;
    private static String txtDirCannotBeOpened;

    /**
     * Text pro chybovou hlášku o tom, že třída Desktop není podporována na používané platformě za účelem odeslání
     * emailu, resp. otevření emailového klienta:
     */
    private static String txtEmailCannotBeSent;

    /**
     * Text pro chybovou hlášku o tom, že akce pro otevření emailového klienta (/ zaslání emailu) není na používané
     * platformě podporována.
     */
    private static String txtEmailActionIsNotSupported;


    /**
     * Text pro chybovou hlášku o tom, že třída Desktop není na používané platformě podporována za účelem otevření
     * výchozího prohlížeče a v něm příslušnou webovou stránku.
     */
    private static String txtBrowserCannotBeOpened;

    /**
     * Texty pro chybovou hlášku o tom, že akce pro otevření výchozího prohlížeče a v něm příslušnou webovou stránku
     * není na používané platformě podporována.
     */
    private static String txtBrowseActionIsNotSupported;


    private DesktopSupport() {
    }

    /**
     * Metoda, která otevře adresář ve výchozím průzkumníku souborů nebo soubor ve výchozím průzkumníku souborů nebo ve
     * výchozí aplikaci zvolenou pro otevření příslušného typu souboru (nastaveno v OS).
     * <p>
     * Pokud bude openInFileExplorer true, pak se file otevře vždy v průzkumníku souborů. Ať už je to adresář nebo
     * soubor.
     * <p>
     * Pokud bude openInFileExplorer false, pak se file v případě, že je to adresář otevře ve výchozím průzkumníku
     * souborů a pokud je file nějaký soubor, pak se otevře ve výchozí zvolené aplikaci (nastaveno v OS)
     *
     * @param file
     *         - Soubor nebo adresář, který se má otevřit ve výchozí aplikaci nastavenou pro otevření příslušného typu
     *         souboru nebo adresáře v zvoleném průzkumníku souborů (dle OS).
     * @param openInFileExplorer
     *         - Označuje, zda se má file otevřít vždy v průzkumníku souborů, ať už je to soubor nebo adresář (true),
     *         jinak, když je tento parametr false, pak se file otevře v průzkumníku souborů v případě, že je file
     *         adresář. Pokud je file soubor, otevře se ve výchozí nastavení aplikaci zvolené dle OS pro otevření
     *         příslušného typu souboru.
     */
    public static void openFileExplorer(final File file, final boolean openInFileExplorer) {
        try {
            if (file == null || !file.exists()) {
                JOptionPane.showMessageDialog(null, txtFileDoNotExistText + "\n" + txtPath + ": " + file,
                        txtFileDoNotExistTitle, JOptionPane.ERROR_MESSAGE);

                return;
            }

            /*
             * Zde se nejprve otestuje, zda se má file otevřit pouze v průzkuníku souborů. Tj., že se nemá otevřit
             * samotný soubor, pouze se má v průzkumníku projektů otevřít umístění příslušného souboru nebo adresáře.
             *
             * Pokud ano a aplikace je spuštěna na zařízení s OS Windows, pak se využije metoda, která otevře
             * průzkumník projektů tak, že v něm přímo příslušný soubor označí.
             *
             * (Toto funguje pouze v OS Windows, na OS Linux se to nepodařilo zprovoznit.)
             */
            if (openInFileExplorer && App.KIND_OF_OS == KindOfOS.WINDOWS) {
                selectFileDirInExplorer(file);
                return;
            }


            /*
             * Uložení cesty k adresáři, který se má otevřit v průzkumníku projektů, nebo soubor, který se má otevřít
             * buď v průzkumníku projektů nebo v definované aplikaci v OS. Například textový soubor třeba v notepadu
             * apod.
             */
            final File openFileOrDir;

            /*
             * V případě, že se jedná o otevření souboru pouze v průzkumníku souborů, pak je třeba si vzít pouze
             * cestu k adresář, který se má v průzkumníku souborů otevřit.
             *
             * Tedy pokud je file cesta k souboru, tak si vezmu jeho rodičovský adresář, ve kterém se ten soubor
             * nachází a ten se otevře. Pokud se jedná
             * o adresář, tak se pouze uloží cesta k tomu adresáři.
             */
            if (openInFileExplorer) {
                openFileOrDir = file.isDirectory() ? file : file.getParentFile();

                /*
                 * Toto by nastat nemělo. Jedná se pouze o upozornění uživatele, když nebude nalezen adresář, kde se
                 * nachází (měl by) příslušný soubor.
                 */
                if (openFileOrDir == null) {
                    JOptionPane.showMessageDialog(null, txtParentDirWasNotFoundText, txtParentDirWasNotFoundTitle,
                            JOptionPane.ERROR_MESSAGE);

                    return;
                }
            }

            else openFileOrDir = file;


            // Zda je podporována třída Desktop:
            if (!isDesktopSupportedForOpen(openFileOrDir))
                return;

            // Zda je podporována akce pro otevření souboru nebo adresáře:
            if (!isOpenActionInDesktopSupported(openFileOrDir))
                return;


            /*
             * Otevření souboru nebo adresáře v průzkumníku souborů nebo v definované / výchozí aplikaci pro otevření
             * souboru, popř. adresáře.
             */
            Desktop.getDesktop().open(openFileOrDir);

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při pokusu o otevření souboru nebo adresáře v průzkumníku souborů nebo ve " +
                                "výchozí aplikaci pro otevření příslušného typu souboru. Soubor nebo adresář: " + file + ". Otevřít v průzkumníku souborů: " + openInFileExplorer + ". Tato výjimka může nastat v případě, že zadaný soubor nemá žádnou přidruženou aplikaci nebo přidruženou aplikaci nelze spustit.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
    }


    /**
     * Kontrola, zda je akce pro otevření souboru nebo adresáře v OS podporována.
     * <p>
     * Jedná se o kontrolu toho, že file je cesta k souboru nebo adresáři, který se má otevřit v průzkumníku souborů,
     * popř. v nějaké jiné aplikaci určené pro otevření souboru či adresáře. Například notepad pro textové soubory apod.
     * Tak, zda je právě toto otevření podporováno.
     *
     * @param file
     *         - cesta k souboru nebo adresáři, je zde pouze pro výpis správné hlášky, že nelze otevřit soubor nebo
     *         adresář (nesmí být null).
     *
     * @return - true, pokud je akce pro otevření souboru nebo adresáře podporována v používané platformě, jinak false.
     */
    private static boolean isOpenActionInDesktopSupported(final File file) {
        if (!Desktop.getDesktop().isSupported(Desktop.Action.OPEN)) {
            final String textToShow;

            if (file.isDirectory())
                textToShow = txtOpenDirActionIsNotSupportedText + " " + txtOpenDirActionIsNotSupportedText2;
            else textToShow = txtOpenFileActionIsNotSupportedText + " " + txtOpenFileActionIsNotSupportedText2;

            JOptionPane.showMessageDialog(null, textToShow, txtOpenActionIsNotSupportedTitle,
                    JOptionPane.ERROR_MESSAGE);

            return false;
        }

        return true;
    }


    /**
     * Test, zda je podporována třída Desktop. Tam jsou možnosti pro otevření souboru v průzkumníku souborů, ve výchozí
     * aplikaci zvolenou pro otevření příslušného souboru apod.
     *
     * @param file
     *         - cesta k souboru nebo adresáři, o kterém se pouze vypíše, že jej není možné otevřít.
     *
     * @return true, pokud je třída Desktop podporována, jinak false.
     */
    private static boolean isDesktopSupportedForOpen(final File file) {
        if (!Desktop.isDesktopSupported()) {
            /*
             * Zde se uloží text, který se vypíše uživateli. Jedná se pouze o informaci, zda se má vypsat, že není
             * možné otevřit soubor nebo adresář.
             */
            final String textToShow;

            if (file.isDirectory())
                textToShow = txtDirCannotBeOpened;
            else textToShow = txtFileCannotBeOpened;

            JOptionPane.showMessageDialog(null, txtErrorInfoText + " " + textToShow, txtErrorInfoTitle,
                    JOptionPane.ERROR_MESSAGE);

            return false;
        }

        return true;
    }


    /**
     * Tento postup funguje pouze pro OS Windows. Jedná se o otevření souboru nebo adresáře (file), ale vždy v
     * průzkumníku projektů tak, že se příslušný adresář nebo soubor označí.
     *
     * @param file
     *         cesta k existujícímu adresáři nebo souboru, který se má otevřit a označit v průzkumníku souborů v OS
     *         Windows. Parametr file již musí existovat.
     */
    private static void selectFileDirInExplorer(final File file) {
        try {

            Runtime.getRuntime().exec("explorer.exe /select," + file.getAbsolutePath());

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při pokusu o otevření souboru: " + file.getAbsolutePath() + " v průzkumn" +
                                "íku souborů (i s označením souboru v průzkumníku souborů). Výjimka nastane v případě" +
                                " chyby při I/O operaci.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
    }


    /**
     * Otevření dostupného emailového klienta (je li) na používané platformě s tím, že se předvyplní adresář (emailová
     * adresera), kam se má email odeslat a předmět emailu (subject), který se ve výchozím případě bude týkat aplikace.
     *
     * @param email
     *         - adresář emailu, který bude předvyplněn.
     * @param subject
     *         - předmět emailu, který bude předvyplněn.
     */
    public static void openEmailClient(final String email, final String subject) {
        try {
            if (!isDesktopSupportedForMail())
                return;

            if (!isMailActionInDesktopSupported())
                return;

            /*
             * Příklad adresy:
             *
             * String message = "mailto:example@domain.com?subject=Happy%20Birthday";
             */
            String message = "mailto:" + email + "?subject=" + subject;

            Desktop.getDesktop().mail(new URI(message));

        } catch (IOException | URISyntaxException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při pokusu o otevření emailového klienta pro zaslání emailu / zprávy na " +
                                "adresu: " + email + " s předmětem: " + subject + ". Tato výjimka může nastat v " +
                                "případě, že nebyl nalezen výchozí emailový klient nebo selže jeho spuštění.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
    }


    /**
     * Test, zda je podporována třída Desktop. Tam jsou možnosti pro otevření souboru v průzkumníku souborů, ve výchozí
     * aplikaci zvolenou pro otevření příslušného souboru apod.
     * <p>
     * Jedná se o zjištění podpory třídy Desktop, zda je možné odeslat email - příslušná hláška bude vypsána v případě,
     * že Desktop není podporován.
     *
     * @return true, pokud je třída Desktop podporována, jinak false.
     */
    private static boolean isDesktopSupportedForMail() {
        if (!Desktop.isDesktopSupported()) {
            JOptionPane.showMessageDialog(null, txtErrorInfoText + " " + txtEmailCannotBeSent, txtErrorInfoTitle,
                    JOptionPane.ERROR_MESSAGE);

            return false;
        }

        return true;
    }


    /**
     * Kontrola, zda je akce pro odeslání emailu podporována - zda je dostupný na používané platformě nějaký emailový
     * klient apod.
     *
     * @return - true, pokud je akce pro odeslání emailu podporována na používané platformě, jinak false.
     */
    private static boolean isMailActionInDesktopSupported() {
        if (!Desktop.getDesktop().isSupported(Desktop.Action.MAIL)) {
            JOptionPane.showMessageDialog(null, txtEmailActionIsNotSupported, txtOpenActionIsNotSupportedTitle,
                    JOptionPane.ERROR_MESSAGE);

            return false;
        }

        return true;
    }


    /**
     * Otevření webové stránky (urlPage) ve výchozím nastaveném prohlížeči na používané platformě.
     *
     * @param urlPage
     *         - odkaz na stránku, která se má otevřit v prohlížeči.
     */
    public static void openBrowser(final String urlPage) {
        try {
            if (!isDesktopSupportedForBrowse())
                return;

            if (!isBrowseActionInDesktopSupported())
                return;

            Desktop.getDesktop().browse(new URI(urlPage));

        } catch (IOException | URISyntaxException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při pokusu o otevření výchozího prohlížeče a v něm načtení stránky: " + urlPage + ". Tato chyba může nastat v případě, že výchozí prohlížeč nebyl nalezen nebo selhalo jeho spuštění.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
    }


    /**
     * Test, zda je podporována třída Desktop. Tam jsou možnosti pro otevření souboru v průzkumníku souborů, ve výchozí
     * aplikaci zvolenou pro otevření příslušného souboru apod.
     * <p>
     * Jedná se o zjištění podpory třídy Desktop, zda je možné otevřít webovou stránku ve výchozím prohlížeči na
     * používané platformě.
     *
     * @return true, pokud je třída Desktop podporována, jinak false.
     */
    private static boolean isDesktopSupportedForBrowse() {
        if (!Desktop.isDesktopSupported()) {
            JOptionPane.showMessageDialog(null, txtErrorInfoText + " " + txtBrowserCannotBeOpened, txtErrorInfoTitle,
                    JOptionPane.ERROR_MESSAGE);

            return false;
        }

        return true;
    }


    /**
     * Kontrola, zda je akce pro otevření výchozí prohlížeče podporována.
     *
     * @return - true, pokud je akce pro otevřeního výchozího webového prohlížeče pro načtení webové stránky podporována
     * na používané platformě, jinak false.
     */
    private static boolean isBrowseActionInDesktopSupported() {
        if (!Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            JOptionPane.showMessageDialog(null, txtBrowseActionIsNotSupported, txtOpenActionIsNotSupportedTitle,
                    JOptionPane.ERROR_MESSAGE);

            return false;
        }

        return true;
    }


    /**
     * Nastavení textů do chybových hlášek v uživatelem zvoleném jazyce.
     *
     * @param properties
     *         - objekt Properties obsahující texty pro tuto aplikaci v uživatelem zvoleném jazyce.
     */
    public static void setLanguage(final Properties properties) {
        if (properties != null) {
            // Texty ohledně podpory třídy Desktop:
            txtErrorInfoText = properties.getProperty("DS_Txt_ErrorInfoText", Constants.DS_TXT_ERROR_INFO_TEXT);
            txtErrorInfoTitle = properties.getProperty("DS_Txt_ErrorInfoTitle", Constants.DS_TXT_ERROR_INFO_TITLE);
            txtOpenActionIsNotSupportedTitle = properties.getProperty("DS_Txt_OpenActionIsNotSupportedTitle",
                    Constants.DS_TXT_OPEN_ACTION_IS_NOT_SUPPORTED_TITLE);

            /*
             * Texty chybové hlášky při pokusu oo otevření souboru nebo adresáře v průzkumníku souborů v OS nebo ve
             * výchozí aplikaci pro otevření příslušného typu souboru:
             */
            txtFileDoNotExistText = properties.getProperty("DS_Txt_FileDoNotExist_Text",
                    Constants.DS_TXT_FILE_DO_NOT_EXIST_TEXT);
            txtFileDoNotExistTitle = properties.getProperty("DS_Txt_FileDoNotExist_Title",
                    Constants.DS_TXT_FILE_DO_NOT_EXIST_TITLE);
            txtPath = properties.getProperty("DS_Txt_Path", Constants.DS_TXT_PATH);
            txtParentDirWasNotFoundText = properties.getProperty("DS_Txt_ParentDirWasNotFound_Text",
                    Constants.DS_TXT_PARENT_DIR_WAS_NOT_FOUND_TEXT);
            txtParentDirWasNotFoundTitle = properties.getProperty("DS_Txt_ParentDirWasNotFound_Title",
                    Constants.DS_TXT_PARENT_DIR_WAS_NOT_FOUND_TITLE);

            /*
             * Texty pro chybové hlášky, že akce pro otevření souboru nebo adresáře není na používané platformě
             * podporováno za účelem otevření souboru nebo adresáře v průzkumníku projektů nebo ve výchozí aplikaci
             * pro otevření příslušného typu souboru:
             */
            txtOpenFileActionIsNotSupportedText = properties.getProperty("DS_Txt_OpenFileActionIsNotSupported_Text",
                    Constants.DS_TXT_OPEN_FILE_ACTION_IS_NOT_SUPPORTED_TEXT);
            txtOpenFileActionIsNotSupportedText2 = properties.getProperty("DS_Txt_OpenFileActionIsNotSupported_Text2"
                    , Constants.DS_TXT_OPEN_FILE_ACTION_IS_NOT_SUPPORTED_TEXT2);

            txtOpenDirActionIsNotSupportedText = properties.getProperty("DS_Txt_OpenDirActionIsNotSupported_Text",
                    Constants.DS_TXT_OPEN_DIR_ACTION_IS_NOT_SUPPORTED_TEXT);
            txtOpenDirActionIsNotSupportedText2 = properties.getProperty("DS_Txt_OpenDirActionIsNotSupported_Text2",
                    Constants.DS_TXT_OPEN_DIR_ACTION_IS_NOT_SUPPORTED_TEXT2);

            /*
             * Texty pro chybové hlášky, že třída Desktop není na používané platformě podporována za účelem otevření
             * souboru nebo adresáře v průzkumníku projektů nebo ve výchozí aplikaci pro otevření příslušného typu
             * souboru.
             */
            txtFileCannotBeOpened = properties.getProperty("DS_Txt_FileCannotBeOpened",
                    Constants.DS_TXT_FILE_CANNOT_BE_OPENED);
            txtDirCannotBeOpened = properties.getProperty("DS_Txt_DirCannotBeOpened",
                    Constants.DS_TXT_DIR_CANNOT_BE_OPENED);

            /*
             * Text pro chybovou hlášku o tom, že třída Desktop není podporována na používané platformě za účelem
             * odeslání emailu, resp. otevření emailového klienta:
             */
            txtEmailCannotBeSent = properties.getProperty("DS_Txt_EmailCannotBeSent",
                    Constants.DS_TXT_EMAIL_CANNOT_BE_SENT);

            /*
             * Text pro chybovou hlášku o tom, že akce pro otevření emailového klienta (/ zaslání emailu) není na
             * používané platformě podporována.
             */
            txtEmailActionIsNotSupported = properties.getProperty("DS_Txt_EmailActionIsNotSupported",
                    Constants.DS_TXT_EMAIL_ACTION_IS_NOT_SUPPORTED);

            /*
             * Text pro chybovou hlášku o tom, že třída Desktop není na používané platformě podporována za účelem
             * otevření výchozího prohlížeče a v něm příslušnou webovou stránku.
             */
            txtBrowserCannotBeOpened = properties.getProperty("DS_Txt_BrowserCannotBeOpened",
                    Constants.DS_TXT_BROWSER_CANNOT_BE_OPENED);

            /*
             * Texty pro chybovou hlášku o tom, že akce pro otevření výchozího prohlížeče a v něm příslušnou webovou
             * stránku není na používané platformě podporována.
             */
            txtBrowseActionIsNotSupported = properties.getProperty("DS_Txt_BrowseActionIsNotSupported",
                    Constants.DS_TXT_BROWSE_ACTION_IS_NOT_SUPPORTED);
        }

        else {
            // Texty ohledně podpory třídy Desktop:
            txtErrorInfoText = Constants.DS_TXT_ERROR_INFO_TEXT;
            txtErrorInfoTitle = Constants.DS_TXT_ERROR_INFO_TITLE;
            txtOpenActionIsNotSupportedTitle = Constants.DS_TXT_OPEN_ACTION_IS_NOT_SUPPORTED_TITLE;

            /*
             * Texty chybové hlášky při pokusu oo otevření souboru nebo adresáře v průzkumníku souborů v OS nebo ve
             * výchozí aplikaci pro otevření příslušného typu souboru:
             */
            txtFileDoNotExistText = Constants.DS_TXT_FILE_DO_NOT_EXIST_TEXT;
            txtFileDoNotExistTitle = Constants.DS_TXT_FILE_DO_NOT_EXIST_TITLE;
            txtPath = Constants.DS_TXT_PATH;
            txtParentDirWasNotFoundText = Constants.DS_TXT_PARENT_DIR_WAS_NOT_FOUND_TEXT;
            txtParentDirWasNotFoundTitle = Constants.DS_TXT_PARENT_DIR_WAS_NOT_FOUND_TITLE;

            /*
             * Texty pro chybové hlášky, že akce pro otevření souboru nebo adresáře není na používané platformě
             * podporováno za účelem otevření souboru nebo adresáře v průzkumníku projektů nebo ve výchozí aplikaci
             * pro otevření příslušného typu souboru:
             */
            txtOpenFileActionIsNotSupportedText = Constants.DS_TXT_OPEN_FILE_ACTION_IS_NOT_SUPPORTED_TEXT;
            txtOpenFileActionIsNotSupportedText2 = Constants.DS_TXT_OPEN_FILE_ACTION_IS_NOT_SUPPORTED_TEXT2;

            txtOpenDirActionIsNotSupportedText = Constants.DS_TXT_OPEN_DIR_ACTION_IS_NOT_SUPPORTED_TEXT;
            txtOpenDirActionIsNotSupportedText2 = Constants.DS_TXT_OPEN_DIR_ACTION_IS_NOT_SUPPORTED_TEXT2;

            /*
             * Texty pro chybové hlášky, že třída Desktop není na používané platformě podporována za účelem otevření
             * souboru nebo adresáře v průzkumníku projektů nebo ve výchozí aplikaci pro otevření příslušného typu
             * souboru.
             */
            txtFileCannotBeOpened = Constants.DS_TXT_FILE_CANNOT_BE_OPENED;
            txtDirCannotBeOpened = Constants.DS_TXT_DIR_CANNOT_BE_OPENED;

            /*
             * Text pro chybovou hlášku o tom, že třída Desktop není podporována na používané platformě za účelem
             * odeslání emailu, resp. otevření emailového klienta:
             */
            txtEmailCannotBeSent = Constants.DS_TXT_EMAIL_CANNOT_BE_SENT;

            /*
             * Text pro chybovou hlášku o tom, že akce pro otevření emailového klienta (/ zaslání emailu) není na
             * používané platformě podporována.
             */
            txtEmailActionIsNotSupported = Constants.DS_TXT_EMAIL_ACTION_IS_NOT_SUPPORTED;

            /*
             * Text pro chybovou hlášku o tom, že třída Desktop není na používané platformě podporována za účelem
             * otevření výchozího prohlížeče a v něm příslušnou webovou stránku.
             */
            txtBrowserCannotBeOpened = Constants.DS_TXT_BROWSER_CANNOT_BE_OPENED;

            /*
             * Texty pro chybovou hlášku o tom, že akce pro otevření výchozího prohlížeče a v něm příslušnou webovou
             * stránku není na používané platformě podporována.
             */
            txtBrowseActionIsNotSupported = Constants.DS_TXT_BROWSE_ACTION_IS_NOT_SUPPORTED;
        }
    }
}
