package cz.uhk.fim.fimj.file;

/**
 * Toto rozhraní obsahuje dvě metody, jedna slouží pro přidání textu do editoru a druhá pro nastavení editoru (barva
 * pozadí, písmo, ... - viz metoda v editoru)
 * <p>
 * Toto rozhrani je vytvořeno pro to, aby hlášky z editoru výstupů mohly být vypsány jak do editoru výsupů v hlavním
 * okne aplikace (vpravo dole) a do editoru výstupů v dialogu pro editrou zdrojového kódu
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface OutputEditorInterface {

    /**
     * Metoda, která do editoru - editoru výstupů přidá text na konec editoru jedná se o výsldek nějako operace -
     * kompilace nebo operace zadaná uživatelem v editoru příkazů (toto platí pro editoru výstupů v hlavním okně
     * alikace), tak zde se promítne její výsledek:
     *
     * @param result
     *         - text, který se přidá na konec editoru
     */
    void addResult(final String result);


    /**
     * Pro Editor výstupů v hlavním okně aplikace: Metoda, která si přečte příslušný soubor, pokud existuje pokud
     * soubor
     * neexistuje, použije se výchozí nastavení pro tento editor
     * <p>
     * ze soubour OutputEditor.properties si přečte výchozí nastavení pro tento editor
     * <p>
     * metoda je volána při starru aplikace nebo při nastavení změn v editor, které je možné provést v settings dialogu
     * - v nastavení aplikace
     * <p>
     * Pro Editor výstupů v editoru zdrojového kódu Metoda danému editoru nastaví nějaké výchozí nastavení - barva
     * pozadí, písmo, ... viz metoda v daném editoru
     */
    void setSettingsEditor();
}