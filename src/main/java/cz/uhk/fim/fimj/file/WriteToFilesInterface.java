package cz.uhk.fim.fimj.file;

import java.util.List;
import java.util.Properties;

import org.jgraph.JGraph;
import org.jgraph.graph.DefaultGraphCell;

import cz.uhk.fim.fimj.buttons_panel.ClassTypeEnum;
import cz.uhk.fim.fimj.class_diagram.GraphClass;

/**
 * Toto rozhraní obsahuje metody, které slouží převážně pro zapisování do souborů na disku resp. zapisování souborů na
 * disk apod. viz jednotlivé metody.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface WriteToFilesInterface {

    /**
     * Metoda, která do Preferences zapíše cestu k adresáři zvolený jako workspace.
     *
     * @param path
     *         - cesta k adresář, který byl zvolen jako workspace pro aplikaci
     */
    void changePathToWorkspace(final String path);


    /**
     * Metoda, která do souboru Defaul.properties v adresáři coby Workspace zapíše hodnotu vlastnosti: Display
     * WorkspaceChooser hodnotu true nebo false, dle toho, zda se má aplikace při dalším spuštění na workspace dotázat
     * nebo ne: true = má se dotázat, false = nemá se dotázat na Workspace
     *
     * @param askForWorkspace
     *         - logická hodnota o tom, zda s má dotázat aplikace na Workspace při jejím příštím spuštění nebo ne, tato
     *         hodnota se nastaví do DefaultProperties ve Workspace
     */
    void setAskForWorkspaceValue(final boolean askForWorkspace);


    /**
     * Metoda, která na uvedenou cestu azpíše "PrintScreen" jednoho z grafů - classDIagram nebo InatanceDiagram Pokud
     * dojde k chybě je zachycena vyjímkou a aplikace běží dál
     *
     * @param path
     *         - cesta k souboru
     * @param graph
     *         - graf, z kterého chce uživatel uložit PrintScreen
     */
    void saveImageOfGraph(final String path, final JGraph graph);


    /**
     * Metoda, která do vytvoří nový adresář na zvolené cestě Metoda nebude testovat, zda nějaký adresář již exituje,
     * apod. protože má myšlenka, nad založením nového projektu, je taková, že se prostě založí nový projekt - složka se
     * zadaným jménem na zadané cestě
     * <p>
     * existence projektu se bude testovat až u otevírání projektu
     * <p>
     * do tohoto nově založeného adresáře projektu se vloží textoý dokument - cz.uhk.fim.fimj.file
     * .Constants#READ_ME_FILE_NAME_WITH_EXTENSION,
     * do kterého si uživatel bude moci psát poznámky o daném projektu dále se do této složky vytvoří adresář src, ve do
     * kterého se budou ukládat balíčky se třídami, které vytvoří uživatelem během běhu aplikace - "editace projektu",
     * dále se tam vytvoří adresář bin, do kterého se budou překopírovávat přeložené třídy - zkompilované a textový
     * dokument, do kterého může uživatel napsat nějaké základní či "úplné" informace o projektu, který se bude v daném
     * projektu vytvářet, co bud eumět, k čemu slouží, návod, ...
     * <p>
     * Jak mile se založí projekt a vytvoří soubory, tak se do hlavního adresáře aplikace uloží do proměnné: 'Open
     * project' cesta k adresáři aktuálně otevřeného projektu Pokud se projekt zavře, nastaví se hodnota této proměnné
     * na 'Closed' Dle této proměnné aplikace pozná, zda je nějaký projekt otevřený, případně na jaké položky v menu lze
     * kliknout, apod.
     *
     * @param path
     *         - cesta kde se má vytvořit příslušný adresář nového projektu
     */
    boolean createNewProjectFolder(final String path);


    /**
     * Metoda, která vytvoři klasickou Javovskou třídu v adresáři src aktuálně otevřeného projektu Dále - pokud jsou
     * zadány balíčky, tak nejpre vytvoří příslušné balíčky - adresáře a do nich tu třídu )
     *
     * @param name
     *         = celý název třídy i s balíčky
     * @param classType
     *         - Výčtová hodnota, dle které poznám, jaký typ Javovské třídy mám vytvořit
     *         <p>
     *         Postup: Metoda dostane jako parametr celý název i s balíčky Tak nejprve otestuje, zda v názvu je nějaká
     *         tečka, pokud ano, tak ho rozseká a text za poslední tečkou je název třídy, ostatní texty - názvy jsou
     *         balíčky tak vytvoří příslušné balíčky a do nich vloží vytvořenou třídu
     * @param generateMainMethod
     *         - logická hodnota o tom, zda se má vygenerovat do nové třídy i hlavní - spouštěcí metoda main nebo ne,
     *         true = má se vygenervat, false = metoda main se nemá vygenerovat
     * @return - true, pokud vše proběhlo v pořádku - třída byla vytvořena i s balíčky, false pokud došlo k chybě při
     * vytváření
     */
    boolean writeClassToProjectSrc(final String name, final ClassTypeEnum classType, final boolean generateMainMethod);


    /**
     * Metoda, která vytvoří balíčky k třídě zadané uživatelem, jak v adresáři src, tak i pro adresář bin, aby se tam
     * pouze kopírovaly přeložené třídy a nemusely se testovat balíčky
     *
     * @param pathToSrcDirectory
     *         - cesta k adresáči src v otevřeném projektu, kde se mají vytvořit balíčky
     * @param pathToBinDirectory
     *         - cesta k adresáři bin, v adresáři projektu, kde se mají vytvořit balíčky
     * @param name
     *         - celý název od uživatele, z něj si vezmu, balíčky k vytvoření
     * @return cestu k adresáře, kde se má vytvořit třída
     */
    String createDirectoriesToClass(String pathToSrcDirectory, String pathToBinDirectory,
                                    final String name);


    /**
     * Metoda, která z adresáře src v adresáři projektu vymaže označenou třídu, resp. třídu s daným názvem
     *
     * @param cellValue
     *         - název třídy i s balíčky
     */
    void deleteClassFromSrcDirectory(final String cellValue);


    /**
     * Metoda, která uloži data v classDiagramu vždy do aktuálně otevřeného adresáře projktu
     *
     * @param classDiagramObjects
     *         - obsah class diagramu pro uložení
     */
    void saveClassDiagram(final List<DefaultGraphCell> classDiagramObjects);


    /**
     * Metoda, která nastaví příslušnou hodnotu v preferencíách na hodnotu value v parametru této metody, jedná se o
     * naplnění proměnné touto hodnotou value, která bude reprezentovat otevřený nebo zavřený projektu - hodnota
     * OpenProject v MyPreferencesApi - moje třída, která obsahuje tuto informaci (mimo jiné).
     *
     * @param value
     *         - hodnota, která obsauje buď hodnotu Closed, pokud je projekt zavřený, takže se má takto zavřít - aby se
     *         o tom aplikace pak mohla dozvědět, a nebo cestu k projektu, pokud je nějaký projekt otevřený
     */
    void setCloseProject(final String value);


    /**
     * Metoda, která vymaže ze třídy na cestě balíček - pokud existuje
     *
     * @param pathToClass
     *         - cesta ke třídě, ve které se má balíček smazat
     */
    void deletePackageIfExist(final String pathToClass);


    /**
     * Metoda, která přidá do zadané třídy balíček, do které ji uživatel chce přidat (package model.data;)
     *
     * @param pathToClass
     *         - cesta k dané třídě
     * @param packageText
     *         - balíček, který se má do třídy přidat
     */
    void addPackageToClass(final String pathToClass, final String packageText);


    /**
     * Metoda, která přejmenuje hlavičku třídy - přejmnenuje název třídy v kódu dané třídy
     * <p>
     * Postup: načtu si třídu ze souboru do kolekce - jeden řádek kódu = jeden index - jedna hodnota v kolekci zjistím
     * si index hlavíčky třídy - v kolekci v na daném řádku - indexu přejmenuji - nahradím název hlavičky za nový
     * zapíšu
     * třídu zpět do souboru
     * <p>
     * Dále potřebuji najít všechny konstruktory - pokud existují, a také je přemenovat
     *
     * @param pathToClass
     *         - cesta ke třídě, která se má přejmenovat
     * @param oldName
     *         - staré jméno, abych věděl jaký znak mám nahradit
     * @param newName
     *         - nový název třídy - abych věděl na na co tu třídu mám přejmenovat
     */
    void renameClass(final String pathToClass, final String oldName, final String newName);


    /**
     * Metoda, která smaže označený objekt
     * <p>
     * Vezmu si vždy jednu označenou položku Pokud to bude komentář, tak ho smažu i s hranou
     * <p>
     * <p>
     * <p>
     * Pokud to bude hrana, tak ji smažu a musím smazat vztah mezi dvěma třídami, který hrana reprezentuje, pokud bude
     * zdroj hrany komentář, tak smažu hranu i s komentářem - komentář už by nešel připojit k jiné třídě, tudíž by byl
     * třeba také smazat
     * <p>
     * <p>
     * <p>
     * Pokud to bude buňka, tak nesmí s ní být spojena žádná jiná řída - nesmí být v libovolném vztahu s jinou
     * třídou až
     * pokud z ničeho nedědí, nic neimplementuje, apod, tak ji mohu smazat
     * <p>
     * U Asociace - smazat z druhé třídy referenci na danou třídu pro smazání
     * <p>
     * U Agregace - Pokud je označena zdrojová třída v daném vztahu, tak není co řešit, ale pokud je to cílová, tak ve
     * zrojové třídě je třeba smazat odkaz
     * <p>
     * U dědičnosti - Pokud se jedná o zdrojovou třídu, pak také není co řešit, pokud o cílovou, tak je třeba nejprve
     * smazat dědičnost ve zrojové třídě
     * <p>
     * U implementace Pokud se jedná o zdrojovou třídu, tak nic neřešit a smazat, pokud o cílovou, tak je třba smazat
     * všechny implementace této označené třídy, abych mohl smaszat toto rozhraní
     *
     * @param classDiagram
     *         - reference na diagram tříd, ze kterého se mají odebrat nějaké objekty
     * @param languageProperties
     *         - proměnná typu Properties, kde jsou všechny texty pro aplikaci ve zvoleném jazyce
     * @param deleteEdgesInCell
     *         - logická proměnná o tom, zda se mají smazt všechny hrany - včetně komentáře, které jsou připojeny k
     *         označené buňce, která se má smazat. true = že se mají smazat všechny hrany, které jsou spojené s
     *         označenou buňkou, false, má se smazat pouze jeden označený objekt v diagramu tříd. Tato možnost je
     *         potřba, pokud se klikne na buňku v diagramu tříd, která reprezentuje třídu, a ale tato třída se již
     *         nenachází v adresáři src v otevřeném projektu, pak se zobrazí pouze možnost odebrat bubku z diagramu a v
     *         trakovém případě se již nesmí řešit vzhay - pokud v diagramu nějakou jsou zobrazeny
     */
    void removeSelectedObject(final GraphClass classDiagram, final Properties languageProperties,
                              final boolean deleteEdgesInCell);


    /**
     * Metoda, která zapíše do příslušného textového souboru v adresáři logs v configuration v adresáři označeného jako
     * workspace záznam o startu aplikace Ten soubor bude vytvořen, protože se jedná o nový start aplikace.
     */
    void writeStartAppInfoToLog();


    /**
     * Metoda, která zapíše do příslušného textového souboru v adresáři logs v configuration v adresáři označeného jako
     * workspace záznam o ukončení aplikace (případně ten soubor vytvoří a poté provede záznam).
     */
    void writeExitAppInfoToLog();
}