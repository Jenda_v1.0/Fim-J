package cz.uhk.fim.fimj.file;

/**
 * Výčet slouží pro určení toho o jaký datový typ listu se jedná, resp. jaký datový typ listu se podařilo zjistit,
 * Například konkrétní datový typ nebo že List nemá definovaný typ, "neznámý" typ (wildcard) apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public enum DataTypeOfListEnum {

    /**
     * Tato hodnota značí, že se příslušnému Listu nebyl zadán dato typ (pouze "List").
     */
    WITHOUT_DATA_TYPE,

    /**
     * Tato hodnota značí, že příslušný List má "lilbovolný" (objektový) datový typ, například List <?>.
     */
    WILDCARD_TYPE,

    /**
     * Tato hodnota značí, že příslušný List má konkrétní datový typ, například List<String>.
     */
    SPECIFIC_DATA_TYPE
}