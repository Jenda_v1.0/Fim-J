package cz.uhk.fim.fimj.file;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.language.EditProperties;

import java.util.Properties;
import java.util.regex.Pattern;

/**
 * Třída slouží pro "manipulaci" s uživatelským jménem.
 * <p>
 * Jedná se o nastavení uživatelského jména, které bude uvedeno jako autor při vytvoření nového projektu v aplikaci, při
 * vytvoření nové třídy, při zápsech do logů atd. Viz výskyty / použití uživatelského jména.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 30.08.2018 10:54
 */

public final class UserNameSupport {

    /**
     * Minimální velikost / délka uživatelského jména (včetně zadané velikosti).
     */
    public static final int MIN_USER_NAME_LENGTH = 3;
    /**
     * Maximální velikost / délka uživatelského jména (včetně zadané velikosti).
     */
    public static final int MAX_USER_NAME_LENGTH = 255;


    /**
     * Regulární výraz pro uživatelské jméno.
     *
     * <i>Uživatelské jméno musí obshovat minimálně MIN_USER_NAME_LENGTH a maximálně MAX_USER_NAME_LENGTH znaků. Jméno
     * musi začínat písmenem. Může obsahovat velká a malá písmena s diakritikou, číslice, podtržítka, pomlčky a mezery
     * nebo tabulátory.</i>
     *
     * <i>(Letter: \p{L} - libovolné písmeno)</i>
     */
    public static final String USER_NAME_REG_EX = "^\\p{Blank}*\\p{L}+(\\p{L}|\\d|_|-|\\p{Blank})*\\p{Blank}*$";


    /**
     * "Komparátor" pro regulární výrazy.
     */
    private static final Pattern COMPILE = Pattern.compile(USER_NAME_REG_EX);


    /**
     * Uživatelské jméno přihlášeného uživatele v OS.
     *
     * <i>Výchozí uživatelské jméno nebude kontrolováno na regulární výraz, to se využije tak, jak je.</i>
     */
    private static final String USER_NAME_IN_OS = System.getProperty("user.name");


    /**
     * Uživatelské jméno, které se bude využívat v aplikaci. Nejprve se nastaví jako jméno přihlášeného uživatele (pro
     * případ, že není nastavené jiné jméno apod). Poté se ale nastaví (může nastavit) na jméno, které si uživatel
     * zvolil (pokud nějaké zvolil).
     *
     * <i>Nastavení proměnné je třeba provést přes "kontrolu", zda je i název příhlášeného uživatele validní. Pokud
     * není, pak se aplikuje výchozí hodnota. Tento postup je třeba aplikovat z toho důvodu, že se při změně hodnot v
     * konfiuračním soubru otestuje, zda je zadaná hodnota validní, pokud by nebyla, pak se znovu vytvoří soubor a opět
     * by se vytvořil s nevalidní hodnotou (rekurzivně by se pokračovalo do nekonečna). Proto se zde aplikuje výchozí
     * ("validní") hodnota.</i>
     */
    private static String usedUserName = getUserNameWithTest(USER_NAME_IN_OS);


    /**
     * Výchozí uživatelské jméno, které se aplikuje v případě, že není jméno, které se má nastavit validní.
     */
    private static final String DEFAULT_USER_NAME = "Unknown";


    private UserNameSupport() {
    }


    /**
     * Kontrola, zda je uživatelské jméno userName v parametru metody validní nebo ne.
     * <p>
     * Tj., zda splňuje požadovanou délku a obsahuje povolené znaky.
     *
     * @param userName
     *         - uživatelské jméno, které se má otestovat, zda je validní nebo ne.
     *
     * @return true v případě, že je userName validní, jinak false.
     */
    public static boolean isUserNameValid(final String userName) {
        // Nemělo by nastat:
        if (userName == null)
            return false;

        if (userName.trim().isEmpty())
            return false;

        if (userName.trim().length() < MIN_USER_NAME_LENGTH || userName.trim().length() > MAX_USER_NAME_LENGTH)
            return false;

        return COMPILE.matcher(userName.trim()).matches();
    }


    /**
     * Uložení uživatelského jména userName do konfiguračního souboru ve workspace v configuration.
     * <p>
     * V případě, že se konfigurační soubor nepodaří načíst, vrátí se false a zápis se neprovede.
     *
     * @param userName
     *         - uživatelské jméno, které se má zapsat do konfiguračního souboru.
     *
     * @return true v případě, že se podaří zapsat uživatelské jméno do souboru, jinak false.
     */
    public static boolean saveUserNameToConfigFile(final String userName) {
        final EditProperties defaultProp =
                App.READ_FILE.getPropertiesInWorkspacePersistComments(Constants.DEFAULT_PROPERTIES_NAME);

        /*
         * V tomto případě se nebudou řešit případy, kdy existují konfigurační soubory apod. K tomu slouží dialog
         * nastavení nebo vytvoření konfiguračních souborů apod.
         */
        if (defaultProp == null)
            return false;

        // Nastavení atributu:
        defaultProp.setProperty("UserName", userName);
        // Uložení do souboru:
        defaultProp.save();

        return true;
    }


    /**
     * Načtení uživatelského jména z konfiguračního souboru z adresáře workspace a nastavení jej do proměnné, ze které
     * se bude brát jméno pro potřeby uživatele. Například vytváření tříd, projektů apod.
     */
    public static void loadAndSetUserNameFromDefaultProp() {
        // Načtení konfiguračního souboru z workspace:
        final Properties defaultPropertiesInWorkspace = ReadFile.getDefaultPropertiesInWorkspace();

        if (defaultPropertiesInWorkspace == null)
            return;

        /*
         * Získání uživatelského jména z načteného souboru ve workspace.
         *
         * V případě, že se jméno nepodaří získat, skončí se, v případě, že se povede získat, nastaví se (pokud
         * projde regulárními výrazy).
         */
        final String userName = defaultPropertiesInWorkspace.getProperty("UserName", null);

        if (userName == null)
            return;

        if (!isUserNameValid(userName))
            return;

        usedUserName = userName;
    }


    /**
     * Getr na uživatelské jméno, které se využívá v aplikaci.
     *
     * @return - uživatelské jméno, které se využívá v aplikaci.
     */
    public static String getUsedUserName() {
        return usedUserName;
    }


    /**
     * Setr na uživatelské jméno, které se využívá v aplikaci.
     *
     * <i>Je třeba hlídat, aby se nastavovala pouze validní hodnota !!! (Viz komentář u příslušné proměnné.)</i>
     *
     * @param usedUserName
     *         - uživatelské jméno, které se bude využívat v aplikaci (musí být validní).
     */
    public static void setUsedUserName(final String usedUserName) {
        UserNameSupport.usedUserName = usedUserName;
    }


    /**
     * Získání uživatelského jména s testem. Princip je, že se zjistí, zda je jméno usedUserName validní, v případě, že
     * je, vrátí se. V případě, že jméno není validní, vrátí se výchozí hodnota (Výchozí uživatelské jméno).
     *
     * <I>Metoda je potřeba v případě, že má například přihlášený uživatel v OS jméno, které obsahuje znaky, které
     * neprojdou regulárnímy výrazy. V takovém případě je třeba vzít výchozí jméno. Hlavně kvůli testování validních
     * hodnot v konfiguračních souborech, jinak by mohlo dojít k rekurzi, kdy by se pořád do kola zapisoval konfigurační
     * soubor s nevalidní hodnotou.</I>
     *
     * @param usedUserName
     *         - uživatelské jméno, o kterém se má zjistit, zda je validní, popřípadě se vrátí. Pokud jméno není
     *         validní, vrátí se výchozí jméno.
     *
     * @return usedUserName v případě, že je validní, jinak se vrátí výchozí uživatelské jméno.
     */
    public static String getUserNameWithTest(final String usedUserName) {
        if (isUserNameValid(usedUserName))
            return usedUserName;

        return DEFAULT_USER_NAME;
    }


    /**
     * Getr na uživatelské jméno přihlášeného uživatele v OS.
     *
     * @return - uživatelské jméno přihlášeného uživatele v OS.
     */
    public static String getUserNameInOs() {
        return USER_NAME_IN_OS;
    }
}
