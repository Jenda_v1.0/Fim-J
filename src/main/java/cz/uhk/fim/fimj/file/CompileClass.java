package cz.uhk.fim.fimj.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.lang.model.SourceVersion;
import javax.swing.JOptionPane;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import org.apache.commons.io.FilenameUtils;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.instances.Instances;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.output_editor.OutputEditor;
import cz.uhk.fim.fimj.preferences.PreferencesApi;

import javax.tools.JavaCompiler.CompilationTask;

/**
 * Tato třída slouží pro kompilaci jiných Javovských tříd
 * <p>
 * obsahuje metodu, která příslušnou třídu zkompiluje - podorbnosti u ní
 * <p>
 * Zdroje pro tyto informace:
 * <p>
 * https://docs.oracle.com/javase/7/docs/api/javax/tools/JavaCompiler.html
 * <p>
 * https://www.javacodegeeks.com/2015/09/java-compiler-api.html
 * <p>
 * <p>
 * <p>
 * Zdroje ohledně zjišřování názvů tříd, které se zkompilují:
 * <p>
 * Vnořené typy tříd: https://www.algoritmy.net/article/24773/Zanorene-typy-15
 * <p>
 * a
 * <p>
 * http://stackoverflow.com/questions/11388840/java-compiled-classes-contain-dollar-signs
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class CompileClass {

	private static JavaCompiler compiler;
	
	private static DiagnosticCollector<JavaFileObject> diagnostics;
	
	private static StandardJavaFileManager manager;
	
	
	
	
	
	// Proměnné pro výpis hlášek o kompilaci do editoru výstupů:
	private static String txtError, txtLine, txtSourceNotFound, txtClass, txtAllClassesAreCompiled,
			txtAllClassesAreNotCompiled, txtClassesAreNotFound, txtClassDiagramIsEmptyForCopilation,
			txtErrorWhileCreatingCompilatorText, txtPossibleErrors, txtIsNotSetPathToJavaHomeDir,
			txtMissingToolsJarFile, txtProbablyMissingFilesInLibTools, txtMissingOrDeprecatedFiles,
			txtErrorWhileCreatingCompilatorTitle, txtActualPath, txtUnknown;
	
	
	
	
	
	
	/**
	 * soubor s tecy ve zvoleném jazyce:
	 */
	private static Properties languagePropeties;
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param languageProperties
	 *            - Objekt, která obsahuje texty pro tuto aplikaci ve zvoleném
	 *            jazyce.
	 */
	public CompileClass(final Properties languageProperties) {
		super();
		
		// (Stačí jej naplnit pouze jednou)
		if (CompileClass.languagePropeties == null)
			CompileClass.languagePropeties = languageProperties;
		
		// Doplnění textů ve zvoleném jazyce do hlášek:
		if (languageProperties != null) {
			txtError = languageProperties.getProperty("Cp_TextError", Constants.CP_TXT_ERROR);
			txtLine = languageProperties.getProperty("Cp_TextLine", Constants.CP_TXT_LINE);
			txtClass = languageProperties.getProperty("Cp_TextClass", Constants.CP_TXT_CLASS);	
			txtSourceNotFound = languageProperties.getProperty("Cp_TextSourceNotFound", Constants.CP_TXT_SOURCE_NOT_FOUND);
			
			txtAllClassesAreCompiled = languageProperties.getProperty("Cp_AllClassesAreCompiled", Constants.CP_ALL_CLASSES_ARE_COMPILED);
			txtAllClassesAreNotCompiled = languageProperties.getProperty("Cp_AllClassesAreNotCompiled", Constants.CP_ALL_CLASSES_ARE_NOT_COMPILED);
			txtClassesAreNotFound = languageProperties.getProperty("Cp_ClassesNotFound", Constants.CP_CLASSES_ARE_NOT_FOUND);	
			txtClassDiagramIsEmptyForCopilation = languageProperties.getProperty("Cp_ClassDiagramIsEmptyForCompilation", Constants.CP_CLASS_DIAGRAM_IS_EMPTY_FOR_COMPILATION);
			
			txtErrorWhileCreatingCompilatorText = languageProperties.getProperty("Cp_TextErrorWhileCreatingCompilatortext", Constants.CP_TXT_ERROR_WHILE_CREATEING_COMPILATOR_TEXT);
			txtPossibleErrors = languageProperties.getProperty("Cp_TextPossibleErrors", Constants.CP_TXT_POSSIBLE_ERRORS);
			txtIsNotSetPathToJavaHomeDir = languageProperties.getProperty("Cp_TextIsNotSetPathToJavaHomeDir", Constants.CP_TXT_IS_NOT_SET_PATH_TO_JAVA_HOME_DIR);
			txtMissingToolsJarFile = languageProperties.getProperty("Cp_TextMissingToolsJarFile", Constants.CP_TXT_MISSING_TOOLS_JAR_FILE);
			txtProbablyMissingFilesInLibTools = languageProperties.getProperty("Cp_TextProbablyMissingFilesInLibTools", Constants.CP_TXT_PROBABLY_MISSING_FILES_IN_LIB_TOOLS);
			txtMissingOrDeprecatedFiles = languageProperties.getProperty("Cp_TextMissingOrDeprecatedFiles", Constants.CP_TXT_MISSING_OR_DEPRECATED_FILES);
			txtErrorWhileCreatingCompilatorTitle = languageProperties.getProperty("Cp_TextErrorWhileCreatingCompilatorTitle", Constants.CP_TXT_ERROR_WHILE_CREATING_COMPILATOR_TITLE);
			txtActualPath = languageProperties.getProperty("Cp_TextActualPath", Constants.CP_TXT_ACTUAL_PATH);
			txtUnknown = languageProperties.getProperty("Cp_TextUnknown", Constants.CP_TXT_UNKNOWN);
		}
		
		
		else {
			txtError = Constants.CP_TXT_ERROR;
			txtLine = Constants.CP_TXT_LINE;
			txtClass = Constants.CP_TXT_CLASS;
			txtSourceNotFound = Constants.CP_TXT_SOURCE_NOT_FOUND;
			
			txtAllClassesAreCompiled = Constants.CP_ALL_CLASSES_ARE_COMPILED;
			txtAllClassesAreNotCompiled = Constants.CP_ALL_CLASSES_ARE_NOT_COMPILED;
			txtClassesAreNotFound = Constants.CP_CLASSES_ARE_NOT_FOUND;
			txtClassDiagramIsEmptyForCopilation = Constants.CP_CLASS_DIAGRAM_IS_EMPTY_FOR_COMPILATION;
			
			txtErrorWhileCreatingCompilatorText = Constants.CP_TXT_ERROR_WHILE_CREATEING_COMPILATOR_TEXT;
			txtPossibleErrors = Constants.CP_TXT_POSSIBLE_ERRORS;
			txtIsNotSetPathToJavaHomeDir = Constants.CP_TXT_IS_NOT_SET_PATH_TO_JAVA_HOME_DIR;
			txtMissingToolsJarFile = Constants.CP_TXT_MISSING_TOOLS_JAR_FILE;
			txtProbablyMissingFilesInLibTools = Constants.CP_TXT_PROBABLY_MISSING_FILES_IN_LIB_TOOLS;
			txtMissingOrDeprecatedFiles = Constants.CP_TXT_MISSING_OR_DEPRECATED_FILES;
			txtErrorWhileCreatingCompilatorTitle = Constants.CP_TXT_ERROR_WHILE_CREATING_COMPILATOR_TITLE;
			txtActualPath = Constants.CP_TXT_ACTUAL_PATH;
			txtUnknown = Constants.CP_TXT_UNKNOWN;
		}
		
		
		
		
		
		
		
		
		// Nastavím cestu k adresáři, kde se nachází soubory potřebné pro kompilaci
		setJavaHomeForCompiler(
				PreferencesApi.getPreferencesValue(PreferencesApi.COMPILER_DIR, Constants.COMPILER_DIR));
		
		
		
		
		
		compiler = ToolProvider.getSystemJavaCompiler();
		
		
		
		
		
		try {
			diagnostics = new DiagnosticCollector<>();
			manager = compiler.getStandardFileManager(diagnostics, null, null);			
		} catch (NullPointerException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informaci o chybě:
				logger.log(Level.INFO,
						"Zachycena výjimka při vytváření \"Kompilátoru\". Třída CompileClass, balíček file.\nTato " +
								"chyba nastane, pokud není uvedena cesta k domovskému adresáři "
								+ "Javy (bud v aplikaci nebo u uživatele na disku) nebo chybí jeden ze souborů: Tool" +
								".jar, currency.data nebo flavormap.properties, tyto tří soubory musí být "
								+ "umístěny v adresáři libTools v aplikace nebo cesta k adresáři u uživatele na disku," +
								" kde se hlavně tyto tří soubory nacházejí!");

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();
			
			
			
			JOptionPane.showMessageDialog(null,
					txtErrorWhileCreatingCompilatorText + "\n"
							+ txtPossibleErrors + ":"
							+ "\n1) "
							+  txtIsNotSetPathToJavaHomeDir
							+ "\n2) "
							+ txtMissingToolsJarFile
							+ "\n3) "
							+ txtProbablyMissingFilesInLibTools
							+ "\n4) " + txtMissingOrDeprecatedFiles + "\n" + txtActualPath + ": "
							+ PreferencesApi.getPreferencesValue(PreferencesApi.COMPILER_DIR, txtUnknown),
					txtErrorWhileCreatingCompilatorTitle,
					JOptionPane.ERROR_MESSAGE);
		}
	} 
	
	
	
	
	
	
	// Zde Nechám původní metodu, jak jsem kompiloval pouze jednu třídu - ale pokud má reference na nějaké jiné
	// třídy, vždy to skončí s chybou, že například chybá nějaké balíčky, nebo něco podobného
	
	
	/*
	 * Metoda, která zkompiluje třídu jejíž cesta je v parametru a výsledek vypíše do editoru výstupů
	 * Bu´d vypíše, ze se třída v pořádku zkompilovala nebo, že byla nalezena chyba a pod tento výpis se vypíše
	 * řádek, kde se chyba / chyby nachází a pod to danou informaci o chybě
	 * 
	 * @param pathToClassToCompile = cesta ke třídě, která se má zkopilovat - úplná Path (C:\\folders\ClassName.java)
	 * 
	 * @param outputEditor = odkaz na instanci editoru výstupů, pro výpis hlášení o kopmilaci
	 * 
	 * @param writeResult = logická hodnota, která určí, zda se má vypsat výsledný text - tj, výsledek o kompilaci, až už
	 * dopadne dobře nebo ne
	 * 
	 * @return true, pokud se třída v pořádku zkompiluje jinak, pokud byla třída zkompilována s chyou neb k nějaké došlo vrátí se false
	 */
//	public static boolean compileClass(final String pathToClassToCompile, OutputEditor outputEditor) {
//	public static boolean compileClass(final String pathToClassToCompile, final OutputEditorInterface outputEditor, final boolean writeResult) {
//		// Potřebuji otestovat, zda se manager != null, protože může chybět knihovna pro kompilarot, nebo nemusí být nastavena spravna cesta,
//		// apod. v takovém případě se nevytvoří instance následující kompinenty v podmínce a nelze tak zkompilovat žádnou třídu:
//		if (manager != null) {
//			
//			final File fileForCompile = new File(pathToClassToCompile);
//			
//			if (fileForCompile.exists()) {
//
//				final Iterable<? extends JavaFileObject> sources = manager.getJavaFileObjectsFromFiles(Arrays.asList(fileForCompile));
//				
//				final CompilationTask task = compiler.getTask(null, manager, diagnostics, null, null, sources);
//				
//				// Následující metody může vyhodit vyjímku, pokud nebudou v nastaveném adresáři uvedeny potřbné soubory,
//				// tj. soubory: flavormap.properties a currency.data, pak následující metoda vyhodí vyjíku FileNotFoundException,
//				// kterou jinde v kódu zachytím a oznámím uživateli, že chybí jeden z uvedených souborů
//				final boolean result = task.call();
//				
//				
//				
//				try {
//					manager.close();
//				} catch (IOException e) {
//					System.out.println("Zachycena vyjimka pri zavirani manageru pro kopilaci trid!");
//					e.printStackTrace();
//					// Došlo k nějaké chybě, třídu se nepodařilo zkompilovat, vrátím false
//					return false;
//				}
//				
//				
//				
//				// Zde již proběhla kompilace a je znán výsledek, tan mně v tuto konkrétní část metody nezajímá, 
//				// pouze přesunu vzniklé zkompilované soubory - .class soubory do adresáře bin:
//				final String pathToBinDir = moveCompiledClasses(Arrays.asList(fileForCompile));
//				
//				final File fileBinDir = new File(pathToBinDir);
//				
//				if (fileBinDir.exists() && fileBinDir.isDirectory())
//					Instances.refreshClassInstances(pathToBinDir, outputEditor, languagePropeties);
//				
//				
//				
//				
//				if (result) {
//					if (outputEditor != null)
//						outputEditor.addResult(txtClass + ": " + fileForCompile.getAbsolutePath() + " " + txtCompileOk);
//					
//					// Třída se v pořádku zkompilovala, mohu vrátít true:
//					return true;
//				}
//							
//				else {
//					if (outputEditor != null && writeResult) {
//						outputEditor.addResult(txtClass + ": " + fileForCompile.getAbsolutePath() + " " + txtCompileWithError);
//						
//						for (final Diagnostic<? extends JavaFileObject> diagnostic : diagnostics.getDiagnostics()) {
//							final String errorText = txtLine + ": " + diagnostic.getLineNumber() + ": \n " + txtError + ": "
//									+ diagnostic.getMessage(null);
//							
//							outputEditor.addResult(errorText);
//						}
//					}
//					
//					// Zde se třída zkompilovala s chybou a byla vypsána, vrátím false:
//					return false;
//				}
//				
//				// Třídu se podařilo úspěšne zkompilovat, tak mohu pokračovat - vrátím true:
////				return true;
//			}
//			
//			else {
//				// Zde se nepodařilo najít zadaný soubor na "konci cesty", takže třída již neexistuje, vypíšu oznámení uživateli a vrátím false:
//				if (outputEditor != null)
//					outputEditor.addResult(txtClassNotFound + "\n " + txtPath + ": " + pathToClassToCompile);
//				
//				return false;
//			}
//		}
//		
//		return false;
//	}
	
	
	
	
	
	
	
	
	
	
	/*
	 * Metod,a která měla přidat knihovnu do ClassPath, ale nějak to nefunguje.
	 * 
	 * Zdroj: https://jimlife.wordpress.com/2007/12/19/java-adding-new-classpath-at-runtime/
	 * 
	 * Dále jsou některé příilady zde, ale taky mi nešly:
	 * http://stackoverflow.com/questions/60764/how-should-i-load-jars-dynamically-at-runtime
	 * 
	 * @param s = cesta k souboru - .jar, resp. knihovně, která se má přidat do ClassPath
	 */
//	private static final void addPath(String s) {
//
//		try {
//			File f = new File(s);
//			URL u = f.toURI().toURL();
//			URLClassLoader urlClassLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
//			Class<?> urlClass = URLClassLoader.class;
//			Method method = urlClass.getDeclaredMethod("addURL", new Class[] { URL.class });
//			method.setAccessible(true);
//			method.invoke(urlClassLoader, new Object[] { u });
//			System.out.println("added");
//		} catch (NoSuchMethodException | SecurityException | MalformedURLException | IllegalAccessException
//				| IllegalArgumentException | InvocationTargetException e) {
//			e.printStackTrace();
//		}
//	}










	/**
	 * Metoda, která zkompiluje všechny třídy, jejich cesty jsou uvedeny v listu - parametr filesList
	 *
	 * @param outputEditor
	 *         - komponenty coby Editor výsupů, kam se budou vypisovat hlášení - v případě, že dojde k chybě při
	 *         kompilaci a nebo že byly třídy zkompilovány v pořádku
	 * @param filesList
	 *         = List - kolekce se soubory coby cesty k třídám, které se mají zkompilovat
	 * @param refreshInstanceDiagram
	 *         - logická proměnná o tom, zda se mají přenačíst vztahy mezi instancemi v diagramu instancí.
	 * @param updateCommandEditorValueCompletion
	 *         - true - pokud se má aktualizovat okno s hodnotami pro automatické doplňění / dokončení kódu do editoru
	 *         příkazů.
	 *
	 * @return true, pokud se včechny třídy zkompiluje v pořádku - bez chyby a nebo false, pokud dojde při kompilaci k
	 * chybě nebo nebude nějaká třída zkompilovaní bez chyby - tj. bude obsahovat alespoň jednu chybu
	 */
    public static boolean compileClass(final OutputEditorInterface outputEditor, final List<File> filesList,
                                       final boolean refreshInstanceDiagram, final boolean updateCommandEditorValueCompletion) {
		
		// Potřebuji otestovat, zda se manager != null, protože může chybět knihovna pro kompilarot, nebo nemusí být nastavena spravna cesta,
		// apod. v takovém případě se nevytvoří instance následující kompinenty v podmínce a nelze tak zkompilovat žádnou třídu:
		if (manager != null) {
			if (filesList != null && !filesList.isEmpty()) {

				final Iterable<? extends JavaFileObject> sources = manager.getJavaFileObjectsFromFiles(filesList);

				final CompilationTask task = compiler.getTask(null, manager, diagnostics, null, null, sources);

				// Následující metody může vyhodit vyjímku, pokud nebudou v nastaveném adresáři uvedeny potřbné soubory,
				// tj. soubory: flavormap.properties a currency.data, pak následující metoda vyhodí vyjíku FileNotFoundException,
				// kterou jinde v kódu zachytím a oznámím uživateli, že chybí jeden z uvedených souborů
				final boolean result = task.call();



				try {
					manager.close();
				} catch (IOException e) {
                    /*
                     * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                     * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                     * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                     * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                     */
                    final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                    // Otestuji, zda se podařilo načíst logger:
                    if (logger != null) {
                        // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                        // Nejprve mou informaci o chybě:
                        logger.log(Level.INFO,
                                "Zachycena výjimka při zavírání manageru pro kompilaci tříd, třída CompilaClass, " +
                                        "metoda: compileClass.");

                        /*
                         * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                         * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                         * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                         */
                        logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                    }

                    /*
                     * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                     */
                    ExceptionLogger.closeFileHandler();

					// Došlo k nějaké chybě, třídy se nepodařilo zkompilovat, vrátím false

					return false;
				}



				// Zde již proběhla kompilace a je znán výsledek, ten mně v tuto konkrétní část metody nezajímá,
				// pouze přesunu vzniklé zkompilované soubory - .class soubory do adresáře bin:
				final String pathToBinDir = moveCompiledClasses(filesList);


				if (ReadFile.existsDirectory(pathToBinDir))
					Instances.refreshClassInstances(pathToBinDir, outputEditor, languagePropeties,
							refreshInstanceDiagram);





				if (result) {
					if (outputEditor != null)
						outputEditor.addResult(txtAllClassesAreCompiled);

                    // Zda se má aktualizovat okno pro doplňování kódu do editoru příkazů:
                    if (updateCommandEditorValueCompletion)
                        App.getCommandEditor().launchThreadForUpdateValueCompletion();

					// Zde se podařilo všechny třídy zkompilovat, mohu vrátit true:
					return true;
				}

				else {
					if (outputEditor != null) {
						outputEditor.addResult(txtAllClassesAreNotCompiled);

						for (final Diagnostic<? extends JavaFileObject> diagnostic : diagnostics.getDiagnostics()) {

							/*
							 * Proměnná, do které vložím text pro chybovou hlášku ohledně kompilace. Je
							 * rozdělena na to, zda se podaří najít zdroj, jednou mi zde totiž vypadla
							 * výjimka NullPointerException, bylo to pouze jednou za celou dobu, co jse tu
							 * aplikace testoval a bohužel jsem si nevšíml při čem přesně nastala, abych
							 * mohl usoudit, co přesně jsem v tu danou chvíli kompiloval za třídu, případně,
							 * co obsahovala za chybu apod. Nicméně, po této oprave mi nic již nevypadlo a
							 * jediný možný výskyt hodnoty mohl být pouze z metody diagnostic.getSource(),
							 * ostatní na tom řádku nemohlo být null, kdyby es vrátila hodnota z té metody
							 * diagnostic.getSource(), pak getname nad výsledkem by se vypsalo (vypsalo by
							 * se null), ale nevypadla by výjimka.
							 */
							final String errorText;

							if (diagnostic.getSource() != null)
								errorText = txtClass + ": " + diagnostic.getSource().getName()
										+ OutputEditor.NEW_LINE_TWO_GAPS + txtLine + ": " + diagnostic.getLineNumber()
										+ ":" + OutputEditor.NEW_LINE_TWO_GAPS + txtError + ": "
										+ diagnostic.getMessage(null);

							else
								errorText = txtClass + ": " + txtSourceNotFound + OutputEditor.NEW_LINE_TWO_GAPS
										+ txtLine + ": " + diagnostic.getLineNumber() + ":"
										+ OutputEditor.NEW_LINE_TWO_GAPS + txtError + ": "
										+ diagnostic.getMessage(null);

							outputEditor.addResult(errorText);
						}
					}

					// Zde se nepodařilo bez chyby zkompilovat třídy, tak vrátím false - nějaká třída obsahuje chybu:
					return false;
				}
			}

			else {
				// Zde se budď nepředala kolekce s třídami (nemělo by nastat), nebo je prázdná, tak není třeba nic
				// kompilovat, zde si akorát otestuji podmínku, zda je list prázdný, abych vypsal jen ten vypis,
				// jinak vypisu ze je null:

				if (outputEditor != null && filesList == null)
					outputEditor.addResult(txtClassesAreNotFound);

					else if (outputEditor != null && filesList.isEmpty())
						outputEditor.addResult(txtClassDiagramIsEmptyForCopilation);

				return false;
			}
		}

		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zkopíruje přeložené - zkompilované třídy (vzniklé soubory s
	 * příponou .class) z příslušného adresáře v src adresáře v projektu do adresáře
	 * bin v adresáři projektu
	 * 
	 * Metoda "projede" cyklem celou kolekci v parametru metody a veškeré přeložené
	 * soubory .class soubory přesune do adresáře bin do pčíslušného balíčku -
	 * případně ho vytvoří, pokud nebude existovatr
	 * 
	 * @param filesList
	 *            - List - kolekce typu File s cestami k třídám, které se měly
	 *            zkompilovat, tato metoda si z nich vezme soubor .class a presune
	 *            je do adresáře bin
	 * 
	 * @return cesta k adresáři bin, který by již měl existovat, pokud neexistoval
	 *         (za předpokladu, že se načetla cesta ze souboru) tato cesta se zde
	 *         vrátí, aby se znovu nemusela načítat při dalším kroku
	 */
	private static String moveCompiledClasses(final List<File> filesList) {
		// cesta k adresáři bin, kam se má přesunout zkompilovaná třída:
		final String pathToBin = App.READ_FILE.getPathToBin();
		
		
		// Cesta do adresáře src, ke které níže v kódu přidám balíčky, kde se nachází
		// zkompilovaný soubor (třída), která se má přesunout
		final String pathToSrc = App.READ_FILE.getPathToSrc();
		
		
		// Jelikož se pomocí reflexe nejde načíst jednoduchou metodou všechny lokální a anonymní třídy, ... aniž bych na to napsal
		// nějaký algoritmus, který by otestoval úplně každou metodu, pro zjištění všech možných přeložených tříd, tak je možné zjistit
		// pouze vnitřní a vnořené třídy - jejich názvy - soubory se vygenerují, zbytek už ne, takže bych si na ty lokální třídy, ...
		// musel napsat regulární výraz, který je rozpozná, tak jsem si ušetřil práci a napsal jeden výraz, který je rozpozná všechny
		
		
		
		
		// Postup:
		// projdu cyklem všechny načtené třídy z diagramu tříd, otestuji, zda byly přeloženy a vznikly příslušné soubory s
		// příponou .class, k nim otestuji všechny vnořené, lokální, vnitřní, anonymní, ... třídy a přesunu je do identického 
		// balíčku v adresáři bin
		
		
		
		
		// Otestuji, zda existuje adresář bin a mám jeho cestu, ale tato možnost
		// by měla nastat vždy
		if (pathToBin != null && pathToSrc != null) {

			filesList.forEach(f -> {
				// Toto bych už nemusel testovat, protože se daná třída již měla zkompilovat, ale pro jistotu,
				// kdyby náhodou se třída smazala z disku - uživatelem:
				if (f.exists() && f.isFile()) {
					// zde daná třída existuje, tak si mohu zjistit v jakém balíčku se nachází a všechny soubory v tomto adresáři
					// otestuvat na regulární výraz, zda se nejedná o potřebn ou přeloženou třídu pro přesunutí do adresáře bin:
					final File parentFile = f.getParentFile();

					// Toto bych již testovat nemusel, jinak by to ani neněmlo jít - ale kdyby náhodou...
					if (parentFile.exists() && parentFile.isDirectory()) {

						// Nejprve si zjistím balíčky z adresáře src k dané třídě, abych je mohl vytvořit v adresáři bin,
						// to udělám tak, že si načtu cestu k adresáři bin, a od jeho délky povedu cyklus az k proměnné parentFile,
						// kde se nachází daná třída, kterou chci přesunout, a v tomto cyklu si budu postupně ukládat celou cestu,
						// coby jednotlivé balíčky


						// Proměnná, ve které bude uložena cesta z adresáře src až k poslednímu balíčku, ve kterém se třída nachází:
						String packages = "";


						if (pathToBin.length() < parentFile.getAbsolutePath().length())
							for (int i = pathToBin.length(); i < parentFile.getAbsolutePath().length(); i++)
								packages += parentFile.getAbsolutePath().charAt(i);

						if (packages.startsWith(File.separator))
							packages = packages.substring(1);



						// Nyní mohu otestovat, zda v adresáři bin výše nalezený adresář (balíček) existuje či nikoliv,
						// pokud ne, tak ho zrovna vytvořím, a pak nemusím řešit, zda exituje, apod.
						createDirectories(pathToBin + File.separator + packages);



						// Zde se jedná o adresář a existuje, tak si mohu načíst soubory vněm:
						final String[] filesArray = parentFile.list();

						// Nyní se vytvořím regulární výraz, který otestuje, zda se vytvořily další zkompilované - přeložené
						// soubory s příponou .class coby anonymní či lokální třídy, apod. které nelze pomocí reflexe snadno nalézt

						// Název třídy bez přípony:
						final String classNameWithoutExtension = FilenameUtils.removeExtension(f.getName());

						final String regExForInnerClass = getClassNameRegEx(classNameWithoutExtension);


						if (filesArray != null)
							for (final String s : filesArray) {
								if (s.matches(regExForInnerClass)) {
									// Zde se našla shoda s přeloženým souborem, tak jej mohu přesunout do adresáře
									// bin
									// do identického balíčku:

									// Zdroj je v testovaném balíčku - adresáři a akorát k němu přidám rozpoznaný
									// název souboru::
									final String srcPath = parentFile.getAbsolutePath() + File.separator + s;

									// cílové umístění je v adresáři bin, pak cesta skrze získané balíčky a název
									// souboru:
									final String desPath = pathToBin + File.separator + packages + File.separator + s;

									// Nyní mohu přesunout soubor:
									moveFile(srcPath, desPath);
								}
							}
					}
				}
			});
		}

		return pathToBin;
	}

	
	





	
	
	
	
	/**
	 * Metoda, která přesune soubor z adresáře, kde se daná třída zkompilovala do
	 * "identického" umístění v adresáři bin, resp. přesune souibor fileSrc na
	 * umístění a přejmenování v destinationPath
	 * 
	 * @param srcPath
	 *            - zdrojový soubor, který se má přesunout (v adresáři, kde se daná
	 *            třída zkompilovala)
	 * 
	 * @param destinationPath
	 *            - cílové umístění a název souboru, kam se má přesunout fileSrc
	 */
	private static void moveFile(final String srcPath, final String destinationPath) {
		// Zde přeložená - zkompilovaná třída coby soubor .class existuje, a mám jeho zdroj, tak ho přesunu do výše
		// otestovaného adresáře bin, který v tuto chvíli již musí existovat:
		try {
			Files.move(Paths.get(srcPath), Paths.get(destinationPath), StandardCopyOption.REPLACE_EXISTING);

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při přesouvání zkompilované - přeložené třídy do adresáře bin!\n třída: "
                                + srcPath + "\na cíl: " + destinationPath);

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí regulární výraz pro otestování názvu zkompilované -
	 * přeložené třídy pro lokální a nebo anonymní třídy, na které jsem nenapsal
	 * algoritmus pro "přesné načítání dle názvu dané třídy, apod."
	 * 
	 * Výraz rozpozná přeloženou - zkompilovanou "hlavní" třídu a všechny ostatní -
	 * vnořené, vnitřní, anonymni, lokální, ...
	 * 
	 * Možné syntaxe:
	 * 
	 * ClassName.class
	 * 
	 * ClassName$Vnit$1_rniTrida$515$dfgadsf$55dfd_asfg$dafadf$ASDD$2252$dfgasdg52.class
	 * 
	 * ClassName$s58_$15ss$Sdgf$_.class
	 * 
	 * apod.
	 * 
	 * @param className
	 *            - název třídy, která se kompiluje a obsahuje zmíněné vnořené
	 *            třídy, které se mi nepodařilo načíst, tak je přesunu dle
	 *            následujícího výrazu, pokud existují
	 * 
	 * @return regulární výraz pro otestoání, zda je z adresáře načtená třída je
	 *         "přeloženou - zkompilovanou" verzí dané třídy, a má se také přesunout
	 *         do adresáře bin
	 */
	private static String getClassNameRegEx(final String className) {
		return "^" + className + "(\\$\\d+|\\$\\w+){0,}\\.class$";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která nastaví "domácí složku", ze které se budou čerpat potřebné data
	 * - soubory pro funkci zkompilování tříd z diagramu tříd
	 * 
	 * @param path
	 *            - cesta k adresáři, odkud se budou čerpat data
	 */
	private static void setJavaHomeForCompiler(final String path) {
		System.setProperty("java.home", path);
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která do proměnné vloží podporované verze Javy, resp. verze Javy,
	 * které dokáže tato aplikace zkompilovat a vrátí jej.
	 * 
	 * @return text typu String, který bude obsahovat seznam verzí Javy, které umí
	 *         tato aplikace zkompilovat
	 */
	public static String getJavaVersions() {
		// Načtu si text ve zvoleném jazyce:
		final String txtJavaVersions;
		
		if (languagePropeties != null)
			txtJavaVersions = languagePropeties.getProperty("Cp_TextJavaVersionsSupported", Constants.CP_TXT_JAVA_SUPPORTED_VERSIONS);
		
		else txtJavaVersions = Constants.CP_TXT_JAVA_SUPPORTED_VERSIONS;
		
		// Proměnná, do které vložím jednotlivé verze
		String text = txtJavaVersions + ":\n";
		
		// Výpis podporovaných verzí Javy:
		if (compiler != null)
			for (final SourceVersion version : compiler.getSourceVersions())
				text += version + "\n";

		return text;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří adresáře na uvedené cestě v parametru metody
	 * 
	 * @param pathToDirForCreate
	 *            - cesta k adresářům, které se mají vytvořit - vytvoří se všechny,
	 *            které ještě na dané cestě neexistují
	 */
	private static void createDirectories(final String pathToDirForCreate) {
		try {
			final File files = new File(pathToDirForCreate);
			
			// Otestuji, zda daný adresář ještě neexistuje a případně ho (je) vytvořím
			if (!files.exists())			
				Files.createDirectories(Paths.get(pathToDirForCreate));

		} catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při vytváření adresáře na cestě: " + pathToDirForCreate
                        + ", třída CompileClass, metoda createFirectories!");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
		}
	}
}