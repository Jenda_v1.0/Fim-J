package cz.uhk.fim.fimj.file;

/**
 * Tato třída slouží jako objekt, který obsahuje informace o datovém typu nějakého Listu nebo jeho implementacích.
 * <p>
 * Jde o to, že když chci zjistit datový typ listu, tak mohou nastat následující možnosti.
 * <p>
 * Buď ten list nemá datový typ, pak je to 'List', nebo má nějaký parametr, například 'List<String>', nebo ten list je
 * generického typu, tj. 'List<?>'.
 * <p>
 * Tato třída obsahuje potřebné hodnoty pro určen, o jaký z těch "typů" listů, resp. o jakou z výše uvedených možností
 * se jedná a dle toho se například vypíše příslušný text listu někde v aplikaci apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class DataTypeOfList {

    /**
     * Tato proměnná značí pouze výčtovou hodnotu pro určení toho, jaký, případně, zda se vůbec podařilo načíst / získat
     * datový typ příslušného Listu. Protože je těch možností více, například ten typ nebyl uveden, nebo je to generický
     * datový typ, nebo konkrétní datový typ apod. Proto nemohu testovat pouze získaný typ, zda je null apod.
     */
    private final DataTypeOfListEnum dataTypeOfListEnum;

    /**
     * Tato prměnná obsahuje konkrétní získaný datový typ Listu, nebo null, pokud nebyl získán, nebo se jedná o
     * generický datový typ apod.
     * <p>
     * To se pozná, dle hodnoty dataTypeOfListEnum, zda si z této proměnné dataType mohu vzít její hodnotu nebo ne.
     */
    private final Object dataType;


    /**
     * Konstruktor této třídy.
     *
     * @param dataTypeOfListEnum
     *         - výčtová hodnota, která značí, zda se podařilo získat datový typ listu, případně o jaký datový typ se
     *         jedná, například o generický, konkrétní datový typ apod.
     * @param dataType
     *         - proměnná, která obsahuje konkrétní získaný datový typ. Tato hodnota bude naplněna pouze v případě, že
     *         se ten typ podařilo získat, jinak, pokud se jedná o generický datový typ apod. Pak bude tato hodnota
     *         null.
     */
    public DataTypeOfList(final DataTypeOfListEnum dataTypeOfListEnum, final Object dataType) {
        this.dataTypeOfListEnum = dataTypeOfListEnum;
        this.dataType = dataType;
    }


    /**
     * Konstruktor této třídy.
     *
     * @param dataTypeOfListEnum
     *         - výčtová hodnota, která značí, zda se podařilo získat datový typ listu, případně o jaký datový typ se
     *         jedná, například o generický, konkrétní datový typ apod.
     *         <p>
     *         V tomto konkrétním případě se jedná o případ, kdy se z nějakého důvodu nepodařilo získat datový typ
     *         příslušného Listu, například jedná o generiku nebo nebyl zadán parametr apod.
     */
    public DataTypeOfList(final DataTypeOfListEnum dataTypeOfListEnum) {
        this.dataTypeOfListEnum = dataTypeOfListEnum;
        dataType = null;
    }


    /**
     * Getr na proměnnou, která drží konkrétní získaný datový typ listu, poud tedy proměnná dataTypeOfListEnum bude
     * značit, že byl získán datový typ, jinak tato proměnná bude null.
     *
     * @return null, nebo konkrétní datový typ listu.
     */
    public Object getDataType() {
        return dataType;
    }

    /**
     * Getr na výčtovou hodnotu, která značí, zda se podařilo získat datový typ Listu nebo ne, případně o jakou hodnotu
     * se jedná, tj. tato hodnota značí, zda se podařilo získat ten datový typ Listu, a zda se jedná o generický datový
     * typ nebo List be zadaného datového typu apod.
     *
     * @return výčtovou hodnotu, která značí, zda se podařilo získat datový typ Listu, případně o jaký typ se jedná.
     */
    public DataTypeOfListEnum getDataTypeOfListEnum() {
        return dataTypeOfListEnum;
    }
}
