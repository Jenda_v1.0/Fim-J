package cz.uhk.fim.fimj.file;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import cz.uhk.fim.fimj.app.LookAndFeelSupport;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.PropertiesConfigurationLayout;
import org.apache.commons.configuration2.ex.ConfigurationException;

import cz.uhk.fim.fimj.language.KeysOrder;
import cz.uhk.fim.fimj.language.SortedProperties;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.settings_form.application_panels.LanguagePanel;

/**
 * Tato třída obsahuje pouze statické metody, které slouží pro vytvoření či zápis konfiguračních souborů, do kterých si
 * aplikace ukládá některá nastavení pro některé komponenty v aplikaci. Uivatel je tak může změnit "ručně" přímo v
 * souborech místo v dialogu pro nastavení aplikace.
 * <p>
 * Metody v této třídě by mohly mít signatury někde v rozhraní a zde samotná implementace, ale změnit jsem to, protože
 * tyto metody nepotřebují žádné proměnné kvůli textům do chybových hlášek apod. Stačí je pouze zavolat a neměla by
 * nastat chyba (v rámci testování ani nenastala), tak je zde rovnou implementuji navíc si ušetřím vytváření instance
 * nějaké třídy apod. Stačí puze přes název třídy zavolat příslušnou metodu.
 * <p>
 * Ty proměnné pro výchozí nastavení "designu" těch konfiguračních souborů, jako je například počet mezer nad
 * komentářem, výchozí způsob řazení apod. Ty by moli možná měli být ve třídě Constants, kde jsou veškeré konstanty pro
 * aplikace, ale zcela výjimečně tyto proměnné deklaruji zde, za účelem lepší přehlednosti, protože ty proměné nejsou
 * nikde jinde využity.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ConfigurationFiles {
	
	/**
	 * Proměnná, která značí počet volných řádků nad komentářem v souborech
	 * '.properties' obsahující nastavení pro aplikaci, ne texty pro aplikaci.
	 * 
	 * Jedná se o mnou zvolené výchozí hodnoty, nelze jej změnit nikde v nastavení
	 * aplikace apod.
	 */
	private static final int COUNT_OF_FREE_LINES_ABOVE_COMMENT = 2;

	/**
	 * Proměnná výčtového typu, která značí definované pořadí hodnot v souborech
	 * '.properties', které obsahují nastavení pro aplikaci (nějakou komponentu), ne
	 * texty pro aplikaci.
	 * 
	 * Jedná se o mnou zvolené výchozí hodnoty, nelze jej změnit nikde v nastavení
	 * aplikace apod.
	 */
	private static final KeysOrder KEYS_ORDER_PROPERTIES = KeysOrder.ASCENDING_BY_ADDING;

	/**
	 * Výchozí globální umístění, které jsem zvolil jako "formát" pro datum
	 * vytvoření, který se bude nacházet v každém souboru .properties obsahující
	 * nastavení pro nějakou komponentu v aplikaci, ne texty pro aplikaci.
	 * 
	 * Jedná se o mnou zvolené výchozí hodnoty, nelze jej změnit nikde v nastavení
	 * aplikace apod.
	 */
	private static final Locale DEFAULT_CREATION_DATE_LOCALE = Locale.ENGLISH;
	
	

		
	/**
	 * Mapa, která slouží pro vkládání komentářů pro soubory .properties.
	 * 
	 * Jako klíč v mapě (1. String) se vkládá index, neboli klíč objektu properties,
	 * ke kterému se má přidat komentář.
	 * 
	 * Jako hodnota v mapě (2. String) se vkládá samotný text komentáře (komentář),
	 * který se má vložit k indexu - klíči v prvním Stringu v mapě.
	 */
	public static final Map<String, String> commentsMap = new HashMap<>();


	private ConfigurationFiles() {
	}



	/**
	 * Metoda, která na uvedenou cestu zapíše výchozí nastavení pro graf / diagram -
	 * diagram tříd. Jedná se například o nastavení fontu, barvy pozadí, barva
	 * písma, velikost, scale (úroveň přiblížení grafu / daigramu), ...
	 * 
	 * @param path
	 *            - cesta / umístění, kam se má zapsat výše popsaný soubor (včetně
	 *            názvu).
	 */
	public static void writeClassDiagramProperties(final String path) {
		final SortedProperties classDiagramProperties = new SortedProperties(KEYS_ORDER_PROPERTIES);

		commentsMap.clear();

		
		// Barva pozadí grafu:
		commentsMap.put("BackgroundColor", "Hodnota, pro barvu pozadí diagramu tříd v RGB hodnotě (bez složky alpha). Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("BackgroundColor", Integer.toString(Constants.CD_BACKGROUND_COLOR.getRGB()));
			
		// Přiblížení grafu - scale:
		commentsMap.put("ClassDiagramScale", "Hodnota, která značí úroveň přiblížení diagramu. Možné hodnoty: <0.5 ; 20.0>. Hodnota '1.0' je 100 % přiblížení.");
		classDiagramProperties.setProperty("ClassDiagramScale", String.valueOf(Constants.CLASS_DIAGRAM_SCALE));

		commentsMap.put("ShowStaticInheritedClassNameWithPackages", "Hodnota, která značí, zda se mají v kontextovém menu nad označenou třídou v diagramu tříd v nabídce 'Zděděné statické metody' zobrazovat názvy tříd (předků) s balíčky nebo bezi nich. Povolené hodnoty 'true' a 'false'. Hodnota 'true' značí, že se v kontextovém menu nad třídou v diagramu tříd v nabídce 'Zděděné statické metody' budou zobrazovat názvy předků s balíčky, ve kterých se třída nachází. Hodnota 'false' značí, že v kontextovém menu v nabídce 'Zděděné statické metody' nebudou zobrazovat předkové s balíčky, bude se jednat o pouhý název třídy.");
		classDiagramProperties.setProperty("ShowStaticInheritedClassNameWithPackages", String.valueOf(Constants.CD_SHOW_STATIC_INHERITED_CLASS_NAME_WITH_PACKAGES));

		// Zda se v kontextovém menu nad třídou v diagramu tříd zpřístupní i soukromé konstruktory:
		commentsMap.put("IncludePrivateConstructors", "Hodnota, která značí, zda se mají v kontextovém menu nad třídou v diagramu tříd zpřístupnit i konstruktory, které jsou soukromé (označené klíčovým slovem 'private'). Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se v kontextovém menu nad třídou v diagramu tříd zobrazí a 'zpřístupní' konstruktory, které jsou soukromé (lze je zavolat). Toto je ale pouze za účelem testování, jinak dle pravidel jazyka Java nelze přístupovat k soukromému konstruktoru od jinud než z třídy samotné. Hodnota 'false' značí, že nebudou v kontextovém menu nad třídou zpřístupněny soukromé konstruktory.");
		classDiagramProperties.setProperty("IncludePrivateConstructors", String.valueOf(Constants.CD_INCLUDE_PRIVATE_CONSTRUCTORS));

		// Zda se v kontextovém menu nad třídou v diagramu tříd zpřístupní i chráněné konstruktory:
		commentsMap.put("IncludeProtectedConstructors", "Hodnota, která značí, zda se mají v kontextovém menu nad třídou v diagramu tříd zpřístupnit i konstruktory, které jsou chráněné (označené klíčovým slovem 'protected'). Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se v kontextovém menu nad třídou v diagramu tříd zobrazí a 'zpřístupní' konstruktory, které jsou chráněné (lze je zavolat). Toto je ale pouze za účelem testování, jinak dle pravidel jazyka Java nelze přístupovat k chráněnému konstruktoru od jinud než z třídy samotné a jejího potomka. Hodnota 'false' značí, že nebudou v kontextovém menu nad třídou zpřístupněny chráněné konstruktory.");
		classDiagramProperties.setProperty("IncludeProtectedConstructors", String.valueOf(Constants.CD_INCLUDE_PROTECTED_CONSTRUCTORS));

		// Zda se v kontextovém menu nad třídou v diagramu tříd zpřístupní i package-private konstruktory:
		commentsMap.put("IncludePackagePrivateConstructors", "Hodnota, která značí, zda se mají v kontextovém menu nad třídou v diagramu tříd zpřístupnit i konstruktory, které jsou package-private (bez označení klíčovým slovem pro viditelnost). Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se v kontextovém menu nad třídou v diagramu tříd zobrazí a 'zpřístupní' konstruktory, které jsou package-private (lze je zavolat). Toto je ale pouze za účelem testování, jinak dle pravidel jazyka Java nelze přístupovat k package-private konstruktoru od jinud než z třídy samotné a z tříd, které jsou ve stejném balíčku. Hodnota 'false' značí, že nebudou v kontextovém menu nad třídou zpřístupněny package-private konstruktory.");
		classDiagramProperties.setProperty("IncludePackagePrivateConstructors", String.valueOf(Constants.CD_INCLUDE_PACKAGE_PRIVATE_CONSTRUCTORS));

		// Zda se v kontextovém menu nad třídou v diagramu tříd mají v nabídce "Vnitřní statické třídy" zobrazovat i soukromé vnitřní třídy:
		commentsMap.put("IncludePrivateInnerStaticClasses", "Hodnota, která značí, zda se mají v kontextovém menu nad třídou v diagramu tříd zpřístupnit i statické vnitřní třídy, které jsou soukromé (označené klíčovým slovem 'private'). Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se v kontextovém menu nad třídou v diagramu tříd zobrazí a 'zpřístupní' statické vnitřní třídy, které jsou soukromé (budou zpřístupněny i její metody). Toto je ale pouze za účelem testování, jinak dle pravidel jazyka Java nelze přístupovat k soukromým vnitřním třídám od jinud než z (vnější) třídy samotné. Hodnota 'false' značí, že nebudou v kontextovém menu nad třídou zpřístupněny statické soukromé vnitřní třídy.");
		classDiagramProperties.setProperty("IncludePrivateInnerStaticClasses", String.valueOf(Constants.CD_INCLUDE_PRIVATE_INNER_STATIC_CLASSES));

		// Zda se v kontextovém menu nad třídou v diagramu tříd mají v nabídce "Vnitřní statické třídy" zobrazovat i chráněné vnitřní třídy:
		commentsMap.put("IncludeProtectedInnerStaticClasses", "Hodnota, která značí, zda se mají v kontextovém menu nad třídou v diagramu tříd zpřístupnit i statické vnitřní třídy, které jsou chráněné (označené klíčovým slovem 'protected'). Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se v kontextovém menu nad třídou v diagramu tříd zobrazí a 'zpřístupní' statické vnitřní třídy, které jsou chráněné (budou zpřístupněny i její metody). Toto je ale pouze za účelem testování, jinak dle pravidel jazyka Java nelze přístupovat k chráněným vnitřním třídám od jinud než z (vnější) třídy samotné a z tříd, které z ní dědí. Hodnota 'false' značí, že nebudou v kontextovém menu nad třídou zpřístupněny statické chráněné vnitřní třídy.");
		classDiagramProperties.setProperty("IncludeProtectedInnerStaticClasses", String.valueOf(Constants.CD_INCLUDE_PROTECTED_INNER_STATIC_CLASSES));

		// Zda se v kontextovém menu nad třídou v diagramu tříd mají v nabídce "Vnitřní statické třídy" zobrazovat i package-private vnitřní třídy:
		commentsMap.put("IncludePackagePrivateInnerStaticClasses", "Hodnota, která značí, zda se mají v kontextovém menu nad třídou v diagramu tříd zpřístupnit i statické vnitřní třídy, které jsou package-private (bez označení klíčovým slovem). Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se v kontextovém menu nad třídou v diagramu tříd zobrazí a 'zpřístupní' statické vnitřní třídy, které jsou viditelné pro třídy ve stejném balíčku (package-private) (budou zpřístupněny i její metody). Toto je ale pouze za účelem testování, jinak dle pravidel jazyka Java nelze přístupovat k package-private vnitřním třídám od jinud než z (vnější) třídy samotné a z tříd, které se nachází ve stejném balíčku. Hodnota 'false' značí, že nebudou v kontextovém menu nad třídou zpřístupněny statické package-private vnitřní třídy.");
		classDiagramProperties.setProperty("IncludePackagePrivateInnerStaticClasses", String.valueOf(Constants.CD_INCLUDE_PACKAGE_PRIVATE_INNER_STATIC_CLASSES));

		// Zda se v kontextovém menu nad třídou v diagramu tříd mají v nabídce "Vnitřní statické třídy" zpřístupnit ke každé vnitřní třídě její soukromé statické metody:
		commentsMap.put("InnerStaticClassesIncludePrivateStaticMethods", "Hodnota, která značí, zda se mají v kontextovém menu nad třídou v diagramu tříd zpřístupnit i statické soukromé metody (označené klíčovým slovem 'private') ve zpřístupněných statických vnitřních třídách. Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se v kontextovém menu nad třídou v diagramu tříd zobrazí a 'zpřístupní' statické metody, které jsou soukromé (tj. viditelné pouze v rámci příslušné [vnitřní] třídy) a nachází se v jedné z vnitřních tříd označené třídy v diagramu. Toto je ale pouze za účelem testování, jinak dle pravidel jazyka Java nelze přístupovat k soukromým metodám od jinud než z třídy samotné. Hodnota 'false' značí, že nebudou v kontextovém menu nad třídou zpřístupněny statické soukromé metody jednotlivých vnitřních tříd.");
		classDiagramProperties.setProperty("InnerStaticClassesIncludePrivateStaticMethods", String.valueOf(Constants.CD_INNER_STATIC_CLASSES_INCLUDE_PRIVATE_STATIC_METHODS));

		// Zda se v kontextovém menu nad třídou v diagramu tříd mají v nabídce "Vnitřní statické třídy" zpřístupnit ke každé vnitřní třídě její chráněné statické metody:
		commentsMap.put("InnerStaticClassesIncludeProtectedStaticMethods", "Hodnota, která značí, zda se mají v kontextovém menu nad třídou v diagramu tříd zpřístupnit i statické chráněné metody (označené klíčovým slovem 'protected') ve zpřístupněných statických vnitřních třídách. Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se v kontextovém menu nad třídou v diagramu tříd zobrazí a 'zpřístupní' statické metody, které jsou chráněné (tj. viditelné pouze v rámci příslušné [vnitřní] třídy a z tříd, které z ní dědí) a nachází se v jedné z vnitřních tříd označené třídy v diagramu. Toto je ale pouze za účelem testování, jinak dle pravidel jazyka Java nelze přístupovat k chráněným metodám od jinud než z třídy samotné a z tříd, které z ní dědí. Hodnota 'false' značí, že nebudou v kontextovém menu nad třídou zpřístupněny statické chráněné metody jednotlivých vnitřních tříd.");
		classDiagramProperties.setProperty("InnerStaticClassesIncludeProtectedStaticMethods", String.valueOf(Constants.CD_INNER_STATIC_CLASSES_INCLUDE_PROTECTED_STATIC_METHODS));

		// Zda se v kontextovém menu nad třídou v diagramu tříd mají v nabídce "Vnitřní statické třídy" zpřístupnit ke každé vnitřní třídě její pacakge-private statické metody:
		commentsMap.put("InnerStaticClassesIncludePackagePrivateStaticMethods", "Hodnota, která značí, zda se mají v kontextovém menu nad třídou v diagramu tříd zpřístupnit i statické package-private metody (bez označení klíčovým slovem) ve zpřístupněných statických vnitřních třídách. Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se v kontextovém menu nad třídou v diagramu tříd zobrazí a 'zpřístupní' statické metody, které jsou package-private (tj. viditelné pouze v rámci příslušné [vnitřní] třídy a tříd, které se nachází ve stejném balíčku) a nachází se v jedné z vnitřních tříd označené třídy v diagramu. Toto je ale pouze za účelem testování, jinak dle pravidel jazyka Java nelze přístupovat k package-private metodám od jinud než z třídy samotné a z tříd ve stejném balíčku. Hodnota 'false' značí, že nebudou v kontextovém menu nad třídou zpřístupněny statické package-private metody jednotlivých vnitřních tříd.");
		classDiagramProperties.setProperty("InnerStaticClassesIncludePackagePrivateStaticMethods", String.valueOf(Constants.CD_INNER_STATIC_CLASSES_INCLUDE_PACKAGE_PRIVATE_STATIC_METHODS));

		// Hodnota, která značí, zda se mají v kontextovém menu nad třídou zobrazít i metody, které jsou privátní.
		commentsMap.put("IncludePrivateMethods", "Hodnota, která značí, zda se mají v kontextovém menu nad třídou v diagramu tříd zpřístupnit i metody, které jsou soukromé (označené klíčovým slovem 'private'). Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se v kontextovém menu nad třídou v diagramu tříd zobrazí a 'zpřístupní' metody, které jsou soukromé (lze je zavolat). Toto je ale pouze za účelem testování, jinak dle pravidel jazyka Java nelze přístupovat k soukromé metodě od jinud než z třídy samotné. Hodnota 'false' značí, že nebudou v kontextovém menu nad třídou zpřístupněny soukromě metody.");
		classDiagramProperties.setProperty("IncludePrivateMethods", String.valueOf(Constants.CD_INCLUDE_PRIVATE_METHODS));

		// Hodnota, která značí, zda se mají v kontextovém menu nad třídou zobrazit i metody, které jsou chráněné.
		commentsMap.put("IncludeProtectedMethods", "Hodnota, která značí, zda se mají v kontextovém menu nad třídou v diagramu tříd zpřístupnit i metody, které jsou chráněné (označené klíčovým slovem 'protected'). Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se v kontextovém menu nad třídou v diagramu tříd zobrazí a 'zpřístupní' metody, které jsou chráněné (lze je zavolat). Toto je ale pouze za účelem testování, jinak dle pravidel jazyka Java nelze přístupovat k chráněné metodě od jinud než z třídy samotné a veškerých tříd, které z ní dědí. Hodnota 'false' značí, že nebudou v kontextovém menu nad třídou zpřístupněny chráněné metody.");
		classDiagramProperties.setProperty("IncludeProtectedMethods", String.valueOf(Constants.CD_INCLUDE_PROTECTED_METHODS));

		// Hodnota, která značí, zda se mají v kontextovém menu nad třídou zobrazit i metody, které jsou package-private.
		commentsMap.put("IncludePackagePrivateMethods", "Hodnota, která značí, zda se mají v kontextovém menu nad třídou v diagramu tříd zpřístupnit i metody, které jsou package-private (bez označení klíčovým slovem pro viditelnost metody). Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se v kontextovém menu nad třídou v diagramu tříd zobrazí a 'zpřístupní' metody, které jsou package-private (lze je zavolat). Toto je ale pouze za účelem testování, jinak dle pravidel jazyka Java nelze přístupovat k package-private metodě od jinud než z třídy samotné a tříd ve stejném balíčku. Hodnota 'false' značí, že nebudou v kontextovém menu nad třídou zpřístupněny metody s viditelností package-private (v rámci balíčku).");
		classDiagramProperties.setProperty("IncludePackagePrivateMethods", String.valueOf(Constants.CD_INCLUDE_PACKAGE_PRIVATE_METHODS));

		// Hodnota pro zpřístupnění proměnných v dialogu pro zavolání konstruktoru.
		commentsMap.put("MakeFieldsAvailableForConstructor", "Hodnota, která značí, zda se v dialogu pro zavolání konstruktoru zpřístupní komponenta pro označení veřejné nebo chráněné proměnné z tříd, instancí tříd nebo proměnných vytvořených uživatelem v editoru příkazů, vždy stejného datového typu jako je příslušný parametr konstruktoru. Z označené proměnné se vezme hodnota a předá se do parametru konstruktoru pří zavolání. Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se zpřístupní uvedené proměnné. Hodnota 'false' značí, že bude zpřístupněno pouze textové pole pro zadaní parametru konstruktoru.");
		classDiagramProperties.setProperty("MakeFieldsAvailableForConstructor", String.valueOf(Constants.CD_MAKE_FIELDS_AVAILABLE_FOR_CALL_CONSTRUCTOR));
		
		// hodnota pro zpřístupnění proměnných v dialogu pro zavolání metody.
		commentsMap.put("MakeFieldsAvailableForMethod", "Hodnota, která značí, zda se v dialogu pro zavolání metody zpřístupní komponenta pro označení veřejné nebo chráněné proměnné z tříd, instancí tříd nebo proměnných vytvořených uživatelem v editoru příkazů, vždy stejného datového typu jako je příslušný parametr metody. Z označené proměnné se vezme hodnota a předá se do parametru metody pří zavolání. Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se zpřístupní uvedené proměnné. Hodnota 'false' značí, že bude zpřístupněno pouze textové pole pro zadaní parametru metody.");
		classDiagramProperties.setProperty("MakeFieldsAvailableForMethod", String.valueOf(Constants.CD_MAKE_FIELDS_AVAILABLE_FOR_CALL_METHOD));		
		
		// Popisky hran půjdou odpojit:
		commentsMap.put("EdgeLabelMovable", "Hodnota pro určení toho, zda půjdou popisky hran od jejich hran odpojit nebo ne. Jedná se o hrany, které reprezentují spojení třídy s komentářem nebo vztahy mezi třídami v diagramu tříd. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že popisky půjdou odpojit od jejich hran. V případě hodnoty 'false' nelze odpojit popisky od jejich hran.");
		classDiagramProperties.setProperty("EdgeLabelMovable", String.valueOf(Constants.CD_COM_LABELS_MOVABLE));

		// Zda se má při volání konstruktoru pomocí kontextového menu zpřístupnit v příslušém dialogu možnsot pro vytvoření nové instance:
		commentsMap.put("ShowCreateNewInstanceOptionInCallConstructor", "Hodnota, která značí, zda se má v dialogu pro zavolání konstruktoru poomcí kontextového menu nad třídou zpřístupnit vytvoření nové instance v případě, že jako příslušný parametr konstruktoru bude třída, která se nachází v diagramu tříd. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že bude na výběr možnost pro vytvoření nové instance. Hodnota 'false' značí, že budou na výběr pouze instance příslušné třídy, které se nachází v diagramu instancí, pokud v diagramu instancí žádná instance příslušné třídy nebude, konstruktor nepůjde zavolat.");
		classDiagramProperties.setProperty("ShowCreateNewInstanceOptionInCallConstructor", String.valueOf(Constants.CD_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_CONSTRUCTOR));
		
		// Zda se má při volání metody pomocí kontextového menu nad třídou zpřístupnit možnost pro vytvoření nové instance
		commentsMap.put("ShowCreateNewInstanceOptionInCallMethod", "Hodnota, která značí, zda se má v dialogu pro zavolání metody poomcí kontextového menu nad třídou zpřístupnit vytvoření nové instance v případě, že jako příslušný parametr metody bude třída, která se nachází v diagramu tříd. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že bude na výběr možnost pro vytvoření nové instance. Hodnota 'false' značí, že budou na výběr pouze instance příslušné třídy, které se nachází v diagramu instancí, pokud v diagramu instancí žádná instance příslušné třídy nebude, metoda nepůjde zavolat.");
		classDiagramProperties.setProperty("ShowCreateNewInstanceOptionInCallMethod", String.valueOf(Constants.CD_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD));
		
		// Zda se mají zobrazit metody v dialogu pro zavolání statické metody pro předání návratové hodnoty do parametru metody:
		commentsMap.put("MakeMethodsAvailableForCallMethod", "Hodnota, která značí, zda se mají v dialogu pro zavolání statické metody nad třídou v diagramu tříd zobrazit i metody. Tzn. Zda bude možné označit metodu, jejíž návratová hodnota se předá do příslušného parametru metody. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se v dialogu pro zavolání metody budou nabízet metody pro předání jejich návratové hodnoty do příslušného parametru metody. Hodnota 'false' značí, že v dialogu pro zavolání statické metody nad třídou v diagramu tříd nebudou na výběr metody pro předání jejich návratové hodnoty do příslušného parametru metody.");
		classDiagramProperties.setProperty("MakeMethodsAvailableForCallMethod", String.valueOf(Constants.CD_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD));
		
		// Zda se mají zobrazit metody v dialogu pro zavolání konstruktoru:
		commentsMap.put("MakeMethodsAvailableForCallConstructor", "Hodnota, která značí, zda se mají v dialogu pro zavolání konstruktoru nad třídou v diagramu tříd zobrazit v příslušném dialogu i metody. Tzn. Zda bude možné označit metodu, jejíž návratová hodnota se předá do příslušného parametru konstruktoru. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se v dialogu pro zavolání konstruktoru budou nabízet metody pro předání jejich návratové hodnoty do příslušného parametru konstruktoru. Hodnota 'false' značí, že v dialogu pro zavolání konstruktoru nad třídou v diagramu tříd nebudou na výběr metody pro předání jejich návratového hodnoty do příslušného parametru konstruktoru.");
		classDiagramProperties.setProperty("MakeMethodsAvailableForCallConstructor", String.valueOf(Constants.CD_MAKE_METHODS_AVAILABLE_FOR_CALL_CONSTRUCTOR));
		
		// Zda se mají zobrazovat vztahy tříd na sebe sama.
		commentsMap.put("ShowRelationsShipsToClassItself", "Hodnota pro určení toho, zda se mají zobrazovat vztahy třídy na sebe sama. Jedná se o to, že pokud bude mít třída vztah na sebe sama, tak zda se tento vztah má zobrazit v diagramu tříd nebo ne. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se budou v diagramu tříd zobrazovat vztahy třídy na sebe sama. Hodnota 'false' značí, že se nebudou zobrazovat vztahy třídy na sebe sama.");
		classDiagramProperties.setProperty("ShowRelationsShipsToClassItself", String.valueOf(Constants.CD_SHOW_RELATION_SHIPS_TO_CLASS_ITSELF));			
		
		// Zda se má vztah asociace znázornit jako dvě agregtace nebo klasicky jedna asociace - jedna čára:
		commentsMap.put("ShowAssociationThroughAggregation", "Hodnota pro určení toho, zda se má vztah typu asociace v diagramu tříd znázornit klasicky jako jedna hrana s šipkami na obou koncích (hodnota 'false'). Nebo, zda se má vztah typu asociace v diagramu tříd znázornit jako dvě agregace 1 : 1 (hodnota 'true'). Povolené hodnoty: 'true', 'false'.");
		classDiagramProperties.setProperty("ShowAssociationThroughAggregation", String.valueOf(Constants.CD_SHOW_ASSOCIATION_THROUGH_AGGREGATION));

		commentsMap.put("DefaultSourceCodeEditor", "Hodnota, která značí výchozí editor zdrojového kódu, ve kterém bude otevřen zdrojový kód vybrané třídy v diagramu tříd. Tzn. Když uživatel bude chtít otevřit kód třídy v diagramu tříd tak, že na ni dvakrát klikne levým tlačítkem myši nebo klávesou Enter, tak se otevře v jednom z editorů zdrojového kódu. Povolené hodnoty: 'Source_Code_Editor', 'Project_Explorer'. Hodnota 'Source_Code_Editor' značí, že zdrojový kód třídy bude otevřen v dialogu 'Editor zdrojového kódu' (dialog podobný jako v programu BlueJ). Hodnota 'Project_Explorer' značí, že zdrojový kód třídy bude otevřen v dialogu 'Průzkumník projektů'.");
		classDiagramProperties.setProperty("DefaultSourceCodeEditor", Constants.CD_KIND_OF_DEFAULT_CODE_EDITOR.getValue());
		
		// Zda se mají při vytvoření vztahu pro implementaci rozhraní rovnou vygenerovat i metody:
		commentsMap.put("AddInterfaceMethodsToClass", "Hodnota, která značí, zda se při vytvoření vztahu typu implementace roznraní v diagramu tříd mají do označené třídy vygenerovat metody, které jsou deklarované v označeném rozhraní. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se při vytvoření vztahu implementace rozhraní v diagramu tříd vygenerují do označené třídy veškeré (nestatické) metody, které se budou nacházet v označeném rozhraní. Hodnota 'false' značí, že při vytvoření vztahu typu implementace rozhraní v diagramu tříd se pouze přidá do hlavičky označené třídy klíčové slovo 'implements' a k němu příslušné rozhraní (jeho název), ale metody se již nebudou generovat.");
		classDiagramProperties.setProperty("AddInterfaceMethodsToClass", String.valueOf(Constants.ADD_INTERFACE_METHODS_TO_CLASS));
		
		
		
		
		
		
		
		
		// Cd = Class Diagram - iniciály
		
		// Hodnoty pro buňku v grafu, která reprezentuje třídu v Class diagramu - grafu:
		commentsMap.put("ClassFontType", "Hodnota pro index stylu fontu pro buňku, která reprezentuje třídu v diagramu tříd. Dle zadané hodnoty se aplikuje styl fontu. Možné hodnoty (indexy stylů): '0', '1', '2'. Hodnota '0' - Plain, hodnota '1' - Bold a hodnota '2' - Italic.");
		classDiagramProperties.setProperty("ClassFontType", Integer.toString(Constants.CD_FONT_CLASS.getStyle()));
		
		commentsMap.put("ClassFontSize", "Hodnota pro velikost písma pro buňku, která reprezentuje třídu v diagramu tříd. Možné hodnoty <8 ; 150>.");
		classDiagramProperties.setProperty("ClassFontSize", Integer.toString(Constants.CD_FONT_CLASS.getSize()));
		
		commentsMap.put("VerticalAlignment", "Hodnota pro vertikální zarovnání textu v buňce, která reprezentuje třídu v diagramu tříd. Možné hodnoty: '0', '1' '3'. Hodnota '0' - Center, hodnota '1' - Top a hodnota '3' - Bottom.");
		classDiagramProperties.setProperty("VerticalAlignment", String.valueOf(Constants.CD_VERTICAL_ALIGNMENT));
		
		commentsMap.put("HorizontalAlignment", "Hodnota pro horizontální zarovnání textu v buňce, která reprezentuje třídu v diagramu tříd. Možné hodnoty: '0', '2' '4'. Hodnota '0' - Center, hodnota '2' - Left a hodnota '4' - Right.");
		classDiagramProperties.setProperty("HorizontalAlignment", String.valueOf(Constants.CD_HORIZONTAL_ALIGNMENT));
		
		commentsMap.put("ForegroundColor", "Hodnota pro barvu textu v RGB hodnotě (bez složky alpha) v buňce, která reprezentuje třídu v diagramu tříd. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("ForegroundColor", Integer.toString(Constants.CD_TEXT_COLOR.getRGB()));		
		
			
		// GradienColor je barvy pozadi pro bunku reprezentujici tridu od praveho dolniho rohu
		commentsMap.put("GradientColor", "Hodnota pro barvu pozadí od pravého spodního rohu v RGB hodnotě (bez složky alpha) v buňce, která reprezentuje třídu v diagramu tříd. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("GradientColor", Integer.toString(Constants.CD_GRADIENT_COLOR.getRGB()));
			
		// Barva pozadí pro bunku reprezentujici tridu od leveho horniho rohu
		commentsMap.put("ClassBackgroundColor", "Hodnota pro barvu pozadí od levého horního rohu v RGB hodnotě (bez složky alpha), v buňce, která reprezentuje třídu v diagramu tříd. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("ClassBackgroundColor", Integer.toString(Constants.CD_CLASS_BACKGROUND_COLOR.getRGB()));
			
		commentsMap.put("ClassCellOpaque", "Hodnota, která značí, zda bude buňka, která reprezentuje třídu v diagramu tříd průhledná nebo ne. Možné hodnoty: 'true', 'false'. Hodnota 'true' značí, že buňka bude průhledná a hodnota 'false' značí, že buňka nebude průhledná.");
		classDiagramProperties.setProperty("ClassCellOpaque", String.valueOf(Constants.CD_OPAQUE_CLASS_CELL));
			
		// Barva pozadi bunky reprezentujici tridu ale pouze pro jednu barvu - jedna barva pozadi bunku coby tridy v diagramu trid:
		commentsMap.put("ClassOneBackgroundColor", "Hodnota pro barvu pozadí buňky, která reprezentuje třídu v diagramu tříd. Jedná se o hodnotu, která značí pouze jednu barvu pro celé pozadí buňky. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("ClassOneBackgroundColor", String.valueOf(Constants.CD_ONE_BG_COLOR.getRGB()));
		
		// Do následující proměnné se budou vkládat pouze hodnoty true a nebo false (logické hodnoty), které budou značit,
		// zda se má použít pro barvu pozadí buňky reprezentující třídu v diagramu tříd jedna barva nebo dvě barvy,
		// true = jedna barva pro pozadí bunky coby tridy, false = dvě barvy pro pozadi bunky ocby tridy
		commentsMap.put("ClassOneBgColorBoolean", "Hodnota, která značí, zda bude buňka reprezentující třídu v diagramu tříd obsahovat pouze jednu barvu pozadí nebo dvě (jedna barva začíná v levém horním rohu a druhá v pravém spodním rohu, uprostřed buňky je přechod barev). Možné hodnoty: 'true', 'false'. Hodnota 'true' značí, že buňka bude mít pouze jednu barvu a hodnota 'false', že buňka bude mít dvě barvy (se zmíněným přechodem barev).");
		classDiagramProperties.setProperty("ClassOneBgColorBoolean", String.valueOf(Constants.CD_ONE_BG_COLOR_BOOLEAN));
			
			
		
		

			
			
		
		// Hodnoty pro buňky, které reprezentují komentář v Class diagramu - grafu:
		commentsMap.put("CommentFontType", "Hodnota pro index stylu fontu pro buňku, která reprezentuje komentář v diagramu tříd. Dle zadané hodnoty se aplikuje styl fontu. Možné hodnoty (indexy stylů): '0', '1', '2'. Hodnota '0' - Plain, hodnota '1' - Bold a hodnota '2' - Italic.");
		classDiagramProperties.setProperty("CommentFontType", Integer.toString(Constants.CD_FONT_COMMENT.getStyle()));
		commentsMap.put("CommentFontSize", "Hodnota pro velikost písma pro buňku, která reprezentuje komentář v diagramu tříd. Možné hodnoty <8 ; 150>.");
		classDiagramProperties.setProperty("CommentFontSize", Integer.toString(Constants.CD_FONT_COMMENT.getSize()));
		commentsMap.put("CommentVerticalAlignment", "Hodnota pro vertikální zarovnání textu v buňce, která reprezentuje komentář v diagramu tříd. Možné hodnoty: '0', '1' '3'. Hodnota '0' - Center, hodnota '1' - Top a hodnota '3' - Bottom.");
		classDiagramProperties.setProperty("CommentVerticalAlignment", Integer.toString(Constants.CD_COM_VERTICAL_ALIGNMENT));
		commentsMap.put("CommentHorizontalAlignment", "Hodnota pro horizontální zarovnání textu v buňce, která reprezentuje komentář v diagramu tříd. Možné hodnoty: '0', '2' '4'. Hodnota '0' - Center, hodnota '2' - Left a hodnota '4' - Right.");
		classDiagramProperties.setProperty("CommentHorizontalAlignment", Integer.toString(Constants.CD_COM_HORIZONTAL_ALIGNMENT));
		commentsMap.put("CommentForegroundColor", "Hodnota pro barvu textu v RGB hodnotě (bez složky alpha) v buňce, která reprezentuje komentář v diagramu tříd. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("CommentForegroundColor", Integer.toString(Constants.CD_COM_FOREGROUND_COLOR.getRGB()));
		commentsMap.put("CommentBackgroundColor", "Hodnota pro barvu pozadí od levého horního rohu v RGB hodnotě (bez složky alpha), v buňce, která reprezentuje komentář v diagramu tříd. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("CommentBackgroundColor", Integer.toString(Constants.CD_COM_BACKGROUND_COLOR.getRGB()));
		commentsMap.put("CommentGradientColor", "Hodnota pro barvu pozadí od pravého spodního rohu v RGB hodnotě (bez složky alpha) v buňce, která reprezentuje komentář v diagramu tříd. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("CommentGradientColor", Integer.toString(Constants.CD_COM_GRADIENT_COLOR.getRGB()));
		commentsMap.put("CommentCellOpaque", "Hodnota, která značí, zda bude buňka, která reprezentuje komentář v diagramu tříd průhledná nebo ne. Možné hodnoty: 'true', 'false'. Hodnota 'true' značí, že buňka bude průhledná a hodnota 'false' značí, že buňka nebude průhledná.");
		classDiagramProperties.setProperty("CommentCellOpaque", String.valueOf(Constants.CD_OPAQUE_COMMENT_CELL));		
		
		// Barva pozadi bunky reprezentujici komentář ale pouze pro jednu barvu - jedna barva pozadi bunku coby tridy v diagramu trid:
		commentsMap.put("CommentOneBackgroundColor", "Hodnota pro barvu pozadí buňky, která reprezentuje komentář v diagramu tříd. Jedná se o hodnotu, která značí pouze jednu barvu pro celé pozadí buňky. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("CommentOneBackgroundColor", String.valueOf(Constants.CD_COM_ONE_BG_COLOR.getRGB()));
		
		// DO následující proměnné se budou vkládat pouze hodnoty true a nebo false (logické hodnoty), které budou značit,
		// zda se má použít pro barvu pozadí buňky reprezentující komentář v diagramu tříd jedna barva nebo dvě barvy,
		// true = jedna barva pro pozadí bunky coby komentáře, false = dvě barvy pro pozadi bunky ocby komentáře
		commentsMap.put("CommentOneBgColorBoolean", "Hodnota, která značí, zda bude buňka reprezentující komentář v diagramu tříd obsahovat pouze jednu barvu pozadí nebo dvě (jedna barva začíná v levém horním rohu a druhá v pravém spodním rohu, uprostřed buňky je přechod barev). Možné hodnoty: 'true', 'false'. Hodnota 'true' značí, že buňka bude mít pouze jednu barvu a hodnota 'false', že buňka bude mít dvě barvy (se zmíněným přechodem barev).");
		classDiagramProperties.setProperty("CommentOneBgColorBoolean", String.valueOf(Constants.CD_COM_ONE_BG_COLOR_BOOLEAN));
			
		
		
		
		// Parametry hrany komentáře:
		commentsMap.put("CommentEdgeLineFontStyle", "Hodnota pro index stylu fontu pro hranu, která reprezentuje spojení třídy s komentářem v diagramu tříd. Dle zadané hodnoty se aplikuje styl fontu. Možné hodnoty (indexy stylů): '0', '1', '2'. Hodnota '0' - Plain, hodnota '1' - Bold a hodnota '2' - Italic.");
		classDiagramProperties.setProperty("CommentEdgeLineFontStyle", Integer.toString(Constants.CD_COM_EDGE_FONT.getStyle()));
		commentsMap.put("CommentEdgeLineFontSize", "Hodnota pro velikost písma pro hranu, která reprezentuje spojení třídy s komentářem v diagramu tříd. Možné hodnoty <8 ; 150>.");
		classDiagramProperties.setProperty("CommentEdgeLineFontSize", Integer.toString(Constants.CD_COM_EDGE_FONT.getSize()));
		commentsMap.put("CommentEdgeColor", "Hodnota pro barvu hrany, která reprezentuje spojení třídy s komentářem v diagramu tříd. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("CommentEdgeColor", Integer.toString(Constants.CD_COM_EDGE_COLOR.getRGB()));
		commentsMap.put("CommentEdgeTextColor", "Hodnota pro barvu textu v RGB hodnotě (bez složky alpha) pro hranu, která reprezentuje spojení třídy s komentářem v diagramu tříd. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("CommentEdgeTextColor", Integer.toString(Constants.CD_COM_EDGE_COLOR.getRGB()));
		commentsMap.put("CommentEdgeLabelAlongEdge", "Hodnota pro určení toho, zda má být text hrany, která reprezentuje spojeí třídy s komentářem v diagramu tříd zobrazen podél hrany nebo ne. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že text bude podél hrany a hodnota 'false', že text nebude podél hrany.");
		classDiagramProperties.setProperty("CommentEdgeLabelAlongEdge", String.valueOf(Constants.CD_COM_LABEL_ALONG_EDGE));
		commentsMap.put("CommentEdgeLineStyle", "Hodnota pro určení stylu hrany, která reprezentuje spojení třídy s komentářem v diagramu tříd. Povolené hodnoty: '-1', '11', '12', '13'. Hodnota '-1' - běžná hrana, hodnota '11' - Orthogonal, hodnota '12' - Bezier a hodnota '13' - Spline.");
		classDiagramProperties.setProperty("CommentEdgeLineStyle", Integer.toString(Constants.CD_COM_EDGE_LINE_STYLE));
		commentsMap.put("CommentEdgeLineEnd", "Hodnota pro určení typu / stylu konce hrany, která reprezentuje spojení třídy s komentřem v diagramu tříd. Povolené hodnoty: '0', '1', '2', '4', '5', '7', '8', '9'. Hodnota '0' - None, hodnota '1' - Classic, hodnota '2' - Technical, hodnota '4' - Simple, hodnota '5' - Circle, hodnota '7' - Line, hodnota '8' - Double line a hodnota '9' - Diamond.");
		classDiagramProperties.setProperty("CommentEdgeLineEnd", Integer.toString(Constants.CD_COM_LINE_END));
		commentsMap.put("CommentEdgeLineBegin", "Hodnota pro určení typu / stylu začátku hrany, která reprezentuje spojení třídy s komentřem v diagramu tříd. Povolené hodnoty: '0', '1', '2', '4', '5', '7', '8', '9'. Hodnota '0' - None, hodnota '1' - Classic, hodnota '2' - Technical, hodnota '4' - Simple, hodnota '5' - Circle, hodnota '7' - Line, hodnota '8' - Double line a hodnota '9' - Diamond.");
		classDiagramProperties.setProperty("CommentEdgeLineBegin", Integer.toString(Constants.CD_COM_LINE_BEGIN));
		commentsMap.put("CommentEdgeLineWidth", "Hodnota pro určení šířky hrany, která reprezentuje spojení třídy s komentářem v diagramu tříd. Povolené hodnoty <0.1 ; 20.0>. Hodnota '1.0' určena jako výchozí.");
		classDiagramProperties.setProperty("CommentEdgeLineWidth", String.valueOf(Constants.CD_COM_LINE_WIDTH));
		commentsMap.put("CommentEdgeEndFill", "Hodnota pro určení toho, zda je konec hrany, která reprezentuje spojení třídy s komentářem v diagramu tříd vyplněn / vybarven nebo ne (funguje pouze pro určité styly / typy začátků hran). Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že konec hrany bude vyplněn (při správném tvaru). Hodnota 'false' značí, že konec hrany nebude vyplněn.");
		classDiagramProperties.setProperty("CommentEdgeEndFill", String.valueOf(Constants.CD_COM_LINE_END_FILL));
		commentsMap.put("CommentEdgeBeginFill", "Hodnota pro určení toho, zda je začátek hrany, která reprezentuje spojení třídy s komentářem v diagramu tříd vyplněn / vybarven nebo ne (funguje pouze pro určité styly / typy začátků hran). Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že začátek hrany bude vyplněn (při správném tvaru). Hodnota 'false' značí, že začátek hrany nebude vyplněn.");
		classDiagramProperties.setProperty("CommentEdgeBeginFill", String.valueOf(Constants.CD_COM_LINE_BEGIN_FILL));
			
		
	
			
		// Parametry pro hranu, která reprezentuje Asociaci:
		commentsMap.put("AscEdgeLineFontStyle", "Hodnota pro index stylu fontu pro hranu, která reprezentuje vztah typu asociace mezi třídami v diagramu tříd. Dle zadané hodnoty se aplikuje styl fontu. Možné hodnoty (indexy stylů): '0', '1', '2'. Hodnota '0' - Plain, hodnota '1' - Bold a hodnota '2' - Italic.");
		classDiagramProperties.setProperty("AscEdgeLineFontStyle", Integer.toString(Constants.CD_ASC_EDGE_LINE_FONT.getStyle()));
		commentsMap.put("AscEdgeLineFontSize", "Hodnota pro velikost písma pro hranu, která reprezentuje vztah typu asociace mezi třídami v diagramu tříd. Možné hodnoty <8 ; 150>.");
		classDiagramProperties.setProperty("AscEdgeLineFontSize", Integer.toString(Constants.CD_ASC_EDGE_LINE_FONT.getSize()));
		commentsMap.put("AscEdgeLineTextColor", "Hodnota pro barvu textu v RGB hodnotě (bez složky alpha) pro hranu, která reprezentuje vztah typu asociace mezi třídami v diagramu tříd. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("AscEdgeLineTextColor", Integer.toString(Constants.CD_ASC_EDGE_FONT_COLOR.getRGB()));
		commentsMap.put("AscEdgeLineColor", "Hodnota pro barvu hrany, která reprezentuje vztah typu asociace mezi třídami v diagramu tříd. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("AscEdgeLineColor", Integer.toString(Constants.CD_ASC_EDGE_COLOR.getRGB()));
		commentsMap.put("AscEdgeLabelAlongEdge", "Hodnota pro určení toho, zda má být text hrany, která reprezentuje vztah typu asociace mezi třídami v diagramu tříd zobrazen podél hrany nebo ne. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že text bude podél hrany a hodnota 'false', že text nebude podél hrany.");
		classDiagramProperties.setProperty("AscEdgeLabelAlongEdge", String.valueOf(Constants.CD_ASC_LABEL_ALONG_EDGE));
		
		commentsMap.put("AscEdgeLineStyle", "Hodnota pro určení stylu hrany, která reprezentuje vztah typu asociace mezi třídami v diagramu tříd. Povolené hodnoty: '-1', '11', '12', '13'. Hodnota '-1' - běžná hrana, hodnota '11' - Orthogonal, hodnota '12' - Bezier a hodnota '13' - Spline.");
		classDiagramProperties.setProperty("AscEdgeLineStyle", Integer.toString(Constants.CD_ASC_EDGE_LINE_STYLE));
		commentsMap.put("AscEdgeLineEnd", "Hodnota pro určení typu / stylu konce hrany, která reprezentuje vztah typu asociace mezi třídami v diagramu tříd. Povolené hodnoty: '0', '1', '2', '4', '5', '7', '8', '9'. Hodnota '0' - None, hodnota '1' - Classic, hodnota '2' - Technical, hodnota '4' - Simple, hodnota '5' - Circle, hodnota '7' - Line, hodnota '8' - Double line a hodnota '9' - Diamond.");
		classDiagramProperties.setProperty("AscEdgeLineEnd", Integer.toString(Constants.CD_ASC_EDGE_LINE_END));
		commentsMap.put("AscEdgeLineBegin", "Hodnota pro určení typu / stylu začátku hrany, která reprezentuje vztah typu asociace mezi třídami v diagramu tříd. Povolené hodnoty: '0', '1', '2', '4', '5', '7', '8', '9'. Hodnota '0' - None, hodnota '1' - Classic, hodnota '2' - Technical, hodnota '4' - Simple, hodnota '5' - Circle, hodnota '7' - Line, hodnota '8' - Double line a hodnota '9' - Diamond.");
		classDiagramProperties.setProperty("AscEdgeLineBegin", Integer.toString(Constants.CD_ASC_EDGE_LINE_BEGIN));
		commentsMap.put("AscEdgeLineWidth", "Hodnota pro určení šířky hrany, která reprezentuje vztah typu asociace mezi třídami v diagramu tříd. Povolené hodnoty <0.1 ; 20.0>. Hodnota '1.0' určena jako výchozí.");
		classDiagramProperties.setProperty("AscEdgeLineWidth", String.valueOf(Constants.CD_ASC_EDGE_LINE_WIDTH));
		commentsMap.put("AscEdgeEndFill", "Hodnota pro určení toho, zda je konec hrany, která reprezentuje vztah typu asociace mezi třídami v diagramu tříd vyplněn / vybarven nebo ne (funguje pouze pro určité styly / typy začátků hran). Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že konec hrany bude vyplněn (při správném tvaru). Hodnota 'false' značí, že konec hrany nebude vyplněn.");
		classDiagramProperties.setProperty("AscEdgeEndFill", String.valueOf(Constants.CD_ASC_EDGE_END_FILL));
		commentsMap.put("AscEdgeBeginFill", "Hodnota pro určení toho, zda je začátek hrany, která reprezentuje vztah typu asociace mezi třídami v diagramu tříd vyplněn / vybarven nebo ne (funguje pouze pro určité styly / typy začátků hran). Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že začátek hrany bude vyplněn (při správném tvaru). Hodnota 'false' značí, že začátek hrany nebude vyplněn.");
		classDiagramProperties.setProperty("AscEdgeBeginFill", String.valueOf(Constants.CD_ASC_EDGE_BEGIN_FILL));
			
			
			
		// Parametry pro hranu, která reprezentuje dědičnost: (Ext = Extends)
		commentsMap.put("ExtEdgeLineFontStyle", "Hodnota pro index stylu fontu pro hranu, která reprezentuje vztah typu dědičnost mezi třídami v diagramu tříd. Dle zadané hodnoty se aplikuje styl fontu. Možné hodnoty (indexy stylů): '0', '1', '2'. Hodnota '0' - Plain, hodnota '1' - Bold a hodnota '2' - Italic.");
		classDiagramProperties.setProperty("ExtEdgeLineFontStyle", Integer.toString(Constants.CD_EXT_EDGE_LINE_FONT.getStyle()));
		commentsMap.put("ExtEdgeLineFontSize", "Hodnota pro velikost písma pro hranu, která reprezentuje vztah typu dědičnost mezi třídami v diagramu tříd. Možné hodnoty <8 ; 150>.");
		classDiagramProperties.setProperty("ExtEdgeLineFontSize", Integer.toString(Constants.CD_EXT_EDGE_LINE_FONT.getSize()));
		commentsMap.put("ExtEdgeLineTextColor", "Hodnota pro barvu textu v RGB hodnotě (bez složky alpha) pro hranu, která reprezentuje vztah typu dědičnost mezi třídami v diagramu tříd. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("ExtEdgeLineTextColor", Integer.toString(Constants.CD_EXT_EDGE_FONT_COLOR.getRGB()));
		commentsMap.put("ExtEdgeLineColor", "Hodnota pro barvu hrany, která reprezentuje vztah typu dědičnost mezi třídami v diagramu tříd. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("ExtEdgeLineColor", Integer.toString(Constants.CD_EXT_EDGE_COLOR.getRGB()));
		commentsMap.put("ExtEdgeLabelAlongEdge", "Hodnota pro určení toho, zda má být text hrany, která reprezentuje vztah typu dědičnost mezi třídami v diagramu tříd zobrazen podél hrany nebo ne. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že text bude podél hrany a hodnota 'false', že text nebude podél hrany.");
		classDiagramProperties.setProperty("ExtEdgeLabelAlongEdge", String.valueOf(Constants.CD_EXT_LABEL_ALONG_EDGE));
		commentsMap.put("ExtEdgeLineStyle", "Hodnota pro určení stylu hrany, která reprezentuje vztah typu dědičnost mezi třídami v diagramu tříd. Povolené hodnoty: '-1', '11', '12', '13'. Hodnota '-1' - běžná hrana, hodnota '11' - Orthogonal, hodnota '12' - Bezier a hodnota '13' - Spline.");
		classDiagramProperties.setProperty("ExtEdgeLineStyle", Integer.toString(Constants.CD_EXT_EDGE_LINE_STYLE));
		commentsMap.put("ExtEdgeLineEnd", "Hodnota pro určení typu / stylu konce hrany, která reprezentuje vztah typu dědičnost mezi třídami v diagramu tříd. Povolené hodnoty: '0', '1', '2', '4', '5', '7', '8', '9'. Hodnota '0' - None, hodnota '1' - Classic, hodnota '2' - Technical, hodnota '4' - Simple, hodnota '5' - Circle, hodnota '7' - Line, hodnota '8' - Double line a hodnota '9' - Diamond.");
		classDiagramProperties.setProperty("ExtEdgeLineEnd", Integer.toString(Constants.CD_EXT_EDGE_LINE_END));
		commentsMap.put("ExtEdgeLineBegin", "Hodnota pro určení typu / stylu začátku hrany, která reprezentuje vztah typu dědičnost mezi třídami v diagramu tříd. Povolené hodnoty: '0', '1', '2', '4', '5', '7', '8', '9'. Hodnota '0' - None, hodnota '1' - Classic, hodnota '2' - Technical, hodnota '4' - Simple, hodnota '5' - Circle, hodnota '7' - Line, hodnota '8' - Double line a hodnota '9' - Diamond.");
		classDiagramProperties.setProperty("ExtEdgeLineBegin", Integer.toString(Constants.CD_EXT_EDGE_LINE_BEGIN));
		commentsMap.put("ExtEdgeLineWidth", "Hodnota pro určení šířky hrany, která reprezentuje vztah typu dědičnost mezi třídami v diagramu tříd. Povolené hodnoty <0.1 ; 20.0>. Hodnota '1.0' určena jako výchozí.");
		classDiagramProperties.setProperty("ExtEdgeLineWidth", String.valueOf(Constants.CD_EXT_EDGE_LINE_WIDTH));
		commentsMap.put("ExtEdgeEndLineFill", "Hodnota pro určení toho, zda je konec hrany, která reprezentuje vztah typu dědičnost mezi třídami v diagramu tříd vyplněn / vybarven nebo ne (funguje pouze pro určité styly / typy začátků hran). Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že konec hrany bude vyplněn (při správném tvaru). Hodnota 'false' značí, že konec hrany nebude vyplněn.");
		classDiagramProperties.setProperty("ExtEdgeEndLineFill", String.valueOf(Constants.CD_EXT_EDGE_END_FILL));
		commentsMap.put("ExtEdgeBeginLineFill", "Hodnota pro určení toho, zda je začátek hrany, která reprezentuje vztah typu dědičnost mezi třídami v diagramu tříd vyplněn / vybarven nebo ne (funguje pouze pro určité styly / typy začátků hran). Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že začátek hrany bude vyplněn (při správném tvaru). Hodnota 'false' značí, že začátek hrany nebude vyplněn.");
		classDiagramProperties.setProperty("ExtEdgeBeginLineFill", String.valueOf(Constants.CD_EXT_EDGE_BEGIN_FILL));
					
			
		// Parametry hrany, která reprezentuje implementaci rozhraní: (IMP = Implements)
		commentsMap.put("ImpEdgeLineFontStyle", "Hodnota pro index stylu fontu pro hranu, která reprezentuje implementaci rozhraní v diagramu tříd. Dle zadané hodnoty se aplikuje styl fontu. Možné hodnoty (indexy stylů): '0', '1', '2'. Hodnota '0' - Plain, hodnota '1' - Bold a hodnota '2' - Italic.");
		classDiagramProperties.setProperty("ImpEdgeLineFontStyle", Integer.toString(Constants.CD_IMP_EDGE_LINE_FONT.getStyle()));
		commentsMap.put("ImpEdgeLineFontSize", "Hodnota pro velikost písma pro hranu, která reprezentuje implementaci rozhraní v diagramu tříd. Možné hodnoty <8 ; 150>.");
		classDiagramProperties.setProperty("ImpEdgeLineFontSize", Integer.toString(Constants.CD_IMP_EDGE_LINE_FONT.getSize()));
		commentsMap.put("ImpEdgeLineTextColor", "Hodnota pro barvu textu v RGB hodnotě (bez složky alpha) pro hranu, která reprezentuje implementaci rozhraní v diagramu tříd. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("ImpEdgeLineTextColor", Integer.toString(Constants.CD_IMP_EDGE_FONT_COLOR.getRGB()));
		commentsMap.put("ImpEdgeLineColor", "Hodnota pro barvu hrany, která reprezentuje implementaci rozhraní v diagramu tříd. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("ImpEdgeLineColor", Integer.toString(Constants.CD_IMP_EDGE_COLOR.getRGB()));
		commentsMap.put("ImpEdgeLabelAlongEdge", "Hodnota pro určení toho, zda má být text hrany, která reprezentuje implementaci rozhraní v diagramu tříd zobrazen podél hrany nebo ne. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že text bude podél hrany a hodnota 'false', že text nebude podél hrany.");
		classDiagramProperties.setProperty("ImpEdgeLabelAlongEdge", String.valueOf(Constants.CD_IMP_LABEL_ALONG_EDGE));
		commentsMap.put("ImpEdgeLineStyle", "Hodnota pro určení stylu hrany, která reprezentuje implementaci rozhraní v diagramu tříd. Povolené hodnoty: '-1', '11', '12', '13'. Hodnota '-1' - běžná hrana, hodnota '11' - Orthogonal, hodnota '12' - Bezier a hodnota '13' - Spline.");
		classDiagramProperties.setProperty("ImpEdgeLineStyle", Integer.toString(Constants.CD_IMP_EDGE_LINE_STYLE));
		commentsMap.put("ImpEdgeLineEnd", "Hodnota pro určení typu / stylu konce hrany, která reprezentuje implementaci rozhraní v diagramu tříd. Povolené hodnoty: '0', '1', '2', '4', '5', '7', '8', '9'. Hodnota '0' - None, hodnota '1' - Classic, hodnota '2' - Technical, hodnota '4' - Simple, hodnota '5' - Circle, hodnota '7' - Line, hodnota '8' - Double line a hodnota '9' - Diamond.");
		classDiagramProperties.setProperty("ImpEdgeLineEnd", Integer.toString(Constants.CD_IMP_EDGE_LINE_END));
		commentsMap.put("ImpEdgeLineBegin", "Hodnota pro určení typu / stylu začátku hrany, která reprezentuje implementaci rozhraní v diagramu tříd. Povolené hodnoty: '0', '1', '2', '4', '5', '7', '8', '9'. Hodnota '0' - None, hodnota '1' - Classic, hodnota '2' - Technical, hodnota '4' - Simple, hodnota '5' - Circle, hodnota '7' - Line, hodnota '8' - Double line a hodnota '9' - Diamond.");
		classDiagramProperties.setProperty("ImpEdgeLineBegin", Integer.toString(Constants.CD_IMP_EDGE_LINE_BEGIN));
		commentsMap.put("ImpEdgeLineWidth", "Hodnota pro určení šířky hrany, která reprezentuje implementaci rozhraní v diagramu tříd. Povolené hodnoty <0.1 ; 20.0>. Hodnota '1.0' určena jako výchozí.");
		classDiagramProperties.setProperty("ImpEdgeLineWidth", String.valueOf(Constants.CD_IMP_EDGE_LINE_WIDTH));
		commentsMap.put("ImpEdgeEndLineFill", "Hodnota pro určení toho, zda je konec hrany, která reprezentuje implementaci rozhraní v diagramu tříd vyplněn / vybarven nebo ne (funguje pouze pro určité styly / typy začátků hran). Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že konec hrany bude vyplněn (při správném tvaru). Hodnota 'false' značí, že konec hrany nebude vyplněn.");
		classDiagramProperties.setProperty("ImpEdgeEndLineFill", String.valueOf(Constants.CD_IMP_EDGE_END_FILL));
		commentsMap.put("ImpEdgeBeginLineFill", "Hodnota pro určení toho, zda je začátek hrany, která reprezentuje implementaci rozhraní v diagramu tříd vyplněn / vybarven nebo ne (funguje pouze pro určité styly / typy začátků hran). Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že začátek hrany bude vyplněn (při správném tvaru). Hodnota 'false' značí, že začátek hrany nebude vyplněn.");
		classDiagramProperties.setProperty("ImpEdgeBeginLineFill", String.valueOf(Constants.CD_IMP_EDGE_BEGIN_FILL));
		
		// Uložení pole s výchozím nastaveím rozestupy čerchované hrany, která reprezentuje implementaci:
		commentsMap.put("ImpEdgeDashPattern", "Hodnota pro definování stylu, resp. rozestupů mezi čárkami hrany. Jde o to, že hrana je v případě vztahu implementace rozhraní přerušovaná (/ čerchovaná). Tato hodnota definuje styl / způsob rozezstupů. Možné hodnoty: '10, 2, 2, 2', '10, 10', '5', '12'.");
		classDiagramProperties.setProperty("ImpEdgeDashPattern", Arrays.toString(fromFloatToIntArray(Constants.CD_IMP_PATTERN)));
			
			
			
		// Parametry pro hranu, která reprezentuje agregaci -  1 : 1 
		commentsMap.put("AgrEdgeLineFontStyle", "Hodnota pro index stylu fontu pro hranu, která reprezentuje vztah typu agregace (1 : 1) mezi třídami v diagramu tříd. Dle zadané hodnoty se aplikuje styl fontu. Možné hodnoty (indexy stylů): '0', '1', '2'. Hodnota '0' - Plain, hodnota '1' - Bold a hodnota '2' - Italic.");
		classDiagramProperties.setProperty("AgrEdgeLineFontStyle", Integer.toString(Constants.CD_AGR_EDGE_LINE_FONT.getStyle()));
		commentsMap.put("AgrEdgeLineFontSize", "Hodnota pro velikost písma pro hranu, která reprezentuje vztah typu agregace (1 : 1) mezi třídami v diagramu tříd. Možné hodnoty <8 ; 150>.");
		classDiagramProperties.setProperty("AgrEdgeLineFontSize", Integer.toString(Constants.CD_AGR_EDGE_LINE_FONT.getSize()));
		commentsMap.put("AgrEdgeLineTextColor", "Hodnota pro barvu textu v RGB hodnotě (bez složky alpha) pro hranu, která reprezentuje vztah typu agregace (1 : 1) mezi třídami v diagramu tříd. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("AgrEdgeLineTextColor", Integer.toString(Constants.CD_AGR_EDGE_FONT_COLOR.getRGB()));
		commentsMap.put("AgrEdgeLineColor", "Hodnota pro barvu hrany, která reprezentuje vztah typu agregace (1 : 1) mezi třídami v diagramu tříd. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("AgrEdgeLineColor", Integer.toString(Constants.CD_AGR_EDGE_COLOR.getRGB()));
		commentsMap.put("AgrEdgeLabelAlongEdge", "Hodnota pro určení toho, zda má být text hrany, která reprezentuje vztah typu agregace (1 : 1) mezi třídami v diagramu tříd zobrazen podél hrany nebo ne. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že text bude podél hrany a hodnota 'false', že text nebude podél hrany.");
		classDiagramProperties.setProperty("AgrEdgeLabelAlongEdge", String.valueOf(Constants.CD_AGR_LABEL_ALONG_EDGE));
		commentsMap.put("AgrEdgeLineStyle", "Hodnota pro určení stylu hrany, která reprezentuje vztah typu agregace (1 : 1) mezi třídami v diagramu tříd. Povolené hodnoty: '-1', '11', '12', '13'. Hodnota '-1' - běžná hrana, hodnota '11' - Orthogonal, hodnota '12' - Bezier a hodnota '13' - Spline.");
		classDiagramProperties.setProperty("AgrEdgeLineStyle", Integer.toString(Constants.CD_AGR_EDGE_LINE_STYLE));
		commentsMap.put("AgrEdgeLineEnd", "Hodnota pro určení typu / stylu konce hrany, která reprezentuje vztah typu agregace (1 : 1) mezi třídami v diagramu tříd. Povolené hodnoty: '0', '1', '2', '4', '5', '7', '8', '9'. Hodnota '0' - None, hodnota '1' - Classic, hodnota '2' - Technical, hodnota '4' - Simple, hodnota '5' - Circle, hodnota '7' - Line, hodnota '8' - Double line a hodnota '9' - Diamond.");
		classDiagramProperties.setProperty("AgrEdgeLineEnd", Integer.toString(Constants.CD_AGR_EDGE_LINE_END));
		commentsMap.put("AgrEdgeLineBegin", "Hodnota pro určení typu / stylu začátku hrany, která reprezentuje vztah typu agregace (1 : 1) mezi třídami v diagramu tříd. Povolené hodnoty: '0', '1', '2', '4', '5', '7', '8', '9'. Hodnota '0' - None, hodnota '1' - Classic, hodnota '2' - Technical, hodnota '4' - Simple, hodnota '5' - Circle, hodnota '7' - Line, hodnota '8' - Double line a hodnota '9' - Diamond.");
		classDiagramProperties.setProperty("AgrEdgeLineBegin", Integer.toString(Constants.CD_AGR_EDGE_LINE_BEGIN));
		commentsMap.put("AgrEdgeLineWidth", "Hodnota pro určení šířky hrany, která reprezentuje vztah typu agregace (1 : 1) mezi třídami v diagramu tříd. Povolené hodnoty <0.1 ; 20.0>. Hodnota '1.0' určena jako výchozí.");
		classDiagramProperties.setProperty("AgrEdgeLineWidth", String.valueOf(Constants.CD_AGR_EDGE_LINE_WIDTH));
		commentsMap.put("AgrEdgeEndLineFill", "Hodnota pro určení toho, zda je konec hrany, která reprezentuje vztah typu agregace (1 : 1) mezi třídami v diagramu tříd vyplněn / vybarven nebo ne (funguje pouze pro určité styly / typy začátků hran). Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že konec hrany bude vyplněn (při správném tvaru). Hodnota 'false' značí, že konec hrany nebude vyplněn.");
		classDiagramProperties.setProperty("AgrEdgeEndLineFill", String.valueOf(Constants.CD_AGR_EDGE_END_FILL));
		commentsMap.put("AgrEdgeBeginLineFill", "Hodnota pro určení toho, zda je začátek hrany, která reprezentuje vztah typu agregace (1 : 1) mezi třídami v diagramu tříd vyplněn / vybarven nebo ne (funguje pouze pro určité styly / typy začátků hran). Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že začátek hrany bude vyplněn (při správném tvaru). Hodnota 'false' značí, že začátek hrany nebude vyplněn.");
		classDiagramProperties.setProperty("AgrEdgeBeginLineFill", String.valueOf(Constants.CD_AGR_EDGE_BEGIN_FILL));
			
			
			
		// Parametry pro hranu, která reprezentuje agregaci - bud 1 : N
		commentsMap.put("Agr_1_N_EdgeLineFontStyle", "Hodnota pro index stylu fontu pro hranu, která reprezentuje vztah typu agregace (1 : N) mezi třídami v diagramu tříd. Dle zadané hodnoty se aplikuje styl fontu. Možné hodnoty (indexy stylů): '0', '1', '2'. Hodnota '0' - Plain, hodnota '1' - Bold a hodnota '2' - Italic.");
		classDiagramProperties.setProperty("Agr_1_N_EdgeLineFontStyle", Integer.toString(Constants.CD_AGR_EDGE_N_LINE_FONT.getStyle()));
		commentsMap.put("Agr_1_N_EdgeLineFontSize", "Hodnota pro velikost písma pro hranu, která reprezentuje vztah typu agregace (1 : N) mezi třídami v diagramu tříd. Možné hodnoty <8 ; 150>.");
		classDiagramProperties.setProperty("Agr_1_N_EdgeLineFontSize", Integer.toString(Constants.CD_AGR_EDGE_N_LINE_FONT.getSize()));
		commentsMap.put("Agr_1_N_EdgeLineTextColor", "Hodnota pro barvu textu v RGB hodnotě (bez složky alpha) pro hranu, která reprezentuje vztah typu agregace (1 : N) mezi třídami v diagramu tříd. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("Agr_1_N_EdgeLineTextColor", Integer.toString(Constants.CD_AGR_N_EDGE_FONT_COLOR.getRGB()));
		commentsMap.put("Agr_1_N_EdgeLineColor", "Hodnota pro barvu hrany, která reprezentuje vztah typu agregace (1 : N) mezi třídami v diagramu tříd. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		classDiagramProperties.setProperty("Agr_1_N_EdgeLineColor", Integer.toString(Constants.CD_AGR_N_EDGE_COLOR.getRGB()));
		commentsMap.put("Agr_1_N_EdgeLabelAlongEdge", "Hodnota pro určení toho, zda má být text hrany, která reprezentuje vztah typu agregace (1 : N) mezi třídami v diagramu tříd zobrazen podél hrany nebo ne. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že text bude podél hrany a hodnota 'false', že text nebude podél hrany.");
		classDiagramProperties.setProperty("Agr_1_N_EdgeLabelAlongEdge", String.valueOf(Constants.CD_AGR_N_LABEL_ALONG_EDGE));
		commentsMap.put("Agr_1_N_EdgeLineStyle", "Hodnota pro určení stylu hrany, která reprezentuje vztah typu agregace (1 : N) mezi třídami v diagramu tříd. Povolené hodnoty: '-1', '11', '12', '13'. Hodnota '-1' - běžná hrana, hodnota '11' - Orthogonal, hodnota '12' - Bezier a hodnota '13' - Spline.");
		classDiagramProperties.setProperty("Agr_1_N_EdgeLineStyle", Integer.toString(Constants.CD_AGR_EDGE_N_LINE_STYLE));
		commentsMap.put("Agr_1_N_EdgeLineEnd", "Hodnota pro určení typu / stylu konce hrany, která reprezentuje vztah typu agregace (1 : N) mezi třídami v diagramu tříd. Povolené hodnoty: '0', '1', '2', '4', '5', '7', '8', '9'. Hodnota '0' - None, hodnota '1' - Classic, hodnota '2' - Technical, hodnota '4' - Simple, hodnota '5' - Circle, hodnota '7' - Line, hodnota '8' - Double line a hodnota '9' - Diamond.");
		classDiagramProperties.setProperty("Agr_1_N_EdgeLineEnd", Integer.toString(Constants.CD_AGR_N_EDGE_LINE_END));
		commentsMap.put("Agr_1_N_EdgeLineBegin", "Hodnota pro určení typu / stylu začátku hrany, která reprezentuje vztah typu agregace (1 : N) mezi třídami v diagramu tříd. Povolené hodnoty: '0', '1', '2', '4', '5', '7', '8', '9'. Hodnota '0' - None, hodnota '1' - Classic, hodnota '2' - Technical, hodnota '4' - Simple, hodnota '5' - Circle, hodnota '7' - Line, hodnota '8' - Double line a hodnota '9' - Diamond.");
		classDiagramProperties.setProperty("Agr_1_N_EdgeLineBegin", Integer.toString(Constants.CD_AGR_N_EDGE_LINE_BEGIN));
		commentsMap.put("Agr_1_N_EdgeLineWidth", "Hodnota pro určení šířky hrany, která reprezentuje vztah typu agregace (1 : N) mezi třídami v diagramu tříd. Povolené hodnoty <0.1 ; 20.0>. Hodnota '1.0' určena jako výchozí.");
		classDiagramProperties.setProperty("Agr_1_N_EdgeLineWidth", String.valueOf(Constants.CD_AGR_EDGE_N_LINE_WIDTH));
		commentsMap.put("Agr_1_N_EdgeEndLineFill", "Hodnota pro určení toho, zda je konec hrany, která reprezentuje vztah typu agregace (1 : N) mezi třídami v diagramu tříd vyplněn / vybarven nebo ne (funguje pouze pro určité styly / typy začátků hran). Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že konec hrany bude vyplněn (při správném tvaru). Hodnota 'false' značí, že konec hrany nebude vyplněn.");
		classDiagramProperties.setProperty("Agr_1_N_EdgeEndLineFill", String.valueOf(Constants.CD_AGR_N_EDGE_END_FILL));
		commentsMap.put("Agr_1_N_EdgeBeginLineFill", "Hodnota pro určení toho, zda je začátek hrany, která reprezentuje vztah typu agregace (1 : N) mezi třídami v diagramu tříd vyplněn / vybarven nebo ne (funguje pouze pro určité styly / typy začátků hran). Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že začátek hrany bude vyplněn (při správném tvaru). Hodnota 'false' značí, že začátek hrany nebude vyplněn.");
		classDiagramProperties.setProperty("Agr_1_N_EdgeBeginLineFill", String.valueOf(Constants.CD_AGR_N_EDGE_BEGIN_FILL));
			
			
			
			
			
		/*
		 * Zápis objektu Properties do souboru na zadané cestě.
		 * 
		 * Note:
		 * Jedná se o mnou definované nastavení, jak budou tyto soubory vypadat, toto
		 * již nechci dát jakkoliv na úpravu uživateli. Takto je to z mého pohledu
		 * nejpřehlednější, tak proto pevně definuje mezery nad komentáři apod.
		 */
		WriteToFile.writeProperties(classDiagramProperties, path);

		// Vložím do něj zadané komentáře (s mezerami):
		insertComments(path, COUNT_OF_FREE_LINES_ABOVE_COMMENT);

		// Zapíšu do něj původní komentář a pod něj datum vytvoření:
		WriteToFile.insertCreationComments(path,
				"Soubor, který obsahuje výchozí nastavení pro komponentu diagram tříd, který se nachází v hlavním okně aplikace, kde tvoří převážnou část okna.",
				DEFAULT_CREATION_DATE_LOCALE);
	}
		 
		 
		 
		 
	
	
	
	
	
	/**
	 * Metoda, která na uvedenou cestu zapíše soubor s výchozím nastavením pro graf
	 * / diagram instance diagram, data lze pak uživatelem změnit, soubor obsahuje
	 * výchozí nastavení pro font, barvu pozadí, písma, ...
	 * 
	 * @param path
	 *            - cesta, kam se má soubor zapsat (včetně názvu)
	 */
	public static void writeInstanceDiagramProperties(final String path) {
		final SortedProperties instanceDiagramProperties = new SortedProperties(KEYS_ORDER_PROPERTIES);

		commentsMap.clear();
		
		
		// Přiblížení grafu - Scale:
		commentsMap.put("Scale", "Hodnota, která značí úroveň přiblížení diagramu. Možné hodnoty: <0.5 ; 20.0>. Hodnota '1.0' je 100 % přiblížení.");
		instanceDiagramProperties.setProperty("Scale", String.valueOf(Constants.INSTANCE_DIAGRAM_SCALE));
		
		commentsMap.put("BackgroundColor", "Hodnota, pro barvu pozadí diagramu instancí v RGB hodnotě (bez složky alpha). Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		instanceDiagramProperties.setProperty("BackgroundColor", Integer.toString(Constants.ID_BACKGROUND_COLOR.getRGB()));
		
		commentsMap.put("EdgeLabelMovable", "Hodnota pro určení toho, zda půjdou popisky hran od jejich hran odpojit nebo ne. Jedná se o hrany, které reprezentují naplněné vztahy (proměnné) mezi instancemi tříd v diagramu instancí. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že popisky půjdou odpojit od jejich hran. V případě hodnoty 'false' nelze odpojit popisky od jejich hran.");
		instanceDiagramProperties.setProperty("EdgeLabelMovable", String.valueOf(Constants.ID_EDGE_LABELS_MOVABLE));

		commentsMap.put("ShowInheritedClassNameWithPackages", "Hodnota, která značí, zda se mají v kontextovém menu nad označenou instancí v diagramu instancí v nabídce 'Zděděné metody' zobrazovat názvy tříd (předků) s balíčky nebo bezi nich. Povolené hodnoty 'true' a 'false'. Hodnota 'true' značí, že se v kontextovém menu nad instancí v diagramu instancí v nabídce 'Zděděné metody' budou zobrazovat názvy předků s balíčky, ve kterých se třída nachází. Hodnota 'false' značí, že v kontextovém menu v nabídce 'Zděděné metody' nebudou zobrazovat předkové s balíčky, bude se jednat o pouhý název třídy.");
		instanceDiagramProperties.setProperty("ShowInheritedClassNameWithPackages", String.valueOf(Constants.ID_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES));

		// Hodnota, která značí, zda se mají v kontextovém menu nad třídou zobrazít i metody, které jsou privátní.
		commentsMap.put("IncludePrivateMethods", "Hodnota, která značí, zda se mají v kontextovém menu nad označenou instancí v diagramu instancí zpřístupnit i metody, které jsou soukromé (označené klíčovým slovem 'private'). Povolené hodnoty 'true' a 'false'. Hodnota 'true' značí, že se v kontextovém menu nad instancí v diagramu instancí zobrazí a 'zpřístupní' metody, které jsou soukromé (lze je zavolat). Toto je ale pouze za účelem testování, jinak dle pravidel jazyka Java nelze přístupovat k soukromé metodě od jinud než z třídy samotné. Hodnota 'false' značí, že nebudou v kontextovém menu nad instancí zpřístupněny soukromě metody.");
		instanceDiagramProperties.setProperty("IncludePrivateMethods", String.valueOf(Constants.ID_INCLUDE_PRIVATE_METHODS));

		// Hodnota, která značí, zda se mají v kontextovém menu nad třídou zobrazít i metody, které jsou soukromé.
		commentsMap.put("IncludeProtectedMethods", "Hodnota, která značí, zda se mají v kontextovém menu nad označenou instancí v diagramu instancí zpřístupnit i metody, které jsou chráněné (označené klíčovým slovem 'protected'). Povolené hodnoty 'true' a 'false'. Hodnota 'true' značí, že se v kontextovém menu nad instancí v diagramu instancí zobrazí a 'zpřístupní' metody, které jsou chráněné (lze je zavolat). Toto je ale pouze za účelem testování, jinak dle pravidel jazyka Java nelze přístupovat k chráněné metodě od jinud než z třídy samotné a z jejich potomků. Hodnota 'false' značí, že nebudou v kontextovém menu nad instancí zpřístupněny chráněné metody.");
		instanceDiagramProperties.setProperty("IncludeProtectedMethods", String.valueOf(Constants.ID_INCLUDE_PROTECTED_METHODS));

		// Hodnota, která značí, zda se mají v kontextovém menu nad třídou zobrazít i metody, které jsou package-private.
		commentsMap.put("IncludePackagePrivateMethods", "Hodnota, která značí, zda se mají v kontextovém menu nad označenou instancí v diagramu instancí zpřístupnit i metody, které jsou package-private (bez označení klíčovým slovem pro viditelnost). Povolené hodnoty 'true' a 'false'. Hodnota 'true' značí, že se v kontextovém menu nad instancí v diagramu instancí zobrazí a 'zpřístupní' metody, které jsou package-private (lze je zavolat). Toto je ale pouze za účelem testování, jinak dle pravidel jazyka Java nelze přístupovat k package-private metodě od jinud než z třídy samotné a z tříd ve stejném balíčku. Hodnota 'false' značí, že nebudou v kontextovém menu nad instancí zpřístupněny package-private metody.");
		instanceDiagramProperties.setProperty("IncludePackagePrivateMethods", String.valueOf(Constants.ID_INCLUDE_PACKAGE_PRIVATE_METHODS));
		
		// hodnota pro zpřístupnění proměnných v dialogu pro zavolání metody.
		commentsMap.put("MakeFieldsAvailableForMethod", "Hodnota, která značí, zda se v dialogu pro zavolání metody zpřístupní komponenta pro označení veřejné nebo chráněné proměnné z tříd, instancí tříd nebo proměnných vytvořených uživatelem v editoru příkazů, vždy stejného datového typu jako je příslušný parametr metody. Z označené proměnné se vezme hodnota a předá se do parametru metody pří zavolání. Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se zpřístupní uvedené proměnné. Hodnota 'false' značí, že bude zpřístupněno pouze textové pole pro zadaní parametru metody.");
		instanceDiagramProperties.setProperty("MakeFieldsAvailableForMethod", String.valueOf(Constants.ID_MAKE_FIELDS_AVAILABLE_FOR_CALL_METHOD));		
		
		// Zda se mají zobrazovat vztahy tříd na sebe sama.
		commentsMap.put("ShowRelationsShipsToInstanceItself", "Hodnota pro určení toho, zda se mají zobrazovat vztahy instance na sebe sama. Jedná se o to, že pokud bude mít instance vztah na sebe sama, tak zda se tento vztah má zobrazit v diagramu instancí nebo ne. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se budou v diagramu instancí zobrazovat vztahy instance na sebe sama. Hodnota 'false' značí, že se nebudou zobrazovat vztahy instance na sebe sama.");
		instanceDiagramProperties.setProperty("ShowRelationsShipsToInstanceItself", String.valueOf(Constants.ID_SHOW_RELATION_SHIPS_TO_INSTANCE_ITSELF));

		// Zda se má při zavolání metody zpřístupnit možnost pro vytvoření nové instance, pokud bude parametr metody nějaká třídy z diagramu tříd:
		commentsMap.put("ShowCreateNewInstanceOptionInCallMethod", "Hodnota, která značí, zda se má v dialogu pro zavolání metody poomcí kontextového menu nad instancí zpřístupnit vytvoření nové instance v případě, že jako příslušný parametr metody bude třída, která se nachází v diagramu tříd. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že bude na výběr možnost pro vytvoření nové instance. Hodnota 'false' značí, že budou na výběr pouze instance příslušné třídy, které se nachází v diagramu instancí, pokud v diagramu instancí žádná instance příslušné třídy nebude, metoda nepůjde zavolat.");
		instanceDiagramProperties.setProperty("ShowCreateNewInstanceOptionInCallMethod", String.valueOf(Constants.ID_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD));
		
		// Zda se mají zobrazit metody v dialogu pro zavolání statické metody pro předání návratové hodnoty do parametru metody:
		commentsMap.put("MakeMethodsAvailableForCallMethod", "Hodnota, která značí, zda se mají v dialogu pro zavolání metody nad instancí v diagramu instancí zobrazit i metody. Tzn. Zda bude možné označit metodu, jejíž návratová hodnota se předá do příslušného parametru metody. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se v dialogu pro zavolání metody budou nabízet metody pro předání jejich návratové hodnoty do příslušného parametru metody. Hodnota 'false' značí, že v dialogu pro zavolání metody nad instancí v diagramu instancí nebudou na výběr metody pro předání jejich návratové hodnoty do příslušného parametru metody.");
		instanceDiagramProperties.setProperty("MakeMethodsAvailableForCallMethod", String.valueOf(Constants.ID_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD));
		
		commentsMap.put("UseFastWayToRefreshRelationshipsBetweenInstances", "Hodnota pro určení toho, zda se mají použít algoritmy pro 'rychlou' nebo 'pomalejší' aktualizaci vztahů mezi instancemi v diagramu instancí. 'Rychlá' verze znamená, že se při každé aktualizaci vztahů mezi instancemi v diagramu instancí nejprve vymažou veškeré vztahy mezi všemi instancemi a následně se zjistí vztahy dle naplněných / nenaplněných testovaných proměnných a vytvoří se pouze takové vztahy mezi instancemi, které byly nalezeny. 'Pomalá' verze znamená, že se vždy zjistí vztahy mezi instancemi, které by měli aktuálně existovat v diagramu instancí a dle těchto zjištěných vztahů se v diagramu instancí některé vztahy pouze upraví tak, že se odeberou, přidají nebo ponechazí. Právě toto zjišťování aktuálních vztahů mezi instancemi v diagramu instancí je důvod, prooč se jedná o pomalejší způsob. Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí 'rychlou' verzi a hodnota 'false' značí 'pomalou' verzi zjišťování vztahů mezi instancemi v diagramu instancí.");
		instanceDiagramProperties.setProperty("UseFastWayToRefreshRelationshipsBetweenInstances", String.valueOf(Constants.ID_USE_FAST_WAY_TO_REFRESH_RELATIONSHIPS_BETWEEN_CLASSES));

		commentsMap.put("ShowRelationshipsInInheritedClasses", "Hodnota pro určení toho, zda se mají v diagramu instancí brát pro znázornění vztahů mezi intancemi (třídami) i hodnoty proměnných, které se nacházejí v předcích instance, která se nachází v diagramu instancí. Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se budou v diagramu instancí znázorňovat vztahy, které jsou naplněny v proměnných, které se nacházejí v předcích příslušné instance v diagramu intancí. Hodnota 'false' značí, že se budou pro znázornění vztahů mezi instancemi v diagramu instancí brát pouze hodnoty proměnných v příslušné instanci v diagramu instancí.");
		instanceDiagramProperties.setProperty("ShowRelationshipsInInheritedClasses", String.valueOf(Constants.ID_SHOW_RELATIONSHIPS_IN_INHERITED_CLASSES));
		
		// Zda se má vztah typu asociace znázornit klasicky jako jedna hrana nebo pomocí dvou agregace:
		commentsMap.put("ShowAssociationThroughAggregation", "Hodnota pro určení toho, zda se má vztah typu asociace v diagramu instancí znázornit klasicky jako jedna hrana s šipkami na obou koncích (hodnota 'false'). Nebo, zda se má vztah typu asociace v diagramu instancí znázornit jako dvě agregace 1 : 1 (hodnota 'true'). Povolené hodnoty: 'true', 'false'.");
		instanceDiagramProperties.setProperty("ShowAssociationThroughAggregation", String.valueOf(Constants.ID_SHOW_ASOCIATION_THROUGH_AGREGATION));
		
		// Zda se má v dialogu s přehledem hodnot i instnaci (proměnné, metody, konstruktory) zobrazit u instnací v proměnných, které mají svou reprezentaci v diagramu instancí jejich referenci, kterou jim zadal uživatel při vytvoření:
		commentsMap.put("ShowReferenceVariables", "Hodnota pro určení toho, zda se mají v dialogu s přehledem hodnot o zvolené instanci (dialog s konstruktory, metodami a proměnnými) zobrazit referenční proměnné. Tzn. že se v uvedeném dialogu budou za instancí (v nějaké proměnné typu list, pole nebo 'klasická proměnná') zobrazovat i reference na příslušnou instanci, pokud má příslušná instance v nějaké proměnné svou reprezentaci v diagramu instnací. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se budou v uvedeném dialogu zobrazovat názvy referencí u příslušné instance. Hodnota 'false' značí, že se nebudou v uvedeném dialogu zobrazovat reference na instance v naplněných proměnných.");
		instanceDiagramProperties.setProperty("ShowReferenceVariables", String.valueOf(Constants.ID_SHOW_REFERENCES_VARIABLES));		

		// Zda se mají v dialogu pro zavolání setru na na třídu sebe sama zobrazovat i getry pro předání té hodnoty do prametru setru:
		commentsMap.put("ShowGetterInSetterMethodInInstance", "Hodnota, která značí, zda se v dialogu pro zavolání metody, která značí setr na proměnnou v příslušné instanci třídy v diagramu instancí, kde se ta metoda (setr) nachází mají zobrazovat i metody, které značí getr na stejnou proměnnou ve stejné instanci třídy, kde se ta metoda nachází. Stručně, zda se při zavolání setru mají pro parametry toho setru zobrazovat i getry v té instanci, kde se ten setr nachází. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se budou nabízet možnosti pro předání do parametru setru i návratovou hodnotu getru ve stejné třídě. Hodnota 'false' značí, že se nebude zobrazovat getr pro parametr setru ve stejné instanci třídy, kde se nachází ten setr.");
		instanceDiagramProperties.setProperty("ShowGetterInSetterMethodInInstance", String.valueOf(Constants.ID_SHOW_GETTER_IN_SETTER_METHOD_IN_INSTANCE));
		
		
		
		
		

		// Následují hodnoty pro zvýraznění instance v diagramu instancí například po zavolání metody nebo získání instance z proměnné apod.
		
		// Logická hodnota o tom, zda se vůbec mají instance / buňky v diagramu instancí zvýrazňovat:
		commentsMap.put("HighlightInstances", "Hodnota, která značí, zda se mají zvýrazňovat instance v diagramu instancí. Tzn. Například po zavolání metody nebo získání hodnoty z proměnné apod. Tak, v případě, že ta získaná hodnota je instance, která se nachází v diagramu instancí, tak zda se má v diagramu instancí příslušná buňka / instance zvýraznít nebo ne. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se budou zvýrazňovat instance v diagramu instancí například po zavolání metody či získání instance z proměnné apod. Hodnota 'false' značí, že se nebudou zvýrazňovat instance v diagramu instancí při získání instance z diagramu instancí po zavolání metody či získání z proměnné apod.");
		instanceDiagramProperties.setProperty("HighlightInstances", String.valueOf(Constants.ID_HIGHLIGHT_INSTANCES));
		
		// Čas / interval, který značí dobu, jako má být příslušná buňka / instance zvýrazněna:
		commentsMap.put("HighlightingTime", "Hodnota, která značí, po jakou dobu / interval (v sekundách) zvýraznění příslušné instance / buňky v diagramu instancí. Pokud zde bude číslo '2.0', tak to značí, že instance / buňka v diagramu instancí bude zvýrazněna po dobu dvou vteřin a pak se vrátí do původního 'nastavení' - vzhledu bez zvýraznění. Povolené hodnoty jsou v intervalu: <0.1 ; 5.0>. V aplikaci se zadaný čas / interval vynásobí hodnotu '1000', která jej převede na vteřiny.");
		instanceDiagramProperties.setProperty("HighlightingTime", String.valueOf(Constants.ID_DEFAULT_HIGHLIGHTING_TIME));
		
		// Barva textu / písma zvýrazněné buňky / instance v diagramu instancí:
		commentsMap.put("TextColorOfHighlightedInstance", "Hodnota, která značí barvu písma buňky / instance v případě, že bude buňka / instance v diagramu instancí zvýrazněná. Povolené hodnoty: (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva a druhý konec je černá barva.");
		instanceDiagramProperties.setProperty("TextColorOfHighlightedInstance", Integer.toString(Constants.ID_TEXT_COLOR_OF_HIGHLIGHTED_INSTANCE.getRGB()));

		commentsMap.put("AttributeColorOfHighlightedInstance", "Hodnota, která značí barvu písma atributů v buňce / instanci v případě, že bude buňka / instance v diagramu instancí zvýrazněná. Povolené hodnoty: (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva a druhý konec je černá barva.");
		instanceDiagramProperties.setProperty("AttributeColorOfHighlightedInstance", Integer.toString(Constants.ID_ATTRIBUTE_COLOR_OF_HIGHLIGHTED_INSTANCE.getRGB()));
		
		commentsMap.put("MethodColorOfHighlightedInstance", "Hodnota, která značí barvu písma metod v buňce / instanci v případě, že bude buňka / instance v diagramu instancí zvýrazněná. Povolené hodnoty: (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva a druhý konec je černá barva.");
		instanceDiagramProperties.setProperty("MethodColorOfHighlightedInstance", Integer.toString(Constants.ID_METHOD_COLOR_OF_HIGHLIGHTED_INSTANCE.getRGB()));
		
		// Barva ohraničení zvýrazněné buňky / instance v diagramu instancí:
		commentsMap.put("BorderColorOfHighlightedInstance", "Hodnota, která značí barvu ohraničení buňky / instance v případě, že bude buňka / instance v diagramu instancí zvýrazněná. Povolené hodnoty: (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva a druhý konec je černá barva.");
		instanceDiagramProperties.setProperty("BorderColorOfHighlightedInstance", Integer.toString(Constants.ID_BORDER_COLOR_OF_HIGHLIGHTED_INSTANCE.getRGB()));
		
		// Barva pozadí zvýrazněné buňky / instance v diagramu instancí:
		commentsMap.put("BackgroundColorOfHighlightedInstance", "Hodnota, která značí barvu pozadí buňky / instance v případě, že bude buňka / instance v diagramu instancí zvýrazněná. Povolené hodnoty: (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva a druhý konec je černá barva.");
		instanceDiagramProperties.setProperty("BackgroundColorOfHighlightedInstance", Integer.toString(Constants.ID_BACKGROUND_COLOR_OF_HIGHLIGHTED_INSTANCE.getRGB()));
		
		
		
		
		
		
		
		
		
		// Následují hodnoty pro nastavení atributů a metod - zda se mají v buňce reprentující instanci zobrazit, kolik se jich má zobrazit apod.
		
		// Následují hodnoty pro atributy:
		commentsMap.put("ShowAttributes", "Hodnota, která značí, zda se mají vůbec zobrazit atributy v buňce reprezentující instanci v diagramu instancí. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se v buňce reprezentující instanci třídy z diagramu tříd budou zobrazovat její atributy (buď se zobrazí veškeré atributy nebo jen zvolený počet). Hodnota 'false' značí, že se nebudou zobrazovat atributy v příslušné buňce reprezentující instanci v diagramu instancí.");
		instanceDiagramProperties.setProperty("ShowAttributes", String.valueOf(Constants.ID_SHOW_ATTRIBUTES));
		
		commentsMap.put("ShowAllAttributes", "Hodnota, která značí, zda se mají v buňce reprezentující instanci třídy z diagramu tříd (instance v diagramu instancí) zobrazit veškeré její atributy nebo jen zadaný počet. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se budou v příslušné buňce zobrazovat veškeré atributy, které se nachází v příslušné instanci, kterou ta buňka v diagramu instancí reprezentuje. Hodnota 'false' značí, že se vypíše pouze zadaný počet atributů v příslušné instanci.");
		instanceDiagramProperties.setProperty("ShowAllAttributes", String.valueOf(Constants.ID_SHOW_ALL_ATTRIBUTES));
		
		commentsMap.put("ShowSpecificCountOfAttributes", "Hodnota, která značí počet atributů, který se má vypsat v příslušné buňce reprezentující instanci v diagramu instancí. Tato hodnota se využije pouze v případě, že se mají zobrazit atributy, ale ne všechny atributy. Povolené hodnoty v intervalu: <1 ; 20>.");
		instanceDiagramProperties.setProperty("ShowSpecificCountOfAttributes", Integer.toString(Constants.ID_SPECIFIC_COUNT_OF_ATTRIBUTES));

		commentsMap.put("UseForAttributesOneFontAndFontColorInInstanceCell", "Hodnota, která značí, zda se má pro text, kterým se vypisují atributy využít stejný font a barva písma jako je pro text s referencí. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se má využít pro vypsání atributů do buňky / instance stejný font a barva písma, jako je pro text s referencí. Hodnota 'false' značí, že se pro výpis atributů využije 'zvlášť' nastavený font a barva písma.");
		instanceDiagramProperties.setProperty("UseForAttributesOneFontAndFontColorInInstanceCell", String.valueOf(Constants.ID_USE_FOR_ATTRIBUTES_ONE_FONT_AND_FONT_COLOR_IN_INSTANCE_CELL));

		commentsMap.put("AttributesFontSize", "Hodnota pro velikost písma pro text / písmo, kterým se budou vypisovat atributy v buňce, která reprezentuje instancí třídy v diagramu instancí. Možné hodnoty <8 ; 150>.)");
		instanceDiagramProperties.setProperty("AttributesFontSize", Integer.toString(Constants.ID_ATTRIBUTES_FONT.getSize()));
		
		commentsMap.put("AttributesFontStyle", "Hodnota pro index stylu fontu pro písmo / text, kterým se budou vypisovat atributy v buňce, která reprezentuje instancí třídy v diagramu instancí. Dle zadané hodnoty se aplikuje styl fontu. Možné hodnoty (indexy stylů): '0', '1', '2'. Hodnota '0' - Plain, hodnota '1' - Bold a hodnota '2' - Italic.");
		instanceDiagramProperties.setProperty("AttributesFontStyle", Integer.toString(Constants.ID_ATTRIBUTES_FONT.getStyle()));
		
		commentsMap.put("AttributesTextColor", "Hodnota pro barvu písma / textu kterým se budou vypisovat atributy v buňce, která reprezentuje instanci třídy v diagramu tříd v RGB hodnotě (bez složky alpha). Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		instanceDiagramProperties.setProperty("AttributesTextColor", Integer.toString(Constants.ID_ATTRIBUTES_TEXT_COLOR.getRGB()));
		
		
		// Následují hodnoty pro metody:
		commentsMap.put("ShowMethods", "Hodnota, která značí, zda se mají vůbec zobrazit metody v buňce reprezentující instanci v diagramu instancí. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se v buňce reprezentující instanci třídy z diagramu tříd budou zobrazovat její metody (buď se zobrazí veškeré metody nebo jen zvolený počet). Hodnota 'false' značí, že se nebudou zobrazovat metody v příslušné buňce reprezentující instanci v diagramu instancí.");
		instanceDiagramProperties.setProperty("ShowMethods", String.valueOf(Constants.ID_SHOW_METHODS));
		
		commentsMap.put("ShowAllMethods", "Hodnota, která značí, zda se mají v buňce reprezentující instanci třídy z diagramu tříd (instance v diagramu instancí) zobrazit veškeré její metody nebo jen zadaný počet. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se budou v příslušné buňce zobrazovat veškeré metody, které se nachází v příslušné instanci, kterou ta buňka v diagramu instancí reprezentuje. Hodnota 'false' značí, že se vypíše pouze zadaný počet metod v příslušné instanci.");
		instanceDiagramProperties.setProperty("ShowAllMethods", String.valueOf(Constants.ID_SHOW_ALL_METHODS));
		
		commentsMap.put("ShowSpecificCountOfMethods", "Hodnota, která značí počet metod, který se má vypsat v příslušné buňce reprezentující instanci v diagramu instancí. Tato hodnota se využije pouze v případě, že se mají zobrazit metody, ale ne všechny metody. Povolené hodnoty v intervalu: <1 ; 20>.");
		instanceDiagramProperties.setProperty("ShowSpecificCountOfMethods", Integer.toString(Constants.ID_SPECIFIC_COUNT_OF_METHODS));

		commentsMap.put("UseForMethodsOneFontAndFontColorInInstanceCell", "Hodnota, která značí, zda se má pro text, kterým se vypisují metody využít stejný font a barva písma jako je pro text s referencí. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se má využít pro vypsání metod do buňky / instance stejný font a barva písma, jako je pro text s referencí. Hodnota 'false' značí, že se pro výpis metod využije 'zvlášť' nastavený font a barva písma.");
		instanceDiagramProperties.setProperty("UseForMethodsOneFontAndFontColorInInstanceCell", String.valueOf(Constants.ID_USE_FOR_METHODS_ONE_FONT_AND_FONT_COLOR_IN_INSTANCE_CELL));
		
		commentsMap.put("MethodsFontSize", "Hodnota pro velikost písma pro text / písmo, kterým se budou vypisovat metody v buňce, která reprezentuje instancí třídy v diagramu instancí. Možné hodnoty <8 ; 150>.)");
		instanceDiagramProperties.setProperty("MethodsFontSize", Integer.toString(Constants.ID_METHODS_FONT.getSize()));
		
		commentsMap.put("MethodsFontStyle", "Hodnota pro index stylu fontu pro písmo / text, kterým se budou vypisovat metody v buňce, která reprezentuje instancí třídy v diagramu instancí. Dle zadané hodnoty se aplikuje styl fontu. Možné hodnoty (indexy stylů): '0', '1', '2'. Hodnota '0' - Plain, hodnota '1' - Bold a hodnota '2' - Italic.");
		instanceDiagramProperties.setProperty("MethodsFontStyle", Integer.toString(Constants.ID_METHODS_FONT.getStyle()));
		
		commentsMap.put("MethodsTextColor", "Hodnota pro barvu písma / textu kterým se budou vypisovat metody v buňce, která reprezentuje instanci třídy v diagramu tříd v RGB hodnotě (bez složky alpha). Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		instanceDiagramProperties.setProperty("MethodsTextColor", Integer.toString(Constants.ID_METHODS_TEXT_COLOR.getRGB()));
		
		
		
		
		
		
		
		
		
		
		
		
		
		// Parametry pro buňku, která reprezentuje instanci třídy:
		commentsMap.put("RoundedBorderMargin", "Hodnota, která značí úhel zaoblení hran u buňky, která reprezentuje instanci třídy v diagramu instancí. Povolené hodnoty: <1 ; 60>");
		instanceDiagramProperties.setProperty("RoundedBorderMargin", String.valueOf(Constants.ID_ROUNDED_BORDER_MARGIN));		
		
		commentsMap.put("ShowPackagesInCell", "Hodnota, která značí, zda se má v buňce, která reprezentuje instanci třídy v diagramu instací zobrazit název instance (reference) včetně balíčků, ve kterých se příslušná třída nachází nebo ne. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se má zobrazit název instance (reference) včetně balíčků, ve kterých se příslušná třída nachází. V případě hodnoty 'false' se v buňce zobrazí pouze název instance (reference) (bez balíčků).");
		instanceDiagramProperties.setProperty("ShowPackagesInCell", String.valueOf(Constants.ID_SHOW_PACKAGES_IN_CELL));
		
		commentsMap.put("FontStyle", "Hodnota pro index stylu fontu pro buňku, která reprezentuje instancí třídy v diagramu instancí. Dle zadané hodnoty se aplikuje styl fontu. Možné hodnoty (indexy stylů): '0', '1', '2'. Hodnota '0' - Plain, hodnota '1' - Bold a hodnota '2' - Italic.");
		instanceDiagramProperties.setProperty("FontStyle", Integer.toString(Constants.ID_FONT.getStyle()));
		commentsMap.put("FontSize", "Hodnota pro velikost písma pro buňku, která reprezentuje instancí třídy v diagramu instancí. Možné hodnoty <8 ; 150>.");
		instanceDiagramProperties.setProperty("FontSize", Integer.toString(Constants.ID_FONT.getSize()));
		commentsMap.put("TextColor", "Hodnota pro barvu textu v RGB hodnotě (bez složky alpha) pro buňku, která reprezentuje instanci třídy v diagramu instancí. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		instanceDiagramProperties.setProperty("TextColor", Integer.toString(Constants.ID_TEXT_COLOR.getRGB()));
		
		// Barva ohraničení:
		commentsMap.put("BorderColor", "Hodnota pro barvu ohraničení pro buňku, která reprezentuje instanci třídy v diagramu instancí. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		instanceDiagramProperties.setProperty("BorderColor", Integer.toString(Constants.ID_COLOR_BORDER.getRGB()));
		
		// Barva buňky:
		commentsMap.put("CellColor", "Hodnta pro barvu buňky (uvnitř ohraničení), která reprezentuje instanci třídy v diagramu instancí. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		instanceDiagramProperties.setProperty("CellColor", Integer.toString(Constants.ID_COLOR_CELL.getRGB()));
		// Horizontální a vertikální pozice textu v buňce:
		commentsMap.put("TextHorizontalAlignment", "Hodnota pro horizontální umístění textu v buňce, která v reprezentuje instanci třídy v diagramu instancí. Povolené hodnoty: '0', '2', '4'. Hodnota '0' - Center, hodnota '2' - Left a hodnota '4' - Right.");
		instanceDiagramProperties.setProperty("TextHorizontalAlignment", Integer.toString(Constants.ID_HORIZONTAL_ALIGNMENT));
		commentsMap.put("TextVerticalAlignment", "Hodnota pro vertikální umístění textu v buňce, která v reprezentuje instanci třídy v diagramu instancí. Povolené hodnoty: '0', '1', '3'. Hodnota '0' - Center, hodnota '1' - Top a hodnota '3' - Bottom.");
		instanceDiagramProperties.setProperty("TextVerticalAlignment", Integer.toString(Constants.ID_VERTICAL_ALIGNMENT));
				
		
		
		
		
		// Parametry hrany v instance diagramu, které reprezentují asociace:
		commentsMap.put("AssociationEdgeLineColor", "Hodnota pro barvu hrany, která reprezentuje vztah typu asociace mezi instancemi tříd v diagramu instancí. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		instanceDiagramProperties.setProperty("AssociationEdgeLineColor", Integer.toString(Constants.ID_ASSOCIATION_EDGE_COLOR.getRGB()));
		commentsMap.put("AssociationEdgeLabelsAlongEdge", "Hodnota pro určení toho, zda má být text hrany, která reprezentuje vztah typu asociace mezi instancemi tříd v diagramu instancí zobrazen podél hrany nebo ne. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že text bude podél hrany a hodnota 'false', že text nebude podél hrany.");
		instanceDiagramProperties.setProperty("AssociationEdgeLabelsAlongEdge", String.valueOf(Constants.ID_ASSOCIATION_LABELS_ALONG_EDGE));
		commentsMap.put("AssociationEdgeLineEnd", "Hodnota pro určení typu / stylu konce hrany, která reprezentuje vztah typu asociace mezi instancemi tříd v diagramu instancí. Povolené hodnoty: '0', '1', '2', '4', '5', '7', '8', '9'. Hodnota '0' - None, hodnota '1' - Classic, hodnota '2' - Technical, hodnota '4' - Simple, hodnota '5' - Circle, hodnota '7' - Line, hodnota '8' - Double line a hodnota '9' - Diamond.");
		instanceDiagramProperties.setProperty("AssociationEdgeLineEnd", Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_END));
		commentsMap.put("AssociationEdgeLineBegin", "Hodnota pro určení typu / stylu začátku hrany, která reprezentuje vztah typu asociace mezi instancemi tříd v diagramu instancí. Povolené hodnoty: '0', '1', '2', '4', '5', '7', '8', '9'. Hodnota '0' - None, hodnota '1' - Classic, hodnota '2' - Technical, hodnota '4' - Simple, hodnota '5' - Circle, hodnota '7' - Line, hodnota '8' - Double line a hodnota '9' - Diamond.");
		instanceDiagramProperties.setProperty("AssociationEdgeLineBegin", Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_BEGIN));
		commentsMap.put("AssociationEdgeLineWidth", "Hodnota pro určení šířky hrany, která reprezentuje vztah typu asociace mezi instancemi tříd v diagramu instancí. Povolené hodnoty <0.1 ; 20.0>. Hodnota '1.0' určena jako výchozí.");
		instanceDiagramProperties.setProperty("AssociationEdgeLineWidth", String.valueOf(Constants.ID_ASSOCIATION_EDGE_LINE_WIDTH));
		commentsMap.put("AssociationEdgeLineEndFill", "Hodnota pro určení toho, zda je konec hrany, která reprezentuje vztah typu asociace mezi instancemi tříd v diagramu instancí vyplněn / vybarven nebo ne (funguje pouze pro určité styly / typy začátků hran). Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že konec hrany bude vyplněn (při správném tvaru). Hodnota 'false' značí, že konec hrany nebude vyplněn.");
		instanceDiagramProperties.setProperty("AssociationEdgeLineEndFill", String.valueOf(Constants.ID_ASSOCIATION_EDGE_LINE_END_FILL));
		commentsMap.put("AssociationEdgeLineBeginFill", "Hodnota pro určení toho, zda je začátek hrany, která reprezentuje vztah typu asociace mezi instancemi tříd v diagramu instancí vyplněn / vybarven nebo ne (funguje pouze pro určité styly / typy začátků hran). Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že začátek hrany bude vyplněn (při správném tvaru). Hodnota 'false' značí, že začátek hrany nebude vyplněn.");
		instanceDiagramProperties.setProperty("AssociationEdgeLineBeginFill", String.valueOf(Constants.ID_ASSOCIATION_EDGE_LINE_BEGIN_FILL));	
		commentsMap.put("AssociationEdgeLineTextColor", "Hodnota pro barvu textu v RGB hodnotě (bez složky alpha) pro hranu, která reprezentuje vztah typu asociace mezi instancemi tříd v diagramu instancí. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		instanceDiagramProperties.setProperty("AssociationEdgeLineTextColor", Integer.toString(Constants.ID_ASSOCIATION_EDGE_FONT_COLOR.getRGB()));		
		commentsMap.put("AssociationEdgeLineFontSize", "Hodnota pro velikost písma pro hranu, která reprezentuje vztah typu asociace mezi instancemi tříd v diagramu instancí. Možné hodnoty <8 ; 150>.");
		instanceDiagramProperties.setProperty("AssociationEdgeLineFontSize", Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getSize()));
		commentsMap.put("AssociationEdgeLineFontStyle", "Hodnota pro index stylu fontu pro hranu, která reprezentuje vztah typu asociace mezi instancemi tříd v diagramu instancí. Dle zadané hodnoty se aplikuje styl fontu. Možné hodnoty (indexy stylů): '0', '1', '2'. Hodnota '0' - Plain, hodnota '1' - Bold a hodnota '2' - Italic.");
		instanceDiagramProperties.setProperty("AssociationEdgeLineFontStyle", Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getStyle()));
		
		
		// Parametry pro hranu, reprezentující agregaci:
		commentsMap.put("AggregationEdgeLineColor", "Hodnota pro barvu hrany, která reprezentuje vztah typu agregace mezi instancemi tříd v diagramu instancí. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		instanceDiagramProperties.setProperty("AggregationEdgeLineColor", Integer.toString(Constants.ID_AGGREGATION_EDGE_COLOR.getRGB()));
		commentsMap.put("AggregationEdgeLabelsAlongEdge", "Hodnota pro určení toho, zda má být text hrany, která reprezentuje vztah typu agregace mezi instancemi tříd v diagramu instancí zobrazen podél hrany nebo ne. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že text bude podél hrany a hodnota 'false', že text nebude podél hrany.");
		instanceDiagramProperties.setProperty("AggregationEdgeLabelsAlongEdge", String.valueOf(Constants.ID_AGGREGATION_LABELS_ALONG_EDGE));
		commentsMap.put("AggregationEdgeLineEnd", "Hodnota pro určení typu / stylu konce hrany, která reprezentuje vztah typu agregace mezi instancemi tříd v diagramu instancí. Povolené hodnoty: '0', '1', '2', '4', '5', '7', '8', '9'. Hodnota '0' - None, hodnota '1' - Classic, hodnota '2' - Technical, hodnota '4' - Simple, hodnota '5' - Circle, hodnota '7' - Line, hodnota '8' - Double line a hodnota '9' - Diamond.");
		instanceDiagramProperties.setProperty("AggregationEdgeLineEnd", Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_END));
		commentsMap.put("AggregationEdgeLineBegin", "Hodnota pro určení typu / stylu začátku hrany, která reprezentuje vztah typu agregace mezi instancemi tříd v diagramu instancí. Povolené hodnoty: '0', '1', '2', '4', '5', '7', '8', '9'. Hodnota '0' - None, hodnota '1' - Classic, hodnota '2' - Technical, hodnota '4' - Simple, hodnota '5' - Circle, hodnota '7' - Line, hodnota '8' - Double line a hodnota '9' - Diamond.");
		instanceDiagramProperties.setProperty("AggregationEdgeLineBegin", Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_BEGIN));
		commentsMap.put("AggregationEdgeLineWidth", "Hodnota pro určení šířky hrany, která reprezentuje vztah typu agregace mezi instancemi tříd v diagramu instancí. Povolené hodnoty <0.1 ; 20.0>. Hodnota '1.0' určena jako výchozí.");
		instanceDiagramProperties.setProperty("AggregationEdgeLineWidth", String.valueOf(Constants.ID_AGGREGATION_EDGE_LINE_WIDTH));
		commentsMap.put("AggregationEdgeLineEndFill", "Hodnota pro určení toho, zda je konec hrany, která reprezentuje vztah typu agregace mezi instancemi tříd v diagramu instancí vyplněn / vybarven nebo ne (funguje pouze pro určité styly / typy začátků hran). Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že konec hrany bude vyplněn (při správném tvaru). Hodnota 'false' značí, že konec hrany nebude vyplněn.");
		instanceDiagramProperties.setProperty("AggregationEdgeLineEndFill", String.valueOf(Constants.ID_AGGREGATION_EDGE_LINE_END_FILL));
		commentsMap.put("AggregationEdgeLineBeginFill", "Hodnota pro určení toho, zda je začátek hrany, která reprezentuje vztah typu agregace mezi instancemi tříd v diagramu instancí vyplněn / vybarven nebo ne (funguje pouze pro určité styly / typy začátků hran). Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že začátek hrany bude vyplněn (při správném tvaru). Hodnota 'false' značí, že začátek hrany nebude vyplněn.");
		instanceDiagramProperties.setProperty("AggregationEdgeLineBeginFill", String.valueOf(Constants.ID_AGGREGATION_EDGE_LINE_BEGIN_FILL));	
		commentsMap.put("AggregationEdgeLineTextColor", "Hodnota pro barvu textu v RGB hodnotě (bez složky alpha) pro hranu, která reprezentuje vztah typu agregace mezi instancemi tříd v diagramu instancí. Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		instanceDiagramProperties.setProperty("AggregationEdgeLineTextColor", Integer.toString(Constants.ID_AGGREGATION_EDGE_FONT_COLOR.getRGB()));
		commentsMap.put("AggregationEdgeLineFontSize", "Hodnota pro velikost písma pro hranu, která reprezentuje vztah typu agregace mezi instancemi tříd v diagramu instancí. Možné hodnoty <8 ; 150>.");
		instanceDiagramProperties.setProperty("AggregationEdgeLineFontSize", Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getSize()));
		commentsMap.put("AggregationEdgeLineFontStyle", "Hodnota pro index stylu fontu pro hranu, která reprezentuje vztah typu agregace mezi instancemi tříd v diagramu instancí. Dle zadané hodnoty se aplikuje styl fontu. Možné hodnoty (indexy stylů): '0', '1', '2'. Hodnota '0' - Plain, hodnota '1' - Bold a hodnota '2' - Italic.");
		instanceDiagramProperties.setProperty("AggregationEdgeLineFontStyle", Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getStyle()));
		
		
		
		
		
		/*
		 * Zápis objektu Properties do souboru na zadané cestě.
		 * 
		 * Note:
		 * Jedná se o mnou definované nastavení, jak budou tyto soubory vypadat, toto
		 * již nechci dát jakkoliv na úpravu uživateli. Takto je to z mého pohledu
		 * nejpřehlednější, tak proto pevně definuje mezery nad komentáři apod.
		 */
		WriteToFile.writeProperties(instanceDiagramProperties, path);
		
		// Vložím do něj zadané komentáře (s mezerami):
		insertComments(path, COUNT_OF_FREE_LINES_ABOVE_COMMENT);

		// Zapíšu do něj původní komentář a pod něj datum vytvoření:
		WriteToFile.insertCreationComments(path,
				"Soubor, který obsahuje výchozí nastavení pro komponentu diagram instancí, který se nachází v pravé horní části hlavního okna aplikace.",
				DEFAULT_CREATION_DATE_LOCALE);
	}
	
	
	
	
	
		 
	
	
	/**
	 * Metoda, která na uvedenou cestu - pod daným názvem vytvoří nový soubor jehož
	 * obsahem bude výchozí nastavení pro komponentu editor příkazů (command editor)
	 * v apikaci. Výhozí nastavení jako například: barva pozadí, písma, font, ...
	 * 
	 * Jedná se o hodnoty pro soubor CommandEditor.properties
	 * 
	 * @param path
	 *            - cesta i s názvem, kam se má soubor zapsat (včetně názvu)
	 */
	public static void writeCommandEditorProperties(final String path) {
		final SortedProperties commandEditorProperties = new SortedProperties(KEYS_ORDER_PROPERTIES);

		commentsMap.clear();
		
		// Nastavení výchozích hodnot:
		commentsMap.put("FontStyle", "Hodnota pro index stylu fontu v editoru příkazů. Dle zadané hodnoty se aplikuje styl fontu. Možné hodnoty (indexy stylů): '0', '1', '2'.");
		commandEditorProperties.setProperty("FontStyle", Integer.toString(Constants.DEFAULT_FONT_FOR_COMMAND_EDITOR.getStyle()));
		
		commentsMap.put("FontSize", "Hodnota pro velikost písma v editoru příkazů. Možné hodnoty (možné velikosti písma): <8 ; 100>");
		commandEditorProperties.setProperty("FontSize", Integer.toString(Constants.DEFAULT_FONT_FOR_COMMAND_EDITOR.getSize()));
		
		commentsMap.put("BackgroundColor", "Barva pozadí pro editor příkazů v RGB hodnotě (bez složky alpha). Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		commandEditorProperties.setProperty("BackgroundColor", Integer.toString(Constants.COMMAND_EDITOR_BACKGROUND_COLOR.getRGB()));
		
		commentsMap.put("ForegroundColor", "Barva písma pro editor příkazů v RGB hodnotě (bez složky alpha). Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		commandEditorProperties.setProperty("ForegroundColor", Integer.toString(Constants.COMMAND_EDITOR_FOREGROUND_COLOR.getRGB()));
		
		commentsMap.put("HighlightingJavaCode", "Hodnota, která značí, zda se má v editoru příkazů zvýrazňovat syntaxe jazyka Java nebo ne. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se ve zmíněném editoru bude zvýrazňovat syntaxe jazyka Java. Hodnota 'false' značí, že se nebude zvýrazňovat syntaxe jazyka Java. Barvy textu a pozadí budou dle nastavených parametrů.");
		commandEditorProperties.setProperty("HighlightingJavaCode", String.valueOf(Constants.COMMAND_EDITOR_HIGHLIGHTING_CODE));


        /*
         * Následují hodnoty pro nastavení "vlastností" pro okno s hodnotami pro okno pro automatické doplňování /
         * dokončování příkazů v editoru příkazů.
         *
         * (Okno, které se zobrazí po stisknutí kombinace kláves Ctrl + Space)
         */
        commentsMap.put("ShowAutoCompleteWindow", "Hodnota, která značí, zda se má v editoru příkazů zobrazit po stisknutí kombinace kláves Ctrl + Space okno s hodnotami pro automatické doplňování / dokončování příkazů. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se v editoru příkazů zobrazí po stisknutí zmíněné kombinace kláves okno s hodnotami pro doplnění. Hodnota 'false' značí, že se nebude zobrazovat zmíněné okno s hodnotami v editoru příkazů.");
        commandEditorProperties.setProperty("ShowAutoCompleteWindow", String.valueOf(Constants.CE_SHOW_AUTO_COMPLETE_WINDOW));

        commentsMap.put("ShowReferenceVariablesInCompleteWindow", "Hodnota, která značí, zda se mají u hodnot proměnných v instancích zobrazené v okně pro automatické dokončování příkazů zobrazovat i referenční proměnné. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se budou zobrazovat u hodnot i referenční proměnné v případě, že je v příslušné proměnné uložena reference na instanci, která se nachází v diagramu instancí. Hodnota 'false' značí, že se nebudou zobrazovat u hodnot referenční proměnné. Pouze samotné hodnoty proměnných, případně reference na instanci, ve které se proměnná nachází.");
        commandEditorProperties.setProperty("ShowReferenceVariablesInCompleteWindow", String.valueOf(Constants.CE_SHOW_REFERENCE_VARIABLES_IN_COMPLETE_WINDOW));

        commentsMap.put("AddSyntaxForFillingTheVariable", "Hodnota, která značí, zda se má v okně pro doplňování příkazů zobrazovat i syntaxe pro naplnění / deklaraci proměnných v editoru příkazů. Pokud je v instanci proměnná specifického datového typu (číslo, znak, text, logická hodnota, pole nebo list, také uvedených datových typů), bude možné vytvořit nebo naplnit hodnotou z této proměnné proměnnou v editoru příkazů. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se u proměnných specifického datového typu zobrazí i připravené syntaxe pro naplnění / deklaraci proměnné v editoru příkazů. Hodnota 'false' značí, že nebude zobrazena syntaxe pro deklaraci / naplnění proměnné v editoru příkazů. uživatel jej bude muset napsat sám.");
        commandEditorProperties.setProperty("AddSyntaxForFillingTheVariable", String.valueOf(Constants.CE_ADD_SYNTAX_FOR_FILLING_THE_VARIABLE));

        commentsMap.put("AddFinalWordForFillingTheVariable", "Hodnota, která značí, zda se má v okně pro doplňování příkazů při vytváření / deklaraci 'klasické' proměnné přidat k deklaraci klíčové slovo 'final'. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že v připravené syntaxi pro deklaraci 'klasické' proměnné bude klíčové slovo 'final'. Hodnota 'false' značí, že v připravených syntaxích pro deklaraci proměnné nebude uvedené kličové slovo 'final'.");
        commandEditorProperties.setProperty("AddFinalWordForFillingTheVariable", String.valueOf(Constants.CD_ADD_FINAL_WORD_FOR_FILLING_THE_VARIABLE));

        commentsMap.put("ShowExamplesOfSyntax", "Hodnota, která značí, zda se mají v okně pro doplňování příkazů zobrazovat i připravené ukázky 'základních' syntaxí různých příkazů, které lze použít v editoru příkazů. Stačí zadat klíčové slovo 'example', které vyfiltruje připravené ukázky. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že v okně pro doplňování příkazů budou k dispozici ukázky syntaxí 'základních' příkazů v editoru příkazů. Hodnota 'false' značí, že v okně pro doplňování příkazů nebudou k dispozici ukázky příkazů pro editor příkazů.");
        commandEditorProperties.setProperty("ShowExamplesOfSyntax", String.valueOf(Constants.CE_SHOW_EXAMPLES_OF_SYNTAX));

        commentsMap.put("ShowDefaultMethods", "Hodnota, která značí, zda se mají v okně pro doplňování příkazů zobrazovat výchozí metody, které jsou specifické pro editor příkazů. Jedná se o metody například pro výpis hodnot do editoru výstupů, výpis vytvořených proměnných, výpis dostupných příkladů (nápovědy) apod. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že budou k dispozici na výběr zmíněné metody specifické pro editor příkazů. Hodnota 'false' značí, že nebudou na výběr zmíněné metody. Budou k dispozici pouze hodnoty specifické pro třídy a instance v otevřeném projektu.");
        commandEditorProperties.setProperty("ShowDefaultMethods", String.valueOf(Constants.CE_SHOW_DEFAULT_METHODS));


        // Vlastnosti pro okno s historií příkazů, které uživatel zadal v rámci otevřeného projektu.
        commentsMap.put("ShowProjectHistoryWindow", "Hodnota, která značí, zda se má zpřístupnit okno s historií zadaných příkazů v rámci otevřeného projektu. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se po stisknutí příslušné klávesové zkratky zobrazí okno s historií zadanch příkazů v otevřeném projektu. Hodnota 'false' značí, že se nebude ukládat historie zadaných příkazů v rámci otevřeného projektu. Tudíž okno nebude zpřístupněno.");
        commandEditorProperties.setProperty("ShowProjectHistoryWindow", String.valueOf(Constants.CE_SHOW_PROJECT_HISTORY_WINDOW));

        commentsMap.put("IndexCommandsInProjectHistoryWindow", "Hodnota, která značí, zda se mají číslovat příkazy v okně s historií příkazů zadaných v rámci otevřeného projektu. Pokud ano, nelze filtrovat příkazy zadáním jeho části. Pokud ne, příkazy nebudou seřazeny dle pořadí zadání. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se zadané příkazy budou číslovat, lze je vyhledat dle pořadí, ne části příkazu. Hodnota 'false' značí, že se příkazy nebudou řadit dle pořadí zadání, lze je vyhledat zadáním jeho části.");
        commandEditorProperties.setProperty("IndexCommandsInProjectHistoryWindow", String.valueOf(Constants.CE_INDEX_COMMANDS_IN_PROJECT_HISTORY_WINDOW));


        // Vlastnosti pro okno s historií příkazů, které uživatel zadal v od spuštění aplikace.
        commentsMap.put("ShowCompleteHistoryWindow", "Hodnota, která značí, zda se má zpřístupnit okno s historií zadaných příkazů, které uživatel zadal do editoru příkazů od spuštění aplikace. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se po stisknutí příslušné klávesové zkratky zobrazí okno s historií zadanch příkazů. Hodnota 'false' značí, že se nebude ukládat historie zadaných příkazů od spuštění aplikace. Tudíž okno nebude zpřístupněno.");
        commandEditorProperties.setProperty("ShowCompleteHistoryWindow", String.valueOf(Constants.CD_SHOW_COMPLETE_HISTORY_WINDOW));

        commentsMap.put("IndexCommandsInCompleteHistoryWindow", "Hodnota, která značí, zda se mají číslovat příkazy v okně s historií příkazů zadaných uživatelem od spuštění aplikace. Pokud ano, nelze filtrovat příkazy zadáním jeho části. Pokud ne, příkazy nebudou seřazeny dle pořadí zadání. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se zadané příkazy budou číslovat, lze je vyhledat dle pořadí, ne části příkazu. Hodnota 'false' značí, že se příkazy nebudou řadit dle pořadí zadání, lze je vyhledat zadáním jeho části.");
        commandEditorProperties.setProperty("IndexCommandsInCompleteHistoryWindow", String.valueOf(Constants.CD_INDEX_COMMANDS_IN_COMPLETE_HISTORY_WINDOW));



        /*
         * Zápis obejktu Properties do souboru na zadané cestě.
         *
         * Note:
         *
         * Jedná se o mnou definované nastavení, jak budou tyto soubory vypadat, toto již nechci dát jakkoliv na
         * úpravu uživateli. Takto je to z mého pohledu nejpřehlednější, tak proto pevně definuje mezery nad
         * komentáři apod.
         */
        WriteToFile.writeProperties(commandEditorProperties, path);

        // Vložím do něj zadané komentáře (s mezerami):
        insertComments(path, COUNT_OF_FREE_LINES_ABOVE_COMMENT);

        // Zapíšu do něj původní komentář a pod něj datum vytvoření:
        WriteToFile.insertCreationComments(path,
                "Soubor, který obsahuje výchozí nastavení pro komponentu editor příkazů, která se nachází v levé " +
                        "spodní části hlavního okna aplikace.",
                DEFAULT_CREATION_DATE_LOCALE);
    }
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří nový soubor, který bude obsahovat výchozí nastavení pro
	 * aplikaci, jedná se o hodnoty pro soubor Default.properties.
	 * 
	 * @param path
	 *            - cesta k souboru i s názvem
	 */
	public static void writeDefaultPropertiesAllNew(final String path) {
		final SortedProperties properties = new SortedProperties(KEYS_ORDER_PROPERTIES);
		
		commentsMap.clear();

		// Uživatelské jméno:
		commentsMap.put("UserName", "Hodnota, která značí uživatelské jméno. Jedná se o jméno, které se bude zapisovat jako autor vytořených projektů, tříd, při zápisech do logů apod. Výchozí jméno bude jméno přihlášeného uživatele v OS.");
		properties.setProperty("UserName", UserNameSupport.getUsedUserName());

		// Vlastnosti, resp hodnoty v souboru:
		commentsMap.put("Display WorkspaceChooser", "Hodnota, která značí, zda se má při spuštění aplikace zobrazit dialog (okno) pro dotaz na umístění adresáře označeného jako workspace. Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se při spuštění aplikace zobrazí okno s dotazem naumístěění adresáře workspace. Pokud je zadána hodnota 'false', zobrazí se dialog s dotazem na adresář workspace pouze v případě, že není dosud zadán nebo již neexistuje.");
		properties.setProperty("Display WorkspaceChooser", String.valueOf(Constants.DEFAULT_DISPLAY_WORKSPACE));
		
		commentsMap.put("Language", "Hodnota, která značí jazyk aplikace. Povolené hodnoty: 'CZ', 'EN'. Hodnota 'CZ' značí čeký jazyk a hodnota 'EN' značí anglický jazyk.");
		properties.setProperty("Language", Constants.DEFAULT_LANGUAGE_NAME);
		
		commentsMap.put("Loading Image", "Jedná se o název obrázku typu '.gif', který bude zobrazen v tzv. LoadingDialogu, který se zobrazí po každé, když bude aplikace zpracovávat nějaká data, například uživatel označí třídy, mezi kterými se má vytvořit nějaký vztah, tak se na malý okamžit zobrazí tento dialog, který znázorňuje nějaké zpracovávání na pozadí, aby si uživatel nemyslel, že došlo k chybě apod. Aby to nevypadalo, že se aplikace sekla. Povolené hodnoty (vše s příponou '.gif'): 'balls', 'battery', 'box', 'clock', 'dashinfinity', 'default', 'ellipsis', 'flickr', 'gear', 'gears', 'hourglass', 'infinity', 'pacman', 'reload', 'ring', 'ripple', 'squares', 'sunny', 'triangle', 'wave', 'wheel'.");
		properties.setProperty("Loading Image", Constants.DEFAULT_IMAGE_FOR_LOADING_PANEL);
		
		// Výchozí hodnota pro řazení textů v souborech .properties
		commentsMap.put("Default keys order", "Hodnota, dle které se budou řadit hodnoty v souborech .properties, které obsahují texty pro aplikaci. Povolené hodnoty: 'Without_Sorting', 'Ascending_By_Adding', 'Descending_By_Adding', 'Ascending_By_Alphabet' nebo 'Descending_By_Alphabet'. Hodnota 'Without_Sorting' značí, že texty v souborech nebudou nijak seřazeny, bude aplikováno výchozí řazení. Hodnota 'Ascending_By_Adding' značí, že texty v souborech budou seřazeny vzestupně dle pořadí přidání, při této možnosti je možné vložit i komentáře. Hodnota 'Descending_By_Adding' značí, že texty budou seřazeny sestupně dle pořadí přidání, při této možnosti je možné vložit i komentáře. Hodnota 'Ascending_By_Alphabet' značí, že texty budou seřazeny vzestupně dle abecedy (řadí se dle klíčů). Hodnota 'Descending_By_Alphabet' značí, že texty budou seřazeny sestupně dle abecedy (řadí se dle klíčů).");
		properties.setProperty("Default keys order", Constants.DEFAULT_KEYS_ORDER.getValueForFile());
				
		// Výchozí hodnota prot to, zda se mají vkládat komentář do souboru properties s texty pro aplikaci:
		commentsMap.put("Add comments", "Hodnota, dle které se pozná (dále záleží na způsobu řazení), zda se mají vkládat komentáře do souborů .properties obsahující texty pro aplikaci. Povolené hodnoty 'true' a 'false'. Pokud bude hodnota 'true' a způsob řazení 'Ascending_By_Adding' nebo 'Descending_By_Adding', budou se vkládat komentáře. V ostatních případech se nebudou vkládat komentáře do souborů .properties s texty pro aplikaci.");
		properties.setProperty("Add comments", String.valueOf(Constants.DEFAULT_VALUE_ADD_COMMENTS));
		
		// Výchozí počet volných řádků před komentářem:
		commentsMap.put("Count of free lines before comment", "Hodnota značí počet volných řákdů nad komentářem v souborech .properties obsahující texty pro aplikaci. Povolené hodnoty <0 ; 100>. Zadaný počet volných řádků nad komentářemse aplikuje pouze v případě, že se budou komentáře vkládat do příslušných souborů.");
		properties.setProperty("Count of free lines before comment", String.valueOf(Constants.DEFAULT_COUNT_OF_FREE_LINES_BEFORE_COMMENT));



		// Definovaný způsob řazení položek (/ dříve otevřených projektů) v rozevíracím menu "Nedávno otevřené" v menu "Projekt" v hlavním menu / okně aplikace:
		commentsMap.put("Sorting of recently opened projects", "Hodnota značí způsob řazení položek (/ dříve otevřených projektů) v rozevírací nabídce 'Nedávno otevřené' v menu 'Projekt'. Povolené hodnoty 'ASC' a 'DES'. Hodnota 'ASC' značí vzestupné řazení dříve otevřených projektů dle posledního data otevření projektu. Hodnota 'DES' značí sestupné řazení dříve otevřených projektů dle posledního data otevření projektu.");
		properties.setProperty("Sorting of recently opened projects", Constants.DEFAULT_SORT_RECENTLY_OPENED_PROJECTS.getValue());


		
		// Výchozí Locale pro zadání datumu vytvoření v příslušné podobně dle Locale pro soubory .properties obsahující texty pro aplikaci.
		commentsMap.put("Properties locale", "Hodnota značí 'globální umístění' a je využita pro formátování datum vytvoření v souborech .properties obsahující texty pro aplikaci. Tyto datumy se nacházejí na prvním nebo druhém řádku v uvedených souborech. Povolené hodnoty záleží vždy na aktuálním JVM, ale (asi téměř) v každém případě by měly být povoleny alespoň hodnoty: " + Arrays.toString(getLocalesModel()));
		properties.setProperty("Properties locale", Constants.DEFAULT_LOCALE_FOR_PROPERTIES.toLanguageTag());
		


		
		commentsMap.put("Search compile files", "Hodnota, která značí, zda se při spuštění aplikace má zjistit, zda je nastaven adresář a obsahuje soubory, které aplikace potřebuje pro kompilování tříd. Pokud není, pak se prohledá disk nebo rodičovkské adresáře adresáře, kde je nainstalovaná Java a zkusí je najít a zkopírovat do workspace/configuration/libTools/lib/zde. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se po spuštění aplikace otestuje, zda je adresář nastaven, případně se prohledá disk daného zařízení ve snaze najít příslušné soubory. Hodnota 'false' značí, že se nebude testovat, zda je příslušný adresář se soubory potřebnými pro kompilaci nastaven a až se aplikace pokusí zkompilovat nějakou třídu, vyhodí se oznámení, že nejsou nastaveny příslušné soubory.");
		properties.setProperty("Search compile files", String.valueOf(Constants.SEARCH_COMPILE_FILES_AFTER_START_UP_APP));
		
		
		// Logická hodnota o tom, zda se má prohledávat celý disk nebo jen rodičovksé adresáře ve vklákně pro hledání souborů, které aplikace potřebuje, aby mohla kompilovat Javovské třídy.
		commentsMap.put("Search whole disk for compile files", "Hodnota, která značí, zda se má prohledat celý disk nebo jen veškeré rodičovské adresáře od získaného domovského adresáře Javy (adresáře, kde je nainstalovaná Java) za účelem nalezení souborů, které aplikace potřebuji, aby mohla kompilovat Javovksé třídy. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se bude prohledávat celý disk za účelem nalezení zmíněných souborů, zde ale hrozí riziko nalezní chybných souborů se stejným názvem. Hodnota 'false' značí, že se nebude prohledávat celý disk, pouze rodičovksé adresáře od adresáře, kde je nainstalovaná Java (doporučeno).");
		properties.setProperty("Search whole disk for compile files", String.valueOf(Constants.SEARCH_ENTIRE_DISK_FOR_COMPILE_FILES));
		

		// Logická proměnná o tom, zda se mají vypisovat nezachycené výjimky do dialogu Výstupní terminál.
		commentsMap.put("Write not captured exceptions", "Hodnota, která značí, zda se mají do dialogu 'Výstupní terminál' vypisovat výjimky, které nastavou v aplikaci " + Constants.APP_TITLE + " a nepodaří se je zachytit (nejsou v bloku try - catch). Veškeré výpisy aplikace jsou přesměrovány do uvedeného dialogu, kde je možnost filtrovat texty, které se do toho dialogu mají vypsat a které ne. Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se budou vypisovat výjimky, které nebudou zachyceny do uvedeného dialogu. Hodnota 'false' značí, že se nebudou vypisovat nezachycené výjimky do uvedeného dialogu (pouze pokud budou obshovat text, který bude filtrován).");
		properties.setProperty("Write not captured exceptions", String.valueOf(Constants.WRITE_NOT_CAPTURED_EXCEPTIONS_TO_OUTPUT_TERMINAL));


		// Index LookAndFeelSupport, který se má nastavit při spuštění aplikace.
		commentsMap.put("LookAndFeel", "Hodnota, která značí 'design' aplikace. Hodnota: " + Constants.WITHOUT_SPECIFIC_LOOK_AND_FEEL + " značí, že se aplikuje výchozí styl. Hodnoty specifické pro využívanou platformu: [" + LookAndFeelSupport.getSupportedLookAndFeelsInText() + "].");
		properties.setProperty("LookAndFeel", Constants.DEFAULT_LOOK_AND_FEEL);
		
		
		
		
		
		
		
		
		
		
		// Nastavení do dilogu FontSizeForm:
		
		/*
		 * Následující hodnoty značí pouze nastavení, které zvolil uživatel v dialogu
		 * pro "rychlé" nastavení velikosti písma pro objekty v diagramu tříd a v
		 * diagramu instancí.
		 * 
		 * Jelikož se jedná pouze o nastavení například označených komponent JCheckBox v
		 * dialogu FontSizeForm nebo označený panel nebo položky v menu adpod. Nebudu
		 * kvůli tomu vytvářet nový konfigurační soubor, protože konfigurační soubory
		 * beru jako nastavení jednotlivých komponent pro aplikaci. Ale dialog pro to
		 * nastavení písma již neberu jako dialog, navíc je potřeba uložit jen informace
		 * ohledně označených komponent v dialogu a ne žádná nastavení nějaké komponenty
		 * v aplikaci, jako je například nastavení diagramu tříd apod. Proto nebudu
		 * přidávat nový konfigurační soubor, ale přidám tyto informace sem do
		 * "výchozího" konfiguračního souboru pro aplikaci. Dle mého názoru je to
		 * přehlednější. Opět, je to nastavení dialogu v aplikaci a ně nějaké
		 * komponenty.
		 */
		
		// Když uživatel pohybuje s posuvníkem, tak zda se i během posouvání mají nastavovat velikosti písma nebo ne (tj. až při konečném nastavení):
		commentsMap.put("FontSizeForm_UpdateDiagramsWhileMovingWithSlider", "Hodnota, která značí, zda se mají nastavovat hodnoty v diagramu tříd a v diagramu instancí i během toho, co uživatel pohybuje s posuvníkem v dialogu pro nastavení velikosti písma pro diagram tříd a instancí. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se budou aktualizovat velikosti písma v diagramu tříd a v diagramu instnací i během toho, co uživatel hýbe s posuvníkem. Hodnota 'false' značí, že se velikosti písma nastaví až po tom, co uživatel posune a pustí posuvník.");
		properties.setProperty("FontSizeForm_UpdateDiagramsWhileMovingWithSlider", String.valueOf(Constants.UPDATE_DIAGRAMS_WHILE_MOVING_WITH_SLIDER));
		
		// Označený / vybraný panel s komponentami pro nastavení písma - který panel je označen v dialogu:
		commentsMap.put("FontSizeForm_SelectedPanel", "Hodnota, která značí, jaký panel s komponentami pro možnosti nastavení velikosti písma v diagramu tříd a v diagramu instancí. Povolené hodnoty: 'One_Font_Size_For_All_Objects', 'Font_Size_For_Selected_Objects' a 'One_Font_Size_For_Each_Object'. Hodnota 'One_Font_Size_For_All_Objects' značí, že bude na výběr jeden slider, který slouží pro nastavení jedné velikosti písma pro všechny objekty v diagramu tříd a v diagramu instancí. Hodnota 'Font_Size_For_Selected_Objects' značí, že se nastaví velikosti písma pouze v označených objektech v diagramu tříd a v diagramu instancí. Hodnota 'One_Font_Size_For_Each_Object' značí, že bude na výběr jeden slider pro každý objekt z diagramu tříd a z diagramu instancí, velikost písma se nastavuje individuálně pro každý objekt z diagramu tříd a z diagramu instnací.");
		properties.setProperty("FontSizeForm_SelectedPanel", String.valueOf(Constants.FONT_SIZE_FORM_TYPE_SELECTED_PANEL.getValue()));
		
		
		
		// Následují hodnoty, které značí, jaký chcb je označen v panelu pro individuální nastavení velikosti písma dle označených chcb s objekty v diagramu tříd a instancí:
		
		// označení JCheckBox pro nastavení velikosti písma ve třídě v diagramu tříd:
		commentsMap.put("FontSizeForm_CD_ClassCell", "Hodnota, která značí, zda bude komponenta JCheckBox pro změnu velikosti písma v buňce reprezentující třídu v diagramu tříd označena nebo ne. Jedná se o komponentu v dialogu pro změnu velikosti písma na panelu pro změnu velikosti písma označených objektů. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že uvedená komponenta bude označena, bude se v buňce reprezentující třídu v diagramu tříd měnit velikost písma. Hodnota 'false' značí, že uvedená komponenta nebude označena, nebude se v buňce reprezentující třídu v diagramu tříd měnit velikost písma.");
		properties.setProperty("FontSizeForm_CD_ClassCell", String.valueOf(Constants.FSF_CD_CLASS_CELL));

		// označení JCheckBox pro nastavení velikosti písma ve komentáři v diagramu tříd:
		commentsMap.put("FontSizeForm_CD_CommentCell", "Hodnota, která značí, zda bude komponenta JCheckBox pro změnu velikosti písma v buňce reprezentující komentář v diagramu tříd označena nebo ne. Jedná se o komponentu v dialogu pro změnu velikosti písma na panelu pro změnu velikosti písma označených objektů. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že uvedená komponenta bude označena, bude se v buňce reprezentující komentář v diagramu tříd měnit velikost písma. Hodnota 'false' značí, že uvedená komponenta nebude označena, nebude se v buňce reprezentující komentář měnit velikost písma.");
		properties.setProperty("FontSizeForm_CD_CommentCell", String.valueOf(Constants.FSF_CD_COMMENT_CELL));
		
		// označení JCheckBox pro nastavení velikosti písma na hraně komentáře v diagramu tříd:
		commentsMap.put("FontSizeForm_CD_CommentEdge", "Hodnota, která značí, zda bude komponenta JCheckBox pro změnu velikosti písma hrany reprezentující spojení třídy s komentářem v diagramu tříd označena nebo ne. Jedná se o komponentu v dialogu pro změnu velikosti písma na panelu pro změnu velikosti písma označených objektů. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že uvedená komponenta bude označena, bude se měnit velikost písma hrany reprezentující spojení třídy s komentářem v diagramu tříd. Hodnota 'false' značí, že uvedená komponenta nebude označena, nebude se měnit velikost písma hrany reprezentující spojení třídy s komentářem v diagramu tříd.");
		properties.setProperty("FontSizeForm_CD_CommentEdge", String.valueOf(Constants.FSF_CD_COMMENT_EDGE));

		// označení JCheckBox pro nastavení velikosti písma na hraně vztahu dědičnost v diagramu tříd:
		commentsMap.put("FontSizeForm_CD_ExtendsEdge", "Hodnota, která značí, zda bude komponenta JCheckBox pro změnu velikosti písma hrany reprezentující vztah typu dědičnost v diagramu tříd označena nebo ne. Jedná se o komponentu v dialogu pro změnu velikosti písma na panelu pro změnu velikosti písma označených objektů. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že uvedená komponenta bude označena, bude se měnit velikost písma hrany reprezentující vztah typu dědičnost v diagramu tříd. Hodnota 'false' značí, že uvedená komponenta nebude označena, nebude se měnit velikost písma hrany reprezentující vztah typu dědičnost v diagramu tříd.");
		properties.setProperty("FontSizeForm_CD_ExtendsEdge", String.valueOf(Constants.FSF_CD_EXTENDS_EDGE));
		
		// označení JCheckBox pro nastavení velikosti písma na hraně vztahu implementace rozhraní v diagramu tříd:
		commentsMap.put("FontSizeForm_CD_ImplementsEdge", "Hodnota, která značí, zda bude komponenta JCheckBox pro změnu velikosti písma hrany reprezentující vztah typu implementace rozhraní v diagramu tříd označena nebo ne. Jedná se o komponentu v dialogu pro změnu velikosti písma na panelu pro změnu velikosti písma označených objektů. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že uvedená komponenta bude označena, bude se měnit velikost písma hrany reprezentující vztah typu implementace rozhraní v diagramu tříd. Hodnota 'false' značí, že uvedená komponenta nebude označena, nebude se měnit velikost písma hrany reprezentující vztah typu implementace rozhraní v diagramu tříd.");
		properties.setProperty("FontSizeForm_CD_ImplementsEdge", String.valueOf(Constants.FSF_CD_IMPLEMENTS_EDGE));
		
		// označení JCheckBox pro nastavení velikosti písma na hraně vztahu asociace (symetrická) v diagramu tříd:
		commentsMap.put("FontSizeForm_CD_AssociationEdge", "Hodnota, která značí, zda bude komponenta JCheckBox pro změnu velikosti písma hrany reprezentující vztah typu asociace (symetrickáú v diagramu tříd označena nebo ne. Jedná se o komponentu v dialogu pro změnu velikosti písma na panelu pro změnu velikosti písma označených objektů. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že uvedená komponenta bude označena, bude se měnit velikost písma hrany reprezentující vztah typu asociace (symetrická) v diagramu tříd. Hodnota 'false' značí, že uvedená komponenta nebude označena, nebude se měnit velikost písma hrany reprezentující vztah typu asociace (symetrická) v diagramu tříd.");
		properties.setProperty("FontSizeForm_CD_AssociationEdge", String.valueOf(Constants.FSF_CD_ASSOCIATION_EDGE));
		
		// označení JCheckBox pro nastavení velikosti písma na hraně vztahu agregace 1 : 1 (asymetrická asociace) v diagramu tříd:
		commentsMap.put("FontSizeForm_CD_Aggregation_1_1_Edge", "Hodnota, která značí, zda bude komponenta JCheckBox pro změnu velikosti písma hrany reprezentující vztah typu agregace 1 : 1 (neboli asymetrická asociace) v diagramu tříd označena nebo ne. Jedná se o komponentu v dialogu pro změnu velikosti písma na panelu pro změnu velikosti písma označených objektů. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že uvedená komponenta bude označena, bude se měnit velikost písma hrany reprezentující vztah typu agregace 1 : 1 (neboli asymetrická asociace) v diagramu tříd. Hodnota 'false' značí, že uvedená komponenta nebude označena, nebude se měnit velikost písma hrany reprezentující vztah typu agregace 1 : 1 (asymetrická asociace) v diagramu tříd.");
		properties.setProperty("FontSizeForm_CD_Aggregation_1_1_Edge", String.valueOf(Constants.FSF_CD_AGGREGATION_1_1_EDGE));
		
		// označení JCheckBox pro nastavení velikosti písma na hraně vztahu agregace 1 : N v diagramu tříd:
		commentsMap.put("FontSizeForm_CD_Aggregation_1_N_Edge", "Hodnota, která značí, zda bude komponenta JCheckBox pro změnu velikosti písma hrany reprezentující vztah typu agregace 1 : N v diagramu tříd označena nebo ne. Jedná se o komponentu v dialogu pro změnu velikosti písma na panelu pro změnu velikosti písma označených objektů. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že uvedená komponenta bude označena, bude se měnit velikost písma hrany reprezentující vztah typu agregace 1 : N v diagramu tříd. Hodnota 'false' značí, že uvedená komponenta nebude označena, nebude se měnit velikost písma hrany reprezentující vztah typu agregace 1 : N v diagramu tříd.");
		properties.setProperty("FontSizeForm_CD_Aggregation_1_N_Edge", String.valueOf(Constants.FSF_CD_AGGREGATION_1_N_EDGE));
		
		// označení JCheckBox pro nastavení velikosti písma v buňce coby instance třídy v diagramu instancí:
		commentsMap.put("FontSizeForm_ID_InstanceCell", "Hodnota, která značí, zda bude komponenta JCheckBox pro změnu velikosti písma buňky reprezentující instanci třídy v diagramu instancí označena nebo ne. Jedná se o komponentu v dialogu pro změnu velikosti písma na panelu pro změnu velikosti písma označených objektů. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že uvedená komponenta bude označena, bude se měnit velikost písma buňky reprezentující instanci třídy v diagramu instancí. Hodnota 'false' značí, že uvedená komponenta nebude označena, nebude se měnit velikost písma buňky reprezentující instanci třídy v diagramu instancí.");
		properties.setProperty("FontSizeForm_ID_InstanceCell", String.valueOf(Constants.FSF_ID_INSTANCE_CELL));
		
		// označení JCheckBox pro nastavení velikosti písma pro atributy v buňce coby instance třídy v diagramu instancí:
		commentsMap.put("FontSizeForm_ID_InstanceCell_Attributes", "Hodnota, která značí, zda bude komponenta JCheckBox pro změnu velikosti písma pro atributy v buňce reprezentující instanci třídy v diagramu instancí označena nebo ne. Jedná se o komponentu v dialogu pro změnu velikosti písma na panelu pro změnu velikosti písma označených objektů. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že uvedená komponenta bude označena, bude se měnit velikost písma pro atributy v buňce reprezentující instanci třídy v diagramu instancí. Hodnota 'false' značí, že uvedená komponenta nebude označena, nebude se měnit velikost písma pro atributy v buňce reprezentující instanci třídy v diagramu instancí.");
		properties.setProperty("FontSizeForm_ID_InstanceCell_Attributes", String.valueOf(Constants.FSF_ID_INSTANCE_CELL_ATTRIBUTES));
		
		// označení JCheckBox pro nastavení velikosti písma pro metody v buňce coby instance třídy v diagramu instancí:
		commentsMap.put("FontSizeForm_ID_InstanceCell_Methods", "Hodnota, která značí, zda bude komponenta JCheckBox pro změnu velikosti písma pro metody v buňce reprezentující instanci třídy v diagramu instancí označena nebo ne. Jedná se o komponentu v dialogu pro změnu velikosti písma na panelu pro změnu velikosti písma označených objektů. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že uvedená komponenta bude označena, bude se měnit velikost písma pro metody v buňce reprezentující instanci třídy v diagramu instancí. Hodnota 'false' značí, že uvedená komponenta nebude označena, nebude se měnit velikost písma pro metody v buňce reprezentující instanci třídy v diagramu instancí.");
		properties.setProperty("FontSizeForm_ID_InstanceCell_Methods", String.valueOf(Constants.FSF_ID_INSTANCE_CELL_METHODS));
		
		// označení JCheckBox pro nastavení velikosti písma na hraně vztahu asociace v diagramu instancí:
		commentsMap.put("FontSizeForm_ID_AssociationEdge", "Hodnota, která značí, zda bude komponenta JCheckBox pro změnu velikosti písma hrany reprezentující vztah typu asociace v diagramu instancí označena nebo ne. Jedná se o komponentu v dialogu pro změnu velikosti písma na panelu pro změnu velikosti písma označených objektů. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že uvedená komponenta bude označena, bude se měnit velikost písma hrany reprezentující vztah typu asociace v diagramu instancí. Hodnota 'false' značí, že uvedená komponenta nebude označena, nebude se měnit velikost písma hrany reprezentující vztah typu asociace v diagramu instancí.");
		properties.setProperty("FontSizeForm_ID_AssociationEdge", String.valueOf(Constants.FSF_ID_ASSOCIATION_EDGE));
		
		// označení JCheckBox pro nastavení velikosti písma na hraně vztahu agregace v diagramu instancí:
		commentsMap.put("FontSizeForm_ID_AggregationEdge", "Hodnota, která značí, zda bude komponenta JCheckBox pro změnu velikosti písma hrany reprezentující vztah typu agregace v diagramu instancí označena nebo ne. Jedná se o komponentu v dialogu pro změnu velikosti písma na panelu pro změnu velikosti písma označených objektů. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že uvedená komponenta bude označena, bude se měnit velikost písma hrany reprezentující vztah typu agregace v diagramu instancí. Hodnota 'false' značí, že uvedená komponenta nebude označena, nebude se měnit velikost písma hrany reprezentující vztah typu agregace v diagramu instancí.");
		properties.setProperty("FontSizeForm_ID_AggregationEdge", String.valueOf(Constants.FSF_ID_AGGREGATION_EDGE));
		
		
		
		
		
		
		

		
		
		
		
		
		/*
		 * Zápis obejktu Properties do souboru na zadané cestě.
		 * 
		 * Note:
		 * Jedná se o mnou definované nastavení, jak budou tyto soubory vypadat, toto
		 * již nechci dát jakkoliv na úpravu uživateli. Takto je to z mého pohledu
		 * nejpřehlednější, tak proto pevně definuje mezery nad komentáři apod.
		 */
		WriteToFile.writeProperties(properties, path);
		
		// Vložím do něj zadané komentáře (s mezerami):
		insertComments(path, COUNT_OF_FREE_LINES_ABOVE_COMMENT);
		
		// Zapíšu do něj původní komentář a pod něj datum vytvoření:
		WriteToFile.insertCreationComments(path,
				"Soubor, který obsahuje výchozí nastavení pro aplikaci (nejedná se o nastavení jednotlivých komponent v aplikaci).",
				DEFAULT_CREATION_DATE_LOCALE);
	}

	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která na uvedenou cestu zapíše výchozí nastavení pro komponentu
	 * editor kódu, resp. pro soubor ClassEditor.properties
	 * 
	 * @param path
	 *            - cesta, kam se má soubor zapsat (včetně názvu)
	 */
	public static void writeCodeEditorProperties(final String path) {
		final SortedProperties codeEditorProp = new SortedProperties(KEYS_ORDER_PROPERTIES);
		
		commentsMap.clear();
		
		// Nastavení výchozích hodnot:
		commentsMap.put("FontStyle", "Hodnota pro index stylu fontu v editoru zdrojového kódu. Dle zadané hodnoty se aplikuje styl fontu. Možné hodnoty (indexy stylů): '0', '1', '2'. Hodnota '0' - Plain, hodnota '1' - Bold a hodnota '2' - Italic.");
		codeEditorProp.setProperty("FontStyle", Integer.toString(Constants.DEFAULT_FONT_FOR_CODE_EDITOR.getStyle()));
		
		commentsMap.put("FontSize", "Hodnota pro velikost písma v editoru zdrojového kódu. Možné hodnoty (možné velikosti písma): <8 ; 100>.");
		codeEditorProp.setProperty("FontSize", Integer.toString(Constants.DEFAULT_FONT_FOR_CODE_EDITOR.getSize()));
		
		commentsMap.put("BackgroundColor", "Barva pozadí pro editor zdrojového kódu v RGB hodnotě (bez složky alpha). Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		codeEditorProp.setProperty("BackgroundColor", Integer.toString(Constants.CODE_EDITOR_BACKGROUND_COLOR.getRGB()));
		
		commentsMap.put("ForegroundColor", "Barva písma pro editor zdrojového kódu v RGB hodnotě (bez složky alpha). Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		codeEditorProp.setProperty("ForegroundColor", Integer.toString(Constants.CODE_EDITOR_FOREGROUND_COLOR.getRGB()));
		
		commentsMap.put("HighlightingCodeByKindOfFile", "Hodnota, která značí, zda se má v editoru zdrojového kódu zvýrazňovat syntaxe dle otevřeného typu (přípony) souboru. Dle některých 'povolených' typů otevřených souborů se nastaví zvýrazňování syntaxe, například pro soubor html, java, Json, properties, xml, css atd. Pro všechny ostatní, které nejsou hlídány nebo pro které není podporováno zvýraznění syntaxe bude zvoleno zvýrazňování dle syntaxe jazyka Java. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se ve zmíněném editoru bude zvýrazňovat syntaxe dle otevřeného souboru. Hodnota 'false' značí, že se nebude zvýrazňovat syntaxe dle otevřeného souboru. Barvy textu a pozadí budou dle nastavených parametrů.");
		codeEditorProp.setProperty("HighlightingCodeByKindOfFile", String.valueOf(Constants.CODE_EDITOR_HIGHLIGHTING_CODE_BY_KIND_OF_FILE));
		
		commentsMap.put("HighlightingCode", "Hodnota, která značí, zda se má v editoru zdrojového kódu zvýrazňovat syntaxe nebo ne. Dle některých typů (přípon) otevřených souborů se nastaví zvýrazňování syntaxe, například pro soubor html, java, Json, properties, xml atd. Pro všechny ostatní, které nejsou hlídány nebo podporovány bude zvoleno zvýrazňování dle syntaxe jazyka Java. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se ve zmíněném editoru bude zvýrazňovat syntaxe. Hodnota 'false' značí, že se nebude zvýrazňovat syntaxe. Barvy textu a pozadí budou dle nastavených parametrů.");
		codeEditorProp.setProperty("HighlightingCode", String.valueOf(Constants.CODE_EDITOR_HIGHLIGHTING_CODE));
		
		commentsMap.put("CompileClassInBackground", "Hodnota, která značí, zda se má otevřená třída v editoru zdrojového kódu (případně všechny třídy v diagramu tříd v otevřeném projektu) zkompilovat a v případě úspěchu doplnit nové hodnoty (proměnné a metody) do okna pro auto doplňování kódu. Pokud je toto povoleno, třída (/ třídy) se zkompilují po stisknutí středníku nebo zavírací závorky (složené ']' nebo špičaté '}') na anglické klávesnici. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se má třída (/ třídy) zkompilovat po stisknutí jedné z uvedených kláves. Hodnota 'false' tuto funkci kompilace po stisknutí jedné z uvedených kláves zakáže.");
		codeEditorProp.setProperty("CompileClassInBackground", String.valueOf(Constants.CODE_EDITOR_COMPILE_CLASS_IN_BACKGROUND));

		commentsMap.put("WrapTextInEditor", "Hodnota, která značí, zda se má zalamovat text v dialogu editor zdrojového kódu. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se bude zalamovat text v editoru. Hodnota 'false' značí, že se nebude zalamovat text v editoru.");
		codeEditorProp.setProperty("WrapTextInEditor", String.valueOf(Constants.CODE_EDITOR_WRAP_TEXT_IN_EDITOR));

		commentsMap.put("ShowWhiteSpaceInEditor", "Hodnota, která značí, zda se mají v dialogu editor zdrojového kódu zobrazovat bílé znaky. Povolné hodnoty: 'true', 'false'. Hodnota 'true' značí, že se v editoru budou zobrazovat bílé znaky. Hodnota 'false' značí, že se v editoru nebudou zobrazovat bílé znaky.");
		codeEditorProp.setProperty("ShowWhiteSpaceInEditor", String.valueOf(Constants.CODE_EDITOR_SHOW_WHITE_SPACE_IN_EDITOR));

		commentsMap.put("ClearWhiteSpaceLineInEditor", "Hodnota, která značí, zda se mají v dialogu editor zdrojového kódu odebírat bílé znaky po vložení nového řádku. V takovém případě budou bílé znaky odebrány z řádku, kde byl kurzor při stisknutí klávesy 'Enter' pro vložení nového řádku. Povolné hodnoty: 'true', 'false'. Hodnota 'true' značí, že se v po stisknutí klávesy 'Enter' odeberou bílé znaky z řádku, kde se nachází kurzor. Hodnota 'false' značí, že se v editoru nebudou odebírat bílé znaky.");
        codeEditorProp.setProperty("ClearWhiteSpaceLineInEditor", String.valueOf(Constants.CODE_EDITOR_CLEAR_WHITE_SPACE_LINE_IN_EDITOR));


        // Přidání vlastností pro formátování zdrojového kódu:
        addPropertiesForFormatSourceCode(codeEditorProp);
		
		
		
		
		/*
		 * Zápis obejktu Properties do souboru na zadané cestě.
		 * 
		 * Note:
		 * Jedná se o mnou definované nastavení, jak budou tyto soubory vypadat, toto
		 * již nechci dát jakkoliv na úpravu uživateli. Takto je to z mého pohledu
		 * nejpřehlednější, tak proto pevně definuje mezery nad komentáři apod.
		 */
		WriteToFile.writeProperties(codeEditorProp, path);

		// Vložím do něj zadané komentáře (s mezerami):
		insertComments(path, COUNT_OF_FREE_LINES_ABOVE_COMMENT);

		// Zapíšu do něj původní komentář a pod něj datum vytvoření:
		WriteToFile.insertCreationComments(path,
				"Soubor, který obsahuje výchozí nastavení pro komponentu editor zdrojového kódu.",
				DEFAULT_CREATION_DATE_LOCALE);
	}

	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která na uvedenou cestu zapíše soubor pro nastavení komponenty
	 * editoru kódu v dialogu průzkumník projektů neboli jsou to hodnoty pro soubor
	 * CodeEditorInternalFrame.properties
	 * 
	 * @param path
	 *            - cesta, kam se má výše uvedený soubor zapsat (včetně názvu)
	 */
	public static void writeCodeEditorPropertiesForInternalFrames(final String path) {
		final SortedProperties codeEditorProp = new SortedProperties(KEYS_ORDER_PROPERTIES);
		
		commentsMap.clear();
		
		// Nastavení výchozích hodnot:
		
		// Výchozí hodnoty budou stejne jako pro CodeEditor 
		commentsMap.put("FontStyle", "Hodnota pro index stylu fontu v editoru zdrojového kódu v interních oknech dialogu průzkumník projektů. Dle zadané hodnoty se aplikuje styl fontu. Možné hodnoty (indexy stylů): '0', '1', '2'. Hodnota '0' - Plain, hodnota '1' - Bold a hodnota '2' - Italic.");
		codeEditorProp.setProperty("FontStyle", Integer.toString(Constants.DEFAULT_FONT_FOR_CODE_EDITOR.getStyle()));
		
		commentsMap.put("FontSize", "Hodnota pro velikost písma v editoru zdrojového kódu v interních oknech dialogu průzkumník projektů. Možné hodnoty (možné velikosti písma): <8 ; 100>.");
		codeEditorProp.setProperty("FontSize", Integer.toString(Constants.DEFAULT_FONT_FOR_CODE_EDITOR.getSize()));
		
		commentsMap.put("BackgroundColor", "Barva pozadí pro editor zdrojového kódu v interních oknech dialogu průzkumník projektů v RGB hodnotě (bez složky alpha). Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		codeEditorProp.setProperty("BackgroundColor", Integer.toString(Constants.CODE_EDITOR_BACKGROUND_COLOR.getRGB()));
		
		commentsMap.put("ForegroundColor", "Barva písma pro editor zdrojového kódu v interních oknech dialogu průzkumník projektů v RGB hodnotě (bez složky alpha). Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		codeEditorProp.setProperty("ForegroundColor", Integer.toString(Constants.CODE_EDITOR_FOREGROUND_COLOR.getRGB()));
		
		commentsMap.put("HighlightingCodeByKindOfFile", "Hodnota, která značí, zda se má v editoru zdrojového kódu v interních oknech v dialogu průzkumník projektů zvýrazňovat syntaxe dle otevřeného typu (přípony) souboru. Dle některých 'povolených' typů otevřených souborů se nastaví zvýrazňování syntaxe, například pro soubor html, java, Json, properties, xml, css atd. Pro všechny ostatní, které nejsou hlídány nebo pro které není podporováno zvýraznění syntaxe bude zvoleno zvýrazňování dle syntaxe jazyka Java. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se ve zmíněném editoru bude zvýrazňovat syntaxe dle otevřeného souboru. Hodnota 'false' značí, že se nebude zvýrazňovat syntaxe dle otevřeného souboru. Barvy textu a pozadí budou dle nastavených parametrů.");
		codeEditorProp.setProperty("HighlightingCodeByKindOfFile", String.valueOf(Constants.CODE_EDITOR_HIGHLIGHTING_CODE_BY_KIND_OF_FILE));
		
		commentsMap.put("HighlightingCode", "Hodnota, která značí, zda se má v editoru zdrojového kódu v interních oknech v dialogu průzkumník projektů zvýrazňovat syntaxe. Zvýraznění syntaxe záleží na konkrétním typu (příponě) otevřeného souboru, podporovány jsou například java, html, properties, json, xml, css atd. U těch, kterých nejsou podporovány bude využito zvýraznění dle syntaxe Jazyka Java. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se ve zmíněném editoru bude zvýrazňovat syntaxe jazyka Java. Hodnota 'false' značí, že se nebude zvýrazňovat syntaxe jazyka Java. Barvy textu a pozadí budou dle nastavených parametrů.");
		codeEditorProp.setProperty("HighlightingCode", String.valueOf(Constants.CODE_EDITOR_HIGHLIGHTING_CODE));
		
		commentsMap.put("CompileClassInBackground", "Hodnota, která značí, zda se má otevřená třída v editoru zdrojového kódu v interních oknech v dialogu průzkumník projektů (případně všechny třídy v diagramu tříd v otevřeném projektu) zkompilovat a v případě úspěchu doplnit nové hodnoty (proměnné a metody) do okna pro auto doplňování kódu. Pokud je toto povoleno, třída (/ třídy) se zkompilují po stisknutí středníku nebo zavírací závorky (složené ']' nebo špičaté '}') na anglické klávesnici. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se má třída (/ třídy) zkompilovat po stisknutí jedné z uvedených kláves. Hodnota 'false' tuto funkci kompilace po stisknutí jedné z uvedených kláves zakáže.");
		codeEditorProp.setProperty("CompileClassInBackground", String.valueOf(Constants.CODE_EDITOR_COMPILE_CLASS_IN_BACKGROUND));

		commentsMap.put("WrapTextInAllFrames", "Hodnota, která značí, zda se má zalamovat text ve všech interních oknech (/ otevřených souborech) v dialogu průzkumník projektů. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se bude zalamovat text ve všech interních oknech. Hodnota 'false' značí, že se nebude zalamovat text v žádném interním okně. Pokud se tak nenastaví individuálně pro každé interní okno.");
		codeEditorProp.setProperty("WrapTextInAllFrames", String.valueOf(Constants.CODE_EDITOR_WRAP_TEXT_IN_ALL_FRAMES));

        commentsMap.put("ShowWhiteSpaceInAllFrames", "Hodnota, která značí, zda se mají zobrazovat bílé znaky ve všech interních oknech (/ otevřených souborech) v dialogu průzkumník projektů. Povolené hodnoty: 'true', 'false'. Hodntoa 'true' značí, že se budou zobrazovat bílé znaky ve všech interních oknech. Hodnota 'false' značí, že se nebudou zobrazovat bílé znaky v žádném interním okně. Pokud se tak nenastaví individuálně pro každé interní okno.");
        codeEditorProp.setProperty("ShowWhiteSpaceInAllFrames", String.valueOf(Constants.CODE_EDITOR_SHOW_WHITE_SPACE_IN_ALL_FRAMES));

        commentsMap.put("ClearWhiteSpaceLineInAllFrames", "Hodnota, která značí, zda se mají po stisknutí klávesy Enter odebrat bílé znaky z prázdného řádku, na kterém se nachází kurzor. Toto nastavení platí pro veškerá interní okna (/ otevřené soubory) v dialogu průzkumník projektů. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že v případě prázdného řádku, kde se nachází kurzor se po stisknutí uvedené klávesy odeberou z řádku veškeré bílé znaky. Hodnota 'false' značí, že se nebudou odebírat žádné bílé znaky. Pokud se tak nenastaví individuálně pro každé interní okno.");
        codeEditorProp.setProperty("ClearWhiteSpaceLineInAllFrames", String.valueOf(Constants.CODE_EDITOR_CLEAR_WHITE_SPACE_LINE_IN_ALL_FRAMES));



        // Přidání vlastností pro formátování zdrojového kódu:
        addPropertiesForFormatSourceCode(codeEditorProp);

		
		
		/*
		 * Zápis obejktu Properties do souboru na zadané cestě.
		 * 
		 * Note:
		 * Jedná se o mnou definované nastavení, jak budou tyto soubory vypadat, toto
		 * již nechci dát jakkoliv na úpravu uživateli. Takto je to z mého pohledu
		 * nejpřehlednější, tak proto pevně definuje mezery nad komentáři apod.
		 */
		WriteToFile.writeProperties(codeEditorProp, path);
		
		// Vložím do něj zadané komentáře (s mezerami):
		insertComments(path, COUNT_OF_FREE_LINES_ABOVE_COMMENT);

		// Zapíšu do něj původní komentář a pod něj datum vytvoření:
		WriteToFile.insertCreationComments(path,
				"Soubor, který obsahuje výchozí nastavení pro komponentu editor zdrojového kódu, ale tento editor se nachází v každém interním okně v dialogu průzkumník projektů.",
				DEFAULT_CREATION_DATE_LOCALE);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která na uvedenou cestu zapíše výchozí nastavení pro komponentu
	 * editor výstupů neboli hodnoty, které patří do souboru (z hlediska aplikace)
	 * OutputEditor.properties.
	 * 
	 * @param path
	 *            - cesta, kam se má soubor zapsat (včetně názvu)
	 */
	public static void writeOutputEditorProperties(final String path) {
		final SortedProperties outputEditorProp = new SortedProperties(KEYS_ORDER_PROPERTIES);
		
		commentsMap.clear();
		
				
		// Výchozí hodnoty:
		commentsMap.put("FontStyle", "Hodnota pro index stylu fontu v editoru výstupů. Dle zadané hodnoty se aplikuje styl fontu. Možné hodnoty (indexy stylů): '0', '1', '2'. Hodnota '0' - Plain, hodnota '1' - Bold a hodnota '2' - Italic.");
		outputEditorProp.setProperty("FontStyle", Integer.toString(Constants.DEFAULT_FONT_FOR_OUTPUT_EDITOR.getStyle()));
		
		commentsMap.put("FontSize", "Hodnota pro velikost písma v editoru výstupů. Možné hodnoty (možné velikosti písma): <8 ; 100>.");
		outputEditorProp.setProperty("FontSize", Integer.toString(Constants.DEFAULT_FONT_FOR_OUTPUT_EDITOR.getSize()));
		
		commentsMap.put("BackgroundColor", "Barva pozadí pro editor výstupů v RGB hodnotě (bez složky alpha). Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		outputEditorProp.setProperty("BackgroundColor", Integer.toString(Constants.DEFAULT_BACKGROUND_COLOR_FOR_OUTPUT_EDITOR.getRGB()));
		
		commentsMap.put("ForegroundColor", "Barva písma pro editor výstupů v RGB hodnotě (bez složky alpha). Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		outputEditorProp.setProperty("ForegroundColor", Integer.toString(Constants.DEFAULT_TEXT_COLOR_FOR_OUTPUT_EDITOR.getRGB()));				

		commentsMap.put("ShowReferenceVariables", "Hodnota, která značí, zda se do editoru výstupů mají vypisovat referenční proměnné (reference). Tzn. Pokud bude do editoru výstupů vypsána instance nějaké třídy z diagramu tříd, která se aktuálně nachází v diagramu instancí, tak se za tu instnaci do editoru výstupů vypíše i reference na příslušnou instanci, kterou uživatel zadal při jejím vytváření. Může se jednat o pole, list nebo 'běžnou' instanci. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se za instanci budou vypisovat reference. Hodnota 'false' značí, že se nebudou za instanci do editoru výstupů vypisovat reference.");
		outputEditorProp.setProperty("ShowReferenceVariables", String.valueOf(Constants.OUTPUT_EDITOR_SHOW_REFERENCE_VARIABLES));
		
		
		
		/*
		 * Zápis objektu Properties do souboru na zadané cestě.
		 * 
		 * Note:
		 * Jedná se o mnou definované nastavení, jak budou tyto soubory vypadat, toto
		 * již nechci dát jakkoliv na úpravu uživateli. Takto je to z mého pohledu
		 * nejpřehlednější, tak proto pevně definuje mezery nad komentáři apod.
		 */
		WriteToFile.writeProperties(outputEditorProp, path);
		
		// Vložím do něj zadané komentáře (s mezerami):
		insertComments(path, COUNT_OF_FREE_LINES_ABOVE_COMMENT);
		
		// Zapíšu do něj původní komentář a pod něj datum vytvoření:
		WriteToFile.insertCreationComments(path,
				"Soubor, který obsahuje výchozí nastavení pro komponentu editor výstupů, který se nachází v pravé spodní části hlavního okna aplikace.",
				DEFAULT_CREATION_DATE_LOCALE);
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která nauvedenou cestu zapíše soubor s výchozím nastavením pro
	 * komponentu výstupní terminál (OutputFrame) - jak jsem ho nazval, je to okno,
	 * kde se vypisují výpisy do konzole pomocí metod v System.out nebo System.err
	 * apod.
	 * 
	 * Note:
	 * jsou zde stejné hodnoty jako pro Editor výstupů - OutputEditor
	 * 
	 * @param path
	 *            - cesta, kam se má zapsat soubor (včetně názvu)
	 */
	public static void writeOutputFrameProperties(final String path) {
		final SortedProperties outputFrameProp = new SortedProperties(KEYS_ORDER_PROPERTIES);
		
		commentsMap.clear();
		
				
		
		// Výchozí hodnoty:

		// Hodnoty pro textové pole pro výpisy v System.out:
		commentsMap.put("SO_FontStyle", "Hodnota pro index stylu fontu v textovém poli pro System.out výpisy ve výstupním terminálu. Dle zadané hodnoty se aplikuje styl fontu. Možné hodnoty (indexy stylů): '0', '1', '2'. Hodnota '0' - Plain, hodnota '1' - Bold a hodnota '2' - Italic.");
		outputFrameProp.setProperty("SO_FontStyle", Integer.toString(Constants.DEFAULT_FONT_FOR_SO_OUTPUT_FRAME.getStyle()));
		
		commentsMap.put("SO_FontSize", "Hodnota pro velikost písma v textovém poli pro System.out výpisy ve výstupním terminálu. Možné hodnoty (možné velikosti písma): <8 ; 100>.");
		outputFrameProp.setProperty("SO_FontSize", Integer.toString(Constants.DEFAULT_FONT_FOR_SO_OUTPUT_FRAME.getSize()));
		
		commentsMap.put("SO_BackgroundColor", "Barva pozadí textového pole pro System.out výpisy ve výstupním terminálu v RGB hodnotě (bez složky alpha). Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		outputFrameProp.setProperty("SO_BackgroundColor", Integer.toString(Constants.DEFAULT_BACKGROUND_COLOR_FOR_SO_OUTPUT_FRAME.getRGB()));
		
		commentsMap.put("SO_ForegroundColor", "Barva písma v textovém poli pro System.out výpisy ve výstupním terminálu v RGB hodnotě (bez složky alpha). Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		outputFrameProp.setProperty("SO_ForegroundColor", Integer.toString(Constants.DEFAULT_TEXT_COLOR_FOR_SO_OUTPUT_FRAME.getRGB()));



		// Hodnoty pro textové pole pro výpisy v System.err:
		commentsMap.put("SE_FontStyle", "Hodnota pro index stylu fontu v textovém poli pro System.err výpisy ve výstupním terminálu. Dle zadané hodnoty se aplikuje styl fontu. Možné hodnoty (indexy stylů): '0', '1', '2'. Hodnota '0' - Plain, hodnota '1' - Bold a hodnota '2' - Italic.");
		outputFrameProp.setProperty("SE_FontStyle", Integer.toString(Constants.DEFAULT_FONT_FOR_SE_OUTPUT_FRAME.getStyle()));

		commentsMap.put("SE_FontSize", "Hodnota pro velikost písma v textovém poli pro System.err výpisy ve výstupním terminálu. Možné hodnoty (možné velikosti písma): <8 ; 100>.");
		outputFrameProp.setProperty("SE_FontSize", Integer.toString(Constants.DEFAULT_FONT_FOR_SE_OUTPUT_FRAME.getSize()));

		commentsMap.put("SE_BackgroundColor", "Barva pozadí textového pole pro System.err výpisy ve výstupním terminálu v RGB hodnotě (bez složky alpha). Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		outputFrameProp.setProperty("SE_BackgroundColor", Integer.toString(Constants.DEFAULT_BACKGROUND_COLOR_FOR_SE_OUTPUT_FRAME.getRGB()));

		commentsMap.put("SE_ForegroundColor", "Barva písma v textovém poli pro System.err výpisy ve výstupním terminálu v RGB hodnotě (bez složky alpha). Další možností by bylo mít zde zvlášť každou ze složek RGB(A), bylo by to přehlednější, ale větší množství hodnot. Povolené hodnoty (není započítána složka alpha): <-1 ; -16777216> kde -1 je bílá barva, druhý konec je černá.");
		outputFrameProp.setProperty("SE_ForegroundColor", Integer.toString(Constants.DEFAULT_TEXT_COLOR_FOR_SE_OUTPUT_FRAME.getRGB()));



		// Hodnoty pro označené JCheckBoxy pro mazání textů v polí pro System.out výpisy
		commentsMap.put("ClearTextInSoBeforeCallMethod", "Hodnota, která značí, zda se z textového pole pro výpisy z metod v System.out mají před každým výpisem z uživatelem zavolané metody vymazat veškeré texty. Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se před výpisem do konzole ze zavolané metody vymaže text z textového pole pro tyto výpisy. Hodnota 'false' značí, že se ponechají veškeré výstupy v textovém poli. Resp. nedojde  žádnému smazání textů.");
		outputFrameProp.setProperty("ClearTextInSoBeforeCallMethod", String.valueOf(Constants.CLEAR_TEXT_FOR_SO_BEFORE_CALL_METHOD_IN_OUTPUT_FRAME));

        commentsMap.put("ClearTextInSoBeforeCallConstructor", "Hodnota, která značí, zda se z textového pole pro výpisy z metod v System.out mají před každým výpisem z uživatelem zavolaného konstrktoru vymazat veškeré texty. Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se před výpisem do konzole ze zavolaného konstruktoru vymaže text z textového pole pro tyto výpisy. Hodnota 'false' značí, že se ponechají veškeré výstupy v textovém poli. Resp. nedojde  žádnému smazání textů.");
        outputFrameProp.setProperty("ClearTextInSoBeforeCallConstructor", String.valueOf(Constants.CLEAR_TEXT_FOR_SO_BEFORE_CALL_CONSTRUCTOR_IN_OUTPUT_FRAME));



		// Hodnoty pro označené JCheckBoxy pro zaznamenání hlavičky zavolané metody nebo konstruktoru před výpisem z System.out:
		commentsMap.put("RecordCallMethodsInSo", "Hodnota, která značí, zda se před výpis do konzole pomocí metod v System.out v uživatelem zavolané metodě má přidat název metody, případně hodnoty parametrů. Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se před každý výpis do konzole ze zavolané metody přidá název metody a hodnoty parametrů (jsou li). Hodnota 'false' značí, že se nebude vypisovat název metody s parametry. Bude vypsán pouze text v System.out.");
		outputFrameProp.setProperty("RecordCallMethodsInSo", String.valueOf(Constants.RECORD_CALL_METHODS_IN_SO_IN_OUTPUT_FRAME));

		commentsMap.put("RecordCallConstructorsInSo", "Hodnota, která značí, zda se před výpis do konzole pomocí metod v System.out v uživatelem zavolaném konstruktoru má přidat text konstruktoru, případně hodnoty parametrů. Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se před každý výpis do konzole ze zavolaného konstruktoru přidá text konstruktoru a hodnoty parametrů (jsou li). Hodnota 'false' značí, že se nebude vypisovat text konstruktoru s parametry. Bude vypsán pouze text v System.out.");
		outputFrameProp.setProperty("RecordCallConstructorsInSo", String.valueOf(Constants.RECORD_CALL_CONSTRUCTORS_IN_SO_IN_OUTPUT_FRAME));




		
		
		/*
		 * Zápis objektu Properties do souboru na zadané cestě.
		 * 
		 * Note:
		 * Jedná se o mnou definované nastavení, jak budou tyto soubory vypadat, toto
		 * již nechci dát jakkoliv na úpravu uživateli. Takto je to z mého pohledu
		 * nejpřehlednější, tak proto pevně definuje mezery nad komentáři apod.
		 */
		WriteToFile.writeProperties(outputFrameProp, path);
		
		// Vložím do něj zadané komentáře (s mezerami):
		insertComments(path, COUNT_OF_FREE_LINES_ABOVE_COMMENT);

		// Zapíšu do něj původní komentář a pod něj datum vytvoření:
		WriteToFile.insertCreationComments(path,
				"Soubor, který obsahuje výchozí nastavení pro komponentu výstupní terminál což je okno, do kterého se vypisují veškeré výstupy aplikace, například pomocí metody System.out.print, potenciální výjimky apod.",
				DEFAULT_CREATION_DATE_LOCALE);
	}


    /**
     * Přidání vlastností pro formátování zdrojového kódu do mapy.
     */
    private static void addPropertiesForFormatSourceCode(final SortedProperties properties) {
        // Zda se mají mazat prázdné řádky v blocích kódu, například v metodách apod. (ne mimo ně):
        commentsMap.put("DeleteEmptyLines", "Hodnota, která značí, zda se mají odebírat prázdné řádky v blocích kódu. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se budou odebírat prázdné řádky v blocích kódu, například v metodách, konstruktorech apod. Hodnota 'false' značí, že se nebudou odebírat prázdné řádky.");
        properties.setProperty("DeleteEmptyLines", String.valueOf(Constants.SCF_DELETE_EMPTY_LINES));


        // Zda se mají převádět tabulátory na mezery:
        commentsMap.put("TabSpaceConversion", "Hodnota, která značí, zda se mají bílé znaky tabulátoru převést na mezery. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se budou tabulátory nahrazovat mezerami. Hodnota 'false' značí, že bíle znaky tabulátoru nebudou ničím nahrazeny.");
        properties.setProperty("TabSpaceConversion", String.valueOf(Constants.SCF_TAB_SPACE_CONVERSION));


        // Zda se má zalamovat syntaxe "else if ()". Bude zalomena část za "else":
        commentsMap.put("BreakElseIf", "Hodnota, která značí, zda se má zalamovat kód 'else if ()...'. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se v uvedené syntaxi zalomí část za 'else' na nový řádek. Hodnota 'false' značí, že se uvedená syntaxe nebude zalamovat.");
        properties.setProperty("BreakElseIf", String.valueOf(Constants.SCF_BREAK_ELSE_IF));


        /*
         * V případě, že je na jednom řádku více příkazů, například více zavolání metody apod. (testMethod();
         * anotherMethod();), budou příkazy zalomeny každý na jeden řádek:
         */
        commentsMap.put("SingleStatement", "Hodnota, která značí, zda se mají zalamovat příkazy na jednotlivé řádky. Tedy na jednom řádku může být pouze jeden příkaz. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že na jednom řádku bude jeden příkaz. Pokud by na jednom řádku byly například dvě volání metody, každé volání se zalomí na nový řádek. Hodnota 'false' značí, že na jednom řádku bude moci být více příkazů.");
        properties.setProperty("SingleStatement", String.valueOf(Constants.SCF_SINGLE_STATEMENT));


        /*
         * Zalomení jednořádkového bloku kódu, který se nachází na jednom řádku na více řádků. Například metoda: public Object test() {return null;} bude v případě, že se tato vlastnost nastaví na true zalomena na tři řádky (hlavička, tělo, zavírací závorka).
         */
        commentsMap.put("BreakOneLineBlock", "Hodnota, která značí, zda se má zalomit jednořádkový blok kódu na více řádků. Například metoda, která vrací null hodnotu může být uvedena na jednom řádku nebo zalomena na tři řádky. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se blok kódu, který lze rozdělit na více řádků zalomí na více řádků. Hodnota 'false' značí, že bloky kódu zůstanou na jednom řádku, jsou li tak napsané.");
        properties.setProperty("BreakOneLineBlock", String.valueOf(Constants.SCF_BREAK_ONE_LINE_BLOCK));


        // Zda se má zalomit kód za zavírací závorkou:
        commentsMap.put("BreakClosingHeaderBrackets", "Hodnota, která značí, zda se má zalomit kód za zavírací závorkou. Například v případě bloku 'else' nebo 'catch'. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se kód za zavírací závorkou zalomí na nový řádek. Hodnota 'false' značí, že na řádku za zavírací závorkou bude mocí být další kód.");
        properties.setProperty("BreakClosingHeaderBrackets", String.valueOf(Constants.SCF_BREAK_CLOSING_HEADER_BRACKETS));


        /*
         * Oddělení nesouvisejících bloků kódu prázdnými řádky. Například za zavírací závorkou podmíky je další blok
         * kódu, který s podmínkou nesouvicí, proto se tyto bloky oddělí prázdným řádkem apod.
         */
        commentsMap.put("BreakBlocks", "Hodnota, která značí, zda se mají oddělit nesouvisející bloky kódu prázdnými řádky. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že se nesouvisející bloky kódu oddělí prázdným řádkem. Například za zavírací závorkou podmínky ('if') bude prázdný řádek, za kterým bude následovat další blok kódu. Hodnota 'false' značí, že se nebudou oddělovat nesouvicesjící bloky kódu.");
        properties.setProperty("BreakBlocks", String.valueOf(Constants.SCF_BREAK_BLOCKS));


        // Oddělení operátorů od čísel či jiných operandů. Tzn. operátory budou obaleny mezerami.
        commentsMap.put("OperatorPadding", "Hodnota, která značí, zda se mají operátory obalit mezerami. Tedy operátory budou od operandů odděleny mezerami. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že operátory budou od operandů odděleny mezerami. Hodnota 'false' značí, že se k operátorům nebudou přidávat či od nich odebírat žádné znaky.");
        properties.setProperty("OperatorPadding", String.valueOf(Constants.SCF_OPERATOR_PADDING_MODE));
    }
	
	
	
	
	
	
	
	/**
	 * Metoda, která si načte soubor jednorozměrné pole s modelem aktuálně
	 * dostupných Locales v JVM. Toto se vezme z LanguagePanelu v balíčku
	 * applicationPanels. Tento model je připraven pro komponentu JComboBox, ale zde
	 * je možné jej využít také.
	 * 
	 * Získané pole projdu a zísám si tzv. language tag každého Locale, dle kterého
	 * se pak bude i načítat a už v příslušné metodě pro získávání Locale mám
	 * vyřešené, že všechny načtené Locale budou již existovat a jsou v pořádku.
	 * 
	 * Toto pole pak projdu a převedu jej na Language tag a vložím jej do
	 * jednoduchých uvozovek, toto pole pak vrátím a vloží se do souboru
	 * Default.properties jako možné hodnoty pro příslušnou vlastnost.
	 * 
	 * @return jednorozměrné pole obsahující language tags dostupných pro aktuální
	 *         JVM v jednoduchých uvozovkách.
	 */
	public static String[] getLocalesModel() {
		/*
		 * Získám si model dostupných globálních umístění (Locales) a převedu jej na
		 * text, akorát s jednoduchými uvozovkami kolem každého symbolu pro Locale.
		 * 
		 * Vím, že jsem napsal (v komentáři pro Prperties Locale v Defaut.properties)
		 * že, by měly být dostupné pro skoro každý JVM, takže by ty hodnoty měly být
		 * někde staticky dané, ale když si to uživatel někde nechá vytvořit, například
		 * si vytvoří nový workspace, tak se mu tam vloži aktuální hodnoty dostupné pro
		 * daný JVM, nakterém právě poběží aplikace, tak by měl mít vždy ty aktuální -
		 * dostupné hodnoty. Mohl bych tam tedy vložit tento komentář, ale zase nevím
		 * jistě, zda se tam opravdu znovu vytvoří nebo odněkud nakopírují - kdyby si
		 * ten soubor nebo workspace apod. odněkud nakopíroval, z jiného PC apod. a mohl
		 * by tam mít jiné hodnoty.
		 */

		/*
		 * List, do kterého vložím značky pro objekt Locale v uvozovkách, aby jej bylo
		 * možné vložit jako text do souboru.
		 */
		final List<String> localesList = new ArrayList<>();

		Arrays.stream(LanguagePanel.getLocalesModel()).forEach(l -> localesList.add("'" + l.getTagToFile() + "'"));

		return localesList.toArray(new String[] {});
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která převede jednorozměrné pole typu float (array) do
	 * jednorozměrného pole typu int.
	 * 
	 * @param array
	 *            - jednorozměrné pole typu float, které semá převést do
	 *            jednorozměrného pole typu int.
	 * 
	 * @return jednorozměrné pole typu int, které obsahuje int - tové hodnoty z
	 *         array typu float.
	 */
	public static int[] fromFloatToIntArray(final float[] array) {
		final int[] intArray = new int[array.length];

		for (int i = 0; i < array.length; i++)
			intArray[i] = (int) array[i];

		return intArray;
	}
	
	
	
	
	
	
	
	
	
	
		 
		 
		 
	/**
	 * Metoda, která vloží komentáře, které se nachází v comments_Map do souboru
	 * .properties, který je na cestě path.
	 * 
	 * @param path
	 *            - cesta k souboru .properties, kam se mají zapsat komentáře v
	 *            commentsMap.
	 * 
	 * @param countOfFreeLinesBeforeComment
	 *            - počet volných řádků před komentářem.
	 */
	public static void insertComments(final String path, final int countOfFreeLinesBeforeComment) {
		if (commentsMap.isEmpty())
			return;

		final PropertiesConfiguration config = new PropertiesConfiguration();
		final PropertiesConfigurationLayout layout = new PropertiesConfigurationLayout();

		try (final Reader reader = new BufferedReader(new InputStreamReader(
				new FileInputStream(path), StandardCharsets.UTF_8))) {

			layout.load(config, reader);

			commentsMap.forEach((key, value) -> {
				layout.setBlancLinesBefore(key, countOfFreeLinesBeforeComment);
				layout.setComment(key, value);
			});

			final Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path),
					StandardCharsets.UTF_8));

			layout.save(config, writer);

			writer.flush();
			writer.close();

		} catch (final FileNotFoundException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informaci o chybě:
				logger.log(Level.INFO, "Zachycena výjimka při čtení souboru na cestě: " + path
						+ ". jedná se o metodu, která slouží pro zápis komentářů do souboru .properties. Došlo k chybě" +
						" při jeho načítání do objektu Properties. Třída ConfigurationFiles v balíčku "
						+ "file, metoda insertComments. Tato chyba může nastat například v případě, že soubor na " +
						"uvedené cestě nenxistuje, je to adresář nebo z nějakých dalších důvodů.");

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();

		} catch (final ConfigurationException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informaci o chybě:
				logger.log(Level.INFO, "Zachycena výjimka při pokusu o zápis komentářů do souboru: " + path
						+ ". Výjimka může nastat v případě volání metod load nebo save nad objektem " +
						"PropertiesConfigurationLayout, do které se zapisují "
						+ "zmíněné komentáře. Metoda insertComments ve třídě ConfigurationFiles v balíčku file.");

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();

		} catch (final IOException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informaci o chybě:
				logger.log(Level.INFO, "Zachycena výjimka při pokusu o zápis do souboru: " + path
						+ ". Jedná se o metodu pro zápis komentářů do souboru .properties. Třída ConfigurationFiles v " +
						"balíčku file, metoda insertComments. Tato výjimka "
						+ "může nastat například v případě, že soubor na zadané cestě neexistuje, nebo je to adresář " +
						"apod.");

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();
		}
	}
}
