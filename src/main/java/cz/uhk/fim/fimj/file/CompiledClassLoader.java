package cz.uhk.fim.fimj.file;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;

/**
 * Tato třída slouží pro vytvoření a nastavení ClassLoaderu, který slouží pro načítání přeložených, resp. zkompilovaných
 * tříd, tj. třída s příponou .class.
 * <p>
 * <p>
 * Instance této třídy bude v aplikaci pouze jedna, Classloader se vytvořit definuje po otevření projektu, nebo
 * vytvoření nového projektu - v takovém případě se v tomot ClassLoaderu defunje cesta k adresáři bin, v adresáři
 * otevřeného projektu a z toho se jiz berou cesty k jednotlivým třídám, které je třeba načíst.
 * <p>
 * <p>
 * Dále se tento classLoader nastaví na null hodnotu, pokud se projekt zavře - toto už by nemuselo být akutně, ale mohly
 * (němělo by) se stát, že uživatel pmyslě změní nějaká data v konfiguračních souborech a napřílad při otevření se
 * nezmění cesta k otevřenému projektu, apod - ale to by nemělo pouze prevece pro podobné případy.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class CompiledClassLoader {

    private CompiledClassLoader() {
    }

    /**
	 * ClassLoader, který budou sloužit pro načítání přeložených, resp.
	 * zkompilovaných tříd - soubory .class a načítat je do App pro další manipulaci
	 */
	private static ClassLoader cl;	


	
	
	
	/**
	 * Soubor pro načítání textů ve zvolném - správném jazyce do chybových hlášek:
	 */
	private static Properties languageProperties;
	
	
	
	
	private static String pathToBinDir;
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která nastavím "můj" ClassLoader na null hodnotu, tj. "zruší ho" pro
	 * možnost dalšího načítání přeložených - zkompilovaných tříd - souborů s
	 * příponou .class.
	 * 
	 * Tuto část bych provádět nemusel, ale je to pouze jako převence před chybami,
	 * kdyby například uživatel změnil nějaká data ohledně otevřeného projěktu, a
	 * pří otevření nového projektu v aplikaci by se nepřepsala cesta, resp.
	 * nevytvořil by se nový classLoader k novému adresáři bin v nově otevřeném
	 * projektu, apod.
	 */
	public static void setClassLoaderToNull() {
		cl = null;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Tato metoda slouží pro vytvoření instance mého ClassLoaderu, pomocí které lze
	 * načítat přeložené třídy - zkompilované třídy, ke kterým se zadá název z
	 * "hlavního" balíčku a název třídy, která se má načíst.
	 * 
	 * Jak bylo zmíněné, metoda vytvoří instance ClassLoaderu, který by měla vést k
	 * adresáři bin v aktuálně otevřeném projektu.
	 * 
	 * @param pathToBinDir
	 *            - cesta k adresáři bin v otevřeném projěktu aplikace, kde se
	 *            nachází (měly by) přeložené soubory, resp. zkompilované třídy z
	 *            diagramu tříd
	 * 
	 * @param outputEditor
	 *            - editoru výstupů, do kterého vypíšu "oznámení" uživateli v
	 *            případě, že se nepodařilo vytvořit ClassLoader, takže nelze načíst
	 *            přeložený - zkompilovanou třídu, resp. soubor .class z adresáře
	 *            bin
	 * 
	 * @param languageProperties
	 *            - soubor typu properties, který obsahuje texty aplikace ve
	 *            zvoleném jazyce, abych vypsal text výše do editoru výstupů ve
	 *            správném jazyce
	 */
	public static void createClassLoader(final String pathToBinDir, final OutputEditorInterface outputEditor,
                                         final Properties languageProperties) {
		
		CompiledClassLoader.languageProperties = languageProperties;
		
		CompiledClassLoader.pathToBinDir = pathToBinDir;
		
		try {	
			// Vytvořím si soubor na dané umístění a otestuji, zda existuje
			final File file = new File(pathToBinDir);
			
			
			// Následující podmínka je zbytečná, už když se definuje cesta k adresáří bin,
			// kde jsou umístěny přeložené soubory, tak se testuje, zda daný adresář i s třídou existují,
			// ale jako prevenze - kdyby náhodou ...
			if (file.exists() && file.isDirectory()) {
				
				// Vytvořím si URL - cestu k adresáři - zde by se také mohlo testovat, kdyby byla url null, 
				// pak adresář neexistuje, ale nepoznám, zda je to adresář nebo soubor - kdyby náhodou, ale nemělo by nastat
				final URL url = file.toURI().toURL();
				
				// vytvořím si pole s jedním parametrem, protože to chce URLClassLoader:
				final URL[] urlArray = new URL[] {url};
				
				// Vytvořím si ClassLoader, k cestě k adresáři bin, ze které lze dále načřítat přeložené třídy, resp. zkompilované
				// třídy z diagramu tříd:
				cl = URLClassLoader.newInstance(urlArray, CompiledClassLoader.class.getClassLoader());
			}
			
		} catch (MalformedURLException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informace i chybě:
				logger.log(Level.INFO,
						"Zachycena výjimka při vytváření URL, třída CompiledClassLoader, v balíčku file. Tato chyba může " +
								"nastat například v případě, že nebyl "
								+ "nalezen popisovač protokolu pro adresu URL nebo pokud při vytváření adresy URL " +
								"došlo k jiné chybě");

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();
			
			

			// Otestuji, zda je vůbec ožné vypsat uživateli oznámení a pak si načtupříslušný
			// text s oznámením:
			if (outputEditor != null) {
				if (languageProperties != null)
					outputEditor.addResult(languageProperties.getProperty("Mcl_ErrorWhileCreatingURL", Constants.MCL_ERROR_WHILE_CREATING_URL));
				
				else outputEditor.addResult(Constants.MCL_ERROR_WHILE_CREATING_URL);
			}			
		}	
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která načte třídu zadanou v parametr.
	 * 
	 * @param classNameWithPackages
	 *            - název třídy i s balíčky od adresáře src (bez něj), která se má
	 *            načíst
	 * 
	 * @param outputEditor
	 *            - reference na editoru výstupů, kam se zapíše oznámení uživateli o
	 *            nastalé chybě při čtení přeložené - zkompilované třídy z adresáře
	 *            bin v otevřeném projektu
	 * 
	 * @return načtená třída (v parametru) z adresáře bin v aktuáně otevřeném
	 *         projektu
	 */
	public static Class<?> loadClass(final String classNameWithPackages,
                                     final OutputEditorInterface outputEditor) {
		try {
			/*
			 * Pouze v případě, že cesta k adresáři 'bin' v aktuálěn otevřeném projektu
			 * nebyla zadána nebo neexistuje, tak jej zkusítm načíst, pokud ani tak nebude
			 * nalezen příslušný soubor, nenačte se zkompilovaná třída (resp. soubor
			 * .class).
			 * 
			 * To, že by zde nebyla načtena cesta k adresáři 'bin' v otevřeném projektu by
			 * teoreticky nemělo nastat, ale doplnil jsem to zde, protože pokud se například
			 * v editoru příkazů zavolá příkaz pro naplnění proměnné v nějaké instanci třídy
			 * statickou metodou, tak, kdyby se třídy vůbec nezkompilovaly tak, že by je
			 * někdo úplně vymazal, teoreticky by to možná mohlo nastat, záleží, kde přesně
			 * a jaký přesně příkaz by zadal, ale sem by se to dostat nemuselo, pokud by
			 * neexistovala ani ta instance, jinak to asi možné je, sám jsem to nekrekoval
			 * až sem.
			 */
			if (CompiledClassLoader.pathToBinDir == null || !ReadFile.existsDirectory(pathToBinDir))
				CompiledClassLoader.pathToBinDir = App.READ_FILE.getPathToBin();
			
			final File file = new File(
					pathToBinDir + File.separator + classNameWithPackages.replace(".", File.separator) + ".class");
			
			if (ReadFile.existsFile(file.getAbsolutePath())) {
				/*
				 * Jelikož metoda exist nad příslušným souborem nerozlišuje velká a malá
				 * písmena, tak si musím načíst ještě veškeré soubory v příslušném adresáři a
				 * všechny je znovu prohledat, zda se v nich nachází příslušný soubor (.class),
				 * ale hledat název pomocí equals, aby nedošlo k chybě (názvy musí být case
				 * sensitive):
				 * 
				 * java.lang.NoClassDefFoundError: model/data/Classname (wrong name:
				 * model/data/ClassName)
				 * 
				 * protože tato chyba může nastat právě v případě, kdy se hledá název třídy,
				 * který byl zadán a poslední podmínkou prošlo, že existuje, ale liší se v názvu
				 * velká a malá písmena.
				 * 
				 * Například, pokud uživatel do editoru příkazů zadá příkaz:
				 * 
				 * new model.data.Classname();
				 * 
				 * ale v názvu balíčků nebo třídy se nachází jiné písmeno (písmeno s jinou
				 * velikostí - velké nebo malé), pak dojde k chybě.
				 * 
				 * pokud opravdu na disku existuje: new model.data.ClassName();
				 */
				if (!checkIfFileExistWithCaseSensitive(classNameWithPackages, new File(pathToBinDir), outputEditor))
					return null;
				
				
				/*
				 * Note:
				 * Pokud uživatel úmyslně přesune třídu do jiného balíčku a zkusí aktualizovat
				 * diagram tříd, pak je možné, že pokud ta přesunutá třída nebude validní, tj.
				 * nepůjde zkompilovat, nebo bude mít na začátku dané třídy uveden jiný balíček,
				 * než ve kterém se opravu nachází, vypadně chyba ClassLoderu zhruba na
				 * následujícím řádku u načtení dané třídy, jde o to, že uživatel chybně
				 * přesunul třídu, a nezměnil její package na začátku třídy nebo se ji
				 * nepodařilo zkompilovat.
				 */
				return cl.loadClass(classNameWithPackages.replace(File.separator, "."));
			}

			
		} catch (ClassNotFoundException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při získávání souboru typu Class z adresáře bin v otevřeném projektu,  " +
                                "třídu : "
                                + classNameWithPackages
                                + ", třída CompiledClassLoader, metoda: loadClass. Tato chyba může nastat v případě, že " +
                                "soubor (třída) na zadané cestě nebyla nalezena.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			
			// Otestuji, zda existuje reference na editor výstupů, abych mohl oznámit uživateli, že došlo k chybě:
			if (outputEditor != null) {
				if (languageProperties != null) {
					final String info1, info2;
					
					info1 = languageProperties.getProperty("Mcl_ErrorWhileLoadingClassFile_1", Constants.MCL_ERROR_WHILE_LOADING_CLASS_FILE_1);
					info2 = languageProperties.getProperty("Mcl_ErrorWhileLoadingClassFile_2", Constants.MCL_ERROR_WHILE_LOADING_CLASS_FILE_2);
					
					outputEditor.addResult(info1 + ": " + classNameWithPackages + ".class, " + info2);
				}					
				
				else
					outputEditor.addResult(Constants.MCL_ERROR_WHILE_LOADING_CLASS_FILE_1 + ": " + classNameWithPackages
							+ ".class, " + Constants.MCL_ERROR_WHILE_LOADING_CLASS_FILE_2);
			}						
		}
		
		return null;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro otestování, zda se v zadaném adresáři a k němu
	 * připojenými balíčky nachází soubor s příponou .class. Ale toto testování
	 * probíhá tak, že se testují velikost písmen - case sensitive.
	 * 
	 * Metoda, která zjistí, zda se v adresáři bin v aktuálně otevřeném projektu,
	 * resp. zda se v adresáři file a k němu připojenými balíčky v packages nachází
	 * soubor s příponou .class (název tohoto souboru je za poslední desetinnou
	 * tečku v packages.
	 * 
	 * Proměnná packages obsahuje balíček / balíčky i s názvem třídy, jejíž
	 * přeložená / zkompilovaná verze se má načíst. Naříklad se může jednat o
	 * syntaxi: model.data.ClassName -> Vždy za poslední desetinnou tečkou je název
	 * třídy, pokud se v proměnné packages nenachází desetinná tečka, pak se
	 * automaticky vrátí false.
	 * 
	 * Metoda funguje tak, že se na začátku načtou balíčky - z proměnná packages se
	 * rozdělí dle desetinné tečky.
	 * 
	 * Toto získané pole se iteruje a v každé iteraci se testuje, zda příslušný
	 * adresář obsahuje adresář, reps. balíček (v iteraci).
	 * 
	 * Pokud se projde celé rozdělené pole až po předposlední hodnotu, protože ta
	 * poslední hodnota je název soubor .class, tak se zjistí, zda tento soubor
	 * .class existuje v posledním balíčku, pokud ano, vrátí se true, jinak kdekoliv
	 * výše, pokud dojde k takové chybě, že například neexistuje adresář apod. tak
	 * se vrátí false.
	 * 
	 * @param packages
	 *            - balíčky i s název souboru .class (zkompilovaná třída), která se
	 *            má načíst, například: 'model.data.ClassName'.
	 * 
	 * @param file
	 *            - adresář bin, který se prohledává, zda obsahuje příslušné
	 *            ädresáře v packages (až po poslední -> ten před poslední
	 *            desetinnou tečkou).
	 * 
	 * @param outputEditor
	 *            - reference na editoru výstupů, kam se zapíše oznámení uživateli o
	 *            nastalé chybě při čtení přeložené - zkompilované třídy z adresáře
	 *            bin v otevřeném projektu
	 * 
	 * @return true, pokud se najde soubor .class v příslušných balíčcích s case
	 *         sensitive názvy, jinak false.
	 */
	private static boolean checkIfFileExistWithCaseSensitive(final String packages, final File file,
			final OutputEditorInterface outputEditor) {
		/*
		 * Proměnná, do které si budu vkládat vždy adresář, jehož soubory se mají
		 * prohledávat, zda obsahují příslušný balíček, nebo na konec, zda obshuje
		 * příslušný soubor .class.
		 */
		File fileTemp = file;

		/*
		 * Pole, které obsahuje texty rozdělené dle desetinné tečky z proměné packages.
		 */
		final String[] parts = packages.split("\\.");

		
		/*
		 * Pokud je velikost nula nebo jedna, tak se automaticky skončí, protože v této
		 * aplikaci se musí každá třída (kvůli kompilátoru) nacházet alespoň v nějakém
		 * balíčku, takže minimálně musí být text: 'defaultPackage.ClasName'.
		 */
		if (parts.length <= 1)
			return false;

		
		/*
		 * projde názvy výše a zjistím, zda všechny adresáře existují, názvy se testují
		 * tak, aby byly case sensitive.
		 */
		for (int i = 0; i < parts.length - 1; i++) {
			if (exist(parts[i], fileTemp))
				fileTemp = new File(fileTemp.getAbsolutePath() + File.separator + parts[i]);

			else {
				if (outputEditor != null) {
					if (languageProperties != null) {
						final String part_1, part_2;

						part_1 = languageProperties.getProperty("Mcl_Txt_PackageNotFound_1",
								Constants.MCL_TXT_PACKAGE_NOT_FOUND_1);
						part_2 = languageProperties.getProperty("Mcl_Txt_PackageNotFound_2",
								Constants.MCL_TXT_PACKAGE_NOT_FOUND_2);

						outputEditor.addResult(part_1 + ": '" + parts[i] + "' " + part_2 + ": '" + fileTemp + "'.");
					}

					else
						outputEditor.addResult(Constants.MCL_TXT_PACKAGE_NOT_FOUND_1 + ": '" + parts[i] + "' "
								+ Constants.MCL_TXT_PACKAGE_NOT_FOUND_2 + ": '" + fileTemp + "'.");
				}

				return false;
			}
		}

		
		
		/*
		 * Sem se dostanu, když všechny adresáře / balíčky existují, tak otestuji, zda
		 * se našel i příslušný soubor.
		 */
		if (containsArrayFile(fileTemp.listFiles(), parts[parts.length - 1] + ".class"))
			return true;

		
		
		 if (outputEditor != null) {
			if (languageProperties != null) {
				final String part_1, part_2;

				part_1 = languageProperties.getProperty("Mcl_Txt_InPackage", Constants.MCL_TXT_IN_PACKAGE);
				part_2 = languageProperties.getProperty("Mcl_Txt_FileNotFound", Constants.MCL_TXT_FILE_NOT_FOUND);

				outputEditor.addResult(part_1 + ": '" + fileTemp.getAbsolutePath() + "' " + part_2 + ": '"
						+ parts[parts.length - 1] + ".class'.");
			}

			else
				outputEditor.addResult(Constants.MCL_TXT_IN_PACKAGE + ": '" + fileTemp.getAbsolutePath() + "' "
						+ Constants.MCL_TXT_FILE_NOT_FOUND + ": '" + parts[parts.length - 1] + ".class'.");
		}
		 		

		/*
		 * Sem se dostanu pouze v případě, že se výše nenašel hledaný soubor, ale
		 * balíčky byly nalezeny, tak mohu vrátit false.
		 */
		return false;
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, prohledá pole files a zjistí, zda se v něm nachází
	 * adresář s názvem dirName (case sensitive názvy).
	 * 
	 * @param files
	 *            - list se soubory v nějakém adresáři.
	 * 
	 * @param dirName
	 *            - název adresáře, o kterém se má zjistit, zda se nachází v poli
	 *            files
	 * 
	 * @return true, pokud se název adresáře dirName nachází v poli files, jinak
	 *         false.
	 */
	private static boolean containsArrayDir(final File[] files, final String dirName) {
		return Arrays.stream(files).anyMatch(f -> f.getName().equals(dirName) && f.isDirectory());
	}
	
	
	
	/**
	 * Metoda, která zjistí, zda se v poli files nachází soubor s názvem fileName
	 * (case sensitive názvy).
	 * 
	 * @param files
	 *            - pole, které obsahuje seznam nějakých souborů.
	 * 
	 * @param fileName
	 *            - název souboru, o kterém se má zjistit, zda se nachází v poli
	 *            files.
	 * 
	 * @return true, pokud se soubor s názvem fileName nachází v poli files, jinak
	 *         false.
	 */
	private static boolean containsArrayFile(final File[] files, final String fileName) {
		return Arrays.stream(files).anyMatch(f -> f.getName().equals(fileName) && f.isFile());
	}
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se v adresář file nachází adresář s názvem
	 * dirName.
	 * 
	 * @param dirName
	 *            - název adresáře (balíčku), o kterém se má zjistit, zda se nachází
	 *            v adresáři file.
	 * 
	 * @param file
	 *            - adresář, o kterém se má zjistit, zda obsahuje adresář s názvem
	 *            dirName.
	 * 
	 * @return true, pokud adresář file obsahuje adresář s názvem dirName, jinak
	 *         false.
	 */
	private static boolean exist(final String dirName, final File file) {
		// Tato podmínka by neměla nastat nikdy, není potřeba.
		if (!file.isDirectory())
			return false;

		/*
		 * Pole, do kterého si uložím seznam všech souborů v adresáři file.
		 */
		final File[] files = file.listFiles();

		// Toto by také nemělo nastat:
		if (files == null)
			return false;

		/*
		 * Zde zjistím, zda se v poli files nachází adresář se zadaným názvem - zda v
		 * tom adresáři je příslušný balíček dirName.
		 *
		 * (vrátím false v případě, že nebyl nalezen jeden z adresářů, tak
		 * vrátím false.)
		 */
		return containsArrayDir(files, dirName);
	}
}