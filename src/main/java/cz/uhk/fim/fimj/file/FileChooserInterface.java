package cz.uhk.fim.fimj.file;

import java.io.File;

/**
 * Tato třída, resp. rozhraní obsahuje metody, které implementuje třída FileChooser.java a jsou to metody, které
 * umožňují vybrat umístění či soubor pro otevření / uložení / vytvoření, apod. viz účely jednotlivých metod
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface FileChooserInterface {

    /**
     * Metoda, která vrátí cestu ke zvolenému souboru, do kterého se má uložit příslušný obrázek Pokud nebude vybán typ
     * souboru oznámí se to uživateli chybovou hláškou Pokud se zadá název a správný typ souboru - s možností, cesta se
     * vrátí a dále dojde nejspíše k uložení - to je již jiná část
     *
     * @return ceta k zvolenému nebo zadanému souboru
     */
    String getPathToSaveImage();


    /**
     * Metoda, která vrátí cestu ke zvolenému adresáři nového projektu Vybrat lze pouze adresář, nikolikv souboru do
     * tohoto adresáře se pak uládají další soubory - jiná část aplikace
     *
     * @param pathToWorkspace
     *         - cesta k workspacu - dialog se otevře s výchozím umístěním v tomto adresáři
     * @return cestu k adresáři zvoleným uživatelem
     */
    String getPathToNewProjectDirectory(final String pathToWorkspace);


    /**
     * Metoda, která slouží pro výběr adresáře projektu Metoda se používá např pro výběr adresáře, do kterého se má
     * uloži - nakopírovat projektu po kliknutí na tlačítko v menu Ulozit jako
     *
     * @param defaultDir
     *         - výchozí poloha - adresář, ve kterém se dialog zobrazí
     * @param showOpenDialog
     *         = logická proměnná o tom, zda se má zobrazit dialog pro otevření nebo uložení adresáře
     * @return cestu k zvolenému adresáři
     */
    String getPathToProjectDirectory(final String defaultDir, final boolean showOpenDialog);


    /**
     * Metoda, která zobrazí dialog Jfilechooser pro výběr Javovské třídy (soubor s příponou .java)
     *
     * @return cestu k tomuto souboru
     */
    String getPathToClass();


    /**
     * Metoda, kter8 otev5e dialog pro výběr umístění a názvu jednoho s konfiguračních souborů apliace např
     * default.propertis, ClassDiagram.properties, ...
     *
     * @param dialogTitle
     *         - titulek dialogu
     * @return cestu kde se m8 daný soubor umístit nebo null, pokud bude dialog zavřen
     */
    String getPathToPlaceSomeConfigurationFile(final String dialogTitle);


    /**
     * Metoda, kter8 otev5e dialog pro výběr umístění a názvu adresáře, kde se má umístit - kam se má nakopírovat
     * přislušný adresář s konfiguračními soubory - jazyky, apod.
     *
     * @param dialogTitle
     *         - titulek dialogu pro výběr umístění
     * @return zvolenou cestu kde se má adresář umístit, nebu null, pokud bude dialog uzavřen
     */
    String getPathToPlaceSomeConfigurationDir(final String dialogTitle);


    /**
     * Metoda, která otevře dialog pro výběr souboru (Javovksá třída (.java) nebo Textový dokument (.txt)) a vrátí cestu
     * k tomuto souboru
     *
     * @param dialogTitle
     *         - titulek dialogu
     * @return cestu k výše zmíněmému souboru
     */
    String getPathToClassOrTxtFile(final String dialogTitle);


    /**
     * Metoda, která zobrazí dialog pro výběr pouze konkrétního souboru - currency.data pro kompilátor
     *
     * @param fileCurrencyData
     *         - cesta ke zvolenému souboru. Pokud je to null, využije se výchozí hodnota pro cestu k nainstalovanému
     *         adresáři a názvu souboru, jinak se využije název zadaného adresáře a název zadaného souboru pro výchozí
     *         umístění dialogu.
     * @return null nebo soubor currency.data, který uživatel zvolil
     */
    String getPathToCurrencyData(final File fileCurrencyData);


    /**
     * Metoda, která vrátí cestu ke zvolenému souboru, pouze pokud se ten soubor bude jmenovat tools.jar - přesně! Jinak
     * vyhodí hlášku, že byl označen špatný soubor případně vrátí null po uzavření
     *
     * @param fileToolsJar
     *         - reference na zvolený soubor tools.jar, popřípadě jiný soubor, který uživatel vybral, to jestli je to
     *         správný soubor se pozná až u potvrzení dialogu, ne zde při výběru souborů. Pokud je tato hodnota null,
     *         nastaví se výchozí hodnoty pro zvolený adresář a název souboru, jinak se zvolí jako zvolený soubor ten
     *         zvolený soubor uživatelem a jako adresář adresář, kde se ten zvolený soubor nachází.
     * @return null nebo cestu k souboru tools.jar pro kompilaci
     */
    String getPathToToolsJar(final File fileToolsJar);


    /**
     * Metoda, která zobrazí dialog pro zadání cesty k souboru flavormap.properties jen a pouze k tomuto souboru, který
     * se dále zkopíruje do adresáře libTools v aplikaci.
     *
     * @param fileFlavormapProp
     *         - referena na označený soubor flavormap.properties, který se má v příslušném dialogu označit i s jeho
     *         rodičovským adresářem, nebo null, pak se využijí výchozí hodnota v nainstalovaném adresáří Javy na
     *         příslušném zařízení.
     * @return null nebo cestu k souboru flavormap.properties
     */
    String getPathToFlavormapProp(final File fileFlavormapProp);


    /**
     * Metoda, která zobrazí dialog pro výběr adresáře, coby domovský adresář, kde je nainstalovaná Java na PC
     * uživatele
     *
     * @param fileSelectedDir
     *         - reference na zadaný adresář, ve kterém se má otevřít příslušné okno pro výběr adresáře.
     * @return null nebo cestu k výše zmíněnému adresář
     */
    String getPathToJavaHomeDir(final File fileSelectedDir);


    /**
     * Metoda, která zobrazí dialog pro výběr adresáře, který reprezentuje workspace, tj. slouží pro "ukládání"
     * projektů
     * v tomto adresáři.
     *
     * @return null, pokud uživatel nevybere adresář, nebo cestu k označenému adresáři, který chce uživatel označit
     * jako
     * workspace.
     */
    String getPathToWorkspaceDir();


    /**
     * Metoda, která zobrazí dialog pro výběr cesty k novému souboru, kam se má uložit soubor otevřený v průzkumníkovi
     * projektů v nějakém interním okně
     *
     * @return = cesta k zvolenému, resp. nvému souboru, nebo null, pokud nebude vybrán
     */
    String getPathToNewFile();


    /**
     * Metoda, která zobrazí dialog pro výběr souboru, která se má otevřít v novém intrním okně v průzkumníku projektů,
     * půjde vybrat libovolný soubor, ale po označení se otestuje, zda půjde otevřít nebo ne, ... jiná část App.
     * <p>
     * Dále se tato metoda využívá pro stejný účel, tedy označení souboru pro otevření, ale v hlavním menu aplikace pro
     * otevření souboru v průzkumníku projektů.
     *
     * @return Cestu k oznčenému souboru, nebo null, pokud se soubor nevybere
     */
    String getPathToNewFileForOpen();


    /**
     * Metoda, která zobrazí dialog pro výběr názvu a umístění souboru - včetně přípony,
     *
     * @return cestu k uživatelem zvolenému souboru
     */
    String getPathToNewFileForSave();
}