package cz.uhk.fim.fimj.file;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;

/**
 * Tato třída, resp. rozhraní obsahuje dvě metody, a sice
 * 'getIcon' (přetíženou metodu) která vrátí upravený obrázek na zadané cestě v parametru
 * metody. Více viz konkrétní metoda.
 *
 * Tato metoda se používá pro nastavení výsledné ikony pro nějakou komponentu
 * ať už je to ikona nějakého dialogu či tlačítka v Toolbaru nebo v menu, apod.
 *
 *
 * Note:
 * Ta druhá - přetížená metoda, getIcon s jedním parametrem - path, tak tuto metodu jsem musel
 * doplňit, protože v dialogu coby průzkumníku projektů docházelo (občas) k vyjímce (viz níže),
 * a abych jí předešel, tak jsem si našel na následujícím odkazu postup, jak tomu zabránit a už nevyskakuje:
 * Zdroj:
 *
 * (v débě, kdy jsem to našel do byl hned první komentář se 7 liky)
 * http://stackoverflow.com/questions/17383867/set-icon-image-in-java
 *
 * Text vyjímky, která nastávala, když jsem použil tu první metodu i ve třídě: ProjectExplorerIconInterface.java
 * v metodě getIcon na konci metody pro navrácení ikony.
 * Vyjímka:
 * java.lang.ClassCastException: [I cannot be cast to [B
	at java.awt.image.ColorModel.getAlpha(Unknown Source)
	at java.awt.image.ColorModel.getRGB(Unknown Source)
	at sun.awt.image.ImageRepresentation.convertToRGB(Unknown Source)
	at sun.awt.image.ImageRepresentation.setPixels(Unknown Source)
	at java.awt.image.AreaAveragingScaleFilter.accumPixels(Unknown Source)
	at java.awt.image.AreaAveragingScaleFilter.setPixels(Unknown Source)
	at sun.awt.image.ImageDecoder.setPixels(Unknown Source)
	at sun.awt.image.PNGImageDecoder.sendPixels(Unknown Source)
	at sun.awt.image.PNGImageDecoder.produceImage(Unknown Source)
	at sun.awt.image.InputStreamImageSource.doFetch(Unknown Source)
	at sun.awt.image.ImageFetcher.fetchloop(Unknown Source)
	at sun.awt.image.ImageFetcher.run(Unknown Source)


 * @author Jan Krunčík
 * @version 1.0
 */

public interface GetIconInterface {

    /**
     * Metoda, která slouží pro získání ikony, resp. obrázku na zadané cestě v parametru metody - path
     * <p>
     * Metoda dále příslušný obrázek nastaví na mnou definouvanou velikost ikony, tj výsledná ikona bude mít rozměry
     * 15x15 s vyjímkou ikon dialogu, tam se přízpůsobí již předdefinovaně oknem, a pro MySplashScreen bude mít také
     * mnou definouvanou velikost, ale pro ostatní komponenty, jako je například menu, tlačítka, apod. to bude již mnou
     * definovaná, výše zmíněná velikost
     *
     * @param path
     *         - cesta k obrázku, který se má načíst a vrátit jako "upravená - změnšená" ikona, resp. obrázek typu
     *         Image
     * @param isIcon
     *         - logická proměnné o tom, zda bude výsledná ikona upravena na velikost 15x15 nebo XXX pro obrázek pozadí
     *         MySplashScreen
     *
     * @return obrázek typu ImageIcon coby ikona pro nějakou komponentu
     */
    static ImageIcon getMyIcon(final String path, final boolean isIcon) {
        final URL url = GetIconInterface.class.getResource(path);

        if (url != null) {
            final Image img = new ImageIcon(url).getImage();


            // Otestuji, zda se má vrátit obrázek ve velikosti pro ikonu nebo pozadí pro MySplashScreen:
            if (isIcon)
                // Zde se jedná o ikonu ve velikost 15x15
                return new ImageIcon(img.getScaledInstance(15, 15, Image.SCALE_SMOOTH));

                // Zde se jedná o obrázek pro pozadí SplashScreen
            else
                return new ImageIcon(img.getScaledInstance(img.getWidth(null) - 10, img.getHeight(null) - 10,
                        Image.SCALE_SMOOTH));
        }
        // else - zde nebyl obrázek na cílovém umístění nalezen, tak
        // ho nelze vrátit, tudíž se vrátí null, a ikona nebude nastaven
        // na požadovaný obrázek, ale na výchozí

        return null;
    }


    /**
     * Metoda, která slouží pro získání - načtení obrázku z adresáře icons a dále na cestě path.
     * <p>
     * Tato metoda by vlastně ani nebyla potřeba, jenže když jsem načítal ikony pomocí metody:
     * GetMyIconInterface.getIcon(iconPath, true), tak občas nastala vyjímka, a abych jí předešel, tak jsem si našel
     * tento postup - více informací v hlavičce třídy
     *
     * @param path
     *         - relativní cesta k obrázku, který se má načíst
     *
     * @return načtený obrázek na cestě path v podobně ImageIcon, nebo null, pokud dojde k nějaké chybě třeba pří čtení
     * obrázku, například nebude nalezen na azdané cestě, apod.
     */
    static ImageIcon getMyIcon(final String path) {
        try (final InputStream imgStream = GetIconInterface.class.getResourceAsStream(path)) {

            final BufferedImage myImg = ImageIO.read(imgStream);
            final Image img = new ImageIcon(myImg).getImage();

            return new ImageIcon(img.getScaledInstance(15, 15, Image.SCALE_SMOOTH));

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jakodruhou informace text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informace i chybě:
                logger.log(Level.INFO, "Zachycena výjimka pří získávání obrázku, cesta: " + path
                        + ", třída: GetMyIconInterface, metoda: getIcon - s jedním parametrem.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }

        return null;
    }
}