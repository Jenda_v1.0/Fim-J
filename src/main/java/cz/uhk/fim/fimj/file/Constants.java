package cz.uhk.fim.fimj.file;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import cz.uhk.fim.fimj.menu.SortRecentlyOpenedProjects;
import org.jgraph.graph.Edge.Routing;

import cz.uhk.fim.fimj.class_diagram.KindOfCodeEditor;
import cz.uhk.fim.fimj.font_size_form.FontSizeFormType;
import cz.uhk.fim.fimj.language.KeysOrder;

import org.jgraph.graph.GraphConstants;

/**
 * Tato třída obsahuje veškeré výchozí konstanty do aplikace Obsahuje veškeré výchozí nastavení fontů, velikost písma,
 * barev, jazyka, ... Z této třidy si ostatní "části" aplikace berou výchozí nastavení
 * <p>
 * Dále se tato třída používá na to, že když se při čtení vlastnosti z nějakého souboru (Properties s uloženými
 * vlastnostmi) dojde k "chybě" a potřebná vlastnost v souboru neni, nebo nelze přečíst apod, tak se z této třídy vezme
 * výchozí
 * <p>
 * Tuto třidu si každá potřebná třída (část aplikace) vytvoří a vezme se při čtení zde zadanou výchozí hodnotu pro
 * případ, že by došlo k chybě (zmíněné výše)
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class Constants {

    /**
     * Lomítko dle využívaného OS.
     * <p>
     * Dle tohoto "vzoru" je možné rozdělit cestu k souboru nebo adresáři dle lomítek využívaných konkrétním OS.
     * <p>
     * Je potřeba například v případě, že se má cesta rozdělit dle lomítek na OS Windows, kdy by se nepodařilo rozdělení
     * dle regulurního výrazu v syntaxe se dvěmi zpětnýmy lomítky, musely by být čtyři a zvláště jej testovat pro OS
     * Linux.
     */
    public static final String OS_PATH_PATTERN = Pattern.quote(System.getProperty("file.separator"));


    /**
     * Výchozí "desing", který je bude nastaven pro aplikaci.
     */
    public static final String DEFAULT_LOOK_AND_FEEL = "javax.swing.plaf.nimbus.NimbusLookAndFeel";

    /**
     * Jedná se o konstantu, která značí, že nebude aplikován žádný Look and Feel. Resp. využije se výchozí nastavení.
     */
    public static final String WITHOUT_SPECIFIC_LOOK_AND_FEEL = "WITHOUT_SPECIFIC_LOOK_AND_FEEL";


    /**
     * Cesta k ikoně pro aplikaci.
     */
    public static final String PATH_TO_APP_ICON = "/icons/AppIcon.gif";


    private Constants() {
    }


    /**
     * Symbol pro otevírací html tag: '<'.
     * <p>
     * Protože, když nekde zobrazuji text v html syntaxi, například v nějakém JLabelu, pak se tyto tagy nezobrazují,
     * proto je třeba jej nahradit za speciální znaky.
     */
    public static final String HTML_OPEN_TAG = "&lt;";

	/**
	 * Symbol pro zavírací html tag: '>'.
	 *
	 * Protože, když nekde zobrazuji text v html syntaxi, například v nějakém
	 * JLabelu, pak se tyto tagy nezobrazují, proto je třeba jej nahradit za
	 * speciální znaky.
	 */
	public static final String HTML_CLOSE_TAG = "&gt;";



	/**
	 * Regulární výraz, který slouží pro rozpoznání zavárací špičaté závorky ('}')
	 * na řádku, tj. na příslušném řádku, resp. v příslušném textu musí být pouze ta
	 * zavírací závorka.
	 */
	public static final String REG_EX_CLOSING_BRACKET = "^\\s*}\\s*$";






	// proměnná, ve které je uchován název aplikace, který je zobrazen na určitých
	// místech v oknách aplikace:
	public static final String APP_TITLE = "Fim-J";



	// Proměnná, ve které je uložena cesta k vchozímu adresáři (libTools v adresáři aplikace), ve které jsou výchozí
	// soubory, které potřebuji "můj" kompilkátor, aby mohl kompilovat třídy z diagramu tříd
	// "jedná se o cestu k adresáři, odkud si komilkátor bere potřebná data"

	// Note: tato proměnná se načítá nejprve z souboru Default.properties v adresáři aplikace, ne ve Workspace!
	public static final String COMPILER_DIR = "libTools";

	/**
	 * Proměnná, která obsahuje text 'lib', což značí adresář, ve kterém se mohou
	 * nacházet soubory, které potřebuje aplikace pro to, aby mohla kompilovat
	 * Javovské třídy. Je to zde, protože spojuji cesty z adresáře libTools a lib,
	 * tak abych se někde náhodou neupsal.
	 */
	public static final String COMPILER_DIR_LIB = "lib";




	// Adresář, kde jsou ukládány "hlavní" tři soubory, které jsou potřeba pro kompilaci tříd v diagramu tříd:
    public static final String COMPILER_LIB_DIR = COMPILER_DIR + File.separator + COMPILER_DIR_LIB;



	/**
	 * Proměnné, které obsahují názvy souborů, které jsou potřebné pro to, aby bylo
	 * možné, aby aplikace mohla kompilovat Javovské třídy. Je třeba, aby aplikace
	 * znala cestu k adresáři lib, kde se nachází alespoň tyto soubory.
	 */
    public static final String COMPILER_FILE_FLAVORMAP_PROPERTIES = "flavormap.properties";
    public static final String COMPILER_FILE_TOOLS_JAR = "tools.jar";
    public static final String COMPILER_FILE_CURRENCY_DATA = "currency.data";






	/**
	 * Název adresáře s konfiguračními soubory, který se vždy bude nacházet v
	 * adresáři označený uživatelem jako workspace.
	 */
	public static final String CONFIGURATION_NAME = ".configuration";


	/**
	 * Název adresáře, který obsahuje texty pro aplikaci v příslušných jazycích.
	 * Tento adresář by se měl nacházet v adresáři configuration ve workspace.
	 */
	public static final String LANGUAGE_NAME = "language";



	/**
	 * Proměnná, která drží název adresáře - logs, do kterého se zapisují soubory,
	 * ohledně nastalých vyjímek a spuštění aplikace
	 */
	public static final String LOGS_NAME = "logs";



	/**
	 * Výchozí jazyk pro aplikaci.
	 */
	public static final String DEFAULT_LANGUAGE_NAME = "CZ";


	/**
	 * Logická proměnná o tom, zda se má zobrazit při spuštění aplikace okno coby
	 * dotaz na adresář workspace nebo ne. True - má se zobrazit při spuštění okno
	 * pro zadání adresáře workspace, false nemá se při spuštění aplikace zobrazit
	 * okno pro zadání adresáře workspace.
	 */
	public static final boolean DEFAULT_DISPLAY_WORKSPACE = true;





    /**
     * Název souboru, který se vytvoří při vytvoření projektu. Jedná se o textový soubor s informacemi o projektu pro
     * ostatní uživatele.
     */
    public static final String READ_ME_FILE_NAME = "READ_ME";
    /**
     * Název souboru s příponou ".txt", který se vytvoří při vytvoření projektu. Jedná se o textový soubor s informacemi
     * o projektu pro ostatní uživatele.
     */
    public static final String READ_ME_FILE_NAME_WITH_EXTENSION = READ_ME_FILE_NAME + ".txt";











	// Názevy potřebných - konfiguračních souborů pro jednotlivé komponenty v aplikaci:
	public static final String DEFAULT_PROPERTIES_NAME = "Default.properties";

	public static final String CLASS_DIAGRAM_NAME = "ClassDiagram.properties";

	public static final String INSTANCE_DIAGRAM_NAME = "InstanceDiagram.properties";

	public static final String COMMAND_EDITOR_NAME = "CommandEditor.properties";

	public static final String CODE_EDITOR_NAME = "CodeEditor.properties";

	public static final String OUTPUT_EDITOR_NAME = "OutputEditor.properties";

	public static final String OUTPUT_FRAME_NAME = "OutputFrame.properties";

	public static final String CODE_EDITOR_INTERNAL_FRAMES_NAME = "CodeEditorInternalFrame.properties";










	// Výchozí konstanty pro nastavení nějakých textů k souborům:
	public static final String PATH_TO_DEFAULT_PROPERTIES = CONFIGURATION_NAME + File.separator + DEFAULT_PROPERTIES_NAME;


	public static final String PATH_TO_CLASS_DIAGRAM_PROPERTIES = CONFIGURATION_NAME + File.separator + CLASS_DIAGRAM_NAME;

	public static final String PATH_TO_INSTANCE_DIAGRAM_PROPERTIES = CONFIGURATION_NAME + File.separator + INSTANCE_DIAGRAM_NAME;


	public static final String PATH_TO_COMMAND_EDITOR_PROPERTIES = CONFIGURATION_NAME + File.separator + COMMAND_EDITOR_NAME;

	public static final String PATH_TO_CODE_EDITOR_PROPERTIES = CONFIGURATION_NAME + File.separator + CODE_EDITOR_NAME;

	public static final String PATH_TO_OUTPUT_EDITOR_PROPERTIES = CONFIGURATION_NAME + File.separator + OUTPUT_EDITOR_NAME;

	public static final String PATH_TO_OUTPUT_FRAME_PROPERTIES = CONFIGURATION_NAME + File.separator + OUTPUT_FRAME_NAME;


	public static final String PATH_TO_CODE_EDITOR_INTERNAL_FRAMES_PROPERTIES = CONFIGURATION_NAME + File.separator + CODE_EDITOR_INTERNAL_FRAMES_NAME;



	// Název souboru s Class diagramem:
	public static final String NAME_OF_CLASS_DIAGRAM_FILE = "ClassDiagramProperties.xml";





	// Výchozí balíček pro třídy v diagramu tříd:
	public static final String DEFAULT_PACKAGE = "defaultPackage";





	/**
	 * Proměnná typu String, která obsahuje text Closed, abych ho nemusel opakovaně testovat, tak ho dám do této
	 * proěnné
	 * a je to hodnota, která reprezentuje zavřený projekt v preferencích, jinak bude v příslušné hodnotě v příslušné
	 * proměnné cesta k otevřenému projektu
	 */
	public static final String CLOSED_PROJECT = "Closed";




	/**
	 * Hodnota, která "definuje", že není / nebyl uložen soubor, resp. cesta k spouštěcímu skriptu do preferencí.
	 * <p>
	 * Jde o to, že když sena OS Linux nepovede smazat spuštěcí skript, který se vytvořil, pak cesta k němu uloží do
	 * preferencí.
	 * <p>
	 * Po spuštění aplikace (na OS Linux) se otestuje, zda se v těch preferencích vyksytuje cesta k příslušnému souboru
	 * a pokud ano, ten soubor se smaže, pokud tam ale místo cesty k souboru bude tato hodnota, pak nebyl byl nejspíše
	 * příslušný soubor úspěšně smazán a není potřeba spouštět vlákno.
	 */
	public static final String FILE_NOT_SPECIFIED = "File not specified";





	/**
	 * Proměnná, která obsahuje výchozí název obrázku typu .gif, který bude zobrazen
	 * při načítání v tzv. loading dialogu, když bude aplikace pracovat na pozadí,
	 * napřílad když na pozadí poběží nějaké vlákno, apod, tak aby si uživatel
	 * nemyslel, že se aplikace sekla, apod. tak se zobrazí tzv. loadign dialog,
	 * který bude obsahovat nějaký obrázek, který reprezentuje načítání, v této
	 * proměnné je výchozí obrázek pro případ, že by došlo k nějaké chybě při
	 * načítání příslušných obrázků
	 */
	public static final String DEFAULT_IMAGE_FOR_LOADING_PANEL = "default.gif";






	/**
	 * Logická proměnná, která značí, zda se má po spuštění aplikace otestovat, zda
	 * je nastaven adresář, který obsahuje soubory, které aplikace potřebuje pro
	 * kompilování tříd.
	 *
	 * Pokud není, pak se ty soubory zkusí najít a vložit do adresáře:
	 * workspace/confiiguration/libTools/lib/Zde ty soubory. Pokud by se ani na
	 * disku nenašly, pak jej musí už nastavit uživatel pomocí příslušného dialogu v
	 * aplikaci.
	 *
	 * Hodnota true značí, že se po spuštění aplikace pokaždé otestuje, zda ten
	 * adresář existuje a zda obsahuje ty soubory, případně je zkusí najít a
	 * zkopírovat do výše uvedeného adresáře ve workspace.
	 *
	 * Hodnota false značí, že se nic z toho testovat nebude, prostě se spustí
	 * aplikace a případně až když se aplikace pokusí zkompilovat nějakou třídu, tak
	 * to vyhodí chybovou zprávu uživateli a ten ty soubory pak bude muset nastavit
	 * například pomocí příslušného dialogu.
	 */
	public static final boolean SEARCH_COMPILE_FILES_AFTER_START_UP_APP = true;



	/**
	 * Logická proměnná o tom, zda semá prohledat celý disk, resp. celý PC, na
	 * kterém běží aplikace za účelem toho, aby se našli soubory, které aplikace
	 * potřebuje pro to, aby mohla kompilovat Javovské třídy.
	 *
	 * Dle této proměnné se buď bude prohledávat postupně celý disk poocí rekurze
	 * (pokud bude tato proměnná true). Jinak, pokud bude tato proměnná false, pak
	 * se nebude prohledávat disk pomocí rekurze, ale budou se pouze prohledávat jen
	 * "některé" adrsáře od adresáře zjišťěného jako Java.home až po kořen disku,
	 * jinak pomocí rekurze by se postupně prohledal celý disk a hrozilo by riziko,
	 * že by se mohli najít chybné soubory, pouze by měli stejná název.
	 */
	public static final boolean SEARCH_ENTIRE_DISK_FOR_COMPILE_FILES = false;




	/**
	 * Logická proměnná, která značí, zda se mají vypisovat nezachycené výjimky do
	 * dialogu "Výtupní terminál". True značí, že ano, false značí, že se nebudou
	 * vypisovat nezachycené výjimky do uvedeného dialogu.
	 */
	public static final boolean WRITE_NOT_CAPTURED_EXCEPTIONS_TO_OUTPUT_TERMINAL = true;








	/**
	 * Jedná se o výchozí hodnotu pro řazení hodnot v souborech .properties, tj.
	 * jazyky a nastavení pro tuto aplikaci.
	 *
	 * Původně jsem chtěl zvolit ještě jednu hodnotu se stejným účelem, akorát by se
	 * jednalo o proměnnou pro nastavení jen pro soubory .properties, které obsahuje
	 * texty pro tuto aplikaci v nějakém jazyce a jako druhou proměnnou hodnotu pro
	 * soubory .properties s nastavením pro aplikaci, tj. nastavení pro diagramy,
	 * pro editory, výchozí nastavení, ... Ale pak jsem to zavrhnul, protože pro mě
	 * přijde nejvhodnější (/ nejpřehlednější) varianta s řazením vzestupně dle
	 * přidání do souboru pro všechny soubory, jak pro jazyky, tak i pro ostatní
	 * soubory pro nastavení, protože takto to píšu od začátku aplikace a dávám tak
	 * příslušné "texty" k sobě - ty které k sobě patří.
	 */
	public static final KeysOrder DEFAULT_KEYS_ORDER = KeysOrder.ASCENDING_BY_ADDING;

	/**
	 * Výchozí hodnota pro to, zda se mají přidávat komentáře do souborů
	 * .properties, které obsahují texty pro aplikaci v příslušných jazicích.
	 *
	 * Výchozí hodnota bude true, přidat komentáře pro lepší orientaci.
	 */
	public static final boolean DEFAULT_VALUE_ADD_COMMENTS = true;

    /**
     * Proměnná, která značí výchozí počet volných řádků nad komentářem v souborech .properties, které obsahují texty v
     * nějakém jazyce pro tuto aplikaci.
     */
	public static final int DEFAULT_COUNT_OF_FREE_LINES_BEFORE_COMMENT = 3;

    /**
     * Výchozí definované pořadí pro seřazení dříve otevřených projektů v rozevíracím menu "Nedávno otevřené" v meu
     * "Projekt" v hlavním okně aplikace.
     */
    public static final SortRecentlyOpenedProjects DEFAULT_SORT_RECENTLY_OPENED_PROJECTS = SortRecentlyOpenedProjects
            .DES;




	/**
	 * Zde bude vždy výchozí hodnota pro Locale.
	 *
	 * Jedná se o hodnotu pro zvolení syntaxe pro datum vytvoření v souborech
	 * .properties obsahující texty pro aplikaci. Dle zadaného Locale se vytovří
	 * příslušná syntaxe pro datum.
	 *
	 * Note: Tato hodnota by ani nemusela být zde, stačí vždy využít Locale, ale pro
	 * případ, že bych se někdy v budoucnu rozhodl já, nebo někdo jiný to změnit,
	 * tak to pak stačí změnit pouze tady.
	 */
	public static final Locale DEFAULT_LOCALE_FOR_PROPERTIES = Locale.getDefault();








	/**
	 * Proměnné, které obsahují výchozí text pro dialogy JoptionPane, které obsahují
	 * tlačítka typu YES_OPTION a NO_OPTION. Tyto texty se aplikují na veškeré
	 * JoptionPane, které se v aplikací využívají. a všude, kde jsou uvedná tlačítka
	 * tak budou mít tento text nebo příslušný text dle zvoleného jazyka pro
	 * aplikaci.
	 */
	public static final String JOP_TXT_YES_BUTTON = "Ano", JOP_TXT_NO_BUTTON = "Ne";











	// Text pro FileChooser - text pro titulek dialogu: (FC = FileChooser)
	public static final String FC_SAVE_IMAGE_TITLE = "Uložit obrázek", FC_NEW_PROJECT_TITLE = "Nový projekt",
			FC_CHOOSE_PROJECT_DIR = "Vyberte adresář projektu", FC_CHOOSE_CLASS = "Přidat třídu",
			FC_CHOOSE_FILE = "Vyberte soubor", FC_CHOOSE_LOCATION_AND_KIND_OF_FILE = "Vyberte umístění a typ souboru";

	// Text pro chybové hlášky v FileChooser:
	public static final String FC_FILE_ERROR_TITLE = "Chyba souboru", FC_FILE_ERROR_TEXT = "Není zvolen typ souboru!",
			FC_CHOOSE_CURRENCY_DATA = "Vyberte soubor 'currency.data'", FC_WRONG_FILE_TITLE = "Chybný soubor",
			FC_WRONG_FILE_TEXT = "Lze vybrat pouze soubor", FC_CHOOSE_TOOLS_JAR = "Vyberte soubor 'tools.jar'",
			FC_CHOOSE_FLAVORMAP_PROP = "Vyberte soubor 'flavormap.properties'",
			FC_CHOOSE_JAVA_HOME_DIR = "Vyberte domovský adresář Javy", FC_SELECT_WORKSPACE = "Vyberte workspace";











	// např do NewClassForm - pro otestování názvu třídy:
	// Kličová slovy Javy:
	private static final String KEY_WORD_ABSTRACT = "abstract", KEY_WORD_ASSERT = "assert", KEY_WORD_BOOLEAN = "boolean",
			KEY_WORD_BREAK = "break", KEY_WORD_BYTE = "byte", KEY_WORD_CASE = "case", KEY_WORD_CATCH = "catch",
			KEY_WORD_CHAR = "char", KEY_WORD_CLASS = "class", KEY_WORD_CONTINUE = "continue",
			KEY_WORD_DEFAULT = "default", KEY_WORD_DO = "do", KEY_WORD_DOUBLE = "double", KEY_WORD_ELSE = "else",
			KEY_WORD_EXTEND = "extend", KEY_WORD_EXTENDS = "extends", KEY_WORD_ENUM = "enum", KEY_WORD_FALSE = "false",
			KEY_WORD_FINAL = "final", KEY_WORD_FINALLY = "finally", KEY_WORD_FLOAT = "float", KEY_WORD_FOR = "for",
			KEY_WORD_IF = "if", KEY_WORD_IMPLEMENTS = "implements", KEY_WORD_IMPORT = "import",
			KEY_WORD_INSTANCEOF = "instanceof", KEY_WORD_INT = "int", KEY_WORD_INTERFACE = "interface",
			KEY_WORD_LONG = "long", KEY_WORD_NATIVE = "native", KEY_WORD_NEW = "new", KEY_WORD_NULL = "null",
			KEY_WORD_PACKAGE = "package", KEY_WORD_PRIVATE = "private", KEY_WORD_PROTECTED = "protected",
			KEY_WORD_PUBLIC = "public", KEY_WORD_RETURN = "return", KEY_WORD_SHORT = "short",
			KEY_WORD_STATIC = "static", KEY_WORD_STRING = "String", KEY_WORD_SUPER = "super",
			KEY_WORD_SWITCH = "switch", KEY_WORD_SYNCHRONIZED = "synchronized", KEY_WORD_THIS = "this",
			KEY_WORD_THROW = "whrow", KEY_WORD_THROWS = "throws", KEY_WORD_TRANSIENT = "transient",
			KEY_WORD_TRUE = "true", KEY_WORD_TRY = "try", KEY_WORD_VOID = "void", KEY_WORD_VOLATILE = "volatile",
			KEY_WORD_WHILE = "while";


	/**
	 * Metoda, která vytvoří list, do kterého se vloži klíčová slova jazyka Java
	 * (viz výše). Tento list s klíčovými slovy metoda vrátí.
	 *
	 * @return vrátí List klíčových slov Javy
	 */
	public static List<String> getListOfKeyWords() {
		final List<String> keyWordsList = new ArrayList<>();

		keyWordsList.add(Constants.KEY_WORD_ABSTRACT);
		keyWordsList.add(Constants.KEY_WORD_ASSERT);
		keyWordsList.add(Constants.KEY_WORD_BOOLEAN);
		keyWordsList.add(Constants.KEY_WORD_BREAK);
		keyWordsList.add(Constants.KEY_WORD_BYTE);
		keyWordsList.add(Constants.KEY_WORD_CASE);
		keyWordsList.add(Constants.KEY_WORD_CATCH);
		keyWordsList.add(Constants.KEY_WORD_CHAR);
		keyWordsList.add(Constants.KEY_WORD_CLASS);
		keyWordsList.add(Constants.KEY_WORD_CONTINUE);
		keyWordsList.add(Constants.KEY_WORD_DEFAULT);
		keyWordsList.add(Constants.KEY_WORD_DO);
		keyWordsList.add(Constants.KEY_WORD_DOUBLE);
		keyWordsList.add(Constants.KEY_WORD_ELSE);
		keyWordsList.add(Constants.KEY_WORD_EXTEND);
		keyWordsList.add(Constants.KEY_WORD_EXTENDS);
		keyWordsList.add(Constants.KEY_WORD_ENUM);
		keyWordsList.add(Constants.KEY_WORD_FALSE);
		keyWordsList.add(Constants.KEY_WORD_FINAL);
		keyWordsList.add(Constants.KEY_WORD_FINALLY);
		keyWordsList.add(Constants.KEY_WORD_FLOAT);
		keyWordsList.add(Constants.KEY_WORD_FOR);
		keyWordsList.add(Constants.KEY_WORD_IF);
		keyWordsList.add(Constants.KEY_WORD_IMPLEMENTS);
		keyWordsList.add(Constants.KEY_WORD_IMPORT);
		keyWordsList.add(Constants.KEY_WORD_INSTANCEOF);
		keyWordsList.add(Constants.KEY_WORD_INT);
		keyWordsList.add(Constants.KEY_WORD_INTERFACE);
		keyWordsList.add(Constants.KEY_WORD_LONG);
		keyWordsList.add(Constants.KEY_WORD_NATIVE);
		keyWordsList.add(Constants.KEY_WORD_NEW);
		keyWordsList.add(Constants.KEY_WORD_NULL);
		keyWordsList.add(Constants.KEY_WORD_PACKAGE);
		keyWordsList.add(Constants.KEY_WORD_PRIVATE);
		keyWordsList.add(Constants.KEY_WORD_PROTECTED);
		keyWordsList.add(Constants.KEY_WORD_PUBLIC);
		keyWordsList.add(Constants.KEY_WORD_RETURN);
		keyWordsList.add(Constants.KEY_WORD_SHORT);
		keyWordsList.add(Constants.KEY_WORD_STATIC);
		keyWordsList.add(Constants.KEY_WORD_STRING);
		keyWordsList.add(Constants.KEY_WORD_SUPER);
		keyWordsList.add(Constants.KEY_WORD_SWITCH);
		keyWordsList.add(Constants.KEY_WORD_SYNCHRONIZED);
		keyWordsList.add(Constants.KEY_WORD_THIS);
		keyWordsList.add(Constants.KEY_WORD_THROW);
		keyWordsList.add(Constants.KEY_WORD_THROWS);
		keyWordsList.add(Constants.KEY_WORD_TRANSIENT);
		keyWordsList.add(Constants.KEY_WORD_TRUE);
		keyWordsList.add(Constants.KEY_WORD_TRY);
		keyWordsList.add(Constants.KEY_WORD_VOID);
		keyWordsList.add(Constants.KEY_WORD_VOLATILE);
		keyWordsList.add(Constants.KEY_WORD_WHILE);

		return keyWordsList;
	}















	// Texty do Ohraničení Class diagramu (CD) a instance Diagramu (ID) v hlavním okne aplikace - třída App:
	public static final String CD_BORDER_TITLE = "Diagram tříd", ID_BORDER_TITLE = "Diagram instancí";








	// Třída WriteToFile, metoda: createNewProjectFolder
	// text do poznámkového bloku:
	public static final String TEXT_AT_LINE_1 = "Toto je soubor" + " " + READ_ME_FILE_NAME + ".\r\nZde stručně popiště Váš projekt.\r\nČtenáři, který nemusí o tomto projektu nic vědět, popište vše,\r\nco potřebuje znát, aby mohl Vaši aplikaci používat.";
	public static final String TEXT_AT_LINE_2 = "Tato úvodní pasáž ukončená následujícím řádkem pomlček je určena\r\npro autory - před předáním budoucím uživatelům ji odstraňte.\r\nStejně jako nepotřebné či nevyužité komentáře.";

    public static final String TEXT_AT_LINE_AUTHOR = "AUTOR";
    public static final String TEXT_AT_LINE_PROJECT_NAME = "NÁZEV PROJEKTU";
    public static final String TEXT_AT_LINE_PROJECT_PURPOSE = "CÍL PROJEKTU";
    public static final String TEXT_AT_LINE_HOW_TO_RUN_THE_PROJECT = "JAK PROJEKT SPUSTIT";
    public static final String TEXT_AT_LINE_STARTING_CLASSES = "SPOUŠTĚCÍ TŘÍDY";
    public static final String TEXT_AT_LINE_USER_INSTRUCTIONS = "INSTRUKCE PRO UŽIVATELE";
    public static final String TEXT_AT_LINE_USEFUL_ADDITIONAL_INFORMATION = "UŽITEČNÉ / DOPLŇUJÍCÍ INFORMACE";
    public static final String TEXT_AT_LINE_VERSION = "VERZE";
    public static final String TEXT_AT_LINE_DATE = "DATUM";














	// Text do třídy CompiledClassLoader pro známení, že se nepodařilo vytvořit
	// ClassLoader, resp. konkrétně nastala chyba pří
	// vytváření URL:
	public static String MCL_ERROR_WHILE_CREATING_URL = "Nastala chyba při vytváření komponenty ClassLoader, nelze načíst přeložené - zkompilované třídy. Pravděpodobně nastala chyba při konstrukci URL adresy k adresáři bin v aktuálně otevřeném projektu.";

	// Chyb při čtení třídy:
	public static final String MCL_ERROR_WHILE_LOADING_CLASS_FILE_1 = "Nastala chyba při načítání následující přeložené - zkompilované třídy",
			MCL_ERROR_WHILE_LOADING_CLASS_FILE_2 = "třída nebyla nalezena.";


	// Texty pro chybové hlášky, které mohou nastat při hledání souboru .class v
	// ClassLoaderu, ale s case sensitive názvy balíčků a souboru:
	public static final String MCL_TXT_PACKAGE_NOT_FOUND_1 = "Nebyl nalezen balíček",
			MCL_TXT_PACKAGE_NOT_FOUND_2 = "v adresáři", MCL_TXT_IN_PACKAGE = "V balíčku",
			MCL_TXT_FILE_NOT_FOUND = "Nebyl nalezen soubor";
















	// Konstanty do třídy (Dialogu) WorkspaceForm
	// Tlačítka:
	public static final String BTN_OK = "OK", BTN_CANCEL = "Zrušit", BTN_BROWSE = "Procházet";
	// Popisky - Labely:
	public static final String TITLE = "Pracovní prostor", LBL_SELECT_WORKSPACE = "Vyberte pracovní prostor",
			LBL_INFO = "Aplikace ukládá své projekty do složky s názvem Workspace.",
			LBL_CHOOSE = "Zvolte umístění složky Workspace pro tuto relaci.", LBL_WORKSPACE = "Pracovní prostor",
			CHCB_REMEMBER_PATH = "Použít tuto cestu jako výchozí a znovu se již nedotazovat.";

	// Chybové hlášky - JoptionPane:
	public static final String WORKSPACE_PATH_EMPTY_TITLE = "Cesta neuvedena",
			WORKSPACE_PATH_EMPTY_TEXT = "Cesta ke složce Workspace (k pracovnímu prostoru) není uvedena!",
			WORKSPACE_PATH_FORMAT_TITLE = "Chyba cesty",
			WORKSPACE_PATH_FORMAT_TEXT = "Cesta ke složce Workspace (k pracovnímu prostoru) je ve špatném formátu!";




	// Konstanty do třídy Menu (Balíček jMenu), veškeré txty pložek v menu:
    public static final String TXT_PROJECT = "Projekt", TXT_EDIT = "Editovat", TXT_TOOLS = "Nástroje",
            TXT_HELP = "Nápověda", TXT_NEW_PROJECT = "Nový", TXT_OPEN_PROJECT = "Otevřít",
            TXT_RECENTLY_OPENED_PROJECTS = "Nedávno otevřené", TXT_DELETE_RECENTLY_OPENED_PROJECTS = "Vymazat historii",
            TXT_RB_SORT_RECENTLY_OPENED_PROJECTS_ASC = "Vzestupné pořadí",
            TXT_RB_SORT_RECENTLY_OPENED_PROJECTS_DES = "Sestupné pořadí",
            TXT_CLOSE_PROJECT = "Zavřít", TXT_SAVE = "Uložit", TXT_SAVE_AS = "Uložit jako",
            TXT_SWITCH_WORKSPACE = "Změnit Workspace", TXT_RESTART_APP = "Restartovat", TXT_EXIT = "Zavřit",
            TXT_ADD_CLASS_FROM_FILE = "Přidat třídu ze souboru", TXT_REMOVE = "Odebrat",
            TXT_SAVE_IMAGE_OF_CLASS_DIAGRAM = "Uložit obrázek diagramu tříd",
            TXT_SAVE_IMAGE_OF_INSTANCE_DIAGRAM = "Uložit obrázek diagramu instancí",
			TXT_PRINT_IMAGE_OF_CLASS_DIAGRAM = "Vytisknout obrázek diagramu tříd",
			TXT_PRINT_IMAGE_OF_INSTANCE_DIAGRAM = "Vytisknout obrázek diagramu instnací",
			TXT_REFRESH_CLASS_DIAGRAM = "Aktualizovat diagram tříd",
			TXT_REFRESH_INSTANCE_DIAGRAM = "Aktualizovat diagram instancí", TXT_REMOVE_ALL = "Odebrat vše",
            TXT_REMOVE_INSTANCE_DIAGRAM = "Odebrat instance", TXT_COMPILE = "Zkompilovat",
            TXT_OPEN_CLASS = "V editoru kódu", TXT_OPEN_IN_EDITOR = "Otevřít soubor",
            TXT_OPEN_CLASS_IN_PROJECT_EXPLORER = "V průzkumníku projektů",
            TXT_SHOW_OUTPUT_TERMINAL = "Výstupní terminál", TXT_START_CALL_THE_MAIN_METHOD = "Spustit",
            TXT_SETTINGS = "Nastavení", TXT_FONT_SIZE = "Velikost písma", TXT_JAVA = "Java",
            TXT_JAVA_SETTINGS = "Nastavení", TXT_JAVA_SEARCH = "Vyhledat", TXT_PROJECT_EXPLORER = "Průzkumník projektů",
            TXT_OPEN_IN_FILE_EXPLORER = "Průzkumník souborů", TXT_PROJECT_EXPLORER_PROJECT = "Otevřený projekt",
            TXT_PROJECT_EXPLORER_WORKSPACE = "Adresář Workspace", TXT_FILE_EXPLORER_WORKSPACE = "Adresář Workspace",
            TXT_FILE_EXPLORER_PROJECT = "Otevřený projekt", TXT_INFO = "Info", TXT_ABOUT_APP = "O aplikaci";


	// Konstanty pro popisky jednotlivých položek menu - Tool tip - jejich texty:
    public static final String TT_NEW_PROJECT = "Otevře dialogové okno pro založení nového projektu.",
            TT_OPEN_PROJECT = "Otevře se dialogové okno pro výběr projektu pro otevření.",
            TT_DELETE_RECENTLY_OPENED_PROJECTS = "Vymaže historii dříve otevřených projektů.",
            TT_RB_SORT_RECENTLY_OPENED_PROJECTS_ASC = "Seřadí seznam dostupných dříve otevřených projektů vzestupně dle data otevření.",
            TT_RB_SORT_RECENTLY_OPENED_PROJECTS_DES = "Seřadí seznam dostupných dříve otevřených projektů sestupně dle data otevření.",
            TT_CLOSE_PROJECT = "Zavře se aktuálně otevřený projekt.",
            TT_SAVE = "Uloži se provedené změny aktuálně otevřeného projektu.",
            TT_SAVE_AS = "Otevře se dialogové okno pro změnu místa uložení aktuálně otevřeného projektu.",
            TT_SWITCH_WORKSPACE = "Otevře okno pro výběr nové, resp. jiné složky označené jako workspace.",
            TT_RESTART_APP = "Restartování aplikace (nemusí být podporován všemi JVM).",
			TT_EXIT = "Ukončení aplikace.",
			TT_ADD_CLASS_FROM_FILE = "Otevře se dialogové okno pro výběr javovské třídy pro načtení do aktuálně otevřeného projektu.",
			TT_REMOVE = "Odebere se z diagramu tříd jeden označený objekt (vždy pouze jeden označený objekt).",
			TT_SAVE_IMAGE_OF_CLASS_DIAGRAM = "Otevře se dialogové okno pro výběr místa uložení Print Screenu 'diagramu tříd'.",
			TT_SAVE_IMAGE_OF_INSTANCE_DIAGRAM = "Otevře se dialogové okno pro výběr místa uložení Print Screenu 'diagramu instancí'.",
			TT_PRINT_IMAGE_OF_CLASS_DIAGRAM = "Otevře se dialogové okno pro výběr nastavení vlastností (tiskárna, počet kopií, ...) pro vytisknutí Print Screenu 'diagramu tříd'.",
			TT_PRINT_IMAGE_OF_INSTANCE_DIAGRAM = "Otevře se dialogové okno pro výběr nastavení vlastností (tiskárna, počet kopií, ...) pro vytisknutí Print Screenu 'diagramu instancí'.",
			TT_REFRESH_CLASS_DIAGRAM = "Aktualizace diagramu tříd, tj. prohledá se adresář src v otevřeném projektu a vloží se do diagramu tříd třídy, které se v tomto adresáři najdou, a nebudou v diagramu, dále se znovu otestují vztahy mezi jednotlivými třídami.",
			TT_REFRESH_INSTANCE_DIAGRAM = "Aktualizuje se diagram instancí, tj. že se zkontrolují veškeré vztahy mezi vytvořenými instancemi.",
			TT_REMOVE_ALL = "Odeberou se veškeré objekty v diagramu tříd. (Vymaže se obsah adresáře src v adresáři projektu.)",
			TT_REMOVE_INSTANCE_DIAGRAM = "Vymažou se veškeré instance tříd z diagramu tříd, které byly vytvořeny a vymaže se diagram instancí.",
			TT_OPEN_CLASS = "Otevře dialog pro výběr textového souboru nebo Javovské třídy, která se otevře v editoru zdrojového kódu.",
			TT_OPEN_FILE_IN_PROJECT_EXPLORER = "Otevře dialogové okno pro výběr textové souboru nebo Javovské třídy, která se otevře v dialogu průzkumník projektů s kořenovým adresářem ve workspace.",
			TT_SHOW_OUTPUT_TERMINAL = "Zobrazí okno s výstupy ze zdrojového kódu (okno Výstupní terminál).",
			TT_COMPILE = "Zkompilují se veškeré třídy v aktuálně otevřeném projektu.",
			TT_START_CALL_THE_MAIN_METHOD = "Spuštění projektu. 'Prohledají' se zdrojové kódy tříd v diagramu tříd za účelem nalezení metody main, pokud bude tato metoda nelezena, zavolá se. V opačném případě bude vypsáno hlášení o nenalezení metody main. Note: Pokud se v projektu bude nacházet více tříd s příslušnou metodu, bude zavolána pouze první nalezená metoda, tomu lze předejít kliknutím pravým tlačítkem myši na příslušnou třídu v diagramu tříd s metodou main, kterou chcete spustit, a kliknout na položku spustit. Instance tříd po zavolání této metody nebudou zobrazeny v diagramu instancí.",
			TT_SETTINGS = "Otevře se dialogové okno pro editaci nastavení aplikace.",
			TT_FONT_SIZE = "Otevře dialog pro nastavení velikosti písma objektů v diagramu tříd a v diagramu instancí.",
			TT_JAVA = "Otevře se dialogové okno pro nastavení 'Domovského' adresáře pro Javu a důležité soubory pro kompilaci.",
			TT_ITEM_JAVA_SEARCH = "Zkusí se vyhledat potřebné soubory pro kompilování tříd a zkopírovat jej na následující cestu",
			TT_PROJECT_EXPLORER_WORKSPACE = "Otevře se dialogové okno pro práci se soubory v adresáři zvolený jako Workspace.",
			TT_PROJECT_EXPLORER_PROJECT = "Otevře se dialogové okno pro práci se soubory v adresáři v otevřeném projektu.",
            TT_FILE_EXPLORER_WORKSPACE = "Otevře adresář zvolený jako Workspace v průzkumníku souborů nastaveném v OS.",
            TT_FILE_EXPLORER_OPEN_PROJECT = "Otevře adresář otevřeného projektu v průzkumníku souborů nastaveném v OS.",
            TT_INFO = "Otevře se dialogové okno s informacemi o aplikaci.",
            TT_ABOUT_APP = "Otevře se dialogové okno s informacemi o autorovi aplikace.";


	// Konstanty do chybových hlášek ve třídě WriteToFile.java v metodě: removeSelectedObject();
	public static final String /* MN_JOP_ASSOCIATE_ERROR_TITLE = "Chyba asociace" */ MN_JOP_ASSOCIATE_ERROR_TITLE = "Chyba symetrické asociace",
//			MN_JOP_ASSOCIATE_ERROR_TEXT_JOP_1 = "Označená třída (buňka) je zdrojem vazby asociace, nelze smazat!",
			MN_JOP_ASSOCIATE_ERROR_TEXT_JOP_1 = "Označená třída (buňka) je zdrojem vztahu symetrické asociace, nelze smazat!",
//			MN_JOP_ASSOCIATE_ERROR_TEXT_JOP_2 = "Označená třída (buňka) je cílem vazby asociace, nelze smazat!",
			MN_JOP_ASSOCIATE_ERROR_TEXT_JOP_2 = "Označená třída (buňka) je cílem vztahu symetrické asociace, nelze smazat!",
			MN_JOP_INHERIT_ERROR_TITLE = "Chyba dědičnosti",
			MN_JOP_INHERIT_ERROR_TEXT = "Z označené třídy (buňky) nějaká třída dědí, nelze smazat!",
			MN_JOP_IMPLEMENTS_ERROR_TITLE = "Chyba implementace",
			MN_JOP_IMPLEMENTS_ERROR_TEXT = "Označená třída (buňka) je rozhraní, které nějaká třída implementuje, nelze smazat!",
			MN_JOP_AGGREGATION_1_1_ERROR_TITLE = "Chyba agregace",
//			MN_JOP_AGGREGATION_1_1_TEXT = "Označená třída (buňka) je ve vztahu 'agregace 1 : 1' s jinou třídou (buňkou) (Nějaká třída má referenci na tuto označenou třídu.), nelze smazat!",
			MN_JOP_AGGREGATION_1_1_TEXT = "Označená třída (buňka) je ve vztahu 'asymetrická asociace' s jinou třídou (buňkou) (Nějaká třída má referenci na tuto označenou třídu.), nelze smazat!",
			MN_JOP_AGGREGATION_1_N_ERROR_TEXT = "Označená třída (buňka) je ve vztahu 'agregace 1 : N' s jinou třídou (buňkou) (Nějaká třída má referenci na tuto označenou třídu.), nelze smazat!",
			MN_JOP_SELECTED_OBJ_ERROR_TEXT = "Není označen objekt pro smazání!",
			MN_JOP_SELECTED_OBJ_ERROR_TITLE = "Chyba operace smazat";


	// Chybové hlášky po kliknutí na položku v menu:
	public static final String MN_JOP_MISSING_DIRECTORY_ERROR_TEXT = "Adresář nebyl nalezen !\nCesta",
			MN_JOP_MISSING_DIRECTORY_ERROR_TITLE = "Chyba cesty", MN_JOP_IDENTICAL_DIR_ERROR_TITLE = "Chyba umístění",
			MN_JOP_IDENTICAL_DIR_ERROR_TEXT = "Adresáře jsou identické, vyberte prosím jiný!",
			MN_JOP_MISSING_CLASS_FILE_TITLE = "Chybí soubor",
			MN_JOP_MISSING_CLASS_FILE_TEXT = "Soubor neexistuje !\nCesta";


	// Texty coby chyby při kompilaci a pokusu o spuštění třídy pomocí zavolání metody main v označené třídě
	// nebo nalezení či nenalazení metody main v třídě z diagramu tříd:
	public static final String MN_TXT_MISSING_SRC_DIR_TEXT = "Nebyl nalezen adresář src a nebo adresář bin v adresáři projektu!",
			MN_TXT_MISSING_SRC_DIR_TITLE = "Adresář nenalezen",
			MN_TXT_ERROR_WHILE_CALL_THE_MAIN_METHOD = "Při zavolání metody main nastala chyba!", MN_TXT_CLASS = "Třída",
			MN_TXT_ERROR = "Chyba", MN_TXT_METHOD_IS_NOT_ACCESSIBLE = "Metoda není přístupná!",
			MN_TXT_ILLEGAL_ARGUMENT_EXCEPTION_POSSIBLE_ERRORS = "Objekt není instancí třídy nebo rozhraní. Liši se počet parametrů. Selhala konverze parametrů, apod.",
			MN_TXT_ERROR_WHILE_CALLING_MAIN_METHOD = "Metoda vyvolala výjimku , neznámá chyba.",
			MN_TXT_MAIN_METHOD_NOT_FOUND_TEXT = "Metoda main nebyla v žádné z tříd v diagramu tříd nalezena!",
			MN_TXT_MAIN_METHOD_NOT_FOUND_TITLE = "Metoda nenalezena",
			MN_TXT_CHOOSE_CLASS_OR_TEXT_DOC_DIALOG_TITLE = "Vybrat Javovskou třídu nebo textový dokument";


	public static final String MN_TXT_ERROR_WHILE_PRINTING_CLASS_DIAGRAM_TEXT = "Nastala chyba při pokusu o vytisknutí diagramu tříd!",
			MN_TXT_ERROR_WHILE_PRINTING_CLASS_DIAGRAM_TITLE = "Chyba tisku",
			MN_TXT_FAILED_TO_PRINT_CLASS_DIAGRAM_TEXT = "Nepodařilo se vytisknout Print Screen diagramu tříd!",
			MN_TXT_CLASS_DIAGRAM = "Diagram tříd", MN_TXT_FAILED_TO_PRINT_CLASS_DIAGRAM_TITLE = "Chyba tisku",
			MN_TXT_ERROR_WHILE_PRINTING_INSTANCE_DIAGRAM_TEXT = "Nastala chyba při pokusu o vytisknutí diagramu instancí!",
			MN_TXT_ERROR_WHILE_PRINTING_INSTANCE_DIAGRAM_TITLE = "Chyba tisku",
			MN_TXT_FAILED_TO_PRINT_INSTANCE_DIAGRAM_TEXT = "Nepodařilo se vytisknout PrintScreen diagramu instancí!",
			MN_TXT_INSTANCE_DIAGRAM = "Diagram instancí", MN_TXT_FAILED_TO_PRINT_INSTANCE_DIAGRAM_TITLE = "Chyba tisku",
			MN_TXT_ERROR_WHILE_COPY_FILES_TEXT = "Nastala chyba při kopírování souborů",
			MN_TXT_POSSIBLE_FILE_IS_BEING_USED = "Je možné, že soubor na cílovém umístění již existuje a je využíván.",
			MN_TXT_SOURCE_TEXT = "Zdroj", MN_TXT_DESTINATION_TEXT = "Cíl",
			MN_TXT_ERROR_WHILE_COPY_FILES_TITLE = "Chyba při kopírování";

	// Menu - položka pro ProjectExplorerDialog - v menu pro otevreni:
	public static final String /*MN_TXT_DIR_AS_WORKSPACE_MISSING_TEXT = "Adresář zvolený jako workspace (pracovní prostor) neexituje na zadané cestě, nebo se nejedná o adresář, ale soubor!",*/
			/*MN_TXT_DIR_AS_WORKSPACE_MISSING_TITLE = "Nenalezen workspace",*/
			MN_TXT_PATH_TO_WORKSPACE_DO_NOT_FOUND_TEXT = "Cesta k adresáři workspace nebyla nalezena, nebo příslušný adresář neexistuje.",
			MN_TXT_PATH_TO_WORKSPACE_DO_NOT_FOUND_TITLE = "Chyba workspace",
			MN_TXT_DEFAULT_PROP_IN_APP_DO_NOT_LOAD_TEXT = "Selhalo načtení výchozího souboru default.properties z adresáře aplikace!",
			MN_TXT_DEFAULT_PROP_IN_APP_DO_NOT_LOAD_TITLE = "Selhalo načtení";

	// Menu - chybové hlášky:
	public static final String MN_TXT_OPEN_DIR_DOES_NOT_EXIST_TEXT = "Adresář otevřeného projektu již neexistuje!",
			MN_TXT_ORIGINAL_LOCATION = "Původní umístění",
			MN_TXT_OPEN_DIR_DOES_NOT_EXIST_TITLE = "Adresář projektu neexistuje",
			MN_TXT_PROJECT_IS_NOT_OPEN_TEXT = "Není otevřen žádný projekt!",
			MN_TXT_PROJECT_IS_NOT_OPEN_TITLE = "Projekt zavřen",
			MN_TXT_CLASS_DIAGRAM_DOESNT_CONTAIN_CLASS_TITLE = "Chybí třída",
			MN_TXT_CLASS_DIAGRAM_DOESNT_CONTAIN_CLASS_TEXT = "Diagram tříd neobsahuje žádnou třídu ke kompilaci!";


	// Dotaz na restart aplikace při změně workspace na OS Win:
	public static final String MN_TXT_SWITCH_WORKSPACE_JOP_RESTART_APP_NOW_TEXT = "Aby se projevily změny, je třeba restartovat aplikaci. Přejete si restartovat aplikaci nyní?",
			MN_TXT_SWITCH_WORKSPACE_JOP_RESTART_APP_NOW_TT = "Restartovat aplikaci";

    // Hlášky pro nenalezené adresáře workspace nebo adresáře otevřeného projektu, když chce uživatel otevřít tyto
    // adresáře v průzkumníku souborů:
    public static final String MN_TXT_PATH_TO_WORKSPACE_WAS_NOT_FOUND_TEXT = "Nepodařilo se nalézt cestu k adresáři workspace.",
            MN_TXT_PATH_TO_WORKSPACE_WAS_NOT_FOUND_TITLE = "Workspace nebyl nalezen",
    MN_TXT_PATH_TO_OPEN_PROJECT_WAS_NOT_FOUND_TEXT = "Nepodařilo se nalézt cestu k otevřenému projektu.",
    MN_TXT_PATH_TO_OPEN_PROJECT_WAS_NOT_FOUND_TITLE = "Otevřený projekt nebyl nalezen";







	// Konstanty do třídy ReadFile.java (balíček file)


	// Text do metody: getSelectLanguage() - výpis, že nebyl nalezen soubor s požadovaným jazykem:
	// jop = JoptionPane
	public static final String JOP_ERROR_LANGUAGE_TITLE = "Chyba jazyka",
			JOP_ERROR_LANGUAGE_1 = "Nebyl nalezen soubor s potřebným jazykem",
			JOP_ERROR_LANGUAGE_2 = "\nZvolen výchozí jazyk aplikace a byly překopírovány veškeré jazyky do složky, zvolené jako Workspace!\nAby se projevily změny, restartujte aplikaci.";
	public static final String JOP_MISSING_FILE_ERROR_TITLE = "Chyba souboru",
			JOP_MISSING_FILE_ERROR_TEXT = "Soubor nebyl nalezen !\nCesta";





	// Texty pro chybovou hlášku, která může nastat v případě, že se nepodaří na
	// první pokus nepodaří při spuštění aplikace načíst potřebný soubor, který
	// obsahuje texty pro tuto aplikaci ve zvoleném jazyce, pak se soubor
	// ypřekopírují do adresářeč confugration a znovu načtou:
	public static final String JOP_ERROR_LOAD_LANGUAGE_TEXT_1 = "První pokus o načtení souboru s texty pro aplikaci ve zvoleném jazyce selhal, proto byly soubory",
			JOP_ERROR_LOAD_LANGUAGE_TEXT_2 = "s texty pro tuto aplikaci překopírovány na následující umístění a znovu načteny.",
			JOP_ERROR_LOAD_LANGUAGE_TEXT_3 = "Umístění", JOP_ERROR_LOAD_LANGUAGE_TITLE = "Chyba jazyka";













	// Minimální a maximální hodnota pro velikost písma v diagramu tříd a v diagramu
	// instnací - pro jednotlivé objekty.
	public static final int FONT_SIZE_MIN = 8, FONT_SIZE_MAX = 150;




	// Výchozí nastavení pro Class diagram (třída Graph.java) pro případ, že by selhalo čtení nastavení ze souboru:
	// CD - inicialy Class diagram:
	// Hodnoty pro buňky, které reprezentují třídu:
	public static final Font CD_FONT_CLASS = new Font("Default font for class cell", Font.BOLD, 14);
	public static final int CD_VERTICAL_ALIGNMENT = SwingConstants.CENTER,
			CD_HORIZONTAL_ALIGNMENT = SwingUtilities.CENTER;


	public static final Color CD_TEXT_COLOR = Color.BLACK, CD_CLASS_BACKGROUND_COLOR = Color.WHITE,
			CD_GRADIENT_COLOR = new Color(0xCC0000), CD_ONE_BG_COLOR = new Color(245, 217, 185);

	public static final boolean CD_OPAQUE_CLASS_CELL = false, CD_ONE_BG_COLOR_BOOLEAN = true;
	public static final double CLASS_DIAGRAM_SCALE = 1d;


	/**
	 * Proměnná, která značí, zda se má v kontextovém menu nad třídou v diagramu tříd v menu "Zděděné statické metody"
	 * zobrazovat třídy i s balíčky, ve kterých se nahází nebo ne (pouze název třídy).
	 */
	public static final boolean CD_SHOW_STATIC_INHERITED_CLASS_NAME_WITH_PACKAGES = false;

	/**
	 * Logická proměnná, která značí, zda se v kontextovém menu nad třídou v diagramu tříd zobzraí i soukromé
	 * konstruktory pro zavolání. True - zpřístupní se soukromé konstruktory, false - nebudou zpřístupněny soukromé
	 * konstruktory.
	 */
	public static final boolean CD_INCLUDE_PRIVATE_CONSTRUCTORS = false;

	/**
	 * Logická proměnná, která značí, zda se v kontextovém menu nad třídou v diagramu tříd zobzraí i chráněné
	 * konstruktory pro zavolání. True - zpřístupní se chráněné konstruktory, false - nebudou zpřístupněny chráněné
	 * konstruktory.
	 */
	public static final boolean CD_INCLUDE_PROTECTED_CONSTRUCTORS = false;

	/**
	 * Logická proměnná, která značí, zda se v kontextovém menu nad třídou v diagramu tříd zobzraí i package-private
	 * konstruktory pro zavolání. True - zpřístupní se package-private konstruktory, false - nebudou zpřístupněny
	 * package-private konstruktory.
	 */
	public static final boolean CD_INCLUDE_PACKAGE_PRIVATE_CONSTRUCTORS = false;

	/**
	 * Logická proměnná, která značí, zda se mají v kontextovém menu nad třídou v diagramu tříd v nabídce "Vnitřní
	 * statické třídy" zobrazit i takové vnitřní statické třídy, které jsou označené viditelností private.
	 */
	public static final boolean CD_INCLUDE_PRIVATE_INNER_STATIC_CLASSES = false;

	/**
	 * Logická proměnná, která značí, zda se mají v kontextovém menu nad třídou v diagramu tříd v nabídce "Vnitřní
	 * statické třídy" zobrazit i takové vnitřní statické třídy, které jsou označené viditelností protected.
	 */
	public static final boolean CD_INCLUDE_PROTECTED_INNER_STATIC_CLASSES = false;

	/**
	 * Logická proměnná, která značí, zda se mají v kontextovém menu nad třídou v diagramu tříd v nabídce "Vnitřní
	 * statické třídy" zobrazit i takové vnitřní statické třídy, které jsou označené viditelností package-private.
	 */
	public static final boolean CD_INCLUDE_PACKAGE_PRIVATE_INNER_STATIC_CLASSES = false;

	/**
	 * Logická proměnná, která značí, zda se mají v kontextovém menu nad třídou v diagramu tříd v nabídce "Vnitřní
	 * statické třídy" zobrazit k jednotlivým vnitřním třídám i jejich statické soukromé metody.
	 */
	public static final boolean CD_INNER_STATIC_CLASSES_INCLUDE_PRIVATE_STATIC_METHODS = false;

	/**
	 * Logická proměnná, která značí, zda se mají v kontextovém menu nad třídou v diagramu tříd v nabídce "Vnitřní
	 * statické třídy" zobrazit k jednotlivým vnitřním třídám i jejich statické chráněné metody.
	 */
	public static final boolean CD_INNER_STATIC_CLASSES_INCLUDE_PROTECTED_STATIC_METHODS = false;

	/**
	 * Logická proměnná, která značí, zda se mají v kontextovém menu nad třídou v diagramu tříd v nabídce "Vnitřní
	 * statické třídy" zobrazit k jednotlivým vnitřním třídám i jejich statické package-private metody.
	 */
	public static final boolean CD_INNER_STATIC_CLASSES_INCLUDE_PACKAGE_PRIVATE_STATIC_METHODS = false;

	/**
	 * Logická proměnná, která značí, zda se zpřístupní v kontextovém menu nad
	 * třídou v diagramu tříd i metody označené třídy, které jsou privátní, true
	 * zpřístupní, false nebudou zpřístupněny soukromé metody.
	 */
	public static final boolean CD_INCLUDE_PRIVATE_METHODS = false;

	/**
	 *Logická proměnná, která značí, zda se zpřístupní v kontextovém menu nad třídou v diagramu tříd i metody označené
	 *  třídy, které jsou označené viditelností protected, true - zpřístupní se, false - nebudou přístupní chráněné
	 *  metody.
	 */
	public static final boolean CD_INCLUDE_PROTECTED_METHODS = false;

	/**
	 * Logická proměnná, která značí, zda se mají v kontextovém menu nad třídou v diagramu tříd zpřístupnit její
	 * metody označené package-private viditeností. True - mají se přidat do menu metody s viditelností pro třídy v
	 * rámci balíčku, false - nemají se do kontextového menu přidat takové metody.
	 */
	public static final boolean CD_INCLUDE_PACKAGE_PRIVATE_METHODS = false;

	/**
	 * Logická proměnná, která značí, že se mají zpřístupnit veřejné a chráněné
	 * proměnné z tříd v diagramu tříd, z instancí, které se nachází v diagramu
	 * instancí a proměnných, které uživatel vytvořil v editoru příkazů. Vždy se
	 * bude jednat o proměnnou stejného datového typu, jako je příslušný parametr
	 * konstruktoru. Ale jsou rozlišovány jen některé typy proměnných, například
	 * list, kolekce 1 a 2 - rozměrné pole Javovských datových typů pro číslo, znak,
	 * boolean a text a to samé pro klasické proměnné uvedených datových typů a
	 * těchto typů musí být u listy a kolekce.
	 */
	public static final boolean CD_MAKE_FIELDS_AVAILABLE_FOR_CALL_CONSTRUCTOR = true;

	/**
	 * Logická proměnná, která značí, zda se mají zpřístupnit veřejné a chráněné
	 * proměnné z tříd v diagramu tříd, z instancí v digramu instancí a proměnných
	 * vytvořených uživatelem v editoru příkazů. Vždy sejedná o hodnotu stejného
	 * datového typu jako je příslušný parametr metody.
	 */
	public static final boolean CD_MAKE_FIELDS_AVAILABLE_FOR_CALL_METHOD = true;


	// Barva pozadí Class diagramu - grafu:
	public static final Color CD_BACKGROUND_COLOR = new Color(0xe4e4e4);

	// Hodnoty, které v Class diagramu - grafu reprezentují buňku komentáře:
	public static final Font CD_FONT_COMMENT = new Font("Default font for comment cell", Font.ITALIC, 13);
	// COM = comment
	public static final int CD_COM_VERTICAL_ALIGNMENT = SwingConstants.CENTER,
			CD_COM_HORIZONTAL_ALIGNMENT = SwingConstants.CENTER;

	public static final Color CD_COM_FOREGROUND_COLOR = Color.BLACK, CD_COM_BACKGROUND_COLOR = new Color(0xF7FF00),
			CD_COM_GRADIENT_COLOR = new Color(0xFF7700), CD_COM_ONE_BG_COLOR = new Color(0xFF7700);

	public static final boolean CD_OPAQUE_COMMENT_CELL = false, CD_COM_ONE_BG_COLOR_BOOLEAN = false;

	// Výchozí text do chybové hlášky
	public static final String CD_CELL_ERROR_TITLE1 = "Chyba třídy (buňky)",
			CD_CELL_ERROR_TEXT1 = "Označená třída (buňka) nebyla nalezena!",
			CD_CELL_ERROR_TEXT2 = "Není označena třída (buňka)!", CD_CELL_ERROR_TITLE2 = "Chyba třídy (buňky)",
			CD_CELL_ERROR_TITLE3 = "Chyba tříd (buňek)",
			CD_CELL_ERROR_TEXT3 = "Označená třída (buňka) nebo třídy (buňky) nebyly nalezeny!",
			CD_CELL_ERROR_TITLE4 = "Chyba třídy (buňky)", CD_CELL_ERROR_TEXT4 = "Nejsou označeny třídy (buňky)!",
			CD_CELL_ERROR_TITLE5 = "Chyba tříd (buňek)",
			CD_CELL_ERROR_TEXT5 = "Označená třída (buňka) nebo třídy (buňky) nebyly nalezeny!",
			CD_CELL_ERROR_TITLE6 = "Chyba třídy (buňky)", CD_CELL_ERROR_TEXT6 = "Nejsou označeny třídy (buňky)!",
			CD_CELL_ERROR_TITLE7 = "Chyba tříd (buňek)",
			CD_CELL_ERROR_TEXT7 = "Označená třída (buňka) nebo třídy (buňky) nebyly nalezeny!",
			CD_CELL_ERROR_TITLE8 = "Chyba třídy (buňky)", CD_CELL_ERROR_TEXT8 = "Nejsou označeny třídy (buňky)!",
			CD_CELL_ERROR_TITLE9 = "Chyba třídy (buňky)",
			CD_CELL_ERROR_TEXT9 = "Označená třída / třídy (buňka / ky) nebyly nalezeny!",
			CD_CELL_ERROR_TITLE10 = "Chyba třídy (buňky)",
			CD_CELL_ERROR_TEXT10 = "Alespoň jeden označený objekt není třída (buňka)!",
			CD_EDGE_ERROR_TITLE = "Chyba vztahu", CD_EDGE_ERROR_TEXT = "Nelze vytvořit vztah mezi jednou třídou!",
			CD_MISSING_OBJECT_IN_GRAPH_TITLE = "Objekt nenalezen",
			CD_MISSING_OBJECT_IN_GRAPH_TEXT = "Objekt nebyl nalezen!";

	// Chybové hlášky pro implementaci rozhraní: - některé:
	public static final String CD_TXT_ERROR_WHILE_WRITING_INTERFACE_METHODS_TO_CLASS_TEXT = "Nepodařilo se zapsat metody z rozhraní do třídy",
			CD_TXT_ERROR_WHILE_WRITING_INTERFACE_METHODS_TO_CLASS_TT = "Chyba zápisu", CD_TXT_CLASS = "Třída",
			CD_TXT_ALREADY_IMPLEMENTS_INTERFACE = "již implementuje rozhraní",
			CD_TXT_ALREADY_IMPLEMENTS_INTERFACE_TITLE = "Rozhraní implementováno";


	// Parametry pro hranu komentáře:
	public static final Color CD_COM_EDGE_COLOR = Color.BLACK, CD_COM_EDGE_FONT_COLOR = Color.BLACK;
	public static final boolean CD_COM_LABEL_ALONG_EDGE = true, CD_COM_LABELS_MOVABLE = false,
			CD_COM_LINE_END_FILL = false, CD_COM_LINE_BEGIN_FILL = false;

	/**
	 * Proměnná, která značí, zda se mají zobrazovat v diagramu tříd vztahy třídy na
	 * sebe sama. True značí že ano, false značí, že se nebudou zobrazovag vztahy
	 * třídy na sebe sama.
	 */
	public static final boolean CD_SHOW_RELATION_SHIPS_TO_CLASS_ITSELF = true;

	/**
	 * Logická hodnota, která značí, zda se má v dialogu pro zavolání konstruktoru
	 * poomcí kontextového menu nad třídou zpřístupnit vytvoření nové instance v
	 * případě, že jako příslušný parametr konstruktoru bude třída, která se nachází
	 * v diagramu tříd. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že
	 * bude na výběr možnost pro vytvoření nové instance. Hodnota 'false' značí, že
	 * budou na výběr pouze instance příslušné třídy, které se nachází v diagramu
	 * instancí, pokud v diagramu instancí žádná instance příslušné třídy nebude,
	 * konstruktor nepůjde zavolat.
	 */
	public static final boolean CD_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_CONSTRUCTOR = true;

	/**
	 * Logická hodnota, která značí, zda se má v dialogu pro zavolání metody poomcí
	 * kontextového menu nad třídou zpřístupnit vytvoření nové instance v případě,
	 * že jako příslušný parametr metody bude třída, která se nachází v diagramu
	 * tříd. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že bude na
	 * výběr možnost pro vytvoření nové instance. Hodnota 'false' značí, že budou na
	 * výběr pouze instance příslušné třídy, které se nachází v diagramu instancí,
	 * pokud v diagramu instancí žádná instance příslušné třídy nebude, metoda
	 * nepůjde zavolat.
	 */
	public static final boolean CD_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD = true;

	/**
	 * Logická proměnná, která značí, zda se mají v dialogu pro zavolání statické
	 * metody nad třídou v diagramu tříd zobrazit i dostupné metody,které vracejí
	 * stejný datový typ, jako příslušný parametr metody.
	 *
	 * Pokud je tato proměnná true,k pak se mají zobrazit metody pro předání jejích
	 * návratového hodnoty do parametru příslušné metody.
	 */
	public static final boolean CD_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD = true;



	/**
	 * Logická proměnná, která značí, zda se mají v dialogu pro zavolání
	 * konstruktoru s paametry nabízet i metody, jejíž návratové hodnoty se mají
	 * předat do příslušného parametru konstruktoru (true). Jinak když bude tato
	 * hodnota false, pak se nebudou nabízet metody pro zavolání, aby se jejich
	 * návratová hodnota vložila do parametru konstruktoru.
	 */
	public static final boolean CD_MAKE_METHODS_AVAILABLE_FOR_CALL_CONSTRUCTOR = true;



	/**
	 * Logická Hodnota pro určení toho, zda se má vztah typu asociace v diagramu
	 * tříd znázornit klasicky jako jedna hrana s šipkami na obou koncích (hodnota
	 * 'false'). Nebo, zda se má vztah typu asociace v diagramu tříd znázornit jako
	 * dvě agregace 1 : 1 (hodnota 'true'). Povolené hodnoty: 'true', 'false'."
	 */
	public static final boolean CD_SHOW_ASSOCIATION_THROUGH_AGGREGATION = true;

	/**
	 * Proměnná, která značí, v jakém z editorů zdrojového kódu se má primárně
	 * otevřít zvolená třída v diagramu tříd.
	 *
	 * Tzn. Když uživatel označí třídu v diagramu tříd a stiskne Enter nebo na ni
	 * dvakrát klikne levým tlačítkem myši, tak se otevře ve zvoleném editoru
	 * zdrojového kódu - buď dialog Editor zdrojového kódu nebo dialog Průzkumník
	 * projektů.
	 */
	public static final KindOfCodeEditor CD_KIND_OF_DEFAULT_CODE_EDITOR = KindOfCodeEditor.SOURCE_CODE_EDITOR;

	/**
	 * Ligická proměnná, která značí, zda se při vytvoření vztahu implementace
	 * roznraní v diagramu tříd mají rovnou i vygenerovat příslušné metody do
	 * příslušné třídy.
	 */
	public static final boolean ADD_INTERFACE_METHODS_TO_CLASS = true;


	public static final int CD_COM_EDGE_LINE_STYLE = GraphConstants.STYLE_ORTHOGONAL,
			CD_COM_LINE_END = GraphConstants.ARROW_DOUBLELINE, CD_COM_LINE_BEGIN = GraphConstants.ARROW_NONE;

	public static final Routing CD_COM_EDGE_ROUTING = GraphConstants.ROUTING_SIMPLE;
	public static final float CD_COM_LINE_WIDTH = 1f;
	public static final Font CD_COM_EDGE_FONT = new Font("Default font for comment edge", Font.BOLD, 13);


	// Výchozí text do prvně vytvořené buňky coby komentář:
	public static final String CD_FIRST_TEXT_TO_COMMENT_CELL = "Vložte komentář";


	// Parametry pro hranu, která reprezentuje asociaci:
	// ASC = Asociace
	public static final Color CD_ASC_EDGE_COLOR = Color.BLACK, CD_ASC_EDGE_FONT_COLOR = Color.BLACK;
	public static final boolean CD_ASC_LABEL_ALONG_EDGE = true, CD_ASC_EDGE_END_FILL = false,
			CD_ASC_EDGE_BEGIN_FILL = false;

	public static final int CD_ASC_EDGE_LINE_STYLE = GraphConstants.STYLE_ORTHOGONAL,
			CD_ASC_EDGE_LINE_END = GraphConstants.ARROW_CLASSIC, CD_ASC_EDGE_LINE_BEGIN = GraphConstants.ARROW_CLASSIC;

	public static final float CD_ASC_EDGE_LINE_WIDTH = 1f;
	public static final Font CD_ASC_EDGE_LINE_FONT = new Font("Default font for comment edge", Font.BOLD, 13);


	// Parametry pro hranu, která reprezentuje dědičnost: (EXT = Extends)
	public static final Color CD_EXT_EDGE_COLOR = Color.BLACK, CD_EXT_EDGE_FONT_COLOR = Color.BLACK;
	public static final boolean CD_EXT_LABEL_ALONG_EDGE = true, CD_EXT_EDGE_END_FILL = false,
			CD_EXT_EDGE_BEGIN_FILL = false;

	public static final int CD_EXT_EDGE_LINE_STYLE = -1, CD_EXT_EDGE_LINE_END = GraphConstants.ARROW_TECHNICAL,
			CD_EXT_EDGE_LINE_BEGIN = GraphConstants.ARROW_NONE;

	public static final float CD_EXT_EDGE_LINE_WIDTH = 1f;
	public static final Font CD_EXT_EDGE_LINE_FONT = new Font("Default font for extends edge", Font.BOLD, 13);


	// Parametry pro hranu, která reprezentuje implementaci rozhraní: IMP = IMPLEMENT
	public static final Color CD_IMP_EDGE_COLOR = Color.BLACK, CD_IMP_EDGE_FONT_COLOR = Color.BLACK;
	public static final boolean CD_IMP_LABEL_ALONG_EDGE = true, CD_IMP_EDGE_END_FILL = false,
			CD_IMP_EDGE_BEGIN_FILL = false;

	public static final int CD_IMP_EDGE_LINE_STYLE = GraphConstants.STYLE_ORTHOGONAL,
			CD_IMP_EDGE_LINE_END = GraphConstants.ARROW_TECHNICAL, CD_IMP_EDGE_LINE_BEGIN = GraphConstants.ARROW_NONE;

	public static final float CD_IMP_EDGE_LINE_WIDTH = 1f;
	public static final Font CD_IMP_EDGE_LINE_FONT = new Font("Default font for implements edge", Font.BOLD, 13);


	// hrana, která reprezentuje implementaci rozhraní je čerchovaná, nebo "něco" v tom smyslu - nějakým způsobem přerušovaná čára - čárky, tečka čárka, ...
	// následující pole je výchozí nastavení designu hrany pro implementaci, uživatel si pak v nastavení bude moci zvolit jiný způsob:
	public static final float[] CD_IMP_PATTERN = {10f, 10f};


	// Parametry pro hranu, která reprezentuj agregaci (1 : 1):  (agr = agregace)
	public static final Color CD_AGR_EDGE_COLOR = Color.BLACK, CD_AGR_EDGE_FONT_COLOR = Color.BLACK;
	public static final boolean CD_AGR_LABEL_ALONG_EDGE = true, CD_AGR_EDGE_END_FILL = false,
			CD_AGR_EDGE_BEGIN_FILL = false;

	public static final int CD_AGR_EDGE_LINE_STYLE = GraphConstants.STYLE_ORTHOGONAL,
			CD_AGR_EDGE_LINE_END = GraphConstants.ARROW_CLASSIC, CD_AGR_EDGE_LINE_BEGIN = GraphConstants.ARROW_NONE;

	public static final float CD_AGR_EDGE_LINE_WIDTH = 1f;
	public static final Font CD_AGR_EDGE_LINE_FONT = new Font("Default font for aggregation 1 : 1 edge", Font.BOLD, 13);

	// Parametry pro hranu, která reprezentuj agregaci 1: N:  (agr = agregace)
	public static final Color CD_AGR_N_EDGE_COLOR = Color.BLACK, CD_AGR_N_EDGE_FONT_COLOR = Color.BLACK;
	public static final boolean CD_AGR_N_LABEL_ALONG_EDGE = true, CD_AGR_N_EDGE_END_FILL = false,
			CD_AGR_N_EDGE_BEGIN_FILL = false;

	public static final int CD_AGR_EDGE_N_LINE_STYLE = GraphConstants.STYLE_ORTHOGONAL,
			CD_AGR_N_EDGE_LINE_END = GraphConstants.ARROW_NONE, CD_AGR_N_EDGE_LINE_BEGIN = GraphConstants.ARROW_DIAMOND;

	public static final float CD_AGR_EDGE_N_LINE_WIDTH = 1f;
	public static final Font CD_AGR_EDGE_N_LINE_FONT = new Font("Defautl font for aggregation 1 : N edge", Font.BOLD,
			13);



















	// Výchozí konstanty pro Instance diagram: (třída Graph v balíčku instance diagram(
	// Pro výchozí nastavení: (ID = InstanceDiagram)
	public static final boolean ID_EDGE_LABELS_MOVABLE = false, ID_SHOW_PACKAGES_IN_CELL = false;

	/**
	 * Logická proměnná, která značí, zda se v kontextovém menu nad instancí v diagramu instancí v položce "Zděděné
	 * metody" mají zobrazit třídy (předkové) jako názvy tříd s balíčky nebo bez nich.
	 */
	public static final boolean ID_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES = false;

	/**
	 * Logická proměnná o tom, zda se mají v kontextovém menu nad označenou instancí
	 * v diagramu instancí zpřístupnit pro zavolání i metody, které jsou privátní.
	 */
	public static final boolean ID_INCLUDE_PRIVATE_METHODS = false;

	/**
	 * Logická proměnná o tom, zda se mají v kontextovém menu nad označenou instancí v diagramu instancí zpřístupnit
	 * pro zavolání i metody, které jsou soukromé.
	 */
	public static final boolean ID_INCLUDE_PROTECTED_METHODS = false;

	/**
	 * Logická proměnná o tom, zda se mají v kontextovém menu nad označenou instancí v diagramu instancí zpřístupnit
	 * pro
	 * zavolání i metody, které jsou s viditelností pouze v rámci balíčku (package-private).
	 */
	public static final boolean ID_INCLUDE_PACKAGE_PRIVATE_METHODS = false;

	/**
	 * Logická proměnná, která značí, zda se mají zpřístupnit veřejné a chráněné
	 * proměnné z tříd v diagramu tříd, z instancí v digramu instancí a proměnných
	 * vytvořených uživatelem v editoru příkazů. Vždy sejedná o hodnotu stejného
	 * datového typu jako je příslušný parametr metody.
	 */
	public static final boolean ID_MAKE_FIELDS_AVAILABLE_FOR_CALL_METHOD = true;


	/**
	 * Logická proměnná ohledně toho, zda se mají v diagramu instancí zobrazovat
	 * vztahy instancí na sebe sama nebo ne. True značí, že se budou zobrazovat
	 * vztahy instancí na sebe sama, false že nebudou.
	 */
	public static final boolean ID_SHOW_RELATION_SHIPS_TO_INSTANCE_ITSELF = true;

	/**
	 * Hodnota, která značí, zda se má v dialogu pro zavolání metody poomcí
	 * kontextového menu nad instancí zpřístupnit vytvoření nové instance v případě,
	 * že jako příslušný parametr metody bude třída, která se nachází v diagramu
	 * tříd. Povolené hodnoty: 'true', 'false'. Hodnota 'true' značí, že bude na
	 * výběr možnost pro vytvoření nové instance. Hodnota 'false' značí, že budou na
	 * výběr pouze instance příslušné třídy, které se nachází v diagramu instancí,
	 * pokud v diagramu instancí žádná instance příslušné třídy nebude, metoda
	 * nepůjde zavolat."
	 */
	public static final boolean ID_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD = true;

	/**
	 * Logická proměnná, která značí, zda se mají v dialogu pro zavolání metody nad
	 * instancí v diagramu instancí zobrazit i metody, které vracejí hodnotu
	 * stejného datového typu, jako je příslušný parametr metody.
	 *
	 * True značí, že se v dialogupro zavolání metody budou zobrazovat metody pro
	 * předání jejich návratového hodnoty do přslušného parametru metody, jinak ne.
	 */
	public static final boolean ID_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD = true;

	/**
	 * Logická hodnota pro určení toho, zda se mají použít algoritmy pro 'rychlou'
	 * nebo 'pomalejší' aktualizaci vztahů mezi instancemi v diagramu instancí.
	 * 'Rychlá' verze znamená, že se při každé aktualizaci vztahů mezi instancemi v
	 * diagramu instancí nejprve vymažou veškeré vztahy mezi všemi instancemi a
	 * následně se zjistí vztahy dle naplněných / nenaplněných testovaných
	 * proměnných a vytvoří se pouze takové vztahy mezi instancemi, které byly
	 * nalezeny. 'Pomalá' verze znamená, že se vždy zjistí vztahy mezi instancemi,
	 * které by měli aktuálně existovat v diagramu instancí a dle těchto zjištěných
	 * vztahů se v diagramu instancí některé vztahy pouze upraví tak, že se
	 * odeberou, přidají nebo ponechazí. Právě toto zjišťování aktuálních vztahů
	 * mezi instancemi v diagramu instancí je důvod, prooč se jedná o pomalejší
	 * způsob. Povolené hodnoty: 'true' a 'false'. Hodnota 'true' značí 'rychlou'
	 * verzi a hodnota 'false' značí 'pomalou' verzi zjišťování vztahů mezi
	 * instancemi v diagramu instancí.
	 */
	public static final boolean ID_USE_FAST_WAY_TO_REFRESH_RELATIONSHIPS_BETWEEN_CLASSES = true;

	/**
	 * logicá hodnota pro určení toho, zda se mají v diagramu instancí brát pro
	 * znázornění vztahů mezi intancemi (třídami) i hodnoty proměnných, které se
	 * nacházejí v předcích instance, která se nachází v diagramu instancí. Povolené
	 * hodnoty: 'true' a 'false'. Hodnota 'true' značí, že se budou v diagramu
	 * instancí znázorňovat vztahy, které jsou naplněny v proměnných, které se
	 * nacházejí v předcích příslušné instance v diagramu intancí. Hodnota 'false'
	 * značí, že se budou pro znázornění vztahů mezi instancemi v diagramu instancí
	 * brát pouze hodnoty proměnných v příslušné instanci v diagramu instancí.
	 */
	public static final boolean ID_SHOW_RELATIONSHIPS_IN_INHERITED_CLASSES = true;

	/**
	 * Logická hodnota pro určení toho, zda se má vztah typu asociace v diagramu
	 * instancí znázornit klasicky jako jedna hrana s šipkami na obou koncích
	 * (hodnota 'false'). Nebo, zda se má vztah typu asociace v diagramu instancí
	 * znázornit jako dvě agregace 1 : 1 (hodnota 'true'). Povolené hodnoty: 'true',
	 * 'false'."
	 */
	public static final boolean ID_SHOW_ASOCIATION_THROUGH_AGREGATION = true;

	/**
	 * Logická proměnná, která značí, zda se má v dialogu s přehledem instancí
	 * zobrazit i u instancí v proměnných jejich reference, kterou uživatel zadal
	 * při vytváření té instance v diagramu instancí.
	 *
	 * Naplněná proměnná v instanci, která je typu nějaké třídy z diagramu tříd, tak
	 * v diagramu instancí je reprezentována ta instance, která se nachází v nějaké
	 * proměnné v té instanci, tak u ní se budou zobrazovat ty reference, resp. ta
	 * zadaná reference na tu instanci při jejím vytvoření.
	 */
	public static final boolean ID_SHOW_REFERENCES_VARIABLES = true;

	/**
	 * Logická proměnná, která značí, zda se mají v dialogu pro zavolání nestatické
	 * metody nad instancí třídy v diagramu instancí, tak pokud se bude jednat o
	 * setr na proměnnou typu té třídy, kde se ten setr nachází, tak zda se budou
	 * zobrazovat i getr na tu proměnnou té třídy, kde se ten getr nachází, musí se
	 * jednat o stejnou třídu, kde se nachází setr a parametr toho setru.
	 *
	 * Hodnota true, značí, že se budou zobrazovat getry při zavolání setru na
	 * proměnnou typu třídy, kde se ta metoda - setr nachází. Jinak pokud bude tato
	 * hodnota false, pak se nebudou zobrazovat getr u zavolání setru na tu třídu,
	 * kde se ten setr nachází.
	 */
	public static final boolean ID_SHOW_GETTER_IN_SETTER_METHOD_IN_INSTANCE = true;

	public static final Color ID_BACKGROUND_COLOR = new Color(0xe4e4e4), ID_TEXT_COLOR = Color.WHITE,
			ID_COLOR_CELL = Color.RED, ID_COLOR_BORDER = Color.BLACK;

	// Výchozí hodnota pro zaoblené hrany u buňky, reprezentující instanci třídy:
	public static final int ID_ROUNDED_BORDER_MARGIN = 18;

	// Scale - přiblížení grafu:
	public static final double INSTANCE_DIAGRAM_SCALE = 1d;

	// Font buňky:
	public static final Font ID_FONT = new Font("Default font for instance cell", Font.BOLD, 14);

	// Horizontální a vertikální pozice textu v bu%nce:	(center a center)
	public static final int ID_VERTICAL_ALIGNMENT = 0, ID_HORIZONTAL_ALIGNMENT = 0;

	// Text do chybových hlášek pro smazání objektu z grafu:
	public static final String ID_MISSING_OBJECT_IN_GRAPH_TEXT = "Označená buňka instance třídy nebyla v diagramu nalezena!",
			ID_MISSING_OBJECT_IN_GRAPH_TITLE = "Chyba diagramu instancí";




    // Výchozí hodnoty pro atribu v buňce, která reprezentuje instanci v diagramu instancí:

    /**
     * Hodnota, která značí, zda se mají vůbec zobrazit atributy v buňce
	 * reprezentující instanci v diagramu instancí. Povolené hodnoty: 'true',
	 * 'false'. Hodnota 'true' značí, že se v buňce reprezentující instanci třídy z
	 * diagramu tříd budou zobrazovat její atributy (buď se zobrazí veškeré atributy
	 * nebo jen zvolený počet). Hodnota 'false' značí, že se nebudou zobrazovat
	 * atributy v příslušné buňce reprezentující instanci v diagramu instancí.
	 */
	public static final boolean ID_SHOW_ATTRIBUTES = false;

	/**
	 * Hodnota, která značí, zda se mají v buňce reprezentující instanci třídy z
	 * diagramu tříd (instance v diagramu instancí) zobrazit veškeré její atributy
	 * nebo jen zadaný počet. Povolené hodnoty: 'true', 'false'. Hodnota 'true'
     * značí, že se budou v příslušné buňce zobrazovat veškeré atributy, které
	 * senachází v příslušné instanci, kterou ta buňka v diagramu instancí
     * reprezentuje. Hodnota 'false' značí, že se vypíše pouze zadaná počet atributů
     * v příslušné instanci.
	 */
    public static final boolean ID_SHOW_ALL_ATTRIBUTES = false;

    /**
     * Výchozí hodnota výchozí počet atributů, který bude zobrazen v buňce
	 * reprezentující instanci příslušné třídy v diagramu instancí.
     */
    public static final int ID_SPECIFIC_COUNT_OF_ATTRIBUTES = 3;

    /**
     * Hodnota, která značí, zda se má pro text, kterým se vypisují atributy využít
	 * stejný font a barva písma jako je pro text s referencí. Povolené hodnoty:
	 * 'true', 'false'. Hodnota 'true' značí, že se má využít pro vypsání atributů
	 * do buňky / instance využít stejný font a barva písma, jako je pro text s
	 * referencí. Hodnota 'false' značí, že se pro výpis atributů využije 'zvlášť'
	 * nastavený font a barva písma.
	 */
	public static final boolean ID_USE_FOR_ATTRIBUTES_ONE_FONT_AND_FONT_COLOR_IN_INSTANCE_CELL = false;


	/**
	 * Výchozí font pro písmo, kterým se budou vypisovat atributy v buňce
	 * reprezentující instanci nějaké třídy z diagramu tříd v diagramu instancí.
     */
	public static final Font ID_ATTRIBUTES_FONT = new Font("Default font for attributes in instance cell.", Font.BOLD, 14);

	/**
	 * Výchozí barva pro písmo, kterým se budou vypisovat atributy v buňce
     * reprezentující instanci v diagramu instancí.
	 */
	public static final Color ID_ATTRIBUTES_TEXT_COLOR = Color.GREEN;


	// Následují hodnoty pro nastavení vlastnosti k tomu, zda se mají vypisovat
	// metody v buňce reprezentující instanci v diagramu instancí:

	/**
	 * Logická hodnota, která značí, zda se mají vypisovat metody v buňce
	 * reprezentující instanci v diagramu instancí.
	 */
	public static final boolean ID_SHOW_METHODS = false;

	/**
	 * Logická hodnota, která značí, zda se mají vypisovat veškeré metody v buňce
	 * repreentující instanci v diagramu instancí nebo ne. True značí že ano, pak se
	 * ale vypíšou úplně všechny metody v příslušné instanci, jinak se bude
	 * vypisovat jen zvolený počet metod (hodnota false).
	 */
	public static final boolean ID_SHOW_ALL_METHODS = false;

    /**
	 * Výchozí hodnota pro počet metod, které se mají zobrazit v buňce
	 * reprezentující instanci v diagramu instancí.
	 */
	public static final int ID_SPECIFIC_COUNT_OF_METHODS = 3;

	/**
	 * Logická hodnota, která značí, zda se má ve výchozím nastavení využít pro
	 * text, kterým se budou vypisovat metody v buňce reprezentující instanci v
	 * diagramu instancí, tak zda se pro tento text má využít stejná barva a font,
     * jako pro "klasickou" buňku, resp. jako pro text pro refrenci v buňce.
	 */
	public static final boolean ID_USE_FOR_METHODS_ONE_FONT_AND_FONT_COLOR_IN_INSTANCE_CELL = false;

	/**
	 * Výchozí font, který bude aplikován pro písmo, kterým se budou vypisovat
	 * metody v buňce reprezentující instanci v diagramu instancí.
	 */
	public static final Font ID_METHODS_FONT = new Font("Default font fot methods in instance cell.", Font.BOLD, 14);

	/**
	 * Výchozí barva, která se má aplikovat pro písmo, kterým se budou vypisovat
	 * metody v buňce reprezentující instanci v diagramu instancí.
	 */
	public static final Color ID_METHODS_TEXT_COLOR = Color.YELLOW;









    // Hodnoty pro zvýraznění instance v diagramu instnací (například po vrácení
	// instance jako návratovou hodnotu z metody nebo při získání instance z
	// proměnné v diagramu instancí apod.)

	/**
     * Logická proměnná, která značí výchozí hodnotu pro to, zda se mají v diagramu
	 * instancí zvýrazňovat instance (buňky) v případě, že například metoda vrtá
	 * instanci z diagramu instancí, nebo bude instanci v daigramu instancí získána
	 * z proměnné apod. Tak se zvýrazní v diagramu instancí, aby uživatel věděl, o
	 * jakou hodnotu jde.
     */
	public static final boolean ID_HIGHLIGHT_INSTANCES = true;

	/**
     * Proměnná, která obsahuje výchozí hodnotu, která značí čas / interval - jak
	 * dlouho bude buňka / instance v diagramu instancí zvýrazněná. V případě, že se
     * například vrátí z metody apod.
	 */
    public static final double ID_DEFAULT_HIGHLIGHTING_TIME = 1.0d;

    /**
	 * Barva textu / písma, které bude mít zvýrazněná instance v diagramu instancí.
     */
	public static final Color ID_TEXT_COLOR_OF_HIGHLIGHTED_INSTANCE = Color.BLACK;

	/**
	 * Barva textu / písma, které budou mít atributy ve zvýrazněné instanci.
     */
	public static final Color ID_ATTRIBUTE_COLOR_OF_HIGHLIGHTED_INSTANCE = Color.YELLOW;

	/**
	 * Barva textu / písma, které budou mít metody ve zvýrazněné instanci.
	 */
	public static final Color ID_METHOD_COLOR_OF_HIGHLIGHTED_INSTANCE = Color.YELLOW;

	/**
	 * Barva ohraničení, kterou bude mít příslušná buňka v daigramu instancí v
	 * případě, že bude zvýrazněná.
	 */
    public static final Color ID_BORDER_COLOR_OF_HIGHLIGHTED_INSTANCE = Color.YELLOW;

	/**
	 * Barva pozadí, které bude mít příslušná buňka v diagramu instancí v případě,
	 * že bude zvýrazněná.
	 */
	public static final Color ID_BACKGROUND_COLOR_OF_HIGHLIGHTED_INSTANCE = Color.GREEN;





	// Instance diagram, výchozí hodnoty pro hranu typu Asociace:
	public static final Color ID_ASSOCIATION_EDGE_COLOR = Color.BLACK, ID_ASSOCIATION_EDGE_FONT_COLOR = Color.BLACK;
	public static final boolean ID_ASSOCIATION_LABELS_ALONG_EDGE = true, ID_ASSOCIATION_EDGE_LINE_END_FILL = false,
			ID_ASSOCIATION_EDGE_LINE_BEGIN_FILL = false;

	public static final int ID_ASSOCIATION_EDGE_LINE_END = GraphConstants.ARROW_CLASSIC,
			ID_ASSOCIATION_EDGE_LINE_BEGIN = GraphConstants.ARROW_CLASSIC;

	public static final float ID_ASSOCIATION_EDGE_LINE_WIDTH = 1f;
	public static final Font ID_ASSOCIATION_EDGE_LINE_FONT = new Font("Default font for association edge", Font.BOLD,
			13);

	// Instance diagram, výchozí hodnoty pro hranu typu Agregace:
	public static final Color ID_AGGREGATION_EDGE_COLOR = Color.BLACK, ID_AGGREGATION_EDGE_FONT_COLOR = Color.BLACK;
	public static final boolean ID_AGGREGATION_LABELS_ALONG_EDGE = true, ID_AGGREGATION_EDGE_LINE_END_FILL = false,
			ID_AGGREGATION_EDGE_LINE_BEGIN_FILL = false;

	public static final int ID_AGGREGATION_EDGE_LINE_END = GraphConstants.ARROW_CLASSIC,
			ID_AGGREGATION_EDGE_LINE_BEGIN = GraphConstants.ARROW_NONE;

	public static final float ID_AGGREGATION_EDGE_LINE_WIDTH = 1f;
	public static final Font ID_AGGREGATION_EDGE_LINE_FONT = new Font("Default font for aggregation edge", Font.BOLD,
			13);











	// Výchozí texty tlačítek do třídy ButtonsPanel, v balíčku buttonsPanel (BP = ButtonsPanel)
    public static final String BP_NEW_CLASS = "Vytvořit třídu",
			/* BP_ADD_ASSOCIATION = "Asociace" */BP_ADD_ASSOCIATION = "Symetrická asociace",
			BP_ADD_EXTENDS = "Dědičnost", BP_ADD_IMPLEMENTS = "Implementace", BP_ADD_COMMENT = "Komentář",
			/* BP_ADD_AGGREGATION_1_KU_1 = "Agregace 1 : 1" */BP_ADD_AGGREGATION_1_KU_1 = "Asymetrická asociace",
			BP_ADD_AGGREGATION_1_KU_N = "Agregace 1 : N";

	public static final String BP_NEW_CLASS_TOOL_TIP = "Po kliknutí na tlačítko se otevře dialog, pro zadání názvu nové třídy a označení typu této nové třídy.",
			BP_ADD_ASSOCIATION_TOOL_TIP = "Po kliknutí na toto tlačítko je třeba označit dvě třídy v diagramu tříd (kliknutím na ně levým tlačítkem myši). Obě třídy o sobě budou vědět, tj. první třída bude mít proměnnou typu té druhé třídy a naopak.",
			BP_ADD_EXTENDS_TOOL_TIP = "Po kliknutí na toto tlačítko je třeba označit dvě třídy v diagramu tříd (kliknutím na ně levým tlačítkem myši). První označená třída bude dědit z druhé označené třídy.",
			BP_ADD_IMPLEMENTS_TOOL_TIP = "Po kliknutí na toto tlačítko je třeba označit dvě třídy v diagramu tříd (kliknutím na ně levým tlačítkem myši). První označená třída bude implementovat rozhraní, tj. druhou označenou třídu.",
			BP_BTN_COMMENT_TOOL_TIP = "Po kliknutí na toto tlačítko je třeba označit třídu, ke které se přidá komentář.",
			BP_BTN_AGGREGATION_1_KU_1_TOOL_TIP = "Po kliknutí na toto tlačítko je třeba označit dvě třídy v diagramu tříd (kliknutím na ně levým tlačítkem myši). První označená třída bude mít proměnnou typu té druhé označené třídy.",
			BP_BTN_AGGREGATION_1_KU_N_TOOL_TIP = "Po kliknutí na toto tlačítko je třeba označit dvě třídy v diagramu tříd (kliknutím na ně levým tlačítkem myši). První označená třída (celek) bude mít proměnnou List typu té druhé označené třídy (část).";








	// Texty do třídy PopupMenuRelationships, jedná se o textu a popisky
	// jednotlivých tlačítek v tomto kontextovém menu pro označneí vztahu pro
	// vytvoření mezi třídami v diagramu tříd:
	public static final String PMR_ITEM_ADD_CLASS_TEXT = "Vytvořit třídu",
            PMR_ITEM_ADD_ASSOCIATION_TEXT = "Symetrická asociace", PMR_ITEM_ADD_EXTENDS_TEXT = "Dědičnost",
			PMR_ITEM_ADD_IMPLEMENTS_TEXT = "Implementace",
			PMR_ITEM_ADD_AGGREGATION_1_KU_1_TEXT = "Asymetrická asociace",
            PMR_ITEM_ADD_AGGREGATION_1_KU_N_TEXT = "Agregace 1 : N", PMR_ITEM_ADD_COMMENT_TEXT = "Komentář",

			PMR_ITEM_ADD_CLASS_TT = "Po kliknutí na tlačítko se otevře dialog, pro zadání názvu nové třídy a označení typu této nvé třídy.",
                    PMR_ITEM_ADD_ASSOCIATION_TT = "Po kliknutí na tuto položku je třeba označit dvě třídy v diagramu tříd (kliknutím na ně levým tlačítkem myši). Obě třídy o sobě budou vědět, tj. první třída bude mít proměnnou typu té druhé třídy a naopak.",
			PMR_ITEM_ADD_EXTENDS_TT = "Po kliknutí na tuto položku je třeba označit dvě třídy v diagramu tříd (kliknutím na ně levým tlačítkem myši). První označená třída bude dědit z druhé označené třídy.",
			PMR_ITEM_ADD_IMPLEMENTS_TT = "Po kliknutí na tuto položku je třeba označit dvě třídy v diagramu tříd (kliknutím na ně levým tlačítkem myši). První označená třída bude implementovat rozhraní, tj. druhou označenou třídu.",
                    PMR_ITEM_ADD_AGGREGATION_1_KU_1_TT = "Po kliknutí na tuto položku je třeba označit dvě třídy v diagramu tříd (kliknutím na ně levým tlačítkem myši). První označená třída bude mít proměnnou typu té druhé označené třídy.",
			PMR_ITEM_ADD_AGGREGATION_1_KU_N_TT = "Po kliknutí na tuto položku je třeba označit dvě třídy v diagramu tříd (kliknutím na ně levým tlačítkem myši). První označená třída (celek) bude mít proměnnou List typu té druhé označené třídy (část).",
			PMR_ITEM_ADD_COMMENT_TT = "Po kliknutí na tuto položku je třeba označit třídu, ke které se přidá komentář.";











	// Výchozí texty popisků a tlačítek do dialogu, resp. třídy NewProjectForm.java: (NPF = New Project From)
    public static final String NPF_DIALOG_TITLE = "Nový projekt", NPF_BTN_OK = "OK", NPF_BTN_CANCEL = "Zrušit",
			NPF_BTN_BROWSE = "Procházet", NPF_LBL_NAME = "Název projektu", NPF_LBL_PLACE = "Umístění",
			NPF_LBL_WORKSPACE = "Aplikace ukládá projekty do složky označené jako Workspace.",
			NPF_LBL_CHANGE_PATH = "Chcete změnít umístění tohoto projektu?";

	// Text do chybových hlášek v dialogu NewProjectForm:
	public static final String NPF_PATH_ERROR_TITLE = "Chyba cesty",
			NPF_PATH_ERROR_TEXT = "Cesta k adresáři projektu je ve špatném formátu, nebo obsahuje nepovolené znaky !\n(povolené znaky: a-z, A-Z, 0-9, _)",
            NPF_EMPTY_FIELD_ERROR_TITLE = "Prázdné pole",
			NPF_EMPTY_FIELD_ERROR_TEXT = "Název projektu nebo cesta k projektu není vyplněna!",
            NPF_PROJECT_FOLDER_EXIST_TITLE = "Projekt již existuje",
			NPF_PROJECT_FOLDER_EXIST_TEXT = "Název projektu se již ve zvoleném adresáři označeným jako workspace vyskytuje! Změňte tento název!";

	// text, že složka projektu s daným názvem již existuje:
    public static final String NPF_PROJECT_FOLDER_NAME_EXIST = "Projekt s daným názvem již existuje!";






    // Výchozí konstanty do SettingsForm (SF = SettinsForm) - dialogu pro texty záložek a popisky daných záložek - JtabbedPane
	public static final String SF_TITLE = "Nastavení", SF_APP_TITLE = "Aplikace",
			SF_APP_TOOL_TIP = "Záložka obsahuje možnosti nastavení okna aplikace.",
			SF_CLASS_DIAG_TITLE = "Diagram tříd",
            SF_CLASS_DIAG_TOOL_TIP = "Záložka obsahuje možnosti nastavení pro graf - diagram tříd.",
			SF_INSTANC_DIAG_TITLE = "Diagram instancí",
			SF_INSTANC_DIAG_TOOL_TIP = "Záložka obsahuje možnosti nastaveni pro graf - diagram instancí.",
            SF_COMMAND_EDIT_TITLE = "Editor příkazů",
			SF_COMMAND_EDIT_TOOL_TIP = "Záložka obsahuje možnoti nastavení pro editor příkazů.",
            SF_CODE_EDIT_TITLE = "Editor zdrojového kódu",
			SF_CODE_EDIT_TOOL_TIP = "Záložka obsahuje možnosti nastavení pro editor zdrojového kódu.",
            SF_OUTPUT_EDIT_TITLE = "Editor výstupů",
			SF_OUTPUT_EDIT_TOOL_TIP = "Záložka obshauje možnosti nastavení pro editor výstupů.",
			SF_PNL_PROJECT_EXP_CODE_EDIT_TITLE = "Průzkumník projektů - editor kódu",
            SF_PNL_PROJECT_EXP_CODE_EDIT_TEXT = "Záložka obsahuje nastavení pro editor zdrojového kódu v dialogu průzkumník projektů.",
			SF_PNL_OUTPUT_FRAME_TITLE = "Výstupní terminál",
            SF_PNL_OUTPUT_FRAME_TEXT = "Záložka obsahuje nastavení pro okno (dialog) výstupní termínál.";

	// Texty pro tlačítka:
	public static final String SF_BTN_OK = "OK", SF_BTN_CANCEL = "Zrušit", SF_BUTTONS_FOR_PLACES = "Umístit";


    // Texty pro dotaz, zda se má restartovat aplikace:
    public static final String SF_TXT_RESTART_APP_TEXT = "Aby se projevily změny, je třeba restartovat aplikaci. Přejete si restartovat aplikaci nyní?",
			SF_TXT_RESTART_APP_TITLE = "Restartovat aplikaci";


	// Texty pro ohraničení panelů v ApplicationPanelu v dialog SettingsForm:
    public static final String SF_BORDER_LANGUAGE = "Nastavení jazyka a uspořádání textů",
            SF_PNL_CAPTURE_EXCEPTION = "Vypisovat výjimky",
			SF_PNL_LOADING_FRAME_PANEL = "Obrázek do \"Loading panelu\"",
			SF_BORDER_COPY_DIR = "Vytvoření konfiguračních souborů";



    /**
	 * Texty pro panel, který obsahuje komponenty typu chcb a slouží pro nastavení toho, zda se má po spuštění aplikace otestoat existence adresáře se soubory potřenými pro kompilaci tříd v aplikaci a zda se má pro hledání těch souborů prohledat celý disk nebo ne.
	 */
    public static final String SF_AP_PNL_SEARCH_COMPILE_FILES = "Vyhledat soubory potřebné pro kompilování tříd",
			SF_AP_CHCB_SEARCH_COMPILE_FILES_TEXT = "Po spuštění aplikace vyhledat soubory potřebné pro kompilování tříd (Java Home).",
            SF_AP_CHCB_SEARCH_COMPILE_FILES_TT = "Po spuštění aplikace se spustí vlákno, které zkusí vyhledat soubory potřebné pro kompilování tříd a zkopírovat je do příslušného adresáře ve workspace (více viz Java Home).",
            SF_AP_CHCB_SEARCH_ENTIRE_DISK_FOR_COMPILE_FILES_TEXT = "Za účelem vyhledání souborů prohledat celý disk.",
			SF_AP_CHCB_SEARCH_ENTIRE_DISK_FOR_COMPILE_FILES_TT = "Za účelem nalezení potřebných souborů bude prohledán celý disk na daném zařízení. Hrozí riziko nalezení chybných souborů se stejným názvem.";


	// Výchozí popisky - Labely do ApplicationPanel v SettingsForm:
    public static final String SF_CHCB_ASK_FOR_WORKSPACE = "Po spuštění aplikace se dotázat na umístění adresáře označeného jako workspace.",
            SF_LBL_COPY_CONFIGURATION_DIR = "Vytvořit adresář s výchozím nastavením (adresář configuration) na",
			SF_LBL_COPY_LANGUAGE_DIR = "Vytvořit adresář s jazyky aplikace na",
			SF_LBL_COPY_DEFAULT_PROP = "Vytvořit soubor s výchozím nastavením aplikace Default.properties na",
            SF_LBL_COPY_CD_PROP = "Vytvořit soubor s výchozím nastavením pro diagram tříd (soubor ClassDiagram.properties) na",
			SF_LBL_COPY_ID_PROP = "Vytvořit soubor s výchozím nastavením pro diagram instancí (soubor InstanceDiagram.properties) na",
			SF_LBL_CPY_COMMAND_EDITOR_PROP = "Vytvořit soubor s výchozím nastavením pro editor příkazů (soubor CommandEditor.properties) na",
            SF_LBL_COPY_CLASS_EDITOR_PROP = "Vytvořit soubor s výchozím nastavením pro editor zdrojového kódu třídy (soubor CodeEditor.properties) na",
			SF_LBL_COPY_OUTPUT_EDITOR_PROP = "Vytvořit soubor s výchozím nastavením pro editor výstupů (soubor OutputEditor.properties) na",
            SF_LBL_COPY_OUTPUT_FRAME_PROP = "Vytvořit soubor s výchozím nastavením pro výstupní terminál (soubor OutputFrame.properties) na",
			SF_LBL_COPY_CODE_EDITOR_FOR_INTERNAL_FAMES = "Vytvořit soubor s výchozím nastavením pro editor kódu v průzkumníkovi projektů (soubor CodeEditorInternalFrame.properties) na",
            SF_LBL_ANIMATION_IMAGE = "Obrázek animace",
			SF_CHCB_CAPTURE_EXCEPTIONS_TEXT = "Vypisovat výjimky do výstupního terminálu.",
			SF_CHCB_CAPTURE_EXCEPTIONS_TEXT_TT = "Pokud dojde k neočekávané výjimce v aplikaci" + " " + APP_TITLE + ", "
					+ "vypíše se příslušná informace do dialogu 'Výstupní terminál'. (Při zakázání se hlídají pouze některé potenciální výjimky.)",
            SF_LBL_RESET_APP = "Aby se změny projevily, restartujte prosím aplikaci.",
            SF_ID_LBL_ATTRIBUTES_AND_METHODS_PANEL_TITLE = "Atributy a metody v instanci třídy",
			SF_BTN_RESTORE_DEFAULT = "Obnovit výchozí";


    // Texty do třídy cz.uhk.fim.fimj.settings_form.application_panels.LookAndFeelPanel:
    public static final String LAFP_BORDER_TITLE = "Grafické rozhraní",
            LAFP_LBL_LOOK_AND_FEEL = "Vzhled aplikace",
            LAFP_TXT_WITHOUT_LOOK_AND_FEEL_ITEM_TT = "Nebude aplikován žádný Look and Feel. Využije se výchozí styl " +
                    "společný pro veškeré platformy.";


    // Texty do třídy: cz.uhk.fim.fimj.settings_form.application_panels.UserNamePanel
    public static final String UNP_PNL_BORDER_TITLE = "Uživatelské jméno",
            UNP_LBL_USER_NAME_TEXT = "Jméno",
            UNP_LBL_USER_NAME_TITLE = "Jméno bude využito jako 'autor' vytvořených projektů, tříd, při zápisu do logů " +
                    "apod.",
            UNP_TXT_USER_NAME_FIELD_1 = "Povolené znaky: velká a malá písmena bez / s diakritikou, číslice, " +
                    "podtrřítka, pomlčky, mezery a tabulátory. Jméno musí začínat písmenem.",
            UNP_TXT_USER_NAME_FIELD_2 = "znaků.",
            UNP_BTN_EDIT = "Upravit",
            UNP_BTN_SAVE = "Uložit",
            UNP_BTN_CANCEL = "Zrušit",
            UNP_BTN_RESTORE_DEFAULT_TEXT = "Obnovit výchozí",
            UNP_BTN_RESTORE_DEFAULT_TITLE = "Nastaví se uživatelské jméno příhlášeného uživatele v OS.";
    // Texty do chybových hlášek:
    public static final String UNP_TXT_FAILED_SAVE_USER_NAME_TO_FILE_TEXT = "Nepodařilo se uložit uživatelské jméno " +
            "do konfiguračního souboru.",
            UNP_TXT_FAILED_SAVE_USER_NAME_TO_FILE_TITLE = "Selhalo uložení",
            UNP_TXT_INVALID_NAME_TEXT_1 = "Uživatelské jméno musí začínat písmenem, musí obsahovat",
            UNP_TXT_INVALID_NAME_TEXT_2 = "znaků a může obsahovat pouze velká a malá písmena bez / s diakritkou, " +
                    "číslice, podtržítka, pomlčky, mezery a tabulátory.",
            UNP_TXT_INVALID_NAME_TITLE = "Jméno není validní";


    // Texty do LanguagePanel v applicationPanels:
	public static final String SF_LANGUAGE = "Jazyk", SF_LP_LBL_SORTING_TEXT_IN_PROP_FILE = "Řazení textů v souborech",
			SF_LP_CHCB_ADD_COMMENTS = "Vkládat komentáře.",
            SF_LP_LBL_COUNT_OF_FREE_LINES_BEFORE_COMMENT = "Počet volných řádků nad komentářem",
            SF_LP_LBL_LOCALE = "Globální umístění",
			SF_LP_LBL_LOCALE_TT = "Jedná se o globální umístění pro definici formátu datumu vytvoření v souborech '.properties' obsahující texty pro aplikaci v různých jazycích.";

    /**
     * Jednorozměrné pole, které slouží jako model pro výběr způsobu řazení textů v
     * souboru .properties.
	 *
	 * Toto pole je využito jako model pro cmb cmbKindOfSortingTextInProp v
     * LanguagePanel v applicationPanels.
     */
    public static final String[] SORTING_TEXT_IN_PROP_FILE_MODEL = { "Bez řazení", "Vzestupně dle přidání",
			"Sestupně dle přidání", "Vzestupně dle abecedy", "Sestupně dle abecedy" };

	/**
     * Toto jednorozměrné pole obsahuje popisky pro způsoby řazení v poli
	 * SORTING_TEXT_IN_PROP_FILE_MODEL. Tyto popisky budou zobrazeny jako ToolTip v
     * cmb cmbKindOfSortingTextInProp v nastavení aplikace.
	 */
    public static final String[] SORTING_TEXT_IN_PROP_FILE_TT_MODEL = {
			"Texty v souborech nebudu seřazeny, bude aplikováno výchozí úspořádání.",
			"Texty budou seřazeny vzestupně dle pořadí přidání do souboru.",
            "Texty budou seřazeny sestupně dle pořadí přidání do souboru.",
			"Texty budou seřazeny dle abecedy ve vzestupném pořadí dle klíčových slov.",
            "Texty budou seřazeny dle abecedy v sestupném pořadí dle klíčových slov." };




	// NÁSLEDUJÍCÍ DO SETTINGS_FORM - ClassDiagramPanel:

	// Výchozí popisky do panelu ClassDiagramPanel v dialogu SettingsForm:
    public static final String SF_CHCB_OPAQUE_CELL = "Buňka bude průhledná.",
			SF_LBL_FONT_SIZE_CELL = "Velikost písma", SF_LBL_FONT_STYLE_CELL = "Typ písma",
			SF_LBL_TEXT_ALIGNMENT_X_CELL = "Zarovnání textu - horizontálně",
            SF_LBL_TEXT_ALIGNMENT_Y_CELL = "Zarovnání textu - vertikálně", SF_LBL_TEXT_COLOR_CELL = "Barva písma",
			SF_LBL_BG_COLOR_RIGHT_CELL = "Barva pozadí (od pravého dolního rohu)",
			SF_LBL_BG_COLOR_LEFT_CELL = "Barva pozadí (od levého horního rohu)";

	// Ohraničení panelů - ClassDiagramPanel:
    public static final String SF_CD_PNL_CLASS_CELL_BORDER_TITLE = "Buňka reprezentující třídu",
			SF_CD_PNL_COMMENT_CELL_BORDER_TITLE = "Buňka reprezentující komentář";

	// Texty tlačítek do ClassDiagramPanelu v SettingsForm:
    public static final String SF_CD_BTN_CHOOSE_COLOR = "Vybrat";

	// Texty pro popisky a checkbocy v pnelu ClassDiagramPanel:
    public static final String SF_CD_EDGES_PNL_BORDER_TITLE = "Vlastnosti hran",
			SF_CD_PNL_EXTENDS_EDGE_BORDER_TITLE = "Dědičnost", SF_CD_PNL_IMPLEMENTS_EDGE_BORDER_TITLE = "Implementace",
//			SF_CD_PNL_AGGREGATION_1_KU_1_BORDER_TITLE = "Agregace 1 : 1",
			SF_CD_PNL_AGGREGATION_1_KU_1_BORDER_TITLE = "Asymetrická asociace",
			DF_CD_PNL_AGGREGATION_1_KU_N_BORDER_TITLE = "Agregace 1 : N";

	public static final String /* SF_CD_PNL_ASSOCIATION_EDGE_BORDER_TITLE = "Asociace" */ SF_CD_PNL_ASSOCIATION_EDGE_BORDER_TITLE = "Symetrická asociace",
			SF_CD_CHCB_EDGE_LABEL_ALONG_EDGE = "Text hrany je umístěn podél hrany.",
			SF_CD_CHCB_LINE_BEGIN_FILL = "Začátek hrany je vyplněn.",
			SF_CD_CHCB_LINE_EDN_FILL = "Konec hrany je vyplněn.", SF_CD_LBL_EDGE_COLOR = "Barva hrany",
			SF_CD_LBL_LINE_STYLE = "Styl hrany", SF_CD_LBL_LINE_END = "Typ konce hrany",
			SF_CD_LBL_LINE_BEGIN = "Typ začátku hrany", SF_CD_LBL_LINE_WIDTH = "Šířka hrany";

	// Text do labelu pro typ čerchované hrany - reprezentující implementaci rozhraní, text v ImplementsEdgePane:
	public static final String SF_CD_PNL_IMPLEMENTS_EDGE_PANEL_LBL_LINE_PATTERN = "Styl čerchované hrany";

	// Text do tlačítka pro obnovit výchozí nastavení:
	public static final String SF_BUTTONS_RESTORE_DEFAULT_SETTINGS = "Obnovit výchozí";

	// Texty do chybových hlášek:
	public static final String SF_CD_TXT_CLASS_AND_COMMENT_CELLS_ARE_IDENTICAL_TEXT = "Buňky reprezentující třídu a komentář jsou identické, prosím změňte jejich parametry!",
			SF_CD_TXT_CLASS_AND_COMMENT_CELLS_ARE_IDENTICAL_TITLE = "Chyba duplicity",
			SF_CD_TXT_EDGES_ARE_IDENTICAL_TEXT = "Následující hrany jsou identické, prosím změňte jejich parametry!",
			SF_CD_TXT_EDGES_ARE_IDENTICAL_TITLE = "Chyba duplicity";




	// Pro CommandEditorPanel (CEP) - tedy pro nějaké komponenty v okně:
    public static final String CEP_PNL_AUTO_COMPLETE_WINDOW = "Okno pro doplňování příkazů",
            CEP_CHCB_SHOW_AUTO_COMPLETE_WINDOW_TEXT = "Zpřístupnit okno s příkazy.",
            CEP_CHCB_SHOW_AUTO_COMPLETE_WINDOW_TT = "Stisknutím klávesové zkratky Ctrl + Space bude zobrazena nápověda s dostupnými příkazy.",
            CEP_CHCB_SHOW_REFERENCE_VARIABLES_IN_COMPLETE_WINDOW_TEXT = "Zobrazovat referenční proměnné.",
            CEP_CHCB_SHOW_REFERENCE_VARIABLES_IN_COMPLETE_WINDOW_TT = "U hodnot proměnných zobrazených v okně bude zobrazena referenční proměnná na instance v diagramu instancí.",
            CEP_CHCB_ADD_SYNTAX_FOR_FILLING_THE_VARIABLE_TEXT = "Nabízet vytvoření a naplnění proměnné.",
            CEP_CHCB_ADD_SYNTAX_FOR_FILLING_THE_VARIABLE_TT = "Budou na výběr možnosti pro vytvoření nebo naplnění proměnné hodnotami z proměnných v instancích.",
            CEP_CHCB_ADD_FINAL_WORD_FOR_FILLING_THE_VARIABLE_TEXT = "Deklarovat proměnné s",
            CEP_CHCB_ADD_FINAL_WORD_FOR_FILLING_THE_VARIABLE_TT = "Vygenerovaná deklarace proměnných bude implicitně s 'final'.",
            CEP_CHCB_SHOW_EXAMPLES_OF_SYNTAX_TEXT = "Nabízet ukázkové syntaxe příkazů.",
            CEP_CHCB_SHOW_EXAMPLES_OF_SYNTAX_TT = "Pomocí slova 'example_' bude možné filtrovat a vygenerovat některou z připravených možností pro deklaraci proměnné, Listu, jednorozměrného pole apod.",
            CEP_CHCB_SHOW_DEFAULT_METHODS_TEXT = "Nabízet metody pro editor.",
            CEP_CHCB_SHOW_DEFAULT_METHODS_TT = "Bude možné vygenerovat metody specifické pro editor příkazů (výpis proměnných, nápovědy, hodnot apod.).";


    // Texty do třídy: cz.uhk.fim.fimj.settings_form.command_editor_panels.CompleteHistoryPanel:
    public static final String CHP_TXT_PNL_BORDER_TITLE = "Kompletní historie",
            CHP_TXT_PNL_TOOLTIP_TEXT = "Historie příkazů zadaných od spuštění aplikace.",
            CHP_CHCB_SHOW_HISTORY_TEXT = "Zpřístupnit okno s historií.",
            CHP_CHCB_INDEX_COMMANDS_TEXT = "Číslovat příkazy.",
            CHP_CHCB_INDEX_COMMANDS_TT = "Příkazy budou číslovány dle pořadí zadání (nebude možné filtrovat zadáním " +
                    "částí příkazu).";


    // Texty do třídy: cz.uhk.fim.fimj.settings_form.command_editor_panels.ProjectHistoryPanel:
    public static final String PHP_TXT_PNL_BORDER_TITLE = "Historie projektu",
            PHP_TXT_PNL_TOOLTIP_TEXT = "Historie příkazů zadaných v rámci otevřeného projektu.",
            PHP_CHCB_SHOW_HISTORY_TEXT = "Zpřístupnit okno s historií.",
            PHP_CHCB_INDEX_COMMANDS_TEXT = "Číslovat příkazy.",
            PHP_CHCB_INDEX_COMMANDS_TT = "Příkazy budou číslovány dle pořadí zadání (nebude možné filtrovat zadáním " +
                    "částí příkazu).";





	// Pro ClassDiagramGraphPanel: (CDGP)
	public static final String SF_CD_PNL_CDGP_BORDER_TITLE = "Vlastnosti diagramu",
			SF_CD_PNL_CDGP_CHCB_CON_DIS_CON_LABEL_FROM_EDGE = "Popisky hran lze odpojit od hrany.",
			SF_CD_PNL_CDGP_CHCB_SHOW_RELATION_SHIPS_TO_CLASS_ITSELF = "Zobrazovat vztah třídy na sebe sama.",
			SF_CD_PNL_CDGP_CHCB_SHOW_ASOCIATION_THROUGH_AGREGATION = "Symetrická asociace jako dvě asymetrické.",
			SF_CD_PNL_CDGP_CHCB_SHOW_ASOCIATION_THROUGH_AGREGATION_TT = "Vztah typu symetrická asociace bude zobrazen jako dvě asymetrické asociace.",

			SF_CD_PNL_CDGP_CHCB_ADD_INTERFACE_METHODS_TO_CLASS_TEXT = "Vygenerovat metody při implementaci rozhraní.",
			SF_CD_PNL_CDGP_CHCB_ADD_INTERFACE_METHODS_TO_CLASS_TT = "Při vytvoření vztahu typu implementace rozhraní se do příslušné (označené) třídy vygenerují nestatické metody z označeného rozhraní.",

			SF_CD_PNL_CDGP_CHCB_INCLUDE_PRIVATE_CONSTRUCTORS = "Zpřístupnit soukromé konstruktory v kontextovém menu.",
			SF_CD_PNL_CDGP_CHCB_INCLUDE_PRIVATE_CONSTRUCTORS_TT = "V kontextovém menu nad označenou třídou budou zpřístupněny soukromé konstruktory (tj. konstruktory označené klíčovým slovem 'private').",
			SF_CD_PNL_CDGP_CHCB_INCLUDE_PROTECTED_CONSTRUCTORS = "Zpřístupnit chráněné konstruktory v kontextovém menu.",
			SF_CD_PNL_CDGP_CHCB_INCLUDE_PROTECTED_CONSTRUCTORS_TT = "V kontextovém menu nad označenou třídou budou zpřístupněny chráněné konstruktory (tj. konstruktory označené klíčovým slovem 'protected').",
			SF_CD_PNL_CDGP_CHCB_INCLUDE_PACKAGE_PRIVATE_CONSTRUCTORS = "Zpřístupnit package-private konstruktory v kontextovém menu.",
                    SF_CD_PNL_CDGP_CHCB_INCLUDE_PACKAGE_PRIVATE_CONSTRUCTORS_TT = "V kontextovém menu nad označenou třídou budou zpřístupněny package-private konstruktory (tj. konstruktory bez označení klíčovým slovem pro viditelnost).",

			SF_CD_PNL_CDGP_CHCB_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES = "Názvy zděděných tříd zobrazovat s balíčky.",
                    SF_CD_PNL_CDGP_CHCB_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES_TT = "V kontextovém menu nad třídou v nabídce 'Zděděné statické metody' budou zobrazeny názvy tříd s balíčky, ve kterých / kterém se nchází.",

			SF_CD_PNL_CDGP_CHCB_INCLUDE_PRIVATE_METHODS = "Zpřístupnit soukromé metody v kontextovém menu.",
			SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PRIVATE_METHODS_TT = "V kontextovém menu nad označenou třídou budou zpřístupněny soukromé metody (tj. metody označené klíčovým slovem 'private').",
                    SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PROTECTED_METHODS = "Zpřístupnit chráněné metody v kontextovém menu.",
                    SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PROTECTED_METHODS_TT = "V kontextovém menu nad označenou třídou budou zpřístupněny chráněné metody (tj. metody označené klíčovým slovem 'protected').",
			SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PACKAGE_PRIVATE_METHODS = "Zpřístupnit package-private metody v kontextovém menu.",
			SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PACKAGE_PRIVATE_METHODS_TT = "V kontextovém menu nad označenou třídou budou zpřístupněny package-private metody (tj. metody bez označení klíčovým slovem pro viditelnost).",

			SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PRIVATE_INNER_STATIC_CLASSES = "Zpřístupnit statické soukromé vnitřní třídy v kontextovém menu.",
			SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PRIVATE_INNER_STATIC_CLASSES_TT = "V kontextovém menu nad označenou třídou budou zpřístupněny statické soukromé vnitřní třídy (tj. vnitřní třídy označené klíčovým slovem 'private').",

			SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PROTECTED_INNER_STATIC_CLASSES = "Zpřístupnit statické chráněné vnitřní třídy v kontextovém menu.",
			SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PROTECTED_INNER_STATIC_CLASSES_TT = "V kontextovém menu nad označenou třídou budou zpřístupněny statické chráněné vnitřní třídy (tj. vnitřní třídy označené klíčovým slovem 'protected').",

			SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PACKAGE_PRIVATE_INNER_STATIC_CLASSES = "Zpřístupnit statické package-private vnitřní třídy v kontextovém menu.",
			SF_CD_PNL_CDGP_CHCB_CD_INCLUDE_PACKAGE_PRIVATE_INNER_STATIC_CLASSES_TT = "V kontextovém menu nad označenou třídou budou zpřístupněny statické package-private vnitřní třídy (tj. vnitřní třídy bez označení klíčovým slovem pro viditelnost).",

			SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PRIVATE_STATIC_METHODS = "Zpřístupnit statické soukromé metody ve vnitřních statických třídách v kontextovém menu.",
			SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PRIVATE_STATIC_METHODS_TT = "V kontextovém menu nad označenou třídou budou pro její statické vnitřní třídy zpřístupněny statické soukromé metody (tj. metody označené klíčovým slovem 'private').",

			SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PROTECTED_STATIC_METHODS = "Zpřístupnit statické chráněné metody ve vnitřních statických třídách v kontextovém menu.",
			SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PROTECTED_STATIC_METHODS_TT = "V kontextovém menu nad označenou třídou budou pro její statické vnitřní třídy zpřístupněny statické chráněné metody (tj. metody označené klíčovým slovem 'protected').",

			SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PACKAGE_PRIVATE_STATIC_METHODS = "Zpřístupnit statické package-private metody ve vnitřních statických třídách v kontextovém menu.",
                    SF_CD_PNL_CDGP_CHCB_CD_INNER_STATIC_CLASSES_INCLUDE_PACKAGE_PRIVATE_STATIC_METHODS_TT = "V kontextovém menu nad označenou třídou budou pro její statické vnitřní třídy zpřístupněny statické package-private metody (tj. metody bez označení klíčovým slovem pro viditelnost).",

			SF_ID_PNL_CDGP_CHCB_SHOW_INHERITED_CLASS_NAME_WITH_PACKAGES_TT = "V kontextovém menu nad instancí v nabídce 'Zděděné metody' budou zobrazeny názvy tříd s balíčky, ve kterých / kterém se nchází.",

			SF_CD_PNL_CDGP_CHCB_ID_INCLUDE_PRIVATE_METHODS_TT = "V kontextovém menu nad označenou instancí budou zpřístupněny soukromé metody (tj. metody označené klíčovým slovem 'private').",
                    SF_CD_PNL_CDGP_CHCB_ID_INCLUDE_PROTECTED_METHODS_TT = "V kontextovém menu nad označenou instancí budou zpřístupněny chráněné metody (tj. metody označené klíčovým slovem 'protected').",
			SF_CD_PNL_CDGP_CHCB_ID_INCLUDE_PACKAGE_PRIVATE_METHODS_TT = "V kontextovém menu nad označenou instancí budou zpřístupněny package-private metody (tj. metody bez označení klíčovým slovem pro viditelnost).",

			SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_CONSTRUCTOR = "Při volání konstruktoru zpřístupnit proměnné.",
			SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_CONSTRUCTOR_TT = "V dialogu pro zavolání konstruktoru budou zpřístupněny veřejné a chráněné (popř. statické) proměnné z tříd, instancí tříd a proměnných vytvořených pomocí editoru příkazů ze kterých bude možné předat hodnotu do parametru.",
			SF_CD_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_CONSTRUCTOR = "Při volání konstruktoru zpřístupnit metody.",
			SF_CD_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_CONSTRUCTOR_TT = "V dialogu pro zavolání konstruktoru budou zpřístupněny veřejné (popř. chráněné) statické metody z tříd v diagramu tříd a veřejné metody z instancí v diagramu instancí pro předání návratového hodnoty do parametru konstruktoru.",
			SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_METHOD = "Při volání metody zpřístupnit proměnné.",
			SF_CD_PNL_CDGP_CHCB_MAKE_AVAILABLE_FIELDS_FOR_CALL_METHOD_TT = "V dialogu pro zavolání metody budou zpřístupněny veřejné a chráněné (popř. statické) proměnné z tříd, instancí tříd a proměnných vytvořených pomocí editoru příkazů ze kterých bude možné předat hodnotu do parametru metody.",
			SF_ID_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD = "Při volání metody zpřístupnit metody.",
			SF_ID_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD_TT = "V dialogu pro zavolání metody nad instancí v diagramu instancí budou zpřístupněny veřejné (popř. chráněné) metody z instanci v diagramu instnací a veřejné (popř. chráněné) statické metody z tříd v diagramu tříd pro předání jejich návratové hodnoty do parametru příslušné metody.",
			SF_CD_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD = "Při volání metody zpřístupnit metody.",
			SF_CD_PNL_CDGP_CHCB_MAKE_METHODS_AVAILABLE_FOR_CALL_METHOD_TT = "V dialogu pro zavolání statické metody nad třídou v diagramu tříd budou zpřístupněny veřejné (popř. chráněné) statické metody z tříd v diagramu tříd a veřejné metody z instancí v diagramu instancí pro předání jejich návratové hodnoty do parametru příslušné metody.",
			SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_CONSTRUCTOR = "Při zavolání konstruktoru zpřístupnit vytvoření instance.",
			SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_CONSTRUCTOR_TT = "Pokud bude jako parametr konstruktoru třída, která se nachází v diagramu tříd, bude možné vytvořit její novou instanci.",
			SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD = "Při zavolání metody zpřístupnit vytvoření instance.",
			SF_CD_PNL_CDGP_CHCB_SHOW_CREATE_NEW_INSTANCE_OPTION_IN_CALL_METHOD_TT = "Pokud bude jako parametr metody třída, která se nachází v diagramu tříd, bude možné vytvořit její novou instanci.",
			SF_ID_PNL_CDGP_CHCB_SHOW_GETTER_IN_SETTER_METHOD_IN_INSTANCE = "Při zavolání setru nabízet zavolání getru.",
			SF_ID_PNL_CDGP_CHCB_SHOW_GETTER_IN_SETTER_METHOD_IN_INSTANCE_TT = "Při zavolání setru nad instancí v diagramu instancí nabízet i zavolání getru v příslušné instanci (testováno dle názvů metod).",
			SF_CD_PNL_CDGP_CHCB_WAY_TO_REFRESH_RELATIONSHIPS_BETWEEN_INSTANCES = "Použít rychlý způsob aktualizace vztahů.",
			SF_CD_PNL_CDGP_CHCB_ID_WAY_TO_REFRESH_RELATIONSHIPS_BETWEEN_INSTANCES_TT = "Vztahy mezi instancemi budou nejprve vymazány a následně vytvořeny pouze ty aktuální. Druhý způsob je pomelejší, protože se zjišťuje aktuální a potřebný počet vztahů mezi instancemi, dle toho se následně příslušné vztahy upraví (přidají, odeberou nebo ponechají).",
			SF_CD_PNL_CDGP_CHCB_SHOW_RELATIONSHIPS_IN_INHERITED_CLASSES = "Zobrazit vztahy naplněné v předcích instance.",
			SF_CD_PNL_CDGP_CHCB_SHOW_RELATIONSHIPS_IN_INHERITED_CLASSES_TT = "Zda se mají mezi instancemi zobrazit vztahy, které jsou naplněny v předcích příslušné instance.",
			SF_CD_PNL_CDGP_CHCB_SHOW_REFERENCE_VARIABLES_TEXT = "Zobrazit referenční proměnné.",
			SF_CD_PNL_CDGP_CHCB_SHOW_REFERENCE_VARIABLES_TT = "U proměnných v dialogu s přehledem hodnot o instanci budou zobrazeny reference na instance, které jsou reprezentovány v diagramu instancí.",
			SF_CD_PNL_CDGP_LBL_BG_COLOR = "Barva pozadí grafu", SF_CD_PNL_CDGP_LBL_SCALE = "Úroveň přiblížení grafu",
			//  Texty pro nastavení výchozího editoru zdrojového kodu:
			SF_CD_PNL_CDGP_LBL_CODE_EDITOR_INFO_TEXT = "Výchozí editor zdrojového kódu",
			SF_CD_PNL_CDGP_LBL_CODE_EDITOR_INFO_TT = "Výchozí editor zdrojového kódu, ve kterém se budou primárně otevírat zdrojové kódy tříd v diagramu tříd.",
			// Texty do modelu komponenty JComboBox pro výchozí editor zdrojového kodu:
			SF_CD_PNL_CDGP_TXT_SOURCE_CODE_EDITOR_TEXT = "Editor zdrojového kódu",
			SF_CD_PNL_CDGP_TXT_SOURCE_CODE_EDITOR_TT = "Zdrojový kód tříd v diagramu tříd bude primárně otevřen v dialogu 'Editor zdrojového kódu'.",
			SF_CD_PNL_CDGP_TXT_PROJECT_EXPLORER_TEXT = "Průzkumník projektů",
			SF_CD_PNL_CDGP_TXT_PROJECT_EXPLORER_TT = "zdrojový kód tříd v diagramu tříd bude primárně otevřen v dialogu 'Průzkumník projektů'.";




	// pro text do dialogu pro výběr barvy:
	public static final String SF_TXT_CHOOSE_TEXT_COLOR = "Vyberte barvu písma",
			SF_TXT_CHOOSE_BG_COLOR = "Vyberte barvu pozadí", SF_TXT_CHOOSE_LINE_COLOR = "Vyberte barvu hrany",
            SF_TXT_ONE_COLOR_FOR_CELL = "Jedna barva pro buňku", SF_TXT_TWO_COLOR_FOR_CELL = "Dvě barvy pro buňku",
			SF_TXT_BG_COLOR_FOR_CELL = "Barva pozadí buňky";

	// InstanceDiaggram - ClassCellPanel:
    public static final String SF_ID_PNL_CLASS_CELL_BORDER_TITLE = "Buňka reprezentující instanci třídy";

	// texty:
	public static final String SF_ID_LBL_BORDER_COLOR = "Barva ohraničení",
			SF_ID_SHOW_PACKAGES_IN_CELL = "Zobrazit balíčky třídy.", SF_ID_LBL_BG_COLOR = "Barva buňky",
            SF_ID_DIALOG_CHOOSE_BORDER_COLOR = "Vyberte barvu ohraničení",
			SF_ID_DIALOG_CHOOSE_BG_CELL_COLOR = "Vyberte barvu buňky", SF_ID_LBL_RONDED_BORDER = "Úhel zaoblení hran";

	// EdgePanel:
	public static final String SF_ID_ASSOCIATION_EDGE_PANEL_BORDER_TITLE = "Hrana reprezentující asociaci";

	// AggregationPanel - titulek:
	public static final String SF_ID_AGGREGATION_EDGE_PANEL_BORDER_TITLE = "Hrana reprezentující agregaci";

	// CommandEditorPanel -texty: (CE = Command Editor)
	public static final String SF_CE_BORDER_TITLE = "Vlastnosti pro editor příkazů",
			SF_CE_CHCB_HIGHLIGHTING_CODE = "Zvýrazňování syntaxe jazyka Java.",
			SF_CE_LBL_BACKGROUND_COLOR = "Barva pozadí";

	// Text do OutputEditorPanelu (OEP)
	public static final String SF_OEP_BORDER_TITLE = "Vlastnosti pro editor výstupů";

	// Texty pro chcb, zda se mají vypisovat referenční proměnné u instanci v editoru výstupu, text do třídy OutputEditorPanel.
	public static final String SF_OEP_CHCB_SHOW_REFERENCE_VARIABLES_TEXT = "Vypisovat referenční proměnné.",
			SF_OEP_CHCB_SHOW_REFERENCE_VARIABLES_TT = "Při vypsání instance z diagramu instancí (například po zavolání metody nebo získání instance z proměnné) se vypíše i její reference.";

	// Text do OutputFramePanelu (OfP):
	public static final String SF_OFP_BORDER_TITLE = "Vlastnosti pro výstupní terminál";

    // SettingForm, panel ApplicationPanel - texty do dialogů pro umístění konfiguračních souborů: (FC = FileChooser)
	public static final String SF_APP_FC_CONFIGURATION_DIR = "Umístit adresář configuration",
            SF_APP_FC_LANGUAGE_DIR = "Umístit adresář language",
			SF_APP_FC_DEFAULT_PROP = "Umístit soubor " + DEFAULT_PROPERTIES_NAME,
            SF_APP_FC_CLASS_DIAGRAM_PROP = "Umístit soubor " + CLASS_DIAGRAM_NAME,
			SF_APP_FC_INSTANCE_DIAGRAM_PROP = "Umístit soubor " + INSTANCE_DIAGRAM_NAME,
			SF_APP_FC_COMMAND_EDITOR_PROP = "Umístit soubor " + COMMAND_EDITOR_NAME,
			SF_APP_FC_CLASS_EDITOR_PROP = "Umístit soubor " + CODE_EDITOR_NAME,
			SF_APP_FC_OUTPUT_EDITOR_PROP = "Umístit soubor " + OUTPUT_EDITOR_NAME,
            SF_APP_FC_OUTPUT_FRAME_PROP = "Umístit soubor " + OUTPUT_FRAME_NAME,
			SF_APP_FC_CODE_EDITOR_FOR_INTERNAL_FRAMES_PROP = "Umístit soubor " + CODE_EDITOR_INTERNAL_FRAMES_NAME;

	// Text pro ohraničení v panelu pro hranu komentáře: (CEP = Comment Edge Panel)
	public static final String SF_CEP_BORDER_TITLE = "Hrana komentáře";



	// Texty do CodeEditorPanelu v SettingsForm: (CodeEditorPanel = CodeEP):
	public static final String SF_CODE_EP_BORDER_TITLE = "Vlastnosti pro editor zdrojového kódu",
			SF_CODE_EP_CHCB_HIGHLIGHTING_CODE = "Zvýrazňovat syntaxi.",
			SF_CODE_EP_CHCB_HIGHLIGHTING_CODE_TT = "V dialogu editoru zdrojového kódu bude / nebude zvýrazněna syntaxe textu otevřeného souboru.",
			SF_CODE_EP_CHCB_HIGHLIGHTING_CODE_BY_KIND_OF_FILE = "Zvýrazňovat syntaxi dle otevřeného souboru.",
			SF_CODE_EP_CHCB_HIGHLIGHTING_CODE_BY_KIND_OF_FILE_TT = "Pro některé typy souborů bude využit příslušný způsob zvýrazňování syntaxe (java, json, properties, xml, html, ...). Pro ostatní typy souborů bude využito zvýraznění typické pro jazyk Java (výchozí).",
			SF_CODE_EP_CHCB_COMPILE_CLASS_IN_BACKGROUND = "Kompilovat třídy stisknutím",
			SF_CODE_EP_CHCB_COMPILE_CLASS_IN_BACKGROUND_TT = "Zkompilují se třídy v otevřeném projektu nebo třída, otevřená v editoru kódu. V případě kompilace bez chyb se doplní získané hodnoty (proměnné a metody) do okna s auto doplňováním kódu.";




	/**
	 * Proměnná, která obsahuje znaky, které slouží pro kompilaci tříd v editorech
	 * kódu.
	 */
	public static final String KEYS_FOR_COMPILING = " ';', ']' a '}'";


    // Texty do třídy: cz.uhk.fim.fimj.settings_form.FormattingPropertiesPanel
    public static final String FPP_BORDER_TITLE = "Formátování zdrojového kódu",
            FPP_DELETE_EMPTY_LINE_TEXT = "Odstranit prázdné řádky.",
            FPP_DELETE_EMPTY_LINE_TT = "Prázdné řádky uvnitř metod / bloků kódu budou odebrány.",
            FPP_TAB_SPACE_CONVERSION_TEXT = "Konverze tabulátoru na mezery.",
            FPP_TAB_SPACE_CONVERSION_TT = "Tabulátor (bílé znaky) budou převedeny na mezery.",
            FPP_BREAK_ELSE_IF_TEXT = "Zalomit syntaxi",
            FPP_BREAK_ELSE_IF_TT = "Zalomení části za 'else' na nový řádek.",
            FPP_SET_SINGLE_STATEMENT_TEXT = "Zalomit příkazy na více řádků.",
            FPP_SET_SINGLE_STATEMENT_TT = "'Jeden příkaz na jeden řádek.' Pokud bude na jednom řádku více příkazů, " +
                    "například dvě zavolání metody. Příkazy budou rozděleny na dva řádky.",
            FPP_BREAK_ONE_LINE_BLOCK_TEXT = "Zalomit jednořádkové bloky kódu.",
            FPP_BREAK_ONE_LINE_BLOCK_TT = "Bloky kódu, které lze zapsat na jeden řádek budou zalomeny na více řádků.",
            FPP_BREAK_CLOSING_HEADER_BRACKET_TEXT = "Zalomit kód za zavírací závorkou.",
            FPP_BREAK_CLOSING_HEADER_BRACKET_TT = "Kód za zavírací závorkou bude zalomen na nový řádek.",
            FPP_BREAK_BLOCK_TEXT = "Oddělit nesouvisející bloky.",
            FPP_BREAK_BLOCK_TT = "Nesouvisející bloky kódu budou odděleny prázdnými řádky. Například pod zavírací " +
                    "závorkou podmínky bude prázdný řádek pro oddělení dalšího bloku kódu.",
            FPP_OPERATOR_PADDING_MODE_TEXT = "Oddělit operátory od operandů mezerami.",
            FPP_OPERATOR_PADDING_MODE_TT = "Mezi operátory a operandy budou doplněny mezery.";





	// Texty do ProjectExplorerCodeEditorPanel.java (PECEP) v SettingsForm:
	public static final String SF_PECEP_BORDER_TITLE = "Vlastnosti pro editor kódu v průzkumníku projektů";






	// Texty do třídy: HighlightInstancePanel v instanceDigramPanels. Jedná se o
	// třídu s komponentami pro nastavení jak má vypadat zvýrazněná instance a jak
	// dlouho má být zvýrazněná a zda se mají vůbec zvýrazňovat.
	public static final String HIP_BODER_TITLE = "Zvýraznění instance",
    // Zda se mají zvýrazňovat instance:
			HIP_CHCB_HIGHLIGHT_INSTANCES_TEXT = "Zvýrazňovat instance",
			HIP_CHCB_HIGHLIGHT_INSTANCES_TT = "Pokud je získaná hodnota (z metody či proměnné) instance třídy z diagramu tříd, která se nachází v diagramu instancí, bude zvýrazněna.",

			// Čas, jak dlouho mají být zvýrazněné instance:
			HIP_LBL_HIGHLIGHTING_TIME_TEXT = "Doba zvýraznění instance v sekundách",
			HIP_LBL_HIGHLIGHTING_TIME_TT = "Jedná se o dobu, po kterou bude instance zvýrazněna. Například, hodnota '1.0' znamená, že instance bude zvýrazněna po dobu jedné vteřiny, pak se vrátí do původního vzhledu.",

    // Label pro výběr barvy písma ve zvýrazněné instanci:
			HIP_LBL_TEXT_COLOR_TEXT = "Barva písma", HIP_LBL_TEXT_COLOR_TT = "Barva písma zvýrazněné instance.",

			// label pro výběr barvy písma atributů ve zvýrazněné instanci:
			HIP_LBL_ATTRIBUTE_COLOR_TEXT = "Barva atributů",
			HIP_LBL_ATTRIBUTE_COLOR_TT = "Barva písma atributů ve zvýrazněné instanci.",

			// Label pro výběr barvy písma metod ve zvýrazněné instanci:
			HIP_LBL_METHOD_COLOR_TEXT = "Barva metod",
			HIP_LBL_METHOD_COLOR_TT = "Barva písma metod ve zvýrazněné instanci.",

			// Label pro výběr barvy ohraničení zvýrazněné instance:
            HIP_LBL_BORDER_COLOR_TEXT = "Barva ohraničení",
			HIP_LBL_BORDER_COLOR_TT = "Barva ohraničení zvýrazněné instance.",

			// Label pro výběr barvy pozadí zvýrazněné instance:
			HIP_LBL_BACKGROUND_COLOR_TEXT = "Barva pozadí",
                    HIP_LBL_BACKGROUND_COLOR_TT = "Barva pozadí zvýrazněné instance.",

			// Tlčítka pro označení barev písma, ohraničení a pozadí instance:
			HIP_BTN_CHOOSE_COLOR = "Vybrat",

			// Texty pro dialogy pro výběry barev:
			HIP_TXT_CHOOSE_TEXT_COLOR_TITLE = "Vyberte barvu písma",
			HIP_TXT_CHOOSE_ATTRIBUTE_COLOR_TITLE = "Vyberte barvu písma atributů",
			HIP_TXT_CHOOSE_METHOD_COLOR_TITLE = "Vyberte barvu písma metod",
			HIP_TXT_CHOOSE_BORDER_COLOR_TITLE = "Vyberte barvu ohraničení",
			HIP_TXT_CHOOSE_BACKGROUND_COLOR = "Vyberte barvu pozadí";












	// Texty do panelu InstanceCellMethodsPanel v balíčku instnceDiagramPanel, jedná
	// se o texty pro komponenty pro nastavení toho, zda a jak se mají zobrazovat
	// metody v buňce reprezentující instanci v diagramu instancí.
	public static final String ICMP_BORDER_TITLE = "Metody",
			// Texty pro chcbs:
			ICMP_CHCB_SHOW_TEXT = "Zobrazit metody.", ICMP_CHCB_SHOW_ALL_TEXT = "Zobrazit veškeré metody.",
                    ICMP_CHCB_USE_SAME_FONT_AND_COLOR_AS_REFERENCE_TEXT = "Aplikovat na metody stejný font a barvu písma.",

			ICMP_CHCB_SHOW_TT = "V buňkách reprezentující instance v diagramu instancí budou zobrazeny metody příslušné instance.",
			ICMP_CHCB_SHOW_ALL_TT = "V buňkách reprezentující instance v diagramu instancí budou zobrazeny veškeré metody příslušné instance (označeno) nebo pouze zvolený počet metod (neoznačeno).",
			ICMP_CHCB_USE_SAME_FONT_AND_COLOR_AS_REFERENCE_TT = "Metody budou vykresleny stejným fontem a barvou, která bude nastavena pro barvu buňky (instance).",

    // Label pro počet metod:
			ICMP_LBL_SHOW_SPECIFIC_COUNT_TEXT = "Zobrazit počet metod",
			ICMP_LBL_SHOW_SPECIFIC_COUNT_TT = "V buňkách reprezentující instance v diagramu instancí bude zobrazen pouze definovaný počet metod.",

			// Labely pro velikost, typ a barvu písma pro metody:
			ICMP_LBL_FONT_SIZE = "Velikost písma", ICMP_LBL_FONT_STYLE = "Typ písma",
			ICMP_LBL_TEXT_COLOR = "Barva písma",

    // Text pro tlačítko pro výběr barvy:
			ICMP_BTN_CHOOSE_COLOR = "Vybrat",

			// Titulek do dialogu pro výběr barvy písma pro metody:
			ICMP_TXT_CLR_DIALOG_TITLE = "Vyberte barvu písma pro metody";















	// Texty do panelu InstanceCellAttributesPanel v balíčku instanceDiagramPanel,
	// jedná se o texty pro komponenty, které slouží pro nastavení toho, zda a jak
	// se mají zobrazovat atributy v buňce, která v diagramu instancí reprezentuje
	// instance.
	public static final String ICAP_BORDER_TITLE = "Atributy",
			// Texty pro chcbs:
			ICAP_CHCB_SHOW_TEXT = "Zobrazit atributy.", ICAP_CHCB_SHOW_ALL_TEXT = "Zobrazit veškeré atributy.",
			ICAP_CHCB_USE_SAME_FONT_AND_COLOR_AS_REFERENCE_TEXT = "Aplikovat na atributy stejný font a barvu písma.",

			// Popisky pro chcbs:
            ICAP_CHCB_SHOW_TT = "V buňkách reprezentující instance v diagramu instancí budou zobrazeny atributy příslušné instance.",
			ICAP_CHCB_SHOW_ALL_TT = "V buňkách reprezentující instance v diagramu instancí budou zobrazeny veškeré atributy příslušné instance (označeno) nebo pouze zvolený počet atributů (neoznačeno).",
			ICAP_CHCB_USE_SAME_FONT_AND_COLOR_AS_REFERENCE_TT = "Atributy budou vykresleny stejným fontem a barvou, která bude nastavena pro barvu buňky (instance).",

			// Label pro počet atributů:
			ICAP_LBL_SHOW_SPECIFIC_COUNT_TEXT = "Zobrazit počet atributů",
			ICAP_LBL_SHOW_SPECIFIC_COUNT_TT = "V buňkách reprezentující instance v diagramu instancí bude zobrazen pouze definovaný počet atributů.",

			// Labely pro font a barvu písma metod:
			ICAP_LBL_FONT_SIZE = "Velikost písma", ICAP_LBL_FONT_STYLE = "Typ písma",
			ICAP_LBL_TEXT_COLOR = "Barva písma",

			// Tlačítko pro otevření dialogu pro výběr barvy písma pro metody:
			ICAP_BTN_CHOOSE_COLOR = "Vybrat",

			// Titulek do dialogu pro výběr barvy písma pro atributy:
			ICAP_TXT_CLR_DIALOG_TITLE = "Vyberte barvu písma pro atributy";



















	// NewClassForm - texty: (NCF)
	// Titulek dialogu:
	public static final String NCF_DIALOG_TITLE = "Vytvořit novou třídu";

	// Texty do tlačítek:
	public static final String NCF_BTN_OK = "OK", NCF_BTN_CANCEL = "Zrušit";

	// texty do popisku pro zadani nazvu tridy a ohraniceni:
	public static final String NCF_LBL_CLASS = "Název třídy", NCF_CLASS_KIND_BORDER_TITLE = "Typ třídy",
			NCF_CLASS_LBL_PACKAGE_NAME = "Balíček";

	// Texty do Zaškrtávacích tlačítek:
	public static final String NCF_RB_CLASS = "Třída", NCF_RB_ABSTRACT_CLASS = "Abstraktní třída",
			NCF_RB_INTERFACE = "Rozhraní", NCF_APPLET = "Applet", NCF_RB_ENUM = "Výčet", NCF_RB_UNIT_TEST = "Unit test";

	// Chybové hlášky do dialogu NewClassForm:
	public static final String NCF_EMPTY_CLASS_NAME_TEXT = "Není zadán název třídy!",
			NCF_EMPTY_CLASS_NAME_TITLE = "Chybí název třídy",
			NCF_FORMAT_ERROR_TEXT = "Formát názvu třídy není správný !\nNázev třídy musí začínat velkým písmenem, může obsahovat pouze malá a velká písmena bez diakritiky, číslice, tečku a '_' (podtržítko)!",
			NCF_FORMAT_ERROR_TITLE = "Chybný formát", NCF_END_CLASS_NAME_ERROR_TITLE = "Chybný formát",
			NCF_END_CLASS_NAME_ERROR_TEXT = "Název třídy nesmí končit: '.' (tečkou)!",
			NCF_ERROR_KEY_WORD_TEXT = "Název třídy ani balíčku (kvůli importu) nesmí být kličové slovo Javy !\nChyba: Název",
			NCF_ERROR_KEY_WORD_TITLE = "Chybný název",
			NCF_DUPLICATE_CLASS_NAME_ERROR_TITLE = "Duplicita názvu",
			NCF_DUPLICATE_CLASS_NAME_ERROR_TEXT = "Nový název třídy je identický s původním, změňte ho!",
			NCF_WRONG_NAME_OF_CLASS_TEXT = "Název třídy není zadán ve správné syntaxi, tj. musí začínat velkým písmenem a může obshovat pouze číslice, podtržítka, velká a malá písmena bez diakritiky.",
			NCF_WRONG_NAME_OF_CLASS_TITLE = "Chybný název třídy", NCF_TXT_ERROR = "Chyba",
			NCF_TXT_CLASS_ALREADY_EXIST_IN_SAME_PACKAGE_TEXT = "Zadaná třída již existuje (ve stejném balíčku), zvolte prosím jiný název třídy nebo balíčku.",
			NCF_TXT_CLASS_ALREADY_EXIST_IN_SAME_PACKAGE_TITLE = "Třída již existuje",

			// Texty pro chybovou hlášku - že byl zadán balíček / balíčky v chybné syntaxi:
			NCF_TXT_PACKAGE_IS_IN_WRONG_SYNTAX_TEXT_1 = "Balíček není zadán ve správné syntaxi, tj. může obsahovat pouze velká a malá písmena bez diakritiky, číslice a podtržítka.",
			NCF_TXT_PACKAGE_IS_IN_WRONG_SYNTAX_TEXT_2 = "Jednotlivé adresáře (balíčky) mohou být odděleny desetinnou tečkou, která nesmí být na začátku ani na konci.",
			NCF_TXT_PACKAGE_IS_IN_WRONG_SYNTAX_TEXT_3 = "Zadáno",
			NCF_TXT_PACKAGE_IS_IN_WRONG_SYNTAX_TITLE = "Chybný balíček";















	// Text do Třídy WriteToFile (WTF) - chybové hlášky v metodě: existSrcDirectory(), createAbstractClass()
	public static final String WTF_PROJECT_DIR_ERROR_TITLE = "Chyba adresáře projektu",
			WTF_PROJECT_DIR_TEXT = "Nebyl nalezen adresář projektu!",
			WTF_PROJECT_SRC_DIR_FAILED_TO_CREATE_TITLE = "Chyba při vytváření adresáře",
			WTF_PROJECT_SRC_DIR_FAILED_TO_CREATE_TEXT = "Nastala chyba při vytváření adresáře src v otevřeném projektu!";


	// Texty komentářů do metody: createClass() ve třídě WriteToFile.java:
	public static final String WTF_TEXT_CONSTRUCTOR = "Konstruktor třídy.", WTF_TEXT_COMMENT_CLASS = "Zde můžete napsat komentář k této třídě.";

	// Texty chybové hlášky do metody: createClass ve třídě: WriteToFile:
	public static final String WTF_ERROR_MESSAGE1 = "Třída s názvem", WTF_ERROR_MESSAGE2 = "ve složce",
			WTF_ERROR_MESSAGE3 = "již existuje, zvolte prosím jiný název!",
			WTF_ERROR_MESSAGE_TITLE = "Duplicitní název";


	// stejná třída, texty do metody: existBinDirectory:
	public static final String WTF_BIN_DIR_FAILED_TO_CREATE_ERROR_TITLE = "Chyba při vytváření adresáře",
			WTF_BIN_DIR_FAILED_TO_CREATE_ERROR_TEXT = "Nastala chyba při vytváření adresáře bin v adresáři otevřeného projektu!";
















	// Texty do chybových hlášek do třídy: TestAndCreate.java - v celé třídě:
	public static final String TAC_MISSING_CLASS_TEXT = "Alespoň jedna z označených tříd chybí!",
			TAC_MISSING_CLASS_TITLE = "Chyba třídy",
			TAC_INHERIT_ERROR_TEXT = "'Zdrojová' třída - buňka již z něčeho dědí !\nV Javě neexistuje vícenásobná dědičnost!",
			TAC_INHERIT_ERROR_TITLE = "Chyba dědičnosti",
			TAC_ENUM_INHERIT_ERROR_TEXT = "Výčet nemůže implementovat rozhraní!",
			TAC_ENUM_INHERIT_ERROR_TITLE = "Chyba implementace",
			TAC_INTERFACE_ERROR_TEXT = "Rozhraní nemůže implementovat rozhraní!",
			TAC_INTERFACE_ERROR_TITLE = "Chyba rozhraní",
			TAC_IMPLEMENTS_ERROR_TEXT_1 = "Třída může implementovat pouze rozhraní!\nTřída",
			TAC_IMPLEMENTS_ERROR_TEXT_2 = " není rozhraní!", TAC_IMPLEMENTS_ERROR_TITLE = "Chyba implementace",
			TAC_SELECTED_CLASS_ERROR_TEXT = "Označená třída nebyla nalezena!\nCesta k třídě",
			TAC_SELECTED_CLASS_ERROR_TITLE = "Chyba třídy",
			TAC_MISSING_SRC_DIRECTORY_TEXT = "Adresář src nebyl v adresáři projektu nalezen, prosím, vytvořte jej, nebo otevřete jiný projekt!\nCesta",
			TAC_MISSING_SRC_DIRECTORY_TITLE = "Chyba adresáře src",
			TAC_ENUM_INHERIT_ERROR_TEXT_2 = "Enum nemůže dědit!", TAC_ENUM_INHERIT_ERROR_TITLE_2 = "Chyba dědičnosti",
			TAC_WRONG_ELECTED_OBJECT_TEXT = "Alespoň jeden objekt nění třída, ale komentář!",
			TAC_WRONG_SELECTED_OBJECT_TITLE = "Chybné označení objektů",
			TAC_CHOOSED_COMMENT_NOT_CLASS_TEXT = "Nebyla označena třída, ale komentář, k němu nelze připojit další komentář!",
			TAC_CHOOSED_COMMENT_NOT_CLASS_TITLE = "Označen komentář",
			TAC_MISSING_PROJECT_DIRECTORY_TEXT = "Nebyl nalezen adresář projektu, otevřete prosím jiný projekt!\nCesta",
			TAC_MISSING_PROJECT_DIRECTORY_TITLE = "Chyba projektu";





	// třída ReadFile, metoda getPathToBin:
	public static final String RF_TXT_FAILED_TO_CREATE_BIN_DIR_TEXT = "Nepodařilo se vytvořit adresář bin v adresáři otevřeného projektu!",
			RF_TXT_FAILED_TO_CREATE_BIN_DIR_TITLE = "Selhalo vytvoření adresáře";










	// Texty do chybových hlášek do třídy EditClass.java: (ED = EditClass)
	public static final String ED_MISSING_CLASS_TITLE = "Chyba třídy",
			ED_MISSING_CLASS_TEXT_1 = "Třída / Třídy nebyly nalezeny!\nTřída 1",
			ED_MISSING_CLASS_TEXT_2 = "\nTřída 2";










	// Texty do popisků v třídě ClassFromFileForm.java
	// Titulek dislogu:
	public static final String CFFF_DIALOG_TITLE = "Načtení třídy";
	public static final String CFFF_LBL_CHOOSE_CLASS = "Vyberte třídu (soubor s příponou '.java')",
			CFFF_LBL_PLACE = "Umístění", CFFF_LBL_CLASS_NAME = "Zadejte název třídy",
			CFFF_LBL_CLASS_NAME_TT = "Název třídy lze změnit a umístit jej do nějakého balíčku";

	// Texty tlačítek:
	public static final String CFFF_BTN_OK = "OK", CFFF_BTN_CANCEL = "Zrušit", CFFF_BTN_BROWSE = "Procházet";

	// Texty do chybových hlášek:
    public static final String CFFF_FORMAT_ERROR_TITLE = "Chyba formátu",
			CFFF_PATH_FORMAT_ERROR_TEXT = "Cesta k souboru je ve špatném formátu!",
			CFFF_EMPTY_FIELD_ERROR_TITLE = "Prázdné pole", CFFF_EMPTY_FIELD_ERROR_TEXT = "Nějaké pole je nevyplněné!";











    // Výchozí hodnoty do CommandEditor.properties:
	public static final Font DEFAULT_FONT_FOR_COMMAND_EDITOR = new Font("Default font for command editor", Font.BOLD, 14);
	public static final Color COMMAND_EDITOR_BACKGROUND_COLOR = Color.WHITE, COMMAND_EDITOR_FOREGROUND_COLOR = Color.BLACK;
    public static final boolean COMMAND_EDITOR_HIGHLIGHTING_CODE = true;

    /*
     * Výchozí hodnoty pro CommandEditor.properties pro okno s automatickým doplňováním / dokončováním příkazů v
     * editoru příkazů. Jedná se o okno, které se zobrazí při stisku kláves Ctrl + Space.
     */

    /**
     * Zda se má po stisknutí klávesové zkratky Ctrl + Space zobrazit okno s příkazy pro doplnění do editoru příkazů.
     */
    public static final boolean CE_SHOW_AUTO_COMPLETE_WINDOW = true;

    /**
     * Zda se mají zobrazovat referenční proměnné u hodnot proměnných v okně pro automatické doplňování příkazů v
     * editoru příkazů.
     * <p>
     * Jde o to, že se v okně pro automatické doplňování / dokončování příkazů zobrazují proměnné a u nich jejich
     * hodnoty, pokud se jedná o proměnnou obsahující referenci na instanci v diagramu instancí, zde je možnost, že když
     * bude tato proměnná true, zobrzí se u té reference i text "referenční proměnná" a za ní ta reference, kterou
     * uživatel definoval. Přes kterou přistupuje k té instanci v diagramu instancí.
     */
    public static final boolean CE_SHOW_REFERENCE_VARIABLES_IN_COMPLETE_WINDOW = true;

    /**
     * Zda se má v okně pro automatické doplňování příkazů v editoru příkazů zobrazovat syntaxe pro deklaraci nebo
     * naplnění proměnných z proměnných nacházejících se v instanci v diagramu instancí.
     */
    public static final boolean CE_ADD_SYNTAX_FOR_FILLING_THE_VARIABLE = true;

    /**
     * Zda se má v okně pro automatické doplňování příkazů v editoru příkazů při vytváření "klasické" proměnné přidat k
     * té deklaraci klíčové slovo "final". Jedná se pouze o deklaraci "běžných proměnných", ne datových struktur.
     */
    public static final boolean CD_ADD_FINAL_WORD_FOR_FILLING_THE_VARIABLE = false;

    /**
     * Zda se mají v okně pro doplňování příkazů v editoru příkazů zobrazovat připravené ukázky "základních" syntaxí
     * různých příkazů, které lze "použít" v editoru příkazů.
     */
    public static final boolean CE_SHOW_EXAMPLES_OF_SYNTAX = true;

    /**
     * Zda se mají v okně pro automatické doplňování příkazů v editoru příkazů zobrazit výchozí definované metody pro
     * editoru příkazů. Jedná se o definované metody jako jsou například výpis proměnných, výpis nápovedy pro eitor,
     * restartování či ukončení aplikace apod.
     */
    public static final boolean CE_SHOW_DEFAULT_METHODS = true;

    /**
     * Zda se má zpřístupnit okno s historií v editoru příkazů (po stisknutí příslušné klávesové zkratky). True - bude
     * zpřístupněno okno s historií zadaných příkazů v rámci otevřeného projektu. Jinak false.
     */
    public static final boolean CE_SHOW_PROJECT_HISTORY_WINDOW = true;

    /**
     * Zda se mají příkazy v okně s historií příkazů v rámci otevřeného projektu číslovat. V takovém případě budou
     * seřazeny dle historie zadání. Jinak ne. True - příkazy se budou číslovat, ale nelze je filtrovat. False - příkazy
     * se nebudou číslovat.
     */
    public static final boolean CE_INDEX_COMMANDS_IN_PROJECT_HISTORY_WINDOW = true;

    /**
     * Zda se má zpřístupnit okno s historií v editoru příkazů, které byly zadány od spuštění aplikace (po stisknutí
     * příslušné klávesové zkratky). True - bude zpřístupněno okno s historií zadaných příkazů, které uživatel zadal do
     * editoru příkazů od spuštění aplikace nehledě na přepínání projektů. Jinak false.
     */
    public static final boolean CD_SHOW_COMPLETE_HISTORY_WINDOW = true;

    /**
     * Zda se mají příkazy v okně s historií příkazů, které uživatel zadal od spuštění aplikace číslovat. V takovém
     * případě budou seřazeny dle historie zadání. Jinak ne. True - příkazy se budou číslovat, ale nelze je filtrovat.
     * False - příkazy se nebudou číslovat.
     */
    public static final boolean CD_INDEX_COMMANDS_IN_COMPLETE_HISTORY_WINDOW = true;









	// Výchozí hodnoty do CodeEditor.properties:

	// a zároveň výchozí hodnoty pro CodeEditorInternalFrame.properties
    // Zde nevidím důvod proč nedat stejné výchozí hodnoty ??? akorát se jedná o "jiné" editory
    public static final Font DEFAULT_FONT_FOR_CODE_EDITOR = new Font("Default font for source code editor", Font.BOLD,
			14);
    public static final Color CODE_EDITOR_BACKGROUND_COLOR = Color.WHITE, CODE_EDITOR_FOREGROUND_COLOR = Color.BLACK;
    public static final boolean CODE_EDITOR_HIGHLIGHTING_CODE_BY_KIND_OF_FILE = true,
            CODE_EDITOR_HIGHLIGHTING_CODE = true, CODE_EDITOR_COMPILE_CLASS_IN_BACKGROUND = true;
    /**
     * Zda se mají zalamovat texty ve všech interních oknech / otevřených souborech v dialogu průzkumník projektů.
     */
    public static final boolean CODE_EDITOR_WRAP_TEXT_IN_ALL_FRAMES = false;
    /**
     * Zda se mají zobrazovat bílé znaky ve všech interních oknech / otevřených souborech v dialogu průzkumník
     * projektů.
     */
    public static final boolean CODE_EDITOR_SHOW_WHITE_SPACE_IN_ALL_FRAMES = false;
    /**
     * Zda se mají odebírat bílé znaky z prázdného řádku, kde se nachází kurzor.
     *
     * <i>Po vložení nového řádku - stisknutí klávesy Enter se na řádku, kde se nachází kurzor odeberou veškeré bílé
     * znaky v případě, že je řádek prázdný.</i>
     */
    public static final boolean CODE_EDITOR_CLEAR_WHITE_SPACE_LINE_IN_ALL_FRAMES = false;
    /**
     * Zda se má zalamovat text v dialogu editor zdrojového kódu.
     * <p>
     * Hodnota true značí, že se bude zalamovat text v editoru, takže uživatel nebude muset použít posuvník do strany.
     */
    public static final boolean CODE_EDITOR_WRAP_TEXT_IN_EDITOR = false;
    /**
     * Zda se mají zobrazovat bílé znaky v editoru kódu.
     */
    public static final boolean CODE_EDITOR_SHOW_WHITE_SPACE_IN_EDITOR = false;
    /**
     * Zda se mají po "odentrování" (vložení nového řádku) odebrat bílé znaky z toho "odentrovaného" řádku.
     */
    public static final boolean CODE_EDITOR_CLEAR_WHITE_SPACE_LINE_IN_EDITOR = false;


    // Výchozí hodnoty pro vlastnosti formátování zdrojového kódu. (Source Code Formatting)
    /**
     * True v případě, že se mají odebírat prázdné řádky mezi bloky kódu - uvnitř metod.
     */
    public static final boolean SCF_DELETE_EMPTY_LINES = true;
    /**
     * True v případě, že se mají bílé znaky tabulátoru převést na mezery.
     */
    public static final boolean SCF_TAB_SPACE_CONVERSION = true;
    /**
     * True v případě, že má zalamovat syntaxe: "else if ()" na:
     * <p>
     * else if () ...
     */
    public static final boolean SCF_BREAK_ELSE_IF = false;
    /**
     * True v případě, že se mají rozdělit příkazy na jednom řádku.
     * <p>
     * Například když na jednom řádku bude System.out.println("adf");System.out.println("adf");
     * <p>
     * Pak se zalomí na:
     * <p>
     * System.out.println("adf"); System.out.println("adf");
     */
    public static final boolean SCF_SINGLE_STATEMENT = true;
    /**
     * True v případě, že se má jednořádkový blok kódu zalomit na více řádků.
     *
     * <i>
     * Metoda: public Object test() {return null;}
     * <p>
     * V případě nastavené hodnoty true bude výše uvedená metoda zalomena na tři řádky (hlavička, tělo, zavírací
     * závorka). V případě hodnoty false zůstane syntaxe metody tak jak je (na jednom řádku).
     * </i>
     */
    public static final boolean SCF_BREAK_ONE_LINE_BLOCK = false;
    /**
     * True v případě, že se má zalamovat kód za zavíracímí závorkami.
     * <p>
     * Například:
     * <p>
     * if (true) {
     * <p>
     * } else { ... } Tak když je tato hodnota true, bude část "else" (za zavírací závorkou) vždy na novém řádku.
     */
    public static final boolean SCF_BREAK_CLOSING_HEADER_BRACKETS = false;
    /**
     * True v případě, že se mají oddělist nesouvisející bloky kódu prázdnými řádky.
     * <p>
     * Například:
     * <p>
     * public ClassName() {
     * <p>
     * } if (true) {
     * <p>
     * } System.out.println();
     * <p>
     * Tak nad System.out.println(); bude vložena mezera pro oddělení podmínky.
     */
    public static final boolean SCF_BREAK_BLOCKS = false;
    /**
     * True v případě, že se mají vkládat / oddělit operátory a čísla mezerami. ("Operátory budou obaleny mezerami").
     * <p>
     * Například syntaxe: double i = 5+5+11+0.002;
     * <p>
     * V přpadě hodnoty true se operátory obalí mezerami, například v následující syntaxi:
     * <p>
     * Například syntaxe: double i = 5 + 5 + 11 + 0.002;
     * <p>
     * V případě hodnoty false zůstane příkaz v syntaxe jaké je.
     */
    public static final boolean SCF_OPERATOR_PADDING_MODE = true;








	// Text do OutputEditor.java - akorát výchozí text s nápovědou pro uživatele ohledně příkazů:
    public static final String OE_INFO_ABOUT_COMMANDS = "Přehled příkazů je možné zobrazit metodou";
    // Info pro klávesové zkratky:
    public static final String OE_INFO_FOR_CTRL_ENTER = "Potvrzení příkazu.";
    public static final String OE_INFO_FOR_ENTER_UP_DOWN = "Procházení historie potvrzených příkazů.";
    public static final String OE_INFO_FOR_SHIFT_ENTER = "Nový řádek bez potvrzení příkazu.";
    public static final String OE_INFO_FOR_CTRL_SPACE = "Dostupné příkazy";
    public static final String OE_INFO_FOR_CTRL_H = "Historie příkazů v otevřeném projektu";
    public static final String OE_INFO_FOR_CTRL_SHIFT_H = "Historie příkazů od spuštění aplikace";
















	// PopupMenu pro class diagram - texty do polozek v tomto menu:
    public static final String PPM_ITEM_RENAME = "Přejmenovat", PPM_ITEM_REMOVE = "Odebrat",
            PPM_ITEM_OPEN_IN_EDITOR = "Otevřít", PPM_MENU_COPY_REFERENCE = "Kopírovat", PPM_MENU_STATIC_METHODS =
            "Statické metody",
            PPM_MENU_INHERITED_STATIC_METHODS = "Zděděné statické metody", PPM_MENU_INNER_CLASSES = "Vnitřní statické" +
            " třídy",
            PPM_MENU_CONSTRUCTORS = "Konstruktory",
            PPM_ITEM_OPEN_IN_CODE_EDITOR = "V editoru kódu",
            PPM_ITEM_OPEN_IN_PROJECT_EXPLORER = "V průzkumníku projektů";

    /*
     * Texty pro otevření souboru z diagramu v průzkumníku projektů nastaveného v OS nebo ve výchozí aplikaci
     * nastavené v OS pro otevřené konkrétního typu soubru:
     */
    public static final String PPM_ITEM_OPEN_IN_EXPLORER_IN_OS_TEXT = "V průzkumníku souborů",
            PPM_ITEM_OPEN_IN_EXPLORER_IN_OS_TT = "Otevře průzkumník souborů a označí v něm příslušný soubor.",
            PPM_ITEM_OPEN_IN_DEFAULT_APP_IN_OS_TEXT = "Ve výchozí aplikaci",
            PPM_ITEM_OPEN_IN_DEFAULT_APP_IN_OS_TT = "Otevře označený soubor v aplikaci nastavené v OS pro otevření " +
                    "konkrétního typu souboru.";

    // Texty do položek pro vložení textů do schránky:
    public static final String PPM_ITEM_COPY_ABSOLUTE_PATH = "Absolutní cestu",
            PPM_ITEM_COPY_RELATIVE_PATH = "Relativní cestu",
            PPM_ITEM_COPY_REFERENCE = "Referenci";

    // Texty do hlášek:
    public static final String PPM_JOP_CLASS_IS_IN_RELATIONSHIP_ALREADY_TEXT = "Třída je ve vztahu s jinou třídou, pokud chcete označenou třídu přejmenovat, zruště tento vztah!",
			PPM_JOP_CLASS_IS_IN_RELATIONSHIP_ALREADY_TITLE = "Chyba vztahu",
			PPM_TXT_MISISNG_CLASS_TITLE = "Nepodařilo se nalézt třídu v adresáři src v otevřeném projektu!",
			PPM_TXT_MISING_CLASS_TEXT = "Chybí třída", PPM_TXT_PATH = "Cesta";

	// Texty do výpisů a hlášek pro spuštění - zavolání metody main:
	public static final String PPM_FAILED_TO_CALL_THE_MAIN_METHOD = "Nepodařilo se zavolat metodu 'main'.",
            PPM_TXT_ERROR = "Chyba", PPM_TXT_METHOD_IS_INACCESSIBLE = "Metoda je nepřístupná",
			PPM_TXT_ILLEGAL_ARGUMENT_EXCEPTION = "Objek není instancí třídy nebo rozhraní. Liší se počet parametrů. Nebo selhala konverze parametrů.",
			PPM_TXT_INVOCATION_TARGET_EXCEPTION = "Metoda vyvolala vyjímku, neznámá chyba.", PPM_TXT_START = "Spustit",
            PPM_TXT_FAILED_TO_CREATE_INSTANCE_TEXT = "Nepodařilo se vytvořit instanci třídy",
			PPM_TXT_FAILED_TO_CREATE_INSTANCE_TITLE = "Selhalo vytvoření",
    // Hláška 'Úspěch' po zavolání statické metody:
			PPM_TXT_SUCCESS = "Úspěch",
    // Text: 'Referenční proměnná' info za text proměnné pokud je hodnota instance z
    // ID:
    PPM_TXT_REFERENCE_VARIABLE = "referenční proměnná",
    // V případě, že se nepodaří nalézt cesty pro zkopírování do schránky:
    PPM_TXT_NOT_SPECIFIED = "Neuvedeno";









	// Texty do třídy: RenameClassForm:
    public static final String RCF_DIALOG_TITLE = "Přejmenovat třídu", RCF_LBL_NAME_OF_CLASS = "Název třídy";











	// Výchozí hodnoty pro OutputEditor:
	public static final Font DEFAULT_FONT_FOR_OUTPUT_EDITOR = new Font("Default font for output editor", Font.PLAIN,
			13);
	public static final Color DEFAULT_TEXT_COLOR_FOR_OUTPUT_EDITOR = Color.BLACK,
			DEFAULT_BACKGROUND_COLOR_FOR_OUTPUT_EDITOR = Color.WHITE;

	/**
	 * Logická proměnná, která značí, zda se mají vypisovat referenční proměnné za instanci. Poukd se zavolá třeba metoda v editoru příkazů,
	 * nebo v kontextovém menu nad instancí třídy nebo statická metoda nad třídou nebo sezíská třeba hodnota z pole v editoru příkazů apod.
	 * Tak pokud ta vrácená / získaná hodnota je instance, která se nachází v diagramu instnací, tak se za ní ještě vypíše reference, která na ní
	 * ukazuje, resp. kterou zadal uživatel když tu instanci vytváře.
	 */
	public static final boolean OUTPUT_EDITOR_SHOW_REFERENCE_VARIABLES = true;





	// Výchozí hodnoty pro OutputFrame pro textové pole pro System.out:
	public static final Color DEFAULT_TEXT_COLOR_FOR_SO_OUTPUT_FRAME = Color.BLACK,
			DEFAULT_BACKGROUND_COLOR_FOR_SO_OUTPUT_FRAME = Color.WHITE;
	public static final Font DEFAULT_FONT_FOR_SO_OUTPUT_FRAME = new Font("Default font for System.out output frame", Font.PLAIN, 14);

	// Výchozí hodnoty pro OutputFrame pro textové pole pro System.err:
	public static final Color DEFAULT_TEXT_COLOR_FOR_SE_OUTPUT_FRAME = Color.RED,
			DEFAULT_BACKGROUND_COLOR_FOR_SE_OUTPUT_FRAME = Color.WHITE;
	public static final Font DEFAULT_FONT_FOR_SE_OUTPUT_FRAME = new Font("Default font for System.err output frame",
			Font.PLAIN, 14);
	/**
	 * Vymazání textů v textovém poli pro výpisy z System.out ve výstupním terminálu před zavoláním metody.
	 */
	public static final boolean CLEAR_TEXT_FOR_SO_BEFORE_CALL_METHOD_IN_OUTPUT_FRAME = false;
	/**
	 * Vymazání textů v textovém poli pro výpisy z System.out ve výstupním terminálu před zavoláním konstruktoru.
	 */
	public static final boolean CLEAR_TEXT_FOR_SO_BEFORE_CALL_CONSTRUCTOR_IN_OUTPUT_FRAME = false;
	/**
	 * Zaznamenání hlavičky zavolané metody i s jejími parametry (jsou li) před výpis z System.out ve výstupnim
	 * terminálu.
	 */
	public static final boolean RECORD_CALL_METHODS_IN_SO_IN_OUTPUT_FRAME = true;
    /**
	 * Zaznamenání hlavičky zavolaného konstruktoru i s jeho parametry (jsou li) před výpis z System.out ve výstupnim
     * terminálu.
	 */
	public static final boolean RECORD_CALL_CONSTRUCTORS_IN_SO_IN_OUTPUT_FRAME = true;









	// Výchozí texty do dialogu - třídy: NewInstanceForm (Nif):
	public static final String NIF_DIALOG_TITLE = "Vytvořit instanci objektu",
			NIF_LBL_INFO_TEXT = "Do následujících textových polí zadejte požadované hodnoty pro vytvoření instance.",
			NIF_LBL_REFERENCE_NAME = "Název reference na instanci třídy";

    // Texty do tlačítek:
	public static final String NIF_BTN_OK = "Ok", NIF_BTN_CANCEL = "Zrušit";

	// Texty do chybových hlášek:
	public static final String NIF_FIELD_IS_EMPTY_ERROR_TITLE = "Prázdné pole",
			NIF_FIELD_IS_EMPTY_ERROR_TEXT = "pole pro zadání hodnoty parametru je prázdné!",
			NIF_PARAMETER_FORMAT_ERROR_TITLE = "Chybný formát",
			NIF_PARAMETER_FORMAT_ERROR_TEXT = "parametr konstruktoru není ve správném formátu!",
			NIF_NAME_OF_REFERENCE_IS_ALREADY_TAKEN_TITLE = "Duplicitní název",
			NIF_NAME_OF_REFERENCE_IS_ALREADY_TAKEN_TEXT = "Název reference na instanci objektu je již obsazen, zvolte prosím jiný!",
			NIF_FORMAT_ERROR_REFERENCE_NAME_TITLE = "Chybný formát",
			NIF_FORMAT_ERROR_REFERENCE_NAME_TEXT = "Název reference na instanci objektu není ve správném formátu!",
			NIF_EMTY_FIELD_ERROR_TITLE = "Prázdné pole",
			NIF_EMPTY_FIELD_ERROR_TEXT = "Název reference na instanci třídy není zadán!",
			NIF_BAD_FORMAT_SOME_PARAMETER_TITLE = "Chybný formát",
            NIF_BAD_FORMAT_SOME_PARAMETER_TEXT = "Následující hodnoty jsou zadány ve špatném formátu!",
			NIF_BAD_FORMAT_REFERENCE_NAME = "Název reference na instanci třídy může obsahovat pouze malá, velká písmena bez diakritiky, číslice (nesmí jí začínat) a podtržítka!",
			NIF_CANNOT_CALL_THE_CONSTRUCTOR_TITLE = "Konstruktor nelze zavolat";

	// Texty jsou ve třídě FlexibleCountOfParameters:
	// Texty pro List:
	public static final String FCOP_TXT_LIST_STRING_FORMAT_ERROR = "Hodnoty v listu typu String mohou obsahovat libovolné znaky v normálních (dvojitých) uvozovkách - syntaxe: [\"Můj text.\", \"Další text.\"]",
			FCOP_TXT_LIST_BYTE_FORMAT_ERROR = "Hodnoty v listu typu Byte mohou obsahovat pouze čísla v intervalu",
			FCOP_TXT_LIST_SHORT_FORMAT_ERROR = "Hodnoty v listu typu Short mohou obsahovat pouze čísla v intervalu",
			FCOP_TXT_LIST_INTEGER_FORMAT_ERROR = "Hodnoty v listu typu Integer mohou obsahovat pouze čísla v intervalu",
			FCOP_TXT_LIST_LONG_FORMAT_ERROR = "Hodnoty v listu typu Long mohou obsahovat pouze čísla v intervalu",
			FCOP_TXT_LIST_CHARACTER_FORMAT_ERROR = "Hodnoty v listu typu Character mohou obsahovat libovolné znaky v jednoduchých uvozovkách ('X'), vždy jeden znak v uvozovce, kromě samotné uvozovky.",
			FCOP_TXT_LIST_BOOLEAN_FORMAT_ERROR = "Hodnoty v listu typu Boolean mohou obsahovat pouze logické hodnoty true nebo false.",
			FCOP_TXT_LIST_FLOAT_FORMAT_ERROR = "Hodnoty v listu typu Float mohou obsahovat pouze čísla v intervalu",
			FCOP_TXT_LIST_DOUBLE_FORMAT_ERROR = "Hodnoty v listu typu Double mohou obsahovat pouze čísla v intervalu";


	// Texty pro kolekce:
	public static final String FCOP_TXT__COLLECTION_BYTE_FORMAT_ERROR = "Hodnoty v kolekci typu Byte mohou obsahovat pouze čísla v intervalu",
			FCOP_TXT__COLLECTION_SHORT_FORMAT_ERROR = "Hodnoty v kolekci typu Short mohou obsahovat pouze čísla v intervalu",
			FCOP_TXT__COLLECTION_INTEGER_FORMAT_ERROR = "Hodnoty v kolekci typu Integer mohou obsahovat pouze čísla v intervalu",
			FCOP_TXT__COLLECTION_LONG_FORMAT_ERROR = "Hodnoty v kolekci typu Long mohou obsahovat pouze čísla v intervalu",
			FCOP_TXT__COLLECTION_CHARACTER_FORMAT_ERROR = "Hodnoty v kolekci typu Character mohou obsahovat libovolné znaky v jednoduchých uvozovkách ('X'), vždy jeden znak v uvozovce, kromě samotné uvozovky.",
			FCOP_TXT__COLLECTION_BOOLEAN_FORMAT_ERROR = "Hodnoty v kolekci typu Boolean mohou obsahovat pouze logické hodnoty true nebo false.",
			FCOP_TXT__COLLECTION_FLOAT_FORMAT_ERROR = "Hodnoty v kolekci typu Float mohou obsahovat pouze čísla v intervalu",
			FCOP_TXT__COLLECTION_DOUBLE_FORMAT_ERROR = "Hodnoty v kolekci typu Double mohou obsahovat pouze čísla v intervalu",
			FCOP_TXT__COLLECTION_STRING_FORMAT_ERROR = "Hodnoty v kolekci typu String mohou obsahovat libovolné znaky v normálních (dvojitých) uvozovkách - syntaxe: [\"Můj text.\", \"Další text.\"]";


	// Texty pro jednorozměrné pole:
	public static final String FCOP_TXT_ARRAY_FORMAT_ERROR_FOR_ALL_1 = "Hodnoty v jednorozměrném poli typu",
			FCOP_TXT_ARRAY_FORMAT_ERROR_ALL_2 = "mohou obsahovat pouze čísla v intervalu",
			FCOP_TXT_ARRAY_CHARACTER_FORMAT_ERROR2 = "mohou obsahovat libovolné znaky v jednoduchých uvozovkách ('X'), vždy jeden znak v uvozovce, kromě samotné uvozovky.",
			FCOP_TXT_ARRAY_BOOLEAN_FORMAT_ERROR2 = "mohou obsahovat pouze logické hodnoty typu true nebo false.",
			FCOP_TXT_ARRAY_STRING_FORMAT_ERROR2 = "mohou obsahovat libovolné znaky v normálních (dvojitých) uvozovkách (\"Můj text.\").";

	// Výchozí texty pro pole a list:
	public static final String FCOP_DEFAULT_TEXT_FOR_STRING_LIST_AND_ARRAY_1 = "Můj text.",
			FCOP_DEFAULT_TEXT_FOR_STRING_LIST_AND_ARRAY_2 = "Můj další text.";

	// Hodnoty pro dvourozměrné pole - chybové hlášky ohledně správného formátu syntaxe pro dvourozměrné pole:
	public static final String FCOP_TXT_2_ARRAY_FORMAT_ERROR_ALL_1 = "Hodnoty ve dvourozměrném poli typu";

	// Proměnné - resp. texty do chybového oznámení, pokud se jedná o nepodporovaný datový typ parametru:
	public static final String FCOP_TXT_DATA_TYPE_OF_PARAMETER = "Datový typ parametru",
			FCOP_TXT_IS_NOT_SUPPORTED_FOR_CONSTRUCTOR = "není podporován, konstruktor nelze zavolat!",
			FCOP_TXT_IS_NOT_SUPPORTED_FOR_METHOD = "není podporován, metodu nelze zavolat!",
			FCOP_TXT_CONSTRUCTOR_CANNOT_CALL = "Datové typy více jak jednoho parametru nejsou podporovány, konstruktor nelze zavolat!",
			FCOP_TXT_METHOD_CANNOT_CALL = "Datové typy více jak jednoho parametru nejsou podporovány, metodu nelze zavolat!";

	// Chybové hlášky do editoru výstupů v případě, že se nepodaří vytvořit instanci třídy:
	public static final String FCOP_TXT_FAILED_TO_CREATE_INSTANCE_OF_CLASS = "Nepodařilo se vytvořit instanci třídy",
			FCOP_TXT_POSSIBLE_ERRORS = "nejčastější příčinou může být následující, jedná se o abstraktní třídu či rozhraní, pole, primitivní typ nebo void, nebo nebyl nalezen konstruktor třídy či jiný důvod. Předána null hodnota.",
			FCOP_TXT_POSSIBLE_ERRORS_WITH_PRIVATE_TYPE = "možná příčina může být, že třída nebo její konstruktor není přístupný. Předána null hodnota.",
			FCOP_TXT_INSTANCE_NOT_FOUND_METHOD_CANT_BE_CALL = "Nebyly nalezeny žádné instance pro předání. Metodu nelze zavolat",
			FCOP_TXT_INSTANCE_NOT_FOUND_CONSTRUCTOR_CANT_BE_CALL = "Nebyly nalezeny žádné instance pro předání. Konstruktor nelze zavolat.";













	// Texty do třídy SelectMainMethodForm.java:
	public static final String SMMF_DIALOG_TITLE = "Zavolání metody 'main'";

	// texty do labelů:
	public static final String SMMF_LBL_INFO_1 = "Otevřený projekt obsahuje více hlavních - spouštěcích metod 'main'.",
			SMMF_LBL_INFO_2 = "Níže označte třídu, ve které chcete zavolat metodu main.";

	// Tlačítka:
	public static final String SMMF_BTN_OK = "OK", SMMF_BTN_CANCEL = "Zrušit";

	// hlášky:
    public static final String SMMF_TXT_IS_NOT_CHOOSED_CLASS_TEXT = "Není označena třída, ve které se má zavolat metoda main!",
			SMMF_TXT_IS_NOT_CHOOSED_CLASS_TITLE = "Neoznačená položka";


















	// Výchozí texty do dialogu - třídy GetParametersMethodForm (GPMF):
	public static final String GPMF_DIALOG_TITLE = "Volání metody",
			GPMF_LBL_TEXT_INFO = "Do následujících polí zadejte parametry metody.";

	// Texty do tlačítek:
    public static final String GPMF_BTN_OK = "Ok", GPMF_BTN_CANCEL = "Zrušit";

	// Texty do chybových hlášek v testování hodnot - testData():
	public static final String GPMF_EMPTY_FIELD_PARAMETER_TITLE = "Prázdné pole",
			GPMF_EMPTY_FIELD_PARAMETER_TEXT = "pole pro zadání hodnoty parametru není vyplněné!",
			GPMF_FORMAT_ERROR_PARAMETER_VALUE_TITLE = "Chybný formát",
			GPMF_FORMAT_ERROR_PARAMETER_VALUE_TEXT = "hodnota parametru je ve špatném formátu!",
			GPMF_BAD_FORMAT_SOME_PARAMETER_TITLE = "Chybný formát",
			GPMF_BAD_FORMAT_SOME_PARAMETER_TEXT = "Nějaká hodnota parametru je zadaná ve špatném formátu, viz okno s chybovými hláškami!",
			GPMF_CANNOT_CALL_THE_METHOD = "Metodu nelze zavolat";















	// Výchozí texty do hlášení o kompilaci třídy - texty do třídy: CompileClass.java (Cp):
	public static final String CP_TXT_ERROR = "Chyba", CP_TXT_LINE = "Řádek", CP_TXT_CLASS = "Třída",
			CP_TXT_SOURCE_NOT_FOUND = "Zdroj nenalezen (získáno null).",
			CP_TXT_CLASS_NOT_FOUND = "Třída na zadané cestě nebyla nalezena, nelze zkompilovat!", CP_TXT_PATH = "Cesta";

	public static final String CP_ALL_CLASSES_ARE_COMPILED = "Veškeré třídy z diagramu tříd jsou zkompilované.",
			CP_ALL_CLASSES_ARE_NOT_COMPILED = "Alespoň jednu třídu z diagramu tříd se nepodařilo zkompilovat, níže následuje přehled chyb.",
			CP_CLASSES_ARE_NOT_FOUND = "Nebyly předány soubory, resp. třídy z diagramu tříd, nelze je zkompilovat!",
			CP_CLASS_DIAGRAM_IS_EMPTY_FOR_COMPILATION = "V diagramu tříd se nenachází žádná třída ke kompilaci!";


	// Texty k chybě při vytváření kompilátoru - možné příčiny:
	public static final String CP_TXT_ERROR_WHILE_CREATEING_COMPILATOR_TEXT = "Nastala chyba při vytváření \"kompilátoru\"!",
			CP_TXT_POSSIBLE_ERRORS = "Tato chyba je nejspíše způsobena jednou z následujících možností",
			CP_TXT_IS_NOT_SET_PATH_TO_JAVA_HOME_DIR = "Není nastavena cesta k domovskému adresáři Javy.",
			CP_TXT_MISSING_TOOLS_JAR_FILE = "Výše zmíněný adresář neobsahuje soubor tools.jar nebo se nejedná o jeho aktuální verzi.",
			CP_TXT_PROBABLY_MISSING_FILES_IN_LIB_TOOLS = "Pokud cesta k adresáři Javy směřuje k adresáři libTools v aplikaci, nejspíše se jedná o podobný problém, tj. Není uveden nebo se nejedná o aktuální verzi souboru tools.jar.",
			CP_TXT_MISSING_OR_DEPRECATED_FILES = "Na uvedené cestě k domovskému adresáři Javy nebo v adresáři libTools v aplikaci (záleží co je vybráno) nejsou umístěny nebo jsou zastaralé minimálně soubory: currency.data, flavormap.properties a nebo tools.jar.",
			CP_TXT_ERROR_WHILE_CREATING_COMPILATOR_TITLE = "Chyba kompilátoru", CP_TXT_ACTUAL_PATH = "Aktuální cesta",
			CP_TXT_UNKNOWN = "Neuvedeno", CP_TXT_JAVA_SUPPORTED_VERSIONS = "Podporované verze Javy";















	// Výchozí texty do chybových hlášek do Jlabelů ve třídě: FlexibleCountOfParameters.java
	public static final String FCOP_BYTE_FORMAT_ERROR = "Číslo typu byte může obsahovat pouze hodnoty v intervalu: -128 až +127.",
			FCOP_SHORT_FORMAT_ERROR = "Číslo typu short může obsahovat pouze hodnoty v intervalu: -32768 až +32767.",
			FCOP_INT_FORMAT_ERROR = "Číslo typu int může obsahovat pouze hodnoty v intervalu: -2147483648 až +2147483647.",
			FCOP_LONG_FORMAT_ERROR = "Číslo typu long může obsahovat pouze hodnoty v intervalu: -9223372036854775808 až +9223372036854775807.",
			FCOP_FLOAT_FORMAT_ERROR = "Číslo typu float může obsahovat pouze hodnoty v intervalu: -3.40282e+38 až +3.40282e+38.",
			FCOP_DOUBLE_FORMAT_ERROR = "Číslo typu double může obsahovat pouze hodnoty v intervalu: -1.79769e+308 až +1.79769e+308.",
			FCOP_CHAR_FORMAT_ERROR = "Znak typu char může obsahovat pouze jeden znak v syntaxi: 'X'.",
			FCOP_STRING_FORMAT_ERROR = "Text typu String může obsahovat libovolné znaky v normálních (dvojitých) uvozovkách - syntaxe: \"Můj text.\".",
			FCOP_DEFAULT_VALUE_FOR_STRING_FIELD = "Můj text.",
			FCOP_LBL_TEXT_INFO_PARAM = "Zadejte hodnotu parametru typu",
			FCOP_ERROR_TEXT_FIELD_CONTAINS_ERRROR = "pole obsahuje chybu.", FCOP_ERROR_TEXT_FOR_ERROR_LIST = "Chyby",
			FCOP_PNL_MISTAKES_BORDER_TITLE = "Informace o chybách", FCOP_CHCB_WRAP_LYRICS = "Zalamovat texty.";














    // Texty do menu - PopupMenu - po kliknutí pravým tlačítkem na instanci třídy - v instance Diagramu: (Id_pm = InstanceDiagram PopupMenu)
	public static final String ID_PM_MENU_ITEM_INHERITED_METHODS = "Zděděno z",
            ID_PM_MENU_ITEM_INHERITED_METHODS_FROM_ANCESTORS = "Zděděné metody", ID_PM_ITEM_REMOVE = "Odebrat",
			ID_PM_ITEM_VIEW_VARIABLES = "Prohlížet";

	// Hláška pro úspěšné zavolání metody pomocí kontextového menu nad buńkou
	// reprezentující třídu v diagramu instancí:
	public static final String ID_PM_TXT_SUCCESS = "Úspěch";















	// Texty do třídy: VariablesInstanceOfClass.java: (VIOC)
    // Titulek dialogu a ohraničení:
    public static final String VIOC_DIALOG_TITLE = "Prohlížení instance třídy",
			VIOC_PNL_CONSTUCTORS_BORDER_TITLE = "Přehled konstruktorů instance třídy",
            VIOC_PNL_METHODS_BORDER_TITLE = "Přehled metod instance třídy",
			VIOC_PNL_VARIABLES_BORDER_TITLE = "Přehled proměnných instance třídy",
			VIOC_CHCB_INCLUDE_INHERITED_VALUES = "Zahrnout hodnoty z předků.",
			VIOC_CHCB_INCLUDE_INNER_CLASSES_VALUES = "Zahrnout hodnoty z vnitřních tříd.",
			VIOC_CHCB_WRAP_VALUES_IN_JLISTS = "Zalamovat dlouhé texty.",
			VIOC_CHCB_INCLUDE_INTERFACE_METHODS = "Zahrnout metody z rozhraní.";

	// Texty do okna:
    public static final String VIOC_LBL_CLASS_INFO_1 = "Prohlížení instance třídy",
			VIOC_LBL_CLASS_INFO_2 = "s názvem reference", VIOC_CONSTRUCTOR_TEXT = "konstruktoru",
			VIOC_METHOD_TEXT = "metody (operace)", VIOC_VARIABLE_TEXT = "proměnné (atributu)";

	// Texty do tlačítek:
    public static final String VIOC_BTN_CLOSE = "Zavřít", VIOC_BTN_SEARCH_CONSTRUCTOR = "Vyhledat konstruktor",
            VIOC_BTN_CALL_CONSTRUCTOR = "Zavolat konstruktor", VIOC_BTN_SEARCH_METHOD = "Vyhledat metodu",
            VIOC_BTN_SEARCH_VARIABLE = "Vyhledat proměnnou", VIOC_BTN_CREATE_REFERENCE = "Vytvořit referenci",
            VIOC_BTN_CALL_METHOD = "Zavolat metodu", VIOC_TXT_SUCCESS = "Úspěch";

    /*
     * Texty do kontextového menu, které se zobrazí po kliknutí pravým tlačítkem myši na položku v JListu v dialogu s
     * přehledem hodnot o zvolené instanci v diagramu instancí:
     *
     * Třída: cz.uhk.fim.fimj.instance_diagram.overview_of_instance.PopupMenu
     */
    public static final String OOI_PM_ITEM_COPY = "Kopírovat referenci";













	// Texty do třídy: SearchForm.java:  (SForm)
    public static final String SFORM_DILOG_TITLE = "Dialog pro vyhledání";
	public static final String SFORM_LBL_INFO = "Do vyhledávacího pole níže zadejte řetězec pro vyhledání";

    // Texty do tlačítek:
	public static final String SFORM_BTN_CLOSE = "Zavřít", SFORM_BTN_SEARCH = "Vyhledat";

    // Titulek ohraničení nalezených výrazů:
	public static final String SFORM_JSP_SEARCHED_TEXT = "Nalezená shoda řetězce";


















	// Třída JavaHomeForm.java: - texty: (JHF - Java Home Form)
	// RadioButtony:
    public static final String JHF_DIALOG_TITLE = "Nastavení",
			JHF_RB_SET_JAVA_HOME_DIR = "Vybrat umístění domovského adresáře Javy.",
			JHF_RB_COPY_SOME_FILES = "Vybrat soubory potřebné ke kompilaci.",
			JHF_RB_SET_DEFAULT = "Nastavit jako domovský adresář výchozí adresář v aplikaci";

	// Popisky do labelů:
	public static final String JHF_LBL_JAVA_HOME_DIR_INFO = "Vyberte umístění domovského adresáře Javy",
            JHF_LBL_JAVA_HOME_LIB_DIR_INFO = "Označený adresář musí obsahovat adresář 'lib', který musí obsahovat alespoň soubory",
			JHF_LBL_COPY_SOME_FILES_INFO = "Vyberte soubory, které se mají zkopírovat do adresáře";

	// Tlačítka:
	public static final String JHF_BTN_SELECT_JAVA_HOME_DIR = "Vybrat umístění", JHF_BTN_OK = "OK", JHF_BTN_CANCEL = "Zrušit";

    // Text pro výběr textu:
	public static final String JHF_TEXT_CHOOSE_TEXT = "Vybrat umístění", JHF_TEXT_CHOOSE_FILE = "Vyberte soubor";

    // Texty do hlášek:
	public static final String JHF_TXT_FILES_NOT_FOUND_TEXT = "Následující soubor (/y) nebyly nalezeny",
            JHF_TXT_FILES_NOT_FOUND_TITLE = "Soubory nenalezeny",
			JHF_TXT_BAD_FORMAT_CLASS_PATH_TEXT = "Alespoň jedna cesta k souboru je zadána ve špatném formátu!",
			JHF_TXT_BAD_FORMAT_CLASS_PATH_TITLE = "Chybný formát",
            JHF_TXT_IS_NEEDED_TO_FILL_PATH_TEXT = "Je třeba vyplnit cestu k alespoň jednomu souboru!",
			JHF_TXT_IS_NEEDED_TO_FILL_PATH_TITLE = "Prázdná pole",
			JHF_TXT_MISSING_LIB_OR_IMPORTANT_FILES_TEXT = "Ve zvoleném adresáři pro Javu nebyl nalezen adresář lib, nebo v něm nebyl nalezen alespoň jeden z následujících souborů",
			JHF_TXT_MISSING_LIB_OR_IMPORTANT_FILES_TITLE = "Chybí soubor (/y)",
            JHF_TXT_CHOOSED_DIR_DOES_NOT_EXIST_TEXT = "Vybraný adresář již neexistuje, zvolte prosím jiný!",
			JHF_TXT_CHOOSED_DIR_DOES_NOT_EXIST_TITLE = "Adresář neexistuje",
			JHF_TXT_PATH_IS_NOT_IN_CORRECT_FORMAT_TEXT = "Cesta není ve správném formátu!",
			JHF_TXT_PATH_IS_NOT_IN_CORRECT_FORMAT_TITLE = "Chybný formát",
			JHF_TXT_PATH_TO_HOME_DIR_IS_EMPTY_TEXT = "Pole pro cestu k domovskému adresáři Javy není vyplněné!",
			JHF_TXT_PATH_TO_HOME_DIR_IS_EMPTY_TITLE = "Prázdné pole";
























	// Texty do třídy: SaveCodeAfretCloseDialog.java
	// Texty do chybových hlášek v této třídě:
	public static final String SCACD_SAVE_CHANGES_TEXT = "Chcete uložit změny ve zdrojovém kódu třídy?",
			SCACD_SAVE_CHANGES_TITLE = "Uložit změny", SCACD_PATH = "Cesta",
			SCACD_FAILED_TO_WRITE_CODE_TO_CLASS_TEXT = "Nepodařilo se zapsat v dialogu upravený zdrojový kód zpět do třídy!",
			SCACD_FAILED_TO_WRITE_CODE_TO_CLASS_TITLE = "Chyba uložení kódu",
			SCACD_CLASS_NOT_FOUND_TITLE = "Třída nenalezena", SCACD_CLASS_NOT_FOUND_TEXT = "Třída nebyla nalezena!",
			SCAD_FAILED_TO_LOAD_CLASS_TEXT = "Nepodařilo se načíst kód původní třídy!",
			SCAD_FAILED_TO_LOAD_CLASS_TITLE = "Třída nenalezena";









	// Text do editoru příkazů - CommandEditor (Ce) - tak je potřeba akorát vložit text pokud nebyla rozpoznána syntaxe:
	public static final String CE_SYNTAX_IS_NOT_RECOGNIZED = "Zadaná syntaxe nebyla rozpoznána";
	public static final String CE_REFERENCE_VARIABLE = "referenční proměnná";
















	// Texty do dialogu pro editor kódu (Code Editor Dialog = CED):




	// Texty to třídy: JmenuForCodeEditor.java - texty do menu v tomto dialogu:
    public static final String MFCE_MENU_CLASS = "Třída", MFCE_MENU_EDIT = "Upravit", MFCE_MENU_TOOLS = "Nástroje",
            MFCE_MENU_CLASS_MENU_OPEN_FILE_IN_OS = "Otevřít";

	// Texty do položek v menu MenuClass:
	public static final String MFCE_MENU_CLASS_ITEM_SAVE = "Uložit", MFCE_MENU_CLASS_ITEM_RELOAD = "Znovu načíst",
			MFCE_MENU_CLASS_ITEM_PRINT = "Tisk", MFCE_MENU_CLASS_ITEM_CLOSE = "Zavřít";


	// Texty do položek v menu pro otevření otevřeného souboru v aplikaci OS nebo v průzkumníku souborů v OS:
    public static final String MFCE_ITEM_OPEN_FILE_IN_FILE_EXPLORER = "V průzkumníku souborů",
            MFCE_ITEM_OPEN_FILE_IN_DEFAULT_APP_IN_OS = "Ve výchozí aplikaci";



	// Texty do položek v menu MenuEdit:
    public static final String MFCE_MENU_EDIT_ITEM_CUT = "Vyjmout", MFCE_MENU_EDIT_ITEM_COPY = "Kopírovat",
			MFCE_MENU_EDIT_ITEM_PASTE = "Vložit", MFCE_MENU_EDIT_ITEM_INDENT_MORE = "Odsadit více",
            MFCE_MENU_EDIT_ITEM_INDENT_LESS = "Odsadit méně", MFCE_MENU_EDIT_ITEM_COMMENT = "Zakomentovat",
            MFCE_MENU_EDIT_ITEM_UNCOMMENT = "Odkomentovat",
			MFCE_MENU_EDIT_ITEM_FORMAT_CODE = "Zformátovat kód",
            MFCE_MENU_EDIT_ITEM_INSERT_METHOD = "Vložit metodu";

    // Texty do položek v menu MenuTools:
    public static final String MFCE_MENU_TOOLS_ITEM_REPLACE = "Nahradit",
            MFCE_MENU_TOOLS_ITEM_GO_TO_LINE = "Jdi na řádek", MFCE_MENU_TOOLS_ITEM_COMPILE = "Zkompilovat",
            MFCE_MENU_TOOLS_RADIO_BUTTON_WRAP_TEXT = "Zalamovat text",
            MFCE_MENU_TOOLS_RADIO_BUTTON_SHOW_WHITE_SPACE = "Zobrazit bílé znaky",
            MFCE_MENU_TOOLS_RADIO_BUTTON_CLEAR_WHITE_SPACE_LINE = "Odebírat bílé znaky",
            MFCE_MENU_TOOLS_ITEM_GETTERS_AND_SETTERS = "Generovat getry a setry",
            MFCE_MENU_TOOLS_ITEM_CONSTRUCTOR = "Generovat konstruktor",
            MFCE_MENU_TOOLS_MENU_ADD_SERIAL_ID = "Přidat serial ID",
            MFCE_MENU_TOOLS_ITEM_ADD_DEFAULT_SERIAL_UID = "Vložit výchozí serial version ID",
            MFCE_MENU_TOOLS_ITEM_ADD_GENERATED_SERIAL_UID = "Vygenerovat nové serial version ID";

	// Popisky jednotlivých tlačítek v menu: menuClass:
    public static final String MFCE_TOOL_TIP_ITEM_SAVE = "Uloží aktuální zdrojový kód z dialogu zpět do třídy.",
			MFCE_TOOL_TIP_ITEM_RELOAD = "Znovu načte zdrojový kód třídy do dialogu editoru zdrojového kódu.",
            MFCE_TOOL_TIP_ITEM_PRINT = "Vytiskne zdrojový kód v tomto editoru zdrojového kódu.",
			MFCE_TOOL_TIP_ITEM_CLOSE = "Zavře tento dialog s editorem zdrojového kódu.";

	// Popsiky tlačítek v menu: menuOpenFileInAppsOfOs:
    public static final String MFCE_TOOL_TIP_ITEM_OPEN_FILE_IN_FILE_EXPLORER_IN_OS = "Zobrazí otevřený soubor v " +
            "průzkumníku souborů.", MFCE_TOOL_TIP_ITEM_OPEN_FILE_IN_DEFAULT_APP_IN_OS = "Otevřený soubor bude otevřen v " +
            "aplikaci nastavené dle OS pro otevření příslušného typu souboru.";

    // Popisky jednotlivých tlačítek v menu: menuEdit:
	public static final String MFCE_TOOL_TIP_ITEM_CUT = "Vyjme označenou část zdrojového kódu a vloží jej do schránky.",
            MFCE_TOOL_TIP_ITEM_COPY = "Zkopíruje označenou část zdrojového kódu a vloží jej do schránky.",
			MFCE_TOOL_TIP_ITEM_PASTE = "Vloží zdrojový kód ze schránky na pozici kurzoru v editoru zdrojového kódu.",
            MFCE_TOOL_TIP_ITEM_INDENT_MORE = "Odsadí začátek řádku, na kterém se nachází kurzor pomocí tabulátoru.",
			MFCE_TOOL_TIP_ITEM_INDENT_LESS = "Odebere tabulátor ze začátku řádku, na kterém se nachází kurzor.",
            MFCE_TOOL_TIP_ITEM_COMMENT = "Vloží řádkový komentář na řádek, na kterém se nachází kurzor. (\"Zakomentuje řádek, na kterém se nachází kurzor.\")",
			MFCE_TOOL_TIP_ITEM_UNCOMMENT = "Odebere řádkový komentář z řádku, na kterém se nachází kurzor. (\"Odkomentuje řádek, na kterém se nachází kurzor.\")",
            MFCE_TOOL_TIP_ITEM_FORMAT_CODE = "Zformátuje zdrojový kód v editoru dle Javovské syntaxe.",
			MFCE_TOOL_TIP_ITEM_INSERT_METHOD = "Vloží \"ukázkovou\" metodu, do části editoru zdrojového kódu, kde se nachází kurzor.";

	// Popisky jednotlivých tlačítek v menu: menuTools:
    public static final String MFCE_TOOL_TIP_ITEM_REPLACE = "Zobrazí dialog, ve kterém lze vybrat, zda se má nahradit první výskyt daného textu nebo veškeré výskyty daného textu v editoru zdrojového kódu.",
			MFCE_TOOL_TIP_ITEM_GO_TO_LINE = "Vloží kurzor na řádek, se zadaným číslem.",
            MFCE_TOOL_TIP_ITEM_COMPILE = "Zkompilují se třídy, které se nachází v diagramu tříd.",
			MFCE_TOOL_TIP_ITEM_TO_STRING = "Vloži na pozici kurzoru metodu toString().",
            MFCE_TOOL_TIP_ITEM_GETTERS_AND_SETTERS = "Otevře dialog pro výběr položek (proměnných) třídy, ke kterým je možné vložit přístupovou metodu (tj. getr nebo setr).",
			MFCE_TOOL_TIP_ITEM_CONSTRUCTOR = "Otevře dialog, ve kterém je možno označit položky (proměnné), které je možné přidat do parametru konstruktoru.",
			MFCE_TOOL_TIP_ITEM_ADD_DEFAULT_SERIAL_UID = "Vloží se na pozici kurzoru výchozí serial version ID.",
			MFCE_TOOL_TIP_ITEM_ADD_GENERATED_SERIAL_UID = "Vloží se na pozici kurzoru nově vygenerované serial version ID.";

    // Texty do chybových hlášek:
	public static final String NFCE_JOP_CLASS_NOT_FOUND_TEXT = "Nebyla nalezena třída, jejiž kód je načten v editoru!",
			NFCE_JOP_CLASS_NOT_FOUND_TITLE = "Třída nenalezena", NFCE_TXT_PATH = "Cesta",
            NFCE_JOP_SAVE_CHANGES_TEXT = "Chcete uložit změny?", NFCE_JOP_SAVE_CHANGES_TITLE = "Uložit změny",
			NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TEXT_1 = "Nepodařilo se zapsat zdrojový kód z editoru kódu zpět do třídy!",
			NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TEXT_2 = "Kód nebyl zapsán!",
			NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TITLE = "Chyba při zápisu",
			NFCE_JOP_CLASS_DOES_NOT_IMPLEMENT_INTERFACE_OR_ERROR_TEXT = "Třída neimplementuje rozhraní Serializable nebo nedědí ze třídy, která jej implementuje nebo obsahuje chybu.",
			NFCE_JOP_CLASS_DOES_NOT_IMPLEMENT_INTERFACE_OR_ERROR_TITLE = "Třída neimplementuje Serializable",
            NFCE_TXT_COMMENT_ON_THE_METHOD = "Komentář k metodě, například jaký je její účel, co obsahuje její tělo, postup algoritmu v metodě, ...",
			NFCE_TXT_PARAMETER_DESCRIPTION = "Stručný popis parametru metody.",
            NFCE_TXT_RETURN_VALUE = "Stručný popis návratové hodnoty metody.",
			NFCE_TXT_PUT_YOUR_CODE_HERE = "Zde můžeš vložit svůj vlastní kód.",
			NFCE_TXT_RETURN_STRING_VALUE = "Zde můžeš zadat, jakou hodnotu typu String má metoda vracet.",
			NFCE_TXT_COULD_NOT_LOAD_CLASS_FILE = "Nepodařilo se načíst přeloženou třídu, resp. soubor '.class' zkompilované třídy!",
			NFCE_TXT_CLASS = "Třída", NFCE_TXT_FILE_NOT_FOUND = "Soubor nenalezen",
            MFCE_FILE_DOES_NOT_EXIST_IN_PATH_TEXT = "Soubor na zadané cestě již neexistuje!",
			MFCE_FILE_DOES_NOT_EXIST_IN_PATH_TITLE = "Chybí soubor";









	// Texty do formuláře pro přesun kurzoru na zadané číslo řádku (formálář: GoToLineForm = GTLF):
    public static final String GTLM_TITLE_DIALOG = "Jdi na řádek", GTLM_LBL_LINE = "Jdi na řádek číslo",
			GTLM_LBL_ERROR_TEXT = "Číslo řádku je zadáno ve špatném rozsahu nebo formátu!", GTLM_BTN_OK = "OK",
            GTLM_BTN_CANCEL = "Zrušit",
			GTLM_LINE_NUMBER_IS_NOT_CORRECT_TEXT = "Číslo řádku není zadáno ve správné syntaxi či rozsahu!",
            GTLM_LINE_NUMBER_IS_NOT_CORRECT_TITLE = "Chybný formát nebo rozsah",
			GTLM_EMPTY_FIELD_TEXT = "Není zadáno číslo řádku pro přesun kurzoru!",
            GTLM_EMPTY_FIELD_TITLE = "Prázdné pole";



	// Texty do třídy: ReplaceTextInCodeEditorForm.java (RTICE)
    public static final String RTICE_DIALOG_TITLE = "Nahradit text v kódu",
            RTICE_LBL_INFO = "Níže zadejte řetězec, který chcete nahradit a řetězec, kterým jej chcete nahradit.",
            RTICE_LBL_MISSING_VALUE = "Zdrojový kód neobsahuje zadaný výraz!", RTICE_LBL_FIND = "Najít",
            RTICE_LBL_REPLACE = "Nahradit", RTICE_BTN_OK = "OK", RTICE_BTN_CANCEL = "Zrušit",
            RTICE_CHCB_REPLACE_FIRST = "Nahradit první výskyt řetězce.",
            RTICE_CHCB_REPLACE_ALL = "Nahradit veškeré výskyty řetězce.",
            RTICE_TXT_CODE_DONT_CONTAINS_STRING = "Zdrojový kód neobsahuje zadaný řetězec!",
            RTICE_TEXT_STRING = "Řetězec", RTICE_MISSING_VALUE = "Chybí hodnota",
            RTICE_EMPTY_FIELD_TITLE = "Prázdné pole", RTICE_EMPTY_FIELD_TEXT = "Pole pro nalezení řetězce je prázdne!";




	// Texty do třídy: GenerateConstructorUsingFieldsForm.java:  (GCUFF)
	public static final String GCUFF_DIALOG_TITLE = "Generovat konstruktor s parametry",
            GCUFF_LBL_INFO = "Označte položky, které chcete přidat do parametru konstruktoru.", GCUFF_BTN_OK = "OK",
            GCUFF_BTN_CANCEL = "Zrušit", GCUFF_BTN_SELECT_ALL = "Označit vše", GCUFF_BTN_DESELECT_ALL = "Odznačit vše",
            GCUFF_CHCB_GENERATE_DOC_DOCUMENT = "Vygenerovat dokumentační komentář ke konstruktoru.",
            GCUFF_PNL_CHCB_MODIFIER = "Modifikátory přístupu";


    // Texty do třídy: GenerateAccessmethodsform.java (GAMF):
    public static final String GAMF_DIALOG_TITLE = "Vygenerovat getry a setry",
            GAMF_LBL_INFO = "Označené \"přístupové metody\" (getry a setry) budou vytvořeny.",
            GAMF_BTN_SELECT_ALL = "Označit vše", GAMF_BTN_DESELECT_ALL = "Odznačit vše",
            GAMF_CHCB_GENERATE_COMMENT = "Vygenerovat dokumentační komentář k označeným metodám.", GAMF_BTN_OK = "OK",
            GAMF_BTN_CANCEL = "Zrušit", GAMF_PNL_MODIFIERS_BORDER_TITLE = "Modifikátory přístupu";


    // Texty do třídy: CodeEditor.java (CodeEdit):
    public static final String CODE_EDIT_LOOP_FOR_SUMMARY = "Ukázka cyklu for.",
            CODE_EDIT_PREPARED_LOOP_SUMMARY = "Připravený cyklus for.",
            CODE_EDIT_LOOP_WHILE_SUMMARY = "Ukázka cyklu while.", CODE_EDIT_LOOP_DO_WHILE_SUMMARY = "Ukázka cyklu " +
            "do-while.", CODE_EDIT_LOOP_FOR_REACH = "Ukázka cyklu foreach.",
            CODE_EDIT_LOOP_USING_LAMBDA = "Ukázka cyklu for použitím lambda výrazů.", CODE_EDIT_LOOP_INT_STREAM =
            "Ukázka cyklu IntStream.", CODE_EDIT_INFINITE_WHILE_LOOP = "Ukázka nekonečného cyklu while.",
            CODE_EDIT_PUT_YOUR_CODE = "Zde vložte svůj kód...", CODE_EDIT_LOOP_FOR_INFO = "Cyklus for",
            CODE_EDIT_LOOP_WHILE_INFO = "Cyklus while", CODE_EDIT_LOOP_DO_WHILE_INFO = "Cyklus do-while",
            CODE_EDIT_LOOP_FOREACH_INFO = "Cyklus foreach",
            CODE_EDIT_LOOP_WITH_LAMBDA_INFO = "Cyklus za použití lambda výrazů", CODE_EDIT_LOOP_INT_STREAM_INFO =
            "Cyklus IntStream", CODE_EDIT_INFINITE_WHILE_LOOP_INFO = "Nekonečný cyklus while";

    // Přístupové metody:
    public static final String CODE_EDIT_GENERATE_SETTER = "Vygenerovat setr", CODE_EDIT_GENERATE_GETTER =
            "Vygenerovat " +
            "getr";

    public static final String CODE_EDIT_TXT_WITH_VARIABLE = "S proměnnou", CODE_EDIT_TXT_WITHOUT_VARIABLE = "Bez " +
            "proměnné";













	// Veškeré texty do třídy: MakeOperation.java (MO) - veškeré texty - výpisy do editrou, apod
	// Texty do výpisů v editoru výstupů apod...
	public static final String //MO_TXT_DONT_SHARE_BY_ZERO = "Nelze dělit nulou!",
            //MO_TXT_NUMBER_FORMAT_EXCEPTION = "Početní operace byla zadána ve špatném formátu!",
            MO_TXT_SUCCESS = "Úspěch", MO_TXT_FAILURE = "Neúspěch",
			MO_TXT_INSTANCE_NOT_FOUND = "Nebyla nalezena instance třídy pod návem reference",
            MO_TXT_METHOD_NOT_FOUND = "Metoda nebyla nalezena!",
			MO_TXT_BAD_NAME_OF_METHOD_OR_PARAMETER = "Chybný název metody nebo typ parametru!",
            MO_TXT_REFERENCE_NAME_NOT_FOUND = "Reference na instanci nějaké třídy neexistuje!",
			MO_TXT_FAILED_RETYPE_VALUE = "Zadanou hodnotu nelze vložit do proměnné příslušného typu!",
			MO_TXT_MISSING_VARIABLE_IN_INSTANCE_OF_CLASS = "Nepodařilo se najít zadanou proměnnou v instanci třídy!",
			MO_TXT_ERROR_IN_VARIABLE_NAME_OF_VISIBILITY_VARIABLE = "Chyba názvu proměnne třídy nebo 'viditelnosti'!",
			MO_TXT_INSTANCE_OF_CLASS_NOT_FOUND = "Instance třídy, na kterou ukazuje zadaná proměnná neexistuje!",
            MO_TXT_AT_LEAST_ONE_INSTANCE_OF_CLASS_NOT_FOUND = "Alespoň jedna instance nějaké třídy, na kterou má ukazovat zadaná reference neexistuje!",
			MO_TXT_DATA_TYPES_ARE_NOT_EQUAL = "Datové typy se nerovnají",
            MO_TXT_ERROR_METHOD_OR_PARAMETERS_OR_MODIFIER = "Zadaná metoda v dané instancí třídy neexistuje, liší se počty parametrů, nebo není veřejná! (tj. Není označena modifikátorem public, případně protected.)",
			MO_TXT_VARIABLE_NOT_FOUND_IN_REFERENCE_TO_INSTANCE_OF_CLASS = "V instanci třídy, na kterou ukazuje zadaná reference nebyla nalezana hledaná proměnná!",
			MO_TXT_FAILED_TO_FILL_VARIABLE = "Nepodařilo se naplnit proměnnou!",
            MO_TXT_POSSIBLE_ERRORS_IN_FILL_VARIABLE = "Možné chyby: Objekt není instancí třídy rozhraní či podtřídy, apod.",
            MO_TXT_ERRORS_IMMUTABLE_VISIBILITY_PUBLIC = "Zadaná proměnná je buď neměnná - 'final' nebo není viditelná, tj. označená modifikátorem public!",
			MO_TXT_VARIABLE_NAME = "Název proměnné", MO_TXT_ALREADY_EXIST = "již existuje, zvolte prosím jiný!",
            MO_TXT_SRC_DIR_NOT_FOUND = "Nepodařilo se najít cestu k adresáři src v otevřeném projektu!",
			MO_TXT_CLASS_NOT_FOUND = "Třída nebyla nalezena!",
			MO_TXT_FILED_TO_LOAD_CLASS_FILE = "Nepodařilo se načíst soubor '.class'!",
			MO_TXT_DATA_TYPES_OR_CONSTRUCTOR_NOT_FOUND = "Nepodařilo se najít správné typy hodnot nebo konstuktor!",
            TXT_ERROR_WHILE_CALLING_STATIC_METHOD_1 = "Metodu se nepodařilo zavolat. Ve třídě",
			TXT_ERROR_WHILE_CALLING_STATIC_METHOD_2 = "nebyla nalezena metoda se zadaným názvem nebo parametry, popřípadě se nejedná o statickou metodu.";


	// Veškeré výpisy do editoru výstupů:
	public static final String MO_TXT_OVER_VIEW_OF_COMMANDS = "Přehled příkazů, které je schopen editor příkazů rozpoznat",
			MO_TXT_OVERVIEW_OF_COMMANDS_EDITOR_VARIABLES = "Přehled vytvořených proměnných v editoru příkazů je možné vypsat příkazem",
			MO_TXT_ERROR_IN_INCREMENT_VALUE = "Nepodařilo se inkrementovat číslo",
			MO_TXT_NUMBER_OUT_OF_INTERVAL = "Nejspíše se jedná o číslo mimo rozsah",
			MO_TXT_ERROR_IN_DECREMENT_VALUE = "Nepodařilo se dekrementovat číslo",
			MO_TXT_VARIABLE_DONT_CONTAINS_NUMBER = "Proměnná neobsahuje celé nebo reálné číslo!",
            MO_TXT_FAILED_TO_CAST_NUMBER = "Hodnotu proměnné se nepodařilo přetypovat na číslo!",
			MO_TXT_REFERENCE_DONT_POINTS_TO_INSTANCE = "Zadaná reference neukazuje na instanci třídy či rozhraní deklarující zadanou položku (proměnnou), neboli zadaný objekt není instancí třídy či rozhraní obsahující danou položku (proměnnou)!",
            MO_TXT_VARIABLE_IS_NOT_PUBLIC_OR_IS_FINAL = "Položka není veřejně přístupná, tj. Není označena modifikátorem public, nebo je 'final' - neměnná!",
			MO_TXT_REFERENCE_DOUNT_POINTS_TO_INSTANCE_OF_CLASS = "Zadaná reference neukazuje na instanci žádné třídy!",
            MO_TXT_REFERENCE = "Reference", MO_TXT_VARIABLE = "Proměnná", MO_TXT_ERROR = "Chyba",
			MO_TXT_METHOD_WILL_NOT_BE_INCLUDE = "Metoda nebude započítána do výsledné hodnoty proměnné!",
			MO_TXT_METHOD = "Metoda",
            MO_TXT_METHOD_HAS_NOT_BEEN_CALLED = "Metoda nebyla zavolána, jeji hodnota nebude započítána!",
			MO_TXT_REFERENCE_DOES_NOT_POINTS_TO_INSTANCE_OF_CLASS = "Zadaná reference neukazuje na žádnou instanci třídy!",
			MO_TXT_VALUE_WILL_NOT_BE_COUNTED = "Hodnota nebude započítána!",
			MO_TXT_VALUE_OF_VARIABLE_WILL_NOT_BE_COUNTED = "Hodnota proměnné nebude do výsledné hodnoty započítána!",
			MO_TXT_DATA_TYPE_OF_FIELD = "Datový typ položky",
			MO_TXT_DATA_TYPE_IS_DIFFERENT = "je jiný než datový typ proměnné, do které má být hodnota vložena!",
			MO_TXT_INSTANCE = "Instance",
			MO_TXT_ERROR_WHILE_CONVERTING_VALUES = "Nastala chyba při konverzi zadané hodnoty",
			MO_TXT_TO_TYPE = "na typ",
			MO_TXT_VARIABLE_NAME_MUST_BE_KEY_WORD = "Název proměnné nesmí být klíčové slovo Javy!",
			MO_TXT_VALUE_IS_FINAL_AND_CANNOT_BE_CHANGED = "je 'final', její hodnotu nelze změnit!",
			MO_TXT_ERROR_IN_CLASS_LOADER_OR_ACCESS_TO_FIELD = "Možné chyby, chyba ClassLoaderu, nebo se nepodařilo získat přístup k položce!",
            MO_TXT_DO_NOT_FOUND = "nebyla nalezena!", MO_TXT_FAILED_TO_CAST_VALUE = "Nepodařilo se přetypovat hodnotu",
			MO_TXT_TO_DATA_TYPE = "na datový typ",
			MO_TXT_ERROR_WHILE_PARSING_VALUE = "Nastala chyba při parsování hodnoty",
			MO_TXT_PROBABLY_IS_ABSTRACT_CLASS = "Nejspíše se jedná o abstraktní třídu!",
			MO_TXT_NOT_FOUND_CONSTRUCTOR_TO_CASTING_DATA_TYPE = "Nebyl nalezen konstruktor pro přetypování hodnoty na daný datový typ!",
			MO_TXT_POSSIBLE_PARAMETERS_COUNT_IS_DIFFERENT = "Možná chyba, liší se počet paramerů!",
            MO_TXT_EXCEPTION_WHILE_CALLING_CONSTRUCTOR_TO_CASTING = "Při zavolání konstruktoru pro přetypování hodnoty na zmíněný datový typ nastala výjimka!",
			MO_TXT_VARIABLE_FAILED_TO_LOAD = "Proměnnou se nepodařilo načíst!",
			MO_TXT_VARIABLE_NOT_FOUND_FOR_INSERT_VALUE = "Nebyla nalezena zadaná proměnná, do které se má vložit hodnota!",
            MO_TXT_VARIABLE_NOT_FOUND = "Nenalezena proměnná", MO_TXT_VALUE = "Hodnota",
			MO_TXT_VARIABLE_IS_FINAL_IS_NEED_TO_FILL_IN_CREATION = "Proměnná je 'final', je třeba jí naplnit při vytvoření!",
            MO_TXT_MUST_NOT_BE_FINAL_AND_NULL = "nesmí být 'final' a zároveň null!",
			MO_TXT_VARIABLE_NAME_IS_NEED_TO_CHANGE_IS_ALREADY_TAKEN = "Název proměnné je již obsazen, je třeba jej změnit!",
            MO_TXT_VARIABLE_NAME_NOT_RECOGNIZED = "Nebyl rozpoznán název proměnné!",
			MO_TXT_DATA_TYPE_NOT_FOUND_ONLY_PRIMITIVE_DATA_TYPE = "Nebyl nalezen datový typ proměnné (lze použít pouze primitivní datové typy.)!",
            MO_TXT_DATA_TYPE = "Datový typ",
			MO_TXT_FAILED_TO_CREATE_TEMPORARY_VARIABLE = "Nepodařilo se vytvořit dočasnou proměnnou v editoru příkazů!",
			MO_TXT_DATA = "Data", MO_TXT_VALUE_2 = "hodnota", MO_TXT_MAY_BE_ERROR = "Možná chyba",
			MO_TXT_DATA_TYPE_DO_NOT_SUPPORTED = "Není podporován zadaný datový typ proměnné!",
			MO_TXT_VARIABLE_IS_NULL_CANNOT_BE_INCREMENT_OR_DECREMENT = "Proměnná je null, nelze provést operaci inkrementace nebo dekrementace!",
			MO_TXT_VARIABLE_IS_NULL = "Proměnná je null!",
			MO_TXT_VALUE_WAS_NOT_RECOGNIZED = "Hodnota nebyla rozpoznána!",
			MO_TXT_FAILED_TO_CREATE_VARIABLE = "Nepodařilo se vytvořit proměnnou!",
			MO_TXT_DO_NOT_RETRIEVE_DATA_TYPE_OF_VARIABLE = "Nepodařilo se získat datový typ proměnné!",
			MO_TXT_FAILED_TO_GET_DATA_ABOUT_VARIABLE_WHILE_DECLARATION = "Nepodařilo se získat data o proměnné pro deklaraci!",
			MO_TXT_REFERENCE_DONT_POINT_TO_INSTANCE_OF_LIST = "Zadaná reference neukazuje na instanci listu!",
            MO_TXT_VALUE_AT_INDEX_IS_NULL = "Hodnota na zadaném indexu v poli je null!",
			MO_TXT_CONDITIONS_FOR_INDEX_OF_LIST = "Index hodnoty v listu (kolekci) musí být >= 0 a zároveň index musí být < velikost listu!",
			MO_TXT_SPECIFIED_INDEX_OF_LIST = "Zadaný index",
			MO_TXT_INSTANCE_OF_LIST_WAS_NOT_FOUND_UNDER_REFERENCE = "Instance listu nebyla pod zadanou referencí nalezena!",
			MO_TXT_SIZE_OF_THE_LIST = "Velikost listu - kolekce",
			MO_TXT_DATA_TYPE_FOR_LIST_NOT_FOUND = "Nebyl nalezen datový typ pro list!",
			MO_TXT_DATA_TYPE_FOR_ARRAY_ARE_NOT_IDENTICALY = "Datové typy pro deklaraci a naplnění pole nejsou identické!",
			MO_TXT_DATA_TYPE_FOR_ARRAY_NOT_FOUND = "Nebyl nalazen datový typ pro pole!",
			MO_TXT_INSTANCE_OF_ARRAY_WAS_NOT_FOUND_UNDER_REFERENCE = "Instance jednorozměrného pole nebyla pod zadanou referencí nalezena!",
			MO_TXT_ARRAY_IS_FINAL_DONT_CHANGE_VALUES = "Pole je 'final' (tj. neměnné), nelze měnit jeho hodnoty, jsou pouze ke čtění!",
			MO_TXT_ARRAY = "Pole",
			MO_TXT_INDEX_OUT_OF_RANGE = "Index pozice v poli je mimo rozsah hodnot typu Integer!",
            MO_TXT_CONDITIONAL_FOR_INDEX_IN_ARRAY = "Index hodnoty v poli musí být >= 0 a zárověň index musí být < velikost (délka) pole!",
			MO_TXT_INDEX = "Index", MO_TXT_SIZE_OF_ARRAY = "velikost pole", MO_TXT_NAME = "Název",
			MO_TXT_DOES_NOT_INCREMENT_DECREMET = "Hodnota na zadaném indexu v zadaném poli nelze inkrementovat / dekrementovat, neobsahuje číslo!",
			MO_TXT_INDEX_OF_LIST_IS_OUT_OF_RANGE = "Index hodnoty pozice v listu (kolekci), kam se má vložit hodnota je mimo rozsah velikost listu!",
			MO_TXT_VARIABLE_IS_FINAL_DONT_CHANGE_VALUE = "Proměnná je 'final', nelze měnit její hodnotu!",
			MO_TXT_INSTANCE_IS_NOT_CORRECT_FOR_PARAMETER = "Instance zadané třídy neni instance, kterou vyžaduje parametr!",
			MO_TXT_ARRAY_SIZE_IS_OUT_OF_INTEGER = "Velikost pole je mimo rozsah hodnoty typu Integer!",
			MO_TXT_VALUE_IN_ARRAY_IN_INDEX_IS_NULL = "Hodnota na zadném indexu v zadaném poli je null!",
			MO_TXT_VALUE_AT_INDEX_AT_ARRAY_CANT_INCRE_DECRE = "Hodnota na zadném indexu v zadaném poli nelze inkrementovat / dekrementovat, neobsahuje číslo!",
			MO_TXT_FAILED_TO_CALCULATE_MATH_TERM_1 = "Nepodařilo se vypočítat zadaný výraz",
			MO_TXT_FAILED_TO_CALCULATE_MATH_TERM_2 = "Výraz";




    // Texty ve třídě MakeOperation.java metodě: writeComands (WC) - texty pro
	// informace ohledně příkazů:
	public static final String
	// Matematické operace:
	MO_WC_TXT_MATH_CALCULATION_TITLE = "Matematické výpočty",
            MO_WC_TXT_OPERANDS_FOR_MATH_CALCULATION = "Pro matematické operace je možné využít následující matematické operátory",
			// Logické operaátory:
            MO_WC_TXT_LOGICAL_OPERATIONS_INFO = "Pro logické operace je možné využít následující logické operátory",
			// Konstanty:
            MO_WC_TXT_CONSTANTS = "Konstanty",
			// Podporované funkce:
			MO_WC_TXT_SUPPORTED_FUNCTIONS_TITLE = "Podporované funkce",
			MO_WC_TXT_NAMES_OF_FUNCTIONS_INFO = "Následující názvy funkcí jsou",
    // Příklady:
			MO_WC_TXT_WITHOUT_SEMICOLON_AT_ENDS_OF_EXPRESSION = "V případě matematického výrazu pro výpočet se neuvádí středník na konci výrazu (neplatí při přiřazení výsledku do proměnné).",
			MO_WC_TXT_INCLUDE_VARIABLES_FROM_COMMANDS_EDITOR_INFO = "Do matematického výpočtu lze zahrnout i číselné proměnné vytvořené v editoru příkazů, například v následující syntaxi",
            MO_WC_TXT_VAR_NAMES_CANT_CONTAINS_NUMBERS = "Názvy proměnných použitých v matematickém výrazu / výpočtu nesmí obsahovat číslice.",

			MO_WC_TXT_DECLARED_AND_FULFILLED_VARS = "Dříve deklarované a naplněné proměnné",
                    MO_WC_TXT_DECLARED_AND_FULFILLED_VAR = "Dříve deklarovaná a naplněná proměnná",

			// Matematické operace
			MO_WC_TXT_UNARY_PLUS = "Doplňkový operátor / Unary plus",
                    MO_WC_TXT_UNARY_MINUS = "Operátor subtrakce / Unary mínus",
			MO_WC_TXT_MULTIPLICATION_OPERATOR = "Operátor násobení může být vynechán před otevřenou závorkou",
			MO_WC_TXT_DIVISION_OPERATOR = "Operátor děleno",
			MO_WC_TXT_MODULO_OPERATOR = "Operátor zbývajícího (Modulo)",
			MO_WC_TXT_POWERED_OPERATOR = "Operátor exponent",

			// Logické operátory:
			MO_WC_TXT_EQUALS = "Rovná se", MO_WC_TXT_NOT_EQUALS = "Není rovno", MO_WC_TXT_LESS_THAN = "Méně než",
			MO_WC_TXT_LESS_THAN_OR_EQUAL_TO = "Méně než nebo rovno", MO_WC_TXT_GREATER_THAN = "Větší než",
			MO_WC_TXT_GREATER_THAN_OR_EQUAL_TO = "Větší nebo rovno", MO_WC_TXT_BOOLEAN_AND = "Logický and",
                    MO_WC_TXT_BOOLEAN_OR = "Logický or",

			// Konstanty:
			MO_WC_TXT_VALUE_OF_E = "Hodnota e, přesně na 70 číslic",
                    MO_WC_TXT_VALUE_OF_PI = "Hodnota PI, přesně na 100 číslic", MO_WC_TXT_VALUE_ONE = "Hodnota jedna",
			MO_WC_TXT_VALUE_ZERO = "Hodnota nula",

    // Podporované funkce:
    MO_WC_TXT_FCE_NOT = "Booleovská negace, 1 (znamená pravda), pokud výraz není nula",
			MO_WC_TXT_CONDITION = "Vrací hodnotu jedna, pokud je podmínka vyhodnocena jako pravdivá nebo jinou pokud vyhodnotí false",
            MO_WC_TXT_FCE_RANDOM = "Vytvoří náhodné číslo mezi 0 a 1",
			MO_WC_TXT_FCE_MIN = "Vrací nejmenší z daných výrazů", MO_WC_TXT_FCE_MAX = "Vrací největší z daných výrazů",
			MO_WC_TXT_FCE_ABS = "Vrací absolutní (ne-zápornou) hodnotu výrazu",
			MO_WC_TXT_FCE_ROUND = "Zaokrouhlí hodnotu na určitý počet číslic, používá aktuální režim zaokrouhlení",
			MO_WC_TXT_FCE_FLOOR = "Zaokrouhlí hodnotu na nejbližší celé číslo",
			MO_WC_TXT_FCE_CELLING = "Zaokrouhlí hodnotu na nejbližší celé číslo",
			MO_WC_TXT_FCE_LOG = "Vrací přirozený logaritmus (základ e) výrazu",
			MO_WC_TXT_FCE_LOG_10 = "Vrací společný logaritmus (základ 10) výrazu",
			MO_WC_TXT_FCE_SQRT = "Vrací druhou odmocninu výrazu",
			MO_WC_TXT_FCE_SIN = "Vrací trigonometrický sinus úhlu (ve stupních)",
			MO_WC_TXT_FCE_COS = "Vrací trigonometrický kosinus úhlu (ve stupních)",
			MO_WC_TXT_FCE_TAN = "Vrací trigonometrické tangeny úhlu (ve stupních)",
			MO_WC_TXT_FCE_COT = "Vrací trigonometrické kotangeny úhlu (ve stupních)",
			MO_WC_TXT_FCE_ASIN = "Vrací úhel asin (ve stupních)", MO_WC_TXT_FCE_ACOS = "Vrací úhel acos (ve stupních)",
			MO_WC_TXT_FCE_ATAN = "Vrací úhel atan (ve stupních)", MO_WC_TXT_FCE_ACOT = "Vrací úhel acot (ve stupních)",
			MO_WC_TXT_FCE_ATAN2 = "Vrací úhel atan2 (ve stupních)",
			MO_WC_TXT_FCE_SINH = "Vrací hyperbolický sinus hodnoty",
            MO_WC_TXT_FCE_COSH = "Vrátí hyperbolický kosinus hodnoty",
			MO_WC_TXT_FCE_TAH = "Vrací hyperbolické tangeny hodnoty",
			MO_WC_TXT_FCE_COTH = "Vrací hyperbolické kotangeny hodnoty",
			MO_WC_TXT_FCE_SEC = "Vrací secant (ve stupních)", MO_WC_TXT_FCE_CSC = "Vrací cosecant (ve stupních)",
			MO_WC_TXT_FCE_SECH = "Vrací hyperbolickou secant (ve stupních)",
			MO_WC_TXT_FCE_CSCH = "Vrací hyperbolický cosecant (ve stupních)",
			MO_WC_TXT_FCE_ASINH = "Vrací úhel hyperbolického sinusu (ve stupních)",
			MO_WC_TXT_FCE_ACOSH = "Vrací úhel hyperbolického kosinusu (ve stupních)",
			MO_WC_TXT_FCE_TANH = "Vrací úhel hyperbolických tangenů hodnoty",
			MO_WC_TXT_FCE_RAD = "Převede úhel měřený ve stupních na přibližně ekvivalentní úhel měřený v radiánech",
			MO_WC_TXT_FCE_DEG = "Převede úhel naměřený v radiánech na přibližně ekvivalentní úhel měřený ve stupních",


			// Nějkaé ukázky pro matematické výrazy,které je možné zadat (podmínky, funkce,
			// jejich využití, ...)
			MO_WC_TXT_EXAMPLES_OF_MATH_EXPRESSIONS = "Příklady matematických výrazů pro výpočet",
			MO_WC_TXT_MAKE_INSTANCE = "Vytvoření instance třídy z diagramu tříd",
			MO_WC_TXT_CLASS_NAME = "Název třídy lze zadat s balíčky nebo bez ních, dle toho, zda se daná třída v nějakém balíčku nachází",
			MO_WC_TXT_MAKE_INSTANCE_PARAMETERS_1 = "Dále lze zadat vytvoření instance třídy s parametry: znak ('x'), text (\"Some text.\"), logická hodnota (true, false), celá či desetinná čísla oddělené desetinnou tečkou,",
			MO_WC_TXT_MAKE_INSTANCE_PARAMETERS_2 = "zavolání metody s nejvýše jedním parametrem (referenceName.methodName(parameterName)), inkrementace a dekrementace proměnné v editoru příkazů nebo",
			MO_WC_TXT_MAKE_INSTANCE_PARAMETERS_3 = "v instanci třídy (postfixová i prefixová forma), nebo matematický výraz (bez desetinných čárek)",
			MO_WC_TXT_WITH_PARAMETERS = "S parametry",
			MO_WC_TXT_CALL_CONSTRUCTOR_INFO_1 = "Dále lze vytvořit instanci třídy z diagramu tříd bez vytvoření reference (na příslušnou instanci) zavoláním konstruktoru.",
			MO_WC_TXT_CALL_CONSTRUCTOR_INFO_2 = "Zavolat konstruktor je možné v následující syntaxi",
			MO_WC_TXT_CALL_CONSTRUCTOR_INFO_3 = "Tuto možnost lze také využít pro vytvoření instance třídy z diagramu tříd a následné předání do parametru zavolání metody či konstruktoru nebo naplění proměnné v instanci třídy.",
			MO_WC_TXT_CALL_METHOD_INFO = "Metodu, která není statická, lze zavolat pouze nad instancí třídy z diagramu tříd v následující syntaxi",
			MO_WC_TXT_CALL_STATIC_METHOD_INFO = "Statickou metodu lze volat také nad instancí třídy nebo nad třídou v diagramu tříd (ne nad vytvořenou instancí) v následující syntaxi",
            MO_WC_TXT_CALL_STATIC_METHOD_IN_INNER_CLASS_INFO = "Statickou metodu ve vnitřní statické třídě nějaké třídy v diagramu tříd je možné volat pouze nad touto třídou v diagramu",
			MO_WC_TXT_CALL_METHOD_INFO_1 = "Dále lze do metody přidat parametry se stejnými požadavky jako u výše zmíněného konstruktoru, tj. parametry oddělené desetinnou čárkou, metoda s nejvýše jedním parametrem,",
			MO_WC_TXT_CALL_METHOD_INFO_2 = "dále texty, znaky, proměnné, čísla, apod.",
			// Info ohledně zavolání zděděných metod nad třídou coby potomkem příslušné
			// třídy s danou metodou:
			MO_WC_TXT_INFO_ABOUT_METHODS_TITLE = "Info",
			MO_WC_TXT_INFO_ABOUT_CALL_METHODS_FROM_OBJECT_CLASS = "Nad každou instancí třídy je možné zavolat metody zděděné z třídy Object.",
			MO_WC_TXT_CALL_METHOD_FROM_INHERITED_CLASS_INFO = "Metody označené klíčovými slovy 'public' nebo 'protected' v nějaké třídě lze zavolat ze všech jejích potomků.",
                    MO_WC_TXT_CALL_STATIC_METHOD_FROM_INHERITED_CLASSES_INFO = "To platí i pro statické metody, které nemusí být volány nad instancí třídy.",
			MO_WC_TXT_INCREMENTATION_DCREMENTATION = "Inkrementace a dekrementace proměnné nebo konstanty (prefixová a postfixová forma)",
			MO_WC_TXT_INCREMENTATION_DECREMENTATION_1 = "Tuto operaci lze provést nad proměnnou v instanci třídy, proměnnou vytvořenou v editoru příkazů (vlevo) a nebo nad konstantou (unikátně pouze v editoru příkazů)",
			MO_WC_TXT_VARIABLE_INCREMENTATION = "Ukázka možné syntaxe inkrementace proměnné v instanci třídy",
			MO_WC_TXT_VARIABLE_DECREMENTATION = "Ukázka možné syntaxe dekrementace proměnné v instanci třídy",
			MO_WC_TXT_INCREMENTATION_MY_VARIABLE = "Ukázka možné syntaxe inkrementace proměnné vytvořené v editoru příkazů",
			MO_WC_TXT_DECREMENTATION_MY_VARIABLE = "Ukázka možné syntaxe dekrementace proměnné vytvořené v editoru příkazů",
			MO_WC_TXT_INCREMENTATION_DECREMENTATION_CONSTANTS = "Zcela unikátně pro 'editor příkazů' jsou doplněny operace pro inkrementace a dekrementace konstanty.",
			MO_WC_TXT_INCREMENTATION_CONSTANTS = "Inkrementace konstanty v následující syntaxi",
			MO_WC_TXT_DECREMENTATION_CONSTANTS = "Dekrementace konstanty v následující syntaxi",
			MO_WC_TXT_DECLARATION_OF_VARIABLE = "Deklarace proměnné v editoru příkazů",
			MO_WC_TXT_DECLARATION_VARIABLE_INFO = "V editoru příkazů lze deklarovat proměnné pouze primitivních či objektových datových typů (Javovských typů)",
                    MO_WC_TXT_LIKE_THIS = "jako jsou",
			MO_WC_TXT_DECLARATION_VARIABLE_INFO_2 = "Proměnnou lze deklarovat pomocí následující syntaxe, pokud má být proměnná neměnná (Immutable), je třeba ji označit klíčovým slovem final a při deklaraci naplnit",
			MO_WC_TXT_SYNTAXE = "Syntaxe", MO_WC_TXT_SYNTAXE_FOR_STRING = "Syntaxe pro String",
			MO_WC_TXT_SYNTAXE_FOR_CHAR = "Syntaxe pro znak",
			MO_WC_TXT_SYNTAXE_FOR_DOUBLE = "Syntaxe pro desetinné číslo typu double",
                    MO_WC_TXT_SIMILARY_DATA_TYPES = "apod. pro zbývající zmíněné datové typy.",
			MO_WC_TXT_FULLFILMENT_VARIABLE = "Naplnění proměnné v instanci třídy nebo proměnné, vytvořené v editoru příkazů",
			MO_WC_TXT_FULLFILMENT_VARIABLE_INFO = "Proměnnou lze naplnit konstantou (číslo, text znak) - případně matematickou operací (viz výše), návratovou hodnotou metody nebo hodnotou jiné proměnné",
			MO_WC_TXT_FULLFILMENT_VARIABLE_SYNTAXE_INFO = "Naplnění proměnné lze provést v následující syntaxi",
			MO_WC_TXT_FULLFILMENT_VARIABLE_TEXT = "Naplnění proměnné v instanci třídy",
                    MO_WC_TXT_FULLFILMENT_MY_VARIABLE = "Naplnění proměnné vytvořené v editoru příkazů",
			MO_WC_TXT_FULFILMENT_REPLACE_INFO = "Za hodnotu value lze doplnit zmíněný text (\"Some text.\"), znak ('x'), číslo, logickou hodnotu nebo matematickým výpočtem zmíněným na začátku.",
			MO_WC_TXT_FULLFILMENT_RETURN_VALUE = "Dále lze proměnnou naplnit pomocí návratové hodnoty metody, metoda musí vracet hodnotu stejného datového typu, jako je daná proměnná.",
                    MO_WC_TXT_FULLFILMENT_BY_STATIC_METHOD = "Nebo pomocí návratové hodnoty ze statické metody",
			MO_WC_TXT_FULLFILMENT_BY_STATIC_METHOD_IN_INNER_CLASS = "Nebo pomocí návratové hodnoty ze statické metody statické vnitřní třídy",
			MO_WC_TXT_FULLFILMENT_VARIABLE_SAME_RULES = "Pro výše uvedenou metodu platí stejná pravidla jako u vytváření konstruktoru (výše).",
			MO_WC_TXT_FULLFILMENT_VARIABLE_INFO_2 = "Naplnění proměnné pomocí jiné proměnné, může to být jak proměnná z intance třídy, tak proměnná vytvořená v editoru příkazů",
			MO_WC_TXT_FULLFILMENT_ETC = "apod. dle zmíněných pravidel.",
			MO_WC_TXT_PRINT_METHOD_INFO = "Pro výpis hodnot do 'editoru výstupů' (toto okno) je možné v editoru příkazů využít příkaz",
			MO_WC_TXT_PRINT_METHOD_INFO_2 = "Tato metoda si zjistí o jaké parametry se jedná, například proměnné v instanci třídy, tak se pokusí zjistit hodnotu této proměnné a vypsat ji.",
                    MO_WC_TXT_PRINT_METHOD_INFO_3 = "Dále lze například vypsat návratovou hodnotu metody, nějaký znak, text, konstantu, logickou hodnotu, apod.",
			MO_WC_TXT_PRINT_METHOD_INFO_4 = "V případě, že metoda bude obsahovat více jak jeden parametr, je třeba je oddělit symbolem '+' (plus), nefungují zde početní operace kromě exponentu.",
			MO_WC_TXT_PRINT_METHOD_SYNTAXE_EXAMPLE = "Ukázka syntaxe",
			MO_WC_TXT_PRINT_METHOD_WRITE_RESULT = "vypíše se",
			MO_WC_TXT_PRINT_METHOD_RETURN_VALUE = "návratová hodnota metody",
                    MO_WC_TXT_DECLARATION_LIST_INFO = "Pro deklaraci Listu (kolekce) je možné zadat následující příkaz, podporován pouze ArrayList",
			MO_WC_TXT_DECLARATION_LIST_INFO_2 = "nebo list vytvořit i s parametry",
			MO_WC_TXT_DECLARATION_LIST_INFO_3 = "Datový typ Listu (kolekce) může být", MO_WC_TXT_NOTE = "Poznámka",
			MO_WC_TXT_DECLARATION_LIST_INFO_4 = "Parametr konstruktoru ArrayListu vyžaduje kolekci hodnot, ve výše uvedeném příkazu tomu tak není (pro zjednodušení).",
			MO_WC_TXT_DECLARATION_LIST_INFO_5 = "Ve výše zmíněné syntaxi s parametry není třeba vkládat data nejprve do kolekce, například v syntaxi: '... = new ArrayList<>(Arrays.asList(hodnota1, hodnota2, ...));'. Stačí pouze zadat data jako parametry bez převodu do kolekce.",
			MO_WC_TXT_LIST_METHOD_INFO_1 = "Nad výše zmíněným listem (kolekcí) dále jen list, lze zavolat následující metody",
			MO_WC_TXT_LIST_METHOD_INFO_2 = "Metoda pro přidání položky na konec listu",
			MO_WC_TXT_LIST_METHOD_INFO_3 = "Metoda pro přidání položky na zadaný index v listu, 0 <= index < velikost listu",
			MO_WC_TXT_LIST_METHOD_INFO_4 = "Metoda pro vymazání veškerých položek z listu - jeho \"vyprázdnění\"",
			MO_WC_TXT_LIST_METHOD_INFO_5 = "Metoda pro získání (vrácení) položky z listu na zadaném indexu, 0 <= index < velikost listu",
			MO_WC_TXT_LIST_METHOD_INFO_6 = "Metoda, která nastaví položku v listu na zadaném indexu, 0 <= index < velikost listu",
			MO_WC_TXT_LIST_METHOD_INFO_7 = "Metoda, která zkopíruje položky z listu do jednorozměrného pole a vrátí jej",
			MO_WC_TXT_LIST_METHOD_INFO_8 = "Metoda, která vrátí počet prvků v listu, resp. velikost listu. Funguje podobně jako vlastnost length nad jednorozměrným polem",
			MO_WC_TXT_LIST_METHOD_INFO_9 = "Metoda, která odebere z listu položku na zadaném indexu,  0 <= index < velikost listu",
			MO_WC_TXT_LIST_METHOD_INFO_10 = "Index hodnoty v listu musí být zadané číslo, nikoliv proměnná či metoda, apod.",
			MO_WC_TXT_DECLARATION_ONE_DIMENSIONAL_ARRAY = "Deklarace jednorozměrného pole (čtyřmi způsoby)",
			MO_WC_TXT_ONE_DIM_ARRAY_INFO_1 = "Je třeba definovat datový typ hodnot a velikost pole, velikost pole musí být > 0",
			MO_WC_TXT_ONE_DIM_ARRAY_INFO_2 = "Je třeba definovat datový typ hodnot a dané pole naplnit hodnotami",
			MO_WC_TXT_ONE_DIM_ARRAY_INFO_3 = "Je třeba definovat datový typ hodnot a dané pole naplnit návratovou hodnotou metody",
			MO_WC_TXT_DATA_TYPE_ARRAY_CAN_BE = "Datový typ pole může být",
			MO_WC_TXT_OPERATIONS_WITH_ARRAY = "Operace s polem",
                    MO_WC_TXT_OPERATIONS_WITH_ARRAY_1 = "Přidání hodnoty na index v poli, 0 <= index < velikost pole",
			MO_WC_TXT_OPERATIONS_WITH_ARRAY_2 = "Inkrementace v postfixové formě",
                    MO_WC_TXT_OPERATIONS_WITH_ARRAY_3 = "Inkrementace v prefixové formě",
			MO_WC_TXT_OPERATIONS_WITH_ARRAY_4 = "Dekrementace v postfixové formě",
			MO_WC_TXT_OPERATIONS_WITH_ARRAY_5 = "Dekrementace v prefixové formě",
                    MO_WC_TXT_OPERATIONS_WITH_ARRAY_6 = "Pro získání hodnoty z pole na zadaném indexu",
			MO_WC_TXT_OPERATIONS_WITH_ARRAY_7 = "Pro získání počtu prvků v poli, resp. velikosti pole",
			MO_WC_TXT_TWO_DIM_ARRAY_TITLE = "Dvourozměrné pole",
			MO_WC_TXT_WORK_WITH_TWO_DIM_ARRAY_INFO = "Práce s dvourozměrným polem je možná pouze nad instancí třídy, která je reprezentována v diagramu instancí.",
                    MO_WC_TXT_CANNOT_CREATE_TWO_DIM_ARRAY = "Dvourozměrné pole nelze vytvořit v editoru příkazů.",
			MO_WC_TXT_OPERATIONS_WITH_TWO_DIM_ARRAY = "Operace s dvourozměrným polem",
			MO_WC_TXT_FULLFILMENT_ARRAY_INFO_1 = "Při naplnení proměnné typu pole jiným polem je třeba si hlídat datové typy daných polí.",
                    MO_WC_TXT_FULLFILMENT_ARRAY_INFO_2 = "Pokud dojde k naplnění proměnné typu jednorozměrného pole nějakého datového typu jiným polem - jiného datového typu, tak se datový typ pole, do kterého",
			MO_WC_TXT_FULLFILMENT_ARRAY_INFO_3 = "se vkládá druhé pole \"přepíše\" na datový typ toho vkládaného pole. (Pouze v tomto editoru - nedořešeno)",
			MO_WC_TXT_OPERATIONS_WITH_ARRAY_IN_INSTANCE_TITLE = "Práce s jednorozměrným polem v instanci třídy",
			MO_WC_TXT_OPERATIONS_WITH_ARRAY_IN_INSTANCE_INFO = "Pro operace s jednorozměrným polem, které se nachází v instanci nějaké třídy, která je reprezentována v diagramu instancí platí stejná pravidla, která jsou uvedena výše.",
			MO_WC_TXT_OPERATIONS_WITH_ARRAY_IN_INSTANCE_DIFFERENCE = "Jediný rozdíl je v přístupu k příslušnému poli, resp. proměnné typu pole. Jedná se o přístup pomocí reference na instanci.",
			MO_WC_TXT_INFO_ABOUT_NULL_VALUE = "V editoru příkazů nejsou nikde podporovány null hodnoty! Nelze jí naplnit proměnnou, parametr metody, konstruktoru, apod.",

			// Info ohledně metod pro restartování a ukončení aplikace:
			MO_WC_TXT_INFO_FOR_METHODS_OVER_APP = "Následují metody, které jsou doplněny zcela výjimečně pouze pro tuto aplikaci.",
			// Restartování aplikace:
			MO_WC_TXT_RESTART_APP_TITLE = "Restartování aplikace",
			// Info k restartu - že se uložídiagram tříd apod.
			MO_WC_TXT_INFO_ABOUT_CLOSE_OPEN_FILES_BEFORE_RESTART_APP = "Před restartováním aplikace je vhodné zavřít veškeré otevřené soubory v editoru zdrojového kódu nebo průzkumníku projektů. Provedené změny nebudou uloženy.",
			MO_WC_TXT_INFO_ABOUT_LAUNCH_COUNTDOWN_WHEN_COMMAND_IS_ENTERED = "Po potvrzení jednoho z následujících příkazů bude spuštěn odpočet do restartování aplikace.",
			MO_WC_TXT_INFO_ABOUT_SAVE_CLASS_DIAGRAM_BEFORE_RESTART_APP = "Před restartováním aplikace bude uložen diagram tříd v otevřeném projektu a potřebná data pro aplikaci.",
			MO_WC_TXT_TWO_WAYS_TO_RESTART_APP = "Aplikaci je možné restartovat dvěma způsoby (využitím editoru příkazů)",
			MO_WC_TXT_FIRST_WAY_TO_RESTART_APP_1 = "Zavoláním následující metody bez parametrů bude aplikace restartována po",
			MO_WC_TXT_FIRST_WAY_TO_RESTART_APP_2 = "vteřinách.",
			MO_WC_TXT_SECOND_WAY_TO_RESTART_APP = "Zavoláním následující metody s parametry bude aplikace restartována po X vteřinách zadaných v parametru metody. Za X je možné dosadit pouze přirozené číslo v intervalu",
			// Ukončení aplikace:
			MO_WC_TXT_CLOSE_APP_TITLE = "Ukončení aplikace",
			MO_WC_TXT_INFO_ABOUT_CLOSE_OPEN_FILES_BEFORE_CLOSE_APP = "Před ukončením aplikace je vhodné zavřít veškeré otevřené soubory v editoru zdrojového kódu nebo průzkumníku projektů. Provedené změny nebudou uloženy.",
			MO_WC_TXT_INFO_ABOUT_LAUNCH_COUNTDOWN_AFTER_COMMAND_IS_ENTERED = "Po potvrzení jednoho z následujících příkazů bude spuštěn odpočet do ukončení aplikace.",
			MO_WC_TXT_INFO_ABOUT_SAVE_CLASS_DIAGRAM_BEFORE_CLOSE_APP = "Před ukončením aplikace bude uložen diagram tříd v otevřeném projektu a potřebná data pro aplikaci.",
			MO_WC_TXT_THREE_WAYS_TO_CLOSE_APP = "Aplikaci je možné ukončit třemi způsoby (využitím editoru příkazů)",
			MO_WC_TXT_FIRST_WAY_TO_CLOSE_APP_1 = "Zavoláním následující metody bez parametrů bude aplikace ukončena po",
			MO_WC_TXT_FIRST_WAY_TO_CLOSE_APP_2 = "vteřinách.",
			MO_WC_TXT_SECOND_WAY_TO_CLOSE_APP = "Zavoláním následující metody s parametry bude aplikace ukončena po X vteřinách zadaných v parametru metody. Za X jemožné dosadit pouze přirozené číslo v intervalu",
                    MO_WC_TXT_THIRD_WAY_TO_CLOSE_APP_1 = "Zavoláním následující metody s parametrem 0 (nula). Aplikace bude ukončena po",
			MO_WC_TXT_THIRD_WAY_TO_CLOSE_APP_2 = "vteřinách.",
			MO_WC_TXT_INFO_ABOUT_THIRD_WAY_TO_CLOSE_APP_1 = "Tato metoda je stejná jako ukončení aplikace pomocí zdrojového kódu jazyka Java. V rámci této aplikace je možné zadat následující metodu pouze s parametrem 0 (nula),",
			MO_WC_TXT_INFO_ABOUT_THIRD_WAY_TO_CLOSE_APP_2 = "ale v jazyce Java lze za parametr metody zadat i jiné hodnoty (indexy), které mohou značit kód nějaké chyby. Například -1 nebo 1 apod.",
    // Zrušení odpočtu pro restartování nebo ukončení aplikace:
			MO_WC_TXT_CANCEL_COUNTDOWN_TITLE = "Zrušení odpočítávání pro ukončení nebo restartování aplikace",
            MO_WC_TXT_CANCEL_COUNTDOWN_INFO = "Pokud je spuštěn odpočet pro ukončení nebo restart aplikace, je možné jej ukončit zadáním následujícího příkazu.",

			// Texty, že nebyl nalezen soubor .class pro zavolaní staticke metody a nebo
			// konstruktoru:
            MO_WC_TXT_FILE_DOT_CLASS = "Soubor '.class', vzniklý po zkompilování třídy",
			MO_WC_TXT_FILE_DOT_CLASS_WAS_NOT_FOUND = "nebyl v adresáři 'bin' v aktuálně otevřeném projektu nalezen.",
			MO_WC_TXT_FAILED_TO_FIND_THE_CLASS = "Nepodařilo se nalézt třídu",
			MO_WC_TXT_FAILED_TO_FIND_BIN_DIR = "ani adresář 'bin' se zkompilovanými třídami.",
			MO_WC_TXT_FAILED_TO_FIND_INNER_CLASS = "ani její vnitřní třídu.",
			MO_WC_TXT_INNER_CLASS_IS_NOT_STATIC = "Vnitřní třída není statická.",
			MO_WC_TXT_INNER_CLASS_IS_NOT_PUBLIC_EITHER_PROTECTED = "Vnitřní třída není přístupná. Musí být označena modifikátorem 'public', popř. 'protected'.",
			MO_WC_TXT_SPECIFIC_METHOD_WAS_NOT_FOUND = "nebyla nalezena metoda se zadaným názvem nebo parametry, popřípadě se nejedná o statickou metodu nebo se neshoduje datový typ návratové hodnoty.",
                    MO_WC_TXT_PUBLIC_CONSTRUCTOR_WAS_NOT_FOUND = "Nepodařilo se najít veřejný konstruktor se zadaným počtem nebo typem parametrů, nebo se neshoduje datový typ třídy (s konstruktorem).",
			// Text pro zavolání konstruktoru:
            MO_WC_TXT_CONSTRUCTOR_WAS_NOT_FOUND = "Nepodařilo se najít veřejný konstruktor se zadaným počtem nebo typem parametrů.",
			// Text pro výpis proměnných v editoru příkazů:
			MO_WC_TXT_NO_VARIABLE_WAS_CREATED = "Nebyla vytvořena žádná proměnná.",


			// Texty pro operace s jednorozměrným polem v instanci třídy:
			MO_WC_TXT_CASTING_TO_INTEGER_FAILURE_1 = "Nepodařilo se přetypovat číslo",
			MO_WC_TXT_CASTING_TO_INTEGER_FAILURE_2 = "na datový typ Integer.",
			MO_WC_TXT_INSTANCE_OF_CLASS_IN_ID_NOT_FOUND = "Instance třídy nebyla nalezena. Instance",
			MO_WC_TXT_ENTERED_VARIABLE_1 = "Zadaná proměnná", MO_WC_TXT_ENTERED_VARIABLE_2 = "není typu pole.",
			MO_WC_TXT_IS_NOT_ONE_DIM_ARRAY = "není jednorozměrné pole.",
			MO_WC_TXT_INDEX_VALUE_AT_ARRAY_IS_NOT_IN_INTERVAL = "Index hodnoty v poli musí být >= 0 a zároveň menší než velikost pole, tj. Index musí být v intervalu",
			MO_WC_TXT_VALUE_IN_ARRAY_IS_NULL = "Získaná hodnota v zadaném poli je null.",

    // Texty pro operace nad dvourozměrným polem v instanci třídy:
			MO_WC_TXT_IS_NOT_TWO_DIM_ARRAY = "není dvourozměrné pole.",
			MO_WC_TXT_FAILED_TO_SET_VALUE_TO_TWO_DIM_ARRAY = "Nepodařilo se nastavit hodnotu na zadaném indexu, nejspíše byly zadány chybné indexy.",
            MO_WC_TXT_INDEX_1_TEXT = "Index 1", MO_WC_TXT_INDEX_2_TEXT = "Index 2",
			MO_WC_TXT_FAILED_TO_GET_VALUE_FROM_TWO_DIM_ARRAY = "Nepodařilo se získat hodnotu na zadaném indexu, nejspíše byly zadány chybné indexy.",
			MO_WC_TXT_FAILED_TO_INCREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE = "Hodnotu na zadaném indexu se nepodařilo inkrementovat. Nejedná se o číslo nebo je to null hodnota.",
			MO_WC_TXT_FAILED_TO_DECREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE = "Hodnotu na zadaném indexu se nepodařilo dekrementovat. Nejedná se o číslo nebo je to null hodnota.",
			MO_WC_TXT_VALUE_WAS_NOT_FOUND_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE = "Nebyla nalezena hodnota na zadaném indexu.",



			// Texty pro ukončení a restart aplikace dle zadaného příkazu v editoru příkazů:
			// Texty pro ukončení odpočtu:
			MO_WC_TXT_NO_COUNTDOWN_IS_RUNNING = "Žádný odpočet není spuštěň.",
			MO_WC_TXT_COUNTDOWN_IS_STOPPED = "Odpočet zastaven.",

			// Texty pro spuštění timeru pro ukončení aplikace.
			MO_WC_TXT_COUNTDOWN_FOR_CLOSE_APP_IS_RUNNING = "Odpočet pro ukončení aplikace je již spuštěn.",
			MO_WC_TXT_COUNTDOWN_FOR_CLOSE_APP_IS_NOT_POSSIBLE_TO_START = "Nelze spustit odpočet pro ukončení aplikace, již je spuštěn odpočet pro restart aplikace.",

			MO_WC_TXT_TO_END_OF_APP_REMAINS_1 = "Do ukončení aplikace zbývá",
			MO_WC_TXT_TO_END_OF_APP_REMAINS_2 = "Do ukončení aplikace zbývají",
			MO_WC_TXT_TO_END_OF_APP_REMAINS_3 = "Do ukončení aplikace zbývá",

			MO_WC_TXT_SECONDS_FOR_CLOSE_1 = "vteřin.", MO_WC_TXT_SECONDS_FOR_CLOSE_2 = "vteřiny.",
			MO_WC_TXT_SECONDS_FOR_CLOSE_3 = "vteřina.",

			// Texty pro spuštění timeru pro restartování aplikace:
			MO_WC_TXT_COUNTDOWN_FOR_RESTART_APP_IS_RUNNING = "Odpočet pro restartování aplikace je již spuštěn.",
			MO_WC_TXT_COUNTDOWN_FOR_RESTART_APP_IS_NOT_POSSIBLE_TO_START = "Nelze spustit odpočet pro restartování aplikace, již je spuštěn odpočet pro ukončení aplikace.",

			MO_WC_TXT_TO_RESTART_APP_REMAINS_1 = "Do restartování aplikace zbývá",
			MO_WC_TXT_TO_RESTART_APP_REMAINS_2 = "Do restartování aplikace zbývají",
			MO_WC_TXT_TO_RESTART_APP_REMAINS_3 = "Do restartování aplikace zbývá",

			MO_WC_TXT_SECONDS_FOR_RESTART_1 = "vteřin.", MO_WC_TXT_SECONDS_FOR_RESTART_2 = "vteřiny.",
			MO_WC_TXT_SECONDS_FOR_RESTART_3 = "vteřina.";



















	// texty do třídy RefreshInstance - pouze text do chybové hlášky, pokud dojde k chybě při vytvoření instance:
	public static final String RI_TXT_FAILED_TO_UPDATE_INSTANCE_OF_CLASS = "Nepodařilo se aktualizovat instanci třídy",
			RI_TXT_FAILED_TO_UPDATE_INSTANCE_OF_CLASS_2 = "nejčastější příčina této chyby je, že třída neobsahuje výchozí \"nullary\" konstruktor, prosím ujistětě se, že se v dané třídě tento konstruktor nachází,\n  další možné příčiny: jedná se o abstraktní třídu, rozhraní, pole, primitivní typ nebo void, nebo jiný důvod.";
























	// Texty do třídy: OperationsWithInstances.java (OWI) z balíčku: Instances
	// veškeré texty do výše uvedené třídy, jedná se o texty do výpisů do editrou výtupů v hlavním okně aplikace nebo do chybových hlášek:
	public static final String OWI_TXT_METHOD_IS_NOT_PUBLIC = "Metoda je nepřístupná, nejspíše není označena modifikátorem public (popř. protected), (vráceno null).",
			OWI_TXT_METHOD_IS_NOT_INSTANCE_OF_CLASS = "Nejčastější příčina této chyby je, že se metoda nenachází v zadané instanci třídy, nebo se liší počty či typy parametrů! (vráceno null)",
			OWI_TXT_ERROR_IN_CALL_THE_METHOD = "Chyba při volání metody! (vráceno null)",
			OWI_TXT_ERROR_DURING_CREATE_INSTANCE = "Nastala chyba při vytváření instance třídy!",
			OWI_TXT_POSSIBLE_CAUSES_CREATE_INSNTANCE_ERROR = "Možné příčiny: Jedná se o abstraktní třídu, rozhraní, nenalezen konstruktor, apod.",
			OWI_TXT_CLASS = "Třída",
			OWI_TXT_CANNOT_CREATE_INSTANCE_FROM_ABSTRACT_CLASS = "Jedná se o abstraktní třídu, z té nelze vytvořit instanci!",
			OWI_TXT_CONSTRUCTOR_IS_NOT_PUBLIC = "Konstruktor není přístupný. Není označen modifikátorem public.",
			OWI_TXT_CONSTRUCTOR_WANT_DIFFERENT_COUNT_OR_TYPES_OF_PARAMETERS = "Konstuktor třídy očekává jiný počet nebo datové typy parametrů než jaké byly zadány!",
			OWI_TXT_CONSTRUCTOR_WANT = "Konstruktor očekává", OWI_TXT_BUT_GOT = "Ale dostal",

			OWI_TXT_ERROR_WILE_CALLING_CONSTRUCTOR_BY_THROW_AN_EXCEPTION = "Nastala chyba pro volání konstruktoru, tato chyba může nastat pokud podkladový konstruktor vyhodí výjimku.",

			OWI_TXT_ERROR_DURING_CALL_BASE_CONSTRUCTOR = "Při volání výchozího 'nullary' konstruktoru nastala vyjímka!",
			OWI_TXT_BUT_DID_GOT = "Ale nedostal žádné parametry!",
			OWI_TXT_REFERENCE_NAME_EXIST_MUST_CHANGE = "Název reference již existuje, je třeba jej změnit!",
			OWI_TXT_REFERENCE = "Reference",

			// Texty pro chybové výpisy do editoru výstupů v případě zavolání konstruktoru v
			// metodě callConstructor:
			OWI_TXT_FAILED_TO_CALL_CONSTRUCTOR_IN_ABSTRACT_CLASS = "Nepodařilo se zavolat konstruktor v abstraktní třídě, konstruktor",
			OWI_TXT_INSERTED_PARAMETERS = "Zadané parametry", OWI_TXT_REQUIRED_PARAMETERS = "Konstruktor vyžaduje",
                    OWI_TXT_FAILED_TO_CALL_CONSTRUCTOR = "Nepodařilo se zavolat konstruktor",
			OWI_TXT_CONSTRUCTOR_IS_NOT_ACCESSIBLE = "nejčastější příčína této chyby je, že konstruktor není veřejně přístupný.",
			OWI_TXT_ARGUMENT_ERROR = "může se lišit počet zadaných a vyžadovaných parametrů, nebo pokud selhání konverze pro primitivní argumenty selže, nebo pokud po případném rozbalení nemůže být hodnota parametru převedena na odpovídající typ formálního parametru pomocí konverze vyvolání metody, nebo pokud se tento konstruktor týká typu enum.",
			OWI_TXT_CONSTRUCTOR_THROW_AN_EXCEPTION = "tato chyba může nastat v případě, že příslušný konstruktor vyhodí výjimku.",
			OWI_TXT_REFERENCE_VARIABLE = "referenční proměnná";




















	// Texty do všech tříd v balíčku: projectExplorer (PE - Průzkumník pojektů), resp.
    // níže následují všechny texty, které se načítají ve zvoleném jazyce do aplikace,
	// konkrétně do tříd ve výše zmíněném balíčku:

	// Texty do třídy: GetIndexOfExcelSheetForm.java
	public static final String PE_GIESF_DIALOG_TITLE = "Výběr listu", PE_GIESF_LBL_INFO = "Vyberte list pro otevření",
			PE_GIESF_BTN_OK = "OK", PE_GIESF_BTN_CANCEL = "Zrušit";


	// Texty do třídy: GetNewFileNameForm.java:
	public static final String PE_GNFNF_DIALOG_TITLE = "Přejmenovat položku",
			PE_GNFNF_LBL_INFO = "Zadejte nový název pro položku",
			PE_GNFNF_LBL_ERROR_INFO = "Název je ve špatném formátu (pouze velká a malá písmena bez diakritiky, číslice a podtržítka).",
			PE_GNFNF_LBL_NOTE = "Jedná se o pouhé přejmenování položky, žádný refaktoring!", PE_GNFNF_BTN_OK = "OK",
			PE_GNFNF_BTN_CANCEL = "Zrušit",
			PE_GNFNF_TXT_FILE_NAME_ALREADY_EXIST_TEXT = "Položka se zadaným názvem již v daném umístění existuje!",
			PE_GNFNF_TXT_FILE_NAME_TEXT_NAME = "Název", PE_GNFNF_TXT_FILE_NAME_ALREADY_EXIST_TITLE = "Duplicitní název",
			PE_GNFNF_WRONG_FILE_NAME_TEXT = "Název položky neodpovídá požadovanému formátu!",
			PE_GNFNF_TXT_WRONG_FILE_NAME_TITLE = "Chybný formát",
			PE_GNFNF_TXT_TEXT_FIELD_IS_EMPTY_TEXT = "Pole pro nový název položky je prázdné!",
			PE_GNFNF_TXT_TEXT_FIELD_IS_EMPTY_TITLE = "Prázdné pole";



	// Texty do třídy MyTree:
	public static final String PE_MT_TXT_EXPAND = "Rozšířit", PE_MT_TXT_COLLAPSE = "Kolaps",
			PE_MT_TXT_EXPAND_LEAF = "Rozšířit větev", PE_MT_TXT_COLLAPSE_LEAF = "Kolaps větve";


    // Texty do třídy: JtreePopupMenu.java:
    public static final String PE_JPM_MENU_NEW = "Nový", PE_JPM_MENU_OPEN = "Otevřít",
            PE_JPM_ITEM_OPEN_FILE_IN_INTERNAL_FRAME = "V interním okně",
            PE_JPM_ITEM_OPEN_IN_FILE_EXPLORER_IN_OS = "V průzkumníku souborů",
            PE_JPM_ITEM_OPEN_IN_DEFAULT_APP_IN_OS = "Ve výchozí aplikaci",
            PE_JPM_ITEM_REFRESH = "Obnovit",
            PE_JPM_ITEM_NEW_FILE = "Soubor", PE_JPM_ITEM_NEW_FOLDER = "Adresář", PE_JPM_ITEM_NEW_PROJECT = "Projekt",
            PE_JPM_ITEM_NEW_EXCEL_LIST = "List", PE_JPM_ITEM_DELETE = "Smazat", PE_JPM_ITEM_RENAME = "Přejmenovat",
            PE_JPM_ITEM_PASTE = "Vložit", PE_JPM_ITEM_COPY = "Kopírovat", PE_JPM_ITEM_CUT = "Vyjmout",

			PE_JPM_TXT_FILE_ALREADY_EXIST_IN_DIR_TEXT = "Zadaný soubor již ve zvoleném adresáři existuje, nelze jej vytvořit!",
			PE_JPM_TXT_FILE_ALREADY_EXIST_IN_DIR_TITLE = "Soubor již existuje",
			PE_JPM_TXT_CANT_CREATE_FILE_TEXT = "Nepodařilo se vytvořit požadovaný soubor s názvem",
			PE_JPM_TXT_LOCATION = "Umístění", PE_JPM_TXT_CANT_CREATE_FILE_TITLE = "Nepodařilo se vytvořit soubor",
			PE_JPM_TXT_DIR_ALREADY_EXIST_TEXT = "Zadaný adresář již existuje, nelze jej vytvořít!",
			PE_JPM_TXT_DIR_ALREADY_EXIST_TITLE = "Adresář již existuje",
                    PE_JPM_TXT_ERROR_WHILE_CREATING_DIR_TEXT = "Nastala chyba při vytváření adresáře (/ adresářů)!",
			PE_JPM_TXT_EROR_WHILE_CREATING_DIR_TITLE = "Chyba při vytváření",
			PE_JPM_TXT_CANT_DELETE_FILES_FROM_OPEN_PROJECT_TEXT = "Nelze smazat soubory nebo adresáře z otevřeného projektu!",
                    PE_JPM_TXT_CANT_DELETE_FILES_FROM_OPEN_PROJECT_TITLE = "Chyba mazání",
			PE_JPM_TXT_FILE_DONT_EXIST_ALREADY_TEXT = "Označená položka již na cílovém umístění neexistuje!",
			PE_JPM_TXT_FILE_DONT_EXIST_ALREADY_TITLE = "Položka neexistuje",
                    PE_JPM_TXT_FAILED_TO_RENAME_FILE_TEXT = "Nepodařilo se přejmenovat položku!",
			PE_JPM_TXT_ORIGINAL_NAME = "Původní název", PE_JPM_TXT_NEW_NAME = "Nový název",
			PE_JPM_TXT_FAILED_TO_RENAME_FILE_TITLE = "Chyba při přejmenování",
			PE_JPM_TXT_CANNOT_RENAME_FILE_TEXT = "Nelze přejmenovat položku z aktuálně otevřeného projektu!",
			PE_JPM_TXT_CANNOT_RENAME_FILE_TITLE = "Položka používána",
                    PE_JPM_TXT_FILE_WITH_NAME_ALREADY_EXIST_TEXT = "Cílové umístění již obsahuje adresář se stejným názvem!",
			PE_JPM_TXT_TARGET = "Cíl", PE_JPM_TXT_FILE_WITH_NAME_ALREADY_EXIST_TITLE = "Chyba umístění",
			PE_JPM_TXT_ERROR_WHILE_MOVING_FILE_TEXT = "Nastala chyba při přesunu označeného adresáře na cílové umístění!",
			PE_JPM_TXT_SOURCE = "Zdroj", PE_JPM_TXT_ERROR_WHILE_MOVING_FILE_TITLE = "Chyba přesunu adresáře",
                    PE_JPM_TXT_FILE_ALREADY_EXIST_IN_LOCATION_TEXT = "Cílové umístění již obsahuje soubor se stejným názvem!",
			PE_JPM_TXT_FILE_ALREADY_EXIST_IN_LOCATION_TITLE = "Chyba umístění",
                    PE_JPM_TXT_ERROR_WHILE_MOVING_FILE_TO_LOCATION_TEXT = "Nastala chyba při přesunu označeného souboru na cílové umístění!",
			PE_JPM_TXT_ERROR_WHILE_MOVING_FILE_TO_LOCATION_TITLE = "Chyba přesunu souboru",
                    PE_JPM_TXT_TARGET_PLACE_CONTAIN_FILE_NAME_TEXT = "Cílové umístění již obsahuje adresář se stejným názvem!",
			PE_JPM_TXT_TARGET_PLACE_CONTAIN_FILE_NAME_TITLE = "Chyba umístění",
                    PE_JPM_TXT_ERROR_WHILE_COPYING_DIR_TEXT = "Nastala chyba při kopírování označeného adresáře na cílové umístění!",
			PE_JPM_TXT_ERROR_WHILE_COPYING_DIR_TITLE = "Chyba kopírování adresáře",
			PE_JPM_TXT_ERROR_WHILE_COPYING_SELECTED_FILE_TEXT = "Nastala chyba při kopírování označeného souboru na cílové umístění!",
			PE_JPM_TXT_ERROR_WHILE_COPYING_SELECTED_FILE_TITLE = "chyba kopírování souboru",
                    PE_JPM_TXT_TARGET_LOCATION_DONT_EXIST_TEXT = "Cílové umístění již neexistuje nebo se nejedná o adresář!",
			PE_JPM_TXT_TARGET_LOCATION_DONT_EXIST_TITLE = "Chyba umístění",
                    PE_JPM_TXT_SELECTED_FILE_DOESNT_EXIST_TEXT = "Dříve označený soubor či adresář již neexistuje!",
			PE_JPM_TXT_SELECTED_FILE_DOESNT_EXIST_TITLE = "Chybí soubor",
                    PE_JPM_TXT_CANNOT_MOVE_FILE_FROM_OPEN_PROJECT_TEXT = "Nelze přesunout soubory nebo adresáře z otevřeného projektu!",
			PE_JPM_TXT_CANNOT_MOVE_FILE_FROM_OPEN_PROJECT_TITLE = "Chyba přesunu",
                    PE_JPM_TXT_ERROR_WHILE_DLETING_DIR_TEXT = "Nastala chyba při mazání označeného adresáře!",
			PE_JPM_TXT_ERROR_WHILE_DELETING_DIR_TITLE = "Chyba při odstraňování",
			PE_JPM_TXT_ERROR_WHILE_CREATING_NEW_LIST_IN_EXCEL_FILE_TEXT_1 = "Došlo k chybě při pokusu o vytvoření nového listu s názvem",
                    PE_JPM_TXT_ERROR_WHILE_CREATING_NEW_LIST_IN_EXCEL_FILE_TEXT_2 = "List se nepodařilo vytvořit.",
			PE_JPM_TXT_ERROR_WHILE_CREATING_NEW_LIST_IN_EXCEL_FILE_TEXT_3 = "Soubor",
			PE_JPM_TXT_ERROR_WHILE_CREATING_NEW_LIST_IN_EXCEL_FILE_TITLE = "Nastala chyba při vytváření listu";

	// Texty do třídy: Menu.java:
    // texty do pložek v menuFile:
    public static final String PE_M_MENU_FILE = "Soubor", PE_M_MENU_EDIT = "Upravit", PE_M_MENU_TOOLS = "Nástroje",
            PE_M_MENU_ORDER_FRAMES = "Uspořádat", PE_M_ITEM_OPEN_FILE = "Otevřít soubor",
            PE_M_MENU_OPEN_FILE_IN_APPS_OF_OS = "Otevřít", PE_M_ITEM_OPEN_FILE_IN_FILE_EXPLORER_IN_OS = "V průzkumn" +
            "íku souborů", PE_M_ITEM_OPEN_FILE_IN_DEFAULT_APP_IN_OS = "Ve výchozí aplikaci", PE_M_ITEM_SAVE = "Uložit",
            PE_M_ITEM_SAVE_AS = "Uložit jako", PE_M_ITEM_SAVE_ALL = "Uložit vše",
            PE_M_RB_ITEM_WRAP_TEXT_IN_SELECTED_FRAME = "Zalamovat text", PE_M_RB_ITEM_WRAP_TEXT_IN_ALL_FRAMES =
            "Zalamovat text všude",
            PE_M_RB_ITEM_SHOW_WHITE_SPACE_IN_SELECTED_FRAME = "Zobrazit bílé znaky",
            PE_M_RB_ITEM_SHOW_WHITE_SPACE_IN_ALL_FRAMES = "Zobrazit bílé znaky všude",
            PE_M_RB_ITEM_CLEAR_WHITE_SPACE_LINE_IN_SELECTED_FRAME = "Odebírat bílé znaky",
            PE_M_RB_ITEM_CLEAR_WHITE_SPACE_LINE_IN_ALL_FRAMES = "Odebírat bílé znaky všude",
            PE_M_ITEM_RELOAD = "Znovu načíst",
            PE_M_ITEM_RELOAD_ALL = "Znovu načíst vše", PE_M_ITEM_PRINT = "Tisk",
            PE_M_ITEM_CLOSE_SELECTED_WINDOW = "Zavřít označené okno",
            PE_M_ITEM_CLOSE_ALL_WINDOW = "Zavřít interní okna", PE_M_ITEM_CLOSE = "Zavřít",

			// ToolTipy na tlačítka v menufile:
            PE_M_TT_ITEM_OPEN_FILE = "Otevře dialogové okno pro výběr souboru, který chcete otevřít.",
			PE_M_TT_ITEM_OPEN_FILE_IN_FILE_EXPLORER_IN_OS = "Zobrazí soubor v označeném interním okně v průzkumníku souborů.",
			PE_M_TT_ITEM_OPEN_FILE_IN_DEFAULT_APP_IN_OS = "Otevře soubor v označeném interním okně v aplikaci nastavené pro otevření konkrétního typu souboru.",
                    PE_M_TT_ITEM_SAVE = "Uloží se text z editoru označeného interního okna zpět do původního souboru.",
			PE_M_TT_ITEM_SAVE_AS = "Otevře se dialogové okno pro označení souboru, kam se má uložit soubor otevřený v označeném interním okně.",
                    PE_M_TT_ITEM_SAVE_ALL = "Uloží se veškeré texty otevřené v interních oknech zpět do původních " +
                            "souborů.",
                    PE_M_TT_RB_ITEM_WRAP_TEXT_IN_SELECTED_FRAME = "Zalomit text v označeném interním okně / souboru.",
                    PE_M_TT_RB_ITEM_WRAP_TEXT_IN_ALL_FRAMES = "Zalomit text ve veškerých otevřených souborech / " +
                            "interních oknech.",
                    PE_M_TT_RB_ITEM_SHOW_WHITE_SPACE_IN_SELECTED_FRAME = "Zobrazit bílé znaky v označeném interním " +
                            "okně / souboru.",
                    PE_M_TT_RB_ITEM_SHOW_WHITE_SPACE_IN_ALL_FRAMES = "Zobrazit bílé znaky ve veškerých interních " +
                            "oknech / otevřených souborech.",
                    PE_M_TT_RB_ITEM_CLEAR_WHITE_SPACE_LINE_IN_SELECTED_FRAME = "Odebrat bílé znaky z prázdného řádku " +
                            "po stisknutí klávesy Enter v označeném interním okně / otevřeném souboru.",
                    PE_M_TT_RB_ITEM_CLEAR_WHITE_SPACE_LINE_IN_ALL_FRAMES = "Odebrat bílé znaky z prázdného řádku po " +
                            "stisknutí klávesy Enter ve veškerých interních oknech / otevřených souborech.",
                    PE_M_TT_ITEM_RELOAD = "Přenačte se text ze souboru do editoru v označeném interním okně.",
			PE_M_TT_ITEM_RELOAD_ALL = "Přenačtou se všechny soubory otevřené v interních oknech (pokud ještě existují).",
                    PE_M_TT_ITEM_PRINT = "Zobrazí se okno pro možnost vytisknutí textu v označeném interním okně.",
                    PE_M_TT_ITEM_CLOSE_SELECTED_WINDOW = "Zavře se soubor, který je otevřen v označeném interním okně.",
			PE_M_TT_ITEM_CLOSE_ALL_WINDOW = "Zavřou se všechna otevřená interní okna.",
                    PE_M_TT_ITEM_CLOSE = "Zavře se tento průzkumník projektů.",

			// texty do tlacitek v menuEdit:
            PE_M_ITEM_INDENT_MORE = "Odsadit více", PE_M_ITEM_INDENT_LESS = "Odsadit méně",
                    PE_M_ITEM_COMMENT = "Zakomentovat", PE_M_ITEM_UNCOMMENT = "Odkomentovat",
			PE_M_ITEM_FORMAT_CODE = "Zformátovat kód", PE_M_ITEM_INSERT_METHOD = "Vložit metodu",

			// Popisky do tlačítek v menuEdit:
            PE_M_TT_ITEM_INDENT_MORE = "Odsazení řádku, kde se nachází kurzor pomocí tabulátoru.",
                    PE_M_TT_INDENT_LESS = "Odebere tabulátor ze začátku řádku, kde se nachází kurzor.",
			PE_M_TT_ITEM_COMMENT = "Vloží řádkový komentář na řádek, kde se nachází kurzor.",
                    PE_M_TT_ITEM_UNCOMMENT = "Odebere řádkový komentář z řádku, kde se nachází kurzor.",
                    PE_M_TT_ITEM_FORMAT_CODE = "Zformátuje zdrojový kód v editoru v označeném interním okně dle syntaxe Javy.",
                    PE_M_TT_ITEM_INSERT_METHOD = "Vloží \"ukázkovou\" metodu na pozici kurzoru.",

			// Texty do tlačítek v menuTools:
            PE_M_ITEM_REPLACE_TEXT = "Nahradit", PE_M_ITEM_GO_TO_LINE = "Jdi na řádek",
			PE_M_ITEM_GETTERS_AND_SETTERS = "Generovat getry a setry", PE_M_ITEM_CONSTRUCTOR = "Generovat konstruktor",
                    PE_M_ITEM_COMPILE = "Zkompilovat", PE_M_MENU_ITEM_ADD_SERIAL_VERSION_ID = "Přidat serial ID",
			PE_M_ITEM_ADD_DEFAULT_SERIAL_VERSION_ID = "Vložit výchozí serial version ID",
                    PE_M_ITEM_ADD_GENERATED_SERIAL_VERSION_ID = "Vygenerovat nové serial version ID",

			// Popisky do tlačítek v menuTools:
            PE_M_TT_ITEM_REPLACE_TEXT = "Zobrazí dialog, ve kterém lze vybrat, zda se má nahradit první výskyt daného textu nebo veškeré výskyty daného textu v editoru zdrojového kódu.",
			PE_M_TT_ITEM_GO_TO_LINE = "Vloží kurzor myši na řádek se zadaným číslem.",
                    PE_M_TT_ITEM_TO_STRING = "Vloží na pozici kurzoru metodu toString().",
			PE_M_TT_ITEM_GETTERS_AND_SETTERS = "Otevře dialog pro výběr položek (proměnných) třídy, ke kterým je možné vložit přístupovou metodu (tj. getr nebo setr).",
                    PE_M_TT_ITEM_CONSTRUCTOR = "Otevře dialog, ve kterém je možno označit položky (proměnné), které je možné přidat do parametru konstruktoru.",
			PE_M_TT_ITEM_COMPILE = "Zkompilují se třídy, které se nachází v diagramu tříd.",
                    PE_M_TT_ITEM_ADD_DEFAULT_SERIAL_VERSION_ID = "Vloží se na pozici kurzoru výchozí serial version ID.",
			PE_M_TT_ITEM_ADD_GENERATED_SERIAL_VERSION_ID = "Vloží se na pozici kurzoru nově vygenerované serial version ID.",

			// Texty do položek v menuOrderFrames:
            PE_M_ITEM_TILE = "Dlaždice", PE_M_ITEM_TILE_UNDER_HIM = "Dlaždice pod sebou", PE_M_ITEM_CASCADE = "Kaskáda",
			PE_M_ITEM_MINIMIZE_ALL_FRAMES = "Minimalizovat vše",

    // Popisky tlačítek v menuOrderFrames:
			PE_M_TT_ITEM_TILE = "Interní okna s otevřenými soubory se seřadí do podoby dlaždic neboli tabulky. (Pro tabulku je třeba otevřít 4 a více oken.",
			PE_M_TT_ITEM_TILE_UNDER_HIM = "Interní okna s otevřenými soubory se seřadi do podoby dlaždic, ale pod sebe, resp. tabulka s jedním sloupcem.",
            PE_M_TT_ITEM_CASCADE = "Interní okna s otevřenými soubory se seřadí do podoby tzv. Kaskády.",
            PE_M_TT_ITEM_MINIMIZE_ALL_FRAMES = "Minimalizuje všechna otevřená interní okna, resp. otevřené soubory.",

    // Texty do proměnných - do chybových hlášek:
			PE_M_TXT_CANNOT_SAVE_PDF_FILE_TEXT = "Tato aplikace neumožňuje uložení souboru typu '.pdf', je pouze pro čtení!",
            PE_M_TXT_LOCATION = "Umístění", PE_M_TXT_CANNOT_SAVE_PDF_FILE_TITLE = "Soubor nelze uložit",
			PE_M_TXT_FILE_COULD_NOT_BE_SAVED_TEXT = "Soubor se nepodařilo uložit!",
            PE_M_TXT_FILE_COULD_NOT_BE_SAVED_TITLE = "Chyba uložení",
			PE_M_TXT_CONTAINS_ARE_DIFFERENT_TEXT = "Obsahy editoru a souboru jsou různé, má se obsah editoru uložit?",
            PE_M_TXT_CONTAINS_ARE_DIFFERENT_TITLE = "Uložit soubor",
			PE_M_TXT_FILE_DOESNT_EXIST_TEXT = "Soubor již neexistuje, proto nelze vytisknout!",
            PE_M_TXT_ORIGINAL_LOCATION = "Původní umístění", PE_M_TXT_FILE_DOESNT_EXIST_TITLE = "Soubor neexistuje",
			PE_M_TXT_CLASS_FILE_IS_NOT_LOADED_TEXT = "Nepodařilo se načíst přeložený - zkompilovaný soubor '.class' označené třídy!",
            PE_M_TXT_CLASS_FILE_IS_NOT_LOADED_TITLE = "Nebyl načten soubor",
			PE_M_TXT_CANNOT_GENERATED_ACCESS_METHODS_TEXT = "Nejedná se o třídu z diagramu tříd, nelze vygenerovat přístupové metody!",
            PE_M_TXT_CLASS_IS_NOT_IN_CLASS_DIAGRAM = "Třída není v diagramu tříd",
			PE_M_TXT_CANNOT_GENERATE_CONSTRUCTOR = "Nejedná se o třídu z diagramu tříd, nelze vygenerovat konstruktor!",
			PE_M_TXT_CLASS_DOESNT_EXIST_ALREADY_TEXT = "Třída již na získaném umístění neexistuje!",
			PE_M_TXT_CLASS_DOESNT_EXIST_ALREADY_TITLE = "Soubor chybí",
			PE_M_TXT_SRC_DIR_IS_MISSING_TEXT = "Adresář src, který obsahuje třídy (včetně balíčků) nebyl nalezen v otevřeném projektu!",
			PE_M_TXT_SRC_DIR_IS_MISSING_TITLE = "Chybí adresář src";


	// Texty do třídy: MyInternalFrame.java:
	public static final String PE_MIF_TXT_SAVE_CHANGES_TEXT = "Chcete uložit provedené změny?",
			PE_MIF_TXT_SAVE_CHANGES_TITLE = "Uložit změny",
			PE_MIF_TXT_FILE_IS_NOT_WRITTEN_TEXT = "Soubor se na nové umístění nepodařilo zapsat!",
			PE_MIF_TXT_LOCATION = "Umístění", PE_MIF_TXT_FILE_IS_NOT_WRITTEN_TITLE = "Chyba uložení",
			PE_MIF_TXT_PDF_FILE_CANNOT_SAVE_TEXT = "Tato aplikace neumožňuje uložení souboru typu '.pdf'!",
			PE_MIF_TXT_PDF_FILE_CANNOT_SAVE_TITLE = "Soubor nelze uložit",
			PE_MIF_TXT_ERROR_WHILE_SAVING_FILE_TEXT = "Soubor se nepodařilo uložit!",
			PE_MIF_TXT_ERROR_WHILE_SAVING_FILE_TITLE = "Chyba uložení", PE_MIF_TXT_READ_ONLY = "Pouze pro čtení",
			PE_MIF_TXT_FILE_DOESNT_EXIST_CHOOSE_NEW_LOCATION_TEXT = "Soubor na původním umístění již neexistuje, chcete zvolit nové umístění pro tento soubor?",
			PE_MIF_TXT_ORIGINAL_LOCATION = "Původní umístění",
			PE_MIF_TXT_FILE_DOESNT_EXIST_CHOOSE_NEW_LOCATION_TITLE = "Soubor neexistuje";


	// Texty do třídy: MyWindowListener.java:
	// akorát do oken o tom, zda se mají veci uložit nebo ne:
	public static String PE_MWL_TXT_SAVE_CHANGES_TEXT = "Chcete uložit změny?",
			PE_MWL_TXT_SAVE_CHANGES_TITLE = "Uložit změny";


	// Texty do třídy: NewExcelListNameForm.java:
	// Texty do titulku dialogu a labelů s informacemi:
	public static final String PE_NELNM_DIALOG_TITLE = "Nový list",
			PE_NELNM_LBL_INFO = "Zadejte název nového listu do Excelu",
			PE_NELNM_LBL_ERROR = "Pouze velká a malá pismena (s diakritikou), číslice a podtržítka.",

			// Texty do tlačítek:
			PE_NELNM_BTN_OK = "OK", PE_NELNM_BTN_CANCEL = "Zrušit",

			// Texty do textových proměnných s hláškami:
			PE_NELNM_TXT_ERROR_FILE_NAME_TEXT = "Název listu neodpovídá požadované syntaxi!",
			PE_NELNM_TXT_ERROR_FILE_NAME_TEXT_SYNTAXE = "Syntaxe",
			PE_NELNM_TXT_ERROR_FILE_NAME_TEXT_SYNTAXE_INFO = "Velká a malá písmena (s diakritikou), číslice a podtržítka.",
			PE_NELNM_TXT_ERROR_FILE_NAME_TITLE = "Chybná syntaxe",
			PE_NELNM_TXT_IS_EMPTY_TEXT = "Textové pole pro název listu je prázdné!",
			PE_NELNM_TXT_IS_EMPTY_TITLE = "Prázdné pole";


	// Třída NewFileForm.java
    public static final String PE_NFF_DIALOG_TITLE = "Nový soubor",
			PE_NFF_LBL_INFO = "Zadejte název a vyberte příponu ('typ') souboru";


    // Texty do třídy: NewFolderForm.java:
	public static final String PE_NFOLDER_F_DIALOG_TITLE = "Vytvořit adresář (/ adresáře)", PE_NFOLDER_F_BTN_OK = "OK",
			PE_NFOLDER_F_BTN_CANCEL = "Zrušit",
			PE_NFOLDER_F_LBL_ERROR_INFO = "Adresář /ře jsou zadány ve špatném formátu!",
            PE_NFOLDER_F_LBL_INFO = "Zadejte adresář /ře, pro vytvoření více adresářů využijte desetinnou tečku",

			// Texty do chybovýchhlášek:
			PE_NFOLDER_F_TXT_WRONG_FORMAT_TEXT = "Formát adresářů neodpovídá požadované syntaxi!",
                    PE_NFOLDER_F_TXT_WRONG_FORMAT_TITLE = "Chybná syntaxe",
			PE_NFOLDER_F_TXT_EMPTY_FIELD_TEXT = "Pole pro zadání adresáře /řů je prázdné!",
			PE_NFOLDER_F_TXT_EMPTY_FIELD_TITLE = "Prázdné pole";



	// Texty do třdy: ProjectExplorerDialog.java:
	// text pro titulek dialogu:
    public static final String PE_PED_DIALOG_TITLE = "Průzkumník projektů";

	// texty do chybových a informačních hlášek:
	public static final String PE_PED_TXT_CANNOT_SAVE_PDF_FILE_TEXT = "Tato aplikace neumožňuje uložení souboru typu '.pdf', je pouze pro čtení!",
			PE_PED_TXT_LOCATION = "Umístění", PE_PED_TXT_CANNOT_SAVE_PDF_FILE_TITLE = "Soubor nelze uložit",
			PE_PED_TXT_FILE_IS_ALREADY_OPEN_TEXT = "Příslušý soubor je již otevřen!",
			PE_PED_TXT_FILE_IS_ALREADY_OPEN_TITLE = "Soubor otevřen",
			PE_PED_TXT_EXCEL_DONT_CONTAINS_LIST_TEXT = "Označený Excelovský dokument neobsahuje žádný list!",
			PE_PED_TXT_FILE = "Soubor", PE_PED_TXT_EXCEL_FILE_DONT_CONTAINS_LIST_TITLE = "Soubor neobsahuje žádný list",
			PE_PED_TXT_FILE_KIND = "Soubor typu", PE_PED_TXT_DO_NOT_OPEN = "nelze otevřít v editoru kódu!",
			PE_PED_TXT_FILE_DO_NOT_OPEN = "Soubor nelze otevřít",
			PE_PED_TXT_FILE_DO_NOT_EXIST_TEXT = "Požadovaný soubor již neexistuje, nebo se jedná o adresář!",
			PE_PED_TXT_FILE_DO_NOT_EXIST_TITLE = "Chyba souboru";


















	// Texty do "Výstupního terminálu":
    // Texty do třídy OutputFrame:
	public static final String OF_FRAME_TITLE = "Výstupní terminál";

	// Texty do třídy OutputFrameMenu.java:
    // Texty do tlačítek:
    public static final String OF_MENU_MENU_OPTIONS = "Možnosti", OF_MENU_CLEAR = "Vymazat",
            OF_MENU_CLEAR_SYS_OUT_SCREEN = "Vymazat obrazovku",
            OF_MENU_SAVE_TO_FILE = "Uložit do souboru", OF_MENU_PRINT = "Vytisknout",
            OF_MENU_ITEM_CLOSE = "Zavřít", OF_MENU_ITEM_CLEAR_ALL = "Vše",


    // Popisky - tooltipy k položkám:
    OF_MENU_TT_ITEM_CLOSE = "Zavře toto okno - Výstupní terminál.",

    // Texty pro "skládání" vět pro rozdělení textů dle System.out a err:
    OF_MENU_TXT_AND = "a", OF_MENU_TXT_DELETE_TEXT_IN = "Vymaže texty v", OF_MENU_TXT_SAVE_TEXT_IN = "Otevře dialog " +
            "pro zvolení umístění souboru s texty v", OF_MENU_TXT_PRINT_TEXT_IN = "Zobrazí dialog pro nastavení možností " +
            "tisku textů v",

    // Texty do chybových hlášek:
    OF_MENU_TXT_FILE_IS_NOT_WRITTEN_TEXT = "Soubor se nepodařilo zapsat!", OF_MENU_TXT_LOCATION = "Umístění",
            OF_MENU_TXT_FILE_IS_NOT_WRITTEN_TITLE = "Chyba zápisu",

    // Texty pro JCheckBoxy:
    OF_MENU_CHCB_RECORD_CALL_CONSTRUCTORS = "Zaznamenat volání konstruktorů",
            OF_MENU_CHCB_RECORD_CALL_METHODS = "Zaznamenat volání metod",
            OF_MENU_CHCB_RECORD_CALL_CONSTRUCTORS_TT = "Před výpis do konzole pomocí metod v System.out bude vypsána " +
                    "hlavička zavolaného konstruktoru.",
            OF_MENU_CHCB_RECORD_CALL_METHODOS_TT = "Před výpis do konzole pomocí metod v System.out bude vypsána " +
                    "hlavička zavolané metody.",

    OF_MENU_CHCB_CLEAR_SYS_OUT_SCREEN_WHEN_CALL_CONSTRUCTOR = "Při volání konstruktorů",
            OF_MENU_CHCB_CLEAR_SYS_OUT_SCREEN_WHEN_CALL_METHOD = "Při volání metod",

    OF_MENU_CHCB_CLEAR_SYS_OUT_SCREEN_WHEN_CALL_CONSTRUCTOR_TT = "Před vypsáním textů pomocí metod v System.out " +
            "nacházejícím se v zavolaném konstruktoru bude vymazána obrazovka pro tyto výpisy.",
            OF_MENU_CHCB_CLEAR_SYS_OUT_SCREEN_WHEN_CALL_METHOD_TT = "Před vypsáním textů pomocí metod v System.out " +
                    "nacházejícím se v zavolané metodě bude vymazána obrazovka pro tyto výpisy.";














	// Texty do třídy ExceptionLogger - akorát texty do chybových hlášek:
	public static final String ML_TXT_FAILED_TO_LOAD_OR_CREATING_FILE_TEXT = "Nepodařilo se vytvořit nebo načíst soubor",
			ML_TXT_FOR_WRITE_EXCEPTION = "pro zapsání vzniklé vyjímky.",
			ML_TXT_ERROR_IN_ACCESS_TO_FILE = "Jedná se o typ vyjímky, kdy nastala chyba při přístupu k souboru",
			ML_TXT_FAILED_TO_LOAD_OR_CREATING_FILE_TITLE = "Nepodařilo se načíst soubor pro logs",
			ML_TXT_ERROR_WHILE_OPENING_FILE = "Jedná se o typ vyjímky, kdy nastala chyba při otevírání souboru",
			ML_TXT_ERROR_WHILE_CREATING_DIR_TEXT = "Nastala chyba při pokusu o vytvoření adresářů na následující cestě",
			ML_TXT_ERROR_WHILE_CREATING_DIR_TITLE = "Chyba při vytváření adresářů";













	// Texty do třídy / dialogu NameForInstance:
	public static final String NFI_DIALOG_TITLE = "Vytvořit referenci", NFI_LBL_INSTANCE_CLASS_INFO = "Instance třídy",
			NFI_LBL_INSTANCE = "Instance", NFI_INSERT_REFERENCE = "Zadejte referenci",

			// Texty do labelu, který hlásí chybu při stisku klávesy:
			NFI_TXT_BAD_SYNTAX_IN_REFERENCE = "Zadaná reference neodpovídá požadované syntaxi.",
			NFI_TXT_REFERENCE_ALREADY_EXIST = "Zadaná reference již existuje.",

			// Texty do chybových hlášek:
			NFI_TXT_JOP_EMPTY_FIELD_TEXT = "Pole pro zadání reference je prázdné.",
			NFI_TXT_JOP_EMPTY_FIELD_TITLE = "Prázdné pole",
			NFI_TXT_JOP_BAD_SYNTAX_TEXT = "Zadaná reference neodpovídá požadované syntaxi. Musí začínat velkým nebo malým písmenem a může obsahovat pouze velká a malá písmena bez diakritiky, číslice a podtržítka.",
			NFI_TXT_JOP_BAD_SYNTAX_TITLE = "Chybná syntaxe",
			NFI_TXT_JOP_DUPLICITE_REFERENCE_TEXT = "Zadaná reference je již využita, zvolte prosím jinou.",
			NFI_TXT_JOP_DUPLICITE_REFERENCE_TITLE = "Duplicitní reference",

			// Texty do tlačítek:
			NFI_BTN_OK = "OK", NFI_BTN_CANCEL = "Zrušit";






















	// Texty do dialogu pro informace o autorovi aplikace - dialog, resp. třída AboutAppForm.java:
	// Jendá se o některé texty , které je možné přeložit do více jazyků:
    public static final String AAF_DIALOG_TITLE = "O", AAF_BTN_CLOSE = "Zavřít", AAF_BTN_JAVA_VERSION = "Java verze",
            AAF_TXT_NAME = "Název", AAF_TXT_VERSION = "Verze", AAF_TXT_AUTHOR = "Autor", AAF_TXT_WIKI_ABOUT_APP =
            "Wiki o aplikaci", AAF_TXT_EMAIL = "Email",
            AAF_TXT_UNKNOWN = "Neuvedeno", AAF_TXT_DOES_NOT_EXIST = "Neexistuje", AAF_TXT_DEBUG_INFO = "Protokol běhu" +
            " aplikace",
            AAF_TXT_FOR_APP = "pro aplikaci";

    // Texty pro chybové hlášky pro otevření kontextového menu pro otevření souboru nebo adresáře:
    public static final String AAF_TXT_PATH = "Cesta",
            AAF_TXT_DIR_DOES_NOT_EXIST_TEXT = "Adresář na následující cestě nebyl nalezen.",
            AAF_TXT_DIR_DOES_NOT_EXIST_TITLE = "Adresář nenalezen",
            AAF_TXT_FILE_DOES_NOT_EXIST_TEXT = "Soubor na následující cestě nebyl nalezen.",
            AAF_TXT_FILE_DOES_NOT_EXIST_TITLE = "Soubor nenalezen";


    /*
     * Texty do kontextového menu, keré se otevře po kliknutí pravým tlačítkem myši na jeden z vybraných labelů ve
     * spodní části dialogu s informacemi o autorovi aplikace.. Texty do třídy: cz.uhk.fim.fimj.forms.about_app_form
     * .PopupMenu
     */
    // Texty do položek:
    public static final String AAF_PM_ITEM_OPEN_IN_FILE_EXPLORER_TEXT = "Otevřít v průzkumníku souborů",
            AAF_PM_ITEM_OPEN_IN_PROJECT_EXPLORER_TEXT = "Otevřít v průzkumníku projektů",
            AAF_PM_ITEM_OPEN_IN_CODE_EDITOR_TEXT = "Otevřít v editoru kódu",
            AAF_PM_ITEM_OPEN_IN_DEFAULT_APP = "Otevřít ve výchozí aplikaci",
    // Tooltipy do položek:
    AAF_PM_ITEM_OPEN_IN_FILE_EXPLORER_TT = "Otevře zvolený soubor nebo adresář ve výchozím průzkumníku souborů (dle " +
            "využívané platformy).",
            AAF_PM_ITEM_OPEN_IN_PROJECT_EXPLORER_TT = "Otevře zvolený soubor nebo adresář v dialogu průzkumník " +
                    "projektů.",
            AAF_PM_ITEM_OPEN_IN_CODE_EDITOR_TT = "Otevře zvolený soubor nebo adresář v dialogu editor zdrojového kódu.",
            AAF_PM_ITEM_OPEN_IN_DEFAULT_APP_TT = "Otevře zvolený soubor nebo adresář ve výchozí aplikaci v používané " +
                    "platformě.";








    // Texty do třídy, resp. vlákna CheckCompileFiles v balíčku App. jedná se pouze
	// o texty pro výpisy ohledné výsledku porovední příslušné operace a sice
	// nalezení potřebných souborů pro aplikaci, aby mohla kompilovat Javovské
	// třídy.
	public static final String CCF_JOP_TXT_FILES_ALREADY_EXIST_TEXT = "Potřebné soubory pro kompilování tříd se již nachází na daném umístění",
			CCF_JOP_TXT_FILES_ALREADY_EXIST_TITLE = "Soubory umístěny",
			CCF_JOP_TXT_FILES_SUCCESSFULLY_COPIED_TEXT = "Potřebné soubory pro kompilování tříd byly úspěšně zkopírovány do adresáře",
			CCF_JOP_TXT_FILES_SUCCESSFULLY_COPIED_TITLE = "Soubory zkopírovány",
			CCF_JOP_TXT_FILES_NOT_FOUND_TEXT = "Potřebné soubory pro kompilování tříd nebyly nalezeny. Pro nastavení těchto souborů využijte prosím příslušný dialog.",
			CCF_JOP_TXT_FILES_NOT_FOUND_TITLE = "Soubory nenalezeny";











	// Texty do třídy s infomacemi ohledne ovládaní aplikace - pouze základní inforamce, třída: InfoForm.java (IF - InfoForm)
	public static final String IF_DIALOG_TITLE = "Info", IF_TXT_BASIC_INFO = "Základní ovládání aplikace",
			IF_TXT_CLASS_DIAGRAM = "Diagram tříd", IF_TXT_CREATE_CLASS = "Vytvoření třídy",
			IF_TXT_INFO_HOW_TO_CREATE_CLASS = "Třídu lze vytvořit kliknutím na tlačítko 'Vytvořit třídu' v toolbaru (lišta vlevo).",
			IF_TXT_INFO_HOW_TO_WRITE_CLASS_NAME = "Do dialogu pro vytvoření třídy je třeba zadat název třídy, popř. i balíčky.",
			IF_TXT_INFO_FOR_CLASS_NAME_WITHOUT_PACKAGES = "Pokud nebudou zadány balíčky, třída bude vložena do výchozího balíčku 'defaultPackage'.",
			IF_TXT_CHOOSE_TEMPLATE_FOR_CLASS = "Dále je třeba vybrat jednu z předdefinovaných šablon tříd jazyka Java, popř. připravenou metodu 'main'.",
			IF_TXT_ADD_RELATIONS = "Přidání vztahů",
			IF_TXT_INFO_FOR_CLICK_ON_BUTTON = "Je třeba označit potřebný vztah kliknutím na tlačítko v toolbaru.",
			IF_TXT_CHOOSE_CLASSES_BY_CLICK = "Následně je třeba označit (kliknutím levým tlačítkem myši) dvě třídy v diagramu tříd, mezi kterými se má vztah vytvořit.",
			IF_TXT_ADDED_SYNTAXE_TO_SOURCE_CODE = "Do zdrojového kódu zvolených tříd bude doplněna syntaxe, která reprezentuje zvolený vztah (syntaxe záleží na zvoleném vztahu).",
			IF_TXT_INFO_FOR_CREATE_EDGE = "Na konec bude v diagramu tříd mezi označenými třídami vytvořena šipka (hrana), která bude reprezentovat zvolený vztah mezi třídami.",
			IF_TXT_INFO_FOR_ADD_COMMENT = "V případě přidání komentáře (lze pouze ke třídě) je třeba kliknout na příslušné tlačítko v toolbaru a označit třídu, ke které se má přidat komentář.",
			IF_TXT_REMOVE_OBJECT_OR_RELATIONS = "Odebrání objektů / vztahů",
			IF_TXT_INFO_HOW_TO_REMOVE_OBJECT = "Objekt z diagramu tříd je možné odebrat kliknutím na objekt pravým tlačítkem myši a v kontextovém menu zvolit položku 'Odebrat'.",
			IF_TXT_INFO_FOR_REMOVE_COMMENT = "V případě šipky komentáře bude odebrán i samotný komentář.",
			IF_TXT_INFO_HOW_TO_REMOVE_RELATIONS = "V případě šipky, která reprezentuje jeden z možných vztahů bude odebrána i syntaxe ze zdrojového kódu tříd (pouze v rámci rozpoznaných regulárních výrazů).",
			IF_TXT_INFO_HOW_TO_REMOVE_CLASS = "V případě třídy je třeba smazat téměř všechny vztahy mezi třídami, pak lze třídu odebrat.",
			IF_TXT_INSTANCES_DIAGRAM = "Diagram instancí", IF_TXT_CREATE_INSTANCE_TEXT = "Vytvoření instance",
			IF_TXT_INFO_HOW_TO_CREATE_INSTANCE = "Instanci lze vytvořit kliknutím pravým tlačítkem myši na třídu v diagramu tříd a v kontextovém menu zvolit konstruktor, který se má zavolat.",
			IF_TXT_INFO_ABOUT_DIALOG_FOR_REFERENCE = "Následně bude zobrazeno dialogové okno, kam je třeba zadat název reference na instanci (popř. parametry - pouze některé povolené).",
			IF_TXT_INFO_ABOUT_ADDED_EDGE_TO_DIAGRAM = "Na konec, pokud dojde k úspěšnému zavolání konstruktoru (vytvoření instance) bude do diagramu instancí přidána její reprezentace.",
			IF_TXT_REMOVE_INSTANCE_TEXT = "Odebrání instance",
			IF_TXT_INFO_HOW_TO_REMOVE_INSTANCE = "Instanci lze odebrat kliknutím na položku 'Odebrat' v kontextovém menu nad instancí třídy.",
			IF_TXT_SHOW_CONTEXT_MENU = "Toto menu lze zobrazit kliknutím pravým tlačítkem myši na instanci třídy.",
			IF_TXT_DELETE_INSTANCE_BY_DEL_KEY = "Dále je možné odebrat označenou instanci v diagramu instancí stisknutím klávesy Delete ('Del).",
			IF_TXT_CREATE_RELATIONS_TEXT = "Vytvoření vztahů",
			IF_TXT_INFO_HOW_TO_CREATE_RELATIONS = "Vztahy mezi instancemi lze vytvořit pomocí naplnění příslušných proměnných v instancích třídy.",
			IF_TXT_INFO_HOW_TO_FILL_VARIABLE = "Proměnné lze naplnit například zavoláním metody nebo příslušným příkazem v editoru příkazů.",
			IF_TXT_CALLING_METHOD_TEXT = "Volání metod",
			IF_TXT_INFO_HOW_TO_CALL_METHOD = "Metodu lze zavolat pomocí kontextového menu nad třídou v diagramu tříd nebo nad instancí v diagramu instancí.",
			IF_TXT_INFO_CALL_STATIC_METHOD_IN_CONTEXT_MENU_OVER_CLASS = "Nad třídou v diagramu tříd je možné zavolat pouze statické metody, je třeba pouze vybrat v kontextovém menu nad třídou v záložce 'Statické metody' nebo 'Zděděné statické metody'.",
			IF_TXT_INFO_CALL_METHOD_OVER_INSTANCE_IN_ID = "Nad instancí v diagramu instancí je možné zavolat metody, kterou jsou i nejsou statické, je třeba pouze vybrat v kontextovém menu nad instanci příslušnou metodu, popřípadě v záložce 'Zděděné metody'.",
			IF_TXT_INFO_HOW_TO_CALL_METHOD_2 = "Po kliknutí na příslušou položku reprezentující metodu bude tato metoda v případě, že neobsahuje žádné parametry ihned zavolána a výsledek bude vypsán do editoru výstupů.",
			IF_TXT_CALL_METHOD_DIALOG_FOR_PARAMETERS = "Pokud metoda obsahuje nějaké parametry, bude zobrazen dialog, kde je třeba je vyplnit.",
			IF_TXT_INFO_ONLY_FEW_SUPPORTED_DATA_TYPES_IN_METHOD_PARAMETERS = "V dialogu pro zavolání metody s parametry jsou podporovány pouze některé typy parametrů, jako jsou napříkad 'základní' datové typy Javy, pole, mapa a list.",
			IF_TXT_INFO_ABOUT_LOADING_VARS_FROM_CLASSES = "V případě, že bude parametr metody obsahovat takový typ parametru, který není podporován aplikací pro zadání, bude možné jej naplnit pouze v případě, že nějaká třída nebo instance třídy obsahuje naplněnou proměnnou příslušného datového typu. V opačném případě nebude možné metodu zavolat",
			IF_TXT_COMMAND_EDITOR_TEXT = "Editor příkazů",
			IF_TXT_INFO_ABOUT_COMMAND_EDITOR = "V tomto editoru lze zadávat některé příkazy pro práci s instancemi (včetně jejich vytvoření).",
			IF_TXT_INFO_HOW_TO_PRINT_ALL_COMMANDS = "Veškeré příkazy, které lze v tomto editoru zadávat je možné vypsat příkazem 'printCommands();'.",
			IF_TXT_INFO_ABOUT_LIST_OF_COMMANDS = "Tj. příkazy pro vytvoření instance, naplnení proměnné, zavolání metody, inkrementace či dekrementace atd. (viz po zadání příkazu výše)",
			IF_TXT_SOURCE_CODE_EDITOR_TEXT = "Editor zdrojového kódu",
			IF_TXT_INFO_ABOUT_SOURCE_CODE_EDITOR = "V tomto editoru lze editovat zdrojový kód třídy (případně i jiných souborů, které lze v tomto editoru otevřít).",
			IF_TXT_INFO_ABOUT_FUNCTIONS_OF_EDITOR = "Dále je zde možné třídy zkompilovat, vytisknout zdrojový kód, vygenerovat ukázkovou metodu atd.",
			IF_TXT_INFO_ABOUT_GENERATE_CONSTRUCTOR = "Je zde doplněna možnost vygenerovat konstruktor s proměnnými v dané třídě.",
			IF_TXT_INFO_ABOUT_GENERATE_ACCESS_METHODS = "Je zde doplněna možnost vygenerovat přístupové metody k proměnným v dané třídě.",
			IF_TXT_INFO_ABOUT_CONTENT_OF_CODE_EDITOR = "Editor obsahuje ve spodní části textové pole, do kterého jsou vypisovány zprávy o kompilaci.",
			IF_TXT_INFO_HOW_TO_OPEN_CODE_EDITOR = "Editor lze otevřit pomocí kontextového menu nad třídou v diagramu tříd (pomocí položky 'Otevřít' -> 'V editoru kódu').",
			IF_TXT_OPEN_DIFERENT_FILES_IN_PROJECT_EXPLORER_DIALOG = "Otevření různých souborů je možné provést kliknutím na položku v menu Nástroje -> Otevřít soubor -> V editoru kódu. (Popř. V průzkumníku projektů.)",
			IF_TXT_PROJECT_EXPLORER_TEXT = "Průzkumník projektů",
			IF_TXT_INFO_HOW_TO_OPEN_PROJECT_EXPLORER = "Tento dialog lze otevřít buď pomocí jedné z položek v menu 'Průzkumník projektů' v nabídce 'Nástroje', nebo pomocí kontextového menu nad třídou v diagramu tříd 'Otevřít' -> 'V průzkumníku projektů'.",
			IF_TXT_INFO_HOW_TO_OPEN_CODE_EDITOR_2 = "Nebo dvojklikem na třídu v diagramu tříd, nebo jejím označením a stisknutím klávesy 'Enter'.",
			IF_TXT_INFO_ABOUT_OPENNED_WORKSPACE_DIR = "Ve stromové struktuře tohoto dialogu bude načten jako kořenový adresář buď adresář zvolený jako workspace, nebo adresář otevřeného projektu (dle volby).",
			IF_TXT_INFO_ABOUT_ACCESSED_MENU_FIELD = "Dle této volby se také buď zpřístupní nebo znepřístupní některé položky.",
			IF_TXT_INFO_ABOUT_OPEN_MORE_KIND_OF_FILES = "V tomto dialogu lze otevřít více souborů najednou a různých typů, například Pdf, Excel, Word, Txt, properties, html a další.",
			IF_TXT_INFO_ABOUT_WRITING_SOURCE_CODE = "Obsahuje stejné možnosti manipulace se zdrojovým kódem, jako výše zmíněný editor zdrojového kódu, včetně kompilace, generování konstruktorů a přístupových metod atd.",
			IF_TXT_INFO_HOW_TO_OPEN_FILE = "Otevření souboru je možné provést dvojkliknutím na soubor ve stromové struktuře (vlevo), nebo po kliknutí pravým tlačítkem myši na soubor a zvolit 'Otevřít'.",
			IF_TXT_HOW_TO_OPEN_OTHER_FILE = "Popř. lze zvolit vlastní soubor, který se nenachází v požadovaném adresáři pomocí položky 'Otevřít soubor' v nabídce 'Soubor'.";






























	// Následuje výchozí nastavení pro dialog FontSizeForm a texty do tohoto dialogu
	// (dialogu pro "rychlé" nastavení velikosti textu):


	/**
	 * Logická proměnná, která značí, zda se mají aktualizovat diagram tříd a
	 * diagram instancí i když zrovna uživatel hýbe s jedním z posuvníků v dialogu
	 * FontSizeForm (true), pokud je false, tak se změny velikosti písma projeví až
	 * po tom, co se ten posuvník "nastaví" - přesune a přestane se s ním hýbat, že
	 * se uvolní od myši.
	 */
	public static final boolean UPDATE_DIAGRAMS_WHILE_MOVING_WITH_SLIDER = false;


	/**
	 * Výčtová hodnota, která značí, jaký panel v dialogu FontSizeForm je jako
	 * výchozí označen, tj. jaký panel s komponentami je vybrán a zobrazen v
	 * dialogu.
	 */
	public static final FontSizeFormType FONT_SIZE_FORM_TYPE_SELECTED_PANEL = FontSizeFormType.ONE_SAME_SIZE;


	/**
	 * Nyní následují hodnoty, které značí označené JCheckBoxy v panelu, kde se
	 * nastavuje velikost písma jen pro ty objekty z diagramu tříd a z diagramu
	 * instancí, které uživatel zvolí.
	 */

	public static final boolean FSF_CD_CLASS_CELL = false, FSF_CD_COMMENT_CELL = false, FSF_CD_COMMENT_EDGE = false,
			FSF_CD_EXTENDS_EDGE = false, FSF_CD_IMPLEMENTS_EDGE = false, FSF_CD_ASSOCIATION_EDGE = false,
			FSF_CD_AGGREGATION_1_1_EDGE = false, FSF_CD_AGGREGATION_1_N_EDGE = false, FSF_ID_INSTANCE_CELL = false,
			FSF_ID_INSTANCE_CELL_ATTRIBUTES = false, FSF_ID_INSTANCE_CELL_METHODS = false,
			FSF_ID_ASSOCIATION_EDGE = false, FSF_ID_AGGREGATION_EDGE = false;




	// Následují texty do dialogu FontSizeForm a jeho částí:

	// Texty do dialogu FontSizeForm:
	public static final String FSF_DIALOG_TITLE = "Velikost písma";

	// Texty do menu dialogu FontSizeForm:
	public static final String FSF_MENU_MENU_DIALOG = "Dialog", FSF_MENU_KIND_OF_FORM = "Možnosti nastavení",

			FSF_MENU_RB_KIND_ITEM_ONE_SAME_SIZE_TEXT = "Jedna velikost",
			FSF_MENU_RB_KIND_ITEM_ONE_SIZE_FOR_CHOOSED_OBJECTS_TEXT = "Individuálně dle výběru",
			FSF_MENU_RB_KIND_ITEM_ONE_SIZE_FOR_EACH_OBJECT_TEXT = "Individuálně",

			FSF_MENU_RB_KIND_ITEM_ONE_SAME_SIZE_TT = "Nastavení stejné velikosti písma pro veškeré objekty v diagramu tříd a v diagramu instancí.",
			FSF_MENU_RB_KIND_ITEM_ONE_SIZE_FOR_CHOOSED_OBJECTS_TT = "Nastavení stejné velikosti písma pro vybrané objekty v diagramu tříd a v diagramu instancí.",
			FSF_MENU_RB_KIND_ITEM_ONE_SIZE_FOR_EACH_OBJECT_TT = "Nastavení velikosti písma každého objektu v diagramu tříd a v diagramu instnací individuálně.",

			FSF_MENU_RB_SET_FONTS_WHILE_MOVING_TEXT = "Aktualizovat velikost",
			FSF_MENU_RB_SET_FONTS_WHILE_MOVING_TT = "Velikost písma se nastaví při každém posunutí posuvníku (označeno) nebo až po nastavení posuvníku (neoznačeno).",

			FSF_MENU_ITEM_RESTORE_DEFAULT_TEXT = "Obnovit výchozí",
			FSF_MENU_ITEM_RESTORE_DEFAULT_TT = "Obnovení výchozích velikostí písem všech objektů v diagramu tříd a v diagramu instancí.",

			FSF_MENU_ITEM_CLOSE_TEXT = "Zavřít", FSF_MENU_ITEM_CLOSE_TT = "Uzavření tohoto dialogu.";


	// Texty do panelu OneSizeForEachObjectPanel, tj. panel, který obsahuje
	// komponenty pro nastavení každého objektu zvlášť:
	public static final String FSF_OSFEOP_LBL_INFO = "Nastavení velikosti písma pro každý objekt v diagramu tříd a v diagramu instancí individuálně.",

			FSF_OSFEOP_LBL_CLASS_DIAGRAM = "Diagram tříd", FSF_OSFEOP_LBL_INSTANCE_DIAGRAM = "Diagram instancí",

			FSF_OSFEOP_TXT_SIZE = "Velikost",

			FSF_OSFEOP_LBL_CLASS = "Třída", FSF_OSFEOP_LBL_COMMENT = "Komentář",
			FSF_OSFEOP_LBL_COMMENT_EDGE = "Hrana komentáře", FSF_OSFEOP_LBL_ASSOCIATION = "Hrana symetrické asociace",
			FSF_OSFEOP_LBL_AGGREGATION_1_1 = "Hrana asymetrické asociace", FSF_OSFEOP_LBL_EXTENDS = "Hrana dědičnosti",
			FSF_OSFEOP_LBL_IMPLEMENTS = "Hrana implementace rozhraní",
			FSF_OSFEOP_LBL_AGGREGATION_1_N = "Hrana agregace 1 : N",

			FSF_OSFEOP_LBL_INSTANCE = "Instance", FSF_OSFEOP_LBL_INSTANCE_ATTRIBUTES = "Atributy",
			FSF_OSFEOP_LBL_INSTANCE_METHODS = "Metody", FSF_OSFEOP_LBL_INSTANCE_ASSOCIATION = "Hrana asociace",
			FSF_OSFEOP_LBL_INSTANCE_AGGREGATION = "Hrana agregace";


	// Texty do panelu SameSizePanel, která obsahuje komponenty pro nastavení
	// velikosti písma pro objekty v diagramu tříd a v diagramu instnací, ale tak,
	// že je tam jen jeden slider, kterým se nastavují velikosti všech komponent v
	// diagramu tříd a v diagramu instancí na stejnou velikost.
	public static final String FSF_SSP_LBL_INFO = "Nastavení stejné velikosti písma pro veškeré objekty v diagramu tříd a v diagramu instancí.",
			FSF_SSP_TXT_SIZE = "Velikost", FSF_SSP_FONT_SIZE = "Velikost písma";


	// Texty do dialogu SameSizeSelectedObjectPanel. jedná se o panel, který
	// obsahuje komponenty pro nastavení velikosti písma objektům v diagramu tříd a
	// v diagramu instancí, uživatel si puze označí komponenty, kterým se má
	// nastavit velikost a pak nastaví tu velikost pomocí slideru.
	public static final String FSF_SSSOP_TXT_SIZE = "Velikost",
			FSF_SSSOP_LBL_INFO = "Nastavení stejné velikosti písma pro označené objekty v diagramu tříd a v diagramu instancí.",
			FSF_SSSOP_SLIDER_FONT_SIZE = "Velikost písma";


	// Texty do panelu SizeSelectedObjectCdPanel. Jedná se o panel, který obsahuje
	// komponenty pro označení objektů v diaagramu tříd, kterým se mí nastavit
	// velikost.
	public static final String FSF_SSOCP_BORDER_TITLE = "Diagram tříd",

			FSF_SSOCP_CHCB_CLASS_TEXT = "Třída", FSF_SSOCP_CHCB_COMMENT_TEXT = "Komentář",
			FSF_SSOCP_CHCB_COMMENT_EDGE_TEXT = "Hrana komentáře", FSF_SSOCP_CHCB_EXTENDS_TEXT = "Hrana dědičnost",
			FSF_SSOCP_CHCB_IMPLEMENTS_TEXT = "Hrana implementace rozhraní",
			FSF_SSOCP_CHCB_ASSOCIATION_TEXT = "Hrana symetrická asociace",
			FSF_SSOCP_CHCB_AGGREGATION_1_1_TEXT = "Hrana asymetrická asociace",
			FSF_SSOCP_CHCB_AGGREGATION_1_N_TEXT = "Hrana agregace 1 : N",

			FSF_SSOCP_CHCB_CLASS_TT = "Nastavení velikosti písma pro buňku reprezentující třídu v diagramu tříd.",
			FSF_SSOCP_CHCB_COMMENT_TT = "Nastavení velikosti písma pro buňku reprezentující komentář v diagramu tříd.",
			FSF_SSOCP_CHCB_COMMENT_EDGE_TT = "Nastavení velikosti písma pro hranu reprezentující spojení komentáře s třídou v diagramu tříd.",
			FSF_SSOCP_CHCB_EXTENDS_TT = "Nastavení velikosti písma pro hranu reprezentující vztah typu dědičnost v diagramu tříd.",
			FSF_SSOCP_CHCB_IMPLEMENTS_TT = "Nastavení velikosti písma pro hranu reprezentující vztah typu implementace rozhraní v diagramu tříd.",
			FSF_SSOCP_CHCB_ASSOCIATION_TT = "Nastavení velikosti písma pro hranu reprezentující vztah typu symetrická asociace v diagramu tříd.",
			FSF_SSOCP_CHCB_AGGREGATION_1_1_TT = "Nastavení velikosti písma pro hranu reprezentující asymetrickou asociaci v diagramu tříd.",
			FSF_SSOCP_CHCB_AGGREGATION_1_N_TT = "Nastavení velikosti písma pro hranu reprezentující vztah typu agregace 1 : N v diagramu tříd.",

			FSF_SSOCP_BTN_SELECT_ALL = "Označit vše", FSF_SSOCP_BTN_DESELECT_ALL = "Odznačit vše";



	// Texty do panelu SizeSelectedObjectIdPanel. Jedná se o panel, která obsahuje
	// komponenty pro označení objektů v diagramu instnací, kterým se má nastavit
	// velkost písma.
	public static final String FSF_SSOIP_BORDER_TITLE = "Diagram instancí",

			FSF_SSOIP_CHCB_INSTANCE_TEXT = "Instance", FSF_SSOIP_CHCB_INSTANCE_ATTRIBUTES_TEXT = "Atributy",
			FSF_SSOIP_CHCB_INSTANCE_METHODS_TEXT = "Metody", FSF_SSOIP_CHCB_ASSOCIATION_TEXT = "Asociace",
			FSF_SSOIP_CHCB_AGGREGATION_TEXT = "Agregace",

			FSF_SSOIP_CHCB_INSTANCE_TT = "Nastavení velikosti písma pro buňku reprezentující instanci v diagramu instancí.",
			FSF_SSOIP_CHCB_INSTANCE_ATTRIBUTES_TT = "Nastavení velikosti písma pro atributy v buňce reprezentující instanci v diagramu instancí.",
			FSF_SSOIP_CHCB_INSTANCE_METHODS_TT = "Nastavení velikosti písma pro metody v buňce reprezentující instanci v diagramu instancí.",
			FSF_SSOIP_CHCB_ASSOCIATION_TT = "Nastavení velikosti písma pro hranu reprezentující vztah typu asociace v diagramu instancí.",
			FSF_SSOIP_CHCB_AGGREGATION_TT = "Nastavení velikosti písma pro hranu reprezentující vztah typu agregace v diagramu instancí.",

			FSF_SSOIP_BTN_SELECT_ALL = "Označit vše", FSF_SSOIP_BTN_DELECT_ALL = "Odznačit vše";



	// Texty do třídy CheckDuplicityInDiagrams - texty do chybových hlášek, že byly
	// nalezeny nějaké duplicity.
	public static final String FSF_CDID_TXT_JOP_SAME_EDGES_ERROR_TEXT = "Následující hrany mají stejné nastavení",
			FSF_CDID_TXT_JOP_SAME_EDGES_ERROR_TITLE = "Nalezena duplicita",

			FSF_CDID_TXT_AGGREGATION_1_1 = "Asymetrická asociace", FSF_CDID_TXT_AGGREGATION_1_N = "Agregace 1 : N",
			FSF_CDID_TXT_ASSOCIATION = "Symetrická asociace", FSF_CDID_TXT_COMMENT_EDGE = "Hrana komentáře",
			FSF_CDID_TXT_INHERITANCE = "Dědičnost", FSF_CDID_TXT_UNKNOWN = "Neznámo";



	// Texty do třídy FontSizePanelAbstract, jedná se o abstraktní třídu, které
	// obsahuje některé textové proměnné, které obsahují texty pro chybové hlášky,
	// že byly například nalezeny duplicitní hrany adpod.
	public static final String
	// Duplicita bunky a komentare v diagramu tříd:
	FSF_FSPA_TXT_JOP_SAME_CELLS_TEXT = "Buňka reprezentující třídu a buňka reprezentující komentář v diagramu tříd měli stejné vlastnosti, byla jim nastavena výchozí velikosti písma.",
			FSF_FSPA_TXT_JOP_SAME_CELLS_TITLE = "Duplicitní hodnoty",

			// Texty pro duplicitní hrany v diagramu tříd:
			FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_1 = "Byly nalezeny duplicitní vlastnosti pro alespoň dvě hrany, následujícím hranám byly změneny velikosti písma",
			FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_2 = "Hrana symetrická asociace",
			FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_3 = "Hrana asymetrická asociace",
			FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_4 = "Hrana dědičnost",
			FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_5 = "Hrana agregace 1 : N",
			FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_6 = "Hrana komentáře",
			FSF_FSPA_TXT_JOP_SAME_EDGES_TITLE = "Duplicitní hodnoty",

			// Texty pro duplicitní hrany v diagramu instancí:
			FSF_FSPA_TXT_JOP_SAME_EDGES_IN_ID_TEXT_1 = "Hrany reprezentující vztahy typu asociace a agregace v diagramu instancí mají stejné vlastnosti. Byly jim změněny velikosti písma.",
			FSF_FSPA_TXT_JOP_SAME_EDGES_IN_ID_TEXT_2 = "Asociace",
			FSF_FSPA_TXT_JOP_SAME_EDGES_IN_ID_TEXT_3 = "Agregace",
			FSF_FSPA_TXT_JOP_SAME_EDGES_IN_ID_TITLE = "Duplicitní hodnoty";














	// Texty do dialogu SelectInstancesForCreateReference. Jedná se texty do
	// dialogu, který slouží pro to, aby uživatel označil instance, na které chce
	// vytvořit reference, tento dialog je možé otevřít kliknutím na tlačítko
	// vytvořit referenci v dialogu pro prohlížení instance s označenouu pložkou
	// pole nebo list, když obsahuje více jak jednu instanci, která se ještě
	// nenachází v diagramu instancí.
	public static final String SIFCR_DIALOG_TITLE = "Označení instancí",
			SIFCR_LBL_INFO = "Označte instance, na které chcete vytvořit referenci.",

			SIFCR_BTN_OK = "OK", SIFCR_BTN_CANCEL = "Zrušit",

			SIFCR_BTN_SELECT_ALL = "Označit vše", SIFCR_BTN_DESELECT_ALL = "Odznačit vše";













	// Texty do třídy MyInstanceCell v balíčku instanceDiagram. Jedná se pouze
	// otexty potřebné pro vypsání informací o refernční proměnné, nebo o tom, žese
	// v příslušné instanci nenachází atributy nebo metody.
	public static final String MIS_TXT_REFERENCE_VAR = "referenční proměnná", MIS_TXT_NO_VARIABLES = "Žádné proměnné",
			MIS_TXT_NO_METHODS = "Žádné metody";


















	// Texty do třídy PopupMenu v balíčku clearEditor, jedná se pouze o texty do té
	// jediné položky pro vymazání textu z editoru.
	public static final String CE_PM_ITEM_CLEAR_TEXT = "Vymazat",
			CE_PM_ITEM_CLEAR_TT = "Vymaže veškerý text z editoru.";













	// Texty do třídy ErrroInfo v balíčku checkConfigurationFiles, jedná se pouze o
	// texty do metody toString, která vypisuje příslušné informace o příslušné
	// chybě.
	public static final String EI_TXT_ALLOWED_VALUES = "Povolené hodnoty", EI_TXT_MISSING_KEY = "Chybějící klíč",
			EI_TXT_CELLS_DUPLICATE_ERROR = "Nalezeno duplicitní nastavení pro buňky, které v diagramu tříd reprezentují třídy a komentáře.",
			EI_TXT_CD_EDGES_ERROR = "Nalezeno duplicitní nastavení pro některé hrany, které v diagramu tříd reprezentují některé vztahy mezi třídami nebo spojení třídy s komentářem.",
			EI_TXT_ID_EDGES_ERROR = "Nalezeno duplicitní nastavení pro hrany, které v diagramu instancí reprezentují vztahy typu agregace a asociace.",
			EI_TXT_INVALID_VALUE = "Nevalidní hodnota";










	// Texty do třídy / dialogu ErrorInConfigFilesForm, jedná se o dialog, který
	// slouží pro zobrazení chyb po uložení změn v nějakém konfiguračním souboru,
	// který bude obsahovat chybné hodnoty nebo duplicity.
	public static final String EICFF_DIALOG_TITLE = "Přehled chyb", EICFF_TXT_FILE = "Soubor",
			EICFF_TXT_INFO_REST = "obsahoval chyby, které jsou uvedeny níže. Veškeré hodnoty v uvedeném souboru byly přepsány na výchozí nastavení. Aby se projevily změny, restartujte prosím aplikaci.",
			EICFF_JSP_ERROR_LIST_BORDER_TITLE = "Nalezené chyby",
			EICFF_JSP_DUPLICATES_LIST_BORDER_TITLE = "Nalezené duplicity", EICFF_CHCB_WRAP_TEXT = "Zalamovat texty.",
			EICFF_BTN_OK = "OK";







	// Texty pro hlášky do dialogových okne ve třídě cz.uhk.fim.fimj.app.RestartApi
    public static final String RA_TXT_DIRS_WERE_NOT_FOUND_TITLE = "Ukončit aplikaci",
            RA_TXT_DIRS_WERE_NOT_FOUND_TEXT_PART_ONE = "Nebyl nalezen žádný z adresářů configuration ve workspace, workspace nebo domovský adresář uživatele pro uložení spouštěcího skriptu.",
            RA_TXT_DIRS_WERE_NOT_FOUND_TEXT_PART_TWO = "Přejete si aplikaci ukončit?",

    RA_TXT_RESTART_IS_NOT_SUPPORTED_TITLE = "Restart není podporován", RA_TXT_RESTART_IS_NOT_SUPPORTED_TEXT =
            "Aplikace nepodporuje restart na používaném operačním systému. Přejete si aplikaci ukončit?",

    RA_TXT_CANNOT_CREATE_LAUNCH_SCRIPT_TITLE = "Ukončit aplikaci", RA_TXT_CANNOT_CREATE_LAUNCH_SCRIPT_TEXT = "Pokus o vytvoření spouštěcího skriptu selhal. Přejete si aplikaci ukončit?", RA_TXT_SCRIPT = "Skript";





    // Texty pro chybovou hlášku o tom, že se nepodařilo smazat spouštěcí skript Viz třída: cz.uhk.fim.fimj.app
    // .DeleteLaunchScriptThread
    public static final String DLST_TXT_DELETE_SCRIPT_FAILED_TEXT = "Vytvořený spouštěcí skript pro aplikaci nebylo možné smazat. Můžete tak prosím učinit.",
            DLST_TXT_DELETE_SCRIPT_FAILED_TITLE = "Nepodařilo se smazat spouštěcí skript",
            DLST_TXT_SCRIPT = "Skript";


    // Texty do třídy cz.uhk.fim.fimj.file.DesktopSupport:

    // Texty ohledně podpory třídy Desktop:
    public static final String DS_TXT_ERROR_INFO_TEXT = "Třída 'Desktop' není na používané platformě podporována.",
            DS_TXT_ERROR_INFO_TITLE = "Desktop není podporován",
            DS_TXT_OPEN_ACTION_IS_NOT_SUPPORTED_TITLE = "Akce není podporována",
    /*
     * Texty chybové hlášky při pokusu oo otevření souboru nebo adresáře v průzkumníku souborů v OS nebo ve výchozí
     * aplikaci pro otevření příslušného typu souboru:
     */
    DS_TXT_FILE_DO_NOT_EXIST_TEXT = "Soubor nebo adresář neexistuje.",
            DS_TXT_FILE_DO_NOT_EXIST_TITLE = "Nelze otevřit",
            DS_TXT_PATH = "Cesta",
            DS_TXT_PARENT_DIR_WAS_NOT_FOUND_TEXT = "Nepodařilo se nalézt adresář označeného souboru.",
            DS_TXT_PARENT_DIR_WAS_NOT_FOUND_TITLE = "Adresář nebyl nalezen",
    /*
     * Texty pro chybové hlášky, že akce pro otevření souboru nebo adresáře není na používané platformě podporováno
     * za účelem otevření souboru nebo adresáře v průzkumníku projektů nebo ve výchozí aplikaci pro otevření
     * příslušného typu souboru:
     */
    DS_TXT_OPEN_FILE_ACTION_IS_NOT_SUPPORTED_TEXT = "Akce pro otevření souboru není podporována.",
            DS_TXT_OPEN_FILE_ACTION_IS_NOT_SUPPORTED_TEXT2 = "Soubor není možné otevřít.",
            DS_TXT_OPEN_DIR_ACTION_IS_NOT_SUPPORTED_TEXT = "Akce pro otevření adresáře není podporována.",
            DS_TXT_OPEN_DIR_ACTION_IS_NOT_SUPPORTED_TEXT2 = "Adresář není možné otevřit.",
    /*
     * Texty pro chybové hlášky, že třída Desktop není na používané platformě podporována za účelem otevření souboru
     * nebo adresáře v průzkumníku projektů nebo ve výchozí aplikaci pro otevření příslušného typu souboru.
     */
    DS_TXT_FILE_CANNOT_BE_OPENED = "Soubor není možné otevřít.",
            DS_TXT_DIR_CANNOT_BE_OPENED = "Adresář není možné otevřít.",
    /*
     * Text pro chybovou hlášku o tom, že třída Desktop není podporována na používané platformě za účelem odeslání
     * emailu, resp. otevření emailového klienta:
     */
    DS_TXT_EMAIL_CANNOT_BE_SENT = "Email nelze odeslat.",
    /*
     * Text pro chybovou hlášku o tom, že akce pro otevření emailového klienta (/ zaslání emailu) není na používané
     * platformě podporována.
     */
    DS_TXT_EMAIL_ACTION_IS_NOT_SUPPORTED = "Akce pro odeslání emailu není podporována. Email nelze odeslat.",
    /*
     * Text pro chybovou hlášku o tom, že třída Desktop není na používané platformě podporována za účelem otevření
     * výchozího prohlížeče a v něm příslušnou webovou stránku.
     */
    DS_TXT_BROWSER_CANNOT_BE_OPENED = "Prohlížeč nelze otevřit.",
    /*
     * Texty pro chybovou hlášku o tom, že akce pro otevření výchozího prohlížeče a v něm příslušnou webovou stránku
     * není na používané platformě podporována.
     */
    DS_TXT_BROWSE_ACTION_IS_NOT_SUPPORTED = "Akce pro otevření výchozího prohlížeče není podporována. Stránku nelze " +
            "zobrazit.";
}