package cz.uhk.fim.fimj.file;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

/**
 * Třída slouží pro zkopírování textů do schránky v používaném OS.
 * <p>
 * To samé, jako označit text a stisknout kombinaci kláves Ctrl + C.
 * <p>
 * <i>Třída obsahuje pouze jednu metodu, která obstarává kopírování textu do schránky, ale tato třída byla založena pro
 * potenciální budoucí rozšíření, aby byly operace s "schránkou" vkládány do této třídy.</i>
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 12.08.2018 17:37
 */

public class ClipboardHelper {


    private ClipboardHelper() {
    }


    /**
     * Vložení / Zkopárování textu v proměnné textToCopy do schránky (jako po stisknutí kláves Ctrl + C).
     *
     * @param textToCopy
     *         - text, který se má vložit do schránky, aby jej uživatel mohl "někam" vložit.
     */
    public static void copyToClipboard(final String textToCopy) {
        final StringSelection selection = new StringSelection(textToCopy);

        final Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

        clipboard.setContents(selection, selection);
    }
}
