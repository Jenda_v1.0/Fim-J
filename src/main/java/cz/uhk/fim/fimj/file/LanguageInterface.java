package cz.uhk.fim.fimj.file;

import java.util.Properties;

/**
 * Tento Interfase obsahuje pouze jednu metodu metoda se budou používat na to, aby v každé třídě, která implementuje
 * toto rozhrani nastavlia veškerý text na jazyk zvolený uživatelem
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface LanguageInterface {

    /**
     * Metoda, nastavi veškerý text v příslušné třídy na jazyk zvolený uživatelem nejen jazyk, prostě všechny
     * výchozívlastnosti: font, barva, velikost písma, ...
     *
     * @param properties
     *         - reference na přečtený soubor s potřebnými vlastnosti pro nastavení do okna
     */
    void setLanguage(final Properties properties);
}