package cz.uhk.fim.fimj.file;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.beans.XMLEncoder;
import java.io.*;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import org.jgraph.JGraph;
import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.buttons_panel.ClassTypeEnum;
import cz.uhk.fim.fimj.buttons_panel.TestAndCreateEdge;
import cz.uhk.fim.fimj.check_configuration_files.ControlClass;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.menu.EditClass;
import cz.uhk.fim.fimj.menu.EditClassInterface;
import cz.uhk.fim.fimj.language.EditProperties;
import cz.uhk.fim.fimj.language.KeysOrder;
import cz.uhk.fim.fimj.language.WriteLanguage;
import cz.uhk.fim.fimj.language.WriteLanguageInterface;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.preferences.PreferencesApi;
import cz.uhk.fim.fimj.project_explorer.ReadWriteFile;
import cz.uhk.fim.fimj.project_explorer.ReadWriteFileInterface;

/**
 * Tato třída slouží převážně pro zápis "věci" do souborů
 * <p>
 * Převážně obsahuje metody pro zápis souborů typu Properties nějaké kopírování či vytváření adresářů v rámci potřeb
 * této aplikace, viz konkrétní metody
 * <p>
 * <p>
 * Zdroje k metodě insertComments: https://stackoverflow
 * .com/questions/565932/a-better-class-to-update-property-files/565996#565996
 * <p>
 * https://commons.apache.org/proper/commons-configuration/apidocs/org/apache/commons/configuration2
 * /PropertiesConfigurationLayout.html
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class WriteToFile implements WriteToFilesInterface {

    private static final WriteLanguageInterface writeLanguage = new WriteLanguage();


    /**
     * Třída pro editaci - smaže ze zdrojového kódu třídy příslušné atributy na základě zjištěné hrany (výše) např pokud
     * bude hrana reprezentovat asociace, následující třída smaže na sebe jejich reference
     */
    private static final EditClassInterface editClass = new EditClass();


    /**
     * Metoda, která vytvoři všechny potřebné konfigurační soubory ve zvolené složce na cestě path.
     * <p>
     * Konfigurační soubory například pro diagram tříd a instancí, editor zdrojových kódů, příkazů, výstupů, soubory s
     * texty v příslušných jazycích pro aplikaci apod.
     *
     * @param path
     *         - cesta, kde se má vytvořit adresář .configuration
     */
    public static void createConfigureFilesInPath(final String path) {
        if (path == null)
            return;

        final File fileConfiguration = new File(path + File.separator + Constants.CONFIGURATION_NAME);

        /*
         * Test, zda adresář configuration existuje, pokud ano, tak se pouze vytvoří soubory, pokud ne, tak se
         * adresář vytvoří a pak se vloží soubory:
         */
        if (!fileConfiguration.exists() || !fileConfiguration.isDirectory())
            createDirectories(fileConfiguration);

        // V případě, že adresář existuje, tak nemusí být skrytý:
        if (!fileConfiguration.isHidden())
            hideFile(fileConfiguration);

        /*
         * Pro jistotu se znovu otestuje existence adesáře, i když to není nezbytné, ale kdyby se je nepodařilo
         * vytvořit, tak by mohla nastat výjimka:
         */
        if (ReadFile.existsDirectory(fileConfiguration.getAbsolutePath())) {
            ConfigurationFiles.writeDefaultPropertiesAllNew(
                    fileConfiguration.getPath() + File.separator + Constants.DEFAULT_PROPERTIES_NAME);

            ConfigurationFiles.writeClassDiagramProperties(
                    fileConfiguration.getPath() + File.separator + Constants.CLASS_DIAGRAM_NAME);

            ConfigurationFiles.writeInstanceDiagramProperties(
                    fileConfiguration.getPath() + File.separator + Constants.INSTANCE_DIAGRAM_NAME);

            ConfigurationFiles.writeCommandEditorProperties(
                    fileConfiguration.getPath() + File.separator + Constants.COMMAND_EDITOR_NAME);

            ConfigurationFiles.writeCodeEditorProperties(
                    fileConfiguration.getPath() + File.separator + Constants.CODE_EDITOR_NAME);

            ConfigurationFiles.writeOutputEditorProperties(
                    fileConfiguration.getPath() + File.separator + Constants.OUTPUT_EDITOR_NAME);

            ConfigurationFiles.writeCodeEditorPropertiesForInternalFrames(
                    fileConfiguration.getPath() + File.separator + Constants.CODE_EDITOR_INTERNAL_FRAMES_NAME);

            ConfigurationFiles.writeOutputFrameProperties(
                    fileConfiguration.getPath() + File.separator + Constants.OUTPUT_FRAME_NAME);

            // Doplnit složku language a příslušné jazyky
            writeLanguages(fileConfiguration);
        }
    }


    /**
     * Vytvoření adresáře / adresářů na cestě pathToConfigurationDir.
     *
     * @param pathToConfigurationDir
     *         - cesta k adreáři / adresářům, které se mají vytvořit
     */
    private static void createDirectories(final File pathToConfigurationDir) {
        try {
            final Path path = Paths.get(pathToConfigurationDir.getAbsolutePath());

            Files.createDirectories(path);

        } catch (final IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při vytváření adresáře / adresářů na cestě: " + pathToConfigurationDir);

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
    }


    /**
     * Skrytí souboru / adresáře (nastavení atributu příslušného souboru file jako skrytý - pro OS Windows).
     *
     * @param file
     *         - cesta k adresáři / souboru, který se má nastavit jako skrytý (na OS Windows).
     */
    private static void hideFile(final File file) {
        try {

            final Path path = Paths.get(file.getAbsolutePath());

            Files.setAttribute(path, "dos:hidden", Boolean.TRUE, LinkOption.NOFOLLOW_LINKS);

        } catch (final IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při pokusu o nastavení adresáře na následující cestě jako " +
                        "skrytého. Cesta: " + file);

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
    }


    /**
     * Metoda, která vytvoří, resp. zapíše jazyky pro aplikaci na zadané místě, na zadané cestě v parametru metody -
     * fileConfiguration se vytvoří adresář language, do kterého se zapíšou příslušné jazyky pro tuto aplikaci
     *
     * @param fileConfiguration
     *         - cesta, kam se má vytvořit adresář language a zapsat jazyky pro tuto aplikaci
     */
    public static void writeLanguages(final File fileConfiguration) {
        // Otestuji, zda se daný adresář language vytvořil - i když bych nemusel, pokud by došlo k chybě
        // zachytila by se vyjímka a dále by se nepokračovalo, ale je to lepší i kvuli zadávání cesty:
        final File fileLanguageDir = new File(fileConfiguration + File.separator + Constants.LANGUAGE_NAME);

        if (!fileLanguageDir.exists() || !fileLanguageDir.isDirectory())
            createDirectories(fileLanguageDir);

        // Tato podmínka pouze pro případ, že by výše selhalo vytvoření adresářů:
        if (!fileLanguageDir.exists() || !fileLanguageDir.isDirectory())
            return;


        // Načtu si default.properties: z workspacu (/ popř. z aplikace)
        final Properties defaultProp = ReadFile.getDefaultPropAnyway();


        final KeysOrder keysOrder;
        final boolean addComments;
        final int freeLinesBeforeComment;
        final Locale locale;

        if (defaultProp != null) {
            // Hodnota pro řazení textů v souborech .properties
            keysOrder = KeysOrder.getKeysOrderByFileValue(defaultProp.getProperty("Default keys order",
                    Constants.DEFAULT_KEYS_ORDER.getValueForFile()));

            // Hodnota pro to, zda se mají vkládat komentář do souboru properties s texty pro aplikaci:
            addComments = Boolean.parseBoolean(defaultProp.getProperty("Add comments",
                    String.valueOf(Constants.DEFAULT_VALUE_ADD_COMMENTS)));

            // Počet volných řádků před komentářem:
            final Integer tempInt = castInt(defaultProp.getProperty("Count of free lines before comment",
                    String.valueOf(Constants.DEFAULT_COUNT_OF_FREE_LINES_BEFORE_COMMENT)));
            if (tempInt == null)
                freeLinesBeforeComment = Constants.DEFAULT_COUNT_OF_FREE_LINES_BEFORE_COMMENT;
            else
                freeLinesBeforeComment = tempInt;

            // "globální umístění" pro datumy v souborech .properties s texty pro aplikaci:
            locale = Locale.forLanguageTag(defaultProp.getProperty("Properties locale",
                    Constants.DEFAULT_LOCALE_FOR_PROPERTIES.toLanguageTag()));
        }

        // Část else by nikdy neměla nastat, vždy by se měl načíst alespoň výchozí
        // soubor z aplikace, ale kdyby náhodou:
        else {
            // Hodnota pro řazení textů v souborech .properties
            keysOrder = Constants.DEFAULT_KEYS_ORDER;

            // Hodnota pro to, zda se mají vkládat komentář do souboru properties s texty pro aplikaci:
            addComments = Constants.DEFAULT_VALUE_ADD_COMMENTS;

            // Počet volných řádků před komentářem:
            freeLinesBeforeComment = Constants.DEFAULT_COUNT_OF_FREE_LINES_BEFORE_COMMENT;

            // "globální umístění" pro datumy v souborech .properties s texty pro aplikaci:
            locale = Constants.DEFAULT_LOCALE_FOR_PROPERTIES;
        }


        /*
         * Abych nemusel předávat větší množství parametrů do metoda, tak ty metody pro
         * zápis textů do souborů přetížím tak, že se budou lišit s parametry pro
         * metody, které zadávají komentáře a metody, které nezadávají komentáře.
         *
         * Navíc jsem si díky tomu přetížení ušetřil práci v kopírování, protože s
         * každým jazykem navíc, bych musel kopírovat testování, zda se mají
         * přidatkomentáře apod. Takto stačí pouze vložit další metody pro jazyk s
         * komentářem a bez něj. pak se jen získají texty a je to, nemusí se již řešit
         * ukkládání, komentáře apod. viz jednolivé metody.
         *
         * Dále zde je třeba otestovat, zda semají přidat komentáře nebo na, ale jen v
         * případě, že je označen způsob řazení sestupně nebo vzestupně dle pořadí
         * přidání hodnot, jinak nelze přidávat komentáře.
         */
        if (addComments && (keysOrder == KeysOrder.ASCENDING_BY_ADDING
                || keysOrder == KeysOrder.DESCENDING_BY_ADDING)) {
            writeLanguage.writeLanguageCZ(fileLanguageDir.getPath() + File.separator + "CZ.properties", keysOrder,
                    freeLinesBeforeComment, locale);
            writeLanguage.writeLanguageEN(fileLanguageDir.getPath() + File.separator + "EN.properties", keysOrder,
                    freeLinesBeforeComment, locale);
        }

        else {
            writeLanguage.writeLanguageCZ(fileLanguageDir.getPath() + File.separator + "CZ.properties", keysOrder,
                    locale);
            writeLanguage.writeLanguageEN(fileLanguageDir.getPath() + File.separator + "EN.properties", keysOrder,
                    locale);
        }
    }


	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro přetyopvání textu intInText na hodnotu typu Integer
	 * a pokud nastane výjimka, pak se vrátí hodnota null, tzn. že se to číslo
	 * nepodařilo přetypovat na hodnotu Integer, takže to není číslo typu Integer.
	 * 
	 * @param intInText
	 *            - nějaký načtený text, o kterém se má zjistit, zda se jedná o
	 *            číslo typu int.
	 * 
	 * @return číslo v intInText, pokud se jej opvede přetypovt na Integer, jinak
	 *         null
	 */
	private static Integer castInt(final String intInText) {
		try {
			return Integer.parseInt(intInText);
        } catch (NumberFormatException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při pokusu o přetypování hodnoty: '" + intInText
                        + "' na datový typ Integer.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }

		return null;
	}
	
	
	
	
	
	
	
	
	
	
	

	@Override
	public void changePathToWorkspace(final String path) {
		// Nastavím v preferencích cestu k vybranému adresáři coby Workspace:
		PreferencesApi.setPreferencesValue(PreferencesApi.PATH_TO_WORKSPACE, path);
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zapíše příslušný objekt properties do souboru na uvedené cestě
	 * 
	 * @param properties
	 *            - objekt typu .properties, který se má zapsat
	 * 
	 * @param path
	 *            - cesta kam se má soubor zapsat
	 */
	public static void writeProperties(final Properties properties, final String path) {
		try (final Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path),
				StandardCharsets.UTF_8))) {
			/*
			 * Note:
			 * Komentář zde nezapisuji, když už, tak až v metodě insertCreationComments,
			 * která vkládá komentář na začátek souboru. Protože, když jej zapíšu zde, tak
			 * bych jej stejně musel přepsat, protože by se přepsal (tento komentář na
			 * začátku souboru), když bych zapisovat komentáře do daného souboru - k
			 * jednotlivým klíčům pomocí metody insertComments ve tříeě ConfigurationFiles.
			 */
			properties.store(out, null);

			out.flush();

		} catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při zapisování objektu Properties do souboru třída WriteToFile, metoda " +
                                "writeProperties, na umisteni: "
                                + path);

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
	}
	
	
	
	
	
	
	




	
	@Override
	public void setAskForWorkspaceValue(final boolean askForWorkspace) {
		/*
		 * Je třeba načíst soubor Default.properties z adresáře označného jako
		 * workspace, ale tak, aby se v něm zachovali veškeré komentáře (protože je
		 * třeba ten upravený soubor default.properties zase zapsat zpět na původní
		 * umístění, tak je třeba jej načíst a zapsat pomocí připravené třídy
		 * EditProperties.
		 */
		final EditProperties defaultProp = App.READ_FILE
				.getPropertiesInWorkspacePersistComments(Constants.DEFAULT_PROPERTIES_NAME);

		if (defaultProp != null) {
			defaultProp.setProperty("Display WorkspaceChooser", String.valueOf(askForWorkspace));

			defaultProp.save();
		}

		/*
		 * else - Zde by bylo možné znovu ten soubor vytvořit (pokud existuje
		 * workspace), případně vytvořit celý adresář configuration, ale to nechám v
		 * tomto případě být, měl by o tom uživatel vědět a má možnost si to vytvořit v
		 * menu, v tomto případě to není nijak extra potřeba, mohou se využít výchozí
		 * hodnoty apod.
		 */
	}
	
	
	
	
	
	
	








	
	
	@Override
	public void saveImageOfGraph(final String path, final JGraph graph) {
		try (final OutputStream out = new FileOutputStream(path)) {
			final Color bg = graph.getBackground();
			
			// Parametr inset (v metode getImage) = jak "veliké budou rohy v obraze" jsou nekde 
			// bunky a dle toho cisla tak daleko od nich bude pozadi
			final BufferedImage img = graph.getImage(bg, 20);
			final String typeOfFile = path.substring(path.length() - 3);

			ImageIO.write(img, typeOfFile, out);

			out.flush();

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při ukládání obrázku diagramu do souboru, cesta k souboru: "
                        + path + "\nTřída: WriteToFile.java, metoda: saveImage().");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
	}
	
	
	
	
	
	
	
	
	
	
	@Override
	public boolean createNewProjectFolder(final String path) {
		final String projectFolderName = path.substring(path.lastIndexOf(File.separator) + 1);
		
		try {
			// Vytvoření příslušného adresáře:
			Files.createDirectories(Paths.get(path));
			
			
			// Vytvoření adresáře src pro balíčky a třídy:
			Files.createDirectories(Paths.get(path, File.separator + "src"));
			
			
			// Vytvoření adresáře bin, do kterého se budou "kopírovat" adresáře a přeloženeé třídy
			// z diagramu tříd, tj. soubory .class vzniklé po kompilaci příslušné třídy
			Files.createDirectories(Paths.get(path, File.separator + "bin"));
			
			
			
			// Vložení pooznámkového bloku s výchozí poznámkou:
			final File fileInfoAboutProject = new File(path + File.separator + Constants.READ_ME_FILE_NAME_WITH_EXTENSION);

			try (final Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream
					(fileInfoAboutProject), StandardCharsets.UTF_8))) {

                // Proměnné pro text ve správném jazyce:
                final String txtText;
                final String txtText2;
                final String txtAuthor;
                final String txtProjectName;
                final String txtPurposeOfProject;
                final String txtVersion;
                final String txtDate;
                final String txtHowToRunTheProject;
                final String txtStartingClasses;
                final String txtUserInstructions;
                final String txtUsefulAdditionalInformation;

				final Properties languageProperties = App.READ_FILE.getSelectLanguage();


				// Získám si aktuální datum, abych ho mohl dát do textového souboru se základními informacemi o projektu

				// Datum bude aktuální - datum vytvoření daného projěktu:
				final Calendar calendar = GregorianCalendar.getInstance();
				final DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");


				if (languageProperties != null) {
					txtText = languageProperties.getProperty("TextLineInfo", Constants.TEXT_AT_LINE_1);
					txtText2 = languageProperties.getProperty("TextLineInfo_2", Constants.TEXT_AT_LINE_2);
					txtAuthor = languageProperties.getProperty("TextLineAuthor", Constants.TEXT_AT_LINE_AUTHOR);
					txtProjectName = languageProperties.getProperty("TextLineProjectName", Constants.TEXT_AT_LINE_PROJECT_NAME);
                    txtPurposeOfProject = languageProperties.getProperty("TextLineProjectPurpose", Constants.TEXT_AT_LINE_PROJECT_PURPOSE);
                    txtHowToRunTheProject = languageProperties.getProperty("TextLineHowToRunTheProject", Constants.TEXT_AT_LINE_HOW_TO_RUN_THE_PROJECT);
                    txtStartingClasses = languageProperties.getProperty("TextLineStartingClasses", Constants.TEXT_AT_LINE_STARTING_CLASSES);
                    txtUserInstructions = languageProperties.getProperty("TextLineUserInstructions", Constants.TEXT_AT_LINE_USER_INSTRUCTIONS);
                    txtUsefulAdditionalInformation = languageProperties.getProperty("TextLineUsefulAdditionalInformation", Constants.TEXT_AT_LINE_USEFUL_ADDITIONAL_INFORMATION);
                    txtVersion = languageProperties.getProperty("TextLineVersion", Constants.TEXT_AT_LINE_VERSION);
                    txtDate = languageProperties.getProperty("TextLineDate", Constants.TEXT_AT_LINE_DATE);
				}

				else {
					txtText = Constants.TEXT_AT_LINE_1;
                    txtText2 = Constants.TEXT_AT_LINE_2;
					txtAuthor = Constants.TEXT_AT_LINE_AUTHOR;
					txtProjectName = Constants.TEXT_AT_LINE_PROJECT_NAME;
					txtPurposeOfProject = Constants.TEXT_AT_LINE_PROJECT_PURPOSE;
                    txtHowToRunTheProject = Constants.TEXT_AT_LINE_HOW_TO_RUN_THE_PROJECT;
                    txtStartingClasses = Constants.TEXT_AT_LINE_STARTING_CLASSES;
                    txtUserInstructions = Constants.TEXT_AT_LINE_USER_INSTRUCTIONS;
                    txtUsefulAdditionalInformation = Constants.TEXT_AT_LINE_USEFUL_ADDITIONAL_INFORMATION;
					txtVersion = Constants.TEXT_AT_LINE_VERSION;
					txtDate = Constants.TEXT_AT_LINE_DATE;
				}

				writer.write("---------------------------------------------------------------------\r\n");
				writer.write(txtText + "\r\n");
				writer.write("\r\n");
				writer.write(txtText2 + "\r\n");
                writer.write("---------------------------------------------------------------------\r\n");
				writer.write("\r\n");
				writer.write("\r\n");


				addLineUsingEquals(writer);
				writer.write(txtProjectName + ": " + projectFolderName + "\r\n");
				addLineUsingEquals(writer);
				writer.write("\r\n");
				writer.write(txtAuthor + ": " + UserNameSupport.getUsedUserName() + "\r\n");
				addLineUsingEquals(writer);
				writer.write(txtVersion + ": 1.0" + "\r\n");
				writer.write(txtDate + ": " + dateFormat.format(calendar.getTime()) + "\r\n");
				addLineUsingEquals(writer);
				addThreeFreeLines(writer);
				writer.write(txtPurposeOfProject + ": " + "\r\n");
				addLineUsingEquals(writer);
				addThreeFreeLines(writer);
				writer.write(txtHowToRunTheProject + ": " + "\r\n");
				addLineUsingEquals(writer);
				addThreeFreeLines(writer);
				writer.write(txtStartingClasses + ": " + "\r\n");
				addLineUsingEquals(writer);
				addThreeFreeLines(writer);
				writer.write(txtUserInstructions + ": " + "\r\n");
				addLineUsingEquals(writer);
				addThreeFreeLines(writer);
				writer.write(txtUsefulAdditionalInformation + ": " + "\r\n");
                addLineUsingEquals(writer);

				writer.flush();
			}
			
			// Zde proběhlo vše v pořádku, tak vrátím true:
			return true;
			
		} catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaco o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při vytváření adresáře nového projektu, cesta k novému projektu: " + path
                                + "\nTřída: WriteToFile.java, metoda: createNewProjectFolder().");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			
			// Sem se dostanu, pokud nastane nějaká chyba, tak vrátím false, - došlo k chybě
			return false;
		}
	}








	/**
	 * Metoda pro zapsání řádku do / pužitím writeru. Řádek bude obsahovat pouze několik rovná se za sebou.
	 *
	 * @param writer
	 *         - objekt pro zapisování streamů.
	 *
	 * @throws IOException
	 *         - výjimka, která může nastat při zapisování řádku.
	 */
	private static void addLineUsingEquals(final Writer writer) throws IOException {
		writer.write("================================================\r\n");
	}


	/**
	 * Metoda pro zápis 3 volných řádků.
	 *
	 * @param writer
	 *         - objekt pro zapisování streamů.
	 *
	 * @throws IOException
	 *         - výjimka, která může nastat při zapisování řádků.
	 */
	private static void addThreeFreeLines(final Writer writer) throws IOException {
		writer.write("\r\n");
		writer.write("\r\n");
		writer.write("\r\n");
	}
	
	
	
	
	
	
	
	
	@Override
	public boolean writeClassToProjectSrc(final String name, final ClassTypeEnum classType,
			final boolean generateMainMethod) {
		// Zjistím si, zda existuje adresář src v aktuálně otevřenémp rojektu:
		final String pathToSrcDirectory = existSrcDirectory();

		final String pathToBinDirectory = existBinDirectory();

		if (pathToSrcDirectory != null && pathToBinDirectory != null) {
			// Zde mohu otestovat, zda se mají vytvořit balíčky a třída:
			
			// Nejprve otestuji, zda se to vůbec má rozdělit na balíčky:
			
			final String pathToClassDirectory = createDirectoriesToClass(pathToSrcDirectory, pathToBinDirectory, name);

			if (pathToClassDirectory != null) {
				// Zde jsou vytvořeny balíčky a mám k dispozici cestu k adresáři, kde mám
				// vytvořit příslušnou třídu

				// Ta, vytvořím třídu s daným názvem na dané cestě:

				// Název třídy, která se má vytvořit:
				// +1 - bez nazev tridy bez tecky
				final String className = name.substring(name.lastIndexOf('.') + 1);

				// Z názvu si zjistím balíček, ve kterém má být třída umístěna:
				final String[] partsOfPackage = name.split("\\.");

                final StringBuilder packageTextBuilder = new StringBuilder();

                for (int i = 0; i < partsOfPackage.length - 1; i++)
                    packageTextBuilder.append(partsOfPackage[i]).append(".");

                // Odeberu poslední tečku v cestě balíčku a na konec přidám středník:
                packageTextBuilder.setLength(packageTextBuilder.length() - 1);
                packageTextBuilder.append(";");

                // Vytvořím třídu s názvem na dané cestě:
                if (classType.equals(ClassTypeEnum.CLASS))
                    return createClass(className, pathToClassDirectory, packageTextBuilder.toString(),
                            generateMainMethod);

                else if (classType.equals(ClassTypeEnum.ABSTRACT_CLASS))
                    return createAbstractClass(className, pathToClassDirectory, packageTextBuilder.toString(),
                            generateMainMethod);

                else if (classType.equals(ClassTypeEnum.INTERFACE))
                    return createInterface(className, pathToClassDirectory, packageTextBuilder.toString());

                else if (classType.equals(ClassTypeEnum.APPLET))
                    return createApplet(className, pathToClassDirectory, packageTextBuilder.toString(),
                            generateMainMethod);

                else if (classType.equals(ClassTypeEnum.ENUM))
                    return createEnum(className, pathToClassDirectory, packageTextBuilder.toString());

                else if (classType.equals(ClassTypeEnum.UNIT_TEST))
                    return createJUnitTest(className, pathToClassDirectory, packageTextBuilder.toString(), generateMainMethod);
            }
            return false;
        }
        return false;
    }
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoři třídu pro testování - JUnit
	 * 
	 * @param className
	 *            - název třídy, která se má vytvořit
	 * 
	 * @param path
	 *            - cesta, kde se má daná třída vytvořit
	 * 
	 * @param packageText
	 *            - text pro balíček - do třídy
	 * 
	 * @param generateMainMethod
	 *            - logická hodnota o tom, zda se má vygenerovat hlavní - spuštěcí
	 *            metoda main nebo ne
	 * 
	 * @return true, pokud se třída úspěšně vytvoří, jinak false
	 */
    private static boolean createJUnitTest(final String className, final String path, final String packageText,
                                           final boolean generateMainMethod) {
        final Properties languageProperties = App.READ_FILE.getSelectLanguage();

        // Proměnné do třídy:
        final String commentText;
        final String textConstructor;
        final String errorMessage1;
        final String errorMessage2;
        final String errorMessage3;
        final String errorMessageTitle;
		
		if (languageProperties != null) {
			commentText = languageProperties.getProperty("WtfCommentForClass", Constants.WTF_TEXT_COMMENT_CLASS);
			textConstructor = languageProperties.getProperty("WtfCommentConstructor", Constants.WTF_TEXT_CONSTRUCTOR);

			errorMessage1 = languageProperties.getProperty("EtfErrorMessage1", Constants.WTF_ERROR_MESSAGE1);
			errorMessage2 = languageProperties.getProperty("EtfErrorMessage2", Constants.WTF_ERROR_MESSAGE2);
			errorMessage3 = languageProperties.getProperty("EtfErrorMessage3", Constants.WTF_ERROR_MESSAGE3);
			errorMessageTitle = languageProperties.getProperty("EtfErrorMessageTitle", Constants.WTF_ERROR_MESSAGE_TITLE);
		}
		
		else {
			commentText = Constants.WTF_TEXT_COMMENT_CLASS;
			textConstructor = Constants.WTF_TEXT_CONSTRUCTOR;

			errorMessage1 = Constants.WTF_ERROR_MESSAGE1;
			errorMessage2 = Constants.WTF_ERROR_MESSAGE2;
			errorMessage3 = Constants.WTF_ERROR_MESSAGE3;
			errorMessageTitle = Constants.WTF_ERROR_MESSAGE_TITLE;
		}
		
		
		final File fileClass = new File(path + File.separator + className + ".java");

		if (!fileClass.exists()) {
			try (final Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileClass),
					StandardCharsets.UTF_8))) {

				if (packageText != null) {
					writer.write("package " + packageText + "\n");

					writer.write("\n");
				}



				writer.write("import static org.junit.Assert.*;\n");
				writer.write("\n");
				writer.write("import org.junit.Test;\n");
				writer.write("\n");
				
				// Poznámky:
                writer.write(getAuthorComment(commentText));

				writer.write("public class " + className + " {\n");
				writer.write("\n");
				
				// Konstruktor s dokumentařním komentářem:
				writeConstructorComment(writer, textConstructor);

				writer.write("\tpublic " + className + "() {\n");
				writer.write("\n");
				writer.write("\t}\n");

				writer.write("\n");

				writer.write("\t@Test\n");
				writer.write("\tpublic void test() {\n");
				writer.write("\t\tfail();");
				writer.write("\n");
				writer.write("\t}\n");
				
				if (generateMainMethod)
					addMainMethod(writer);

				writer.write("}");

				writer.flush();
				
				// Zde byla třída v pořádku vytvořena:
				return true;

            } catch (IOException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informacei o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při vytváření třídy s názvem: " + className + ", na cestě: " + path
                                    + ". Chyba nastala ve třídě: WriteToFile.java, v metodě: createClass().");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
			}
		}
		else
			JOptionPane.showMessageDialog(null,
					errorMessage1 + fileClass.getName() + errorMessage2 + fileClass.getPath() + errorMessage3,
					errorMessageTitle, JOptionPane.ERROR_MESSAGE);
		
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří "výčtový typ"
	 * 
	 * @param className
	 *            - název Výčtu, která se má vytvořit
	 * 
	 * @param path
	 *            - cesta, kde se má daná třída vytvořit
	 * 
	 * @param packageText
	 *            - text pro balíček - do třídy
	 * 
	 * @return true, pokud se třída úspěšně vytvoří, jinak false
	 */
    private static boolean createEnum(final String className, final String path, final String packageText) {
        final Properties languageProperties = App.READ_FILE.getSelectLanguage();
        // Proměnné do třídy:
        final String commentText;
        final String errorMessage1;
        final String errorMessage2;
        final String errorMessage3;
        final String errorMessageTitle;
		
		if (languageProperties != null) {
			commentText = languageProperties.getProperty("WtfCommentForClass", Constants.WTF_TEXT_COMMENT_CLASS);
			
			errorMessage1 = languageProperties.getProperty("EtfErrorMessage1", Constants.WTF_ERROR_MESSAGE1);
			errorMessage2 = languageProperties.getProperty("EtfErrorMessage2", Constants.WTF_ERROR_MESSAGE2);
			errorMessage3 = languageProperties.getProperty("EtfErrorMessage3", Constants.WTF_ERROR_MESSAGE3);
			errorMessageTitle = languageProperties.getProperty("EtfErrorMessageTitle", Constants.WTF_ERROR_MESSAGE_TITLE);
		}
		
		
		else {
			commentText = Constants.WTF_TEXT_COMMENT_CLASS;
			
			errorMessage1 = Constants.WTF_ERROR_MESSAGE1;
			errorMessage2 = Constants.WTF_ERROR_MESSAGE2;
			errorMessage3 = Constants.WTF_ERROR_MESSAGE3;
			errorMessageTitle = Constants.WTF_ERROR_MESSAGE_TITLE;
		}
		
		
		
		final File fileClass = new File(path + File.separator + className + ".java");

		if (!fileClass.exists()) {
			try (final Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileClass),
					StandardCharsets.UTF_8))) {

				if (packageText != null) {
					writer.write("package " + packageText + "\n");

					writer.write("\n");
				}

				
				// Poznámky:
                writer.write(getAuthorComment(commentText));

                writer.write("public enum " + className + " {\n");
                writer.write("\n");

                writer.write("}");

                writer.flush();
				
				// Zde byla třída v pořádku vytvořena:
				return true;

            } catch (IOException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při vytváření třídy s názvem: " + className + ", na cestě: " + path
                                    + ". Chyba nastala ve třídě: WriteToFile.java, v metodě: createClass().");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
            }
		}
		else
			JOptionPane.showMessageDialog(null,
					errorMessage1 + fileClass.getName() + errorMessage2 + fileClass.getPath() + errorMessage3,
					errorMessageTitle, JOptionPane.ERROR_MESSAGE);
		
		return false;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří na zadné cestě třídu - Applet, která bud dědit z
	 * JApplet
	 * 
	 * @param className
	 *            - název třídy
	 * 
	 * @param path
	 *            - cesta, kde se má daná třída vytvořit
	 * 
	 * @param packageText
	 *            - text pro balíček - do třídy
	 * 
	 * @param generateMainMethod
	 *            - logická hodnota, true, pokud mám vygenerovat i hlavní metodu
	 *            main jinak false
	 * 
	 * @return true, pokud se třída úspěšně vytvoří, jinak false
	 */
    private static boolean createApplet(final String className, final String path, final String packageText,
                                        final boolean generateMainMethod) {
        final Properties languageProperties = App.READ_FILE.getSelectLanguage();
        // Proměnné do třídy:
        final String commentText;
        final String textConstructor;
        final String errorMessage1;
        final String errorMessage2;
        final String errorMessage3;
        final String errorMessageTitle;
		
		if (languageProperties != null) {
			commentText = languageProperties.getProperty("WtfCommentForClass", Constants.WTF_TEXT_COMMENT_CLASS);
			textConstructor = languageProperties.getProperty("WtfCommentConstructor", Constants.WTF_TEXT_CONSTRUCTOR);
			
			errorMessage1 = languageProperties.getProperty("EtfErrorMessage1", Constants.WTF_ERROR_MESSAGE1);
			errorMessage2 = languageProperties.getProperty("EtfErrorMessage2", Constants.WTF_ERROR_MESSAGE2);
			errorMessage3 = languageProperties.getProperty("EtfErrorMessage3", Constants.WTF_ERROR_MESSAGE3);
			errorMessageTitle = languageProperties.getProperty("EtfErrorMessageTitle", Constants.WTF_ERROR_MESSAGE_TITLE);
		}
		
		
		else {
			commentText = Constants.WTF_TEXT_COMMENT_CLASS;
			textConstructor = Constants.WTF_TEXT_CONSTRUCTOR;
			
			errorMessage1 = Constants.WTF_ERROR_MESSAGE1;
			errorMessage2 = Constants.WTF_ERROR_MESSAGE2;
			errorMessage3 = Constants.WTF_ERROR_MESSAGE3;
			errorMessageTitle = Constants.WTF_ERROR_MESSAGE_TITLE;
		}
		
		
		
		final File fileClass = new File(path + File.separator + className + ".java");
		
		
		/*
		 * Vygenerování serialVersionUID pro deserializaci
		 * 
		 * Toto jsem zavrhnul, protože bych musel třídu vytvořit, zkompilovat, načíst
		 * soubor .class, vygenerovat ID a vložit jej do kódu třídy . Ne, že by to
		 * nešlo, ale je to trochu zdlouhavé a přece jen trochu zbytečné.
		 */
//		final ObjectStreamClass osc = ObjectStreamClass.lookup(fileClass.getClass());
//		final long serialId = osc.getSerialVersionUID();
		
		final String serialId = "\t/**\n"
				+ "\t*\n"
				+ "\t*/\n"
				+ "\tprivate static final long serialVersionUID = 1L;";


		if (!fileClass.exists()) {
			try (final Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileClass),
					StandardCharsets.UTF_8))) {

				if (packageText != null) {
					writer.write("package " + packageText + "\n");

					writer.write("\n");
				}


                writer.write("import java.applet.Applet;\n");

                writer.write("\n");
				
				// Poznámky:
                writer.write(getAuthorComment(commentText));

                writer.write("public class " + className + " extends Applet {\n");
                writer.write("\n");
				
				// Výchozí číslo:
                writer.write(serialId + "\n");

                writer.write("\n");
				
				// Konstruktor s dokumentařním komentářem:
				writeConstructorComment(writer, textConstructor);

                writer.write("\tpublic " + className + "() {\n");
                writer.write("\n");
                writer.write("\t}\n");
				
				if (generateMainMethod)
					addMainMethod(writer);

                writer.write("}");

                writer.flush();
				
				// Zde byla třída v pořádku vytvořena:
				return true;
				
			} catch (IOException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při vytváření třídy s názvem: " + className + ", na cestě: " + path
                                    + "Chyba nastala ve třídě: WriteToFile.java, v metodě: createClass().");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
            }
		}
		else
			JOptionPane.showMessageDialog(null,
					errorMessage1 + fileClass.getName() + errorMessage2 + fileClass.getPath() + errorMessage3,
					errorMessageTitle, JOptionPane.ERROR_MESSAGE);
		
		return false;
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří na zadané cestě Interface - zvolený uživatelem
	 * 
	 * @param className
	 *            - název třídy, resp. název Interfacu
	 * 
	 * @param path
	 *            - cesta, kde se má daná třída vytvořit
	 * 
	 * @param packageText
	 *            - text pro balíček - do třídy
	 * 
	 * @return true, pokud se třída úspěšně vytvoří, jinak false
	 */
    private static boolean createInterface(final String className, final String path, final String packageText) {
        final Properties languageProperties = App.READ_FILE.getSelectLanguage();

        final String commentText;
        final String errorMessage1;
        final String errorMessage2;
        final String errorMessage3;
        final String errorMessageTitle;
		
		if (languageProperties != null) {
			commentText = languageProperties.getProperty("WtfCommentForClass", Constants.WTF_TEXT_COMMENT_CLASS);
			
			errorMessage1 = languageProperties.getProperty("EtfErrorMessage1", Constants.WTF_ERROR_MESSAGE1);
			errorMessage2 = languageProperties.getProperty("EtfErrorMessage2", Constants.WTF_ERROR_MESSAGE2);
			errorMessage3 = languageProperties.getProperty("EtfErrorMessage3", Constants.WTF_ERROR_MESSAGE3);
			errorMessageTitle = languageProperties.getProperty("EtfErrorMessageTitle", Constants.WTF_ERROR_MESSAGE_TITLE);
		}
		
		
		else {
			commentText = Constants.WTF_TEXT_COMMENT_CLASS;
			
			errorMessage1 = Constants.WTF_ERROR_MESSAGE1;
			errorMessage2 = Constants.WTF_ERROR_MESSAGE2;
			errorMessage3 = Constants.WTF_ERROR_MESSAGE3;
			errorMessageTitle = Constants.WTF_ERROR_MESSAGE_TITLE;
		}
		
		
		
		final File fileClass = new File(path + File.separator + className + ".java");

		if (!fileClass.exists()) {
			try (final Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileClass),
					StandardCharsets.UTF_8))) {

				if (packageText != null) {
					writer.write("package " + packageText + "\n");

					writer.write("\n");
				}

				
				// Poznámky:
                writer.write(getAuthorComment(commentText));

                writer.write("public interface " + className + " {\n");
                writer.write("\n");

                writer.write("}");

                writer.flush();
				
				// Zde byla třída v pořádku vytvořena:
				return true;

            } catch (IOException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při vytváření třídy s názvem: " + className + ", na cestě: " + path
                                    + ". Chyba nastala ve třídě: WriteToFile.java, v metodě: createInterface()!");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
            }
		}
		else
			JOptionPane.showMessageDialog(null,
					errorMessage1 + fileClass.getName() + errorMessage2 + fileClass.getPath() + errorMessage3,
					errorMessageTitle, JOptionPane.ERROR_MESSAGE);
		
		return false;
	}
	
	

	
	
	
	
	
	/**
	 * Metoda, která vytvoří na zadané cestě abstraktní třídu - zvolenou uživatelem
	 * 
	 * @param className
	 *            - název třídy
	 * 
	 * @param path
	 *            - cesta, kde se má třída vytvořit
	 * 
	 * @param packageText
	 *            - text do proměnné balíček
	 * 
	 * @param generateMainMethod
	 *            - logická hodnota, dle které poznám, zda mám vygenerovat hlavní
	 *            metodu main nebo ne
	 * 
	 * @return true, pokud se třída v pořádku vytvoří, jinak false
	 */
    private static boolean createAbstractClass(final String className, final String path,
                                               final String packageText, final boolean generateMainMethod) {
        final Properties languageProperties = App.READ_FILE.getSelectLanguage();
        // Jazyk pro výchozí výpisky do soubory:
        final String commentText;
        final String errorMessage1;
        final String errorMessage2;
        final String errorMessage3;
        final String errorMessageTitle;
		
		if (languageProperties != null) {
			commentText = languageProperties.getProperty("WtfCommentForClass", Constants.WTF_TEXT_COMMENT_CLASS);
			
			errorMessage1 = languageProperties.getProperty("EtfErrorMessage1", Constants.WTF_ERROR_MESSAGE1);
			errorMessage2 = languageProperties.getProperty("EtfErrorMessage2", Constants.WTF_ERROR_MESSAGE2);
			errorMessage3 = languageProperties.getProperty("EtfErrorMessage3", Constants.WTF_ERROR_MESSAGE3);
			errorMessageTitle = languageProperties.getProperty("EtfErrorMessageTitle", Constants.WTF_ERROR_MESSAGE_TITLE);
		}
		
		
		else {
			commentText = Constants.WTF_TEXT_COMMENT_CLASS;
			
			errorMessage1 = Constants.WTF_ERROR_MESSAGE1;
			errorMessage2 = Constants.WTF_ERROR_MESSAGE2;
			errorMessage3 = Constants.WTF_ERROR_MESSAGE3;
			errorMessageTitle = Constants.WTF_ERROR_MESSAGE_TITLE;
		}
		
		
		
		final File fileClass = new File(path + File.separator + className + ".java");

		if (!fileClass.exists()) {
			try (final Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileClass),
					StandardCharsets.UTF_8))) {

				if (packageText != null) {
					writer.write("package " + packageText + "\n");

					writer.write("\n");
				}									
				
				
				// Poznámky:
                writer.write(getAuthorComment(commentText));

                writer.write("public abstract class " + className + " {\n");
                writer.write("\n");
				
				if (generateMainMethod)
					addMainMethod(writer);

                writer.write("}");

                writer.flush();
				
				// Zde byla třída v pořádku vytvořena:
				return true;

            } catch (IOException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při vytváření třídy s názvem: " + className + ", na cestě: " + path
                                    + ". Chyba nastala ve třídě: WriteToFile.java, v metodě: createAbstractClass()!");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
			}
		}
		else
			JOptionPane.showMessageDialog(null,
					errorMessage1 + fileClass.getName() + errorMessage2 + fileClass.getPath() + errorMessage3,
					errorMessageTitle, JOptionPane.ERROR_MESSAGE);
		
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	

	/**
	 * Metoda, která zapíše komentář do "souboru" předaném v parametru Tato metoda,
	 * je pouze pro ušetření několika málo řádků a zvýšení přehlednosti v kódu
	 * protože stejný kód budu zapisovat v 6 podobných situacích - třídách
	 * 
	 * @param bw
	 *            - Writer, který do souboru třídy zapíše "základní kód -
	 *            stavbu třídy"
	 * 
	 * @param textConstructor
	 *            - text pro komentář konstruktoru v uživatelem zvoleném jazyce
	 */
	private static void writeConstructorComment(final Writer bw, final String textConstructor) {
		try {

			bw.write("\t/**\n");
			bw.write("\t * " + textConstructor + "\n");
			bw.write("\t */\n");

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při zapisování dokumentačního komentáře pro konstruktor třídy "
                                + "do souboru třídy, metoda, writeConstructorComment ,trida WriteToFile.java.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
	}


    /**
     * Získání komentáře k hlavičce třídy.
     *
     * @param commentText
     *         - text, který se má vložit jako komentář k hlavičce třídy.
     */
    public static String getAuthorComment(final String commentText) {
        final String actualDateTime = new SimpleDateFormat("dd.MM.yyyy HH:mm").format(new Date());

        return "/**\n" +
                " * " + commentText + "\n" +
                " * \n" +
                " * @author " + UserNameSupport.getUsedUserName() + "\n" +
                " * @version " + 1.0 + "\n" +
                " * @since " + actualDateTime + "\n" +
                " */\n" +
                "\n";
    }



	
	
	/**
	 * Metoda, která vytvoří klasickou Javovskou třídu požadovanou uživatelem
	 * 
	 * @param className
	 *            - název třídy
	 * 
	 * @param path
	 *            - cesta, kde se má třída vytvořit, resp. kam se má uložit
	 * 
	 * @param packageText
	 *            - text pro balíček ve třídě
	 * 
	 * @param generateMainMethod
	 *            - logická proměnná o tom, zda se má vygenerovat hlavní - spouštěcí
	 *            metoda main nebo ne
	 * 
	 * @return true, pokud byla třída v pořádku vytvořena, jinak false
	 */
    private static boolean createClass(final String className, final String path, final String packageText,
                                       final boolean generateMainMethod) {
        final Properties languageProperties = App.READ_FILE.getSelectLanguage();
        // Proměnné do třídy:
        final String commentText;
        final String textConstructor;
        final String errorMessage1;
        final String errorMessage2;
        final String errorMessage3;
        final String errorMessageTitle;
		
		if (languageProperties != null) {
			commentText = languageProperties.getProperty("WtfCommentForClass", Constants.WTF_TEXT_COMMENT_CLASS);
			textConstructor = languageProperties.getProperty("WtfCommentConstructor", Constants.WTF_TEXT_CONSTRUCTOR);
			
			errorMessage1 = languageProperties.getProperty("EtfErrorMessage1", Constants.WTF_ERROR_MESSAGE1);
			errorMessage2 = languageProperties.getProperty("EtfErrorMessage2", Constants.WTF_ERROR_MESSAGE2);
			errorMessage3 = languageProperties.getProperty("EtfErrorMessage3", Constants.WTF_ERROR_MESSAGE3);
			errorMessageTitle = languageProperties.getProperty("EtfErrorMessageTitle", Constants.WTF_ERROR_MESSAGE_TITLE);
		}
		
		
		else {
			commentText = Constants.WTF_TEXT_COMMENT_CLASS;
			textConstructor = Constants.WTF_TEXT_CONSTRUCTOR;
			
			errorMessage1 = Constants.WTF_ERROR_MESSAGE1;
			errorMessage2 = Constants.WTF_ERROR_MESSAGE2;
			errorMessage3 = Constants.WTF_ERROR_MESSAGE3;
			errorMessageTitle = Constants.WTF_ERROR_MESSAGE_TITLE;
		}


		final File fileClass = new File(path + File.separator + className + ".java");

		if (!fileClass.exists()) {
			try (final Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileClass),
					StandardCharsets.UTF_8))) {

				if (packageText != null) {
					writer.write("package " + packageText + "\n");

					writer.write("\n");
				}

				
				// Poznámky:
                writer.write(getAuthorComment(commentText));

                writer.write("public class " + className + " {\n");
                writer.write("\n");
				
				// Konstruktor s dokumentařním komentářem:
				writeConstructorComment(writer, textConstructor);

                writer.write("\tpublic " + className + "() {\n");
                writer.write("\n");
                writer.write("\t}\n");

				// Přidání metody main:
				if (generateMainMethod)
					addMainMethod(writer);

                writer.write("}");


                writer.flush();
				
				// Zde byla třída v pořádku vytvořena:
				return true;

            } catch (IOException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při vytváření třídy jménem: " + className + ", na cestě: " + path
                                    + ". Chyba nastala ve třídě: WriteToFile.java, v metodě: createClass().");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
            }
        }
		else
			JOptionPane
					.showMessageDialog(null,
							errorMessage1 + ": " + fileClass.getName() + " " + errorMessage2 + ": "
									+ fileClass.getPath() + " " + errorMessage3,
							errorMessageTitle, JOptionPane.ERROR_MESSAGE);
		
		// Pokud dojdu sem, tak došlo k chybě při vytvoření třídy:
		return false;
	} 
	
	
	
	
	
	
	/**
	 * Metoda, která přidá hlavní - spouštěcí metodu main do vytvoření třídy
	 * 
	 * @param bw
	 *            - Writer, kam mám zapsat danou metodu
	 */
	private static void addMainMethod(final Writer bw) {
		try {

			bw.write("\n");
			bw.write("\tpublic static void main(java.lang.String[] args) {\n");
			bw.write("\t\n");
			bw.write("\t}\n");

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při vkládání hlavní / spuštěcí metody 'main' do souboru (třídy), metoda, " +
                                "addMainMethod, trida WriteToFile.java.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
	} 
	
	
	
	
	

	
	
	
	@Override
	public String createDirectoriesToClass(String pathToSrcDirectory, String pathToBinDirectory, final String name) {
		final String[] partsOfClassName = name.split("\\.");

		// Projdu si celé pole s vyjímkou poslední hodnoty, tj. název třídy a né balíček
		// a vytvořím příslušné adresáře.
		for (int i = 0; i < partsOfClassName.length - 1; i++) {
			pathToSrcDirectory += File.separator + partsOfClassName[i];
			pathToBinDirectory += File.separator + partsOfClassName[i];

			// Vytvořím příslušný adresář (Pokud již neexistuje):
			try {
				final File fileDir = new File(pathToSrcDirectory);
				// Pouze pokud ještě příslušná složka neexistuje, tak ji vytvořím, jinak ji není
				// třeba vytvářet znovu:
				if (!fileDir.exists() && !fileDir.isDirectory())
					Files.createDirectory(Paths.get(pathToSrcDirectory));	
				
				
				// Otestuji, zda v adresáři bin příslušný balíček ještě neexistuje, a případně ho vytvořím,
				// (podobně jako výše pro adresář src): 
				final File fileBinDir = new File(pathToBinDirectory);
				
				if (!fileBinDir.exists() && !fileBinDir.isDirectory())
					Files.createDirectory(Paths.get(pathToBinDirectory));

            } catch (IOException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při vytváření balíčků do souboru na cestě: " + pathToSrcDirectory);

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
				
				
				return null;
			}
		}
		
		// Zde jsem vytvořil všechny balíčky, zbývá vytvořit třídu:
		// tak vrátím cestu k adresáři , ve kterém se má daná třída vytvořit:
		return pathToSrcDirectory;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda vrátí buď cestu k adresáři src v otevřeném projektu nebo null Pokud
	 * vrátí null, došlo k nějaké chybě
	 * 
	 * Pokud vrátí cestu k adresáři src v projektu, dopadlo to dobře a mohu
	 * pokračovat ve vytváření balíčků a tříd
	 * 
	 * @return String pokud bude nalezena cesta k adresáři src vprojektu jinak vrátí
	 *         null
	 */
    private static String existSrcDirectory() {
        // Text do chybových hlášek:
        final Properties languageProperties = App.READ_FILE.getSelectLanguage();

        final String projectDirErrorTitle;
        final String projectDirErrorText;
        final String projectSrcDirJopErrorText;
        final String projectSrcDirJopErrorTitle;
		
		if (languageProperties != null) {
			projectDirErrorTitle = languageProperties.getProperty("WtfProjectDirErrorTitle", Constants.WTF_PROJECT_DIR_ERROR_TITLE);
			projectDirErrorText = languageProperties.getProperty("WtfProjectDirErrorText", Constants.WTF_PROJECT_DIR_TEXT);			
			projectSrcDirJopErrorText = languageProperties.getProperty("WtfProjectSrcDirJopFailedToCreateErrorText", Constants.WTF_PROJECT_SRC_DIR_FAILED_TO_CREATE_TEXT);
			projectSrcDirJopErrorTitle = languageProperties.getProperty("WtfProjectSrcDirJopFailedToCreateErrorTitle", Constants.WTF_PROJECT_SRC_DIR_FAILED_TO_CREATE_TITLE);
		}
		else {
			projectDirErrorTitle = Constants.WTF_PROJECT_DIR_ERROR_TITLE;
			projectDirErrorText = Constants.WTF_PROJECT_DIR_TEXT;
			projectSrcDirJopErrorText = Constants.WTF_PROJECT_SRC_DIR_FAILED_TO_CREATE_TEXT;
			projectSrcDirJopErrorTitle = Constants.WTF_PROJECT_SRC_DIR_FAILED_TO_CREATE_TITLE;
		}


        final String pathToProjectDirectory = PreferencesApi.getPreferencesValue(PreferencesApi.OPEN_PROJECT, null);

        if (pathToProjectDirectory == null)
            return null;

        // Otestuji, zda ten project ještě existuje:
        if (!ReadFile.existsDirectory(pathToProjectDirectory)) {
            JOptionPane.showMessageDialog(null, projectDirErrorText, projectDirErrorTitle,
                    JOptionPane.ERROR_MESSAGE);
            return null;
        }


        // Zde existuje adresář projektu, tak otestuje adresář src, pokud nebude nalazen nebudu ho vytvářet
        // Pouze to onámím uživateli, aby ho znovu vytvořil, protože
        // uživatel např chce aby se souboruy ukládaly jinam, tak si je tam
        // ukládá třeba "ručně" sám, nebo došlo k nějaé chybě s byl přesunut, apod.

        final File fileSrcDirectory = new File(pathToProjectDirectory + File.separator + "src");

        if (ReadFile.existsDirectory(fileSrcDirectory.getAbsolutePath()))
            // Zde existuje adresář src, tak mohu vrátit cestu do tohoto
            // adresáře a vytvořit v něm případné balíčky a třídu:
            return fileSrcDirectory.getAbsolutePath();


        // Zde adresáš src v otevřeném projktu neexistuje, tak ho
        // vytvořím:
        try {
            Files.createDirectories(Paths.get(fileSrcDirectory.getPath()));
        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při vytváření adresáře 'src' v adresáři projektu, třídy " +
                                "WriteToFile, metoda: existSrcDirectory. Cesta k adresáři src: "
                                + fileSrcDirectory.getAbsolutePath());

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();

            JOptionPane.showMessageDialog(null, projectSrcDirJopErrorText, projectSrcDirJopErrorTitle,
                    JOptionPane.ERROR_MESSAGE);
        }
        // zde vrátím cestu k vytvořenému adresáři:
        return fileSrcDirectory.getAbsolutePath();
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda existuje adresář bin v adresáře otevřeného
	 * projektu, pokud existuje, vrátí k němu cestu, pokud neexistuje, tak se ho
	 * pokusí vytvořit a vrátí k němu cestu, pokud by něco selhalo a adresář bin by
	 * stále neexistoval, metoda v určité části vypíše chybové hlášení uživateli a
	 * vrátí null
	 * 
	 * @return null, nebo cestu k adrsář bin v otevřeném projektu
	 */
	private static String existBinDirectory() {
        // Text do chybových hlášek:
        final Properties languageProperties = App.READ_FILE.getSelectLanguage();

        final String projectDirErrorTitle;
        final String projectDirErrorText;
        final String projectBinJopErrorText;
        final String projectBinJopErrorTitle;
		
		if (languageProperties != null) {
			projectDirErrorTitle = languageProperties.getProperty("WtfProjectDirErrorTitle", Constants.WTF_PROJECT_DIR_ERROR_TITLE);
			projectDirErrorText = languageProperties.getProperty("WtfProjectDirErrorText", Constants.WTF_PROJECT_DIR_TEXT);
			projectBinJopErrorText = languageProperties.getProperty("WtfProjectBinDirErrorText", Constants.WTF_BIN_DIR_FAILED_TO_CREATE_ERROR_TEXT);
			projectBinJopErrorTitle = languageProperties.getProperty("WtfProjectBinDirErrorTitle", Constants.WTF_BIN_DIR_FAILED_TO_CREATE_ERROR_TITLE);
		}
		
		
		else {
            projectDirErrorTitle = Constants.WTF_PROJECT_DIR_ERROR_TITLE;
            projectDirErrorText = Constants.WTF_PROJECT_DIR_TEXT;
            projectBinJopErrorText = Constants.WTF_BIN_DIR_FAILED_TO_CREATE_ERROR_TEXT;
            projectBinJopErrorTitle = Constants.WTF_BIN_DIR_FAILED_TO_CREATE_ERROR_TITLE;
        }


        // Zjistím si cestu k projektu a do adresáře bin - pokud existuje
        final String pathToProjectDirectory = PreferencesApi.getPreferencesValue(PreferencesApi.OPEN_PROJECT, null);

        if (pathToProjectDirectory == null)
            return null;

        // Otestuji, zda ten project ještě existuje:
        if (!ReadFile.existsDirectory(pathToProjectDirectory)) {
            JOptionPane.showMessageDialog(null, projectDirErrorText, projectDirErrorTitle,
                    JOptionPane.ERROR_MESSAGE);
            return null;
        }
        // Zde existuje adresář projektu, tak otestuje adresář bin, pokud bude nalezen, vrátí se potřebná cesta, pokud
        // ne, vytvoří se znovu

        final File fileBinDirectory = new File(pathToProjectDirectory + File.separator + "bin");

        if (ReadFile.existsDirectory(fileBinDirectory.getAbsolutePath()))
            // Zde existuje adresář src, tak mohu vrátit cestu do tohoto adresáře a vytvořit v něm případné balíčky a
            // třídu:
            return fileBinDirectory.getAbsolutePath();


        // Zde adresář bin bu�? neexistuje nebo tam existuje nějaký soubor a nejedná se o adresář,
        // Ať tak či tak, potřrbuji ho znovu vytvořit::
        try {
            Files.createDirectories(Paths.get(fileBinDirectory.getPath()));
        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při vytváření adresáře 'bin' v adresáři projektu, třídy " +
                                "WriteToFile, metoda: existBinDirectory. Cesta k adresáři bin: "
                                + fileBinDirectory.getAbsolutePath());

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();

            JOptionPane.showMessageDialog(null, projectBinJopErrorText, projectBinJopErrorTitle,
                    JOptionPane.ERROR_MESSAGE);
        }
        // zde vrátím cestu k vytvořenému adresáři:
        return fileBinDirectory.getAbsolutePath();
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	@Override
	public void deleteClassFromSrcDirectory(final String cellValue) {
		final String srcDirectory = existSrcDirectory();
		
		if (srcDirectory != null) {
			// Zde existuje adresář src a mám k němu cestu, nyní zjistím název třídy pro smazání:
			
			// Název třídy, ve ktrém případně nahrandím tečky za zpětná lomítka do cesty k souboru
			final String className = cellValue.replace(".", File.separator);

			// Nyní otestuji existencí třídy:
			final File fileClass = new File(srcDirectory + File.separator + className + ".java");

			if (fileClass.exists() && fileClass.isFile())
				fileClass.delete();
		}
	}
	
	
	
	
	
	
	
	
	

	@Override
	public void saveClassDiagram(final List<DefaultGraphCell> classDiagramObjects) {
		// Zjistím si cestu k aktuálně otevřenému projektu

		final String pathToProject = PreferencesApi.getPreferencesValue(PreferencesApi.OPEN_PROJECT, null);

		// Otestuji, zda vůbec existuje - nemusel bych - protože na něm uživatel
		// právě pracuje v aplikace
		// ale kdyby mez tím náhodou smazal projektu, aby nenastala chyba:
        if (pathToProject == null)
            return;

        final File fileProjectDir = new File(pathToProject);

        // Zde adresář projektu opravdu existuje, a je to adresář, tak
        // uložím class diagram:
        if (fileProjectDir.exists() && fileProjectDir.isDirectory()) {
            try (final OutputStream out = new FileOutputStream(
                    fileProjectDir.getPath() + File.separator + Constants.NAME_OF_CLASS_DIAGRAM_FILE)) {

                final XMLEncoder end = new XMLEncoder(out);

                end.writeObject(classDiagramObjects);
                end.close();

                out.flush();

            } catch (FileNotFoundException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při ukládání class diagramu do souboru. Třída WriteToFile.java, " +
                                    "metoda saveClassDiagram(). Tato chyba může nastat například v případě, "
                                    + "že soubor existuje, ale jedná se o adresář a ne o 'běžný soubor', nebo " +
                                    "neexistuje a nemůže / nepodařilo se jej vytvořit, nebo nemůže být otevřen z" +
                                    " nějakého jiného důvodu.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();

            } catch (IOException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při ukládání class diagramu do souboru. Třída WriteToFile.java,"
                                    + " metoda saveClassDiagram(). Jedná se o chybu, ke které mohlo dojít při " +
                                    "uzavírání streamu k danému souboru.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
            }
        }
    }

	
	
	
	
	
	
	
	
	

	@Override
	public void setCloseProject(final String value) {
		// V preferencich nastavim promennou, ktera si pamatuje cestu k otevrenemu
		// projektu
		// na Closed, dle ceho se pozna, ze je projekt zavreny - není otevreny
		PreferencesApi.setPreferencesValue(PreferencesApi.OPEN_PROJECT, value);
	}
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public void deletePackageIfExist(final String pathToClass) {
		final ReadWriteFileInterface editCodeClass = new ReadWriteFile();
		
		// Přečtu si text třídy:
		final List<String> textOfClass = editCodeClass.getTextOfFile(pathToClass);
		
		
		// Vyhledám v ní balíček, případně ho smažu:
		final String patterForPackage = "^\\s*package\\s+\\w+[\\.\\w+\\s*]*\\s*;?\\s*$";
		
		for (int i = 0;i < textOfClass.size(); i++) {
			if (textOfClass.get(i).matches(patterForPackage))
				textOfClass.remove(i);
		}
		
		
		// Zapíšu text zpět do třídy:
		editCodeClass.setTextToFile(pathToClass, textOfClass);	
	}


	
	
	


	
	
	@Override
	public void addPackageToClass(final String pathToClass, final String packageText) {
		final ReadWriteFileInterface editCodeClass = new ReadWriteFile();
		
		final List<String> textOfClass = editCodeClass.getTextOfFile(pathToClass);
		
		textOfClass.add(0, "package " + packageText + ";");
		
		editCodeClass.setTextToFile(pathToClass, textOfClass);
	}


	
	
	
	
	
	
	

	@Override
	public void renameClass(final String pathToClass, final String oldName, final String newName) {
		if (!oldName.equals(newName)) {
			final ReadWriteFileInterface editCodeClass = new ReadWriteFile();
			
			List<String> textOfClass = editCodeClass.getTextOfFile(pathToClass);
			
			
			// regulární výraz pro konstruktor: public className() ( } )  (muze byt i parametr)
			final String regexConstructor = "^\\s*public\\s+" + oldName + "\\s*[(]\\s*[\\w*,\\s*()\\.]*\\s*[)]\\s*[{]?\\s*$";
			
			
			// Dále se ohledně konstruktoru mohou nacházet  dané třídě následující syntaxe:
			
			
			// new Classname();
			final String regExNewClassName = "^\\s*new\\s+" + oldName + "\\s*[(]\\s*[)]\\s*;\\s*$";
			
			// new ClassName(s parametry, ..);
			final String regExNewClassNameWithParameters = "^\\s*new\\s+" + oldName + "\\s*[(]\\s*('.'|\".*\"|true|false|null|[\\w\\s(),\\.'\"\\+-])+\\s*[)]\\s*;\\s*$";
			
			// variable = new ClassName();
			final String regExNewfillVar = "^\\s*\\w+\\s*=\\s*new\\s+" + oldName + "\\s*[(]\\s*[)]\\s*;\\s*$";
			
			// variable = new ClassName(s parametry);
			final String regExNewfillVarWithParameters = "^\\s*\\w+\\s*=\\s*new\\s+" + oldName + "\\s*[(]\\s*('.'|\".*\"|true|false|null|[\\w\\s(),\\.'\"\\+-])+\\s*[)]\\s*;\\s*$";
			
			// (private|public|protected static final) ClassName variable = new ClassName();
			final String regExAllVar = "^\\s*(private\\s+|public\\s+|protected\\s+)?((static\\s+)?(final\\s+)|final\\s+)?(static\\s+)?" + oldName + "\\s+\\w+\\s*=\\s*new\\s+" + oldName + "\\s*[(]\\s*[)]\\s*;\\s*$";
			
			// (private static final) ClassName variable = new ClassName(s parametry);
			final String regExAllVarWithParameters = "^\\s*(private\\s+|public\\s+|protected\\s+)?((static\\s+)?(final\\s+)|final\\s+)?(static\\s+)?" + oldName + "\\s+\\w+\\s*=\\s*new\\s+" + oldName + "\\s*[(]\\s*('.'|\".*\"|true|false|null|[\\w\\s(),\\.'\"\\+-])+\\s*[)]\\s*;\\s*$";
			
			// (private|public|protected static final) ClassName variable;
			final String regExDeclarationVar = "^\\s*(private\\s+|public\\s+|protected\\s+)?((static\\s+)?(final\\s+)|final\\s+)?(static\\s+)?" + oldName + "\\s+\\w+\\s*;\\s*$";
			
			
			
			for (int i = 0; i < textOfClass.size(); i++) {
				if (TestAndCreateEdge.regularExpressionForClassHead(textOfClass.get(i), oldName) > -1)
					// Zde jsem našel hlavičku, tak ji přejmenuji:					
					textOfClass = replaceTextInCode(textOfClass, i, oldName, newName);
				
				
				else if (textOfClass.get(i).matches(regexConstructor))
					// Zde jsem našel konstruktor, tak ho přejmenuji:
					textOfClass = replaceTextInCode(textOfClass, i, oldName, newName);
				
				
				else if (textOfClass.get(i).matches(regExNewClassName))
					// Zde zadaný řádek odpovídá syntaxi: new Classname();
					textOfClass = replaceTextInCode(textOfClass, i, oldName, newName);
				
				
				else if (textOfClass.get(i).matches(regExNewClassNameWithParameters))
					// Zde zadaný řádek odpovídá syntaxi: new ClassName(s parametry, ..);
					textOfClass = replaceTextInCode(textOfClass, i, oldName, newName);
				
				
				else if (textOfClass.get(i).matches(regExNewfillVar))
					// Zde zadaný řádek odpovídá syntaxi: variable = new ClassName();
					textOfClass = replaceTextInCode(textOfClass, i, oldName, newName);
				
				
				else if (textOfClass.get(i).matches(regExNewfillVarWithParameters))
					// Zde zadaný řádek odpovídá syntaxi: variable = new ClassName(s parametry);
					textOfClass = replaceTextInCode(textOfClass, i, oldName, newName);
				
				
				else if (textOfClass.get(i).matches(regExAllVar))
					// Zde zadaný řádek odpovídá syntaxi: (private|public|protected static final) ClassName variable = new ClassName();
					textOfClass = replaceTextInCode(textOfClass, i, oldName, newName);
				
				
				else if (textOfClass.get(i).matches(regExAllVarWithParameters))
					// Zde zadaný řádek odpovídá syntaxi: (private|public|protected static final) ClassName variable = new ClassName(s parametry); 
					textOfClass = replaceTextInCode(textOfClass, i, oldName, newName);
				
				
				else if (textOfClass.get(i).matches(regExDeclarationVar))
					// (private|public|protected static final) ClassName variable; 
					textOfClass = replaceTextInCode(textOfClass, i, oldName, newName);
			}
			
			
			
			// Zapíšu upravený kód zpět do třídy:
			editCodeClass.setTextToFile(pathToClass, textOfClass);
		}
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která nahradí část textu v zdrojovém kódu třídy. Konkrétně nahradí
	 * původní - starý název třídy za nový - přejmenování třídy
	 * 
	 * V takovém případě je třeba přepsat ve zdrojovém kódu třídy její hlavičku a
	 * konstruktory
	 * 
	 * @param textOfClass
	 *            - zdrojový kód třídy, ve kterém se má nahradit nový název třídy za
	 *            starý na daném řádku třídy
	 * 
	 * @param line
	 *            - index řádku - resp. hodnoty v kolekci, na kterém chci nahradit
	 *            původní název na nový
	 * 
	 * @param oldName
	 *            - původní - starý název třídy, který chci nahradit
	 * 
	 * @param newName
	 *            - nový název třídy, kterým se nahradí původní název třídy -
	 *            parametr výše
	 * 
	 * @return kolekci s nahrazeným řádkem
	 */
	private static List<String> replaceTextInCode(final List<String> textOfClass, final int line, final String oldName,
			final String newName) {
		String textFromCode = textOfClass.get(line);
		
		textFromCode = textFromCode.replace(oldName, newName);
		textOfClass.set(line, textFromCode);
		
		return textOfClass;
	}
	
	
	
	
	
	
	
	
	
	
	@Override
    public void removeSelectedObject(final GraphClass classDiagram, final Properties languageProperties,
                                     final boolean deleteEdgesInCell) {
        // Promenné do chybových hlášek v metodě;
        final String jopAssociateTitle;
        final String jopAssociateText_1;
        final String jopAssociateText_2;
        final String jopInheritErrorTitle;
        final String jopInheritErrorText;
        final String jopImplementsErrorTitle;
        final String jopImplementsErrorText;
        final String jopAggregationErrorTitle;
        final String jopAggregation_1_1_ErrorText;
        final String jopAggregation_1_N_ErrorText;
        final String jopSelectedObjErrorText;
        final String jopSelectedObjErrorTitle;
		
		
		
		if (languageProperties != null) {
			// texty do chybových hlášek:
			jopAssociateTitle = languageProperties.getProperty("MnJopAssociationErrorTitle", Constants.MN_JOP_ASSOCIATE_ERROR_TITLE);
			jopAssociateText_1 = languageProperties.getProperty("MnJopAssociationErrorText_1", Constants.MN_JOP_ASSOCIATE_ERROR_TEXT_JOP_1);
			jopAssociateText_2 = languageProperties.getProperty("MnJopAssociationErrorText_2", Constants.MN_JOP_ASSOCIATE_ERROR_TEXT_JOP_2);
			jopInheritErrorTitle = languageProperties.getProperty("MnJopInheritErrorTitle", Constants.MN_JOP_INHERIT_ERROR_TITLE);
			jopInheritErrorText = languageProperties.getProperty("MnJopInheritErrorText", Constants.MN_JOP_INHERIT_ERROR_TEXT);
			jopImplementsErrorTitle = languageProperties.getProperty("MnJopImplementsErrorTitle", Constants.MN_JOP_IMPLEMENTS_ERROR_TITLE);
			jopImplementsErrorText = languageProperties.getProperty("MnJopImplementsErrorText", Constants.MN_JOP_IMPLEMENTS_ERROR_TEXT);
			jopAggregationErrorTitle = languageProperties.getProperty("MnJopAggregation1_1_1_ErrorTitle", Constants.MN_JOP_AGGREGATION_1_1_ERROR_TITLE);
			jopAggregation_1_1_ErrorText = languageProperties.getProperty("MnJopAggregation1_1_1_ErrorText", Constants.MN_JOP_AGGREGATION_1_1_TEXT);
			jopAggregation_1_N_ErrorText = languageProperties.getProperty("MnJopMnJopAggregation1_1_N_ErrorText", Constants.MN_JOP_AGGREGATION_1_N_ERROR_TEXT);
			jopSelectedObjErrorText = languageProperties.getProperty("MnJopSelectedObjectErrorText", Constants.MN_JOP_SELECTED_OBJ_ERROR_TEXT);
			jopSelectedObjErrorTitle = languageProperties.getProperty("MnJopSelectedObjectErrorTitle", Constants.MN_JOP_SELECTED_OBJ_ERROR_TITLE);
		}
		
		
		else {
			jopAssociateTitle = Constants.MN_JOP_ASSOCIATE_ERROR_TITLE;
			jopAssociateText_1 = Constants.MN_JOP_ASSOCIATE_ERROR_TEXT_JOP_1;
			jopAssociateText_2 = Constants.MN_JOP_ASSOCIATE_ERROR_TEXT_JOP_2;
			jopInheritErrorTitle = Constants.MN_JOP_INHERIT_ERROR_TITLE;
			jopInheritErrorText = Constants.MN_JOP_INHERIT_ERROR_TEXT;
			jopImplementsErrorTitle = Constants.MN_JOP_IMPLEMENTS_ERROR_TITLE;
			jopImplementsErrorText = Constants.MN_JOP_IMPLEMENTS_ERROR_TEXT;
			jopAggregationErrorTitle = Constants.MN_JOP_AGGREGATION_1_1_ERROR_TITLE;
			jopAggregation_1_1_ErrorText = Constants.MN_JOP_AGGREGATION_1_1_TEXT;
			jopAggregation_1_N_ErrorText = Constants.MN_JOP_AGGREGATION_1_N_ERROR_TEXT;
			jopSelectedObjErrorText = Constants.MN_JOP_SELECTED_OBJ_ERROR_TEXT;
			jopSelectedObjErrorTitle = Constants.MN_JOP_SELECTED_OBJ_ERROR_TITLE;
		}
		
		
		
		
		
		
		// Oznčený objekt v grafu:
		final Object selectedObject = classDiagram.getSelectionCell();
				
		
		// Zjistím si cestu k adresáři src, abych mohl zjistit, zda existuje daná třída:
		final String pathToSrcDir = App.READ_FILE.getPathToSrc();
		
		
		if (selectedObject != null && pathToSrcDir != null) {
			if (selectedObject instanceof DefaultEdge) {
				// Zde je označená hrana:
			
				// Otestuji, zda její zdroj existuje v adresáři projektu, pokud ne, jedná se o hranu připojenou ke komentáři
				// tak ji smažu i s komentářem:

				
				// Zdroj hrany src a cíl hrany - des
				// PŮVODNÍ:
//				final DefaultGraphCell srcCell = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) selectedObject).getSource()).getParent();
//				final DefaultGraphCell desCell = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) selectedObject).getTarget()).getParent();
				
				// NOVE:
				final DefaultGraphCell srcSource = (DefaultGraphCell) ((DefaultEdge) selectedObject).getSource();
				final DefaultGraphCell desTarget = (DefaultGraphCell) ((DefaultEdge) selectedObject).getTarget();
				
				
				/*
				 * V případě, že se nepodaří načíst zdroj nebo cíl hrany selectedObject, tak tu
				 * hranu mohu rovnou smazat. Tento případ by neměl nastat, ale když se někde
				 * například smažou nějaké objekty apod. A z nějakého důvodu se u nějaké hrany
				 * smažou její zdroj a cíl mezi kterými hrana existuje, samotná hrana senesmaže,
				 * tak v tomto případě se smaže.
				 */
				if (srcSource == null || desTarget == null) {
					if (GraphClass.getCellsList().contains(selectedObject))
						classDiagram.deleteObjectFromGraph(selectedObject, GraphClass.getCellsList());
					else
						classDiagram.deleteObjectFromModelOfGraph(selectedObject, GraphClass.getCellsList());
					
					return;
				}
				
				final DefaultGraphCell srcCell = (DefaultGraphCell) srcSource.getParent();
				final DefaultGraphCell desCell = (DefaultGraphCell) desTarget.getParent();
				

				
			
				// Otestuji, zda se náhodou jeden z objektů, se kterými je
				// spojena hrana náhodou neexistuje,
				// - k tomu by dojít nemělo, ale kdby náhodou, tak tuto hrena
				// smažu, protože už nemohu otestovat,
				// zbytek
				if (srcCell == null || desCell == null) {
					classDiagram.deleteObjectFromGraph(selectedObject, GraphClass.getCellsList());
					return;
				}
				

				// Zde se jedná o hranu, která reprezentuje nějaký vztah mezi třídami:
				
				if (GraphClass.KIND_OF_EDGE.isCommentEdge((DefaultEdge) selectedObject)) {
					// // Zde se jedná hranu pro komentář, tak ho i s hranou smažu:
					classDiagram.deleteObjectFromGraph(srcCell, GraphClass.getCellsList());
					classDiagram.deleteObjectFromGraph(selectedObject, GraphClass.getCellsList());
				}

				
				// Hrana reprezentuje asociaci - tak v obou třídách smazat
				// reference na tu druhou třídu
				else if (GraphClass.KIND_OF_EDGE.isAssociationEdge((DefaultEdge) selectedObject)) {
					/*
					 * Zde si nejprve získám popisky na hraně - jedná se o kardinalitu na příslušné
					 * hraně, která reprezentuje vztaha typu (symetrická) asociace.
					 */
					final Object[] labels = GraphConstants.getExtraLabels(((DefaultEdge) selectedObject).getAttributes());
					
					/*
					 * Zde si vezmu jednu z položek, obě by měli být stejné, u vztahu typu asociace
					 * jsou na obou koncích hran stejná čísla.
					 * 
					 * Dále zde nebudu zachytávat NumberFormatEx... protože se ty čísla přidávájí v
					 * kódu a uživatel je nemůže nijak ovlivnit, proto mám jistotu, že tam opravdu
					 * budou a typu integer, tak si tu hodnotu prostě vezmu a přetypuji.
					 * 
					 * Jedná se o počet proměnných pro smazání z příslušné třídy - obou tříd.
					 */
					final int cardinality = Integer.parseInt(labels[0].toString());

					// Zde vím, že oznašená hrana je asociace, tak smažu
					// reference v obou třídách:
					if (editClass.deleteAssociation(pathToSrcDir, srcCell.toString(), desCell.toString(), cardinality))
						// Zde proběhlo úspěšně smazání atributů, tak mohu smazat i hranu v grafu:
						classDiagram.deleteObjectFromGraph(selectedObject, GraphClass.getCellsList());
				}

				// Hrana reprezentuje dědičnost - ve zdrojové třídě smazat
				// dědičnost té cílové třídy
				else if (GraphClass.KIND_OF_EDGE.isInheritEdge((DefaultEdge) selectedObject)) {
					if (editClass.deleteInherit(pathToSrcDir, srcCell.toString()))
						classDiagram.deleteObjectFromGraph(selectedObject, GraphClass.getCellsList());
				}

				
				// Hrana reprezentuje Implementaci rozhraní - v zdrojové třídě
				// smazat implementaci rozhraní
				else if (GraphClass.KIND_OF_EDGE.isImplementsEdge((DefaultEdge) selectedObject)) {
					if (editClass.deleteImplements(pathToSrcDir, srcCell.toString(), desCell.toString()))
						classDiagram.deleteObjectFromGraph(selectedObject, GraphClass.getCellsList());
				}

				// Hrana reprezentuje agregaci 1:1 - ve zrojové třídě smazat
				// referenci na cílovou:
				else if (GraphClass.KIND_OF_EDGE.isAggregation_1_ku_1_Edge((DefaultEdge) selectedObject)) {
					/*
					 * Získám si text na obou koncích hrany - text, který reprezentuje kardinalitu /
					 * (multiplicitu) vztahu.
					 */
					final Object[] labels = GraphConstants
							.getExtraLabels(((DefaultEdge) selectedObject).getAttributes());

					/*
					 * Získám si počet proměnných v té první třídě typu té druhé třídy - popisky na
					 * hranách jsou obráceně, takže si vezmu ten na druhém místě (index: 1).
					 */
					final int cardinalityInSrc = Integer.parseInt(labels[1].toString());
					/*
					 * Získám si počet proměnných v té druhé třídě typu té první třídy - popisky na
					 * hranách jsou obráceně, takže si vezmu ten na prvním místě (index: 0).
					 */
					final int cardinalityInDes = Integer.parseInt(labels[0].toString());

					// Smažu příslušnou hranu a proměnné:
					if (editClass.deleteAggregation_1_ku_1(pathToSrcDir, srcCell.toString(), desCell.toString(),
							cardinalityInDes, cardinalityInSrc, classDiagram.isShowAssociationThroughAggregation()))
						classDiagram.deleteObjectFromGraph(selectedObject, GraphClass.getCellsList());
				}

				// Hrana reprezentuje agregaci 1 : N - ve zdrojové třídě smazat
				// referenci na list cílové třídy
				else if (GraphClass.KIND_OF_EDGE.isAggregation_1_ku_N_Edge((DefaultEdge) selectedObject)) {
					if (editClass.deleteAggregation_1_ku_N(pathToSrcDir, srcCell.toString(), desCell.toString()))
						classDiagram.deleteObjectFromGraph(selectedObject, GraphClass.getCellsList());
				}
			}
			
			
			else {
				// Zde se jedná o buňku:
				// pro začátek otestuji, zda se příslušná třída (pokud to není komentář) nemá smazat úplně i se všemy hranami:
				if (deleteEdgesInCell) {
					// Nejprve se smažou všechny hrany k příslušnému komentáři - buňce:
					classDiagram.deleteAllEdgesByCell((DefaultGraphCell) selectedObject);

					// a nyní i samotnou buňku coby komentář:
					classDiagram.deleteObjectFromGraph(selectedObject, GraphClass.getCellsList());
				}
				
				

				else {

					// Otestuji, zda se jedná o bunku, ktera reprezentuje
					// komentar v diagramu trid:
					if (GraphClass.KIND_OF_EDGE.isCommentCell((DefaultGraphCell) selectedObject)) {
						// Zde daný soubor neexistuje, tudíž se jedná o
						// komentář, tak ho mohu smazat: i s jeho hranou:
						final DefaultGraphCell cell = (DefaultGraphCell) selectedObject;

						final DefaultEdge edgeOfTheCommentCell = GraphClass.getEdgeOfTheCommentCell(cell);

						if (edgeOfTheCommentCell != null)
							classDiagram.deleteObjectFromGraph(edgeOfTheCommentCell, GraphClass.getCellsList());

						// Zde smažu označenou bu�?ku zvlášt, pro případ, že by nebyla nalezena hrana
						// kdybych to dal do podmínky výše, nesmazala by se ani označeý komentář:
						classDiagram.deleteObjectFromGraph(cell, GraphClass.getCellsList());
					}

					// Zde se jedná o označenou třídu, tak k ní musím najít a
					// otestovat všechny vazby a dle toho rozhodnout, že půjde
					// nebo nepůjde smazat
					else {
						// Musím zjistit všechny hrany, resp, vazby - vztah, ve
						// kterých je daná třída
							
						// Zjistit všechny hrany, které jsou s danou třídou spojeny, ať uz je daná bu�?ka zdroj nebo cíl
							
							
						// Projdu kolekci hran a otestuji, zda jejich zdroj je daná třída, pokud ano, tak musím otestovat
						
						//  asociaci, tam je reference mezi oběma třídami, pokud existuje, třída NELZE smazat
						// U ostatních vazev - pokud je daná třída zdrojem, smazat lze:

						// Hlavně smazat hrany:
						// Pokud třída dědí, smaže se i vazba dědičnosti (smazat hranu)
						// Pokud třída Implemetnuje, smaže se implementace cílové třídy - což je v té třídě (smazat hranu)
						// třída je zdrojem agreface 1 : 1 (smazat hranu)
						// Třída je zdrojem agregace 1 : N (smazat hranu)
						
						
						
						// Projdu tu samou kolekci a:
						// Pokud je daná třída cílem asociace - smazat nelze - nejprve smazat vazbu asociace
						// Pokud je daná třída cílem dědičnosti - smazat nelze - nejprve smazat vazbu dědičnosti
						// Pokud je daná třída cílem implementace - smazat nelze - nejprve smazt vazbu implementace
							// Pokud je daná třída cílem agregace 1 : 1 - smazat nelze - nejprve smazat vazbu agregace
						// POkud je daná třída cílem agregace 1 : N - smazat nelze - jeprve smazat vazbu agregace 
							
							
							
							
							
							
						// Mám označenou bu�?ku a vím, že je to třída:
						// Projdu kolekci a otestuji, vsechny hrany z dané bu�?ky:
						// Mám označenou třídu, tak při procházení se budu ptát jestli se jedná o hranu, pokud ano,
						// tak otstuji možnosti výše:
						final List<DefaultGraphCell> listForDelete = new ArrayList<>();
						
						// Proměnná, dle které poznám, zda mohu kolekci vymazat nebo ne,
						// pokud jen jediná podmínka nebude splněna, proměnná se nastaví na false, a objekty se nevymažou
						boolean deleteVariable = true;
						
						for (final DefaultGraphCell cell : GraphClass.getCellsList()) {
							if (cell instanceof DefaultEdge) {
								// Zde se jedná o hranu v grafu:

								// Betmu si zdroj a cíl hrany:
								final DefaultGraphCell srcCell = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) cell)
										.getSource()).getParent();
								final DefaultGraphCell desCell = (DefaultGraphCell) ((DefaultGraphCell) ((DefaultEdge) cell)
										.getTarget()).getParent();

								if (GraphClass.KIND_OF_EDGE.isAssociationEdge((DefaultEdge) cell)) {
									// Zde je aktuální hrana v kolekci asociace, pokud je označená bu�?ka zdrojem nebo
									// cílem asociace, nelze smazat - mají na sebe reference,
									// dokud se bazba neodstraní, tak nelze smazat:

									if (selectedObject == srcCell) {
										// Zde je označená třída - bu�?ka zdroje asociace, má vztah na cílovou tridu a
										// cilova na zdrojovou, tudíž
										// dokud tento vztah existuje, třída - bu�?ka nelze smazt
										deleteVariable = false;
										
										JOptionPane.showMessageDialog(classDiagram, jopAssociateText_1,
												jopAssociateTitle, JOptionPane.ERROR_MESSAGE);
										
										// Ukončím cyklus, protože bu�?ku nelze
										// smaazt, nemá smysl dál testovat ostatní možnosti
										break;
									}
									
									else if (selectedObject == desCell) {
										// Zde je označená třída cílem vztahu
										// asociace, zdrojová bu�?ka ma na tuto
										// třídu reference a naopak, dokud tento
										// vztah bude existovat trída - bu'�?ka
										// nelze smazat
										deleteVariable = false;
										
										JOptionPane.showMessageDialog(classDiagram, jopAssociateText_2,
												jopAssociateTitle, JOptionPane.ERROR_MESSAGE);
										
										break;
									}
								}

								else if (GraphClass.KIND_OF_EDGE.isInheritEdge((DefaultEdge) cell)) {
									// Zde se jedná o hranu v kolekci, která reprezentuje dědičnost

									// Pokud je označená třída zdrojem dědičnost, tak lze smazat
									// Pokud je označená třída cílem dědičnost, tak nelze smazat - nějaká třídá z té
									// aktuálně označené dědí
									if (selectedObject == srcCell)
										// Hrana se také smaže, tak ji přidám do
										// kolekce pro smazání
										listForDelete.add(cell);
									
									
									else if (selectedObject == desCell) {
										// Zde je označená třída cílem dědíčnosti, tak nelze smazat, -
										// nějaká třída z této označené třídy dědí:
										deleteVariable = false;
										
										JOptionPane.showMessageDialog(classDiagram, jopInheritErrorText,
												jopInheritErrorTitle, JOptionPane.ERROR_MESSAGE);
										
										break;
									}
								}

								else if (GraphClass.KIND_OF_EDGE.isImplementsEdge((DefaultEdge) cell)) {
									// Zde se jedná o hranu v kolekci, která reprezentuje implementaci rozhraní

									// Pokud je označená třáda zdroj, tak implementuje cílovou třídu - rozhraní,
									// takže ji i s hranou mohu smazat

									// POkud je označená třída cíl, pak se jedná o rozhraní a nějaká třída ho
									// implementaje, takže dokud bude existovat hrana,
									// resp. třída, která toto rozhraní implementuje, pak nepůjde smazat:

									if (selectedObject == srcCell)
										// Zde je označená třída zdroj, -
										// implementuje cílové rozhraní, lze i s hranou smazat:
										listForDelete.add(cell);
									
									
									else if (selectedObject == desCell) {
										// Zde je označená třída cílem implementace, tudiž se jedná o
										// rozhraní a nějaká třída ho implementuje, nelze smazat:
										deleteVariable = false;
										
										JOptionPane.showMessageDialog(classDiagram, jopImplementsErrorText,
												jopImplementsErrorTitle, JOptionPane.ERROR_MESSAGE);
										
										break;
									}
								}

								else if (GraphClass.KIND_OF_EDGE.isAggregation_1_ku_1_Edge((DefaultEdge) cell)) {
									// Zde se jedná o hranu v kolekci, která
									// rerezentuje agregaci - 1 : 1
									// Zdrojová třída má referenci na cílovou

									if (selectedObject == srcCell)
										// Zde má označená třída referenci na cílovou, třídu i s hranou mohu
										// smazat:
										listForDelete.add(cell);
									
									
									else if (selectedObject == desCell) {
										// Zde je označená třída cílem agregac 1 : 1, - nějaká třída má na tuto
										// označenou referenci, nelze smazat:
										deleteVariable = false;
										
										JOptionPane.showMessageDialog(classDiagram, jopAggregation_1_1_ErrorText,
												jopAggregationErrorTitle, JOptionPane.ERROR_MESSAGE);
										
										break;
									}
								}

								else if (GraphClass.KIND_OF_EDGE.isAggregation_1_ku_N_Edge((DefaultEdge) cell)) {
									// Zde se jedná o hranu v kolekci, která reprezentuje agregaci - 1 : N

									if (selectedObject == srcCell)
										// Označená třída má refereci na List cílových tříd, lze smazat, i s
										// hranou:
										listForDelete.add(cell);
									
									
									else if (selectedObject == desCell) {
										// Zde má nějaká třída referenci na List této označeno třídy, nelze smazat:
										deleteVariable = false;
										
										JOptionPane.showMessageDialog(classDiagram, jopAggregation_1_N_ErrorText,
												jopAggregationErrorTitle, JOptionPane.ERROR_MESSAGE);
										
										break;
									}
								}
									
								// Zde už zbývá pouze hrana komentáře, tak musím otestovat, zda cíl hrany je označený objekt
								// pokud ano, tak její zdroj nesmí existovat, pokud tomu tak je, tak smažu hranu i její zdroj - buňka komentaře
								
								// NOvá podmínka pro komentár:
								else if (GraphClass.KIND_OF_EDGE.isCommentEdge((DefaultEdge) cell)) {
									if (selectedObject == desCell) {
										// Zde je označená buňka nejspíše cíl komentáře, tak otestuji, zda je to opravdu komentář nebo nějaká třída - pro jistotu:
										if (GraphClass.KIND_OF_EDGE.isCommentCell(srcCell)) {
											// Zde je zdrojová bunka bunka, která reprezentuje komentar, tak ho muzu smazat i s hranou:
											listForDelete.add(cell);
											listForDelete.add(srcCell);
										}
									}
								}
							}
						}
							
						// na konec vymažu z grafu označené položky pro smazání (pokud můžu):
						if (deleteVariable) {
							// Nejprve smažu třídu ze souibour, protože pak už nebude existovat okaz,k abych zjistil název
							// třídy:
							deleteClassFromSrcDirectory(selectedObject.toString());

							// ZDe do kolekce pro smazání přídám i označenou třídu:
							listForDelete.add((DefaultGraphCell) selectedObject);
							// Nyní již mohu vymyzat ojekty z grafu: listForDelete.forEach(cell ->
							// classDiagram.deleteObjectFromGraph(cell));
							listForDelete.forEach(
									cell -> classDiagram.deleteObjectFromGraph(cell, GraphClass.getCellsList()));
						}
					}
				}

			}


            /*
             * Zde by bylo možné aktalizovat digram tříd a diagram instancí, le to záleží na tom, jestli se odebrala
             * například třída
             * nebo nějaký vztah (atribut).
             *
             * Dále dle toho, co se odebralo by se mohlo řešit, zda se mají odebrat i veškeré instance v případě, že
             * byla odebrána třída.
             *
             * V případě odebraného atributu je otázka, zda se mají rovnou aktualizovat veškeré instance a odebrat z
             * nich ten atribut.
             *
             * Proto se zde aktualizují vztahy, tedy odebre třída nebo atribut, ale do instancí se již nebude nijak
             * zasahovat. Nechám na uživateli, aby si aktualizoval diagram tříd a poté diagram instancí "sám", aby se
             * změny projevily.
             */


            /*
             * Zde se smazal něaký objekt z diagramu tříd, i když to mohl být komentář, pak je to jedno, ale v
             * případě, že se
             * smazal nějaký vztah, tedy proměnná / proměnné z nějaké třídy / tříd, nebo třída samotná apod. Pak je
             * třeba aktualizovat
             * hodnoty v okně pro automatické doplňování příkazů do editoru příkazů, aby se v tom okně nenabízely již
             * neexistující
             * proměnné, metody, třídy apod.
             */
            App.getCommandEditor().launchThreadForUpdateValueCompletion();
        }
		else
			JOptionPane.showMessageDialog(classDiagram, jopSelectedObjErrorText, jopSelectedObjErrorTitle,
					JOptionPane.ERROR_MESSAGE);
	}


	
	
	
	


	
	
	
	
	
	
	
	
	
	/**
	 * Tato metoda se zavolá pouze jednou při spuštění aplikace. A jedniný úkol této
	 * metody je že se zkontroluje, zda se v adresáři workspace/configuration
	 * vyskytují veškeré koknfigurační soubory, resp. soubory, (.properties), které
	 * obsahují některá nastavení pro tuto aplikaci, která může uživatel ovlivnit.
	 * 
	 * Dále je ještě potřeba v případě že příslušný konfigurační soubor existuje,
	 * tak musím otestovat, zda jsou zadány pouze validní hodnoty, a zda jsou
	 * uvedeny veškeré potřebné hodnoty, pokud ne, pak je třeba ten soubor vytvořit
	 * znovu s výchozím nastavením, protože po dobu, co byla aplikace ukončena mohl
	 * uživatel manipulovat s konfiguračními soubory a při spuštění by to aplikace
	 * netestovala, a mohlo by tedy dojít k potenciální chybě například při načítání
	 * nějaké hodnoty apod.
	 * 
	 * 
	 * Note:
	 * 
	 * Zde by bylo možné testovat i soubory z jazyky, ale to je již pořešené jinde,
	 * tato metoda má za úkol zkontrolovat a případně vytvořit pouze ty konfigurační
	 * soubory (pokud neexistují, nebo obsahují chyby či duplicitní nastavení).
	 * 
	 * Zavolá se tedy pouze jednou chvilku po spuštění aplikace ve chvíli, kdy se
	 * zná adresář workspace.
	 * 
	 * 
	 * Note:
	 * 
	 * Bylo by možné tuto metodu zavolat ještě při změně adresáře workspace
	 * (například). Ale to jsem zavrhl, protože je třeba aplikaci stejně
	 * restartovat, protože jinak se žádné změny neprojeví a při spuštění aplikce se
	 * tato metoda zvolá a zkontroluje, zda se v tom novém adresáři
	 * workspace/configuration ty konfigurační soubory vyskytují, případně je
	 * vytvoří.
	 * 
	 * 
	 * Note:
	 * 
	 * Původně jsem chtěl psát tuto metodu jako vlákno, ale to mi nevyšlo, protože
	 * občas mohlo trvat trochu déle, než se všechny ty soubory zkontrolovaly a
	 * během té doby si je již aplikace stihla načíst se špatnými údaji (když jsem
	 * je změnil přes spuštěním), proto jsem to místo vlákna napsal jen na tuto
	 * metodu a až se provede, pak se může pokračovat
	 */
	public static void checkConfigFiles() {
		/*
		 * Zde si musím vytvořit instanci té třídy ControlClass, protože když to
		 * neudělám, tak v případě, že se nebudou nacházet chyby v konfiguračním souboru
		 * pro diagram tříd nebo diagram instancí, pak se v těchto souborech budou
		 * testovat duplicity, ale k tomu se musí vytvořit ještě jednáa instance jedné
		 * třídy, která potřebuje v parametru konsturktoru texty, ale ty potřeba
		 * nebudou, protože se uživateli stejně nic zobrazovat v tomto vlákně nebude,
		 * tak mohu předat null.
		 */
		new ControlClass(null);
		
		/*
		 * Získám si cestu k adresáři workspace.
		 */
		final String pathToWorkspace = ReadFile.getPathToWorkspace();
		
		/*
		 * pokud adresář workspace nebyl nalezen, pak nemá smysl pokračovat.
		 */
		if (pathToWorkspace == null)
			return;
		
		
		
		/*
		 * Zde si získám cestu k adresáři configuration ve workspace:
		 */
		final String pathToConfigDir = pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME;
		
		/*
		 * V případě, že adresář configuration neexistuje, tak jej vytvořím, ale tato
		 * podmínka by měla být zbytečná.
		 */
		if (!ReadFile.existsDirectory(pathToConfigDir))
			// Zde neexistuje configuration, tak ho nakopíruji:
			createConfigureFilesInPath(pathToWorkspace);
		
		
		
		
		/*
		 * Tato podmínka by měla být také zbytečná, ale pro případ, že by opravdu došlo
		 * k nějaké mně neznámé chybě, pak nemohu pokračovat, pokud neexistuje adresář
		 * configuration, protože by se neměli kde nacházet konfigurační soubory.
		 */
		if (!ReadFile.existsDirectory(pathToConfigDir))
			return;
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		/*
		 * Zde už následuje pouze to, že otestuji, zda existují jednotlivé (pouze)
		 * konfigurační soubory pro tuto aplikaci, které obsahují nastavení pro tuto
		 * aplikaci, které může uživatel ovlivnit, pokud nějaký z těch souborů
		 * neexistuje, pak jej vytvořím, to je vše.
		 */
		
		
		
		/*
		 * Sestavím si cestu k souboru Default.properties.
		 */
		final String pathToDefaultProp = pathToConfigDir + File.separator + Constants.DEFAULT_PROPERTIES_NAME;
		
		/*
		 * Pokud soubor Default.properties neexistuje, pak jej vytvořím:
		 */
		if (!ReadFile.existsFile(pathToDefaultProp))
			ConfigurationFiles.writeDefaultPropertiesAllNew(pathToDefaultProp);
		
		/*
		 * Zde již ten soubor existuje, tak potřebuji pouze otestovat, zda obsahuje
		 * veškeré validní hodnoty, protože po dobu, co byla aplikace ukončená mohl
		 * uživatel manipulovat s konfiguračními soubory a pokud úmyslně někde zadal
		 * nějaké nevalidní hodnoty, mohlo by to aplikaci shodit, což nemohu dopustit,
		 * tak pokud opravdu bude nalezena nějaká nevalidní hodnota, pak ten soubor
		 * vytvořím znovu s výchozím nastavením.
		 */
		else
			ControlClass.createdModifiedFile(Constants.DEFAULT_PROPERTIES_NAME, false);
		
		
		
		
		
		
		
		
		/*
		 * Sestavím si cestu k souboru ClassDiagram.properties.
		 */
		final String pathToClassDiagramProp = pathToConfigDir + File.separator + Constants.CLASS_DIAGRAM_NAME;

		/*
		 * Pokud soubor ClassDiagram.properties neexistuje, pak jej vytvořím:
		 */
		if (!ReadFile.existsFile(pathToClassDiagramProp))
			ConfigurationFiles.writeClassDiagramProperties(pathToClassDiagramProp);
		
		/*
		 * Zde již ten soubor existuje, tak potřebuji pouze otestovat, zda obsahuje
		 * veškeré validní hodnoty, protože po dobu, co byla aplikace ukončená mohl
		 * uživatel manipulovat s konfiguračními soubory a pokud úmyslně někde zadal
		 * nějaké nevalidní hodnoty, mohlo by to aplikaci shodit, což nemohu dopustit,
		 * tak pokud opravdu bude nalezena nějaká nevalidní hodnota, pak ten soubor
		 * vytvořím znovu s výchozím nastavením.
		 */
		else
			ControlClass.createdModifiedFile(Constants.CLASS_DIAGRAM_NAME, false);
		
		
		
		
		
		
		
		
		
		/*
		 * Sestavím si cestu k souboru InstanceDiagram.properties.
		 */
		final String pathToInstanceDiagramProp = pathToConfigDir + File.separator + Constants.INSTANCE_DIAGRAM_NAME;

		/*
		 * Pokud soubor InstanceDiagram.properties neexistuje, pak jej vytvořím:
		 */
		if (!ReadFile.existsFile(pathToInstanceDiagramProp))
			ConfigurationFiles.writeInstanceDiagramProperties(pathToInstanceDiagramProp);
		
		/*
		 * Zde již ten soubor existuje, tak potřebuji pouze otestovat, zda obsahuje
		 * veškeré validní hodnoty, protože po dobu, co byla aplikace ukončená mohl
		 * uživatel manipulovat s konfiguračními soubory a pokud úmyslně někde zadal
		 * nějaké nevalidní hodnoty, mohlo by to aplikaci shodit, což nemohu dopustit,
		 * tak pokud opravdu bude nalezena nějaká nevalidní hodnota, pak ten soubor
		 * vytvořím znovu s výchozím nastavením.
		 */
		else
			ControlClass.createdModifiedFile(Constants.INSTANCE_DIAGRAM_NAME, false);
		
		
		
		
		
		
		
		
		
		/*
		 * Sestavím si cestu k souboru CommandEditor.properties.
		 */
		final String pathToCommandEditorProp = pathToConfigDir + File.separator + Constants.COMMAND_EDITOR_NAME;

		/*
		 * Pokud soubor CommandEditor.properties neexistuje, pak jej vytvořím:
		 */
		if (!ReadFile.existsFile(pathToCommandEditorProp))
			ConfigurationFiles.writeCommandEditorProperties(pathToCommandEditorProp);
		
		/*
		 * Zde již ten soubor existuje, tak potřebuji pouze otestovat, zda obsahuje
		 * veškeré validní hodnoty, protože po dobu, co byla aplikace ukončená mohl
		 * uživatel manipulovat s konfiguračními soubory a pokud úmyslně někde zadal
		 * nějaké nevalidní hodnoty, mohlo by to aplikaci shodit, což nemohu dopustit,
		 * tak pokud opravdu bude nalezena nějaká nevalidní hodnota, pak ten soubor
		 * vytvořím znovu s výchozím nastavením.
		 */
		else
			ControlClass.createdModifiedFile(Constants.COMMAND_EDITOR_NAME, false);
		
		
		
		
		
		
		
		
		
		
		/*
		 * Sestavím si cestu k souboru CodeEditor.properties.
		 */
		final String pathToCodeEditorProp = pathToConfigDir + File.separator + Constants.CODE_EDITOR_NAME;

		/*
		 * Pokud soubor CodeEditor.properties neexistuje, pak jej vytvořím:
		 */
		if (!ReadFile.existsFile(pathToCodeEditorProp))
			ConfigurationFiles.writeCodeEditorProperties(pathToCodeEditorProp);
		
		/*
		 * Zde již ten soubor existuje, tak potřebuji pouze otestovat, zda obsahuje
		 * veškeré validní hodnoty, protože po dobu, co byla aplikace ukončená mohl
		 * uživatel manipulovat s konfiguračními soubory a pokud úmyslně někde zadal
		 * nějaké nevalidní hodnoty, mohlo by to aplikaci shodit, což nemohu dopustit,
		 * tak pokud opravdu bude nalezena nějaká nevalidní hodnota, pak ten soubor
		 * vytvořím znovu s výchozím nastavením.
		 */
		else
			ControlClass.createdModifiedFile(Constants.CODE_EDITOR_NAME, false);
		
		
		
		
		
		
		
		
		
		/*
		 * Sestavím si cestu k souboru OutputEditor.properties.
		 */
		final String pathToOutputEditorProp = pathToConfigDir + File.separator + Constants.OUTPUT_EDITOR_NAME;

		/*
		 * Pokud soubor OutputEditor.properties neexistuje, pak jej vytvořím:
		 */
		if (!ReadFile.existsFile(pathToOutputEditorProp))
			ConfigurationFiles.writeOutputEditorProperties(pathToOutputEditorProp);
		
		/*
		 * Zde již ten soubor existuje, tak potřebuji pouze otestovat, zda obsahuje
		 * veškeré validní hodnoty, protože po dobu, co byla aplikace ukončená mohl
		 * uživatel manipulovat s konfiguračními soubory a pokud úmyslně někde zadal
		 * nějaké nevalidní hodnoty, mohlo by to aplikaci shodit, což nemohu dopustit,
		 * tak pokud opravdu bude nalezena nějaká nevalidní hodnota, pak ten soubor
		 * vytvořím znovu s výchozím nastavením.
		 */
		else
			ControlClass.createdModifiedFile(Constants.OUTPUT_EDITOR_NAME, false);
		
		
		
		
		
		
		
		
		
		/*
		 * Sestavím si cestu k souboru OutputFrame.properties.
		 */
		final String pathToOutputFrameProp = pathToConfigDir + File.separator + Constants.OUTPUT_FRAME_NAME;

		/*
		 * Pokud soubor OutputFrame.properties neexistuje, pak jej vytvořím:
		 */
		if (!ReadFile.existsFile(pathToOutputFrameProp))
			ConfigurationFiles.writeOutputFrameProperties(pathToOutputFrameProp);

		/*
		 * Zde již ten soubor existuje, tak potřebuji pouze otestovat, zda obsahuje
		 * veškeré validní hodnoty, protože po dobu, co byla aplikace ukončená mohl
		 * uživatel manipulovat s konfiguračními soubory a pokud úmyslně někde zadal
		 * nějaké nevalidní hodnoty, mohlo by to aplikaci shodit, což nemohu dopustit,
		 * tak pokud opravdu bude nalezena nějaká nevalidní hodnota, pak ten soubor
		 * vytvořím znovu s výchozím nastavením.
		 */
		else
			ControlClass.createdModifiedFile(Constants.OUTPUT_FRAME_NAME, false);
		
		
		
		
		
		
		
		
		
		
		/*
		 * Sestavím si cestu k souboru CodeEditorInternalFrame.properties.
		 */
		final String pathToCodeEditorInternalFramesProp = pathToConfigDir + File.separator
				+ Constants.CODE_EDITOR_INTERNAL_FRAMES_NAME;

		/*
		 * Pokud soubor CodeEditorInternalFrame.properties neexistuje, pak jej vytvořím:
		 */
		if (!ReadFile.existsFile(pathToCodeEditorInternalFramesProp))
			ConfigurationFiles.writeCodeEditorPropertiesForInternalFrames(pathToCodeEditorInternalFramesProp);

		/*
		 * Zde již ten soubor existuje, tak potřebuji pouze otestovat, zda obsahuje
		 * veškeré validní hodnoty, protože po dobu, co byla aplikace ukončená mohl
		 * uživatel manipulovat s konfiguračními soubory a pokud úmyslně někde zadal
		 * nějaké nevalidní hodnoty, mohlo by to aplikaci shodit, což nemohu dopustit,
		 * tak pokud opravdu bude nalezena nějaká nevalidní hodnota, pak ten soubor
		 * vytvořím znovu s výchozím nastavením.
		 */
		else
			ControlClass.createdModifiedFile(Constants.CODE_EDITOR_INTERNAL_FRAMES_NAME, false);
	}







	/**
	 * Metoda, která do souboru .properties na cestě path vloží na začátek toho souboru "dokumentační" komentář ohledně
	 * daného souboru (nějaká informace k danému souboru .properties) a pod něj datum vytvoření v definovaném formátu
	 * (převzato z Properties).
	 *
	 * @param path
	 *         - cesta k souboru .properties, kam se má vložit komentář a datum.
	 * @param comment
	 *         - komentář, který se má vložit na začátek souboru.
	 * @param locale
	 *         - "Geografická pozice", konkrétní využití je pro sestavení formátu datumu vytvoření dle zadané
	 *         lokalizace, toto je tak trochu "zbytečné", jemožné využít vždy výchozí Locale dle JVM, tak to je také
	 *         nastaveno jako výchozí nastavení pro tuto aplikaci, ale pro někoho je třeba lepší, když je to pro
	 *         nějakou
	 *         jinou pozici, například Čína apod.
	 */
	public static void insertCreationComments(final String path, final String comment, final Locale locale) {
		/*
		 * Formát, v jakém se budou zapisovat datumy vytvoření pro daný soubor, tento
		 * formát jsem převzal z objektu Properties, tecy formát jsem sám napsal, ale
		 * snažil jsem se, aby byl stejný, jako v objektu Properties, snad jsem uspěl.
		 */
		final SimpleDateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy", locale);
		
		/*
		 * Postup:
		 * - Nejprve si načtu všechny řádky - texty ze souboru na path.
		 * - Pak zapíšu na první řádek komentář - pokud není null nebo prázdný.
		 * - Pak zapíšu datum vytvoření, resp. aktuální datum.
		 * - Pak zapíšeu zpět načtené řádky v listu v prvním kroku.
		 */
		
		
		/*
		 * List, do kterého si načtu všechny řádky ze souboru na cestě path.
		 */
		final List<String> rowsFromFile = new ArrayList<>();

		// Načtení všech řádků - veškerého textu ze souboru:
		try (final BufferedReader reader = new BufferedReader(new InputStreamReader(
				new FileInputStream(path), StandardCharsets.UTF_8))) {

			String row;

			while ((row = reader.readLine()) != null)
				rowsFromFile.add(row);

		} catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při čtení dat ze souboru: " + path
                        + ". Metoda insertCreationComments v balíčku file.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
		
		
		
		
		
		
		
		
		
		
		
		/*
		 * Nyní zapíšu veškeré načtené řádky zpět do souboru, akorát nejprve zapíšu
		 * první dva řádky -> první bude komentář a jako druhý bude datum vytvoření,
		 * resp. aktuální datum.
		 */
		try (final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path),
				StandardCharsets.UTF_8))) {

			// Pokud je zadán komentář, pak jej zapíšu, jinak pokračuji datumem vytvoření:
			if (comment != null && !comment.replaceAll("\\s", "").isEmpty()) {
				writer.write("#" + comment);
				writer.newLine();
			}

			// Datum vytvoření se zapíše pokaždé:
			writer.write("#" + formatter.format(new Date()));
			writer.newLine();

			// Nyní zapíšu i zbytek textů, které jsem dříve načetl:
			rowsFromFile.forEach(r -> {
				try {

                    writer.write(r);
                    writer.newLine();

                } catch (IOException e) {
                    /*
                     * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                     * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                     * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                     * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                     */
                    final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                    // Otestuji, zda se podařilo načíst logger:
                    if (logger != null) {
                        // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                        // Nejprve mou informaci o chybě:
                        logger.log(Level.INFO,
                                "Zachycena výjimka při zapisování dat do souboru: " + path
                                        + ". Konkrétně při zapísování řádku: " + r
                                        + ". Došlo k chybě nebo při vytvoření nového - prázdného řádku.");

                        /*
                         * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                         * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                         * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                         */
                        logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                    }
                    /*
                     * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                     */
                    ExceptionLogger.closeFileHandler();
                }
			});

            writer.flush();
				
		} catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při vytváření objektu FileWriter k souboru: " + path
                        + ". Může se například jednat o to, že daný soubor neexistuje, nelze vytvořit, je to adresář " +
                        "apod.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
	}
	
	
	
	
	

	
	
	@Override
    public void writeStartAppInfoToLog() {
        // Zapíšu do logu, resp. do příslušného souboru oznámení coby
        // spuštění aplikace, i když se jedná o název souboru, tak toto
        // až tak není potřebna:
        final Logger logger = ExceptionLogger.getLogger(ExtractEnum.LAUNCH_APP);

        if (logger != null) {
            // Sestavím si cestu k projektu nebo k aplikaci:
            final String path = getPathToProjectApp();

            // Text, který se zapíše do logu:
            final String textForLog = Constants.APP_TITLE + " launched by " + UserNameSupport.getUsedUserName() + " on " +
                    "the " + path;

            logger.log(Level.INFO, textForLog);

            ExceptionLogger.closeFileHandler();
        }
    }





    /**
     * Metoda, která sestaví cestu k umístění aplikace (.jar souboru) nebo adresáři, kde byl projekt spuštěn. Tento
     * případ nastane v případě, že je spuštěna aplikace například v prostředí IntelliJ Idea, Eclipse apod. - Nejedná se
     * o spuštění vygenerované aplikace.
     *
     * @return cestu k souboru .jar - spuštěné aplikaci nebo adresáři, kde byla aplikace spuštěna.
     */
    private static String getPathToProjectApp() {
        try {

            final File file = new File(WriteToFile.class.getProtectionDomain().getCodeSource().getLocation().toURI());

            return createAppNameForLog(file);

        } catch (URISyntaxException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při vytváření proměnné typu File, která by měla obsahovat " +
                        "cestu k umístění aplikace. Buď se jedná o umístění spuštěného .jar souboru nebo adresáře, " +
                        "kde byl " +
                        "spuštěn projekt. Tato výjimka může nastat v případě, že cesta není správně naformátovaná dle" +
                        " RFC2396 a nemůže tak být převedena do URI.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }

        // Jiný způsob, který vrátí cestu k aplikace - místu spuštění, popřípadě projektu (edná se pouze o "náhradní
        // / záložní" výpis v případě, že by výše nastala výjimka).
        final File file = new File(WriteToFile.class.getProtectionDomain().getCodeSource().getLocation().getFile());

        return createAppNameForLog(file);
    }




    /**
     * Metoda, která sestaví cestu k adresáři nebo jar souboru, kde byla aplikace spuštěna.
     * <p>
     * Sestaví cesty probíhá tak, že se zjistí, zda je file adresář, pokud ano, aplikace byla spuštěna například v
     * prostředí IntelliJ Idea - v nějakém prostředí, pak se vrátí cesta k tomuto adresáři, kde byla aplikace spuštěna.
     * Pokud se nejedná o adresář, pak se jedná o .jar soubor. V takovém případě byla spuštěna vygenerovaná aplikace a
     * vrátí se cesta k tomuto .jar souboru.
     *
     * @param file
     *         - cesta k adresář, kde byla spuštěna aplikace nebo k .jar souboru - spuštěné aplikaci.
     * @return výše zmíněnou cestu k .jar souboru nebo adresáři, kde byla spuštěna aplikace. Syntaxe je taková, že se
     * vrátí buď celá cesta k .jar souboru nebo v případě adresáře bude vrácena cesta k adresáři a za posledním lomítkem
     * bude text: "the project was launch", který "informuje" uživatele, že byla apuštěna aplikace v nějakém prostředí -
     * nejedná se o vygenerovanou aplikaci.
     */
    private static String createAppNameForLog(final File file) {
        return file.isDirectory() ? file.getAbsolutePath() + File.separator + "the project was launched" : file
                .getAbsolutePath();
    }
	
	
	
	
	
	
	
	@Override
    public void writeExitAppInfoToLog() {
        // Zapíšu do příslušného souboru v logs v configuration ve Workspace,
        // ukončení aplikace - toto už není potřebna, ale pro přehlednost:
        final Logger logger = ExceptionLogger.getLogger(ExtractEnum.TERMINATE_APP);

        if (logger != null) {
            logger.log(Level.INFO, Constants.APP_TITLE + " terminated");
            ExceptionLogger.closeFileHandler();
        }
    }
}