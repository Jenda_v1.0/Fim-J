package cz.uhk.fim.fimj.file;

import java.io.File;
import java.util.List;
import java.util.Properties;

import org.jgraph.graph.DefaultGraphCell;

import cz.uhk.fim.fimj.language.EditProperties;

/**
 * Toto rozhraní slouží pro deklaraci metod, které slouží pro načítání souborů z disku - mimo aplikace, dále pro
 * kompilaci tříd z diagramu tříd, dáel načtení příslušné zkompilované - přeložené třídy, ... viz jednotlivé metody.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface ReadFileInterface {

    /**
     * Metoda, která vrátí objekt EditProperties, který obsahuje načtený konfigurační soubor typu '.properties', nebo
     * null, pokud nebude uvedený soubor nalezen.
     * <p>
     * Příslušný soubor typu .properties se hledá v adresáři označený jako workspace v jeho podadresáři configration.
     *
     * @return výše popsanou instanci třídy EditProperties, která obsahuje naštený soubor typu .properties nebo null,
     * pokud nebude soubor ve workspace nalezen.
     */
    EditProperties getPropertiesInWorkspacePersistComments(final String fileName);


    /**
     * Metoda, která vrátí vybrány jazyk Nastavený jazyk pro aplikace se nachází ve výchozím souboru
     * 'Default.properties', takže ho přečtě, zjistí si nastavený jazyk, a pak ze složky language načte soubor
     * .properties s příslušným jazykem a ten se dále nastaví v aplikaci (- to už neřeší tato metoda)
     *
     * @return vybraný jazyk v podobě jazyk.properties vrátí např: CZ, PL, ... a tímto názvem jsou pojmenévány soubory s
     * jazyky .properties
     */
    Properties getSelectLanguage();


    /**
     * Metoda, načte příslušný soubor typu .properties na uvedené cestě, pokud ho nenajde, vrátí null
     *
     * @param path
     *         - cesta k souboru type. properties, který chci "načíst"
     *
     * @return null, pokud soubor nebude nalezen, jinak příslušný soubor typu .properties na uvedené cestě
     */
    Properties getProperties(final String path);


    /**
     * Metoda, která vrátí soubor typu Properties = ClassDiagram.properties vrátí soubor s nastavením pro graf z
     * Workspace, pokud tento soubor ve Wrokspace nebude, vrátí ten z adresáře aplikace - výchozí nastavení
     * <p>
     * Postup: Zjistím si cestu do Workspace, pokud Workspace neexistuje, použiji výchozí nastavení Zjistím, zda
     * existuje configuration - adresář
     *
     * @return ClassDiagram.properties = výchozí nastavení pro class diagram
     */
    Properties getClassDiagramProperties();

    /**
     * Metoda, která vrátí InstanceDiagram.properties = soubor s nastavením pro graf s instancemi Metoda se pokusí
     * vrátit tento soubor z Workspace, pokud nebude nalezen vrátí ten s adresáře aplikace, pokud ani ten nenajde,
     * budeou použity hodnoty z constants
     * <p>
     * Postup: zjistím si cestu do Workspace z souboru v adresári aplikace, tam jestli existuje configuration a v nem
     * instance diagram.properties, jinak: Pokud neexistuje, pouziji výchozí souboru
     *
     * @return InstanceDiagram.properties z Workspace nebo z adresáře aplikace
     */
    Properties getInstanceDiagramProperties();


    /**
     * Metoda, která vrátí cestu k adresáři projektu ve složce coby Workspace nebo null
     *
     * @return null nebo cestu k adresáři src v projektu
     */
    String getPathToSrc();


    /**
     * Metoda, která se pokusí najít a vrátit cestu k adresáři bin, v aktuálně otevreném projektu, nebo null.
     *
     * @return null, pokud nebude adresář nalezen, jinak cestu k adresáři bin v adresáři otevřeného projektu
     */
    String getPathToBin();


    /**
     * Metoda, která načte ze souboru soubor s vlastnostmi a objekty do grafu - class diagramu
     *
     * @param path
     *         - cesta k souboru
     *
     * @return vrátí list objektů do grafu - s vlastnosmi
     */
    List<DefaultGraphCell> openClassDiagramProperties(final String path);


    /**
     * Metoda, která vrátí soubor ClassDiagram.properties z Workspace, pokud nebude nalezen tak vrátí null Metoda se
     * používá v ClassDiagramPanelu pro uložení změněných hodnot v nastavení Ukládat změny chci pouze do Workspace a ne
     * do výchozího adresáře do daného soubouru, ten je pouze pro výchozí nastavení
     *
     * @return ClassDiagram.properties z adresáře configuration ve Workspace jinak null, pokud nebude nalezen
     */
    Properties getClassDiagramFromWorkspaceOnly();


    /**
     * Metoda, která vrátí soubor typu Properties - InstanceDiagram.properties z Workspace jinak null
     *
     * @return InstanceDiagram.properties nebo null - pokud nebude nalezen
     */
    Properties getInstanceDiagramFromWorkspaceOnly();


    /**
     * Metoda, která se pokusí načíst soubor CommandEditor.properties z Workspace - pokud existuje pokud nenajde vrátí
     * ten z aplikace - pokud existuje, jinak prátí null
     *
     * @return CommandEditor.properties z workspace nebo adresáře aplikace nebo null
     */
    Properties getCommandEditorProperties();


    /**
     * Metoda, která se pokudí najít soubor CommandEditor.propertis ve složce configuration ve Workspace pokud ho
     * nenajde, vrátí null
     *
     * @return soubor CommandEditor.properties nebo null
     */
    Properties getCommandEditorPropertiesFromWorkspaceOnly();


    /**
     * Metoda, která se pokusí najít soubor CodeEditor.properteis ve workspace, pokud ho tamnenajde zkusí vrátit ten z
     * adresáře aplikace a když ani ten nenajde, vrátí null
     *
     * @return soubor CodeEditor.propertes typu properties nebo null
     */
    Properties getCodeEditorProperties();


    /**
     * Metoda, která se pokusí najít soubor CodeEditor.properties ve workspace, a pokud ho tam nenajde vrátí null
     *
     * @return soubor CodeEditor.properties typu Propeties nebo null
     */
    Properties getCodeEditorPropertiesFromWorkspaceOnly();


    /**
     * Metoda, která se pokusí načíst soubor CodeEditorInternalFrame.properties z adresáře configuration ve Workspace a
     * pokud ho nenajde zkusí ho najít v adresáři configuration v adresáři aplikace.
     * <p>
     * Jedná se o soubor, který obsahuje informace s hodnotami pro editor kodu - textu pro interní okna v dialogu pro
     * průzkumík projektů
     *
     * @return null pokud se výše zmíněné soubory nenajdou, jinak soubor CodeEditorInternalFrame.properties načtený buď
     * z workspace nebo z adresáře aplikace
     */
    Properties getCodeEditorPropertiesForInternalFrames();


    /**
     * Metoda, která zkusí získat soubor CodeEditorInternalFrame.properties z adresáře configuration pouze ze složky
     * označené jako Workspace, buď se najde a vrátí, nebo se vrátí null
     *
     * @return soubor CodeEditorInternalFrame.properties z adresáře configuration ve Workspace, nebo null, pokud nebude
     * nalezen
     */
    Properties getCodeEditorPropertiesForInternalFramesFromWorkspaceOnly();


    /**
     * Metoda, která se pokusí najít soubor OutputEditor.properties ve Workspace, pokud neuspěje zkusí najít ten v
     * adresáři aplikace v configuration, pokud ani ten nenajde, vrátí null
     *
     * @return OutputEditor.propeties nebo null
     */
    Properties getOutputEditorProperties();


    /**
     * Metoda, která se pokusí najít ve workspace v configuration soubor OutputEditor.properties, a vrátí ho v typu
     * Properties, pokud ho tam nenajde vrátí null
     *
     * @return null nebo OutputEditor.properties
     */
    Properties getOutputEditorPropertiesFromWorkspaceOnly();


    /**
     * Metoda, která se pokusí najít soubor OutputFrame.properties ve Workspace, a pokud ho tam nenajde, tak zkusí najít
     * ten v adresáři aplikace v adresáři configuration a pokud ani ten nenajde, tak vrátí null, jinak vrátí jeden z
     * nalezených souborů
     *
     * @return null nebo OutputFrame.properties
     */
    Properties getOutputFrameProperties();


    /**
     * Metoda, která zkusí najít ve workspace v adresáři configuration soubor OutputFrame.properties a vrátí ho, pokud
     * honenajde, tak vrátí null
     *
     * @return null nebo soubor OutputFrame.properties ve Workspace - jen zde, ne z aplikace
     */
    Properties getOutputFramePropertiesFromWorkspaceOnly();


    /**
     * Metoda, která vrátí soubory typu Class z souboru .java, což bude třída v balíčku src Metoda, která vrátí
     * zkompilovanou třídu, která je potřeba, případně zkompiluje všechny třídy v diagramu tříd
     * <p>
     * Metoda si zjistí cestu k adresáři src, pak si vytvoří instanci URLClassLoaderu a načte třídu v parameteu
     * <p>
     * <p>
     * Metoda nejprve zkompiluje zadanou třídu a pokud se to nepovede, je možné, že daná třída má nějaké vztahy i s
     * ostatními třídami v diagramu tříd, proto se zkompilují všechny a oznámí se výsledek
     *
     * @param cellTextWithDot
     *         - název třídy z buňky s tečkami - i s balíčky prostě jen celý název z buňky v grafu, která reprezentuje
     *         třídu
     * @param outputEditor
     *         = editro výstupů, do které se vypíšou výsledky o kompilaci třídy tato reference je potřeba, protže abych
     *         získat soubor typu .Class, který je potřeba, tak musím danou třídu nejprve zkompilovat, aby se vytvořil
     *         soubor .Class, který pak "získám" no a po kompilaci třídy se vypíše hlášení, o stavu kompilace - úspqšně
     *         nebo výpis chyb
     * @param refreshInstanceDiagram
     *         - logická proměnná o tom, zda se mají při zkompilování tříd v diagramu tříd i přenačíst vztahy mezi
     *         instancemi v diagramu instancí. Tato proměnná se občas hodí, když se třída například kompiluje na pozadí
     *         v editorech zdrojového kódu, tak asi nemá smysl tyto vztahy přenačítat, když uživatel píše kód, ale když
     *         se například načítá nějaká třída, tak se vytvoří i nový classlodeer pro aktuální třídy, pak se přenačtou
     *         instance, tak se hodí přenačíst i vztahy, zda je vše, jak má být, pokud ne, tak aby to bylo alespoň
     *         zobrazeno.
     * @param updateCommandEditorValueCompletion
     *         - zda se mají aktualizovat hodnoty v okně pro automatické doplnění kódu / příkazů do editoru příkazů.
     *
     * @return Class z zadané třídy výše, nebo null - pokud nastane chyba
     */
    Class<?> getClassFromFile(final String cellTextWithDot, final OutputEditorInterface outputEditor,
                              final List<DefaultGraphCell> filesList, final boolean refreshInstanceDiagram,
                              final boolean updateCommandEditorValueCompletion);


    /**
     * Metoda, která se pokusí načíst přeložený - zkompilovaný soubor .class nějaké třídy
     * <p>
     * Note: Původně jsem chtěl tuto metodu napsat "na jiném principu" s tím, že by se zadávala pouze cesta k dané
     * třídě, kterou chci načíst, jenže ClassLoader, který používám pro načtení třídy vyžaduje cestu ke zkompilované
     * třídě !včetěn balíčků!, proto jsem tuto metodu dále napříkald nepřetížíl s parametry coby pouze cesta k souboru,
     * apod. Potřebuje vždy balíčky, kde se daná třída (přeložená - zkompilovaná) má hledat - oddělené na balíčky a
     * "hlavní adresář", v mém případě se jedná o adresář src nebo bin a dále následujíc zmíněné balíčky, kde se třída
     * nachází
     *
     * @param cellTextWithDot
     *         - cesta k dané třídě, resp. soubor typu .class
     * @param outputEditor
     *         - reference na editor výstupů, aby se mohli případně vypsat chybové zprávy uživateli, kdyby došlo k
     *         nějaké chybě
     *
     * @return null nebo načtený soubor .class zkompilované třídy
     */
    Class<?> loadCompiledClass(final String cellTextWithDot, final OutputEditorInterface outputEditor);


    /**
     * Metoda, která vytvoří z tříd v diagramu tříd proměnné typu File, které budou obsahovat cestu k dané třídě v
     * diagramu tříd na disku.
     * <p>
     * Metoda projde cyklem všechny objekty v diagramu tříd, otestuje, zda se nejedná o hranu a pokud ne zkusi z daného
     * textu zjistit, zda se jedná o třídu, tj. zda existueje příslušný soubor na disku.
     *
     * @param pathToSrc
     *         - cesta k adresáři src
     * @param cellsList
     *         - list - kolekce s obekty v diagramu tříd
     * @param outputEditor
     *         - reference na istnacei třídy outpuEditor, aby mohla metoda případně vypsat hlášku, pokud nebude nějaký
     *         soubor existovat
     * @param languageProperties
     *         - reference na proměnnou typu Properties, která obsahuje texty ve zvoleném jazyce
     *
     * @return List - kolekci typu File s příslušnými soubory typu File coby cesty ke třídám v diagramu tříd
     */
    List<File> getFileFromCells(final String pathToSrc, final List<DefaultGraphCell> cellsList,
                                final OutputEditorInterface outputEditor, final Properties languageProperties);
}