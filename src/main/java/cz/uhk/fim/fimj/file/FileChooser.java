package cz.uhk.fim.fimj.file;

import java.io.File;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;

/**
 * Tato třída slouží pro deklaraci metod, pomocí kterých je možnné si vybrat nějaký soubor či adresář, ať už pro
 * otevření uložení, apod. viz konkrétní metody
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class FileChooser extends JFileChooser implements FileChooserInterface {

	private static final long serialVersionUID = 1L;
	
	
	// Proměnné pro texty do titulků a chybových hlášek:
	private static String fileErrorTitle, fileErrorText, saveImageTitle, newProjectTitle, chooseProjectDirectory,
			chooseClassTitle, chooseCurrencyData, wrongFileTitle, wrongFileText, chooseToolsJar, chooseFlavormapProp,
			chooseJavaHomeDir, txtSelectWorkspace, txtChooseFile, txtChooseLocationAndKindOfFile;
	
	
	
	public FileChooser() {
		super(System.getProperty("user.home"));		
		
		// Zvolím jazyk pro hlášky, texty, ...
		final Properties languageProperties = App.READ_FILE.getSelectLanguage();
		
		
		if (languageProperties != null) {
			fileErrorTitle = languageProperties.getProperty("FcFileErrorTitle", Constants.FC_FILE_ERROR_TITLE);
			fileErrorText = languageProperties.getProperty("FcFileErrorText", Constants.FC_FILE_ERROR_TEXT);
			saveImageTitle = languageProperties.getProperty("FcSaveImageTitle", Constants.FC_SAVE_IMAGE_TITLE);
			newProjectTitle = languageProperties.getProperty("FcNewProjectTitle", Constants.FC_NEW_PROJECT_TITLE);
			chooseProjectDirectory = languageProperties.getProperty("FcChooseProjectDirectory", Constants.FC_CHOOSE_PROJECT_DIR);
			chooseClassTitle = languageProperties.getProperty("FcChooseJavaClass", Constants.FC_CHOOSE_CLASS);			
			txtChooseFile = languageProperties.getProperty("FcChooseFile", Constants.FC_CHOOSE_FILE);
			txtChooseLocationAndKindOfFile = languageProperties.getProperty("FcChooseLocationAndKindOfFile", Constants.FC_CHOOSE_LOCATION_AND_KIND_OF_FILE);
			
			chooseCurrencyData = languageProperties.getProperty("Fc_ChooseCurrencyData", Constants.FC_CHOOSE_CURRENCY_DATA);		
			wrongFileTitle = languageProperties.getProperty("Fc_WrongFileTitle", Constants.FC_WRONG_FILE_TITLE);
			wrongFileText = languageProperties.getProperty("Fc_WrongFileText", Constants.FC_WRONG_FILE_TEXT);
			chooseToolsJar = languageProperties.getProperty("Fc_ChooseToolsJar", Constants.FC_CHOOSE_TOOLS_JAR);
			chooseFlavormapProp = languageProperties.getProperty("Fc_ChooseFlavormapProp", Constants.FC_CHOOSE_FLAVORMAP_PROP);
			chooseJavaHomeDir = languageProperties.getProperty("Fc_ChooseJavaHomeDir", Constants.FC_CHOOSE_JAVA_HOME_DIR);			
			txtSelectWorkspace = languageProperties.getProperty("Fc_SelectWorkspace", Constants.FC_SELECT_WORKSPACE);
		}
		
		
		else {
			fileErrorTitle = Constants.FC_FILE_ERROR_TITLE;
			fileErrorText = Constants.FC_FILE_ERROR_TEXT;			
			saveImageTitle = Constants.FC_SAVE_IMAGE_TITLE;
			newProjectTitle = Constants.FC_NEW_PROJECT_TITLE;
			chooseProjectDirectory = Constants.FC_CHOOSE_PROJECT_DIR;
			chooseClassTitle = Constants.FC_CHOOSE_CLASS;
			txtChooseFile = Constants.FC_CHOOSE_FILE;
			txtChooseLocationAndKindOfFile = Constants.FC_CHOOSE_LOCATION_AND_KIND_OF_FILE;
			
			chooseCurrencyData = Constants.FC_CHOOSE_CURRENCY_DATA;		
			wrongFileTitle = Constants.FC_WRONG_FILE_TITLE;
			wrongFileText = Constants.FC_WRONG_FILE_TEXT;
			chooseToolsJar = Constants.FC_CHOOSE_TOOLS_JAR;
			chooseFlavormapProp = Constants.FC_CHOOSE_FLAVORMAP_PROP;
			chooseJavaHomeDir = Constants.FC_CHOOSE_JAVA_HOME_DIR;
			txtSelectWorkspace = Constants.FC_SELECT_WORKSPACE;
		}
	}
	
	
	
	
	

	@Override
	public String getPathToSaveImage() {
		setDialogTitle(saveImageTitle);
		
		// Vrátím do původního nastavení: - odeberu všechny vybrané soubory
		// Pro případ, že by se tam např vyskytovaly typy souborů pro ukladani do jpg, png, apod.
		// tak tento výběr odebere a mlhu nastavit nově "podmínky"
		resetChoosableFileFilters();		
		
		// Ukládat lze pouze do souboru:
		setFileSelectionMode(FILES_ONLY);
		
		// Povolené hodnoty - na výběr:		
		setFileFilter(new FileNameExtensionFilter("*.jpg", "jpg", "jpeg"));
		setFileFilter(new FileNameExtensionFilter("*.png", "png"));
		setFileFilter(new FileNameExtensionFilter("*.gif", "gif"));
		setFileFilter(new FileNameExtensionFilter("*.raf", "raf"));
		setFileFilter(new FileNameExtensionFilter("*.bmp", "bmp"));
		
		
		if (showSaveDialog(this) == APPROVE_OPTION) {
			// Cesta k souboru:
			String fileName = getSelectedFile().getAbsolutePath();
					
			// Otestuji, zda uživatel již nezadal nějaký typ souboru - správný typ souboru:
			if (fileName.endsWith(".jpg") || fileName.endsWith("png") || fileName.endsWith("gif")
					|| fileName.endsWith("raf") || fileName.endsWith("bmp"))
				return fileName;
			
			else {// (else není třeba - pouze pro přehlednosta potenciální editaci)
				try {
					// K němu připojím zadaný název:
					final String[] extensionsArray = ((FileNameExtensionFilter) getFileFilter()).getExtensions();
					// K souboru připojít typ:
					fileName += "." + extensionsArray[0];
					
					return fileName;
				} catch (Exception e) {
					/*
					 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
					 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
					 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
					 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
					 */
					final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

					// Otestuji, zda se podařilo načíst logger:
					if (logger != null) {
						// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

						// Nejprve mou informaci o chybě:
						logger.log(Level.INFO,
								"Zachycena výjimka při získávání typu souboru, v dialogu nejspíše nebyl vybrán typ " +
										"souboru nebo něco podobného.");

						/*
						 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
						 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
						 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
						 */
						logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
					}
					/*
					 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
					 */
					ExceptionLogger.closeFileHandler();
					
					JOptionPane.showMessageDialog(this, fileErrorText, fileErrorTitle, JOptionPane.ERROR_MESSAGE);
					
				}
			}
		}
		
		return null;
	}

	
	
	
	
	@Override
	public String getPathToNewProjectDirectory(final String pathToWorkspace) {
		setCurrentDirectory(new File(pathToWorkspace));

		// titulek dialogu:
		setDialogTitle(newProjectTitle);

		// Resetuji nastavení, pro případ, že by zůstalo z předhozích metod, - nemělo by nastat
		// protože vytvořám pokaždé nové instance tohoto dialogu
		resetChoosableFileFilters();		
		
		// Pro nový projekt se vybere umístění pouoze adresářů:
		setFileSelectionMode(DIRECTORIES_ONLY);
		
		return getSelectedDir(true);
	}
	
	
	
	
	

	@Override
	public String getPathToProjectDirectory(final String defaultDir, final boolean showOpenDialog ) {
		// Nastavím výchozí cestu - pokud je uvedena:
		if (defaultDir != null)
			setCurrentDirectory(new File(defaultDir));		
		
		// titulek dialogu:
		setDialogTitle(chooseProjectDirectory);
		
		// Resetuji nastavení, pro případ, že by zůstalo z předhozích metod, - nemělo by nastat
		// protože vytvořám pokaždé nové instance tohoto dialogu
		resetChoosableFileFilters();		
		
		// Pro nový projekt se vybere umístění pouoze adresářů:
		setFileSelectionMode(DIRECTORIES_ONLY);
		
		if (showOpenDialog) {
			if (showOpenDialog(this) == APPROVE_OPTION)
				// Vrátím cestu k zvolenému adresáři:
				return getSelectedFile().getAbsolutePath();
		}
		
		
		else if (showSaveDialog(this) == APPROVE_OPTION)
				// Vrátím cestu k zvolenému adresáři:
				return getSelectedFile().getAbsolutePath();
		

		
		return null;
	}
	
	
	
	
	

	@Override
	public String getPathToClass() {
		// Pokud ji najdu, nastavím cestu do Workspacu - tam se většina tříd nejspíše bude nacházet
		
		final String pathToWorkspace = ReadFile.getPathToWorkspace();
		
		if (pathToWorkspace != null)
			setCurrentDirectory(new File(pathToWorkspace));
		
		
		
		setDialogTitle(chooseClassTitle);
		
		resetChoosableFileFilters();	
		
		setFileSelectionMode(FILES_ONLY);
		
		
		setFileFilter(new FileNameExtensionFilter("*.java", "java"));
		
		
		if (showOpenDialog(this) == APPROVE_OPTION) {
			final String choosedClass = getSelectedFile().getAbsolutePath();
			
			if (choosedClass.endsWith(".java"))
				return choosedClass;	
		}
		
		return null;
	}

	
	
	
	
	
	@Override
	public String getPathToPlaceSomeConfigurationFile(final String dialogTitle) {
		if (dialogTitle != null)
			setDialogTitle(dialogTitle);
		
		
		resetChoosableFileFilters();		
		
		setFileSelectionMode(FILES_ONLY);
		
		
		setFileFilter(new FileNameExtensionFilter("*.properties", "properties"));
		
		
		if (showSaveDialog(this) == APPROVE_OPTION) {
			final String pathToFile = getSelectedFile().getAbsolutePath();
			
			if (pathToFile.endsWith(".properties"))
				return pathToFile;
			
			else return pathToFile + ".properties";
		}		
		
		return null;
	}
	
	
	
	

	@Override
	public String getPathToPlaceSomeConfigurationDir(final String dialogTitle) {
		if (dialogTitle != null)
			setDialogTitle(dialogTitle);

		resetChoosableFileFilters();
		
		setFileSelectionMode(DIRECTORIES_ONLY);
		
		return getSelectedDir(false);
	}






	@Override
	public String getPathToClassOrTxtFile(final String dialogTitle) {
		if (dialogTitle != null)
			setDialogTitle(dialogTitle);
		
		// Pokud ji najdu, nastavím cestu do Workspacu - tam se většina tříd nejspíše bude nacházet
		final String pathToWorkspace = ReadFile.getPathToWorkspace();
		
		if (pathToWorkspace != null)
			setCurrentDirectory(new File(pathToWorkspace));
		
		
		
		
		resetChoosableFileFilters();
		
		
		setFileSelectionMode(FILES_ONLY);
		
		
		
		// V tomto dialogu půjde vybrat pouze textový dokument nebo Javovská třída:
		setFileFilter(new FileNameExtensionFilter("*.java", "java"));
		setFileFilter(new FileNameExtensionFilter("*.txt", "txt"));		
		
		// Navíc	 - vlzal jsem některé přípony z průzkumníku projektů, z
		// ProjectExplorerIconInterface.java, aby šli i v tom "klasickém" editoru kódu
		// otevřít jiné textové soubory než Javovskýá třída - ale to je pouze takové zpestření, nic extra
		setFileFilter(new FileNameExtensionFilter("*.wps", "wps"));		
		setFileFilter(new FileNameExtensionFilter("*.dotx", "dotx"));		
		setFileFilter(new FileNameExtensionFilter("*.dotm", "dotm"));		
		setFileFilter(new FileNameExtensionFilter("*.dot", "dot"));		
		setFileFilter(new FileNameExtensionFilter("*.docm", "docm"));		
		setFileFilter(new FileNameExtensionFilter("*.doc", "doc"));		
		setFileFilter(new FileNameExtensionFilter("*.xls", "xls"));		
		setFileFilter(new FileNameExtensionFilter("*.dbf", "dbf"));		
		setFileFilter(new FileNameExtensionFilter("*.svg", "svg"));		
		setFileFilter(new FileNameExtensionFilter("*.thmx", "thmx"));		
		setFileFilter(new FileNameExtensionFilter("*.eml", "eml"));		
		setFileFilter(new FileNameExtensionFilter("*.mde", "mde"));		
		setFileFilter(new FileNameExtensionFilter("*.msg", "msg"));		
		setFileFilter(new FileNameExtensionFilter("*.odf", "odf"));		
		setFileFilter(new FileNameExtensionFilter("*.scd", "scd"));				
		setFileFilter(new FileNameExtensionFilter("*.vcf", "vcf"));		
		setFileFilter(new FileNameExtensionFilter("*.wtx", "wtx"));		
		setFileFilter(new FileNameExtensionFilter("*.xlb", "xlb"));	
		setFileFilter(new FileNameExtensionFilter("*.xll", "xll"));		
		setFileFilter(new FileNameExtensionFilter("*.xlk", "xlk"));		
		setFileFilter(new FileNameExtensionFilter("*.xlm", "xlm"));		
		setFileFilter(new FileNameExtensionFilter("*.xlv", "xlv"));		
		setFileFilter(new FileNameExtensionFilter("*.xnk", "xnk"));		
		setFileFilter(new FileNameExtensionFilter("*.xps", "xps"));		
		setFileFilter(new FileNameExtensionFilter("*.xml", "xml"));		
		setFileFilter(new FileNameExtensionFilter("*.js", "js"));			
		setFileFilter(new FileNameExtensionFilter("*.html", "html"));
		setFileFilter(new FileNameExtensionFilter("*.cshtml", "cshtml"));
		setFileFilter(new FileNameExtensionFilter("*.xhtml", "xhtml"));
		setFileFilter(new FileNameExtensionFilter("*.htm", "htm"));
		setFileFilter(new FileNameExtensionFilter("*.rtf", "rtf"));
		setFileFilter(new FileNameExtensionFilter("*.csv", "csv"));
		setFileFilter(new FileNameExtensionFilter("*.tsv", "tsv"));
		setFileFilter(new FileNameExtensionFilter("*.css", "css"));
		setFileFilter(new FileNameExtensionFilter("*.properties", "properties"));
		setFileFilter(new FileNameExtensionFilter("*.rb", "rb"));
		setFileFilter(new FileNameExtensionFilter("*.c", "c"));
		setFileFilter(new FileNameExtensionFilter("*.h", "h"));
		setFileFilter(new FileNameExtensionFilter("*.cs", "cs"));
		setFileFilter(new FileNameExtensionFilter("*.json", "json"));
		setFileFilter(new FileNameExtensionFilter("*.jsp", "jsp"));
		setFileFilter(new FileNameExtensionFilter("*.php", "php"));
		setFileFilter(new FileNameExtensionFilter("*.groovy", "groovy"));
		setFileFilter(new FileNameExtensionFilter("*.scala", "scala"));
		setFileFilter(new FileNameExtensionFilter("*py.", "py"));
		setFileFilter(new FileNameExtensionFilter("*pyc.", "pyc"));
		setFileFilter(new FileNameExtensionFilter("*pyo.", "pyd"));
		setFileFilter(new FileNameExtensionFilter("*cpp.", "cpp"));
		setFileFilter(new FileNameExtensionFilter("*.class", "class"));
		setFileFilter(new FileNameExtensionFilter("*.cproject", "cproject"));


		if (showOpenDialog(this) == APPROVE_OPTION)
			return getSelectedFile().getAbsolutePath();

		return null;
	}





	
	
	
	

	@Override
	public String getPathToCurrencyData(final File fileCurrencyData) {
		setDialogTitle(chooseCurrencyData);
		
		
		// Otestuji, zda uživatel zadal nějaký soubor, který se má označít:
		if (fileCurrencyData != null) {
			final File fileParent = fileCurrencyData.getParentFile();

			if (fileParent != null) {
				setSelectedFile(fileCurrencyData);
				setCurrentDirectory(fileParent);
			}

			else {
				setSelectedFile(new File(Constants.COMPILER_FILE_CURRENCY_DATA));
				setCurrentDirectory(new File(App.PATH_TO_JAVA_HOME_DIR));
			}
		}


		else {
			// Jako označený soubor nastavím text pro currency.data:
			setSelectedFile(new File(Constants.COMPILER_FILE_CURRENCY_DATA));
			
			/*
			 * Zde nastavím výchozí cestu k Java home adresáři, kde je nainstalován na
			 * příslušném zařízení, nebo níže zakomentovaná varianta nastaví cestu k
			 * adresáří Java home, který má aplikace, ten by měl být v
			 * workspace/configuration/libTools <- tento adresář.
			 */
			setCurrentDirectory(new File(App.PATH_TO_JAVA_HOME_DIR));
			// nebo:
//			setCurrentDirectory(new File(System.getProperty("java.home")));
		}
					
		
		
		setFileSelectionMode(FILES_ONLY);
		
		
		resetChoosableFileFilters();		
		
		
		// V tomto dialogu půjde vybrat pouze textový dokument nebo Javovská třída:
		
		// Následující v tomto konrétním případě nefungovalo:
//		setFileFilter(new FileNameExtensionFilter("*.data", "data"));
		
		// Zde oprava:
		 final FileFilter filter = new FileNameExtensionFilter("*.data", "data");
		 addChoosableFileFilter(filter);
		 
		 setFileFilter(filter);
		 
		 
		
		if (showOpenDialog(this) == APPROVE_OPTION) {
			final String choosedFile = getSelectedFile().getAbsolutePath();
			
			// Otestuji, zda je opravdu označen soubor currency.data 
			final File file = new File(choosedFile);
			
			
			if (file.getName().equals(Constants.COMPILER_FILE_CURRENCY_DATA))
				return choosedFile;

			else
				JOptionPane.showMessageDialog(this,
						wrongFileText + ": \"" + Constants.COMPILER_FILE_CURRENCY_DATA + "\" !", wrongFileTitle,
						JOptionPane.ERROR_MESSAGE);
		}
		
		return null;
	}






	@Override
	public String getPathToToolsJar(final File fileToolsJar) {
		setDialogTitle(chooseToolsJar);
		
		
		if (fileToolsJar != null) {
			final File fileParent = fileToolsJar.getParentFile();

			if (fileParent != null) {
				setSelectedFile(fileToolsJar);
				setCurrentDirectory(fileParent);
			}

			else {
				setSelectedFile(new File(Constants.COMPILER_FILE_TOOLS_JAR));
				setCurrentDirectory(new File(App.PATH_TO_JAVA_HOME_DIR));
			}
		}


		else {
			// Jako označený soubor nastavím text pro tools.jar:
			setSelectedFile(new File(Constants.COMPILER_FILE_TOOLS_JAR));
			
			/*
			 * Zde nastavím výchozí cestu k Java home adresáři, kde je nainstalován na
			 * příslušném zařízení, nebo níže zakomentovaná varianta nastaví cestu k
			 * adresáří Java home, který má aplikace, ten by měl být v
			 * workspace/configuration/libTools <- tento adresář.
			 */
			setCurrentDirectory(new File(App.PATH_TO_JAVA_HOME_DIR));
			// nebo:
//			setCurrentDirectory(new File(System.getProperty("java.home")));
		}
			
		
		// Toto by se dalo nastavit, kdybych chtěl jen prázdné pole v názvu souboru:
//		setSelectedFile(new File(""));
		

		setFileSelectionMode(FILES_ONLY);
		
		
		
		resetChoosableFileFilters();
		
		
		
		// V tomto dialogu půjde vybrat pouze textový dokument nebo Javovská třída:
		
		// Toto v tomto konkrétním případě nefungovalo:
//		setFileFilter(new FileNameExtensionFilter("*.jar", "jar"));
		
		// Zde oprava:
		 final FileFilter filter = new FileNameExtensionFilter("*.jar", "jar");
		 addChoosableFileFilter(filter);
		 
		 setFileFilter(filter);
		 
		 
		
		if (showOpenDialog(this) == APPROVE_OPTION) {
			final String choosedFile = getSelectedFile().getAbsolutePath();
			
			// Otestuji, zda je opravdu označen soubor currency.data 
			final File file = new File(choosedFile);

			if (file.getName().equals(Constants.COMPILER_FILE_TOOLS_JAR))
				return choosedFile;

			else
				JOptionPane.showMessageDialog(this, wrongFileText + ": \"" + Constants.COMPILER_FILE_TOOLS_JAR + "\" !",
						wrongFileTitle, JOptionPane.ERROR_MESSAGE);
		}
		
		return null;
	}






	@Override
	public String getPathToFlavormapProp(final File fileFlavormapProp) {
		setDialogTitle(chooseFlavormapProp);
		
		
		// Otestuji, zda uživatel zadal nějaký soubor, který se má označít:
		if (fileFlavormapProp != null) {
			final File fileParent = fileFlavormapProp.getParentFile();

			if (fileParent != null) {
				setSelectedFile(fileFlavormapProp);
				setCurrentDirectory(fileParent);
			}

			else {
				setSelectedFile(new File(Constants.COMPILER_FILE_FLAVORMAP_PROPERTIES));
				setCurrentDirectory(new File(App.PATH_TO_JAVA_HOME_DIR));
			}
		}


		else {
			// Jako označený soubor nastavím text pro currency.data:
			setSelectedFile(new File(Constants.COMPILER_FILE_FLAVORMAP_PROPERTIES));
			
			/*
			 * Zde nastavím výchozí cestu k Java home adresáři, kde je nainstalován na
			 * příslušném zařízení, nebo níže zakomentovaná varianta nastaví cestu k
			 * adresáří Java home, který má aplikace, ten by měl být v
			 * workspace/configuration/libTools <- tento adresář.
			 */
			setCurrentDirectory(new File(App.PATH_TO_JAVA_HOME_DIR));
			// nebo:
//			setCurrentDirectory(new File(System.getProperty("java.home")));
		}		
		
		
		setFileSelectionMode(FILES_ONLY);
		
		
		resetChoosableFileFilters();
		
		
		// V tomto dialogu půjde vybrat pouze textový dokument nebo Javovská třída:
		
		// Následující v tomto konkrétním případě nefungovalo:
//		setFileFilter(new FileNameExtensionFilter("*.properties", "properties"));
		
		// Zde oprava:
		 final FileFilter filter = new FileNameExtensionFilter("*.properties", "properties");
		 addChoosableFileFilter(filter);
		 
		 setFileFilter(filter);
		
		
		if (showOpenDialog(this) == APPROVE_OPTION) {
			final String choosedFile = getSelectedFile().getAbsolutePath();
			
			// Otestuji, zda je opravdu označen soubor currency.data 
			final File file = new File(choosedFile);
			
			
			if (file.getName().equals(Constants.COMPILER_FILE_FLAVORMAP_PROPERTIES))
				return choosedFile;
			
			else
				JOptionPane.showMessageDialog(this,
						wrongFileText + ": \"" + Constants.COMPILER_FILE_FLAVORMAP_PROPERTIES + "\" !", wrongFileTitle,
						JOptionPane.ERROR_MESSAGE);
		}
		
		return null;				
	}






	@Override
	public String getPathToJavaHomeDir(final File fileSelectedDir) {
		setDialogTitle(chooseJavaHomeDir);
		

		/*
		 * V případě, že uživatel již zadal existující adresář, a chce ho změnit, tak
		 * ten zadaný adresář zde otevřu.
		 */
		if (fileSelectedDir != null) {
			/*
			 * Zde vím, že uživatel již zadal nějaký adresář, tak si zkusím načíst jeho
			 * předka a pokud existuje, tak ten předek bude jako zvolený adresář a akorát v
			 * poli pro zadání cesty označím ten uživatelem vybraný adresář.
			 */
			final File fileParentFile = fileSelectedDir.getParentFile();
			
			if (fileParentFile != null) {
				setCurrentDirectory(fileParentFile);
				setSelectedFile(fileSelectedDir);
			}
			
			/*
			 * Zde nemá předka, tak nastavím ten existující adresář, toto by stačilo i bez
			 * získávání předka, akorát by se pokaždé ten adresář jako by otevřel, že vždy
			 * by se načetla cesta k tomu adresáři a byly by vidět jeho soubory.
			 */
			else setCurrentDirectory(fileSelectedDir);
		}
			
		/*
		 * Zde nastavím výchozí cestu k Java home adresáři, kde je nainstalován na
		 * příslušném zařízení, nebo níže zakomentovaná varianta nastaví cestu k
		 * adresáří Java home, který má aplikace, ten by měl být v
		 * workspace/configuration/libTools <- tento adresář.
		 */
		else
			setCurrentDirectory(new File(App.PATH_TO_JAVA_HOME_DIR));
		
		// nebo:
//		setCurrentDirectory(new File(System.getProperty("java.home")));
		
		resetChoosableFileFilters();
		
		setFileSelectionMode(DIRECTORIES_ONLY);
		
		return getSelectedDir(true);
	}


	
	
	
	
	
	@Override
	public String getPathToWorkspaceDir() {
		setDialogTitle(txtSelectWorkspace);

		resetChoosableFileFilters();
		
		setFileSelectionMode(DIRECTORIES_ONLY);
		
		return getSelectedDir(true);
	}
	
	
	
	
	
	




	@Override
	public String getPathToNewFile() {
		setDialogTitle(txtChooseFile);

		resetChoosableFileFilters();		
		
		return getSelectedDir(false);
	}






	@Override
	public String getPathToNewFileForOpen() {
		setDialogTitle(txtChooseFile);
		
		/*
		 * Zkusím si najít cestu k adresáři označeném jako workspace, pokud se to
		 * povede, tak nastavím výchozí cestu v adresáři workspace. Když ne, otevře se
		 * někde v domácím adresáři
		 */
		final String pathToWorkspace = ReadFile.getPathToWorkspace();
		
		if (pathToWorkspace != null)
			setCurrentDirectory(new File(pathToWorkspace));

		resetChoosableFileFilters();

		return getSelectedDir(true);
	}



	
	
	
	
	
	
	
	/**
	 * Metoda, která otevře dialog pro označení nějakého adresáře a vráti se buď
	 * cesta k označenému adresáři, který uživatel vybral nebo null, pokud uživatel
	 * žádný adresář nevybral a dialog zavřel.
	 * 
	 * @param showOpenDialog
	 *            - logická proměnná, dle které se pozná, zda se má otevřit dialog
	 *            pro otevření nebo uložení. Tj. zda bude tlačítko pro potvrzení
	 *            obsahovat text Open nebo Save, resp. zda si uživatel vybere
	 *            adresář pro otevření nebo uložení nějakého souboru apod.
	 * 
	 * @return null nebo uživatelem označený adresář.
	 */
	private String getSelectedDir(final boolean showOpenDialog) {
		// Otestuji, zda se má otevřít okno pro otevření nějakého adresře:
		if (showOpenDialog) {
			if (showOpenDialog(this) == APPROVE_OPTION)
				return getSelectedFile().getAbsolutePath();
		}

		else if (showSaveDialog(this) == APPROVE_OPTION)
			return getSelectedFile().getAbsolutePath();

		return null;
	}
	
	
	
	


	
	
	
	
	

	@Override
	public String getPathToNewFileForSave() {
		setDialogTitle(txtChooseLocationAndKindOfFile);
		
		resetChoosableFileFilters();		
		
		setFileFilter(new FileNameExtensionFilter("*.txt", "txt"));
		setFileFilter(new FileNameExtensionFilter("*.wps", "wps"));
		setFileFilter(new FileNameExtensionFilter("*.rtf", "rtf"));
		setFileFilter(new FileNameExtensionFilter("*.csv", "csv"));
		setFileFilter(new FileNameExtensionFilter("*.xml", "xml"));
		setFileFilter(new FileNameExtensionFilter("*.xlm", "xlm"));
		setFileFilter(new FileNameExtensionFilter("*.properties", "properties"));
		
		
		if (showSaveDialog(this) == APPROVE_OPTION) {
			// Cesta k souboru:
			String fileName = getSelectedFile().getAbsolutePath();
					
			// Otestuji, zda uživatel již nezadal nějaký typ souboru - správný typ souboru:
			if (fileName.endsWith(".txt") || fileName.endsWith(".wps") || fileName.endsWith(".rtf")
					|| fileName.endsWith(".csv") || fileName.endsWith(".xml") || fileName.endsWith(".xlm")
					|| fileName.endsWith(".properties"))
				return fileName;
			
			else {
				try {
					// K němu připojím zadaný název:
					final String[] extensionsArray = ((FileNameExtensionFilter) getFileFilter()).getExtensions();
					// K souboru připojít typ:
					fileName += "." + extensionsArray[0];
					
					return fileName;

				} catch (Exception e) {
                    /*
                     * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                     * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                     * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                     * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                     */
                    final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                    // Otestuji, zda se podařilo načíst logger:
                    if (logger != null) {
                        // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                        // Nejprve mou informaci o chybě:
                        logger.log(Level.INFO,
                                "Zachycena výjimka při získávání typu souboru, v dialogu nejspíše nebyl vybrán typ " +
                                        "souboru nebo něco podobného.");

                        /*
                         * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                         * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                         * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                         */
                        logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                    }
                    /*
                     * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                     */
                    ExceptionLogger.closeFileHandler();
					
					JOptionPane.showMessageDialog(this, fileErrorText, fileErrorTitle, JOptionPane.ERROR_MESSAGE);
				}
			}
		}
		
		return null;
	}
}