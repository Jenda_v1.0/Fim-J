package cz.uhk.fim.fimj.file;

/**
 * Tato třída slouží pouze jako objekt pro předání a "snažší" předávání - udržování informací o zadaných datech pro
 * nastavení domovského adresáře pro "můj" kompilátor.
 * <p>
 * Tato třída bude slouží k tomu, aby se zde naplnili hodnoty níže nějakými daty - buď pouze data pro zadání cesty k
 * domovskému adresáři Javy někde u uživatele ne PC a nebo data potřebná pro zkopírování souborů typu currency.data,
 * tools.jar nebo flavormap.properties, tyto tři soubory jsou potřebné pro to, aby aplikace umožnila zkompilování tříd v
 * diagramu tříd.
 * <p>
 * Tak se k nim zadá pomocí dialogu JavaHomeForm.java cesty a pomocí této třídy (objektu) se předají informace do
 * příslušného menu, kde se tyto soubory zkopírují, ... (zpracují), vice viz daná část menu.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class JavaHomeInfo {

    /**
     * Logická proměnná, ve které bude uložena hodnota true, pokud se bude jednat o nastavení domovského adresáře pro
     * Javu, ve které se nachází zmíněné tří soubory (hlavně) v adresáři lib pokud v této proměnné bude hodnota false,
     * pak se jedná o zkopírování alespoň jednoho ze zmíněných souborů do adresáře libTools v aplikaci
     */
    private final boolean isPathToJavaHomeDir;


    /**
     * proměnná, ve které bude uložena cesta k domovskému adresáři na disku. a proměnná isPathToJavaHomeDir bude true:
     */
    private final String pathToJavaHomeDir;


    /**
     * Do následujících proměnných se uloží cesty k jednotlivým souborům, které se mají zkopírovat do adresáře libTools
     * v aplikaci - soubory potřehné pro kompilaci tříd v diagramu tříd
     */
    private final String pathToCurrencyData;
    private final String pathToToolsJar;
    private final String pathToFlavormapPro;


    /**
     * Tento konstruktor slouží pro zadání hodnoty coby cesty k adresáři s Javou, odkud se budou brát potřebné soubory
     * pro kompilaci tříd v diagramu tříd
     *
     * @param pathToJavaHomeDir
     *         - cesta k domovskému adresáři Javy u uživatele na disku
     */
    public JavaHomeInfo(final String pathToJavaHomeDir) {
        super();

        this.pathToJavaHomeDir = pathToJavaHomeDir;

        isPathToJavaHomeDir = true;

        // následující proměnné budou null - nebudou potřebä:
        pathToCurrencyData = null;
        pathToToolsJar = null;
        pathToFlavormapPro = null;
    }


    /**
     * Konstruktor, pomocí kterého se naplní cesty k souborům, které se mají zkopírovat do adresáře libTools v
     * aplikaci,
     * a tyto soubory jsou potřebné pro kompilaci tříd v diagramu tříd - aby fungoval kompilátor aplikace
     *
     * @param pathToCurrencyData
     *         - cesta k souboru currency.data
     * @param pathToToolsJar
     *         - cesta ke knihovně tools.jar
     * @param pathToFlavormapPro
     *         - cesta k souboru flavormap.properties
     */
    public JavaHomeInfo(final String pathToCurrencyData, final String pathToToolsJar, final String pathToFlavormapPro) {
        super();

        this.pathToCurrencyData = pathToCurrencyData;
        this.pathToToolsJar = pathToToolsJar;
        this.pathToFlavormapPro = pathToFlavormapPro;

        isPathToJavaHomeDir = false;

        // Zde se nejedná o cestu k domovskému adresáři ale o zkopírování alespoň
        // jednoho ze tri souboru
        // potřehných pro kompilaci, takže cesta bude na null- nebude potřeba:
        pathToJavaHomeDir = null;
    }


    public final boolean isPathToJavaHomeDir() {
        return isPathToJavaHomeDir;
    }


    public final String getPathToJavaHomeDir() {
        return pathToJavaHomeDir;
    }


    public final String getPathToCurrencyData() {
        return pathToCurrencyData;
    }


    public final String getPathToToolsJar() {
        return pathToToolsJar;
    }


    public final String getPathToFlavormapPro() {
        return pathToFlavormapPro;
    }
}