package cz.uhk.fim.fimj.file;

/**
 * Tato třída slouží pouze jako "sdílená" třída pro vlákna, které vytváření vztahy bud mezi instancemi tříd v diagramu
 * instancí nebo mezi třídami samotnými v diagramu tříd.
 * <p>
 * Resp. je to společná třída pro metody, které využívají obe vlákna: CheckRelationShipsBetweenInstances a
 * CheckRelationShipsBetweenClasses
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class AbstractForRelationShipThreads {

    /**
     * Proměnná, která značí logickou hodnotu o tom, zda se mají zobrazovat vztahy ve třídách v diagramu tříd nebo v
     * diagramu instancí samy na sebe. Tzn. že když třeba nějaká třída má proměnnou typu sebe sama, tj. ukazuje daná
     * třída sama na sebe, tak zda se má i v diagramu tříd nebo instancí zobrazit tento vztah třídy sama na sebe.
     * <p>
     * True značí, že se má vztah třídy zobrazit sám na sebe, false značí, že se v daigramu tříd nebo instancí nebudou
     * zobrazovat vztahy tříd na sebe sama.
     * <p>
     * Note: Tuto proměnnou je možné si získavat pomocí getru z přslušného diagramu, ale abych kvůli tomu nemusel
     * opakované volat getr nebo vytvářet další proměnou v příslušných vláknech pro diagram tříd a instancí, tak ji zde
     * jednou naplním a pak pouze využívám.
     */
    protected final boolean showRelationShipsToClassItself;


    /**
     * Proměnná, která určuje, zda se má zobrazit vztah asociace "jako asociace", tj. jedna hrana s šipkami na obou
     * koncích (hodnota false). Nebo, zda se má asociace zobrazit jako dvě hrany s šipkou vždy na jednom konci - ve
     * směru vztahu / reference (hodnota true).
     */
    protected final boolean showAssociationThroughAggregation;


    /**
     * Konstruktor této třídy.
     *
     * @param showRelationShipsToClassItself
     *         - logická hodnota, která značí, zda se mají v diagramu tříd nebo instancí zobrazovat vztahy třídy nebo
     *         instance na sebe sama.
     * @param showAssociationThroughAggregation
     *         - logická proměnná, která značí, zda se má pro znázornění vztahů typu asociace využít dva vztahy typu
     *         agregace (hodnota true), jinak se pro vztah typu asociace využije normálně jedna čára s dvěma šipkami na
     *         koncích hrany (hodnota false).
     */
    public AbstractForRelationShipThreads(final boolean showRelationShipsToClassItself,
                                          final boolean showAssociationThroughAggregation) {

        this.showRelationShipsToClassItself = showRelationShipsToClassItself;
        this.showAssociationThroughAggregation = showAssociationThroughAggregation;
    }
}