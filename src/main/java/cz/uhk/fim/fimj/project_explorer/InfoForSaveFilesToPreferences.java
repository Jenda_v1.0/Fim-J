package cz.uhk.fim.fimj.project_explorer;

import java.io.File;
import java.io.Serializable;

/**
 * Tato třída slouží pouze jako objekt, který se bude ukládat do jednorozměrného pole, které se následně vloží do
 * Preferences API, aby si aplikace pamatovala, jaké soubory má otevřít v průzkumníku souborů a jaké jsou to typy,
 * vlastně je potřeba rozpoznávat pouze soubor typu Excel a cestu k němu, protože v Excelovském souboru je třeba otevřít
 * příslušný list, a u ostatních souborů se toto neřeší, k nim stačí pouze cesta.
 * <p>
 * Takto tato třída má pouze dvě proměnné - jedna pro file = cesta k souboru a druhá pro název listu, který se má v
 * dokumentu otevřit
 * <p>
 * Kvůli ukládání instancí těchto objěktů do Preferences, je třeba, aby tato třída implementovala rozhraní Serializable
 * - aby šly uložit jako objekty
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class InfoForSaveFilesToPreferences implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * Tato proměnná slouží pro uchování cesty k souboru, který se má otevřít.
     */
    private final File file;


    /**
     * Proměnná typu String, která v případě, že se jedná o excelovský dokument bude nabývat konkrétní hodnoty, jinak
     * bude vždy null, a je zde akorát proto, že v interním okně je třeba zobrazit název excelovského listu, aby měl
     * uživatel lepší přehled
     */
    private final String listName;


    /**
     * Konstruktor této třídy - slouží hlavně pro naplnení proměnných.
     *
     * @param file
     *         - proměnná typu File, která ukazuje na soubor, který byl otevřen a má se otevřít při dalším otevření
     *         průzkumníku projektů v adresáři Workspace - pokud bude jště existovat
     * @param listName
     *         - název listu v případě excelovského dokumentu, je zde třeba, aby se v interním okně zobrazil jeho název,
     *         pokud bude otevřen soubor typu Excel
     */
    public InfoForSaveFilesToPreferences(final File file, final String listName) {
        super();

        this.file = file;
        this.listName = listName;
    }


    public final File getFile() {
        return file;
    }


    String getListName() {
        return listName;
    }
}