package cz.uhk.fim.fimj.project_explorer;

import java.awt.BorderLayout;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import org.apache.commons.io.FilenameUtils;
import org.fife.ui.rtextarea.RTextScrollPane;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.FileChooser;

/**
 * Tato třída slouží jako JinternalFrame, což je "vnitřníů okno, které se vkládá do komponenty JDesktopPane (v dialog
 * coby průzkumnik projektu tj. ProjectExplorerDialog.java).
 * <p>
 * Pokaždé, když se přidá nové okno, do JDesktopPane, tak se vytvoří instance této třídy, to bude každé jedno vnitřní
 * okno.
 * <p>
 * A jedná se o otevřený nějaká z možných souborů, které jsem dal, že jdou otevčít, ty které jsem nezaznamenal, tak na
 * ně se použije výchozí metoda a buď se to povede, nebo se zobrazí nějaké ty znaky například jako při otevření orázku,
 * apod. nebo dojde k nějaké chybě, pokud to třeba otevřít nepůjde, apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class FileInternalFrame extends JInternalFrame {

    private static final long serialVersionUID = -2766131165508742762L;


    /**
     * Následující komponenta bude statická pro všechny instance této třídy, aby se nemusela vytvářet pokaždé znovu a
     * slouží pouze pro to, aby bylo možné zapsat nebo získat kód, resp. text ze požadovaného souboru
     */
    private static final ReadWriteFileInterface EDIT_CODE = new ReadWriteFile();


    /**
     * Následující "komponenta" obsahuje metody pro čtení a zápis do vybraných souborů od Microsoftu, například do Wordu
     * a excelu
     */
    private static final ReadWriteFileInterface readWriteMicrosoftFile = new ReadWriteFile();


    /*
     * Následující proměnné slouží pro nastavení pozice levého horního rohu, pokaždé, když se nějaké okno otevře, tak
     * se tyto hodnoty zvýší a následující otevřené okno se tak posune o kus doprava a dolů (doprava dolů po diagonále)
     */
    private static int LEFT_X = 0;
    private static int LEFT_Y = 0;
    private static int WIDTH = 0;


    /**
     * Tato proměnná slouží pro uchování informace o souboru, který je zobrazen v tomto okně, abych pak věděl, kam se má
     * co uložit, apod.
     * <p>
     * Dále si pomocí této proměnné zjistím název okna, což bude název souboru, který bude v tomto okně otevřen -
     * načten
     * <p>
     * a na konec si pmocí této proměnné zjistím ikonu, která bude zobrazena v tomto okně - ikona tohoto rámu okna -
     * Framu
     */
    private File file;


    /**
     * Pokud je v průzkumníku souborů otevřen adresář projekt v aplikaci, tak bude v této proměnné, uložen balíček a
     * název třídy, ve kterém se nachází tato otevřená třída - pokud se jedná o Javovskou třídu.
     * <p>
     * Tento balíček a název třídy bude v syntaxi stejné, jako je v diagramu tříd, např: cz.uhk.fim.ClassName bezu
     * mezer, názvy oddělené desetinnou tečkou, a bez přípony.
     * <p>
     * Tato proměnná je zde potřebna kvůli získání přeložené - zkompilované třídy po kompilaci projektu
     */
    private final String packageText;


    /**
     * Pokud je otevřený soubor typu Excel, tak se do následující proměnné uloží i index listu (počítáno od nuly), který
     * se má otevřít v editoru kodu v aplikaci, resp. v tomto okně, a do kterého se při uložení data z tohoto okna -
     * editoru opět zapíšoiu
     */
    private final int listIndex;


    /**
     * Proměnná pro uložení názvu zvoleného excelovského listu, toto by zde ani nemusel být, ale může nastat situace,
     * kdy se smaže původní - otevřený soubor a pak si ho už nezjistím, resp. nedokážu si už vzít ten původní list,
     * abych ho mohl přepsat a pokud se uživatel rozhodne pro nové uložení, tak budu muset vytvořit nový list, ale
     * nevěděl bych s jakým názvem, tak si ho zde budu držet, i když se ve většině případů nevyužije:
     */
    private final String listName;


    /**
     * Proměnná, do které uložím jeden z typů souboru, které je třeba si hlídat, protože používám různé metody pro
     * otevření příslušných souborů, například jiné metody pro otevření soubory typu Word, jiné pro excel, jiné pro
     * textový dokument, ...
     */
    private final KindOfFile kindOfFile;


    /**
     * Zda se má zalamovat text v editoru (true), jinak false.
     */
    private boolean lineWrap;


    /**
     * editor kodu, do kterého se vloži text z načteného souboru a tento editor se voži na stred tohoto okna:
     */
    private final CodeEditor codeEditor;


    // Proměnné pro výpisy chybových hlášek s texty ve zvoleném jazyce a pro texty do této třídy (interního okna):
    private static String txtSaveChangesText;
    private static String txtSaveChangesTitle;
    private static String txtFileIsNotWrittenText;
    private static String txtLocation;
    private static String txtFileIsNotWrittenTitle;
    private static String txtPdfFileCannotSaveText;
    private static String txtPdfFileCannotSaveTitle;
    private static String txtErrorWhileSavingFileText;
    private static String txtErrorWhileSavingFileTitle;
    private static String txtReadOnly;
    private static String txtFileDoesNotExistChooseNewLocationText;
    private static String txtOriginalLocation;
    private static String txtFileDoesNotExistChooseNewLocationTitle;


    /**
     * Konstruktor této třídy. Slouží k vytvoření instance této tříey coby nějakého vnitřního okna a k naplnění
     * proměnných v této třídě.
     *
     * @param file
     *         - cesta k souboru, který je otevřen v tomto okně
     * @param languageProperties
     *         - soubor typu Properties, který obsahuje texty pro aplikaci ve zvoleném jazyce
     * @param kindOfFile
     *         - výčtoý typ ohledně toho, o jaký typ souboru se jedná - ze sledovaných, kvůli typu pro otevření či zápis
     *         do daného souboru
     * @param listIndex
     *         - pokud se jedná o sobour typu Exce, pak tato proměnná bude značit index listu, který se má otevřít, a do
     *         kterého se mají případně zapsat data
     * @param listName
     *         - pokud je otevřený soubor typu Exce, tak v této proměnné bud euložen název listu, který chce uživatel
     *         otevřít, toto ani není potřebna, ale v případě, že by uživatel smazal otevřený soubor, pak se může v
     *         aplikaci jeětě uložit do stejného listu, který se pojmenuje právě díky této proměnné
     * @param loadedClass
     *         - jedná se o načtený soubor .class již přeložené - zkompilované třídy, tento parametr bude ve většině
     *         případů null, protože konstrukto již obsahuje metody pro načtení příslušné třídy - pokud je potřeba,
     *         akorát nebude null, pokud se tento konstruktor zavolá v jednom případě a sice, že uživatel chtěl otevřít
     *         třídu z diagramu tříd pomocí pravého tlačítka na třídu a dal otevřit v editoru, v takovém případě je již
     *         v menu načtená zmíněná přeložená třída a není důvod ji znovu načítat, tak ji pouze v tomto parametru
     *         předám a ušetřím si další načítání, ostatně bych to samš dělal dvakrát, místo jednou
     */
    FileInternalFrame(final File file, final Properties languageProperties, final KindOfFile kindOfFile,
                      final int listIndex, final String listName, final ProjectExplorerDialog projectExplorerDialog,
                      final Class<?> loadedClass) {

        // nastavím tomuto oknu titulek:
        super(file.getName(), true, true, true, true);


        this.file = file;
        this.kindOfFile = kindOfFile;
        this.listIndex = listIndex;
        this.listName = listName;


        fillVariable(languageProperties);


        // Otestuji, zda se jedná o Javovskou třídu a pokud ano, tak, zda se jedná o třídu,
        // která je v otevřeném projektu, resp. zda je dialog otevřen pouze pro adresář otevřeného
        // projektu:
        if (file.getAbsolutePath().endsWith(".java") && projectExplorerDialog.getFileProjectDir() != null)
            packageText = getPackageOfClass(projectExplorerDialog.getFileProjectDir());

        else
            packageText = null;


        setDialogTitle();


        // nastavím tomuto oknu ikonu:
        setFrameIcon(new ImageIcon(ProjectExplorerIconInterface.getIcon(file).getImage()));

        // Výchozí velikost okna při jeho vytvoření:
        setSize(600, 400);

        // nastavím levý horní okraj na nějakou pozici a pak tuto pozici zvýším
        setLocation(LEFT_X, LEFT_Y);

        // Vypočítám novou pozici pro další okno:
        checkNewPositionOfFrame();


        // nastavím manažera rozvržení komponent na BorderLayout, abych mohl na jeho střed přidat editor
        // kodu - textu
        setLayout(new BorderLayout());


        // Otestuji, zda není hodnota proměnné packageText null, pokud není, tak se jedná nejspíše
        // o třídu, z diagramu tříd, která je otevřen v pruzkumníkovi (v tomto konkrétním interním okně)
        // a pokud se v diagramu tříd nachází třída, která tuto reprezentuje, tak zkusím i načíst
        // její přeloženou - zkompilovanou verzi, abych mohl do nápovědy pro editor kódu
        // do ní doplnit i proměnné, metody, ... říslušné otevřené třídy

        // Deklaruji si proměnnou a pokudsím se do ní načíst přeloženou třídu - pokud se jedná o
        // otevřená soubor coby nějaká třída z otevřeného projektu, jinak se tato přeložená třída ani načítat nebude:
        final Class<?> clazz;

        // Nyní otestuji, zda je proěnní v parametru konsturktoru loadedClass null nebo ne, pokud ano,
        // tak je třeba ji načíst - pokud se jedná o otevřnou třídu v otevřeném projektu,
        // jinak když není null, tak je již předaná v paraetru a v takovémpřípadě se do proměnné clazz pouze vloži
        // a nemusím tuto přeloženou třídu znovu načítat
        if (loadedClass == null) {
            // Otestuji, zda se jedná o třídu z otevřeného projektu a zda je tato třída v
            // diagramu tříd:
            if (packageText != null && GraphClass.isClassWithTextInClassDiagram(packageText))
                // Zde byl nalezen, tak zkusím načíst příslušnou třídu, která je otevřena:
                clazz = App.READ_FILE.loadCompiledClass(packageText, null);

                // Zde se nejedná o třídu z otevřeného projektu, tak nastavím proměnou clazz na
                // null, jako že se mají
                // zobrazit pouze původní, resp. pevně definovaná klíčová slova:
            else
                clazz = null;
        } else
            clazz = loadedClass;


        // vytvořím editor kodu, do kterého se vloží text nebo kod ze soubour (zalezi co za typ souboru se ma otevrit)
        // a přidá se tento editoru do okna:

        // Zde akorát otestuji, zda je otevřena nějaká třída v otevřeném projektu v průzkumníkovi nebo ne,
        // pokud je otevřena nějaká třída, tak její balíčky budou uloženy v proměnné packageText a tudiž nebude null,
        // pak se vytvoří editor kodu s druhým parametrem true, jako, že se má načíst hodnota o tom, zda se mají
        // zkompilovat třídy na pozadí nebo ne
        if (packageText != null)
            codeEditor = new CodeEditor(this, true, languageProperties, projectExplorerDialog,
                    projectExplorerDialog.getCodeEditorProp());

        else
            codeEditor = new CodeEditor(this, false, languageProperties, projectExplorerDialog,
                    projectExplorerDialog.getCodeEditorProp());


        // Zde nastavím auto - doplňování do editoru kodu a moná i včetně přeložené třídy - pokud se povede její
        // načtení, pokud ne, pak bude proměnná clazz null a v takovém případě se nic neděje, v okně s texty (po Ctrl
        // + Sopace)
        // budou zobrazeny pouze ýchozí hdonoty
        codeEditor.setAutoCompletion(clazz);



        /*
         * V případě, že se mají zalamovat texty ve všech interních oknech / otevřených souborech, pak je třeba zde
         * při otevření nového interního okna / souboru také (ne) zalamovat texty.
         *
         * To, zda se mají zalamovat texty se pozná dle označené komponenty v menu.
         */
        wrapTextInEditor(projectExplorerDialog.getMenu().isRbWrapTextInAllFramesSelected());

        // Zda se mají zobrazovat bílé znaky:
        setShowWhiteSpaceInEditor(projectExplorerDialog.getMenu().isRbShowWhiteSpaceInAllFrameSelected());

        setClearWhiteSpaceLineInEditor(projectExplorerDialog.getMenu().isRbClearWhiteSpaceLineInAllFramesSelected());


        final RTextScrollPane rspCodeEditor = new RTextScrollPane(codeEditor);

        add(rspCodeEditor, BorderLayout.CENTER);


        loadTextToEditor();


        // přidám události na zavření okna, zda se má uložit, apod.
        addInternalFrameListener(new InternalFrameAdapter() {

            @Override
            public void internalFrameClosing(final InternalFrameEvent e) {
                /*
                 * Postup:
                 *
                 * Test, zda otevřený soubor ještě existuje, pokud ne, nabídne se jeho vytvoření a vloží se do něj
                 * data z editoru.
                 *
                 * Pokud existuje, tak se načtou znovu data z daného souboru, a otestuje se, zda jsou
                 * příslušná data, resp. texty stejné pokud ano, tak není co řešit, a zavře se okno, jinak pokud ne, tak
                 * se dotážeme uživtele, zda se mají data z editoru uložit pokud ne, zavře se okno a nic víc, pokud
                 * ano, tak se data z editoru zapíšou zpět do souboru a pak se zavře okno
                 */
                saveCodeBeforeCloseFrame();
            }

            @Override
            public void internalFrameActivated(final InternalFrameEvent e) {
                /*
                 * V menu se nastaví jako (ne) označená komponenta, která značí, zda se v označeném interním okně
                 * mají nebo nemají zalamovat texty.
                 *
                 * Tato metoda se zavolá při každém označení interního okna. Protože právě v tomto případě je třeba
                 * nastavit označenou komponentu v menu, aby obsahovala nastavení, zda se zalamují texty vždy pro
                 * konkrétní  interní okno.
                 */
                projectExplorerDialog.getMenu().setSelectedRbWrapTextInSelectedFrame(lineWrap);

                // Při označení "tohoto" interního okna se nastaví označená položka v menu:
                projectExplorerDialog.getMenu().setSelectedRbShowWhiteSpaceInSelectedFrame(isWhiteSpaceVisibleInEditor());

                projectExplorerDialog.getMenu().setSelectedRbClearWhiteSpaceLineInSelectedFrame(isClearWhitespaceLinesInEditor());
            }
        });


        // na konec, když je vše připraveno, zobrazím nové okno:
        setVisible(true);
    }


    /**
     * Nastavení, zda se má zalamovat text, který je zobrazen v editoru. Tzn. že se budou zalamovat dlouhé text tak, aby
     * nešli "do stran", aby uživatel nemusel používat posuvník do stran.
     *
     * @param wrap
     *         - true, pokud se mají zalamovat text, jinak false.
     */
    void wrapTextInEditor(final boolean wrap) {
        lineWrap = wrap;
        codeEditor.wrapTextInEditor(wrap);
    }


    /**
     * Nastavení, zda se mají v editoru zobrazovat bílé znaky nebo ne.
     *
     * @param showWhiteSpace
     *         - true v případě, že se mají zobrazovat bílé znaky. Jinak false.
     */
    void setShowWhiteSpaceInEditor(final boolean showWhiteSpace) {
        codeEditor.showWhiteSpace(showWhiteSpace);
    }


    /**
     * Zjištění, zda se mají v editoru zobrazovat bílé znaky.
     *
     * @return - true v případě, že se mají v editoru zobrazovat bílé znaky. Jinak false.
     */
    boolean isWhiteSpaceVisibleInEditor() {
        return codeEditor.isWhitespaceVisible();
    }


    /**
     * Nastavení, zda se mají odebírat bílé znaky z prázdných řádků v případě, že se na nich stiskne klávesa Enter.
     *
     * @param clearWhiteSpaceLineInEditor
     *         - true v případě, že se mají odebírat bílé znaky na prázdném řádku po tom, zda se na tomto řádku s
     *         kurzorem stiskne klávesy Enter (vloží nový řádek).
     */
    void setClearWhiteSpaceLineInEditor(final boolean clearWhiteSpaceLineInEditor) {
        codeEditor.setClearWhiteSpaceLine(clearWhiteSpaceLineInEditor);
    }


    /**
     * Zjištění, zda se mají odebírat bílé znaky z prázdných řádků v případě, že se na ních nachází kurzor a stiskne se
     * klávesa Enter - vloží se nový řádek.
     *
     * @return true v případě, že se mají odebírat bílé znaky z přízdných řádků po stisknutí klávesy Enter s kurzorem na
     * prázdném řádku. Jinak false.
     */
    boolean isClearWhitespaceLinesInEditor() {
        return codeEditor.isClearWhitespaceLinesEnabled();
    }


    /**
     * Metoda, která naplní textové proměnné, které slouží pro uchování textů pro chybové hlášky, a nějkteré texty do
     * oken, apod.
     *
     * @param properties
     *         - ´proměnná typu Properties, která obsahuje texty pro tuto aplikaci vuživatelem zvoleném jazyce a
     *         příslušné texty z této proměnné se vloží do jednotlivých textovým proměnných
     */
    private static void fillVariable(final Properties properties) {
        if (properties != null) {
            txtSaveChangesText = properties.getProperty("Pe_Mif_Txt_SaveChangesText",
                    Constants.PE_MIF_TXT_SAVE_CHANGES_TEXT);
            txtSaveChangesTitle = properties.getProperty("Pe_Mif_Txt_SaveChangesTitle",
                    Constants.PE_MIF_TXT_SAVE_CHANGES_TITLE);
            txtFileIsNotWrittenText = properties.getProperty("Pe_Mif_Txt_FileIsNotWrittenText",
                    Constants.PE_MIF_TXT_FILE_IS_NOT_WRITTEN_TEXT);
            txtLocation = properties.getProperty("Pe_Mif_Txt_Location", Constants.PE_MIF_TXT_LOCATION);
            txtFileIsNotWrittenTitle = properties.getProperty("Pe_Mif_Txt_FileIsNotWrittenTitle",
                    Constants.PE_MIF_TXT_FILE_IS_NOT_WRITTEN_TITLE);
            txtPdfFileCannotSaveText = properties.getProperty("Pe_Mif_Txt_PdfFileCannotSaveText",
                    Constants.PE_MIF_TXT_PDF_FILE_CANNOT_SAVE_TEXT);
            txtPdfFileCannotSaveTitle = properties.getProperty("Pe_Mif_Txt_PdfFileCannotSaveTitle",
                    Constants.PE_MIF_TXT_PDF_FILE_CANNOT_SAVE_TITLE);
            txtErrorWhileSavingFileText = properties.getProperty("Pe_Mif_Txt_ErrorWhileSavingFileText",
                    Constants.PE_MIF_TXT_ERROR_WHILE_SAVING_FILE_TEXT);
            txtErrorWhileSavingFileTitle = properties.getProperty("Pe_Mif_Txt_ErrorWhileSavingFileTitle",
                    Constants.PE_MIF_TXT_ERROR_WHILE_SAVING_FILE_TITLE);
            txtReadOnly = properties.getProperty("Pe_Mif_Txt_ReadOnly", Constants.PE_MIF_TXT_READ_ONLY);
            txtFileDoesNotExistChooseNewLocationText = properties.getProperty(
                    "Pe_Mif_Txt_FileDoesntExistChooseNewLocationText",
                    Constants.PE_MIF_TXT_FILE_DOESNT_EXIST_CHOOSE_NEW_LOCATION_TEXT);
            txtOriginalLocation = properties.getProperty("Pe_Mif_Txt_OriginalLocation",
                    Constants.PE_MIF_TXT_ORIGINAL_LOCATION);
            txtFileDoesNotExistChooseNewLocationTitle = properties.getProperty(
                    "Pe_Mif_Txt_FileDoesntExistChooseNewLocationTitle",
                    Constants.PE_MIF_TXT_FILE_DOESNT_EXIST_CHOOSE_NEW_LOCATION_TITLE);
        } else {
            txtSaveChangesText = Constants.PE_MIF_TXT_SAVE_CHANGES_TEXT;
            txtSaveChangesTitle = Constants.PE_MIF_TXT_SAVE_CHANGES_TITLE;
            txtFileIsNotWrittenText = Constants.PE_MIF_TXT_FILE_IS_NOT_WRITTEN_TEXT;
            txtLocation = Constants.PE_MIF_TXT_LOCATION;
            txtFileIsNotWrittenTitle = Constants.PE_MIF_TXT_FILE_IS_NOT_WRITTEN_TITLE;
            txtPdfFileCannotSaveText = Constants.PE_MIF_TXT_PDF_FILE_CANNOT_SAVE_TEXT;
            txtPdfFileCannotSaveTitle = Constants.PE_MIF_TXT_PDF_FILE_CANNOT_SAVE_TITLE;
            txtErrorWhileSavingFileText = Constants.PE_MIF_TXT_ERROR_WHILE_SAVING_FILE_TEXT;
            txtErrorWhileSavingFileTitle = Constants.PE_MIF_TXT_ERROR_WHILE_SAVING_FILE_TITLE;
            txtReadOnly = Constants.PE_MIF_TXT_READ_ONLY;
            txtFileDoesNotExistChooseNewLocationText = Constants.PE_MIF_TXT_FILE_DOESNT_EXIST_CHOOSE_NEW_LOCATION_TEXT;
            txtOriginalLocation = Constants.PE_MIF_TXT_ORIGINAL_LOCATION;
            txtFileDoesNotExistChooseNewLocationTitle =
                    Constants.PE_MIF_TXT_FILE_DOESNT_EXIST_CHOOSE_NEW_LOCATION_TITLE;
        }
    }


    /**
     * Metoda, která otestuje a získá balíčky třídy a jejího názvu (podobně jako názvy v diagramu tříd), která je
     * otevřená v tomto konkrétním okně - pokud se tedy jedná o Javovskou třídu a musí být v adresáři src v aktuálně
     * otevřeném projektu.
     * <p>
     * Metoda porovná, zda jsou stejné cesty až k adresáři otevřeného projektu a adresáři src v něm, od tud se budou
     * brát balíčky a na konci název této třídy
     * <p>
     * Je to takto potřebna zjistit, protože pokud se má třída zkompilovat, tak potřebuji vědět v jakém ja balíčku,
     * stejně tak když se má brát její zkompilovaná verze - přeložená třída, tak také potřebuji vědět z jakého balíčku
     * jí mám vzít
     *
     * @param projectDirFile
     *         - proměnná typu File, která ukazuje na adreář otevřeného projěktu v aplikaci
     *
     * @return název třídy i s balíčky coby adresáře od adresáře src k příslušné třídě
     */
    private String getPackageOfClass(final File projectDirFile) {
        // Cyklus bude iterovat od nuly po X =´délka cesty k otevřenému projektu a adresáři src
        // a pokaždé se otestuje, zda se shodují jednotlivé znaky na cestě,

        // pokud ne, pak se nejedná o třídu, která je v adresáři src v příslušném projektu,
        // může se jednat například o třídu, mimo adresář src apod, ale už se neude nacházet v
        // diagramu tříd

        // Pro začátek ale musím otestovat, zda je adresář projektu + src měnší než
        // cesta ke konkrétní třídě, která je otevřená v tomto okně - pokud se jedná o třídu,
        // v adresáři src, tak by tak pokaždé mělo být, jinak ne:

        // Note: Zde nebudu ani testovat, zda příslušné adresáře ještě existují, apod.
        // to je na jiné části aplikaci, zde se pouze testují balíčky, ve kterých by se třída měla
        // nacházet, ale to zda ještě na disku, reps. na příslušném umístění existují, nyní vědět nepotřebuji:

        final File fileSrc = new File(projectDirFile.getAbsoluteFile() + File.separator + "src");

        // vezmu si délky cest k otevřenému souboru a adresáři projěktu:
        final int srcDirLength = fileSrc.getAbsolutePath().length();
        final int openFileLength = file.getAbsolutePath().length();


        if (srcDirLength < openFileLength) {
            // Zde se shodují délky cest, tak mohu otestovat, zda jsou
            // názvy stejné - k adrsáří src:

            boolean areIdentical = true;

            for (int i = 0; i < srcDirLength; i++)
                if (fileSrc.getAbsolutePath().charAt(i) != file.getAbsolutePath().charAt(i)) {
                    // Zde se cesty neshodují, tak mohu skončít:
                    areIdentical = false;
                    break;
                }


            // Nyní otestuji, zda se cesty shodují nebo ne,
            // pokud ano, tak mohu určit balíčet třídy a pokud se neshodují, tak není co řešit:
            if (areIdentical) {
                // ZDe jsou cesty k adresáři src stejné, tak si vezmu balíčky a název třídy, tj.
                // texty od adresáře src dále (v cestě otevřeného souboru):

                // proměnná, do které vložím výsledný text s balíčky:
                // balíček je od adresáře src + 1 (lomítko) k poslední tečte, za kterou je přípona coby typ souboru,
                // a na konec nahradím všechna zpětná lomítka za desetinnou tečku

                return file.getAbsolutePath().substring(srcDirLength + 1, file.getAbsolutePath().lastIndexOf('.'))
                        .replace(File.separator, ".");
            }
        }

        return null;
    }


    /**
     * Metoda, která se zavolá pře zavřením tohoto okna.
     * <p>
     * Metoda otestuje, zda příslušný soubor jeěte existuje, případně nabídne možnost uložit soubor na nové místo, pokud
     * existuje, tak otestuje, zda došlo k nějaké změne, pokud ano, tak nabídne možnost uložení změn.
     */
    final void saveCodeBeforeCloseFrame() {
        // Zjistím, zda daný soubor ještě existuje na uvedeném umístění:
        final boolean result = existFile();

        if (result) {
            // Zde soubor ještě existuje, tak si ho znovu načtu a otestuji, zda jsou příslušná data z editoru v aplikaci
            // a data z příslušného souboru stejná - zda se mají uložit nebo ne:

            final boolean areIdentical = areCodeIdentical();

            if (!areIdentical) {
                // Zde již data nejsou identická, tak se zeptám uživatele, zda se mají data z editoru
                // respl. pozměněná data uložit nebo ne:
                final int option = JOptionPane.showConfirmDialog(this, txtSaveChangesText,
                        txtSaveChangesTitle, JOptionPane.YES_NO_OPTION);

                if (option == JOptionPane.YES_OPTION) {
                    // Zde se mají uložit změny, tak je uložím:
                    // Zde příslušnýá soubor ještě existuje, tak ho mohu opět uložit:
                    saveFile();
                }
            }
        }

        // Zde soubor neexostuje, tak se uživatele dotážu na nové umístění:
        else saveFileToNewLocation();
    }


    /**
     * Metoda, která znovu načte text z příslušného souboru a vloží jej do editoru v daném okně.
     * <p>
     * Jedná se o tzv. "Reload souboru".
     */
    final void loadTextToEditor() {
        // DO následující proměnné si uložím text ze souboru, který si přečtu metodou, pro čtení příslušného
        // typu souboru
        final List<String> textOfFile = getTextFromFile(file, kindOfFile, listIndex);

        // získaný obsah, resp. text ze souboru vložím do editoru v tomto okně:
        codeEditor.setTextToCodeEditor(textOfFile);
    }


    /**
     * Metoda, která se postará o uložení souboru, akorát se dotáže uživatele na novou pozici souboru a na tu případně
     * příslušný soubor zapíše.
     */
    final void saveFileToNewLocation() {
        final String newLocation = checkNewLocation();

        // Otestuji, zda uživatel vybral nové umístění a případně zapíšu soubor na nově zvolené umístění:
        if (newLocation != null) {
            final boolean savedFile = saveFile(new File(newLocation));

            if (!savedFile)
                JOptionPane.showMessageDialog(this,
                        txtFileIsNotWrittenText + "\n" + txtLocation + ": " + newLocation, txtFileIsNotWrittenTitle,
                        JOptionPane.ERROR_MESSAGE);
        }
    }


    /**
     * Metoda, která se postará o uložení souboru, případně vypíše hlášku, že se uložení nepovedlo.
     * <p>
     * Ale jen v případě, že se nejedná o soubor typu .pdf, pokud se jedná o tento typ souboru (.pdf), tak nepůjde
     * uložit, pouze se uživateli vypíše informativní hlíška a tím uložení pro dané okn oskončí.
     */
    final void saveFile() {
        if (kindOfFile.equals(KindOfFile.PDF)) {
            JOptionPane.showMessageDialog(this,
                    txtPdfFileCannotSaveText + "\n" + txtLocation + ": " + file.getAbsolutePath(),
                    txtPdfFileCannotSaveTitle, JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        final boolean savedFile = saveFile(file);

        if (!savedFile)
            JOptionPane.showMessageDialog(this,
                    txtErrorWhileSavingFileText + "\n" + txtLocation + ": " + file.getAbsolutePath(),
                    txtErrorWhileSavingFileTitle, JOptionPane.ERROR_MESSAGE);
    }


    /**
     * Metoda, která otestuji, zda jsou soubor, který je otevřen v tomto okně na nějakém svém lokálním umístění na disku
     * a text tohoto souboru v editoru, tak zda tyto texty jsou identicke
     * <p>
     * Pokud se jedná o soubor typu .pdf, tak se vždy vrátí true, jako že jsou texty stejné, protože tato apliikace
     * neumí uložit soubor typu .pdf, protože se mi nepodařilo vymyslet způsob, jak vzít text z editoru a rozdělit ho na
     * řádky a stránky tak, aby se mohli zapsat příslušným způsobem do souboru typu .pdf.
     *
     * @return true, pokud jsou texty v editoru a příslušného souboru na disku stejné, jinak false
     */
    final boolean areCodeIdentical() {
        // Otestuji, zda se náhodou nejedná o soubor typu .pdf, pak se vrátí true:
        if (kindOfFile.equals(KindOfFile.PDF))
            return true;


        // Znovu si získám data ze souboru:
        final List<String> dataFromFile = getTextFromFile(file, kindOfFile, listIndex);

        // otestuji, zda jsou data identická:
        return codeEditor.compareSourceCode(dataFromFile);
    }


    /**
     * Tato metoda slouží pouze pro nastavení nové pozice pro nové vytvořené okno úplně na začátku je pozice levého
     * horního rohu 0x0, a akorát přičítám hodnotu 30 ke každé souřadnici dokud nebude pozice pro X - sovou souřadnici
     * hodnota < 250, pak se přidá takto hodnota leftX nastaví na hodnotu width += 50, tj. ze se pokazde nove okno
     * posune o kousek vpravo a takto dokud ta hodnota width nebude 300, pak se bude celý cyklus opakovat od začátku
     */
    private static void checkNewPositionOfFrame() {
        if (LEFT_X < 250) {
            LEFT_X += 30;
            LEFT_Y += 30;
        } else {
            LEFT_X = WIDTH += 50;
            LEFT_Y = 0;
        }

        if (WIDTH == 300) {
            WIDTH = -50;
        }
    }


    /**
     * Metoda, která zjistí dle typu souboru, jaký soubor má přečíst a jakou metodu, a výsledek resp. přečtený soubor
     * vrátí v podobně List - kolekce typu String, který budou obsahovat přečtený soubor, jedna položka v kolekci =
     * jeden řádek v souboru.
     *
     * @param file
     *         - soubor, který se má přečíst
     * @param kindOfFile
     *         - typ souboru, resp. o jaký soubor se jedná a dle toho se pouzžíje příslušný metoda pro čtení souboru
     * @param listIndex
     *         - index listu, který bude potřeba, pokud se bude jednat o soubor typu excel
     *
     * @return vždy se vrátí minimálně prázdný List, nebo list, který obsahuje textový obsah souboru
     */
    private static List<String> getTextFromFile(final File file, final KindOfFile kindOfFile,
                                                final int listIndex) {
        switch (kindOfFile) {
            case WORD:
                return readWriteMicrosoftFile.readWordFile(file);

            case EXCEL:
                return readWriteMicrosoftFile.readExcelFile(file, listIndex);

            case PDF:
                return readWriteMicrosoftFile.readPdfFile(file);

            case OTHER:
                return EDIT_CODE.getTextOfFile(file.getAbsolutePath());

            default:
                return new ArrayList<>();
        }
    }


    /**
     * Metoda, která slouží pro uložení tohoto konkrétního souboru otevřeného v tomto okně - editoru.
     *
     * @param file
     *         - proměnná typu File, která ukazuje na soubor, do kterého se má zapsat text z editoru - tohoto okna
     *
     * @return logickou hodnotu o tom, zda se zápis povedl nebo ne, pokud ne, vrátí se false, jinak true
     */
    final boolean saveFile(final File file) {
        switch (kindOfFile) {
            case WORD:
                // Zde nejprve musím vzít všechna data z editoru a vložit je do jednoho Stringu, takže
                // se vezmu všechna data z editoru a pomocí cyklu proiteruji získaný list a vložím každou
                // položku do toho Stringu a na konec přidám jeětě \n pro nový řádek:


                // proměnná, do které se vloží výsledný text:
                final StringBuilder stringBuilderText = new StringBuilder();

                for (final String s : codeEditor.getTextFromCodeEditor())
                    stringBuilderText.append(s).append("\n");

                final boolean resultWord = readWriteMicrosoftFile.writeToWordFile(file, stringBuilderText.toString());

                if (resultWord)
                    // Zde se povedl zápis, tak ještě otestuji, zda se shoduje umístění souborů, protože pokud ne,
                    // například uživatel zvolil nové umístění, tak ho tam musím zapsat:
                    checkLocationOfFile(file);

                return resultWord;


            case EXCEL:
                // v proměnné textToFileList mám sice získaná data z editoru v konkrétní instanci tohoto okna, ale
                // pro to,
                // abych mohl zapsat excelovský dokument je třeba ještě tyto data zpracovat do podoby dvojrozměrného
                // pole,
                // a sice že rozdělím získané řádky v listu dle středníků:

                // docílím toho tak, že si vytvořím dvojerozměrné pole o velikosti jako je list,
                // a pak tento list proiteruji a na stejné index vložím další pole, které získám rozdělením
                // hodnot v konkrétním řádku dle středníků:

                // Vezmu si text z editoru kodu:
                final List<String> textToFileList = codeEditor.getTextFromCodeEditor();

                final Object[][] arrayToFile = new Object[textToFileList.size()][];

                for (int i = 0; i < textToFileList.size(); i++)
                    arrayToFile[i] = textToFileList.get(i).split(";");


                final boolean resultExcel = readWriteMicrosoftFile.writeToExcelFile(file, arrayToFile, listIndex,
                        listName);
                if (resultExcel)
                    // Zde se povedl zápis, tak ještě otestuji, zda se shoduje umístění souborů, protože pokud ne,
                    // například uživatel zvolil nové umístění, tak ho tam musím zapsat:
                    checkLocationOfFile(file);

                return resultExcel;


            case OTHER:
                final boolean resultFile = EDIT_CODE.setTextToFile(file.getAbsolutePath(),
                        codeEditor.getTextFromCodeEditor());
                if (resultFile)
                    // Zde se povedl zápis, tak ještě otestuji, zda se shoduje umístění souborů, protože pokud ne,
                    // například uživatel zvolil nové umístění, tak ho tam musím zapsat:
                    checkLocationOfFile(file);

                return resultFile;


            default:
                break;
        }

        return false;
    }


    /**
     * Metoda, která otestuje, zda se má změnit umístění pro proměnnou file, která ukazuje na otevřený soubor, v tomto
     * okne - editoru, musí se změnit například, když se má uložit někam jinam - uživatel chce změnit umístění, nebo se
     * příslušný soubor smaže a uživatel vybere nové umístění, apod.
     *
     * @param file
     *         - umístění souboru, kam se má zapsat, vždy se otestuje, zda jsou stejné cesty nebo ne, pokud ne, tak se
     *         uloží ta nová, a pokud jsou stejné, tak se nic nedělá
     */
    private void checkLocationOfFile(final File file) {
        if (!this.file.getAbsolutePath().equals(file.getAbsolutePath())) {
            // Uložím si soubor - resp. nové umístění:
            this.file = file;

            // Dále je třeba změnit název okna - pkud došlo ke změne,
            // a pokud se jedná o excel, tak případně i název listu:
            setDialogTitle();
        }
    }


    /**
     * Metoda, která nastaví nový titulek tohoto interního okna. Otestuje, zda se jendá o otevřený soubor typu Excel a
     * pkud ano, tak je třeba nebo není to třeba, ale je dobré, aby uživatel věděl, který list je otevřen, tak bude
     * jeětě za názvem souiboru příslušný název otevřeného excelovského listu.
     * <p>
     * Pro případ, že by měl například otevřených více záložek - listů stejného Excelovského souboru poukd se nejedná o
     * soubor typu Excel, tak se nastaví titulek prostě název otevřeného soubrou.
     * <p>
     * A pokud se jedná o soubor typu PDF, tak se do okna ještě přidá text - Pouze pro čtení, protože se mi nepodařilo
     * najít způsob jak vložit text z editoru kodu do pdf souboru se zarovnáním textu na jednu stránku a takto pro
     * každou stránku v daném souiboru.
     */
    final void setDialogTitle() {
        if (kindOfFile.equals(KindOfFile.EXCEL))
            setTitle(file.getName() + " - " + listName);

        else if (kindOfFile.equals(KindOfFile.PDF))
            setTitle(file.getName() + " - " + txtReadOnly);

            // Zde se nejedná o soubor typu Excel, tak nemusím psát i list, pouze název
            // souboru:
        else
            setTitle(file.getName());
    }


    /**
     * Metoda, která zobrazí dotaz uživateli s tím, že se zeptá na to, zda se má zvolit nové umístění pro smazaný
     * soubor, resp. soubor, který je otevřen v této konkrétní instnaci třídy - okně ale na zvoleném umístění již
     * neexistuje, tak se zeptám uživatele, zda se má vybrat nové umístění pro tento soubor nebo ne.
     * <p>
     * Pokud ano, tak se zobrazí dialog pro zvolení nového umístění pro soubor, jinak nic.
     *
     * @return null, pokud uživatel nevybere cestu, resp. nové umístění otevřeného souboru, jinak novou cestu k
     * otevřenému souboru v tomto okně
     */
    private String checkNewLocation() {
        final int result = JOptionPane.showConfirmDialog(this,
                txtFileDoesNotExistChooseNewLocationText + "\n" + txtOriginalLocation + ": " + file.getAbsolutePath(),
                txtFileDoesNotExistChooseNewLocationTitle, JOptionPane.YES_NO_OPTION);

        if (result == JOptionPane.YES_OPTION)
            return getNewFileLocation();

        return null;
    }


    /**
     * Metoda, která slouží pro výběr nové pozice pro umístění souboru, například v případě, že neexistuje, nebo se má
     * příslušný soubor uložit někam jinam, apod.
     *
     * @return cestu k novému souboru včetně srávně přípny nebo null, pokud uživatel zavře dialog
     */
    final String getNewFileLocation() {
        final String newPath = new FileChooser().getPathToNewFile();

        if (newPath != null) {
            // Zde si uživatel vybral nové umístěněí pro daný soubor, tak otestuji, zda již k názvu napsal příponu,
            // nebo ne, pokud ne, tak ji tam musím přidat:

            // Získám si příponu otevřeného souboru
            final String suffix = FilenameUtils.getExtension(file.getAbsolutePath());

            // Otestuji, zda zvolený název končí stejnou příponou, pokud ne, tak ji přípojím:
            if (newPath.endsWith("." + suffix))
                return newPath;

            else return newPath + "." + suffix;
        }

        return null;
    }


    /**
     * Metoda, která otestuje, zda soubor, který je otevřen v tomto editoru - okně ještě existuje nebo ne, pokud
     * neexistuje, vrátí se false - nebo to neni soubor, a pokud existuje a je to souhor, tak se vrátí true.
     *
     * @return popsáno výše
     */
    final boolean existFile() {
        return file.exists() && file.isFile();
    }


    /**
     * Getr na proměnnou typu File, která drží informaci o tom, který soubor je otevřen v tomto okně
     *
     * @return reference na výše zmíněnou proměnnou
     */
    public final File getFile() {
        return file;
    }


    /**
     * Metoda, která vrátí příponu souboru, který je otevřen v tomto konkrétním interním okně.
     * <p>
     * Přípona bude s desetinnou tečnou, tj. v syntaxi: '.extension'
     *
     * @return přípona otevřeného souboru
     */
    final String getExtensionOfOpenedFile() {
        return "." + FilenameUtils.getExtension(file.getAbsolutePath());
    }


    /**
     * Klasický getr na promennou file, která drží reference na umístění otevřenéhou souboru v příslušném okne -
     * editoru.
     *
     * @param file
     *         - nový soubor, resp. nová reference na nové umístění souboru, který je otevřen v tomto okně - editoru,
     *         mohl se například přejmenovat, apod.
     */
    public final void setFile(final File file) {
        this.file = file;
    }


    /**
     * Klasický getr na proměnnou packageText, která obsahuje buď text coby balíčky a název třídy (oddělené desetinnou
     * tečkou), pokud se jedná o Javovsku třídu, a zárověň tato třída se nachází v adresáři src v otevřeném projektu a
     * tento adresář projektu je otevřen v průzkumníkovi souborů - dialogu, jinak bude tato promnná null.
     *
     * @return referenci na výše uvedenou proměnnou packageText
     */
    public final String getPackageText() {
        return packageText;
    }


    /**
     * Klasický getr na proměnou typu CodeEditor, což je v tomto případě reference na insntaci zmíněné třídy a jedná se
     * o komponentu, coby editor zdrojového ködu, takže takový editor, ve kterém bude zobrazen obsah, resp. v tomto
     * případě pouze text souboru.
     *
     * @return reference na instancí výše zmíněné třídy CodeEditor v tomto konkrétním okně (instanci okna)
     */
    final CodeEditor getCodeEditor() {
        return codeEditor;
    }


    /**
     * Metoda coby klasický getr na proměnnou kindOfFile, která drží výčtový typ ohledně typu otevřeného souboru
     *
     * @return kindOfFile - výčtový typ otevřeného souboru
     */
    final KindOfFile getKindOfFile() {
        return kindOfFile;
    }


    /**
     * Klasický getr na proměnnou listName, která drží název excelovského listu, který je otevřen
     *
     * @return listName - název excelovského listu, který je otevřen, jinak null, pokud není otevřen excell
     */
    final String getListName() {
        return listName;
    }


    /**
     * Getr na proměnnou, která značí, zda se mají zalamovat texty v (tomto konkrétním) editoru.
     *
     * @return true, pokud se mají zalamovat texty v editoru, jinak false.
     */
    boolean isLineWrap() {
        return lineWrap;
    }
}