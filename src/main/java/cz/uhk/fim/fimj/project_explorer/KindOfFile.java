package cz.uhk.fim.fimj.project_explorer;

/**
 * Tato třída coby výčet slouží pro definování některých typů souborů, které je třeba si hlídat v čem se maí otevřít -
 * jakou metodou, v současné době, si budu hlídat pouze soubory typu Word, Excel a ostatní, ale pokud budu tuto aplikaci
 * dále rozšiřovat, definuji i ostatní, jako je třeba OneNote, Access, ... - pro textové soubory
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public enum KindOfFile {

    /**
     * Značí, že se jedná o soubory typu Excel, resp. o soubor, který obsahuje jednu z přípon, který značí, že se jedná
     * o soubor typu Excel:
     */
    EXCEL,

    /**
     * Značí, že se jedná o soubor typu Word - s nějakou z jeho přípon - testovaných - hlídaných aplikací
     */
    WORD,

    /**
     * Značí že se jedná o ostatní soubor, které lze otevřít
     */
    OTHER,

    /**
     * značí, že se jedná o soubory, které nelze otevřžít v editoru kodu:
     */
    DO_NOT_OPEN,

    /**
     * Značí, že se jedná o soubor typu .pdf, který lze otevřít příslušnou metodou - ne klasicky přes buffered Reader
     */
    PDF
}