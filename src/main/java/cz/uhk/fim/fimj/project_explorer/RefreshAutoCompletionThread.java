package cz.uhk.fim.fimj.project_explorer;

import java.io.File;
import java.util.List;
import java.util.Properties;

import javax.swing.JInternalFrame;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.file.CompileClass;

/**
 * Tato třída slouží jako vlákno, které obsahuje metody pro kompilaci tříd a přenačtení okna s doplňováním kódu v
 * otevřených třídách v otevřeném projektu.
 * <p>
 * Toto vlákno se spustí, pokud je v dialogu coby průzkumník projektů otevřen adresář otevřeného projektu v aplikaci a
 * klikne se na tlačítko zkompilovat, nebo pokud je nastaveno, že se má po stisknutí středníku nebo závorek na pozadí
 * kompilovat třídy a načítat proměnné do auto - doplňování.
 * <p>
 * Pak se spustí toto vlákno, které zkompiluje všechny třídy, které se nacházejí v diagramu tříd, a pokud se všechny
 * třídy zkompilují v pořádku - bez chyby, pak se zavolá metoda, která prohledá všechny interní okna, které jsou
 * otevřená a zároveň se jedná o Javovskou třídu, a proměnná v daném interním okne: packageText není null a tento text,
 * který obsahuje se nachází v diagramu tříd, pokud jsou tyto podmínky splněny, pak se může načíst přeložený soubor na
 * dané cestě a zavolat se metoda, která přenačte napovědu s textem v daném interním okně.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class RefreshAutoCompletionThread extends Thread {

	/**
	 * Proměnná, která osabuje texty pro aplikaci v uživatele zvoleném jazyce.
	 */
	private final Properties languageProperties;
	
	
	
	/**
	 * Proměnná, která obshuje referenci na adresář src v otevřeném projektu. Tento
	 * adresář obsahuje dále třídy, které se mají zkompilovat
	 */
	private final File fileSrc;
	
	
	
	/**
	 * Reference na instanci třídy typu ProjectExplorerDialog, ze které si metoda
	 * run bude potřebovat vzít nějaké reference, například na editoru výstupů, pak
	 * pokud se třídy zkompilují v pořádku, tak i na interní okna, aby se mohli
	 * přenačíst nápovědy pro autodoplňování, apod. viz zmíněná metoda.
	 */
	private final ProjectExplorerDialog projectExplorerDialog;
	
	
	
	
	
	/**
	 * Logická proměnná o tom, zda se mají vypisovat hlášky například ohledně
	 * výsledku kompilace tříd, apod. do editoru výstupů, aby o nich věděl uživatel,
	 * tato možnost je zde proto, že když se například stistkně středník a má se
	 * přenačíst auto - doplnovani, tak asi uživatel nechce viddet výsledky o
	 * kompilaci, pokažde kdyz tak udela, ale jen když o to opravdu stojí, tj, že
	 * kdyz klikne na tlačítk kompilovat
	 */
	private final boolean writeResultsFromCompilation;
	
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * Slouží pro naplnění potřenýh proměnných.
	 * 
	 * @param languageProperties
	 *            - reference na soubor s texty, pro aplikaci ve zvoleném jazyce
	 * 
	 * @param fileSrc
	 *            - proměnná typu File, která "ukazuje" na adresář src v aktuálně
	 *            otevřeném projektu
	 * 
	 * @param projectExplorerDialog
	 *            - reference na instanci třídy ProjectExplorerDialog, ze které
	 *            budou potřeba nějaké reference, například na interní okna, editor
	 *            výstupů, apod.
	 * 
	 * @param writeResultsFromCompilation
	 *            - logická proměnná o tom, zda se mají vypisovat hlášky po
	 *            kompilaci nebo ne, protže pokud se jedná o volání tohoto
	 *            konstruktoru po stisknutí středníku apod. tak se nemusí třídy
	 *            zkompilovat, ale pokud se jedná o kliknutí uživatelem na tlačítko
	 *            zkompilovat, tak mu tyto informace musím vypsat
	 */
	RefreshAutoCompletionThread(final Properties languageProperties, final File fileSrc,
			final ProjectExplorerDialog projectExplorerDialog, final boolean writeResultsFromCompilation) {

		super();

		this.languageProperties = languageProperties;
		this.fileSrc = fileSrc;
		this.projectExplorerDialog = projectExplorerDialog;
		this.writeResultsFromCompilation = writeResultsFromCompilation;
	}
	
	
	
	

	
	@Override
	public void run() {
		// Získám si list se soubory coby cesty k třídám, které se mají zkompilovat
		final List<File> filesToCompileList = App.READ_FILE.getFileFromCells(fileSrc.getAbsolutePath(),
				GraphClass.getCellsList(), projectExplorerDialog.getOutputEditor(), languageProperties);
		
		// Nyní mám List typu File, kde jsou uloženy získané cesty ke třídám, které se mají zkompilovat,
		// tak tento list předám do metody pro kompilaci, kde se vyřeší zbytek:
		new CompileClass(languageProperties);
		
		// uložím si logickou proměnnou o tom, zda se třídy zkompilovaly v pořádku nebo ne,
		// abych věděl, zda se má přenačíst autodoplňování pro otevřené třídy nebo ne:
		final boolean result;
		
				
		/*
		 * Otestuji, zda se mají vypisovat výpisy ohledně výsledku kompilace nebo ne,
		 * dle toho buď naplním proměnnou pro výstupní editor nebo ne.
		 * 
		 * Dále v tomto případě vždy zakážu aktualizovat vztahy mezi instancemi v
		 * diagramu instancí, ty se přenačtou až v případě, že budu mít jistotu, zda se
		 * třídy zkompilovaly bez chyby.
		 */
		if (writeResultsFromCompilation)
			result = CompileClass.compileClass(projectExplorerDialog.getOutputEditor(), filesToCompileList, false, false);

		else
			result = CompileClass.compileClass(null, filesToCompileList, false, false);
		
		
		if (result) {
			// Zde se podařilo třídy zkompiovat bez chyby ,takže se již vytvořil příslušné soubory .class
			// a přesunuly se do adresáře bin a identické cesty z adresáře src - v příslušných balíčcích,
			// tak mohu zavolat metodu, která otestuje, zda je otevřena nějaká třída z příslušného adresáře src
			// a přenačtu její auto - doplnovaní kodu - pro naříklad nově zadané proměnné, metody, konstruktory, apod.
			// více viz. třída s autodoplňováním
			
			// Získám si všechna otevřená okna:
			final JInternalFrame[] internalFramesArray = projectExplorerDialog.getDesktopPane().getAllFrames();
			
			// proiteruji je, abych otestoval, zda se jedná o třídu, vysvětelno nad hlavičkou třídy v komentáři:
			for (final JInternalFrame f : internalFramesArray) {
				// nemusel bych, ale pro jistotu otestuji, zda neni null, a je dané interní okno instance MyInternalFrame,
				// jiné typy oken, tak ani nedávám a null by také být nemělo, ale kdyby náhodou...
				if (f instanceof FileInternalFrame) {
					final FileInternalFrame myFrame = (FileInternalFrame) f;
					
					// Otestuji, zda není proměnná packageText null - tj. jedná se o třídu z adresáře src v 
					// otevřeném projektu a je otevřen projekt v průzkumníku souborů, a zároveň se tatotřída 
					// nachází v diagramu tříd:
					if (myFrame.getPackageText() != null
							&& GraphClass.isClassWithTextInClassDiagram(myFrame.getPackageText())) {
						// Zde jsou podmínky splněny, tak mohu si mohu načíst příslušný přeložený soubor
						// tříd - .class soubor
						// a vložit jej do metody pro přenačtení nápovědy:

						// Zde existuje adresář bin, ostatně by vlastně měl, protože se povedla
						// kompilace, a pokud tam nejěkké třídy
						// vůbec jsou, tak se po kompilaci do adresáře bin do stejného balíčku příslušné
						// přeložené třídy přesunou

						// Nyní si tedy mohu načíst aktuálně testovanou třídu, která je otevřena v právě
						// testovaném interním okně,
						// a pokusit se načíst přeloženou třídu - soubor .class
						final Class<?> clazz;

						// Zde opět otestuji, zda se mají vypisovat hlášky do editoru výstupů:
						if (writeResultsFromCompilation)
							// clazz = readFile.loadCompiledClass(myFrame.getPackageText(),
							// projectExplorerDialog.getOutputEditor(), pathToBin);
							// clazz = App.READ_FILE.loadCompiledClass(myFrame.getPackageText(),
							// projectExplorerDialog.getOutputEditor(), pathToBin);
							clazz = App.READ_FILE.loadCompiledClass(myFrame.getPackageText(),
									projectExplorerDialog.getOutputEditor());

						else
							clazz = App.READ_FILE.loadCompiledClass(myFrame.getPackageText(), null);

						// Otestuji, zda se podařilo výše zmíněnou přeloženou třídu načíst a pokud ano,
						// tak ji mohu zavolat metodu v právě testovaném interním okně - "otevřeném
						// souboru"
						// a zavolat metodu pro přenačtení dialogu - okna pro tzv. auto - doplňování,
						// resp. nápovědy,
						// která se zobrazí po stisknutí kombinace kláves Ctrl + Space
						if (clazz != null)
							myFrame.getCodeEditor().setAutoCompletion(clazz);
					}
				}
			}
		}
	}
}