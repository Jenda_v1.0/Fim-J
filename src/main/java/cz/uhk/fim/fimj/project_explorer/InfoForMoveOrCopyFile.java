package cz.uhk.fim.fimj.project_explorer;

import java.io.File;

/**
 * Tato třída slouží puze pro uložení informací o cestě k souboru nebo adresáři, který se má přesunout nebo zkopírovat a
 * informaci o tom, zda se soubor či adresář na cestě má přesunout nebo zkopírovat
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class InfoForMoveOrCopyFile {

    /**
     * Tato proměnná slouží pro uchování cesty k souboru nebo adrsáři, který se má buď přesunout nebo zkopírovat
     */
    private final File file;


    /**
     * Tato logická proměnná slouzží pro uložení informací o tom, zda se má soubor v proměnné file zkopírovat nebo
     * přesunout. cut = true znaměná, že se má soubor přesunout na cílové umístění a cut = false znamený, že se má
     * zkopirovat na cílové umístění
     */
    private final boolean cut;


    /**
     * Tento konstruktor slouží pouze pro naplnění proměnných a vytvoření instance této třídy.
     *
     * @param file
     *         - cesta k souboru nebo adresáři, který se má přesunout nebo zkopírovat
     * @param cut
     *         - logická hodnota o tom, zda se má soubor nebo adresář v proměnné file zkopírovat nebo přesunout
     */
    public InfoForMoveOrCopyFile(final File file, final boolean cut) {
        super();

        this.file = file;
        this.cut = cut;
    }


    public final File getFile() {
        return file;
    }


    public final boolean isCut() {
        return cut;
    }
}