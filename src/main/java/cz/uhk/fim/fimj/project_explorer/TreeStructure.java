package cz.uhk.fim.fimj.project_explorer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import javax.swing.JTree;
import javax.swing.ToolTipManager;
import javax.swing.tree.TreePath;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;

/**
 * Tato třída slouží jako komponenta Jtree, která slouží pro zobrazení adresářové struktury - stromová struktura souborů
 * od adresáře coby Workspace
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class TreeStructure extends JTree implements LanguageInterface {

    private static final long serialVersionUID = 1L;

    /**
     * Reference na soubor coby adresář označený jako Workspace pro aplikaci:
     */
    private final File fileWorkspaceDir;


    /**
     * Proměnná, do které se vloží hodnota pro zkopírování nebo přesunutí nějakého souboru nebo adresáře
     */
    private static InfoForMoveOrCopyFile infoForMoveOrCopyFile;


    /**
     * Proměnná, která slouží jako okno pro zobrazení obrázku v případě, že uživatel najede ve stromové struktuře
     * souborů na soubor, která je obrázek, například jpg, png, ...
     * <p>
     * Pokud tedy uživatel najeden myší na soubor - obrázek, pak se v tomto okně zobrazí náhled na příslušný obrázek,
     * pokud to bude cokoli jiného, pak se v tom dialogu - okně místo obrázku zobrazí pouze text, který bude obsahovat
     * cestu k příslušnému souboru, na který uživatel najel myší.
     */
    private ImageView imageView;


    // Textové proměnné pro uložení textu pro předání do položky pro rozšíření nebo
    // kilaps větvě.
    private String txtExpand;
    private String txtCollapse;
    private String txtExpandAll;
    private String txtCollapseAll;


    /**
     * Konstruktor této třídy.
     *
     * @param fileWorkspaceDir
     *         - proměnná typu File, která předpokládá, cestu k adresáři coby Workspace pro tuto aplikaci a tento
     *         adresář již musí existovat, abych to nedělal na více místech, tak se to otestuje pouze při předání do
     *         dialogu ProjectExplorerDialog a zde už tento adresář tedy musí existovat.
     * @param languageProperties
     *         - reference na soubor typu Properties, který obsahuje texty pro aplikaci v uživatelem zvoleném jazyce,
     *         aby se mohla předat dále do menu po kliknutí pravým tlačítkem na položku ve stromu
     * @param projectExplorerDialog
     *         - reference na instanci třídy ProjectExplorerDialog, protože v něm je definovaná metoda pro vytvoření
     *         nového internalFrame
     * @param enableRenameItem
     *         - Logická proměnná, do které uložím proměnnou trud, pokud se má zakázat tlačítko pro přejmenování položky
     *         pomocí tohoto menu, protože je otevřen nějaký projekt v pruzkumnikovi, jinak bude tato proměnna false,
     *         pokud je otevřen adresář Workspace, v takovém případě půjde normálně označená položka přejmenovat
     * @param fileList
     *         - kolekce obsahující cesty k adresářům, které se mají rozbalit v adresářové struktuře.
     * @param expandAllSubDirs
     *         - true v případě, že se mají rozbalit veškeré podadresáře adresářů v listu fileLIst. Jinak false v
     *         případě, že se má rozbalit pouze jeden každý adresář v listu fileList.
     */
    TreeStructure(final File fileWorkspaceDir, final Properties languageProperties,
                  final ProjectExplorerDialog projectExplorerDialog, final boolean enableRenameItem,
                  final List<File> fileList, final boolean expandAllSubDirs) {
        /*
         * Toto je potřeba, aby bylo možné zobrazit tooltip nad souborem, nad kterým je právě kurzor myši.
         */
        ToolTipManager.sharedInstance().registerComponent(this);


        this.fileWorkspaceDir = fileWorkspaceDir;


        // nastavím, aby se použil pro vykreslování můj renderer:
        setCellRenderer(new TreeStructureCellRenderer());


        // Zda má být viditelný adresář root - v tomto případě se jedná o Workspace,
        // bud ta složka bude viditelná v stromu a z toho budou vycházet ostatní objekty
        // nebo ne a vše v tomto adresáři bude na stajné úrovní - od adresáře coby Workspace
        // true = bude root vidět (Workspace), false = nebude vidět - vše na stejné úrovní
        setRootVisible(true);


        // Zda se mají zobrazit šipky i u roota - šipky pro otevření nebo uzavření otevření,
        // resp rozbalení
        setShowsRootHandles(true);


        setBackground(new Color(0xEDEBEB));


        // načtu si strukturu souborů v adresáři Workspace:
        refreshTree(fileList, expandAllSubDirs);

        setSelectionRow(0);


        // Přidání událostí po kliknutí tlačítkem myši na nějakou položku ve stromu:
        // menu se otevře po kliknutí pravým tlačítkem ny položku,
        // nebo pokud je to soubor, tak se pokusí otevřít v rámech - editrech kodu - po kliknutí dvakrát
        // levým tlačítkem na položku
        addMouseListener(new MouseAdapter() {

            @Override
            public void mouseReleased(MouseEvent e) {
                // Bud se klikne 2x levým tlačítkem myši na objekt ve stromu, tím se pokusí otevřit
                // příslušný soubor v editoru kodu - v novém okně - pokud ještě není příslušný soubor otevřen,
                // pokud je, tak se pouze zobrazí příslušné okno:
                if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON1) {
                    // Vezmu si označenou komponentu, resp. položku, na kterou uživatel dvakrát klikl:
                    final File selectedFile = (File) getLastSelectedPathComponent();

                    if (selectedFile != null && !selectedFile.isDirectory())
                        projectExplorerDialog.createInternalFrame(selectedFile, null);
                }


                // Zde se kliklo na nějaký objekt ve stromu pravým tlačítkem myši, tak otevřu menu s
                // tlačítky pro další manipulaci
                //				else if (SwingUtilities.isRightMouseButton(e)) {
                else if (e.getButton() == MouseEvent.BUTTON3) {
                    // Nejprve nastavím položku pro rozvětvení - pokud je to možné:
                    final int selectedRow = getRowForLocation(e.getX(), e.getY());

                    /*
                     * Nastavím označeou položku:
                     */
                    final TreePath selectedPath = getPathForLocation(e.getX(), e.getY());



                    /*
                     * Získám si soubor, na který uživatel klikkl - pokud je to možné a neklikl
                     * třeba do prázdného místa apod.
                     */
                    final File file;
                    if (getLastSelectedPathComponent() != null)
                        file = (File) getLastSelectedPathComponent();
                    else
                        file = null;


                    setSelectionPath(selectedPath);

                    if (selectedRow > -1)
                        setSelectionRow(selectedRow);



                    /*
                     * Kontextové menu:
                     */
                    final JtreePopupMenu popupMenu;

                    if (file != null) {
                        popupMenu = new JtreePopupMenu(file, TreeStructure.this, projectExplorerDialog);
                        popupMenu.setLanguage(languageProperties);

                        // Zde jsem musel nastavit velikosti, protože defaulně se nepřizpůsobila, nevím
                        // proč:
                        popupMenu.setPreferredSize(new Dimension(95, 225));

                        // Otestuji, zda je otevřen nějaký projekt v průzkumnikovi a pokud ano, tak
                        // zakážu kliknutí na
                        // tlačítko přejmenovat:
                        if (enableRenameItem)
                            popupMenu.enableItem();

                        popupMenu.show(TreeStructure.this, e.getX(), e.getY());
                    }

                    else
                        popupMenu = null;






                    /*
                     * Nastavení, zda se má položka pro rozšíření nebo uzavření větve zpřístupnit
                     * nebo znepřístupnit a natavit na rozšíření nebo uzavření.
                     */
                    if (selectedPath != null && popupMenu != null) {
                        popupMenu.setSelectedPath(selectedPath);

                        if (isExpanded(selectedPath)) {
                            popupMenu.putValueToActCollapseExpandLeaf(txtCollapse);
                            popupMenu.putValueToActCollapseExpandWholeLeaf(txtCollapseAll);
                        }

                        else {
                            popupMenu.putValueToActCollapseExpandLeaf(txtExpand);
                            popupMenu.putValueToActCollapseExpandWholeLeaf(txtExpandAll);
                        }



                        /*
                         * Nejprve zjistím, zda se jedná o adresář a pokud ano, pak znepřístupním
                         * položku pro rozšíření příslušné větve. V opačnémpřípadě jej zpřístupním.
                         */
                        if (!file.isDirectory()) {
                            popupMenu.setActCollapseExpandLeafEnabled(false);
                            popupMenu.setActCollapseExpandWholeLeafEnabled(false);
                        }

                        else {
                            popupMenu.setActCollapseExpandLeafEnabled(true);
                            popupMenu.setActCollapseExpandWholeLeafEnabled(true);
                        }
                    }
                }
            }


            @Override
            public void mouseExited(MouseEvent e) {
                /*
                 * Test pro uzavření dialogu s obrázkem - pokud je zobrazen:
                 */
                if (imageView != null) {
                    imageView.dispose();
                    imageView = null;
                }
            }
        });









        addMouseMotionListener(new MouseMotionAdapter() {

            @Override
            public void mouseMoved(MouseEvent e) {
                // Získám si pozice kurzoru myši:
                final int x = e.getX();
                final int y = e.getY();

                /*
                 * V tomto konkrétním případě musím označit příslušnou položku, protože v
                 * použité komponentě jinak nezjistím označeou položku, ta reaguje jen nalevé
                 * tlačítko myši.
                 */
                final TreePath selectedPath = getPathForLocation(x, y);

                setSelectionPath(selectedPath);

                // Zde je třeba zavřít dialog s obrázkem pokud je otevřen, jinak by se oteváral pořád do kola:
                if (imageView != null) {
                    imageView.dispose();
                    imageView = null;
                }


                // Otestuji, zda je vůbec něco označeno a případně otevřu menu, jink nic nedělám:
                if (getLastSelectedPathComponent() != null) {
                    /*
                     * Zde má uživatel kurzor nad nějakým souborem, tak zjistím o jaký soubor jde.
                     */
                    final File file = (File) getLastSelectedPathComponent();

                    /*
                     * Pokud je označen obrázek, tj. soubor s jednou s testovaných přípon, tak jej zobraím, jinak nic nedělám:
                     */
                    if (ProjectExplorerIconInterface.isImage(file.getAbsolutePath()))
                        imageView = new ImageView(file, x, y);
                }
            }
        });


        setLanguage(languageProperties);
    }


    @Override
    public String getToolTipText(MouseEvent event) {
        if (event == null)
            return null;

        final TreePath path = getPathForLocation(event.getX(), event.getY());

        if (path == null)
            return null;

        final TreePath selectedPath = getPathForLocation(event.getX(), event.getY());


        setSelectionPath(selectedPath);

        if (getLastSelectedPathComponent() == null)
            return null;

        /*
         * Označený soubor:
         */
        final File file = (File) getLastSelectedPathComponent();

        // return (f == null ? null : f.getAbsolutePath());
        return (file == null ? super.getToolTipText(event) : file.getAbsolutePath());
    }


    /*
     * Následující metodu 'expandCollapseAll' jsem na konec nevyužil, protože nebyla
     * potřeba, jedná se o metodu, která rozbalí nebo zabalí úplně celou adresářovou
     * strukturu. Tuto metodu jsem nahradil metodou 'expandCollapseSelectedDir',
     * která rozbalí nebo zabalí všechny adresáře v označeném adresáři - rozbalí /
     * zabalí celý uzel.
     */

    /*
     * Metoda, která slouží pro rozbalení nebo uzavření nějaké větvě, tj. pokud se
     * jedná například o rozbalený adresář ve stromu apod. tak, zda se ten adresář
     * má "otevřít", aby byly vidět jeho soubory nebo "uzavřit" apod.
     *
     * @param expand
     *            - logikcá proměnná o tom, zda se má adresář otevřít nebo uzavřit.
     */
//    public final void expandCollapseAll(final boolean expand) {
//        if (expand)
//            for (int i = 0; i < getRowCount(); i++)
//                expandRow(i);
//
//        else
//        /*
//         * Zde je třeba vést opačný cykluc, jinak bych zabalil první větev a tím by
//         * cyklus skončil, protože by již žádné další řádky ve stromové struktuře
//         * nebyli.
//         */
//            for (int i = getRowCount(); i >= 0; i--)
//                collapseRow(i);
//    }


    /**
     * Metoda, která slouží pro rozbalení neboli rozšíření / zabalení všech adresářů, které se nachází v adresáři
     * treePath.
     *
     * @param expand
     *         - logicá prměnná, která znčí, zda se mají větve v adresáři treePath rozšířit nebo zabalit, true značí
     *         rozšíčit, false zabalit.
     * @param treePath
     *         - cesta k uživatelem zvolenému adresáři ve stromové struktuře jehož podadresáře se mají rozšířit /
     *         zabalit.
     */
    final void expandCollapseSelectedDir(final boolean expand, final TreePath treePath) {
        /*
         * Zjistím si cestu k označenému adrsáři (vím, že je to adresář, jiné nelze
         * označit tak, aby byla zpřistupněna možnost jej rozbalit / zabalit).
         */
        final String selectedPath = treePath.getLastPathComponent().toString();

        if (expand)
            for (int i = 0; i < getRowCount(); i++) {
                final boolean expandLeaf = isFileDirOrSubFile(selectedPath, i);

                if (expandLeaf)
                    expandRow(i);
            }

        else
            /*
             * Tento cyklus musí být testovat položky v opačném směru, jinak by se schovala
             * ta první větev, ale dál by se nešlo, protože už by žádné další řádky nebyly.
             */
            for (int i = getRowCount() - 1; i >= 0; i--) {
                final boolean collapseLeaf = isFileDirOrSubFile(selectedPath, i);

                if (collapseLeaf)
                    collapseRow(i);
            }
    }


    /**
     * Metoda, která zjistí, zda je soubor nebo adresář na řádku iteratingRow soubor selectedPath nebo se jedná o soubor
     * nebo adresář, který je vnořený někde v adresář selectdPath.
     *
     * @param selectedPath
     *         - uživatelem označený adresář ve stromové struktuře.
     * @param iteratingRow
     *         právě itreroný řádek ve stromové struktuře. Jedná se o index souboru nebo adresáře ve stromové struktuře,
     *         o kterém chci zjistit, zda se nachází v nekde v adresáři selectedPath (může být více vnořený v
     *         selectdPath) nebo se jedná o adresář selectedPath.
     *
     * @return true v případě, že iterovaný soubor / adresář je adresář selectedPath nebo je to soubor nebo adresář
     * vnořený nekde v adresáři selectedPath, jinak false.
     */
    private boolean isFileDirOrSubFile(final String selectedPath, final int iteratingRow) {
        /*
         * Označená cesta ve stromové struktuře.
         */
        final TreePath iterationPath = getPathForRow(iteratingRow);
        /*
         * Cesta k aktuálně iterovanému souboru / adresáři:
         */
        final String iterationPathInText = iterationPath.getLastPathComponent().toString();

        /*
         * Zde si zjistím, zda se má příslušný adresář / větev rozšiřit nebo zabalit, to
         * zjistím tak, že porovnám cesty souborů, pokud je iterationgPathInText soubor
         * nebo adresář, který se nachází v adresáři selectedPath nebo se jedná o
         * adresář selectedPath, tak právě iterovanou větev rozšířím / zabalím, jinak
         * nic nedělám.
         */
        return isFileInDir(selectedPath, iterationPathInText);
    }


    /**
     * Metoda, která zjistí, zda cesta k souboru (pathToSubFile) se nachází v adresáři pathToDir nebo se jedná o adresář
     * pathToDir.
     * <p>
     * Jde o to, že abych zjistil jaké větve se mohou rozšířit / zabalit v případě, že chce uživatel rozbalit / zabalit
     * pouze zvolený adresář, tak potřebuji nějak zjistit, jaké větve se nachází ve zvoleném adresáři a jaké ne, jelikož
     * mám celou stromovou struktur tvořenou jakoby ze souborů, tak si pouze otestuji jejich cesty.
     * <p>
     * Například: PathToDir: /dir1/dir2 PPathToSubFile: /dir1/dir2/xxx
     * <p>
     * Z cesty výše poznám, že pathToSubFile je v adresáři dir2 (pathToDir), kdyby se cesta k adresáři pathToDir
     * nenacházela od začátku souboru po nějakou cestu v cestě pathToSubFile, tak se nejedná o adresář / soubor z
     * příslušného adresáře pathToDir.
     *
     * @param pathToDir
     *         - cesta k nějakému adresáři, o kterém se má zjistit, zda obsahuje nebo se jedná o adresář pathToSubFile.
     * @param pathToSubFile
     *         - cesta k nějakému souboru nebo adresáři, o kterém se má zjistit, zda se nachází v pathToDir adresáři
     *         nebo se jedná o pathToDir adresář.
     *
     * @return true, pokud pathTosubFile je adresář pathToDir nebo je pathToSubFile podadresářem / podsouborem adresáře
     * pathToDir, jinak false.
     */
    private static boolean isFileInDir(final String pathToDir, final String pathToSubFile) {
        if (pathToDir.length() > pathToSubFile.length())
            return false;

        for (int i = 0; i < pathToDir.length(); i++)
            if (pathToDir.charAt(i) != pathToSubFile.charAt(i))
                return false;

        return true;
    }


    /**
     * Metoda, která spustí vlákno, které načte celou adresářovou strukturu od adresáře Workspace
     *
     * @param fileList
     *         - kolekce, která obsahuje cesty k adresářům, které se mají rozbalit ve stromové struktuře.
     * @param expandAllSubDirs
     *         - true v případě, že se mají rozbalit veškeré podadresáře každého adresáře v listu fileList. False, když
     *         se má vždy rozbalit pouze ten jeden každý adresář v listu fileList.
     */
    final void refreshTree(final List<File> fileList, final boolean expandAllSubDirs) {
        new LoadTreeThread(this, fileList, expandAllSubDirs).start();
    }


    /**
     * Metoda coby klasický getr na proměnnou fileForkspaceDir, která vrátí referenci na adresář, označený jako
     * Workspace
     *
     * @return reference na adresář typu File coby Workspace
     */
    final File getFileWorkspaceDir() {
        return fileWorkspaceDir;
    }


    static InfoForMoveOrCopyFile getInfoForMoveOrCopyFile() {
        return infoForMoveOrCopyFile;
    }


    static void setInfoForMoveOrCopyFile(InfoForMoveOrCopyFile infoForMoveOrCopyFile) {
        TreeStructure.infoForMoveOrCopyFile = infoForMoveOrCopyFile;
    }


    /**
     * Rozbalení adresářů, které se nachází v listu fileList.
     * <p>
     * Jde o to, že když uživatel otevře dialog průzkumník projektů, tak je vhodné, aby se rozbalil například adresář
     * src v tom otevřeném projektu nebo jiný otevřený adresář apod.
     *
     * @param fileList
     *         - kolekce obsahující cesty k adresářům, které se mají rozbalit ve stromové struktuře.
     * @param expandAllSubDirs
     *         - true v případě, že se mají rozbalit veškeré podadresáře každého adresáře v listu fileList, které se
     *         mají rozbalit. Pokud bude tato proměnná false, rozbalí se vždy jen ten jeden požadovaný adresář.
     */
    final void expandSrcDir(final List<File> fileList, final boolean expandAllSubDirs) {
        if (fileList == null || fileList.isEmpty())
            return;

        fileList.forEach(f -> {
            // Hledají se pouze adresáře:
            if (f == null || f.isFile())
                return;

            // Vždy kořen stromové struktury:
            final Object[] roots = getPathForRow(0).getPath();


            /*
             * Note:
             *
             * zde by se mohlo místo první položky v poli projít celé pole. Ale v průzkumníku projektů je vždy pouze
             * jeden kořenový adresář a sice workspace nebo adresář otevřeného projektu.
             *
             * Dále by se tomu muselo přizpůsobit sestavování cesty pro rozevírání větví / souborů.
             */

            /*
             * Kořenový adresář ve stromové struktuře pro zahájení vyhledávání požadovaného adresáře pro rozbalení.
             */
            final File rootFile;
            // Na indexu nula ja vždy kořenový adresář:
            if (roots[0] instanceof File)
                rootFile = (File) roots[0];

            else rootFile = new File(roots[0].toString());


            /*
             * Kolekce, do které se vloží názvy adresářů, které se mají rozbalit ve stromové struktuře.
             *
             * Je to potřeba, protože se vždy musí rozbalit postupně vždy specifický adresář, která je aktuálně
             * zobrazen na konkrétní úrovní vzhledem ke kořenovému adresáři.
             *
             * Například se má rozbalit workspace/configuration/libTools/lib, tak se tento adresář nachází v listu
             * fileList, ten se iteruje a postupně se prochází veškeré soubory ve stromové struktuře, nejprve se
             * najde ve workspace configuration (1. položka v listu). pak libTools (2. položka v listu) a pak lib,
             * tyto hodnoty z listu se postupně vkládají níže do stringbuilderu, a hledá se konkrétní adresář na
             * konkrétní úrovni ve stromové struktuře.
             */
            final List<String> partsOfPath = getPartsOfPathToDestinationDir(f, rootFile);



            /*
             * Objekt pro "skládání" názvů adresářeů, které se mají rozbalit.
             *
             * Jde o to, že iterovaný adresář f je třeba rozdělit na části od kořenového adresáře ve stromové struktuře
             *
             * Například se má otevřít adresář:
             *
             * C:\\Users\krunc\Desktop\MyWorkspace\configuration\libTools\lib
             *
             * Ale kořenový adresář ve stromové struktuře je:
             *
             * C:\\Users\krunc\Desktop\MyWorkspace
             *
             * Proto se do téhoto builderu postupně vkládájí / přidávají názvy adresářů configuration, libTools a lib
             * tak, aby se ve stromové struktuře vždy rozbalil ten správný adresář.
             *
             * Tedy ve výchozím nastavení je adresář workspace rozbalen ve stromové struktuře a ten je třeba iterovat
             * - veškeré jeho podadresáře, takže ve stromové struktuře je nejprve třeba rozablit adresář
             * configuration, a v něm najít adresář libTools a ten také rozbalit a v něm najít adresář lib a ten
             * také rozbalit.
             */
            final StringBuilder partsOfDirName = new StringBuilder();
            partsOfDirName.append(rootFile.getAbsolutePath());

            partsOfPath.forEach(p -> {
                partsOfDirName.append(File.separator).append(p);

                /*
                 * Zde se projde adresářová struktura a pro každou položku na každé úrovní té struktury (vnořené
                 * adresáře) se otestuje, zda se shoduje s adreářem, který se má otevřít - vždy s konkrétní části,
                 * která odpovídá konkrétní úrovni vnořeného adresáře.
                 */
                for (int i = 0; i < getRowCount(); i++) {
                    // Právě iterovaná "komponenta" ve stromu:
                    final TreePath iterationPath = getPathForRow(i);

                    // Cesta k aktuálně iterovanému souboru / adresáři:
                    final String iterationPathInText = iterationPath.getLastPathComponent().toString();

                    /*
                     * Otestuji, zda se našel adresář, který se má rozbalit tak, že se otestuje shoda cest k souboru,
                      * pokud se našel požadovaný adresář k rozbalení, tak je možné skončit tento cyklus (další
                      * adresáře se hledat nebudou, na v jedném adresáři nemůže být více adresářů se stejným názvem).
                     */
                    if (iterationPathInText.equals(partsOfDirName.toString())) {
                        // Zda se mají rozbalit veškeré adresáře v konkrétním / iterovaném adresáři:
                        if (expandAllSubDirs)
                            expandCollapseSelectedDir(true, iterationPath);
                            // Zde se rozbalí pouze konkrétní / aktuálně iterovaný adresář:
                        else expandRow(i);

                        break;
                    }
                }
            });
        });
    }


    /**
     * Získání názvů adresářů od adresáře startDir po adresář destinationDir (získají se adresáře mezi těmito dvěma
     * cestami).
     * <p>
     * Například:
     * <p>
     * destinationDir: C:\\Users\krunc\Desktop\MyWorkspace\configuration\libTools\lib
     * <p>
     * startDir: C:\\Users\krunc\Desktop\MyWorkspace
     * <p>
     * výsledek budou adresáře mezi výše uvedenými cestami, tedy: configuration, pak libTools, a na konec lib.
     *
     * @param destinationDir
     *         - cílový adersář, ze kterého se mají "rozkouskovat" jednotlivé adresáře od adresáře startDir - je li na
     *         této cestě.
     * @param startDir
     *         - zdrojový adresář, od kterého se má začít vyhledávat / "kouskovat" adresáře na cestě destinationDir
     *
     * @return kolekci, která bude obsahovat výše zmíněné / popsané a ukázané adresáře, které se nachází mezi startDir a
     * destinationDir.
     */
    private static List<String> getPartsOfPathToDestinationDir(final File destinationDir, final File startDir) {
        final String desDirPath = destinationDir.getAbsolutePath();
        final String strDirPath = startDir.getAbsolutePath();

        /*
         * Toto by měla být zbytečná podmínka, ale pro případ, že by se nějakým způsobem zadala cesta, která úplně
         * není v kořenovém adreáři, tak by se zde mělo skončit.
         */
        if (strDirPath.length() > desDirPath.length())
            return new ArrayList<>();

        /*
         * V tomto případě není tato podmínka potřeba, ale v případě, že by ve stromové struktuře byl více než jeden
         * kořenový adresář, pak by byla tato podmínka potřeba, aby se nehledalo v jiné kořenévém adresáři.
         *
         * Jedná se o zjištění, zda je kořenový adresář, kde se má začít vyhledávat stejný jako část cesty k cílovému
          * adresáři, který semá rozbalit.
         */
        final String sub = desDirPath.substring(0, strDirPath.length());
        if (!sub.equals(strDirPath))
            return new ArrayList<>();


        /*
         * Získání názvů adresářů, které se mají vyhledávat ve stromové struktuře.
         *
         * Tedy rozdělení adresářů od adresáře workspace, aby se ve stromové struktuře vyhledávali vžýdy jednotlivé
         * podadresáře, které se mají rozbalit než se najde ten cílový.
         *
         * Například cílový adresář:
         *
         * C:\\Users\krunc\Desktop\MyWorkspace\configuration\libTools\lib
         *
         * A kořenový adresář ve stromové struktuře je:
         *
         * C:\\Users\krunc\Desktop\MyWorkspace
         *
         * Pak se cesta rozdělí dle lomítka dle aktuálně využívané platformy tak, aby se vyhledávaly vždy adresáře
         * configuration, pak libTools a v něm lib.
         */

        /*
         * Na konci je třeba odebrat lomítko, jinak by neprošlo porovnávání cest k adresářům se stromovou strukturou.
         *
         * Ale lomítko se odebere pouze v případě, že je cesta k cílovému adresáři větší než cesta k adresáři, kde se
          * má začíst "kouskovat", jinak by nastala výjimky, že není co odebrat StringIndexOutOfBoundsException.
         */
        final String rest;
        if (desDirPath.length() == strDirPath.length())
            rest = desDirPath.substring(strDirPath.length());
        else rest = desDirPath.substring(strDirPath.length() + 1);


        final String[] partsOfDirs = rest.split(Constants.OS_PATH_PATTERN);

        return new ArrayList<>(Arrays.asList(partsOfDirs));
    }


    @Override
    public void setLanguage(final Properties properties) {
        if (properties != null) {
            txtExpand = properties.getProperty("Pe_Mt_Txt_Expand", Constants.PE_MT_TXT_EXPAND);
            txtCollapse = properties.getProperty("Pe_Mt_Txt_Collapse", Constants.PE_MT_TXT_COLLAPSE);

            txtExpandAll = properties.getProperty("Pe_Mt_Txt_ExpandLeaf", Constants.PE_MT_TXT_EXPAND_LEAF);
            txtCollapseAll = properties.getProperty("Pe_Mt_Txt_CollapseLeaf", Constants.PE_MT_TXT_COLLAPSE_LEAF);
        }

        else {
            txtExpand = Constants.PE_MT_TXT_EXPAND;
            txtCollapse = Constants.PE_MT_TXT_COLLAPSE;

            txtExpandAll = Constants.PE_MT_TXT_EXPAND_LEAF;
            txtCollapseAll = Constants.PE_MT_TXT_COLLAPSE_LEAF;
        }
    }
}