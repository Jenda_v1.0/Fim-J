package cz.uhk.fim.fimj.project_explorer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;

/**
 * Tato třída slouží jako dialog - formulář pro zadání jména a zvolené přípony souboru, který chce uživatel vytvořit,
 * tento dialog je je řelk bych zbytečný, ale chctěl jsem uživateli dát možnost vytvoření nového souboru už v
 * průzkumniku projektů, když už by s ním pracoval, jinak je jasné, že si raději uživatel otevře klasický průzkumník
 * projektů či TotalComander, apod. kde je mnohem lepší práce s dokumenty, ale k tomu můj průzkumník projektů neslouží,
 * nicméně, jsem zde tedy dodělal možnost, že si uživatel může v rychlosti vytvořit nový soubor, pokud označí ve
 * stromové struktuře nejaký adresář, pak když klikne na vytvoření nového soubour, tak se zobrazí tento dialog, do
 * kterého se zadá název nového souboru, a zvolí se jedna možných přípon, coby typu souboru, které tato aplikace může
 * vytvořit,k resp. ty které umí vytvoit BufferedWriter který používám pro vytvoření souboru.
 * <p>
 * Název souboru je testovám dle regulárních výrazů, je možné zadat livolný název bez diakritiky a mezer
 * <p>
 * <p>
 * Pro zobrazení tohoto dialogu se zavolá metoda getNewFileInfo
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class NewFileForm extends NameFormAbstract implements LanguageInterface, ActionListener {

	private static final long serialVersionUID = 1L;


	/**
	 * Jednorozměrné pole, které slouží jako model pro JcomboBox, pro výběr přípony
	 * pro nový soubor, resp. pro typ souboru. Toto pole obsahuje veškeré přípony
	 * coby typy souborů, které lze vytvořit:
	 */
	private static final List<String> EXTENSIONS_MODEL = Collections
			.unmodifiableList(Arrays.asList(".java", ".docx", ".xlsx", ".xlsm", ".properties", ".txt", ".wps", ".dotx",
					".dotm", ".dot", ".docm", ".doc", ".xls", ".rtf", ".csv", ".dbf", ".svg", ".thmx", ".eml", ".mde",
					".msg", ".odf", ".scd", ".vcf", ".wtx", ".xlb", ".xlk", ".xll", ".xlm", ".xlv", ".xnk", ".xps"));
	
	
	/**
	 * JcheckBox, který po vytvoření bude obsahovat povolené přípony coby typy
	 * souborů, které lze vytvořit pomocí této aplikace
	 */
	private final JComboBox<String> cmbExtensions;
	

	
	/**
	 * Konstruktor této třídy, vytvoří Gui pro tento dialog, atd. 
	 */
	public NewFileForm() {
		super();
		
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/projectExplorerIcons/NewFileFormIcon.png",
				new Dimension(500, 220), true);
		
		
		
		gbc = createGbc();
		
		index = 0;
		
		
		
		gbc.gridwidth = 2;
		
		
		
		lblInfo = new JLabel("", JLabel.CENTER);
		setGbc(index, 0, lblInfo);
		
		
		
		
		lblError = new JLabel("", JLabel.CENTER);
		lblError.setFont(ERROR_FONT);
		lblError.setForeground(Color.RED);
		setGbc(++index, 0, lblError);
		lblError.setVisible(false);
		
		
		
		
		
		
		gbc.gridwidth = 1;
		txtName = new JTextField("FileName", 25);
		// Přidám reakci na tistknutí enteru:
		txtName.addKeyListener(getMyKeyAdapter());
		setGbc(++index, 0, txtName);
		
		
		
		
		cmbExtensions = new JComboBox<>(EXTENSIONS_MODEL.toArray(new String[] {}));
		cmbExtensions.setSize(20, 25);
		setGbc(index, 1, cmbExtensions);
		
		
		
		
		
		gbc.gridwidth = 2;
		
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		btnOk = new JButton();
		btnCancel = new JButton();
		
		btnOk.addActionListener(this);
		btnCancel.addActionListener(this);
		
		pnlButtons.add(btnOk);
		pnlButtons.add(btnCancel);
		
		setGbc(++index, 0, pnlButtons);
		
		
		
		pack();
		setLocationRelativeTo(null);
	}

	
	
	/**
	 * Metoda, která zobrazí tento dialog, který se následně stane mnodálním, a čeká
	 * na zavření dialogu nebo na kliknutí tlačítko ok, pak se otewstuje, zda
	 * uživatle vyplnil správně údaje - ve správné syntaxi.
	 */
	final String getNewFileInfo() {
		// zobrazím dialog a ten se následně stane modálním:
		setVisible(true);
		
		// otestuji, zda se kliknlo na tlačítko OK, pak bude variable = true,
		// jinak false, pokud se zavřel dialog - jakkoli
		if (variable)
			// Zde uživatel klikl na tlačítko OK, tak pokud se otestovali data  a jsou bez chyby,
			// pak vrátím údaje o názvu souboru a jeho příponě:
			return txtName.getText().trim() + cmbExtensions.getSelectedItem();
				
		return null;
	}







	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnOk)
			testData();
			
			
			else if (e.getSource() == btnCancel) {
				variable = false;
				dispose();						
			}
		}
	}
	
	
	



	@Override
	public void setLanguage(final Properties properties) {
		// zavolám metodu, která naplní zbývající proměnné:
		fillTextVariables(properties);
		
		if (properties != null) {
			setTitle(properties.getProperty("Pe_Nff_DialogTitle", Constants.PE_NFF_DIALOG_TITLE));
			
			lblInfo.setText(properties.getProperty("Pe_Nff_LblInfo", Constants.PE_NFF_LBL_INFO) + ":");
		}

		else {
			setTitle(Constants.PE_NFF_DIALOG_TITLE);
			
			lblInfo.setText(Constants.PE_NFF_LBL_INFO + ":");
		}
	}
}