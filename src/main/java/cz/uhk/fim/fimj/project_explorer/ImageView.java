package cz.uhk.fim.fimj.project_explorer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Dialog.ModalExclusionType;
import java.io.File;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JWindow;

import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;

/**
 * Tato třída slouží jako okno, ve kterém bude po najetí myši zobrazen obrázek v případě, že se jedná o obrázek s
 * příslušnou ikonou.
 * <p>
 * Tento dialog se se zobrazí v případě, že uživatel nejede myší na nějaký soubor (popsáno výše, ale nesmí to být
 * adresář) v dialogu průzkumník projektů.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

class ImageView extends JWindow {

	private static final long serialVersionUID = 1L;

	/**
	 * Přednastavená šířka okna.
	 */
	private static final int WIDTH = 350;

	/**
	 * Přednastavená výška okna.
	 */
	private static final int HEIGHT = 380;
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param file
	 *            - cesta k obrázku, který se má zobrazit.
	 * 
	 * @param locationX
	 *            - pozice / souřadnice X, kde se má zobrazit toto okno.
	 * 
	 * @param locationY
	 *            - pozice / souřadnice Y, kde se má zobrazit toto okno.
	 */
	ImageView(final File file, final int locationX, final int locationY) {
		super();

		setPreferredSize(new Dimension(WIDTH, HEIGHT));

		setLayout(new BorderLayout());

		setModalExclusionType(ModalExclusionType.TOOLKIT_EXCLUDE);
		
		
		
		
		
		try {
			// Následující způsob načtení obrázku občas vyhodil výjimku (níže):
			// ImageIcon image = new ImageIcon(ImageIO.read(file));

			// Tento způsob načtení obrázku při testování žádnou chybu nevyhodil:
			ImageIcon image = new ImageIcon(file.toURI().toURL());

			if ((image.getIconWidth() >= WIDTH || image.getIconHeight() >= HEIGHT))
				image = new ImageIcon(image.getImage().getScaledInstance(WIDTH - 5, HEIGHT - 5, Image.SCALE_SMOOTH));

			final JLabel lblImage = new JLabel();

			lblImage.setIcon(image);

			lblImage.setHorizontalAlignment(JLabel.CENTER);

			add(lblImage, BorderLayout.CENTER);

		} catch (MalformedURLException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informaci o chybě:
				logger.log(Level.INFO,
						"Zachycena výjimka při pokusu o získání souboru pro zobrazení jej v dialogu po najetí myší na " +
								"soubor, který představuje obrázek: "
								+ file.getAbsolutePath()
								+ ". Chyba vznikla při převodu objektu cesty v uvedeném objektu file na URL adresu pro" +
								" načtení obrázku. Tato chyba může nastat "
								+ "napříkla v případě, žekdy nebyl nalezen popisovač protokolu pro adresu URL nebo " +
								"pokud při vytváření adresy URL došlo k jiné chybě");

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();
		}

		
		
		
		
		
		
		
		
		
		
		
		/*
		 * Zde je možnost doplnit například další komponentu Jlabel, ve které bude
		 * zobrazena cesta k načtenému souboru, nebo je předkovi apod. ale k tomu
		 * využívám ToolTip, tak jej zde nechám zakomentován pouze jako potenciální
		 * možnosti, kdyby si někdo něco zkoušel s těmito zdrojovými kódy.
		 */
//		if (file.getParentFile() != null) {
//			final JLabel lblParent = new JLabel(file.getAbsolutePath());
//
//			lblParent.setHorizontalAlignment(JLabel.CENTER);
//
//			add(lblParent, BorderLayout.SOUTH);
//		}
		
			
			
		
		pack();
		setLocation(locationX, locationY);
		setVisible(true);
	}
	
	
	
	/*
	 * Následuje výjimky, která obsač vypadla při použití následující kódu pro
	 * načtení obrázku do dialogu
	 * 
	 * Kód:
	 * ImageIcon image = new ImageIcon(ImageIO.read(file));
	 * 
	 * 
	 * Výjimka:
	 * 
	 * Exception in thread "AWT-EventQueue-0" java.lang.NullPointerException at
	 * java.awt.color.ICC_Profile.intFromBigEndian(Unknown Source) at
	 * java.awt.color.ICC_Profile.getNumComponents(Unknown Source) at
	 * sun.java2d.cmm.lcms.LCMSTransform.<init>(Unknown Source) at
	 * sun.java2d.cmm.lcms.LCMS.createTransform(Unknown Source) at
	 * java.awt.color.ICC_ColorSpace.fromRGB(Unknown Source) at
	 * com.sun.imageio.plugins.jpeg.JPEGImageReader.setImageData(Unknown Source) at
	 * com.sun.imageio.plugins.jpeg.JPEGImageReader.readImageHeader(Native Method)
	 * at com.sun.imageio.plugins.jpeg.JPEGImageReader.readNativeHeader(Unknown
	 * Source) at
	 * com.sun.imageio.plugins.jpeg.JPEGImageReader.checkTablesOnly(Unknown Source)
	 * at com.sun.imageio.plugins.jpeg.JPEGImageReader.gotoImage(Unknown Source) at
	 * com.sun.imageio.plugins.jpeg.JPEGImageReader.readHeader(Unknown Source) at
	 * com.sun.imageio.plugins.jpeg.JPEGImageReader.readInternal(Unknown Source) at
	 * com.sun.imageio.plugins.jpeg.JPEGImageReader.read(Unknown Source) at
	 * javax.imageio.ImageIO.read(Unknown Source) at
	 * javax.imageio.ImageIO.read(Unknown Source) at
	 * cz.uhk.fim.fimj.projectExplorer.ImageView.<init>(ImageView.java:78) at
	 * cz.uhk.fim.fimj.projectExplorer.MyTree$2.mouseMoved(MyTree.java:235) at
	 * java.awt.AWTEventMulticaster.mouseMoved(Unknown Source) at
	 * java.awt.AWTEventMulticaster.mouseMoved(Unknown Source) at
	 * java.awt.Component.processMouseMotionEvent(Unknown Source) at
	 * javax.swing.JComponent.processMouseMotionEvent(Unknown Source) at
	 * java.awt.Component.processEvent(Unknown Source) at
	 * java.awt.Container.processEvent(Unknown Source) at
	 * java.awt.Component.dispatchEventImpl(Unknown Source) at
	 * java.awt.Container.dispatchEventImpl(Unknown Source) at
	 * java.awt.Component.dispatchEvent(Unknown Source) at
	 * java.awt.LightweightDispatcher.retargetMouseEvent(Unknown Source) at
	 * java.awt.LightweightDispatcher.processMouseEvent(Unknown Source) at
	 * java.awt.LightweightDispatcher.dispatchEvent(Unknown Source) at
	 * java.awt.Container.dispatchEventImpl(Unknown Source) at
	 * java.awt.Window.dispatchEventImpl(Unknown Source) at
	 * java.awt.Component.dispatchEvent(Unknown Source) at
	 * java.awt.EventQueue.dispatchEventImpl(Unknown Source) at
	 * java.awt.EventQueue.access$500(Unknown Source) at
	 * java.awt.EventQueue$3.run(Unknown Source) at
	 * java.awt.EventQueue$3.run(Unknown Source) at
	 * java.security.AccessController.doPrivileged(Native Method) at
	 * java.security.ProtectionDomain$JavaSecurityAccessImpl.doIntersectionPrivilege(Unknown
	 * Source) at
	 * java.security.ProtectionDomain$JavaSecurityAccessImpl.doIntersectionPrivilege(Unknown
	 * Source) at java.awt.EventQueue$4.run(Unknown Source) at
	 * java.awt.EventQueue$4.run(Unknown Source) at
	 * java.security.AccessController.doPrivileged(Native Method) at
	 * java.security.ProtectionDomain$JavaSecurityAccessImpl.doIntersectionPrivilege(Unknown
	 * Source) at java.awt.EventQueue.dispatchEvent(Unknown Source) at
	 * java.awt.EventDispatchThread.pumpOneEventForFilters(Unknown Source) at
	 * java.awt.EventDispatchThread.pumpEventsForFilter(Unknown Source) at
	 * java.awt.EventDispatchThread.pumpEventsForFilter(Unknown Source) at
	 * java.awt.WaitDispatchSupport$2.run(Unknown Source) at
	 * java.awt.WaitDispatchSupport$4.run(Unknown Source) at
	 * java.awt.WaitDispatchSupport$4.run(Unknown Source) at
	 * java.security.AccessController.doPrivileged(Native Method) at
	 * java.awt.WaitDispatchSupport.enter(Unknown Source) at
	 * java.awt.Dialog.show(Unknown Source) at java.awt.Component.show(Unknown
	 * Source) at java.awt.Component.setVisible(Unknown Source) at
	 * java.awt.Window.setVisible(Unknown Source) at
	 * java.awt.Dialog.setVisible(Unknown Source) at
	 * cz.uhk.fim.fimj.projectExplorer.ProjectExplorerDialog.<init>(ProjectExplorerDialog.java:241)
	 * at cz.uhk.fim.fimj.jMenu.Menu.actionPerformed(Menu.java:2198) at
	 * javax.swing.AbstractButton.fireActionPerformed(Unknown Source) at
	 * javax.swing.AbstractButton$Handler.actionPerformed(Unknown Source) at
	 * javax.swing.DefaultButtonModel.fireActionPerformed(Unknown Source) at
	 * javax.swing.DefaultButtonModel.setPressed(Unknown Source) at
	 * javax.swing.AbstractButton.doClick(Unknown Source) at
	 * javax.swing.AbstractButton.doClick(Unknown Source) at
	 * javax.swing.plaf.basic.BasicMenuItemUI$Actions.actionPerformed(Unknown
	 * Source) at javax.swing.SwingUtilities.notifyAction(Unknown Source) at
	 * javax.swing.JComponent.processKeyBinding(Unknown Source) at
	 * javax.swing.JMenuBar.processBindingForKeyStrokeRecursive(Unknown Source) at
	 * javax.swing.JMenuBar.processBindingForKeyStrokeRecursive(Unknown Source) at
	 * javax.swing.JMenuBar.processBindingForKeyStrokeRecursive(Unknown Source) at
	 * javax.swing.JMenuBar.processBindingForKeyStrokeRecursive(Unknown Source) at
	 * javax.swing.JMenuBar.processBindingForKeyStrokeRecursive(Unknown Source) at
	 * javax.swing.JMenuBar.processKeyBinding(Unknown Source) at
	 * javax.swing.KeyboardManager.fireBinding(Unknown Source) at
	 * javax.swing.KeyboardManager.fireKeyboardAction(Unknown Source) at
	 * javax.swing.JComponent.processKeyBindingsForAllComponents(Unknown Source) at
	 * javax.swing.JComponent.processKeyBindings(Unknown Source) at
	 * javax.swing.JComponent.processKeyEvent(Unknown Source) at
	 * java.awt.Component.processEvent(Unknown Source) at
	 * java.awt.Container.processEvent(Unknown Source) at
	 * java.awt.Component.dispatchEventImpl(Unknown Source) at
	 * java.awt.Container.dispatchEventImpl(Unknown Source) at
	 * java.awt.Component.dispatchEvent(Unknown Source) at
	 * java.awt.KeyboardFocusManager.redispatchEvent(Unknown Source) at
	 * java.awt.DefaultKeyboardFocusManager.dispatchKeyEvent(Unknown Source) at
	 * java.awt.DefaultKeyboardFocusManager.preDispatchKeyEvent(Unknown Source) at
	 * java.awt.DefaultKeyboardFocusManager.typeAheadAssertions(Unknown Source) at
	 * java.awt.DefaultKeyboardFocusManager.dispatchEvent(Unknown Source) at
	 * java.awt.Component.dispatchEventImpl(Unknown Source) at
	 * java.awt.Container.dispatchEventImpl(Unknown Source) at
	 * java.awt.Window.dispatchEventImpl(Unknown Source) at
	 * java.awt.Component.dispatchEvent(Unknown Source) at
	 * java.awt.EventQueue.dispatchEventImpl(Unknown Source) at
	 * java.awt.EventQueue.access$500(Unknown Source) at
	 * java.awt.EventQueue$3.run(Unknown Source) at
	 * java.awt.EventQueue$3.run(Unknown Source) at
	 * java.security.AccessController.doPrivileged(Native Method) at
	 * java.security.ProtectionDomain$JavaSecurityAccessImpl.doIntersectionPrivilege(Unknown
	 * Source) at
	 * java.security.ProtectionDomain$JavaSecurityAccessImpl.doIntersectionPrivilege(Unknown
	 * Source) at java.awt.EventQueue$4.run(Unknown Source) at
	 * java.awt.EventQueue$4.run(Unknown Source) at
	 * java.security.AccessController.doPrivileged(Native Method) at
	 * java.security.ProtectionDomain$JavaSecurityAccessImpl.doIntersectionPrivilege(Unknown
	 * Source) at java.awt.EventQueue.dispatchEvent(Unknown Source) at
	 * java.awt.EventDispatchThread.pumpOneEventForFilters(Unknown Source) at
	 * java.awt.EventDispatchThread.pumpEventsForFilter(Unknown Source) at
	 * java.awt.EventDispatchThread.pumpEventsForHierarchy(Unknown Source) at
	 * java.awt.EventDispatchThread.pumpEvents(Unknown Source) at
	 * java.awt.EventDispatchThread.pumpEvents(Unknown Source) at
	 * java.awt.EventDispatchThread.run(Unknown Source)
	 */
}
