package cz.uhk.fim.fimj.project_explorer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Arrays;
import java.util.Properties;

import javax.swing.*;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.code_editor.MenuInterface;
import cz.uhk.fim.fimj.code_editor.MenuSharedMethods;
import cz.uhk.fim.fimj.file.*;
import cz.uhk.fim.fimj.print.Printer;

/**
 * Tato třída slouží jako menu - hlavní menu v dialogu pro Project Explorer - Průzkumník projektů.
 * <p>
 * Note: Možná by bylo vhodné vytvořit nějakou abstraktní třídu pro tuto třídu a třídu JMenuForCodeEditor. Jednalo by se
 * pouze o třídu, kde by byla deklarace některých společných položek pro menu, ale to je asi tak vše, dále by tam mohla
 * býtmetoda pro přísluná tlačítka, který by jim nastavila klávesové zkratky a obrázky, ale to je asi tak vše, navíc, se
 * jedná pouze o uvedená dvě menu, a žádný další editoru kódu vytváře nebudu, k čemu taky? Jsou zde dva, a když už něco,
 * tak by stačilo jeden z nich doplnit o nové funkce nebo tak něco, ale proč vytvářet další.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class Menu extends JMenuBar implements LanguageInterface, ActionListener {

    private static final long serialVersionUID = 3431545351269923246L;


    /**
     * "komponenta", která obsahuje "sdílené" metody pro menu, metody pro manipulaci s textem jako pomůcky pro
     * programování
     */
    private static MenuInterface menuMethods;


    /**
     * Proměnná pro vlákno, které slouží pro doplnění hodnot do dialogu pro auto - doplňování kodu.
     * <p>
     * Tato proměnná je zde pro "uložení" vlákna v případě, že bude "běžet", abych mohl testovat, zda to vlákno běží a
     * pokud ano, tak aby se nevytvářelo další, protože když bude uživatel opakované rychle mačkat klávesu pro
     * kompilování - resp. příslušné tlačítko v menu nebo jeho klávesovou zkratku, tak by mohlo dojít k chybě, že by se
     * například již pokoušela našíst v nějakém z vláken přeložená / zkompilovaná třída, kterou právě využívá jiné
     * vlákno - to samé, jen dříve spuštěné.
     */
    private static RefreshAutoCompletionThread refreshAutoCompletionThread;


    // Hlavní menu:
    private JMenu menuFile;
    private JMenu menuEdit;
    private JMenu menuTools;
    private JMenu menuOrderFrames;


    // Položky do menu File:
    /**
     * otevře dialogové okno pro výběr souboru, která se má otevřit v novém interním okně
     */
    private JMenuItem itemOpenFile;
    /**
     * zavře dialog
     */
    private JMenuItem itemClose;
    /**
     * uloží označený soubor / soubor v označeném interním okně
     */
    private JMenuItem itemSave;
    /**
     * otevře dialogovo okno pro uloženi označeného okna - souboru jako a vybere si cestu a název
     */
    private JMenuItem itemSaveAs;
    /**
     * uloží všechny otevřené soubory v desktopPane - veškerá oteřená interní okna
     */
    private JMenuItem itemSaveAll;
    /**
     * Zobrazí okno pro nastavení možností tisku označeného souboru / interního okna.
     */
    private JMenuItem itemPrint;
    /**
     * zavře se označené nterní okno
     */
    private JMenuItem itemCloseSelectedWindow;
    /**
     * zavřou se veškerá otevřená interní okna
     */
    private JMenuItem itemCloseAllWindow;
    /**
     * Přenačte soubor v označeném souboru / interním okně.
     */
    private JMenuItem itemReload;
    /**
     * přenačtou se veškerá okna - otevřená v desktopPane
     */
    private JMenuItem itemReloadAll;


    /**
     * Nastavení, zda se má zalomit / zalamovat text v označeném souboru / interním okně.
     */
    private JRadioButtonMenuItem rbWrapTextInSelectedFrame;
    /**
     * Nastavení, zda se má zalomit / zalamovat text ve všech otevřených souborech / interních oknech.
     */
    private JRadioButtonMenuItem rbWrapTextInAllFrames;


    /**
     * Nastavení, zda se mají zobrazovat bílé znaky v označeném interním okně / souboru.
     */
    private JRadioButtonMenuItem rbShowWhiteSpaceInSelectedFrame;
    /**
     * Nastavení, zda se mají zobrazovat bílé znaky ve všech interních oknech / otevřených souborech.
     */
    private JRadioButtonMenuItem rbShowWhiteSpaceInAllFrames;

    /**
     * Nastavení, zda se mají odebírat bílé znaky v označeném interním okně / souboru.
     *
     * <i>Po stisknutí klávesy "Enter" se (ne) odeberou bílé znaky, které se nacházely na řádku, kde byl kurzor při
     * stisku klávesy "Enter". Pokud se na tom řádku nachází pouze bílé znaky.</i>
     */
    private JRadioButtonMenuItem rbClearWhiteSpaceLineInSelectedFrame;
    /**
     * Nastavení, zda se mají odebírat bílé znaky ve všech interních oknech / otevřených souborech.
     *
     * <i>Po stisknutí klávesy "Enter" se (ne) odeberou bílé znaky, které se nacházely na řádku, kde byl kurzor při
     * stisku klávesy "Enter". Pokud se na tom řádku nachází pouze bílé znaky.</i>
     */
    private JRadioButtonMenuItem rbClearWhiteSpaceLineInAllFrames;


    // Položky do menu menuEdit:
    private JMenuItem itemIndentMore;
    private JMenuItem itemIndentLess;
    private JMenuItem itemComment;
    private JMenuItem itemUncomment;
    private JMenuItem itemFormatCode;
    private JMenuItem itemInsertMethod;


    // Položky do menuTools:
    private JMenuItem itemReplaceText;
    private JMenuItem itemGoToLine;
    private JMenuItem itemToString;
    private JMenuItem itemGettersAndSetters;
    private JMenuItem itemConstructor;
    private JMenuItem itemCompile;


    // Položky do menuOrderFrames:
    private JMenuItem itemTile;
    private JMenuItem itemTileUnderHim;
    private JMenuItem itemCascade;
    private JMenuItem itemMinimizeAllFrames;


    /**
     * Menu pro položky, které otevřou označený otevřený soubor v průzkumníku souborů definovaném v používaném OS a nebo
     * ve výchozí aplikaci zvolené pro otevření příslušného typu souboru.
     */
    private JMenu menuOpenFileInAppsOfOs;

    /**
     * Otevření označeného souboru v průzkumníku projektů definovaném v OS.
     */
    private JMenuItem itemOpenFileInFileExplorerInOs;
    /**
     * Otevření označeného souboru ve výchozí aplikaci zvolené v OS pro otevření příslušného typu souboru.
     */
    private JMenuItem itemOpenFileInDefaultAppInOs;


    /**
     * Tato komponenta slouží jako menu pro položky pro vygenerování nebo přidání výchozího ID pro seriazlizaci
     * objektů.
     */
    private JMenu menuAddSerialVersionID;


    /**
     * Přidání výchozího serial ID
     */
    private JMenuItem itemAddDefaultSerialVersionID;
    /**
     * Vygenerování serial ID.
     */
    private JMenuItem itemAddGeneratedSerialVersionId;


    /**
     * Tato promenná slouží pro uložení reference na soubor, který obsahuje texty v uživatelem zvoleném jazyce pro
     * aplikaci, je zde potřeba, protože potřebuji tyto texty předat dále do třídy pro kompilaci.
     */
    private final Properties languageProperties;


    /**
     * Proměnná, která ukazuje na instanci třídy ProjectExplorerDialog, která je zded potřebna kvůli volání některých
     * metod v dané třídě
     */
    private final ProjectExplorerDialog projectExplorer;


    // Proměnné pro texty pro toto menu ve zvoleném jazyce. Například texty pro chybové hlášky:
    private String txtCannotSavePdfFileText;
    private String txtLocation;
    private String txtCannotSavePdfFileTitle;
    private String txtFileCouldNotBeSavedText;
    private String txtFileCouldNotBeSavedTitle;
    private String txtContainsAreDifferentText;
    private String txtContainsAreDifferentTitle;
    private String txtFileDoesNotExistText;
    private String txtOriginalLocation;
    private String txtFileDoesNotExistTitle;
    private String txtClassFileIsNotLoadedText;
    private String txtClassFileIsNotLoadedTitle;
    private String txtCannotGenerateAccessMethodsText;
    private String txtClassIsNotInClassDiagram;
    private String txtCannotGenerateConstructor;
    private String txtClassDoesNotExistAlreadyText;
    private String txtClassDoesNotExistAlreadyTitle;
    private String txtSrcDirIsMissingText;
    private String txtSrcDirIsMissingTitle;


    /**
     * Konstruktor třídy.
     *
     * @param projectExplorer
     *         - reference na instanci třídy {@link ProjectExplorerDialog}
     * @param languageProperties
     *         - soubor s texty pro aplikaci ve zvoleném jazyce
     */
    public Menu(final ProjectExplorerDialog projectExplorer, final Properties languageProperties) {
        this.projectExplorer = projectExplorer;
        this.languageProperties = languageProperties;

        menuMethods = new MenuSharedMethods(languageProperties);


        createMenuItems();


        setIconsForMenuItems();


        setShortcutForMenuItems();


        setLanguage(languageProperties);
    }


    /**
     * Metoda, která vytvoří hlavní položky v menu, včetně těch hlavních menu
     */
    private void createMenuItems() {
        // Inicializace hlavních menu:
        menuFile = new JMenu();
        menuEdit = new JMenu();
        menuTools = new JMenu();
        menuOrderFrames = new JMenu();

        // přidání hlavních menu:
        add(menuFile);
        add(menuEdit);
        add(menuTools);
        add(menuOrderFrames);


        // položky do menuFile:
        itemOpenFile = new JMenuItem();
        menuOpenFileInAppsOfOs = new JMenu();
        itemSave = new JMenuItem();
        itemSaveAs = new JMenuItem();
        itemSaveAll = new JMenuItem();
        rbWrapTextInSelectedFrame = new JRadioButtonMenuItem();
        rbWrapTextInAllFrames = new JRadioButtonMenuItem();
        rbShowWhiteSpaceInSelectedFrame = new JRadioButtonMenuItem();
        rbShowWhiteSpaceInAllFrames = new JRadioButtonMenuItem();
        rbClearWhiteSpaceLineInSelectedFrame = new JRadioButtonMenuItem();
        rbClearWhiteSpaceLineInAllFrames = new JRadioButtonMenuItem();
        itemReload = new JMenuItem();
        itemReloadAll = new JMenuItem();
        itemPrint = new JMenuItem();
        itemCloseSelectedWindow = new JMenuItem();
        itemCloseAllWindow = new JMenuItem();
        itemClose = new JMenuItem();

        // přidání událostí na tlačítka:
        itemOpenFile.addActionListener(this);
        itemSave.addActionListener(this);
        itemSaveAs.addActionListener(this);
        itemSaveAll.addActionListener(this);
        rbWrapTextInSelectedFrame.addActionListener(this);
        rbWrapTextInAllFrames.addActionListener(this);
        rbShowWhiteSpaceInSelectedFrame.addActionListener(this);
        rbShowWhiteSpaceInAllFrames.addActionListener(this);
        rbClearWhiteSpaceLineInSelectedFrame.addActionListener(this);
        rbClearWhiteSpaceLineInAllFrames.addActionListener(this);
        itemReload.addActionListener(this);
        itemReloadAll.addActionListener(this);
        itemPrint.addActionListener(this);
        itemCloseSelectedWindow.addActionListener(this);
        itemCloseAllWindow.addActionListener(this);
        itemClose.addActionListener(this);

        // Přidání tlačítek - položek do menuFile:
        menuFile.add(itemOpenFile);
        menuFile.add(menuOpenFileInAppsOfOs);
        menuFile.addSeparator();
        menuFile.add(itemSave);
        menuFile.add(itemSaveAs);
        menuFile.add(itemSaveAll);
        menuFile.addSeparator();
        menuFile.add(rbWrapTextInSelectedFrame);
        menuFile.add(rbWrapTextInAllFrames);
        menuFile.addSeparator();
        menuFile.add(rbShowWhiteSpaceInSelectedFrame);
        menuFile.add(rbShowWhiteSpaceInAllFrames);
        menuFile.addSeparator();
        menuFile.add(rbClearWhiteSpaceLineInSelectedFrame);
        menuFile.add(rbClearWhiteSpaceLineInAllFrames);
        menuFile.addSeparator();
        menuFile.add(itemReload);
        menuFile.add(itemReloadAll);
        menuFile.addSeparator();
        menuFile.add(itemPrint);
        menuFile.addSeparator();
        menuFile.add(itemCloseSelectedWindow);
        menuFile.add(itemCloseAllWindow);
        menuFile.addSeparator();
        menuFile.add(itemClose);


        // Položky do menu menuOpenFileInAppsOfOs:
        itemOpenFileInFileExplorerInOs = new JMenuItem();
        itemOpenFileInDefaultAppInOs = new JMenuItem();

        itemOpenFileInFileExplorerInOs.addActionListener(this);
        itemOpenFileInDefaultAppInOs.addActionListener(this);

        menuOpenFileInAppsOfOs.add(itemOpenFileInFileExplorerInOs);
        menuOpenFileInAppsOfOs.addSeparator();
        menuOpenFileInAppsOfOs.add(itemOpenFileInDefaultAppInOs);


        // Položky do menuEdit:
        itemIndentMore = new JMenuItem();
        itemIndentLess = new JMenuItem();
        itemComment = new JMenuItem();
        itemUncomment = new JMenuItem();
        itemFormatCode = new JMenuItem();
        itemInsertMethod = new JMenuItem();

        itemIndentMore.addActionListener(this);
        itemIndentLess.addActionListener(this);
        itemComment.addActionListener(this);
        itemUncomment.addActionListener(this);
        itemFormatCode.addActionListener(this);
        itemInsertMethod.addActionListener(this);

        menuEdit.add(itemIndentMore);
        menuEdit.add(itemIndentLess);
        menuEdit.addSeparator();
        menuEdit.add(itemComment);
        menuEdit.add(itemUncomment);
        menuEdit.addSeparator();
        menuEdit.add(itemFormatCode);
        menuEdit.addSeparator();
        menuEdit.add(itemInsertMethod);


        // Položky do menuTools:

        // Menu pro vygenerování nebo přidání ID pro seralizaci:
        menuAddSerialVersionID = new JMenu();

        itemAddDefaultSerialVersionID = new JMenuItem();
        itemAddGeneratedSerialVersionId = new JMenuItem();

        itemAddDefaultSerialVersionID.addActionListener(this);
        itemAddGeneratedSerialVersionId.addActionListener(this);

        menuAddSerialVersionID.add(itemAddDefaultSerialVersionID);
        menuAddSerialVersionID.addSeparator();
        menuAddSerialVersionID.add(itemAddGeneratedSerialVersionId);

        // Položky od menu menuTools:
        itemReplaceText = new JMenuItem();
        itemGoToLine = new JMenuItem();
        itemToString = new JMenuItem();
        itemGettersAndSetters = new JMenuItem();
        itemConstructor = new JMenuItem();
        itemCompile = new JMenuItem();

        itemReplaceText.addActionListener(this);
        itemGoToLine.addActionListener(this);
        itemToString.addActionListener(this);
        itemGettersAndSetters.addActionListener(this);
        itemConstructor.addActionListener(this);
        itemCompile.addActionListener(this);

        menuTools.add(itemReplaceText);
        menuTools.add(itemGoToLine);
        menuTools.addSeparator();
        menuTools.add(itemToString);
        menuTools.add(itemGettersAndSetters);
        menuTools.add(itemConstructor);
        menuTools.addSeparator();
        menuTools.add(menuAddSerialVersionID);
        menuTools.addSeparator();
        menuTools.add(itemCompile);


        // Položky do menuOrderFrames:
        itemTile = new JMenuItem();
        itemTileUnderHim = new JMenuItem();
        itemCascade = new JMenuItem();
        itemMinimizeAllFrames = new JMenuItem();

        itemTile.addActionListener(this);
        itemTileUnderHim.addActionListener(this);
        itemCascade.addActionListener(this);
        itemMinimizeAllFrames.addActionListener(this);

        menuOrderFrames.add(itemTile);
        menuOrderFrames.add(itemTileUnderHim);
        menuOrderFrames.addSeparator();
        menuOrderFrames.add(itemCascade);
        menuOrderFrames.addSeparator();
        menuOrderFrames.add(itemMinimizeAllFrames);
    }


    /**
     * Metoda, která nastaví ikony do položek v tomto menu
     */
    private void setIconsForMenuItems() {
        itemOpenFile.setIcon(GetIconInterface.getMyIcon("/icons/projectExplorerIcons/menu/OpenFileIcon.png", true));
        itemOpenFileInFileExplorerInOs.setIcon(GetIconInterface.getMyIcon("/icons/app/FileExplorerIcon.png", true));
        itemOpenFileInDefaultAppInOs.setIcon(GetIconInterface.getMyIcon("/icons/app/DefaultAppIcon.png", true));
        itemClose.setIcon(GetIconInterface.getMyIcon("/icons/projectExplorerIcons/menu/CloseIcon.png", true));
        itemSave.setIcon(GetIconInterface.getMyIcon("/icons/projectExplorerIcons/menu/SaveIcon.png", true));
        itemSaveAs.setIcon(GetIconInterface.getMyIcon("/icons/projectExplorerIcons/menu/SaveAsIcon.jpg", true));
        itemSaveAll.setIcon(GetIconInterface.getMyIcon("/icons/projectExplorerIcons/menu/SaveAllIcon.jpg", true));
        itemPrint.setIcon(GetIconInterface.getMyIcon("/icons/projectExplorerIcons/menu/PrintIcon.png", true));
        itemCloseSelectedWindow.setIcon(GetIconInterface.getMyIcon("/icons/projectExplorerIcons/menu/CloseFrame.png",
                true));
        itemCloseAllWindow.setIcon(GetIconInterface.getMyIcon("/icons/projectExplorerIcons/menu/CloseAllFrames.png",
                true));
        itemReload.setIcon(GetIconInterface.getMyIcon("/icons/projectExplorerIcons/menu/RefreshIcon.png", true));
        itemReloadAll.setIcon(GetIconInterface.getMyIcon("/icons/projectExplorerIcons/menu/RefreshIcon.png", true));
        itemIndentMore.setIcon(GetIconInterface.getMyIcon("/icons/app/IndentMoreIcon.png", true));
        itemIndentLess.setIcon(GetIconInterface.getMyIcon("/icons/app/IndentLessIcon.png", true));
        itemComment.setIcon(GetIconInterface.getMyIcon("/icons/app/CommentIcon.jpg", true));
        itemUncomment.setIcon(GetIconInterface.getMyIcon("/icons/app/UncommentIcon.jpg", true));
        itemFormatCode.setIcon(GetIconInterface.getMyIcon("/icons/app/FormatCodeIcon.png", true));
        itemInsertMethod.setIcon(GetIconInterface.getMyIcon("/icons/app/InsertMethodIcon.png", true));
        itemReplaceText.setIcon(GetIconInterface.getMyIcon("/icons/app/FindAndReplaceIcon.png", true));
        itemGoToLine.setIcon(GetIconInterface.getMyIcon("/icons/app/GoToLineIcon.jpeg", true));
        itemToString.setIcon(GetIconInterface.getMyIcon("/icons/app/ToStringIcon.png", true));
        itemGettersAndSetters.setIcon(GetIconInterface.getMyIcon("/icons/app/GettersAndSettersIcon.png", true));
        itemConstructor.setIcon(GetIconInterface.getMyIcon("/icons/app/ConstructorIcon.png", true));

        itemAddDefaultSerialVersionID.setIcon(GetIconInterface.getMyIcon("/icons/app/GenerateSeriaVersionIdIcon.png",
                true));
        itemAddGeneratedSerialVersionId.setIcon(GetIconInterface.getMyIcon("/icons/app/GenerateSeriaVersionIdIcon" +
                ".png", true));

        itemCompile.setIcon(GetIconInterface.getMyIcon("/icons/app/CompileIcon.png", true));
        itemTile.setIcon(GetIconInterface.getMyIcon("/icons/projectExplorerIcons/menu/OrderByTileIcon.png", true));
        itemTileUnderHim.setIcon(GetIconInterface.getMyIcon("/icons/projectExplorerIcons/menu/OrderByTileUnderHimIcon" +
                ".png", true));
        itemCascade.setIcon(GetIconInterface.getMyIcon("/icons/projectExplorerIcons/menu/OrderByCascadeIcon.png",
                true));
        itemMinimizeAllFrames.setIcon(GetIconInterface.getMyIcon("/icons/projectExplorerIcons/menu" +
                "/MinimizeAllFramesIcon.png", true));
    }


    /**
     * Metoda, která nastaví zkratky pro položky vmenu zkratky = klávesové zkratky
     */
    private void setShortcutForMenuItems() {
        // Ctrl + O
        itemOpenFile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));

        // Alt + F4
        itemClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_DOWN_MASK));

        // Ctrl + S
        itemSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));

        // Ctrl + Shift + S
        itemSaveAll.setAccelerator(KeyStroke.getKeyStroke("control shift S"));

        // Ctrl + P
        itemPrint.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_DOWN_MASK));

        // Ctrl + W
        itemCloseSelectedWindow.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_DOWN_MASK));

        // Ctrl + Shift + W
        itemCloseAllWindow.setAccelerator(KeyStroke.getKeyStroke("control shift W"));

        // Ctrl + R
        itemReload.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_DOWN_MASK));

        // Ctrl + Shift + R
        itemReloadAll.setAccelerator(KeyStroke.getKeyStroke("control shift R"));

        // Ctrl + I
        itemComment.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_DOWN_MASK));

        // Ctrl + U
        itemUncomment.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, InputEvent.CTRL_DOWN_MASK));

        // Ctrl + Shift + F
        itemFormatCode.setAccelerator(KeyStroke.getKeyStroke("control shift F"));

        // Ctrl + T
        itemGoToLine.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_DOWN_MASK));

        // shift + F6
        itemCompile.setAccelerator(KeyStroke.getKeyStroke("shift F6"));

        // Ctrl + M
        itemTile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, InputEvent.CTRL_DOWN_MASK));

        // Ctrl + N
        itemTileUnderHim.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));

        // Ctrl + B
        itemCascade.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_DOWN_MASK));

        // Ctrl + Y
        itemMinimizeAllFrames.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, InputEvent.CTRL_DOWN_MASK));
    }


    @Override
    public void setLanguage(final Properties properties) {

        if (properties != null) {
            // Texty do hlavních menu:
            menuFile.setText(properties.getProperty("Pe_M_MenuFile", Constants.PE_M_MENU_FILE));
            menuEdit.setText(properties.getProperty("Pe_M_MenuEdit", Constants.PE_M_MENU_EDIT));
            menuTools.setText(properties.getProperty("Pe_M_MenuTools", Constants.PE_M_MENU_TOOLS));
            menuOrderFrames.setText(properties.getProperty("Pe_M_MenuOrderFrames", Constants.PE_M_MENU_ORDER_FRAMES));


            // texty do pložek v menuFile:
            itemOpenFile.setText(properties.getProperty("Pe_M_ItemOpenFile", Constants.PE_M_ITEM_OPEN_FILE));
            menuOpenFileInAppsOfOs.setText(properties.getProperty("Pe_M_MenuOpenFileInAppsOfOs",
                    Constants.PE_M_MENU_OPEN_FILE_IN_APPS_OF_OS));
            itemOpenFileInFileExplorerInOs.setText(properties.getProperty("Pe_M_ItemOpenFileInFileExplorerInOs",
                    Constants.PE_M_ITEM_OPEN_FILE_IN_FILE_EXPLORER_IN_OS));
            itemOpenFileInDefaultAppInOs.setText(properties.getProperty("Pe_M_ItemOpenFileInDefaultAppInOs",
                    Constants.PE_M_ITEM_OPEN_FILE_IN_DEFAULT_APP_IN_OS));
            itemSave.setText(properties.getProperty("Pe_M_ItemSave", Constants.PE_M_ITEM_SAVE));
            itemSaveAs.setText(properties.getProperty("Pe_M_ItemSaveAs", Constants.PE_M_ITEM_SAVE_AS));
            itemSaveAll.setText(properties.getProperty("Pe_M_ItemSaveAll", Constants.PE_M_ITEM_SAVE_ALL));
            rbWrapTextInSelectedFrame.setText(properties.getProperty("Pe_M_Rb_Item_WrapTextInSelectedFrame",
                    Constants.PE_M_RB_ITEM_WRAP_TEXT_IN_SELECTED_FRAME));
            rbWrapTextInAllFrames.setText(properties.getProperty("Pe_M_Rb_Item_WrapTextInAllFrames",
                    Constants.PE_M_RB_ITEM_WRAP_TEXT_IN_ALL_FRAMES));
            rbShowWhiteSpaceInSelectedFrame.setText(properties.getProperty(
                    "Pe_M_Rb_Item_ShowWhiteSpaceInSelectedFrame",
                    Constants.PE_M_RB_ITEM_SHOW_WHITE_SPACE_IN_SELECTED_FRAME));
            rbShowWhiteSpaceInAllFrames.setText(properties.getProperty("Pe_M_Rb_Item_ShowWhiteSpaceInAllFrames",
                    Constants.PE_M_RB_ITEM_SHOW_WHITE_SPACE_IN_ALL_FRAMES));
            rbClearWhiteSpaceLineInSelectedFrame.setText(properties.getProperty(
                    "Pe_M_Rb_Item_ClearWhiteSpaceLineInSelectedFrame",
                    Constants.PE_M_RB_ITEM_CLEAR_WHITE_SPACE_LINE_IN_SELECTED_FRAME));
            rbClearWhiteSpaceLineInAllFrames.setText(properties.getProperty(
                    "Pe_M_Rb_Item_ClearWhiteSpaceLineInAllFrames",
                    Constants.PE_M_RB_ITEM_CLEAR_WHITE_SPACE_LINE_IN_ALL_FRAMES));
            itemReload.setText(properties.getProperty("Pe_M_ItemReload", Constants.PE_M_ITEM_RELOAD));
            itemReloadAll.setText(properties.getProperty("Pe_M_ItemReloadAll", Constants.PE_M_ITEM_RELOAD_ALL));
            itemPrint.setText(properties.getProperty("Pe_M_ItemPrint", Constants.PE_M_ITEM_PRINT));
            itemCloseSelectedWindow.setText(properties.getProperty("Pe_M_ItemCloseSelectedWindow",
                    Constants.PE_M_ITEM_CLOSE_SELECTED_WINDOW));
            itemCloseAllWindow.setText(properties.getProperty("Pe_M_ItemCloseAllWindow",
                    Constants.PE_M_ITEM_CLOSE_ALL_WINDOW));
            itemClose.setText(properties.getProperty("Pe_M_ItemClose", Constants.PE_M_ITEM_CLOSE));

            // Popisky tlačítek v menuFile:
            itemOpenFile.setToolTipText(properties.getProperty("Pe_M_TT_ItemOpenFile",
                    Constants.PE_M_TT_ITEM_OPEN_FILE));
            itemOpenFileInFileExplorerInOs.setToolTipText(properties.getProperty(
                    "Pe_M_TT_ItemOpenFileInFileExplorerInOs", Constants.PE_M_TT_ITEM_OPEN_FILE_IN_FILE_EXPLORER_IN_OS));
            itemOpenFileInDefaultAppInOs.setToolTipText(properties.getProperty("Pe_M_TT_ItemOpenFileInDefaultAppInOs"
                    , Constants.PE_M_TT_ITEM_OPEN_FILE_IN_DEFAULT_APP_IN_OS));
            itemSave.setToolTipText(properties.getProperty("Pe_M_TT_ItemSave", Constants.PE_M_TT_ITEM_SAVE));
            itemSaveAs.setToolTipText(properties.getProperty("Pe_M_TT_ItemSaveAs", Constants.PE_M_TT_ITEM_SAVE_AS));
            itemSaveAll.setToolTipText(properties.getProperty("Pe_M_TT_ItemSaveAll", Constants.PE_M_TT_ITEM_SAVE_ALL));
            rbWrapTextInSelectedFrame.setToolTipText(properties.getProperty("Pe_M_TT_Rb_ItemWrapTextInSelectedFrame",
                    Constants.PE_M_TT_RB_ITEM_WRAP_TEXT_IN_SELECTED_FRAME));
            rbWrapTextInAllFrames.setToolTipText(properties.getProperty("Pe_M_TT_Rb_ItemWrapTextInAllFrames",
                    Constants.PE_M_TT_RB_ITEM_WRAP_TEXT_IN_ALL_FRAMES));
            rbShowWhiteSpaceInSelectedFrame.setToolTipText(properties.getProperty(
                    "Pe_M_TT_Rb_ItemShowWhiteSpaceInSelectedFrame",
                    Constants.PE_M_TT_RB_ITEM_SHOW_WHITE_SPACE_IN_SELECTED_FRAME));
            rbShowWhiteSpaceInAllFrames.setToolTipText(properties.getProperty(
                    "Pe_M_TT_Rb_ItemShowWhiteSpaceInAllFrames",
                    Constants.PE_M_TT_RB_ITEM_SHOW_WHITE_SPACE_IN_ALL_FRAMES));
            rbClearWhiteSpaceLineInSelectedFrame.setToolTipText(properties.getProperty(
                    "Pe_M_TT_Rb_ItemClearWhiteSpaceLineInSelectedFrame",
                    Constants.PE_M_TT_RB_ITEM_CLEAR_WHITE_SPACE_LINE_IN_SELECTED_FRAME));
            rbClearWhiteSpaceLineInAllFrames.setToolTipText(properties.getProperty(
                    "Pe_M_TT_Rb_ItemClearWhiteSpaceLineInAllFrames",
                    Constants.PE_M_TT_RB_ITEM_CLEAR_WHITE_SPACE_LINE_IN_ALL_FRAMES));
            itemReload.setToolTipText(properties.getProperty("Pe_M_TT_ItemReload", Constants.PE_M_TT_ITEM_RELOAD));
            itemReloadAll.setToolTipText(properties.getProperty("Pe_M_TT_ItemReloadAll",
                    Constants.PE_M_TT_ITEM_RELOAD_ALL));
            itemPrint.setToolTipText(properties.getProperty("Pe_M_TT_ItemPrint", Constants.PE_M_TT_ITEM_PRINT));
            itemCloseSelectedWindow.setToolTipText(properties.getProperty("Pe_M_TT_ItemCloseSelectedWindow",
                    Constants.PE_M_TT_ITEM_CLOSE_SELECTED_WINDOW));
            itemCloseAllWindow.setToolTipText(properties.getProperty("Pe_M_TT_ItemCloseAllWindow",
                    Constants.PE_M_TT_ITEM_CLOSE_ALL_WINDOW));
            itemClose.setToolTipText(properties.getProperty("Pe_M_TT_ItemClose", Constants.PE_M_TT_ITEM_CLOSE));


            // Texty do položek v menuEdit:
            itemIndentMore.setText(properties.getProperty("Pe_M_ItemIndentMore", Constants.PE_M_ITEM_INDENT_MORE));
            itemIndentLess.setText(properties.getProperty("Pe_M_ItemIndentLess", Constants.PE_M_ITEM_INDENT_LESS));
            itemComment.setText(properties.getProperty("Pe_M_ItemComment", Constants.PE_M_ITEM_COMMENT));
            itemUncomment.setText(properties.getProperty("Pe_M_ItemUncomment", Constants.PE_M_ITEM_UNCOMMENT));
            itemFormatCode.setText(properties.getProperty("Pe_M_ItemFormatCode", Constants.PE_M_ITEM_FORMAT_CODE));
            itemInsertMethod.setText(properties.getProperty("Pe_M_ItemInsertMethod",
                    Constants.PE_M_ITEM_INSERT_METHOD));

            // Popisky tlačítek v menuEdit:
            itemIndentMore.setToolTipText(properties.getProperty("Pe_M_TT_ItemIndentMore",
                    Constants.PE_M_TT_ITEM_INDENT_MORE));
            itemIndentLess.setToolTipText(properties.getProperty("Pe_M_TT_IndentLess", Constants.PE_M_TT_INDENT_LESS));
            itemComment.setToolTipText(properties.getProperty("Pe_M_TT_ItemComment", Constants.PE_M_TT_ITEM_COMMENT));
            itemUncomment.setToolTipText(properties.getProperty("Pe_M_TT_ItemUncomment",
                    Constants.PE_M_TT_ITEM_UNCOMMENT));
            itemFormatCode.setToolTipText(properties.getProperty("Pe_M_TT_ItemFormatCode",
                    Constants.PE_M_TT_ITEM_FORMAT_CODE));
            itemInsertMethod.setToolTipText(properties.getProperty("Pe_M_TT_ItemInsertMethod",
                    Constants.PE_M_TT_ITEM_INSERT_METHOD));


            // Texty do položek v menuTools:
            itemReplaceText.setText(properties.getProperty("Pe_M_ItemReplaceText", Constants.PE_M_ITEM_REPLACE_TEXT));
            itemGoToLine.setText(properties.getProperty("Pe_M_ItemGoToLine", Constants.PE_M_ITEM_GO_TO_LINE));
            itemGettersAndSetters.setText(properties.getProperty("Pe_M_ItemGettersAndSetters",
                    Constants.PE_M_ITEM_GETTERS_AND_SETTERS));
            itemConstructor.setText(properties.getProperty("Pe_M_ItemConstructor", Constants.PE_M_ITEM_CONSTRUCTOR));
            itemCompile.setText(properties.getProperty("Pe_M_ItemCompile", Constants.PE_M_ITEM_COMPILE));
            menuAddSerialVersionID.setText(properties.getProperty("Pe_M_ItemMenuItemAddSerialVersionId",
                    Constants.PE_M_MENU_ITEM_ADD_SERIAL_VERSION_ID));
            itemAddDefaultSerialVersionID.setText(properties.getProperty("Pe_M_ItemAddDefaultSerialVersionId",
                    Constants.PE_M_ITEM_ADD_DEFAULT_SERIAL_VERSION_ID));
            itemAddGeneratedSerialVersionId.setText(properties.getProperty("Pe_M_ItemAddGeneratedSerialVersionId",
                    Constants.PE_M_ITEM_ADD_GENERATED_SERIAL_VERSION_ID));

            // Popisky tlačítek v menuTools:
            itemReplaceText.setToolTipText(properties.getProperty("Pe_M_TT_ItemReplaceText",
                    Constants.PE_M_TT_ITEM_REPLACE_TEXT));
            itemGoToLine.setToolTipText(properties.getProperty("Pe_M_TT_ItemGoToLine",
                    Constants.PE_M_TT_ITEM_GO_TO_LINE));
            itemToString.setToolTipText(properties.getProperty("Pe_M_TT_ItemToString",
                    Constants.PE_M_TT_ITEM_TO_STRING));
            itemGettersAndSetters.setToolTipText(properties.getProperty("Pe_M_TT_ItemGettersAndSetters",
                    Constants.PE_M_TT_ITEM_GETTERS_AND_SETTERS));
            itemConstructor.setToolTipText(properties.getProperty("Pe_M_TT_ItemConstructor",
                    Constants.PE_M_TT_ITEM_CONSTRUCTOR));
            itemCompile.setToolTipText(properties.getProperty("Pe_M_TT_ItemCompile", Constants.PE_M_TT_ITEM_COMPILE));
            itemAddDefaultSerialVersionID.setToolTipText(properties.getProperty(
                    "Pe_M_TT_ItemAddDefaultSerialVersionId", Constants.PE_M_TT_ITEM_ADD_DEFAULT_SERIAL_VERSION_ID));
            itemAddGeneratedSerialVersionId.setToolTipText(properties.getProperty(
                    "Pe_M_TT_ItemAddGeneratedSerialVersionId", Constants.PE_M_TT_ITEM_ADD_GENERATED_SERIAL_VERSION_ID));


            // Texty do položek v menuOrderFrames:
            itemTile.setText(properties.getProperty("Pe_M_ItemTile", Constants.PE_M_ITEM_TILE));
            itemTileUnderHim.setText(properties.getProperty("Pe_M_ItemTileUnderHim",
                    Constants.PE_M_ITEM_TILE_UNDER_HIM));
            itemCascade.setText(properties.getProperty("Pe_M_ItemCascade", Constants.PE_M_ITEM_CASCADE));
            itemMinimizeAllFrames.setText(properties.getProperty("Pe_M_ItemMinimizeAllFrames",
                    Constants.PE_M_ITEM_MINIMIZE_ALL_FRAMES));

            // Popisky tlačítek v menuOrderFrames:
            itemTile.setToolTipText(properties.getProperty("Pe_M_TT_ItemTile", Constants.PE_M_TT_ITEM_TILE));
            itemTileUnderHim.setToolTipText(properties.getProperty("Pe_M_TT_ItemTileUnderHim",
                    Constants.PE_M_TT_ITEM_TILE_UNDER_HIM));
            itemCascade.setToolTipText(properties.getProperty("Pe_M_TT_ItemCascade", Constants.PE_M_TT_ITEM_CASCADE));
            itemMinimizeAllFrames.setToolTipText(properties.getProperty("Pe_M_TT_ItemMinimizeAllFrames",
                    Constants.PE_M_TT_ITEM_MINIMIZE_ALL_FRAMES));


            // Texty do proměnných, které se načtou v chybových hláškách:
            txtCannotSavePdfFileText = properties.getProperty("Pe_M_Txt_CannotSavePdfFileText",
                    Constants.PE_M_TXT_CANNOT_SAVE_PDF_FILE_TEXT);
            txtLocation = properties.getProperty("Pe_M_Txt_Location", Constants.PE_M_TXT_LOCATION);
            txtCannotSavePdfFileTitle = properties.getProperty("Pe_M_Txt_CannotSavePdfFileTitle",
                    Constants.PE_M_TXT_CANNOT_SAVE_PDF_FILE_TITLE);
            txtFileCouldNotBeSavedText = properties.getProperty("Pe_M_Txt_FileCouldNotBeSavedText",
                    Constants.PE_M_TXT_FILE_COULD_NOT_BE_SAVED_TEXT);
            txtFileCouldNotBeSavedTitle = properties.getProperty("Pe_M_Txt_FileCouldNotBeSavedTitle",
                    Constants.PE_M_TXT_FILE_COULD_NOT_BE_SAVED_TITLE);
            txtContainsAreDifferentText = properties.getProperty("Pe_M_Txt_ContainsAreDifferentText",
                    Constants.PE_M_TXT_CONTAINS_ARE_DIFFERENT_TEXT);
            txtContainsAreDifferentTitle = properties.getProperty("Pe_M_Txt_ContainsAreDifferentTitle",
                    Constants.PE_M_TXT_CONTAINS_ARE_DIFFERENT_TITLE);
            txtFileDoesNotExistText = properties.getProperty("Pe_M_Txt_FileDoesntExistText",
                    Constants.PE_M_TXT_FILE_DOESNT_EXIST_TEXT);
            txtOriginalLocation = properties.getProperty("Pe_M_Txt_OriginalLocation",
                    Constants.PE_M_TXT_ORIGINAL_LOCATION);
            txtFileDoesNotExistTitle = properties.getProperty("Pe_M_Txt_FileDoesntExistTitle",
                    Constants.PE_M_TXT_FILE_DOESNT_EXIST_TITLE);
            txtClassFileIsNotLoadedText = properties.getProperty("Pe_M_Txt_ClassFileIsNotloadedText",
                    Constants.PE_M_TXT_CLASS_FILE_IS_NOT_LOADED_TEXT);
            txtClassFileIsNotLoadedTitle = properties.getProperty("Pe_M_Txt_ClassFileIsNotLoadedTitle",
                    Constants.PE_M_TXT_CLASS_FILE_IS_NOT_LOADED_TITLE);
            txtCannotGenerateAccessMethodsText = properties.getProperty("Pe_M_Txt_CannotGeneratedAccessMethodsText",
                    Constants.PE_M_TXT_CANNOT_GENERATED_ACCESS_METHODS_TEXT);
            txtClassIsNotInClassDiagram = properties.getProperty("Pe_M_Txt_ClassIsNotInClassDiagram",
                    Constants.PE_M_TXT_CLASS_IS_NOT_IN_CLASS_DIAGRAM);
            txtCannotGenerateConstructor = properties.getProperty("Pe_M_Txt_CannotGenerateConstructor",
                    Constants.PE_M_TXT_CANNOT_GENERATE_CONSTRUCTOR);
            txtClassDoesNotExistAlreadyText = properties.getProperty("Pe_M_Txt_ClassDoesntExistAlreadyText",
                    Constants.PE_M_TXT_CLASS_DOESNT_EXIST_ALREADY_TEXT);
            txtClassDoesNotExistAlreadyTitle = properties.getProperty("Pe_M_Txt_ClassDoesntExistAlreadyTitle",
                    Constants.PE_M_TXT_CLASS_DOESNT_EXIST_ALREADY_TITLE);
            txtSrcDirIsMissingText = properties.getProperty("Pe_M_Txt_SrcDirIsMissingText",
                    Constants.PE_M_TXT_SRC_DIR_IS_MISSING_TEXT);
            txtSrcDirIsMissingTitle = properties.getProperty("Pe_M_Txt_SrcDirIsMissingTitle",
                    Constants.PE_M_TXT_SRC_DIR_IS_MISSING_TITLE);
        } else {
            // Texty do hlavních menu:
            menuFile.setText(Constants.PE_M_MENU_FILE);
            menuEdit.setText(Constants.PE_M_MENU_EDIT);
            menuTools.setText(Constants.PE_M_MENU_TOOLS);
            menuOrderFrames.setText(Constants.PE_M_MENU_ORDER_FRAMES);


            // texty do pložek v menuFile:
            itemOpenFile.setText(Constants.PE_M_ITEM_OPEN_FILE);
            menuOpenFileInAppsOfOs.setText(Constants.PE_M_MENU_OPEN_FILE_IN_APPS_OF_OS);
            itemOpenFileInFileExplorerInOs.setText(Constants.PE_M_ITEM_OPEN_FILE_IN_FILE_EXPLORER_IN_OS);
            itemOpenFileInDefaultAppInOs.setText(Constants.PE_M_ITEM_OPEN_FILE_IN_DEFAULT_APP_IN_OS);
            itemSave.setText(Constants.PE_M_ITEM_SAVE);
            itemSaveAs.setText(Constants.PE_M_ITEM_SAVE_AS);
            itemSaveAll.setText(Constants.PE_M_ITEM_SAVE_ALL);
            rbWrapTextInSelectedFrame.setText(Constants.PE_M_RB_ITEM_WRAP_TEXT_IN_SELECTED_FRAME);
            rbWrapTextInAllFrames.setText(Constants.PE_M_RB_ITEM_WRAP_TEXT_IN_ALL_FRAMES);
            rbShowWhiteSpaceInSelectedFrame.setText(Constants.PE_M_RB_ITEM_SHOW_WHITE_SPACE_IN_SELECTED_FRAME);
            rbShowWhiteSpaceInAllFrames.setText(Constants.PE_M_RB_ITEM_SHOW_WHITE_SPACE_IN_ALL_FRAMES);
            rbClearWhiteSpaceLineInSelectedFrame.setText(Constants.PE_M_RB_ITEM_CLEAR_WHITE_SPACE_LINE_IN_SELECTED_FRAME);
            rbClearWhiteSpaceLineInAllFrames.setText(Constants.PE_M_RB_ITEM_CLEAR_WHITE_SPACE_LINE_IN_ALL_FRAMES);
            itemReload.setText(Constants.PE_M_ITEM_RELOAD);
            itemReloadAll.setText(Constants.PE_M_ITEM_RELOAD_ALL);
            itemPrint.setText(Constants.PE_M_ITEM_PRINT);
            itemCloseSelectedWindow.setText(Constants.PE_M_ITEM_CLOSE_SELECTED_WINDOW);
            itemCloseAllWindow.setText(Constants.PE_M_ITEM_CLOSE_ALL_WINDOW);
            itemClose.setText(Constants.PE_M_ITEM_CLOSE);

            // Popisky tlačítek v menuFile:
            itemOpenFile.setToolTipText(Constants.PE_M_TT_ITEM_OPEN_FILE);
            itemOpenFileInFileExplorerInOs.setToolTipText(Constants.PE_M_TT_ITEM_OPEN_FILE_IN_FILE_EXPLORER_IN_OS);
            itemOpenFileInDefaultAppInOs.setToolTipText(Constants.PE_M_TT_ITEM_OPEN_FILE_IN_DEFAULT_APP_IN_OS);
            itemSave.setToolTipText(Constants.PE_M_TT_ITEM_SAVE);
            itemSaveAs.setToolTipText(Constants.PE_M_TT_ITEM_SAVE_AS);
            itemSaveAll.setToolTipText(Constants.PE_M_TT_ITEM_SAVE_ALL);
            rbWrapTextInSelectedFrame.setToolTipText(Constants.PE_M_TT_RB_ITEM_WRAP_TEXT_IN_SELECTED_FRAME);
            rbWrapTextInAllFrames.setToolTipText(Constants.PE_M_TT_RB_ITEM_WRAP_TEXT_IN_ALL_FRAMES);
            rbShowWhiteSpaceInSelectedFrame.setToolTipText(Constants.PE_M_TT_RB_ITEM_SHOW_WHITE_SPACE_IN_SELECTED_FRAME);
            rbShowWhiteSpaceInAllFrames.setToolTipText(Constants.PE_M_TT_RB_ITEM_SHOW_WHITE_SPACE_IN_ALL_FRAMES);
            rbClearWhiteSpaceLineInSelectedFrame.setToolTipText(Constants.PE_M_TT_RB_ITEM_CLEAR_WHITE_SPACE_LINE_IN_SELECTED_FRAME);
            rbClearWhiteSpaceLineInAllFrames.setToolTipText(Constants.PE_M_TT_RB_ITEM_CLEAR_WHITE_SPACE_LINE_IN_ALL_FRAMES);
            itemReload.setToolTipText(Constants.PE_M_TT_ITEM_RELOAD);
            itemReloadAll.setToolTipText(Constants.PE_M_TT_ITEM_RELOAD_ALL);
            itemPrint.setToolTipText(Constants.PE_M_TT_ITEM_PRINT);
            itemCloseSelectedWindow.setToolTipText(Constants.PE_M_TT_ITEM_CLOSE_SELECTED_WINDOW);
            itemCloseAllWindow.setToolTipText(Constants.PE_M_TT_ITEM_CLOSE_ALL_WINDOW);
            itemClose.setToolTipText(Constants.PE_M_TT_ITEM_CLOSE);


            // Texty do položek v menuEdit:
            itemIndentMore.setText(Constants.PE_M_ITEM_INDENT_MORE);
            itemIndentLess.setText(Constants.PE_M_ITEM_INDENT_LESS);
            itemComment.setText(Constants.PE_M_ITEM_COMMENT);
            itemUncomment.setText(Constants.PE_M_ITEM_UNCOMMENT);
            itemFormatCode.setText(Constants.PE_M_ITEM_FORMAT_CODE);
            itemInsertMethod.setText(Constants.PE_M_ITEM_INSERT_METHOD);

            // Popisky tlačítek v menuEdit:
            itemIndentMore.setToolTipText(Constants.PE_M_TT_ITEM_INDENT_MORE);
            itemIndentLess.setToolTipText(Constants.PE_M_TT_INDENT_LESS);
            itemComment.setToolTipText(Constants.PE_M_TT_ITEM_COMMENT);
            itemUncomment.setToolTipText(Constants.PE_M_TT_ITEM_UNCOMMENT);
            itemFormatCode.setToolTipText(Constants.PE_M_TT_ITEM_FORMAT_CODE);
            itemInsertMethod.setToolTipText(Constants.PE_M_TT_ITEM_INSERT_METHOD);


            // Texty do položek v menuTools:
            itemReplaceText.setText(Constants.PE_M_ITEM_REPLACE_TEXT);
            itemGoToLine.setText(Constants.PE_M_ITEM_GO_TO_LINE);
            itemGettersAndSetters.setText(Constants.PE_M_ITEM_GETTERS_AND_SETTERS);
            itemConstructor.setText(Constants.PE_M_ITEM_CONSTRUCTOR);
            itemCompile.setText(Constants.PE_M_ITEM_COMPILE);
            menuAddSerialVersionID.setText(Constants.PE_M_MENU_ITEM_ADD_SERIAL_VERSION_ID);
            itemAddDefaultSerialVersionID.setText(Constants.PE_M_ITEM_ADD_DEFAULT_SERIAL_VERSION_ID);
            itemAddGeneratedSerialVersionId.setText(Constants.PE_M_ITEM_ADD_GENERATED_SERIAL_VERSION_ID);

            // Popisky tlačítek v menuTools:
            itemReplaceText.setToolTipText(Constants.PE_M_TT_ITEM_REPLACE_TEXT);
            itemGoToLine.setToolTipText(Constants.PE_M_TT_ITEM_GO_TO_LINE);
            itemToString.setToolTipText(Constants.PE_M_TT_ITEM_TO_STRING);
            itemGettersAndSetters.setToolTipText(Constants.PE_M_TT_ITEM_GETTERS_AND_SETTERS);
            itemConstructor.setToolTipText(Constants.PE_M_TT_ITEM_CONSTRUCTOR);
            itemCompile.setToolTipText(Constants.PE_M_TT_ITEM_COMPILE);
            itemAddDefaultSerialVersionID.setToolTipText(Constants.PE_M_TT_ITEM_ADD_DEFAULT_SERIAL_VERSION_ID);
            itemAddGeneratedSerialVersionId.setToolTipText(Constants.PE_M_TT_ITEM_ADD_GENERATED_SERIAL_VERSION_ID);


            // Texty do položek v menuOrderFrames:
            itemTile.setText(Constants.PE_M_ITEM_TILE);
            itemTileUnderHim.setText(Constants.PE_M_ITEM_TILE_UNDER_HIM);
            itemCascade.setText(Constants.PE_M_ITEM_CASCADE);
            itemMinimizeAllFrames.setText(Constants.PE_M_ITEM_MINIMIZE_ALL_FRAMES);

            // Popisky tlačítek v menuOrderFrames:
            itemTile.setToolTipText(Constants.PE_M_TT_ITEM_TILE);
            itemTileUnderHim.setToolTipText(Constants.PE_M_TT_ITEM_TILE_UNDER_HIM);
            itemCascade.setToolTipText(Constants.PE_M_TT_ITEM_CASCADE);
            itemMinimizeAllFrames.setToolTipText(Constants.PE_M_TT_ITEM_MINIMIZE_ALL_FRAMES);


            // Texty do proměnných, které se načtou v chybových hláškách:
            txtCannotSavePdfFileText = Constants.PE_M_TXT_CANNOT_SAVE_PDF_FILE_TEXT;
            txtLocation = Constants.PE_M_TXT_LOCATION;
            txtCannotSavePdfFileTitle = Constants.PE_M_TXT_CANNOT_SAVE_PDF_FILE_TITLE;
            txtFileCouldNotBeSavedText = Constants.PE_M_TXT_FILE_COULD_NOT_BE_SAVED_TEXT;
            txtFileCouldNotBeSavedTitle = Constants.PE_M_TXT_FILE_COULD_NOT_BE_SAVED_TITLE;
            txtContainsAreDifferentText = Constants.PE_M_TXT_CONTAINS_ARE_DIFFERENT_TEXT;
            txtContainsAreDifferentTitle = Constants.PE_M_TXT_CONTAINS_ARE_DIFFERENT_TITLE;
            txtFileDoesNotExistText = Constants.PE_M_TXT_FILE_DOESNT_EXIST_TEXT;
            txtOriginalLocation = Constants.PE_M_TXT_ORIGINAL_LOCATION;
            txtFileDoesNotExistTitle = Constants.PE_M_TXT_FILE_DOESNT_EXIST_TITLE;
            txtClassFileIsNotLoadedText = Constants.PE_M_TXT_CLASS_FILE_IS_NOT_LOADED_TEXT;
            txtClassFileIsNotLoadedTitle = Constants.PE_M_TXT_CLASS_FILE_IS_NOT_LOADED_TITLE;
            txtCannotGenerateAccessMethodsText = Constants.PE_M_TXT_CANNOT_GENERATED_ACCESS_METHODS_TEXT;
            txtClassIsNotInClassDiagram = Constants.PE_M_TXT_CLASS_IS_NOT_IN_CLASS_DIAGRAM;
            txtCannotGenerateConstructor = Constants.PE_M_TXT_CANNOT_GENERATE_CONSTRUCTOR;
            txtClassDoesNotExistAlreadyText = Constants.PE_M_TXT_CLASS_DOESNT_EXIST_ALREADY_TEXT;
            txtClassDoesNotExistAlreadyTitle = Constants.PE_M_TXT_CLASS_DOESNT_EXIST_ALREADY_TITLE;
            txtSrcDirIsMissingText = Constants.PE_M_TXT_SRC_DIR_IS_MISSING_TEXT;
            txtSrcDirIsMissingTitle = Constants.PE_M_TXT_SRC_DIR_IS_MISSING_TITLE;
        }


        itemToString.setText("toString()");
    }


    @Override
    public void actionPerformed(final ActionEvent e) {
        if (e.getSource() instanceof JRadioButtonMenuItem) {
            if (e.getSource() == rbWrapTextInSelectedFrame) {
                // Převod na typ okna pro "přehlednější" převod datového typu a získání souboru:
                final FileInternalFrame myFrame = projectExplorer.getSelectedFrame();

                if (myFrame != null) {
                    myFrame.wrapTextInEditor(rbWrapTextInSelectedFrame.isSelected());

                    /*
                     * Zde se nastaví označený rb na true v případě, že mají veškerá interní okna nastaveno, že se mají
                     * zalamovat.
                     *
                     * Podobně, když mají veškerá interní okna nastaveno, že se nemají zalamovat texty, tak rb pro
                     * nastavení zalamování textu ve všech interních oknech nebude označen.
                     */
                    rbWrapTextInAllFrames.setSelected(projectExplorer.areAllFramesSetToWrapText());
                }
            }

            else if (e.getSource() == rbWrapTextInAllFrames) {
                final JInternalFrame[] allFrames = projectExplorer.getDesktopPane().getAllFrames();

                Arrays.stream(allFrames).forEach(f -> {
                    if (f instanceof FileInternalFrame) {
                        final FileInternalFrame fileInternalFrame = (FileInternalFrame) f;

                        fileInternalFrame.wrapTextInEditor(rbWrapTextInAllFrames.isSelected());
                    }
                });

                /*
                 * Zde se musí nastavit komponenta pro nastavení zalamování textů v jednom okně na to, zda se
                 * zalamují texty ve všech oknech nebo jen v nějakém.
                 */
                rbWrapTextInSelectedFrame.setSelected(rbWrapTextInAllFrames.isSelected());
            }

            else if (e.getSource() == rbShowWhiteSpaceInSelectedFrame) {
                // Převod na typ okna pro "přehlednější" převod datového typu a získání souboru:
                final FileInternalFrame myFrame = projectExplorer.getSelectedFrame();

                if (myFrame != null) {
                    myFrame.setShowWhiteSpaceInEditor(rbShowWhiteSpaceInSelectedFrame.isSelected());

                    /*
                     * Zde se nastaví označený rb na true / false v případě, že mají veškerá interní okna nastaveno,
                     * že mají zobrazovat bílé znaky.
                     */
                    rbShowWhiteSpaceInAllFrames.setSelected(projectExplorer.areInAllFramesShowWhiteSpace());
                }
            }

            else if (e.getSource() == rbShowWhiteSpaceInAllFrames) {
                final JInternalFrame[] allFrames = projectExplorer.getDesktopPane().getAllFrames();

                Arrays.stream(allFrames).forEach(f -> {
                    if (f instanceof FileInternalFrame) {
                        final FileInternalFrame fileInternalFrame = (FileInternalFrame) f;

                        fileInternalFrame.setShowWhiteSpaceInEditor(rbShowWhiteSpaceInAllFrames.isSelected());
                    }
                });

                /*
                 * Zde se musí nastavit komponenta pro nastavení zobrazování bílých znaků v jednom okně dle toho, zda
                  * se zobrazují bílé znaky ve všech interních oknech nebo ne.
                 */
                rbShowWhiteSpaceInSelectedFrame.setSelected(rbShowWhiteSpaceInAllFrames.isSelected());
            }


            else if (e.getSource() == rbClearWhiteSpaceLineInSelectedFrame) {
                // Převod na typ okna pro "přehlednější" převod datového typu a získání souboru:
                final FileInternalFrame myFrame = projectExplorer.getSelectedFrame();

                if (myFrame != null) {
                    myFrame.setClearWhiteSpaceLineInEditor(rbClearWhiteSpaceLineInSelectedFrame.isSelected());

                    /*
                     * Zde se nastaví označený rb na true / false v případě, že mají veškerá interní okna nastaveno,
                     * že se v nich mají odebírat bílé znaky z prázdného řádku kde se nachází kurzor při stisknu
                     * klávey Enter.
                     */
                    rbClearWhiteSpaceLineInAllFrames.setSelected(projectExplorer.areInAllFramesSetClearWhiteSpaceLine());
                }
            }

            else if (e.getSource() == rbClearWhiteSpaceLineInAllFrames) {
                final JInternalFrame[] allFrames = projectExplorer.getDesktopPane().getAllFrames();

                Arrays.stream(allFrames).forEach(f -> {
                    if (f instanceof FileInternalFrame) {
                        final FileInternalFrame fileInternalFrame = (FileInternalFrame) f;

                        fileInternalFrame.setClearWhiteSpaceLineInEditor(rbClearWhiteSpaceLineInAllFrames.isSelected());
                    }
                });

                /*
                 * Zde se musí nastavit komponenta pro nastavení odebírání bílých znaků z prázdných řádků po stisknu
                 * klávesy Enter v jednom okně dle toho, zda se je tak nastaveno ve všech interních oknech nebo ne.
                 */
                rbClearWhiteSpaceLineInSelectedFrame.setSelected(rbClearWhiteSpaceLineInAllFrames.isSelected());
            }
        }



        else if (e.getSource() instanceof JMenuItem) {
            if (e.getSource() == itemOpenFile) {
                // Zde se otevře dialogové okno, kde se vybere soubor pro otevření a zavolá se příslušná metoda,
                // která otestuje, zda lze soubor otevřít v okně nebo ne
                final String pathToFile = new FileChooser().getPathToNewFileForOpen();

                if (pathToFile != null)
                    // Zde byl vybrán soubor pro otevření:

                    // Note:
                    // Zde ani nemusím testovat, zda příslušný soubor existuje nebo ne, vše zjistí následující zavolaná
                    // metoda pro vytvoření okna a navíc v editoru pro výber souiboruz ani nelze vybratr neexistující
                    // soubor.
                    projectExplorer.createInternalFrame(new File(pathToFile), null);
            }


            else if (e.getSource() == itemOpenFileInFileExplorerInOs) {
                // Převod na typ okna pro "přehlednější" převod datového typu a získání souboru:
                final FileInternalFrame myFrame = projectExplorer.getSelectedFrame();

                if (myFrame != null)
                    // Otevření souboru v průzkumníku souborů:
                    DesktopSupport.openFileExplorer(myFrame.getFile(), true);
            }


            else if (e.getSource() == itemOpenFileInDefaultAppInOs) {
                // Převod na typ okna pro "přehlednější" převod datového typu a získání souboru:
                final FileInternalFrame myFrame = projectExplorer.getSelectedFrame();

                if (myFrame != null)
                    // Otevření souboru v průzkumníku souborů:
                    DesktopSupport.openFileExplorer(myFrame.getFile(), false);
            }


            else if (e.getSource() == itemSave)
                saveSelectedFile();


            else if (e.getSource() == itemSaveAs) {
                // Zobrazí se dialogové okno, kde se vybere umístění, kam se má uložit soubor otevřený v označeném
                // interním okně
                final JInternalFrame internalFrame = projectExplorer.getDesktopPane().getSelectedFrame();

                if (internalFrame instanceof FileInternalFrame) {
                    final FileInternalFrame myFrame = (FileInternalFrame) internalFrame;

                    // Otestuji, zda náhodou není otevřen soubor typu .pdf, pokud ano, tak pouze vypíšu hlášku,
                    // že tento soubor nelze uložit, protože nelze uložit
                    if (!myFrame.getKindOfFile().equals(KindOfFile.PDF)) {
                        final String newLocation = myFrame.getNewFileLocation();

                        if (newLocation != null) {
                            // Zde uživatel vybral nové umístění pro příslušný soubor, kam se má zapsat
                            final boolean result = myFrame.saveFile(new File(newLocation));

                            if (!result)
                                JOptionPane.showMessageDialog(this,
                                        txtFileCouldNotBeSavedText + "\n" + txtLocation + ": " + newLocation,
                                        txtFileCouldNotBeSavedTitle, JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    else
                        JOptionPane.showMessageDialog(this,
                                txtCannotSavePdfFileText + "\n" + txtLocation + ": "
                                        + myFrame.getFile().getAbsolutePath(),
                                txtCannotSavePdfFileTitle, JOptionPane.INFORMATION_MESSAGE);
                }
            }


            else if (e.getSource() == itemSaveAll) {
                // Získají se všechny otevřená interní okna - otevřené soubory a uloží se, pokud nejaký soubor již
                // neexistuje,
                // tak se uživatele zeptám, zda se má pro příslušný soubor vybrat nové umístění, a případně se tam
                // uloži, jinak nic
                final JInternalFrame[] internalFramesArray = projectExplorer.getDesktopPane().getAllFrames();

                projectExplorer.saveDataInAllFrames(internalFramesArray);
            }


            else if (e.getSource() == itemReload) {
                // Přenačte se soubor v označeném interním okne, pokud již eexistuje, nebidne se zápis souboru
                // na nové umístění
                final JInternalFrame internalFrame = projectExplorer.getDesktopPane().getSelectedFrame();

                if (internalFrame instanceof FileInternalFrame)
                    reloadInternalFrame(internalFrame);
            }


            else if (e.getSource() == itemReloadAll) {
                // Přenačtou se všechny soubory otevřené v interních okne, pokud nějaký soubor již neexistuje, nabídne
                // se možnost pro výběr nového umístění, takkto se proiterují všechny interní okna:

                // Získám si všechna interní okna otevřená v komponentě JdesktopPane, kam se vkládají:
                final JInternalFrame[] internalFramesArray = projectExplorer.getDesktopPane().getAllFrames();

                // Všchny je proiteruji a ujistím se, že žádné není null a jsou instance MyInternalFrame, ale toto
                // bych ani
                // testovat nemusel - pouze prevence, jiné typy oken tam ani nevkládám (do JdesktopPane) - alespoˇne
                // vědomně a úmyslně
                for (final JInternalFrame f : internalFramesArray) {
                    if (f instanceof FileInternalFrame)
                        reloadInternalFrame(f);
                }
            }


            else if (e.getSource() == itemPrint) {
                // Zde se otestuji, zda je označen nějaký otevřený soubor (interní okno) pro vytisknutí, pokud ano,
                // tak se otestuje, zda příslušný soubor ještě existuje, případně se někam uloží a otestuji se i obsahy
                // editoru a souboru, zda jsou různé, případně se uloží a na konec , když soubor existuje, tak se
                // může vytisknout

                // Získám si označené interní okno:
                final JInternalFrame internalFrame = projectExplorer.getDesktopPane().getSelectedFrame();

                // Otestuji, zda je vůbec nějaké okno označené a pro jistotu, zda je to insntce MyInternalFrame:
                if (internalFrame instanceof FileInternalFrame) {
                    final FileInternalFrame myFrame = (FileInternalFrame) internalFrame;

                    // Zde mám přísluené okno, tak mohu uložit jeho obsah a vytisknout ho:

                    // Ale nejprve otestuji, zda došlo ke změně, pokud ano, tak se zeptám, zda se má před vytisknutím
                    // obsah
                    // uložit, protože se bude tisknout cílový soubor a ne obsah v editoru:
                    if (!myFrame.existFile())
                        // Zde příslušný soubor už neexistuje, tak se zeptám uživatele, zda se má uložit,
                        // na nějaké nové umístění:
                        myFrame.saveFileToNewLocation();


                    if (myFrame.existFile()) {
                        // Zde už by měl příslušný soubor existovat alepoň na novém nebo původním umístění
                        //tak mohu porovnat koduy, zda jsou stejné - pokud se uložil na nové umístění, tak budou,
                        // ale jinak ne:
                        if (!myFrame.areCodeIdentical()) {
                            // Zde kódy nejsou stejné, tak se uživatele zeptám, zda se mají kódy uložit nebo ne:
                            // prootže se bude tisknout obsah souboru a ne obsah editoru:
                            final int result = JOptionPane.showConfirmDialog(this, txtContainsAreDifferentText,
                                    txtContainsAreDifferentTitle, JOptionPane.YES_NO_OPTION);

                            // Otestuji, zda uživatel zvolil ano a případně uložím obsah editoru do původního souboru
                            // a vytiskne se:
                            if (result == JOptionPane.YES_OPTION)
                                myFrame.saveFile();
                        }


                        // Zde musím naposledy otestovat, zda ještě příslušný soubor opravdu existuje, protože pokud ne,
                        // resp. uživatel mohl odmítnou nové ulolžení souboru, tak by pořád neexistoval, tak zde se
                        // rozhodne:
                        if (myFrame.existFile())
                            new Printer(myFrame.getFile().getAbsolutePath());
                    }
                    else
                        JOptionPane.showMessageDialog(this,
                                txtFileDoesNotExistText + "\n" + txtOriginalLocation + ": "
                                        + myFrame.getFile().getAbsolutePath(),
                                txtFileDoesNotExistTitle, JOptionPane.ERROR_MESSAGE);
                }
            }


            else if (e.getSource() == itemCloseSelectedWindow) {
                // Slouží pro zavření označeného interního okna, pokud je nějaké označené, tak se
                // otestuje, provedené změny a existence souboru, viz konkrétní metody
                final JInternalFrame internalFrame = projectExplorer.getDesktopPane().getSelectedFrame();

                if (internalFrame instanceof FileInternalFrame)
                    closeFrame(internalFrame);
            }


            else if (e.getSource() == itemCloseAllWindow) {
                //Načtu si všechny interní otevřená okna - soubory a všehny je pozavírám s tím, že si pokaždé
                // zjistím, zda příslušný soubor existuje a případně nabídnu možnost nového umístění,
                // a pkud existuje tak otestuji změny v souboru a případně nabídnu jejich uložení:
                final JInternalFrame[] internalFramesArray = projectExplorer.getDesktopPane().getAllFrames();

                for (final JInternalFrame f : internalFramesArray) {
                    if (f instanceof FileInternalFrame)
                        closeFrame(f);
                }
            }


            else if (e.getSource() == itemClose)
                // Zavření dialogu - po zavření se otestjí data v otevřených interních oknech pro uložení:
                projectExplorer.dispose();


                // menuEdit:
            else if (e.getSource() == itemIndentMore) {
                // Získám si označené interní okno:
                final FileInternalFrame selectedFrame = projectExplorer.getSelectedFrame();

                // Otestji, zda bylo načteno, resp. zda je vůbec nějaké inerní okno otevřeno a označeno:
                if (selectedFrame != null)
                    // Zde je označeno nějaké interní okno, tak mohu zavolat metodu:
                    menuMethods.indentMoreText(selectedFrame.getCodeEditor());
            }


            else if (e.getSource() == itemIndentLess) {
                // Získám si označené interní okno:
                final FileInternalFrame selectedFrame = projectExplorer.getSelectedFrame();

                // Otestji, zda bylo načteno, resp. zda je vůbec nějaké inerní okno otevřeno a označeno:
                if (selectedFrame != null)
                    // Zde je označeno nějaké interní okno, tak mohu zavolat metodu:
                    menuMethods.indentLessText(selectedFrame.getCodeEditor());
            }


            else if (e.getSource() == itemComment) {
                // Získám si označené interní okno:
                final FileInternalFrame selectedFrame = projectExplorer.getSelectedFrame();

                // Otestji, zda bylo načteno, resp. zda je vůbec nějaké inerní okno otevřeno a označeno:
                if (selectedFrame != null)
                    // Zde je označeno nějaké interní okno, tak mohu zavolat metodu:
                    menuMethods.commentText(selectedFrame.getCodeEditor());
            }


            else if (e.getSource() == itemUncomment) {
                // Získám si označené interní okno:
                final FileInternalFrame selectedFrame = projectExplorer.getSelectedFrame();

                // Otestji, zda bylo načteno, resp. zda je vůbec nějaké inerní okno otevřeno a označeno:
                if (selectedFrame != null)
                    // Zde je označeno nějaké interní okno, tak mohu zavolat metodu:
                    menuMethods.uncommentText(selectedFrame.getCodeEditor());
            }


            else if (e.getSource() == itemFormatCode) {
                // Získám si označené interní okno:
                final FileInternalFrame selectedFrame = projectExplorer.getSelectedFrame();

                // Otestji, zda bylo načteno, resp. zda je vůbec nějaké inerní okno otevřeno a označeno:
                if (selectedFrame != null) {
                    // Zde mám označené interní okno, tak otestuji, zda příslušný soubor ještě existuje:
                    if (!selectedFrame.existFile())
                        // Zde příslušný soubor již neexistuje, tak nabídnu možnost nového uložení
                        selectedFrame.saveFileToNewLocation();


                    // Nyní musím zunovu otestovat, zda příslušný souibor existuje, protrože pokud neexistoval,
                    // a uživatel nevybral nové umístění, tak pořad neexistuje:

                    if (selectedFrame.existFile()) {
                        // Zde příslušný soubor existuje, tak otestuji, zda se má soubor uložit nebo ne,
                        // dle toho, zda se shodují kody v editoru a v původním souboru:

                        // Nyní otestuji, zda jsou kody v označeném interním okně - souboru stejné, jako v původním
                        // souboru, pokud ne, tak se změny ulolží pokud ano, tak se jde dál - není třeba jej ukládat
                        if (!selectedFrame.areCodeIdentical())
                            selectedFrame.saveFile();


                        // Nyní vím, že příslušný soubor existuje, a jeho kód je ulolžen (pokud nebyl), tak mohu
                        // příslušný kód zformátovat a vložit jej zpět do editoru v označeném interním okně:
                        menuMethods.formatText(selectedFrame.getCodeEditor(), selectedFrame.getFile());
                    }
                }
            }


            else if (e.getSource() == itemInsertMethod) {
                // Získám si označené interní okno:
                final FileInternalFrame selectedFrame = projectExplorer.getSelectedFrame();

                // Otestji, zda bylo načteno, resp. zda je vůbec nějaké inerní okno otevřeno a označeno:
                if (selectedFrame != null)
                    // Zde je označeno nějaké interní okno, tak mohu zavolat metodu:
                    menuMethods.insertMethod(selectedFrame.getCodeEditor());
            }


            // menuTools:
            else if (e.getSource() == itemReplaceText) {
                // Získám si označené interní okno:
                final FileInternalFrame selectedFrame = projectExplorer.getSelectedFrame();

                // Otestji, zda bylo načteno, resp. zda je vůbec nějaké inerní okno otevřeno a označeno:
                if (selectedFrame != null)
                    // Zde je označeno nějaké interní okno, tak mohu zavolat metodu:
                    menuMethods.replaceText(selectedFrame.getCodeEditor());
            }


            else if (e.getSource() == itemGoToLine) {
                // Získám si označené interní okno:
                final FileInternalFrame selectedFrame = projectExplorer.getSelectedFrame();

                // Otestji, zda bylo načteno, resp. zda je vůbec nějaké inerní okno otevřeno a označeno:
                if (selectedFrame != null)
                    // Zde je označeno nějaké interní okno, tak mohu zavolat metodu:
                    menuMethods.goToLine(selectedFrame.getCodeEditor());
            }


            else if (e.getSource() == itemToString) {
                // Získám si označené interní okno:
                final FileInternalFrame selectedFrame = projectExplorer.getSelectedFrame();

                // Otestji, zda bylo načteno, resp. zda je vůbec nějaké inerní okno otevřeno a označeno:
                if (selectedFrame != null)
                    // Zde je označeno nějaké interní okno, tak mohu zavolat metodu:
                    menuMethods.toStringMethod(selectedFrame.getCodeEditor());
            }


            else if (e.getSource() == itemGettersAndSetters) {
                // Postup:
                // Nejprve si zjistím označený otevřený soubor - otevřené okno,
                // otestuji, zda příslušný soubor ještě opravdu existuje, resp. zda existuje příslušná třída
                // pokud ano, tak ji zkusím zkompilovat a načíst, jinak se vezme za původní v adresáři bin, a když
                // tam nebude, tak se vypíše oznámění, že se daný soubor nepodařilo najít:

                // Získám si označené interní okno:
                final FileInternalFrame selectedFrame = projectExplorer.getSelectedFrame();

                // Otestji, zda bylo načteno, resp. zda je vůbec nějaké inerní okno otevřeno a označeno:
                if (selectedFrame != null) {

                    // Uložím všechny otevřené třídy v interních oknech (pokud jsou nějaké otevřené:)
                    projectExplorer.saveDataInFramesWithClassFromProject();

                    if (selectedFrame.existFile()) {
                        // Zde již vím, že soubor existuje, a byly uloženy všechny ostatní
                        // Nejprve potřebuji ještě otestovat, zda se vůbec jedná o Javovskou třídu, která se nachází
                        // v diagramu tříd

                        if (selectedFrame.getPackageText() != null && GraphClass.isClassWithTextInClassDiagram(selectedFrame.getPackageText())) {
                            // Zde se jedná o třídu z diagramu tříd, tak jej mohu zkompilovat a zkusit načíst
                            // přeloženou - zkompilovaný soubor .class:

                            // Nyní soubor existuje a měly se změny uložit (pokud nebyly), tak mohu zkompilovat
                            // projekt a načíst příslušný soubor .class a předadt jej do dialogu pro generování
                            // přístupových metod
                            final Class<?> clazz = App.READ_FILE.getClassFromFile(selectedFrame.getPackageText(), null,
                                    GraphClass.getCellsList(), true, false);

                            if (clazz != null)
                                menuMethods.generateGettersAndSetters(selectedFrame.getCodeEditor(), clazz);

                            else
                                JOptionPane.showMessageDialog(this,
                                        txtClassFileIsNotLoadedText + "\n" + txtLocation + ": "
                                                + selectedFrame.getFile().getAbsolutePath(),
                                        txtClassFileIsNotLoadedTitle, JOptionPane.ERROR_MESSAGE);
                        } else
                            JOptionPane.showMessageDialog(this, txtCannotGenerateAccessMethodsText,
                                    txtClassIsNotInClassDiagram, JOptionPane.ERROR_MESSAGE);
                    } else
                        JOptionPane.showMessageDialog(this,
                                txtClassDoesNotExistAlreadyText + "\n" + txtOriginalLocation + ": "
                                        + selectedFrame.getFile().getAbsolutePath(),
                                txtClassDoesNotExistAlreadyTitle, JOptionPane.ERROR_MESSAGE);
                }
            }


            else if (e.getSource() == itemConstructor) {
                // Zde se jedná v podstatě o podobný postup jako u generování přístupových metod (výše) akorát se míto
                // těchto metod bude generovat konstruktor, ale princip je stejný:

                // Nejprve si zjistím, zda je vůbec nějaký označený soubor, resp. otevřené okno, a
                // pokud ano, tak ho uložím a pokusím se daný projek zkompilovat,
                // a pak načíst příslušný soubor .class označené třídy a předat ho do dialogu pro vygenerování,
                // zmíněného konstruktoru:

                // Získám si označené interní okno:
                final FileInternalFrame selectedFrame = projectExplorer.getSelectedFrame();

                // Otestji, zda bylo načteno, resp. zda je vůbec nějaké inerní okno otevřeno a označeno:
                if (selectedFrame != null) {

                    // Uložím všechny otevřené třídy v interních oknech (pokud jsou nějaké otevřené:)
                    projectExplorer.saveDataInFramesWithClassFromProject();

                    if (selectedFrame.existFile()) {
                        // Zde již vím, že soubor existuje, a byly uloženy všechny ostatní
                        // Nejprve potřebuji ještě otestovat, zda se vůbec jedná o Javovskou třídu, která se nachází
                        // v diagramu tříd

                        if (selectedFrame.getPackageText() != null
                                && GraphClass.isClassWithTextInClassDiagram(selectedFrame.getPackageText())) {
                            // Zde se jedná o třídu z diagramu tříd, tak jej mohu zkompilovat a zkusit načíst
                            // přeloženou - zkompilovaný soubor .class:

                            // Nyní soubor existuje a měly se změny uložit (pokud nebyly), tak mohu zkompilovat
                            // projekt a načíst příslušný soubor .class a předadt jej do dialogu pro generování
                            // přístupových metod
                            final Class<?> clazz = App.READ_FILE.getClassFromFile(selectedFrame.getPackageText(), null,
                                    GraphClass.getCellsList(), true, false);

                            if (clazz != null)
                                menuMethods.generateConstructor(selectedFrame.getCodeEditor(), clazz);

                            else
                                JOptionPane.showMessageDialog(this,
                                        txtClassFileIsNotLoadedText + "\n" + txtLocation + ": "
                                                + selectedFrame.getFile().getAbsolutePath(),
                                        txtClassFileIsNotLoadedTitle, JOptionPane.ERROR_MESSAGE);
                        } else
                            JOptionPane.showMessageDialog(this, txtCannotGenerateConstructor,
                                    txtClassIsNotInClassDiagram, JOptionPane.ERROR_MESSAGE);
                    } else
                        JOptionPane.showMessageDialog(this,
                                txtClassDoesNotExistAlreadyText + "\n" + txtOriginalLocation + ": "
                                        + selectedFrame.getFile().getAbsolutePath(),
                                txtClassDoesNotExistAlreadyTitle, JOptionPane.ERROR_MESSAGE);
                }
            }


            else if (e.getSource() == itemCompile) {
                // zkompiluji všechny třídy a výsledek si uložím do proměnné, abych věděl,
                // zda mohu přenačíst "autodoplňování" v jednotlivých třídách nebo ne,
                // pokud se třídy zkompilují s chybou, tak se nebudou přenačítat nápovědy,
                // pokud se třídy zkompilují ez chyby, pak se do proměnné result uloží hodnota true,
                // a prohledají se všechny otevřaná okna, a pokud se jedná o otevřenou třídu,
                // která se nachází i v diagramu tříd, tak se přenačte příslušné auto - doplňování

                // Zjistím si cestu k adresáři src, zde již potřebuji zjistit, zda ještě existuje, protože pokud
                // ne, mohlo by dojít k chybě, protože v tomto adresáři jsou (měli by být) třídy, které se mají
                // zkompilovat a pokud neexistuje, pak není ani co kompilovat
                // Note:
                // Ne přímo existenci adresáře src se ještě testuje v následující metodě: getFileFromCells, ale
                // tam se jedná o existenci vyloženě souboru na zadané cestě včetně adresáře src, zde se testuje
                // pouze existence adresáře src
                final File fileSrc =
                        new File(projectExplorer.getFileProjectDir().getAbsoluteFile() + File.separator + "src");


                // Note.
                // Navíc se zde může jednat o to, že je třeba náhodou jen přejmenován, apod. to tato aplikace ale
                // neřeší,
                // musí to být adresář src, jiné varianty tato aplikace nepozná:
                if (fileSrc.exists() && fileSrc.isDirectory()) {
                    // Uložím všechny otevřené třídy v interních oknech (pokud jsou nějaké otevřené:)
                    projectExplorer.saveDataInFramesWithClassFromProject();



                    /*
                     * Vlákno pro doplnění kódu spustím pouze v případě, že ještě nebylo vytvořené
                     * nebo už neběží, jinak by běželo zbytečně více vláken se stejným účelem.
                     */
                    if (refreshAutoCompletionThread == null || !refreshAutoCompletionThread.isAlive()) {
                        // spustím vlákno, které přeloží třídy v diagramu tříd, které jsou v aktuálně
                        // otevřeném projektu:
                        refreshAutoCompletionThread = new RefreshAutoCompletionThread(languageProperties, fileSrc,
                                projectExplorer, true);

                        refreshAutoCompletionThread.start();
                    }
                } else
                    JOptionPane.showMessageDialog(this,
                            txtSrcDirIsMissingText + "\n" + txtLocation + ": " + fileSrc.getAbsolutePath(),
                            txtSrcDirIsMissingTitle, JOptionPane.ERROR_MESSAGE);
            }


            else if (e.getSource() == itemAddDefaultSerialVersionID) {
                // Získám si označené interní okno:
                final FileInternalFrame selectedFrame = projectExplorer.getSelectedFrame();

                // Otestuji, zda bylo načteno, resp. zda je vůbec nějaké inerní okno otevřeno a
                // označeno:
                if (selectedFrame != null)
                    // Zde je označeno nějaké interní okno, tak mohu zavolat metodu pro vložení
                    // serial ID:
                    menuMethods.insertDefaultSerialVersionId(selectedFrame.getCodeEditor());
            }


            else if (e.getSource() == itemAddGeneratedSerialVersionId) {
                // Získám si označené interní okno:
                final FileInternalFrame selectedFrame = projectExplorer.getSelectedFrame();

                // Otestuji, zda bylo načteno, resp. zda je vůbec nějaké inerní okno otevřeno a
                // označeno, jinak zdemohu skončit:
                if (selectedFrame == null)
                    return;

                // Uložím aktuálně označený otevřený soubor:
                saveSelectedFile();


                /*
                 * Podmínka, kde se otestuje, zda je otevřen v příslušném interním okně nějaká
                 * třída z diagramu tříd. Pokud ne, pak je příslušná proměnná null, ale k tomuto
                 * by dojít nemělo, protože pokud není otevřena třída z diagramu třídu, pak by
                 * ani nemělo být přístupně toto tlačítko pro vygenerování serial version ID.
                 */
                if (selectedFrame.getPackageText() == null)
                    return;

                /*
                 * Zde zkusím nejprve všechny třídy zkompilovat a pokud se tak povede, tak si
                 * načtu příslušnou třídu, resp. její zkompilovaný soubor .class. Pokud dojde k
                 * nějaké chybě, tak si zkusím i tak načíst původní soubor bez kompilace tříd.
                 * Tento půvdoní soubor by se měl v adresáří bin již nacházet pokd již byla
                 * třída dříve zkompilována a to stačí pro vygenerování serial version ID.
                 *
                 * Note:
                 * Vím, že ostatní třídy otevřené v dialogu průzkumník projektů jsem neuložil,
                 * protože nevím, zda to tak uživatel chce, třeba někde nechce provést změny
                 * apod. Tak aby se po zavření dialogu nedivil, že se mu vše uložilo i když
                 * třeba nechtěl apod. Tak uložím pouze tu jednu pro vygenerování ID.
                 */
                final Class<?> clazz = App.READ_FILE.getClassFromFile(selectedFrame.getPackageText(), null,
                        GraphClass.getCellsList(), false, false);

                if (clazz == null)
                    App.READ_FILE.loadCompiledClass(selectedFrame.getPackageText(), null);

                // Vygeneruji ID:
                menuMethods.insertGeneratedSerialVersionId(clazz, selectedFrame.getCodeEditor());
            }


            // MenuOrderFrames:
            else if (e.getSource() == itemTile)
                projectExplorer.orderFramesByTile();

            else if (e.getSource() == itemTileUnderHim)
                projectExplorer.orderFramesByTileUnderHim();

            else if (e.getSource() == itemCascade)
                projectExplorer.orderFramesByCascade();

            else if (e.getSource() == itemMinimizeAllFrames)
                projectExplorer.minimizeAllFrames();
        }
    }


    /**
     * Metoda, která slouží pro uložení aktuálně označeného otevřeného souboru.
     */
    private void saveSelectedFile() {
        // Uloží se soubor označeném interním okně, pokud na zdrojovím umístění již neexisstuje, nabídne se
        // možnost pro nově umístění
        final JInternalFrame internalFrame = projectExplorer.getDesktopPane().getSelectedFrame();

        // do DesktopPane ani jine typy oken nevlkládám, ale pro jistotu otestuji, zda se jedná o
        // instanci třídy MyInterlFrame - kdyz by to nemusel být:
        if (internalFrame instanceof FileInternalFrame) {
            final FileInternalFrame myFrame = (FileInternalFrame) internalFrame;

            if (!myFrame.getKindOfFile().equals(KindOfFile.PDF)) {
                // Zjistím, zda daný soubor ještě existuje na uvedeném umístění:
                final boolean result = myFrame.existFile();

                if (result)
                    // Zde příslušný soubor ještě existuje, tak ho mohu opět uložit:
                    myFrame.saveFile();

                    // Zde soubor neexistuje, tak se uživatele dotáži na nové umístění:
                else myFrame.saveFileToNewLocation();
            }

            else
                JOptionPane.showMessageDialog(this,
                        txtCannotSavePdfFileText + "\n" + txtLocation + ": "
                                + myFrame.getFile().getAbsolutePath(),
                        txtCannotSavePdfFileTitle, JOptionPane.INFORMATION_MESSAGE);
        }
    }


    /**
     * Metoda, která zavře interní okno.
     * <p>
     * Ještě před zavřením se zavolá metoda, která otestuje, zda příslušný souhor existuje, případně nabídne možnost
     * nového umístění souboru a pokud existuje tak otestuje, zda došlo k nějakým změnám, případně je uloží
     *
     * @param internalFrame
     *         - interní okno (instance třídy MyInterNalFrame), které se má zavřít
     */
    private static void closeFrame(final JInternalFrame internalFrame) {
        // Přetypuji si okno pro snažší práci:
        final FileInternalFrame myFrame = (FileInternalFrame) internalFrame;

        // Otestuji, zda soubor ještě existuje případně se vytvoří nové umístění,
        // a případně se i uloží zmněy v souboru, pokud k nějakým došlo:
        myFrame.saveCodeBeforeCloseFrame();

        // zavřu označené okno:
        myFrame.dispose();
    }


    /**
     * Metoda, která přenačte interní okno v parametru metody, vždy se otestuje, zda soubor otevřený v daném okně ještě
     * existuje a pokud ano, tak se znovu načte do příslušného editoru, pokud již neexistuje, tak se nabídne možnost pro
     * névé umítění souboru otevřeného v editoru
     *
     * @param internalFrame
     *         - interní okno v komponentě JdesktopPane
     */
    private static void reloadInternalFrame(final JInternalFrame internalFrame) {
        if (((FileInternalFrame) internalFrame).existFile())
            // Zde příslušný soubor ještě existuje, tak ho znovu přečtu a výsledný text vložím do editoru
            // v příslušném okně:
            ((FileInternalFrame) internalFrame).loadTextToEditor();

            // Zde již soubor na původním neexistuje, tak se ho zeptám, zda se má uložit někam jinam:
        else ((FileInternalFrame) internalFrame).saveFileToNewLocation();
    }


    /**
     * Metoda, která znepřístupní - "zakáže" některá tlačítka v tomot menu, Tato metoda se zavolá z konstruktoru ve
     * třídě coby projectExplorerDialog - průzkumník projektu pokud se otevře kořenový adresář coby adresář zvolený jako
     * Workspace, pak nelze například kompilovat třídy, takže se zakáže tlačítko kompilovat, dále nepůjde třídu uložit
     * jako, protože by se mohla vybrat umístění mimo projekt, ... viz tlačítka v metodě:
     */
    final void enableMenuItems() {
        itemGettersAndSetters.setEnabled(false);
        itemConstructor.setEnabled(false);
        itemCompile.setEnabled(false);

        menuAddSerialVersionID.setEnabled(false);
    }


    /**
     * Nastavení označení komponenty JRadioButton na selected.
     * <p>
     * Jedná se o komponentu, která značí, zda je v označeném interním okně nastaveno zalamování textů. Tato metoda jej
     * označení nebo odznačí. Dle toho, zda se v příslušném interním okně zalamují texty nebo ne.
     *
     * @param selected
     *         true pro nastavení označené výše zmíněné komponenty, false - neoznačené.
     */
    void setSelectedRbWrapTextInSelectedFrame(final boolean selected) {
        rbWrapTextInSelectedFrame.setSelected(selected);
    }


    /**
     * Nastavení označení komponenty JRadioButton na hodnotu selected. Komponenta značí, zda se mají zalamovat texty ve
     * všech interních oknech.
     *
     * @param selected
     *         - true, pokud se má označit výše zmíněná komponenta, tj. mají se zalamovat texty ve všech interních
     *         oknech, jinak false.
     */
    void setSelectedRbWrapTextInAllFrames(final boolean selected) {
        rbWrapTextInAllFrames.setSelected(selected);
    }


    /**
     * Nastavení označení komponenty na selected.
     *
     * <i>Jedná se o označení komponenty, která značí, zda se v označeném interním okně zobrazují bílé znaky nebo
     * ne.</i>
     *
     * @param selected
     *         true v případě, že se v označeném interním okně zobrazují bílé znaky, jinak false.
     */
    void setSelectedRbShowWhiteSpaceInSelectedFrame(final boolean selected) {
        rbShowWhiteSpaceInSelectedFrame.setSelected(selected);
    }


    /**
     * Nastavení označení komponenty, která značí, zda se mají ve všech interních oknech zobrazovat bílé znaky.
     *
     * @param selected
     *         - true v případě, že se mají ve všech interních oknech zobrazovat bílé znaky. Jinak false.
     */
    void setSelectedRbShowWhiteSpaceInAllFrames(final boolean selected) {
        rbShowWhiteSpaceInAllFrames.setSelected(selected);
    }


    void setSelectedRbClearWhiteSpaceLineInSelectedFrame(final boolean selected) {
        rbClearWhiteSpaceLineInSelectedFrame.setSelected(selected);
    }

    /**
     * Nastavení označení komponenty, která značí, zda se mají ve všech interních oknech odebírat bílé znaky z
     * "odentrovaného" řádku.
     *
     * @param selected
     *         - true v případě, že se mají odebírat bílé znaky z "odentrovaného" řádku (/ po vložení nového řádku).
     *         Jinak false.
     */
    void setSelectedRbClearWhiteSpaceLineInAllFrames(final boolean selected) {
        rbClearWhiteSpaceLineInAllFrames.setSelected(selected);
    }


    /**
     * Zjištění, zda je označena komponenta pro nastavení zalamování textů ve veškerých interních oknech / otevřených
     * souborech.
     *
     * @return true v případě, že se mají zalamovat texty ve veškerých interních oknech, jinak false.
     */
    boolean isRbWrapTextInAllFramesSelected() {
        return rbWrapTextInAllFrames.isSelected();
    }


    /**
     * Zjištění, zda je označena komponenta pro zobrazování bílých znaků ve veškerých interních oknech / otevřených
     * souborech.
     *
     * @return - true v případě, že se mají ve všech interních oknech zobrazovat bílé znaky. Jinak false.
     */
    boolean isRbShowWhiteSpaceInAllFrameSelected() {
        return rbShowWhiteSpaceInAllFrames.isSelected();
    }


    /**
     * Zjištění, zda je označena komponenta pro odebrání veškerých bílých znaků z prázdného řádku v editoru v případě,
     * že se stiskne klávesa Enter.
     *
     * @return - true v případě, že se mají odebírat bílé znaky z řádku, kde se stiskné klávesa Enter. Jinak false.
     */
    boolean isRbClearWhiteSpaceLineInAllFramesSelected() {
        return rbClearWhiteSpaceLineInAllFrames.isSelected();
    }
}