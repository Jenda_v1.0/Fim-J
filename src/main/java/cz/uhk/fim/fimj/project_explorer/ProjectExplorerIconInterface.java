package cz.uhk.fim.fimj.project_explorer;

import java.io.File;
import javax.swing.ImageIcon;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.GetIconInterface;

/**
 * Tato třída slouží pouze pro vrácení ikony, tyto ikony jsou pouze v rámcí průzkumínku souborů 'Project Exploreru'
 * vracejí se pouze ikony do stromové struktury pro příslušné typy souborů a do vnitřních rámců
 * <p>
 * <p>
 * Přípony čerpány ze zdroje: http://pripony.superia.cz/audio_soubory/
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface ProjectExplorerIconInterface {

    /**
     * Tato metoda slouží pro výběr přiřazení ikony dle typu souboru nebo adresáři
     *
     * @param file
     *         - proměnná typu File, která obsahuje cestu k souboru, dle jeho koncovky - přípony zjistím o jaký typ
     *         souboru jde a dle toho se vybere příslušná ikona, pokud to bude nějaký nedefinovaný formát pro apiikaci,
     *         použije se výchozí ikona pro soubor.
     *
     * @return zvolenou ikonu pro danou položku dle jejího typu
     */
    static ImageIcon getIcon(final File file) {
        /*
         * Vložím si cestu k souboru abych poznal jeho typ do proměnné typu String,
         * protože // se to lépe v podmínkách zpracovává:
         */
        final String filePath = file.getAbsolutePath();


        /*
         * proměnná, do které bude uložena cesta k ikoně:
         */
        final String iconPath;


        if (file.isDirectory()) {
            // Zde vím, že je to adresář, tak otstuji:

            // zda tato cesta obsahuje adresář src, ale nekončí na něj, tj. že poslední adresář není src
            // , pak se použijje ikona pro balíček
            // jinak je to nějaká složka, tak se použije výchozí ikona

            if (containsSrcDir(file) && !file.getName().equals("src"))
                // Zde se jedná o cestu k adresáři, který obsahuje src, ale nekončí na něj, tak se jedná
                // nejspíše o balíček:
                iconPath = "/icons/projectExplorerIcons/PackageIcon.png";

                // Zde se jedná o cestu, která neobsahuje adresář src, tak použiji normální ikonu pro složku:
            else iconPath = "/icons/projectExplorerIcons/ProjectIcon.gif";
        }

        else {
            // Zde se jedná o nějaký soubor, který tento Jlabel reprezentuje,
            // tak si zjistím co je to za typ souboru a dle toho mu nastavím
            // příslušnou ikonu:


            // Javovská třída:
            if (filePath.endsWith(".java"))
                iconPath = "/icons/projectExplorerIcons/JavaClassIcon.jpg";


                // Textové soubory:
            else if (filePath.endsWith(".wps") || filePath.endsWith(".dotx") || filePath.endsWith(".dotm")
                    || filePath.endsWith(".dot") || filePath.endsWith(".docm") || filePath.endsWith(".doc")
                    || filePath.endsWith(".xls") || filePath.endsWith(".rtf") || filePath.endsWith(".csv")
                    || filePath.endsWith(".svg") || filePath.endsWith(".thmx")
                    || filePath.endsWith(".eml") || filePath.endsWith(".mde") || filePath.endsWith(".msg")
                    || filePath.endsWith(".odf") || filePath.endsWith(".scd") || filePath.endsWith(".txt")
                    || filePath.endsWith(".vcf") || filePath.endsWith(".wtx") || filePath.endsWith(".xlb")
                    || filePath.endsWith(".xlk") || filePath.endsWith(".xll") || filePath.endsWith(".xlm")
                    || filePath.endsWith(".xlv") || filePath.endsWith(".xnk") || filePath.endsWith(".xps")
                    || filePath.endsWith(".properties"))
                iconPath = "/icons/projectExplorerIcons/TextFileIcon.png";


                // Obrázek:
            else if (filePath.endsWith(".anb") || filePath.endsWith(".ani") || filePath.endsWith(".bmp")
                    || filePath.endsWith(".cdr") || filePath.endsWith(".cur") || filePath.endsWith(".emf")
                    || filePath.endsWith(".eps") || filePath.endsWith(".gif") || filePath.endsWith(".ico")
                    || filePath.endsWith(".iff") || filePath.endsWith(".jfif") || filePath.endsWith(".jpe")
                    || filePath.endsWith(".jpg") || filePath.endsWith(".jpeg") || filePath.endsWith(".pbm")
                    || filePath.endsWith(".pcx") || filePath.endsWith(".pdp") || filePath.endsWith(".pgm")
                    || filePath.endsWith(".pic") || filePath.endsWith(".pix") || filePath.endsWith(".png")
                    || filePath.endsWith(".ppm") || filePath.endsWith(".psd") || filePath.endsWith(".psp")
                    || filePath.endsWith(".pxr") || filePath.endsWith(".raw") || filePath.endsWith(".rle")
                    || filePath.endsWith(".sxd") || filePath.endsWith(".std") || filePath.endsWith(".tex")
                    || filePath.endsWith(".tga") || filePath.endsWith(".tif") || filePath.endsWith(".tiff")
                    || filePath.endsWith(".vsd") || filePath.endsWith(".vst") || filePath.endsWith(".vsw")
                    || filePath.endsWith(".wi") || filePath.endsWith(".wbmp") || filePath.endsWith(".wmf")
                    || filePath.endsWith(".xbm") || filePath.endsWith(".xpm"))
                iconPath = "/icons/projectExplorerIcons/ImageIcon.png";


                // Access
            else if (filePath.endsWith(".accdb") || filePath.endsWith(".accde") || filePath.endsWith(".accdr") || filePath.endsWith(".dbf") || filePath.endsWith(".mda") || filePath.endsWith(".mdb") || filePath.endsWith(".mdw") || filePath.endsWith(".mdz"))
                iconPath = "/icons/projectExplorerIcons/AccessIcon.png";


                // Word:
            else if (isWordFile(filePath))
                iconPath = "/icons/projectExplorerIcons/WordIcon.png";


                // OneNote
            else if (filePath.endsWith(".one") || filePath.endsWith(".onepkg") || filePath.endsWith(".onetmp")
                    || filePath.endsWith(".onetoc") || filePath.endsWith(".onetoc2"))
                iconPath = "/icons/projectExplorerIcons/OneNoteIcon.png";


                // Excell
            else if (isExcelFile(filePath))
                iconPath = "/icons/projectExplorerIcons/ExcelIcon.png";


                // PowerPoint
            else if (filePath.endsWith(".pot") || filePath.endsWith(".potm") || filePath.endsWith(".potx")
                    || filePath.endsWith(".ppa") || filePath.endsWith(".ppam") || filePath.endsWith(".pps")
                    || filePath.endsWith(".ppsm") || filePath.endsWith(".ppsx") || filePath.endsWith(".ppt")
                    || filePath.endsWith(".pptm") || filePath.endsWith(".pptx") || filePath.endsWith(".sldm")
                    || filePath.endsWith(".sldx"))
                iconPath = "/icons/projectExplorerIcons/PowerPointIcon.png";


                // PDF:
            else if (filePath.endsWith(".pdf"))
                iconPath = "/icons/projectExplorerIcons/PdfIcon.jpg";


                // Jar:
            else if (filePath.endsWith(".jar"))
                iconPath = "/icons/projectExplorerIcons/JarIcon.jpg";


                // Video:
            else if (filePath.endsWith(".aif") || filePath.endsWith(".aiff") || filePath.endsWith(".asf")
                    || filePath.endsWith(".au") || filePath.endsWith(".avi") || filePath.endsWith(".cda")
                    || filePath.endsWith(".dvr-ms") || filePath.endsWith(".fla") || filePath.endsWith(".miv")
                    || filePath.endsWith(".m3u") || filePath.endsWith(".midi") || filePath.endsWith(".mpeg")
                    || filePath.endsWith(".mswmm") || filePath.endsWith(".ogg") || filePath.endsWith(".swf")
                    || filePath.endsWith(".wma") || filePath.endsWith(".wmp") || filePath.endsWith(".wms")
                    || filePath.endsWith(".wmv") || filePath.endsWith(".wpl") || filePath.endsWith(".wvx"))
                iconPath = "/icons/projectExplorerIcons/VideoIcon.png";


                // Wav:
            else if (filePath.endsWith(".wav"))
                iconPath = "/icons/projectExplorerIcons/WavIcon.jpg";


                // Mp3
            else if (filePath.endsWith(".mp3"))
                iconPath = "/icons/projectExplorerIcons/Mp3Icon.png";


                // Internetove stranky:
            else if (filePath.endsWith(".asp") || filePath.endsWith(".jsp") || filePath.endsWith(".vbs") || filePath.endsWith(".vbp") || filePath.endsWith("."))
                iconPath = "/icons/projectExplorerIcons/InternetPagesIcon.png";


                // XML:
            else if (filePath.endsWith(".xml"))
                iconPath = "/icons/projectExplorerIcons/XmlIcon.png";


                // html:
            else if (filePath.endsWith(".hta") || filePath.endsWith(".htm") || filePath.endsWith(".html")
                    || filePath.endsWith(".mht") || filePath.endsWith(".mhtml"))
                iconPath = "/icons/projectExplorerIcons/HtmlIcon.png";


                // JavaScript
            else if (filePath.endsWith(".js"))
                iconPath = "/icons/projectExplorerIcons/JavaScriptIcon.jpg";


                // PHP:
            else if (filePath.endsWith(".php"))
                iconPath = "/icons/projectExplorerIcons/PhpIcon.png";


                // css:
            else if (filePath.endsWith(".css"))
                iconPath = "/icons/projectExplorerIcons/CssIcon.png";


                // Zip
            else if (filePath.endsWith(".zip"))
                iconPath = "/icons/projectExplorerIcons/ZipIcon.png";


                // 7Zip
            else if (filePath.endsWith(".7z"))
                iconPath = "/icons/projectExplorerIcons/7zIcon.png";


                // Rar
            else if (filePath.endsWith(".rar"))
                iconPath = "/icons/projectExplorerIcons/RarIcon.png";


                // Ostatní kompresní formáty:
            else if (filePath.endsWith(".ace") || filePath.endsWith(".arj") || filePath.endsWith(".cab")
                    || filePath.endsWith(".ctt") || filePath.endsWith(".gz") || filePath.endsWith(".iso")
                    || filePath.endsWith(".msi") || filePath.endsWith(".nrg") || filePath.endsWith(".pnq"))
                iconPath = "/icons/projectExplorerIcons/CompressionFormatIcon.png";


                // Výchozí ikona:
            else iconPath = "/icons/projectExplorerIcons/DefaultFileIcon.png";
        }


        // PŮVODNÍ - S CHYBOU:
        //		return GetMyIconInterface.getIcon(iconPath, true);


        // NOVE - ABY NESKÁKALY VYJÍMKY:
        return GetIconInterface.getMyIcon(iconPath);
    }


    /**
     * Metoda, která zjistí, zda je nějaký z rodiču souboru v parametru metody je adresář src, resp. zda se příslušný
     * soubor v parametru metody vyskytuje v nějakém adresáři src.
     * <p>
     * Vychází z toho, zda v parametru metody je nějaký soubor - konečný, takže celá jeho předešla cesta počinaje jeho
     * rodičem musí být vše adresáře, tak akorát stači otestovat, zda některý z jeho rodičů je adresář src, ani nemusím
     * testovat, zda je to adresář, jen, zda se jmenuje src
     *
     * @param file
     *         - soubor nebo adrsář o kterém chci vědět, zda se nachází v adresáři src
     *
     * @return true, pokuud se nachíází v adresáři src, jinak false
     */
    static boolean containsSrcDir(final File file) {
        // Zjistím si rodiče:
        final File parentFile = file.getParentFile();

        boolean containsSrcDir = false;

        if (parentFile != null) {
            // rozdělím si celou cestu dle lomítek dle využívaného OS:
            final String[] dirsOnPath = parentFile.getAbsolutePath().split(Constants.OS_PATH_PATTERN);


            // a otestuji, zda se nějaký název adresáře jmenuje src:
            for (final String s : dirsOnPath)
                if (s.equals("src")) {
                    containsSrcDir = true;
                    break;
                }
        }

        return containsSrcDir;
    }


    /**
     * Metoda, která otestuje, zda se jedná o soubor typu Word, který se pozná dle jeho přípony, takže pokud bude mít
     * soubor příslušnou příponu, která náleží souboru typu Word, tak se vrátí true, jinak false.
     * <p>
     * Note: pokusil jsem se všechny soubory otevřít pomocí metody pro otevření dokumentů typu Word, ale nešlo to, tak
     * jsem je přestěhoval do textoych souborů, a ty jdou otevřít
     *
     * @param filePath
     *         - absolutní cesta k souboru i s jeho příponou
     *
     * @return pokud budou konec cesty k souboru (proměnná filePath) končit jednou z přípon, které náleží soubory typu
     * Word, tak se vrátí true, jinak false, pokud se jedná o jiný typ souboru
     */
    static boolean isWordFile(final String filePath) {
        return filePath.endsWith(".docx");
    }


    /**
     * Metoda, která otestuje, zda se jedná o soubor typu Excel. To otestuje dle přípony souboru - proměnná filePath
     * <p>
     * Pokud se jedná o příponu, která náleží soubory typu Excel viz podmínky metody, pak se vrátí true, jinak false
     *
     * @param filePath
     *         - absolutní cesta k souboru i s jeho příponou, ze které se pozná typ souboru - pouze některé hlídané, ne
     *         doslova všechny přípony
     *
     * @return true, pokud se jedná o soubor typu Excel s hlídanou příponou, jinak false
     */
    static boolean isExcelFile(final String filePath) {
        return filePath.endsWith(".dif") || filePath.endsWith(".prn") || filePath.endsWith(".slk")
                || filePath.endsWith(".wk1") || filePath.endsWith(".wk2") || filePath.endsWith(".wk3")
                || filePath.endsWith(".wk4") || filePath.endsWith(".wk5") || filePath.endsWith(".wq1")
                || filePath.endsWith(".xla") || filePath.endsWith(".xlam") || filePath.endsWith(".xlsb")
                || filePath.endsWith(".xlsm") || filePath.endsWith(".xlsx") || filePath.endsWith(".xlt")
                || filePath.endsWith(".xlw");
    }


    /**
     * Metoda, která otestuje, zda se jedná o soubor typu Pdf. To otestuje dle přípony souboru - proměnná filePath
     * <p>
     * Pokud se jedná o příponu, která náleží souboru typu Pdf viz podmínky metody, pak se vrátí true, jinak false
     *
     * @param filePath
     *         - absolutní cesta k souboru i s jeho příponou, ze které se pozná typ souboru - pouze některé hlídané, ne
     *         doslova všechny přípony
     *
     * @return true, pokud se jedná o soubor typu Pdf s hlídanou příponou, jinak false
     */
    static boolean isPdfFile(final String filePath) {
        return filePath.endsWith(".pdf");
    }


    /**
     * Metoda, která otestuje, zda náhodnou soubor nekončí na jednu z přípon, například spustitelných souborů, souborů
     * typu video, mp3, mp4, ... prostě, zda se nejedná o soubor, který nelze otevřít v editou kodu, který umí otevřít
     * pouze text, resp. zdrojový kodu třídy, což je jeho účetl, ale už neumí otevřít zmíněné formáty typu vide, ...,
     * tak to musím hlídat.
     * <p>
     * Note: obrázkové formáty zde nebudu uvádět v takových případěch se zobrazí pouze "taková hatmatilka" ale už
     * nepůjdou komprimované formáty, vide, zvuky, ...
     *
     * @param filePath
     *         - absolutní cesta k souboru, abych mohl otestovat jeho příponu
     *
     * @return true, pkud se jedná o jeden z formátů souborů, který nepůjde otevřít (viz testované přípony souborů),
     * jinak false, pokud se jedná o soubor, který by měl jít otevřít v editoru kodu
     */
    static boolean isDoNotOpenFile(final String filePath) {
        return filePath.endsWith(".exe") || filePath.endsWith(".jar") || filePath.endsWith(".apk")
                || filePath.endsWith(".msi") || filePath.endsWith("swf.") || filePath.endsWith(".pif")
                || filePath.endsWith(".bat") || filePath.endsWith(".lnk") || filePath.endsWith(".aac")
                || filePath.endsWith(".cdr") || filePath.endsWith(".m3u") || filePath.endsWith(".mp3")
                || filePath.endsWith(".pls") || filePath.endsWith(".wav") || filePath.endsWith(".cda")
                || filePath.endsWith("flac.") || filePath.endsWith(".mka") || filePath.endsWith(".wma")
                || filePath.endsWith(".7z") || filePath.endsWith(".arj") || filePath.endsWith(".bz2")
                || filePath.endsWith(".rar") || filePath.endsWith(".zip") || filePath.endsWith(".ace")
                || filePath.endsWith(".axx") || filePath.endsWith(".gz") || filePath.endsWith(".lha")
                || filePath.endsWith(".tar") || filePath.endsWith(".avhd") || filePath.endsWith(".img")
                || filePath.endsWith(".mdf") || filePath.endsWith(".nrg") || filePath.endsWith(".cue")
                || filePath.endsWith(".iso") || filePath.endsWith(".mds") || filePath.endsWith(".vhd")
                || filePath.endsWith(".3gp") || filePath.endsWith(".flv") || filePath.endsWith(".mov")
                || filePath.endsWith(".trp") || filePath.endsWith(".wmv") || filePath.endsWith(".avi")
                || filePath.endsWith(".mkv") || filePath.endsWith(".qt") || filePath.endsWith(".ts");

    }


    /**
     * Metoda, která zjistí, zda se jedná o obrázek, tj. zda se jedná o soubor, který obsahuje jednu z přípon pro
     * obrázek.
     *
     * @param filePath
     *         - cesta k nějakému souboru, o kterém se má zjistit, zda se jedná o obrázek.
     *
     * @return true, pokud soubor pedstavuje obrázek, tj. obsahuje jednu z příslušných přípon jinak false.
     */
    static boolean isImage(final String filePath) {

        return filePath.endsWith(".ai") || filePath.endsWith(".bmp") || filePath.endsWith(".cb7")
                || filePath.endsWith(".cba") || filePath.endsWith(".cbr") || filePath.endsWith(".cbt")
                || filePath.endsWith(".cbz") || filePath.endsWith(".cdr") || filePath.endsWith(".dds")
                || filePath.endsWith(".dgn") || filePath.endsWith(".dwg") || filePath.endsWith(".gif")
                || filePath.endsWith(".ico") || filePath.endsWith(".jpe") || filePath.endsWith(".jpeg")
                || filePath.endsWith(".jpg") || filePath.endsWith(".mpo") || filePath.endsWith(".pcx")
                || filePath.endsWith(".png") || filePath.endsWith(".psd") || filePath.endsWith(".skp")
                || filePath.endsWith(".srf") || filePath.endsWith(".stp") || filePath.endsWith(".svg")
                || filePath.endsWith(".tif") || filePath.endsWith(".tiff");
    }
}