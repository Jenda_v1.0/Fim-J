package cz.uhk.fim.fimj.project_explorer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.forms.SourceDialogForm;

/**
 * Tato třída slouží jako dialog, který slouží pro zadání údajů pro vytvoření nového adresáře, případně adresářů, resp.
 * dalo by se říci že balíčků v projektu.
 * <p>
 * Třída pomocí regulárních výrazů otestuje syntaxi zadaných adresářů. Syntaxe: folder1.folder2 názvy adresářů oddělené
 * tečkami - pokud jich je více a tyto adresáře se vloží do adresáře, který se označil jako položka ve stromové
 * struktuře
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class NewFolderForm extends SourceDialogForm implements LanguageInterface, ActionListener {
	
	private static final long serialVersionUID = 1L;


	/**
	 * Regulární výraz pro rozpoznání syntaxe pro zadání adresáře / řů: Syntaxe:
	 * folder_12 (. next_Folder) adresáře oddělené desetinnou tečkou a mohou
	 * obsahovat pouze čísla a písmena bez diakritiky a podtržítko
	 */
	private static final String REGEX_FOR_FOLDER = "^\\s*\\w+(\\s*\\.\\s*\\w+)*\\s*$";
	
	
	// Popisek s informací a label s informací o chybě
	private final JLabel lblInfo, lblErrorInfo;
	
	
	
	/**
	 * textové pole pro zadání adresářů k vytvoření
	 */
	private final JTextField txtFolder;
	
	
	
	// Proměnné typu String, do kterých si vložím texty načtené ze souboru, popř. výchozí hodnoty (texty)
	// pokud se nepodaří soubor načíst:
	private static String txtWrongFormatText, txtWrongFormatTitle, txtEmptyFieldText, txtEmptyFieldTitle;
	

	
	/**
	 * Konstruktor této třídy.
	 */
	public NewFolderForm() {
		super();
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/projectExplorerIcons/NewFolderDialogIcon.png",
				new Dimension(520, 250), true);
		
		gbc = createGbc();
		
		index = 0;
		
		
		
		gbc.gridwidth = 2;
		
		
		lblErrorInfo = new JLabel("", JLabel.CENTER);
		lblErrorInfo.setFont(ERROR_FONT);
		lblErrorInfo.setForeground(Color.RED);
		lblErrorInfo.setVisible(false);
		
		setGbc(index, 0, lblErrorInfo);
		
		
		
		
		
		lblInfo = new JLabel("", JLabel.CENTER);
		setGbc(++index, 0, lblInfo);
		
		
		
		
		
		
		txtFolder = new JTextField();
		setGbc(++index, 0, txtFolder);
		
		
		
		txtFolder.addKeyListener(new KeyAdapter() {
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					testData();
				
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					variable = false;
					dispose();
				}
			}
			
			
			@Override
			public void keyReleased(KeyEvent e) {
				NameFormAbstract.highlightTextField(txtFolder, lblErrorInfo, REGEX_FOR_FOLDER);
			}
		});
		
		
		
		
		
		
		
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		btnOk = new JButton();
		btnCancel = new JButton();
		
		btnOk.addActionListener(this);
		btnCancel.addActionListener(this);
		
		pnlButtons.add(btnOk);
		pnlButtons.add(btnCancel);
		setGbc(++index, 0, pnlButtons);
		
		
		
		
		addWindowFocusListener(getWindowsAdapter());
		
		
		pack();
		setLocationRelativeTo(null);
	}
	
	

	
	
	
	/**
	 * Metoda, která zviditelní dialog a udělá ho tak modálním, a až uživatel klikne
	 * na tlačítko ok nebo stiskne enter, tak se otestuji zadaná data a případně se
	 * vrátí adresáře, které se mají vytvořit nebo vypíše chybové hlášení, pokud
	 * bude nějaká chyba
	 * 
	 * @return text coby adresáře oddělené tečkou, které se mají vytvořit
	 */
	final String getFoldersForCreate() {
		setVisible(true);
		
		if (variable)
			return txtFolder.getText().replace("\\s", "");
		
		return null;
	}
	
	

	
	
	
	
	/**
	 * Metoda, která se zavolá po kliknutí na tlačítko OK, nebo po stisknutí klávesy
	 * Enter. Metoda otestuje zadaná data a buď je vrátí nebo vypíše nějakou
	 * chybovou hlášku s informací o chybě, ke které došlo
	 */
	private void testData() {
		if (!txtFolder.getText().isEmpty()) {
			if (txtFolder.getText().replace("\\s", "").matches(REGEX_FOR_FOLDER)) {
				variable = true;
				dispose();
			}
			else
				JOptionPane.showMessageDialog(this, txtWrongFormatText, txtWrongFormatTitle, JOptionPane.ERROR_MESSAGE);
		}		
		else
			JOptionPane.showMessageDialog(this, txtEmptyFieldText, txtEmptyFieldTitle, JOptionPane.ERROR_MESSAGE);
	}






	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnOk)
				testData();
			
			
			else if (e.getSource() == btnCancel) {
				variable = false;
				dispose();
			}
		}		
	}
	

	
	

	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			setTitle(properties.getProperty("Pe_N_Folder_F_DialogTitle", Constants.PE_NFOLDER_F_DIALOG_TITLE));
			
			btnOk.setText(properties.getProperty("Pe_N_Folder_F_BtnOK", Constants.PE_NFOLDER_F_BTN_OK));
			btnCancel.setText(properties.getProperty("Pe_N_Folder_F_BtnCancel", Constants.PE_NFOLDER_F_BTN_CANCEL));
			
			lblErrorInfo.setText(properties.getProperty("Pe_N_Folder_F_LblErrorInfo", Constants.PE_NFOLDER_F_LBL_ERROR_INFO));
			
			lblInfo.setText(properties.getProperty("Pe_N_Folder_F_LblInfo", Constants.PE_NFOLDER_F_LBL_INFO) + ":");
			
			// Texty do chybových hlášek:
			txtWrongFormatText = properties.getProperty("Pe_N_Folder_F_TxtWrongFormatText", Constants.PE_NFOLDER_F_TXT_WRONG_FORMAT_TEXT);
			txtWrongFormatTitle = properties.getProperty("Pe_N_Folder_F_TxtWrongFormatTitle", Constants.PE_NFOLDER_F_TXT_WRONG_FORMAT_TITLE);
			txtEmptyFieldText = properties.getProperty("Pe_N_Folder_F_TxtEmptyFieldText", Constants.PE_NFOLDER_F_TXT_EMPTY_FIELD_TEXT);
			txtEmptyFieldTitle = properties.getProperty("Pe_N_Folder_F_TxtEmptyFieldTitle", Constants.PE_NFOLDER_F_TXT_EMPTY_FIELD_TITLE);
		}
		
		
		else {
			setTitle(Constants.PE_NFOLDER_F_DIALOG_TITLE);
			
			btnOk.setText(Constants.PE_NFOLDER_F_BTN_OK);
			btnCancel.setText(Constants.PE_NFOLDER_F_BTN_CANCEL);
			
			lblErrorInfo.setText(Constants.PE_NFOLDER_F_LBL_ERROR_INFO);
			
			lblInfo.setText(Constants.PE_NFOLDER_F_LBL_INFO + ":");
			
			// Texty do chybových hlášek:
			txtWrongFormatText = Constants.PE_NFOLDER_F_TXT_WRONG_FORMAT_TEXT;
			txtWrongFormatTitle = Constants.PE_NFOLDER_F_TXT_WRONG_FORMAT_TITLE;
			txtEmptyFieldText = Constants.PE_NFOLDER_F_TXT_EMPTY_FIELD_TEXT;
			txtEmptyFieldTitle = Constants.PE_NFOLDER_F_TXT_EMPTY_FIELD_TITLE;
		}
	}
}