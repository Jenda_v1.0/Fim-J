package cz.uhk.fim.fimj.project_explorer;

import java.io.File;
import java.util.List;

import javax.swing.tree.TreeModel;

/**
 * Tato třída slouží jako vlákno, které slouží pro načtení stromové struktury souborů ve zvoleném adresáři coby
 * Workspace nebo adresář otevřeného projektu. Resp. toto vlákno zmapuje soubory a adresáře v označeném adresáři při
 * otevření průzkumníku projektů.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class LoadTreeThread extends Thread {

    /**
     * Komponenta, která představuje stromovou strukturu - do této komponenty načtu zmapované soubory.
     */
    private final TreeStructure myTree;


    /**
     * Kolekce obsahující cesty k adresářům, které se mají rozbalit ve stromové struktuře.
     */
    private final List<File> fileList;


    /**
     * True značí, že se mají vždy rozbalit veškeré podadresáře adresáře, který se má rozablit ve stromové struktuře.
     * <p>
     * Tedy v listu fileList jsou cesty k adresářům, které se mají rozbalit v případě, že je tato proměnná true, rozbalí
     * se spolu s tím s tím požadovaným adesářem i veškeré jeho podadresáře.
     * <p>
     * Pokud bude tato proměnná false, rozbalí se pouze ten jeden požadovaný adresář v listu fileList.
     */
    private final boolean expandAllSubDirs;


    /**
     * Konstruktor třídy.
     *
     * @param myTree
     *         - komponenta Jtree - pro stromovou strukturu.
     * @param fileList
     *         - kolekce cest k adresářům, které se mají rozbalit ve stromové struktuře.
     * @param expandAllSubDirs
     *         - true v případě, že se mají rozbalit veškeré podadresáře adresářů, které se má rozbalit (v listu
     *         fileList).
     */
    public LoadTreeThread(final TreeStructure myTree, final List<File> fileList, final boolean expandAllSubDirs) {
        this.myTree = myTree;
        this.fileList = fileList;
        this.expandAllSubDirs = expandAllSubDirs;
    }


    @Override
    public void run() {
        final File fileWorkspaceDir = myTree.getFileWorkspaceDir();

        final TreeModel model = new FileTreeModel(fileWorkspaceDir);

        myTree.setModel(model);

        // Rozbalení vybraných adresářů ve stromové struktuře:
        myTree.expandSrcDir(fileList, expandAllSubDirs);

        myTree.repaint();
        myTree.revalidate();
    }
}