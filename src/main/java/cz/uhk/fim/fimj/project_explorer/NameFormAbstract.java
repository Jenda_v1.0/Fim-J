package cz.uhk.fim.fimj.project_explorer;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Properties;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.forms.SourceDialogForm;

/**
 * Tato třída slouží jako abstraktní třída pro dialogy pro název nového souboru a pro dialog pro zadání názvu
 * excelovského listu, dialogy: NewFileForm.java a NewExcelListNameForm.java
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class NameFormAbstract extends SourceDialogForm {

	private static final long serialVersionUID = 1L;


	/**
	 * Komponenta, která slouží pro zadání návzu souboru nebo listu.
	 */
	protected JTextField txtName;

	
	/**
	 * Promenná typu Jlabel, ve které bude text se základní informací o tom, že se
	 * má do textového pole zadat název nového souboru nebo názvu listu a vybrat se
	 * jeho přípona coby typ souboru (v případě nového souboru, ne listu)
	 */
	protected JLabel lblInfo;
	
	
	/**
	 * Proměná typu Jlabel, ve které bude uložen text, který bude uživatele
	 * informovat, že zadal text ve špatné syntaxi
	 */
	JLabel lblError;
	

	
	
	// Proměnné do chybových výpisů - hlášek:
	private static String txtErrorFileNameText, txtErrorFileNameTitle, txtIsEmptyText, txtIsEmptyTitle;
	

	
	
	/**
	 * Metoda, která vytvoří a vrátí posluachač na stisknutí klávesy, který bude
	 * reagovat na enter, tak, že otestuje data z komponent od uživatele - název
	 * souboru nebo listu, dále po stisknutí klávesy Escape zavře dialog, a po
	 * stisknutí libolvolné jiné klávesy otestuje zadaná data z hlediska syntaxe a
	 * případně zobrazí hlášení uživateli o chybně syntaxi
	 * 
	 * @return novou instanci keyAdapteru popsaného výše
	 */
	KeyAdapter getMyKeyAdapter() {
		return new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				// Jestliže se stisknul enter, tak otestuji data:
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					testData();
				
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					variable = false;
					dispose();
				}	
				
				// jinak se stisklo, resp. v tomto případě uvolnilo libovolné
				// jiné tlačítko, tak otestuji zadanou syntaxi:
				else highlightTextField(txtName, lblError, REGEX_FOR_FILE_NAME);
			}
		};
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metdda, která otestuje zadaná data po kliknutí na tlačítko OK, nebo
	 * stisknutím enteru v textovém poli pro zadání názvu nového souboru nebo listu.
	 * Vlastně se jedná akorát o to, že se otestuje zadaný název souboru nebo listu
	 * regulárním výrazem, zda odpovídá požadované syntaxi a pak se buď vypíše
	 * hláška nebo se vytvoří požadovaný soubor
	 * 
	 * Pokud zadaný text bude odpovídat regulárnímu výrazu - správné syntaxe, tak se
	 * nastaví hodnota proměnné variable na true a zavře si dialog, a pomocí
	 * zavolané metody ze vrátí text z textového pole včetně přípony (- v případě
	 * názvu souboru, u listu ne, tam jen samotná název listu)
	 */
	protected final void testData() {
		// Zde není moc co řešit, akorát otestuji, zda zadaná syntaxi pro název souboru
		// odpovída regulárnímu výrazu:
		if (!txtName.getText().replaceAll("\\s", "").isEmpty()) {
			// Zde není pole prázdné, tak mohu otestovat regulární výraz:
			if (txtName.getText().matches(REGEX_FOR_FILE_NAME)) {
				variable = true;
				dispose();
			}
			else
				JOptionPane.showMessageDialog(this, txtErrorFileNameText, txtErrorFileNameTitle,
						JOptionPane.ERROR_MESSAGE);
		}
		else
			JOptionPane.showMessageDialog(this, txtIsEmptyText, txtIsEmptyTitle, JOptionPane.ERROR_MESSAGE);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která po uvolnění libovolného tlačítka červeně obarví pozadí textvého
	 * pole pro název souboru nebo naopak ho obarví bíle, pokud bude syntaxe
	 * zadaného názvu souboru odpovídat regulárnímu výrazu, pokud nebude, tak se
	 * obarví červeně, dle dle chybného názvu ještě zobrazí lblError, pokud nebude
	 * syntaxe odpovídat regulárnímu výrazu, jinak ho zneviditelní - pokud bude
	 * videt
	 * 
	 * @param txtField
	 *            - textové pole, jehož text se má testovat
	 * 
	 * @param lblError
	 *            - komponenta typu Jlabel, která se má zobrazit, pokud nebude text
	 *            v textovém pole - txtField odpovídat regulárnímu výrazu REGEX
	 * 
	 * @param REGEX
	 *            - regulární výraz, kterému by měl odpovídat text v textovém pole
	 *            txtField
	 */
	public static void highlightTextField(final JTextField txtField, final JLabel lblError, final String REGEX) {
		// Otestuji, zda text v poli odpovídá syntaxi:
		if (txtField.getText().matches(REGEX)) {
			// Zde text v poli odpovídá syntaxi, tak otestuji, zda je pole obarvené bílou barvou,
			// a pokud ne, tak ho obarvím bílou, protože je název v "pořádku"
			if (txtField.getBackground() != Color.WHITE)
				txtField.setBackground(Color.WHITE);
			
			// Dále zneviditelním text - pokud není:
			if (lblError != null && lblError.isVisible())
				lblError.setVisible(false);
		}
			
		// Zde zadaný název neodpovídá syntaxi, tak obravím pole na červeno (pokud není)
		else {
			if (txtField.getBackground() != Color.RED)
				txtField.setBackground(Color.RED);
			
			if (lblError != null && !lblError.isVisible())
				lblError.setVisible(true);
		} 
	}
	
	
	
	
	

	
	/**
	 * Metoda, která naplní textové proměnné, které jsou potřeba pro případně vpis
	 * uživateli, tato metoda je splečná pro dialogy NewFileForm.java a
	 * NewExcelListNameForm.java, tak se do této metody akorát předá parametr coby
	 * proměnná s načtenými texty a naplní se příslušné proměnné nějakými hodnotami
	 * ve zvoleném jazyce (nebo výchozími)
	 * 
	 * @param properties
	 *            - proměnná typu Properties, která obsahuje texty pro tuto aplikaci
	 *            ve zvoleném jazyce
	 */
	protected final void fillTextVariables(final Properties properties) {
		if (properties != null) {
			// TEXT NÍZE  STEJNY JAKO V : NewFileForm.java
			lblError.setText(properties.getProperty("Pe_Nelnf_LblError", Constants.PE_NELNM_LBL_ERROR));
			
			btnOk.setText(properties.getProperty("Pe_Nelnf_BtnOk", Constants.PE_NELNM_BTN_OK));
			btnCancel.setText(properties.getProperty("Pe_Nelnf_BtnCancel", Constants.PE_NELNM_BTN_CANCEL));
			
			txtErrorFileNameText = properties.getProperty("Pe_Nelnf_TxtErrorFileNameText",
					Constants.PE_NELNM_TXT_ERROR_FILE_NAME_TEXT)
					+ "\n"
					+ properties.getProperty("Pe_Nelnf_TxtErrorFileNameTextSyntaxe",
							Constants.PE_NELNM_TXT_ERROR_FILE_NAME_TEXT_SYNTAXE)
					+ ": " + properties.getProperty("Pe_Nelnf_TxtErrorFileNameTextSyntaxeInfo",
							Constants.PE_NELNM_TXT_ERROR_FILE_NAME_TEXT_SYNTAXE_INFO);
			
			txtErrorFileNameTitle = properties.getProperty("Pe_Nelnf_TxtErrorFileNameTitle", Constants.PE_NELNM_TXT_ERROR_FILE_NAME_TITLE);
			txtIsEmptyText = properties.getProperty("Pe_Nelnf_TxtIsEmptyText", Constants.PE_NELNM_TXT_IS_EMPTY_TEXT);
			txtIsEmptyTitle = properties.getProperty("Pe_Nelnf_TxtIsEmptyTitle", Constants.PE_NELNM_TXT_IS_EMPTY_TITLE);
		}
		
		
		else {
			lblError.setText(Constants.PE_NELNM_LBL_ERROR);
			
			btnOk.setText(Constants.PE_NELNM_BTN_OK);
			btnCancel.setText(Constants.PE_NELNM_BTN_CANCEL);
			
			txtErrorFileNameText = Constants.PE_NELNM_TXT_ERROR_FILE_NAME_TEXT + "\n"
					+ Constants.PE_NELNM_TXT_ERROR_FILE_NAME_TEXT_SYNTAXE + ": "
					+ Constants.PE_NELNM_TXT_ERROR_FILE_NAME_TEXT_SYNTAXE_INFO;
			
			txtErrorFileNameTitle = Constants.PE_NELNM_TXT_ERROR_FILE_NAME_TITLE;
			txtIsEmptyText = Constants.PE_NELNM_TXT_IS_EMPTY_TEXT;
			txtIsEmptyTitle = Constants.PE_NELNM_TXT_IS_EMPTY_TITLE;
		}
	}
}