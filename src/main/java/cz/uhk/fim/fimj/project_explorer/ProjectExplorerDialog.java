package cz.uhk.fim.fimj.project_explorer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.beans.PropertyVetoException;
import java.io.File;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JDesktopPane;
import javax.swing.JDialog;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.code_editor_abstract.RSyntaxTextAreaKeyActionHelper;
import cz.uhk.fim.fimj.language.EditProperties;
import org.apache.commons.io.FilenameUtils;

import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.GetIconInterface;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.preferences.PreferencesApi;

/**
 * Tato třída slouží jako dialog, který slouží pro prohlížení struktury adresáře, který slouží jako Workspace, tento
 * adresář, který slouží jako Workspace, se pokaždé otestuje, zda ještě existuje, a pokud ano, tak se do tohoto dialogu
 * načtou všechny příslušné soubory, které se najdou v příslušné testované složce, podobně jako například v Eclipse také
 * bude možné, ale v tomto případně pouze některé soubory také zobrazit nepůjde například otevřít obrazáze, apod. poouze
 * se načte jeho binární verze,... podobně se všemi ostatními soubory, půjdou načíst pouze ty textové.
 * <p>
 * Dále každý takto otevřený soubor se otevře vždy v novém okně coby InternalFrames aby bylo možné jich zobrazit více
 * najednou a editovat příslušné soubory v podstatě dle libosti a dostupných funkcí.
 * <p>
 * Tento dialog slouží jako "hlavní" okno, do které se pouze vkládají jednotlivé komponenty, jako je například třída,
 * která sloužíá jako strom - stromová struktura projektů, dále třída coby JdesktopPane, pro internalFrames což budou
 * konkrétní otevření soubory, dále menu, ...
 * <p>
 * Libovolný soubor lze otevřít vždy pouze jednou.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ProjectExplorerDialog extends JDialog implements LanguageInterface {

    private static final long serialVersionUID = 1574698086738373559L;


    /**
     * Tato "komponenta" obsahuje metody pro čtení a zápis do vybraných souborů od Microsoftu, například do Wordu a
     * excelu
     */
    private static final ReadWriteFileInterface readWriteMicrosoftFile = new ReadWriteFile();


    /**
     * Komponenta, do které se budou vkládat ostatní InternalFrames, v tomto případě instance třídy MyInternalFrame
     */
    private final JDesktopPane desktopPane;


    /**
     * Menu pro dialog.
     */
    private final Menu menu;


    /**
     * soubor s texty v označeném jazyce pro předání:
     */
    private static Properties languageProperties;


    /**
     * statická a finální proměnná, která značí velikost o kolik se budou posouvat okna v kaskádovém stylu - "Cascade",
     * pokud se tak uživatel rozhodne okna seřadit tj. levý horní roh interních oken coby otevřených souborů se vždy
     * posune o velikost FRAME_OFFSET doprava a dolu
     */
    private static final int FRAME_OFFSET = 30;


    /**
     * Tato proměnná slouží pro uchování cesty k otevřenému projektu v aplikaci a v tomto dialogu i jako kořenový
     * adresář, je zde potřebna jako globální neboli instanční proměnná pro získání balíčku, když se otevře nějaá třída,
     * tak aby se tím nezatěžovala každá třída, ale stačí, když bude uložena pouze zde, a pomocí getrů se k ní bude
     * přístupovat a k její modifikaci docházet nebude, nebude se menit adresář otevřeného projektu - kromě
     * přejmenování
     */
    private final File fileProjectDir;


    /**
     * Tato proměnná slouží coby editor výstupů, do kterého se vypisují výstupy z kompilace, apod. je zobrazen, pouze
     * pokud se otevře tento dialog coby průzkumník projektů s otevřeným projektem v aplikaci
     */
    private final OutputEditorForCompilation outputEditor;


    /**
     * Tato proměnná slouží pro uložení cesty k adresáři, který je otevřen v tomto průzkumníku souborů, jedná se pouze o
     * to, že se do této proměnné uloží cesta k otevřenému adresářu v průzkumníkovi, protože jsou zde dva konstruktory,
     * jeden pro Workspace a druhý pro projekt, ale ten pro Workspace zde není potřebna binaj nijak džet a ten pro
     * projekt, ano, ale nebylo by moc přehledné testovat, jestli nějaká proměnná není například null a dle toho napsat
     * nějakou cestu, apod. tak je na to tato proměnná, do které se vloží cesta k otevřenému adresář a pomocí této
     * proměnné se vloží text do titulku tohoto dialogu, aby měl uživatel "lepší přehled" o tom, jaký projekt je
     * otevřen, protože je tento dialog modální a nebylo by pro uživatele moc dobré přepínat mezi tímto dialogem a oknem
     * aplikace
     */
    private final String pathToOpenDir;


    /**
     * Objekt s načteným souborem .properties obsahující nastavení pro dialog průzkumník projektů.
     * <p>
     * Proměnná je zde, protože se předává dále do interního okna a do editoru a do menu kvůli nastavení zalamování
     * textů apod.
     */
    private final Properties codeEditorProp;


    // Proměnné pro texty do chybových hlášek (nějaké upozornění apod.):
    private String txtCannotSavePdfFileText;
    private String txtLocation;
    private String txtCannotSavePdfFileTitle;
    private String txtFileIsAlreadyOpenText;
    private String txtFileIsAlreadyOpenTitle;
    private String txtExcelFileDoNotContainsListText;
    private String txtFile;
    private String txtExcelFileDoNotContainsListTitle;
    private String txtFileKind;
    private String txtDoNotOpen;
    private String txtFileDoNotOpen;
    private String txtFileDoNotExistText;
    private String txtFileDoNotExistTitle;


    /**
     * Konstruktor této třídy.
     * <p>
     * Tento konstruktor se zavolá, pokud se má otevřít tento dialog s kořenovým adresářem ve Workspace.
     *
     * @param fileWorkspaceDir
     *         - reference typu File na adresář coby Workspace
     * @param languageProperties
     *         - soubor s texty ve zvoleném jazyce uživatelem pro aplikaci
     */
    public ProjectExplorerDialog(final File fileWorkspaceDir, final Properties languageProperties) {
        // tuto proměnnou nebudu inicializovat, není v tomto případě ani potřeba
        outputEditor = null;

        fileProjectDir = null;

        // uložím si cestu k otevřenému adresáři coby Workspace, abych ji mohl zobrazit v titulku dialogu:
        pathToOpenDir = fileWorkspaceDir.getAbsolutePath();


        ProjectExplorerDialog.languageProperties = languageProperties;

        // Načtení konfiguračního souboru s nastavením pro editor kódu:
        codeEditorProp = App.READ_FILE.getCodeEditorPropertiesForInternalFrames();

        /*
         * Povolení vybraných klávesových zkratek pro editor kódu. Jedná se o akce (zkratky), které byly zakázány pro
         * editor příkazů.
         */
        RSyntaxTextAreaKeyActionHelper.enableSpecificAction(true);

        initGui();


        menu = new Menu(this, languageProperties);
        createMenu(true);

        /*
         * Načtení proměnné pro to, zda se mají zalamovat texty v editorech kódu (interních oknech).
         *
         * (Je třeba načíst zde, protože musí existovat instance položek v menu, aby se nastavila označená komponenta
         * pro zalamování textů.)
         */
        loadProjectExplorerProperties(codeEditorProp);


        desktopPane = new JDesktopPane();
        desktopPane.setDesktopManager(new DesktopManager());
        final JScrollPane jspDesktopPane = createDesktopPane();


        // inicializace komponenty coby stromové struktury:
        final TreeStructure myTree = new TreeStructure(fileWorkspaceDir, languageProperties, this, false, null, false);
        final JScrollPane jspMyTree = new JScrollPane(myTree);


        // Komponenta, která se přídá na střed dialogu, a na levo bude stromová struktura a na pravo
        // bude komponenta pro zobrazení InternalFrames coby otevřené soubory ze stromové struktury
        final JSplitPane jspTreeAndFrames = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, jspMyTree,
                jspDesktopPane);
        jspTreeAndFrames.setOneTouchExpandable(true);

        add(jspTreeAndFrames, BorderLayout.CENTER);


        setLanguage(languageProperties);


        // Přidám událost na zavření tohoto dialogu, že se otestují všechny otevřené okna - editory, zda se mají uložit,
        // pokud je to třeba, a pak se zavře dialogu, nebo se zavře dialog bez nějakého ukládání, apod.

        addWindowListener(new ProjectExplorerWindowListener(this, languageProperties, false, null, true));


        // Zkusím otevřít dříve otevřené a nezavřené soubory v tomto dialogu:
        openSavedFiles();


        pack();
        setLocationRelativeTo(null);

        // Kvůli metodě pack musím nastavit posuvníky až zde:
        jspTreeAndFrames.setDividerLocation(0.25d);

        setVisible(true);
    }


    /**
     * Konstruktor této třídy.
     * <p>
     * Tento konstruktor se zavolá, pokud se má otevřít tento dialog s kořenovým adresářem ve Workspace, ale má se
     * otevřít nějaký soubor, který zvolil uživatel.
     *
     * @param fileWorkspaceDir
     *         - reference typu File na adresář coby Workspace
     * @param languageProperties
     *         - soubor s texty ve zvoleném jazyce uživatelem pro aplikaci
     * @param openFile
     *         - reference na soubor, který se má otevřit (mimo jiné)
     * @param fileList
     *         - kolekce obsahující cesty k adresářům, které se mají rozbalit v adresářové struktuře při otevření
     *         dialogu.
     * @param expandAllSubDirs
     *         - true v případě, že se mají rozbalit veškeré podadresáře každého adresáře v listu fileList. Jinak false,
     *         v případě, že se má rozbalit vždy jen každý adresář v listu fileList.
     */
    public ProjectExplorerDialog(final File fileWorkspaceDir, final Properties languageProperties,
                                 final File openFile, final List<File> fileList, final boolean expandAllSubDirs) {
        // tuto proměnnou nebudu inicializovat, není v tomto případě ani potřeba
        outputEditor = null;

        fileProjectDir = null;

        // uložím si cestu k otevřenému adresáři coby Workspace, abych ji mohl zobrazit v titulku dialogu:
        pathToOpenDir = fileWorkspaceDir.getAbsolutePath();


        ProjectExplorerDialog.languageProperties = languageProperties;

        // Načtení konfiguračního souboru s nastavením pro editor kódu:
        codeEditorProp = App.READ_FILE.getCodeEditorPropertiesForInternalFrames();

        /*
         * Povolení vybraných klávesových zkratek pro editor kódu. Jedná se o akce (zkratky), které byly zakázány pro
         * editor příkazů.
         */
        RSyntaxTextAreaKeyActionHelper.enableSpecificAction(true);

        initGui();


        menu = new Menu(this, languageProperties);
        createMenu(true);

        /*
         * Načtení proměnné pro to, zda se mají zalamovat texty v editorech kódu (interních oknech).
         *
         * (Je třeba načíst zde, protože musí existovat instance položek v menu, aby se nastavila označená komponenta
         * pro zalamování textů.)
         */
        loadProjectExplorerProperties(codeEditorProp);


        desktopPane = new JDesktopPane();
        desktopPane.setDesktopManager(new DesktopManager());
        final JScrollPane jspDesktopPane = createDesktopPane();


        // inicializace komponenty coby stromové struktury:
        final TreeStructure myTree = new TreeStructure(fileWorkspaceDir, languageProperties, this, false, fileList,
                expandAllSubDirs);
        final JScrollPane jspMyTree = new JScrollPane(myTree);


        // Komponenta, která se přídá na střed dialogu, a na levo bude stromová struktura a na pravo
        // bude komponenta pro zobrazení InternalFrames coby otevřené soubory ze stromové struktury
        final JSplitPane jspTreeAndFrames = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, jspMyTree,
                jspDesktopPane);
        jspTreeAndFrames.setOneTouchExpandable(true);

        add(jspTreeAndFrames, BorderLayout.CENTER);


        setLanguage(languageProperties);


        // Přidám událost na zavření tohoto dialogu, že se otestují všechny otevřené okna - editory, zda se mají uložit,
        // pokud je to třeba, a pak se zavře dialogu, nebo se zavře dialog bez nějakého ukládání, apod.
        addWindowListener(new ProjectExplorerWindowListener(this, languageProperties, false, null, true));


        // Zkusím otevřít dříve otevřené a nezavřené soubory v tomto dialogu:
        openSavedFiles();


        if (openFile != null) {
            /*
             * Nejprve si zjistím, jaký typ souboru se má otevřit.
             */
            final KindOfFile kindOfFile = getKindOfFile(openFile.getAbsolutePath());

            /*
             * V případě, že je to excel, tak zde použiji velice podobný kód, jako je v
             * metodě createInternalFrame až na nějaké malé úpravy.
             *
             * Pokud se tedy jedná o Excel, tak si zde nechám uživatele, aby označil list,
             * který se má otevřít. A otestuje se, zda ješě náhodou není otevřen, pokud ano,
             * tak by se měl jen označit v metodě isFileAlreadyOpen, jinak se nově otevře.
             *
             * Tato úprava je zde proto, že kdybych to testovat jen třeba metodou
             * isFileAlreadyOpen a pak zavolat metodu createInternalFrame, pak by mohly být
             * menší takové né chyby ale bylo by to nepříjemné, že by buď vyskakovaly
             * oznámení, že je soubor již otevřen, nebo by se dotazovalo třeba dvakrát na
             * list, protože aby se otestovalo, za je již otevřen, tak se musí nejprve
             * zjistit list pro otevření v excelovském souboru, a pokud by nebyl, pak se v
             * metodě createInternalFrame zase dotázalo na list, který se má otevřit.
             */
            if (kindOfFile == KindOfFile.EXCEL) {
                // Nejprve si potřebuji zjistit názvy listů, které příslušný dokument obsahuje:
                final List<String> sheetNamesList = readWriteMicrosoftFile.getSheetNames(openFile);

                // Následující podmínka by měla být zbytečná, exceovský dokument
                // již při vytvoření obsahuje alespoň jeden list s nějakým  názvem, ale alespoň předejdu vyjímka,
                // kvůli špatnému
                // zjišťování indexu listu v excelu: i když by tato nožnost nastat neměla:
                if (!sheetNamesList.isEmpty()) {
                    /*
                     * V této části kódu vím, že příslušný excelovský soubor obsahuje nejméně jeden
                     * list, tak se uživatele dotážu, který list se má má otevřít a do této proměnné
                     * si uložím index toho listu.
                     */
                    final int listIndex = getSelectedExcelSheet(sheetNamesList);

                    // Otestuji, zda uživatel zvlil nějakou záložku nebo dialog
                    // zavřel a zrušil tak otevření okna:
                    if (listIndex >= 0) {
                        // proměnná pro uchování názvu listu z excelu - pokud se
                        // má otevřít excel
                        final String listName = sheetNamesList.get(listIndex);

                        /*
                         * Tato podmínka by mohla být v nějaké metodě, protože se jedná téměř o stejný
                         * kód, jako v metodě: createInternalFrame, ale je to zde takto jen dvakrát, tak
                         * to nechám být, jinak by bylo lepší to dát do metody.
                         */
                        if (!isFileAlreadyOpen(openFile, kindOfFile, listName)) {
                            // zde mám všechny potřehné údaje, tak mohu vytvořit
                            // okno:
                            final FileInternalFrame newInternalFrame = new FileInternalFrame(openFile,
                                    languageProperties,
                                    kindOfFile, listIndex, listName, this, null);

                            desktopPane.add(newInternalFrame);

                            setSelectedMyFrame(newInternalFrame);
                        }
                    }
                }
                // Toto by nastat nemělo:
                else
                    JOptionPane.showMessageDialog(this,
                            txtExcelFileDoNotContainsListText + "\n" + txtFile + ": " + openFile.getAbsolutePath(),
                            txtExcelFileDoNotContainsListTitle, JOptionPane.ERROR_MESSAGE);
            }


            /*
             * Zde se nejedná o soubor typu Excel, tak jej zkusím klasicky otevřít, protože
             * není třeba testovat názvy listů otevřeného excelovského souboru.
             */
            else if (!isFileAlreadyOpen(openFile, kindOfFile, null))
                // zavolám metodu, která příslušné okno vytvoří, resp. otevře příslušný soubor:
                createInternalFrame(openFile, null);



            /*
             * Nyní je vytvořeno jediné okno v komponentě JdesktopPane, tak to pro jistotu
             * otestuji, zda je opravdu jedno okno oevřené a rovnou ho maximalizuji - pokud
             * je to okno opravdu otevřené:
             */
            maximizeWindow();
        }


        pack();
        setLocationRelativeTo(null);

        // Kvůli metodě pack musím nastavit posuvníky až zde:
        jspTreeAndFrames.setDividerLocation(0.25d);

        setVisible(true);
    }


    /**
     * Konstruktor třídy.
     * <p>
     * Konstruktor se zavolá, pokud se má otevřít tento dialog s kořenovým adresářem coby adresář otevřeného projektu v
     * aplikaci.
     *
     * @param fileProjectDir
     *         - proměnná, která ukazuje na adresář otevřeného projektu
     * @param languageProperties
     *         - reference na soubor s texty pro aplikaci ve zvoleném jazyce
     * @param classDiagram
     *         - reference na diagram tříd, abcyh ho mohl předat do vlákna, které po zavření tohoto dialogu otestuje
     *         vztahy mezi třídami v diagramu tříd, protože mohlo dojít ke změne
     */
    public ProjectExplorerDialog(final File fileProjectDir, final Properties languageProperties,
                                 final GraphClass classDiagram) {

        this.fileProjectDir = fileProjectDir;

        // uložím si cestu k otevřenému adresáři otevřeného projektu, abych ji mohl zobrazit v titulku dialogu:
        pathToOpenDir = fileProjectDir.getAbsolutePath();

        ProjectExplorerDialog.languageProperties = languageProperties;

        // Načtení konfiguračního souboru s nastavením pro editor kódu:
        codeEditorProp = App.READ_FILE.getCodeEditorPropertiesForInternalFrames();

        /*
         * Povolení vybraných klávesových zkratek pro editor kódu. Jedná se o akce (zkratky), které byly zakázány pro
         * editor příkazů.
         */
        RSyntaxTextAreaKeyActionHelper.enableSpecificAction(true);

        initGui();


        menu = new Menu(this, languageProperties);
        createMenu(false);

        /*
         * Načtení proměnné pro to, zda se mají zalamovat texty v editorech kódu (interních oknech).
         *
         * (Je třeba načíst zde, protože musí existovat instance položek v menu, aby se nastavila označená komponenta
         * pro zalamování textů.)
         */
        loadProjectExplorerProperties(codeEditorProp);


        desktopPane = new JDesktopPane();
        desktopPane.setDesktopManager(new DesktopManager());
        final JScrollPane jspDesktopPane = createDesktopPane();


        // inicializace komponenty coby stromové struktury:
        final List<File> fileList = new ArrayList<>(Collections.singletonList(new File(App.READ_FILE.getPathToSrc())));
        final TreeStructure myTree = new TreeStructure(fileProjectDir, languageProperties, this, true, fileList, true);
        final JScrollPane jspMyTree = new JScrollPane(myTree);


        outputEditor = new OutputEditorForCompilation(languageProperties);
        final JScrollPane jspOutputEditor = new JScrollPane(outputEditor);


        // Vytvořím novou "posouvací" kompnentu - posuvník, ve které budou umístěno výše vytvořené JDesktopPane pro
        // interní okna
        // a "pod ním" výše vytvořený editoru výstupů, kam se budou zapisovat informace o kompilaci, chyby, úspěch,
        // apod.
        final JSplitPane jspDesktopPaneAndOutputEditor = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true, jspDesktopPane,
                jspOutputEditor);
        // Aby se při změně velikosti okna dialogu neměnila i pozice posuvníku, tak zde
        // nachám tuto výchozí velikost, jinak nastavit až po zavolání metody pack:
        jspDesktopPaneAndOutputEditor.setResizeWeight(0.95d);
        jspDesktopPaneAndOutputEditor.setOneTouchExpandable(true);


        // Komponenta, která se přídá na střed dialogu, a na levo bude stromová struktura a na pravo
        // bude komponenta pro zobrazení InternalFrames coby otevřené soubory ze stromové struktury
        // a pod ní bude editor výstupů
        final JSplitPane jspTreeAndFrames = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, jspMyTree,
                jspDesktopPaneAndOutputEditor);
        jspTreeAndFrames.setOneTouchExpandable(true);

        add(jspTreeAndFrames, BorderLayout.CENTER);


        setLanguage(languageProperties);


        addWindowListener(new ProjectExplorerWindowListener(this, languageProperties, true, classDiagram, false));


        pack();
        setLocationRelativeTo(null);

        // Kvůli metodě pack musím nastavit posuvníky až zde:
        //		jspDesktopPaneAndOutputEditor.setDividerLocation(0.9d);
        jspTreeAndFrames.setDividerLocation(0.25d);

        setVisible(true);
    }


    /**
     * Konstruktor třídy.
     * <p>
     * Tento konstruktor se zavolá po dvojtém kliknutí na objekt v diagramu tříd, který reprezentuje nějakou třídu v
     * projektu.
     * <p>
     * Tento konstruktor otevře tento dialog s kořenovým adresářem coby adresář otevřeného projektu v aplikaci a otevřít
     * zrovna nějaká třída.
     *
     * @param fileProjectDir
     *         - proměnná, která ukazuje na adresář otevřeného projektu
     * @param openClassFile
     *         - proměnná typu File, která ukazuje na soubory, resp. třídu z diagramu tříd, která se má otevřít v tomto
     *         dialogu - průzkumníku projektů
     * @param languageProperties
     *         - reference na soubor s texty pro aplikaci ve zvoleném jazyce
     * @param classDiagram
     *         - reference na diagram tříd, abcyh ho mohl předat do vlákna, které po zavření tohoto dialogu otestuje
     *         vztahy mezi třídami v diagramu tříd, protože mohlo dojít ke změne
     * @param loadedClass
     *         = načtena - prelozena trida, resp. soubor. class, který se preda do prislusneho interního okna, kde se
     *         doplni do nápovedy v editoru zdrojoveho kodu jednotlive metody, promenne, konstruktory, ... z dané třídy
     */
    public ProjectExplorerDialog(final File fileProjectDir, final File openClassFile,
                                 final Properties languageProperties, final GraphClass classDiagram,
                                 final Class<?> loadedClass) {

        this.fileProjectDir = fileProjectDir;

        // uložím si cestu k otevřenému adresáři otevřeného projektu, abych ji mohl zobrazit v titulku dialogu:
        pathToOpenDir = fileProjectDir.getAbsolutePath();

        ProjectExplorerDialog.languageProperties = languageProperties;

        // Načtení konfiguračního souboru s nastavením pro editor kódu:
        codeEditorProp = App.READ_FILE.getCodeEditorPropertiesForInternalFrames();

        /*
         * Povolení vybraných klávesových zkratek pro editor kódu. Jedná se o akce (zkratky), které byly zakázány pro
         * editor příkazů.
         */
        RSyntaxTextAreaKeyActionHelper.enableSpecificAction(true);

        initGui();


        menu = new Menu(this, languageProperties);
        createMenu(false);

        /*
         * Načtení proměnné pro to, zda se mají zalamovat texty v editorech kódu (interních oknech).
         *
         * (Je třeba načíst zde, protože musí existovat instance položek v menu, aby se nastavila označená komponenta
         * pro zalamování textů.)
         */
        loadProjectExplorerProperties(codeEditorProp);


        desktopPane = new JDesktopPane();
        desktopPane.setDesktopManager(new DesktopManager());
        final JScrollPane jspDesktopPane = createDesktopPane();


        // inicializace komponenty coby stromové struktury:
        final List<File> fileList = new ArrayList<>(Collections.singletonList(new File(App.READ_FILE.getPathToSrc())));
        final TreeStructure myTree = new TreeStructure(fileProjectDir, languageProperties, this, true, fileList, true);
        final JScrollPane jspMyTree = new JScrollPane(myTree);


        outputEditor = new OutputEditorForCompilation(languageProperties);
        final JScrollPane jspOutputEditor = new JScrollPane(outputEditor);


        // Vytvořím novou "posouvací" kompnentu - posuvník, ve které budou umístěno výše vytvořené JDesktopPane pro
        // interní okna
        // a "pod ním" výše vytvořený editoru výstupů, kam se budou zapisovat informace o kompilaci, chyby, úspěch,
        // apod.
        final JSplitPane jspDesktopPaneAndOutputEditor = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true, jspDesktopPane,
                jspOutputEditor);
        jspDesktopPaneAndOutputEditor.setOneTouchExpandable(true);


        // Komponenta, která se přídá na střed dialogu, a na levo bude stromová struktura a na pravo
        // bude komponenta pro zobrazení InternalFrames coby otevřené soubory ze
        // stromové struktury
        // a pod ní bude editor výstupů
        final JSplitPane jspTreeAndFrames = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, jspMyTree,
                jspDesktopPaneAndOutputEditor);
        // Aby se při změně velikosti okna dialogu neměnila i pozice posuvníku, tak zde
        // nachám tuto výchozí velikost, jinak nastavit až po zavolání metody pack:
        jspDesktopPaneAndOutputEditor.setResizeWeight(0.95d);
        jspTreeAndFrames.setOneTouchExpandable(true);

        add(jspTreeAndFrames, BorderLayout.CENTER);


        setLanguage(languageProperties);


        addWindowListener(new ProjectExplorerWindowListener(this, languageProperties, true, classDiagram, false));


        // Nyní mám připravené GUI tohoto dialogu - průzkuníku projektů, tak musím ještě otevřít příslušnou třídu,
        // Zde již vím, že daná třída existuje, je v diagramu tříd, ... prostě vše potřebné pro otevření,
        // tak ji rovnou otevřu a jelikož je to jediná třída, která se á otevřit, tak ji rovnou i maximalizuji

        // zavolám metodu, která příslušné okno vytvoří, resp. otevře příslušný soubor:
        createInternalFrame(openClassFile, loadedClass);

        // Nyní je vytvořeno jediné okno v komponentě JdesktopPane, tak to pro jistotu otestuji, zda
        // je opravdu jedno okno oevřené a rovnou ho maximalizuji - pokud je to okno opravdu otevřené:
        maximizeWindow();


        pack();
        setLocationRelativeTo(null);

        // Kvůli metodě pack musím nastavit posuvníky až zde:
        //		jspDesktopPaneAndOutputEditor.setDividerLocation(0.9d);
        jspTreeAndFrames.setDividerLocation(0.25d);

        setVisible(true);
    }


    /**
     * Metoda, která "maximalizuje" okno, ve kterém je otevřen nějaký soubor. Ale puze v případě, že je otevřeno pouze
     * jedno okno, pokud je jich otevřené více, pak se nic nestane, tj. žádné okno nebude maximalizováno.
     */
    private void maximizeWindow() {
        final JInternalFrame[] internalFrameArray = desktopPane.getAllFrames();

        // Otestuji, zda existuje opravdu jen to jedno okno, coby třídu, kterou chtěl uživatel otevřít,
        // a pokud ano, tak se jistí, že je to instance třídy MyInternalFrame (to už byh nemusel, jiná okna
        // by tam býtani neěly) a pokud jsou tyto podmínky splněny, tak mohu příslušné okno aximalizovat
        // akorát je třeba zachytávat vyjímku, kterou příslušná metody vyhazuje
        if (internalFrameArray.length == 1 && internalFrameArray[0] instanceof FileInternalFrame) {
            try {
                internalFrameArray[0].setMaximum(true);
            } catch (final PropertyVetoException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při volání metody 'setMaximum' nad otevřeným interním oknem se " +
                                    "souborem" +
                                    " na cestě: "
                                    + ((FileInternalFrame) internalFrameArray[0]).getFile().getAbsolutePath()
                                    + ". Tato výjimka může nastat v případě, že pokus o nastavení vlastnosti je " +
                                    "vetován JInternalFrame.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
            }
        }
    }


    /**
     * Metoda, která si z příslušné hodnoty v Preferences API získá jednorozměrné pole typu File, které obsahuje uložené
     * soubory, k dříve otevřeným souborů v tomto dialogu v interních okněch, pokud se tedy povede toto načtení
     * zmíněného pole, tak ho celé proiteruji a otestuji, zda příslušný soubor ještě existuje a pokud ano, tak ho opět
     * otevřu, aby ho uživatel nemusel hledat
     * <p>
     * Postup: - načtu si z preferencí příslušné pole se soubory - pokud se načetlo, tak ho proiteruji, a pokaždé
     * otestuji: - zda příslušný soubor ještě existuje pak jeho typ a dle typu buď existenci excelovského listu nebo
     * klasicky otevřu soubor
     * <p>
     * Note: testuje se pouze excelovský soubor, kvůli listu pro otevření, zbytek testovat nemusím, protože byl
     * příslušný soubor již jednou otevřen, jinka by se ani nemohl uložit, takže vím, že otevřít jde.
     */
    private void openSavedFiles() {
        final InfoForSaveFilesToPreferences[] filesArray = (InfoForSaveFilesToPreferences[]) PreferencesApi
                .getPreferencesObject(PreferencesApi.OPEN_FILES_ARRAY);

        if (filesArray == null)
            return;

        for (final InfoForSaveFilesToPreferences i : filesArray) {
            // Nejprve otestuji, zda příslušný soubor ještě vůbec existuje:
            if (!i.getFile().exists() || !i.getFile().isFile())
                continue;


            // tuto podmínku - možnost bych ani nemusel testovat, protože uživatel by neměl stihnout otevřit
            // libovolné okno během volání této metody v konstruktoru a navíc by zde ani neměly být duplicity, protože
            // to už by nešlo okno ani otevřit, ale kdyby náhodou, ... ???
            final KindOfFile kindOfFile = getKindOfFile(i.getFile().getAbsolutePath());

            if (!kindOfFile.equals(KindOfFile.EXCEL)) {
                if (!isFileAlreadyOpen(i.getFile(), kindOfFile, i.getListName()))
                    desktopPane.add(
                            new FileInternalFrame(i.getFile(), languageProperties, kindOfFile, -1, null, this, null));

                continue;
            }

            // Zde se nejedná o excel, tak stačí normálně otevřit:

            // Nyní otestuji, zda příslušný dokument ještě není
            // otevřen - ale toto je opět zbytečná podmínka, protože už byl otevřen jednou před
            // uložením a dvakrát nelze otevřít stejný soubor, takže bych to ani testovat nemusel, ale kdyby
            // náhodou
            if (isFileAlreadyOpen(i.getFile(), kindOfFile, i.getListName()))
                continue;

            // Nejprve si potřebuji zjistit názvy listů, které
            // příslušný dokument obsahuje:
            final List<String> sheetNamesList = readWriteMicrosoftFile.getSheetNames(i.getFile());

            // Otestuji, zda je alespoň nějaký list v excelovském souboru, a pokud ano,
            // tak otestuji, zda je název dříve otevřeného listu ještě pořád vdaném souboru,
            // abych jej mohl znovu otevřít:
            if (!sheetNamesList.isEmpty() && sheetNamesList.contains(i.getListName())) {
                final int listIndex = sheetNamesList.indexOf(i.getListName());

                desktopPane.add(new FileInternalFrame(i.getFile(), languageProperties, kindOfFile,
                        listIndex, i.getListName(), this, null));
            }
        }
    }


    /**
     * Metoda, která přidá "hlavní menu" pro tento dialog do okna tohoto dialogu.
     *
     * @param isOpenWorkspace
     *         - logická proměnná o tom, zda je otevřen Workspace nebo ne, a dle toho se znepřístupní některá tlačítka
     */
    private void createMenu(final boolean isOpenWorkspace) {
        // Znepřístupním některá tlačítka (pokued je otevřen adresář Workspace):
        if (isOpenWorkspace)
            menu.enableMenuItems();

        setJMenuBar(menu);
    }


    /**
     * Metoda, která nastaví komponentě JdesktopPane, do které se vkládají interní okna pozadí a další nastavení pro
     * manipulaci s ní - viz tělo metody.
     * <p>
     * Note: Inicializace této kompnenty je prováděna v konstruktoru.
     *
     * @return JscrollPane - scrolovací komponentu, která obshuje výše zmíněný JdesktopPane pro vkládání do ní intenrích
     * oken coby otevřených souborů
     */
    private JScrollPane createDesktopPane() {
        // inicializace komponenty JdesktopPane coby "okno", do kterého se následně budou vkládat ty interní
        // rámce, což budou jednotlivé editory kodu
        desktopPane.setBackground(new Color(0xE2DDDD));
        // Při posunu okna se bude nejprve zobrazovat rámeček coby cílové umístění,
        // a ažkdyž se pustí myš, ak se tepre přeune okno na cílové umístění:
        desktopPane.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);

        return new JScrollPane(desktopPane);
    }


    /**
     * Metoda, která proiteruje všechny otevření interní okna za účelem uložení příslušných dat - otevřených souborů.
     * <p>
     * Metoda při každé iteraci otestuje, zda se jedná o instanci MyInternalFrames, pokud ano, tak se zjistí, zda ještě
     * existuje příslušný soubor, který je otevřen v daném okkně a pokud ano, tak se do něj zapíší data z příslušného
     * okna z editoru, pokud daný soubor již neexistuje, tak se dotáže uživatele, zda se má vybrat pro příslušný soubor
     * nové umístění
     *
     * @param internalFramesArray
     *         - jednorozměrné pole tyup JinternalFrame, které obsahuje veškeré otevřená okna, resp. veškeré otevřené
     *         soubory uživatelem
     */
    final void saveDataInAllFrames(final JInternalFrame[] internalFramesArray) {
        // Zde se mají uložit změny, tak projedu všechny okna a uložím příslušná data:
        for (final JInternalFrame f : internalFramesArray)
            if (f instanceof FileInternalFrame) {
                final FileInternalFrame myFrame = (FileInternalFrame) f;

                // Nejprve otestuji, zda se nejedná o soubor typu .pdf, protože ten nelze uložit:
                if (!myFrame.getKindOfFile().equals(KindOfFile.PDF)) {
                    // Pro začátek otestuji, zda příslušný soubor ještě existuje:
                    if (myFrame.existFile())
                        // Zde soubor ještě existuje, tak není co řešit a mohu zapsat soubor:
                        myFrame.saveFile();


                        // Zde příslušný soubor již neexistuje, tak se zeptám, zda ho chce uložit na nové
                        // umístění a případně ho uložím na nově zvolené místo:
                    else myFrame.saveFileToNewLocation();
                } else
                    JOptionPane.showMessageDialog(this,
                            txtCannotSavePdfFileText + "\n" + txtLocation + ": " + myFrame.getFile().getAbsolutePath(),
                            txtCannotSavePdfFileTitle, JOptionPane.INFORMATION_MESSAGE);
            }
    }


    /**
     * Metoda, která Prohledá všechna otevřená interní okna, resp. okna coby soubory, které uživatel otevřel, a
     * otestuje, zda se jedná o otevřený kód třídy, která je v adresáři src v otevřeném projektu. Tedy pokud se jedná o
     * otevřený projekt v tomto průzkumníku projektů, jinak se nezavolá,
     * <p>
     * Pokud jsou tedy podmínky splněny, tj. z hlediska kódu to bude akorát testování, zda není v příslušném okně
     * proměnná packageText null a zároveň se v diagrau tříd, nachází reprezentace této třídy, tj. že v diagramu tříd
     * bude existovat buňka, která obsahuje text z výše uvedené proměnné: packageText, pokud ano, tak se otestuje, zda
     * tento soubor na disku ještě existuje, a pokud ano, tak se uloží, protože je třeba jej zkompilovat a pokud už
     * neexistuje, tak se nabidně možnost pro nové umístění této třídy.
     * <p>
     * V podstatě stručně řečeno: prohledají se všedhny otevřené soubory - interní okna, a pokud je otevřen v przkumníku
     * souborů¨ nějaký projekt, tak se bude testovat, zda se jedná o třídu z adresáře src z tohoto projektu a jestli se
     * tato třída s příslušnými baličky ještě nachází v diagramu tříd, pokud ano, ak se otestje, zda tato třída ještě
     * existuje a buď se uloži nebo se nabídne mžnost nového uložení
     */
    final void saveDataInFramesWithClassFromProject() {
        final JInternalFrame[] internalFramesArray = desktopPane.getAllFrames();

        for (final JInternalFrame f : internalFramesArray) {
            if (f instanceof FileInternalFrame) {// toto bych testovat nemusel, jinak by tomu ani být nemělo
                // Přtypuji si okno na "správný typ" pro snažší práci
                final FileInternalFrame myFrame = (FileInternalFrame) f;

                // Otstuji, zda příslušný soubor ještě existuje, jedná se o třídu z otevřeného projektu
                // tj. (packageText != null) a text z této proměnné se nachází v diagramu tříd:
                if (myFrame.existFile()) {
                    if (myFrame.getPackageText() != null && GraphClass.isClassWithTextInClassDiagram(myFrame.getPackageText())) {
                        // Zde jsou výše uvedené podmínky splněny, tak ohu uložit soubor:
                        myFrame.saveFile();
                    }
                }

                // Zde již soubor, který je otevřen v příslušném (právě testovaném) interním okně neexistuje,
                // tak nabídnu možnost pro nové uložení, resp. uložení stejného souboru na nové místo:
                else myFrame.saveFileToNewLocation();
            }
        }
    }


    /**
     * Metoda, která vytvoří nové okno coby interni Frame s nějakým otevřeným souborem.
     * <p>
     * Nejprve ale potřebuji zjistit o jaký soubor se jedná, protože pokud se jedná o soubor typu Excel, tak je potřebna
     * zjistit, jaký list chce uživatel otevřít, tak se otevře dialog, kde se zvolí jeden z listů, pokud se nejedná o
     * soubor typu Excel, tak se v podstatě akorát zjistí jeden z hládaných typů souborů kvůli otevření a otevře se,
     * resp. vytvoří se nové okno a tam se děje zbytek
     *
     * @param selectedFile
     *         - cesta k souboru, který se má otevřít v interním okně v komponentě JdesktopPane
     * @param clazz
     *         - přeložená - zkompilovaná a načtená třída, resp.její soubor .class, tento parametru bude ve vetšíně
     *         případů null, až na jeden, kdy uživatel otevře třídu poocí pravého tlačítka nad třídou v diagramu tříd, v
     *         takovém případě se tento soubor. class již načte v menu, tak ho sem pouze předám, ale jinak se musí
     *         načít, tak bude v takových případech tento parametr null
     */
    final void createInternalFrame(final File selectedFile, final Class<?> clazz) {
        /*
         * Otestuji, zda se nejedná o adrsář a označený soubor na disku ještě opravdu
         * existuje (to by mělo být vždy splněno), a případně ho otevřu v okne:
         */
        if (!ReadFile.existsFile(selectedFile.getAbsolutePath())) {
            JOptionPane.showMessageDialog(this,
                    txtFileDoNotExistText + "\n" + txtFile + ": " + selectedFile.getAbsolutePath(),
                    txtFileDoNotExistTitle, JOptionPane.ERROR_MESSAGE);

            return;
        }


        /*
         * do proměnné kidOfFile si uložím typ soubory, který se pozná dle přípony - ale
         * testuji jen některé vybrané ne doslova všechny:
         */
        final KindOfFile kindOfFile = getKindOfFile(selectedFile.getAbsolutePath());

        // Potřebuji si zjistit, resp. otestovat, zda se jedná o Excel a pokud ano, tak potřebuji zjistit,
        // jaký list se má otevřít, resp. list na jakém indexu, Index lze zjistit tak, že se buď zadá, ale kdo b si
        // pamatoval
        // všechny záložky, tak si zjistím návzy listů, v daném excelu a vypíšu si je, pak dle jejich pořadí
        // (počítáno od
        // nuly) si zjistím index listu, který se má otevřít.:
        if (kindOfFile.equals(KindOfFile.EXCEL)) {
            // Nejprve si potřebuji zjistit názvy listů, které příslušný dokument obsahuje:
            final List<String> sheetNamesList = readWriteMicrosoftFile.getSheetNames(selectedFile);

            // Následující podmínka by měla být zbytečná, exceovský dokument
            // již při vytvoření obsahuje alespoň jeden list s nějakým  názvem, ale alespoň předejdu vyjímka, kvůli
            // špatnému
            // zjišťování indexu listu v excelu: i když by tato nožnost nastat neměla:
            if (!sheetNamesList.isEmpty()) {
                // proměnná pro uchování indexu listu v excelu, který se má
                // otevřit - pokud se jedná o excel:
                // Zkusím si od uživatele získat list pro otevření:
                final int listIndex = getSelectedExcelSheet(sheetNamesList);

                // Otestuji, zda uživatel zvolil nějakou záložku nebo dialog
                // zavřel a zrušil tak otevření okna:
                if (listIndex < 0)
                    return;


                // proměnná pro uchování názvu listu z excelu - pokud se
                // má otevřít excel
                final String listName = sheetNamesList.get(listIndex);

                if (!isFileAlreadyOpen(selectedFile, kindOfFile, listName)) {
                    // zde mám všechny potřehné údaje, tak mohu vytvořit
                    // okno:
                    final FileInternalFrame newInternalFrame = new FileInternalFrame(selectedFile,
                            languageProperties, kindOfFile, listIndex, listName, this, clazz);

                    desktopPane.add(newInternalFrame);

                    setSelectedMyFrame(newInternalFrame);
                } else
                    JOptionPane.showMessageDialog(this,
                            txtFileIsAlreadyOpenText + "\n" + txtLocation + ": "
                                    + selectedFile.getAbsolutePath(),
                            txtFileIsAlreadyOpenTitle, JOptionPane.INFORMATION_MESSAGE);
            }
            // Toto by nastat nemělo:
            else
                JOptionPane.showMessageDialog(this,
                        txtExcelFileDoNotContainsListText + "\n" + txtFile + ": " + selectedFile.getAbsolutePath(),
                        txtExcelFileDoNotContainsListTitle, JOptionPane.ERROR_MESSAGE);
        } else if (kindOfFile.equals(KindOfFile.DO_NOT_OPEN))
            JOptionPane.showMessageDialog(this,
                    txtFileKind + ": '" + FilenameUtils.getExtension(selectedFile.getName()) + "' " + txtDoNotOpen
                            + "\n" + txtLocation + ": " + selectedFile.getAbsolutePath(),
                    txtFileDoNotOpen, JOptionPane.ERROR_MESSAGE);


            // Zde se nejedná o soubor typu excel, tak není co řešit a prostě
            // vytvořím nové okno a vložím některé výchozí hodnoty
        else if (!isFileAlreadyOpen(selectedFile, kindOfFile, null))
            desktopPane
                    .add(new FileInternalFrame(selectedFile, languageProperties, kindOfFile, -1, null, this, clazz));


        else
            JOptionPane.showMessageDialog(this,
                    txtFileIsAlreadyOpenText + "\n" + txtLocation + ": " + selectedFile.getAbsolutePath(),
                    txtFileIsAlreadyOpenTitle, JOptionPane.INFORMATION_MESSAGE);
    }


    /**
     * Metoda, která otevře dialogové okno, ve kterém si uživatel vybere list v příslušném excelovském souboru, který se
     * má otevřít.
     *
     * @param sheetNamesList
     *         - list, který obsahuje názvy listů, které obshuje příslušný excelovský soubor.
     *
     * @return index listu v excelovském souboru, který se má otevřít.
     */
    private static int getSelectedExcelSheet(final List<String> sheetNamesList) {
        // Nyní vím, že obsahuje alespon jeden list - což by mělo
        // být vždy, tak zobrazím dialogu, kde si uživatel vybere list, které chce
        // otevřít a já si z toho zjistím
        // index listu, více nepotřebuji:
        final GetIndexOfExcelSheetForm getIndexForm = new GetIndexOfExcelSheetForm(sheetNamesList);
        getIndexForm.setLanguage(languageProperties);

        /*
         * Vrátím index listu v příslušném excelovském souboru, který chce uživatel
         * otevřít.
         */
        return getIndexForm.getSelectedSheet();
    }


    /**
     * Metoda, která minimalizuje všechna otevřená interní okna coby otevřené soubory v JdesktopPnae. - "Shodí" je a
     * zarovná dolů pro možné znovu zobrazení na původní pozici s původní velikostí příslušného okna
     */
    final void minimizeAllFrames() {
        // Načtu si všechna otevřená interní okna:
        final JInternalFrame[] allFramesArray = desktopPane.getAllFrames();

        // Otestuji, zda je vůbec nějaké interní okno otevřeno, jinak není co minimalizovat:
        if (allFramesArray.length == 0)
            return;


        // zde je alespoň jedno interní okno otevřeno, tak je všechny mohu proiterovat
        // a minimalizovat:
        for (final JInternalFrame f : allFramesArray) {
            if (f != null) {// -> tato podmínka by zde ani nemusela být, nemělo by nastat, že by nějaké okno¨bylo
                // null, ale kdyby náhodou...
                try {
                    // Metoda setIcon, která slouží pro minimalizaci okna:
                    f.setIcon(true);

                } catch (final PropertyVetoException e) {
                    /*
                     * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                     * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                     * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                     * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                     */
                    final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                    // Otestuji, zda se podařilo načíst logger:
                    if (logger != null) {
                        // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                        // Nejprve mou informaci o chybě:
                        logger.log(Level.INFO,
                                "Zachycena výjimka při volání metody 'setIcon' nad interním oknem, tato metoda slouži" +
                                        " pro minimalizaci interního okna v JDesktopPane, "
                                        + "třída ProjectExplorerDialog.jara, metoda: minimizeAllFrames. Tato výjimka " +
                                        "může nastat v případě, že pokus o nastavení vlastnosti je vetován " +
                                        "JInternalFrame.");

                        /*
                         * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                         * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                         * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                         */
                        logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                    }
                    /*
                     * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                     */
                    ExceptionLogger.closeFileHandler();
                }
            }
        }
    }


    /**
     * Metoda slouží pro seřazení všech otevřených do podoby dlaždice, ale pouze pod sebe. Tj. že se všechna okna
     * roztáhnou na šířku komponenty JdesktopPane a přizpůsobí se jejich rozměry, tak aby se všechny okna vešly pod
     * sebe
     * <p>
     * Zdroj - metoda: tileFrames https://jira.spring.io/secure/attachment/10347/ScrollingDesktopPane.java
     */
    final void orderFramesByTileUnderHim() {
        // Načtu si všechna otevřená okna:
        final JInternalFrame[] allFramesArray = desktopPane.getAllFrames();

        // Otestuji, zda jsou vůbec nějaká okna otevřená, jinak mohu skončit:
        if (allFramesArray.length == 0)
            return;


        // v podstatě rozměry komponenty JdesktopPane - její ohraničení
        final Rectangle bounds = desktopPane.getBounds();

        final int frameHeight = bounds.getBounds().height / allFramesArray.length;

        int y = 0;

        for (final JInternalFrame anAllFramesArray : allFramesArray) {
            anAllFramesArray.setSize(bounds.getBounds().width, frameHeight);
            anAllFramesArray.setLocation(0, y);
            y = y + frameHeight;
        }
    }


    /**
     * Metoda, která seřadí všechna otevřená okna doslova do podoby dlaždic, přípomínaí chodník. Metoda seřadí okna vždy
     * tak aby se hodily vedla i pod sebe.
     * <p>
     * Připomínají tabulku.
     */
    final void orderFramesByTile() {
        // test - OK - dlazdice se razenim - OK: - tile - dlaždice - pod i vedle sebe
        // http://www.javalobby.org/java/forums/t15696.html

        // Načtu si všechna otevřená okna:
        final JInternalFrame[] allFramesArray = desktopPane.getAllFrames();


        // Otestuji, zda je nějaké okno otevřené, jinak není co seřazovat:
        if (allFramesArray.length == 0)
            return;

        // v podstatě rozměry komponenty JdesktopPane - její ohraničení
        final Rectangle bounds = desktopPane.getBounds();


        final int cols = (int) Math.sqrt(allFramesArray.length);
        int rows = (int) (Math.ceil(((double) allFramesArray.length) / cols));
        final int lastRow = allFramesArray.length - cols * (rows - 1);

        int width;
        final int height;

        if (lastRow == 0) {
            rows--;
            height = bounds.height / rows;
        } else {
            height = bounds.height / rows;

            if (lastRow < cols) {
                rows--;
                width = bounds.width / lastRow;

                for (int i = 0; i < lastRow; i++)
                    allFramesArray[cols * rows + i].setBounds(i * width, rows * height, width, height);
            }
        }

        width = bounds.width / cols;

        for (int j = 0; j < rows; j++) {
            for (int i = 0; i < cols; i++)
                allFramesArray[i + j * cols].setBounds(i * width, j * height, width, height);
        }
    }


    /**
     * Tato metoda zarovná otevřená okna do stalu Cascade.
     * <p>
     * Zdroj - metoda: cascadeFrames: https://jira.spring.io/secure/attachment/10347/ScrollingDesktopPane.java
     */
    final void orderFramesByCascade() {
        // Načtu si všechna otevřená okna:
        final JInternalFrame[] allFramesArray = desktopPane.getAllFrames();

        // Otestuji, zda je co řadit, tj. zda jsou nějaká okna vůbec otevřená:
        if (allFramesArray.length == 0)
            return;


        // v podstatě rozměry komponenty JdesktopPane - její ohraničení
        final Rectangle bounds = desktopPane.getBounds();

        final int frameHeight = (bounds.getBounds().height - 5) - allFramesArray.length * FRAME_OFFSET;
        final int frameWidth = (bounds.getBounds().width - 5) - allFramesArray.length * FRAME_OFFSET;

        int x = 0;
        int y = 0;

        for (int i = allFramesArray.length - 1; i >= 0; i--) {
            allFramesArray[i].setSize(frameWidth, frameHeight);
            allFramesArray[i].setLocation(x, y);

            x = x + FRAME_OFFSET;
            y = y + FRAME_OFFSET;
        }
    }


    /**
     * Metoda, která zjistí, zda je označený soubor již otevřen v nějakém interním okně nebo ne.
     * <p>
     * Metoda proiteruje všechny otevřené interní okna a zjistí jejich "file", resp. porovná příslušnou cestu otevřeného
     * souboru s tím aktuálním, a pokud se najde shoda cyklus mže skončít a vrátí se true.
     * <p>
     * Jinak pokud se projdou všechny interní okna a nenajde se shoda s otevřeným souborem, tak se na konec po skončení
     * cyklu vrátí false
     * <p>
     * Note: parametry kindOfFile a listName jsou zde navíc pouze proto, že pokud se testuje, zda není náhodou otevřen
     * excelovský dokument, tak je třeba kromě cesty k souboru otestovat dále i list v příslušném excelu, který se má
     * otevřít a pokud už je otevřen, tak ho pouze zvýrazním v příslušném okně v průzkumníku projektů, protže když bych
     * testovat pouze cestu k souboru, tak by šel otevřít pouze jeden jedniný list v příslušném excelovském souboru -
     * vždy jen jeden, protože při dalším pokusu oo otevření by se zjistilo, že je již příslušný soubor otevřen, ale
     * nevedělo by se na jakém listu, tak proto
     *
     * @param file
     *         - proměnná, která ukazuje na označený soubor, který se má otevřít
     * @param kindOfFile
     *         - výčtový typ, který drží informaci o typu souboru
     * @param listName
     *         - název listu v excelovském který se má otevřít, musí se otestovat, zda je již příslušný list jednou
     *         otevřen nebo ne, v případě excelu nestačí pouze esta k souboru, je třeba otestovat i oevřený lsit, tato
     *         proměnná bude ve většíně případnů null, konkrétní hodnoty nabude pouze v případě, že se má otevřít soubor
     *         typu Excel
     *
     * @return true, pokud užje otevřen v nějakém interním okně příslušný soubor, jinak false, pokud daný soubor ještě
     * není otevřen
     */
    private boolean isFileAlreadyOpen(final File file, final KindOfFile kindOfFile, final String listName) {
        final JInternalFrame[] internalFramesArray = desktopPane.getAllFrames();

        for (final JInternalFrame f : internalFramesArray) {
            if (f instanceof FileInternalFrame) {// -> němelo by nastat, že by bylo null, nebo nebylo insntaci
                // MyInternalFraame
                final FileInternalFrame myFrame = (FileInternalFrame) f;

                // Otestuji, zda se shodují cesty k souboru:

                // Nejprve musím otstovat, zda se náhodou nemá otevřít soubor typu excel, pokud ano,
                // je třeba otstovat, u příslušného interního okna, pokud je v něm otevřen excel, tak název
                // otevřeného listu,
                // a jestli se jedná o stejný excelovský dokument
                if (kindOfFile.equals(KindOfFile.EXCEL)) {
                    if (myFrame.getListName() != null && myFrame.getListName().equals(listName) && myFrame.getFile()
                            .getAbsolutePath().equals(file.getAbsolutePath())) {
                        // Zde jsem načel shodu, tedy příslušný soubor je již otevřený, tak pouze označím příslušné
                        // okno a oznámím to uživateli
                        setSelectedMyFrame(myFrame);

                        // mohu vrátit true, soubor je již otevřen:
                        return true;
                    }
                }


                // Zde se nejedná o excel, ale o v podstatě libovolný jiný dokument, tak akorát stačí otestovat cestu
                // k souboru pro tevření, zda již náhodou není jednou otevřen
                else if (myFrame.getFile().getAbsolutePath().equals(file.getAbsolutePath())) {
                    // Zde jsem načel shodu, tedy příslušný soubor je již otevřený, tak pouze označím příslušné
                    // okno a oznámím to uživateli
                    setSelectedMyFrame(myFrame);

                    // mohu vrátit true, soubor je již otevřen:
                    return true;
                }
            }
        }

        return false;
    }


    /**
     * Metoda, která označí interní okno v parametru metody, resp. nastaví ho jako označené, takže se v desktopPane
     * zvýrazní.
     * <p>
     * Je na to tato metoda, abych nemusel při každém volání této metody zachytávat vyjímku.
     *
     * @param myFrame
     *         - instance třídy MyInternalFrame, které se má "zvýraznit".
     */
    private static void setSelectedMyFrame(final FileInternalFrame myFrame) {
        try {
            myFrame.setSelected(true);
        } catch (final PropertyVetoException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při volání metody 'setSelected' za účelem označení příslušného okna, třída" +
                                " ProjectExplorerDialog, metoda "
                                + "setSelectedMyFrame. Tato výjimka může nastat například v případě, že pokus o " +
                                "nastavení vlastnosti je vetován JInternalFrame.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
    }


    /**
     * Metoda, která otestuje, zda je soubor, který se přejmenoval otevřen v nějakém interním okne v desktopPane, pokud
     * ano, tak musím změnit i jeho název a umístění, aby se moh případně zapisovat na nové umístění - pokud ho bude
     * chtít uživatel uložit, apod.
     * <p>
     * Jelikož lze otevřít každé okno poouze jednou, tak stačí najít první soubor, se kterým se najde shoda amohu
     * skončít, pokud projdu celý cyklus, resp. všechny interní okna a nenajde se shoda, pak příslušný soubor, který se
     * přejmenoval není otevřen a není co řešit.
     *
     * @param oldFile
     *         - původní soubor, který se přejmenoval na newFile
     * @param newFile
     *         - nový, resp. přejmenovaný soubor oldFile
     */
    final void checkRenamedFile(final File oldFile, final File newFile) {
        final JInternalFrame[] internalFramesArray = desktopPane.getAllFrames();

        for (final JInternalFrame f : internalFramesArray) {
            if (f instanceof FileInternalFrame) {
                // Nyní si mohu přetypovat příslušné okno pro snažší práci:
                final FileInternalFrame myFrame = (FileInternalFrame) f;

                // otestuji, zda je otevřený soubor shodný s tím přejmenovaným a pokud ano,
                // uložím do příslušného okna novou cestu k souboru, změním titulek okna a označím ho,
                // aby si uživatel povšiml změny a na konec mohu ukončit cyklus, pokud ale
                // se soubory neshodují, tak pokračuji dál
                if (myFrame.getFile().getAbsolutePath().equals(oldFile.getAbsolutePath())) {
                    // nastavím novou cestu k danému otevřenému souboru:
                    myFrame.setFile(newFile);

                    // označím příslušné okno
                    setSelectedMyFrame(myFrame);

                    // a nastavím mu nový popisek:
                    myFrame.setDialogTitle();

                    // a jelikož je možné otevřít jeden soubour pouze jednou, tak mohu skončit cyklus:
                    break;
                }
            }
        }
    }


    /**
     * Metoda, která nastaví základní "design" a nějaké vlastnosti pro tento dialog, jako je třeba ikona, velikost okna,
     * manažera rozvržení komponent, ... viz metoda
     */
    private void initGui() {
        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(1100, 700));
        setIconImage(GetIconInterface.getMyIcon("/icons/projectExplorerIcons/ProjectExplorerDialogIcon.gif", true).getImage());
        setModal(true);
    }


    /**
     * Metoda, která zjistí, zda se jedná o nějkterý z hlídaných souborů, a pokud ano, tak vrátí jeden z příslušných
     * výčtových typů.
     *
     * @return výčtový typ pro příslušný typ souboru
     */
    private static KindOfFile getKindOfFile(final String filePath) {
        if (ProjectExplorerIconInterface.isWordFile(filePath))
            return KindOfFile.WORD;

        else if (ProjectExplorerIconInterface.isExcelFile(filePath))
            return KindOfFile.EXCEL;

        else if (ProjectExplorerIconInterface.isPdfFile(filePath))
            return KindOfFile.PDF;

        else if (ProjectExplorerIconInterface.isDoNotOpenFile(filePath))
            return KindOfFile.DO_NOT_OPEN;


        else return KindOfFile.OTHER;
    }


    /**
     * Klasický getr na indtanci třídy JdesktopPane, abch mohl například v menu získat označená okna, apod.
     *
     * @return referenci na instnaci třídy JdesktopPane, do které se vkládají uživatelem otevřené soubory
     */
    final JDesktopPane getDesktopPane() {
        return desktopPane;
    }


    /**
     * Klasická getr na proměnnou typu File, která drží cestu k adresáři coby otevřený projekt v aplikaci, a nebo null,
     * pkud je otevřen adrsář Workspace.
     *
     * @return výše zmíněnou proměnnou fileProjectDir, která ukazuje na adresář coby otevřený projekt nebo null, pokud
     * je otevřen Workspace a né otevřený projěkt
     */
    final File getFileProjectDir() {
        return fileProjectDir;
    }


    /**
     * Klasický getr na promennou outputEditor - resp. komponenta, která slouží jako editor výstupu pro například
     * informaci o kompilaci tříd, apod.
     *
     * @return reference na instanci třídy OutputEditorForCompilation - výše zmíněnou
     */
    public final OutputEditorForCompilation getOutputEditor() {
        return outputEditor;
    }


    /**
     * Metoda, která vrátí označné interní okno, resp. označený otevřený soubor, nebo null, pokud není žádné označené či
     * otevřeno. Metoda dále otestuje, i když zbytečně, protože do komponenty JdesktopPane ani jiná okna nedávám než
     * MyInternalFrame, takže bych to ani nemusel testovat, ale pro jistotu to otestuji.
     *
     * @return výše zmíněný označené interní okno nebo null
     */
    final FileInternalFrame getSelectedFrame() {
        final JInternalFrame selectedFrame = desktopPane.getSelectedFrame();

        if (selectedFrame instanceof FileInternalFrame)
            return (FileInternalFrame) selectedFrame;

        return null;
    }


    /**
     * Zjištění, zda veškerá interní okna mají nastaveno, že se v nich mají zalamovat texty.
     *
     * @return true, pokud se ve všech interních oknech mají zalamovat texty, jinak false.
     */
    boolean areAllFramesSetToWrapText() {
        final JInternalFrame[] allFrames = desktopPane.getAllFrames();

        for (final JInternalFrame frame : allFrames) {
            if (frame instanceof FileInternalFrame) {
                final FileInternalFrame fileInternalFrame = (FileInternalFrame) frame;

                if (!fileInternalFrame.isLineWrap())
                    return false;
            }
        }

        return true;
    }


    /**
     * Zjištění, zda se mají ve všech otevřených interních oknech (otevřených souborech) zobrazovat bílé znaky.
     *
     * @return - true v případě, že se mají ve všech interních oknech zobrazovat bílé znaky, jinak false.
     */
    boolean areInAllFramesShowWhiteSpace() {
        final JInternalFrame[] allFrames = desktopPane.getAllFrames();

        for (final JInternalFrame frame : allFrames) {
            if (frame instanceof FileInternalFrame) {
                final FileInternalFrame fileInternalFrame = (FileInternalFrame) frame;

                if (!fileInternalFrame.isWhiteSpaceVisibleInEditor())
                    return false;
            }
        }

        return true;
    }


    /**
     * Zjištění, zda mají veškerá interní okna / otevřené soubory nastaveno, že se v nich mají odebírat bílé znaky z
     * prázdných řádků po stisknutí klávesy Enter.
     *
     * @return true v případě, že se mají ve všech interních oknech odebírat bílé znaky po stisknutí klávesy Enter z
     * řádku, kde se nachází kurzor. Jinak false.
     */
    boolean areInAllFramesSetClearWhiteSpaceLine() {
        final JInternalFrame[] allFrames = desktopPane.getAllFrames();

        for (final JInternalFrame frame : allFrames) {
            if (frame instanceof FileInternalFrame) {
                final FileInternalFrame fileInternalFrame = (FileInternalFrame) frame;

                if (!fileInternalFrame.isClearWhitespaceLinesInEditor())
                    return false;
            }
        }

        return true;
    }


    /**
     * Getr na menu v okně dialogu.
     *
     * @return referenci na instanci menu v okně dialogu.
     */
    public Menu getMenu() {
        return menu;
    }


    /**
     * Getr na načtený objekt Properties - načtený soubor .properties obsahující nastavení pro editor kódu (průzkumník
     * projektů).
     *
     * @return referenci na objekt properties obsahující načtený konfigurační soubor s nastavením pro průzkumník
     * projektů.
     */
    Properties getCodeEditorProp() {
        return codeEditorProp;
    }


    /**
     * Načtení proměnných pro označení a nastavení vybraných vlastností pro editory kódu v interních oknech. Například
     * zalamování textu, práce s bílýmy znaky apod. Jedná se o načtení vlastností pro veškerá interní okna / otevřené
     * soubory.
     *
     * @param properties
     *         - objekt Properties, což je načtený konfigurační soubor .properties obsahující nastavení pro dialog
     *         průzkumník projektů. Z tohoto parametru se berou potřebné hodnoty.
     */
    private void loadProjectExplorerProperties(final Properties properties) {
        final boolean wrapTextInAllFrames;
        final boolean showWhiteSpaceInAllFrames;
        final boolean clearWhiteSpaceLineInAllFrames;

        if (properties != null) {
            wrapTextInAllFrames = Boolean.parseBoolean(properties.getProperty("WrapTextInAllFrames",
                    String.valueOf(Constants.CODE_EDITOR_WRAP_TEXT_IN_ALL_FRAMES)));
            showWhiteSpaceInAllFrames = Boolean.parseBoolean(properties.getProperty("ShowWhiteSpaceInAllFrames",
                    String.valueOf(Constants.CODE_EDITOR_SHOW_WHITE_SPACE_IN_ALL_FRAMES)));
            clearWhiteSpaceLineInAllFrames = Boolean.parseBoolean(properties.getProperty(
                    "ClearWhiteSpaceLineInAllFrames",
                    String.valueOf(Constants.CODE_EDITOR_CLEAR_WHITE_SPACE_LINE_IN_ALL_FRAMES)));
        } else {
            wrapTextInAllFrames = Constants.CODE_EDITOR_WRAP_TEXT_IN_ALL_FRAMES;
            showWhiteSpaceInAllFrames = Constants.CODE_EDITOR_SHOW_WHITE_SPACE_IN_ALL_FRAMES;
            clearWhiteSpaceLineInAllFrames = Constants.CODE_EDITOR_CLEAR_WHITE_SPACE_LINE_IN_ALL_FRAMES;
        }


        menu.setSelectedRbWrapTextInAllFrames(wrapTextInAllFrames);
        menu.setSelectedRbShowWhiteSpaceInAllFrames(showWhiteSpaceInAllFrames);
        menu.setSelectedRbClearWhiteSpaceLineInAllFrames(clearWhiteSpaceLineInAllFrames);
    }


    /**
     * Uložení vybraných vlastností pro editory kódu v interních oknech. Jedná se o vlastnosti jako jsou například zda
     * se mají zalamovat texty, zobrazovat bílé znaky apod.
     * <p>
     * Princip je, že se z workspace zkusít načíst konfigurační soubor pro nastavení vybraných vlastností pro dialog
     * průzkumník projektů. Pokud se jej nepodaří načíst, skončí se. Zde se neřeší doplňování souborů apod. Pokud se
     * soubor podaří načíst, zapíše se do něj vlastnost, která značí, zda se mají zalamovat texty v interních oknech.
     */
    void storeProjectExplorerProperties() {
        final EditProperties codeEditorProperties = App.READ_FILE
                .getPropertiesInWorkspacePersistComments(Constants.CODE_EDITOR_INTERNAL_FRAMES_NAME);

        if (codeEditorProperties == null)
            return;

        codeEditorProperties.setProperty("WrapTextInAllFrames", String.valueOf(menu.isRbWrapTextInAllFramesSelected()));
        codeEditorProperties.setProperty("ShowWhiteSpaceInAllFrames",
                String.valueOf(menu.isRbShowWhiteSpaceInAllFrameSelected()));
        codeEditorProperties.setProperty("ClearWhiteSpaceLineInAllFrames",
                String.valueOf(menu.isRbClearWhiteSpaceLineInAllFramesSelected()));

        codeEditorProperties.save();
    }


    @Override
    public void setLanguage(final Properties properties) {
        if (properties != null) {
            setTitle(properties.getProperty("Pe_Ped_DialogTitle", Constants.PE_PED_DIALOG_TITLE));

            txtCannotSavePdfFileText = properties.getProperty("Pe_Ped_Txt_CannotSavePdfFileText",
                    Constants.PE_PED_TXT_CANNOT_SAVE_PDF_FILE_TEXT);
            txtLocation = properties.getProperty("Pe_Ped_Txt_Location", Constants.PE_PED_TXT_LOCATION);
            txtCannotSavePdfFileTitle = properties.getProperty("Pe_Ped_Txt_CannotSavePdfFileTitle", Constants.PE_PED_TXT_CANNOT_SAVE_PDF_FILE_TITLE);
            txtFileIsAlreadyOpenText = properties.getProperty("Pe_Ped_Txt_FileIsAlreadyOpenText", Constants.PE_PED_TXT_FILE_IS_ALREADY_OPEN_TEXT);
            txtFileIsAlreadyOpenTitle = properties.getProperty("Pe_Ped_Txt_FileIsAlreadyOpenTitle", Constants.PE_PED_TXT_FILE_IS_ALREADY_OPEN_TITLE);
            txtExcelFileDoNotContainsListText = properties.getProperty("Pe_Ped_Txt_ExcelDontContainsListText", Constants.PE_PED_TXT_EXCEL_DONT_CONTAINS_LIST_TEXT);
            txtFile = properties.getProperty("Pe_Ped_Txt_File", Constants.PE_PED_TXT_FILE);
            txtExcelFileDoNotContainsListTitle = properties.getProperty("Pe_Ped_Txt_ExcelFileDontContainsListTitle", Constants.PE_PED_TXT_EXCEL_FILE_DONT_CONTAINS_LIST_TITLE);
            txtFileKind = properties.getProperty("Pe_Ped_Txt_FileKind", Constants.PE_PED_TXT_FILE_KIND);
            txtDoNotOpen = properties.getProperty("Pe_Ped_Txt_DoNotOpen", Constants.PE_PED_TXT_DO_NOT_OPEN);
            txtFileDoNotOpen = properties.getProperty("Pe_Ped_Txt_FileDoNotOpen", Constants.PE_PED_TXT_FILE_DO_NOT_OPEN);
            txtFileDoNotExistText = properties.getProperty("Pe_Ped_Txt_FileDoNotExistText", Constants.PE_PED_TXT_FILE_DO_NOT_EXIST_TEXT);
            txtFileDoNotExistTitle = properties.getProperty("Pe_Ped_Txt_FileDoNotExistTitle", Constants.PE_PED_TXT_FILE_DO_NOT_EXIST_TITLE);
        } else {
            setTitle(Constants.PE_PED_DIALOG_TITLE);

            txtCannotSavePdfFileText = Constants.PE_PED_TXT_CANNOT_SAVE_PDF_FILE_TEXT;
            txtLocation = Constants.PE_PED_TXT_LOCATION;
            txtCannotSavePdfFileTitle = Constants.PE_PED_TXT_CANNOT_SAVE_PDF_FILE_TITLE;
            txtFileIsAlreadyOpenText = Constants.PE_PED_TXT_FILE_IS_ALREADY_OPEN_TEXT;
            txtFileIsAlreadyOpenTitle = Constants.PE_PED_TXT_FILE_IS_ALREADY_OPEN_TITLE;
            txtExcelFileDoNotContainsListText = Constants.PE_PED_TXT_EXCEL_DONT_CONTAINS_LIST_TEXT;
            txtFile = Constants.PE_PED_TXT_FILE;
            txtExcelFileDoNotContainsListTitle = Constants.PE_PED_TXT_EXCEL_FILE_DONT_CONTAINS_LIST_TITLE;
            txtFileKind = Constants.PE_PED_TXT_FILE_KIND;
            txtDoNotOpen = Constants.PE_PED_TXT_DO_NOT_OPEN;
            txtFileDoNotOpen = Constants.PE_PED_TXT_FILE_DO_NOT_OPEN;
            txtFileDoNotExistText = Constants.PE_PED_TXT_FILE_DO_NOT_EXIST_TEXT;
            txtFileDoNotExistTitle = Constants.PE_PED_TXT_FILE_DO_NOT_EXIST_TITLE;
        }

        // nastavím ještě cestu k adresáři, který je otevřen, buď to může být adresář Workspace
        // a adresář coby otevřená projekt:
        setTitle(getTitle() + " - " + pathToOpenDir);
    }
}