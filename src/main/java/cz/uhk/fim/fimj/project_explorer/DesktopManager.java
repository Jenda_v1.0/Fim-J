package cz.uhk.fim.fimj.project_explorer;

import java.awt.Dimension;

import javax.swing.DefaultDesktopManager;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;

/**
 * Zdroj:
 * <p>
 * http://www.java2s.com/Code/Java/Swing-JFC/InterestingthingsusingJInternalFramesJDesktopPaneandDesktopManager2.htm
 * <p>
 * Tato třídaq slouží pro to, aby uživatel nemohl "vysunout" interní okna z komponenty JdesktopPane, protože zde není
 * JscrollPane, tak jsem využil tuto třídu, kterou jsem si našel na výše uvedeném zdroji, a slouží tedy pro to, že
 * interní okna coby otevřené soubory nešli vysunout jakoby "za dialog" - do stran
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class DesktopManager extends DefaultDesktopManager {

	private static final long serialVersionUID = 1L;
	

	// This is called anytime a frame is moved. This
	// implementation keeps the frame from leaving the desktop.
    @Override
	public final void dragFrame(final JComponent f, int x, int y) {
		
		if (f instanceof JInternalFrame) { // Deal only w/internal frames
			
			final JInternalFrame frame = (JInternalFrame) f;
			final JDesktopPane desk = frame.getDesktopPane();
			final Dimension d = desk.getSize();

			// Nothing all that fancy below, just figuring out how to adjust
			// to keep the frame on the desktop.
			if (x < 0) // too far left?
				x = 0; // flush against the left side

			
			else if (x + frame.getWidth() > d.width) // too far right?
				x = d.width - frame.getWidth(); // flush against right side

			
			if (y < 0) // too high?
				y = 0; // flush against the top

			
			else if (y + frame.getHeight() > d.height) // too low?
				y = d.height - frame.getHeight(); // flush against the
			// bottom
		}

		// Pass along the (possibly cropped) values to the normal drag handler.
		super.dragFrame(f, x, y);
	}
}