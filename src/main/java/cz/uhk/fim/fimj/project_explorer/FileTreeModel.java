package cz.uhk.fim.fimj.project_explorer;

import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.event.TreeModelListener;
import javax.swing.event.EventListenerList;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.io.File;
import java.io.Serializable;

/**
 * Example of a simple static TreeModel. It contains a (java.io.File) directory structure. (C) 2001 Christian Kaufhold
 * (ch-kaufhold@gmx.de)
 * <p>
 * <p>
 * Tuto třídu jsem stáhnul na následujícím odkaze:
 * <p>
 * http://www.chka.de/swing/tree/FileTreeModel.html
 * <p>
 * a trochu ji upravil a smazal hlavní - spouštěcí metodu main, tu zde nepotřebuji
 * <p>
 * <p>
 * <p>
 * Tuto třída jsem našel někde na insternetu - na odkaze výše, a slouží pro to, abych nemusel napsat vlastní algoritmus,
 * který by prohledal zadaný adresář coby Workspace a načet do stromu všechny soubory v něm, tak jsem si ulehčil práci a
 * našel tuto třídu, a o zbytek se již "postarám sám" ohledně ikno, událostí atd.
 * <p>
 * Tato třída vrací do Jtree již připravené hodnoty typu File - to je každá položka zobrazená ve strmové struktuře,
 * takže lze jednoznačně určit o jaký soubor jde.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class FileTreeModel implements TreeModel, Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	private EventListenerList listeners;

	private static final Object LEAF = new Serializable() {
		private static final long serialVersionUID = 1L;
	};

	private Map<File, Object> map;

	private final File root;

	public FileTreeModel(final File root) {
		super();
		
		this.root = root;

		if (!root.isDirectory())
			map.put(root, LEAF);

		this.listeners = new EventListenerList();

		this.map = new HashMap<>();
	}

	public final Object getRoot() {
		return root;
	}

	public final boolean isLeaf(final Object node) {
		return map.get(node) == LEAF;
	}

	public final int getChildCount(final Object node) {
		final List<Object> children = children(node);

		if (children == null)
			return 0;

		return children.size();
	}

	public final Object getChild(final Object parent, final int index) {
		return children(parent).get(index);
	}

	public final int getIndexOfChild(final Object parent, final Object child) {
		return children(parent).indexOf(child);
	}

	private List<Object> children(final Object node) {
		final File f = (File) node;

		final Object value = map.get(f);

		if (value == LEAF)
			return null;

		@SuppressWarnings("unchecked")
		List<Object> children = (List<Object>) value;

		if (children == null) {
			final File[] c = f.listFiles();

			if (c != null) {
				children = new ArrayList<>(c.length);

				for (int len = c.length, i = 0; i < len; i++) {
					children.add(c[i]);
					if (!c[i].isDirectory())
						map.put(c[i], LEAF);
				}
			}
			else children = new ArrayList<>(0);

			map.put(f, children);
		}

		return children;
	}

	public final void valueForPathChanged(final TreePath path, final Object value) {
	}

	public final void addTreeModelListener(final TreeModelListener l) {
		listeners.add(TreeModelListener.class, l);
	}

	public final void removeTreeModelListener(final TreeModelListener l) {
		listeners.remove(TreeModelListener.class, l);
	}

	public final Object clone() {
		try {
			final FileTreeModel clone = (FileTreeModel) super.clone();

			clone.listeners = new EventListenerList();

			clone.map = new HashMap<>(map);

			return clone;
		} catch (CloneNotSupportedException e) {
			throw new InternalError();
		}
	}
}