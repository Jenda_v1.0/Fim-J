package cz.uhk.fim.fimj.project_explorer;

import java.io.File;
import java.util.List;

/**
 * Toto rozhraní slouží pro deklaraci metod, které slouží pro čtení a nebo zápiis do souiborů různých typů, Microsoft
 * Excel, Word, nebo PDF, atd.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface ReadWriteFileInterface {

    /**
     * Metoda, která zapíše kód třídy (text z editoru) do příslušného souboru.
     *
     * @param pathToFile
     *         - cesta k třídě - souboru
     * @param textList
     *         - kód třídy / text z editoru
     *
     * @return true, pokud zápis dopadne dobře, jinak false
     */
    boolean setTextToFile(final String pathToFile, final List<String> textList);


    /**
     * Metoda, která zapíše kód třídy (text z editoru) do příslušného souboru.
     *
     * <i>Zápis probíhá tak, že se iterují položky v listu textList a každá položka se zapíše do souboru, ale za ní se
     * nepřídá nový řádek.</i>
     *
     * @param pathToFile
     *         - cesta k třídě - souboru
     * @param textList
     *         - kód třídy / text z editoru
     *
     * @return true, pokud zápis dopadne dobře, jinak false
     */
    boolean setTextToFileWithoutNewLine(final String pathToFile, final List<String> textList);


    /**
     * Metoda, která z zadaného souboru / třídy přečte její text - kód.
     *
     * @param pathToFile
     *         - cesta k souboru / třídě, jejíž koód / text se má přečíst do kolekce
     *
     * @return kolekci zdrojového kódu, resp. text z daného souboru / třídy, každy text v kolekci = 1 řádek třídy
     */
    List<String> getTextOfFile(final String pathToFile);


    /**
     * Metoda, která přečte Wordovský dokument a vrátí jeho text v podobne List - kolekce, jedna hodnota v kolekci =
     * jeden řádek ve Wordu.
     * <p>
     * V rámci použitých komponent se jedná pouze o čtení textu z daného dokumentu bez nějakých obrázků, fontů, apod .
     * to vše již záleží na použitém editoru, který slouží hlavně pro editoraci zdrojového kodu ne pro obrázky, apod.
     * takže nebudou zobrazeny.
     *
     * @param file
     *         - proměnná, která ukazuje na soubor, který se má přečíst.
     *
     * @return vrátí List - kolekci typu String, který obsahuje všechny texty z příslušného dokumentu
     */
    List<String> readWordFile(final File file);


    /**
     * Metoda, která slouží pro zápis textu do Wordovského dokumentu. Metoda daný soubor přepíše, takže bude obsahovat
     * pouze text z editoru kodu - v aplikaci, bez nějakých obrázků, formátování, apod pouze samotný text, který se
     * vezme z editoru v aplikaci.
     *
     * @param file
     *         - proměnná, která ukazuje na soubor, do kterého se má zapsat data z editoru
     * @param text
     *         - proměnná typu String, která se má vložít do Wordowského dokumentu
     *
     * @return logickou hodnotu o tom, zda se podaří zapsat text do příslušného dokumentu, pokud nenastane žádná chyba
     * vrátí se true, jinak false
     */
    boolean writeToWordFile(final File file, final String text);


    /**
     * Metoda, která přečte excelovský dokument, a transformuje získaná data do proměnné typu List - kolekce Stringů,
     * opět jedna položka v této kolekci = jeden řádek v excelu se všemi sloupci
     * <p>
     * Každý řádek obsahuje získaná data oddělená středníkem. Středník odděluje jednotlivé sloupce v dokumentu
     * <p>
     * Metoda přečtě List na zadaném indexu v dokumentu (List > 0 && LIst < SheetCount) - mensi nez počet listu v
     * excelovském dokumentu
     *
     * @param file
     *         - proměnná, která ukazjje na excelovský dokument, ze kterého se mají přešíst data
     * @param listIndex
     *         - index listu, který se má otevřít
     *
     * @return List - kolekci Stringu, která obsahuje transformovaná data získaná z dokumentu, jedna položka v listu =
     * jeden řádek v excelu a středník oddeluje jednotlivé sloupce
     */
    List<String> readExcelFile(final File file, final int listIndex);


    /**
     * Metoda, která zapíše data z editoru v aplikaci do excelovského dokumentu, na stejnou pozici - list, ze kterého se
     * četli.
     * <p>
     * Získaná data z editoru - jeden řádek v editoru = jeden řádek v dokumentu a středník odděluje jednotlivé sloupce
     * na každém řádku.
     * <p>
     * Metoda umí pouze přepsat zmíněný list v dokumentu, takže se v pohodě přečtě, ale pokud se do něj májí data
     * zapsat, tak se přepíše pouze ten list, ze kterého se data přečetla a nejspíše nastane chyba, pokud tam daný list
     * už nebude, resp. list, na zadaném indexu
     *
     * @param file
     *         - proměnná, která ukazuje na excelovský dokument, do kterého se mají data zapsat
     * @param data
     *         - dvojrozměrné pole typu Object, které obsahuje získaná a upravená resp. transformovaná data z editoru
     *         kodu v aplikaci do podoby, aby se daly zapsat zpět do excelovského souboru
     * @param listIndex
     *         - index listu, který se má přepsat, resp. list, který byl načte, takže se do něj zapíší i získaná a
     *         upravený data z editoru v aplikaci
     * @param listName
     *         - Stringová proměnná, ve které se bude nacházet název otevřeného listu v excelovském souboru, je zde
     *         potřeba, protože pokud ho uživatel otevře, a pak před zavřenémm v této aplikaci soubor na lokálním
     *         umístění smaže, a bude ten co je otevřený v aplikaci uložit, tak bych musel vymyslet nějaký nový název,
     *         místo toho si alu bude držet ten původní název - tento
     *
     * @return logickou hodnotu o tom, zda se zápis pvedl nebo ne, true, pokud se zápis dat zpět do souboru povede,
     * jinak false
     */
    boolean writeToExcelFile(final File file, final Object[][] data, final int listIndex, final String listName);


    /**
     * Metoda, která vytovří v excelovském dokumentu (file) nový excelovský list - novou záložku v excelovském souboru s
     * názvem listName. Tento nově vytvořený list listName bude prázdný.
     * <p>
     * ("Metoda pro vytvoření nového listu s názvem listName v excelovském souboru")
     *
     * @param file
     *         - Excelovský dokument, kde se má vytvořit nový list s názvem listName.
     * @param listName
     *         - názvev list, který se má vytvořit v excelovském souboru.
     *
     * @return true, pokud se do excelovského souboru úspěšně přidá nový prázdný list s názvem listName, jinak false.
     */
    boolean createNewListInExcelFile(final File file, final String listName);


    /**
     * Metoda, která zjistí Názvy záložek, které příslušný dokument typu Excel obsahuje.
     *
     * @param file
     *         - proměnná, která ukazuje na Excelovský dokument, ze kterého potřebuji zjistit názvy záložek, které
     *         obsahuje
     *
     * @return List - kolekce typu String, který bude osabovat návzy záložek v Excelovském dokumentu, při nejhorším bude
     * prázdný, pokud žádnou záložku obsahovat nebode příslušný dokument
     */
    List<String> getSheetNames(final File file);


    /**
     * Metoda, která přečte text z souboru typu .pdf a transformuje získaný text do kolekce typu String, ve které je co
     * položka to řádek v příslušném souboru .pdf.
     * <p>
     * Jelikož použitý editor umí zobrazit pouze text, tak se z příslušného souboru vezme, resp. přešte pouze text a to
     * je vše.
     *
     * @param file
     *         - proměnná, která ukazuje na soubor, který se má otevřít, v tomto případě se jedná o soubor typu .pdf,
     *         který se má otevřít
     *
     * @return kolekci - list typu String, který obshauje získané texty z souboru typu .pdf
     */
    List<String> readPdfFile(final File file);
}