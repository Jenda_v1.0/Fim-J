package cz.uhk.fim.fimj.project_explorer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.border.BevelBorder;
import javax.swing.tree.TreePath;

import cz.uhk.fim.fimj.file.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.forms.NewProjectForm;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.preferences.PreferencesApi;

/**
 * Tato třída slouží jako menu, které se zobrazí po kliknutí pravým tlačítkem myši nějaký objekt ve stromové struktuře,
 * resp. v MyTree.java třídě, kde jsou načteny všechny.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class JtreePopupMenu extends JPopupMenu implements LanguageInterface, ActionListener {

	private static final long serialVersionUID = 1L;

	/**
	 * Akce, která slouží pro rozbalení / otevření nebo schování / zabalení nějakého
	 * adresáře ve stromové struktuře.
	 */
	private final transient Action actCollapseExpandLeaf;
	
	/**
	 * Akce, která slouží pro rozbalení / zabalení celého označeného adresáře. Na
	 * rozdíl od položky 'actCollapseExpandLeaf', která rozbalí / zabalí pouze ten
	 * jeden označený adresář - tu jednu úroveň. Tak položka
	 * 'actCollapseExpandWholeLeaf' rozbalí / zabalí úplně celý adresář i s jeho sub
	 * adresáři a sub soubory.
	 */
	private final transient Action actCollapseExpandWholeLeaf;

	
	
	/**
	 * V podstatě cesta k položce v Jtree na kterou uživatel klikl. Tato proměnná je
	 * zde kvůli tomu, abych mohl zjistit, zda je položka rozbalenáí nebo ne, v
	 * případě, že je to možné, tj. jedná se o označený adresář.
	 */
	private TreePath selectedPath;


    /**
     * Rozevírací menu, které obsahuje položky pro otevření souboru v interném okně v dialogu průzkumník projektů nebo v
     * průzkumníku souborů v OS nebo otevření souboru ve výchozí aplikaci dle používané platformy apod.
     */
    private final JMenu menuOpen;
    /**
     * Otevře zadaný soubor v okně pro internal Frames - pokud ještě není
     */
    private final JMenuItem itemOpenInInternalFrame;
    /**
     * Otevře označený soubor nebo aresář ve výchozím průzkumníku souborů na používané platformě.
     */
    private final JMenuItem itemOpenInFileExplorerInOs;
    /**
     * Otevře označený soubor ve výchozí aplikaci na používané platoformě pro otevření konkrétního typu souboru
     */
    private final JMenuItem itemOpenInDefaultAppInOs;


    /**
     * Přenačte stromovou strukturu souborů
     */
    private final JMenuItem itemRefresh;
    /**
     * Vytvoří novou složku v označeném adresáři
     */
    private final JMenuItem itemNewFolder;
    /**
     * Zobrazí dialog pro zadání informací pro nový projekt a případně ho vytvoří a přenačte strmovou strukturu
     */
    private final JMenuItem itemNewProject;
    /**
     * Zobrazi dialog pro nový list do souboru typu Excel - je zpristupnena pouze v pripade, ze je oznacen excelovsky
     * dokument
     */
    private final JMenuItem itemNewExcelList;
    /**
     * Smaže označený soubor nebo adresář - v případě adresáře smaže i celý jeho obsah
     */
    private final JMenuItem itemDelete;
    /**
     * Přejmenuje adresář nebo označenou položku - soubor
     */
    private final JMenuItem itemRename;
    /**
     * Vloží do označeného adresáře položku dříve vloženou do "schránky" - v mém případě do pomocné proměnné
     */
    private final JMenuItem itemPaste;
    /**
     * Vloží cestu k označeném objektu do pomocné proměnné a informaci o tom, že se má někam zkopírovat
     */
    private final JMenuItem itemCopy;
    /**
     * Vloží cestu k objektu dopomocné proměnné a informaci o tom, že se má nějakm přeusnout
     */
    private final JMenuItem itemCut;
    /**
     * Položka pro vytvoření nového souboru z předpřipravenéo dialogu
     */
    private final JMenuItem itemNewFile;



	/**
	 * položka, resp. rozevírací - rozvětvené menu, do kterého se vloží položky pro
	 * nový projekt, třídu a složku
	 */
	private final JMenu menuNew;

	

	/**
	 * Reference pro to, abych mohl zavolat některé metody, třeba pro vlákno, aby
	 * přenačetlo strukturu souborů a pro proměnnou pro kopírování a přesouvání, ...
	 */
	private final TreeStructure myTree;

	
	
	/*
	 * Reference na uživatelem označený soubor ve stromové strukturře, // se kterým
	 * se bude dále precovat
	 */
	private final File selectedFile;
	
	
	/**
	 * referencena proměnnou typu Properteis, která obsahuje veškeré texty aplikace
	 * v uživatelem zvoleném jazyce, je zde potřeban abych ho předal do formulářů
	 */
	private Properties languageProperties;

	
	
	/**
	 * reference na instanci třídy ProjectExplorerDialog, což je ten hlavní dialog a
	 * je v něm umístěna komponenta JdesktopPane, a metoda, která vytvoří vnitřní
	 * ramy do tohoto JdesktopPane
	 */
	private final ProjectExplorerDialog projectExplorerDialog;
	
	
	
	
	
	// Proměnné pro texty do chybových hlášek a výstupů pro uživatele:
	private String txtFileAlreadyExistInDirText, txtFileAlreadyExistInDirTitle, txtCantCreateFileText,
			txtLocation, txtCantCreateFileTitle, txtDirAlreadyExistText, txtDirAlreadyExistTitle,
			txtErrorWhileCreatingDirText, txtErrorWhileCreatingDirTitle, txtCantDeleteFilesFromOpenProjectText,
			txtCantDeleteFilesFromOpenProjectTitle, txtFileDontExistAlreadyText, txtFileDontExistAlreadyTitle,
			txtFailedToRenameFileText, txtOriginalName, txtNewName, txtFailedToRenameFileTitle, txtCannotRenameFileText,
			txtCannotRenameFileTitle, txtFileWithNameAlreadyExistText, txtTarget, txtFileWithNameAlreadyExistTitle,
			txtErrorWhileMovingFileText, txtSource, txtErrorWhileMovingFileTitle, txtFileAlreadyExistInLocationText,
			txtFileAlreadyExistInLocationTitle, txtErrorWhileMovingFileToLocationText,
			txtErrorWhileMovingFileToLocationTitle, txtTargetPlaceContainFileNameText,
			txtTargetPlaceContainFileNameTitle, txtErrorWihleCopyingDirText, txtErrorWihleCopyingDirTitle,
			txtErrorWhileCopyingSelectedFileText, txtErrorWhileCopyingSelectedFileTitle, txtTargetLocationDontExistText,
			txtTargetLocationDontExistTitle, txtSelectedFileDoesntExistText, txtSelectedFileDoesntExistTitle,
			txtCannotMoveFileFromOpenProjectText, txtCannotMoveFileFromOpenProjectTitle, txtErrorWhileDeletingDirText,
			txtErrorWhileDeletingDirTitle, txtErrorWhileCreatingNewListInExcelFileText_1,
            txtErrorWhileCreatingNewListInExcelFileText_2, txtErrorWhileCreatingNewListInExcelFileText_3,
            txtErrorWhileCreatingNewListInExcelFileTitle;

    /**
     * Text pro úvodního komentáře do nové třídy jazyka Java - v případě, že ji uživatel vytvoří.
     */
    private String txtCommentText;
    /**
     * Komentáře pro konstruktor nově vytvořené třídy.
     */
    private String txtConstructorComment;





    /**
     * Tento konstruktor slouží nejen k vytvoření instance této třídy, ale také pro naplnění referencí na následující
     * třídy, jsou zde potřebba. selectedFile je reference na soubor, na který se kliklo v menu, a myTree je zde potřeba
     * kvůli přístupu k proměnné, ve které bude uloženy informace pro přesun a kopírování souboru případně adresáře.
     *
     * @param selectedFile
     *         - označený soubor, resp. položka ve stromové struktuře, na kterou uživatel klikl pravým tlačítkem myši
     * @param myTree
     *         - reference na Jtree - stromovou strukturu souborů
     * @param projectExplorer
     *         - reference na hlavní dialog, kde je umístěna metoda, která umožní vytvářet vnitřní rámce do kterého se
     *         vloží označený soubor ve stromové struktuře
     */
    public JtreePopupMenu(final File selectedFile, final TreeStructure myTree,
                          final ProjectExplorerDialog projectExplorer) {
		/*
		 * Nastaveí "zvýšeného" vzhledu tohoto JpopupMenu - pouze vzhledová záležitost.
		 */
		setBorder(new BevelBorder(BevelBorder.RAISED));
		
		
		this.myTree = myTree;
		this.selectedFile = selectedFile;
		projectExplorerDialog = projectExplorer;
		
		
		
		
		
		actCollapseExpandLeaf = new AbstractAction() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				if (selectedPath == null)
					return;

				if (myTree.isExpanded(selectedPath))
					myTree.collapsePath(selectedPath);

				else
					myTree.expandPath(selectedPath);
			}
		};
		
		add(actCollapseExpandLeaf);
		addSeparator();
		
		
		
		
		
		actCollapseExpandWholeLeaf = new AbstractAction() {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				if (selectedPath == null)
					return;

				if (myTree.isExpanded(selectedPath))
					myTree.expandCollapseSelectedDir(false, selectedPath);

				else
					myTree.expandCollapseSelectedDir(true, selectedPath);
			}
		};
		
		
		add(actCollapseExpandWholeLeaf);
		addSeparator();
		
		
		
		
		
		
		
		
		
		// Inicializace tlačítek:
		menuNew = new JMenu();
        menuOpen = new JMenu();
		
		itemOpenInInternalFrame = new JMenuItem();
        itemOpenInFileExplorerInOs = new JMenuItem();
        itemOpenInDefaultAppInOs = new JMenuItem();

		itemRefresh = new JMenuItem();
		itemNewFile = new JMenuItem();
		itemNewFolder = new JMenuItem();
		itemNewProject = new JMenuItem();
		itemNewExcelList = new JMenuItem();
		itemDelete = new JMenuItem();
		itemRename = new JMenuItem();
		itemPaste = new JMenuItem();
		itemCopy = new JMenuItem();
		itemCut = new JMenuItem();
		
		// Přidání tlačítek do menu pro Nový:
		menuNew.add(itemNewFile);
		menuNew.addSeparator();
		menuNew.add(itemNewFolder);
		menuNew.addSeparator();
		menuNew.add(itemNewProject);
		menuNew.addSeparator();
		menuNew.add(itemNewExcelList);

		// Přidání položek do menu pro otevření:
        menuOpen.add(itemOpenInInternalFrame);
        menuOpen.addSeparator();
        menuOpen.add(itemOpenInFileExplorerInOs);
        menuOpen.addSeparator();
        menuOpen.add(itemOpenInDefaultAppInOs);


		
		
		
		
		
		
		// Přidání událostí na kliknutí na tlačítka:
		itemOpenInInternalFrame.addActionListener(this);
        itemOpenInFileExplorerInOs.addActionListener(this);
        itemOpenInDefaultAppInOs.addActionListener(this);

		itemRefresh.addActionListener(this);
		itemNewFile.addActionListener(this);
		itemNewFolder.addActionListener(this);
		itemNewProject.addActionListener(this);
		itemNewExcelList.addActionListener(this);
		itemDelete.addActionListener(this);
		itemRename.addActionListener(this);
		itemPaste.addActionListener(this);
		itemCopy.addActionListener(this);
		itemCut.addActionListener(this);
		
		
		
		// Přidání vytvořených tlačíek do menu, resp. do tohoto panelu:
		add(menuOpen);
		addSeparator();
		add(itemRefresh);
		addSeparator();
		add(menuNew);
		addSeparator();
		add(itemCut);
		add(itemCopy);
		add(itemPaste);
		addSeparator();
		add(itemRename);
		addSeparator();
		add(itemDelete);






        // Otestuji, zda je označený objekt adresář, pokud ano, tak zakážu položku otevřit, protože
        // adresář nejde otevíř v editoru kodu:
        if (selectedFile.isDirectory()) {
            itemOpenInInternalFrame.setEnabled(false);
            itemNewExcelList.setEnabled(false);
            itemOpenInDefaultAppInOs.setEnabled(false);
        }


        // Zde se nejedná o adresář, tak ani nepůjde vytvořit třída, ani
        // adresář, protože do souboru jej nelze vložit
        else {
            itemNewFile.setEnabled(false);
            itemNewFolder.setEnabled(false);
            itemNewProject.setEnabled(false);

            /*
             * Pouze v případě, že je označen excelovský dokument zpřístipním položku ,
             * která umožňuje vytvoření nového prázdného listu se zadaným názvem v označeném
             * excelovském souboru.
             */
            if (!selectedFile.getName().endsWith(".xlsx") || selectedFile.getName().endsWith(".xlsm"))
                itemNewExcelList.setEnabled(false);
        }


        // Dále otestuji, zda uživatel již něco dříve označil pro přesun nebo zkopírování, pokud ano,
        // tak zpřístupním položku vložit, pokud nic takového uživatel neudělal, tak nění co kopírovat nebo přesouvat
        // tak ani nepůjde kliknout na tlačítko vložit:
        if (TreeStructure.getInfoForMoveOrCopyFile() == null || !selectedFile.isDirectory())
            itemPaste.setEnabled(false);
    }

	
	









	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JMenuItem) {
            if (e.getSource() == itemOpenInInternalFrame)
                projectExplorerDialog.createInternalFrame(selectedFile, null);

            else if (e.getSource() == itemOpenInFileExplorerInOs)
                // Otevře se soubor nebo adresář v průzkumníku projektů v používané platformě:
                DesktopSupport.openFileExplorer(selectedFile, true);

            else if (e.getSource() == itemOpenInDefaultAppInOs)
                // Otevře se soubor ve výchozí aplikaci pro otevření příslušného typu souboru na používané platformě:
                DesktopSupport.openFileExplorer(selectedFile, false);

			
			
			
			// Přenačtení stromové struktury se soubory ve Workspace:
			else if (e.getSource() == itemRefresh)
				// zde by bylomožné otevřít například adresář src ve stromové struktuře:
				myTree.refreshTree(null, false);
			
			
			
			// Vytvoření nového souboru:
			else if (e.getSource() == itemNewFile) {
				final NewFileForm newFileForm = new NewFileForm();
				newFileForm.setLanguage(languageProperties);
				
				final String newFileName = newFileForm.getNewFileInfo();
				
				// Otestuji, zda se vrátil název nebo null - pokud se dialog zavřen - bez potvrzení jména
				if (newFileName != null) {
					// Zde se kliklo na OK nebo Enter - potvrdil se nový název souboru:
					
					// Zde už vím, že je označen adresář, jinak by na toto tlačítko nešlo v menu kliknout, tak
					// stačí připojit název souboru a vytvořít příslušný soubor
					final File newFile = new File(selectedFile.getAbsolutePath() + File.separator + newFileName);
					
					if (newFile.exists()) {
						JOptionPane.showMessageDialog(this, txtFileAlreadyExistInDirText, txtFileAlreadyExistInDirTitle,
								JOptionPane.ERROR_MESSAGE);
						return;
					}
					
						
					try {						
						/*
						 * Proměnná, do které uložím logickou hodnotu o tom, zda se podařilo soubor vytvořit nebo ne,
						 * pokud se soubor podařilo vytvořit, tak se do této proměnné vloží hodnota true, v opačném případě
						 * hodnota false
						 */
						final boolean result;
						
						// Pro přehlednost, zde testuji vytvoření akorát souboru typu Word tj. .docx a soubor typu Excel tj. .xlsx,
						// ostatní soubory, které lze pomocí dialogu NewFileForm vytvořit - pomocí tohoto tlačítka itemNewFile
						// lez vytvořit pomocí metody níže - createNewFile nad proměnnou typu File a ty které jsou v nabídce v dialogu
						// jsou odzkoušené a fungují, ale je to opět doplnět, tak to nebudu se soubory přehánět a nechám pouze nějaké
						// - pouze ty textové, ve kterých by se mohl nacháze kod či nějaké poznámky k němu, apod.
						
						// Otestuji, zda se jedná o soubor typu Word, v takovém případě je třeba jej zapsat jinou metodou:
						if (newFileName.endsWith(".docx"))
							result = new ReadWriteFile().writeToWordFile(newFile, "");
						
						
						// Otestuji, zda to není požadavek na vytvoření souboru typu Excel:
						else if (newFileName.endsWith(".xlsx") || newFileName.endsWith(".xlsm")) {
							// zde je třeba ještě zadat název listu:
							
							// Vytvořím si instanci dialogu pro zadání názvu listu:
							final NewExcelListNameForm excelListForm = new NewExcelListNameForm();
							// nastavím mu jazyk:
							excelListForm.setLanguage(languageProperties);
							
							// Zavolám nad ním metodu, která zobrazí dialog a získá od uživatele název listu,
							// v požadované syntaxi
							final String excelListName = excelListForm.getExcelListName();
							
							// Otestuji, zda uživatel potvrdil název listu, nebo dialog zavřel a zrušil tak vytvořní souboru
							if (excelListName != null)
								result = new ReadWriteFile().writeToExcelFile(newFile, new Object[][] {}, -1, excelListName);
							
							// Pokud uživatel zavřel dialog například kliknutím na křížek, tak ukončím u vytváření,
							// noveého excelovského souboru, protože nezadal název, ješě je tu následující zakomentovaná
							// možnost, že by se použil nějaký výchozí název listu:
							else return;
						}
						
						
						// Zde to není ani soubor typu Word ani Excel, tak ho vytvořím pomocí metody nad proměnou typu File:
						else result = newFile.createNewFile();
						
						
						if (result) {
                            // přenačtu soubory, aby se zohrazil i ten právě vytvořený soubor:

                            // Zde by bylo možné otevřít například adresář src ve stromové struktuře:
                            myTree.refreshTree(null, false);

                            // Otestuji, zda se jedná o soubor typu .java (Javovskou třídu) a pokud ano,
                            // tak do ní zapíšu alespoň základní "kostru" Javovské třídy, resp. hlavičku:
                            if (newFileName.endsWith(".java")) {
                                final String className = FilenameUtils.removeExtension(newFile.getName());

                                // Kolekce pro doplnění kódu třídy:
                                final List<String> baseCodeOfClassList = new ArrayList<>();
                                // Balíček, ve kterém by se měla třída nacházet (záleží na nalezení adresáře src):
                                final String txtPackage = "package " + getPackages(newFile) + ";\n";
                                baseCodeOfClassList.add(txtPackage);
                                baseCodeOfClassList.add("\n");

                                // Přidání úvodního komentáře:
                                baseCodeOfClassList.add(WriteToFile.getAuthorComment(txtCommentText));

                                // Kód třídy:
                                baseCodeOfClassList.add(
                                        "public class " + className + " {\n\n"
                                                + "\t/**\n"
                                                + "\t * " + txtConstructorComment + "\n"
                                                + "\t */\n"
                                                + "\tpublic " + className + "() {\n\n"
                                                + "\t}\n"
                                                + "}");
                                new ReadWriteFile().setTextToFileWithoutNewLine(newFile.getAbsolutePath(),
                                        baseCodeOfClassList);
                            }
                        }

						
						// Na následující podmínku by nejspiše odjít protože když už se nepodaří vytvořit soubor, tak vyskočí nejspíše
						// vyjímka																		
						else
							JOptionPane.showMessageDialog(this,
									txtCantCreateFileText + ": " + newFileName + "!\n" + txtLocation + ": "
											+ newFile.getAbsolutePath(),
									txtCantCreateFileTitle, JOptionPane.ERROR_MESSAGE);
					} catch (IOException e1) {
						/*
						 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
						 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
						 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
						 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
						 */
						final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

						// Otestuji, zda se podařilo načíst logger:
						if (logger != null) {
							// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

							// Nejprve mou informaci o chybě:
							logger.log(Level.INFO, "Zachycena výjimka při vytváření nového souboru, umístění: "
									+ newFile.getAbsolutePath()
									+ ", třída v aplikaci: JtreePopupMenu, metoda - po kliknutí na tlačítko nové -> " +
									"soubor a vyplnění dialogu.");

							/*
							 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
							 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
							 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
							 */
							logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
						}
						/*
						 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
						 */
						ExceptionLogger.closeFileHandler();
						
						JOptionPane
								.showMessageDialog(this,
										txtCantCreateFileText + ": " + newFileName + "!\n" + txtLocation + ": "
												+ newFile.getAbsolutePath(),
										txtCantCreateFileTitle, JOptionPane.ERROR_MESSAGE);
					}
				}
			}
			
			
			
			
			else if (e.getSource() == itemNewFolder) {
				final NewFolderForm nff = new NewFolderForm();
				nff.setLanguage(languageProperties);
				
				final String createFolder = nff.getFoldersForCreate();
				
				if (createFolder != null) {
					// Otestuji, zda označený adresář ještě existuje na disku, případně vypíšu hlášku,
					// poukd existuje, tak vytvořím novou cestu ze získaných dat od uživtele a
					// vytvořím příslušné adresáře:
					
					if (new File(selectedFile.getAbsolutePath() + File.separator + createFolder).exists()) {
						JOptionPane.showMessageDialog(this, txtDirAlreadyExistText, txtDirAlreadyExistTitle,
								JOptionPane.ERROR_MESSAGE);
						return;
					}
					
					if (selectedFile.exists()) {
						try {
							Files.createDirectories(Paths.get(selectedFile.getAbsolutePath(),
									createFolder.replace(".", File.separator)));
							
							// Přenačtu stromovou strukturu:

                            // Zde by bylomožné otevřít například adresář src ve stromové struktuře:
							myTree.refreshTree(null, false);

                        } catch (IOException e1) {
                            /*
                             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                             */
                            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                            // Otestuji, zda se podařilo načíst logger:
                            if (logger != null) {
                                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                                // Nejprve mou informaci o chybě:
                                logger.log(Level.INFO,
                                        "Zachycena výjimka při vytváření adresářů, třída JtreePopupMenu, po kliknutí " +
                                                "na tlačítko pro vytvoření nového adresáře, zdroj: "
                                                + selectedFile.getAbsolutePath() + ", adresáře: "
                                                + createFolder.replace(".", File.separator));

                                /*
                                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                                 */
                                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
                            }
                            /*
                             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                             */
                            ExceptionLogger.closeFileHandler();
							
							JOptionPane.showMessageDialog(this,
									txtErrorWhileCreatingDirText + "\n" + txtLocation + ": "
											+ selectedFile.getAbsolutePath() + File.separator
											+ createFolder.replace(".", File.separator),
									txtErrorWhileCreatingDirTitle, JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}
			
			
			
			
			
			else if (e.getSource() == itemNewProject) {
				// Narozdíl od tlačítka v menu, zde mě zajímá pouze vytvoření projektu v rámci souborů na 
				// disku zbytek ohledně diagram, ... to je již jiná část, zde se pouze vytvoří soubory na disku
				// coby nový projekt a přenačte se strom, více nic
				final NewProjectForm projectForm = new NewProjectForm();
				projectForm.setLanguage(languageProperties);
				
				final String pathToProjectFolder = projectForm.getPathToProject();
				
				if (pathToProjectFolder != null)
					App.WRITE_TO_FILE.createNewProjectFolder(pathToProjectFolder);
				
				
				// Přenačtu stromovou strukturu:

                // Zde by bylomožné otevřít například adresář src ve stromové struktuře:
				myTree.refreshTree(null, false);
			}
			
			
			
			
			
			
			
			else if (e.getSource() == itemNewExcelList) {
				// Vytvořím si instanci dialogu pro zadání názvu listu:
				final NewExcelListNameForm excelListForm = new NewExcelListNameForm();
				// nastavím mu jazyk:
				excelListForm.setLanguage(languageProperties);
				
				// Zavolám nad ním metodu, která zobrazí dialog a získá od uživatele název listu,
				// v požadované syntaxi
				final String excelListName = excelListForm.getExcelListName();
				
				// Otestuji, zda uživatel potvrdil název listu, nebo dialog zavřel a zrušil tak vytvořní souboru
                if (excelListName != null && !new ReadWriteFile().createNewListInExcelFile(selectedFile, excelListName))
                    JOptionPane.showMessageDialog(this, txtErrorWhileCreatingNewListInExcelFileText_1 + ": "
                                    + excelListName + ". " + txtErrorWhileCreatingNewListInExcelFileText_2 + "\n"
                                    + txtErrorWhileCreatingNewListInExcelFileText_3 + ": " + selectedFile.getAbsolutePath(),
                            txtErrorWhileCreatingNewListInExcelFileTitle, JOptionPane.ERROR_MESSAGE);
			}
			
			
			
			
			
			
			
			
			
			
			else if (e.getSource() == itemDelete) {
				// Pro jistotu ještě otestuji, zda oznčený soubor či adresář (položka) ještě na disku - cílovém umístění existuje,
				// případně to oznámím uživateli:
				if (selectedFile.exists()) {
					
					if (!isFileFromOpenProject())
						deleteFile();
					
					else
						JOptionPane.showMessageDialog(this, txtCantDeleteFilesFromOpenProjectText,
								txtCantDeleteFilesFromOpenProjectTitle, JOptionPane.ERROR_MESSAGE);					
				}
				
				else
					JOptionPane.showMessageDialog(this,
							txtFileDontExistAlreadyText + "\n" + txtLocation + ": " + selectedFile.getAbsolutePath(),
							txtFileDontExistAlreadyTitle, JOptionPane.ERROR_MESSAGE);
			}
			
			
			
			
			else if (e.getSource() == itemRename) {
				// Pro přejmenování si vezmu označený soubor nebo adresář, pokud je to adresář,
				// získám si i všechny soubory uvnitř adresáře a vložím je do kolekce, pak si zjistím i ředka
				// označené položky a opět otestuji, zda je to adresář a vložím tam dané názvy souborů, aby uživatel nepřejmenoval
				// položku na něco co již v daném umístění existuje.
				
				// Note: 
				// Pokud se přejmenuje nějaký adresář coby balíček v adresáři src nebo třída, tak se pouze přejmenuje soubor,
				// ale ne její kód, apod. pouze názvy, zbytek musí uživatel udělat sám ručně.
				
				// Otstuji, zda se nejedná o položku na přejmenování z aktuálně otevřeného projektu,
				// pokud ne, lze ji přejmenovat:
				if (!isFileFromOpenProject()) {
					final List<String> fileNamesList = new ArrayList<>();
					
					final File parentFile = selectedFile.getParentFile();
					
					// Přidám do kolekce názvů název předka - tak se nesmí jmenovat nový název položky
					if (parentFile != null) {
						fileNamesList.add(parentFile.getName());
						
						// parentFile je rodič označené položky, tak si zkusím načíst i její hodnoty, což budou vlastně
						// hodnoty ve stejném adresáři, tak je přidám do kolekce:
						final String[] array = parentFile.list();

						if (array != null)
							fileNamesList.addAll(Arrays.asList(array));
					}
						
					
					// Zkusím do kolekce přidat i názvy položek v adresář - pokud je to adresář:
					if (selectedFile.list() != null) {
						final String[] array = selectedFile.list();

						if (array != null)
							// Přidám do kolekce s názvy položek ještě ty co se nachází ve stejném adresáři
							fileNamesList.addAll(Arrays.asList(array));
					}
					
					
					
					// Nyní mohu zobrazit dialog pro zadání nového názvu souboru:
					final GetNewFileNameForm fileNameForm = new GetNewFileNameForm(fileNamesList, selectedFile.getName());
					fileNameForm.setLanguage(languageProperties);
					
					final String newName = fileNameForm.getNewFileName();

                    if (newName != null && parentFile != null) {// -> nemělo by nastat
                        final File newFileName = new File(parentFile.getAbsolutePath() + File.separator + newName);

                        final boolean success = selectedFile.renameTo(newFileName);

                        // Otestuji, zda se podařilo přejmenovanání souboru:
                        if (success) {
                            // Přenačtu stromovou strukturu:

                            // Zde by bylomožné otevřít například adresář src ve stromové struktuře:
                            myTree.refreshTree(null, false);

                            // zavolám metodu, která otestuji, zda náhodou není příslušný soubor otevřen v okně,
                            // pokud ano,
                            // tak musím změnit jeho název v okně a cestu k novému umístění, resp. k novému názvu
                            // souboru:
                            projectExplorerDialog.checkRenamedFile(selectedFile, newFileName);
                        }

                        else
                            JOptionPane.showMessageDialog(this,
                                    txtFailedToRenameFileText + "\n" + txtOriginalName + ": "
                                            + selectedFile.getAbsolutePath() + "\n" + txtNewName + ": "
                                            + newFileName.getAbsolutePath(),
                                    txtFailedToRenameFileTitle, JOptionPane.ERROR_MESSAGE);
                    }
				}
				else
					JOptionPane.showMessageDialog(this, txtCannotRenameFileText, txtCannotRenameFileTitle,
							JOptionPane.ERROR_MESSAGE);			
			}
			
			
			
			
			
			else if (e.getSource() == itemPaste) {
				final InfoForMoveOrCopyFile info = TreeStructure.getInfoForMoveOrCopyFile();
				
				// Otestuji, zda uživatel vytvořil nějaký soubor pro přesun nebo zkopírování:
				if (info != null) {
					// zde se uživatel rozhodl, že chce něco zkopírovat nebo přesunout:
					// tak si zjistím co to je a přesunu to na právě zvolené místo:
					
					// Pro začátek ale ještě otestuji, zda ten dríve označený soubor ještě existuje,
					// a zda ten cílový je adresář:
					final File fileSrc = info.getFile();
					
					if (fileSrc.exists()) {
						// Zde dříve označený soubor ještě existuje, tak ho mohu případně zkopírovat, pokud to samé platí i o 
						// cílovém adresáři a je to adresář!
						
						if (selectedFile.exists()) {
							// Cílové umístění:
							final File fileDes = new File(selectedFile + File.separator + fileSrc.getName());
							
							if (info.isCut()) {
								// Potřebuji otestovat, zda se jedná o soubor nebo adresář, kvůli metodě, kterou 
								// mám použít, pokud je to adresář, potřebuji přesunout i obsah adresáře o souboru se jedná
								// jen o ten soubor:
																
								if (fileSrc.isDirectory()) {
									try {
										// Otestji, zda cílové umístění exituje a není to jedno a to samé:
										if (!fileDes.exists() && !fileSrc.getAbsolutePath().equals(selectedFile.getAbsolutePath())) {
											
											FileUtils.moveDirectoryToDirectory(fileSrc, selectedFile, true);
											
											// po přesunutí nastavím proměnnou na null, protože se již jednou přesunula
											TreeStructure.setInfoForMoveOrCopyFile(null);
											
											// Přenačtu stromovou strukturu:
                                            // Zde by bylomožné otevřít například adresář src ve stromové struktuře:
											myTree.refreshTree(null, false);
										}

										else
											JOptionPane.showMessageDialog(this,
													txtFileWithNameAlreadyExistText + "\n" + txtTarget + ": "
															+ fileDes.getAbsolutePath(),
													txtFileWithNameAlreadyExistTitle, JOptionPane.ERROR_MESSAGE);
										
									} catch (IOException e1) {
                                        /*
                                         * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                                         * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                                         * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                                         * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                                         */
                                        final Logger logger = ExceptionLogger.getLogger(ExtractEnum
                                                .EXTRACT_EXCEPTION);

                                        // Otestuji, zda se podařilo načíst logger:
                                        if (logger != null) {
                                            // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                                            // Nejprve mou informaci o chybě:
                                            logger.log(Level.INFO,
                                                    "Zachycena výjimka pří přesouvání adresáře, třída JtreePopupMenu," +
                                                            " po kliknutí na tlačítko vložit v menu pro Jtree po " +
                                                            "kliknutí pravým tlačítkem, zdroj: "
                                                            + fileSrc.getAbsolutePath() + ", cíl: "
                                                            + selectedFile.getAbsolutePath());

                                            /*
                                             * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                                             * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v
                                             * jaké metodě apod. Proto vypisuji celou "informaci" o výjimce.
                                             */
                                            logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
                                        }
                                        /*
                                         * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                                         */
                                        ExceptionLogger.closeFileHandler();
										
										JOptionPane.showMessageDialog(this,
												txtErrorWhileMovingFileText + "\n" + txtSource + ": "
														+ fileSrc.getAbsolutePath() + "\n" + txtTarget + ": "
														+ selectedFile.getAbsolutePath(),
												txtErrorWhileMovingFileTitle, JOptionPane.ERROR_MESSAGE);
									}
								}
								
								else {
									// Zde se jedná o přesun souboru:
									try {
										// Otestuji, zda cílové umístění ještě daný soubor neobsahuje, jinak vypíšu hlášku:
										if (!fileDes.exists() && !fileSrc.getAbsolutePath().equals(selectedFile.getAbsolutePath())) {
											FileUtils.moveFileToDirectory(fileSrc, selectedFile, true);
											
											// po přesunutí nastavím proměnnou na null, protože se již jednou přesunula
											TreeStructure.setInfoForMoveOrCopyFile(null);
											
											// Přenačtu stromovou strukturu:
                                            // Zde by bylomožné otevřít například adresář src ve stromové struktuře:
											myTree.refreshTree(null, false);
										}
										
										else
											JOptionPane.showMessageDialog(this,
													txtFileAlreadyExistInLocationText + "\n" + txtTarget + ": "
															+ fileDes.getAbsolutePath(),
													txtFileAlreadyExistInLocationTitle, JOptionPane.ERROR_MESSAGE);

									} catch (IOException e1) {
										/*
                                         * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                                         * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                                         * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                                         * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                                         */
                                        final Logger logger = ExceptionLogger.getLogger(ExtractEnum
                                                .EXTRACT_EXCEPTION);

                                        // Otestuji, zda se podařilo načíst logger:
                                        if (logger != null) {
                                            // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                                            // Nejprve mou informaci o chybě:
                                            logger.log(Level.INFO,
                                                    "Zachycena výjimka pří přesouvání souboru, třída JtreePopupMenu, " +
                                                            "po kliknutí na tlařítko vložit v menu pro Jtree po " +
                                                            "kliknutí pravým tlačítkem, zdroj: "
                                                            + fileSrc.getAbsolutePath() + ", cíl: "
                                                            + selectedFile.getAbsolutePath());

                                            /*
                                             * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                                             * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v
                                             * jaké metodě apod. Proto vypisuji celou "informaci" o výjimce.
                                             */
                                            logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
                                        }
                                        /*
                                         * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                                         */
                                        ExceptionLogger.closeFileHandler();
										
										JOptionPane.showMessageDialog(this,
												txtErrorWhileMovingFileToLocationText + "\n" + txtSource + ": "
														+ fileSrc.getAbsolutePath() + "\n" + txtTarget + ": "
														+ selectedFile.getAbsolutePath(),
												txtErrorWhileMovingFileToLocationTitle, JOptionPane.ERROR_MESSAGE);
									}
								}
							}
							
							
							else {
								// Zde se jedná o zkopírování:
								// Opět musím otestovat, zda se jedná o adresář nebo soubor
								
								if (fileSrc.isDirectory()) {									
									try {						
										if (!fileSrc.getAbsolutePath().equals(fileDes.getAbsolutePath())) {
											FileUtils.copyDirectory(fileSrc, fileDes, true);
											
											// po přesunutí nastavím proměnnou na null, protože se již jednou přesunula
											TreeStructure.setInfoForMoveOrCopyFile(null);
											
											// Přenačtu stromovou strukturu:
                                            // Zde by bylomožné otevřít například adresář src ve stromové struktuře:
											myTree.refreshTree(null, false);
										}

										else
											JOptionPane.showMessageDialog(this,
													txtTargetPlaceContainFileNameText + "\n" + txtTarget + ": "
															+ fileDes.getAbsolutePath(),
													txtTargetPlaceContainFileNameTitle, JOptionPane.ERROR_MESSAGE);
									
									} catch (IOException e1) {
                                        /*
                                         * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                                         * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                                         * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                                         * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                                         */
                                        final Logger logger = ExceptionLogger.getLogger(ExtractEnum
                                                .EXTRACT_EXCEPTION);

                                        // Otestuji, zda se podařilo načíst logger:
                                        if (logger != null) {
                                            // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                                            // Nejprve mou informaci o chybě:
                                            logger.log(Level.INFO,
                                                    "Zachycena výjimka při kopírování adresáře, třída JtreePopupMenu " +
                                                            "- po kliknutí na tlačítko zkopírovat, zdroj: "
                                                            + fileSrc.getAbsolutePath() + ", cíl: "
                                                            + fileDes.getAbsolutePath());

                                            /*
                                             * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                                             * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v
                                             * jaké metodě apod. Proto vypisuji celou "informaci" o výjimce.
                                             */
                                            logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
                                        }
                                        /*
                                         * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                                         */
                                        ExceptionLogger.closeFileHandler();
										
										JOptionPane.showMessageDialog(this,
												txtErrorWihleCopyingDirText + "\n" + txtSource + ": "
														+ fileSrc.getAbsolutePath() + "\n" + txtTarget + ": "
														+ fileDes.getAbsolutePath(),
												txtErrorWihleCopyingDirTitle, JOptionPane.ERROR_MESSAGE);
									}
								}
								
								
								else {
									try {
										if (!fileSrc.getAbsolutePath().equals(fileDes.getAbsolutePath())) {
											FileUtils.copyFile(fileSrc, fileDes);
											
											// po přesunutí nastavím proměnnou na null, protože se již jednou přesunula
											TreeStructure.setInfoForMoveOrCopyFile(null);
											
											// Přenačtu stromovou strukturu:
                                            // Zde by bylomožné otevřít například adresář src ve stromové struktuře:
											myTree.refreshTree(null, false);
										}
										else
											JOptionPane.showMessageDialog(this,
													txtFileAlreadyExistInLocationText + "\n" + txtTarget + ": "
															+ fileDes.getAbsolutePath(),
													txtFileWithNameAlreadyExistTitle, JOptionPane.ERROR_MESSAGE);
									
									} catch (IOException e1) {
                                        /*
                                         * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                                         * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                                         * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                                         * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                                         */
                                        final Logger logger = ExceptionLogger.getLogger(ExtractEnum
                                                .EXTRACT_EXCEPTION);

                                        // Otestuji, zda se podařilo načíst logger:
                                        if (logger != null) {
                                            // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                                            // Nejprve mou informaci o chybě:
                                            logger.log(Level.INFO,
                                                    "Zachycena výjimka při kopírování souboru, třída JtreePopupMenu -" +
                                                            " po kliknutí na tlačítko zkopírovat, zdroj: "
                                                            + fileSrc.getAbsolutePath() + ", cíl: "
                                                            + fileDes.getAbsolutePath());

                                            /*
                                             * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                                             * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v
                                             * jaké metodě apod. Proto vypisuji celou "informaci" o výjimce.
                                             */
                                            logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
                                        }
                                        /*
                                         * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                                         */
                                        ExceptionLogger.closeFileHandler();
										
										JOptionPane.showMessageDialog(this,
												txtErrorWhileCopyingSelectedFileText + "\n" + txtSource + ": "
														+ fileSrc.getAbsolutePath() + "\n" + txtTarget + ": "
														+ fileDes.getAbsolutePath(),
												txtErrorWhileCopyingSelectedFileTitle, JOptionPane.ERROR_MESSAGE);
									}
								}
							}
						}
						
						else
							JOptionPane.showMessageDialog(this,
									txtTargetLocationDontExistText + "\n" + selectedFile.getAbsolutePath(),
									txtTargetLocationDontExistTitle, JOptionPane.ERROR_MESSAGE);
					}
					
					else
						JOptionPane.showMessageDialog(this,
								txtSelectedFileDoesntExistText + "\n" + fileSrc.getAbsolutePath(),
								txtSelectedFileDoesntExistTitle, JOptionPane.ERROR_MESSAGE);

				}
			}
			
			
			
			
			else if (e.getSource() == itemCopy)
				// Uložím si informaci o tom, že se má nějaký soubor zkopírovat:
				TreeStructure.setInfoForMoveOrCopyFile(new InfoForMoveOrCopyFile(selectedFile, false));
			
			
			
			
			else if (e.getSource() == itemCut) {
				if (!isFileFromOpenProject())				
					// Uložím si informaci o tom, že uživatel chce nějaký soubor přesunout:
					TreeStructure.setInfoForMoveOrCopyFile(new InfoForMoveOrCopyFile(selectedFile, true));
				
				else
					JOptionPane.showMessageDialog(this, txtCannotMoveFileFromOpenProjectText,
							txtCannotMoveFileFromOpenProjectTitle, JOptionPane.ERROR_MESSAGE);
			}
		}
	}


    /**
     * Získání buď výchozího balíčku ("defaultPackage") nebo balíčků od adresáře "src" na cestě file.
     *
     * <i>Zjistí se, zda na cestě existuje adresář "src", pokud ano, pak se vrátí balíčky (/ adresáře) od tohoto
     * adresáře "src" - bez něj až po soubor file. V případě, že se na cestě file nenachází adresář "src" vrátí se výše
     * zmíněný výchozí balíček.</i>
     *
     * @param file
     *         - cesta k třídě jazyka Java (musí být cesta k souboru - ne nezbytně soubor s příponou .java, ale musí být
     *         soubor).
     *
     * @return buď adresáře, kterou jsou mezi adresářem "src" a adresářem, kde se nachází cílový soubor file nebo
     * výchozí balíčkek "defaultPackage".
     */
    private static String getPackages(final File file) {
        final File fileSrcDir = getPathToSrcDir(file);

        // Podmínka by neměla nikdy nastat:
        if (fileSrcDir == null)
            return Constants.DEFAULT_PACKAGE;


        final String pathToFile = file.getAbsolutePath();
        final String pathToSrcDir = fileSrcDir.getAbsolutePath();

        // Podmínka by neměla nastat:
        if (pathToFile.length() < pathToSrcDir.length())
            return Constants.DEFAULT_PACKAGE;


        final String pathToParentDirOfFile = file.getParent();

        /*
         * Tato podmínka může nastat v případě, že uživatel bude chtít vytvořit soubor file právě v adresáři "src" v
         * takovém případě, se nevezmou žádné balíčky, ale je třeba vrátit výchozí balíček. I když by to nemělo být,
         * když se do třídy vloží výchozí balíček, ale třída bude v adresáři "src", ale to už by měl uživatel vědět.
         */
        if (pathToParentDirOfFile.length() == pathToSrcDir.length())
            return Constants.DEFAULT_PACKAGE;

        // +1 - na odstranění lomítka v cestě:
        final String packagesWithSlash = pathToFile.substring(pathToSrcDir.length() + 1,
                pathToParentDirOfFile.length());

        // Vrátí se text (balíčky) tak, že se nahradí lomítka za desetinné tečky pro oddělení jednotlivých balíčků:
        return packagesWithSlash.replace(File.separator, ".");
    }


    /**
     * Získání cesty k adresář "src".
     *
     * <i>Prohledá se cesta k souboru file tak, že se budou postupně brát jednotlivé rodičovské adresáře a skončí se v
     * případě, že se najde adresář s názvem "src", pokud se adresář nenajde, vrátí se null.</i>
     *
     * @param file
     *         - cesta k nějakému souboru. Z této cesty se má vrátit adresář "src".
     *
     * @return cestu k adresáři "src" na cestě file, jinak null, pokud nebude adresář "src na cestě file nalezen.
     */
    private static File getPathToSrcDir(final File file) {
        File temp = file;

        File fileSrcDir = null;

        while (temp != null) {
            temp = temp.getParentFile();

            // temp může být null při posledním získání předka výše:
            if (temp != null && temp.getName().equalsIgnoreCase("src")) {
                fileSrcDir = temp;
                break;
            }
        }
        return fileSrcDir;
    }


    /**
     * Metoda, která vymaže soubor nebo adresář na označené pozici
     */
    private void deleteFile() {
        // Zde potřebuji kvůli metodě, kterou mám použit otestovat, zda se jedná o soubor nebo adresář:
        if (selectedFile.isDirectory()) {
            // Zde se jedná o adresář, tak musím použít metodu, která smaže jak adresář, tak i jeho obsah:
            try {
                FileUtils.deleteDirectory(selectedFile);

                // Přenačtu stromovou strukturu:
                // Zde by bylomožné otevřít například adresář src ve stromové struktuře:
                myTree.refreshTree(null, false);

            } catch (final IOException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO, "Zachycena výjimka při mazání adresáře na umístění: "
                            + selectedFile.getAbsolutePath()
                            + ", třída JtreePopupMenu, metoda po kliknutí na tlačítko smazat v menu pro Jtree.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();

                JOptionPane.showMessageDialog(this,
                        txtErrorWhileDeletingDirText + "\n" + txtLocation + ": " + selectedFile.getAbsolutePath(),
                        txtErrorWhileDeletingDirTitle, JOptionPane.ERROR_MESSAGE);
            }
        }


        // Zde se jedná o soubor, tak stačí použít metodu, která smaže pouze označený soubor:
        else {
            FileUtils.deleteQuietly(selectedFile);

            // Přenačtu stromovou strukturu:
            // Zde by bylomožné otevřít například adresář src ve stromové struktuře:
            myTree.refreshTree(null, false);
        }
    }


    /**
     * Metoda,která zjistí, zda uživatel označil soubor nebo adresář pro přesun nebo smazání zda se nachází v aktuálně
     * otevřeném projektu v aplikaci, pokud ano vrátí true, pokud ne, vrátí se false.
     *
     * @return true, pokud uživatel označil soubor nebo adresář, který se nachází v aktuálně otevřeném projektu v
     * aplikaci, jinak false
     */
    private boolean isFileFromOpenProject() {
        final String openProject = PreferencesApi.getPreferencesValue(PreferencesApi.OPEN_PROJECT, null);

        if (openProject == null)
            return false;

        // Zde se podařilo načíst nějakou hodnotu, tak otestuji, zda je to closed, a
        // pokud ne, tak otestuji,
        // zda je to cesta k adresáři projektu a ještě ten projekt existuje:
        if (!openProject.equals(Constants.CLOSED_PROJECT) && ReadFile.existsDirectory(openProject)) {

            if (openProject.length() <= selectedFile.getAbsolutePath().length()) {
                // Zde se může jednat minimálně o adresář projektu pro smazání, tak musím
                // otestovat
                // jejich cesty:
                // final String fileOpenProject = file.getAbsolutePath();
                final String fileForDelete = selectedFile.getAbsolutePath();

                // promenná, kterou nastavím na true, a pokud se v následujícím cyklu nebude
                // rovnat
                // jediné písmeno, tak se nejedná o soubor, který je na cestě v aktuálně
                // otevřeném projektu
                boolean isOnThePath = true;

                for (int i = 0; i < openProject.length(); i++)
                    if (openProject.charAt(i) != fileForDelete.charAt(i)) {
                        // Zde se alespoň jeden znak na cestě nerovná, takže mohu smazat příslušný
                        // soubor
                        isOnThePath = false;
                        break;
                    }

                return isOnThePath;
            }

            // Zde se ani nemůže jednat o adresář otevřeného projektu, protože je menší
            // délky:
        }
        // Zde se v souboru s nastavením nenachází položka s informací o otevřeném
        // projektu, tuto možnost, ale
        // podobně jako níže vyloučím:


        return false;
    }


    /**
     * Metoda, která slouží pouze pro "znepřístupnění" tlačítka v tomto menu. Tj. že na toto tlačítko nepůjde kliknout,
     * protože pokud je otevřen v průzkumníkovi projektu nejaky projekt, tak by nemelo jit například přejmenovat třidu,
     * apod. protože by se to již neprojevilo v samotném otevřeném projektu v aplikaci, tak toto tlačítko tedy
     * znepřístupním, ale pokud je otevřen Workspace, tak lze příslušný soubor ši adresář, klasicky přejmenovat
     */
    final void enableItem() {
        itemRename.setEnabled(false);
    }


    /**
     * metoda, která slouží pro vložení textu do akce, která slouží pro nastavení, zda se má příslušná větev rozbalit
     * nebo schovat hodnotu, která slouží jako text - pro tlačítko, resp. tu akci.
     *
     * @param value
     *         - text pro příslušnou akci, která slouží jako tlačítko pro rozbalení nebo zabalení příslušné větve.
     */
    final void putValueToActCollapseExpandLeaf(final String value) {
        actCollapseExpandLeaf.putValue(Action.NAME, value);
    }


    /**
     * Metoda, která slouží pro nastavení toho, zda se má akce pro rozbalení nebo zabalení větve zpřístupnit nebo ne,
     * tj. když je například označen soubor, pak se tato akce, resp. možnost pro rozbalení / zabalení větve zpřístupní v
     * ostatních případech nebude přístupná.
     *
     * @param enable
     *         logická proměnná, která značí, zda se má výše popsaná akce / tlačítko zpřístupnit nebo ne.
     */
    final void setActCollapseExpandLeafEnabled(final boolean enable) {
        actCollapseExpandLeaf.setEnabled(enable);
    }


    /**
     * metoda, která slouží pro vložení textu do akce, která slouží pro nastavení, zda se má příslušná větev rozbalit
     * (či rozšířit) nebo zabalit i se všemi sub adresáři hodnotu, která slouží jako text - pro tlačítko, resp. tu
     * akci.
     *
     * @param value
     *         - text pro příslušnou akci, která slouží jako tlačítko pro rozbalení nebo zabalení příslušné větve.
     */
    final void putValueToActCollapseExpandWholeLeaf(final String value) {
        actCollapseExpandWholeLeaf.putValue(Action.NAME, value);
    }


    /**
     * Metoda, která slouží pro nastavení toho, zda se má akce pro rozbalení nebo zabalení celé větve i s jejími sub
     * adresáři zpřístupnit nebo ne, tj. když je například označen soubor, pak se tato akce, resp. možnost pro rozbalení
     * / zabalení větve se sub adresáři zpřístupní v ostatních případech nebude přístupná.
     *
     * @param enable
     *         logická proměnná, která značí, zda se má výše popsaná akce / tlačítko zpřístupnit nebo ne.
     */
    final void setActCollapseExpandWholeLeafEnabled(final boolean enable) {
        actCollapseExpandWholeLeaf.setEnabled(enable);
    }


    /**
     * Metoda, která slouží jako setr na proměnnou, která značí v podstatě označenou položku v Jtree - ve stromové
     * struktuře.
     *
     * @param selectedPath
     *         - ceska k označené položce ve stromové struktuře (Jtree).
     */
    final void setSelectedPath(final TreePath selectedPath) {
        this.selectedPath = selectedPath;
    }
	
	
	
	
	
	
	
	
	
	
	@Override
	public void setLanguage(final Properties properties) {
		languageProperties = properties;
		
		if (properties != null) {
			menuNew.setText(properties.getProperty("Pe_Jpm_MenuNew", Constants.PE_JPM_MENU_NEW));
            menuOpen.setText(properties.getProperty("Pe_Jpm_MenuOpen", Constants.PE_JPM_MENU_OPEN));

			itemOpenInInternalFrame.setText(properties.getProperty("Pe_Jpm_ItemOpenFileInInternalFrame", Constants.PE_JPM_ITEM_OPEN_FILE_IN_INTERNAL_FRAME));
            itemOpenInFileExplorerInOs.setText(properties.getProperty("Pe_Jpm_ItemOpenInFileExplorerInOs", Constants.PE_JPM_ITEM_OPEN_IN_FILE_EXPLORER_IN_OS));
            itemOpenInDefaultAppInOs.setText(properties.getProperty("Pe_Jpm_ItemOpenInDefaultAppInOs", Constants.PE_JPM_ITEM_OPEN_IN_DEFAULT_APP_IN_OS));

			itemRefresh.setText(properties.getProperty("Pe_Jpm_ItemRefresh", Constants.PE_JPM_ITEM_REFRESH));
			itemNewFile.setText(properties.getProperty("Pe_Jpm_ItemNewFile", Constants.PE_JPM_ITEM_NEW_FILE));
			itemNewFolder.setText(properties.getProperty("Pe_Jpm_ItemNewFolder", Constants.PE_JPM_ITEM_NEW_FOLDER));
			itemNewProject.setText(properties.getProperty("Pe_Jpm_ItemNewProject", Constants.PE_JPM_ITEM_NEW_PROJECT));
			itemNewExcelList.setText(properties.getProperty("Pe_Jpm_ItemNewExcelList", Constants.PE_JPM_ITEM_NEW_EXCEL_LIST));
			itemDelete.setText(properties.getProperty("Pe_Jpm_ItemDelete", Constants.PE_JPM_ITEM_DELETE));
			itemRename.setText(properties.getProperty("Pe_Jpm_ItemRename", Constants.PE_JPM_ITEM_RENAME));
			itemPaste.setText(properties.getProperty("Pe_Jpm_ItemPaste", Constants.PE_JPM_ITEM_PASTE));
			itemCopy.setText(properties.getProperty("Pe_Jpm_ItemCopy", Constants.PE_JPM_ITEM_COPY));
			itemCut.setText(properties.getProperty("Pe_Jpm_ItemCut", Constants.PE_JPM_ITEM_CUT));
			
			
			// Texty do proměnných do chybových hlášek:
			txtFileAlreadyExistInDirText = properties.getProperty("Pe_Jpm_Txt_FileAlreadyExistInDirText", Constants.PE_JPM_TXT_FILE_ALREADY_EXIST_IN_DIR_TEXT);
			txtFileAlreadyExistInDirTitle = properties.getProperty("Pe_Jpm_Txt_FileAlreadyExistInDirTitle", Constants.PE_JPM_TXT_FILE_ALREADY_EXIST_IN_DIR_TITLE);
			txtCantCreateFileText = properties.getProperty("Pe_Jpm_Txt_CannotCreateFileText", Constants.PE_JPM_TXT_CANT_CREATE_FILE_TEXT);
			txtLocation = properties.getProperty("Pe_Jpm_Txt_Location", Constants.PE_JPM_TXT_LOCATION);
			txtCantCreateFileTitle = properties.getProperty("Pe_Jpm_Txt_CantCreateFileTitle", Constants.PE_JPM_TXT_CANT_CREATE_FILE_TITLE);
			txtDirAlreadyExistText = properties.getProperty("Pe_Jpm_Txt_DirAlreadyExistText", Constants.PE_JPM_TXT_DIR_ALREADY_EXIST_TEXT);
			txtDirAlreadyExistTitle = properties.getProperty("Pe_Jpm_Txt_DirAlreadyExistTitle", Constants.PE_JPM_TXT_DIR_ALREADY_EXIST_TITLE);
			txtErrorWhileCreatingDirText = properties.getProperty("Pe_Jpm_Txt_ErrorWhileCreatingDirText", Constants.PE_JPM_TXT_ERROR_WHILE_CREATING_DIR_TEXT);
			txtErrorWhileCreatingDirTitle = properties.getProperty("Pe_Jpm_Txt_ErrorWhileCreatingDirTitle", Constants.PE_JPM_TXT_EROR_WHILE_CREATING_DIR_TITLE);
			txtCantDeleteFilesFromOpenProjectText = properties.getProperty("Pe_Jpm_Txt_CannotDeleteFilesFromOpenProjectText", Constants.PE_JPM_TXT_CANT_DELETE_FILES_FROM_OPEN_PROJECT_TEXT);
			txtCantDeleteFilesFromOpenProjectTitle = properties.getProperty("Pe_Jpm_Txt_CannotDeleteFilesFromOpenProjectTitle", Constants.PE_JPM_TXT_CANT_DELETE_FILES_FROM_OPEN_PROJECT_TITLE);
			txtFileDontExistAlreadyText = properties.getProperty("Pe_Jpm_Txt_FileDontExistAlreadyText", Constants.PE_JPM_TXT_FILE_DONT_EXIST_ALREADY_TEXT);
			txtFileDontExistAlreadyTitle = properties.getProperty("Pe_Jpm_Txt_FileDontExistAlreadyTitle", Constants.PE_JPM_TXT_FILE_DONT_EXIST_ALREADY_TITLE);
			txtFailedToRenameFileText = properties.getProperty("Pe_Jpm_Txt_FailedToRenameFileText", Constants.PE_JPM_TXT_FAILED_TO_RENAME_FILE_TEXT);
			txtOriginalName = properties.getProperty("Pe_Jpm_Txt_OriginalName", Constants.PE_JPM_TXT_ORIGINAL_NAME);
			txtNewName = properties.getProperty("Pe_Jpm_Txt_NewName", Constants.PE_JPM_TXT_NEW_NAME);
			txtFailedToRenameFileTitle = properties.getProperty("Pe_Jpm_Txt_FailedToRenameFileTitle", Constants.PE_JPM_TXT_FAILED_TO_RENAME_FILE_TITLE);
			txtCannotRenameFileText = properties.getProperty("Pe_Jpm_Txt_CannotRenameFileText", Constants.PE_JPM_TXT_CANNOT_RENAME_FILE_TEXT);
			txtCannotRenameFileTitle = properties.getProperty("Pe_Jpm_Txt_CannotRenameFileTitle", Constants.PE_JPM_TXT_CANNOT_RENAME_FILE_TITLE);
			txtFileWithNameAlreadyExistText = properties.getProperty("Pe_Jpm_Txt_FileWithNameAlreadyExistText", Constants.PE_JPM_TXT_FILE_WITH_NAME_ALREADY_EXIST_TEXT);
			txtTarget = properties.getProperty("Pe_Jpm_Txt_Target", Constants.PE_JPM_TXT_TARGET);
			txtFileWithNameAlreadyExistTitle = properties.getProperty("Pe_Jpm_Txt_FileWithNameAlreadyExistTitle", Constants.PE_JPM_TXT_FILE_WITH_NAME_ALREADY_EXIST_TITLE);
			txtErrorWhileMovingFileText = properties.getProperty("Pe_Jpm_Txt_ErrorWhileMovingFileText", Constants.PE_JPM_TXT_ERROR_WHILE_MOVING_FILE_TEXT);
			txtSource = properties.getProperty("Pe_Jpm_Txt_Source", Constants.PE_JPM_TXT_SOURCE);
			txtErrorWhileMovingFileTitle = properties.getProperty("Pe_Jpm_Txt_ErrorWhileMovingFileTitle", Constants.PE_JPM_TXT_ERROR_WHILE_MOVING_FILE_TITLE);
			txtFileAlreadyExistInLocationText = properties.getProperty("Pe_Jpm_Txt_FileAlreadyExistInLocationText", Constants.PE_JPM_TXT_FILE_ALREADY_EXIST_IN_LOCATION_TEXT);
			txtFileAlreadyExistInLocationTitle = properties.getProperty("Pe_Jpm_Txt_FileAlreadyExistInLocationTitle", Constants.PE_JPM_TXT_FILE_ALREADY_EXIST_IN_LOCATION_TITLE);
			txtErrorWhileMovingFileToLocationText = properties.getProperty("Pe_Jpm_Txt_ErrorWhileMovingFileToLocationText", Constants.PE_JPM_TXT_ERROR_WHILE_MOVING_FILE_TO_LOCATION_TEXT);
			txtErrorWhileMovingFileToLocationTitle = properties.getProperty("Pe_Jpm_Txt_ErrorWhileMovingFileToLocationTitle", Constants.PE_JPM_TXT_ERROR_WHILE_MOVING_FILE_TO_LOCATION_TITLE);
			txtTargetPlaceContainFileNameText = properties.getProperty("Pe_Jpm_Txt_TargetPlaceContainFileNameText", Constants.PE_JPM_TXT_TARGET_PLACE_CONTAIN_FILE_NAME_TEXT);
			txtTargetPlaceContainFileNameTitle = properties.getProperty("Pe_Jpm_Txt_TargetPlaceContainFileNameTitle", Constants.PE_JPM_TXT_TARGET_PLACE_CONTAIN_FILE_NAME_TITLE);
			txtErrorWihleCopyingDirText = properties.getProperty("Pe_Jpm_Txt_ErrorWhileCopyingDirText", Constants.PE_JPM_TXT_ERROR_WHILE_COPYING_DIR_TEXT);
			txtErrorWihleCopyingDirTitle = properties.getProperty("Pe_Jpm_Txt_ErrorWhileCopyingDirTitle", Constants.PE_JPM_TXT_ERROR_WHILE_COPYING_DIR_TITLE);
			txtErrorWhileCopyingSelectedFileText = properties.getProperty("Pe_Jpm_Txt_ErrorWhileCopyingSelectedFileText", Constants.PE_JPM_TXT_ERROR_WHILE_COPYING_SELECTED_FILE_TEXT);
			txtErrorWhileCopyingSelectedFileTitle = properties.getProperty("Pe_Jpm_Txt_ErrorWhileCopyingSelectedFileTitle", Constants.PE_JPM_TXT_ERROR_WHILE_COPYING_SELECTED_FILE_TITLE);
			txtTargetLocationDontExistText = properties.getProperty("Pe_Jpm_Txt_TargetLocationDontExistText", Constants.PE_JPM_TXT_TARGET_LOCATION_DONT_EXIST_TEXT);
			txtTargetLocationDontExistTitle = properties.getProperty("Pe_Jpm_Txt_TargetLocationDontExistTitle", Constants.PE_JPM_TXT_TARGET_LOCATION_DONT_EXIST_TITLE);
			txtSelectedFileDoesntExistText = properties.getProperty("Pe_Jpm_Txt_SelectedFiledoesntExistText", Constants.PE_JPM_TXT_SELECTED_FILE_DOESNT_EXIST_TEXT);
			txtSelectedFileDoesntExistTitle = properties.getProperty("Pe_Jpm_Txt_SelectedFileDoesntExistTitle", Constants.PE_JPM_TXT_SELECTED_FILE_DOESNT_EXIST_TITLE);
			txtCannotMoveFileFromOpenProjectText = properties.getProperty("Pe_Jpm_Txt_CannotMoveFileFromOpenProjectText", Constants.PE_JPM_TXT_CANNOT_MOVE_FILE_FROM_OPEN_PROJECT_TEXT);
			txtCannotMoveFileFromOpenProjectTitle = properties.getProperty("Pe_Jpm_Txt_CannotMoveFileFromOpenProjectTitle", Constants.PE_JPM_TXT_CANNOT_MOVE_FILE_FROM_OPEN_PROJECT_TITLE);
			txtErrorWhileDeletingDirText = properties.getProperty("Pe_Jpm_Txt_ErrorWhileDeletingDirText", Constants.PE_JPM_TXT_ERROR_WHILE_DLETING_DIR_TEXT);
			txtErrorWhileDeletingDirTitle = properties.getProperty("Pe_Jpm_Txt_ErrorWhileDeletingDirTitle", Constants.PE_JPM_TXT_ERROR_WHILE_DELETING_DIR_TITLE);
			txtErrorWhileCreatingNewListInExcelFileText_1 = properties.getProperty("Pe_Jpm_Txt_ErrorWhileCreatingNewListInExcelFileText_1", Constants.PE_JPM_TXT_ERROR_WHILE_CREATING_NEW_LIST_IN_EXCEL_FILE_TEXT_1);
			txtErrorWhileCreatingNewListInExcelFileText_2 = properties.getProperty("Pe_Jpm_Txt_ErrorWhileCreatingNewListInExcelFileText_2", Constants.PE_JPM_TXT_ERROR_WHILE_CREATING_NEW_LIST_IN_EXCEL_FILE_TEXT_2);
			txtErrorWhileCreatingNewListInExcelFileText_3 = properties.getProperty("Pe_Jpm_Txt_ErrorWhileCreatingNewListInExcelFileText_3", Constants.PE_JPM_TXT_ERROR_WHILE_CREATING_NEW_LIST_IN_EXCEL_FILE_TEXT_3);
			txtErrorWhileCreatingNewListInExcelFileTitle = properties.getProperty("Pe_Jpm_Txt_ErrorWhileCreatingNewListInExcelFileTitle", Constants.PE_JPM_TXT_ERROR_WHILE_CREATING_NEW_LIST_IN_EXCEL_FILE_TITLE);

            txtCommentText = properties.getProperty("WtfCommentForClass", Constants.WTF_TEXT_COMMENT_CLASS);
            txtConstructorComment = languageProperties.getProperty("WtfCommentConstructor", Constants.WTF_TEXT_CONSTRUCTOR);
		}
		
		
		else {
			menuNew.setText(Constants.PE_JPM_MENU_NEW);
            menuOpen.setText(Constants.PE_JPM_MENU_OPEN);
			
			itemOpenInInternalFrame.setText(Constants.PE_JPM_ITEM_OPEN_FILE_IN_INTERNAL_FRAME);
            itemOpenInFileExplorerInOs.setText(Constants.PE_JPM_ITEM_OPEN_IN_FILE_EXPLORER_IN_OS);
            itemOpenInDefaultAppInOs.setText(Constants.PE_JPM_ITEM_OPEN_IN_DEFAULT_APP_IN_OS);

			itemRefresh.setText(Constants.PE_JPM_ITEM_REFRESH);
			itemNewFile.setText(Constants.PE_JPM_ITEM_NEW_FILE);
			itemNewFolder.setText(Constants.PE_JPM_ITEM_NEW_FOLDER);
			itemNewProject.setText(Constants.PE_JPM_ITEM_NEW_PROJECT);
			itemNewExcelList.setText(Constants.PE_JPM_ITEM_NEW_EXCEL_LIST);
			itemDelete.setText(Constants.PE_JPM_ITEM_DELETE);
			itemRename.setText(Constants.PE_JPM_ITEM_RENAME);
			itemPaste.setText(Constants.PE_JPM_ITEM_PASTE);
			itemCopy.setText(Constants.PE_JPM_ITEM_COPY);
			itemCut.setText(Constants.PE_JPM_ITEM_CUT);
			
			
			// Texty do proměnných do chybových hlášek:
			txtFileAlreadyExistInDirText = Constants.PE_JPM_TXT_FILE_ALREADY_EXIST_IN_DIR_TEXT;
			txtFileAlreadyExistInDirTitle = Constants.PE_JPM_TXT_FILE_ALREADY_EXIST_IN_DIR_TITLE;
			txtCantCreateFileText = Constants.PE_JPM_TXT_CANT_CREATE_FILE_TEXT;
			txtLocation = Constants.PE_JPM_TXT_LOCATION;
			txtCantCreateFileTitle = Constants.PE_JPM_TXT_CANT_CREATE_FILE_TITLE;
			txtDirAlreadyExistText = Constants.PE_JPM_TXT_DIR_ALREADY_EXIST_TEXT;
			txtDirAlreadyExistTitle = Constants.PE_JPM_TXT_DIR_ALREADY_EXIST_TITLE;
			txtErrorWhileCreatingDirText = Constants.PE_JPM_TXT_ERROR_WHILE_CREATING_DIR_TEXT;
			txtErrorWhileCreatingDirTitle = Constants.PE_JPM_TXT_EROR_WHILE_CREATING_DIR_TITLE;
			txtCantDeleteFilesFromOpenProjectText = Constants.PE_JPM_TXT_CANT_DELETE_FILES_FROM_OPEN_PROJECT_TEXT;
			txtCantDeleteFilesFromOpenProjectTitle = Constants.PE_JPM_TXT_CANT_DELETE_FILES_FROM_OPEN_PROJECT_TITLE;
			txtFileDontExistAlreadyText = Constants.PE_JPM_TXT_FILE_DONT_EXIST_ALREADY_TEXT;
			txtFileDontExistAlreadyTitle = Constants.PE_JPM_TXT_FILE_DONT_EXIST_ALREADY_TITLE;
			txtFailedToRenameFileText = Constants.PE_JPM_TXT_FAILED_TO_RENAME_FILE_TEXT;
			txtOriginalName = Constants.PE_JPM_TXT_ORIGINAL_NAME;
			txtNewName = Constants.PE_JPM_TXT_NEW_NAME;
			txtFailedToRenameFileTitle = Constants.PE_JPM_TXT_FAILED_TO_RENAME_FILE_TITLE;
			txtCannotRenameFileText = Constants.PE_JPM_TXT_CANNOT_RENAME_FILE_TEXT;
			txtCannotRenameFileTitle = Constants.PE_JPM_TXT_CANNOT_RENAME_FILE_TITLE;
			txtFileWithNameAlreadyExistText = Constants.PE_JPM_TXT_FILE_WITH_NAME_ALREADY_EXIST_TEXT;
			txtTarget = Constants.PE_JPM_TXT_TARGET;
			txtFileWithNameAlreadyExistTitle = Constants.PE_JPM_TXT_FILE_WITH_NAME_ALREADY_EXIST_TITLE;
			txtErrorWhileMovingFileText = Constants.PE_JPM_TXT_ERROR_WHILE_MOVING_FILE_TEXT;
			txtSource = Constants.PE_JPM_TXT_SOURCE;
			txtErrorWhileMovingFileTitle = Constants.PE_JPM_TXT_ERROR_WHILE_MOVING_FILE_TITLE;
			txtFileAlreadyExistInLocationText = Constants.PE_JPM_TXT_FILE_ALREADY_EXIST_IN_LOCATION_TEXT;
			txtFileAlreadyExistInLocationTitle = Constants.PE_JPM_TXT_FILE_ALREADY_EXIST_IN_LOCATION_TITLE;
			txtErrorWhileMovingFileToLocationText = Constants.PE_JPM_TXT_ERROR_WHILE_MOVING_FILE_TO_LOCATION_TEXT;
			txtErrorWhileMovingFileToLocationTitle = Constants.PE_JPM_TXT_ERROR_WHILE_MOVING_FILE_TO_LOCATION_TITLE;
			txtTargetPlaceContainFileNameText = Constants.PE_JPM_TXT_TARGET_PLACE_CONTAIN_FILE_NAME_TEXT;
			txtTargetPlaceContainFileNameTitle = Constants.PE_JPM_TXT_TARGET_PLACE_CONTAIN_FILE_NAME_TITLE;
			txtErrorWihleCopyingDirText = Constants.PE_JPM_TXT_ERROR_WHILE_COPYING_DIR_TEXT;
			txtErrorWihleCopyingDirTitle = Constants.PE_JPM_TXT_ERROR_WHILE_COPYING_DIR_TITLE;
			txtErrorWhileCopyingSelectedFileText = Constants.PE_JPM_TXT_ERROR_WHILE_COPYING_SELECTED_FILE_TEXT;
			txtErrorWhileCopyingSelectedFileTitle = Constants.PE_JPM_TXT_ERROR_WHILE_COPYING_SELECTED_FILE_TITLE;
			txtTargetLocationDontExistText = Constants.PE_JPM_TXT_TARGET_LOCATION_DONT_EXIST_TEXT;
			txtTargetLocationDontExistTitle = Constants.PE_JPM_TXT_TARGET_LOCATION_DONT_EXIST_TITLE;
			txtSelectedFileDoesntExistText =  Constants.PE_JPM_TXT_SELECTED_FILE_DOESNT_EXIST_TEXT;
			txtSelectedFileDoesntExistTitle = Constants.PE_JPM_TXT_SELECTED_FILE_DOESNT_EXIST_TITLE;
			txtCannotMoveFileFromOpenProjectText = Constants.PE_JPM_TXT_CANNOT_MOVE_FILE_FROM_OPEN_PROJECT_TEXT;
			txtCannotMoveFileFromOpenProjectTitle = Constants.PE_JPM_TXT_CANNOT_MOVE_FILE_FROM_OPEN_PROJECT_TITLE;
			txtErrorWhileDeletingDirText = Constants.PE_JPM_TXT_ERROR_WHILE_DLETING_DIR_TEXT;
			txtErrorWhileDeletingDirTitle = Constants.PE_JPM_TXT_ERROR_WHILE_DELETING_DIR_TITLE;
			txtErrorWhileCreatingNewListInExcelFileText_1 = Constants.PE_JPM_TXT_ERROR_WHILE_CREATING_NEW_LIST_IN_EXCEL_FILE_TEXT_1;
			txtErrorWhileCreatingNewListInExcelFileText_2 = Constants.PE_JPM_TXT_ERROR_WHILE_CREATING_NEW_LIST_IN_EXCEL_FILE_TEXT_2;
			txtErrorWhileCreatingNewListInExcelFileText_3 = Constants.PE_JPM_TXT_ERROR_WHILE_CREATING_NEW_LIST_IN_EXCEL_FILE_TEXT_3;
			txtErrorWhileCreatingNewListInExcelFileTitle = Constants.PE_JPM_TXT_ERROR_WHILE_CREATING_NEW_LIST_IN_EXCEL_FILE_TITLE;

            txtCommentText = Constants.WTF_TEXT_COMMENT_CLASS;
            txtConstructorComment = Constants.WTF_TEXT_CONSTRUCTOR;
		}
	}
}