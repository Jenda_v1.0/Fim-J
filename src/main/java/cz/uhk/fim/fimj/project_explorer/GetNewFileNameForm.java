package cz.uhk.fim.fimj.project_explorer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.forms.SourceDialogForm;

/**
 * Tato třída slouží jako dialog pro získání nového názvu nějakého souboru nebo adresáře na disku.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class GetNewFileNameForm extends SourceDialogForm implements ActionListener, LanguageInterface {

	private static final long serialVersionUID = 1L;


	/**
	 * Regulární výraz pro rozpoznání názvu souboru nebo adresáře file_Name(.extension)
	 */
	private static final String REGEX_FILE_NAME = "^\\s*\\w+\\s*(\\s*\\.\\s*\\w+)?\\s*$";
	
	
	/**
	 * Půvdní název pložky:
	 */
	private final String oldName;
	
	
	
	// Jlabely pro poznámky - informace pro uživatele:
	// lblInfo = informace o tom, co se má zadat do textového pole
	// lblErrorInfo = chybová hláška - že jsou zadány chybné údaje
	// lblNote = poznámka o tom, že se pouze přejmenuje označená položka, ale pokud to bude například
	// třída, apod. tak se už nepřejmenují ani balíčky, konstruktory, ... pouze název položky
	private final JLabel lblInfo, lblErrorInfo, lblNote;
	
	
	
	
	/**
	 * Textové pole pro zadání nového jména souboru:
	 */
	private final JTextField txtName;
	
	
	
	
	/**
	 * list se jménama položek, kteržch nesmí nový název nybývat
	 */
	private final List<String> fileNamesList;

	
	
	// Proměnné pro texty do chybových hlášek ve zvoleném jazyce:
	private static String txtFileNameAlreadyExistText, txtFileNameTextName, txtFileNameAlreadyExistTitle,
			txtWrongFileNameText, txtWrongFileNameTitle, txtTextFieldIsEmptyText, txtTextFieldIsEmptyTitle;
	
	

	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param fileNamesList
	 *            - List názvů, které nesmí uživatel zadat, resp. názvy, ak se nesmí
	 *            jmenovat nově uživatelem zvolené jméno.
	 * 
	 * @param oldName
	 *            - původní název položky
	 */
	public GetNewFileNameForm(final List<String> fileNamesList, final String oldName) {
		super();
		
		this.oldName = oldName;
		this.fileNamesList = fileNamesList;
		
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/projectExplorerIcons/RenameFileFormIcon.jpg",
				new Dimension(630, 260), true);
		
		
		
		gbc = createGbc();
		
		index = 0;
		
		
		
		
		gbc.gridwidth = 2;
		
		
		lblInfo = new JLabel("", JLabel.CENTER);
		setGbc(index, 0, lblInfo);
		
		
		
		lblNote = new JLabel("", JLabel.CENTER);
		setGbc(++index, 0, lblNote);
		
		
		
		
		
		lblErrorInfo = new JLabel("", JLabel.CENTER);
		lblErrorInfo.setFont(ERROR_FONT);
		
		lblErrorInfo.setForeground(Color.RED);
		setGbc(++index, 0, lblErrorInfo);
		
		lblErrorInfo.setVisible(false);
		
		
		
		
		
		
		txtName = new JTextField(oldName);
		setGbc(++index, 0, txtName);
		
		
		
		txtName.addKeyListener(new KeyAdapter() {
			
			@Override
			public void keyReleased(KeyEvent e) {				
				NameFormAbstract.highlightTextField(txtName, lblErrorInfo, REGEX_FILE_NAME);
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					testData();
				
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					variable = false;
					dispose();
				}				
			}
		});
		
		
		
		
		
		
		
		
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		btnOk = new JButton();
		btnCancel = new JButton();
		
		btnOk.addActionListener(this);
		btnCancel.addActionListener(this);
		
		pnlButtons.add(btnOk);
		pnlButtons.add(btnCancel);
		setGbc(++index, 0, pnlButtons);
		
		
		
		
		
		addWindowFocusListener(getWindowsAdapter());
		
		
		
		pack();
		setLocationRelativeTo(null);
	}

	
	

	
	/**
	 * Metoda, která otestuje zadaná data.
	 */
	private void testData() {
		if (!txtName.getText().isEmpty()) {
			if (txtName.getText().replace("\\s", "").matches(REGEX_FILE_NAME)) {
				final String newName = txtName.getText().replace("\\s", "");
				
				// logická proměnná, do které se uloží hodnota o tom, zda je zadaný název již použit nebo ne:
				boolean isUnique = true;
				
				for (final String s : fileNamesList) {
					if (s.equalsIgnoreCase(newName)) {
						JOptionPane.showMessageDialog(this,
								txtFileNameAlreadyExistText + "\n" + txtFileNameTextName + ": " + s,
								txtFileNameAlreadyExistTitle, JOptionPane.ERROR_MESSAGE);
						isUnique = false;
						break;
					}
				}
				
				if (isUnique) {
					variable = true;
					dispose();
				}				
			}
			else
				JOptionPane.showMessageDialog(this, txtWrongFileNameText, txtWrongFileNameTitle,
						JOptionPane.ERROR_MESSAGE);
		}
		else
			JOptionPane.showMessageDialog(this, txtTextFieldIsEmptyText, txtTextFieldIsEmptyTitle,
					JOptionPane.ERROR_MESSAGE);		
	}
	
	
	
	
	

	
	/**
	 * Metoda, která zobrazí dialog, čímž ho udělá modálním a až uživatel klikne na
	 * OK, tak vrátí data od uživatele - pokud budou v pořádku
	 * 
	 * @return nový název pro pložku od uživatele
	 */
	final String getNewFileName() {
		setVisible(true);
		
		if (variable)
			return txtName.getText().replace("\\s", "");
		
		return null;
	}
	
	

	
	
	
	
	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			setTitle(properties.getProperty("Pe_Gnfnf_DialogTitle", Constants.PE_GNFNF_DIALOG_TITLE) + ": " + oldName);
			
			lblInfo.setText(properties.getProperty("Pe_Gnfnf_LblInfo", Constants.PE_GNFNF_LBL_INFO) + ":");
			lblErrorInfo.setText(properties.getProperty("Pe_Gnfnf_LblErrorInfo", Constants.PE_GNFNF_LBL_ERROR_INFO));
			lblNote.setText(properties.getProperty("Pe_Gnfnf_LblNote", Constants.PE_GNFNF_LBL_NOTE));
			
			btnOk.setText(properties.getProperty("Pe_Gnfnf_BtnOk", Constants.PE_GNFNF_BTN_OK));
			btnCancel.setText(properties.getProperty("Pe_Gnfnf_BtnCalcel", Constants.PE_GNFNF_BTN_CANCEL));
			
			// Texty do proměnných pro výpis vchybových hláškách:
			txtFileNameAlreadyExistText = properties.getProperty("Pe_Gnfnf_TxtFileNameAlreadyExistText", Constants.PE_GNFNF_TXT_FILE_NAME_ALREADY_EXIST_TEXT);
			txtFileNameTextName = properties.getProperty("Pe_Gnfnf_TxtFileNameTextName", Constants.PE_GNFNF_TXT_FILE_NAME_TEXT_NAME);
			txtFileNameAlreadyExistTitle = properties.getProperty("Pe_Gnfnf_TxtFileNameAlreadyExistTitle", Constants.PE_GNFNF_TXT_FILE_NAME_ALREADY_EXIST_TITLE);
			txtWrongFileNameText = properties.getProperty("Pe_Gnfnf_WrongFileNameText", Constants.PE_GNFNF_WRONG_FILE_NAME_TEXT);
			txtWrongFileNameTitle = properties.getProperty("Pe_Gnfnf_TxtWrongFileNameTitle", Constants.PE_GNFNF_TXT_WRONG_FILE_NAME_TITLE);
			txtTextFieldIsEmptyText = properties.getProperty("Pe_Gnfnf_TxtTextFieldIsEmptyText", Constants.PE_GNFNF_TXT_TEXT_FIELD_IS_EMPTY_TEXT);
			txtTextFieldIsEmptyTitle = properties.getProperty("Pe_Gnfnf_TxtTextFieldIsEmptyTitle", Constants.PE_GNFNF_TXT_TEXT_FIELD_IS_EMPTY_TITLE);
		}
		
		
		else {
			setTitle(Constants.PE_GNFNF_DIALOG_TITLE + ": " + oldName);
			
			lblInfo.setText(Constants.PE_GNFNF_LBL_INFO + ":");
			lblErrorInfo.setText(Constants.PE_GNFNF_LBL_ERROR_INFO);
			lblNote.setText(Constants.PE_GNFNF_LBL_NOTE);
			
			btnOk.setText(Constants.PE_GNFNF_BTN_OK);
			btnCancel.setText(Constants.PE_GNFNF_BTN_CANCEL);
			
			// Texty do proměnných pro výpis vchybových hláškách:
			txtFileNameAlreadyExistText = Constants.PE_GNFNF_TXT_FILE_NAME_ALREADY_EXIST_TEXT;
			txtFileNameTextName = Constants.PE_GNFNF_TXT_FILE_NAME_TEXT_NAME;
			txtFileNameAlreadyExistTitle = Constants.PE_GNFNF_TXT_FILE_NAME_ALREADY_EXIST_TITLE;
			txtWrongFileNameText = Constants.PE_GNFNF_WRONG_FILE_NAME_TEXT;
			txtWrongFileNameTitle = Constants.PE_GNFNF_TXT_WRONG_FILE_NAME_TITLE;
			txtTextFieldIsEmptyText = Constants.PE_GNFNF_TXT_TEXT_FIELD_IS_EMPTY_TEXT;
			txtTextFieldIsEmptyTitle = Constants.PE_GNFNF_TXT_TEXT_FIELD_IS_EMPTY_TITLE;
		}
	}

	

	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnOk)
				testData();
			
			
			else if (e.getSource() == btnCancel) {
				variable = false;
				dispose();
			}
		}
	}
}