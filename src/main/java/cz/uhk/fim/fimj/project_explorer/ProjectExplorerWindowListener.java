package cz.uhk.fim.fimj.project_explorer;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.class_diagram.CheckRelationShipsBetweenClassesThread;
import cz.uhk.fim.fimj.code_editor_abstract.RSyntaxTextAreaKeyActionHelper;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.instances.Instances;
import cz.uhk.fim.fimj.preferences.PreferencesApi;
import cz.uhk.fim.fimj.swing_worker.ThreadSwingWorker;

/**
 * Tato třída slouží pouze jako Window Adapter, který obsahuje implementované metody, které reagují na zavření okna -
 * dialogu průzkumníku projektů.
 * <p>
 * V obou metodých se volá stejná metoda a sice metoda, která otestuje, zda se mají uložit otevřené soubory v komponentě
 * JdesktopPane - jednotlivá interní okna nebo ne. Více viz konkrétní metoda
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ProjectExplorerWindowListener extends WindowAdapter implements LanguageInterface {

    /**
     * Reference na instanci třídy ProjectExplorerDialog, aby tato třída mohla zavolat příslušné metody, a nebo získat
     * potřebné reference na otevřená okna:
     */
    private final ProjectExplorerDialog projectExplorerDialog;


    /**
     * Proměnné pro uchování textu pro zobrazení příslušného textu v dialogu:
     */
    private String txtSaveChangesText;
    private String txtSaveChangesTitle;


    /**
     * logická proměnná o tom, zda se mají po zavření dialogu - pruzkumniku projektů spustit vlákno, které přenačte
     * vztahy v diagramu tříd nebo ne
     */
    private final boolean checkRelationsBetweenClasses;


    /**
     * Reference na instanci třídy diagram tříd, která je potřeba, pokud se má spustit vlákno, které zkontroluje vztahy
     * v tomto diagramu mezi třídami
     */
    private final GraphClass classDiagram;


    /**
     * Tato logická proměnná slouží pro to, abych věděl, zda se mají do preferencí uložit otevřená okna nebo ne, aby se
     * znovu mohli načíst - pokud budou příslušné soubory ješě existovat při přísčím otevření tohoto dialogu s kořenovým
     * adresářem ve Wroskapce
     */
    private final boolean saveOpenFiles;


    /**
     * Proměnná, která slouží pro dočasné "uložení" reference na proměnnou s texty pro tuto aplikaci ve zvoleném jazyce,
     * aby bylo možné tyto texty předat dále do přenačtení diagramu instancí.
     */
    private final Properties languageProperties;


    /**
     * Konstrutor této třídy.
     *
     * @param projectExplorerDialog
     *         - reference na instanci třídy ProjectExplorerDialog, aby tato třída mohla získat reference na instnci
     *         JdesktopPane komponentu, kde jsou otevřené veškerá okna - soubory, a nebo zavolat potřebné meotdy v této
     *         třídě
     * @param languageProperties
     *         - soubor typu .properties, který obsahuje texty pro aplikaci ve zvoleném jazyce
     * @param checkRelationsBetweenClasses
     *         - logická proměnná o tom, zda se mají po zavření třídy coby dialogu - průzkuníku projektu zkontrolovat,
     *         resp. zda se má spustit vlákno, které zkontroluje vztahy mezi třídami v diagramu tříd, tato možnost
     *         nastavene pouze v případě, že se v pruzkumniku projektu - dialogu otevře projekt, ktery je otevren v
     *         aplikaci, pokud se otevre workspace, tak bude tato promenna false, protože nejspise neni otevren projekt,
     *         tak by se ani nemusel prenacitat vztahy, a i kdyby byl nejaky projekt otevren, tak by nebnylo jiste, v
     *         jakem projektu uzivatel co delal, a navic to muze prenasit rucne kliknutím na tlacitko v menu
     * @param saveOpenFiles
     *         - logkcká proměnná, dle které poznám, zda se mají otevřené soubory uložit do Preferencí pro opětovné
     *         načtení při přísštím otevření dialogu v adresáři Workspace nebo ne. Tato hodnota bude true puze v
     *         případě, že se jedná o dialog otevřený s kořenovým adresářem ve Workspace, jinak bude tato proměnná
     *         false, protože si nelze pamatovat soubory v každém otevřeném projektu zvlášt, resp. jde, ale neuhlídal
     *         bych jeho smazání, takže by zde v podstatě zůstaly napořád
     */
    ProjectExplorerWindowListener(final ProjectExplorerDialog projectExplorerDialog,
                                  final Properties languageProperties, final boolean checkRelationsBetweenClasses,
                                  final GraphClass classDiagram, final boolean saveOpenFiles) {
        this.projectExplorerDialog = projectExplorerDialog;
        this.checkRelationsBetweenClasses = checkRelationsBetweenClasses;
        this.classDiagram = classDiagram;
        this.saveOpenFiles = saveOpenFiles;
        this.languageProperties = languageProperties;

        setLanguage(languageProperties);
    }


    @Override
    public void windowClosing(final WindowEvent e) {
        // tato metoda se zavolá například, pokud se klikne na křížek v dialogu, nebo pomocí
        // kombinace kláves Alt + F4, apod. ...

        // Po zavření tohoto dialogu musím otestovat, zda jsou otevřeny nějaké soubory,
        // pokud ano, tak aby se případně uložily.

        // Takže si zjistím, zda jsou nějaká okna otevřená a pokud ano, tak je všechny proiteruji
        // a zjistím, zda došlo v nějakém okně ke změně, pokud ano, tak se pak spustí druhý
        // cyklus, který uloží všechny otevřená okna

        saveAllInternalFrames();

        /*
         * Zakázání vybraných klávesových zkratek pro editor příkazů (obecně pro veškeré implementace komponenty
         * {@link org.fife.ui.rsyntaxtextarea.RSyntaxTextArea}. Aby nebylo možné odebrat většítko apod.
         */
        RSyntaxTextAreaKeyActionHelper.enableSpecificAction(false);
    }


    @Override
    public void windowClosed(final WindowEvent e) {
        // Toto se zavolá například po zavření okna pomocí metody dispose z menu
        saveAllInternalFrames();

        /*
         * Zakázání vybraných klávesových zkratek pro editor příkazů (obecně pro veškeré implementace komponenty
         * {@link org.fife.ui.rsyntaxtextarea.RSyntaxTextArea}. Aby nebylo možné odebrat většítko apod.
         */
        RSyntaxTextAreaKeyActionHelper.enableSpecificAction(false);
    }


    /**
     * Metoda, která má za úkol uložit všechny texty, které jsou otevřeny v interních ráměch v desktopPane, tak tyto
     * texty z editoru uložit zpět do příslušných souborů - pokud tak uživatel zvolí.
     * <p>
     * Metoda otestuje, zda se alespoň jeden z z editorů - jejich text liší od těch skutečných - lokálních, a pokud ano,
     * tak se zeptá uživatele, zda se mají data uložit, pokud ano, tak se všechny souibory, které jsou otevřeny
     * vinterních rámech uložít texty z editorů zpět do původních souborů - se zapíšou
     */
    private void saveAllInternalFrames() {
        // Načtu si všechna okna v desktopPane:
        final JInternalFrame[] internalFramesArray = projectExplorerDialog.getDesktopPane().getAllFrames();

        // Zda došlo ke změně v souborechotevřených v dialogu:
        final boolean areTextDifferent = areTextInEditorsDifferent(internalFramesArray);


        if (areTextDifferent) {
            // Zde jsou texty různé, tak se zeptám uživatle, zda se mají změny uložit:
            final int choice = JOptionPane.showConfirmDialog(projectExplorerDialog, txtSaveChangesText,
                    txtSaveChangesTitle, JOptionPane.YES_NO_OPTION);

            if (choice == JOptionPane.YES_OPTION)
                projectExplorerDialog.saveDataInAllFrames(internalFramesArray);
        }


        /*
         * Zde se uloží vybrané vlastnosti pro to interní okna. Například, zda se mají zalamovat texty, zobrazovat
         * bílé znaky apod.
         */
        projectExplorerDialog.storeProjectExplorerProperties();


        // Zde ať už se data uložily nebo ne, to mně v tuto chvíli nezajíma, tak otestuji, zda se mají
        // uložit data, resp. otevřené soubory - interní okna do Preferences API, aby se při příštím otevření tohoto
        // dialogu -
        // průzkumníku projektů mohli zase otevřít (podobně jako napřílad Eclipse či NetBeans - tyto nástroje si také
        // pamatují otevřené třídy),
        // tak pomocí Preferences API si bude tato aplikace pamatovat i otevřené soubory v tomto dialogu, pokud se
        // bude jednat o dialog
        // otevřený s adresářem Workspace, jinak bychmusel hlídat každý projekt a to nebojím se zde napsat, že to
        // nejde, jak bych například hlídal
        // smazání projektů, apod.??? Vzdy bych musel zjistit, zda projekt existuje, kdyz ne, tak jej smazat, ale
        // taky se můze stat, ze
        // uzivatel bude vse otevirat v druhem editoru kodu a toto se uz nikdy neotevre, pak v Preferences budou
        // ulozeny veskere cesty zbytecne,
        // proto jsem sem nedaval to ukladani otevrenych projektu, uzivatel můze pokazdé pracovat na jinem projektu a
        // bylo by caste mazani ci
        // pridavani cest apod.

        // Oproti tomu, kdyz je otevren workspace, tak se v nem moc casto cele projekty nebo soubory nemanipuluje, i
        // kdyz uzivatel
        // otevre soubor mimo workspace, ne vzdy se můze smazat, navic pri zavreni ci otevreni staci otestovat, zda
        // jeste existuje, kdyz ne,
        // pak jej proste smazu. Resp. pri otevreni ignoruji a pri zavreni uz zde nebude. Neco podobneho by slo i u
        // otevreneho projektu vyse,
        // ale musel bych resit jednotlive projekty, což už je trochu težší a nad rámec bakalářské práce.

        // takže pokud je otevřen adresář Workspace, tak se pokusím uložit otevřené soubory, aby se pří příštím
        // otevření mohli znovu otevřít,
        // pokud budou tedy ještě existovat
        if (saveOpenFiles)
            saveFilesToPreferences(internalFramesArray);


        // Test, zda se má spustit vlákno, které otestuje vztahy mezi třídami v diagramu tříd:
        if (!checkRelationsBetweenClasses || classDiagram == null)
            return;


        // Spustí se vlákno, které otestuje vztahy mezi třídami v diagramu tříd


        // vytvoří se ThreadSwingWorker a předá se mu vlákno, jehož metoda run se vykoná:
        final ThreadSwingWorker mySw2 =
                new ThreadSwingWorker(new CheckRelationShipsBetweenClassesThread(classDiagram, null,
                        classDiagram.isShowRelationShipsToItself(),
                        classDiagram.isShowAssociationThroughAggregation()));

        // Spustí se swingWorker
        mySw2.execute();


        // Zobrazení loading dialogu, tím se stane modálním, dokud nedoběhne metoda run:
        mySw2.setVisibleLoadingDialog();






        /*
         * Dále je vhodné přenačíst i samotný diagram instancí, protože uživatel mohl například v otevřené třídě
         * odebrat nějakou proměnnou, která je aktuálně v nějaké instanci naplněna a zobzrauje se tak pomocí
         * příslušnéhovztahu mezi instancemi, ale když tuto proměnnou odebral, tak už by v diagramu instancí být
         * neměla, takže jej musím znovu přenačíst, aby se potenciální změny projevily i v diagramu instancí mezi
         * intancemi.
         *
         */
        final String pathToBinDir = App.READ_FILE.getPathToBin();

        if (pathToBinDir != null)
            Instances.refreshClassInstances(pathToBinDir, null, languageProperties, true);
    }


    /**
     * Zjištění, zda se texty v otevřeným interních oknech  souborech liší od těch, co je v těch souborech uloženo.
     * Resp. jestli došlo k nějaké změně v souborech.
     *
     * @param internalFramesArray
     *         - interní okna otevřená v dialogu.
     *
     * @return true v případě, že došlo k nějaké změně. Tedy uživatel změnil obsah nějakého otevřeného souboru /
     * interního okna. Jinak false, když nedošlo k žádné změně.
     */
    private static boolean areTextInEditorsDifferent(final JInternalFrame[] internalFramesArray) {
        // Zjistí se, zda došlo k nějaké změne v editoru:
        for (final JInternalFrame f : internalFramesArray) {
            // jiné typy oken do desktopPane nejsou ukládány, takže by nemělo být potřeba testovat následující
            // podmínku, ale pro jistotu ...
            if (!(f instanceof FileInternalFrame))
                continue;

            /*
             * Pokud alespon jeden otevřený soubor, tak jestli se jeho text v příslušném editoru v okně, liší od toho
              * na lokálním umístění, tak skončí cyklus vrátí se true. Došlo ke změně, tak se dotázat uživatelel na
              * uložení změn.
             *
             * Pokud soubor již neexistuje, nebo došlo ke změne, tak se vrátí true. Pak je dále na výběr uložení na
             * nové umístění apod.
             */
            if (!((FileInternalFrame) f).existFile() || !((FileInternalFrame) f).areCodeIdentical()) {
                return true;
            }
        }

        return false;
    }


    /**
     * Uložení cest k otevřeným souborům do preferencí. Toto je potřeba při otevřeném dialogu s kořenovým adresářem ve
     * workspace. Aby bylo možné je při dalším otevření dialogu otevřít - budou li existovat.
     *
     * @param internalFramesArray
     *         - interní okna / otevřené soubory, které se mají uložit do preferencí.
     */
    private static void saveFilesToPreferences(final JInternalFrame[] internalFramesArray) {
        // Existující otevřené soubory v dialogu:
        final List<InfoForSaveFilesToPreferences> filesList = getExistOpenedFiles(internalFramesArray);

        // Nyní musím z listu vložit všechny hodnoty do jednorozměrného pole,
        // abych je mohl vložít do preferencí, ale hlavně e ve správném formátu také získat:
        final InfoForSaveFilesToPreferences[] filesArray = new InfoForSaveFilesToPreferences[filesList.size()];

        for (int i = 0; i < filesList.size(); i++)
            filesArray[i] = filesList.get(i);

        // Nyní jsou v poli filesArray odkazy na otevřené soubory, tak se uloží do Preferencí, aby si je aplikace
        // pamatovala při příštím spuštění:
        PreferencesApi.setPreferencesObject(PreferencesApi.OPEN_FILES_ARRAY, filesArray);
    }


    /**
     * Získání potřebných informací o souborech, které se mají uložit do preferencí. To jsou soubory, které jsou
     * otevřené v dialogu průzkumník projektů a ještě existují.
     *
     * @param internalFramesArray
     *         - otevřené soubory / interní okna v průzkumníku projektů.
     *
     * @return kolekci, která bude obsahovat potřebné informace osouborech, které se mají uložit do preferencí pro
     * jejich znovu otevření při příštím spuštění dialogu s kořenovým adresářem ve workspace.
     */
    private static List<InfoForSaveFilesToPreferences> getExistOpenedFiles(final JInternalFrame[] internalFramesArray) {
        // List - kolekce, do které se vloží všechny otevřené soubory v okně - pokud existují:
        final List<InfoForSaveFilesToPreferences> filesList = new ArrayList<>();

        /*
         * Zde se projdou všechna otevřená okna a otestuje se, zda příslušný soubor ještě existuje, pokud ano, tak
         * uloží se jeho proměnná typu File - v příslušném otevřeném interním okně, které ukazuje na otevřený soubor:
         */
        for (final JInternalFrame f : internalFramesArray) {
            // možná zbytečná podmínka:
            if (!(f instanceof FileInternalFrame))// Nemělo by být potřeba
                continue;

            final FileInternalFrame myFrame = (FileInternalFrame) f;

            // Otestuji, zda otevřený soubor v příslušném interním okně na disku ještě existuje.
            // a pokud ano, tak ho vložím do listu, aby se vložíl do Preferencí:
            if (myFrame.existFile())
                filesList.add(new InfoForSaveFilesToPreferences(myFrame.getFile(), myFrame.getListName()));
        }

        return filesList;
    }


    @Override
    public void setLanguage(final Properties properties) {
        if (properties != null) {
            txtSaveChangesText = properties.getProperty("Pe_Mwl_Txt_SaveChangesText",
                    Constants.PE_MWL_TXT_SAVE_CHANGES_TEXT);
            txtSaveChangesTitle = properties.getProperty("Pe_Mwl_Txt_SaveChangesTitle",
                    Constants.PE_MWL_TXT_SAVE_CHANGES_TITLE);
        } else {
            txtSaveChangesText = Constants.PE_MWL_TXT_SAVE_CHANGES_TEXT;
            txtSaveChangesTitle = Constants.PE_MWL_TXT_SAVE_CHANGES_TITLE;
        }
    }
}