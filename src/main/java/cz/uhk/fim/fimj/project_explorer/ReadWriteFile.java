package cz.uhk.fim.fimj.project_explorer;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;

/**
 * Tato třída slouží pro čtení a některých vybraných souborů od Microsoftu, jako je například word a excel, dále pdf a
 * podobné soubory, které lze otevřít pomocí dostupných metod, další možná v budoucnu.
 * <p>
 * Pro otevření těchto souborů jsem využil knihovnu Apache POI, kterou jsem stáhnul z násldujících stránek:
 * https://poi.apache.org/download.html#POI-3.15
 * <p>
 * Dále k tomu byl potřebný soubor xmlBeans.jar, který jsem stáhnul z následujících stránek:
 * http://www.java2s.com/Code/Jar/x/Downloadxmlbeansjar.htm
 * <p>
 * <p>
 * Postup pro čtení excelovského dokumentu jsem našel zde: http://www.codejava
 * .net/coding/how-to-read-excel-files-in-java-using-apache-poi
 * <p>
 * Zápis do excelovského dokumentu jsem našel zde: http://www.codejava
 * .net/coding/how-to-write-excel-files-in-java-using-apache-poi
 * <p>
 * <p>
 * <p>
 * čtení Wordovského dokumentu jsem našel zde: https://gist.github.com/madan712/10641676
 * <p>
 * Zápis do Wordovského dokumentu jsem našel zde: http://javing.blogspot
 * .cz/2011/06/using-apache-poi-to-read-from-word.html
 * <p>
 * <p>
 * <p>
 * <p>
 * Pro PDF: Zdroj pro knihovnu: https://pdfbox.apache.org/download.cgi#20x
 * <p>
 * Zdroj pro čtení textu z Pdf souboru (str. 23): https://www.tutorialspoint.com/pdfbox/pdfbox_tutorial.pdf
 * <p>
 * Zdroj pro čtení textu z Pdf souboru - pro verzi2.0.4: http://stackoverflow
 * .com/questions/23813727/how-to-extract-text-from-a-pdf-file-with-apache-pdfbox
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ReadWriteFile implements ReadWriteFileInterface {

    @Override
    public boolean setTextToFile(final String pathToFile, final List<String> textList) {
        try (final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(pathToFile),
                StandardCharsets.UTF_8))) {

            for (final String s : textList) {
                writer.write(s);
                writer.newLine();
            }

            writer.flush();

            return true;

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při zapisování textu - zdrojového kódu do souboru: "
                        + pathToFile + ", třída v aplikaci: ReadWriteFile.java, metoda: setTextToFile().");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();

            return false;
        }
    }


    @Override
    public boolean setTextToFileWithoutNewLine(final String pathToFile, final List<String> textList) {
        try (final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(pathToFile),
                StandardCharsets.UTF_8))) {

            for (final String s : textList) {
                writer.write(s);
            }

            writer.flush();

            return true;

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při zapisování textu - zdrojového kódu do souboru: "
                        + pathToFile + ", text:\n" + textList);

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();

            return false;
        }
    }




    @Override
    public List<String> getTextOfFile(final String pathToFile) {
        // Vytořím si novou kolekce, do které přečtu text, resp. kó třídy po řádcích pořádcích
        final List<String> textOfClass = new ArrayList<>();

        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(pathToFile), StandardCharsets.UTF_8))) {

            String line;

            while ((line = reader.readLine()) != null)
                textOfClass.add(line);

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při čtení souboru - třídy na cestě: " + pathToFile
                        + ", třída v aplikaci: ReadWriteFile.java, metoda: getTextOfFile().");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();

            return textOfClass;
        }
        return textOfClass;
    }









    @Override
    public List<String> readWordFile(final File file) {
        // vytvořím si List, do kterého vložím získaná data:
        final List<String> textFromFileList = new ArrayList<>();

        try (final FileInputStream fis = new FileInputStream(file)) {

            // "Přečtu si dokument":
            final XWPFDocument document = new XWPFDocument(fis);

            // Získám si jednotlivé asi řádky:
            final List<XWPFParagraph> paragpraphsList = document.getParagraphs();


            // Transformuji data získaná z dokumentu do Stringu, abych jej
            // mohl vložit do editoru kodu v aplikaci:
            for (final XWPFParagraph p : paragpraphsList)
                textFromFileList.add(p.getText());

            // Uzavřu otevřené "proudy"
            document.close();

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při čtení Wordovského dokumentu: " + file.getAbsolutePath()
                        + ", třída v aplikaci: ReadWriteFile, metoda: readWordFile.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }

        // vrátím získaný text v podobě listu:
        return textFromFileList;
	}








    @Override
    public boolean writeToWordFile(final File file, final String text) {
        try (final XWPFDocument document = new XWPFDocument()) {

            final XWPFParagraph tmpParagraph = document.createParagraph();

            final XWPFRun tmpRun = tmpParagraph.createRun();

            tmpRun.setText(text);

            final FileOutputStream fos = new FileOutputStream(file);

            document.write(fos);

            fos.flush();

            return true;

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při zapisování textu do Wordovského dokumentu: "
                        + file.getAbsolutePath() + ", třída v aplikaci: ReadWriteFile, metoda: writeToWordFile.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }

        return false;
	}

	
	
	
	
	
	
	
	@Override
    public List<String> readExcelFile(final File file, final int listIndex) {
        // vytvořím si List, do kterého vložím získaná data:
        final List<String> textFromFileList = new ArrayList<>();

        try (final FileInputStream fis = new FileInputStream(file)) {

            final Workbook workBook = new XSSFWorkbook(fis);

            // Zde pro jistotu otestuji, zda je zadán index listu > -1, tj. od nuly výše,
            // i když by tato možnost nastat neměla.
            if (listIndex <= -1 || listIndex >= workBook.getNumberOfSheets()) {
                workBook.close();
                return textFromFileList;
            }

            final Sheet selectedSheet = workBook.getSheetAt(listIndex);

            for (final Row nextRow : selectedSheet) {
                final Iterator<Cell> cellIterator = nextRow.cellIterator();

                /*
                 * Proměnná, do které vložím text v daném řádku - oddělený středníkem coby oddělení sloupcu v
                 * excelu:
                 */
                final StringBuilder textAtRowBuilder = new StringBuilder();

                while (cellIterator.hasNext()) {
                    final Cell cell = cellIterator.next();

                    switch (cell.getCellTypeEnum()) {
                        case BOOLEAN:
                            textAtRowBuilder.append(cell.getBooleanCellValue());
                            break;

                        case NUMERIC:
                            textAtRowBuilder.append(cell.getNumericCellValue());
                            break;


                        case FORMULA:
                            textAtRowBuilder.append(cell.getCellFormula());
                            break;

                        case ERROR:
                            textAtRowBuilder.append(cell.getErrorCellValue());
                            break;

                        case _NONE:
                        case BLANK:
                        case STRING:
                            textAtRowBuilder.append(cell.getStringCellValue());
                            break;

                        default:
                            break;
                    }
                    textAtRowBuilder.append("\t;\t");
                }

                // Otestuji, zda náhodou nekončí řádek na střeník - ten pslední odeberu:
                if (textAtRowBuilder.toString().endsWith("\t;\t"))
                    textAtRowBuilder.setLength(textAtRowBuilder.toString().lastIndexOf(';'));


                // získaný řádek vložím do kolekce rpo vložení do editoru v apliakci:
                textFromFileList.add(textAtRowBuilder.toString());
            }

            workBook.close();

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka pří čtení Excelovského dokumentu: " + file.getAbsolutePath()
                        + ", třída v aplikaci: ReadWriteFile, metoda: readExcelFile.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }

        return textFromFileList;
    }









    @Override
    public boolean writeToExcelFile(final File file, final Object[][] data, final int listIndex,
                                    final String listName) {
        // Zkusím si načíst data z souboru - pokud ještě existuje:
        XSSFWorkbook workBook = getWorkBook(file);

        final XSSFSheet sheet;

        // Otestuji, zda původní soubor ještě existuje, pokud ne, pak bude proměnna workBook null, tak
        // musím vytvořit nový List, kam se data zapíšou

        // V podmínce musím ještě otestovat, zda je index listu menší než jejich počet, protože, pokud například
        // uživatel otevře soubor typu excel, na lokálním umístění ho smaže, apk ho pomocí aplikace někam uložit, zapíše
        // se do něj pouze ten jeden list, takže pokud pří prvním otevření otevřel například list 3,
        // pak je na špatném listu a musí se vytvořit nový - i když by v tomto měl být teotreticky jen jeden list,
        // ale mohl tam npříklad další vytvořit pomocí jiného programu.
        if (workBook != null && listIndex < workBook.getNumberOfSheets())
            sheet = workBook.getSheetAt(listIndex);


        else {
            // Zde již původní soubor neexistuje, tak musím vytvořit nová data:
            workBook = new XSSFWorkbook();
            sheet = workBook.createSheet(listName);
        }


        int rowCount = 0;

        for (final Object[] o1 : data) {
            final Row row = sheet.createRow(++rowCount);

            int columntCount = 0;

            for (final Object o2 : o1) {
                final Cell cell = row.createCell(++columntCount);

                cell.setCellValue(o2.toString());
            }
        }

        try (final FileOutputStream fos = new FileOutputStream(file)) {

            workBook.write(fos);

            fos.flush();

            return true;

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při zapisování do excelovského dokumentu: "
                        + file.getAbsolutePath() + ", třída v aplikaci: ReadWriteFile, metoda: writeToExcelFile.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        } finally {
            try {

                workBook.close();

            } catch (IOException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO, "Zachycena výjimka při zavírání'streamu' pro zápis do excelovského " +
                            "dokumentu: "
                            + file.getAbsolutePath() + ". Jedná se o zavření workBook (excelovksý dokument) v bloku " +
                            "finally.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
            }
        }

        return false;
    }








    @Override
    public boolean createNewListInExcelFile(final File file, final String listName) {
        // Zkusím si načíst data z souboru - pokud ještě existuje (mělo by být pokaždé):
        final XSSFWorkbook workBook = getWorkBook(file);

        if (workBook == null)
            return false;

        /*
         * Vytvořím si nový list s příslušným názvem.
         */
        final XSSFSheet sheet = workBook.createSheet(listName);

        int rowCount = 0;

        for (final Object[] o1 : new Object[][]{}) {
            final Row row = sheet.createRow(++rowCount);

            int columnCount = 0;

            for (final Object o2 : o1) {
                final Cell cell = row.createCell(++columnCount);

                cell.setCellValue(o2.toString());
            }
        }


        try (final FileOutputStream fos = new FileOutputStream(file)) {
            workBook.write(fos);

            workBook.close();
            fos.flush();

            return true;

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při vytváření nového listu v excelovském dokumentu: "
                        + file.getAbsolutePath() + ", list: " + listName);

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }

        return false;
    }










    /**
     * Metoda, která si načte požadovaný excelovsky dokument a vrátí jeho "něco jako takový přehled listů", ze kterého
     * si dále získám potřehný list, kam se mají data zapsaqt, ...
     *
     * @param file
     *         - soubor, ze kterého si vezmu potřebné informace pro zápis na požadovaný list.
     *
     * @return XSSFWorkbook, který obsahuje informace o listech v daném excelovském dokumentu, a na tyto informace budu
     * dále zapisovat data z editoru zeditované uživatelem, ... a nebo null, pokud se tento údaj nepodaří získat
     */
    private static XSSFWorkbook getWorkBook(final File file) {
        if (ReadFile.existsFile(file.getAbsolutePath())) {
            try (final FileInputStream fis = new FileInputStream(file)) {

                return new XSSFWorkbook(fis);

            } catch (IOException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při získvání hodnoty typu: XSSFWorkbook, kterou potřebuji abych mohl " +
                                    "zapsat data z editoru v aplikaci"
                                    + " na příslušný list, třída v aplikaci: ReadWriteFile, metoda: getWorkBook, " +
                                    "čtení souboru: "
                                    + file.getAbsolutePath());

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
            }
        }

        return null;
    }








    @Override
    public List<String> getSheetNames(final File file) {
        final List<String> sheetNameList = new ArrayList<>();

        try (final FileInputStream fis = new FileInputStream(file)) {
            final Workbook workBook = new XSSFWorkbook(fis);

            // Do následující proměnné si uložím počet listů v Excelovském dokumentu:
            final int countOfSheets = workBook.getNumberOfSheets();

            for (int i = 0; i < countOfSheets; i++)
                // do kolekce - listu si vložím název příslušné záložky:
                sheetNameList.add(workBook.getSheetName(i));

            // Uzavřu "proudy"
            workBook.close();

            // vrátím získaný list s názvy listů v příslušném excelu:
            return sheetNameList;

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka pří čtení souboru typu Excel za účelem zjištění názvů záložek v příslušném" +
                                " Excelovském dokumentu,"
                                + " třída v aplikaci: ReadWriteFile, metoda: getSheetNames. Excelovský dokument je na" +
                                " cestě: "
                                + file.getAbsolutePath());

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }

        return sheetNameList;
    }







    @Override
    public List<String> readPdfFile(final File file) {
        final List<String> textFromFileList = new ArrayList<>();
		
		// Tento kod take funguje (nalezeno pro starši knihovny pdfBox):
//		try {
//			final PDFParser parser = new PDFParser(new RandomAccessBufferedFileInputStream(file));
//			parser.parse();
//			
//			final COSDocument cosDoc = parser.getDocument();
//			final PDFTextStripper pdfStripper = new PDFTextStripper();
//			final PDDocument pdDoc = new PDDocument(cosDoc);
//			
//			pdDoc.getNumberOfPages();
//			pdfStripper.setStartPage(0);
//			pdfStripper.setEndPage(pdDoc.getNumberOfPages());
//
//			// Získám si text z pdf - ka
//			final String textFromFile = pdfStripper.getText(pdDoc);
//
//			// rozdělím ho dle řádků:
//			final String[] textArray = textFromFile.split("\n");
//
//			// a jednotlivé řádky vložím do kolekce a tu vrátím:
//			for (final String s : textArray)
//				textFromFileList.add(s);
//			
//		} catch (IOException e) {
//			System.out.println("Zachycena vyjímka při čtení PDF souboru, cesta: " + file.getAbsolutePath() + ", "
//					+ "třída: ReadWriteMicrocsftFile, metoda: readPdfFile");
//			e.printStackTrace();
//		}





        try {
            // Tento kód, resp. hned první řádek pro inicializaci promenné
            // document hází chybu, pokud použiji knihovnu PDfbox app 2.0.4,
            // Proto jsem použil starší verzi - 1.8.13 a je tetno kód bych chyby,
            // akorát s varováním

            // NEZAPOMENOUT PREPSAT INFO V LIB_PDFBOX !!!
            final PDDocument document = PDDocument.load(file);
            final String textFromPdfFile = new PDFTextStripper().getText(document);
            document.close();


            final String[] textArray = textFromPdfFile.split("\n");

            Collections.addAll(textFromFileList, textArray);

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informac text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při čtení souboru typu '.pdf', třída v aplikaci: ReadWriteFile.java, " +
                                "metoda: readPdfFile. Čtení souboru na cestě: "
                                + file.getAbsolutePath());

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }


        return textFromFileList;
    }
}