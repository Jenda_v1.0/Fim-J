package cz.uhk.fim.fimj.project_explorer;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Properties;

import org.fife.ui.rsyntaxtextarea.SyntaxConstants;

import cz.uhk.fim.fimj.code_editor_abstract.CodeEditorAbstract;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;

/**
 * Tato třída slouží jako editor zdrojového kódu, až na to, že nemusí jít pouze o zdrojový kód, ale tento editoru, který
 * bude zobrazen v dialogu pro průzkumník projektů, když uživatel otevře nějaký téměr libovolný soubor, tak se jeho kód
 * otevře v tomto editoru, kde budou k dispozici i mnou předdefinované zkratky pro doplňování kódu, ale už se nebudou
 * testovat metody, proměnné, apod. jako v tom druhém pro jedu třídu, protže soubory otevřené v tomto editoru nepůjdou
 * zkompilovat a navíc se nemusí jednat ani o Javovskou třídu a i kdyby se o ní jednalo, tak může mít reference na jiné
 * třídy z projektu, apod. a v takovém případě by se jí opět nepodařilo zkompilovat a získat soubor .class pro získání
 * potřebných hodnot pro doplnění do nápvědy.
 * <p>
 * Tato třída je obdoba Třídy: CodeEditor.java z balíčku: cz.uhk.fim.fimj.codeEditor; akorát zde nebude zmíněné načítání
 * proměnných, apod. prostě taková očesaná třída pouze pro otevírání a zobrazování obsahu souborů.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class CodeEditor extends CodeEditorAbstract implements LanguageInterface {

    private static final long serialVersionUID = -119876910512132591L;


    /**
     * Vlákno, které slouží pro aktualizaci dialogu pro autodoplňování, tj. že znovu zkompiluje a načte veškeré
     * proměnné, metody, ... z přísušné třídy a vloží je do zmíněného dialogu pro auto - doplňování.
     */
    private static RefreshAutoCompletionThread refreshAutoCompletionThread;





    /**
     * Konstruktor této třídy
     *
     * @param myFrame
     *         - reference na instanci třídy MyInternalFrame, protože zde, resp. v tomto editoru potřebuji reagovat na
     *         stisknutí kláves Ctrl + S, pro uložení tohoto konkrétního otevřeného souboru, a po stisknutí výše zmíněné
     *         kombinace kláves se zavolá metoda v myFrame pro uložení souboru
     * @param isOpenClass
     *         - logická proměnná o tom, zda je otevřena třída v otevřeném projektu, protože pokud ano, tak je možnost,
     *         že uživatel chce po stisknutí klávesy středníku nebo závorek spustit vlákno, které na pozadí zkompiluje
     *         třídy a přenačte jejich hodnoty do editoru
     * @param languageProperties
     *         - proměnny typu Properties, která obsahuje texty pro aplikaci ve zvoleném jazyce
     * @param projectExplorerDialog
     *         - reference na instanci třídy ProjectExplorerDialog, která je zde potřeba pro předání do vlákna na
     *         kompilaci a nastavení proměnných do auto - doplnování
     * @param codeEditorProp
     *         - objekt Properties, který obshuje veškeré nastavení pro editor (varvu písma, pozadí, ...).
     */

    public CodeEditor(final FileInternalFrame myFrame, final boolean isOpenClass, final Properties languageProperties,
                      final ProjectExplorerDialog projectExplorerDialog, final Properties codeEditorProp) {

        super(myFrame.getExtensionOfOpenedFile());


        // Nastavení typ jazyka pro zvýrazňování kódu a povolení zvýrazňování:
        setCodeFoldingEnabled(true);
        setAntiAliasingEnabled(true);


        // zvětšování a změnšování písma, resp. příbližování a oddalování: na rolování myší a držení CTRL
        addMyMouseWheelListener();


        // Při otevření editoru si ze souboru přečte nastavení:
        setSettingEditor(codeEditorProp, isOpenClass);


        setLanguage(languageProperties);









        /*
         * Note:
         * Pokud není otevřena třída, tak bych ani nemusel ten ten posluchač na
         * stisknutí klávesy přidávat, ale pouze u stisknutí klávesy testuji, zda se
         * mají zkompilovat třídyy a načíst potenciálně nové či nenačítat některé
         * proměnné apod.
         *
         * Je to takto napsáno, kdybych náhodou v budoucnu potřeboval reagovat při
         * uvolnení klávesy ještě na něco jiného apod.
         */

        addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(KeyEvent e) {
                // Zde otestuji, zda se má po stisknutí středníku nebo závorky spustit vlákno, které na pozadí
                // zkompiluje třídy a pokud se tak povede bez chyby, tak si načte jejich přeložené třídy
                // a do otevřených souborů - tříd načte příslušnou třídu do autodoplňování:
                if (e.getKeyCode() == KeyEvent.VK_SEMICOLON || e.getKeyCode() == KeyEvent.VK_CLOSE_BRACKET)
                    // Nyní otestuji, zda se mají třídy na pozadí kompilovat:
                    if (compiledClassInBackground) {
                        // Nejprve musím uložit všechna data z otevřených tříd, které je možné zkompilovat
                        projectExplorerDialog.saveDataInFramesWithClassFromProject();

                        // a na konec spustím vlákno, které zkusí třídy zkompilovat a případně přenačíst jejich auto
                        // - doplňování:
                        // Note:
                        // u adresáře src už ani nemusím testovat jeho existenci, protože se jedná o třídu, takže
                        // jsou balíčky získány,
                        // a už tam se testuje existence, dále pak v násleudjícím vlákně existence jednotlivých tříd
                        // se také testuje,
                        // tam by se to poznalo


                        /*
                         * Vlákno pro zkompilování a načtení hodnot do dialogu pro auto - doplňování
                         * spustím pouze v případě, že to vlákno ještě není vytvořeno nebo už neběží -
                         * doběhlo.
                         *
                         * Protože když to vlákno beží, tak by se zbytečně pouštělo další, což je k
                         * ničemu, toto může nastat například v případě, kdy uživatel opakované mačká
                         * klávesu pro kompilaci - středník nebo jednu ze zavíraích závorek.
                         */
                        if (refreshAutoCompletionThread == null || !refreshAutoCompletionThread.isAlive()) {
                            refreshAutoCompletionThread = new RefreshAutoCompletionThread(languageProperties, new File(
                                    projectExplorerDialog.getFileProjectDir().getAbsolutePath() + File.separator + "src"),
                                    projectExplorerDialog, false);

                            refreshAutoCompletionThread.start();
                        }
                    }
            }
        });
    }
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která si přečte příslušný soubor, pokud existuje pokud soubor
	 * neexistuje, použije se výchozí nastavení
	 * 
	 * ze souboru CodeEditor.properties si přečte výchozí nastavení pro tento editor
	 * 
	 * Metoda e zavolá, akorát při vytvoření instance této třídy - editoru - neboli
	 * když uživatel otevře zdrojový kód třídy z diagramu tříd
	 * 
	 * Note:
	 * Metoda je stejná jako ta v CodeEditor v dialogu editor zdrojového kodu, ale
	 * původně nebyla, tak by mohla být v předkovi, například kdybych změnil nějaké
	 * proměnné, nějaké doplnil, smzatl, upravil hodnoty či názvy apod.
	 * 
	 * @param codeEditorProp
	 *            - proměná typu Properties, která nese hodnoty pro nastavení
	 *            editoru, resp. hodnoty s nastavením třeba barvy písma, pozadí, ...
	 *            více v metodě v jednotlých nastaveních
	 * 
	 * 
	 * @param isOpenClass
	 *            - logická proměnná o tom, zda se má nastavit logická proměnná
	 *            compiledClassInBackground, promenna isOpenClass značí akorát to,
	 *            zda se má nastavit logická promenná compiledClassInBackground,
	 *            která určuje, zda se mají na pozadí kompilovat třídy po stisknutí
	 *            stredniku nebo zavorek, pokud není otevřena trída, tak bude
	 *            proměnná IsIpenClass false a proměnna compiledClassInBackground se
	 *            nastaví také na false, protože když není ani otevřena třída nebo
	 *            projekt, tak nelze nic kompilovat
	 */
	private void setSettingEditor(final Properties codeEditorProp, final boolean isOpenClass) {
		
		if (codeEditorProp != null) {
			// navstím promennou compiledClassInBackground bud na nastavenou hodnotu (pokud se ji podari ziskat) nebo na
			// výchozí hodnotu zadanou v Constants, 
			if (isOpenClass)
				// Zde je otevřena nějaká třída, tak mohu načíst proměnnou, zda se mají na pozadí třídy kompilovat a 
				// načítat kůvli nápovědě
				compiledClassInBackground = Boolean.parseBoolean(codeEditorProp.getProperty("CompileClassInBackground",
						String.valueOf(Constants.CODE_EDITOR_COMPILE_CLASS_IN_BACKGROUND)));

			// zde není otevřena třída, tak se ani nemohou kompilovat třídy, ani nemusí být
			// otevřena, tak nebudu nic kompilovat:
			else
				compiledClassInBackground = false;
			
			
			// Zde se padařilo načíst soubor bud s výchozím nastavením nebo ve workspace,
			// Tak nastavím hodnoty do editoru v app:
			final int fontStyle = Integer.parseInt(codeEditorProp.getProperty("FontStyle", Integer.toString(Constants.DEFAULT_FONT_FOR_CODE_EDITOR.getStyle())));
			final int fontSize = Integer.parseInt(codeEditorProp.getProperty("FontSize", Integer.toString(Constants.DEFAULT_FONT_FOR_CODE_EDITOR.getSize())));
			
			setFont(new Font(Constants.DEFAULT_FONT_FOR_CODE_EDITOR.getName(), fontStyle, fontSize));
			
			
			final String bgColor = codeEditorProp.getProperty("BackgroundColor", Integer.toString(Constants.CODE_EDITOR_BACKGROUND_COLOR.getRGB()));
			setBackground(new Color(Integer.parseInt(bgColor)));
			
			
			final String foregroundColor = codeEditorProp.getProperty("ForegroundColor", Integer.toString(Constants.CODE_EDITOR_FOREGROUND_COLOR.getRGB()));
			setForeground(new Color(Integer.parseInt(foregroundColor)));
			
			
			
			// Zde se zjistí, zda se má využít některý syntaxe dle přípon souborů:
			final boolean highlightCodeByKindOfFile = Boolean.parseBoolean(codeEditorProp.getProperty("HighlightingCodeByKindOfFile", String.valueOf(Constants.CODE_EDITOR_HIGHLIGHTING_CODE_BY_KIND_OF_FILE)));
			
			/*
			 * Zde otestuji, zda se má nastavit zvýrazňování dle přípon souborů, pokud ano,
			 * tak jej nastavím a pokud ne, pak nastavím výchozí styl zvýrazňování - Java.
			 */
			if (highlightCodeByKindOfFile)
				setHighlightingCodeByKindOfFile(extensionOfOpenFile);
			else
				setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
			
			
			final boolean highlightingCode = Boolean.parseBoolean(codeEditorProp.getProperty("HighlightingCode", String.valueOf(Constants.CODE_EDITOR_HIGHLIGHTING_CODE)));
			
			// Pokud se nemá zvýrazňovat syntaxe, pak jej vypnu:
			if (!highlightingCode)
				// Zde se nebude zvýrazňovat kód - text otevřeného souboru:
				setSyntaxEditingStyle(null);
		}
		
		
		else {
			// Zde použiji výchozí nastavení:
			if (isOpenClass)
				compiledClassInBackground = Constants.CODE_EDITOR_COMPILE_CLASS_IN_BACKGROUND;
			
			else
				compiledClassInBackground = false;
				
			
			setFont(Constants.DEFAULT_FONT_FOR_CODE_EDITOR);
			
			setBackground(Constants.CODE_EDITOR_BACKGROUND_COLOR);
			
			setForeground(Constants.CODE_EDITOR_FOREGROUND_COLOR);
			
			
			/*
			 * Zde otestuji, zda se má nastavit zvýrazňování dle přípon souborů, pokud ano,
			 * tak jej nastavím a pokud ne, pak nastavím výchozí styl zvýrazňování - Java.
			 */
			if (Constants.CODE_EDITOR_HIGHLIGHTING_CODE_BY_KIND_OF_FILE)
				setHighlightingCodeByKindOfFile(extensionOfOpenFile);
			else
				setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);

			
			if (!Constants.CODE_EDITOR_HIGHLIGHTING_CODE)
				setSyntaxEditingStyle(null);
		}

        // Vytvoření objektu pro formátování zdrojového kódu:
        createFormatter(codeEditorProp);
    }
	
	
	

	
	
	@Override
	public void setLanguage(final Properties properties) {
        setFulfillmentVarForCodeComment(properties);
	}
}