package cz.uhk.fim.fimj.project_explorer;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.forms.SourceDialogForm;

/**
 * Třída lsoužýí jako dialogu pro výběr Listu z excelovského dokumentu, který se má načíst do editoru - do příslušného
 * vnitřníhookna
 * <p>
 * Tato třída slouží jako formulář - dialog, který bude obsahovat pouze jeden JComboBox, který bude obsahovat získané
 * názvy listu z excelovského dokumentu a uživatel si vybere, který list se má otevřít, resp. text z jakého listu z
 * příslušného excelovského dokumentu se má načíst do editoru v okně
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class GetIndexOfExcelSheetForm extends SourceDialogForm implements LanguageInterface, ActionListener {

	private static final long serialVersionUID = 1L;


	/**
	 * Label s informacemi o tom, ze si uzivatel ma vybrat list, který chce otevrit:
	 */
	private final JLabel lblInfo;

	
	/**
	 * Komponenta JComboBox, která bude obsahovat získané názvy listů z excelu a uživatel si
	 * jeden z nich vybere, a já si z něj zjistím index:
	 */
	private final JComboBox<String> cmbSheetNames;
	

	
	/**
	 * Konstruktor této třídy, který obsahuje proměnnou typu List s názvy listu
	 * 
	 * @param sheetNamesList
	 *            - proměnná typu List Stringů, která obsahuje názvy listů v
	 *            excelovském souboru a uživatel si jeden vybere, a ten se otevře a
	 *            zároveň se z něj zjistí index toho listu.
	 */
	public GetIndexOfExcelSheetForm(final List<String> sheetNamesList) {
		super();
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/projectExplorerIcons/GetIndexOfExcelSheetFormIcon.png",
				new Dimension(450, 200), true);
		
		
		gbc = createGbc();
		
		index = 0;
		
		
		
		// vytvoření a přidání do okna labelu s informací o otevření
		lblInfo = new JLabel();
		setGbc(index, 0, lblInfo);
		
		
		
		
		// usím transformovat data z listu do pole následující způsobem:
		final String[] arrayModel = new String[sheetNamesList.size()];
		
		for (int i = 0; i < sheetNamesList.size(); i++)
			arrayModel[i] = sheetNamesList.get(i);
		
		
		// vytvořím komponentu s texty:
		cmbSheetNames = new JComboBox<>(arrayModel);
		cmbSheetNames.setSelectedIndex(0);
		
		setGbc(index, 1, cmbSheetNames);
		
		
		
		
		
		
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		btnOk = new JButton();
		btnCancel = new JButton();
		
		btnOk.addActionListener(this);
		btnCancel.addActionListener(this);
		
		pnlButtons.add(btnOk);
		pnlButtons.add(btnCancel);
		
		gbc.gridwidth = 2;
		setGbc(++index, 0, pnlButtons);
		
		
		
		
		
		addWindowListener(getWindowsAdapter());
		
		
		
		
		
		pack();
		setLocationRelativeTo(null);
	}
	
	
	

	
	
	/**
	 * Metoda, která zobrazí dialog, tím se stane modální a pak pokud klikne
	 * uživatel na tlačítko OK nebo zrušit se buď vrátí -1 (v případě tlačítka
	 * zrušit) nebo index listu, který se má vrátit
	 *
	 * @return index označeného listu.
	 */
	final int getSelectedSheet() {
		setVisible(true);
		
		if (variable)
			return cmbSheetNames.getSelectedIndex();
		
		return -1;
	}
	
	
	
	

	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			setTitle(properties.getProperty("Pe_Gioesf_DialogTitle", Constants.PE_GIESF_DIALOG_TITLE));
			
			lblInfo.setText(properties.getProperty("Pe_Gioesf_LblInfo", Constants.PE_GIESF_LBL_INFO) + ": ");
			
			btnOk.setText(properties.getProperty("Pe_Gioesf_BtnOK", Constants.PE_GIESF_BTN_OK));
			btnCancel.setText(properties.getProperty("Pe_Gioesf_BtnCancel", Constants.PE_GIESF_BTN_CANCEL));
		}
		
		
		else {
			setTitle(Constants.PE_GIESF_DIALOG_TITLE);
			
			lblInfo.setText(Constants.PE_GIESF_LBL_INFO + ": ");
			
			btnOk.setText(Constants.PE_GIESF_BTN_OK);
			btnCancel.setText(Constants.PE_GIESF_BTN_CANCEL);
		}
	}







	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnOk)
				// Zde není co řešit, resp. co testovat, už jako výchozí hodnotu jsem oznašil první,
				// resp. nultou položku, takže minimálně ta první bude označena, tak stačí vrátit její index
				setVarForClose(true);
			
			
			else if (e.getSource() == btnCancel)
				setVarForClose(false);
		}
	}
	
	
	
	
	
	/**
	 * Metoda, která nastaví logickou proměnnou variable, která drží hodnotu o tom,
	 * zda se kliklo na tlačítko OK nebo jiná tlačítka pro zavření - zrušit,
	 * případně, klávesy, apod.
	 * 
	 * @param var
	 *            - logická hodnota na kterou se má nastavit proměnná variable
	 */
	private void setVarForClose(final boolean var) {
		variable = var;
		dispose();		
	}
}