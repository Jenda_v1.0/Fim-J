package cz.uhk.fim.fimj.project_explorer;

import java.awt.Component;
import java.io.File;

import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.TreeCellRenderer;

/**
 * Tato třída slouží jako "vykreslovač" pro Jtree. Pomocí této třídy nastavím různé ikony různým objektům ve stromu,
 * něčemu nastavím ikonu pro adresář, něčemu pro obrázek, pro třídu, ...
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class TreeStructureCellRenderer implements TreeCellRenderer {

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
                                                  boolean leaf, int row, boolean hasFocus) {

        /*
         * Do následující proměnné si vložím text, který byl načten a předtavuje cestu k
         * nějakému spouboru či adresáři:
         */
        final String text = value.toString();




        /*
         * Vytvořím si proměnnou typu File, která bude ukazovat na získanou cestu:
         * a otestuji, zda se jedná o adresář nebo soubor,
         * dle toho nastavím příslušnou ikonu
         */
        final File file = new File(text);




        /*
         * Vytvořím si Jlabel, kterému nastavím ikonu a text coby nějaký soubor na disku, který
         * reprezentuje:
         */
        final JLabel label = new JLabel(file.getName());


        // Nastaqvím ikonu labelu, který se bude vykreslovat ve stromové strukturře:
        label.setIcon(ProjectExplorerIconInterface.getIcon(file));


        return label;
    }
}