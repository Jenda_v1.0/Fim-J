package cz.uhk.fim.fimj.project_explorer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;

/**
 * Tato třída slouží jako dialog pro zadání návzu excelovského listu, který se má vytvořit v nějakém novém Excelovském
 * souboru po kliknutí v průzkuniku projektů na nějaký adresář, zvolené novy a vybere se soubor typu Excel - přípona
 * .xlsx
 * <p>
 * Note: Tento dialog ani není nezbytně nutný, lze použít například pomocí metody JOptionPane.showInputDialog prozadaání
 * pouhého návzu, ale už bych nezjitil, zda je text zadán správně - syntakticky, i když by bylo možné tento název
 * otestovat, po jeho zadání, ale pokud by se neshodoval, tak bych musel opakovaně otevírat tento dialog, a navíc pomocí
 * tohoto "mého" dialogu mohu uživteli sdělit více informací ohledně názvu, apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class NewExcelListNameForm extends NameFormAbstract implements ActionListener, LanguageInterface {

	private static final long serialVersionUID = 1L;


	/**
	 * Konstruktor této třídy.
	 */
	public NewExcelListNameForm() {
		super();
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/projectExplorerIcons/NewExcelListNameForm.png",
				new Dimension(500, 200), true);
		
		
		gbc = createGbc();
		
		index = 0;
		
		
		
		lblInfo = new JLabel("", JLabel.CENTER);
		setGbc(index, 0, lblInfo);
		
		
		
		lblError = new JLabel("", JLabel.CENTER);
		lblError.setFont(ERROR_FONT);
		lblError.setForeground(Color.RED);
		setGbc(++index, 0, lblError);
		lblError.setVisible(false);
		
		
		
		
		txtName = new JTextField("ListName", 25);
		txtName.addKeyListener(getMyKeyAdapter());
		setGbc(++index, 0, txtName);
		
		
		
		
		
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		btnOk = new JButton();
		btnCancel = new JButton();
		
		btnOk.addActionListener(this);
		btnCancel.addActionListener(this);
		
		pnlButtons.add(btnOk);
		pnlButtons.add(btnCancel);
		setGbc(++index, 0, pnlButtons);
		
		
		
		
		
		pack();
		setLocationRelativeTo(null);
	}
	
	

	
	
	/**
	 * Metoda, která zobrazí tento dialog a tím ho udělá modálním, dále čeká na
	 * kliknutí, resp. potvrzení dialogu nabo jeho zavření, pak se dle nastavené
	 * proměné variable buď vrátí text coby název listu z textového pole, jinak se
	 * vrátí null, pokud uživatel dialog zavře
	 * 
	 * @return pokud je název syntakticky v pořádku, tak tento text z textového
	 *         pole, jinak null, pokud se dialog zavře
	 */
	final String getExcelListName() {
		setVisible(true);
		
		if (variable)
			return txtName.getText().trim();
		
		return null;
	}

	
	
	

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnOk)
				testData();
			
			else if (e.getSource() == btnCancel) {
				variable = false;
				dispose();
			}
		}
	}



	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		// zavolám metodu, která naplní zbývající proměnné:
		fillTextVariables(properties);

		if (properties != null) {
			setTitle(properties.getProperty("Pe_Nelnf_DialogTitle", Constants.PE_NELNM_DIALOG_TITLE));

			lblInfo.setText(properties.getProperty("Pe_Nelnf_LblInfo", Constants.PE_NELNM_LBL_INFO) + ":");
		}

		else {
			setTitle(Constants.PE_NELNM_DIALOG_TITLE);

			lblInfo.setText(Constants.PE_NELNM_LBL_INFO + ":");
		}
	}
}