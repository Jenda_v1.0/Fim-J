package cz.uhk.fim.fimj.redirect_sysout;

import java.io.OutputStream;
import java.util.Arrays;

/**
 * Třída, která slouží pro přesměrování výstupů v System.out do příslušného textového pole ve výstupním terminálu.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 20.07.2018 11:03
 */

public class SysOut extends Redirect {

    /**
     * Konstruktor třídy.
     *
     * @param out
     *         - stream pro výpis
     */
    SysOut(final OutputStream out) {
        super(out);
    }



    @Override
    protected void checkRedirectedText(final String text) {
        printRedirectedText(text, false);
    }




    @Override
    public void println(String x) {
        checkRedirectedText(x);
    }

    @Override
    public void println(Object x) {
        checkRedirectedText(x.toString());
    }

    @Override
    public void println() {
        checkRedirectedText("");
    }

    @Override
    public void println(boolean x) {
        checkRedirectedText(String.valueOf(x));
    }

    @Override
    public void println(char x) {
        checkRedirectedText(String.valueOf(x));
    }

    @Override
    public void println(char[] x) {
        checkRedirectedText(Arrays.toString(x));
    }

    @Override
    public void println(double x) {
        checkRedirectedText(String.valueOf(x));
    }

    @Override
    public void println(float x) {
        checkRedirectedText(String.valueOf(x));
    }

    @Override
    public void println(int x) {
        checkRedirectedText(String.valueOf(x));
    }

    @Override
    public void println(long x) {
        checkRedirectedText(String.valueOf(x));
    }

    @Override
    public void print(boolean b) {
        checkRedirectedText(String.valueOf(b));
    }

    @Override
    public void print(char[] s) {
        checkRedirectedText(Arrays.toString(s));
    }

    @Override
    public void print(long l) {
        checkRedirectedText(String.valueOf(l));
    }

    @Override
    public void print(int i) {
        checkRedirectedText(String.valueOf(i));
    }

    @Override
    public void print(char c) {
        checkRedirectedText(String.valueOf(c));
    }

    @Override
    public void print(double d) {
        checkRedirectedText(String.valueOf(d));
    }

    @Override
    public void print(float f) {
        checkRedirectedText(String.valueOf(f));
    }

    @Override
    public void print(Object obj) {
        checkRedirectedText(obj.toString());
    }

    @Override
    public void print(String s) {
        checkRedirectedText(s);
    }
}
