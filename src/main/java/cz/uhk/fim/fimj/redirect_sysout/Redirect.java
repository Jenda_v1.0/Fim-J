package cz.uhk.fim.fimj.redirect_sysout;

import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Třída, která slouží pro výpis textů pomocí metod v System.out a System.err do konzole - do jednoho z polí ve
 * výstupním terminálu.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 20.07.2018 10:50
 */

public abstract class Redirect extends PrintStream {

    /**
     * Konstruktor třídy.
     *
     * @param out
     *         - stream pro výpis
     */
    Redirect(final OutputStream out) {
        super(out, true);
    }


    /**
     * Metoda, která slouží pro výpis textů do příslušných textových polí. Dle proměnné isErr se pozná, kam se má text
     * (text) vypsat - do jakého textového pole.
     * <p>
     * Pokud je isErr false, pak se text vypíše do textového pole pro výpis "klasických" textů do konzole (System.out)
     * <p>
     * Pokud je isErr true, pak se text vypíše do textového pole pro výpis "chybových" textů do konzole (System.err)
     *
     * @param text
     *         - text, který se má vypsat do textového pole - nějaký výpis od uživatele.
     * @param isErr
     *         - proměnná, která značí, do jakého textového pole se má text vypsat. True - do pole pro výpis System.err,
     *         false - do pole pro výpis System.out
     */
    static void printRedirectedText(final String text, final boolean isErr) {
        /*
         * Zde otestuji, zda náhodou není vytvořené okno pro přesměrování výpisů. Tato možnost by nikdy nastat
         * neměla, ale kdyby
         * se náhodou okno zavřelo / ukončilo z nějakého mmě neznámého důvodu, tak aby nenastala výjimka, kterou by
         * nemylo možné opravit za běhu aplikace.
         */
        if (RedirectApp.getOutputFrame() == null)
            RedirectApp.createOutputFrame();

        // Rozhodnutí, do kterého textového pole se má text vypsat:
        if (isErr)
            RedirectApp.getOutputFrame().appendSysErrText(text);

        else {
            /*
             * Zde se jedná o výpis pomocí metod v System.out.
             *
             * Nejprve otestuji, zda se má vymazat textové pole při zavolání metody nebo konstruktoru, případně
             * alespoň jedno z toho, pokud ano, vyprázdním textové pole.
             *
             * Poté otestuji, zda se mají vypisovat hlavičky metod nebo konstruktorů, aby uživatel viděl, v jaké
             * metodě nebo konstruktoru mohlo dojít k výpisu.
             *
             * Poté se vypíše samotný text v System.out.
             *
             * Poté musím nastavit hodnotu calledMethod na null, protože se texty do výstupního terminálu vypisují po
              * řádcích, tak kdybych toto neudělal,
             * tak by se při dalším výpisu, resp. při výpisu dalšího řádku by se znovu vypsali ty hodnoty, popř.
             * vymazal text.
             *
             * Hodnotu calledMethod nastavit na null by bylo možné ještě ve třídě: cz.uhk.fim.fimj.instances
             * .OperationsWithInstances, kde
             * se tato proměnné získává svou hodnotu (u jednotlivých volání metod a konstruktorů). Ale to už není
             * potřeba, protože když se ve výše uvedené třídě
             * nastaví tato proměnné, a nedojde k žádnému výpisu, tak to nevadí, protože k dylšímu výpisu může dojít
             * až při dalším zavolání metody nebo konstruktoru.
             * Dále pokud by nastala při volání metody nebo konstruktoru nějaká chyba, tak to také nevadí, že v té
             * proměnné calledMethod zůstane
             * hodnota, protože se při dalším zavolání metody nebo konstruktoru přepíše na aktuální hodnotu a až s
             * tou se zde bude případně vypisovat hlavička apod.
             */

            // Zda se podařilo uložit zavolanou metodu nebo konstruktor:
            final CalledMethod calledMethod = RedirectApp.getCalledMethod();


            /*
             * Pokud se má textové pole pro výpis textů z metod z System.out vymazat / vyprázdnit (/ odstranit text)
             * před každým zavoláním metoda nebo konstruktoru - nebo jedno z toho, tak to zde zjistím a případně
             * vymažu příslušné textové pole, poté už se zapíšou případně nově hlavičky metody nebo konstruktoru a
             * poté text, který se má vypsat do konzole.
             */
            if (calledMethod != null && ((OutputFrame.isClearTextBeforeCallMethod() && calledMethod.getMethod() !=
                    null) || (OutputFrame.isClearTextBeforeCallConstructor() && calledMethod.getConstructor() != null)))
                OutputFrame.getTxtAreaSysOut().setText(null);


            // Zda se mají vypisovat hlavičky metod nebo konstruktorů:
            if (calledMethod != null && ((OutputFrame.isRecordCallMethods() && calledMethod.getMethod() != null) ||
                    (OutputFrame.isRecordCallConstructors() && calledMethod.getConstructor()
                            != null))) {
                RedirectApp.getOutputFrame().appendSysOutText(calledMethod.toString());
                RedirectApp.setCalledMethod(null);
            }

            // Výpis textu do konzole z System.out:
            RedirectApp.getOutputFrame().appendSysOutText(text);
        }

        // Pokud uživatel zavřel okno, tak jej zde zobrazím:
        RedirectApp.markOutputFrame();
    }


    /**
     * Metoda, kteroui implementují potomti této třídy. Jedná se o metodu, ve které se definují parametry pro výše
     * uvedenou metodu cz.uhk.fim.fimj.redirect_sysout.Redirect#printRedirectedText(java.lang.String, boolean).
     *
     * @param text
     *         - text, který se má vypsat, případně ještě přefiltrovat texty a poté vypsat do textového pole pro výpis
     *         uživateli.
     */
    protected abstract void checkRedirectedText(final String text);
}
