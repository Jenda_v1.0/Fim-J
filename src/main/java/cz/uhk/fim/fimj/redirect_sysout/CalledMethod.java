package cz.uhk.fim.fimj.redirect_sysout;

import cz.uhk.fim.fimj.commands_editor.MakeOperation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Třída, která slouží pro předávání informací o tom, jakou metodu nebo konstruktor uživatel zavolal a převod této
 * hodnoty do podoby textu.
 * <p>
 * Jedná se o to, že jsem zpřístupnil uživateli možnost, že si ve výstupním terminálu může nastavit, zda se mají nebo
 * nemají vypisovat hlavičky konstruktorů a metod, které uživatel zavolal a došlo v nich k výpisu pomocí metod v
 * System.out.
 * <p>
 * Pokud jsou tyto podmínky splněny, tedy uživatel zavolal metodu nebo konstruktor, kde se nachází výpis v metodě v
 * System.out, pak se zjistí, zda uživatel povolil v okně výstupní terminál výpis těchto hodnot a pokud ano, tak se
 * nejprve vypíše hlavička konstruktoru nebo metody a až potom text v System.out.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 20.07.2018 21:15
 */

public class CalledMethod {

    /**
     * Proměnná definuje výchozí hodnotu pro případ, že by byl konstruktor nebo metoda null. Tato varianta by ale nastat
     * neměla, nebo by neměla být využita, proto nebudu tuto hodnotu překládat apod.
     * <p>
     * Jde o to, že například při zavolání výchozího konstruktoru v editoru příkazů se například nepodaří získat výchozí
     * konstruktor, tak se zde předá null místo konstruktoru, v takovém případě to ale nemusím řešit, protože by se
     * takový konstruktor neměl ani zavolat nebo by mělo dojít k nějaké chybě. Proto zde nechám tuto hodnotu, kdyby k
     * tomu náhodou došlo.
     */
    private static final String UNKNOWN = "Unknown";

    /**
     * Uživatelem zavolaný konstruktor.
     */
    private final Constructor constructor;

    /**
     * Uživatelem zavolaná metoda.
     */
    private final Method method;

    /**
     * Proměnná, do které se vloží text zavolané metody nebo konstruktoru. Resp. zvolená část metody nebo konstruktoru,
     * která se v případě potřeby vypíše uživateli do okna výstupního terminálu.
     */
    private final String methodInText;


    /**
     * Konstruktor pro naplnění zavolaného konstruktoru a jeho převedení do podoby textu, aby se mohl vypsat v okně
     * výstupní terminál (pokud je tak nastaveno).
     *
     * @param constructor
     *         - uživatelem zavolaný konstruktor.
     * @param parameters
     *         - parametry konstruktoru
     */
    public CalledMethod(final Constructor constructor, final Object... parameters) {
        this.constructor = constructor;
        method = null;

        if (constructor != null)
            methodInText = createTextOfConstructor(constructor, parameters);
        else methodInText = UNKNOWN;
    }

    /**
     * Konstruktor pro naplnění zavolané metody a její převedení do podoby textu, aby se mohla vypsat v okně výstupní
     * terminál (pokud je tak nastaveno).
     *
     * @param method
     *         - uživatelem zavolaná metoda.
     * @param parameters
     *         - parametry metody.
     */
    public CalledMethod(final Method method, final Object... parameters) {
        this.method = method;
        constructor = null;

        if (method != null)
            methodInText = createTextOfMethod(method, parameters);
        else methodInText = UNKNOWN;
    }


    /**
     * Metoda, která sestaví text z metody a konkrétních parametrů.
     * <p>
     * Bude se jednat o syntaxi: (packages.)název_metody(hodnoty parametrů);
     *
     * @param method
     *         - metoda, která se převedena text.
     * @param parameters
     *         - zadané parametry metody, které se také převedou na text.
     *
     * @return metoda method ve výše uvedené syntaxi.
     */
    private static String createTextOfMethod(final Method method, final Object... parameters) {
        // Metoda bude v sytaxi: název_metody(její parametry - jsou li);

        // Objekt pro sestavení textu z metody:
        final StringBuilder builder = new StringBuilder();

        // Přidám text třídy před metodu (název třídy s balíčky, ve kterých se nachází):
        builder.append(method.getDeclaringClass().getName());

        // Přidám hashtag pro oddělení tříd / třídy a metody:
        builder.append("#");

        // Přidání názvu metody a závorky:
        builder.append(method.getName()).append("(");

        /*
         * Pokud nejsou žádné parametry, pak mohu přidat uzavírací závorku a středník a text vrátit.
         */
        if (parameters == null || parameters.length == 0) {
            builder.append(");");
            return builder.toString();
        }


        // Získám si parametry v podobě textu:
        builder.append(getParametersInText(parameters));

        builder.append(");");

        return builder.toString();
    }


    /**
     * Metoda, která sestaví text z konstruktoru a konkrétních parametrů.
     * <p>
     * constructor = className
     * <p>
     * Bude se jednat o syntaxi: (packages.)constructor(hodnoty parametrů);
     *
     * @param constructor
     *         - konstruktor, který se převede do podoby textu.
     * @param parameters
     *         - parametry konstruktoru, které se také převedoudo podoby textu.
     *
     * @return konstruktor v podobě textu ve výše uvedené syntaxi.
     */
    private static String createTextOfConstructor(final Constructor<?> constructor, final Object... parameters) {
        // Konstruktor bude v syntaxi: (packages.)ClassName(parametry - jsou li);

        // Objekt pro sestavení textu z konstruktoru:
        final StringBuilder builder = new StringBuilder();


        // Přidám název metody:
        builder.append(constructor.getName());

        builder.append("(");

        /*
         * Pokud nejsou žádné parametry, pak mohu přidat uzavírací závorku a středník a text vrátit.
         */
        if (parameters == null || parameters.length == 0) {
            builder.append(");");
            return builder.toString();
        }


        // Získám si parametry v podobě textu:
        builder.append(getParametersInText(parameters));

        builder.append(");");

        return builder.toString();
    }


    /**
     * Metoda, která převede parametry (parameters) do podoby textu.
     *
     * @param parameters
     *         - parametry zavolané metody nebo konstruktoru, které se převedou do podoby textu.
     *
     * @return paramery v podobě textu oddělené desetinnou čárkou.
     */
    private static String getParametersInText(final Object... parameters) {
        final StringBuilder builder = new StringBuilder();

        /*
         * Zde projdu veškeré parametry a pro každý parametr přidám do builderu, akorát si musím dát pozor, protože
         * pole je jediný datový typ
         * proměnné, který je třeba zvlášť převést na text. Ostaní datové typy již mají implementovanou metodu
         * toString, ale v případě pole je třeba
         * zjistit, zda se jedná o jedno nebo více - rozměrné pole a dle toho jej převést na text.
         */
        Arrays.stream(parameters).forEach(p -> {
            if (p != null && p.getClass().isArray()) {
                // Převedení pole do textové podoby:
                builder.append(MakeOperation.fromArrayToString(p));

                builder.append(", ");

                return;
            }

            builder.append(p);

            builder.append(", ");
        });

        /*
         * V této metodě vím, že v parameters je alespoň jedna položka, takže mohu odebrat poslední dva znaky -
         * desetinná čárka s mezerou:
         */
        builder.setLength(builder.length() - 2);

        return builder.toString();
    }


    public Constructor getConstructor() {
        return constructor;
    }

    public Method getMethod() {
        return method;
    }

    @Override
    public String toString() {
        return methodInText;
    }
}
