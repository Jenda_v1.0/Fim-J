package cz.uhk.fim.fimj.redirect_sysout;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.ReadFile;

import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 * Třída, která slouží pro přesměrování výstupů v System.err do příslušného textového pole ve výstupním terminálu.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 20.07.2018 11:03
 */

public class SysErr extends Redirect {

    /**
     * True značí o tom, že se mají vypisovat výjimky, které nastanou v aplikaci a nepodaří se je zachytit, aby se
     * vypsali do logu.
     * <p>
     * Resp. jedná se o nezachytávané výjimky.
     * <p>
     * Hodnota false značí, že se nemají vypisovat nastavelé výjimky v aplikaci, tedy budou se filtrovat dle
     * rozpoznaného textu.
     */
    private final boolean writeException;


    /**
     * List, který obsahuje "části" výjimek, které když nastanou, tak se nemají vypisovat.
     * <p>
     * Jedná se pouze o "některé" případy, které i občas nastaly, nebo mohou nastat.
     * <p>
     * Note:
     * <p>
     * Na konec listu jsem se rozhodl doplnit text: 'java', tj., že když nastane výjimka, tak ve většíně případů bude
     * obsahovat tento text, například at java.lang at javax. ... Toto není nejlepší řešení, ale zabráním tak většíne
     * "klasických" výjimek aby se vypsaly, případně alespoň větší část, například když ten stackTrace bude obsahovat i
     * knihovnu apod. Tak ta se vypíše.
     * <p>
     * Pojem Java se může například nacházet i v nějakém výpisu od uživatele, pak se také nevypíše. Ale to bude nejspíše
     * minimum textu (když už).
     */
    private static final List<String> EXCEPTIONS_LIST = Collections.unmodifiableList(Arrays.asList(
            "Exception in thread \"AWT-EventQueue-0\"", "java.lang.NullPointerException", "at org.jgraph.",
            "at javax.swing.", "at java.security.", "at java.awt.", "at cz.uhk.fim.fimj.",
            "java.awt.geom.IllegalPathStateException", "java.lang.IndexOutOfBoundsException:", "at java.util.",
            "java.awt.IllegalComponentStateException:", "at sun.awt.", "java.lang.ArrayIndexOutOfBoundsException",
            "at java.lang.", "java.lang.ClassCastException:", "java.lang", "java", "at sun.reflect",
            // Dále, když text bude obsahovat nějakou z následujících výimek, tak se také
            // nevypíše:
            "ArithmeticException", "ArrayIndexOutOfBoundsException", "ArrayStoreException", "ClassCastException",
            "IllegalArgumentException", "IllegalMonitorStateException", "IllegalStateException",
            "IllegalThreadStateException", "IndexOutOfBoundsException", "NegativeArraySizeException",
            "NullPointerException", "NumberFormatException", "SecurityException", "StringIndexOutOfBounds",
            "UnsupportedOperationException", "ClassNotFoundException", "CloneNotSupportedException",
            "IllegalAccessException", "InstantiationException", "InterruptedException", "NoSuchFieldException",
            "NoSuchMethodException", "StackOverflowError"));


    /**
     * Konstruktor třídy.
     *
     * @param out
     *         - stream pro výpis
     */
    SysErr(final OutputStream out) {
        super(out);


        /*
         * Načtu si soubor Default.properties z adresáře workspace, který obsahuje
         * uživatelem zvolené nastavení toho, zda se mají nebo nemají vypisovat, resp.
         * filtrovat vypisované texty (kvůli výpisu výjimek) do dialogu výstupního terminálu.
         *
         * Pokud se uvedený soubor nepodaří načíst z adresáře workspace, tak se načte z
         * adresáře v aplikaci (to by mělo nastat vždy, pokud jej úmyslně před
         * vygenerováním aplikace někdo nesmaže). Pokud se v tom souboru nebude
         * příslušná proměnná nacházet (němělo by nastat) využije se výchozí nastavení z
         * Constants.
         */
        final Properties defaultProp = ReadFile.getDefaultPropAnyway();

        writeException = Boolean.parseBoolean(defaultProp.getProperty("Write not captured exceptions",
                String.valueOf(Constants.WRITE_NOT_CAPTURED_EXCEPTIONS_TO_OUTPUT_TERMINAL)));
    }



    @Override
    protected void checkRedirectedText(final String text) {
        /*
         * Pro začátek zde otestuji, zda se mají filtrovat nastalé výjimky, pokud ano,
         * pak při každém textu pro vypsání otestuji, zda ten text obsahuje nějakou
         * část, která se má vynechat, pokud ano, pak se text nevypíše.
         */
        if (!writeException && text != null && isPartOfExceptionInListForSkip(text))
            return;


        // Sem se dostanu v případě že se text má vypsat, tak jej vypíšu do příslušného textového pole:
        printRedirectedText(text, true);
    }




    /**
     * Metoda, která zjistí, zda se část výjimky (partOfException) nachází v listu EXCEPTIONS_LIST s částmi výjimek,
     * které se nemají vypisovat do editoru výstupů.
     *
     * @param partOfException
     *         - řádek, který semá vypsat.
     * @return true, pokud partOfException obsahuje text (resp. část výjimky), který nachází v listu EXCEPTIONS_LIST,
     * jinak false.
     */
    private static boolean isPartOfExceptionInListForSkip(final String partOfException) {
        return EXCEPTIONS_LIST.stream().anyMatch(e -> partOfException.toUpperCase().contains(e.toUpperCase()));
    }




    @Override
    public void println(String x) {
        checkRedirectedText(x);
    }

    @Override
    public void println(Object x) {
        checkRedirectedText(x.toString());
    }

    @Override
    public void println() {
        checkRedirectedText("");
    }

    @Override
    public void println(boolean x) {
        checkRedirectedText(String.valueOf(x));
    }

    @Override
    public void println(char x) {
        checkRedirectedText(String.valueOf(x));
    }

    @Override
    public void println(char[] x) {
        checkRedirectedText(Arrays.toString(x));
    }

    @Override
    public void println(double x) {
        checkRedirectedText(String.valueOf(x));
    }

    @Override
    public void println(float x) {
        checkRedirectedText(String.valueOf(x));
    }

    @Override
    public void println(int x) {
        checkRedirectedText(String.valueOf(x));
    }

    @Override
    public void println(long x) {
        checkRedirectedText(String.valueOf(x));
    }

    @Override
    public void print(boolean b) {
        checkRedirectedText(String.valueOf(b));
    }

    @Override
    public void print(char[] s) {
        checkRedirectedText(Arrays.toString(s));
    }

    @Override
    public void print(long l) {
        checkRedirectedText(String.valueOf(l));
    }

    @Override
    public void print(int i) {
        checkRedirectedText(String.valueOf(i));
    }

    @Override
    public void print(char c) {
        checkRedirectedText(String.valueOf(c));
    }

    @Override
    public void print(double d) {
        checkRedirectedText(String.valueOf(d));
    }

    @Override
    public void print(float f) {
        checkRedirectedText(String.valueOf(f));
    }

    @Override
    public void print(Object obj) {
        checkRedirectedText(obj.toString());
    }

    @Override
    public void print(String s) {
        checkRedirectedText(s);
    }
}
