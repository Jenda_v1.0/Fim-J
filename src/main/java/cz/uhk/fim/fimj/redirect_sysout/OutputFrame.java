package cz.uhk.fim.fimj.redirect_sysout;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.io.File;
import java.util.Properties;

import javax.swing.*;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.GetIconInterface;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.language.EditProperties;
import cz.uhk.fim.fimj.preferences.PreferencesApi;

/**
 * Tato třída slouží jako okno, resp. Jframe, který obsahuje pouze "stručné" menu s některými možnostmi - omezenými a
 * editor coby klasická JtextArea, který ovšem není editovatelná - uživtelelm nelze editovat, pouze se do ní vypisují
 * výpisy z aplikace.
 * <p>
 * Toto okno si při spuštění načte výchozí nastavení, jako je třeba barva pozadí, velikost písma, barva písma, ... a až
 * se vše načte, tak se do JtestArey vloží nějaký text vypsaný z aplikace a zobrazí se toto okno, uživatel ho může
 * zavřít, nebo ho nechat otevřené, apod.
 * <p>
 * Note: Toto okno není povinné, vlastně by zde ani nemuselo být, a v určitém smyslu je to i práce navíc (možná i
 * zbytečná) a je možné, že to pro někoho nebude přehledné a tyto výpisy ve třídě pomocí System.out, apod. jsem mohl
 * přesměrovat do tzv. OutputEditoru - ten co je v hlavním okně aplikace umístěn vpravo dole - pro výpisy, apod. Ale
 * chtěl jsem, to udělat podobně jako to má BlueJ a navíc zrovna v tomot případě nevím, co má uživatel s výpisy z té
 * jeho vytvořené aplikace v plánu - jestli s nimi chce dále pracovat, pokud by podle nich chtěl například dále ladit
 * nějaký kod, apod. tak by to ani nešlo, protože by to nemohl tento kod otevrit spolu s nějakým editoru kodu v
 * aplikaci, tak jsem napsal toto oknopro výpisy, které lze otevřít a není modální, a uživatel si tak může pracovat s
 * výpisy i s oevřeným kódem, napříilad při zmíněném ladění, apod. a navíc si příslušný text může například vytisknout,
 * uložit do pdf souboru, uložit do jiné souboru - textového, apod. Takže s tím má alespoň nějakou prác než kdybych tyto
 * výpisy pomocí System.out přesměroval do zmíněnéhzho editoru výstupů - OuputEditoru
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class OutputFrame extends JFrame implements LanguageInterface {

	private static final long serialVersionUID = 1L;


    /**
     * Komponenta, která slouží pro výpisy pomocí metod v System.out od uživatele, resp. z uživatelova projektu v
     * aplikaci.
     * <p>
     * Tedy, pokud bude mít uživatel nějakou třídu, ve které bude mít například v konstruktoru výpis pomocí
     * System.out.println();, pak se při  zavolání tohoto konstruktoru text vypíšu do této komponenty - textového pole.
     */
    private static JTextArea txtAreaSysOut;

    /**
     * Komponenta, která slouží pro výpisy pomocí metod v System.err od uživatele, resp. z uživatelova projektu v
     * aplikaci.
     * <p>
     * Tedy, pokud bude mít uživatel nějakou třídu, ve které bude mít například v konstruktoru výpis pomocí
     * System.err.println();, pak se při  zavolání tohoto konstruktoru text vypíšu do této komponenty - textového pole.
     */
    private static JTextArea txtAreaSysErr;



    /**
     * Proměnná pro nastavení toho, zda se mají do výstupního terminálu vypisovat i signatury metod - pokud se v
     * zavolané metodě nachází výpis do konzole pomocí metod v System.out.
     */
    private static boolean recordCallMethods;
    /**
     * Proměnná pro nastavení toho, zda se mají do výstupního terminálu vypisovat i signatury konstruktorů - pokud se v
     * zavolaném konstruktoru nachází výpis do konzole pomocí metod v System.out.
     */
    private static boolean recordCallConstructors;




    /**
     * Nastavení toho, zda se mají při každém zavolání metody, která obsahuje nějaký výpis do konzole pomocí metod v
     * System.out vymazat textové pole v okně výstupního terminálu pro výstupy z System.out.
     * <p>
     * Tedy pokud se například zavolá metoda, která obsahuje výpis do konzole, pak se před tím výpisem nejprve vymaže
     * veškerý text v textovém poli pro tyto výstupy ve výstupním terminálu.
     */
    private static boolean clearTextBeforeCallMethod;
    /**
     * Nastavení toho, zda se mají při každém zavolání konstruktoru, který obsahuje nějaký výpis do konzole pomocí metod
     * v System.out vymazat textové pole pro výstupy z System.out v okně výstupního terminálu.
     * <p>
     * Tedy pokud se například zavolá konstruktor, který obsahuje výpis do konzole, pak se před tím výpisem nejprve
     * vymaže veškerý text v textovém poli pro tyto výstupy ve výstupním terminálu.
     */
    private static boolean clearTextBeforeCallConstructor;




    /**
     * Proměnná, která si bude držet nastavený jazyk pro aplikaci. Je to potřeba, protože musím při každém výpisu
     * nějakého textu do tohoto okna hlídat, zda náhodou není otevřen jiný projekt, protože pokud ano, musim změnit
     * titulek dialogu na správný otevřený projekt. Jinak by se nastavil pouze jednou a to při prvním vytvoření, ale pak
     * se mohl za běhu aplikace otevřít jiný projekt a toto okno by to již nepozanalo, tak je třeba toto testovat při
     * každém výpisu.
     */
    private final Properties languageProperties;

	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param languageProperties
	 *            - Objekt, který obsahuje texty pro tuto aplikaci ve
	 *            zvolenémjazyce.
	 */
	public OutputFrame(final Properties languageProperties) {
		super();

		this.languageProperties = languageProperties;
		
		initGui();
		
		
		final OutputFrameMenu myMenu = new OutputFrameMenu(this);
		myMenu.setLanguage(languageProperties);
		setJMenuBar(myMenu);





        txtAreaSysOut = new JTextArea();
        txtAreaSysOut.setEditable(false);

        txtAreaSysErr = new JTextArea();
        txtAreaSysErr.setEditable(false);

        final JScrollPane jspSysout = new JScrollPane(txtAreaSysOut);
        final JScrollPane jspSyserr = new JScrollPane(txtAreaSysErr);

        jspSysout.setBorder(BorderFactory.createTitledBorder("System.out"));
        jspSyserr.setBorder(BorderFactory.createTitledBorder("System.err"));

        final JSplitPane jspAreas = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true, jspSysout, jspSyserr);
        jspAreas.setResizeWeight(0.7d);
        jspAreas.setOneTouchExpandable(true);

        add(jspAreas, BorderLayout.CENTER);

		
		
		
		
		setTextArea();

        /*
         * Zde potřebuji zavolat nad menu tuto metodu, aby se označily chcb s výchozími hodnotami načtěné zmetody
         * setTextArea.
         */
        myMenu.prepareChcbs();


        setMouseListenersToTxtAreas();
		
		
		
		setLanguage(languageProperties);
		
		
		pack();
		setLocationRelativeTo(null);
	}








    /**
     * Metoda, která nastaví textovým polím pro výpisy od uživatele posluchač, který bude hlídat rolování kolečka myši,
     * aby se po stiknutém Ctrl a rolováním kolečka myši nahoru nebo dolů zvětšovalo nebo zmenšovalo písmo.
     */
    private static void setMouseListenersToTxtAreas() {
        addMouseWheelListener(txtAreaSysOut);
        addMouseWheelListener(txtAreaSysErr);
    }


    /**
     * Metoda, která nastaví komponentě txtArea událost na rolování kolečkem myši. Tedy pokud nad příslušnou komponentou
     * budeme rolovat kolečkem myši nahoru nebo dolů, písmo v té komponentě se bude zvětšovat (nahoru) nebo změnšovat
     * (dolů)
     *
     * @param txtArea
     *         - komponenta, ke které se má přidat posluchač na rolování kolečkem myši.
     */
    private static void addMouseWheelListener(final JTextArea txtArea) {
        txtArea.addMouseWheelListener(e -> {

            if (e.getWheelRotation() < 0 && e.isControlDown()) { // Rotace kolečkem myši nahoru - zvětšování písma
                float fontSize = txtArea.getFont().getSize();

                if (fontSize < 100)
                    txtArea.setFont(txtArea.getFont().deriveFont(++fontSize));
            } else if (e.getWheelRotation() > 0 && e.isControlDown()) { // Rotování kolečkem myši dolů - zmenšování
                // písma
                float fontSize = txtArea.getFont().getSize();

                if (fontSize > 8)
                    txtArea.setFont(txtArea.getFont().deriveFont(--fontSize));
            }
        });
    }







    /**
     * Metoda, která si načte ze souboru soubor OutputFrame.properties a podle získaných hodnot nastaví někteé
     * vlastnosti pro Jtextareu, případně použije výchozí
     */
    private static void setTextArea() {
        final Properties outputFrameProp = App.READ_FILE.getOutputFrameProperties();

        setAreaProperties(outputFrameProp, txtAreaSysOut, "SO_FontStyle", Constants.DEFAULT_FONT_FOR_SO_OUTPUT_FRAME,
                "SO_FontSize", "SO_BackgroundColor", Constants.DEFAULT_BACKGROUND_COLOR_FOR_SO_OUTPUT_FRAME,
                "SO_ForegroundColor", Constants.DEFAULT_TEXT_COLOR_FOR_SO_OUTPUT_FRAME);

        setAreaProperties(outputFrameProp, txtAreaSysErr, "SE_FontStyle", Constants.DEFAULT_FONT_FOR_SE_OUTPUT_FRAME,
                "SE_FontSize", "SE_BackgroundColor", Constants.DEFAULT_BACKGROUND_COLOR_FOR_SE_OUTPUT_FRAME,
                "SE_ForegroundColor", Constants.DEFAULT_TEXT_COLOR_FOR_SE_OUTPUT_FRAME);


        if (outputFrameProp != null) {
            recordCallMethods = Boolean.parseBoolean(outputFrameProp.getProperty("RecordCallMethodsInSo", String.valueOf(Constants.RECORD_CALL_METHODS_IN_SO_IN_OUTPUT_FRAME)));
            recordCallConstructors = Boolean.parseBoolean(outputFrameProp.getProperty("RecordCallConstructorsInSo", String.valueOf(Constants.RECORD_CALL_CONSTRUCTORS_IN_SO_IN_OUTPUT_FRAME)));

            clearTextBeforeCallMethod = Boolean.parseBoolean(outputFrameProp.getProperty("ClearTextInSoBeforeCallMethod", String.valueOf(Constants.CLEAR_TEXT_FOR_SO_BEFORE_CALL_METHOD_IN_OUTPUT_FRAME)));
            clearTextBeforeCallConstructor = Boolean.parseBoolean(outputFrameProp.getProperty("ClearTextInSoBeforeCallConstructor", String.valueOf(Constants.CLEAR_TEXT_FOR_SO_BEFORE_CALL_CONSTRUCTOR_IN_OUTPUT_FRAME)));
        }

        else {
            recordCallMethods = Constants.RECORD_CALL_METHODS_IN_SO_IN_OUTPUT_FRAME;
            recordCallConstructors = Constants.RECORD_CALL_CONSTRUCTORS_IN_SO_IN_OUTPUT_FRAME;

            clearTextBeforeCallMethod = Constants.CLEAR_TEXT_FOR_SO_BEFORE_CALL_METHOD_IN_OUTPUT_FRAME;
            clearTextBeforeCallConstructor = Constants.CLEAR_TEXT_FOR_SO_BEFORE_CALL_CONSTRUCTOR_IN_OUTPUT_FRAME;
        }
    }






    /**
     * Metoda, která nastaví vlastnosti pro text areu, která slouží buď pro výpisy z System.out nebo System.err.
     *
     * @param outputFrameProp
     *         - objekt properties s vlastnostmi pro výstupní terminál.
     * @param area
     *         - textové pole, kterému se mají nastavit vlastnosti.
     * @param keyFontStyle
     *         - klíč, dle kterého se vezme hodnota pro styl fontu.
     * @param font
     *         - výchozí font v případě, že se nepodaří získat hodnoty pro font písma.
     * @param keyFontSize
     *         - klíč, dle kteréoh se vezme hodnota pro velikost písma / fontu.
     * @param keyBackgroundColor
     *         - klíč, dle kterého se vezme hodnota pro barvu pozadí textového pole.
     * @param defaultBgColor
     *         - výchozí barva pozadí textového pole, která se aplikuje v případě, že se nepodaří získat hodnotu z
     *         objektu properties.
     * @param keyForegroundColor
     *         - klíč, dle kterého se vezme barva písma z objektu properties.
     * @param defaultForegroundColor
     *         - výchozí hodnota pro barvu písma, která se aplikuje v případě, že se nepodaří získat barvu písma z
     *         objektu properties.
     */
    private static void setAreaProperties(final Properties outputFrameProp, final JTextArea area, final String
            keyFontStyle, final Font font, final String keyFontSize, final String keyBackgroundColor, final Color
                                                  defaultBgColor, final String keyForegroundColor, final Color
            defaultForegroundColor) {

        if (outputFrameProp != null) {
            final int fontStyle = Integer.parseInt(outputFrameProp.getProperty(keyFontStyle, Integer.toString(font
                    .getStyle())));
            final int fontSize = Integer.parseInt(outputFrameProp.getProperty(keyFontSize, Integer.toString(font
                    .getSize())));

            area.setFont(new Font(font.getName(), fontStyle, fontSize));


            final String bgColor = outputFrameProp.getProperty(keyBackgroundColor, Integer.toString(defaultBgColor
                    .getRGB()));
            area.setBackground(new Color(Integer.parseInt(bgColor)));


            final String foregroundColor = outputFrameProp.getProperty(keyForegroundColor, Integer.toString
                    (defaultForegroundColor.getRGB()));
            area.setForeground(new Color(Integer.parseInt(foregroundColor)));
        }

        else {
            area.setFont(font);

            area.setBackground(defaultBgColor);

            area.setForeground(defaultForegroundColor);
        }
    }











    /**
     * Metoda, která přidá nový text do JtextArey pro výpis textů z System.out.
     *
     * @param text
     *         - text, který se má zobrazit uživateli - přidat na konec textového pole.
     */
    final void appendSysOutText(final String text) {
        /*
         * Musím znovu zavolat metodu pro nastaveni jazyka, ale jedná se pouze o nastavení titulku dialogu - kvuli
         * zmene otevreneho projektu
         */
        setLanguage(languageProperties);

        // Přidám text a nový řádek, protože by se jinak připojoval text na stejný řádek:
        txtAreaSysOut.append(text + "\n");
    }


    /**
     * Metoda, která přidá nový text do JtextArey pro výpis textů z System.out.
     *
     * @param text
     *         - text, který se má zobrazit uživateli - přidat na konec textového pole.
     */
    final void appendSysErrText(final String text) {
        /*
         * Musím znovu zavolat metodu pro nastaveni jazyka, ale jedná se pouze o nastavení titulku dialogu - kvuli
         * zmene otevreneho projektu
         */
        setLanguage(languageProperties);

        // Přidám text a nový řádek, protože by se jinak připojoval text na stejný řádek:
        txtAreaSysErr.append(text + "\n");
    }






    /**
     * Getr na proměnnou typu JtextArea, do které se vkládají výstupy z System.out od uživatele - z jeho projektu.
     *
     * @return referenci na instanci třídy JtextArea
     */
    static JTextArea getTxtAreaSysOut() {
        return txtAreaSysOut;
    }


    /**
     * Getr na proměnnou typu JtextArea, do které se vkládají výstupy z System.err od uživatele - z jeho projektu.
     *
     * @return referenci na instanci třídy JtextArea
     */
    static JTextArea getTxtAreaSysErr() {
        return txtAreaSysErr;
    }

    /**
     * Getr na hodnotu, která značí, zda se mají vypisovat signatury zavolaných metod v případě, že obsahují výpis do
     * konzole.
     *
     * @return true, pokud se mají vypisovat výše zmíněné signatury metod, jinak false.
     */
    static boolean isRecordCallMethods() {
        return recordCallMethods;
    }

    /**
     * Setr na proměnnou, která značí, zda mají vypisovat signatury zavolaných metod v případě, že obsahují výpis do
     * konzole.
     *
     * @param recordCallMethods
     *         true -> mají se vypisovat signatury metod, jinak false.
     */
    static void setRecordCallMethods(boolean recordCallMethods) {
        OutputFrame.recordCallMethods = recordCallMethods;

        changeValueInConfigFile("RecordCallMethodsInSo", recordCallMethods);
    }

    /**
     * Getr na hodnotu, která značí, zda se mají vypisovat signatury zavolaných konstruktorů v případě, že obsahují
     * výpis do konzole.
     *
     * @return true, pokud se mají vypisovat výše zmíněné signatury konstruktorů, jinak false.
     */
    static boolean isRecordCallConstructors() {
        return recordCallConstructors;
    }

    /**
     * Setr na proměnnou, která značí, zda mají vypisovat signatury zavolaných konstruktorů v případě, že obsahují výpis
     * do konzole.
     *
     * @param recordCallConstructors
     *         true -> mají se vypisovat signatury konstruktorů, jinak false.
     */
    static void setRecordCallConstructors(boolean recordCallConstructors) {
        OutputFrame.recordCallConstructors = recordCallConstructors;

        changeValueInConfigFile("RecordCallConstructorsInSo", recordCallConstructors);
    }




    /**
     * Getr na proměnnou, která značí, zda se má textové pole pro výpis textů do konzole v System.out vymazat před
     * každým výpisem / zavoláním metody.
     * <p>
     * Tedy, pokud uživatel zavolá metodu, která obsahuje výpis do konzole, pak se před tímto výpisem nejprve vymaže
     * veškerý text v textovém poli pro výstupy v System.out a až poté se vypíše příslušný text.
     *
     * @return true, mají se před výpisem z metody vymazat veškeré texty v textovém poli pro výstupy z System.out, jinak
     * false.
     */
    static boolean isClearTextBeforeCallMethod() {
        return clearTextBeforeCallMethod;
    }

    /**
     * Setr na proměnnou, která značí, zda se má při zavolání metody s výpisem do konzole pomocí System.out nejprve
     * vymazat textové pole pro výpisy ve výstupnim terminálu.
     *
     * @param clearTextBeforeCallMethod
     *         true, pokud se má vymazat textové pole před výpisem z metody, jinak false.
     */
    static void setClearTextBeforeCallMethod(boolean clearTextBeforeCallMethod) {
        OutputFrame.clearTextBeforeCallMethod = clearTextBeforeCallMethod;

        changeValueInConfigFile("ClearTextInSoBeforeCallMethod", clearTextBeforeCallMethod);
    }

    /**
     * Getr na proměnnou, která značí, zda se má textové pole pro výpis textů do konzole v System.out vymazat před
     * každým výpisem / zavoláním konstruktoru.
     * <p>
     * Tedy, pokud uživatel zavolá metodu, která obsahuje výpis do konzole, pak se před tímto výpisem nejprve vymaže
     * veškerý text v textovém poli pro výstupy v System.out a až poté se vypíše příslušný text.
     *
     * @return true, mají se před výpisem z konstruktoru vymazat veškeré texty v textovém poli pro výstupy z System.out,
     * jinak false.
     */
    static boolean isClearTextBeforeCallConstructor() {
        return clearTextBeforeCallConstructor;
    }


    /**
     * Setr na proměnnou, která značí, zda se má při zavolání konstruktoru s výpisem do konzole pomocí System.out
     * nejprve vymazat textové pole pro výpisy ve výstupnim terminálu.
     *
     * @param clearTextBeforeCallConstructor
     *         true, pokud se má vymazat textové pole před výpisem z metody, jinak false.
     */
    static void setClearTextBeforeCallConstructor(boolean clearTextBeforeCallConstructor) {
        OutputFrame.clearTextBeforeCallConstructor = clearTextBeforeCallConstructor;

        changeValueInConfigFile("ClearTextInSoBeforeCallConstructor", clearTextBeforeCallConstructor);
    }






    /**
	 * Metoda, která nastaví "výchozí" GUI pro toto okno, výchozí design, jako je
	 * například ikona okna, způsob zavření, velikost okna, ... více viz metoda.
	 */
	private void initGui() {
		setPreferredSize(new Dimension(850, 650));
		setLayout(new BorderLayout());
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setIconImage(GetIconInterface.getMyIcon("/icons/outputFrameIcons/OutputFrameIcon.png", true).getImage());
	}







    /**
     * Metoda, která načte konfigurační soubor OutputFrame.properties z workspace, zapíše do něj ke klíči key hodnotu
     * value a uloží zpět do souboru na disku.
     * <p>
     * Pokud nebude konfigurační soubor nalezen, nic se nestane.
     *
     * @param key
     *         - klíč, ke kterému se má uložit hodnota value.
     * @param value
     *         - hodnota, která se má uložit ke klíči key.
     */
    private static void changeValueInConfigFile(final String key, final boolean value) {
        /*
         * Získám si konfigurační soubor OutputFrame.properties z adresáře configuration ve workspace, kam uložím
         * změněou hodnotu.
         */
        final EditProperties outputFrameProperties = App.READ_FILE.getPropertiesInWorkspacePersistComments(Constants
                .OUTPUT_FRAME_NAME);

        /*
         * Zde otestuji, zda se podařilo načíst příslušný konfigurační soubor, což mělo by nastat v každém případě.
         * Proto zde nebudu řešit
         * případy, kdy se ten soubor nenačte, navíc když by se ten soubor nepodařilo načíst, tak to ani nijak nevadí
         * . Uživatelem nastavená hodnota se
         * ukládá do proměnné za běhu aplikace, takže vždy bude mít oznčenou hodnotu a dle toho se budou / nebudou
         * zobrazovat hlavičky metod a konstruktorů, případně mazat textové pole pro System.out.
         *
         * Pokud se soubor nepodaří načíst, tak se při novém spuštění aplikace nebo otevření dialogu nastavení
         * aplikace ty soubory vytvoří a bude načtena příslušná hodnota. Navíc
         * běží vlákno, které "hlídá" soubory, aby do nich uživatel nezadal nevalidní data, nebo je nesmazal za běhu
         * aplikace.
         *
         * Navíc bych nechtěl, aby se ty konfiguraní soubory vytvářely kvůli "této" komponentě výstupní terminál,
         * spíše nechám řešení konfiguračních souborů
         * na dialogu nastavení a "apliaci" - spouštění apod.
         */
        if (outputFrameProperties == null)
            return;

        /*
         * Sem se dostanu v případě, že se podařilo načíst konfigurační soubor, tak mohu zapsat změněnou hodnotu.
         */
        outputFrameProperties.setProperty(key, String.valueOf(value));

        // Uložení zpět na disk:
        outputFrameProperties.save();
    }






	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null)
			setTitle(properties.getProperty("Of_FrameTitle", Constants.OF_FRAME_TITLE));

		else
			setTitle(Constants.OF_FRAME_TITLE);
		
		
		// zjistím si název otevřeného projektu a nastavím jej do titulku okna:
		final String openProject = PreferencesApi.getPreferencesValue(PreferencesApi.OPEN_PROJECT, null);
		
		if (openProject != null) {
			final File fileProject = new File(openProject);
			// Zde již vím, že je nějaký projekt otevřen, ale už nevím, zda ještě existuje na disku, a to mně ani
			// v tuto chvíli nezajímá, potřebuji pouze vědět název adresáře, a jelikož otevřít jde pouze adresář,
			// tak už ani nemusím testovat, zda je to cesta k adresáři a pouze si vezmu jeho název:
			setTitle(getTitle() + " - " + fileProject.getName());		
		}
	}
}