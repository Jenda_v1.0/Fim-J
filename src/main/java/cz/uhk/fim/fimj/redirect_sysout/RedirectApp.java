package cz.uhk.fim.fimj.redirect_sysout;

import java.util.Properties;

/**
 * Třída slouží pro definování přesměrování textů od uživatele.
 * <p>
 * Jedná se o vytvoření tříd pro přesměrování výpisů a nastavení samotného přesměrování výpisů pomocí metod v System.out
 * a System.err.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 20.07.2018 11:37
 */

public class RedirectApp {

    /**
     * Okno - výstupní terminál, které obsahuje textová pole pro výpis textů od uživatele pomocí System.out a
     * System.err.
     */
    private static OutputFrame outputFrame;

    /**
     * Objekt pro uložení textů pro aplikace, konkrétně pro okno OutputFrame, je jej třeba nastavit například v případě
     * změny jazyka nebo otevření projektu apod.
     */
    private static Properties languageProperties;

    /**
     * Proměnná, která slouží pro uložení konstruktoru nebo metody, kterou uživatel zavolal.
     * <p>
     * Pokud uživatel povolil, že se v okně  výstupní terminál mají vypisovat hlavičky konstruktorů nebo metod, které
     * něco vypisují do konzole pomocí metod v System.out, tak se z této proměnné vezme hodnota a případně se vypíše do
     * výstupního terminálu do System.out okna.
     */
    private static CalledMethod calledMethod;


    private RedirectApp() {
    }


    /**
     * Metoda, která "připraví / nastaví přesměrování".
     * <p>
     * Metoda vytvoří instanci okna výstupního terminálu, aby se do něj mohli přesměrovávat veškeré výpisy.
     * <p>
     * Dále vytvoří instance tříd a nastaví přesměrování textů v metodách v System.out a System.err.
     *
     * @param languageProperties
     *         - texty pro aplikaci ve zvoleném jazyce.
     */
    public static void prepareRedirects(final Properties languageProperties) {
        // Vytvoření okna:
        createOutputFrame(languageProperties);

        // Nastavení přesměrování výpisů do výstupního terminálu:
        final Redirect sysOut = new SysOut(System.out);
        final Redirect sysErr = new SysErr(System.err);

        // Nastavení přesměrování výspisů:
        System.setOut(sysOut);
        System.setErr(sysErr);
    }


    /**
     * Vytvoření okna výstupní terminál s předáním textů pro aplikaci.
     */
    static void createOutputFrame() {
        createOutputFrame(languageProperties);
    }

    /**
     * Vytvoření okna výstupní terminál v případě, že neexistuje (tato možnost by měla nastat pouze při prvním vytváření
     * okna výstupní terminál v konstruktoru této třídy).
     *
     * @param languageProperties
     *         - texty pro aplikaci ve zvoleném jazyce.
     */
    private static void createOutputFrame(final Properties languageProperties) {
        if (outputFrame == null)
            outputFrame = new OutputFrame(languageProperties);

        if (RedirectApp.languageProperties == null && languageProperties != null)
            RedirectApp.languageProperties = languageProperties;
    }


    /**
     * Zviditelnění okna výstupní terminál v případě, že není. To může nastat například v případě, že se jedná o první
     * výpis textu nebo uživatel zavřel dialog výstupní terminál.
     */
    public static void markOutputFrame() {
        // okno by nemělo být null, pouze v případě, že selže inicializace (nikdy nenastalo, pokud jsem to
        // nezakometoval)
        if (outputFrame != null && !outputFrame.isVisible())
            outputFrame.setVisible(true);
    }


    /**
     * Metoda, která slouží pro "schování" okna výstupního terminálu (pokud je zobrazné).
     */
    public static void disposeOutputFrame() {
        if (outputFrame != null && outputFrame.isVisible())
            outputFrame.dispose();
    }

    /**
     * Změna titulku okna výstupního terminálu.
     * <p>
     * Například při otevření nebo zavření projektu v aplikaci je třeba změnit titulek v okně výstupního terminálu,
     * protože jinak by se v tom titulku zobrazoval pořád zavřený nebo předchozí otevřený projekt apod.
     */
    public static void changeOutputFrameTitle() {
        // Nemělo by nastat nikdy, pokud není zakomentováno úvodní vytvoření (nastalo při úmyslením zakomentování při
        // testování, jinak OK)
        if (outputFrame != null)
            outputFrame.setLanguage(languageProperties);
    }


    /**
     * Metoda, která vypíše text v parametru text do okna výstupního terminálu do textového pole pro výpisy System.out.
     * <p>
     * Jedná se pouze o výpis podporovaných verzí Javy.
     *
     * @param text
     *         text, který obsahuje podporováne verze Javy dle nastaveného kompilátoru. Tento text se má vypsat do okna
     *         výstupní terminál
     */
    public static void addSupportedJavaVersions(final String text) {
        outputFrame.appendSysOutText(text);
    }


    /**
     * Getr na okno výstupní terminál.
     *
     * @return referenci na instanci okna výstupní terminál.
     */
    static OutputFrame getOutputFrame() {
        return outputFrame;
    }

    /**
     * Getr na proměnnou, do které se odkládají zavolané konstruktory a metody, které uživatel zavolal a kde mohlo dojít
     * k výpisu pomocí metod v System.out.
     *
     * @return referenci na proměnnou calledMethod, viz výše.
     */
    static CalledMethod getCalledMethod() {
        return calledMethod;
    }

    /**
     * Setr na proměnnou calledMethod. Do této proměnné se zavolaná metody nebo konstruktor, který se může vypsat před
     * vypsání textů do výstupního terminálu, jedná se například o konstruktor nebo metodu, kterou uživatel zavolal a
     * kde mohlo dojít k výpisu do konzole pomocí System.out.
     *
     * @param calledMethod
     *         - reference na objekt, který může obsahovat uživatelem zavolanou metodu nebo konstruktor, který může
     *         obsahovat nějaké výpisy do konzole pomocí metod v System.out.
     */
    public static void setCalledMethod(CalledMethod calledMethod) {
        RedirectApp.calledMethod = calledMethod;
    }
}
