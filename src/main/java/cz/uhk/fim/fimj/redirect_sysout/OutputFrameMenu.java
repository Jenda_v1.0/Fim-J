package cz.uhk.fim.fimj.redirect_sysout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.swing.*;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.FileChooser;
import cz.uhk.fim.fimj.file.GetIconInterface;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.print.Printer;
import cz.uhk.fim.fimj.project_explorer.ReadWriteFile;

/**
 * Tato třída slouží jako menu pro okno coby "výstupní terminál" resp. pro třídu OutputFrameMenu.java.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class OutputFrameMenu extends JMenuBar implements LanguageInterface, ActionListener {

	private static final long serialVersionUID = 1L;

	private static final String TXT_SYSTEM_OUT = "System.out";
	private static final String TXT_SYSTEM_ERR = "System.err";


    /**
     * Menu, které slouží pro umístění položek coby možností, co lze provést s hodnotami v editoru, resp. v okně
     */
    private final JMenu menuOptions;
    /**
     * Menu pro položky, které slouží pro vymazání textu z jednotlivých textových polí.
     */
    private final JMenu menuClear;
    /**
     * Menu pro položky, které slouží pro vymazání textu z textového pole pro výpis textů do konzole pomocí metod v
     * System.out.
     */
    private final JMenu menuClearSysOutScreen;
    /**
     * Menu pro položky, které umožňují uložit text z výstupního terminálu do souboru zvoleného uživatelem.
     */
    private final JMenu menuSaveToFile;
    /**
     * Menu pro položky, které umožňují vytisknutí textů z výstupního terminálu, například pomocí tiskárny, do pdf
     * souboru apod.
     */
    private final JMenu menuPrint;







    /**
     * Položka pro vymazání obou textových polí (System.out a System.err).
     */
    private final JMenuItem itemClearAll;
    /**
     * Položka pro vymazání textového pole pro výpisy z System.out
     */
    private final JMenuItem itemClearSysOut;
    /**
     * Položka pro vymazání textového pole pro výpisy z System.err
     */
    private final JMenuItem itemClearSysErr;





    /**
     * Položka pro vymazání textového pole pro výpis textů z System.out při volání metody s výpisy do konzole.
     */
    private final JCheckBoxMenuItem chcbClearSysOutScreenWhenCallMethod;
    /**
     * Položka pro vymazání textového pole pro výpis textů z System.out při volání konstruktoru s výpisy do konzole.
     */
    private final JCheckBoxMenuItem chcbClearSysOutScreenWhenCallConstructor;







    /**
     * Uložení textů v okně pro výpis textů System.out do souboru.
     */
    private final JMenuItem itemSaveSysOutToFile;
    /**
     * Uložení textů v okně pro výpis textů System.err do souboru.
     */
    private final JMenuItem itemSaveSysErrToFile;
    /**
     * Uložení textů do souboru. Nejprve budou uloženy texty vypsané v System.out, pak jeden volný řádek a pak metody v
     * System.err. (Včetně nadpisů System.out a System.err)
     */
    private final JMenuItem itemSaveSysOutAndSysErrToFile;
    /**
     * Uložení textů do souboru. Nejprve budou uloženy texty vypsané v System.err, pak jeden volný řádek a pak metody v
     * System.out. (Včetně nadpisů System.err a System.out)
     */
    private final JMenuItem itemSaveSysErrAndSysOutToFile;






    /**
     * Zobrazí dialog pro možnosti nastavení vytisknutí textů v textovém poli pro texty z System.out
     */
    private final JMenuItem itemPrintSysOut;
    /**
     * Zobrazí dialog pro možnosti nastavení vytisknutí textů v textovém poli pro texty z System.err
     */
    private final JMenuItem itemPrintSysErr;
    /**
     * Zobrazí dialog pro možnosti nastavení vytisknutí textů v textovém poli pro texty z System.out a pod to
     * System.err
     */
    private final JMenuItem itemPrintSysOutAndSysErr;
    /**
     * Zobrazí dialog pro možnosti nastavení vytisknutí textů v textovém poli pro texty z System.err a pod to
     * System.out
     */
    private final JMenuItem itemPrintSysErrAndSysOut;





    /**
     * Komponenta pro nastavení toho, zda se mají v okně výstupní terminál ke každému výpisu System.out přidat i metoda,
     * která byla zavolána - ve které došlo k výpisu v System.out.
     */
    private final JCheckBoxMenuItem chcbRecordCallMethods;
    /**
     * Komponenta pro nastavení toho, zda se mají v okně výstupní terminál ke každému výpisu System.out přidat i
     * konstruktor, kde došlo k tomuto výpisu. Tedy, když uživatel zavolal například konstruktor, který obsahuje výpis
     * do konzole v System.out, tak se vypíše i příslušný kontruktor.
     */
    private final JCheckBoxMenuItem chcbRecordCallConstructors;





    /**
     * Zavření okna - výstupního terminálu
     */
    private final JMenuItem itemClose;







    /**
	 * Proměnná typu OutputFrame, je zde potřeba, protože je v tomto menu potgřeba
	 * reference na instanci třídy OutputFrame, aby šlo toto okno napřílad zavřít,
	 * mohl jsem si z něj vzít text pro vytisknutí, apod.
	 */
	private final OutputFrame outputFrame;
	
	
	
	
	
	// Proměnné do chybových hlášek:
	private String txtFileIsNotWrittenText;
    private String txtLocation;
    private String txtFileIsNotWrittenTitle;
	
	

	
	
	/**
	 * Konstruktor této třídy, inicializjí se zde tlačítka
	 * 
	 * @param outputFrame
	 *            - reference na instanci třídy OutputFrame, je zde potřeba, abych
	 *            si vzal referenci na JtextAreu, mohl zavíř okno, ...
	 */
	public OutputFrameMenu(final OutputFrame outputFrame) {
		super();
		
		this.outputFrame = outputFrame;
		
		// inicializace apřídání menu do okna:
		menuOptions = new JMenu();
		add(menuOptions);


		// Menu s položkami pro vymazání textových polí:
        menuClear = new JMenu();

        itemClearSysOut = new JMenuItem();
        itemClearSysErr = new JMenuItem();
        itemClearAll = new JMenuItem();

        itemClearSysOut.addActionListener(this);
        itemClearSysErr.addActionListener(this);
        itemClearAll.addActionListener(this);

        menuClear.add(itemClearSysOut);
        menuClear.add(itemClearSysErr);
        menuClear.addSeparator();
        menuClear.add(itemClearAll);



        // Menu s položkami pro vymazání textového pole pro System.out při volání metody a konstruktoru s výpisy:
        menuClearSysOutScreen = new JMenu();

        chcbClearSysOutScreenWhenCallConstructor = new JCheckBoxMenuItem();
        chcbClearSysOutScreenWhenCallMethod = new JCheckBoxMenuItem();

        chcbClearSysOutScreenWhenCallConstructor.addActionListener(this);
        chcbClearSysOutScreenWhenCallMethod.addActionListener(this);

        menuClearSysOutScreen.add(chcbClearSysOutScreenWhenCallConstructor);
        menuClearSysOutScreen.addSeparator();
        menuClearSysOutScreen.add(chcbClearSysOutScreenWhenCallMethod);



		// Menu s položkami pro uložení textů ve výstupním terminálu:
        menuSaveToFile = new JMenu();

        itemSaveSysOutToFile = new JMenuItem();
        itemSaveSysErrToFile = new JMenuItem();
        itemSaveSysOutAndSysErrToFile = new JMenuItem();
        itemSaveSysErrAndSysOutToFile = new JMenuItem();

        itemSaveSysOutToFile.addActionListener(this);
        itemSaveSysErrToFile.addActionListener(this);
        itemSaveSysOutAndSysErrToFile.addActionListener(this);
        itemSaveSysErrAndSysOutToFile.addActionListener(this);

        menuSaveToFile.add(itemSaveSysOutToFile);
        menuSaveToFile.add(itemSaveSysErrToFile);
        menuSaveToFile.addSeparator();
        menuSaveToFile.add(itemSaveSysOutAndSysErrToFile);
        menuSaveToFile.add(itemSaveSysErrAndSysOutToFile);



        menuPrint = new JMenu();

        itemPrintSysOut = new JMenuItem();
        itemPrintSysErr = new JMenuItem();
        itemPrintSysOutAndSysErr = new JMenuItem();
        itemPrintSysErrAndSysOut = new JMenuItem();

        itemPrintSysOut.addActionListener(this);
        itemPrintSysErr.addActionListener(this);
        itemPrintSysOutAndSysErr.addActionListener(this);
        itemPrintSysErrAndSysOut.addActionListener(this);

        menuPrint.add(itemPrintSysOut);
        menuPrint.add(itemPrintSysErr);
        menuPrint.addSeparator();
        menuPrint.add(itemPrintSysOutAndSysErr);
        menuPrint.add(itemPrintSysErrAndSysOut);



        // Výpis konstruktorů a metod s výpisy do konzole:
        chcbRecordCallConstructors = new JCheckBoxMenuItem();
        chcbRecordCallMethods = new JCheckBoxMenuItem();

        chcbRecordCallConstructors.setSelected(OutputFrame.isRecordCallConstructors());
        chcbRecordCallMethods.setSelected(OutputFrame.isRecordCallMethods());

        chcbRecordCallConstructors.addActionListener(this);
        chcbRecordCallMethods.addActionListener(this);






		// Položka pro zavření okna - výstupní terminál:
		itemClose = new JMenuItem();
		itemClose.addActionListener(this);



		// Přidání do menu v okně výstupního terminálu:
		menuOptions.add(menuClear);
		menuOptions.addSeparator();
        menuOptions.add(menuClearSysOutScreen);
		menuOptions.addSeparator();
		menuOptions.add(menuSaveToFile);
		menuOptions.addSeparator();
		menuOptions.add(menuPrint);
		menuOptions.addSeparator();
		menuOptions.add(chcbRecordCallConstructors);
		menuOptions.add(chcbRecordCallMethods);
		menuOptions.addSeparator();
		menuOptions.add(itemClose);
		
		
		
		setShortcutsToItems();
		
		
		setIconsToItems();
	}
	
	
	

	
	
	/**
	 * Metoda, která nastaví klávesové zkratky pro tlačítka v menu
	 */
	private void setShortcutsToItems() {
		// Ctrl + W
		itemClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_DOWN_MASK));
	}
	
	
	

	
	/**
	 * Metoda, která nastaví ikonu položkám v menu
	 */
	private void setIconsToItems() {
		menuClear.setIcon(GetIconInterface.getMyIcon("/icons/outputFrameIcons/ClearIcon.png", true));
		
		menuSaveToFile.setIcon(GetIconInterface.getMyIcon("/icons/outputFrameIcons/SaveToFileIcon.png", true));
		
		menuPrint.setIcon(GetIconInterface.getMyIcon("/icons/outputFrameIcons/PrintIcon.png", true));
		
		itemClose.setIcon(GetIconInterface.getMyIcon("/icons/outputFrameIcons/CloseIcon.png", true));

		menuClearSysOutScreen.setIcon(GetIconInterface.getMyIcon("/icons/outputFrameIcons/ClearScreenIcon.png", true));
	}
	
	





	@Override
	public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof JMenuItem) {
            if (e.getSource() == itemClearSysOut)
                OutputFrame.getTxtAreaSysOut().setText(null);

            else if (e.getSource() == itemClearSysErr)
                OutputFrame.getTxtAreaSysErr().setText(null);

            else if (e.getSource() == itemClearAll) {
                OutputFrame.getTxtAreaSysOut().setText(null);
                OutputFrame.getTxtAreaSysErr().setText(null);
            }

			



			else if (e.getSource() == itemSaveSysOutToFile) {
                final List<String> text = Arrays.asList(OutputFrame.getTxtAreaSysOut().getText().split("\n"));

                saveTextToFile(text);
            }

            else if (e.getSource() == itemSaveSysErrToFile) {
                final List<String> text = Arrays.asList(OutputFrame.getTxtAreaSysErr().getText().split("\n"));

                saveTextToFile(text);
            }

            else if (e.getSource() == itemSaveSysOutAndSysErrToFile) {
                /*
                 * List, do kterého se vloží veškeré texty pro vytisknutí.
                 */
                final List<String> text = new ArrayList<>();

                // Titulek, aby uživatel věděl, že jako první je System.out:
                text.add(TXT_SYSTEM_OUT);
                text.addAll(Arrays.asList(OutputFrame.getTxtAreaSysOut().getText().split("\n")));

                //  Volná řádek pro oddělení:
                text.add("\n");

                // Titulek, aby uživatel věděl, že následují chybové výstupy:
                text.add(TXT_SYSTEM_ERR);
                text.addAll(Arrays.asList(OutputFrame.getTxtAreaSysErr().getText().split("\n")));

                // Dotaz na míto uložení a samotné uložení:
                saveTextToFile(text);
            }

            else if (e.getSource() == itemSaveSysErrAndSysOutToFile) {
                /*
                 * List, do kterého se vloží veškeré texty pro vytisknutí.
                 */
                final List<String> text = new ArrayList<>();

                // Titulek, aby uživatel věděl, že jako první jsou chybové výstupy:
                text.add(TXT_SYSTEM_ERR);
                text.addAll(Arrays.asList(OutputFrame.getTxtAreaSysErr().getText().split("\n")));

                //  Volná řádek pro oddělení:
                text.add("\n");

                // Titulek, aby uživatel věděl, že následuje System.out:
                text.add(TXT_SYSTEM_OUT);
                text.addAll(Arrays.asList(OutputFrame.getTxtAreaSysOut().getText().split("\n")));

                // Dotaz na míto uložení a samotné uložení:
                saveTextToFile(text);
            }






            else if (e.getSource() == itemPrintSysOut) {
                // Pro vytisknutí si potřebuji vzít text z JtextArey a rozdělit ho dle nových řádku do
                // list a ten předat do metody pro vytisknutí:

                final String text = OutputFrame.getTxtAreaSysOut().getText();
                final List<String> listByLine = Arrays.asList(text.split("\n"));

                // vytisknu text:
                Printer.printToPrinter(listByLine);
            }

            else if (e.getSource() == itemPrintSysErr) {
                final String text = OutputFrame.getTxtAreaSysErr().getText();
                final List<String> listByLine = Arrays.asList(text.split("\n"));

                // vytisknu text:
                Printer.printToPrinter(listByLine);
            }

            else if (e.getSource() == itemPrintSysOutAndSysErr) {
                final List<String> text = new ArrayList<>();

                text.add(TXT_SYSTEM_OUT);

                final String textSysOut = OutputFrame.getTxtAreaSysOut().getText();
                text.addAll(Arrays.asList(textSysOut.split("\n")));

                text.add("\n");
                text.add(TXT_SYSTEM_ERR);

                final String textSysErr = OutputFrame.getTxtAreaSysErr().getText();
                text.addAll(Arrays.asList(textSysErr.split("\n")));

                // vytisknu text:
                Printer.printToPrinter(text);
            }

            else if (e.getSource() == itemPrintSysErrAndSysOut) {
                final List<String> text = new ArrayList<>();

                text.add(TXT_SYSTEM_ERR);

                final String textSysErr = OutputFrame.getTxtAreaSysErr().getText();
                text.addAll(Arrays.asList(textSysErr.split("\n")));

                text.add("\n");
                text.add(TXT_SYSTEM_OUT);

                final String textSysOut = OutputFrame.getTxtAreaSysOut().getText();
                text.addAll(Arrays.asList(textSysOut.split("\n")));

                // vytisknu text:
                Printer.printToPrinter(text);
            }






            // JCheckBoxMenuItem je instancí JMenuItem, proto je v této podmínce (pro JMenuItem):
            else if (e.getSource() == chcbRecordCallConstructors)
                OutputFrame.setRecordCallConstructors(chcbRecordCallConstructors.isSelected());


            else if (e.getSource() == chcbRecordCallMethods)
                OutputFrame.setRecordCallMethods(chcbRecordCallMethods.isSelected());




            else if (e.getSource() == chcbClearSysOutScreenWhenCallConstructor)
                OutputFrame.setClearTextBeforeCallConstructor(chcbClearSysOutScreenWhenCallConstructor.isSelected());

            else if (e.getSource() == chcbClearSysOutScreenWhenCallMethod)
                OutputFrame.setClearTextBeforeCallMethod(chcbClearSysOutScreenWhenCallMethod.isSelected());




			
			
			else if (e.getSource() == itemClose)
				outputFrame.dispose();
		}
    }






    /**
     * Dotaz uživatele na místo uložení, název a typ souboru, kam se mají uložit zvolené texty z výstupního terminálu.
     *
     * @param text
     *         list, který obsahuje zvolené texty, kter échce uživatel uložit.
     */
    private void saveTextToFile(final List<String> text) {
        // Nabídnu možnost pro zvolení místa, kam se má dialog uložit
        // a zapíšu jej to příslušného souboru, který si uživatel zvolí
        final String pathToFile = new FileChooser().getPathToNewFileForSave();

        if (pathToFile != null) {
            final boolean result = new ReadWriteFile().setTextToFile(pathToFile, text);

            if (!result)
                JOptionPane.showMessageDialog(this,
                        txtFileIsNotWrittenText + "\n" + txtLocation + ": " + pathToFile,
                        txtFileIsNotWrittenTitle, JOptionPane.ERROR_MESSAGE);
        }
    }







    /**
     * Metoda, která označí chcb po tom, co se načtou jejich výchozí hodnoty z konfiguračního souboru. Tato metoda se
     * volá pouze jednou při výchozí instanciaci okna.
     */
    void prepareChcbs() {
        chcbRecordCallConstructors.setSelected(OutputFrame.isRecordCallConstructors());
        chcbRecordCallMethods.setSelected(OutputFrame.isRecordCallMethods());

        chcbClearSysOutScreenWhenCallConstructor.setSelected(OutputFrame.isClearTextBeforeCallConstructor());
        chcbClearSysOutScreenWhenCallMethod.setSelected(OutputFrame.isClearTextBeforeCallMethod());
    }




	
	
	
	
	@Override
	public void setLanguage(final Properties properties) {
        final String txtAnd;
        final String txtDeleteTextIn;
        final String txtSaveTextIn;
        final String txtPrintTextIn;


		if (properties != null) {
			menuOptions.setText(properties.getProperty("Of_Menu_MenuOptions", Constants.OF_MENU_MENU_OPTIONS));			
						
			// Texty do položek:
			menuClear.setText(properties.getProperty("Of_Menu_Clear", Constants.OF_MENU_CLEAR));
            menuClearSysOutScreen.setText(properties.getProperty("Of_Menu_ClearSysOutScreen", Constants.OF_MENU_CLEAR_SYS_OUT_SCREEN));
			menuSaveToFile.setText(properties.getProperty("Of_Menu_ItemSaveToFile", Constants.OF_MENU_SAVE_TO_FILE));
			menuPrint.setText(properties.getProperty("Of_Menu_Print", Constants.OF_MENU_PRINT));
			itemClose.setText(properties.getProperty("Of_Menu_ItemClose", Constants.OF_MENU_ITEM_CLOSE));

            itemClearAll.setText(properties.getProperty("Of_Menu_ItemClearAll", Constants.OF_MENU_ITEM_CLEAR_ALL));


			// Popisky do položek:
			itemClose.setToolTipText(properties.getProperty("Of_Menu_TT_ItemClose", Constants.OF_MENU_TT_ITEM_CLOSE));


			// Texty do chybových hlášek:
			txtFileIsNotWrittenText = properties.getProperty("Of_Menu_Txt_FileIsNotWrittenText", Constants.OF_MENU_TXT_FILE_IS_NOT_WRITTEN_TEXT);
			txtLocation = properties.getProperty("Of_Menu_Txt_Location", Constants.OF_MENU_TXT_LOCATION);
			txtFileIsNotWrittenTitle = properties.getProperty("Of_Menu_Txt_FileIsNotWrittenTitle", Constants.OF_MENU_TXT_FILE_IS_NOT_WRITTEN_TITLE);


			// "Části" vět:
			txtAnd = properties.getProperty("Of_Menu_Txt_And", Constants.OF_MENU_TXT_AND);
            txtDeleteTextIn = properties.getProperty("Of_Menu_Txt_DeleteTextIn", Constants.OF_MENU_TXT_DELETE_TEXT_IN);
            txtSaveTextIn = properties.getProperty("Of_Menu_Txt_SaveTextIn", Constants.OF_MENU_TXT_SAVE_TEXT_IN);
            txtPrintTextIn = properties.getProperty("Of_Menu_Txt_PrintTextIn", Constants.OF_MENU_TXT_PRINT_TEXT_IN);


            // Texty do chcbs:
            chcbRecordCallConstructors.setText(properties.getProperty("Of_Menu_Chcb_RecordCallConstructors", Constants.OF_MENU_CHCB_RECORD_CALL_CONSTRUCTORS));
            chcbRecordCallMethods.setText(properties.getProperty("Of_Menu_Chcb_RecordCallMethods", Constants.OF_MENU_CHCB_RECORD_CALL_METHODS));

            chcbRecordCallConstructors.setToolTipText(properties.getProperty("Of_Menu_Chcb_RecordCallConstructors_TT", Constants.OF_MENU_CHCB_RECORD_CALL_CONSTRUCTORS_TT));
            chcbRecordCallMethods.setToolTipText(properties.getProperty("Of_Menu_Chcb_RecordCallMethods_TT", Constants.OF_MENU_CHCB_RECORD_CALL_METHODOS_TT));


            chcbClearSysOutScreenWhenCallConstructor.setText(properties.getProperty("Of_Menu_Chcb_ClearSysOutScreenWhenCallConstructor", Constants.OF_MENU_CHCB_CLEAR_SYS_OUT_SCREEN_WHEN_CALL_CONSTRUCTOR));
            chcbClearSysOutScreenWhenCallMethod.setText(properties.getProperty("Of_Menu_Chcb_ClearSysOutScreenWhenCallMethod", Constants.OF_MENU_CHCB_CLEAR_SYS_OUT_SCREEN_WHEN_CALL_METHOD));

            chcbClearSysOutScreenWhenCallConstructor.setToolTipText(properties.getProperty("Of_Menu_Chcb_ClearSysOutScreenWhenCallConstructor_TT", Constants.OF_MENU_CHCB_CLEAR_SYS_OUT_SCREEN_WHEN_CALL_CONSTRUCTOR_TT));
            chcbClearSysOutScreenWhenCallMethod.setToolTipText(properties.getProperty("Of_Menu_Chcb_ClearSysOutScreenWhenCallMethod_TT", Constants.OF_MENU_CHCB_CLEAR_SYS_OUT_SCREEN_WHEN_CALL_METHOD_TT));
		}
		
		
		else {
            menuOptions.setText(Constants.OF_MENU_MENU_OPTIONS);

            // Texty do položek:
            menuClear.setText(Constants.OF_MENU_CLEAR);
            menuSaveToFile.setText(Constants.OF_MENU_SAVE_TO_FILE);
            menuPrint.setText(Constants.OF_MENU_PRINT);
            itemClose.setText(Constants.OF_MENU_ITEM_CLOSE);

            itemClearAll.setText(Constants.OF_MENU_ITEM_CLEAR_ALL);


            // Popisky do položek:
            itemClose.setToolTipText(Constants.OF_MENU_TT_ITEM_CLOSE);


            // Texty do chybových hlášek:
            txtFileIsNotWrittenText = Constants.OF_MENU_TXT_FILE_IS_NOT_WRITTEN_TEXT;
            txtLocation = Constants.OF_MENU_TXT_LOCATION;
            txtFileIsNotWrittenTitle = Constants.OF_MENU_TXT_FILE_IS_NOT_WRITTEN_TITLE;


            // "Části" vět:
            txtAnd = Constants.OF_MENU_TXT_AND;
            txtDeleteTextIn = Constants.OF_MENU_TXT_DELETE_TEXT_IN;
            txtSaveTextIn = Constants.OF_MENU_TXT_SAVE_TEXT_IN;
            txtPrintTextIn = Constants.OF_MENU_TXT_PRINT_TEXT_IN;


            // Texty do chcbs:
            chcbRecordCallConstructors.setText(Constants.OF_MENU_CHCB_RECORD_CALL_CONSTRUCTORS);
            chcbRecordCallMethods.setText(Constants.OF_MENU_CHCB_RECORD_CALL_METHODS);

            chcbRecordCallConstructors.setToolTipText(Constants.OF_MENU_CHCB_RECORD_CALL_CONSTRUCTORS_TT);
            chcbRecordCallMethods.setToolTipText(Constants.OF_MENU_CHCB_RECORD_CALL_METHODOS_TT);


            chcbClearSysOutScreenWhenCallConstructor.setText(Constants.OF_MENU_CHCB_CLEAR_SYS_OUT_SCREEN_WHEN_CALL_CONSTRUCTOR);
            chcbClearSysOutScreenWhenCallMethod.setText(Constants.OF_MENU_CHCB_CLEAR_SYS_OUT_SCREEN_WHEN_CALL_METHOD);

            chcbClearSysOutScreenWhenCallConstructor.setToolTipText(Constants.OF_MENU_CHCB_CLEAR_SYS_OUT_SCREEN_WHEN_CALL_CONSTRUCTOR_TT);
            chcbClearSysOutScreenWhenCallMethod.setToolTipText(Constants.OF_MENU_CHCB_CLEAR_SYS_OUT_SCREEN_WHEN_CALL_METHOD_TT);
		}

		final String dot = ".";


        itemClearAll.setToolTipText(txtDeleteTextIn + " " + TXT_SYSTEM_OUT + " " + txtAnd + " " + TXT_SYSTEM_ERR + dot);

        itemClearSysOut.setText(TXT_SYSTEM_OUT);
        itemClearSysErr.setText(TXT_SYSTEM_ERR);
        itemClearSysOut.setToolTipText(txtDeleteTextIn + " " + TXT_SYSTEM_OUT + dot);
        itemClearSysErr.setToolTipText(txtDeleteTextIn + " " + TXT_SYSTEM_ERR + dot);



        itemSaveSysOutToFile.setText(TXT_SYSTEM_OUT);
        itemSaveSysErrToFile.setText(TXT_SYSTEM_ERR);
        itemSaveSysOutAndSysErrToFile.setText(TXT_SYSTEM_OUT + " " + txtAnd + " " + TXT_SYSTEM_ERR);
        itemSaveSysErrAndSysOutToFile.setText(TXT_SYSTEM_ERR + " " + txtAnd + " " + TXT_SYSTEM_OUT);

        itemSaveSysOutToFile.setToolTipText(txtSaveTextIn + " " + TXT_SYSTEM_OUT + dot);
        itemSaveSysErrToFile.setToolTipText(txtSaveTextIn + " " + TXT_SYSTEM_ERR + dot);
        itemSaveSysOutAndSysErrToFile.setToolTipText(txtSaveTextIn + " " + TXT_SYSTEM_OUT + " " + txtAnd + " " + TXT_SYSTEM_ERR + dot);
        itemSaveSysErrAndSysOutToFile.setToolTipText(txtSaveTextIn + " " + TXT_SYSTEM_ERR + " " + txtAnd + " " + TXT_SYSTEM_OUT + dot);



        itemPrintSysOut.setText(TXT_SYSTEM_OUT);
        itemPrintSysErr.setText(TXT_SYSTEM_ERR);
        itemPrintSysOutAndSysErr.setText(TXT_SYSTEM_OUT + " " + txtAnd + " " + TXT_SYSTEM_ERR);
        itemPrintSysErrAndSysOut.setText(TXT_SYSTEM_ERR + " " + txtAnd + " " + TXT_SYSTEM_OUT);

        itemPrintSysOut.setToolTipText(txtPrintTextIn + " " + TXT_SYSTEM_OUT + dot);
        itemPrintSysErr.setToolTipText(txtPrintTextIn + " " + TXT_SYSTEM_ERR + dot);
        itemPrintSysOutAndSysErr.setToolTipText(txtPrintTextIn + " " + TXT_SYSTEM_OUT + " " + txtAnd + " " + TXT_SYSTEM_ERR + dot);
        itemPrintSysErrAndSysOut.setToolTipText(txtPrintTextIn + " " + TXT_SYSTEM_ERR + " " + txtAnd + " " + TXT_SYSTEM_ERR + dot);
	}
}