package cz.uhk.fim.fimj.swing_worker;

import java.awt.*;
import java.net.URL;
import java.util.Properties;

import javax.swing.*;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.ReadFile;

/**
 * Tato třída slouží jako Jdialog, který bude zobrazen pokud bude pracovat nějaké vlákno na pozadí a zároveň by se
 * nemělo "manipulovat" s aplikací - aby uživatel nic s aplikací nedělal, a věděl, že aplikace něco zpracovává, tak
 * pokud poběží nějaké vlákno, resp. ThreadSwingWorker na pozadí, tak se zároveň zobrazí tento dialog, aby uživatel
 * věděl, že aplikace něco zpracovává a nedošlo k nějaké chybě.
 * <p>
 * Jedná se o dialog, který se zobrazí pokud běží nějaké vlákno na pozadí resp. nějaký swingWorker něco zpracovává, a
 * tento dialog si načte uživatelem nastavený obrázek coby nějaká animace, která se zobrazí s dialogem. Dialog bude
 * modální, protože něchci, aby uživatel mezitím co běží něco na pozadí nějak manipuloval s aplikací.
 * <p>
 * <p>
 * Zdroj pro vygenerování obrázků - animací: http://loading.io/
 * <p>
 * <p>
 * Dále je dolě třída MyContentPane, která dědí z Jpanelu, a slouží pro to, aby se definovala, která část dialogu má být
 * průhledná, a která ne, ale není nutné ji použít, pokud se použije, tak bude z aobrázkem to průhledné pozadí,
 * definované třídou MyContentPane, ale pokud se tato třída nepoužije, tak bude zobrazen pouze obrázek, to jsou opět
 * možnosti, jak je možné zobrazit dialog, nechám zde obě, ale tu třídu nepoužiji, pouze jako alternativní možnost,
 * kdybych jáí nebo někdo jiný změnil názor a chtěl to "udělat jinak". Pro tu průhlednost je dále nutné definovat si
 * nějaké parameetry v konstruktoru jsou i nich poznámky o které se jedná
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class LoadingDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	

	/**
	 * Konstruktor této třídy.
	 */
	public LoadingDialog() {
		super();
		
		initGui();
		
		// Je nezbytně nutné nastavit transparentnost okna:
		// bez toho to nebude fungovat
		setBackground(new Color(0, 0, 0, 0));
		
		
		
		
		
		// Zde jsou následující dva řádky kódu potřeba odkomentovat,
		// pokud se má použít pouze průhledné pozadí, nikoliv naprosto transparentní,
		// bude na pozadí videt zbytek dialogu, akorát bude průhledný
//		setContentPane(new MyContentPane());
		// zde si je dobré akorát zvolit barvu pozadí
//		getContentPane().setBackground(Color.BLACK);
	
		
		
		
		
		
		
		// Vytvořím si Jlabel, do kterého vložím ikonu nebo text,
		// pokud nebude žádný obrázek načten
		final JLabel lblIcon = new JLabel("", JLabel.CENTER);
		
		
		// Načtu si adresu k obrázku:
		final URL url = getImageUrl();
		
		// Otestuji, zda se podařilo načíst adresu k obrázku:
		if (url != null) {
			// Zde se podařilo načíst adresu, tak si načtu příslušný obrázek
			final ImageIcon img = new ImageIcon(url);
			// a nastavím ho jako ikonu do Jlabelu:
			lblIcon.setIcon(img);
		}
		
		// Zde se nepodařilo načíst adresu k obrázku - nemělo by 
		// nastat, ale pokud ano, tak se zobrazí pouze text s načítáním
		else lblIcon.setText("Loading ...");
		
		
		
		
		
		// Přidám vytvořený Jlabel do okna dialogu:
		// (buď s textem nebo s obrázkem)
		add(lblIcon, BorderLayout.CENTER);
		
			
		
		
		
		// Okno se přizpůsobí obrázku:
		pack();
		// nastaví se na střed obrazovky okna:
		setLocationRelativeTo(null);
	}
	
	
	
	


	
	
	
	/**
	 * Metoda, která zkusí načíst cestu k obrázku, který zvolil uživatel a vrátí
	 * adresu k tomuto obrázku, jinak se vrátí null, pokud se obrázek nenajde, ale
	 * to by nastat nemělo, vždy by se měla použít alespoň výchozí cesta k výchozímu
	 * obrázku
	 * 
	 * @return URL adresu k obrátzku, který semá načíst jako animace coby
	 *         reprezentace načítání / zpracovávání, nebo null, pkud nebude nalezen,
	 *         ale to by nastat nemělo
	 */
	private static URL getImageUrl() {
		final Properties defaultProp = ReadFile.getDefaultPropertiesInWorkspace();

		if (defaultProp != null) {
			final String imageName = defaultProp.getProperty("Loading Image", Constants
					.DEFAULT_IMAGE_FOR_LOADING_PANEL);

			return LoadingDialog.class.getResource("/icons/loadingImages/" + imageName);
		}

		return null;
	}
	

	
	
	
	/**
	 * Metoda, která "připraví" základní design tohoto dialogu,
	 * pro zobrazení.
	 * Nastaví parametrxy jako je modalita okna, rozměry, ... 
	 * viz tělo metody
	 */
	private void initGui() {
		// Okno bude po zobrazení modální, aby nešlo nic dělat s 
		// hlavním oknem aplikace
		setModal(true);
		
		// V dialogu nebudou zobrazeny tlačítka pro minimalizaci,
		// zavření, a maximalzaci:
		setUndecorated(true);
		
		// Nastavím "výchozí" rozměry dialogu - dále v konstruktoru
		// se přízpůsobí okno dle obrázku - metoda pack()
		setSize(Toolkit.getDefaultToolkit().getScreenSize());
		
		setLayout(new BorderLayout());
	}






	/**
	 * Tato třída slouží pouze jako "komponenta", která definuje která část dialogu má být průhledná, a která ne, ale
	 * není třeba ji použít, pokud se použije, tak bude pozadí dialogu pouze průhledné nikoliv neviditelné - zcela
	 * transparentní, tak jako tomu je bez této třídy
	 *
	 * Zdroj:
	 * našel jsem ji někde na internetu, tuším na StagOverflow, ale bohužel jsem si neuložil odkaz na tuto třídu /
	 * stránku a navíc, ji ani nepoužiívám, je zde pouze pro možnost, jak tento dialog s načitám truchu změnit - tedy
	 * alespoň jeho design
	 *
	 * @author Jan Krunčík
	 * @version 1.0
	 */
	private static final class MyContentPane extends JPanel {

		private static final long serialVersionUID = 1L;

		public MyContentPane() {
			setOpaque(false);
		}



		@Override
		protected void paintComponent(Graphics g) {
	        // Allow super to paint
	        super.paintComponent(g);

	        // Apply our own painting effect
	        Graphics2D g2d = (Graphics2D) g.create();

	        // 50% transparent Alpha
//	        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
	        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.2f));

	        g2d.setColor(getBackground());
	        g2d.fill(getBounds());

	        g2d.dispose();
		}
	}
}
