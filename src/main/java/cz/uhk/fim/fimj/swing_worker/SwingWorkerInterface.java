package cz.uhk.fim.fimj.swing_worker;

/**
 * Tato třída - rozhraní obsahuje pouze jednu metodu, která slouží jako náhrada za metodu run ve vlákně - Thread,
 * protože jsem vlákna přepsal na běžné třídy, které implementují toto rozhraní a tato metoda se zavolá ve SwingWorkeru,
 * kde se na pozadí aplikaci vykonají příslušné úlohy a zobrazí se dialog s načítáním a až ta úloha doběhne ,dialog se
 * zavře a uživatel může dále pokračovat v práci s aplikací.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface SwingWorkerInterface {

    /**
     * Metoda, která pro mé potřeby nahradila metodu run ve vlákněch a zavolá ji ThreadSwingWorker, metoda obshauje v
     * příslušné třídě kod, který má běžět na pozadí, ale abych mohl zobrazit dialog, tak jsem vlákna pepsal na obyčejné
     * třídy, které implementují toto rozhraní
     */
    void run();
}