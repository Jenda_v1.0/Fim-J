package cz.uhk.fim.fimj.swing_worker;

import javax.swing.*;

/**
 * Tato třída slouží jako ThreadSwingWorker, do kterého se předá vlákno, které se má spustit a jak mile se toto vlákno
 * spustí, tak se otevře dialog, který se stane modálním a na pozadí se bude vykonávat předané vlákno, dialog není v
 * podstatě dialog jako takový ale bude to dialog, který je upraven, tak aby uživateli reprezentoval, že aplikace na
 * pozadí něco dělá a nevypadá to, že se aplikace sekla, nebo něco v tom smyslu.
 * <p>
 * Tuto třídu jsem napsal pouze za tímto účelem - tj. aby šel zobrazit dialog coby loading window, který uživateli
 * "říká", že aplikaci něco zpracovává, a že nemá nic dělat.
 * <p>
 * Původně byla tato implementace pouze přes vlákna, ale nepodařilo se mi najít způsob, který by zobrazil tento výše
 * zmíněný loadi Dialog, tak jsem napsal tuto třídu ve které se vytvoří instance loadigDialogu, který se zobrazí, a
 * stane se tak modálním, dokud nedoběhně úloha, kterou mělo vykonat vlákno, ta úloha, která mělo původně vykonat vlákno
 * jsem přepsal na obyčejné třídy, (obyčejná třída místo vlákna), které implementuje rozhraníMySwingWorkerInterface
 * která obsahuje metodu start, kterou zavolá tento ThreadSwingWorker, a tím začne vykonávat úlohu na pozadí a zárověň
 * se zobrazí dialog s načítáním, aby si uživatel nemylel, že došlo k nějaké chybě, apod. ale aby věděl, že aplikace
 * zpracovává data
 * <p>
 * Proto se v konstruktoru předá instance nějaké třídy (bývalé vlákno), která implementuje zmíněné rozhraní a v metodě
 * diInBg se zavolá nad danou instancí třídy metoda start, a začne se vykonávat příslušná úloha na pozadí a zobazí se
 * pak dialog, který to "oznámí" uživateli načítacím logem
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ThreadSwingWorker extends SwingWorker {

    /**
     * Dialog, který reprezentuje načítání- obsahuje obrázek - animaci s načítáním
     */
    private final LoadingDialog loadingDialog;


    /**
     * Proměnná, do které vložím nějakou instanci třídy, coby nahrazení vlákna, a tato třída implementuje rozhraní
     * MySwingWorkerInterface a obsahuje tak metodu start, která vykoná kod příslušné úlohy na pozadí, zatím co bude
     * dialog zobrazen - bude modální
     */
    private final SwingWorkerInterface myTask;


    /**
     * Konstruktor této třídy.
     * <p>
     * Slouží akorát k naplnění proměnné a vytvoření instance dialogu coby LoadigDialog.
     *
     * @param myTask
     *         - instance nějaké třídy, která implementuje rozhraní: MySwingWorkerInterface a obsahuje tak metodu start,
     *         která se má vykonat na pozadí, zatímco bude zobrazen dialog s načítáním
     */
    public ThreadSwingWorker(final SwingWorkerInterface myTask) {
        super();

        this.myTask = myTask;

        loadingDialog = new LoadingDialog();
    }


    @Override
    protected Object doInBackground() {
        // Otestuji, zda byla předána reference:
        if (myTask != null)
            // mohu zavolat metodu run, která provede úlohy na pozadí:
            myTask.run();

        // na konec vrátím null - nic nepotřebuji vracet
        return null;
    }


    @Override
    protected void done() {
        loadingDialog.dispose();
    }


    /**
     * Metoda, která zobrazí dialog, který se stane modálním a načte si příslušný obrázek s načítacím "logem" - animací
     * a dá tak najevo uživateli, že aplikace zpracovává nějaká data, až doběhne úloha na pozadí - metoda done, tak se
     * tento dialog zase zavře
     */
    public final void setVisibleLoadingDialog() {
        loadingDialog.setVisible(true);
    }


    /**
     * Metoda, která "spustí nějaké vlákno tak, aby se to vlákno vykonalo v MySwingWorkeru".
     * <p>
     * Tato metoda vytvoří instanci třídy MySwingWorker, jako parametr konstruktoru dostanu "úlohu" task, která obsahuje
     * metodu run, která se vykoná.
     *
     * @param task
     *         - Třída, která implementuje rozhraní MySwingWorkerInterface a tím obsahuje implementovanou metodu 'run',
     *         která se vykoná. Dokud se bude vykonávat nějaká úloha, tj. bude se vykonávat ta metoda run v task, tak
     *         bude zobrazen loading dialog.
     */
    public static void runMyThread(final SwingWorkerInterface task) {
        /*
         * vytvořím si můj ThreadSwingWorker a předám mu třídu, která implementuje rozhraní
         * MySwingWorkerInterface, aby věděl, co má prověst - metodu run
         */
        final ThreadSwingWorker mySw = new ThreadSwingWorker(task);

        // Spustím swingWorker
        mySw.execute();

        // zobrazím loading dialog a stane se modálním, do kud nedoběhne metoda run:
        mySw.setVisibleLoadingDialog();
    }
}