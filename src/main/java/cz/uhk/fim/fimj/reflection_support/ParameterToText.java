package cz.uhk.fim.fimj.reflection_support;

import java.lang.reflect.*;
import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Metody pro převedení parametrů konstruktorů, metod, návratového datového typu metody a proměnných do textové podoby.
 * To je potřeba například v kontextovém menu nad třídou při zavolání metod nebo konstruktorů. Dále pro zobrazení metody
 * v dialogu pro zavolání konstruktoru nebo metody s parametry atd.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 02.09.2018 11:54
 */

public final class ParameterToText {

    /**
     * Desetinná čárka s mezerou. Využívá se například pro oddělení jednotlivých parametrů.
     */
    private static final String COMMA_WITH_SPACE = ", ";


    private ParameterToText() {
    }


    /**
     * Získání parametrů, které se nachází v poli parameters v textovém podobě tak, že jednotlivé parametry budou od
     * sebe odděleny desetinnou čárkou. Parametry budou zobrazeny / uvedeny buď s nebo bez balíčků a jednotlivých
     * datových typů (dle proměnné withPackages).
     *
     * @param parameters
     *         - parametry, které se mají převést do výše popsané textové podoby.
     * @param withPackages
     *         - true v případě, že se mají jednotlivé datové typy parametrů v parameters uvádět i s balíčky. Jinak
     *         false - datové typy budou bez balíčků.
     *
     * @return text, který bude parametry v parameters převedené do textu ve výše uvedené syntaxi.
     */
    public static String getParametersInText(final Parameter[] parameters, final boolean withPackages) {
        // Proměnná pro sestavení textové podoby:
        final StringBuilder parametersBuilder = new StringBuilder();

        // Vložení parametrů do builderu:
        Arrays.stream(parameters).forEach(parameter -> parametersBuilder.append(getParameterInText(parameter,
                withPackages)).append(COMMA_WITH_SPACE));

        if (parametersBuilder.toString().endsWith(COMMA_WITH_SPACE))
            parametersBuilder.setLength(parametersBuilder.length() - COMMA_WITH_SPACE.length());

        return parametersBuilder.toString();
    }


    /**
     * Získání parametru parametr v textové podobě. Pokud bude parametr withPackages true, pak se příslušný parametr
     * vrátí v textu tak, že u datového typu budou uvedeny balíčky.
     *
     * @param parameter
     *         - parametr, který se má převést do textové podoby.
     * @param withPackages
     *         - true v případě, že se má datový typ parametru parametr zobrazit i s balíčky. Jinak false.
     *
     * @return datový typ parametru parameter v textu buď s balíčky nebo bez nich.
     */
    public static String getParameterInText(final Parameter parameter, final boolean withPackages) {
        /*
         * V případě, že se má parametr převést do textové podoby tak, že se mají zobrazit datové typy i s balíčky,
         * je možné využít následující metodu, která převedě v podstatě veškeré typy do podoby textu.
         */
        if (withPackages)
            return parametersToText(parameter);


        if (!ReflectionHelper.isParameterizedType(parameter.getParameterizedType())) {
            if (parameter.getType().isArray()) {
                /*
                 * Získání datového typu pole v textu, ale nebudou uvedeny parametrizované datové typy datových
                 * struktur:
                 */
                return getDataTypeOfArrayInText(parameter);
            }

            return parameter.getType().getSimpleName();
        }

        /*
         * V této fází metody se jedná o datovou strukturu, která má uvedený datový typ. Například List<String>,
         * Map<?, Object> atd.
         *
         * Následuje získání zmíněných parametrů.
         */

        final Type genericParameterType = parameter.getParameterizedType();

        final ParameterizedType parametrizedType = (ParameterizedType) genericParameterType;

        /*
         * V této proměnné se budou nacházet veškeré parametrizované typy parametru parameter. V případě, že se jedná
         * o mapu, budou zde například dvě položky - dva typy.
         */
        final Type[] parameterArgTypes = parametrizedType.getActualTypeArguments();


        // Nyní je možné sestavit text z výše získaných datových typů:
        final StringBuilder parameterTextBuilder = new StringBuilder();

        // Zde se přidá datový typ parametru (List, Map apod.) bez balíčků:
        parameterTextBuilder.append(parameter.getType().getSimpleName()).append("<");


        for (final Type parameterArgType : parameterArgTypes) {
            convertTypeToText(parameterArgType, parameterTextBuilder);
            parameterTextBuilder.append(COMMA_WITH_SPACE);
        }

        // Odebrání poslední desetinné čárky pro oddělení parametrů:
        if (parameterTextBuilder.toString().endsWith(COMMA_WITH_SPACE))
            parameterTextBuilder.setLength(parameterTextBuilder.length() - COMMA_WITH_SPACE.length());

        parameterTextBuilder.append(">");

        return parameterTextBuilder.toString();
    }


    /**
     * Převedení parametru type do textové podoby.
     * <p>
     * V případě, že type je v textové podobě pouze otazník, jedná se o generický datový typ, tak se vrátí pouze ten
     * otazník.
     * <p>
     * V ostatních případech se zjistí, zda se jedná o parametrizovaný datový typ (v podstatě datovou strukturu), pokud
     * ne, jedná se o "klasickou" proměnnou - ne datovou strukturu.
     * <p>
     * Pokud se jedná o parametrizovaný datový typ, bude se rekurzí pokračovat pro veškeré datové typy. Pokud se nejedná
     * o parametrický datový typ, pouze se převede příslušná hodnota do textu. Pokud se jedná o pole, převede se pole do
     * textu vždy i s balíčky.
     *
     * @param type
     *         - hodnota / datový typ, který se má převést do textové podoby.
     * @param stringBuilder
     *         - objekt, do kterého se přidávají veškeré hodnoty type v textové podobě.
     */
    private static void convertTypeToText(final Type type, final StringBuilder stringBuilder) {
        /*
         * Při generickem typu je pouze "?" (v textu).
         *
         * Nelze využít metodu contains protože by se mohlo jednat například o List<List<?>>, pak se při prvním listu
         * chybně vrátil otazník jako datový typ prvního listu.
         */
        if (type instanceof WildcardType) {
            stringBuilder.append("?");
        }


        else if (type instanceof ParameterizedType) {
            final ParameterizedType parameterizedType = (ParameterizedType) type;

            /*
             * Zde se přidá datový typ parametru (List, Map apod.) bez balíčků:
             *
             * Note:
             *
             * Metoda: "parameterizedType.getRawType()" vrací datový typ Type, ale vždy se jedná o "Class<?>", proto
             * se vrácená hodnota přetypuje na Class a získá se název.
             */

            //Původní přetypování:
//            stringBuilder.append(((ParameterizedTypeImpl) parameterizedType).getRawType().getSimpleName());
            stringBuilder.append(((Class<?>) parameterizedType.getRawType()).getSimpleName());

            stringBuilder.append("<");

            final Type[] types = parameterizedType.getActualTypeArguments();

            for (final Type t : types) {
                convertTypeToText(t, stringBuilder);
                stringBuilder.append(COMMA_WITH_SPACE);
            }

            // Odebrání desetinné čárky pro oddělení parametrů, je li uvedena:
            if (stringBuilder.toString().endsWith(COMMA_WITH_SPACE))
                stringBuilder.setLength(stringBuilder.length() - COMMA_WITH_SPACE.length());

            stringBuilder.append(">");
        }


        // Toto je například pro vnořená pole: Map<List<Object>[][], ?>
        else if (type instanceof GenericArrayType) {
            /*
             * Zde se získá "vnitřní dimeze pole.
             *
             * Například pro "celkové" testovaný parametr:  "Map<List<Object>[][], ?>" se do této podmínky dostana
             * část: "List<Object>[][]". Metoda "((GenericArrayType) type).getGenericComponentType()" vrátí:
             * "List<Object>[]" a pří další iteraci jen: "List<Object>".
             */
            convertTypeToText(((GenericArrayType) type).getGenericComponentType(), stringBuilder);
            stringBuilder.append("[]");
        }


        // Generické typy:
        else if (type instanceof TypeVariable<?>) {
            /*
             * Tento parametr se týký generických typů, například: "T", tak se přidá pouze text: "T".
             *
             * Note: Neřeši zde zde balíčky apod. Proto využita metoda "getName()".
             */
            stringBuilder.append(((TypeVariable<?>) type).getName());
        }


        // "Ostatní" typy:
        else if (type instanceof Class<?>) {
            final Class<?> type1 = (Class<?>) type;

            if (type1.isArray())
                // Zd se vezme výpis s balíčky:
                stringBuilder.append(type1.getTypeName());

                // Zde se přidá název datového typu bez balíčků:
            else stringBuilder.append(type1.getSimpleName());
        }


        // Převod do textu pro datové typy, pro které není doplněna implementace:
        else stringBuilder.append(type.getTypeName());
    }


    /**
     * Převedení typu v parametru objType do textové podoby. Může to být syntaxe s balíčky
     * "packages(.anotherPackage).ClassName" nebo bez nich - pouze název třídy "ClassName".
     *
     * <i>Jedná se o převedení parametru do textu například proto, aby se mohlo zjistit, zda se v diagramu tříd nachází
     * třída se stejným názvem a umístěním.
     * <p>
     * Note: V případě, že se má získat název třídy, tak převádět parametr objType do textu má smysl pouze v případě, že
     * se jedná o typ "Class<?>". Protože pouze v tomto případě to může být typ nějaké třídy z diagramu tříd. V
     * ostatních případech se jedná například o vnořený List či pole apod. Proto mohou být ignorovýny.
     * <p>
     * Ve všech ostatních případech, kdy se nejedná o parametr typu "Class<?>" se vždy parametr převede do textu i s
     * balíčky a vrátí se, ale takový název se nikde nezjistí jako název třídy v diagramu tříd, proto se zde nebude
     * řešit vracení null hodnoty apod.</i>
     *
     * @param objType
     *         - parametr, který se má převést do textové podoby jako název třídy (s balíčky).
     * @param withPackages
     *         - true v případě, že se má převést objType do textu i s balíčky, jinak false - bez balíčků.
     *
     * @return název třídy s balíčky. Případně otazník v případě, že bude parametr objType null. V takovém případě se
     * beze, že se jedná o generický datový typ nebo List bez uvedeného datového typu.
     */
    public static String getDataTypeInText(final Object objType, final boolean withPackages) {
        /*
         * Může nastat například v případě, že se jedná o List nebo mapu bez definovaných parametrů, pak se pro
         * získání typu například Listu vrátí null, null hodnota se pošle sem a zde je třeba také vrátit null,
         * nejedná se o datový typ.
         */
        if (objType == null)
            return null;

        /*
         * V případě sestavení textu s balíčky ze postupně zjistí, jaký typ v objType je (Class, ParametrizedType, ..
         * .) Dle toho se zavolá příslušná metoda, která jej převede do textu.
         */
        if (withPackages)
            return getObjectNameWithPackages(objType);

        // Pro sestavení textu:
        final StringBuilder stringBuilder = new StringBuilder();

        if (objType instanceof WildcardType)
            convertTypeToText((WildcardType) objType, stringBuilder);

        else if (objType instanceof ParameterizedType)
            convertTypeToText((ParameterizedType) objType, stringBuilder);

        else if (objType instanceof GenericArrayType)
            convertTypeToText((GenericArrayType) objType, stringBuilder);

        else if (objType instanceof TypeVariable<?>)
            convertTypeToText((TypeVariable) objType, stringBuilder);

        else if (objType instanceof Class<?>)
            convertTypeToText((Class<?>) objType, stringBuilder);

            // Zde není uvedena podpora pro příslušný typ:
        else return objType.toString();

        return stringBuilder.toString();
    }


    /**
     * Převedení datového typu objType do textové podoby i s balíčky. Bude uveden název příslušné třídy (popř. Listu
     * apod.) i s balíčky.
     *
     * @param objType
     *         - datový typ, který se má převést do textové podoby v syntaxi názvu třídy s balíčky.
     *
     * @return - název datového typu s balíčky.
     */
    private static String getObjectNameWithPackages(final Object objType) {
        /*
         * Může nastat v případě, kdy je objType například bez parametrizovaný List nebo Map
         */
        if (objType == null)
            return null;

        if (objType instanceof WildcardType) {
            // Zobrazí typ i s balíčky, například, když bude List Listů, tak i typy Listu budou zobrazeny (s balíčky).
            return ((WildcardType) objType).getTypeName();
        }

        else if (objType instanceof ParameterizedType) {
            // Zobrazí typ i s balíčky, například, když bude List Listů, tak i typy Listu budou zobrazeny (s balíčky).
            return ((ParameterizedType) objType).getTypeName();
        }

        else if (objType instanceof GenericArrayType) {
            return ((GenericArrayType) objType).getTypeName();
        }

        else if (objType instanceof TypeVariable<?>) {
            return ((TypeVariable<?>) objType).getTypeName();
        }

        else if (objType instanceof Class<?>) {
            if (((Class) objType).isArray())
                return ((Class) objType).getTypeName();

            return ((Class) objType).getName();
        }

        // Zde není doplněna podpora pro příslušný typ:
        return objType.toString();
    }


    /**
     * Převedení parametru typu N - rozměrné pole (parametr) do textové podoby. Datové typy budou bez uvedeny bez
     * balíčků.
     *
     * <i>Bohužel nebylo zjištěno, když se jedná o datovou strukturu typu pole, například List<String>[], už nebude
     * uveden datový typ String.</i>
     *
     * @param parameter
     *         - parametr, který se má převést do textové podoby. Musí se jednat o N - rozměrné pole. A nesmí být null.
     *
     * @return datový typ pole parametr v textové podobě (bez balíčků).
     */
    private static String getDataTypeOfArrayInText(final Parameter parameter) {
        final int countOfDimensionArray = ReflectionHelper.getCountOfDimensionArray(parameter.getType());

        final Class<?> dataTypeOfArray = ReflectionHelper.getDataTypeOfArray(parameter.getType());

        final StringBuilder arrayTextBuilder = new StringBuilder();

        /*
         * V této části se vždy ztratí informace o datovém typu pole v případě datových struktur. Například když se
         * jedná o pole: List<String>[], pak se již nezjistí datový typ String.
         */
        // TODO - Bylo by dobré zjistit datový typ pole tak, aby se mohli získat datové typy i datových struktur v poli. Pak by se mohlo doplnit i vypisování s / bez balíčků.
        convertTypeToText(dataTypeOfArray, arrayTextBuilder);

        // Přidá se počet hranatých závorek, kolik má pole dimenzí / rozměrů:
        IntStream.range(0, countOfDimensionArray).forEach(i -> arrayTextBuilder.append("[]"));

        return arrayTextBuilder.toString();
    }


    /**
     * Získání návratového datového typu metody method v podobě textu. Datový typ bude uveden buď s nebo bez balíčků -
     * dle proměnné withPackages.
     *
     * @param method
     *         - metoda, jejíž návratový datový typ se má převést do podoby textu.
     * @param withPackages
     *         true v případě, že se má návratový datový typ vypisovat i s balíčky. Jinak false.
     *
     * @return návratový typ metoda method v podobě textu.
     */
    public static String getMethodReturnTypeInText(final Method method, final boolean withPackages) {
        if (method == null)
            return null;

        if (method.getReturnType().equals(Void.TYPE))
            return "void";

        if (withPackages)
            return method.getGenericReturnType().getTypeName();

        final StringBuilder returnTypeBuilder = new StringBuilder();

        convertTypeToText(method.getGenericReturnType(), returnTypeBuilder);

        return returnTypeBuilder.toString();
    }


    /**
     * Získání textové podoby datového typu proměnné field.
     *
     * @param field
     *         - proměnná, jejíž datový typ se má převést do textové podoby.
     * @param withPackages
     *         true v případě, že se má datový typ proměnné uvést v textové podobě s balíčky. Jinak false (bude zobrazen
     *         datový typ bez balíčků, například pouze Integer, ne java.lang.Integer apod.).
     *
     * @return výše zmíněný datový typ proměnné v textové podobě.
     */
    public static String getFieldInText(final Field field, final boolean withPackages) {
        if (field == null)
            return null;

        if (withPackages)
            return field.getGenericType().getTypeName();

        final StringBuilder returnTypeBuilder = new StringBuilder();

        convertTypeToText(field.getGenericType(), returnTypeBuilder);

        return returnTypeBuilder.toString();
    }


    /**
     * Převedení datového typu proměnné field do textové podoby tak, že se vezme datový typ pole, ale pouze o jednu
     * dimenzi vnořenou. Field musí být N - rozměrné pole. Jinak se vrátí null hodnota.
     *
     * <i>Princip je takový, že se převede datový typ proměnné field do textové podoby a pouze se odebere poslední pár
     * hranatých závorek. Takže například pro "int[]" se vrátí: "int" a pro "int[][][]" se vrátí: "int[][]" atd.</i>
     *
     * @param field
     *         - proměnná typu N - rozměrné pole, která se má převést do textové podoby a odebrat poslední pár hranatých
     *         závorek.
     * @param withPackages
     *         - true v případě, že se má typ pole zobrazit s balíčky, jinak false.
     *
     * @return datový typ proměnné field v textové podobě s odebraným posledním párem hranatých závorek.
     */
    public static String getFirstArrayTypeOfFieldInText(final Field field, final boolean withPackages) {
        if (field == null)
            return null;

        /*
         * - Když je to jednorozměrné pole, zjisí se jeho typ a převede se do textové podoby.
         *
         * - V případě, že se jedná o více rozměrné pole, zjistí se datový typ pole o jednu dimenzi méně. Například,
         * když je pole "int[][]", pak výsledek bude: "int[]" apod. Vždy se zjistí typ o jednu dimenzi méně. A tento
         * typ se vrátí v textové podobě.
         */

        if (!field.getType().isArray())
            return null;

        // Získání typu proměnné v textové podobě:
        final String fieldInText = getFieldInText(field, withPackages);

        // Odebrání posledního páru hranatých závorek:
        return fieldInText.substring(0, fieldInText.length() - 2);
    }


    /**
     * Převedení parametru (parameter) do textové podoby. Názvy datových typů budou vždy uvedeny s balíčky.
     * <p>
     * Princip je takový, že se otestuje, zda se jedná o pole, pokud ano, využije se metoda pro převedení příslušného
     * typu pole do textové podoby. V případě Listu polí nebo mapy polí nebudou uvedeny datové typy Listu ani mapy -
     * obecně nebudou k dispozici datové typy datových strukturu.
     * <p>
     * Pokud se jedná o objekt (List, Mapu, ...), který je bez parametru, například List, u kterého není specifikován
     * datový typ apod. tak se vrátí příslušný typ.
     * <p>
     * Jinak se z parametru získá objekt {@link ParameterizedType}, ze kterého se získá textová podoba příslušného
     * parametru.
     *
     * @param parameter
     *         - parametr, který se má převést do textové podoby.
     *
     * @return parametr v textové podobě (s balíčky).
     */
    static String parametersToText(final Parameter parameter) {
        if (parameter.getType().isArray())
            return parameter.getType().getTypeName();

        if (!ReflectionHelper.isParameterizedType(parameter.getParameterizedType()))
            return parameter.getType().getName();

        final Type genericParameterType = parameter.getParameterizedType();

        final ParameterizedType parametrizedType = (ParameterizedType) genericParameterType;

        return parametrizedType.getTypeName();
    }
}