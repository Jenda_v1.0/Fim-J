package cz.uhk.fim.fimj.reflection_support;

import cz.uhk.fim.fimj.file.DataTypeOfList;
import cz.uhk.fim.fimj.file.DataTypeOfListEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.logger.ExtractEnum;

import java.lang.reflect.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Třída obsahuje metody, které slouží pro provádění různých operací s reflexí. Například sestavování textů metod nebo
 * konstruktorů pro zavolání v kontextovém menu nad třídou či instancí apod.
 * <p>
 * ("Obsahuje metody pro práci s reflexí, které se využívají v různých komponentých aplikace.")
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 18.07.2018 22:56
 */

public final class ReflectionHelper {

    private ReflectionHelper() {
    }


    /**
     * Metoda, která vrátí datový typ hodnot, které lze do kolekce vkládat, resp. zjistí typ kolekce.
     * <p>
     * Zdroj: https://stackoverflow.com/questions/1942644/get-generic-type-of-java-util-list
     *
     * @param field
     *         - položka - kolekce, jejiž typ hodnot chci zjistit.
     *         <p>
     *         - proměnná (atribut) načtený z nějaké třídy. Tato proměnná je typu List a chci zjistit jakého datové
     *         typu, například list typu String nebo Integer, Double, ...
     *
     * @return objekt, který bude obsahovat informace o tom jakého datového typu je list field, zda - li vůbec má nějaký
     * datov typ.
     */
    public static DataTypeOfList getDataTypeOfList(final Field field) {
        /*
         * Pro urychlení postupu zde následuje zjištění, zda je to vůbec List s parametry, pokud ne, může se rovnou
         * vrátit příslušná "informace".
         *
         * V opačném případě následuje zjištění konkrétního datového typu Listu.
         */
        if (!isParameterizedType(field.getGenericType()))
            return new DataTypeOfList(DataTypeOfListEnum.WITHOUT_DATA_TYPE);

        // Získání typu Listu:
        final Object typeOfList = ListHelper.getTypeOfList(field);

        return getDataTypeOfList(typeOfList);
    }


    /**
     * Metoda, která zjistí datový typ návratové hodnoty metody. Jedná se ale o datový typ listu, tedy metoda v
     * parametru této metody (method), tak o této metodě už se musí vědět, že vrací datový typ List. Tato metoda pouze
     * vrátí datový typ toho listu.
     *
     * @param method
     *         - metoda, která vrací datový typ List a tato metoda zjistí datový typ toho listu.
     *
     * @return objekt, který bude obsahovat datový typ Listu, který vrací metoda method.
     */
    public static DataTypeOfList getDataTypeOfList(final Method method) {
        /*
         * Pro urychlení postupu zde následuje zjištění, zda je to vůbec List s parametry, pokud ne, může se rovnou
         * vrátit příslušná "informace".
         *
         * V opačném případě následuje zjištění konkrétního datového typu Listu.
         */
        if (!isParameterizedType(method.getGenericReturnType()))
            return new DataTypeOfList(DataTypeOfListEnum.WITHOUT_DATA_TYPE);

        // Získání typu Listu:
        final Object typeOfList = ListHelper.getTypeOfList(method);

        return getDataTypeOfList(typeOfList);
    }


    /**
     * Metoda, která si zjistí datový typ hodnot, které lze vkládat do Listu z baličku java.util, nacházející se v
     * parametru metody nebo konstruktoru.
     * <p>
     * Tento nalezený datový typ vrátí
     *
     * @param parameter
     *         - parametr metody nebo konstruktoru, v tomto případě se jedná o List, tak si z něj pouze "vytáhnu"
     *         hledaný datový typ
     *
     * @return nalezený datový typ hodnot, které lze vkládat do Listu
     */
    public static DataTypeOfList getDataTypeOfList(final Parameter parameter) {
        /*
         * Pro urychlení postupu zde následuje zjištění, zda je to vůbec List s parametry, pokud ne, může se rovnou
         * vrátit příslušná "informace".
         *
         * V opačném případě následuje zjištění konkrétního datového typu Listu.
         */

        if (!isParameterizedType(parameter.getParameterizedType()))
            return new DataTypeOfList(DataTypeOfListEnum.WITHOUT_DATA_TYPE);

        // Získání typu Listu:
        final Object typeOfList = ListHelper.getTypeOfList(parameter);

        return getDataTypeOfList(typeOfList);
    }


    /**
     * Zjištění / Převedení datového typu Listu do výčtové podoby tak, aby se poznalo o jaký typ Listu se jedná. Resp.
     * do jaké "kategorie" ten typ spadá. Zda li se jedná o konkrétní typ, neznámý typ apod.
     *
     * @param typeOfList
     *         - zjištěný datový typ Listu.
     *
     * @return informace o tom, o jaký typ Listu se jedná.
     */
    private static DataTypeOfList getDataTypeOfList(final Object typeOfList) {
        /*
         * Tato podmínka je spíše jako "pojistka", která by neměla nastat. Protože už se ví, že se jedná o List
         * definovanými parametry.
         */
        if (typeOfList == null)
            return new DataTypeOfList(DataTypeOfListEnum.WITHOUT_DATA_TYPE);

        // Zjištění, zda se jedná o "libovolný" objektový typ.
        if (typeOfList instanceof WildcardType)
            return new DataTypeOfList(DataTypeOfListEnum.WILDCARD_TYPE);

        return new DataTypeOfList(DataTypeOfListEnum.SPECIFIC_DATA_TYPE, typeOfList);
    }


    /**
     * Metoda, která vytvoří syntaxi pro název metody nebo třídy (pro konstrukor) a v kulatých závorkách budou její /
     * jeho parametry, jaké se mají zadávat do metody či konstruktoru Syntaxe: název metody či konstruktoru
     * (parameters)
     *
     * @param name
     *         - název metody nebo konstruktoru (/ třídy)
     * @param parameters
     *         parametr, ze kterých si vytáhnu jejích datové typy
     *
     * @return vytvořím výše uvedenou syntaxi a vrátím jí
     */
    public static String getMethodNameAndParametersInText(final String name, final Parameter[] parameters) {
        return name + getParametersInText(parameters);
    }


    /**
     * Získání parametrů v polid parameters v textové podobě. Jednotlivé parametry budou oddělené desetinnou čárkou a
     * nebudou u nich zobrazené balíčky.
     *
     * <i>Parametry budou obalené kulatými závorkami.</i>
     *
     * @param parameters
     *         - parametry metody nebo konstruktory, které se mají převést do podoby textu oddělené desetinnou čárkou.
     *
     * @return datové typy parametrů v parameters oddělené desetinnou čárkou uvedené bez balíčků.
     */
    public static String getParametersInText(final Parameter[] parameters) {
        return "(" +
                ParameterToText.getParametersInText(parameters, false) +
                ")";
    }


    /**
     * Převedení parametrů (parameters) do textové podoby. Veškeré parametry budou oddělené desetinnou čárkou a u
     * každého parametru budou uvedeny i balíčky. Převedou se do textové podoby v podstatě veškeré parametry, ať už se
     * jedná o List Listů, mapu, mapu, kde třeba klíč je další list apod. Může se jednat i o generické datové typy nebo
     * neuvedené datové typy třeba Listu apod.
     * <p>
     * Například pro metodu s parametry: (Map<int[], ?> e, Integer[] in, Map<?, Object>[][][] q, Map m, Map<?, ?> mx,
     * Map<String, List<Map<?, String>>> m2,List d, List<?> l, List<String> ls, List<List<ArrayList<String>>> more)
     * <p>
     * se vypíše text: java.util.Map<int[], ?>, java.lang.Integer[], java.util.Map[][][], java.util.Map,
     * java.util.Map<?, ?>, java.util.Map<java.lang.String, java.util.List<java.util.Map<?, java.lang.String>>>,
     * java.util.List, java.util.List<?>, java.util.List<java.lang.String>, java.util.List<java.util.List<java.util
     * .ArrayList<java.lang.String>>>
     *
     * @param parameters
     *         - parametry, které se mají převést do textové podoby.
     *
     * @return výše popsané parametry v textové podobě oddělené desetinnou čárkou.
     */
    public static String getMethodNameAndParametersInText(final Parameter[] parameters) {
        final StringBuilder stringBuilder = new StringBuilder();

        Arrays.stream(parameters).forEach(p -> stringBuilder.append(ParameterToText.parametersToText(p)).append(", "));

        stringBuilder.setLength(stringBuilder.length() - 2);

        return stringBuilder.toString();
    }


    /**
     * Metoda, která zjistí, zda jsou modifiers modifikátory pro viditelnost "objektu" v rámci package-private, tedy
     * pouze pro třídy v rámci stejného balíčku.
     * <p>
     * To poznáme tak, že modifikátor viditelnosti (modifiers) není veřejný, ani chráněný, ani soukromý, pokud jsou tyto
     * podmínky splněny, pak je package-private.
     *
     * @param modifiers
     *         - modifikátory příslušného objektu (metody, konstruktoru, třídy apod.). Díky tomu se pozná, o jakou
     *         viditelnost jde.
     *
     * @return true, pokud se jedná o modifikátory s viditelností package-private, jinak false.
     */
    public static boolean isPackagePrivateVisibility(final int modifiers) {
        return !Modifier.isPublic(modifiers) && !Modifier.isPrivate(modifiers) &&
                !Modifier.isProtected(modifiers);
    }


    /**
     * Zjištění, zda se jedná o parametrizovaný typ. Například když je proměnná pouze typu "List", pak se vrátí false,
     * ale když je proměnná typu "List<String>", pak se vrátí true. Podobně pro Mapu a další typy s parametry.
     *
     * @param type
     *         - datový typ, o kterém se má zjistit, zda se jedná o datovou strukturu s definovanými parametry popsanými
     *         výše.
     *
     * @return true v případě, že type je typ s definovanými parametry, jinak false.
     */
    public static boolean isParameterizedType(final Type type) {
        return type instanceof ParameterizedType;
    }


    /**
     * Metoda, která vypočítá a vrátí počet dimenzí pole v parametru metody.
     *
     * @param array
     *         - X - rozměrné pole, o kterém potřebuji znát kolika rozměrné je, resp. kolik má dimezí
     *
     * @return počet dimenzí pole v parametru metody - array
     */
    public static int getCountOfDimensionArray(final Object array) {
        int count = 0;

        Class<?> clazz = array.getClass();

        while (clazz.isArray()) {
            clazz = clazz.getComponentType();
            count++;
        }

        return count;
    }


    /**
     * Získání počtu dimenzí pole classArray.
     * <p>
     * <i>(Zjištění, kolika rozměrné pole je classArray.)</i>
     *
     * @param classArray
     *         - pole, o kterém se má zjistit, kolik má dimenzí.
     *
     * @return počet dimenzí pole classArray.
     */
    public static int getCountOfDimensionArray(final Class<?> classArray) {
        int count = 0;

        Class<?> classArrayVar = classArray;

        while (classArrayVar.isArray()) {
            classArrayVar = classArrayVar.getComponentType();
            count++;
        }

        return count;
    }


    /**
     * Metoda, která slouží pro získání hodnoty z proměnné field, která se nachází v instancí objInstance.
     *
     * @param field
     *         - proměnná, ze které se mí získat jeji hodnota.
     * @param objInstance
     *         - instance nějaké třídy, která obsahuje hodnotu field, ze které se má získat její hodnota.
     *
     * @return hodnotu z proměnné field nebo null. Null se může vrátit i když jej obsahuje ta proměnná.
     */
    public static Object getValueFromField(final Field field, final Object objInstance) {
        if (!field.isAccessible())
            field.setAccessible(true);

        try {

            return field.get(objInstance);

        } catch (final IllegalArgumentException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při získávání hodnoty z proměnné: '" + field.getName() + "'" +
                        " z instance třídy: '" + objInstance + "', konkrétně: '" + objInstance.getClass() + "'. Chyba" +
                        " může nastat například v případě, že zadaný objekt není instancí třídy či rozhraní (nebo " +
                        "jeho / její potomek), který deklaruje zadanou proměnnou (jinými slovy, proměnná se zadaným " +
                        "názvem se nevyskytuje v zadané instanci třídy - v tomto případě by to ale nastat němělo).");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();

        } catch (final IllegalAccessException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při získávání hodnoty z proměnné: '" + field.getName() + "'" +
                        " z instance třídy: '" + objInstance + "', konkrétně: '" + objInstance.getClass() + "'. resp." +
                        " souboru .class. Tato chyba může nastat například v případě, že zadaná proměnná není v dané " +
                        "instanci třídy přístupná.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }

        return null;
    }


    /**
     * Metoda, která zjistí, o jaký datový typ se jedná. Konkrétně, v proměnné clazz v parametru této metody musí být N
     * - rozměrné pole, a iteruje se jím tak dlouho, dokud se nezjistí konečný datový typ příslušného pole.
     * <p>
     * "Metoda slouží pro zjištění datového typu pole clazz".
     *
     * @param clazz
     *         - objekt class příslušného pole, ve kterém se má zjistit datový typ.
     *
     * @return datový typ pole clazz.
     */
    public static Class<?> getDataTypeOfArray(final Class<?> clazz) {
        Class<?> array = clazz;

        while (array.isArray())
            array = array.getComponentType();

        return array;
    }


    /**
     * Zjištění, zda je datový typ v parametru metody datového typu List
     *
     * @param clazz
     *         - datový typ, o kterém se zjistí, zda je typu List nebo ne
     *
     * @return true, pokud je datový typ v parametru metody typu List, jinak false.
     */
    public static boolean isDataTypeOfList(final Class<?> clazz) {
        return Collection.class.isAssignableFrom(clazz);
    }


    /**
     * Zjištění, zda je datový typ clazz datového typu Map.
     *
     * @param clazz
     *         - datový typ, o kterém se má zjistit, zda se jedná o typ Map.
     *
     * @return true v případě, že clazz je datový typ Map, jinak false.
     */
    public static boolean isDataTypeOfMap(final Class<?> clazz) {
        return Map.class.isAssignableFrom(clazz);
    }


    /**
     * Získání veškerých vnitřních tříd z každé vnitřní třídy v classes. Neřeší se viditelnost vnitřních tříd, ani zda
     * jsou statické či jiné vlasnosti.
     *
     * @param classes
     *         vnitřní třídy získané z nějaké třídy. Toto pole vnitřních tříd se bude iterovat a pro každou vnitřní
     *         třídu se získají i její vnitřní třídy.
     * @param classList
     *         - list, do kterého se budou vkládat získané / nalezné vnitřní třídy.
     *
     * @return list classList, který bude obsahovat veškeré nalezené vnitřní třídy.
     */
    public static List<Class<?>> getAllInnerClasses(final Class<?>[] classes, final List<Class<?>> classList) {
        if (classes == null)
            return classList;

        Arrays.stream(classes).forEach(c -> {
            classList.add(c);

            getAllInnerClasses(c.getDeclaredClasses(), classList);
        });

        return classList;
    }


    /**
     * Porovnání objektů object1 a object2. Jedná se o porovnání datových typů.
     * <p>
     * Konkrétně: "Zda li je možné vložit hodnoty typu object2 do objektu typu object1."
     * <p>
     * Například: "Zda li je možné vložit hodnoty z mapy s typem klíče nebo hodnoty object2 do mapy s datový typem klíče
     * nebo hodnoty object1." Podobně pro List.
     * <p>
     * <i>Metoda byla napsána za účelem testování hodnot typu klíče a hodnoty v mapě (zda li je možné vložit hodnotu
     * určitého typu do mapy na pozici klíče nebo hodnoty). Proto zde budou uváděny p59kladz postupu pro objekt Map, ale
     * lze jej aplikovat na porovnávání veškerých typů, nejen typy týkající se Mapy.</i>
     *
     * <i>Porovnávání: V případě, že je object1 nebo object2 null. Vrátí se false. To znamená, že jeden z typů je
     * wildcard, kde se uložila pouze výčtová hodnota, ne samotný typ wildcard, popř. bez uvedeného datového typu (toto
     * u tohoto porovnávání nastat nemělo).
     * <p>
     * V ostatních případech se zjistí, zda jsou oba parametry typu ParameterizedType, GenericArrayType, Class, ... (viz
     * tělo metody) a porovnají se pomocí metody equals. Výsledek se vrátí.
     * <p>
     * Note: V budoucnu by bylo možné doplnít testování tak, že není například parametr object1 typu List<?>. V takovém
     * případě by se mohlo zjistit, zda je object2 List libovolného typu List<(Object, String, Double, ...). Podobně pro
     * Mapy a další. Ale bylo by třeba do této metody předávat i samotnou Mapu či List.</i>
     *
     * @param object1
     *         - datový typ klíče nebo hodnoty Mapy, o kterém se má zjistit, zda "do něj" lze vložit hodnotu datového
     *         typu object2. <i>Obecně datový typ, o kterém se má zjistit, zda "do něj" lze vložit hodnotu typu
     *         object2.</i>
     * @param object2
     *         - datový typ klíče nebo hodnoty Mapy, o kterém se má zjistit, zda li je možné vložit hodnotu tohoto typu
     *         do object1. <i>Obecně datový typ hodnoty, o kterém se má zjistit, zda li je možné vložit hodnotu tohoto
     *         typu do objektu typu object1.</i>
     *
     * @return true v případě, že object2 je takový typ, který je shodný s object1. "Hodnotu typu object2 lze vložit do
     * mapy na pozici klíče nebo hodnoty typu object1."
     */
    public static boolean compareDataType(final Object object1, final Object object2) {
        if (object1 == null || object2 == null) {
            /*
             * Tato část může nastat například v případě, že je v parametru uživatelem zavolané metody například
             * List<String> a v nějaké existující instanci je veřejná proměnná typu List<?>. Tak se bude jako object1
             * porovnávat String a jako object2 bude null, protože se u příslušného Listu s Wildcard typem zjistí,
             * že je to wildcard, ale uloží se o tom pouze výčtová hodnota, protoo bude jako object2 null - nebude
             * se jednat o typ Listu.
             */
            return false;
        }

        // "Neznámý" typ:
        else if (object1 instanceof WildcardType) {
            /*
             * Do "neznámého" typu lze vložit libovolnou objektovou hodnotu, tak stačí zjistit, jestli je object2
             * objektová hodnota, pokud ano, lze jej vložit do object1.
             */
            return !object2.getClass().isPrimitive();
        }

        // List s "libovolným" parametrem, která není N - rozměrné pole:
        else if (object1 instanceof ParameterizedType) {
            if (!(object2 instanceof ParameterizedType))
                return false;

            /*
             * Note:
             *
             * Zde by bylo možné doplnit například testování, když je object1 List, pak do něj bude možné vložit
             * veškeré jeho implementace. Ale bylo by třeba dořešit, zda se mají testovat i parametry Listu a jeho
             * implementací apod. Proto vynecháno.
             */
            return object1.equals(object2);
        }

        // Vnořená pole: "Map<List<Object>[][], ?>"
        else if (object1 instanceof GenericArrayType) {
            if (!(object2 instanceof GenericArrayType))
                return false;

            return object1.equals(object2);
        }

        // Generické typy: "T"
        else if (object1 instanceof TypeVariable<?>) {
            if (!(object2 instanceof TypeVariable<?>))
                return false;

            return object1.equals(object2);
        }

        // "Ostatní" typy, které nejsou "hlídány" pro testování:
        else if (object1 instanceof Class<?>) {
            if (!(object2 instanceof Class<?>))
                return false;

            if (object1.equals(Object.class))
                /*
                 * V případě, že je object1 typu Object, pak se může vrátit true, protože do objektu lze vložit v
                 * podstatě libovolný jiný typ parametru mapy.
                 *
                 * Zde už je jasné, že object2 není generický datový typ nebo mapa bez definovaných parametrů.
                 */
                return true;

            return object1.equals(object2);
        }

        return object1.equals(object2);
    }
}