package cz.uhk.fim.fimj.reflection_support;

import java.lang.reflect.*;

/**
 * Třída obsahuje metody pro práci s reflexí v rámci potřeb pro práci s objektem typu List. Například získávání datového
 * typu Listu, porovnávání apod.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 07.09.2018 20:03
 */

final class ListHelper {

    private ListHelper() {
    }


    /**
     * Získání datového typu Listu.
     *
     * @param parameter
     *         - parametr, o kterém se musí vědět, že se jedná o List. Jinak se vrátí null. Pokud je to List, získá se
     *         jeho datový typ.
     *
     * @return datový typ Listu v parametru parameter. Vrátí se null v případě, že List nemá uveden parametr (je pouze
     * "List") nebo v případě generického datového typu. V ostatních případech se vrátí datový typ Listu. Může se jednat
     * o hodnotu typu ParameterizedType - List s definovaným typem, GenericArrayTypeImpl - List s polem nebo Class<?> -
     * List s jiným objektem (ne datovou strukturou).
     */
    static Object getTypeOfList(final Parameter parameter) {
        if (parameter == null)
            return null;

        return getTypeOfList(parameter.getParameterizedType());
    }


    /**
     * Získání datového typu Listu.
     *
     * @param field
     *         - proměnná, o které se musí vědět, že se jedná o List. Jinak se vrátí null. Pokud je to List, získá se
     *         jeho datový typ.
     *
     * @return datový typ Listu v proměnné field. Vrátí se null v případě, že List nemá uveden parametr (je pouze
     * "List") nebo v případě generického datového typu. V ostatních případech se vrátí datový typ Listu. Může se jednat
     * o hodnotu typu ParameterizedType - List s definovaným typem, GenericArrayTypeImpl - List s polem nebo Class<?> -
     * List s jiným objektem (ne datovou strukturou).
     */
    static Object getTypeOfList(final Field field) {
        if (field == null)
            return null;

        return getTypeOfList(field.getGenericType());
    }


    /**
     * Získání datového typu Listu, který se vrací jako návratová hodnota metody method.
     *
     * @param method
     *         - metoda, o které se musí vědět, že vrací hodnotu datového typu List. Jinak se vrátí null. Pokud je to
     *         List, získá se jeho datový typ.
     *
     * @return datový typ Listu návratové hodnoty metody. Vrátí se null v případě, že List nemá uveden parametr (je
     * pouze "List") nebo v případě generického datového typu. V ostatních případech se vrátí datový typ Listu. Může se
     * jednat o hodnotu typu ParameterizedType - List s definovaným typem, GenericArrayTypeImpl - List s polem nebo
     * Class<?> - List s jiným objektem (ne datovou strukturou).
     */
    static Object getTypeOfList(final Method method) {
        if (method == null)
            return null;

        return getTypeOfList(method.getGenericReturnType());
    }


    /**
     * Získání datového typu Listu.
     *
     * @param type
     *         - List, o kterém se má zjistit datový typ.
     *
     * @return datový typ Listu. Získá se "ihned" datový typ Listu, nebude se zjišťovat nic rekurzivně apod.
     *
     * <i>Například, když je "List<String>", vrátí se "String". Když je "List<ArrayList<Float>>", vrátí se
     * "ArrayList<Float>".</i>
     */
    private static Object getTypeOfList(final Type type) {
        if (type == null)
            return null;

        if (!ReflectionHelper.isParameterizedType(type))
            // Zde není uveden datový typ, je pouze "List", popř. N - rozměrné pole typu List:
            return null;

        final ParameterizedType parameterizedType = (ParameterizedType) type;

        // Je třeba zjistit, zda li je to vůbec List:
        if (!ReflectionHelper.isDataTypeOfList((Class<?>) parameterizedType.getRawType()))
            return null;

        final Type[] types = parameterizedType.getActualTypeArguments();

        /*
         * List má vždy pouze jeden "parametr / datový typ". Proto se otestuje, zda tomu tak opravdu je a zjiští se.
         */
        if (types.length != 1)
            return null;

        final Type typeTemp = types[0];

        return DataTypeHelper.getDataType(typeTemp);
    }
}
