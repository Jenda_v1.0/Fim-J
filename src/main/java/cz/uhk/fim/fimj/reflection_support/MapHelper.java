package cz.uhk.fim.fimj.reflection_support;

import java.lang.reflect.*;

/**
 * Metoda obsahuje metody pro práci s reflexí v rámci objektu Map. Například získávání typu klíče a hodnoty mapy, jejich
 * porovnávání apod.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 03.09.2018 22:33
 */

public final class MapHelper {

    private MapHelper() {
    }


    /**
     * Získání datového typu klíče mapy.
     *
     * @param parameter
     *         - o tomto parametru je třeba vědět, že se jedná o typ Mapy. Jinak se vrátí null hodnota.
     *
     * @return datový typ klíče mapy nebo null hodnotu v případě, že se buď nejedná o parametr typu mapa nebo Mapa nemá
     * definované parametry nebo se jedná o generický datový typ klíče mapy.
     */
    public static Object getKeyTypeOfMap(final Parameter parameter) {
        if (parameter == null)
            return null;

        return getTypeOfMap(parameter.getParameterizedType(), 0);
    }


    /**
     * Získání datového typu klíče mapy.
     *
     * @param field
     *         - proměnná, o které se ví, že se jedná o typ Map a má se zjistit datový typ klíče mapy. (Musí se vědět,
     *         že se jedná o mapu, jinak se vrátí null.)
     *
     * @return datový typ klíče mapy nebo null hodnotu v případě, že se buď nejedná o parametr typu mapa nebo Mapa nemá
     * definované parametry nebo se jedná o generický datový typ klíče mapy.
     */
    public static Object getKeyTypeOfMap(final Field field) {
        if (field == null)
            return null;

        return getTypeOfMap(field.getGenericType(), 0);
    }


    /**
     * Získání datového typu klíče mapy z návratové hodnoty metody.
     *
     * @param method
     *         - metoda, o které se ví, že vrací objekt typu Map a má se zjistit datový typ klíče mapy. (Musí se vědět,
     *         že se jedná o mapu, jinak se vrátí null.)
     *
     * @return datový typ klíče mapy nebo null hodnotu v případě, že se buď nejedná o parametr typu mapa nebo Mapa nemá
     * definované parametry nebo se jedná o generický datový typ klíče mapy.
     */
    public static Object getKeyTypeOfMap(final Method method) {
        if (method == null)
            return null;

        return getTypeOfMap(method.getGenericReturnType(), 0);
    }


    /**
     * Získání datového typu hodnoty mapy.
     *
     * @param parameter
     *         - o tomto parametru je třeba vědět, že se jedná o typ Mapy. Jinak se vrátí null hodnota.
     *
     * @return datový typ hodnoty mapy nebo null hodnotu v případě, že se buď nejedná o parametr typu mapa nebo Mapa
     * nemá definované parametry nebo se jedná o generický datový typ hodnoty mapy.
     */
    public static Object getValueTypeOfMap(final Parameter parameter) {
        if (parameter == null)
            return null;

        return getTypeOfMap(parameter.getParameterizedType(), 1);
    }


    /**
     * Získání datového typu hodnoty mapy.
     *
     * @param field
     *         - proměnná, o které se ví, že se jedná o typ Map a má se zjistit datový typ hodnoty mapy. (Musí se vědět,
     *         že se jedná o mapu, jinak se vrátí null.)
     *
     * @return datový typ hodnoty mapy nebo null hodnotu v případě, že se buď nejedná o parametr typu mapa nebo Mapa
     * nemá definované parametry nebo se jedná o generický datový typ hodnoty mapy.
     */
    public static Object getValueTypeOfMap(final Field field) {
        if (field == null)
            return null;

        return getTypeOfMap(field.getGenericType(), 1);
    }


    /**
     * Získání datového typu hodnoty mapy v návratové hodnotě metody.
     *
     * @param method
     *         - metoda, o které se ví, že vrací objekty typu Map a má se zjistit datový typ hodnoty mapy. (Musí se
     *         vědět, že se jedná o mapu, jinak se vrátí null.)
     *
     * @return datový typ hodnoty mapy nebo null hodnotu v případě, že se buď nejedná o parametr typu mapa nebo Mapa
     * nemá definované parametry nebo se jedná o generický datový typ hodnoty mapy.
     */
    public static Object getValueTypeOfMap(final Method method) {
        if (method == null)
            return null;

        return getTypeOfMap(method.getGenericReturnType(), 1);
    }


    /**
     * Získání datového typu mapy na pozici index. Jedná se o získání typu klíče nebo hodnoty mapy.
     *
     * @param type
     *         - musí se jednat o mapu. Pro Listy či jiné kolekce a typy proměnných není připraven postup.
     * @param index
     *         - index může být 0 nebo 1. 0 značí získání typu klíče mapy. 1 značí získání typu hodnoty mapy.
     *
     * @return typ klíče nebo hodnoty mapy. Získá se "ihned" typ klíče nebo mapy. Tj. ať už je například typ klíče v
     * mapě jakýkoliv datový typ či vnořený datový typ, například List Listů nebo mapa, která má jako klíč mapy a v ní
     * jsou List Listů, pak pole apod. Vždy se získá "toto všecho" jako hodnota klíče "první mapy".
     *
     * <i>Například parametr (type) bude: "Map<LinkedList<ArrayList<String>>, HashSet<?>[]> map", pak se jako typ klíče
     * vrátí: "LinkedList<ArrayList<String>>" a jako typ hodnoty: "HashSet<?>[]".</i>
     */
    private static Object getTypeOfMap(final Type type, final int index) {
        if (type == null)
            return null;

        if (!ReflectionHelper.isParameterizedType(type))
            // Zde není uveden datový typ, je pouze "Map", popř. N - rozměrné pole typu Map:
            return null;

        final ParameterizedType parameterizedType = (ParameterizedType) type;

        /*
         * Zde je type objekt s parametry, takže může být například List<String> - konkrétní datový typ či generická
         * hodnota. Zde se zjistí, zda se jedná o mapu, jinak se vrátí null.
         *
         * Note:
         *
         * Metoda: "parameterizedType.getRawType()" vrací datový typ Type, ale vždy se jedná o "Class<?>", proto se
         * vrácená hodnota přetypuje na Class a zjistí se, zda se jedná o Mapu.
         */
        // Původní způsob přetypování:
        //        if (!Map.class.isAssignableFrom(((ParameterizedTypeImpl) parameterizedType).getRawType()))
        if (!ReflectionHelper.isDataTypeOfMap((Class<?>) parameterizedType.getRawType()))
            return null;

        final Type[] types = parameterizedType.getActualTypeArguments();

        // Pokud není možné získat index, je možné skončit:
        if (types.length <= 0 || index >= types.length)
            return null;

        final Type typeTemp = types[index];

        return DataTypeHelper.getDataType(typeTemp);
    }


    /**
     * Zjištění, zda je proměnná field datového typu Map. Map může být libovolného datového typu s nebo bez definovaných
     * parametrů.
     *
     * @param field
     *         - proměnná, o které se má zjistit, zda se jedná o proměnnou typu Map (nebo její implementace).
     *
     * @return true v případě, že je proměnná field typu Map. Jinak false.
     */
    public static boolean isFieldTypeOfMap(final Field field) {
        if (field == null)
            return false;

        return isTypeOfMap(field.getGenericType());
    }


    /**
     * Zjištění, zda je parametr metody nebo konstruktoru (parameter) datového typu Map. Map může být libovolného
     * datového typu s nebo bez definovaných parametrů.
     *
     * @param parameter
     *         - parametr metody nebo konstruktoru, o kterém se má zjistit, zda je datoévho typu Map (nebo její
     *         implementace).
     *
     * @return true v případě, že je parameer datového typu Map. Jinak false.
     */
    public static boolean isParameterTypeOfMap(final Parameter parameter) {
        if (parameter == null)
            return false;

        return isTypeOfMap(parameter.getParameterizedType());
    }


    /**
     * Zjištění, zda metoda vrací hodnotu datového typu Map. Map může být libovolného datového typu s nebo bez
     * definovaných parametrů.
     *
     * @param method
     *         - metoda, o které se má zjistit, zda vrací hodnotu datového typu Map (nebo jednu z její implementací).
     *
     * @return true v případě, že metoda method vrací datový typ Map (či její implementace). Jinak false.
     */
    public static boolean methodReturnsMap(final Method method) {
        if (method == null)
            return false;

        return isTypeOfMap(method.getGenericReturnType());
    }


    /**
     * Zjištění, zda je type datový typ Mapy. (Může být s definovanými parametry i bez.)
     *
     * @param type
     *         - datový typ, o kterém se má zjistit, zda se jedná o datový typ "Map".
     *
     * @return true v případě, že type je libovolný datový typ mapy. Jinak false.
     */
    private static boolean isTypeOfMap(final Type type) {
        if (type == null)
            return false;

        /*
         * Note:
         *
         * Zde by bylo možné testovat pouze podmínku: "Map.class.isAssignableFrom(parameter.getType() || field
         * .getType())". Tedy jako parametr předávat pouze hodnotu typu "Class<?>". Ale museli by se zvláště získávat
          * parametry z metod a atributů.
         */

        if (!ReflectionHelper.isParameterizedType(type)) {
            // Zde není uveden datový typ, je například "Map" nebo "List":
            if (type instanceof Class<?>) {
                // Zde se jedná o mapu bez definovaných parametrů, pouze "Map".
                return ReflectionHelper.isDataTypeOfMap((Class<?>) type);
            }
            return false;
        }

        final ParameterizedType parameterizedType = (ParameterizedType) type;

        /*
         * Zde je type objekt s parametry, takže může být například List<String> - konkrétní datový typ či generická
         * hodnota. Zde se zjistí, zda se jedná o mapu, jinak se vrátí false.
         *
         * Note:
         *
         * Metoda: "parameterizedType.getRawType()" vrací datový typ Type, ale vždy se jedná o "Class<?>", proto se
         * vrácená hodnota přetypuje na Class a zjistí se, zda se jedná o Mapu.
         */
        // Původní způsob přetypování:
        //        return Map.class.isAssignableFrom(((ParameterizedTypeImpl) parameterizedType).getRawType());
        return ReflectionHelper.isDataTypeOfMap((Class<?>) parameterizedType.getRawType());
    }
}
