package cz.uhk.fim.fimj.reflection_support;

import java.lang.reflect.*;

/**
 * Společné metody pro potomky.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 07.09.2018 20:21
 */

interface DataTypeHelper {

    /**
     * Zde se získá konkrétní datový typ mapy nebo Listu, ale v takové podobě, aby bylo možné jej dále porovnávat s
     * jinými typy.
     *
     * @param type
     *         - datový typ, který se má zjistit.
     *
     * @return datový typ type.
     * <p>
     * Vrátí se null v případě, že se jedná o generický datový typ. ParameterizedType v případě, že se jedná například o
     * List nebo mapu s datový typem. GenericArrayTypeImpl v případě vnořených polí, tedy když je Map a jako klíč nebo
     * hodnotu má N - rozměrné pole (podobně s Listem). V ostatních případech se jendá o Class<?>. Pro více informací
     * viz tělo metody.
     */
    static Object getDataType(final Type type) {
        // Při generickem typu je pouze "?" (v textu):
        if (type instanceof WildcardType)
            return type;


            // Pro List a Map s definovanými datovými typy:
        else if (type instanceof ParameterizedType)
            return type;


            // Toto je například pro vnořená pole: Map<List<Object>[][], ?>
        else if (type instanceof GenericArrayType)
            return type;


            // Generické typy, například: "T"
        else if (type instanceof TypeVariable<?>)
            return type;


        // Zde se jedná o typ Class<?> (List a Map bez parametrů nebo "klasické" parametry - ne datové struktury)
        return type;
    }
}
