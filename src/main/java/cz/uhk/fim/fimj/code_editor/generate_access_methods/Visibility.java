package cz.uhk.fim.fimj.code_editor.generate_access_methods;

/**
 * Výčet, který obsahuje hodnoty pro viditelnosti - pro snažší / přehlednější předávání hodnot do parametrů metodám pro
 * generování přístupových metod apod.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 11.08.2018 14:49
 */

public enum Visibility {

    PUBLIC("public"), PRIVATE("private"), PROTECTED("protected"), PACKAGE_PRIVATE("");


    /**
     * Text viditelnosti - pro snažší / přehlednější převední do textové podoby.
     */
    final String txtVisibility;

    Visibility(String txtVisibility) {
        this.txtVisibility = txtVisibility;
    }

    public String getTxtVisibility() {
        return txtVisibility;
    }
}
