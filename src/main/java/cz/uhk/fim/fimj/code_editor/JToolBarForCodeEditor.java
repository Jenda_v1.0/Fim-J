package cz.uhk.fim.fimj.code_editor;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;
import java.util.stream.IntStream;

import javax.swing.*;

import cz.uhk.fim.fimj.code_editor.combo_box_with_radio_button.CmbItem;
import cz.uhk.fim.fimj.code_editor.combo_box_with_radio_button.CmbRadioButton;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.GetIconInterface;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.project_explorer.ReadWriteFile;

/**
 * Tato třída slouží jako Toolbar - nástrojová lišty s "nejvíce používanými tlačítky" pro manipulaci s textem zdrojového
 * kódu
 * <p>
 * Tato "lišta" se přidá do okna dialogu při jeho vytvoření
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class JToolBarForCodeEditor extends JToolBar implements LanguageInterface, ActionListener {

    private static final long serialVersionUID = 3007471689672067261L;

    private final JButton btnCompile;
    private final JButton btnCut;
    private final JButton btnCopy;
    private final JButton btnPaste;
    private final JButton btnClose;


    /**
     * Nastavení, zda se má zalamovat text v editoru.
     */
    private final transient CmbItem cmbItemWrapText;
    /**
     * Nastavení, zda se mají v editoru zobrazovat bílé znaky.
     */
    private final transient CmbItem cmbItemShowWhiteSpace;
    /**
     * Nastavení, zda se mají v editoru "čistit" bílé znaky.
     */
    private final transient CmbItem cmbItemClearWhiteSpaceLine;


    /**
     * Proměnná pro uchování reference na instanci třídy SharedClassForMethods, důvod je v komentáři konstruktoru
     */
    private final transient SharedClassForMethodsInterface sharedClass;


    private Properties languageProperties;


    /**
     * Reference na editor kódu, aby bylo možné mu po zkompilování nastavit novou zkompilovatnou třídu:
     */
    private final CodeEditor codeEditor;


    /**
     * Konstruktor třídy Reference na instanci třídy, která je předána v parametru (viz níže) je potřeba předávat zde a
     * je vytvořena pro každou instanci editoru zdrojového kódu (res. společná pro instance - static), protože obsahuje
     * proměnnou pro uchování textu z editoru zdrojového kódu, kterou je možné uložit buď tlačítkem z tohoto toolBaru
     * nebo položkou v menu v dalogu, stejně tak její hodnotu vložit do editoru
     *
     * @param sharedClass
     *         - reference na instanci třídy SharedClassForMethods kvůli přístupu k jedné proměnné (výše)
     * @param codeEditor
     *         - reference na instanci třídy CodeEditor - pro nastavení zkompiloané třídy - doplňování, apod.
     */
    JToolBarForCodeEditor(final SharedClassForMethodsInterface sharedClass, final CodeEditor codeEditor) {

        this.sharedClass = sharedClass;
        this.codeEditor = codeEditor;

        setLayout(new FlowLayout(FlowLayout.CENTER, 20, 0));


        cmbItemWrapText = new CmbItem();
        cmbItemShowWhiteSpace = new CmbItem();
        cmbItemClearWhiteSpaceLine = new CmbItem();


        btnCompile = new JButton();
        btnCut = new JButton();
        btnCopy = new JButton();
        btnPaste = new JButton();
        btnClose = new JButton();


        // Události na tlačítka:
        btnCompile.addActionListener(this);
        btnCut.addActionListener(this);
        btnCopy.addActionListener(this);
        btnPaste.addActionListener(this);
        btnClose.addActionListener(this);


        // Vytvoření cmb s položkami pro nastavení vybraných vlastností editoru kódu:
        final CmbItem[] modelForCmb = getModelForCmb();
        /*
         * Komponenta, ve které budou jako položky {@link JRadioButton}, které budou slouží pro nastavení vlastností
         * v editoru. Například zalamování textů, práce s bílímy znaky apod.
         */
        final JComboBox<CmbItem> cmbItems = new CmbRadioButton(modelForCmb, this);

        // Přidání tlačítek do toolbaru:
        add(cmbItems);
        add(btnCompile);
        add(btnCut);
        add(btnCopy);
        add(btnPaste);
        add(btnClose);


        // Přidám ikony tlačítkům:
        addIconToButtons();
    }


    /**
     * Získání modelu pro cmbItems (položky).
     *
     * @return pole s položkami, které slouží jako model zmíněné komponenty.
     */
    private CmbItem[] getModelForCmb() {
        return getModelForCmb(cmbItemWrapText, cmbItemShowWhiteSpace, cmbItemClearWhiteSpaceLine);
    }


    /**
     * Vytvoření a vrácení jednorozměrného pole objektů, které slouží jako model pro komponentu cmbItems.
     *
     * @param cmbItems
     *         - veškeré položky, které se mají nacházet ve výše zmíněné komponentě.
     *
     * @return jednorozměrné pole, které bude obsahovat položky v cmbItems.
     */
    private static CmbItem[] getModelForCmb(final CmbItem... cmbItems) {
        final CmbItem[] items = new CmbItem[cmbItems.length];

        IntStream.range(0, cmbItems.length).forEach(i -> items[i] = cmbItems[i]);

        return items;
    }


    /**
     * Změna v nastavní editoru kódu.
     *
     * <i>Metoda se zavolá po kliknutí na jednu z položek v cmbItems. V takovém případě je třeba změnit nastavení del
     * toho, jaké nastavení uživatel změnil, například chce zobrazovat bílé znaky apod.</i>
     *
     * @param cmbItem
     *         - položka, na kterou uživatel klikl v cmb. Položka značí konkrétní nastavení (vlastnosti) v editoru
     *         kódu.
     */
    public void changeCodeEditorSettings(final CmbItem cmbItem) {
        if (cmbItem == null)
            return;

        if (cmbItem.equals(cmbItemWrapText))
            codeEditor.wrapTextInEditor(cmbItemWrapText.isSelected());

        else if (cmbItem.equals(cmbItemShowWhiteSpace))
            codeEditor.showWhiteSpace(cmbItemShowWhiteSpace.isSelected());


        else if (cmbItem.equals(cmbItemClearWhiteSpaceLine))
            codeEditor.setClearWhiteSpaceLine(cmbItemClearWhiteSpaceLine.isSelected());
    }


    @Override
    public void setLanguage(final Properties properties) {
        languageProperties = properties;

        if (properties != null) {

            // Texty do tlačítek:
            cmbItemWrapText.setText(properties.getProperty("Ced_Mfce_MenuToolsRadioButtonWrapText",
                    Constants.MFCE_MENU_TOOLS_RADIO_BUTTON_WRAP_TEXT));
            cmbItemShowWhiteSpace.setText(properties.getProperty("Ced_Mfce_MenuToolsRadioButtonShowWhiteSpace",
                    Constants.MFCE_MENU_TOOLS_RADIO_BUTTON_SHOW_WHITE_SPACE));
            cmbItemClearWhiteSpaceLine.setText(properties.getProperty(
                    "Ced_Mfce_MenuToolsRadioButtonClearWhiteSpaceLine",
                    Constants.MFCE_MENU_TOOLS_RADIO_BUTTON_CLEAR_WHITE_SPACE_LINE));

            btnCompile.setText(properties.getProperty("Ced_Mfce_MenuToolsItemCompile",
                    Constants.MFCE_MENU_TOOLS_ITEM_COMPILE));
            btnCut.setText(properties.getProperty("Ced_Mfce_MenuEditItemCut", Constants.MFCE_MENU_EDIT_ITEM_CUT));
            btnCopy.setText(properties.getProperty("Ced_Mfce_MenuEditItemCopy", Constants.MFCE_MENU_EDIT_ITEM_COPY));
            btnPaste.setText(properties.getProperty("Ced_Mfce_MenuEditItemPaste", Constants.MFCE_MENU_EDIT_ITEM_PASTE));
            btnClose.setText(properties.getProperty("Ced_Mfce_MenuClassItemClose",
                    Constants.MFCE_MENU_CLASS_ITEM_CLOSE));


            // Popisky tlačítek:
            btnCompile.setToolTipText(properties.getProperty("Ced_Mfce_MenuToolsToolTipItemCompile",
                    Constants.MFCE_TOOL_TIP_ITEM_COMPILE));
            btnCut.setToolTipText(properties.getProperty("Ced_Mfce_MenuEditToolTipItemCut",
                    Constants.MFCE_TOOL_TIP_ITEM_CUT));
            btnCopy.setToolTipText(properties.getProperty("Ced_Mfce_MenuEditToolTipItemCopy",
                    Constants.MFCE_TOOL_TIP_ITEM_COPY));
            btnPaste.setToolTipText(properties.getProperty("Ced_Mfce_MenuEditToolTipItemPaste",
                    Constants.MFCE_TOOL_TIP_ITEM_PASTE));
            btnClose.setToolTipText(properties.getProperty("Ced_Mfce_MenuClassToolTipItemClose",
                    Constants.MFCE_TOOL_TIP_ITEM_CLOSE));
        } else {
            cmbItemWrapText.setText(Constants.MFCE_MENU_TOOLS_RADIO_BUTTON_WRAP_TEXT);
            cmbItemShowWhiteSpace.setText(Constants.MFCE_MENU_TOOLS_RADIO_BUTTON_SHOW_WHITE_SPACE);
            cmbItemClearWhiteSpaceLine.setText(Constants.MFCE_MENU_TOOLS_RADIO_BUTTON_CLEAR_WHITE_SPACE_LINE);

            btnCompile.setText(Constants.MFCE_MENU_TOOLS_ITEM_COMPILE);
            btnCut.setText(Constants.MFCE_MENU_EDIT_ITEM_CUT);
            btnCopy.setText(Constants.MFCE_MENU_EDIT_ITEM_COPY);
            btnPaste.setText(Constants.MFCE_MENU_EDIT_ITEM_PASTE);
            btnClose.setText(Constants.MFCE_MENU_CLASS_ITEM_CLOSE);


            // Popisky tlačítek:
            btnCompile.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_COMPILE);
            btnCut.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_CUT);
            btnCopy.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_COPY);
            btnPaste.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_PASTE);
            btnClose.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_CLOSE);
        }
    }


    /**
     * Metoda, která "znepřístupní" - zakáže tlačítko Zkompilovat
     */
    final void setEnabledButtonCompile() {
        btnCompile.setEnabled(false);
    }


    /**
     * Nastavení označení komponenty, která značí, zda se mají zalamovat texty v editoru.
     *
     * @param selected
     *         - true, pokud se mají zalamovat texty v editoru, jinak false. True v případě, že má být komponenta
     *         označena.
     */
    final void setSelectedRbWrapText(final boolean selected) {
        cmbItemWrapText.setSelected(selected);
    }


    /**
     * Nastavení označení komponenty, která značí, zda se mají zobraovat bílé znaky v editoru.
     *
     * @param selected
     *         - true v případě, že se mají zobrazovat bílé znaky v editoru, jinak false.
     */
    final void setSelectedRbShowWhiteSpace(final boolean selected) {
        cmbItemShowWhiteSpace.setSelected(selected);
    }


    /**
     * Nastavení označení komponenty, která značí, zda se má po stisknutí klávesy Enter v editoru (vložení nového řádku)
     * vyčistit vždy ten předchozí od bílých znaků - v případě, že je obsahuje.
     *
     * @param selected
     *         - true v případě, že se mají čistit "odentrované" řádky. Jinak false.
     */
    final void setSelectedRbClearWhiteSpaceLine(final boolean selected) {
        cmbItemClearWhiteSpaceLine.setSelected(selected);
    }


    /**
     * Getr na komponentu, která značí, zda je nastavené zalamování textů nebo ne.
     *
     * @return true v případě, že je komponenta pro nastavení zalamování textů označena (mají se zalamovat texty). Jinak
     * false.
     */
    final boolean isRbWrapTextSelected() {
        return cmbItemWrapText.isSelected();
    }


    /**
     * Getr na označení komponenty, která značí, zda se mají v editoru zobrazovat bílé znaky.
     *
     * @return true v případě, že je komponenta označena, tj. mají se v editoru zobrazovat bílé znaky. Jinak false
     */
    final boolean isRbShowWhiteSpaceSelected() {
        return cmbItemShowWhiteSpace.isSelected();
    }


    /**
     * Getr na označení komponenty, která značí, zda se mají po "odentrování" řádku vymazat bílé znaky z toho předhozího
     * ("odentrovaného") řádku - pokud je obsahuje.
     *
     * @return true v případě, že je komponenta označena, tj. mají se odebírat bílé znaky z "odentrovaných" řádku. Jinak
     * false.
     */
    final boolean isRbClearWhiteSpaceLineSelected() {
        return cmbItemClearWhiteSpaceLine.isSelected();
    }


    /**
     * Metoda, která každému tlačítku v menu nastaví ikonu
     */
    private void addIconToButtons() {
        btnClose.setIcon(GetIconInterface.getMyIcon("/icons/app/CloseIcon.png", true));
        btnCut.setIcon(GetIconInterface.getMyIcon("/icons/app/CutIcon.jpg", true));
        btnCopy.setIcon(GetIconInterface.getMyIcon("/icons/app/CopyIcon.png", true));
        btnPaste.setIcon(GetIconInterface.getMyIcon("/icons/app/PasteIcon.png", true));
        btnCompile.setIcon(GetIconInterface.getMyIcon("/icons/app/CompileIcon.png", true));
    }


    @Override
    public void actionPerformed(final ActionEvent e) {
        if (e.getSource() instanceof JButton) {
            if (e.getSource() == btnCompile) {
                // Note: Zde mohu vytvořit novou instanci třídy EditCodeOfClass, abych nemusel zbytečně předávat
                // referenci, aby nezabírala pamět,
                // takto bude existovat pouze po dobu metody
                final Class<?> clazz = sharedClass.getCompiledClass(new ReadWriteFile(), languageProperties, true);

                codeEditor.setAutoCompletion(clazz);
            }

            else if (e.getSource() == btnCut)
                sharedClass.cutString();


            else if (e.getSource() == btnCopy)
                sharedClass.copyString();


            else if (e.getSource() == btnPaste)
                sharedClass.insertString();


            else if (e.getSource() == btnClose)
                sharedClass.closeDialog();
        }
    }
}