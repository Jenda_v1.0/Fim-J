package cz.uhk.fim.fimj.code_editor.generate_access_methods;

import java.util.Arrays;
import java.util.Vector;

/**
 * Tato třída slouží jako "pojmenování" položky v dialogu pro generování přístupových mtod
 * 
 * Tento Vektor je v podstatě stromový kořen pro položky - JCheckboxy ve finálním stromu
 * v tomto vektoru budou vždy dvě položky - getr a setr na danou položku třídy
 * 
 * Příklad:
 *  
 * Vector
 *     - Getr      (JCechbox pro oznčaní položky)
 *     - Setr
 *     
 * Vector 2
 *      - Getr
 *      - Setr
 * ...
 * 
 * 
 * 
 * Note: kořen celého stromu nebude vidět - metoda: tree.setRootVisible(false);
 * Kořen
 *   - Vector
 *        - Getr      (JCechbox pro oznčaní položky)
 *        - Setr
 *     
 *    - Vector 2
 *        - Getr
 *        - Setr
 * ...
 * 
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class NamedVector extends Vector<Object> {


	private static final long serialVersionUID = 1L;


	private final String name;


	public NamedVector(final String name) {
		super();

		this.name = name;
	}


	public NamedVector(final String name, final Object[] elements) {
		super();

		this.name = name;

		this.addAll(Arrays.asList(elements));
	}


	@Override
	public final String toString() {
		return name;
	}
}