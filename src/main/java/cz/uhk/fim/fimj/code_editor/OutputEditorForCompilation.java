package cz.uhk.fim.fimj.code_editor;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Properties;

import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import cz.uhk.fim.fimj.clear_editor.ClearInterface;
import cz.uhk.fim.fimj.clear_editor.PopupMenu;
import cz.uhk.fim.fimj.file.OutputEditorInterface;
import cz.uhk.fim.fimj.output_editor.OutputEditor;

/**
 * Tato třída slouží jako "Editor výstupů" pro kompilaci třídy, jejíž zdrojový kód je editorvám v editoru kódu (dialogu
 * - CodeEditorDialog.java)
 * <p>
 * Do tohoto dialogu lze pouze zapisovat výstupy z kompilace dané třídy - pokud byla Úspěšně zkompilována, případně
 * výpisy o vzniklé chybě, apod.
 * <p>
 * Uživatel s tímto dialogem v podstatě nemůže nic dělat, jedná se pouze o zobrazení informací
 * <p>
 * <p>
 * Note: Tento editor si ani nebude načítat nastavení ze souboru, bude mít "pevné" výchozí nastavení, které nelze
 * změnit. (pevný font, barva písma, pozadí, ...)
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class OutputEditorForCompilation extends JTextArea implements OutputEditorInterface, ClearInterface {

    private static final long serialVersionUID = 1L;


    /**
     * Objekt, který obsahuje texty pro tuto aplikace v uživatelem zvoleném jazyce.
     */
    private final Properties languageProperties;


    /**
     * Konstruktor této třídy.
     *
     * @param languageProperties
     *         - objekt, který obsahuje veškeré texty pro tuto aplikaci v uživatelem zvoleném jazyce.
     */
    public OutputEditorForCompilation(final Properties languageProperties) {
        super();

        this.languageProperties = languageProperties;

        setSettingsEditor();

        addMouseListener();


        // Klávesová zkratka Ctrl + L pro vymazání textu z editoru:
        addKeyListener(PopupMenu.getKeyAdapterForClearEditor(this));
    }


    /**
     * Metoda, která přidá posluchač na uvolnění pravého tlačítka myši, kdy se otevře kontextové menu, k teré bude
     * obsahovat jedinou položku, která bude sloužít pro vymazání veškerého textu z tohoto editoru.
     */
    private void addMouseListener() {
        addMouseListener(new MouseAdapter() {

            @Override
            public void mouseReleased(MouseEvent e) {
                /*
                 * Pokud lze editor výstupů editovat, tj. je otevřen nějaký projekt a uvolní se
                 * pravé tlačítko myši, pak otevřu kontxtové menu s položkou pro vymazání textu.
                 */
                if (isEnabled() && SwingUtilities.isRightMouseButton(e))
                    PopupMenu.createPopupMenu(languageProperties, OutputEditorForCompilation.this,
                            OutputEditorForCompilation.this, e);
            }
        });
    }


    @Override
    public void addResult(final String result) {
        append(OutputEditor.NEW_LINE_TWO_GAPS + result + OutputEditor.NEW_LINE_TWO_GAPS);
    }


    @Override
    public void setSettingsEditor() {
        // zakážu editaci:
        setEditable(false);


        // Pro tento "Editor" nastavím výchozí font
        setFont(new Font("Default font for OutputEditor for results of the compilaton of Class.", Font.BOLD, 15));


        // a výchozí pozadí:
        setBackground(new Color(220, 220, 220));
    }


    @Override
    public void clearEditor() {
        setText(null);
    }
}