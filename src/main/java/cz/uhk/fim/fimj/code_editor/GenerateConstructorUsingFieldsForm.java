package cz.uhk.fim.fimj.code_editor;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;

import cz.uhk.fim.fimj.file.*;
import cz.uhk.fim.fimj.forms.SourceDialogForm;
import cz.uhk.fim.fimj.reflection_support.ParameterToText;

/**
 * Tato třída slouží jako dialog pro výběr konstruktoru, který chce zadat
 * <p>
 * V dialogu je třeba označit položky - proměnné, které chce uživatel přidat do konstruktoru, dále jestli má být
 * konstruktor privátní, veřejný nebo chráněný
 * <p>
 * Položky - proměnné jsou načteny do pole, z něj si "vytáhnu" jejich názvy, které přidám do kolekce s checkbocy, které
 * si uživatel zaškrtne a zaškrtnuté položky budou přidány do parametru konstruktory - ve výsledku
 * <p>
 * Zdroj pro zaškrtávání: http://stackoverflow.com/questions/17951886/cannot-add-checkbox-to-the-jlist
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class GenerateConstructorUsingFieldsForm extends SourceDialogForm
		implements LanguageInterface, ActionListener {
	
	private static final long serialVersionUID = 1L;


	/**
	 * Kolekce, ve které budou uloženy checkbocy, které budou reprezentovat položku
	 * - proměnnou ve třídě Uživatel jej bude moci zaškrtnou a tím označí položku,
	 * která se přidá do parametru výsledného konstruktoru
	 */
	private final transient List<FieldAndCheckBox> chcbFieldsList;
	
	
	/**
	 * Label s informací o zaškrtávání položek v listu
	 */
	private final JLabel lblInfo;
	
	
	/**
	 * Tlačítka, které mohou označit všechny položky v listu nebo je Odznačit
	 */
	private final JButton btnSelectAll;
	private final JButton btnDeselectAll;
	
	
	/**
	 * JCheckBox pro označení, zda se mají ke konstruktoru vytvořit i dokumentační
	 * komentáře:
	 */
	private final JCheckBox chcbGenerateDocComment;
	
	
	/**
	 * Checkboxy, které určí modifikátor přísupu konstruktoru - zda má být
	 * konstruktor privátní, veřejný nebo chráněný
	 */
	private final JRadioButton chcbPrivate;
    private final JRadioButton chcbPublic;
    private final JRadioButton chcbProtected;
	
	
	/**
	 * Následující proměnná musí bát globální / instanční proměnná, aych danému
	 * panelu mohl nastavit ohraničení s textem ve správném jazyce
	 */
	private final JPanel pnlChcbModifier;
	
	
	/**
	 * Následující proměnné - JList s JchceckBoxy coby položkami musí být také
	 * globální neboli instanřní proměnná, protože potřebuji při kliknutí na tlaříka
	 * odznacit vse nebo oznacit vse provest prislusnou operaci nad listem a ten
	 * překreslit
	 */
	private final JList<FieldAndCheckBox> list;
	
	
	/**
	 * Proměnná pro uchování názvu třídy, abych mohl "sestavit" konstruktor
	 */
	private final String className;
	
	
	/**
	 * Konstruktor této třídy s parametr, který značí načtenou třídu - její soubor s
	 * příponou .class, abych mohl získat její položky
	 * 
	 * @param clazz
	 *            - načtený soubor .class třídy otevřené v editoru zdrojového kódu
	 */
	public GenerateConstructorUsingFieldsForm(final Class<?> clazz) {
		super();
		
		className = clazz.getSimpleName();
		
		chcbFieldsList = new ArrayList<>();
		
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/app/ConstructorIcon.png", new Dimension(470, 500), true);
		
		
		
		
		gbc = createGbc();
		
		index = 0;
		
		
		gbc.gridwidth = 2;
		
		// Panel s labelem s informací o zaškrtávání položek v listu:
		final JPanel pnlLblInfo = new JPanel(new FlowLayout(FlowLayout.LEFT));		
		lblInfo = new JLabel();
		pnlLblInfo.add(lblInfo);
		
		setGbc(index, 0, pnlLblInfo);
		
		
		
		
		
		
		// Zjistím si proměnné z třídy:
		final Field[] fieldsArray = clazz.getDeclaredFields();
		
		// Naplním kolekci proměnnýma:
		for (final Field f : fieldsArray)
			createAndAddCheckBoxToList(f);
			
		

		
		
		// Model do Jlistu:
		final DefaultListModel<FieldAndCheckBox> model = new DefaultListModel<>();
		
		// Jlist, do kterého vložím JChecboxy pro označení položky pro přidání jako parametru do výsledného konstruktoru
		list = new JList<>(model);
		
		// Renderer, aby sli vykreslovat JCheckBoxy:
		list.setCellRenderer(new CheckBoxListCellRenderer());
	
		// Přidám do modelu načtené položky u třídy:
		for (final FieldAndCheckBox f : chcbFieldsList)
			model.addElement(f);
		
		
		
		list.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				@SuppressWarnings("unchecked")
				final JList<FieldAndCheckBox> chcbsList = (JList<FieldAndCheckBox>) e.getSource();
			
				// Index položky, na kterou uživatel klikl
				final int index = chcbsList.locationToIndex(e.getPoint());
				
				// zjistím si položku (konkrétně mně zajímá JcheckBox), na kterou se klilo
				final FieldAndCheckBox item = chcbsList.getModel().getElementAt(index);
				
				// Změním její hodnotu 
				item.setSelected(!item.isChcbSelected());
				
				// Překlreslení
				chcbsList.repaint(chcbsList.getCellBounds(index, index));
			}
		});
		
		final JScrollPane jspList = new JScrollPane(list);		
		
		setGbc(++index, 0, jspList);
		
		
		
		
		
		
		
		
		final JPanel pnlButtonsSelect = new JPanel(new FlowLayout(FlowLayout.CENTER));
		
		btnSelectAll = new JButton();
		btnDeselectAll = new JButton();
		
		btnSelectAll.addActionListener(this);
		btnDeselectAll.addActionListener(this);
		
		pnlButtonsSelect.add(btnSelectAll);
		pnlButtonsSelect.add(btnDeselectAll);
		
		setGbc(++index, 0, pnlButtonsSelect);
		
		
		
		
		
		
		
		pnlChcbModifier = new JPanel();
		pnlChcbModifier.setLayout(new FlowLayout(FlowLayout.CENTER));
		
		chcbPublic = new JRadioButton("public");
		chcbPrivate = new JRadioButton("private");
		chcbProtected = new JRadioButton("protected");
		
		final ButtonGroup bg = new ButtonGroup();
		bg.add(chcbPublic);
		bg.add(chcbPrivate);
		bg.add(chcbProtected);
		chcbPublic.setSelected(true);
		
		pnlChcbModifier.add(chcbPublic);
		pnlChcbModifier.add(chcbPrivate);
		pnlChcbModifier.add(chcbProtected);
		
		setGbc(++index, 0, pnlChcbModifier);
		
		
		
		
		
		
		
		final JPanel pnlGeneraceComment = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		chcbGenerateDocComment = new JCheckBox();
		pnlGeneraceComment.add(chcbGenerateDocComment);
		
		setGbc(++index, 0, pnlGeneraceComment);
		
		
		
		
		
		
		
		
		
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		btnOk = new JButton();
		btnCancel = new JButton();
		
		btnOk.addActionListener(this);
		btnCancel.addActionListener(this);
		
		pnlButtons.add(btnOk);
		pnlButtons.add(btnCancel);
		
		setGbc(++index, 0, pnlButtons);
				
			
		
		
		
		
		addWindowListener(getWindowsAdapter());
		
		
		
		
		
		pack();
		setLocationRelativeTo(null);
	}
	
	

	
	
	
	
	
	/**
	 * Metoda, která vytvoří "nový" konstruktor, s parametry - dle zaškrtnutých
	 * položek v listu a modifikátor dle zaškrtnutého JCechBoxu s modifikatorem
	 * 
	 * @return tento vytvřený konstruktor v podobě textu, který se následně přídá do
	 *         editrou (- jiná část app)
	 */
	final String getNewConstructor() {
		setVisible(true);

		if (!variable)
			return null;

		final StringBuilder constructorBuilder = new StringBuilder();
		constructorBuilder.append("\n\n");

		if (chcbGenerateDocComment.isSelected()) {
			// Zde se mají ke konstruktoru vygenerovat dokumentační komentář:

			constructorBuilder.append("\t/**\n");
			constructorBuilder.append("\t*\n");

			for (final FieldAndCheckBox f : chcbFieldsList) {
				if (f.isChcbSelected())
					constructorBuilder.append("\t* @param ").append(f.getFieldName()).append("\n");
			}

			constructorBuilder.append("\t*/\n");
		}


		if (chcbPublic.isSelected())
			constructorBuilder.append("\tpublic");

		else if (chcbPrivate.isSelected())
			constructorBuilder.append("\tprivate");

		else if (chcbProtected.isSelected())
			constructorBuilder.append("\tprotected");

		constructorBuilder.append(" ").append(className).append("(");


		for (final FieldAndCheckBox f : chcbFieldsList) {
			if (!f.isChcbSelected())
				continue;

			/*
			 * nejprve si zjistím datový typ příslušné (právě iterované) proměnné, zda je to
			 * list, mapa nebo "klasická" proměnná, pak , pak přidám mezeru a za ni název
			 * proměnné.
			 */
			constructorBuilder.append(ParameterToText.getFieldInText(f.getField(), false));

			// Mezera mezi datovým typem parametru a názvem parametru:
			constructorBuilder.append(" ");

			// Název parametru:
			constructorBuilder.append(f.getFieldName()).append(", ");
		}

		// Odeberu poslední čárku za parametrem - pokud se tam vyskytuje:
		if (constructorBuilder.toString().endsWith(", "))
			constructorBuilder.setLength(constructorBuilder.length() - 2);

		constructorBuilder.append(") {\n");

		constructorBuilder.append("\t\tsuper();\n\n");


		/*
		 * Zde je třeba cyklem iterovat veškeré položky znovu a u těch, které uživatel označil přidat do těla
		 * konstruktoru jejich naplnění.
		 */
		for (final FieldAndCheckBox f : chcbFieldsList) {
			if (f.isChcbSelected())
				constructorBuilder.append("\t\tthis.").append(f.getFieldName()).append(" = ").append(f.getFieldName()).append(";\n");
		}

		constructorBuilder.append("\t}\n\n");

		return constructorBuilder.toString();
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoři JCheckBox s daným názvem proměnné a přidá jej do
	 * kolekce, která bude načtena do dialogu pro označení položek
	 * 
	 * @param field
	 *            - proměnná z instance třídy, kterou uživatel bude moci zaškrtnout
	 *            v dialogu
	 */
	private void createAndAddCheckBoxToList(final Field field) {
		final JCheckBox chcbField = new JCheckBox(field.getName());		
		
		chcbFieldsList.add(new FieldAndCheckBox(field, chcbField));
	}
	
	
	
	
	

	
	
	
	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			setTitle(properties.getProperty("Gcuff_DialogTitle", Constants.GCUFF_DIALOG_TITLE));
			
			lblInfo.setText(properties.getProperty("Gcuff_LabelInfo", Constants.GCUFF_LBL_INFO));
			
			btnOk.setText(properties.getProperty("Gcuff_ButtonOk", Constants.GCUFF_BTN_OK));
			btnCancel.setText(properties.getProperty("Gcuff_ButtonCancel", Constants.GCUFF_BTN_CANCEL));
			
			btnSelectAll.setText(properties.getProperty("Gcuff_ButtonSelectAll", Constants.GCUFF_BTN_SELECT_ALL));
			btnDeselectAll.setText(properties.getProperty("Gcuff_ButtonDeselectAll", Constants.GCUFF_BTN_DESELECT_ALL));
			
			chcbGenerateDocComment.setText(properties.getProperty("Gcuff_ChcbGenerateDocDocument", Constants.GCUFF_CHCB_GENERATE_DOC_DOCUMENT));
			
			
			pnlChcbModifier.setBorder(BorderFactory.createTitledBorder(properties.getProperty("Gcuff_PanelChcbModifierTitle", Constants.GCUFF_PNL_CHCB_MODIFIER)));
		}
		
		else {
			setTitle(Constants.GCUFF_DIALOG_TITLE);
			
			lblInfo.setText(Constants.GCUFF_LBL_INFO);
			
			btnOk.setText(Constants.GCUFF_BTN_OK);
			btnCancel.setText(Constants.GCUFF_BTN_CANCEL);
			
			btnSelectAll.setText(Constants.GCUFF_BTN_SELECT_ALL);
			btnDeselectAll.setText(Constants.GCUFF_BTN_DESELECT_ALL);
			
			chcbGenerateDocComment.setText(Constants.GCUFF_CHCB_GENERATE_DOC_DOCUMENT);
			
			
			pnlChcbModifier.setBorder(BorderFactory.createTitledBorder(Constants.GCUFF_PNL_CHCB_MODIFIER));
		}
	}

	
	
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {// Zbytečná podmínka.
			if (e.getSource() == btnOk) {
				// Zde nemusím nic řešit, uživtel klikl na Ok, tak to potvrdím, zde je již vše předpřipravené - alespoň "základy"
				variable = true;
				dispose();
			}
			
			else if (e.getSource() == btnCancel) {
				variable = false;
				dispose();
			}
			
			else if (e.getSource() == btnSelectAll) {
				chcbFieldsList.forEach(f -> f.setSelected(true));
				list.repaint();
			}
			
			else if (e.getSource() == btnDeselectAll) {
				chcbFieldsList.forEach(f -> f.setSelected(false));
				list.repaint();
			}
		}
	}
}