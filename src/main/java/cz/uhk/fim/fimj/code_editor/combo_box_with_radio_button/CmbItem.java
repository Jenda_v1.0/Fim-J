package cz.uhk.fim.fimj.code_editor.combo_box_with_radio_button;

/**
 * Třída, která obsahuje položky, které jsou potřeba pro zobrazení jako {@link javax.swing.JRadioButton}.
 *
 * Zdroj:
 *
 * http://www.java2s.com/Tutorials/Java/Swing_How_to/JComboBox/Add_JCheckBox_components_to_JComboBox.htm
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 01.09.2018 13:46
 */

public class CmbItem {

    /**
     * Text, který bude zobrazen.
     */
    private String text;

    /**
     * Zda je "JRadioButton" (/ tato položka) označena nebo ne.
     */
    private boolean selected;


    /**
     * Getr na text, který má být zobrazen.
     *
     * @return text, který má být zobrazen (v cmb).
     */
    public String getText() {
        return text;
    }


    /**
     * Setr na text, který má být zobrazen.
     *
     * @param text
     *         - text, který má být zobrazen.
     */
    public void setText(final String text) {
        this.text = text;
    }


    /**
     * Getr na zjištění, zda je "tato" položka (v cmb) označena nebo ne.
     *
     * @return true v případě, že je položka označena, jinak false.
     */
    public boolean isSelected() {
        return selected;
    }


    /**
     * Setr na proměnnou, která značí, zdaje tato položka (v cmb) označena nebo ne.
     *
     * @param selected
     *         - true, když je / má být položka označena, jinak false.
     */
    public void setSelected(final boolean selected) {
        this.selected = selected;
    }
}
