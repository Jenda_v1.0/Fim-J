package cz.uhk.fim.fimj.code_editor.generate_access_methods;

import cz.uhk.fim.fimj.reflection_support.ParameterToText;

import java.lang.reflect.Field;

/**
 * Tato třída slouží jako v podstatě jako samotný JCheckbox, který bude ve finálním stromu v dialogu zobrazen
 * <p>
 * Také se dá napsat, že tato třída slouží coby konkrétní položka pro getr nebo setr na danou položku ve třídě v editoru
 * kodu, kterou je možné označeit, a dle toho, zda bude označená se budˇgetr nebo settr na danou pložku vygeneruje nebo
 * ne
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class CheckBoxNode {

    /**
     * text v JChecboxu ve stromu v dialogu
     */
    private final String text;


    /**
     * Hodnota, zda je Checkbox zaškrtnutý či nikoliv
     */
    private boolean selected;


    /**
     * Proměnná, kterou instance této třídy "reprezentuje"
     */
    private final Field field;


    /**
     * Prměnná, dle které zjistím, zda instance této třídy reprezentuje getr nebo setr na výše uvedenou proměnnou typu
     * Field (proměnná z třídy)
     */
    private final boolean setter;


    /**
     * "Kopykonstruktor" této třídy, který vytvoří instancí této třídy, a změní hodnotu pro zaškrtnutí daného
     * JCheckBoxu
     * <p>
     * Tento konstruktor je volán po kliknutí na příslušný JCheckBox v dialogu ve stromu po kliknuti na libovolný
     * JCheckbox se vytvoří nový a zde si z něj vezmu všechny hodnoty kromě té pro zaškrtnutí, ta se po kliknutí na nej
     * v dialogu změnila, a v parametru konstruktoru je aktuální hodnota
     * <p>
     * Tento konstruktor je volán ze třídy: CheckBoxNodeEditor.java, který převezme data ze třídy:
     * CheckBoxNodeRenderer.java
     *
     * @param node
     *         - intance této třídy, ze které se převezmu všechny hodnoty, kromě hodnoty, která "říká", zda je označen
     *         daný JChexkBox ve stromu v dialogu
     * @param selected
     *         - hodnota, na kterou mám nastavit proměnnou selected - zda je JCheckbox zaškrtnutý nebo ne
     */
    CheckBoxNode(final CheckBoxNode node, final boolean selected) {
        this.selected = selected;

        text = node.getText();

        field = node.getField();

        setter = node.isSetter();
    }


    /**
     * Konstruktor této třídy, který se zavolá pro vytvoření "původní" položky ve stromu - v dialogu - Slouží pro úplně
     * první vytvoření dané položky, kde se třídí, zda se má vytvořit getr nebo setr na danou položku v parametru
     *
     * @param field
     *         - položka / proměnná z třídy, na kterou se má vytvořit getr nebo setr (dle následujícího parametru)
     * @param setter
     *         - logická hodnota o tom, zda se má na položku v předchozím parametru (field) vytvořit setr nebo ne pokud
     *         je true, pak se na danou proměnnou má vyvořit setr, jinak getr
     */
    CheckBoxNode(final Field field, final boolean setter) {

        this.field = field;
        this.setter = setter;

        // Výchozí hodnota bude vžy na false pro následující proměnnou:
        // - vždy bude Odškrtnuty JCheckBox - jako výchozí hodnota:
        selected = false;

        // uložím si jméno proměnné a první písmeno bude vždy velké:
        final String fieldName = String.valueOf(Character.toUpperCase(field.getName().charAt(0)))
                + field.getName().substring(1);

        // Zde otestuji, zda mám vytvořit getr nebo setr na danou proměnou
        if (setter)
            // Zde mám na danou proměnnou vytvořit setr:
            text = "set" + fieldName + "(" + ParameterToText.getFieldInText(field, false) + ")";

            // Zde mám na danou proměnnou vytvořit getr:
        else {
            // Nejprve otestuji, zda je to boolean, pokud ano, pak metoda bude začínat s 'is', jinak s 'get':
            if (field.getType().equals(Boolean.class) || field.getType().equals(boolean.class))
                text = "is" + fieldName + "()";
            else
                text = "get" + fieldName + "()";
        }
    }


    /**
     * Metoda, která vrátí logickou hodnotu o tom, zda je daný JCheckbox zaškrtnutý nebo ne
     *
     * @return true / false - dle toho, zda je JCheckBox zaškrtnutý
     */
    public final boolean isSelected() {
        return selected;
    }


    /**
     * Metoda, která zaškrtně nebo Odškrtne JCheckBox
     *
     * @param newValue
     *         logická hodnota o tom, zda se má daný JCheckBox zaškrtnou nebo Odškrtnout
     */
    public final void setSelected(final boolean newValue) {
        selected = newValue;
    }


    /**
     * Metoda, která vrátí položku - proměnnou z třídy
     *
     * @return proměnnou z třídy
     */
    public final Field getField() {
        return field;
    }


    /**
     * Metoda, která vrátí logickou hodnotu o tom, zda tato instance této konkrétní třídy coby rebrezentace getru nebo
     * setru reprezentuje právě getr nebo setr
     *
     * @return true, pokud se jedná o setr, jinak false
     */
    public final boolean isSetter() {
        return setter;
    }


    /**
     * Metoda, která vrátí text u JCheckBoxu
     *
     * @return text u JCheckBoxu
     */
    public final String getText() {
        return text;
    }
}