package cz.uhk.fim.fimj.code_editor;

import java.io.File;
import java.util.List;
import java.util.Properties;

import javax.swing.JOptionPane;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.project_explorer.ReadWriteFileInterface;

/**
 * Tato třída slouží pouze jak název říká "sdílená třída" pro menu a toolbar s tlačítky v dialogu pro editor zdrojového
 * kódu.
 * <p>
 * V této tříde jsou implementovány pouze některé metody, které jsou obsaženy v Menu zmíněného dialogu pro editor
 * zdrojového kódu a pro JToolbar s některými tlačítky
 * <p>
 * V podstatě stručně řečeno, tato třída obsahuje metody, které se vykonaní po kliknutí na tlačíko v toolbaru nebo na
 * některou položku v menu v dialogu pro editor zdrojového kódu
 * <p>
 * Tuto třídu jsem napsal, proto abych nemusel psát dvakrát stené metody, také je možné nechat tyto metody
 * implementované například v menu a předat odkaz na tyto metody do Jtoolbaru.
 * <p>
 * Konkrétně se jedná o metody - operace, které se vykonají po kliknutí na tlačítka: Zkompilovat, Vyjmou, Kopírovat,
 * Vložit a Zavřít
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class SharedClassForMethods implements SharedClassForMethodsInterface {

	/**
	 * Proměnné, do které vložím označené části textu z editoru po kliknutí na
	 * položky v menu pro zkopírování či vyjmutí text z editoru dále z tété proměnné
	 * vezmu hodnotu pro vložení textu do editoru
	 * 
	 * Note:
	 * Toto nijak nesouvisí se zkratkami Ctrl + X, C a V - tyto položky v menu si
	 * ukládají vlastni hodnotu
	 */
	private String variableCutCopy;
	
	
	
	/**
	 * Proměnná pro uchování reference do editoru zdrojového kódu pro manipulaci s
	 * ním (viz metody níže)
	 */
	private CodeEditorDialog codeEditorDialog;
	
	
	
	
	
	/**
	 * Konstruktor této řídy.
	 * 
	 * @param codeEditorDialog
	 *            - dialog s editorem zdrojového kódu.
	 */
	public SharedClassForMethods(final CodeEditorDialog codeEditorDialog) {
		super();
		
		this.codeEditorDialog = codeEditorDialog;
	}
	
	
	
	
	
	@Override
	public Class<?> getCompiledClass(final ReadWriteFileInterface editCodeOfClass, final Properties languageProperties,
			final boolean writeResult) {
		// proměnné pro uchování textů ve zvolenem jazyce do chybovych hlasek:
		
		final String txtErrorWhileSavingChangesToClassText_1;
		final String txtErrorWhileSavingChangesToClassText_2;
		final String txtPath;
		final String txtErrorWhileSavingChangesToClassTitle;
		final String txtClassNotFoundText;
		final String txtClassNotFoundTitle;
		
		
		if (languageProperties != null) {
			txtErrorWhileSavingChangesToClassText_1 = languageProperties.getProperty("Ced_Mfce_JopErrorWhileSavingChangesToClassText_1", Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TEXT_1);
			txtErrorWhileSavingChangesToClassText_2 = languageProperties.getProperty("Ced_Mfce_JopErrorWhileSavingChangesToClassText_2", Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TEXT_2);
			txtErrorWhileSavingChangesToClassTitle = languageProperties.getProperty("Ced_Mfce_JopErrorWhileSavingChangesToClassTitle", Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TITLE);
			txtPath = languageProperties.getProperty("Ced_Mfce_TextPath", Constants.NFCE_TXT_PATH);
			txtClassNotFoundText = languageProperties.getProperty("Ced_Mfce_JopClassNotFoundText", Constants.NFCE_JOP_CLASS_NOT_FOUND_TEXT);
			txtClassNotFoundTitle = languageProperties.getProperty("Ced_Mfce_JopClassNotFoundTitle", Constants.NFCE_JOP_CLASS_NOT_FOUND_TITLE);
		}
		
		else {
			txtErrorWhileSavingChangesToClassText_1 = Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TEXT_1;
			txtErrorWhileSavingChangesToClassText_2 = Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TEXT_2;
			txtErrorWhileSavingChangesToClassTitle = Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TITLE;
			txtPath = Constants.NFCE_TXT_PATH;
			txtClassNotFoundText = Constants.NFCE_JOP_CLASS_NOT_FOUND_TEXT;
			txtClassNotFoundTitle = Constants.NFCE_JOP_CLASS_NOT_FOUND_TITLE;
		}
		
		
		
		
		// Uložím kód třídy, poté ho zkompiluji
		// a výsledek se vyíše do editoru výstupů
		
		
		if (existFile(codeEditorDialog.getPathToClass())) {
			// zde daná třída ještě existuje, mohu uložit její kód:
			
			
			// Načtu si kód původní třídy, abych otestoval, zda byl kód změněn:
			final List<String> codeOfOriginalClass = editCodeOfClass.getTextOfFile(codeEditorDialog.getPathToClass());
		
			
			// Otstuji, zda jsou kódy stejné:
			final boolean areCodesIdenticaly = codeEditorDialog.getCodeEditor().compareSourceCode(codeOfOriginalClass);
		
			
			// Proměnná, do které uložím, zda byl kód zapsaán v pořádku, nebo true, pokud jsou kódy stejné
			final boolean isWritten;
			
			
			if (!areCodesIdenticaly) {
				// Zde kódy nejsou identické, tak ho uložím:
				final List<String> codeFromEditor = codeEditorDialog.getCodeEditor().getTextFromCodeEditor();
				
				isWritten = editCodeOfClass.setTextToFile(codeEditorDialog.getPathToClass(), codeFromEditor);
				
				if (!isWritten)
					JOptionPane.showMessageDialog(null,
							txtErrorWhileSavingChangesToClassText_1 + "\n" + txtErrorWhileSavingChangesToClassText_2
									+ "\n" + txtPath + ": " + codeEditorDialog.getPathToClass(),
							txtErrorWhileSavingChangesToClassTitle, JOptionPane.ERROR_MESSAGE);
			}
			
			else isWritten = true;
			
			
			
			if (isWritten) {
				/*
				 * zde jsou kódy stejné, nebo byl v pořádku uložen, tak mohu zkompilovat třídu,
				 * akorát si ještě otestuji, zda se mají nebo nemají vypisovat výsledky ohledně
				 * kompilace třídy do editoru výstupů (ve spodní části dialogu).
				 */
				if (writeResult)
					return App.READ_FILE.getClassFromFile(codeEditorDialog.getClassNameWithDot(),
							codeEditorDialog.getOutputEditor(), GraphClass.getCellsList(), false, false);

				return App.READ_FILE.getClassFromFile(codeEditorDialog.getClassNameWithDot(), null,
						GraphClass.getCellsList(), false, false);
			}

			// Zde není třeba nic dělat navíc, hláška o kompilaci byla vypsána do editoru
			// výstupů v editoru kódu uživateli
		}
		
		else
			JOptionPane.showMessageDialog(null,
					txtClassNotFoundText + "\n" + txtPath + ": " + codeEditorDialog.getPathToClass(),
					txtClassNotFoundTitle, JOptionPane.ERROR_MESSAGE);
		
		return null;
	}

	
	
	
	
	
	@Override
	public void cutString() {
		// Vložím označený text do proměnné:
		variableCutCopy = codeEditorDialog.getCodeEditor().getSelectedText();
		
		// Odeberu označený text v editoru kódu:
		codeEditorDialog.getCodeEditor().replaceSelection("");
	}
	
	
	
	

	@Override
	public void copyString() {
		// Vložím označený text do proměnné "- schránky" v editoru kódu:
		variableCutCopy = codeEditorDialog.getCodeEditor().getSelectedText();	
	}

	
	
	
	
	@Override
	public void insertString() {
		// Vložím na pozici v editoru kódu, kde se nachází kurzor text, který se nachází
		// ve schránce (pomocí metody insert):
		codeEditorDialog.getCodeEditor().insert(variableCutCopy, codeEditorDialog.getCodeEditor().getCaretPosition());
	}

	
	
	
	
	
	

	@Override
	public void closeDialog() {
		codeEditorDialog.dispose();
	}	
	
	
	
	
	
	/**
	 * Metoda, která vrátí, logickou hodnotu o tom, zda existuje soubor na dané
	 * cestě
	 * 
	 * Metoda otestuje existenci souboru na dané cestě
	 * 
	 * @param pathToFile
	 *            - cesta k souboru, jehož exitenci chci zjistit
	 * 
	 * @return true, pokud soubor existuje, jinak false
	 */
	@Override
	public boolean existFile(final String pathToFile) {
		final File file = new File(pathToFile);
		
		return file.exists();
	}
}