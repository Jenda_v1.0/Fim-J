package cz.uhk.fim.fimj.code_editor;

import java.util.Properties;

import cz.uhk.fim.fimj.project_explorer.ReadWriteFileInterface;

/**
 * Toto rozhraní obsahuje signatury a kontrakty metod pro provedení operací zmíněných v třídě
 * SharedClassForMethods.java
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface SharedClassForMethodsInterface {

    /**
     * Metoda, která zkompiluje třídu, jejíž kód je aktuálně otevřený v editoru zdrojového kódu a vypíše informace o
     * kompilace do editoru výstupů, který se nachází v editoru zdrojového kódu
     *
     * @param editCodeOfClass
     *         - reference na instanci třídy ReadWriteFile.java, pomocí které je možné získávat či ukládat hodnoty
     *         (resp. zdrojový kód) do nebo z Javovské třídy
     * @param languageProperties
     *         - reference na proměnnou typu Properties, ve které ze které si metoda zjistí, jaký text má použít do
     *         chybových hlášek
     * @param writeResult
     *         logická hodnota, která značí, zda se mají vypisovat výsledky ohledně komplaci tříd do editoru výstupů v
     *         příslušném dialogu, konkrétně v dialogu editor zdrojového kódu. Tj., když se například zkompiluje třída,
     *         která je otevřená v dialogu editorz zdrojového kódu, tak zda se mají vypsat výsledky ohledně kompilace
     *         nebo ne.
     * @return reference na soubor .class, vzniklýá po kompilace dané třídy - pokud se pdařilo, jinak null
     */
    Class<?> getCompiledClass(final ReadWriteFileInterface editCodeOfClass, final Properties languageProperties,
                              final boolean writeResult);


    /**
     * Metoda z editoru zdrojového kódu (z dialogu) vyjme kurzorem myši označenou část textu (zdrojového kódu) a vloží
     * jej do proměnné, ze které je dále možno opět hodnotu získat - jiné metody
     */
    void cutString();


    /**
     * Metoda, které z editoru zdrojového kódu zkopíruje označenou část textu (zdrojového kódu) a vloží jej do proměnné
     * coby "scharánky" pro další využití
     */
    void copyString();


    /**
     * Metoda, která si vezme text z proměnné (variableCutCopy) a vloží jej na pozici, kde se v editoru zdrojového kódu
     * (dialogu) nachzí kurzor myši
     */
    void insertString();


    /**
     * Metoda, která uzavře dialog (= editor zdrojového kódu)
     */
    void closeDialog();


    /**
     * Metoda, která vrátí, logickou hodnotu o tom, zda existuje soubor na dané cestě
     * <p>
     * Metoda otestuje existenci souboru na dané cestě
     *
     * @param pathToFile
     *         - cesta k souboru, jehož exitenci chci zjistit
     * @return true, pokud soubor existuje, jinak false
     */
    boolean existFile(final String pathToFile);
}
