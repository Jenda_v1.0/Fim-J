package cz.uhk.fim.fimj.code_editor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.File;
import java.util.List;
import java.util.Properties;

import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.code_editor_abstract.RSyntaxTextAreaKeyActionHelper;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.language.EditProperties;
import org.fife.ui.rtextarea.RTextScrollPane;

import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.file.GetIconInterface;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.project_explorer.ReadWriteFile;

/**
 * Tato Třída slouží jako Jdialog (dialog) pro editaci zdrojového kódu zvolené Javovské třídy z diagramu tříd, tento
 * dialog lze otevřit po kliknutí pravým tlačítkem myší na buňku reprezentující třídu v diagramu tříd, zobrazí se menu s
 * "možnostmi" a je třeba zvolit položku "Otevřít v editoru", pak se zobrazí tento dialog se zdrojovým kódem dané třídy
 * <p>
 * Do tohoto dialogu vložím menu - resp. tomuto dialogu nastavim JMenu (Třída: JmenuForCodeËditor.java)
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class CodeEditorDialog extends JDialog implements LanguageInterface {

    private static final long serialVersionUID = 7887184491609611145L;


    /**
     * Menu musí být globální, neboli instanční proměnná, abych mu mohl nastavit text ve zvoleném jazyce v metodě
     * setLanguage
     */
    private final JMenuForCodeEditor menu;


    /**
     * Stejně tak i toolbar s tlačítky potřebuji texty pro tlačítka, tak mu také musím nastavit jazyk proto musí být
     * také globálně přístupný:
     */
    private final JToolBarForCodeEditor toolBar;


    private final transient SaveCodeAfterCloseDialog saveCodeAfterCloseDialog;


    /**
     * Proměnná, ve které bude uložena cesta ke třídě, jejiž kód je načtený v tomto dialogu - editoru
     */
    private final String pathToClassAll;

    /**
     * Proměnná, ve které bude uložen název třídy - včetně balíčků bud s tečkami nebo s limítky:
     */
    private final String classNameWithDot;


    /**
     * Musím z editoru kodu udělat Globální / Instanční proměnnéu, abych mohl přístupovat ke kódu v tomto editoru z menu
     * a z toolbaru
     */
    private final transient CodeEditor codeEditor;


    /**
     * Následující proměnná musí být globální / instanční proměnná, abych na ní mohl předávat referenci, případně nad ní
     * volat nějaké metody:
     */
    private final OutputEditorForCompilation outputEditor;


    /**
     * Proměnná pro "sdilení" operací pro operace: vyjmout, zkopirovat, vlozit a nějake operace, které lze provest jak v
     * liště toolbar, tak v menu v editoru zdrojového kódu (v tomto dialogu)
     */
    private static SharedClassForMethodsInterface sharedClass;


    /**
     * Načtený konfigurační soubor s hodnotami pro nastavení editoru zdrojového kódu.
     */
    private final Properties codeEditorProp;


    /**
     * Konstruktor této třídy který se zavolá, pokud je otevřena třída z diagramu tříd.
     *
     * @param pathToSrc
     *         - cesta k adresáři src v adresáři projěktu
     * @param pathToClass
     *         - cesta k třídě, kterou chce uživatel otevřít - cesta je akorát od adresáře src po konkrétní třídu
     * @param languageProperties
     *         - soubor typu Properties s texty ve zvoleném jazyce
     */
    public CodeEditorDialog(final String pathToSrc, final String pathToClass, final Class<?> clazz,
                            final Properties languageProperties, final GraphClass classDiagram) {
        /*
         * Zde si naplním proměnnou, která by měla obsahovat místo lomítek pouze tečky -
         * cesta ke třídě od adresáře src (bez adresáře src). Je to potřeba kvůli
         * kompilátoru, aby věděl jakou třídu zkompilovat, a dále jaká třída z jakého
         * balíčku se má načíst.
         */
        classNameWithDot = pathToClass.replace(File.separator, ".");
        // PŮVODNÍ: - nefungovalo by generování metod apod. Nenašel by se potřebný
        // soubor .class pro načtení:
        // this.classNameWithDot = pathToClass;


        pathToClassAll = pathToSrc + File.separator + pathToClass + ".java";

        // Jako titulek dialogu nastavím cestu k třídě, jejíž zdrojový kód je načten:
        setTitle(pathToClassAll);

        // Načtení konfiguračního souboru pro nastavení:
        codeEditorProp = App.READ_FILE.getCodeEditorProperties();

        initGui();


        sharedClass = new SharedClassForMethods(this);


        // Nastavím menu do okna aplikace:
        menu = new JMenuForCodeEditor(this, sharedClass, languageProperties);
        setJMenuBar(menu);

        /*
         * Povolení vybraných klávesových zkratek pro editor kódu. Jedná se o akce (zkratky), které byly zakázány pro
         * editor příkazů.
         */
        RSyntaxTextAreaKeyActionHelper.enableSpecificAction(true);

        // Vytvořit editor:
        codeEditor = new CodeEditor(this, ".java", true, codeEditorProp);
        final RTextScrollPane rspCodeEditor = new RTextScrollPane(codeEditor);


        // Přidám toolbar s tlačítky do dialogu hned pod menu - jako lišta s "nejvíce používanými tlačítky":
        toolBar = new JToolBarForCodeEditor(sharedClass, codeEditor);
        // Nastavení proměnné pro zalamování textuů:
        loadCodeEditorSettings(codeEditorProp);

        add(toolBar, BorderLayout.NORTH);


        // Vytvořit editro výstupů pro výstupní informace pro kompilaci a chyby:
        outputEditor = new OutputEditorForCompilation(languageProperties);
        final JScrollPane jspOutputEditor = new JScrollPane(outputEditor);


        final JSplitPane jspEditors = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true, rspCodeEditor, jspOutputEditor);
        jspEditors.setOneTouchExpandable(true);
        // Aby se posuvník nehýbal při změně velikosti okna, je třeba jej nastavit zde,
        // ale jinak až po zavolání metody pack:
        jspEditors.setResizeWeight(0.99d);

        add(jspEditors, BorderLayout.CENTER);


        // Zde se již vytvořily potřebné komponenty pro editraci kódu, tak nyní mohu načíst kód
        // z dané třídy:

        // Note: Zde již nemusím testovat,zda třída existuje nebo ne, to jsem již udělal před otevřením dialogu v
        // menu v diagramu tříd
        final List<String> textOfOriginalClass = new ReadWriteFile().getTextOfFile(pathToClassAll);

        // Nastavím text do editoru:
        codeEditor.setTextToCodeEditor(textOfOriginalClass);


        saveCodeAfterCloseDialog = new SaveCodeAfterCloseDialog(this, classDiagram, outputEditor);
        addWindowListener(saveCodeAfterCloseDialog);


        // Nejprve potřebuji nastavit jazyk do tothoto dialogu:
        setLanguage(languageProperties);
        // a nyní potřebuji vytvořit dialog s nápovědou:

        // Note: musím to vytvořit zde, protože potřebuji nejprve do dané třídy (codeEditor) doplnit jazyky,
        // abych byly zobrazeny texty ve správném jazyce - komentare v dialogu po CTRL + SPACE
        // Jinak by se texty nenačetly, resp. načetly, ale už by se změny neprojevily v dialogu s doplňováním:
        codeEditor.setAutoCompletion(clazz);


        pack();
        setLocationRelativeTo(null);

        /*
         * Kvůli metodě pack je třeba nastavit správnou pozici posuvníku až zde, ale
         * nechávám pouze výše nastavení příslušné výšky, aby se při změně velikosti
         * okna tenposuvník nehýbal:
         */
        //		jspEditors.setDividerLocation(0.9d);
    }


    /**
     * Konstruktor této třídy, který se zavolá, po kliknutí na příslušnou položku v hlavním menu aplikace ("Otevřít
     * třídu" - v češtině)
     * <p>
     * Tento konstuktor umožňuje uživateli aplikace otevřít libovolnou Javovskou třídu (né pouze tu v projektu aplikace)
     * dále je to proto, že tato třída nepůjde zkompilovat, protože je třeba zadat příslušnou cestu k dané třídě včetně
     * "rozdělení" cesty na balíčky, kvůli načtení souboru .class pro získání položek na getry, setry a kompilaci, kvůli
     * tomu že tuto cestu nejsem schopný rozdělit jako tu z aplikace, nepůjde s takto otevřenou třídou provádět zmíněné
     * operace, takže musím v menu "znepřístupnít - zablokovat" příslušná tlačítka
     *
     * @param pathToClass
     *         - absolutní cesta ke konkrétní třídě, kterou chce uživatel otevřít, nemusí se jednat o adresář projěktu,
     *         ale může být načtena "odkudkoliv" - plocha, disk, internet, ...
     */
    public CodeEditorDialog(final String pathToClass, final Properties languageProperties) {
        // následující proměnnou není třeba inicializovat:
        classNameWithDot = null;
        pathToClassAll = pathToClass;

        // Jako titulek dialogu nastavím cestu k třídě, jejíž zdrojový kód je načten:
        setTitle(pathToClassAll);

        // Načtení konfiguračního souboru pro nastavení:
        codeEditorProp = App.READ_FILE.getCodeEditorProperties();

        // Připravení Gui:
        initGui();


        sharedClass = new SharedClassForMethods(this);


        // Nastavím menu do okna aplikace:
        menu = new JMenuForCodeEditor(this, sharedClass, languageProperties);
        setJMenuBar(menu);

        // Zablokuji některá tlačítka:
        menu.blockEntries();








        /*
         * Zjistím si příponu souboru:
         */
        final String extension = pathToClass.substring(pathToClass.lastIndexOf('.'));

        // Vytvořit editor:
        codeEditor = new CodeEditor(this, extension, false, codeEditorProp);
        final RTextScrollPane rspCodeEditor = new RTextScrollPane(codeEditor);

        // Povolení akcí pro editor kódu. Jedná se o akce, které byly zakázány pro editor příkazů.
        RSyntaxTextAreaKeyActionHelper.enableSpecificAction(true);


        // Přidám toolbar s tlačítky do dialogu hned pod menu - jako lišta s
        // "nejvíce používanými tlačítky"
        toolBar = new JToolBarForCodeEditor(sharedClass, codeEditor);
        // Nastavení proměnné pro zalamování textuů:
        loadCodeEditorSettings(codeEditorProp);

        add(toolBar, BorderLayout.NORTH);

        //V toolbaru je druhé tlačítko pro kompilace - tlačítko pro "rychly přístup", to je třeba také zněpřístupnit:
        toolBar.setEnabledButtonCompile();


        // Editor výstupů není v tomto případě potřeba
        outputEditor = null;
        add(rspCodeEditor, BorderLayout.CENTER);


        // Zde se již vytvořily potřebné komponenty pro editraci kódu, tak nyní mohu načíst kód
        // z dané třídy:

        // Note: Zde již nemusím testovat,zda třída existuje nebo ne, to jsem již udělal před otevřením dialogu v
        // menu v diagramu tříd
        final List<String> textOfOriginalClass = new ReadWriteFile().getTextOfFile(pathToClassAll);

        // Nastavím text do editoru:
        codeEditor.setTextToCodeEditor(textOfOriginalClass);


        saveCodeAfterCloseDialog = new SaveCodeAfterCloseDialog(this, null, null);
        addWindowListener(saveCodeAfterCloseDialog);


        pack();
        setLocationRelativeTo(null);


        /*
         * Kvůli metodě pack je třeba nastavit správnou pozici posuvníku až zde, ale
         * nechávám pouze výše nastavení příslušné výšky, aby se při změně velikosti
         * okna tenposuvník nehýbal:
         */
        //		jspEditors.setDividerLocation(0.9d);
    }


    /**
     * Metoda, která nastaví "výchozí design" okna dialogu - jako je například ikona, velikost okna, layout, ... více
     * viz metoda
     */
    private void initGui() {
        setLayout(new BorderLayout());
        setIconImage(GetIconInterface.getMyIcon("/icons/app/CodeEditorIcon.png", true).getImage());
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setPreferredSize(new Dimension(800, 650));
        setModal(true);
    }


    /**
     * @return vrátí cestu ke třídě, jejíž kód je aktuálně načten v editoru kódu - v tomto dialogu
     */
    final String getPathToClass() {
        return pathToClassAll;
    }


    /**
     * Metoda, která vrátí název třídy s balíčky (pokud se třída v nějakém nachází), jejíž kód je načten v editoru
     * zdrojového kódu
     *
     * @return vrátí název výše zmíněné třídy
     */
    final String getClassNameWithDot() {
        return classNameWithDot;
    }


    /**
     * Metoda, která vrátí referenci na instanci OutpuEditoru v tomto dialogu
     *
     * @return referenci na output editor v tomto dialogu
     */
    public final OutputEditorForCompilation getOutputEditor() {
        return outputEditor;
    }


    /**
     * @return reference na editor zdrojového kódu
     */
    final CodeEditor getCodeEditor() {
        return codeEditor;
    }


    /**
     * Načtení vybraných vlastností pro editor kódu. Například, zda se mají zalalamovat texty v editoru, zobrazovat bílé
     * znaky apod. Viz tělo metody.
     *
     * @param codeEditorProp
     *         - načtený soubor .properties obsahující nastavení pro editor kódu.
     */
    private void loadCodeEditorSettings(final Properties codeEditorProp) {
        final boolean wrapText;
        final boolean showWhiteSpace;
        final boolean clearWhiteSpace;

        if (codeEditorProp != null) {
            wrapText = Boolean.parseBoolean(codeEditorProp.getProperty("WrapTextInEditor",
                    String.valueOf(Constants.CODE_EDITOR_WRAP_TEXT_IN_EDITOR)));
            showWhiteSpace = Boolean.parseBoolean(codeEditorProp.getProperty("ShowWhiteSpaceInEditor",
                    String.valueOf(Constants.CODE_EDITOR_SHOW_WHITE_SPACE_IN_EDITOR)));
            clearWhiteSpace = Boolean.parseBoolean(codeEditorProp.getProperty("ClearWhiteSpaceLineInEditor",
                    String.valueOf(Constants.CODE_EDITOR_CLEAR_WHITE_SPACE_LINE_IN_EDITOR)));
        }

        else {
            wrapText = Constants.CODE_EDITOR_WRAP_TEXT_IN_EDITOR;
            showWhiteSpace = Constants.CODE_EDITOR_SHOW_WHITE_SPACE_IN_EDITOR;
            clearWhiteSpace = Constants.CODE_EDITOR_CLEAR_WHITE_SPACE_LINE_IN_EDITOR;
        }


        // Nastavení označené komponenty:
        toolBar.setSelectedRbWrapText(wrapText);
        // Nastavení zalamování textu v editoru:
        codeEditor.wrapTextInEditor(wrapText);

        toolBar.setSelectedRbShowWhiteSpace(showWhiteSpace);
        codeEditor.showWhiteSpace(showWhiteSpace);

        toolBar.setSelectedRbClearWhiteSpaceLine(clearWhiteSpace);
        codeEditor.setClearWhiteSpaceLine(clearWhiteSpace);
    }


    /**
     * Uložení (vybraných) vlastností editoru kódu. Například, zda se mají zalamovat texty, zobrazovat bílé znaky apod.
     * <p>
     * <i>Princip je takový, že se načte konfigurační soubor z adresáře workspace tak, aby se zachovaly i komentáře,
     * když se soubor nenačte, pak se nic neděje, nic se neuloží, skončí se. Zde se nebudou řešit chybějící soubory
     * apod. To mají na starosti jiné komponenty. Pokud se soubor načte nastaví se v něm příslušné vlastnosti a změny se
     * uloží zpět do souboru.</i>
     */
    void saveCodeEditorSettings() {
        final EditProperties codeEditorProperties = App.READ_FILE
                .getPropertiesInWorkspacePersistComments(Constants.CODE_EDITOR_NAME);

        if (codeEditorProperties == null)
            return;

        codeEditorProperties.setProperty("WrapTextInEditor", String.valueOf(toolBar.isRbWrapTextSelected()));
        codeEditorProperties.setProperty("ShowWhiteSpaceInEditor",
                String.valueOf(toolBar.isRbShowWhiteSpaceSelected()));
        codeEditorProperties.setProperty("ClearWhiteSpaceLineInEditor",
                String.valueOf(toolBar.isRbClearWhiteSpaceLineSelected()));

        codeEditorProperties.save();
    }


    @Override
    public void setLanguage(final Properties properties) {
        // Texty konkrétně do této třídy nejsou potřeba, ale do ostatních částí tohoto
        // editoru zdrojového kódu ano, tak poze predám referenci:
        toolBar.setLanguage(properties);

        saveCodeAfterCloseDialog.setLanguage(properties);

        codeEditor.setLanguage(properties);
    }
}