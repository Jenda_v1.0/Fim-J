package cz.uhk.fim.fimj.code_editor;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.List;
import java.util.Properties;

import javax.swing.JOptionPane;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.class_diagram.CheckRelationShipsBetweenClassesThread;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.code_editor_abstract.RSyntaxTextAreaKeyActionHelper;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.OutputEditorInterface;
import cz.uhk.fim.fimj.instances.Instances;
import cz.uhk.fim.fimj.project_explorer.ReadWriteFile;
import cz.uhk.fim.fimj.project_explorer.ReadWriteFileInterface;
import cz.uhk.fim.fimj.swing_worker.ThreadSwingWorker;

/**
 * Tato třída slouží "posluchač" editoru pro editaci zdrojového kódu zvolené třídy z diagramu tříd, po zavření tohoto
 * dialogu se otestuje, zda byl kód změněn, a pokud ano, tak se před zavřením dialogu zepta uživatele, zda chce změněný
 * kód uložit či nikoliv, pokud ano, uloží es změny, pokud ne, dialog pro editaci zdrojového kódu se prostě zavře a
 * "opět se zpřístupní" aplikace pro další využití
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class SaveCodeAfterCloseDialog extends WindowAdapter implements LanguageInterface {

    private final CodeEditorDialog codeEditorDialog;


    /**
     * Proměnná na uchování reference na diagram tříd, abych ji mohl předat vláknu:
     */
    private final GraphClass classDiagram;


    // Proměnné pro texty do chybových lášek:
    private String txtSaveChangesTitle;
    private String txtSaveChangesText;
    private String txtClassNotFoundTitle;
    private String txtClassNotFoundText;
    private String txtPath;
    private String txtFailedToWriteCodeToClassText;
    private String txtFailedToWriteCodeToClassTitle;
    private String txtFailedToLoadClassText;
    private String txtFailedToLoadClassTitle;


    /**
     * Reference na editor výstupů, která se předá do vlákna a pro kompilaci, aby se mohli vypsat hlášky o kompilaci,
     * apod. případně dále pro aktualizaci instancí:
     */
    private final OutputEditorInterface outputEditor;


    /**
     * Proměnná pro uložení textů pro aplikaci ve zvoleném jazyce. Je zde jako globální neboli intančí proměnná puze pro
     * to, aby jej bylo možé předat do metody pro přenačtení diagramu instancí.
     */
    private Properties languageProperties;


    /**
     * Konstruktor této třídy.
     *
     * @param codeEditorDialog
     *         - reference na dialog, ne jehož zavření má reagovat "tato" událost - metody windowClosed.
     * @param classDiagram
     *         - reference na diagram tříd.
     * @param outputEditor
     *         - referencena editor výstupů, tj. kam se budou vypisovat hlášení ohledně kompilace tříd apod.
     */
    SaveCodeAfterCloseDialog(final CodeEditorDialog codeEditorDialog, final GraphClass classDiagram,
                             final OutputEditorInterface outputEditor) {

        this.codeEditorDialog = codeEditorDialog;
        this.classDiagram = classDiagram;
        this.outputEditor = outputEditor;
    }


    @Override
    public void windowClosed(final WindowEvent e) {
        // Získám si zdrojový kód z editoru kódu:
        final List<String> codeFromCodeEditor = codeEditorDialog.getCodeEditor().getTextFromCodeEditor();

        // Porovnám kolekce se zdrojovými kódy z původní třídy a aktuální z editoru

        // Zde si musím načíst kó originální třídy znovu, protože mohl být změnen v jiném editoru:
        final File fileOriginalClass = new File(codeEditorDialog.getPathToClass());


        if (fileOriginalClass.exists()) {
            final ReadWriteFileInterface editCodeOfClass = new ReadWriteFile();

            final List<String> codeOfOriginalClass = editCodeOfClass.getTextOfFile(codeEditorDialog.getPathToClass());

            final boolean areCodeIdentical = codeEditorDialog.getCodeEditor().compareSourceCode(codeOfOriginalClass);

            if (!areCodeIdentical) {
                // Zde se kód změnil, tak se zeptám uživatele, zda chce změny uložit:
                final int result = JOptionPane.showConfirmDialog(null, txtSaveChangesText, txtSaveChangesTitle,
                        JOptionPane.YES_NO_OPTION);


                if (result == JOptionPane.YES_OPTION) {
                    // Zde uživatel klikl na Ano - uložit změny


                    // Zde otestuji, zda daná třída ještě existuje - během doby, co aplikace "stagnovala" při zobrazení
                    // okna s potvrzením, zda es má kód uložít uživatel mohl smazat daný soubor, tak
                    // pro jistotu znovu otestuji jeho existenci, abych předešel vyjímce:
                    if (fileOriginalClass.exists()) {

                        // Zde zadaná třída ještě existuje, tak mohu zapsat zdrojový kíod zpět do dané třídy:
                        final boolean isWritten = editCodeOfClass.setTextToFile(codeEditorDialog.getPathToClass(),
                                codeFromCodeEditor);


                        // Otestuji, zda se podařilo zapsat zdrojový kód zpět do třídy:
                        if (!isWritten)
                            // Zde se nepodařilo zapsat v dialogu upravený zdrojoývý kód zpět do třídy, oznámím to
                            // uživateli:
                            JOptionPane.showMessageDialog(null,
                                    txtFailedToWriteCodeToClassText + "\n" + txtPath + ": "
                                            + codeEditorDialog.getPathToClass(),
                                    txtFailedToWriteCodeToClassTitle, JOptionPane.ERROR_MESSAGE);
                    }
                    else {
                        JOptionPane.showMessageDialog(null,
                                txtClassNotFoundText + "\n" + txtPath + ": " + codeEditorDialog.getPathToClass(),
                                txtClassNotFoundTitle, JOptionPane.ERROR_MESSAGE);
                    }
                }
                // else - zde uživatel nechce změny uložit, tak není třeba dělat nic, prostě se zavře dialog
            }
            // else - jsou zdrojové kódy stejné, není důvod se uživatele ptát na uložení - nedošlo ke změně, tak
            // dialog prostě zavřu
        }
        else {
            JOptionPane.showMessageDialog(null, txtFailedToLoadClassText, txtFailedToLoadClassTitle, JOptionPane
                    .ERROR_MESSAGE);
        }


        // Zakázání akcí, aby je nebylo možné využít pro editor příkazů:
        RSyntaxTextAreaKeyActionHelper.enableSpecificAction(false);

        // Uloží se vlastnost, zda se mají zalamovat texty:
        codeEditorDialog.saveCodeEditorSettings();


        /*
         *  Pokud v editoru kodu bylo otevřeného něco jiného než Javovská třída z diagramu tříd, tj. byl otevřen
         *  nějaký jiný soubor pomocí hlavního menu v hlavním okně aplikce pomocí tlačítka otevřit v editoru v menu
         *  Nástroje, tak bude classDiagram null, protože zde nebyl třeba, tak ani nemusím volat vlákno pro
         *  otestování vztahů mezi instancemi.
         */
        if (classDiagram != null) {
            /*
             * na konec se po zavření dialogu spustí vlákno, ktere otestuje potenciální doplněné či smazané nebo
             * zeditované vztahy mezi třídami.
             *
             * Například se doplní dědičnost, ... více viz vlákno: poslední parametr = false = chci otstovat jenom
             * právě označenou třídu, ne všechny
             */
            ThreadSwingWorker.runMyThread(new CheckRelationShipsBetweenClassesThread(classDiagram, outputEditor,
                    classDiagram.isShowRelationShipsToItself(), classDiagram.isShowAssociationThroughAggregation()));


            /*
             * Dále je na nejvíš vhodné přenačíst i samotný diagram instancí, protože
             * uživatel mohl například v otevřené třídě odebrat nějakou proměnnou, která je
             * aktuálně v nějaké instanci naplněna a zobzrauje se tak pomocí příslušného
             * vztahu mezi instancemi, ale když tuto proměnnou odebral, tak už by v diagramu
             * instancí být neměla, takže jej musím znovu přenačíst, aby se potenciální
             * změny projevily i v diagramu instancí mezi intancemi.
             */
            final String pathToBinDir = App.READ_FILE.getPathToBin();

            if (pathToBinDir != null)
                Instances.refreshClassInstances(pathToBinDir, outputEditor, languageProperties, true);
        }
    }


    @Override
    public void setLanguage(final Properties properties) {
        languageProperties = properties;

        if (properties != null) {
            txtSaveChangesTitle = properties.getProperty("Scacd_Jop_txtSaveChangesTitle",
                    Constants.SCACD_SAVE_CHANGES_TITLE);
            txtSaveChangesText = properties.getProperty("Scacd_Jop_TxtSaveChangesText",
                    Constants.SCACD_SAVE_CHANGES_TEXT);
            txtClassNotFoundTitle = properties.getProperty("Scacd_Jop_TxtClassNotFoundTitle",
                    Constants.SCACD_CLASS_NOT_FOUND_TITLE);
            txtClassNotFoundText = properties.getProperty("Scacd_Jop_TxtClassNotFoundText",
                    Constants.SCACD_CLASS_NOT_FOUND_TEXT);
            txtPath = properties.getProperty("Scacd_Jop_TxtPath", Constants.SCACD_PATH);
            txtFailedToWriteCodeToClassText = properties.getProperty("Scacd_Jop_TxtFailedToWriteCodeToClassText",
                    Constants.SCACD_FAILED_TO_WRITE_CODE_TO_CLASS_TEXT);
            txtFailedToWriteCodeToClassTitle = properties.getProperty("Scacd_Jop_TxtFailedToWriteCodeToClassTitle",
                    Constants.SCACD_FAILED_TO_WRITE_CODE_TO_CLASS_TITLE);
            txtFailedToLoadClassTitle = properties.getProperty("Scacd_Jop_TxtFailedToLoadClassTitle", Constants.SCAD_FAILED_TO_LOAD_CLASS_TITLE);
            txtFailedToLoadClassText = properties.getProperty("Scacd_Jop_TxtFailedToLoadClassText", Constants.SCAD_FAILED_TO_LOAD_CLASS_TEXT);
        } else {
            txtSaveChangesTitle = Constants.SCACD_SAVE_CHANGES_TITLE;
            txtSaveChangesText = Constants.SCACD_SAVE_CHANGES_TEXT;
            txtClassNotFoundTitle = Constants.SCACD_CLASS_NOT_FOUND_TITLE;
            txtClassNotFoundText = Constants.SCACD_CLASS_NOT_FOUND_TEXT;
            txtPath = Constants.SCACD_PATH;
            txtFailedToWriteCodeToClassText = Constants.SCACD_FAILED_TO_WRITE_CODE_TO_CLASS_TEXT;
            txtFailedToWriteCodeToClassTitle = Constants.SCACD_FAILED_TO_WRITE_CODE_TO_CLASS_TITLE;
            txtFailedToLoadClassTitle = Constants.SCAD_FAILED_TO_LOAD_CLASS_TITLE;
            txtFailedToLoadClassText = Constants.SCAD_FAILED_TO_LOAD_CLASS_TEXT;
        }
    }
}