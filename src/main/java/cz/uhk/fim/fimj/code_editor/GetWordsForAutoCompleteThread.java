package cz.uhk.fim.fimj.code_editor;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.project_explorer.ReadWriteFile;

/**
 * Tato třída slouží jako vlákno, které se zavolá pokaždé, když uživatel v editoru kódu stiskne středník nebo uzavírací
 * hranatou nebo špičatou závorku (funguje pouze na anglické klávesnici)
 * <p>
 * Po stisknutí zmíněného středníku nebo uzavírací hranaté či špičaté závorky se zavoláí toto vlákno a to zkusí
 * zkompilovat danou třídu, pokud se to opvede, vygeneruje se nový soubor - přeložený coby zkompilovaná třída s příponou
 * .class, tak toto vlákno zkusí na pozadí zkompilovat danou třídu a načíst její soubor .class pokud se třídu nepodaří
 * zkompilovat, tak je možné že již existuje její zkompilovaný soubor .class, který se vytvořil při prvním otevření,k
 * pokud ne, tak nápovědy fungovat nebudou pro položky v dané třídě.
 * <p>
 * Pokud by se vláknu podařilo zkompilovat a načíst přeloženou - zkompilovanou třídu, tak se předá do metody
 * setAutoCompletion v dané instanci editoru zdrojového kódu a "doplní" se nápověda s doplňováním.
 * <p>
 * Pouze v případě, že se danou třídu podaří úspěšně zkommpilovat a načíst přeložený soubor, jinak by se načetlo null, a
 * v takovém případě se nic dělat nebude
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class GetWordsForAutoCompleteThread extends Thread {
	
	/**
	 * reference na dialog, ve kterém je umístěn mimo jiné i editor zdrojového kodu,
	 * reference na nej zde potrebuji kvuli cestam na tridu, ziskani textu , ... viz
	 * metoda run
	 */
	private final CodeEditorDialog codeEditorDialog;


	/**
	 * Konstruktor tohoto vlákna
	 *
	 * @param codeEditorDialog
	 *         - reference na dialog, ve kterém je umístěn editor zdrojového kódu
	 */
	public GetWordsForAutoCompleteThread(final CodeEditorDialog codeEditorDialog) {
		super();

		this.codeEditorDialog = codeEditorDialog;
	}
	
	
	
	
	
	@Override
	public void run() {
		// Úplně na začátku musím danou třídu uložít, toto vlíkno se zvolá v případě, že uživatel stiskl středník
		// nebo zavírací špičatou nebo hranatou závorku, takže se text musel změnit tudíž nemá smysl porovnávat změny
		// zdrojových kódu a už vůbec ne ptát se uživatele na uložení - musím ho uložit a pak zkompilovat:
		
		
		// Získám si reference na editor zdrojového kódu, abych z něj mohl později získat 
		// nově upravený kod uzivatelem
		final CodeEditor codeEditor = codeEditorDialog.getCodeEditor();
		
		
		// testa ke třídě, kam se má uložit kod, resp. třída, jejíz kod je nacten v editoru zdrojového kodu
		final String pathToClass = codeEditorDialog.getPathToClass();
		
		// Zkusím zapsat nove upraveny kod uzivatelem do tridy, abych ji mohl zkompilovat:
		final boolean isWritten = new ReadWriteFile().setTextToFile(pathToClass, codeEditor.getTextFromCodeEditor());
		
		
		
		// Otestuji, zda se podarilo kod zapssat:
		if (isWritten) {
			/*
			 * vezmu si text z bunky, kvůli ClassLoaderu - aby vedel balicky, kde ma tridu
			 * hledat v adresari bin.
			 */
			final String cellText = codeEditorDialog.getClassNameWithDot();
			
			
			// Pokusím se zkompilovat načtenou třídu v editoru zdrojového kódu,
			// abych mohl doplnit novou proměnnou, metodu, ... prostě nově přidanou položku

			final Class<?> clazz = App.READ_FILE.getClassFromFile(cellText, null, GraphClass.getCellsList(), false,
					false);
			
			
			// Otestuji, zda byla načtena - zkomilována bez chyby, pokud ano, tak aktualizuji
			// okno s položkami třídy, pokud ne, tak třída obsahuje chybu, nelze zkompilovat nebo někde mohlo
			// dojít k chybě, každopdně v takovém případě nic dělat nebudu, nechám tam původní nápovědu uživateli
			
			if (clazz != null)
				codeEditor.setAutoCompletion(clazz);
		}
	}
}