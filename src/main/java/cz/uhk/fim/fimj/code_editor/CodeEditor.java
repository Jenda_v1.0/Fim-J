package cz.uhk.fim.fimj.code_editor;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Properties;

import cz.uhk.fim.fimj.code_editor_abstract.CodeEditorAbstract;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;

/**
 * Tato třída slouží jako Komponenta pro editaci zdrojového kódu zvolené třídy. Komponenta je vložena do dialogu pro
 * editaci kódu (CodeEditorDialog.java)
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class CodeEditor extends CodeEditorAbstract implements LanguageInterface {

    private static final long serialVersionUID = 1620013904054594914L;


    /**
     * Vlákno, které slouží pro zkompilování tříd a jejich znovunačtení, aby se do tohoto editoru znovu načetli veškeré
     * potřebné hodnoty - proměnné, metody, konstruktory apod.
     */
    private static GetWordsForAutoCompleteThread refreshAutoCompletionThread;


    /**
     * V konstruktoru se pouze předává název třídy i s balíčky od adresáře src až po ten, ve kterém je daná třída
     * umístěna. Je to takto potřeba proto, že tento název třídy potřebuji dále předat vláknu, abych mohl zkompilovt
     * danou třídu
     *
     * @param codeEditorDialog
     *         - reference na dialog, ve kterém se tento editor kódu nachází, je zde potřeba kvůli předání do vlákna,
     *         aby si mohl zjistit všechny věci o třídě, kod, ... - viz dano vlakno GetWordsForAutoCompleteThread
     * @param extensionOfOpenFile
     *         - textová proměnná, která obsahuje příponu souboru v syntaxi: '.extension'
     * @param openClassInCd
     *         - logická proměnná, která značí, zda je otevřenanějaká třída z diagramu tříd nebo ne. True - je otevřena
     *         nějaká třída z diagramu tříd, false - je otevřen v podstatě libovolný soubor mimo diagram tříd.
     * @param codeEditorProp
     *         - Objekt Properties obsahující načtený konfigurační soubor s hodnotami pro nastavení například barvy
     *         pozadí, textu apod.
     */
    public CodeEditor(final CodeEditorDialog codeEditorDialog, final String extensionOfOpenFile,
                      final boolean openClassInCd, final Properties codeEditorProp) {

        super(extensionOfOpenFile);


        // Nastavení typ jazyka pro zvýrazňování kódu a povolení zvýrazňování:
        setCodeFoldingEnabled(true);
        setAntiAliasingEnabled(true);


        addMyMouseWheelListener();


        // Při otevření editru si ze souboru přešte nastavení:
        setSettingEditor(codeEditorProp, openClassInCd);


        // Otestuji, zda se má přidat posluchac na rekci stredniku a zaviraci spicate a hranate zavorky na anglicke
        // klavesnici
        // aby se zavolalo vlakno pro zkompilovani a nacteni hodnot
        if (compiledClassInBackground)
            addKeyListener(new KeyAdapter() {

                @Override
                public void keyReleased(final KeyEvent e) {
                    /*
                     * Po stisknutí středníku nebo zavírací závorky se otestuje zda vlákno ještě neběží, popřípadě
                     * neexistuje, kdyby uživatel mačkal pořád do kola středník, pak se mohlo dojít k chybě, že by se
                     * například opakovaně načíst nějaký soubor apod.
                     */
                    if ((e.getKeyCode() == KeyEvent.VK_SEMICOLON || e.getKeyCode() == KeyEvent.VK_CLOSE_BRACKET) && (refreshAutoCompletionThread == null || !refreshAutoCompletionThread.isAlive())) {
                        // Zde byla stisknuta strednik nebo ] nebo } tak
                        // vytvorim a spustim vlakno pro nacteni hodnot:
                        refreshAutoCompletionThread = new GetWordsForAutoCompleteThread(codeEditorDialog);
                        refreshAutoCompletionThread.start();
                    }
                }
            });
    }


    /**
     * Metoda, která si přečte příslušný soubor, pokud existuje pokud soubor neexistuje, použije se výchozí nastavení
     * <p>
     * ze souboru CodeEditor.properties si přečte výchozí nastavení pro tento editor
     * <p>
     * Metoda e zavolá, akorát při vytvoření instance této třídy - editoru - neboli když uživatel otevře zdrojový kód
     * třídy z diagramu tříd
     * <p>
     * Note: Tato metoda je v podstatě stejná, jako setSettingEditor ve třídě CodeEditor v dialog průzkumníku projektů,
     * mohla by tedy být v předkovi a jen ji volat s parametrem proeperties nastaveným pro konkrétná editor, ale původně
     * jsem si zde zkoušel nějaké jiné věci, které jsem pak zamítnul, původně tedy nebyla stejná, tak kdybych se v
     * budoucnu rozhodl, že nebudou stejné, alespoň bych je nemusel přepisovat, proto jsem je nedával do předka, ale
     * jsou v těch třídách dvakrát.
     *
     * @param codeEditorProp
     *         - proměná typu Properties, která nese hodnoty pro nastavení editoru, resp. hodnoty s nastavením třeba
     *         barvy písma, pozadí, ... více v metodě v jednotlých nastaveních
     * @param isOpenClassFromCd
     *         - logická proměnná o tom, zda se má nastavit logická proměnná compiledClassInBackground, promenna
     *         isOpenClass značí akorát to, zda se má nastavit logická promenná compiledClassInBackground, která určuje,
     *         zda se mají na pozadí kompilovat třídy po stisknutí stredniku nebo zavorek, pokud není otevřena trída,
     *         tak bude proměnná IsIpenClass false a proměnna compiledClassInBackground se nastaví také na false,
     *         protože když není ani otevřena třída nebo projekt, tak nelze nic kompilovat
     */
    private void setSettingEditor(final Properties codeEditorProp, final boolean isOpenClassFromCd) {
        if (codeEditorProp != null) {
            if (isOpenClassFromCd)
                // navstím promennou compiledClassInBackground bud na nastavenou hodnotu (pokud
                // se ji podari ziskat) nebo na
                // výchozí hodnotu zadanou v Constants,
                compiledClassInBackground = Boolean.parseBoolean(codeEditorProp.getProperty("CompileClassInBackground",
                        String.valueOf(Constants.CODE_EDITOR_COMPILE_CLASS_IN_BACKGROUND)));

            else
                compiledClassInBackground = false;


            // Zde se padařilo načíst soubor bud s výchozím nastavením nebo ve workspace,
            // Tak nastavím hodnoty do editoru v app:
            final int fontStyle = Integer.parseInt(codeEditorProp.getProperty("FontStyle",
                    Integer.toString(Constants.DEFAULT_FONT_FOR_CODE_EDITOR.getStyle())));
            final int fontSize = Integer.parseInt(codeEditorProp.getProperty("FontSize",
                    Integer.toString(Constants.DEFAULT_FONT_FOR_CODE_EDITOR.getSize())));
            setFont(new Font(Constants.DEFAULT_FONT_FOR_CODE_EDITOR.getName(), fontStyle, fontSize));


            final String bgColor = codeEditorProp.getProperty("BackgroundColor",
                    Integer.toString(Constants.CODE_EDITOR_BACKGROUND_COLOR.getRGB()));
            setBackground(new Color(Integer.parseInt(bgColor)));


            final String foregroundColor = codeEditorProp.getProperty("ForegroundColor",
                    Integer.toString(Constants.CODE_EDITOR_FOREGROUND_COLOR.getRGB()));
            setForeground(new Color(Integer.parseInt(foregroundColor)));


            // Zde se zjistí, zda se má využít některý syntaxe dle přípon souborů:
            final boolean highlightCodeByKindOfFile = Boolean.parseBoolean(codeEditorProp.getProperty(
                    "HighlightingCodeByKindOfFile",
                    String.valueOf(Constants.CODE_EDITOR_HIGHLIGHTING_CODE_BY_KIND_OF_FILE)));

            /*
             * Zde otestuji, zda se má nastavit zvýrazňování dle přípon souborů, pokud ano,
             * tak jej nastavím a pokud ne, pak nastavím výchozí styl zvýrazňování - Java.
             */
            if (highlightCodeByKindOfFile)
                setHighlightingCodeByKindOfFile(extensionOfOpenFile);
            else
                setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);


            // Zda se vůbec má zvýrazňovat nějaká syntaxe - ano nebo ne, ať už je nastaveno hlídání typů souborů nebo
            // ne:
            final boolean highlightingCode = Boolean.parseBoolean(codeEditorProp.getProperty("HighlightingCode",
                    String.valueOf(Constants.CODE_EDITOR_HIGHLIGHTING_CODE)));

            // Pokud se nemá zvýrazňovat syntaxe, pak jej vypnu:
            if (!highlightingCode)
                // Zde se nebude zvýrazňovat kód - text otevřeného souboru:
                setSyntaxEditingStyle(null);
        }

        else {
            // Zde použiji výchozí nastavení:
            compiledClassInBackground = Constants.CODE_EDITOR_COMPILE_CLASS_IN_BACKGROUND;

            setFont(Constants.DEFAULT_FONT_FOR_CODE_EDITOR);

            setBackground(Constants.CODE_EDITOR_BACKGROUND_COLOR);

            setForeground(Constants.CODE_EDITOR_FOREGROUND_COLOR);


            if (Constants.CODE_EDITOR_HIGHLIGHTING_CODE_BY_KIND_OF_FILE)
                setHighlightingCodeByKindOfFile(extensionOfOpenFile);
            else
                setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);


            if (!Constants.CODE_EDITOR_HIGHLIGHTING_CODE)
                setSyntaxEditingStyle(null);
        }

        // Vytvoření objektu pro formátování zdrojového kódu:
        createFormatter(codeEditorProp);
    }


    @Override
    public void setLanguage(final Properties properties) {
        setFulfillmentVarForCodeComment(properties);
    }
}