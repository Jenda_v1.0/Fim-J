package cz.uhk.fim.fimj.code_editor.generate_access_methods;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JCheckBox;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;

/**
 * Třída slouží pro "překreslování JCheckBoxů ve stromu v dialogu" když se klikne na nějakou položku, zaznamená to
 * metoda: getTreeCellRendererComponent která si zjistí data z dané položky ve stromu a vrátí nový JcheckBox, dle
 * kterého si zjistím zda uživatel označil nebo odznačil zmíněný JCheckBox ve stromu v dialogu
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class CheckBoxNodeRenderer implements TreeCellRenderer {
	
	private JCheckBox leafRenderer = new JCheckBox();

	private DefaultTreeCellRenderer nonLeafRenderer = new DefaultTreeCellRenderer();

	private Color selectionForeground, selectionBackground, textForeground, textBackground;
     
	JCheckBox getLeafRenderer() {
		return leafRenderer;
	}
     
     

	public CheckBoxNodeRenderer() {
		final Font fontValue = UIManager.getFont("Tree.font");

		if (fontValue != null)
			leafRenderer.setFont(fontValue);

		final Boolean booleanValue = (Boolean) UIManager.get("Tree.drawsFocusBorderAroundIcon");

		leafRenderer.setFocusPainted((booleanValue != null) && (booleanValue));

		selectionForeground = UIManager.getColor("Tree.selectionForeground");
		selectionBackground = UIManager.getColor("Tree.selectionBackground");
		textForeground = UIManager.getColor("Tree.textForeground");
		textBackground = UIManager.getColor("Tree.textBackground");
	}
     
	
	
	
     
	public final Component getTreeCellRendererComponent(final JTree tree, final Object value, final boolean selected,
			final boolean expanded, final boolean leaf, final int row, final boolean hasFocus) {

		final Component returnValue;
		if (leaf) {

			final String stringValue = tree.convertValueToText(value, selected, expanded, leaf, row, false);

			leafRenderer.setText(stringValue);
			leafRenderer.setSelected(false);

			leafRenderer.setEnabled(tree.isEnabled());

			if (selected) {
				leafRenderer.setForeground(selectionForeground);
				leafRenderer.setBackground(selectionBackground);
			}

			else {
				leafRenderer.setForeground(textForeground);
				leafRenderer.setBackground(textBackground);
			}

			// Nastaví se zaškrtnutí / odškrtnutí JCheckboxu:
			if (value instanceof DefaultMutableTreeNode) {
				final Object userObject = ((DefaultMutableTreeNode) value).getUserObject();

				if (userObject instanceof CheckBoxNode) {
					final CheckBoxNode node = (CheckBoxNode) userObject;

					leafRenderer.setText(node.getText());
					leafRenderer.setSelected(node.isSelected());
				}
			}

			returnValue = leafRenderer;
		}

		else
			returnValue = nonLeafRenderer.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row,
					hasFocus);

		return returnValue;
	}
}