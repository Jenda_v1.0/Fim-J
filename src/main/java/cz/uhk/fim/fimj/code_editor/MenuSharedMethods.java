package cz.uhk.fim.fimj.code_editor;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;

import com.github.abrarsyed.jastyle.ASFormatter;
import com.github.abrarsyed.jastyle.FormatterHelper;
import com.github.abrarsyed.jastyle.constants.EnumFormatStyle;

import cz.uhk.fim.fimj.code_editor.generate_access_methods.GenerateAccessMethodForm;
import cz.uhk.fim.fimj.code_editor_abstract.CodeEditorAbstract;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.forms.GoToLineForm;
import cz.uhk.fim.fimj.forms.ReplaceTextInCodeEditorForm;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.project_explorer.ReadWriteFile;
import cz.uhk.fim.fimj.project_explorer.ReadWriteFileInterface;

/**
 * Tato třída implementuje rozhraní MenuInterface, které obsahuje signratury metod, které je třeba zde implementovat,
 * protože se jedná o sdílené metody pro oba editoru kódu, jak ten "klasický" editor kodu, tak i ten v průzkumníkovi
 * projektů - v interních okne.
 * <p>
 * Tato třída obsahuje sdílené metody pro obě menu, ve zmíněných editorech a obsahuje metody, které jsou spolešné,
 * například pro odsazení textu, zakomentování, odkomentování, zformátování,...
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class MenuSharedMethods implements MenuInterface, LanguageInterface {

    /**
     * Tato "kompnenta" obsahuje metody pro čtení a zapisování do souborů, tyto konkrétně jsou používány pro zápis a
     * nebo čtení Javovských tříd kvůli formátování jejich zdrojového kódu
     */
    private static final ReadWriteFileInterface editCode = new ReadWriteFile();


    /**
     * Regulární výrazpro rozpoznání, zda se na začátku textu nachízí nějaká bílý znak pro odsazení (mezerea, tabulátor,
     * apod.)
     */
    private static final String REG_EX_INDENT_AT_START_LINE = "^\\s+.*$";


    /**
     * Regulární výraz, který zjistí, zda se na začátku textu nachází tabulátor:
     */
    private static final String REG_EX_TABULATOR_AT_START_LINE = "^\t.*$";


    // Proměnné pro texty například vkládaným textem do komentářeů apod.:
    private String txtCommentOnTheMethod;
    private String txtParameterDescription;
    private String txtReturnValue;
    private String txtPutYouCodeHere;
    private String txtReturnStringValue;
    private String txtErrorWhileSavingChangesToClassText_1;
    private String txtErrorWhileSavingChangesToClassText_2;
    private String txtErrorWhileSavingChangesToClassTitle;
    private String txtPath;
    private String txtClassDoesNotImplementInterfaceOrErrorText;
    private String txtClassDoesNotImplementInterfaceOrErrorTitle;


    /**
     * Proměnná, která obsahuje texty pro aplikaci ve zvoleném jazyce
     */
    private static Properties languageProperties;


    /**
     * Konstruktor této třídy, ktery vatvoří instanci této třídy a dále naplní promennou, kterárá "drží" referencen na
     * soubor s texty pro aplikaci a zavolá metodu, která naplní textové proměnné aby obsahovali text ve zvoleném
     * jazyce, pro případ výpisu nějaké hlášky s příslušnými texty.
     *
     * @param languageProperties
     *         - soubor s texty pro aplikaci ve zvoleném jazyce
     */
    public MenuSharedMethods(final Properties languageProperties) {
        MenuSharedMethods.languageProperties = languageProperties;

        setLanguage(languageProperties);
    }


    @Override
    public void setLanguage(final Properties properties) {
        if (properties != null) {
            txtCommentOnTheMethod = properties.getProperty("Ced_Mfce_TextCommentOnTheMethod",
                    Constants.NFCE_TXT_COMMENT_ON_THE_METHOD);
            txtParameterDescription = properties.getProperty("Ced_Mfce_TextParametersDescription",
                    Constants.NFCE_TXT_PARAMETER_DESCRIPTION);
            txtReturnValue = properties.getProperty("Ced_Mfce_TextReturnValue", Constants.NFCE_TXT_RETURN_VALUE);
            txtPutYouCodeHere = properties.getProperty("Ced_Mfce_TextPutYoutCodeHere",
                    Constants.NFCE_TXT_PUT_YOUR_CODE_HERE);
            txtReturnStringValue = properties.getProperty("Ced_Mfce_TextReturnStringValue",
                    Constants.NFCE_TXT_RETURN_STRING_VALUE);

            txtErrorWhileSavingChangesToClassText_1 = properties.getProperty(
                    "Ced_Mfce_JopErrorWhileSavingChangesToClassText_1",
                    Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TEXT_1);
            txtErrorWhileSavingChangesToClassText_2 = properties.getProperty(
                    "Ced_Mfce_JopErrorWhileSavingChangesToClassText_2",
                    Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TEXT_2);
            txtErrorWhileSavingChangesToClassTitle = properties.getProperty(
                    "Ced_Mfce_JopErrorWhileSavingChangesToClassTitle",
                    Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TITLE);
            txtPath = properties.getProperty("Ced_Mfce_TextPath", Constants.NFCE_TXT_PATH);

            txtClassDoesNotImplementInterfaceOrErrorText = properties.getProperty(
                    "Ced_Mfce_JopClassDoesNotImplementInterfaceOrErrorText",
                    Constants.NFCE_JOP_CLASS_DOES_NOT_IMPLEMENT_INTERFACE_OR_ERROR_TEXT);
            txtClassDoesNotImplementInterfaceOrErrorTitle = properties.getProperty(
                    "Ced_Mfce_JopClassDoesNotImplementInterfaceOrErrorTitle",
                    Constants.NFCE_JOP_CLASS_DOES_NOT_IMPLEMENT_INTERFACE_OR_ERROR_TITLE);
        } else {
            txtCommentOnTheMethod = Constants.NFCE_TXT_COMMENT_ON_THE_METHOD;
            txtParameterDescription = Constants.NFCE_TXT_PARAMETER_DESCRIPTION;
            txtReturnValue = Constants.NFCE_TXT_RETURN_VALUE;
            txtPutYouCodeHere = Constants.NFCE_TXT_PUT_YOUR_CODE_HERE;
            txtReturnStringValue = Constants.NFCE_TXT_RETURN_STRING_VALUE;

            txtErrorWhileSavingChangesToClassText_1 = Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TEXT_1;
            txtErrorWhileSavingChangesToClassText_2 = Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TEXT_2;
            txtErrorWhileSavingChangesToClassTitle = Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TITLE;
            txtPath = Constants.NFCE_TXT_PATH;

            txtClassDoesNotImplementInterfaceOrErrorText =
                    Constants.NFCE_JOP_CLASS_DOES_NOT_IMPLEMENT_INTERFACE_OR_ERROR_TEXT;
            txtClassDoesNotImplementInterfaceOrErrorTitle =
                    Constants.NFCE_JOP_CLASS_DOES_NOT_IMPLEMENT_INTERFACE_OR_ERROR_TITLE;
        }
    }


    @Override
    public void indentMoreText(final CodeEditorAbstract codeEditor) {
        // Postup pro odsazení:
        // Zjistím si řádek na kterém se nachází kurzor
        // Na začátek tohoto řádku vložím tabulátor

        // Dokument - z editoru kódu v dialogu
        final Document document = codeEditor.getDocument();
        final Element rootElem = document.getDefaultRootElement();

        // Řádek, na kterém se nachzí kurzor:
        final Element lineElem = rootElem.getElement(codeEditor.getCaretLineNumber());

        // Index začátku řádku:
        final int startOffset = lineElem.getStartOffset();

        // Na index začátku řádku vložím tabulátor:
        codeEditor.insert("\t", startOffset);
    }


    @Override
    public void indentLessText(final CodeEditorAbstract codeEditor) {
        // Zjistím si řádkek, na kterém se nachází kurzor
        // a zde nepotřebuji zjšťovat, první index řádku, jednoduš z řádku odeberu

        try {
            final Document document = codeEditor.getDocument();
            final Element rootElem = document.getDefaultRootElement();

            // index aktuálního řádku:
            final int curentLine = codeEditor.getCaretLineNumber();

            // Získám si řádek z editoru:
            final Element lineElem = rootElem.getElement(curentLine);

            // Index tačátku řádku:
            final int lineStart = lineElem.getStartOffset();
            // Index konce řádku:
            final int lineEnd = lineElem.getEndOffset();


            // Nyní si mohu vzít text na řádku:
            // Note: na konci řádku ještě odeberu zalomení nového řádku - pokud existuje, aby fungovaly výrazy
            // sprváně (a na správném řádku)
            final String textAtLine = document.getText(lineStart, lineEnd - lineStart).replace("\n", "");


            // Note: Pro odebrání odsazení chcí nejprve odebrat pouze tabulátory a až pak mezery,
            // proto neodebítám rovnou bíle znaky:

            // Nyní otestuji, zda se na začátku řádku ještě nachází odsazení k odebrání (tabulátor na začátku řádku):

            // Note: Při testování musím odebrat na konci zalomení řádku:
            if (textAtLine.matches(REG_EX_INDENT_AT_START_LINE)) {
                // Zde se na začátku řádku nachází nějaký bílí znak
                // Nyní otestuji, zda se jedná o tabulátor:
                if (textAtLine.matches(REG_EX_TABULATOR_AT_START_LINE)) {
                    // Zde se jedná o tabulátor na začátku řádku, tak ho odeberu:

                    // Nejprve odeberu řádek v editoru
                    document.remove(lineStart, (lineEnd - lineStart) - 1);

                    // Zde odeberu odsazení a vložím ho zpět - bez tabulátoru:
                    final String indentedLine = textAtLine.replaceFirst("\t", "");
                    codeEditor.insert(indentedLine, lineStart);

                    // Nastavím kurzor třeba na začátek řádku:
                    codeEditor.setCaretPosition(lineStart);
                }

                else {
                    // Zde se na začátiu řádku nachází nějaký bílý znak, tak ho odeberu - nejedná se o tabulátor,
                    // např mezera

                    // Nejprve odeberu řádek v editoru
                    document.remove(lineStart, (lineEnd - lineStart) - 1);

                    final String indentedLine = textAtLine.replaceFirst("\\s", "");
                    codeEditor.insert(indentedLine, lineStart);

                    // Nastavím kurzor třeba na začátek řádku:
                    codeEditor.setCaretPosition(lineStart);
                }
            }
            // else - zde se na daném řádku nenachází žádný bílý znak k odsazení

        } catch (final BadLocationException e1) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka pří získávání řádku, kde se nachází kurzor pro odebrání odsazení, třída " +
                                "MenuSharedMethods.java, metoda: po kliknutí na položku v menu: indentLessText.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
    }


    @Override
    public void commentText(final CodeEditorAbstract codeEditor) {
        // Na začátek řádku, na kterém se nachází kurzor se vloží dvé normální lomítka - coby komentář:
        final Document document = codeEditor.getDocument();
        final Element rootElem = document.getDefaultRootElement();

        final Element lineElem = rootElem.getElement(codeEditor.getCaretLineNumber());

        final int lineStart = lineElem.getStartOffset();

        codeEditor.insert("//", lineStart);
    }


    @Override
    public void uncommentText(final CodeEditorAbstract codeEditor) {
        // Zjistím si text na řádku, kde se nachází kurzor v editoru kódu a na daném řádku,
        // pokaždé, když se klikne na tuto pololžku v menu, tak odeberu první výskty znaků: '//' pro odkomentování:
        // je možné, že se daný řádek zakomentovatl vícekrát, tak tento symbol odeberu, dokud se na daném řádku bude
        // vyskytovat


        final Document document = codeEditor.getDocument();
        final Element rootElem = document.getDefaultRootElement();

        // index aktuálního řádku:
        final int currentLine = codeEditor.getCaretLineNumber();

        // Získám si řádek z editoru:
        final Element lineElem = rootElem.getElement(currentLine);

        // Index tačátku řádku:
        final int lineStart = lineElem.getStartOffset();
        // Index konce řádku:
        final int lineEnd = lineElem.getEndOffset();


        // Nyní si mohu vzít text na řádku:
        try {
            final String textAtLine = document.getText(lineStart, lineEnd - lineStart);

            if (textAtLine.contains("//")) {
                // Zde řádek obsahuje komentář:

                // Odeberu daný řádek z editoru:
                document.remove(lineStart, lineEnd - lineStart);

                final String uncommentedLine = textAtLine.replaceFirst("//", "");

                // Vložím odkomentovaný řádek zpět do editoru:
                codeEditor.insert(uncommentedLine, lineStart);

                // Nstavím kurzor třeba na začátek daného řádku:
                codeEditor.setCaretPosition(lineStart);
            }

        } catch (final BadLocationException e1) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka pří získávání řádku, kde se nachází kurzor pro odebrání komentáře - " +
                                "odkomentování, třída MenuSharedethods.java, metoda: po kliknutí na položku v menu: " +
                                "itemUncomment.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
    }


    @Override
    public void formatText(final CodeEditorAbstract codeEditor, final File fileClassToFormat) {
        try (final Reader reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(fileClassToFormat), StandardCharsets.UTF_8))) {

            // Podmínka by neměla nastat:
            if (codeEditor.getFormatter() == null)
                codeEditor.createFormatter(null);


            // Zformátování kódu:
            final String formattedCode = FormatterHelper.format(reader, codeEditor.getFormatter());


            // Přepíšu kód do listu dle zalomení řádku:
            final List<String> formattedSourceCode = Arrays.asList(formattedCode.split("\n"));

            /*
             * Zápis zformátovaného textu / kódu do souboru.
             *
             * Note:
             *
             * Je třeba provést zápis zformátovaného textu / kódu bez přidávání nových mezer při každém zápisu.
             * (Přidání nového řádku již bude uvedeno ve zformátovaném kódu.)
             */
            final boolean isWritten = editCode.setTextToFileWithoutNewLine(fileClassToFormat.getAbsolutePath(),
                    formattedSourceCode);

            // Podmínka není nezbytná, vždy by mělo projít:
            if (isWritten)
                /*
                 * Zde se podařilo zapsat zformátovaný kód, tak je možné jej načíst do editoru. Ale je třeba načíst
                 * text tak, že se přečte text zapsaný do souboru (viz výše). Kdyby se vložil do editoru výše
                 * zformátovaný text, neshodoval by se s textem uloženým v souboru, protože by se mohlu ponechat
                 * některé znaky, které by v souboru nebyly nebo naopak.
                 */
                codeEditor.setTextToCodeEditor(editCode.getTextOfFile(fileClassToFormat.getAbsolutePath()));


            else
                JOptionPane.showMessageDialog(null,
                        txtErrorWhileSavingChangesToClassText_1 + "\n"
                                + txtErrorWhileSavingChangesToClassText_2 + "\n" + txtPath + ": "
                                + fileClassToFormat.getAbsolutePath(),
                        txtErrorWhileSavingChangesToClassTitle, JOptionPane.ERROR_MESSAGE);

        } catch (final IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při čtení souboru na cestě: "
                        + fileClassToFormat.getAbsolutePath()
                        + ", soubor nejspíše neexistuje, třída MenuSharedMethods.java, metoda - po kliknutí na " +
                        "položku v metnu formátovat, toto konkrétně je metoda: formatText.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }
    }


    @Override
    public void insertMethod(final CodeEditorAbstract codeEditor) {
        final String methodText = "\t/**\n"
                + "\t* " + txtCommentOnTheMethod + "\n"
                + "\t*\n"
                + "\t* @param parameterName = " + txtParameterDescription + "\n"
                + "\t*\n"
                + "\t* @return " + txtReturnValue + "\n"
                + "\t*/\n"
                + "\tprivate int methodName(int parameterName) {\n"
                + "\t\t// " + txtPutYouCodeHere + "\n"
                + "\t\treturn parameterName;\n"
                + "\t}\n";


        // Vložení textu na danou pozici v editoru - pomocí metody insert:
        codeEditor.insert(methodText, codeEditor.getCaretPosition());
    }


    @Override
    public void insertDefaultSerialVersionId(final CodeEditorAbstract codeEditor) {
        final String serialVersionId = "\t/**\n"
                + "\t*\n"
                + "\t*/\n"
                + "\tprivate static final long serialVersionUID = 1L;\n";

        // Vložení textu na danou pozici v editoru - pomocí metody insert:
        codeEditor.insert(serialVersionId, codeEditor.getCaretPosition());
    }


    @Override
    public void insertGeneratedSerialVersionId(final Class<?> clazz, final CodeEditorAbstract codeEditor) {
        String serialVersionId = "\t/**\n"
                + "\t*\n"
                + "\t*/\n"
                + "\tprivate static final long serialVersionUID = ";

        try {
            final ObjectStreamClass osc = ObjectStreamClass.lookup(clazz);
            final long serialId = osc.getSerialVersionUID();
            serialVersionId += serialId;

            serialVersionId += "L;\n";

            // Vložení textu na danou pozici v editoru - pomocí metody insert:
            codeEditor.insert(serialVersionId, codeEditor.getCaretPosition());

        } catch (final NullPointerException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informace i chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při generování serial UID.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();


            /*
             * Tato výjimka nastane v případě, že uživatel zvolil přidání do třídy
             * vygenerování serial version ID, ale příslušná třída neimplementuje rozhraní
             * serializable, nebo nedědi z nějaké třídy, která ano.
             */
            JOptionPane.showMessageDialog(null, txtClassDoesNotImplementInterfaceOrErrorText,
                    txtClassDoesNotImplementInterfaceOrErrorTitle, JOptionPane.ERROR_MESSAGE);
        }
    }


    @Override
    public void replaceText(final CodeEditorAbstract codeEditor) {
        // Otevře se dialog, ve kterém se nahradí zadaný kód a ten se vrátí
        final ReplaceTextInCodeEditorForm replaceFrom = new ReplaceTextInCodeEditorForm(codeEditor.getText());
        replaceFrom.setLanguage(languageProperties);

        // Vrátí nahrazený kód nebu null, pokud byl dialog uzvařen:
        final String replacementText = replaceFrom.findAndReplace();


        if (replacementText != null) {
            // zde uživatel potvrdil data z dialogu:

            // Uložím si aktuální pozdi kurzoru:
            final int caretPosition = codeEditor.getCaretPosition();

            // vložím nový kod do editoru :
            final List<String> newCodeToEditor = Arrays.asList(replacementText.split("\n"));
            codeEditor.setTextToCodeEditor(newCodeToEditor);

            // vložím kurzor tam kde byl:
            codeEditor.setCaretPosition(caretPosition);
        }
    }


    @Override
    public void goToLine(final CodeEditorAbstract codeEditor) {
        final Document document = codeEditor.getDocument();
        final Element rootElem = document.getDefaultRootElement();

        // Počet řádků v editoru:
        final int numLines = rootElem.getElementCount();

        // Otevřu dialog pro získání čísla řádku, na který se má přesunout kurzor:
        final GoToLineForm gtl = new GoToLineForm(numLines);
        gtl.setLanguage(languageProperties);
        final int goToLine = gtl.getLineToGo();

        // zde se vrátli hodnota, tak otestuji, zda byl dialog zavřen a vrátil hodnotu:
        if (goToLine > -1) {
            // Zjistím si řádek:
            final Element lineElem = rootElem.getElement(goToLine - 1);

            // A zjistím si počet znaků k začátku daného řadku a nastavím kurzor na zacatek daneho radku:
            final int lineStart = lineElem.getStartOffset();

            codeEditor.setCaretPosition(lineStart);
        }
    }


    @Override
    public void toStringMethod(final CodeEditorAbstract codeEditor) {

        final String textOfMethod = "\t@Override\n"
                + "\tpublic String toString() {\n"
                + "	\t// " + txtReturnStringValue + "\n"
                + "\t\treturn super.toString();\n"
                + "\t}\n";

        // Vložím "sestavenou" metodu výše na pozici kurzoru v editoru:
        codeEditor.insert(textOfMethod, codeEditor.getCaretPosition());
    }


    @Override
    public void generateGettersAndSetters(final CodeEditorAbstract codeEditor, final Class<?> clazz) {
        /*
         * Vytvořím dialog pro označení položek s přístupovými metodami, které se mají
         * přidat:
         */
        final GenerateAccessMethodForm amf = new GenerateAccessMethodForm(clazz);
        amf.setLanguage(languageProperties);

        /*
         * Získám si text, který bude obsahovat veškeré označené přístupové metody.
         */
        final String modifiers = amf.generateGettersAndSetters();



        /*
         * Pokud se žádné texty nevrátily, tj. uživatel dialog zavřel, pak mohu skončit.
         */
        if (modifiers == null)
            return;



        /*
         * Zde byly vráceny getry a nebo setry - nebyl zavřen dialog, ale kliknuto na
         * OK, tak je mohu vložit do editoru na pozici kurzoru myši nebo na konec třídy:
         */
        // if (codeEditor.getCaretPosition() > 0)
        if (codeEditor.getCaretPosition() != 0)
            /*
             * Pokud není pozice kurzoru na pozici nula, tj. úplně na začátku dokumentu, pak
             * se vloži ten text s přístupovými metodami na pozici kurzoru.
             *
             * Ale pokud je pozice dokumentu na pozici nula, pak se vloži před poslední
             * zavírací závorku.
             */
            codeEditor.insert(modifiers, codeEditor.getCaretPosition());


        else {
            /*
             * Získám si veškerý text z dokumentu a zjistím si text poslední zavírací
             * špičaté závorky ('}').
             */
            final List<String> allText = codeEditor.getTextFromCodeEditor();

            /*
             * Index řádku, kde se nachází nalezená zavírací závorka.
             */
            int lineIndex = -1;

            /*
             * Projdu veškerý text získaný z příslušné třídy (souboru) a vyhledám na konci
             * dokumentu zavírací špičatou závorku, pokud ji najdu, pak se do proměnné
             * lineIndex vloží index řádku, kde byla ta závorka nalezena.
             */
            for (int i = allText.size() - 1; i >= 0; i--) {
                if (allText.get(i).matches(Constants.REG_EX_CLOSING_BRACKET)) {
                    lineIndex = i;

                    // Break, protože stačí najít jen první závorku od konce, zde jsem ji našel, tak
                    // mohu skončit cyklus:
                    break;
                }
            }


            /*
             * Otestuji, zda jsem nalezel příslušnou závorku, pokud ne, pak mi nezbývá, než
             * vložit ty texty na pozici kurzoru, aby si uživatel nemyslel, že nastala
             * nějaká chyba apod.
             */
            if (lineIndex == -1) {
                /*
                 * Zde nebyla nalezna poslední zavírací špičatá závorka, tak vložím získané /
                 * označené přístupové metody na pozici kurzoru.
                 */
                codeEditor.insert(modifiers, codeEditor.getCaretPosition());

                // Mohu skončit, dále už je jen testování, když byla závorka nalezena:
                return;
            }


            final Document document = codeEditor.getDocument();
            final Element rootElem = document.getDefaultRootElement();

            // Zjistím si řádek s nalezenou závorkou:
            final Element lineElem = rootElem.getElement(lineIndex);

            /*
             * A zjistím si počet znaků k začátku daného řadku a nastavím kurzor na začatek
             * daneho radku:
             */
            final int lineStart = lineElem.getStartOffset();

            // Nastavení kurzoru na požadovanou pozici - začátek řádku se závorkou:
            codeEditor.setCaretPosition(lineStart);

            // Vložím do dokumentu text s metodami:
            codeEditor.insert(modifiers, codeEditor.getCaretPosition());
        }
    }


    @Override
    public void generateConstructor(final CodeEditorAbstract codeEditor, final Class<?> clazz) {
        // Vytvořím dialog pro označení položek, které se mají přidat:
        final GenerateConstructorUsingFieldsForm generateConstructor = new GenerateConstructorUsingFieldsForm(clazz);
        generateConstructor.setLanguage(languageProperties);

        final String constructor = generateConstructor.getNewConstructor();

        if (constructor != null)
            // Zde mohu vložit konstruktor:
            codeEditor.insert(constructor, codeEditor.getCaretPosition());
    }
}