package cz.uhk.fim.fimj.code_editor.combo_box_with_radio_button;

import javax.swing.*;
import java.awt.*;

/**
 * Třída slouží pro vykreslování komponenty typu {@link javax.swing.JRadioButton} v komponentě {@link
 * javax.swing.JComboBox}.
 *
 * Zdroj:
 *
 * http://www.java2s.com/Tutorials/Java/Swing_How_to/JComboBox/Add_JCheckBox_components_to_JComboBox.htm
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 01.09.2018 13:52
 */

public class RadioButtonRenderer implements ListCellRenderer<CmbItem> {

    /**
     * Komponenta, která se má v cmb vykreslit.
     */
    private final JRadioButton radioButton;


    /**
     * Konstruktor třídy (pro vytvoření instance JRadioButton).
     */
    RadioButtonRenderer() {
        this.radioButton = new JRadioButton();
    }


    @Override
    public Component getListCellRendererComponent(final JList<? extends CmbItem> list, final CmbItem value,
                                                  final int index, final boolean isSelected,
                                                  final boolean cellHasFocus) {
        if (value == null)
            return radioButton;

        radioButton.setText(value.getText());
        radioButton.setSelected(value.isSelected());

        return radioButton;
    }
}
