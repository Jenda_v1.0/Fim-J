package cz.uhk.fim.fimj.code_editor;

import java.lang.reflect.Field;

import javax.swing.JCheckBox;

/**
 * Tato třída coby objekt obsahuje JCheckBox a Field a třída je použíta pro vytvoření "uložiště" pro položku - proměnnou
 * z instance třídy a JCheckBoxu který lze zaškrtnout
 * <p>
 * Dle toho, zda je daný JchechBox zaškrnutý či nikoliv poznám, zda mám do parametru konstruktoru přidat danou položku -
 * proměnnou
 * <p>
 * Tato třída se pouzžívá ve třídě: GenerateConstructorUsingFieldsForm.java, aby si uživatel mohl zvolit, jaké položky
 * chce přidat do parametru konstruktoru
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class FieldAndCheckBox {

    private final Field field;

    private final JCheckBox chcb;


    FieldAndCheckBox(final Field field, final JCheckBox chcb) {
        this.field = field;
        this.chcb = chcb;
    }


    final boolean isChcbSelected() {
        return chcb.isSelected();
    }

    public final String getFieldName() {
        return field.getName();
    }

    public final void setSelected(final boolean selected) {
        chcb.setSelected(selected);
    }

    public Field getField() {
        return field;
    }
}