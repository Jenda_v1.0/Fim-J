package cz.uhk.fim.fimj.code_editor.generate_access_methods;

import java.awt.Component;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreePath;

/**
 * Třída slouží pro "aktualizaci" komponent (JCheckboxů) ve stromu v dialogu po kliknutí na něj - pro změnu označení,
 * zda je dané JCheckBox zaškrtnutý nebo ne
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class CheckBoxNodeEditor extends AbstractCellEditor implements TreeCellEditor {

	private static final long serialVersionUID = 1L;

	
	private final CheckBoxNodeRenderer renderer = new CheckBoxNodeRenderer();

	

    private final JTree tree;

    
    
    
    public CheckBoxNodeEditor(final JTree tree) {
    	super();
    	
      this.tree = tree;
    }

    
    
    
    
    
    /**
     * Metoda, která vrátí upravenou hodnotu z dialogu - ze stromu
     * po kliknutí na příslušnou hodnotu
     */
    public final Object getCellEditorValue() {
    	// Zde si převezmu nová data po kliknutí na nějaký JCheckBox z dialogu:
    	// metoda getLeafRenderer mi vrátí editované označení políčka
      final JCheckBox checkbox = renderer.getLeafRenderer();      
      
      // stačí si vzít označenou položku ve stromu a vzít si z ní data:
      
      // označený řádek:
      final int selectedRowInTree = tree.getSelectionModel().getLeadSelectionRow();
      
      final TreePath path = tree.getPathForRow(selectedRowInTree);
      
      if (path != null) {
    	  final Object node = path.getLastPathComponent();
    	  
    	  if (node instanceof DefaultMutableTreeNode) {
    		  final DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) node;
    		  
    		  final Object userObject = treeNode.getUserObject();
    		  
    		  if ((treeNode.isLeaf()) && (userObject instanceof CheckBoxNode)) {
    			  final CheckBoxNode n = (CheckBoxNode) userObject;
    			  
    			  return new CheckBoxNode(n, checkbox.isSelected());
    		  }
    	  }
      }
      
      // Nemělo by nastat:
      return null;
    }
    
    

    
    
    
    
	public final boolean isCellEditable(final EventObject event) {
		boolean returnValue = false;

		if (event instanceof MouseEvent) {
			final MouseEvent mouseEvent = (MouseEvent) event;

			final TreePath path = tree.getPathForLocation(mouseEvent.getX(), mouseEvent.getY());

			if (path != null) {
				final Object node = path.getLastPathComponent();

				if (node instanceof DefaultMutableTreeNode) {
					final DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) node;
					final Object userObject = treeNode.getUserObject();

					returnValue = ((treeNode.isLeaf()) && (userObject instanceof CheckBoxNode));
				}
			}
		}

		return returnValue;
	}

    
	
	
    
    
	public final Component getTreeCellEditorComponent(final JTree tree, final Object value, final boolean selected,
			final boolean expanded, final boolean leaf, final int row) {

		final Component editor = renderer.getTreeCellRendererComponent(tree, value, true, expanded, leaf, row, true);

		// editor always selected / focused
		final ItemListener itemListener = itemEvent -> {
			if (stopCellEditing())
				fireEditingStopped();
		};

		if (editor instanceof JCheckBox)
			((JCheckBox) editor).addItemListener(itemListener);

		return editor;
	}
}