package cz.uhk.fim.fimj.code_editor.generate_access_methods;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import cz.uhk.fim.fimj.file.*;
import cz.uhk.fim.fimj.forms.SourceDialogForm;

/**
 * Třída slouží jako dialog pro označení getrů a setřů položek / proměnných z třídy, která je otevřená v editoru
 * zdrojového kódu
 * <p>
 * Uživatel označí položky - coby getr a setr, po kliknutí na tlačítko OK se označené getry a setry přídají na pozici
 * kurzoru v editoru zdrojového kódu
 * <p>
 * Dále jsou k dispozici tlačítka označit vše nebo odznačít vše
 * <p>
 * Po kliknutí na tlačítko Zrušit (v četěině) se dialog zavře a je možné dále pokračovat v editaci zdrojového kódu
 * <p>
 * <p>
 * Pro Jtree s JCheckBoxy jsem využil následující zdroje: http://www.java2s
 * .com/Code/Java/Swing-JFC/CheckBoxNodeTreeSample.htm
 * http://www.java2s.com/Code/Java/Swing-JFC/ConvertingAllNodesinaJTreeComponenttoaTreePathArray.htm
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class GenerateAccessMethodForm extends SourceDialogForm implements LanguageInterface, ActionListener {

    private static final long serialVersionUID = -8720827414357005139L;


    /**
     * Label s informacemi o zaškrtávání tlačítek:
     */
    private final JLabel lblInfo;


    private final JButton btnSelectAll;
    private final JButton btnDeselectAll;


    private final JRadioButton rbPrivate;
    private final JRadioButton rbPublic;
    private final JRadioButton rbProtected;

    private final JCheckBox chcbFinal;
    private final JCheckBox chcbSynchronized;
    private final JCheckBox chcbGenerateComment;


    /**
     * Následující proměnná je globální, kvůli nastavení ohraničení:
     */
    private final JPanel pnlModifiers;


    /**
     * Z následující proměnné msí být globální / instanční proměnná
     */
    private final JTree tree;


    /**
     * Proměnná, která obshuje načtenou (zkompilovanou) třídu, do které se mají
     * vytvořit přístupové metody (getry a setry).
     *
     * Tato proměnná je zde jako globální neboli instanční, protože je potřeba
     * získat její název v případě setru na statickou proměnnou, kde se využije
     * název proměnné.
     *
     * Je také možné místo proměnné tohoto typu vytvořit proměnnou například typu
     * String a do ní ten název uložit, ale to je již v celku jedno, je to tak jako
     * tak jedna reference.
     */
    private final Class<?> clazz;


    /**
     * Konstruktor této třídy.
     *
     * @param clazz
     *            - Třídy typu class načtená ze souboru po kompilaci dané třídy
     */
    public GenerateAccessMethodForm(final Class<?> clazz) {
        this.clazz = clazz;

        initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), "/icons/app/GettersAndSettersIcon.png", new Dimension(500, 650),
                true);


        gbc = createGbc();

        index = 0;

        gbc.gridwidth = 2;


        // Pnale s labelem s informace
        final JPanel pnlInfo = new JPanel(new FlowLayout(FlowLayout.CENTER));

        lblInfo = new JLabel();
        pnlInfo.add(lblInfo);

        setGbc(index, 0, pnlInfo);
		
		
		
		
		// Postup:
		// Načtu si položky z třídy
		// Dále vytvořím příslušné položky ve stromu s getry a setry (pokud není proměnná final)
		// a vložím jej do dialogu:


        // Načtu si všechny položky z zkompilované třídy:
        final Field[] fields = clazz.getDeclaredFields();

        // Deklaruji si pole, kam vložím všechny vytvořrné pložky:
        final Object rootNodes[] = new Object[fields.length];


        for (int i = 0; i < fields.length; i++)
            rootNodes[i] = createNode(fields[i]);





        // Kořen stromu (nebude vidět):
        final Vector<Object> rootVector = new NamedVector("Root", rootNodes);

        // deklaruji strom:
        tree = new JTree(rootVector);

        // vykreslování JChecboxů
        tree.setCellRenderer(new CheckBoxNodeRenderer());


        tree.setCellEditor(new CheckBoxNodeEditor(tree));

        // Aby šlo ve stromu zaškrtávat JCheckboxy - povolená manipulace:
        tree.setEditable(true);

        // Viditelnost: Kořenový adresář:
        tree.setRootVisible(false);

        // Viditelnost "čáty" spojující položky:
        tree.setShowsRootHandles(false);

        // Jedna moznost pro oznaceni:
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

        // Aby šlo se strome rolovat, pokud tam bude hodně položek:
        final JScrollPane jspTree = new JScrollPane(tree);

        jspTree.setPreferredSize(new Dimension(450, 330));

        // Přidání do dialogu:
        setGbc(++index, 0, jspTree);



        /*
         * Tento cyklus je potřeba pouze k tomu, když chci, aby při otevření / spuštění
         * dialogu byly veškeré položky rozbalené.
         *
         * Tj. cyklus projde veškeré položky ve strmou a všechny je rozbalí.
         */
        for (int i = 0; i < tree.getRowCount(); i++)
            tree.expandRow(i);






        // Tlačítka pro označení a odznačení:
        final JPanel pnlSelectDeselect = new JPanel(new FlowLayout(FlowLayout.CENTER));

        btnSelectAll = new JButton();
        btnDeselectAll = new JButton();

        btnSelectAll.addActionListener(this);
        btnDeselectAll.addActionListener(this);

        pnlSelectDeselect.add(btnSelectAll);
        pnlSelectDeselect.add(btnDeselectAll);

        setGbc(++index, 0, pnlSelectDeselect);






        // Panel s modifikátory:
        // V tomto panelu budou uloženy dva panely s radioButtony a s checkbocy:
        pnlModifiers = new JPanel();
        pnlModifiers.setLayout(new BorderLayout());

        rbPublic = new JRadioButton("public");
        rbPrivate = new JRadioButton("private");
        rbProtected = new JRadioButton("protected");

        final ButtonGroup bg = new ButtonGroup();
        bg.add(rbPublic);
        bg.add(rbPrivate);
        bg.add(rbProtected);
        rbPublic.setSelected(true);


        // V tomto panelu budou uloženy radioButtony s modifikátory getru a setrů
        final JPanel pnlRb = new JPanel(new FlowLayout(FlowLayout.CENTER));
        pnlRb.add(rbPublic);
        pnlRb.add(rbPrivate);
        pnlRb.add(rbProtected);


        // Panel s JCheckboxy:
        final JPanel pnlChcb = new JPanel(new FlowLayout(FlowLayout.CENTER));
        chcbFinal = new JCheckBox("final");
        chcbSynchronized = new JCheckBox("synchronized");

        pnlChcb.add(chcbFinal);
        pnlChcb.add(chcbSynchronized);


        // Přidání panelů do panelu s modifikátory:
        pnlModifiers.add(pnlRb, BorderLayout.NORTH);
        pnlModifiers.add(pnlChcb, BorderLayout.CENTER);

        setGbc(++index, 0, pnlModifiers);




        chcbGenerateComment = new JCheckBox();
        setGbc(++index, 0, chcbGenerateComment);








        // Tlačítka pro potvrzení nebo uzavření dialogu:
        final JPanel pnlButtos = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        btnOk = new JButton();
        btnCancel = new JButton();

        btnOk.addActionListener(this);
        btnCancel.addActionListener(this);

        pnlButtos.add(btnOk);
        pnlButtos.add(btnCancel);

        setGbc(++index, 0, pnlButtos);




        pack();
        setLocationRelativeTo(null);
    }
	
	
	
	
	
	
	
	
    
	/**
	 * Metoda, která vytvoří "položku" ve stromu.
	 * v postatě vyvoří celý jeden uzel = 
	 *   Vector
	 * 		- Getr
	 * 		- Setr
	 * 
	 * @param field = proěnná / položka ve třídě, na kterou se má vytvořit getr a setr
	 * 
	 * @return vytvořený zmíněný uzel
	 */
    private static Vector<Object> createNode(final Field field) {
        final CheckBoxNode[] node;


        // Zde otestuji, zda je proměnná final nebo ne,
        // pokud ano, tak vytvořím pouze getr na danou proměnnou, jinak i setr:
        if (Modifier.isFinal(field.getModifiers())) {
            node = new CheckBoxNode[1];
            // Zde je daná proměnná final, takže vytvořím pouze getr na danou proměnnou:
            node[0] = new CheckBoxNode(field, false);
        }

        else {
            // Zde není proměnná final, tak vytvoří getr i setr na danou proměnnou
            node = new CheckBoxNode[2];
            node[0] = new CheckBoxNode(field, false);
            node[1] = new CheckBoxNode(field, true);
        }

        return new NamedVector(field.getName(), node);
    }






    /**
     * Metoda, která na začátku zviditelní dialog, tím se stane modální, a po kliknutí na tlačítko OK se otestuje, zda
     * je nějaký JCheckBox označen, aby se přidal getr nebo setr pro vrácení, pokud je nějaký označen, tak se "převede"
     * do metody a v podobě textu se všechny getry a setry vrátí
     * <p>
     * Pokud se klikne na zrušit, dialog se zavře a metoda vrátí null
     *
     * @return null nebo označené getry a setry v textu - Stringu pro přidání do editoru zdrojového kódu
     */
    public final String generateGettersAndSetters() {
        setVisible(true);

        if (!variable)
            return null;

        // Builder pro sestavení textu s označenými přístupovýmy metodamy:
        final StringBuilder accessMethodsBuilder = new StringBuilder();
        accessMethodsBuilder.append("\n\n");

        // načtu si všechny "cesty" a dle toho, která je zaškrtnutá, tak buď vytvořím příslušnou metodu nebo "jdu dál"
        final TreePath[] pathsTest = getPaths(tree, false);

        for (final TreePath t : pathsTest) {
            final Object node = t.getLastPathComponent();

            if (!(node instanceof DefaultMutableTreeNode))
                continue;


            final DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) node;

            final Object useObject = treeNode.getUserObject();

            if ((!treeNode.isLeaf()) || (!(useObject instanceof CheckBoxNode)))
                continue;


            final CheckBoxNode n = (CheckBoxNode) useObject;

            // Zda je daný JCheckBox u dané položky označený pro přidání:
            if (!n.isSelected())
                continue;


            // zde je JCheckBox označený, tak vytvořím příslušnou metodu:
            final Field field = n.getField();

            /*
             * Zda není položka null, v takovém případně došlo k nějaké chybě a není možné sestavit metodu (nemělo by
             * nastat).
             */
            if (field == null)
                continue;

            /*
             * Zde je vše v pořádku, tak je možné sestavit dle označených komponent v okně dialogu přístupovou metodu.
             */
            final String accessMethod = GenerateAccessMethodHelper.getAccessMethod(field, clazz,
                    chcbGenerateComment.isSelected(), n.isSetter(),
                    getVisibility(), chcbSynchronized.isSelected(), chcbFinal.isSelected());

            accessMethodsBuilder.append(accessMethod);
        }

        // Na konec se vrátí výsledný text s přístupovými metodami:
        return accessMethodsBuilder.toString();
    }



    /**
     * Získání viditelnosti přístupové metody dle označené komponenty pro nastavení viditelnosti.
     *
     * @return výčtovou hodnotu, která značí viditelnost přístupové metody.
     */
    private Visibility getVisibility() {
        if (rbPublic.isSelected())
            return Visibility.PUBLIC;

        else if (rbPrivate.isSelected())
            return Visibility.PRIVATE;

        else if (rbProtected.isSelected())
            return Visibility.PROTECTED;

        return Visibility.PACKAGE_PRIVATE;
    }





    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof JButton) {
            if (e.getSource() == btnOk) {
                // Zde nepotřebuji nic testovat, zda je nějaké pole vyplněné, či zaškrtnuté, apod.
                // buď dle toho co uživatel označil nebo nechal pouze výchozí nastavení se po klikntí na toto
                // tlačítko vytvoří příslušné metody
                variable = true;
                dispose();
            } else if (e.getSource() == btnCancel) {
                variable = false;
                dispose();
            } else if (e.getSource() == btnSelectAll)
                selectDeselectJTree(true);


            else if (e.getSource() == btnDeselectAll)
                selectDeselectJTree(false);
        }
    }






    @Override
    public void setLanguage(final Properties properties) {
        if (properties != null) {
            setTitle(properties.getProperty("Gamf_DialogTitle", Constants.GAMF_DIALOG_TITLE));

            lblInfo.setText(properties.getProperty("Gamf_LabelInfo", Constants.GAMF_LBL_INFO));

            btnSelectAll.setText(properties.getProperty("Gamf_ButtonSelectAll", Constants.GAMF_BTN_SELECT_ALL));
            btnDeselectAll.setText(properties.getProperty("Gamf_ButtonDeselectAll", Constants.GAMF_BTN_DESELECT_ALL));

            chcbGenerateComment.setText(properties.getProperty("Gamf_CheckBoxGenerateComment",
                    Constants.GAMF_CHCB_GENERATE_COMMENT));

            btnOk.setText(properties.getProperty("Gamf_ButtonOk", Constants.GAMF_BTN_OK));
            btnCancel.setText(properties.getProperty("Gamf_ButtonCancel", Constants.GAMF_BTN_CANCEL));

            pnlModifiers.setBorder(BorderFactory.createTitledBorder(properties.getProperty(
                    "Gamf_PanelModifiersBorderTitle", Constants.GAMF_PNL_MODIFIERS_BORDER_TITLE)));
        }

        else {
            setTitle(Constants.GAMF_DIALOG_TITLE);

            lblInfo.setText(Constants.GAMF_LBL_INFO);

            btnSelectAll.setText(Constants.GAMF_BTN_SELECT_ALL);
            btnDeselectAll.setText(Constants.GAMF_BTN_DESELECT_ALL);

            chcbGenerateComment.setText(Constants.GAMF_CHCB_GENERATE_COMMENT);

            btnOk.setText(Constants.GAMF_BTN_OK);
            btnCancel.setText(Constants.GAMF_BTN_CANCEL);

            pnlModifiers.setBorder(BorderFactory.createTitledBorder(Constants.GAMF_PNL_MODIFIERS_BORDER_TITLE));
        }
    }








    /**
     * Metoda, která v daném "stromu" s položkami coby getry a setry zaškrtne nebo odškrtne (dle parametru) všechny
     * JCheckBox v tomto stromu v dialogu
     *
     * @param selectDeselect
     *         - logická hodnota o tom, zda se mají všechny JChecBoxy zaškrtnout nebo Odškrtnout
     */
    private void selectDeselectJTree(final boolean selectDeselect) {
        final TreePath[] pathsTest = getPaths(tree, false);

        for (final TreePath t : pathsTest) {
            final Object node = t.getLastPathComponent();

            if (!(node instanceof DefaultMutableTreeNode))
                continue;

            final DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) node;

            final Object useObject = treeNode.getUserObject();

            if ((treeNode.isLeaf()) && (useObject instanceof CheckBoxNode)) {
                final CheckBoxNode n = (CheckBoxNode) useObject;
                n.setSelected(selectDeselect);
            }
        }

        // Znovu překreslím "strom", aby se projevila změna v dialogu:
        tree.repaint();
    }







    /**
     * Metoda pro získání "cest" stromu - Jtree s přístupovými metodami
     *
     * @param tree
     *         - Jtree v dialogu
     * @param expanded
     *         - rozšířeno
     *
     * @return testy, abych mohl zjistti, který JChecbox je zaškrtnutý
     */
    private static TreePath[] getPaths(final JTree tree, final boolean expanded) {
        final TreeNode root = (TreeNode) tree.getModel().getRoot();
        final List<TreePath> list = new ArrayList<>();
        getPaths(tree, new TreePath(root), expanded, list);

        return list.toArray(new TreePath[list.size()]);
    }






    private static void getPaths(final JTree tree, final TreePath parent, final boolean expanded,
                                 final List<TreePath> list) {
        if (expanded && !tree.isVisible(parent))
            return;

        list.add(parent);

        final TreeNode node = (TreeNode) parent.getLastPathComponent();

        if (node.getChildCount() >= 0) {

            for (@SuppressWarnings("unchecked") final Enumeration<TreeNode> e = node.children(); e.hasMoreElements(); ) {

                final TreeNode n = e.nextElement();
                final TreePath path = parent.pathByAddingChild(n);
                getPaths(tree, path, expanded, list);
            }
        }
    }
}