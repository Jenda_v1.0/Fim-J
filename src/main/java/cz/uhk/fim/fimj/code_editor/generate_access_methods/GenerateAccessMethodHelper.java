package cz.uhk.fim.fimj.code_editor.generate_access_methods;

import cz.uhk.fim.fimj.reflection_support.ParameterToText;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * Podpora pro vytváření / sestavování přístupových metod, které se následně vkládají do editoru kódu.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 11.08.2018 14:29
 */

public class GenerateAccessMethodHelper {


    private GenerateAccessMethodHelper() {
    }


    /**
     * Vygenervání přístupové metody (getru nebo setru - dle proměnné setr) pro proměnnou field.
     * <p>
     * Vždy se sestaví takový text metody, aby jej bylo možné vložit do editoru kódu.
     * <p>
     * Vygenerovaná přístupová metoda bude statická v případě, že i proměnná field je statická.
     *
     * @param field
     *         - proměnná, na kterou se má vytvořit přístupová metoda (Nesmí být null - není testováno). V případě, že
     *         je proměnná final, neměla by se tato metoda volat s parametry pro vygenerování setru (není testováno).
     * @param clazz
     *         - třída, kde se nachází proměnná field. Je zde potřeba pouze pro získání názvu třídy v případě naplnění
     *         statické proměnné. Ale raději zde předávám referenci na "třídu", pro potenciální budoucí rozšíření, kdy
     *         by bylo potřeba z tohoto objektu brát více hodnot (Nesmí být null - není testováno).
     * @param generateComment
     *         - true v případě, že se má vygenerovat komentář k přístupové metodě, jinak false.
     * @param isSetter
     *         - true v případě, že se má jednat o setr, jinak false - bude se jednat o getr.
     * @param visibility
     *         - výčtová hodnota, která značí, jaké viditelnosti má přístupová metoda být.
     * @param isSynchronized
     *         - zda má být metoda synchronized.
     * @param isFinal
     *         - zda má být metoda final.
     *
     * @return výše popsanou přístupovou metodu k proměnné field.
     */
    public static String getAccessMethod(final Field field, final Class<?> clazz, final boolean generateComment,
                                         final boolean isSetter, final Visibility visibility,
                                         final boolean isSynchronized, final boolean isFinal) {

        final StringBuilder accessMethodBuilder = new StringBuilder();

        // Nejprve otestuji, zda je se má vyvořit komentář k dané metodě:
        if (generateComment)
            addJavaDocComment(field.getName(), isSetter, accessMethodBuilder);
            // Zde se nemá vytvořit komentář, tak přidám alespoň tabulátor, jinak by začátek nebyl zarovnaný:
        else accessMethodBuilder.append("\t");


        // Přidání viditelnosti metody:
        accessMethodBuilder.append(visibility.getTxtVisibility());

        // Mezera:
        accessMethodBuilder.append(" ");


        if (Modifier.isStatic(field.getModifiers()))
            accessMethodBuilder.append("static").append(" ");

        if (isSynchronized)
            accessMethodBuilder.append("synchronized").append(" ");

        if (isFinal)
            accessMethodBuilder.append("final").append(" ");


        /*
         * Přidání návratové hodnoty metody a začátek názvu metody. V případě, že se jendá o setr, ta se přidá "set",
         * jinak buď "get" nebo "is".
         */
        accessMethodBuilder.append(getReturnDataType(field, isSetter));

        // Přidání názvu proměnné s prvním velkým písmenem:
        accessMethodBuilder.append(getFieldNameFirstLetterUpperCase(field.getName()));

        // Přádní buď prázdných závorek nebo závorek s parametrem metody:
        accessMethodBuilder.append(getParentheses(isSetter, field));


        // Otevírací složená závorka:
        accessMethodBuilder.append("{\n");
        // Odsazení pro správné formátování kódu:
        accessMethodBuilder.append("\t\t");


        /*
         * Přidání těla metody. Buď se bude jednat o vrácení proměnné nebo naplnění proměnné, liší se případy pro
         * naplnění staticé a nestatické proměnné v konkrétní instanci.
         */
        accessMethodBuilder.append(getBodyOfMethod(isSetter, field, clazz));

        // Nový řádek a zakončení:
        accessMethodBuilder.append("\n\t}");

        // Pro další metodu odřádkování:
        accessMethodBuilder.append("\n\n");

        return accessMethodBuilder.toString();
    }


    /**
     * @param isSetter
     *         - true v případě, že se jedná o setr, jinak false (= getr).
     * @param field
     *         - v případě, že se jedná o setr, tak se sestaví syntaxe pro naplnění této proměnné, jinak pro vrácení
     *         její hodnoty.
     * @param clazz
     *         - třída, kde se nachází proměnná field. Jedná se puze o získání názvu třídy.
     *
     * @return syntaxe pro naplnění proměnné field nebo pro vrácení její hodnoty.
     */
    private static String getBodyOfMethod(final boolean isSetter, final Field field, final Class<?> clazz) {
        final StringBuilder bodyOfMethodBuilder = new StringBuilder();

        if (isSetter) {
            /*
             * Pokud je proměnná statická, pak se naplnění proměnné musí nacházet v syntaxi:
             * ClassName.variableName = methodParameter;, jinak je místo názvu třídy klíčové
             * slovo this, které ukazuje na konkrétní instanci třídy.
             */
            if (Modifier.isStatic(field.getModifiers()))
                bodyOfMethodBuilder.append(clazz.getSimpleName());

            else
                // Zde se jedná o syntaxi: this.variableName = variableName;
                bodyOfMethodBuilder.append("this");

            bodyOfMethodBuilder.append(".").append(field.getName()).append(" = ").append(field.getName()).append(";");
        }

        // Zde se jedná o getr, přidám syntaxi: return variableName;
        else bodyOfMethodBuilder.append("return ").append(field.getName()).append(";");

        return bodyOfMethodBuilder.toString();
    }


    /**
     * Získání kulatých závorek za název metody. Bude se jedná buď o prázdné závorky v případě, že metoda nemá parametr
     * nebo se získá z proměnné field datový typ proměnné, což bude typ paremetru metody a název parametru.
     *
     * @param isSetter
     *         - true v případě, že se jedná o setr, pak se bude přidávat parametr metody (dle proměnné field).
     * @param field
     *         - proměnná, ze které se získá datový typ, která se vloží do parametru metody.
     *
     * @return kulaté závorky buď prázdné nebo s parametrem field.
     */
    private static String getParentheses(final boolean isSetter, final Field field) {
        final StringBuilder parenthesesBuilder = new StringBuilder();

        parenthesesBuilder.append("(");

        if (isSetter)
            /*
             * Zde se jedná o setr, metoda má parametr.
             *
             * U proměnné nemusím řešit první malé písmeno, to už by mělo být dle syntaxe zadané, tak se jen přidá
             */
            parenthesesBuilder.append(ParameterToText.getFieldInText(field, false)).append(" ").append(field.getName());

        parenthesesBuilder.append(")");

        return parenthesesBuilder.toString();
    }


    /**
     * Získání názvu name s prvním písmenem velkým.
     *
     * @param name
     *         - text, jehož první písmeno se vrátí s prvním velkým písmenem.
     *
     * @return text name, který bude jako první písmeno obsahovat velké písmeno.
     */
    public static String getFieldNameFirstLetterUpperCase(final String name) {
        final String firstLetterUpperCase = String.valueOf(Character.toUpperCase(name.charAt(0)));

        return firstLetterUpperCase + name.substring(1);
    }


    /**
     * Získání návratového datového typu metody. Návratový datový typ metody se vezme dle datového typu proměnné field.
     *
     * @param field
     *         - proměnná, ze které e vezme datový typ, který se přidá jako návratový datový typ metody.
     * @param isSetter
     *         - true v případě, že se jedná o setr, jinak false (getr).
     *
     * @return text, který bude obsahovat návratový datový typ metody (datový typ proměnné) a část názvu přístupové
     * metody.
     */
    private static String getReturnDataType(final Field field, final boolean isSetter) {
        /*
         * Zde se sestaví návratový datový typ metody - pokud se jedná o setr, pak to bude pouze "void". Jinak se
         * doplní konkrétní návratový datový typ metody.
         */
        if (isSetter)
            return "void set";

        else {
            /*
             * Zde se jedná o getr, tak musím zjistit datový typ, který má vracet a pokud je
             * to boolean, pak bude is, jinak get.
             */
            if (field.getType().equals(Boolean.class) || field.getType().equals(boolean.class))
                return ParameterToText.getFieldInText(field, false) + " is";
            else
                return ParameterToText.getFieldInText(field, false) + " get";
        }
    }


    /**
     * Přidání JavaDoc komentáře k metodě (getru nebo setru).
     *
     * @param fieldName
     *         - název proměnné. Je potřeba v případě, že se jedná o setr, protože se přidá jako název parametru
     *         metody.
     * @param isSetter
     *         - true v případě, že se jedná o setr, jinak false - jedná se o getr.
     * @param accessMethodBuilder
     *         - objekt, kam se přidávají texty (komentář). Jedná se o proměnnou, kde se sestavuje metoda.
     */
    private static void addJavaDocComment(final String fieldName, final boolean isSetter,
                                          final StringBuilder accessMethodBuilder) {
        accessMethodBuilder.append("\t/**\n");
        accessMethodBuilder.append("\t*\n");

        // Jestli se jedná o setr, tak bude mít parametr
        if (isSetter)
            accessMethodBuilder.append("\t* @param ").append(fieldName).append("\n");

            // Jinak je to getr, pak nemá parametr ale vrací nějakou hodnotu:
        else accessMethodBuilder.append("\t* @return \n");

        accessMethodBuilder.append("\t*/\n\t");
    }
}
