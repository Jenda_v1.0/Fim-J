package cz.uhk.fim.fimj.code_editor.combo_box_with_radio_button;

import cz.uhk.fim.fimj.code_editor.JToolBarForCodeEditor;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Třída, která slouží jako komponenta {@link JComboBox}, která zobrazuje jako položky komponenty typu JRadioButton. Ale
 * jedná se o komponentu {@link CmbItem}.
 *
 * Zdroj:
 *
 * http://www.java2s.com/Tutorials/Java/Swing_How_to/JComboBox/Add_JCheckBox_components_to_JComboBox.htm
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 01.09.2018 13:45
 */

public class CmbRadioButton extends JComboBox<CmbItem> {

    /**
     * Reference na toolbar pro dialog editor zdrojového kódu.
     *
     * <i>Je potřeba, když se klikne na jednu z položek v cmb, tak aby se změnilo nastaveni v editoru kódu.</i>
     */
    private final JToolBarForCodeEditor toolBarForCodeEditor;


    /**
     * Konstruktor třídy.
     *
     * @param aModel
     *         - hodnoty, které slouží jako model cmb. Jedná se o položky zobrazené v cmb.
     * @param toolBarForCodeEditor
     *         - reference na toolbar pro dialog editor zdrojového kódu. Je potřeba pouze v případě, že se nad ním má
     *         zavolat metoda když se klikne na jednu z položek v tomto cmb. Tak ty položka značí nějaké nastavení v
     *         editoru kódu, tak aby se to nastavení změnilo.
     */
    public CmbRadioButton(final CmbItem[] aModel, final JToolBarForCodeEditor toolBarForCodeEditor) {
        super(aModel);

        this.toolBarForCodeEditor = toolBarForCodeEditor;

        setRenderer(new RadioButtonRenderer());

        addActionListener(this);
    }


    @Override
    public void actionPerformed(final ActionEvent e) {
        // Podmínka by neměla být potřeba:
        if (!(e.getSource() instanceof JComboBox<?>))
            return;

        final JComboBox<?> comboBox = (JComboBox<?>) e.getSource();

        // V případě, že není označena položka, nemá smysl pokračovat:
        if (comboBox.getSelectedIndex() == -1)
            return;

        final CmbItem cmbItem = (CmbItem) comboBox.getSelectedItem();

        // Podmínka by neměla být potřeba, otestováné výše:
        if (cmbItem != null) {
            cmbItem.setSelected(!cmbItem.isSelected());

            // Změní se nastavení v editoru kódu:
            if (toolBarForCodeEditor != null)
                toolBarForCodeEditor.changeCodeEditorSettings(cmbItem);
        }
    }
}
