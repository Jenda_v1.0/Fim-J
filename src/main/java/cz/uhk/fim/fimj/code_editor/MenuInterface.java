package cz.uhk.fim.fimj.code_editor;

import cz.uhk.fim.fimj.code_editor_abstract.CodeEditorAbstract;

import java.io.File;

/**
 * Tato třída, resp. Interface slouží pro definici "společných" metod, které slouží pro některé ovládání textu. Resp.
 * metody, které umožňují manipulovat s textem z hlediska "programátorských pomůcek", jako jsou například zakomentování
 * kódu a jeho odkomentování dále zformátování kódu, odsazení textu tabulátorem, dále vložení ukázkové metody,
 * přístupové metody, ... více viz jednotlivé metody.
 * <p>
 * Toto rozhraní jsem napsal, kvůli těmto metodám, které jsou společné pro oba editory kódu, jak ten "klasický" editor
 * kodu, tak i ten v průzkuníku souborů, tak se budou volat stejné metody
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface MenuInterface {

    /**
     * Metoda, která slouží pro odsazení textu na řádku, kde se nachází kurzor v zadaném editoru, tak na daném řádku,
     * kde se nachází kurzor se odsadí text pomocí tabulátoru
     *
     * @param codeEditor
     *         - instance jednoho z editorů kodu, buď v klasickém editoru kodu, nebo jeden z editorů, který je v
     *         instancí nějakého vnitřního okna v průzkumníku souborů
     */
    void indentMoreText(final CodeEditorAbstract codeEditor);


    /**
     * Metoda, která na řádku, kde se nachází kurzor v editoru v parametru metody odebere odsazení, resp. odebere
     * tabulátor z daného řádku, pokud se na daném řádku již žádný nenachází, tak se nic neodebere, v podstatě žádná
     * reakce, na stisknutí tlačítka
     *
     * @param codeEditor
     *         - instance jednoho z editorů kodu, buď v klasickém editoru kodu, nebo jeden z editorů, který je v
     *         instancí nějakého vnitřního okna v průzkumníku souborů
     */
    void indentLessText(final CodeEditorAbstract codeEditor);


    /**
     * Metoda, která zakomentuje řádek v daném editoru kodu, resp. přidá na jeho začátek dve normální lomítka.
     *
     * @param codeEditor
     *         - instance jednoho z editorů kodu, buď v klasickém editoru kodu, nebo jeden z editorů, který je v
     *         instancí nějakého vnitřního okna v průzkumníku souborů
     */
    void commentText(final CodeEditorAbstract codeEditor);


    /**
     * Metoda, která odkomentuje řádek, na kterém se nachází kurzor, v jednom z editorů kódu. Pokaždé se odeberou dvě
     * normální lomítka na daném řádku, pokud nebudou žádná nalezena, nic se neděje
     *
     * @param codeEditor
     *         - instance jednoho z editorů kodu, buď v klasickém editoru kodu, nebo jeden z editorů, který je v
     *         instancí nějakého vnitřního okna v průzkumníku souborů
     */
    void uncommentText(final CodeEditorAbstract codeEditor);


    /**
     * Metoda, která zformátuje text v příslušném souboru - fileClassToFormat a načte tento zformátovaný text do
     * příslušného editoru - codeEditor
     *
     * @param codeEditor
     *         - instance jednoho z editorů kodu, buď v klasickém editoru kodu, nebo jeden z editorů, který je v
     *         instancí nějakého interního okna v průzkumníku souborů coby nějaký otevřený soubor
     * @param fileClassToFormat
     *         - proěnná typu File, která ukazuje na soubor, který se má zformátovat a tento zformátovaný köd načíst
     *         zpet do editoru, ve kterém byl otevřen
     */
    void formatText(final CodeEditorAbstract codeEditor, final File fileClassToFormat);


    /**
     * Metoda, která vloží do označeného editoru kódu ukázkovou metodu, která vrací číslo v parametru metody typu int -
     * včetně dokumentačního komentáře, jedná se pouze o ukázkovou metodu - protože se "jedná o aplikaci pro podporu
     * výuky"
     *
     * @param codeEditor
     *         - instance jednoho z editorů kodu, buď v klasickém editoru kodu, nebo jeden z editorů, který je v
     *         instancí nějakého interního okna v průzkumníku souborů coby nějaký otevřený soubor
     */
    void insertMethod(final CodeEditorAbstract codeEditor);


    /**
     * Metoda, která vloží do editoru codeEditor na pozici kurzoru výchozí serial version ID.
     *
     * @param codeEditor
     *         - instance jednoho z editorů kodu, buď v klasickém editoru kodu, nebo jeden z editorů, který je v
     *         instancí nějakého interního okna v průzkumníku souborů coby nějaký otevřený soubor
     */
    void insertDefaultSerialVersionId(final CodeEditorAbstract codeEditor);


    /**
     * Metoda, která vygeneruje serial version ID a vloží jej na pozici kurzoru v editour kodu codeEditor.
     *
     * @param clazz
     *         - Načtený soubor .class -> Jedná se o načtení zkompilované třídy ke které se má vytvořit serial version
     *         ID.
     * @param codeEditor
     *         - instance jednoho z editorů kodu, buď v klasickém editoru kodu, nebo jeden z editorů, který je v
     *         instancí nějakého interního okna v průzkumníku souborů coby nějaký otevřený soubor
     */
    void insertGeneratedSerialVersionId(final Class<?> clazz, final CodeEditorAbstract codeEditor);


    /**
     * Metoda, která v otevře dialog, ve kterém si uživatel zvolí, zda se má nahradit pouze první výskyt nebo všechny
     * výskyty daného slova, které uživatel zadá a také slovo, kterým se má původní nahradit, pak tyto zadané výrazy
     * nahradí v označeném editoru kodu
     *
     * @param codeEditor
     *         - instance jednoho z editorů kodu, buď v klasickém editoru kodu, nebo jeden z editorů, který je v
     *         instancí nějakého interního okna v průzkumníku souborů coby nějaký otevřený soubor
     */
    void replaceText(final CodeEditorAbstract codeEditor);


    /**
     * Metoda, která zobrazí dialog pro zadání čísla řádku, kam se má přesunout kurzor, a pokud jej uživatel zadá ve
     * správném formátu a intervalu (počet řádků 0 - X), a klikne v dialogu na tlačítko OK, tak se kurzor v označeném
     * eidtoru přesune na začátek zadaného čísla řádku
     *
     * @param codeEditor
     *         - instance jednoho z editorů kodu, buď v klasickém editoru kodu, nebo jeden z editorů, který je v
     *         instancí nějakého interního okna v průzkumníku souborů coby nějaký otevřený soubor
     */
    void goToLine(final CodeEditorAbstract codeEditor);


    /**
     * Metoda, která vloží na pozici kurzoru v otevřeném či označeném editoru kodu tak na tuto pozici kurzoru vloží
     * metodu toString()
     *
     * @param codeEditor
     *         - instance jednoho z editorů kodu, buď v klasickém editoru kodu, nebo jeden z editorů, který je v
     *         instancí nějakého interního okna v průzkumníku souborů coby nějaký otevřený soubor
     */
    void toStringMethod(final CodeEditorAbstract codeEditor);


    /**
     * Metoda, která zobrazí dialog, do kterého se předá zkompilovaná třída, resp. její soubor .class a tento dialog si
     * zjistí všechny proměnné, kter kterým lze vygenerovat přístupové metody a dle toho které uživatel označeí, se pak
     * na pozici kurzoru myšiv označeném editoru kodu vloží označené příístupové metody
     *
     * @param codeEditor
     *         - instance jednoho z editorů kodu, buď v klasickém editoru kodu, nebo jeden z editorů, který je v
     *         instancí nějakého interního okna v průzkumníku souborů coby nějaký otevřený soubor
     * @param clazz
     *         - načtený soubor .class označené a bez chyby zkomilované - přeloženeé označené třídy, ze kterého se
     *         načtou příslušné proměnné, ke kterým je možné vygenerovat zmíněné přístupové metody (getry a setry)
     */
    void generateGettersAndSetters(final CodeEditorAbstract codeEditor, final Class<?> clazz);


    /**
     * Metoda, která zobrazí dialog, do kterého se předá parametr clazz, a v tomto dialogu si uživatel bude moci vybrat
     * proěmnné, které se mají vygenerovat, resp. přidat do parametru konstruktoru příslušné třídy
     *
     * @param codeEditor
     *         - instance jednoho z editorů kodu, buď v klasickém editoru kodu, nebo jeden z editorů, který je v
     *         instancí nějakého interního okna v průzkumníku souborů coby nějaký otevřený soubor
     * @param clazz
     *         - soubor .class příslušné zkomplované -přeloženeé třídy
     */
    void generateConstructor(final CodeEditorAbstract codeEditor, final Class<?> clazz);
}