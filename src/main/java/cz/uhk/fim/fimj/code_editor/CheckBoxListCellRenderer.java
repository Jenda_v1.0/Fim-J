package cz.uhk.fim.fimj.code_editor;

import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * Tato třída slouží jako Renderer pro vykreslování JCheckBoxů do Jlist komponenty (včetně jejích textů, zda jsou
 * zaškrtnuté, barva pozadí, ...)
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class CheckBoxListCellRenderer extends JCheckBox implements ListCellRenderer<FieldAndCheckBox> {

    private static final long serialVersionUID = 1L;

    @Override
    public Component getListCellRendererComponent(JList<? extends FieldAndCheckBox> list, FieldAndCheckBox value,
                                                  int index, boolean isSelected, boolean cellHasFocus) {

        setComponentOrientation(list.getComponentOrientation());
        setFont(list.getFont());
        setBackground(list.getBackground());
        setForeground(list.getForeground());
        setSelected(value.isChcbSelected());
        setEnabled(list.isEnabled());

        setText(value.getFieldName());

        return this;
    }
}