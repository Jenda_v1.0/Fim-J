package cz.uhk.fim.fimj.code_editor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.List;
import java.util.Properties;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.DesktopSupport;
import cz.uhk.fim.fimj.file.GetIconInterface;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.print.Printer;
import cz.uhk.fim.fimj.project_explorer.ReadWriteFile;
import cz.uhk.fim.fimj.project_explorer.ReadWriteFileInterface;

/**
 * Tato třída slouží jako menu do dialogu pro editaci zdrojového kódu třídy zvolené v diagramu tříd
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class JMenuForCodeEditor extends JMenuBar implements LanguageInterface, ActionListener {
	
	private static final long serialVersionUID = 1L;


	/**
	 * "komponenta", která obsahuje "sdílené" metody pro menu, metody pro manipulaci
	 * s textem jako pomůcky pro programování
	 */
	private static MenuInterface menuMethods;
	
	
	
	
	
	// Hlavní menu pro jednotlivé položky:
	private JMenu menuClass, menuEdit, menuTools;
	
	// Položky do menu: menuClass:
	private JMenuItem itemSave, itemReload, itemPrint, itemClose;
	
	// Položky do menu: menuEdit:
	private JMenuItem itemCut, itemCopy, itemPaste, itemIndentMore, itemIndentLess, itemComment, itemUncomment,
			itemFormatCode, itemInsertMethod;
		
	// Položky do menu: menuTools:
	private JMenuItem itemReplaceText, itemGoToLine, itemToString, itemGettersAndSetters, itemConstructor, itemCompile;


    /**
     * Menu, které obsahuje položky pro otevření otevřeného souboru v dialogu v průzkumníku projektů nebo ve výchozí
     * aplikaci definovanou v OS - aplikace pro otevřené konkrétního typu souboru.
     */
    private JMenu menuOpenFileInAppsOfOs;

    /**
     * Otevření souboru v průzkumníku souborů v OS. V nastaveném průzkumníku souboru v příslušném OS se otevře / najde
     * soubor, který je otevřen v tomto dialogu.
     */
    private JMenuItem itemOpenFileInFileExplorerInOs;
    /**
     * Otevření souboru ve výchozí aplikaci v OS. Například když je v tomto dialogu otevřen textový soubor (.txt), pak
     * jako výchozí aplikace v OS může být například notepad pro otevření tohoto typu souboru apod.
     */
    private JMenuItem itemOpenFileInDefaultAppInOs;





	/**
	 * Tato komponenta slouží jako menu pro položky pro vygenerování nebo přidání
	 * výchozího ID pro seriazlizaci objektů.
	 */
	private JMenu menuAddSerialVersionID;



    /**
     * Přidání výchozího serial ID
     */
    private JMenuItem itemAddDefaultSerialVersionID;
    /**
     * Vygenerování serial ID.
     */
    private JMenuItem itemAddGeneratedSerialVersionId;
	
	
	
	
	
	/**
	 * Reference na dialog, protože potřebuji získávat data z tohodo dialogu
	 * například text kodu, zavreni dialogu, ... viz tlačítkaníže
	 */
	private final CodeEditorDialog codeEditorDialog;
	
	
	
	
	/**
	 * Proměnná pro práci se zdrojovým kódem třídy - načítání a ukládání:
	 */
	private static final ReadWriteFileInterface editCodeOfClass = new ReadWriteFile();
	
		
	
	
	// Proměnné pro uložení hodnoty pro text v chybových hláškách:
	private String txtClassNotFoundText, txtClassNotFoundTitle, txtPath, txtSaveChangesText, txtSaveChangesTitle,
			txtErrorWhileSavingChangesToClassText_1, txtErrorWhileSavingChangesToClassText_2,
			txtErrorWhileSavingChangesToClassTitle, txtCouldNotLoadClassFile, txtClass, txtFileNotFound,
			txtFileDoesNotExistInPathText, txtFileDoesNotExistInPathTitle;
	
		
	
	
	/**
	 * Třída pro "společné operace" mezi metu a lištou Toolbar v dialogu - editoru
	 * kodu:
	 */
	private final transient SharedClassForMethodsInterface sharedClass;
	
	
	
	
	/**
	 * Proměnná pro referenci na soubor s načteným jazykem pro texty v aplikaci:
	 */
	private final Properties languageProperties;
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param codeEditorDialog
	 *            - dialog s editorem zdrojovém kódu, ve kterém se má otevřít nějaký
	 *            soubor / třídy.
	 * 
	 * @param sharedClass
	 *            - reference pro rozhraní se sdílenými metodami.
	 * 
	 * @param languageProperties
	 *            - objekt, který obsahuje texty pro tuto aplikaci ve zvoleném
	 *            jazyce.
	 */
	public JMenuForCodeEditor(final CodeEditorDialog codeEditorDialog, final SharedClassForMethodsInterface sharedClass,
			final Properties languageProperties) {
		super();
		
		this.codeEditorDialog = codeEditorDialog;
		this.sharedClass = sharedClass;
		this.languageProperties = languageProperties;
		
		menuMethods = new MenuSharedMethods(languageProperties);
		
		
		createAndAddMenuItem();
		
		// přidání klávesových zkratek:
		addKeyStrokeToMenuItems();
		
		
		setLanguage(languageProperties);
	}



	
	/**
	 * Metoda, která vytvoři jednotlivé menu a položky do nich dále příslušné
	 * položky přidá do menu, kam patří
	 */
	private void createAndAddMenuItem() {
		// Vytvoření intance pro "hlavní menu":
		menuClass = new JMenu();
		menuEdit = new JMenu();
		menuTools = new JMenu();
		
		// Přidání do hlavního menu.
		add(menuClass);
		add(menuEdit);
		add(menuTools);
		



		// Inicializace položek do menu pro otevření v jedné z aplikací OS:
        menuOpenFileInAppsOfOs = new JMenu();

        itemOpenFileInFileExplorerInOs = new JMenuItem();
        itemOpenFileInDefaultAppInOs = new JMenuItem();

        itemOpenFileInFileExplorerInOs.addActionListener(this);
        itemOpenFileInDefaultAppInOs.addActionListener(this);

        menuOpenFileInAppsOfOs.add(itemOpenFileInFileExplorerInOs);
        menuOpenFileInAppsOfOs.addSeparator();
        menuOpenFileInAppsOfOs.add(itemOpenFileInDefaultAppInOs);




                // Instance položek do menuClass:
		itemSave = new JMenuItem();
		itemReload = new JMenuItem();
		itemPrint = new JMenuItem();
		itemClose = new JMenuItem();
		
		
		// Přidání reakce na kliknutí na danou položku:
		itemSave.addActionListener(this);
		itemReload.addActionListener(this);
		itemPrint.addActionListener(this);
		itemClose.addActionListener(this);
		
		
		// Přidání vytvořených položek do menuClass:
		menuClass.add(itemSave);
		menuClass.add(itemReload);
		menuClass.addSeparator();
		menuClass.add(menuOpenFileInAppsOfOs);
        menuClass.addSeparator();
		menuClass.add(itemPrint);
		menuClass.addSeparator();
		menuClass.add(itemClose);
		

		
		
		
		
		// Instance položek do menuEdit:
		itemCut = new JMenuItem();
		itemCopy = new JMenuItem();
		itemPaste = new JMenuItem();
		itemIndentMore = new JMenuItem();
		itemIndentLess = new JMenuItem();
		itemComment = new JMenuItem();
		itemUncomment = new JMenuItem();
		itemFormatCode = new JMenuItem();
		itemInsertMethod = new JMenuItem();
		
		// Přidání reakce na kliknutí na danou položku:
		itemCut.addActionListener(this);
		itemCopy.addActionListener(this);
		itemPaste.addActionListener(this);
		itemIndentMore.addActionListener(this);
		itemIndentLess.addActionListener(this);
		itemComment.addActionListener(this);
		itemUncomment.addActionListener(this);
		itemFormatCode.addActionListener(this);
		itemInsertMethod.addActionListener(this);
		
		
		// Přidání vytvořených položek do menuEdit:
		menuEdit.add(itemCut);
		menuEdit.add(itemCopy);
		menuEdit.add(itemPaste);
		menuEdit.addSeparator();
		menuEdit.add(itemIndentMore);
		menuEdit.add(itemIndentLess);
		menuEdit.addSeparator();
		menuEdit.add(itemComment);
		menuEdit.add(itemUncomment);
		menuEdit.addSeparator();
		menuEdit.add(itemFormatCode);
		menuEdit.addSeparator();
		menuEdit.add(itemInsertMethod);
		
		
		
		
		
		
		
		
		
		
		// Instance položek do menuTools:
		
		// Menu pro vygenerování nebo přidání ID pro seralizaci:
		menuAddSerialVersionID = new JMenu();
				
		itemAddDefaultSerialVersionID = new JMenuItem();
		itemAddGeneratedSerialVersionId = new JMenuItem();
		
		itemAddDefaultSerialVersionID.addActionListener(this);
		itemAddGeneratedSerialVersionId.addActionListener(this);
		
		menuAddSerialVersionID.add(itemAddDefaultSerialVersionID);
		menuAddSerialVersionID.addSeparator();
		menuAddSerialVersionID.add(itemAddGeneratedSerialVersionId);
		
		
		
		// Položky do menu menuTools:
		itemReplaceText = new JMenuItem();
		itemGoToLine = new JMenuItem();
		itemCompile = new JMenuItem();
		itemToString = new JMenuItem();
		itemGettersAndSetters = new JMenuItem();
		itemConstructor = new JMenuItem();
	
		itemReplaceText.addActionListener(this);
		itemGoToLine.addActionListener(this);
		itemCompile.addActionListener(this);
		itemToString.addActionListener(this);
		itemGettersAndSetters.addActionListener(this);
		itemConstructor.addActionListener(this);
		
		// Přidání vytvořených položek do menuTools:
		menuTools.add(itemReplaceText);
		menuTools.add(itemGoToLine);
		menuTools.addSeparator();
		menuTools.add(itemToString);
		menuTools.add(itemGettersAndSetters);
		menuTools.add(itemConstructor);
		menuTools.addSeparator();
		menuTools.add(menuAddSerialVersionID);
		menuTools.addSeparator();
		menuTools.add(itemCompile);
		
		
		
		
		
		addIconToItems();
	}
	
	
	
	
	
	
	/**
	 * Metoda, která každému tlačítku v menu nastaví ikonu
	 */
	private void addIconToItems() {
		// Ikony v menuClass:
        itemSave.setIcon(GetIconInterface.getMyIcon("/icons/app/SaveIcon.png", true));
        itemReload.setIcon(GetIconInterface.getMyIcon("/icons/app/ReloadIcon.png", true));
        itemOpenFileInFileExplorerInOs.setIcon(GetIconInterface.getMyIcon("/icons/app/FileExplorerIcon.png", true));
        itemOpenFileInDefaultAppInOs.setIcon(GetIconInterface.getMyIcon("/icons/app/DefaultAppIcon.png", true));
        itemPrint.setIcon(GetIconInterface.getMyIcon("/icons/app/PrintIcon.png", true));
        itemClose.setIcon(GetIconInterface.getMyIcon("/icons/app/CloseIcon.png", true));
		
		
		// Ikony v menuEdit:
		itemCut.setIcon(GetIconInterface.getMyIcon("/icons/app/CutIcon.jpg", true));
		itemCopy.setIcon(GetIconInterface.getMyIcon("/icons/app/CopyIcon.png", true));
		itemPaste.setIcon(GetIconInterface.getMyIcon("/icons/app/PasteIcon.png", true));
		itemIndentMore.setIcon(GetIconInterface.getMyIcon("/icons/app/IndentMoreIcon.png", true));
		itemIndentLess.setIcon(GetIconInterface.getMyIcon("/icons/app/IndentLessIcon.png", true));
		itemComment.setIcon(GetIconInterface.getMyIcon("/icons/app/CommentIcon.jpg", true));
		itemUncomment.setIcon(GetIconInterface.getMyIcon("/icons/app/UncommentIcon.jpg", true));
		itemFormatCode.setIcon(GetIconInterface.getMyIcon("/icons/app/FormatCodeIcon.png", true));
		itemInsertMethod.setIcon(GetIconInterface.getMyIcon("/icons/app/InsertMethodIcon.png", true));
		
		
		
		// Ikony v Tools:
		itemReplaceText.setIcon(GetIconInterface.getMyIcon("/icons/app/FindAndReplaceIcon.png", true));
		itemGoToLine.setIcon(GetIconInterface.getMyIcon("/icons/app/GoToLineIcon.jpeg", true));
		itemToString.setIcon(GetIconInterface.getMyIcon("/icons/app/ToStringIcon.png", true));
		itemGettersAndSetters.setIcon(GetIconInterface.getMyIcon("/icons/app/GettersAndSettersIcon.png", true));
		itemConstructor.setIcon(GetIconInterface.getMyIcon("/icons/app/ConstructorIcon.png", true));
		itemCompile.setIcon(GetIconInterface.getMyIcon("/icons/app/CompileIcon.png", true));
		
		itemAddDefaultSerialVersionID.setIcon(GetIconInterface.getMyIcon("/icons/app/GenerateSeriaVersionIdIcon.png", true));
		itemAddGeneratedSerialVersionId.setIcon(GetIconInterface.getMyIcon("/icons/app/GenerateSeriaVersionIdIcon.png", true));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá klávesové zkratky jednotlivým položkám v menu
	 */
	private void addKeyStrokeToMenuItems() {
		// Zkratky do menuClass:
		itemSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
		itemReload.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_DOWN_MASK));
		itemPrint.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_DOWN_MASK));
		itemClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_DOWN_MASK));
		
		
		
		
		itemIndentMore.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
		itemIndentLess.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_DOWN_MASK));
		itemComment.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_DOWN_MASK));
		itemUncomment.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, InputEvent.CTRL_DOWN_MASK));
		
		itemFormatCode.setAccelerator(KeyStroke.getKeyStroke("control shift F"));
		itemInsertMethod.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, InputEvent.CTRL_DOWN_MASK));
		
		
		
		
		// Zkratky pro položky do menuTools:
		itemReplaceText.setAccelerator(KeyStroke.getKeyStroke("control shift R"));
		itemGoToLine.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_DOWN_MASK));
		
//		itemCompile.setAccelerator(KeyStroke.getKeyStroke("control shift C"));
		itemCompile.setAccelerator(KeyStroke.getKeyStroke("shift F6"));
	}
	
	
	
	
	
	
	

	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			
			// Nastavení textu do "hlavních menu."
			menuClass.setText(properties.getProperty("Ced_Mfce_MenuClass", Constants.MFCE_MENU_CLASS));
			menuEdit.setText(properties.getProperty("Ced_Mfce_MenuEdit", Constants.MFCE_MENU_EDIT));
			menuTools.setText(properties.getProperty("Ced_Mfce_MenuTools", Constants.MFCE_MENU_TOOLS));

			// Nastavení textů do položek v rozevíracím menu Otevřít v menu Třída:
            menuOpenFileInAppsOfOs.setText(properties.getProperty("Ced_Mfce_MenuClass_MenuOpenFileInOs", Constants.MFCE_MENU_CLASS_MENU_OPEN_FILE_IN_OS));


			// Nastaveni textu do pložek do menuClass:
			itemSave.setText(properties.getProperty("Ced_Mfce_MenuClassItemSave", Constants.MFCE_MENU_CLASS_ITEM_SAVE));
			itemReload.setText(properties.getProperty("Ced_Mfce_MenuClassItemReload", Constants.MFCE_MENU_CLASS_ITEM_RELOAD));
			itemPrint.setText(properties.getProperty("Ced_Mfce_MenuClassItemPrint", Constants.MFCE_MENU_CLASS_ITEM_PRINT));
			itemClose.setText(properties.getProperty("Ced_Mfce_MenuClassItemClose", Constants.MFCE_MENU_CLASS_ITEM_CLOSE));

			// Nastavení textů do položek v menu menuOpenFileInAppsOfOs:
            itemOpenFileInFileExplorerInOs.setText(properties.getProperty("Ced_Mfce_ItemOpenFileInFileExplorer", Constants.MFCE_ITEM_OPEN_FILE_IN_FILE_EXPLORER));
            itemOpenFileInDefaultAppInOs.setText(properties.getProperty("Ced_Mfce_ItemOpenFileInDefaultAppInOs", Constants.MFCE_ITEM_OPEN_FILE_IN_DEFAULT_APP_IN_OS));

			
			// Nastaveni popisků do pložek do menuClass:
			itemSave.setToolTipText(properties.getProperty("Ced_Mfce_MenuClassToolTipItemSave", Constants.MFCE_TOOL_TIP_ITEM_SAVE));
			itemReload.setToolTipText(properties.getProperty("Ced_Mfce_MenuClassToolTipItemReload", Constants.MFCE_TOOL_TIP_ITEM_RELOAD));
			itemPrint.setToolTipText(properties.getProperty("Ced_Mfce_MenuClassToolTipItemPrint", Constants.MFCE_TOOL_TIP_ITEM_PRINT));
			itemClose.setToolTipText(properties.getProperty("Ced_Mfce_MenuClassToolTipItemClose", Constants.MFCE_TOOL_TIP_ITEM_CLOSE));

            // Nastavení popisků do položek v menu menuOpenFileInAppsOfOs:
            itemOpenFileInFileExplorerInOs.setToolTipText(properties.getProperty("Ced_Mfce_ToolTipItemOpenFileInFileExplorerInOs", Constants.MFCE_TOOL_TIP_ITEM_OPEN_FILE_IN_FILE_EXPLORER_IN_OS));
            itemOpenFileInDefaultAppInOs.setToolTipText(properties.getProperty("Ced_Mfce_ToolTipItemOpenFileInDefaultAppInOs", Constants.MFCE_TOOL_TIP_ITEM_OPEN_FILE_IN_DEFAULT_APP_IN_OS));
			
			
			
			// Nastaveni textu do pložek do menuEdit:
			itemCut.setText(properties.getProperty("Ced_Mfce_MenuEditItemCut", Constants.MFCE_MENU_EDIT_ITEM_CUT));
			itemCopy.setText(properties.getProperty("Ced_Mfce_MenuEditItemCopy", Constants.MFCE_MENU_EDIT_ITEM_COPY));
			itemPaste.setText(properties.getProperty("Ced_Mfce_MenuEditItemPaste", Constants.MFCE_MENU_EDIT_ITEM_PASTE));
			itemIndentMore.setText(properties.getProperty("Ced_Mfce_MenuEditItemIndentMore", Constants.MFCE_MENU_EDIT_ITEM_INDENT_MORE));
			itemIndentLess.setText(properties.getProperty("Ced_Mfce_MenuEditItemIndentLess", Constants.MFCE_MENU_EDIT_ITEM_INDENT_LESS));
			itemComment.setText(properties.getProperty("Ced_Mfce_MenuEditItemComment", Constants.MFCE_MENU_EDIT_ITEM_COMMENT));
			itemUncomment.setText(properties.getProperty("Ced_Mfce_MenuEditItemUncomment", Constants.MFCE_MENU_EDIT_ITEM_UNCOMMENT));
			itemFormatCode.setText(properties.getProperty("Ced_Mfce_MenuEditItemFormatCode", Constants.MFCE_MENU_EDIT_ITEM_FORMAT_CODE));
			itemInsertMethod.setText(properties.getProperty("Ced_Mfce_MenuEditItemInsertMethod", Constants.MFCE_MENU_EDIT_ITEM_INSERT_METHOD));
			
			
			
			
			// Nastaveni popisků do pložek do menuEdit:
			itemCut.setToolTipText(properties.getProperty("Ced_Mfce_MenuEditToolTipItemCut", Constants.MFCE_TOOL_TIP_ITEM_CUT));
			itemCopy.setToolTipText(properties.getProperty("Ced_Mfce_MenuEditToolTipItemCopy", Constants.MFCE_TOOL_TIP_ITEM_COPY));
			itemPaste.setToolTipText(properties.getProperty("Ced_Mfce_MenuEditToolTipItemPaste", Constants.MFCE_TOOL_TIP_ITEM_PASTE));
			itemIndentMore.setToolTipText(properties.getProperty("Ced_Mfce_MenuEditToolTipItemIndentMore", Constants.MFCE_TOOL_TIP_ITEM_INDENT_MORE));
			itemIndentLess.setToolTipText(properties.getProperty("Ced_Mfce_MenuEditToolTipItemIndentLess", Constants.MFCE_TOOL_TIP_ITEM_INDENT_LESS));
			itemComment.setToolTipText(properties.getProperty("Ced_Mfce_MenuEditToolTipItemComment", Constants.MFCE_TOOL_TIP_ITEM_COMMENT));
			itemUncomment.setToolTipText(properties.getProperty("Ced_Mfce_MenuEditToolTipItemUncomment", Constants.MFCE_TOOL_TIP_ITEM_UNCOMMENT));
			itemFormatCode.setToolTipText(properties.getProperty("Ced_Mfce_MenuEditToolTipItemFormatCode", Constants.MFCE_TOOL_TIP_ITEM_FORMAT_CODE));
			itemInsertMethod.setToolTipText(properties.getProperty("Ced_Mfce_MenuEditToolTipItemInsertMethod", Constants.MFCE_TOOL_TIP_ITEM_INSERT_METHOD));
			
			

			

			
			// Nastaveni textu do ppložek do menuTools:
			itemReplaceText.setText(properties.getProperty("Ced_Mfce_MenuToolsItemReplace", Constants.MFCE_MENU_TOOLS_ITEM_REPLACE));
			itemGoToLine.setText(properties.getProperty("Ced_Mfce_MenuToolsItemGoToLine", Constants.MFCE_MENU_TOOLS_ITEM_GO_TO_LINE));
			itemCompile.setText(properties.getProperty("Ced_Mfce_MenuToolsItemCompile", Constants.MFCE_MENU_TOOLS_ITEM_COMPILE));
			itemGettersAndSetters.setText(properties.getProperty("Ced_Mfce_MenuToolsItemAccessMethods", Constants.MFCE_MENU_TOOLS_ITEM_GETTERS_AND_SETTERS));
			itemConstructor.setText(properties.getProperty("Ced_Mfce_MenuToolsItemConstructors", Constants.MFCE_MENU_TOOLS_ITEM_CONSTRUCTOR));
			menuAddSerialVersionID.setText(properties.getProperty("Ced_Mfce_MenuToolsMenuAddSerialID", Constants.MFCE_MENU_TOOLS_MENU_ADD_SERIAL_ID));
			itemAddDefaultSerialVersionID.setText(properties.getProperty("Ced_Mfce_MenuToolsItemAddDefaultSerialID", Constants.MFCE_MENU_TOOLS_ITEM_ADD_DEFAULT_SERIAL_UID));
			itemAddGeneratedSerialVersionId.setText(properties.getProperty("Ced_Mfce_MenuToolsItemAddGeneratedSerialID", Constants.MFCE_MENU_TOOLS_ITEM_ADD_GENERATED_SERIAL_UID));
			
			
			// Nastaveni popisků do pložek do menuTools:
			itemReplaceText.setToolTipText(properties.getProperty("Ced_Mfce_MenuToolsToolTipItemReplace", Constants.MFCE_TOOL_TIP_ITEM_REPLACE));
			itemGoToLine.setToolTipText(properties.getProperty("Ced_Mfce_MenuToolsToolTipItemGoToLine", Constants.MFCE_TOOL_TIP_ITEM_GO_TO_LINE));
			itemCompile.setToolTipText(properties.getProperty("Ced_Mfce_MenuToolsToolTipItemCompile", Constants.MFCE_TOOL_TIP_ITEM_COMPILE));
			itemToString.setToolTipText(properties.getProperty("Ced_Mfce_MenuToolsToolTipItemToString", Constants.MFCE_TOOL_TIP_ITEM_TO_STRING));
			itemGettersAndSetters.setToolTipText(properties.getProperty("Ced_Mfce_MenuToolsToolTipItemAccessMethods", Constants.MFCE_TOOL_TIP_ITEM_GETTERS_AND_SETTERS));
			itemConstructor.setToolTipText(properties.getProperty("Ced_Mfce_MenuToolsToolTipItemConstructor", Constants.MFCE_TOOL_TIP_ITEM_CONSTRUCTOR));			
			itemAddDefaultSerialVersionID.setToolTipText(properties.getProperty("Ced_Mfce_MenuToolsToolTipItemAddDefaultSerialId", Constants.MFCE_TOOL_TIP_ITEM_ADD_DEFAULT_SERIAL_UID));
			itemAddGeneratedSerialVersionId.setToolTipText(properties.getProperty("Ced_Mfce_MenuToolsToolTipItemAddGeneratedSerialId", Constants.MFCE_TOOL_TIP_ITEM_ADD_GENERATED_SERIAL_UID));
			
			
			
			
			
			txtClassNotFoundText = properties.getProperty("Ced_Mfce_JopClassNotFoundText", Constants.NFCE_JOP_CLASS_NOT_FOUND_TEXT);
			txtClassNotFoundTitle = properties.getProperty("Ced_Mfce_JopClassNotFoundTitle", Constants.NFCE_JOP_CLASS_NOT_FOUND_TITLE);
			txtPath = properties.getProperty("Ced_Mfce_TextPath", Constants.NFCE_TXT_PATH);
			txtSaveChangesText = properties.getProperty("Ced_Mfce_JopSaveChangesText", Constants.NFCE_JOP_SAVE_CHANGES_TEXT);
			txtSaveChangesTitle = properties.getProperty("Ced_Mfce_JopSaveChangesTitle", Constants.NFCE_JOP_SAVE_CHANGES_TITLE); 
			txtErrorWhileSavingChangesToClassText_1 = properties.getProperty("Ced_Mfce_JopErrorWhileSavingChangesToClassText_1", Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TEXT_1);
			txtErrorWhileSavingChangesToClassText_2 = properties.getProperty("Ced_Mfce_JopErrorWhileSavingChangesToClassText_2", Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TEXT_2); 
			txtErrorWhileSavingChangesToClassTitle = properties.getProperty("Ced_Mfce_JopErrorWhileSavingChangesToClassTitle", Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TITLE);
			txtCouldNotLoadClassFile = properties.getProperty("Ced_Mfce_TextCouldNotLoadClassFile", Constants.NFCE_TXT_COULD_NOT_LOAD_CLASS_FILE); 
			txtClass = properties.getProperty("Ced_Mfce_TextClass", Constants.NFCE_TXT_CLASS);
			txtFileNotFound = properties.getProperty("Ced_Mfce_TextFileNotFound", Constants.NFCE_TXT_FILE_NOT_FOUND);
			
			txtFileDoesNotExistInPathText = properties.getProperty("Ced_Mfce_FileDoesNotExistInPathText", Constants.MFCE_FILE_DOES_NOT_EXIST_IN_PATH_TEXT);
			txtFileDoesNotExistInPathTitle = properties.getProperty("Ced_Mfce_FileDoesNotExistInPathTitle", Constants.MFCE_FILE_DOES_NOT_EXIST_IN_PATH_TITLE);
		}
		
		
		else {
			// Nastavení textu do "hlavních menu."
			menuClass.setText(Constants.MFCE_MENU_CLASS);
			menuEdit.setText(Constants.MFCE_MENU_EDIT);
			menuTools.setText(Constants.MFCE_MENU_TOOLS);

            // Nastavení textů do položek v rozevíracím menu Otevřít v menu Třída:
            menuOpenFileInAppsOfOs.setText(Constants.MFCE_MENU_CLASS_MENU_OPEN_FILE_IN_OS);
			
			
			
			// Nastaveni textu do pložek do menuClass:
			itemSave.setText(Constants.MFCE_MENU_CLASS_ITEM_SAVE);
			itemReload.setText(Constants.MFCE_MENU_CLASS_ITEM_RELOAD);
			itemPrint.setText(Constants.MFCE_MENU_CLASS_ITEM_PRINT);
			itemClose.setText(Constants.MFCE_MENU_CLASS_ITEM_CLOSE);

            // Nastavení textů do položek v menu menuOpenFileInAppsOfOs:
            itemOpenFileInFileExplorerInOs.setText(Constants.MFCE_ITEM_OPEN_FILE_IN_FILE_EXPLORER);
            itemOpenFileInDefaultAppInOs.setText(Constants.MFCE_ITEM_OPEN_FILE_IN_DEFAULT_APP_IN_OS);
			
			
			// Nastaveni popisků do pložek do menuClass:
			itemSave.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_SAVE);
			itemReload.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_RELOAD);
			itemPrint.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_PRINT);
			itemClose.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_CLOSE);

            // Nastavení popisků do položek v menu menuOpenFileInAppsOfOs:
            itemOpenFileInFileExplorerInOs.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_OPEN_FILE_IN_FILE_EXPLORER_IN_OS);
            itemOpenFileInDefaultAppInOs.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_OPEN_FILE_IN_DEFAULT_APP_IN_OS);
			
			
			
			// Nastaveni textu do pložek do menuEdit:
			itemCut.setText(Constants.MFCE_MENU_EDIT_ITEM_CUT);
			itemCopy.setText(Constants.MFCE_MENU_EDIT_ITEM_COPY);
			itemPaste.setText(Constants.MFCE_MENU_EDIT_ITEM_PASTE);
			itemIndentMore.setText(Constants.MFCE_MENU_EDIT_ITEM_INDENT_MORE);
			itemIndentLess.setText(Constants.MFCE_MENU_EDIT_ITEM_INDENT_LESS);
			itemComment.setText(Constants.MFCE_MENU_EDIT_ITEM_COMMENT);
			itemUncomment.setText(Constants.MFCE_MENU_EDIT_ITEM_UNCOMMENT);
			itemFormatCode.setText(Constants.MFCE_MENU_EDIT_ITEM_FORMAT_CODE);
			itemInsertMethod.setText(Constants.MFCE_MENU_EDIT_ITEM_INSERT_METHOD);
			
			
			
			
			// Nastaveni popisků do pložek do menuEdit:
			itemCut.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_CUT);
			itemCopy.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_COPY);
			itemPaste.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_PASTE);
			itemIndentMore.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_INDENT_MORE);
			itemIndentLess.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_INDENT_LESS);
			itemComment.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_COMMENT);
			itemUncomment.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_UNCOMMENT);
			itemFormatCode.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_FORMAT_CODE);
			itemInsertMethod.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_INSERT_METHOD);
			
			
			
			
			
			// Nastaveni textu do ppložek do menuTools:
			itemReplaceText.setText(Constants.MFCE_MENU_TOOLS_ITEM_REPLACE);
			itemGoToLine.setText(Constants.MFCE_MENU_TOOLS_ITEM_GO_TO_LINE);
			itemCompile.setText(Constants.MFCE_MENU_TOOLS_ITEM_COMPILE);			
			itemGettersAndSetters.setText( Constants.MFCE_MENU_TOOLS_ITEM_GETTERS_AND_SETTERS);
			itemConstructor.setText(Constants.MFCE_MENU_TOOLS_ITEM_CONSTRUCTOR);
			menuAddSerialVersionID.setText(Constants.MFCE_MENU_TOOLS_MENU_ADD_SERIAL_ID);
			itemAddDefaultSerialVersionID.setText(Constants.MFCE_MENU_TOOLS_ITEM_ADD_DEFAULT_SERIAL_UID);
			itemAddGeneratedSerialVersionId.setText(Constants.MFCE_MENU_TOOLS_ITEM_ADD_GENERATED_SERIAL_UID);
			
			
			
			// Nastaveni popisků do pložek do menuTools:
			itemReplaceText.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_REPLACE);
			itemGoToLine.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_GO_TO_LINE);
			itemCompile.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_COMPILE);
			itemToString.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_TO_STRING);
			itemGettersAndSetters.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_GETTERS_AND_SETTERS);
			itemConstructor.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_CONSTRUCTOR);
			itemAddDefaultSerialVersionID.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_ADD_DEFAULT_SERIAL_UID);
			itemAddGeneratedSerialVersionId.setToolTipText(Constants.MFCE_TOOL_TIP_ITEM_ADD_GENERATED_SERIAL_UID);
			
												
			txtClassNotFoundText = Constants.NFCE_JOP_CLASS_NOT_FOUND_TEXT;
			txtClassNotFoundTitle = Constants.NFCE_JOP_CLASS_NOT_FOUND_TITLE;
			txtPath = Constants.NFCE_TXT_PATH;
			txtSaveChangesText = Constants.NFCE_JOP_SAVE_CHANGES_TEXT;
			txtSaveChangesTitle =  Constants.NFCE_JOP_SAVE_CHANGES_TITLE; 
			txtErrorWhileSavingChangesToClassText_1 = Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TEXT_1;
			txtErrorWhileSavingChangesToClassText_2 = Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TEXT_2; 
			txtErrorWhileSavingChangesToClassTitle = Constants.NFCE_JOP_ERROR_WHILE_SAVING_CHANGES_TO_CLASS_TITLE;
			txtCouldNotLoadClassFile = Constants.NFCE_TXT_COULD_NOT_LOAD_CLASS_FILE; 
			txtClass = Constants.NFCE_TXT_CLASS;
			txtFileNotFound = Constants.NFCE_TXT_FILE_NOT_FOUND;
			
			txtFileDoesNotExistInPathText = Constants.MFCE_FILE_DOES_NOT_EXIST_IN_PATH_TEXT;
			txtFileDoesNotExistInPathTitle = Constants.MFCE_FILE_DOES_NOT_EXIST_IN_PATH_TITLE;
		}
		
		// tato proměnná bude mít vždy stejný text:
		itemToString.setText("toString()...");
	}
	
	
	
	
	
	


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JMenuItem) {
			
			// Události na položky v menuClass:
			if (e.getSource() == itemSave)
				saveTextBackToFile();
			
			
			
			
			else if (e.getSource() == itemReload) {
				// Postup:
				// Nejprve otestuji, zda došlo ke změně, pokud ano, tak se uživatele zeptam, zda se má kód uložit, pokud ano, tak se uloží, 
				// pokud ne, tak se pouze přenačte
								
				if (sharedClass.existFile(codeEditorDialog.getPathToClass())) {
					// Načtu si kód původní třídy, abych otestoval, zda byl kód změněn:
					final List<String> codeOfOriginalClass = editCodeOfClass.getTextOfFile(codeEditorDialog.getPathToClass());
				
					// Otstuji, zda jsou kódy stejné:
					final boolean areCodesIdenticaly = codeEditorDialog.getCodeEditor().compareSourceCode(codeOfOriginalClass);
				
					if (!areCodesIdenticaly) {
						// Zde nejsou kódy stejné, tak se zeptám, zda mám tento upravený kód uložit nebo ne:
					
						final int result = JOptionPane.showConfirmDialog(this, txtSaveChangesText, txtSaveChangesTitle, JOptionPane.YES_NO_OPTION);
					
						if (result == JOptionPane.YES_OPTION) {
							// Zde uživatel klikl na Ano, uložit změny, tak je uložím a to je v podstatě vše, není třeba přenačítat kód
						
							final List<String> textFromCodeEditor = codeEditorDialog.getCodeEditor().getTextFromCodeEditor();
							
							final boolean isCodeWritten = editCodeOfClass.setTextToFile(codeEditorDialog.getPathToClass(), textFromCodeEditor);
							
							if (!isCodeWritten)
								// Zde se nastala chyba při zápisu zdrojového kódu zpět do třídy:, oznámim to uživateli:
								JOptionPane.showMessageDialog(this,
										txtErrorWhileSavingChangesToClassText_1 + "\n"
												+ txtErrorWhileSavingChangesToClassText_2,
										txtErrorWhileSavingChangesToClassTitle, JOptionPane.ERROR_MESSAGE);
						}
					
						else if (result == JOptionPane.NO_OPTION)
							// Zde uživatel klikl na Ne - Nechce uložit změny, tak pouze znovu načtu kód dané třídy a vložím jej do editoru:
							codeEditorDialog.getCodeEditor().setTextToCodeEditor(codeOfOriginalClass);
					}
					// else - Zde jsou kódy stejné, tak není třeba je přenačítat ani nic ukládat - tak či tak, nedošlo by ke změně
				}
				else
					JOptionPane.showMessageDialog(this,
							txtClassNotFoundText + "\n" + txtPath + ": " + codeEditorDialog.getPathToClass(),
							txtClassNotFoundTitle, JOptionPane.ERROR_MESSAGE); 					
			}
			
			
			
			
			// Note:
			// Pro vytisknutí je třeba nejprve uložit změny, pokud se otevře soiubor a provedou změny,
			// tak se zmeny již neprojeví, pouze ten původní otevřený, resp. uložený soubor.
			// Nedal jsem sem ještě ulolže před výtisk úmyslně, protože nevím, to přesně s tím uživatel zamýšlí,
			// jestli chce vytisknout i ty změny nebo ne. ???
			else if (e.getSource() == itemPrint) {
				// potřebuji zde ještě otestovat, zda se daný soubor na zadané ceste opravu ještě vyskytuje, aby nenastala vyjímka
				// nebo nedošlo k nějaké chybě:
				final File fileClass = new File(codeEditorDialog.getPathToClass());
				
				if (fileClass.exists())
					new Printer(codeEditorDialog.getPathToClass());

				else
					JOptionPane.showMessageDialog(this, txtFileDoesNotExistInPathText, txtFileDoesNotExistInPathTitle,
							JOptionPane.ERROR_MESSAGE);
			}







			else if (e.getSource() == itemOpenFileInFileExplorerInOs) {
                final String pathToClass = codeEditorDialog.getPathToClass();

                DesktopSupport.openFileExplorer(new File(pathToClass), true);
            }

            else if (e.getSource() == itemOpenFileInDefaultAppInOs) {
                final String pathToClass = codeEditorDialog.getPathToClass();

                DesktopSupport.openFileExplorer(new File(pathToClass), false);
            }

				
			


			
			
			else if (e.getSource() == itemClose)
				// Záležitosti ohledně změny kódu je řešeno při zavírání, zde stačí zavřít dialog:
				sharedClass.closeDialog();
			
			
			
			
			
			
			// Události na kliknutí na položku v menuEdit:
			else if (e.getSource() == itemCut)
				// Vložím označený text do proměnné:
				sharedClass.cutString();
			
			
			
			else if (e.getSource() == itemCopy)
				// Vložím označený text do proměnné "- schránky" v editoru kódu:
				sharedClass.copyString();
			
			
			
			
			else if (e.getSource() == itemPaste)
				// Vložím na pozici v editoru kódu, kde se nachází kurzor text, který se nachází ve schránce (pomocí metody insert):
				sharedClass.insertString();
			
			
			
			
			
			else if (e.getSource() == itemIndentMore)
				menuMethods.indentMoreText(codeEditorDialog.getCodeEditor());
					
			
			
			
			else if (e.getSource() == itemIndentLess)
				// Zjistím si řádkek, na kterém se nachází kurzor
				// a zde nepotřebuji zjšťovat, první index řádku, jednoduš z řádku odeberu 
				menuMethods.indentLessText(codeEditorDialog.getCodeEditor());
				
			
			
			
			else if (e.getSource() == itemComment)
				// Na začátek řádku, na kterém se nachází kurzor se vloží dvé normální lomítka - coby komentář:				
				menuMethods.commentText(codeEditorDialog.getCodeEditor());
				
			
			
			
			
			else if (e.getSource() == itemUncomment)
				// Zjistím si text na řádku, kde se nachází kurzor v editoru kódu a na daném řádku,
				// pokaždé, když se klikne na tuto pololžku v menu, tak odeberu první výskty znaků: '//' pro odkomentování:
				// je možné, že se daný řádek zakomentovatl vícekrát, tak tento symbol odeberu, dokud se na daném řádku bude vyskytovat												
				menuMethods.uncommentText(codeEditorDialog.getCodeEditor());
			
			
			
			
			else if (e.getSource() == itemFormatCode) {		
				/*
				 * Note:
				 * Není potřeba ukládat soubor, stačí jen vzít text z editoru kodu, zformátovat
				 * jej a vložit zpět, ale, chci u toho ještě testovat další věci, jako je třeba
				 * existence souboru apod.
				 */
				
				final File fileClassToFormat = new File(codeEditorDialog.getPathToClass());
				
				if (fileClassToFormat.exists()) {
					// Nejprve otestuji, zda se kód změnil - došlo ke změně, abych ho nemusel zbytečně ukládat, .. viz níže:
					// Načtu si kód původní třídy, abych otestoval, zda bylkód změněn:
					final List<String> codeOfOriginalClass = editCodeOfClass.getTextOfFile(codeEditorDialog.getPathToClass());
				
					// Otstuji, zda jsou kódy stejné:
					final boolean areCodesIdenticaly = codeEditorDialog.getCodeEditor().compareSourceCode(codeOfOriginalClass);
				
					// Proměnná, do které uložím logickou hodnotu, dle toho zda se kód zapsal nebo ne
					// pokud se tam pří zápisu vloží false - kód se nepodařilo zapsat zpět do třídy, tak nemohu zformátovat kód
					final boolean isWritten;
					
					if (!areCodesIdenticaly) {
						// Zde kódy nejsou identické, tak ho musím uložiz:
						
						//  mohu uložit její kód:
						final List<String> codeFromEditor = codeEditorDialog.getCodeEditor().getTextFromCodeEditor();
						
						isWritten = editCodeOfClass.setTextToFile(codeEditorDialog.getPathToClass(), codeFromEditor);
						
					}
					else isWritten = true;					 
					
					
					if (!isWritten) {
						// else - kód se nepodařilo zapsat:

						JOptionPane.showMessageDialog(this,
								txtErrorWhileSavingChangesToClassText_1 + "\n" + txtErrorWhileSavingChangesToClassText_2
										+ "\n" + txtPath + ": " + codeEditorDialog.getPathToClass(),
								txtErrorWhileSavingChangesToClassTitle, JOptionPane.ERROR_MESSAGE);

						return;
					}

					// Zde jsou kódy stejné, nebo byly změny již uloženy, tak mohu kód zformátovat:
					menuMethods.formatText(codeEditorDialog.getCodeEditor(), fileClassToFormat);

				}
				else
					JOptionPane.showMessageDialog(this,
							txtClassNotFoundText + "\n" + txtPath + ": " + codeEditorDialog.getPathToClass(),
							txtClassNotFoundTitle, JOptionPane.ERROR_MESSAGE);
			}
			
			
			
				
			
			else if (e.getSource() == itemInsertMethod)
				menuMethods.insertMethod(codeEditorDialog.getCodeEditor());
			
			
			
			
			
			
			// Události na kliknutí na položku v menuTools:
			else if (e.getSource() == itemReplaceText)
				// Otevře se dialog, ve kterém se nahradí zadaný kód a ten se vrátí
				menuMethods.replaceText(codeEditorDialog.getCodeEditor());
				
			
			
			
			else if (e.getSource() == itemGoToLine)				
				menuMethods.goToLine(codeEditorDialog.getCodeEditor());
				
			
			
			
			else if (e.getSource() == itemCompile) {
				// Uložím kód třídy, poté ho zkompiluji
				// a výsledek se vyíše do editoru výstupů
				final Class<?> clazz = sharedClass.getCompiledClass(editCodeOfClass, languageProperties, true);
				
				// Note: Zde nemusím testovat, zda se podařilo načíst danou třídu nebo ne, pokud se to nepovedlo,
				// tak se v nsledující metodě vygenerují pouze přednastavené možnosti, jinak se z dané třídy vezmou i ty požadované
				
				codeEditorDialog.getCodeEditor().setAutoCompletion(clazz);
			}

			
			
			
			
			
			else if (e.getSource() == itemToString)
				menuMethods.toStringMethod(codeEditorDialog.getCodeEditor());
			
			
			
			
			else if (e.getSource() == itemGettersAndSetters) {								
				// Načtu si .class soubor třídy, jejiž zdrojový kód je načten v editrou a tuto referenci na Class předám do 
				// dialogu, aby mohl uživatel označit, které přístupové metody chce přidat				
				final Class<?> clazz = sharedClass.getCompiledClass(editCodeOfClass, languageProperties, false);
								
				
				if (clazz != null)
					// Vytvořím dialog pro označení položek s přístupovými metodami, které se mají přidat:
					menuMethods.generateGettersAndSetters(codeEditorDialog.getCodeEditor(), clazz);
				
				else
					JOptionPane.showMessageDialog(this,
							txtCouldNotLoadClassFile + "\n" + txtClass + ": " + codeEditorDialog.getPathToClass(),
							txtFileNotFound, JOptionPane.ERROR_MESSAGE);	
			}			
			
			
			
			
			
			else if (e.getSource() == itemConstructor) {								
				// Načtu si .class soubor třídy, jejiž zdrojový kód je načten v editrou a tuto referenci na Class předám do 
				// dialogu, abych mohl načíst proměnné do kontruktoru:
				
				final Class<?> clazz = sharedClass.getCompiledClass(editCodeOfClass, languageProperties, false);
				
				if (clazz != null)
					// Vytvořím dialog pro označení položek, které se mají přidat:
					menuMethods.generateConstructor(codeEditorDialog.getCodeEditor(), clazz);
				
				else
					JOptionPane.showMessageDialog(this,
							txtCouldNotLoadClassFile + "\n" + txtClass + ": " + codeEditorDialog.getPathToClass(),
							txtFileNotFound, JOptionPane.ERROR_MESSAGE);
			}
			
			
			
			
			
			
			
			else if (e.getSource() == itemAddDefaultSerialVersionID)
				menuMethods.insertDefaultSerialVersionId(codeEditorDialog.getCodeEditor());
			
			
			
			
			else if (e.getSource() == itemAddGeneratedSerialVersionId) {
				/*
				 * Nejprve uložím aktuální text v editoru zdrojového kódu zpět do souboru.
				 */
				if (!saveTextBackToFile())
					return;
				
				
				
				/*
				 * Získám si název otevřené třídy včetně balíčků, kde se nachází, abych mohl
				 * načíst příslušnou třídu.
				 */
				final String cellText = codeEditorDialog.getClassNameWithDot();
				
				/*
				 * Pokud nebyl zadán název třídy s balíčky. Tj. název třídy, která je otevřena v
				 * editoru zdrojového kódu, pak mohu skončit, ale toto by němwlo nastat, protože
				 * tato hodnota je vždy zadána, pokud je otevřena třída v tomto dialogu, a pokud
				 * není, tato položka - možnost pro vygenerování serial version ID by neměla být
				 * přístupná, ale kdyby náhodou se někde neco pokazilo, tak alespoň nevypadne
				 * výjimka NullPointer.
				 */
				if (cellText == null)
					return;
				
				
				
				/*
				 * Zde zkusím nejprve všechny třídy zkompilovat a pokud se tak povede, tak si
				 * načtu příslušnou třídu, resp. její zkompilovaný soubor .class. Pokud dojde k
				 * nějaké chybě, tak si zkusím i tak načíst původní soubor bez kompilace tříd.
				 * Tento půvdoní soubor by se měl v adresáří bin již nacházet pokd již byla
				 * třída dříve zkompilována a to stačí pro vygenerování serial version ID.
				 */
				Class<?> clazz = App.READ_FILE.getClassFromFile(cellText, null, GraphClass.getCellsList(),
						false, false);

				if (clazz == null)
					App.READ_FILE.loadCompiledClass(cellText, null);

				// Vygeneruji ID:
				menuMethods.insertGeneratedSerialVersionId(clazz, codeEditorDialog.getCodeEditor());
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která uloží text, který se nachází v otevřeném dialogu v editoru
	 * zdrojového kódu zpět do souboru, odkud byl načten, případně vyhodí chybové
	 * hlášení o tom, že soubor již neexistuje apod.
	 * 
	 * @return logická proměnná, která značí, zda byl dokument uložen nebo ne. True,
	 *         pokud byl dokument úspěšně uložen, jinak false.
	 */
	private boolean saveTextBackToFile() {
		// Zde se má uložit kód v editoru zpět do třídy:
		// Otestuji, zda třída, jejiž kód je v načtený v editoru ještě existuje, a pokud ano, tak si vezmu kod a zapíšu ho zpět:								
		if (sharedClass.existFile(codeEditorDialog.getPathToClass())) {
			// zde daná třída ještě existuje, mohu uložit její kód:
			final List<String> codeFromEditor = codeEditorDialog.getCodeEditor().getTextFromCodeEditor();
			
			final boolean isWritten = editCodeOfClass.setTextToFile(codeEditorDialog.getPathToClass(), codeFromEditor);

			if (isWritten)
				return true;

			else
				JOptionPane.showMessageDialog(this,
						txtErrorWhileSavingChangesToClassText_1 + "\n" + txtErrorWhileSavingChangesToClassText_2 + "\n"
								+ txtPath + ": " + codeEditorDialog.getPathToClass(),
						txtErrorWhileSavingChangesToClassTitle, JOptionPane.ERROR_MESSAGE);
		}

		else
			JOptionPane.showMessageDialog(this,
					txtClassNotFoundText + "\n" + txtPath + ": " + codeEditorDialog.getPathToClass(),
					txtClassNotFoundTitle, JOptionPane.ERROR_MESSAGE);

		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která znepřístupní některé položky v menu - položky, které uožňují
	 * provést operace, které nelze provést se zdrojovým kódem třídy, která není
	 * načtena z diagramu tříd
	 */
	final void blockEntries() {
		// Tlačítka pro: kompilace, getry a setry, konstruktor:
		itemGettersAndSetters.setEnabled(false);
		itemConstructor.setEnabled(false);
		itemCompile.setEnabled(false);
		itemFormatCode.setEnabled(false);
		itemToString.setEnabled(false);
		itemInsertMethod.setEnabled(false);
		itemComment.setEnabled(false);
		itemUncomment.setEnabled(false);
		
		menuAddSerialVersionID.setEnabled(false);
	}
}