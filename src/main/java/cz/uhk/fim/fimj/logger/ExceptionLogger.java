package cz.uhk.fim.fimj.logger;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.file.WriteToFile;

/**
 * Tato třída slouží pro přesměrování chyb, resp. vyjímek, které mohou nastat, a které "v této aplikaci hlídám" do
 * souboru v adresáři configuration v adresáři zvolený uživatelem coby Workspace ve složce logs, tak do této složky se
 * budou zapisovat veškeré chyby, kterére v aplikaci na pozadí nastanou.
 * <p>
 * Stručně - přesměrovávají se do zmíněného souboru .log zachytávané vyjímky v aplikaci.
 * <p>
 * Po každé, když se spustí aplikace, tak se v příslušném adresáři vytvoří nový soubor s aktuálním datem a časem
 * spuštění aplikace, aby se vědělo, kdy k příslušné vyjímce došlo.
 * <p>
 * Note: Nepodařilo se mi přesměrovat všechny výpisy z této aplikace, takže například u Preferencí či nějakých výpisů z
 * knihoven to nefunguje, takže se zobrazí stejně v příslušném dialogu pro uživatelovy výpisy z projektu, který si
 * "vytvoří"
 * <p>
 * Note: Původně jsem se pokoušel napsat třídu, která dědí z Loggeru, a pouze přepsat metodu pro zapsání příslušné
 * informac e - metoda warning nebo log, ale to se mi nepodařilo, tak jsem zvolil tuto nevím, zda složitější, ale určitě
 * delší a trochu "náročnější" variantu pro zápis informací o chybách do souboru, i když jsem mohl pouze zapisovat
 * například nějaké syntaxe do txt souboru.
 * <p>
 * Note: Dále je u každého zachycení vyjímky následující kód: (v parametru metody getLogger se jedná o to o jaký výpis
 * se jedná, více v daném výčtu)
 * <p>
 * final Logger logger = Exception.getLogger(ExtractEnum.EXTRACT_EXCEPTION); if (logger != null) {
 * logger.log(Level.INFO, "App launched"); Exception.closeFileHandler(); }
 * <p>
 * - Je to proto, že když nastane vyjímka, tak se pomocí metody log zapíše příslušný text do souboru, ale s tím, se dále
 * vygeneruje, resp. zjistí třída a metody a vygeneruje čas, kdy k tomu došlo, což bych musel vše dělat sám, pokud bych
 * nepoužil tuto "kompnentu" Logger
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ExceptionLogger {

	private ExceptionLogger() {
	}

	/**
	 * Proměnná typu Logger, do které se budou zapisovat nastalě výjimky a ten je zapíše
	 * v nastaveném formátu do souboru
	 */
	private static final Logger LOGGER = Logger.getLogger("ExceptionLogger");





	/**
	 * Proměnná typu String, která drží čas a datum v definovaném formátu, takže když se spustí aplikace, tak se
	 * příslušný čas v daném formátu uloží do této proměnné a po dobu běhu aplikace se budou potenciální výjimky
	 * zapisovat do souboru / logu s tímto názvem
	 */
	private static final String MY_DATE_TIME_FILE_NAME = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss_a").format(new Date
			());
	
	
	
	
	
	/**
	 * Proměnná, do které se vloží cesta, resp. načte se stream k danému souboru, do
	 * kterého se má zapsat nějaké nové hlášení o nastalé vyjímce
	 */
	private static FileHandler fh;
	
	
	
	
	
	/**
	 * Proměnná typu Properties, která obsahuje texty pro aplikaci v uživatelem
	 * zvoleném jazyce, je zde potřeba, aby se pomocí ní naplnily proměnné pro
	 * chybové hlášky, kdyby k ním náhodou došlo
	 */
	private static Properties languageProperties;
	
	
	
	
	// texty do chybových hlášek
	private static String txtFailedToLoadOrCreatingFileText, txtForWriteException, txtErrorInAccessToFile,
			txtFailedToLoadOrCreatingFileTitle, txtErrorWhileOpeningFile, txtErrorWhileCreatingDirText,
			txtErrorWhileCreatingDirTitle;
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která v adresáři logs v adresáři configuration ve Workspace buď
	 * vytvoří a nebo načte - pokud již příslušný soubor existuje tak načte tento
	 * soubor s názvem coby datum a čas spuštění aplikace a přidá jej do Loggeru,
	 * který tato metoda vrátí, aby se mohla do příslušného souboru vrátit
	 * 
	 * Pokud neexistuje workspace, tak není co řešit, v takovém případě se nic dělat
	 * nebude, protože o tom uživatel jistě ví kvůli projěktům,apod. - ostatní práce
	 * s aplikací, ale pokud neexistuje adresář configuration, tak se vytvooří,
	 * resp. znovu nakopíruje do adresáře coby Workspace. pokud adresář
	 * configuration existuje, tak se otestuje existence adresáře logs, a pokud
	 * neexistuje, tak se vytvoří, a pokud existuje, tak není co řešit:
	 * 
	 * Pokud ještě neexistuje soubor s aktuálním datem spuštění, tak se vytvoří a
	 * pokud již existuje, tak se pouze na koec souboru připíše další "hlášení" o
	 * nastalé vyjímce - zachycené
	 * 
	 * @param kindOfExtract
	 *            - proměnná výštového typu, dle které se pozná o jaký se jedná
	 *            zápis, jendá se o zápisy coby hlášení z vyjímky, při spuštění
	 *            aplikace se tam zapíše spuštění aplikace, a při ukončení aplikace
	 *            se do příslušnéh souboru .log zapíše informace o ukončení aplikace
	 * 
	 * @return Logger, který je připraven k zapisování do přislušného souboru, jinak
	 *         null, pokud se něco nepodaří - viz kroky metody
	 */
	public static Logger getLogger(final ExtractEnum kindOfExtract) {
		/*
		 * Nejprve pro případ, že by došlo k nějaké chybě někde v metodách v této třídě potřebuji
		 * naplnit textové proměnné, které slouží pro oznámení uživateli, že došlo k nějaké chybě
		 * při zápisu do logu, tak to musím oznmit uživateli a nezapisovat to do logu - to vlastně selhalo.
		 * 
		 * Nejprve otestuji, zda je proměnná laguageProperties null a pokud ano, tak se ještě nenačetl
		 * souor s jazyky pro aplikaci, pokud se již načetl a nebyl null, tak se uloži do proměné
		 * a hlášky se naplnily, pokud je ale null, tak se použijou výchozí texty v aplikaci
		 */
		if (languageProperties == null) {
			languageProperties = App.READ_FILE.getSelectLanguage();
			setLanguage(languageProperties);
		}
		
		
		// Zde mohu pokračotvat zjištěním cesty k adresáři coby Workspace:
		final String pathToWorkspace = ReadFile.getPathToWorkspace();

        if (pathToWorkspace == null)
            /*
             * else - zde neexistuje adresář coby Workspace, tak není co řešit, ale je tu možnost, že se může aplikace
             * odtázat uživatele na workspace, ale to už by měl uživatel vědět, a navíc se to dozví u projektů
             */
            return null;


        // Zde se vrátila nějaká cesta, tak otestuji, zda říslušný adresář existuje:

        // Zde existuje adresář workspace, tak otestuji adresář configuration
        final File fileConfiguration = new File(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME);

        if (!ReadFile.existsDirectory(fileConfiguration.getAbsolutePath()))
            // Zde adresář configuration bu'd neexistuje, nebo se nejedná o adresář,
            // ať tak či tak, je třeba jej vytvořit, tak ho znovu nakopíruji:
            WriteToFile.createConfigureFilesInPath(pathToWorkspace);


        // Nyní musím znovu otestovat, zda existuje adresář configuration, protože mohlo
        // napříkad dojiít k chybě
        // při kopírování, v takovém případě nemohu pokračovat:
        if (ReadFile.existsDirectory(fileConfiguration.getAbsolutePath())) {
            // Zde již existuje - pokud neexistovat, tak mohu otestovat existenci adresáře
            // logs:

            final File fileLogs = new File(
                    fileConfiguration.getAbsolutePath() + File.separator + Constants.LOGS_NAME);


            if (!ReadFile.existsDirectory(fileLogs.getAbsolutePath()))
                createDir(fileLogs.getAbsolutePath());


            // Zde musím opět otestovat, zda se podařilo vytovřit adresář logs - pokud
            // neexistoval (tato podmínka by neměla být splněna):
            if (!ReadFile.existsDirectory(fileLogs.getAbsolutePath()))
                return null;

            // Zde již adresář logs existuje, tak mohu zapsat nové hlášení,


            /*
             * Note:
             *
             * Zde již nemusím testovat, zda existuje soubor s přísluným názvem coby datum a čas spuštění
             * protože to volám pomocí metody FileHandler, vlastně konstruktoru, a ten pokud příslušný soubor
             * neexistuje, tak ho vytvoří a pokud existuje, tak ho načte a pomocí druhého parametru - true,
             * mu dám vědět, že pokud příslušný soubor existuje, tak má přidat nové hlášení na konec souiboru
             * - že ho nemá přepsat celý soubor na nový:
             */
            fh = getMyFileHandler(fileLogs.getAbsolutePath() + File.separator + MY_DATE_TIME_FILE_NAME + "" +
                    "log", kindOfExtract);


            // Otestuji, zda se podařilo vytvořit FileHandler pro zapsání nového hlášení do
            // příslušného
            // souboru:
            if (fh != null) {
                // Zde se podařilo vytvořit nebo načíst soubor, do kterého se má zapsta nové
                // hlášení,
                // tak ho mohu vrátit:

                // Nastavím loggeru, že nemá do konzole vypisovat hlášení o zapsání:
                LOGGER.setUseParentHandlers(false);

                // Nastavím loggeru soubor, kam se mají data zapisovat:
                LOGGER.addHandler(fh);

                return LOGGER;
            }
        }


        return null;
    }
	
	

	
	
	
	/**
	 * Metoda, která zapvře "proud" - stream k otevřenému souboru, do kterého se
	 * zapsala nejaká infomace - hlášení o nasalé vyjímce.
	 * 
	 * Je to třeba, aby si aplikace nedržela pořád tento "proud" k souboru, protože,
	 * by se ukončil až po zavření aplikace a do té doby, by s ním uživatel nemohl
	 * nijak manipulovat
	 */
	public static void closeFileHandler() {
		if (fh != null)
			fh.close();
	}



	
	
	
	/**
	 * Metoda, která (vytvoří - pokud neexistuje) a načte soubor, kam se má zapsat
	 * hlášení o vyjímce, cesta k nějakému souboru typu .log, v adresáři logs v
	 * configuration ve Wroskapce
	 * 
	 * @param path
	 *            - cesta k příslušnému souboru
	 * 
	 * @param kindOfExtract
	 *            - proměnný výčtového typu, dle které se pozná, zda se má
	 *            odřádkovat nebo ne ve výsledném souboru .log - dle toho, o jaký se
	 *            jedná zápis - jestli o spuštění aplikace, nebo ukončení aplikace
	 *            nebo klasické hlášení o vyjímce
	 * 
	 * @return načtený "Stream" k souboru pro zapsání
	 */
	private static FileHandler getMyFileHandler(final String path, final ExtractEnum kindOfExtract) {
		try {
			// Vytovřím si příslušný soubor pro hlášení - pokud existuje, tak ho jen načtu:
			final FileHandler fh = new FileHandler(path, true);
			
			// Nastavím mé formátování pro přslušné zápisy a to zárověň formátuje
			// i text, který se má zapsat do souboru - pouze mnou zvolené nutné informace v mnou
			// zvolené syntaxe, když tak si to v dané třídě - MyFormatter můžete změnit
			fh.setFormatter(new ExceptionFormatter(kindOfExtract));
			
			return fh;
			
		} catch (SecurityException e) {
			/*
			 * Tato vyjímka mi ještě nikdy nevypadla, ale jedná se o to, že se
			 * nemůže vytvočit soubor, takže tato metoda nevrátí "cestu"  k potřebnému souboru,
			 * a nelze tedy zapisovat do logu, takže je nesmysl zavolat regurzivne metodu pro 
			 * zapsání do logu, tak to pouze oznámím uživateli
			 * V těchto vyjímkách to asi nevypisovat, ale oznámit uživateli ???
			 */
			
			JOptionPane.showMessageDialog(null,
					txtFailedToLoadOrCreatingFileText + ": " + path + "\n" + txtForWriteException + "\n"
							+ txtErrorInAccessToFile + " (SecurityException).",
					txtFailedToLoadOrCreatingFileTitle, JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			
			/*
			 * Zde se jedná o podobný způsob jako u vyjímky výše - nelze rekurzivně volat metodu pro
			 * vytvoření log souboru, tak to pouze oznámím uživateli
			 */
			
			JOptionPane.showMessageDialog(null,
					txtFailedToLoadOrCreatingFileText + ": " + path + "\n" + txtForWriteException + "\n"
							+ txtErrorWhileOpeningFile + " (IOException).",
					txtFailedToLoadOrCreatingFileTitle, JOptionPane.ERROR_MESSAGE);
		}
		
		
		return null;
	}
	
	
	




	/**
	 * Metoda, která se pokusí vytvořít adresář logs na zadané cestě - pathToLogs
	 * 
	 * @param pathToLogs
	 *            - proměnná typu String, která se pokusí vytvořit adresář na
	 *            zadaném umístění
	 */
	private static void createDir(final String pathToLogs) {
		try {
			Files.createDirectories(Paths.get(pathToLogs));						
		} catch (IOException e) {
			/*
			 * Zde nastala nějaká chyba při vytváření adresáře, tak ani nemohu volat metodu pro
			 * opětovné vytvoření zápisu logu, protože bych se moná nekonečně zacyklyl, 
			 * tak to pouze oznámím uživateli
			 */
			
			JOptionPane.showMessageDialog(null, txtErrorWhileCreatingDirText + ":\n" + pathToLogs,
					txtErrorWhileCreatingDirTitle, JOptionPane.ERROR_MESSAGE);
		}
	}
	
	

	
	
	
	
	/**
	 * Metoda, která převede vzniklou vyjímku na text, který lze vožit do Loggeru -
	 * do příslušného souboru pro přesnější informaci
	 * 
	 * Zdroj (byl to druhý komentář s 55 Liky - v této době hledání):
	 * http://stackoverflow.com/questions/18546842/stack-trace-as-string
	 * 
	 * @param throwable
	 *            - vzniklá vyjímka - informace o ní
	 * 
	 * @return vzniklá vyjímka převedená na text - v vlastně se jedná o stactTrace,
	 *         který se vypisuje do konzole například v Eclipse metodou
	 *         e.printStactTrace()
	 */
	public static String getStackTrace(final Throwable throwable) {
	     final StringWriter sw = new StringWriter();
	     
	     final PrintWriter pw = new PrintWriter(sw, true);
	     
	     throwable.printStackTrace(pw);
	     
	     return sw.getBuffer().toString();
	}






    /**
     * Getr na proměnnou typu String, která drží název souboru, který odpovídá datumu a času spuštění aplikace -
     * proměnná MY_DATE_TIME_FILE_NAME
     *
     * @return referenci na výše popsanou proměnnou MY_DATE_TIME_FILE_NAME
     */
    public static String getDateTimeNameFile() {
        return MY_DATE_TIME_FILE_NAME;
    }

	




	public static void setLanguage(final Properties properties) {
		if (properties != null) {		
			txtFailedToLoadOrCreatingFileText = properties.getProperty("Ml_Txt_FailedToLoadOrCreatingFileText", Constants.ML_TXT_FAILED_TO_LOAD_OR_CREATING_FILE_TEXT);
			txtForWriteException = properties.getProperty("Ml_Txt_ForWriteException", Constants.ML_TXT_FOR_WRITE_EXCEPTION);
			txtErrorInAccessToFile = properties.getProperty("Ml_Txt_ErrorInAccessToFile", Constants.ML_TXT_ERROR_IN_ACCESS_TO_FILE);
			txtFailedToLoadOrCreatingFileTitle = properties.getProperty("Ml_Txt_FailedToLoadOrCreatingFileTitle", Constants.ML_TXT_FAILED_TO_LOAD_OR_CREATING_FILE_TITLE);
			txtErrorWhileOpeningFile = properties.getProperty("Ml_Txt_ErrorWhileOpeningFile", Constants.ML_TXT_ERROR_WHILE_OPENING_FILE);
			txtErrorWhileCreatingDirText = properties.getProperty("Ml_Txt_ErrorWhileCreatingDirText", Constants.ML_TXT_ERROR_WHILE_CREATING_DIR_TEXT);
			txtErrorWhileCreatingDirTitle = properties.getProperty("Ml_Txt_ErrorWhileCreatingDirTitle", Constants.ML_TXT_ERROR_WHILE_CREATING_DIR_TITLE);
		}
		
		else {
			txtFailedToLoadOrCreatingFileText =Constants.ML_TXT_FAILED_TO_LOAD_OR_CREATING_FILE_TEXT;
			txtForWriteException = Constants.ML_TXT_FOR_WRITE_EXCEPTION;
			txtErrorInAccessToFile = Constants.ML_TXT_ERROR_IN_ACCESS_TO_FILE;
			txtFailedToLoadOrCreatingFileTitle = Constants.ML_TXT_FAILED_TO_LOAD_OR_CREATING_FILE_TITLE;
			txtErrorWhileOpeningFile = Constants.ML_TXT_ERROR_WHILE_OPENING_FILE;
			txtErrorWhileCreatingDirText = Constants.ML_TXT_ERROR_WHILE_CREATING_DIR_TEXT;
			txtErrorWhileCreatingDirTitle = Constants.ML_TXT_ERROR_WHILE_CREATING_DIR_TITLE;
		}
	}
}