package cz.uhk.fim.fimj.logger;

/**
 * Tento výčet obsahuje tří hodnoty (viz níže) a je zde definován pouze pro to, aby se pozanlo při formátování u výpisu
 * do příslušného souboru, zda se má odřádkovat výpis nebo ne, protoe pokud se bude jednat o výpis o spuštění nebo
 * ukončení aplikace, tak bude buď na začátku souboru, nebo úplně na koneci souboru a nad ním mezera, a nebo výpis z
 * vyjímky, pak se jedná o běžné formátování, takže dva řádky u sebe, a kolem nich prázdný řádek
 * <p>
 * Tento výčet slouží pouze pro to, aby se definovalo, zda se jedná o ukončení aplikace, spuštění aplikace, nebo klsický
 * výpis nějaké vyjíky do logs
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public enum ExtractEnum {

    /**
     * Tato hodnota značí, že se jendá o spuštění aplikace
     */
    LAUNCH_APP,

    /**
     * Tato hodnota značí, že se jedná o ukončení aplikace
     */
    TERMINATE_APP,

    /**
     * Tato hodnota značí, že se jedná o výpis vyjímky do příslušného souboru
     */
    EXTRACT_EXCEPTION
}