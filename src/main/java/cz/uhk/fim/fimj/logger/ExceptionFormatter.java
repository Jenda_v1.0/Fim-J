package cz.uhk.fim.fimj.logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * Tato třída slouží pouze jako formátování a zároveň filtrace textu, který se má zapsat do souboru typu .log v případě,
 * že dojde k zachycení nějaké vyjímky v této aplikaci
 * <p>
 * Jak bylo zmíněno, tato třída filtruje některé texty, resp. definuje, pouze ty hodnoty, které se mají zapsat, a vybral
 * jsem pouze ty důležité, jako jsou text samotné vyjímky, kdy k ní došlo a kde k ní došlo - třída a metoda - i když
 * tyto informace jsou už v textu té vyjímky, tak pro přehlednost...
 * <p>
 * Zdroj:
 * <p>
 * https://kodejava.org/how-do-i-create-a-custom-logger-formatter/
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ExceptionFormatter extends Formatter {


    /**
     * Definuji si formát času, který se zobrazí při zapsání do souboru .log s nějakou chybou, resp zápisem o nastalé
     * vyjímce
     */
    private static final DateFormat DF = new SimpleDateFormat("HH:mm:ss");

	
	/**
	 * Tato proměnná slouží pro definici toho, kdy se má odřádkovat pří zápisu do
	 * příslušného souboru .logs, protože chci, aby byly alespoň trochu prehledné ty
	 * výpisy, tak budou vždy dva řádky u sebe, pak jeden řádek mezera a pak další
	 * dvá řádky výpis, tak tato proměnná bude vždy inkrementovat a když bude dva,
	 * tak ji nastavím na nulu a vytvořím příslušnou mezeru v souboru
	 */
	private static int countLine = 0;

	
	
	
	
	
	/**
	 * Proměnná výčtového typu, která drží hodnotu o tom, zda se má odřádkovat výpis
	 * v souboru nebo ne, pokud bude hodnota proměnné pro spuštění aplikace, tak se
	 * zaíše hlášení a odrádkuje se pokud se jedná o ukončení aplikace, tak se
	 * odřádukje, a pak se provede zápis
	 */
	private final ExtractEnum kindOfExtract;
	
	
	
	
	
	
	
	/**
	 * Konstruktor slouží hlavně pro naplnění hodnoty proměnné
	 * isAppExtract, aby se vědělo, zda se jedná o zápis do logu, resp. příslušného
	 * souboru za účelem zapsání hlášení o spuštění nebo ukončení aplikace, nebo ne
	 *
	 * @param kindOfExtract - výčtová hodnota, dle které se pozná, o jaký typ zápisu se jedná - viz výše nebo hodnoty
	 *                         ve výčtu.
	 */
	ExceptionFormatter(final ExtractEnum kindOfExtract) {
		super();

		this.kindOfExtract = kindOfExtract;
	}
	
	
	
	
	
	
	
	public final String format(final LogRecord record) {
		final StringBuilder builder = new StringBuilder();

		// pokud se jedná o druhý zápis, tak vytvořím 3 řádky mezeru:		
		if (countLine == 2) {
			countLine = 0;
			builder.append("\r\n");
			builder.append("\r\n");
			builder.append("\r\n");
		}
		
		
		/*
		 * Pokud se jedná o ukončení aplikace, pak zapíšu informace jako je čas ukončení
		 * a informaci o tom, že se jedná o ukončení aplikace a text vrátím.
		 */
		if (kindOfExtract.equals(ExtractEnum.TERMINATE_APP)) {
			builder.append(DF.format(new Date(record.getMillis())));
			builder.append(" -> ");
			builder.append(record.getMessage());
			return builder.toString();
		}
				
		
		
		/*
		 * Pokud se jedná o spuštění aplikace, pak vrátím text, který bude obsahovat
		 * čas, kdy byla spuštěna aplikace a informaci o tom, že se jedná o spuštění
		 * aplikace a tento text vrátím.
		 */
		else if (kindOfExtract.equals(ExtractEnum.LAUNCH_APP)) {
			builder.append(DF.format(new Date(record.getMillis())));
			builder.append(" -> ");
			builder.append(record.getMessage());
			builder.append("\r\n");

			countLine = 2;
			return builder.toString();
		}
		
		
		
		/*
		 * V této části kódu se nejedná o spuštění nebo ukončení aplikace ale o nějakou
		 * výjimku, tak o ní vypíšu potřebné informace:
		 */
		
		
		// zapíšu datum, kdy došlo k dané vyjímce:
		builder.append(DF.format(new Date(record.getMillis()))).append(" - ");

		// za to připojím název třídy, ve které vyjímka nastala
		builder.append("[ Class: ").append(record.getSourceClassName()).append(" ], ");

		// Metoda, kde k ní došlo:
		builder.append("[ Method: ").append(record.getSourceMethodName()).append(" ], ");

		// Úrověň, resp. priorita či důležitost vyjímky
		builder.append("[ Level: ").append(record.getLevel()).append(" - ");

		// a text té vyjímky:
		builder.append(record.getMessage()).append(" ];\r\n");

		// inkrementuji proměnnou, jako že nastal další zápis
		countLine++;

		
		// vrátím text, který se má zapsat do souboru coby nějaké hlášení o nastalé
		// vyjímce
		return builder.toString();
	}
}