package cz.uhk.fim.fimj.commands_editor;

/**
 * Třída, resp. rozhraní slouží pro deklaraci metod, které budou testovat zadanou syntaxi pomocí regulárních výrazů.
 * Syntaxe jako je například vytvoření instanci, zavolání metody, naplnení proměnné, apod. Viz jednotlivé metody.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface RegularExpressionsInterface {
	
	/*
	 * Přehled syntaxí, které umí editor rozpoznat:
	 * 
	 * metody, která vytiskne do editoru výstupů všechny příkazy, které lze zadávat do editoru příkazů:
	 * 		printCommands();
	 * 
	 * Početní operace v rámci použité knihovny, lze je pomocí v printCommands();
	 * 
	 * 
	 * vytvoření instance: model.data.ClassName variable = new model.data.ClassName(); - i s kombinaci s baličky nebo bez
	 * 
	 * vytvoření instance s parametrem: model.data.ClassName variable = new model.data.ClassName(parameterName); - + kombinace s balíčky
	 * 
	 * 
	 * 
	 * Parametry instance mohou být pouze primitivní datové typy, predani vytvorene instance - reference na ni - nikoliv vytvoreni!, zavolná metody s danám navratovym typem
	 * kdy tato metoda muze mit nejvice jeden parametr, jinak selze získavani hodnot pri rozdelovani parametru dle desetinne carky!
	 * načtení hodnoty z proměnní instance třídy nebo editoru
	 * vytvoření instance s parametry: model.data.ClassName variable = new model.data.ClassName(parameterName1, parameterName2, ...); - + kombinace s balíčky
	 * 
	 * PARAMETRY PRI VYTVORENI INSTANCE MUZE BYT I DALSI METODA POUZE S JEDNIM PARAMETREM, PROTOZE ROZDĚLUJI PARAMETRY METODY NEBO KONSTUKTORU DLE CARKY A PAK JE DALSE
	 * TESTUJI, KDYZ BUDE V PARAMETRU TREBA METODY DALSI METODY, KTERA BUDE MIT VICE PARAMETRU, PARAMETRY KONSTRUKTORU SE ROZDELI SPATNE DLE DESETINNE CACRKY !!!
	 * 
	 * 
	 * 
	 * zavolání metody: variableName.NameOfMethod(); 
	 * 
	 * zavolání metody s více parametry: variableName.NameOfMethod(parametrName1, parameterName2, ...);
	 * 
	 * 
	 * naplnění proměnné: variableClassName.variable = X;
	 * 
	 * naplnení proměné hodnotou jiné proměnné:  variableClassName.variable = variableClassName2.variable;
	 * 
	 * naplnění proměnné pomocí metody nějaké třídy, která vrací daný typ hodnoty:   variableClassName.variable = variableClassName.getr();
	 * 
	 * naplnění prměnné pomocí metody nějaké třídy, která vrací daný typ hodnoty s proměnnou:   variableClassName = vaiableClassName.Getrmethod(parameterName, ...);
	 * 
	 * To samé co je výše popsáno s metodami lze zavolat i se statickou metodou: model.data.ClassName.methodName(parameter, ...);
	 * 
	 * Inkrementace a dekrementace čísla:  X++ a X--  a ++X  a --X
	 * 
	 * 
	 * ++ClassName.variableName;	 
	 * --ClassName.variableName;
	 * 
	 * ClassName.variableName++;
	 * ClassName.variableName--;
	 * 
	 * 
	 * 
	 * Dále výpis hodnot do OutputEditoru, pomocí metody print(); se vytiskně příslušná hodnota do tohoto editoru:
	 *  print(value);
	 *  print(classNameVariable.variableName);
	 *  print(variableClassName.getrMethod());
	 * 
	 * Dále je tento příkaz doplněn o výpis textu:
	 * 		print("example text");
	 * 
	 *  a různá kombinace (pokud se jedná o tzv. kombinaci, tak je text rozdělen dle znaku plus a dále testován
	 *  pro zjišťování hodnota promnných , metod, ..., tak je třeba dávat pozor, kde se tento znak napíše!):
	 *  
	 *  print("text" + value + classNameVariable, ...);
	 *  
	 *  
	 *  operace nad kolekci a polem:
	 *  nad polem length --> jak v instanci třídy, tak proměnná vytvořená v editoru příkazů, vždy se přistupuje 
	 *  přes instanci nebo jen název proměnné v případě editoru zdrojového kodu
	 *  
	 *  získání a vložení hodnoty do jednorozměrného pole. Tato proměnná může být v editoru příkazů nebo v instanci třídy.
	 */
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda uživatel zadal příkaz pro vypsání do editoru
	 * výstupů všechny příkazy, které lze zadat do editoru příkazů - neboli příkazy,
	 * které lze v tomto editoru rozpoznat:
	 * 
	 * Syntaxe: printCommands();
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isPrintCommands(final String command);
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda uživatel zadal příkaz v syntaxi:
	 * 'printTempVariables();'.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return true, pokud uživatel zadal příkaz ve výše uvedené syntaxi, jinak
	 *         false.
	 */
	boolean isPrintTempVariables(final String command);
	
	
	
	
	
	
	
	/**
	 * Metoda, který zjistí, zda uživatel zadal příkaz v syntaxi: 'exit();'.
	 * 
	 * (Tento příkaz slouží pro ukončení aplikace.)
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isCloseApplicationByExit(final String command);
	
	
	/**
	 * Metoda, který zjistí, zda uživatel zadal příkaz v syntaxi: 'exit(X);'.
	 * 
	 * Za X se může dosadit libovolé číslo v intervalu 0 - 59.
	 * 
	 * (Tento příkaz slouží pro ukončení aplikace.)
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isCloseApplicationByExitWithCount(final String command);
	
	
	/**
	 * Metoda, který zjistí, zda uživatel zadal příkaz v syntaxi: 'System.exit(0);'.
	 * 
	 * (Tento příkaz slouží pro ukončení aplikace.)
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isCloseApplicationBySystemExit(final String command);
	
	
	/**
	 * Metoda, k terá zjistí, zda uživatel zadan příkaz v syntaxi: 'restart();'
	 * 
	 * (Tento příkaz slouží pro restartování aplikace.)
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isRestartApplication(final String command);
	
	
	/**
	 * Metoda, k terá zjistí, zda uživatel zadan příkaz v syntaxi: 'restart(X);'
	 * 
	 * Za X se může doplnit číslo v intervalu <0 ; 59>.
	 * 
	 * (Tento příkaz slouží pro restartování aplikace.)
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isRestartApplicationWithCount(final String command);
	
	
	
	/**
	 * Metoda,která zjistí, zda uživatel zadal příkaz v syntaxi:
	 * 'cancelCountdown();'. Jedná se o příkaz, který slouží pro ukončeí
	 * odpočítávání do restartu nebo ukončení aplikace.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isCancelCountdown(final String command);
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o bíle znaky, například, zda jsou v textu
	 * v parametru této metody zadány pouze bíle znaky, například, tabulátory,
	 * mezery, apod.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz nebo jeho část, o které je třeba
	 *            zjistit, zda se nachází ve výše uvedené syntaxi (zda obsahuje bíle
	 *            znaky)
	 * 
	 * @return true, pokud se jedná o výše uveenou syntaxi, jinak false
	 */
	boolean isWhiteSpace(final String command);
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se text v parametru nachází v podobě nějakého textu
	 * v uvozovkách:
	 * 
	 * Syntaxe: "some text and other characters"
	 * 
	 * @param text
	 *            - část uživatelem zadaného příkazu, například parametr metody,
	 *            apod.
	 * 
	 * @return true, pokus je v textu v parametru text ve výše uvedené syntaxi,
	 *         jinak false
	 */
	boolean isTextInQuotes(final String text);
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje text v parametru, a zjistí, zda se jedná o název třídy
	 * případně název třídy i s balíčky
	 * 
	 * Příklad: model.data.ClassName nebo ClassName - bez balíčků
	 * 
	 * @param text
	 *            - text, o kterém chci zjistit, zda se jedná u uvedenou syntaxi
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isClassName(final String text);
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o referenci na instanci třídy v
	 * následující syntaxi:
	 * 
	 * Syntaxe: referenceName (mohl by být í středník na konci, ale v tomto případě
	 * se bude tento text nacházet někde v parametru například metody, apod.)
	 * 
	 * - první je vždy písmeno, pak se mohou opakovat písmena bez diakritiky,
	 * číslice a podtržítka
	 * 
	 * @param text
	 *            - část uživatelem zadaného příkazu, například
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isReferenceToInstanceOfClass(final String text);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se text v parametru metody nachází v syntaxi, která
	 * odpovídá výrazu pro text: "some text", čísla: - 12374.78 nebo znaku: 'c'
	 * 
	 * @param text
	 *            - část uživatelem zadaného příkazu v editoru příkazů
	 * 
	 * @return true, pokud se jedná o zmíněnou syntaxi, jinak false
	 */
	boolean isConstantOrLiteral(final String text);
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o název třídy se závorkami v syntaxi:
	 * 
	 * model.data.ClassName();
	 * 
	 * nebo:
	 * 
	 * ClassName();
	 * 
	 * @param text
	 *            - text, o kterém chci zjistit, zda se jedná o výše uvedenou
	 *            syntaxi
	 * 
	 * @return true, pokud je parametr ve výše uvedenoé syntaxi, jinak false
	 */
	boolean isClassNameWithBracket(final String text);
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o celé číslo
	 * 
	 * XXXX
	 * 
	 * @param number
	 *            - uživatelem zadané číslo, o kterém chci zjistit, zda se jedná o
	 *            celé číslo
	 * 
	 * @return true, pokud se jedná o celé číslo, jinak false
	 */
	boolean isInteger(final String number);
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o desetinné číslo
	 * 
	 * X.Y
	 * 
	 * @param number
	 *            - uživatelem zadané číslo, o kterém chci zjistit, zda se jedná o
	 *            desetinné číslo
	 * 
	 * @return true, pokud se jedná o desetinné číslo, jinak false
	 */
	boolean isDecimal(final String number);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o syntaxi pro metematický výpočet.
	 * 
	 * Regulární výraz pro testování textu jsem sestavil dle možností využité
	 * knihovny.
	 * 
	 * Zrovna matematický výraz se testuje jen velmi těžko, zvláště k počtu
	 * dostupných funkcí (cos, sin, tan, atan, ...), proto netestuji přímo funkce
	 * ale jen písmena.
	 * 
	 * Což je také důvod proč je potřeba dodatečně testovat, zda zadaný text
	 * obsahuje alespoň znaméhko plus nebo mínu spoad. A je vhodné tento výraz
	 * testovat jako poslední, nebo jeden z posledních, protože jím mohou projí i
	 * prosté texty,jako je třeba název proměnné apod.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return true, pokud se jedná o syntaxi pro matematický výraz (převážně, až na
	 *         nějaké výjimky), jinak false.
	 */
	boolean isMathematicalExpression(final String command);
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o příkaz pro výpočet exponentu:
	 * 
	 * X ^ Y
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výče uvedenou syntaxi, jinak false
	 */
	boolean isExponent(final String command);
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje zadaou syntaxi (command), zda se jedná o syntaxi pro
	 * zavolání konstruktoru nějaké třídy v následující syntaxi:
	 * 
	 * new packageName. (anotherPackageName.) ClassName(parameters); (-> parameters
	 * jsou volitelné)
	 * 
	 * Jedná se pouze o zavolání příslušného konstruktoru, pokud se úspěšně zavolá,
	 * sice se tím vytvoří instance, ale tato instance se nikam ukládat nebude.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isCallConstructor(final String command);
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda zadaná uživatel zadal příkaz pro vytvoření nové
	 * instance ať už s parametry nebo bez - prostě příkaz pro vytvoření nové
	 * instance To, zda se bude obsahovat parametry nebo ne bude testováno dále při
	 * jejím vytvíření
	 * 
	 * Syntaxe: model.data.ClassName variableNae = new model.data.ClassName();
	 * Syntaxe: model.data.ClassName variableNae = new model.data.ClassName(p1, p2,
	 * ...);
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isNewInstanceCommand(final String command);
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda uživatel zadal příkaz ve formátu pro novou
	 * insntanci
	 * 
	 * Příklad: model.data.ClassName variableName = new model.data.ClassName();
	 * 
	 * nebo pokud nebudou balíčky: ClassName variableName = new ClasName();
	 * 
	 * případně nějaká kombinace obou výše uvedených příkladů
	 * 
	 * @param command
	 *            - zadaný příkaz
	 * 
	 * @return true, pokud uživatel zadal příkaz ve formátu vytvoření nové instance
	 *         třídy, jinak false
	 */
	boolean isNewInstance(final String command);
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda uživatel zadal příkaz ve formátu pro novou
	 * insntanci
	 * 
	 * Příklad: model.data.ClassName variableName = new
	 * model.data.ClassName(parameterName1, parameterName2, ...);
	 * 
	 * nebo pokud nebudou balíčky: ClassName variableName = new
	 * ClasName(parameterName, parameterName2, ...);
	 * 
	 * případně nějaká kombinace obou výše uvedených příkladů
	 * 
	 * @param command
	 *            - zadaný příkaz
	 * 
	 * @return true, pokud uživatel zadal příkaz ve formátu vytvoření nové instance
	 *         třídy, jinak false
	 */
	boolean isNewInstanceWithParameters(final String command);
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o zavolání metody nad nějakoku instancí
	 * třídy, v této syntaxi mohou a nemusí být parametry, jde pouze o zjištění, zda
	 * se jedná o zavolání metody, dále pokud obsahuje parametry nebo ne, se bude
	 * zjišťovat až před jejím zavoláním, ale to je již jiná část aplikace
	 * 
	 * Syntaxe variable.methodName();
	 * 
	 * Syntaxe variable.methodName(p);
	 * 
	 * Syntaxe variable.methodName(p1, p2, ...);
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isCallTheMethod(final String command);
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o syntaxi pro zavolání staticé metody
	 * nad třídou v diagramu tříd, ne nad instancí třídy nebo nad nějakým objektem v
	 * editoru příkazů - například nad listem apdo.
	 * 
	 * Syntaxe: packageName. (someOthersPackage. nextPackage.) ClassName .
	 * methodName(parameters);
	 * 
	 * musí být alespoň jeden balíček (ten výchozí), pak název třídy, pak název
	 * metody, parametry metody mohou a nemusí být, jsou volitelné, výrazem projde
	 * výše uvedená syntaxe s parametry i bez parametrů (- tj. prázdná závorka)
	 * 
	 * @param command
	 *            - příkaz zadaný uživatelem v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isCallStaticMethod(final String command);
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o příkaz ve formátu zavolání nějaké metody
	 * třídy metoda je bez parametrů
	 * 
	 * Příklad: variableName.NameOfMethod();
	 * 
	 * 
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz
	 * 
	 * @return true, pokud se jedná o příkaz pro zavolání metody, jinak false
	 */
	boolean isCallTheMethodWithoutParameters(final String command);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o příkaz ve formátu zavolná nějaké metody
	 * třídy - mtoda , který může obsahovat více parametrů
	 * 
	 * Příklad: variableName.NameOfMethod(parametrName1, parameterName2, ...);
	 * 
	 * - v parametru může být literál - číslo nebo hodnota proměnné nějaké třídy,
	 * nebo zavolán getr nad nějakou třídou
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz
	 * 
	 * @return true, pokud se jedná o příkaz pro zavolání metody, jinak false
	 */
	boolean isCallTheMethodWithParameters(final String command);
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o naplnění proměnné nějaké třídy nějakou
	 * konstantou - číslem:
	 * 
	 * Příklad: variableClassName.variable = X;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz do editoru
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isFullfillmentOfVariablesNumber(final String command);
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o příkaz pro naplnení hodnoty proměnné
	 * nějaké třídy pomocí jiné proměnné nějaké třídy:
	 * 
	 * Příklad: variableClassName.variable = (variableClassName.)variable;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz
	 * 
	 * @return true, pokud se jedná u výše uvedenou syntaxi, jinak false
	 */
	boolean isFullfillmentOfVariableVariable(final String command);	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o příkaz pro naplnení hodnoty proměnné
	 * nějaké třídy pomocí zavolání metotody - 'getru' nad nějakou instancí třídy
	 * nebo zavoláním statické metody nad třídou (ne instancí) v diagramu tříd.
	 * 
	 * Příslušná metoda musí vždy vracet stejný datový typ, jako je proměnná.
	 * 
	 * Příklad: - variableClassName.variable = variableClassName.getr();
	 * 
	 * - variableClassName.variable = packageName. (anotherPackageName.)
	 * ClassName.someStaticMethodName();
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz
	 * 
	 * @return true, pokud se jedná u výše uvedenou syntaxi, jinak false
	 */
	boolean isFullfillmentOfVariableGetMethod(final String command);
	
	
	
	
		
	
	/**
	 * Metoda, která zjistí, zda se jedná o příkaz pro naplnění proměnné v nějaké
	 * instanci třídy jinou instancí třídy, která vznikne zavoláním konstruktoru.
	 * 
	 * Syntaxe:
	 * 
	 * instanceName.variableName = new packageName. (anotherPackageName.)
	 * ClassName(parameters); (parameters jsou volitelné)
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isFullfilmentOfVariableByCallingConstructor(final String command);
	
	
	
	
	
	
	
	
	
	
	/*
	 * Následují operace s hodnotami:
	 * inkrementace, dekrementace
	 */
	
	
	/**
	 * Metoda, která zjistí, zda je zadán příkaz v syntaxi pro inkrementaci čísla:
	 * 
	 * Příklad: X++;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isIncrementValueBehind(final String command);
	
	
	
	/**
	 * Metoda, která zjistí, zdaje zadaán příkaz v syntaxi pro dekrementaci čísla:
	 * 
	 * Příklad: X--;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isDecrementValueBehind(final String command);
	
	
	
	/**
	 * Metoda, která zjistí, zdaje zadaán příkaz v syntaxi pro inkrementaci čísla:
	 * 
	 * Příklad: ++X;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isIncrementValueBefore(final String command);
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zdaje zadaán příkaz v syntaxi pro dekrementaci čísla:
	 * 
	 * Příklad: --X;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isDecrementValueBefore(final String command);
	
	
	
	
	/**
	 * Metoda, která zjistí, zdaje zadaán příkaz v syntaxi pro inkrementaci hodnoty
	 * v proměnné nějaké třídy:
	 * 
	 * 
	 * Příklad: ++ClassName.variableName;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isIncrementClassVariableBefore(final String command);
	
	
	
	
	/**
	 * Metoda, která zjistí, zdaje zadaán příkaz v syntaxi pro dekrementaci hodnoty
	 * v proměnné nějaké třídy:
	 * 
	 * Příklad: --ClassName.variableName;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isDecrementClassVariableBefore(final String command);
	
	
	
	
	/**
	 * Metoda, která zjistí, zdaje zadaán příkaz v syntaxi pro inkrementaci hodnoty
	 * v proměnné nějaké třídy:
	 * 
	 * Příklad: ClassName.variableName++;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isIncrementClassVariableBehind(final String command);
	
	
	
	
	/**
	 * Metoda, která zjistí, zdaje zadaán příkaz v syntaxi pro dekrementaci hodnoty
	 * v proměnné nějaké třídy:
	 * 
	 * Příklad: ClassName.variableName--;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isDecrementClassVariableBehind(final String command);
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda e jedná o syntaxi pro inkrementaci proměnne v
	 * editoru příkazů
	 * 
	 * Syntaxe: editorVariableName++;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isIncrementCommandEditorVariableBehind(final String command);
	
	
	
	/**
	 * Metoda, která zjistí, zda e jedná o syntaxi pro inkrementaci proměnne v
	 * editoru příkazů
	 * 
	 * Syntaxe: ++editorVariableName;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isIncrementCommandEditorVariableBefore(final String command);
	
	
	/**
	 * Metoda, která zjistí, zda e jedná o syntaxi pro dekrementaci proměnne v
	 * editoru příkazů
	 * 
	 * Syntaxe: editorVariableName--;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isDecrementCommandEditorVariableBehind(final String command);
	
	
	
	
	/**
	 * Metoda, která zjistí, zda e jedná o syntaxi pro dekrementaci proměnne v
	 * editoru příkazů
	 * 
	 * Syntaxe: --editorVariableName;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isDecrementCommandEditorVariableBefore(final String command);
	
	
	
	
	
	
	
	
	
	
	
	
	// Výpis hodnot do OutputEditoru:
	
	/**
	 * Metoda, která zjistí, zda uživatelem zadaný příkaz je v syntaxi, kdy chce
	 * vypsat nějaké hodnoty do "konzole", v tomto případě se jedná o mnou nazvaný
	 * tzv. OutputEditor neboli Editor výstupů
	 * 
	 * Pro výpis hodnot do konzole se bude jednat o následující syntaxi:
	 * 
	 * Příklad: print(value);
	 * 
	 * v této metodě: value = X - literál, metoda, proměnná (v instanti třídy nebo v
	 * editoru příkazů)
	 * 
	 * Dále se může jedna o kombinaci výpisů: print(referenceName.getMethod());
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz
	 * 
	 * @return true, pokud se bude jednato výše uvedenou syntaxi, jinak false
	 */
	boolean isPrintValueLiteral(final String command);
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o proměnnou nějaké třídy v syntaxi:
	 * 
	 * příklad: (-) classNameVariable.variable
	 * 
	 * @param command
	 *            - část příkazu od uživatele
	 * 
	 * @return true, pokud se jedná o načtení proměnné dané třídy, jinak false
	 */
	boolean isClassVariable(final String command);
	
	
	/**
	 * Metoda, která zjistí,k zda se jedná o proměnnou, kterou uživatel vytvořil v
	 * editoru příkazů
	 * 
	 * Příklad: variable; - středník není povinný
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz nebo jeho část - pro ověření, zda se
	 *            jedná o zmíněnou proměnnou
	 * 
	 * @return true, pokud se text v parametru command nachází v uvedené syntaxi,
	 *         jinak false
	 */
	boolean isVariableInEditor(final String command);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	 * Následují operace s hodnotami:
	 * syntaxe pro rozpoznání:
	 * 
	 * promenna, do ktere se ma vlozit hodnota musi byt pouze pormenna v tride nebo editoru
	 * to za = muze byt konstanta - literal (znak, text, cislo), metoda, proměnna ve tride ci editoru
	 * 
	 * vzit si datovy typ z proměnne pro vlozeni hodnoty a do toho vlozit výsledek pocetni operace
	 * 
	 * ClassNameVariable.variable = classNameVariable.variable + classNameVariable.variable;
	 * ClassNameVariable.variable = classNameVariable.variable - classNameVariable.variable;
	 * ClassNameVariable.variable = classNameVariable.variable * classNameVariable.variable;
	 * ClassNameVariable.variable = classNameVariable.variable / classNameVariable.variable;
	 * ClassNameVariable.variable = classNameVariable.variable % classNameVariable.variable;
	 * 
	 * 
	 * Vždy doplnit prikazy pro rozpoznání metody nebo promenne:
	 * napriklad: isVariable = isCallTheMethod + isVariable, apod - kombinace: (počítat i s parametry metod)
	 * 
	 * classNameVariable.variable = classNameVariable.getMethod() + classNameVariable.getMethod();
	 * classNameVariable.variable = classNameVariable.getMethod() - classNameVariable.getMethod();
	 * classNameVariable.variable = classNameVariable.getMethod() * classNameVariable.getMethod();
	 * classNameVariable.variable = classNameVariable.getMethod() / classNameVariable.getMethod();
	 * classNameVariable.variable = classNameVariable.getMethod() % classNameVariable.getMethod();
	 * 
	 * 
	 * classNameVariable.variable = isCallTheMethod + isVariable;
	 * classNameVariable.variable = isVariable + isMethod;	- všechny početní operace
	 */
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která jistí, zda se jedná o deklaraci proměnné v editoru příkazů v
	 * syntaxi:
	 * 
	 * (final) dataType variableName;
	 * 
	 * Konkrétní příklad: final byte var1;
	 * 
	 * a další primitívní datové typy
	 * 
	 * @param command
	 *            - zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokd se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isDeclarationEditorVariable(final String command);
	
	
	
	
	/**
	 * Metoda, která se pokusí rozpoznat syntaxi pro deklaraci a naplnění proměnné v
	 * editoru příkazů s naplněním jednou hodnotou a sice konstantou (logická
	 * hodnota, číslo, text, znak)
	 * 
	 * Možné syntaxe.
	 * 
	 * (final) dataType variableName = ...;
	 * 
	 * final String var = "some text!";
	 * 
	 * int var = 15.55;
	 * 
	 * f float var = 5256^5;
	 * 
	 * boolean var = true;
	 * 
	 * apod. pro primitivní datové typy
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isDeclarationAndFulfillmentEditorVariableConstant(final String command);
	
	
	
	/**
	 * Metoda, která se pokusí rozpoznat syntaxi pro deklaraci a naplnění proměnné v
	 * editoru příkazů s naplněním proměnné z instance třídy nebo z jiné proměnné,
	 * také deklarované v editoru příkazů
	 * 
	 * syntaxe:
	 * 
	 * (final) dataType variableName = referencename.variable;
	 * 
	 * (final) dataType variableName = variable;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isDeclarationAndFulfillmentEditorVariableVariable(final String command);
	
	
	
	
	/**
	 * Metoda, která se pokusí rozpoznat syntaxi pro deklaraci a naplnění proměnné v
	 * editoru příkazů s naplněním proměnné pomocí metody, která vrací hodnotu
	 * stejného datového typu, metoda může být s parametrem i bez parametrů
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isDeclarationAndFulfillmentEditorVariableGetMethod(final String command);
	
	
	

	
	/**
	 * Metoda, která zjistí, zda se jedná o příkaz pro naplnení proměnné, vytvořené
	 * v editoru příkazů zavolání metotody - getru nad nějakou třídou nebo zavoláním
	 * statické metody nad třídou (ne instancí) v diagramu tříd.
	 * 
	 * Příklad:
	 * 
	 * - variable = variableClassName.getr(parameters); - metoda, která vrací stený
	 * typ hodnoty (parameters jsou volitelné)
	 * 
	 * - variable = packageNme. (anotherPackageName.) ClassName .
	 * methodName(parameters); - metoda, která vrací stený typ hodnoty (parameters
	 * jsou volitelné)
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz
	 * 
	 * @return true, pokud se jedná u výše uvedenou syntaxi, jinak false
	 */
	boolean isFullfillmentOfEditorVariableGetMethod(final String command);
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o příkaz pro naplnení proměnné, vytvořené
	 * v editoru příkazů pomcí jiné proměnné nějaké třídy nebo proměnné v editoru
	 * příkazů:
	 * 
	 * Příklad: variable = (variableClassName.)variable;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz
	 * 
	 * @return true, pokud se jedná u výše uvedenou syntaxi, jinak false
	 */
	boolean isFullfillmentOfEditorVariableVariable(final String command);
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o naplnění proměnné, vytvořené v editoru
	 * příkazů nějakou konstantou - číslem: znakem nebo textem
	 * 
	 * Příklad: variable = X || "some text." || 'c';
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz do editoru
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isFullfillmentOfEditorVariableConstant(final String command);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se zadaná syntaxe v parametru shoduje s
	 * následující:
	 * 
	 * syntaxe pro naplnení proměnné (třícy nebo editoru příkazů) pomocí operací
	 * součet nekolika hodnot:
	 * 
	 * (classVariable.)variableName = var.getMethod() + editorVar;
	 * 
	 * (classVariable.)variableName = var.getMethod(p1, p2) + -520 + -editorVar +
	 * var.method();
	 * 
	 * (classVariable.)variableName = var.getMethod() + editorVar + "some text" +
	 * '.';
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isFulfillmentOfVariableUsingTotal(final String command);
	
	
	
	/**
	 * Metoda, která zjistí, zda se zadaná syntaxe v parametru shoduje s
	 * následující:
	 * 
	 * syntaxe pro naplnení proměnné (třídy nebo editoru příkazů) pomocí operace
	 * mínus - rozdíl nekolika hodnot:
	 * 
	 * (classVariable.)variableName = var.getMethod() - editorVar;
	 * 
	 * (classVariable.)variableName = var.getMethod(p1, p2) - -520 - -editorVar -
	 * var.method();
	 * 
	 * (classVariable.)variableName = var.getMethod() - editorVar - "some text" -
	 * '.';
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isFulfillmentOfVariableUsingDifference(final String command);
	
	
	
	/**
	 * Metoda, která zjistí, zda se zadaná syntaxe v parametru shoduje s
	 * následující:
	 * 
	 * syntaxe pro naplnení proměnné (třídy nebo editoru příkazů) pomocí operace
	 * součinu nekolika hodnot:
	 * 
	 * (classVariable.)variableName = var.getMethod() * editorVar;
	 * 
	 * (classVariable.)variableName = var.getMethod(p1, p2) * -520 * -editorVar *
	 * var.method();
	 * 
	 * (classVariable.)variableName = var.getMethod() * editorVar * "some text" *
	 * '.';
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isFulfillmentOfVariableUsingProduct(final String command);
	
	
	/**
	 * Metoda, která zjistí, zda se zadaná syntaxe v parametru shoduje s
	 * následující:
	 * 
	 * syntaxe pro naplnení proměnné (třídy nebo editoru) pomocí operace podílu
	 * nekolika hodnot:
	 * 
	 * (classVariable.)variableName = var.getMethod() / editorVar;
	 * 
	 * (classVariable.)variableName = var.getMethod(p1, p2) / -520 / -editorVar /
	 * var.method();
	 * 
	 * (classVariable.)variableName = var.getMethod() / editorVar / "some text" /
	 * '.';
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isFulfillmentOfVariableUsingShare(final String command);
	
	
	/**
	 * Metoda, která zjistí, zda se zadaná syntaxe v parametru shoduje s
	 * následující:
	 * 
	 * syntaxe pro naplnení proměnné (třídy či editoru příkazů) pomocí operace
	 * zbatek po dělení - modulo nekolika hodnot:
	 * 
	 * (classVariable.)variableName = var.getMethod() % editorVar;
	 * 
	 * (classVariable.)variableName = var.getMethod(p1, p2) % -520 % -editorVar %
	 * var.method();
	 * 
	 * (classVariable.)variableName = var.getMethod() % editorVar % "some text" %
	 * '.';
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isFulfillmentOfVariableUsingModulo(final String command);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o inkrementaci (postfixová nebo prefixová
	 * forma) proměnné v instanci třídy
	 * 
	 * Syntaxe: (-) referenceName.variable++ nebo: ++referenceName.variable
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru přkazů nebo jeho část pro
	 *            otestování, zda se jedná o inkrementaci proměnné v instanci třídy
	 * 
	 * @return true, pokud se jendá o výše uvedenou syntaxi, jinak false
	 */
	boolean isIncrementClassVariable(final String command);
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o dekrementaci proměnné v instanci třídy
	 * (postfixová nebo prefixová forma)
	 * 
	 * Syntaxe: (-) referenceName.variable-- nebo: --referenceName.variable
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů nebo jeho nějaká
	 *            část, například parametr metody, apod.
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isDecrementClassVariable(final String command);
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o syntaxi pro inkrementaci proměnné v
	 * editoru příkazů - buď v prefixové formě nebo postfixové formě
	 * 
	 * možné syntaxe:
	 * 
	 * (-) variableName ++
	 * 
	 * ++ variableName
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz nebo jeho část, např parametr metody
	 *            pro otestování, zda se jedná o zmíněnou inkrementaci v dané formě
	 *            - podobně
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isIncrementEditorVariable(final String command);
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o syntaxi pro dekremantaci proměnné v
	 * editoru příkazů - buď v prefixové nebo postfixové formě
	 * 
	 * možné syntaxe:
	 * 
	 * (-) variableName --
	 * 
	 * -- variableName
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz nebo jeho část, např parametr metody
	 *            pro otestování, zda se jedná o zmíněnou dekrementaci v dané formě
	 *            - podobně
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isDecrementEditorVariable(final String command);
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se text v aprametru této metody nachází v syntaxi
	 * pro deklaraci Listu - konkrétně instance ArrayListu z balíčku java.util
	 * 
	 * Syntaxe:
	 * 
	 * (final) List<DataType> variableName = new ArrayList<>();
	 * 
	 * (final) List<DataType> variableName = new ArrayList<>(SomeParameters);
	 * 
	 * SomeParameters - čísla, texty, znaky, zavolání metody s jedním parameter,
	 * hodnota z proměnné, apod - stejně jako parametry u zavolání metody či
	 * konstruktoru třídy
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz
	 *            
	 * @return true, pokud se jedná o jednu z výše uvedených syntaxí, jinak false
	 */
	boolean isCreateAndFullfillmentList(final String command);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o část příkazu pro deklaraci List, který
	 * má být final
	 * 
	 * Syntaxe:
	 * 
	 * final List<DataType> variableName
	 * 
	 * @param text
	 *            - část uživatelem zadaného příkazu
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isDeclarateListWithFinal(final String text);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda je zadán příkaz pro přidání hodnoty na konec
	 * uživatelem vytvořeného Listu v editoru příkazů. Přidání hodnoty pomocí metody
	 * add(value); nad daným listem:
	 * 
	 * Syntaxe:
	 * 
	 * variableNameForList.add(specificValue);
	 * 
	 * specificValue = vždy jena konkrétní hodnota, napří text, proměnná, čísla,
	 * apod.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isAddValueToEndOfList(final String command);
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o přidání hodnoty do listu, vytvořeného
	 * v editoru příkaů na zadaný index v listu (kolekci)
	 * 
	 * Ukázka syntaxe: var.add(index, specificValue);
	 * 
	 * index >= 0 && index < list.size
	 * 
	 * specificValue = hodnota, která se má vložit do kolekce - zálěži na datovém
	 * typu hodnot, které lze do listu vkládat
	 * 
	 * Konkrétní příklad, pokud se jedná o list s Integerem: var.add(1, -1);
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editrou příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isAddValueToIndexAtList(final String command);
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která ostestuje, zda se jedná o vymazání obsahu kolekce - list pomocí
	 * metody clear
	 * 
	 * Možná syntaxe:
	 * 
	 * referenceToList.clear();
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editrou příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isClearList(final String command);
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedna o získání hodnoty z kolekce na nějakém
	 * indexu v následující syntaxi:
	 * 
	 * referencetoList.get(X);
	 * 
	 * kde X je celé číslo >= 0
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz do editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isGetValueAtIndexOfList(final String command);
	
	
	
	
	
	/**
	 * Metoda, která nastaví hodnotu na zadaném indexu v kolekci - listu
	 * 
	 * možná syntaxe:
	 * 
	 * referenceToList.set(index, value);
	 * 
	 * index = celé číslo >= 0 && index < list.size
	 * 
	 * value = hodnota, která se ma vlozit na zadany index v listu - kolekci
	 * 
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz nebo jeho část
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi
	 */
	boolean isSetValueAtIndexOfList(final String command);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná příkaz pro konverzi Listu (kolekce) do
	 * jenorozměrného pole daného typu kolekce
	 * 
	 * Syntaxe: referenceToList.toArray();
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editrou příkazů
	 * 
	 * @return true, pokud se jená o výše uvedenou syntaxi, jinak false
	 */
	boolean isListToOneDimensionalArray(final String command);
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o příkaz, který má zjistit velikost /
	 * délku Listu - kolekce
	 * 
	 * Sysntaxe: referenceToList.size();
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů, nebo jeho část
	 *            (například parametr metody, konstruktoru, apod.)
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isListSize(final String command);
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o příkaz pro odebrání, resp. vymazání
	 * hodnoty z Listu - kolekce na zadaném indexu.
	 * 
	 * Syntaxe: referenceToList.remove(index); index = celé číslo >= 0 && index <
	 * kolekce.size();
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isRemoveValueAtIndexOfList(final String command);
	
	
	
	
	
	
	
	
	
	
	// Jednorozměrné pole:
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o příkaz pro vytvoření jednorozměrného
	 * pole ze "základních" datových typů Javy: byte, Byte, short, Short, int,
	 * Integer, long, Long, float, Float, double, Double, char, Character, boolean,
	 * Boolean, String s naplněním
	 * 
	 * Syntaxe: (final) dataType (výše)[] variableName = new dataType[] {value1,
	 * value2, ...};
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isDeclarationAndFullfillmentArray(final String command);
	
	
	
	
	/**
	 * Metoda, která slouží pro otestování, zda se jedná o syntaxi příkazu pro
	 * vytvoření / deklaraci jednorozměrného pole v následující syntaxi:
	 * 
	 * (final) dataType[] variableName = {value1, value2, ...};
	 * 
	 * dataType může být pouze jeden ze "základních" datových typů Javy: byte, Byte,
	 * short, Short, int, Integer, long, Long, float, Float, double, Double, char,
	 * Character, boolean, Boolean, String
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o syntaxi pro výše uvedený příkaz, jinak false.
	 */
	boolean isDeclarationAndFulfillmentArrayShort(final String command);
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o příkaz pro vytvoření jednorozměrného
	 * pole s definovanou dělkou pole.
	 * 
	 * Syntaxe:
	 * 
	 * dataType[] variableName = new dataType[size];
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editrou příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isDeclarationArrayWithLength(final String command);
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o syntaxi příkazu pro vytvoření proměnné
	 * v editoru příkazů. Tato proměnná je typu jednorozměrné pole typu jednoho ze
	 * "základních" datových typů Javy, jako je například String, int, double, ...
	 * 
	 * Toto pole se naplní zavooláním metody nad instancí třídy nebo pomocí statické
	 * metody nad třídou v diagramu tříd.
	 * 
	 * Touto metodou projdou následující syntaxe:
	 * 
	 * - (final) dataType[] variableName = instance.methodName(parameters); ->
	 * (parameters jsou volitelné a dataType může být například: byte, Byte, short,
	 * Short, int, Integer, long, Long, float, Float, double, Double, char,
	 * Character, boolean, Boolean, String)
	 * 
	 * - (final) dataType[] variableName = packageName. (anotherPackageName.)
	 * ClassName.staticMethodName(parameters); -> (stejne podmínky pro parameters a
	 * dataType jako výše)
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud bude se jedná o výraz v uvedené syntaxi (viz výše), jinak
	 *         false
	 */
	boolean isDeclarationArrayByCallingMethod(final String command);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o příkaz pro naplnění hodnoty na nějakém
	 * indexu v jednorozměrném poli pomocí konstanty,, například textu, znaku,
	 * čísla, apod.
	 * 
	 * Možná syntaxe: var[index] = value;
	 * 
	 * value = znak, text, číslo, ...
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isFullfillmentValueAtIndexOfArrayConstant(final String command);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o naplnění hodnoty na nějakém indexu
	 * jednorozměrného pole pomocí hodnoty jine proměnné, bud proměnná v editrou
	 * příkazů, v instanci třídy, nebo hodnota z jiného pole
	 * 
	 * Možná syntaxe:
	 * 
	 * var[index] = variable;
	 * 
	 * var[index] = referenceName.variable;
	 * 
	 * var[index] = variable[index];
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isFullfillmentValueAtIndexOfArrayVariable(final String command);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o naplnění hodnoty v poli na zadném
	 * indexu pomocí metody.
	 * 
	 * Možná syntaxe:
	 * 
	 * var[index] = referenceName.methodName(parameters);
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isFullfillmentValueAtIndexOfArrayMethod(final String command);
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o syntaxi příkazu pro získání hodnoty z
	 * pole na nějakém indexu.
	 * 
	 * "Tento příkaz se nachází v nějakém příkazu", tj. například u naplnění
	 * hodnoty, proměnné, metody, apod.
	 * 
	 * Možná syntaxe:
	 * 
	 * (-) variableArray [index]
	 * 
	 * @param text
	 *            - část uživatelem zadaného příkazu, (vetčinou se tato část nachází
	 *            za rovnáse)
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isGetValueFromArrayAtIndexInCommand(final String text);
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o příkaz pro získání hodnoty z pole na
	 * zadaném indexu v poli.
	 * 
	 * Možná syntaxe:
	 * 
	 * variableName [index];
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isGetValueFromArrayAtIndex(final String command);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o příkaz pro inkrementaci hodnoty v
	 * prefixové formě na zadaném indexu v jednorozměrném poli, vytvořeném
	 * uživatelem v editoru příkazů
	 * 
	 * Syntaxe:
	 * 
	 * ++ arrayName [index];
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isIncrementArrayValueBefore(final String command);
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o příkaz pro inkrementaci hodnoty v
	 * postfixové formě na zadaném indexu v jednorozměrném poli, vytvořeném
	 * uživatelem v editoru příkazů
	 * 
	 * Syntaxe:
	 * 
	 * arrayName [index] ++;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isIncrementArrayValueBehind(final String command);
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o příkaz pro dekrementaci hodnoty v
	 * prefixové formě na zadaném indexu v jednorozměrném poli, vytvořeném
	 * uživatelem v editoru příkazů
	 * 
	 * Syntaxe: -- arrayName [index];
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isDecrementArrayValueBefore(final String command);
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o příkaz pro dekrementaci hodnoty v
	 * postfixové formě na zadaném indexu v jednorozměrném poli, vytvořeném
	 * uživatelem v editoru příkazů
	 * 
	 * Syntaxe:
	 * 
	 * arrayName [index] --;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isDecrementArrayValueBehind(final String command);
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o příkaz pro inkrementaci hodnoty v
	 * jednorozměrném poli, vytvořeném uživatelem v editoru příkazů (postfixová
	 * forma)
	 * 
	 * v syntaxi:
	 * 
	 * (-) arrayName [index] ++
	 * 
	 * @param text
	 *            - část uživatelem zadaného příkazu (například parametr metody,
	 *            konstruktoru, apod.)
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isIncrementArrayValueInParameterBehind(final String text);
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o příkaz pro dekrementaci hodnoty v
	 * jednorozměrném poli, vytvořeném uživatelem v editoru příkazů (postfixová
	 * forma)
	 * 
	 * v syntaxi:
	 * 
	 * (-) arrayName [index] --
	 * 
	 * @param text
	 *            - část uživatelem zadaného příkazu (například parametr metody,
	 *            konstruktoru, apod.)
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isDecrementArrayValueInParameterBehind(final String text);
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o příkaz pro získání délky pole, resp.
	 * počtu položek - prvků v poli
	 * 
	 * Syntaxe:
	 * 
	 * arrayName.length; -> nejedná se o metodu, ale vlastnost, proto bez závorek
	 * 
	 * @param command
	 *            - uživatelem zadaná příkaz v editrou příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isGetArrayLength(final String command);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o příkaz pro zjištění délky pole pomocí
	 * vlastnosti length, nad daným polem.
	 * 
	 * dané pole se může nacházet například v parametru mtody, konstruktoru, pří
	 * naplnění hodnoty proměnné, apod.
	 * 
	 * Možná syntaxe:
	 * 
	 * (-) arrayName.length
	 * 
	 * Note: Středník na konci příkazů neni - může být v parametru, apod., dále na
	 * začátku může být negace hodnoty například pro délka pole je 5, tak ve
	 * výsledku bude mínus pět: -5, apod.
	 * 
	 * @param command
	 *            - uživatelem zadaná část příkazu v editoru příkazů, například se
	 *            výše uvedená syntaxe nachází v parametru metody, konstruktoru, pří
	 *            naplnění proměnné, apod.
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false
	 */
	boolean isGetArrayLengthInParameter(final String command);
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda sejedná o příkaz v syntaxi pro naplnění položky
	 * jednorozměrného pole návratovou hodnotou metody (metoda může být statická
	 * nebo v instanci nějaké třídy) v syntaxi:
	 * 
	 * referenceName.arrayVariableName[index] =
	 * model.data.ClassName.methodName(params, ...);
	 * 
	 * referenceName.arrayVariableName[index] = referenceName.methodName(params,
	 * ...);
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud sejedná o výše uvedenou syntaxi, jiank false.
	 */
	boolean isFulfillmentOfArrayAtIndexInInstanceByMethod(final String command);
	
	
	
	/**
	 * Metoda, která otestuje, zda sejedná o příkaz v syntaxi pro naplnění položky
	 * jednorozměrného pole hodnotou objektového typu a sice nově vytvořenou
	 * instancí nějaké třídy v daigramu tříd.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.arrayVariableName[index] = new model.data.ClassNaame(params,
	 * ...);
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud sejedná o výše uvedenou syntaxi, jiank false.
	 */
	boolean isFulfillmentOfArrayAtIndexInInstanceByConstructor(final String command);
	
	
	/**
	 * Metoda, která otestuje, zda sejedná o příkaz v syntaxi pro naplnění položky
	 * jednorozměrného pole hodnotou z nějaké proměnné, která musí být stejného
	 * datového typu jako příslušné pole. proměnná může být buď z instance třídy
	 * nebo z editoru příkazů.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.arrayVariableName[index] = referenceName.variableName;
	 * 
	 * nebo
	 * 
	 * referenceName.arrayVariableName[index] = variableNameInCommandEditor;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud sejedná o výše uvedenou syntaxi, jiank false.
	 */
	boolean isFulfillmentOfArrayAtIndexInInstanceByVariable(final String command);
	
	
	/**
	 * Metoda, která otestuje, zda sejedná o příkaz v syntaxi pro naplnění položky
	 * jednorozměrného pole nějakou hodnotou, například konstantou, například true,
	 * false, String, char apod. Dále se může jednat například o výpočetní operaci.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.arrayVariableName[index] = true
	 * 
	 * nebo
	 * 
	 * referenceName.arrayVariableName[index] = ';';
	 * 
	 * referenceName.arrayVariableName[index] = 5*5 + (5^2);
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud sejedná o výše uvedenou syntaxi, jiank false.
	 */
	boolean isFulfillmentOfArrayAtIndexInInstanceByConstant(final String command);
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o příkaz pro získání hodnoty z
	 * jednorozměrného pole na zadaném indexu, pole se nachází v instanci nějaké
	 * třídy.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.variableArrayname[index];
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isGetValueFromArrayAtIndexInInstance(final String command);
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o příkaz pro získání velikosti / délky
	 * pole, které se nachází v instancinějaké třídy, ta instance se nachází v
	 * diagramu instancí.
	 * 
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.variableArrayname[index]
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isGetLengthOfArrayInInstance(final String command);
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o syntaxi přkazu pro prefixovou
	 * inkrementaci položky v jednorozměrném poli v instancí nějaké třídy v diagramu
	 * instancí.
	 * 
	 * Syntaxe:
	 * 
	 * ++ referenceName.variableArrayName[index];
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isPrefixIncrementValueAtArrayInInstance(final String command);

	/**
	 * Metoda, která zjistí, zda se jedná o syntaxi přkazu pro prefixovou
	 * dekrementaci položky v jednorozměrném poli v instancí nějaké třídy v diagramu
	 * instancí.
	 * 
	 * Syntaxe:
	 * 
	 * -- referenceName.variableArrayName[index];
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isPrefixDecrementValueAtArrayInInstance(final String command);

	/**
	 * Metoda, která zjistí, zda se jedná o syntaxi přkazu pro postfixovou
	 * inkrementaci položky v jednorozměrném poli v instancí nějaké třídy v diagramu
	 * instancí.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.variableArrayName[index] ++;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isPostfixIncrementValueAtArrayInInstance(final String command);

	/**
	 * Metoda, která zjistí, zda se jedná o syntaxi přkazu pro postfixovou
	 * dekrementaci položky v jednorozměrném poli v instancí nějaké třídy v diagramu
	 * instancí.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.variableArrayName[index] --;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isPostfixDecrementValueAtArrayInInstance(final String command);
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o příkaz pro inkrementaci položky v
	 * jednorozměrném poli v nějaké instanci třídy a na začátku může a nemusí být
	 * mínus.
	 * 
	 * Syntaxe (bez středníku na konci):
	 * 
	 * (-) referenceName.variableArrayname[index] ++
	 * 
	 * @param command
	 *            - uživatelme zadaný příkaz v editoru příkaz nebo jeho část,
	 *            například parametr metody, konstruktoru apod.
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isPostFixIncrementWithMinus(final String command);
	
	/**
	 * Metoda, která zjistí, zda se jedná o příkaz pro dekrementaci položky v
	 * jednorozměrném poli v nějaké instanci třídy a na začátku může a nemusí být
	 * mínus.
	 * 
	 * Syntaxe (bez středníku na konci):
	 * 
	 * (-) referenceName.variableArrayname[index] --
	 * 
	 * @param command
	 *            - uživatelme zadaný příkaz v editoru příkaz nebo jeho část,
	 *            například parametr metody, konstruktoru apod.
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isPostFixDecrementWithMinus(final String command);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Dvourozměrné pole, ale pouze v instanci nějaké třídy, která se nachází v
	// diagramu instancí:
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se jedná o naplnění hodnoty na nějakém indexu ve
	 * dvourozměrném poli v nějaké instanci třídy návratovou hodnotou zavolané
	 * metodÿ.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName . variableArrayname[index][index] = ref.method(aprams);
	 * 
	 * referenceName . variableArrayname[index][index] =
	 * model.data.ClassName.methodName(aprams);
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isFulfillmentIndexAtTwoDimArrayInInstanceByMethod(final String command);
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o příkaz v syxntaxi pro naplnění hodnoty
	 * ve dvourozměrném poli v isntancí nějaké třídy hodnotou -> vytvořenou instancí
	 * nějaé třídy.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.variableArray [index] [index] = new model.data.ClassName();
	 * 
	 * referenceName.variableArray [index] [index] = new
	 * model.data.ClassName(params);
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isFulfillmentIndexAtTwoDimArrayInInstanceByConstructor(final String command);
	
		
	/**
	 * Metoda, která zjistí, zda se jedná o příkaz v syntaxi pro naplnění hodnoty
	 * dvourozměrného pole v nějaké ínstanci třídy na nějakém indexu nějakou
	 * proměnnou.
	 * 
	 * Syntaxe:
	 * 
	 * refernceName.variableArrayName [index][index] = reference.variableName;
	 * 
	 * refernceName.variableArrayName [index][index] = reference.variableName
	 * [index];
	 * 
	 * refernceName.variableArrayName [index][index] = reference.variableName
	 * [index] [index];
	 * 
	 * apod.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isFulfillmentIndexAtTwoDimArrayInInstanceByVariable(final String command);
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o příkaz v syntaxi pro naplnění hodnoty ve
	 * dvourozměrném poli v instanci nějaké třídy na nějakém indexu pomocí nějaké
	 * konstanty, tj. hodnota true, false, matematický výpočet apod.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName . variableName [index] [index] = true;
	 * 
	 * referenceName . variableName [index] [index] = false;
	 * 
	 * referenceName . variableName [index] [index] = 5+5+5 * (2 ^ 3);
	 * 
	 * referenceName . variableName [index] [index] = "some text";
	 * 
	 * ...
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isFulfillmentIndexAtTwoDimArrayInInstanceByConstant(final String command);
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o příkaz pro získání hodnoty z
	 * dvourozměrného pole na zadaném index, pole se nachází v nějaké instanci
	 * třídy, která je reprezentována v diagramu instancí.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName . variableArrayName [index] [index];
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isGetValueAtIndexAtTwoDimArrayInInstance(final String command);
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o příkaz pro získání položky z indexu
	 * dvourozměrného pole v nějaké instanci třídy, ale na konci není středník a na
	 * začátku může být mínus (pro negaci výsledné hodnoty).
	 * 
	 * Syntaxe:
	 * 
	 * (-) referenceName. variableName [index] [index]
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxi, jinak false.
	 */
	boolean isGetValueAtIndexAtTwoDimArrayInIntanceWithMinus(final String command);
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o syntaxi pro příkaz prefixové
	 * inkrementace hodnoty na zadaném indexu ve dvourozměrném poli v nějaké
	 * instanci třídy.
	 * 
	 * Syntaxe:
	 * 
	 * ++ referenceName . variableArrayName [index] [index];
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return true, pokud se jedá o výše uvedenou syntaxi, jinak false.
	 */
	boolean isPrefixIncrementValueAtIndexInTwoDimArrayInInstance(final String command);
	
	/**
	 * Metoda, která zjistí, zda se jedná o syntaxi pro příkaz prefixové
	 * dekrementace hodnoty na zadaném indexu ve dvourozměrném poli v nějaké
	 * instanci třídy.
	 * 
	 * Syntaxe:
	 * 
	 * -- referenceName . variableArrayName [index] [index];
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return true, pokud se jedá o výše uvedenou syntaxi, jinak false.
	 */
	boolean isPrefixDecrementValueAtIndexInTwoDimArrayInInstance(final String command);
	
	/**
	 * Metoda, která zjistí, zda se jedná o syntaxi pro příkaz postfixové
	 * inkrementace hodnoty na zadaném indexu ve dvourozměrném poli v nějaké
	 * instanci třídy.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName . variableArrayName [index] [index] ++;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return true, pokud se jedá o výše uvedenou syntaxi, jinak false.
	 */
	boolean isPostfixIncrementValueAtIndexInTwoDimArrayInInstance(final String command);
	
	/**
	 * Metoda, která zjistí, zda se jedná o syntaxi pro příkaz postfixové
	 * dekrementace hodnoty na zadaném indexu ve dvourozměrném poli v nějaké
	 * instanci třídy.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName . variableArrayName [index] [index] --;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return true, pokud se jedá o výše uvedenou syntaxi, jinak false.
	 */
	boolean isPostfixDecrementValueAtIndexInTwoDimArrayInInstance(final String command);
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o syntaxi pro příkaz pro inkrementaci
	 * hodnoty ve dvourozměrném poli v postfixové formě a na začáku může být mínus
	 * pro negaci hodnoty. (Bez středníku na konci, tento výraz se používá pouze pro
	 * zjištění parametru například metody, konstruktoru apod.) Syntaxe.
	 * 
	 * (-) referenceName.variableName [index] [index] ++
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxik, jinak false.
	 */
	boolean isPostFixIncrementValueatIndexInTwoDimarrayInInstanceWithMinus(final String command);
	
	/**
	 * Metoda, která zjistí, zda se jedná o syntaxi pro příkaz pro dekrementaci
	 * hodnoty ve dvourozměrném poli v postfixové formě a na začáku může být mínus
	 * pro negaci hodnoty. (Bez středníku na konci, tento výraz se používá pouze pro
	 * zjištění parametru například metody, konstruktoru apod.) Syntaxe.
	 * 
	 * (-) referenceName.variableName [index] [index] --
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return true, pokud se jedná o výše uvedenou syntaxik, jinak false.
	 */
	boolean isPostFixDecrementValueatIndexInTwoDimarrayInInstanceWithMinus(final String command);
}