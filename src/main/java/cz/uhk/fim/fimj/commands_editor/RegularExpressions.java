package cz.uhk.fim.fimj.commands_editor;

/**
 * Tato třída obsahuje implementované metody Rozhraní: RegularExpressionsInterface, kde jsou jejich signatury a
 * kontrakty
 * <p>
 * Editor příkazů volá postuponě jednotlivé metody z této třídy (ne všechny), aby zjistil, jaký typ Javovského příkazu
 * uživatel zadal, ten je dále zpracováván, ale to je již jiná část aplikace
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class RegularExpressions implements RegularExpressionsInterface {

	/**
	 * Tento regulární výraz jsem se pokoušel napsat v rámci možnosti použité
	 * knihovny, ale vzhledem k počtu možných funkcí a kombinací se mi to moc
	 * nepovedlo, tak jsem pouze napsal výraz, který v podstatě testuje využité
	 * znaky. Takže problém je například to, že tímto výrazem například projde i
	 * naplněné proměnné apod.
	 * 
	 * Asi nejdůležitější částí je, že na konci výrazu není středník, takže by se
	 * díky tomu měl od ostatních výrazů odlišit, ale je třeba jej testovat mezi
	 * posledními. Protože ostatní výrazy jsou blíže specifikované, například zmíněné
	 * naplnění proměnné nebo zavolání metody apod.
	 * 
	 * Také jsem v tomto výraze úmyslně nepovolil mezery, takže před otestováním je
	 * třeba veškeré bíle znaky odebrat, což je také jeden z důvodů pro rozlišení
	 * ostatních syntaxí, například u vytvoření instance apod. Tam je potřeba
	 * ClassName c = new apod. A za klíčovým slovem new musí být mezera.
	 */
	private static final String REG_EX_MATHEMATICAL_EXPRESSION = "^[a-zA-Z\\d\\.\\^\\+\\-\\*/\\(\\)%=(!=)\\<\\>\\|,\\&]*$";
	
	/**
	 * Zde se jedná v podstatě téměr o ten stejný výraz jako je v proměnné
	 * 'REG_EX_MATHEMATICAL_EXPRESSION', ale v tomto případě jde o to, že v
	 * matematickém výraze mohou být mezery.
	 * 
	 * Dále přímo tento výraz není doslova "regulární výraz", jedná se pouze o
	 * hodnoty, které může obsahovatmatematický výraz, tato proměnná je zde
	 * vytvořena pouze pro to, aby bylo možné jej vkládat do jiných regulárních
	 * výrazů, jako je například výraz pro naplnění proměnné apod. Abych do toho
	 * výrazu naplnění proměnné přidal možnost pro naplnění matematickou operací.
	 */
	private static final String ONLY_VALUES_FOR_MATHEMATICAL_EXPRESSION_WITH_SPACES = "[a-zA-Z\\d\\.\\^\\+\\-\\*/\\(\\)%=(!=)\\<\\>\\|,\\&\\s]*";
	
	
	
	
	/**
	 * Výraz pro klíčové slovo "new", vím, že v Javě je to povoleno pouze s malými
	 * písmeny, ale budu trochu benevolentní:
	 */
	private static final String KEY_NEW = "[nN][eE][wW]";
	
	/**
	 * Výraz pro parametry metody či konstruktoru.
	 */
	private static final String KEY_PARAMETERS = "\\s*[(]\\s*('.'|\".*\"|[\\w\\s()\\[\\],\\.\\^'\"\\+\\*/%-])*\\s*[)]\\s*";
	
	
	
	
	
	/**
	 * Proměnná, která obsahuje "základní" datové typy jazyka Java, jedná se o
	 * datové typy, kterých mohou nabývat například proměnné, vytvořené veditoru
	 * příkazů, to samé pro pole, vytvořená v editoru příkazů.
	 */
	private static final String KEY_JAVA_DATA_TYPES = "(byte|Byte|short|Short|int|Integer|long|Long|float|Float|double|Double|char|Character|boolean|Boolean|String)";
	
	
	
	
	// Následují regulární výrazy pro syntaxe pro editoru příkazů:
	
	

	
	
	
	
	// Regulární výraz pro výpis všech příkazů do editrou výstupů
	private static final String REG_EX_PRINT_COMMANDS = "^\\s*printCommands\\s*[(]\\s*[)]\\s*;\\s*$";
	
	
	/**
	 * Regulární výraz pro syntaxi příkazu, který slouží pro výpis proměnných
	 * vytvořených v editoru příkazů, tzv. dočasných proměnných.
	 * 
	 * Syntaxi: printTempVariables();
	 */
	private static final String REG_EX_PRINT_TEMP_VAIABLES_IN_EDITOR = "^\\s*printTempVariables\\s*\\(\\s*\\)\\s*;\\s*$";
	

	/**
	 * Regulární výraz pro ukončení aplikace v syntaxi: 'exit();'
	 */
	private static final String REG_EX_CLOSE_APP_BY_EXIT_COMMAND = "^\\s*exit\\s*\\(\\s*\\)\\s*;\\s*$";
		
	/**
	 * Regulární výraz, která slouží pro ukončení aplikace v syntaxi: 'exit(X);';
	 * 
	 * Za X je možné dosadit libovolné přoirozené číslo v intervalu <0 ; 59>.
	 */
	private static final String REG_EX_CLOSE_APP_BY_EXIT_WITH_COUNT_COMMAND = "^\\s*exit\\s*\\(\\s*(\\d|[0-5]\\s*\\d)\\s*\\)\\s*;\\s*$";
	
	/**
	 * Regulární výraz pro ukončení aplikace v syntaxi: 'System.exit(0);'
	 */	
	private static final String REG_EX_CLOSE_APP_BY_SYSTEM_EXIT_COMMAND = "^\\s*System\\s*\\.\\s*exit\\s*\\(\\s*0\\s*\\)\\s*;\\s*$";
	
	
	/**
	 * Regulární výraz, který slouží pro rozpoznání syntaxe: 'restart();'
	 * 
	 * Jedná se o příkaz, který slouží pro restartování aplikace.
	 */
	private static final String REG_EX_RESTART_APP_COMMAND = "^\\s*restart\\s*\\(\\s*\\)\\s*;\\s*$";

	/**
	 * Regulární výraz, který slouží pro rozpoznání syntaxe: 'restart(X);'
	 * 
	 * Za X je možné doplnit libovolné přirozené číslo v intervalu: < 0 ; 59>.
	 * 
	 * Jedná se o příkaz, který slouží pro restartování aplikace.
	 */
	private static final String REG_EX_RESTART_APP_WITH_COUNT_COMMAND = "^\\s*restart\\s*\\(\\s*(\\d|[0-5]\\s*\\d)\\s*\\)\\s*;\\s*$";
	
	/**
	 * Regulrání výraz, který slouží pro ukončení odpočtu do restartu nebo ukončení
	 * aplikace.
	 * 
	 * Syntaxe: cancelCountdown();
	 */
	private static final String REG_EX_CANCEL_COUNTDOWN_COMMAND = "^\\s*cancelCountdown\\s*\\(\\s*\\)\\s*;\\s*$";
	
	
	
	
	
	// Regulární výraz pro bíle znaky - mezery, tabulátory, apod.
	private static final String REG_EX_WHITE_SPACE = "^\\s*$";
	
	
	
	// Regulární výraz pro text v uvozovkách: (Syntaxe: "some text"):
	private static final String REG_EX_TEXT_IN_QUOTATION_MARKS = "^\\s*\".*\"\\s*$";
	
	
	
	// Výraz pro otestování názvu třídy ve tvaru: model.data.ClassName nebo ClassName
	private static final String REG_EX_CLASS_NAME = "^\\s*[a-zA-Z][\\w\\.]*\\s*$";
	
	// Výraz pro nalezení text názvu třídy i s balíčky a závorkami bez parametru: model.data.CLassName(); nebo ClassName();
	private static final String REG_EX_CLASS_NAME_WITH_BRACKET = "^\\s*[a-zA-Z]\\w*\\s*(\\s*\\.\\s*\\w+)*" + KEY_PARAMETERS + ";\\s*$";
	
	
	
	// Regulární výraz pro rozpoznání výreazu pro referenci na instanci tříy:
	private static final String REG_EX_REFERENCE_TO_INSTANCE_OF_CLASS = "^\\s*[a-zA-Z]\\w*\\s*$";
	
	
	
	
	private static final String REG_EX_INTEGER_NUMBER = "^\\s*-?\\s*[\\d\\s*]+\\s*$";
	private static final String REG_EX_DECIMAL_NUMBER = "^\\s*-?\\s*[\\d+\\s*]+\\s*\\.\\s*[\\d+\\s*]+\\s*$";
	
	
	
	
	
	/**
	 * Tento výraz je pouze "jedna" část pro exponentu - výraz: regExExponentValue -> ten je složen z tohoto výrazu.
	 */
	private static final String PART_OF_EXPONENT = "\\s*((-?\\s*[\\d\\s]+)|(-?\\s*[\\d\\s]+\\s*\\.\\s*[\\d\\s*]+)|(-|--)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*|(-|--)?\\s*[a-zA-Z]\\w*|(-|--)?\\s*\\w+\\s*(\\s*\\.\\s*\\w+)+" + KEY_PARAMETERS + ")\\s*";
	
	/**
	 * Regulární výraz pro zjištění, zda se jedná o výpočet exponentu ve formě (-)X
	 * ^ (-)X - kde X může být číslo (celé nebo reálné), proměnná z instnace třídy
	 * nebo z editoru příkazů, nebo metoda
	 * 
	 * Tento výraz je složen z jednoho výrazu, tak abych je nemusel psát vícekrát.
	 */
	private static final String REG_EX_EXPONENT_VALUE = "^" + PART_OF_EXPONENT + "\\s*\\^\\s*" + PART_OF_EXPONENT + "$";
	
	
	
	
	
	/**
	 * Výraz pro vytvoření instance jsem přepsal, aby se rozpoznával i s mezerami
	 * mezi balíčky.
	 */
	private static final String REG_EX_NEW_INSTANCE = "^\\s*\\w+\\s*(\\s*\\.\\s*\\w+)*\\s+[a-zA-Z]\\w*\\s*=\\s*" + KEY_NEW + "\\s+\\w+\\s*(\\s*\\.\\s*\\w+)+\\s*[(]\\s*[)]\\s*;\\s*$";
	
	
	
	
	/**
	 * Následující dva regulární vyárazy pro instanci s parametrem a více se liší
	 * akorát v čárce v závorce pro parametry - s více parametry musí obsahovat,
	 * jeden parametr ne
	 */
	private static final String REG_EX_NEW_INSTANCE_WITH_PARAMETERS = "^\\s*\\w+\\s*(\\s*\\.\\s*\\w+)+\\s+[a-zA-Z]\\w*\\s*=\\s*" + KEY_NEW + "\\s+\\w+\\s*(\\s*\\.\\s*\\w+)+" + KEY_PARAMETERS + ";\\s*$";
	
	

	/**
	 * Výraz pro zavolání konstruktoru (parametry mohou a nemusí být - nejsou povinné).
	 * 
	 * syntaxe: new packageName. (anotherPackageName.) ClassNmae(parameters); (-> parameers jsou volitelné)
	 */
	private static final String REG_EX_CALL_CONSTRUCTOR = "^\\s*" + KEY_NEW + "\\s+\\w+\\s*(\\s*\\.\\s*\\w+)+" + KEY_PARAMETERS + ";\\s*$";
	
	
	/**
	 * Výraz pro otestování, zda uživatel zadal syntaxo pro vytvoření nov= instance
	 * s parametry nebo bez - prostě nová instance
	 */
	private static final String REG_EX_NEW_INSTANCE_COMMAND = "^\\s*\\w+\\s*(\\s*\\.\\s*\\w+)*\\s+[a-zA-Z]\\w*\\s*=\\s*" + KEY_NEW + "\\s+\\w+\\s*(\\s*\\.\\s*\\w+)*" + KEY_PARAMETERS + ";\\s*$";
	
	
	

	/**
	 * Výraz pro zjištění, zda se jedná o zavolání metody nad instancí - parametry
	 * nejsou podstatně - s nimi i bez nich
	 */
	private static final String REG_EX_CALL_THE_METHOD = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+" + KEY_PARAMETERS + ";\\s*$";
	
	/**
	 * Výraz, zda se jedná o zavolání metody nad instancí bez parametrů:
	 */
	private static final String REG_EX_CALL_THE_METHOD_WITHOUT_PARAMETERS = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*[(]\\s*[)]\\s*;\\s*$";
	

	/**
	 * Výraz pro zjištění, zda se jedná o zavolání metody nad instancí s prametry
	 */
	private static final String REG_EX_CALL_THE_METHOD_WITH_PARAMETERS = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+" + KEY_PARAMETERS + ";\\s*$";
	
	/**
	 * Proměnná s regulárním výrazem pro rozpoznání, zda se jedná o zavolání
	 * statické metody nad třídou v diagramu tříd, ne nad instancí třídy v diagramu
	 * instancí.
	 * 
	 * Syntaxe: packageName. (someOtherPackage. nextPackage.) ClassName .
	 * methodName(parameters);
	 * 
	 * parametry jsou volitelné, výrazem projde i prázdná závorka i zadanén
	 * parametry. podmínkou je, aby třída byla alespoň v jednom balíčku, tj. byla
	 * syntaxe - alespoň jeden balíček, pak název třídy, pak název metody a pak ty
	 * kulaté závorky (s parametry uvnitř nebo ne, tj. je jedno, výrazem projde
	 * oboje.)
	 */
	private static final String REG_EX_CALL_STATIC_METHOD = "^\\s*\\w+\\s*(\\s*\\.\\s*\\w+){2,}" + KEY_PARAMETERS + ";\\s*$";
	
	
	
	
	
	
	
	/**
	 * V následujícím výrazu dále testovat, zda se jedná o naplnění proměnné vhodnou
	 * hodnoutou - literál, text - char, konstanta...
	 */
	private static final String REG_EX_FULL_FILLMENT_OF_VARIABLE_NUMBER = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*=\\s*('.'|\".*\"|true|false|-?\\s*[\\d\\s*]+\\s*[lLfFdD]?|-?\\s*[\\d\\s*]+\\s*\\.\\s*[\\d\\s*e]+\\s*[fFdD]?|" + ONLY_VALUES_FOR_MATHEMATICAL_EXPRESSION_WITH_SPACES + ")\\s*;\\s*$";
	
	

	/**
	 * Regulární výraz pro naplnení proměnné v instanci třídy nějakou hodnotou z
	 * proměnné, proměnné v editoru příkazů, pole a pole v instnaci třídy.
	 */
	private static final String REG_EX_FULL_FILLMENT_OF_VARIABLE_VARIABLE = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*=\\s*(((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*|-?\\s*[a-zA-Z]\\w*\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]|-?\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\.\\s*length|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\.\\s*length\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}\\s*(--|\\+\\+)?))\\s*;\\s*$";
	
	
	
	/**
	 * Tento výraz rozpozná syntaxi pro naplnení proměnné v instanci třídy pomocí metody nad jinou instancí třídy nebo pomocí statické metody nad třídou v diagramu tříd.
	 * 
	 * Možné syntaxe:
	 * 
	 * - instance.methodName(parameters); (parameters - jsou volitelné)
	 * nebo
	 * - packageName.ClassName.methodName(parameters); (parameters - jsou volitelné)
	 */
	private static final String REG_EX_FULLFILLMENT_OF_VARIABLE_GET_METHOD = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*=\\s*\\w+\\s*(\\s*\\.\\s*\\w+)+" + KEY_PARAMETERS + ";\\s*$";
	
	
	
	
	/**
	 * Regulární výraz pro naplnění proměnné v instanci třídy jinou instancí třídy,
	 * která vznikne zavoláním konstruktoru nějaké třídy.
	 * 
	 * Syntaxe:
	 * 
	 * instanceName.variableName = new packageName. (anotherPackageName.)
	 * ClassName(parameters); (-> parameters jsou volitlné)
	 */
	private static final String REG_EX_FULLFILLMENT_OF_VARIABLE_BY_CALLING_CONSTRUCTOR = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*=\\s*" + KEY_NEW + "\\s+\\w+\\s*(\\s*\\.\\s*\\w+)+" + KEY_PARAMETERS + ";\\s*$";
	
	
	
	
	
	
	
	
	
	/**
	 * Regulární výrazy pro naplnení proměnné, vytovřené v editoru příkazů - dočasná
	 * proměnná - po dobu otevřeného projektu: Výraz pro naplnění proměnné pomocí
	 * konstanty - čísla, text, znak
	 */
	private static final String REG_EX_FULLFILLMENT_OF_EDITOR_VARIABLE_CONSTANT = "^\\s*[a-zA-Z]\\w*\\s*=\\s*('.'|\".*\"|true|false|-?\\s*[\\d\\s*]+\\s*[lLdDfF]?|-?\\s*[\\d\\s*]+\\s*\\.\\s*[\\d\\s*e]+\\s*[fFdD]?|" + ONLY_VALUES_FOR_MATHEMATICAL_EXPRESSION_WITH_SPACES + ")\\s*;\\s*$";
	
	
	/**
	 * Výraz pro naplnění proměnné pomocí proměnné, nebo hodnoty z pole v instanci
	 * třídy apod.
	 */
	private static final String REG_EX_FULLFILLMENT_OF_EDITOR_VARIABLE_VARIABLE = "^\\s*[a-zA-Z]\\w*\\s*=\\s*(((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*|-?\\s*[a-zA-Z]\\w*\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]|-?\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\.\\s*length|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\.\\s*length\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}\\s*(--|\\+\\+)?))\\s*;\\s*$";
	

	/**
	 * Výraz pro naplnění proměnné v editoru příkazů pomocí pomocí návratové hodnoty
	 * z metody. Metoda může být zavolána nad instancí třídy nebo to může být
	 * statická (veřejná) metoda zavolána nad třídou v diagramu tříd.
	 * 
	 * Výrazem projdou následující syntaxe:
	 * 
	 * - variableNameInEditor = ReferenceName.methodName(parameters); (parameters -
	 * jsou volitelné)
	 * 
	 * - variableNameInEditor = packageName. (anotherPackageName.)
	 * ClassName.methodName(parameters); (parameters - jsou volitelné)
	 */ 
	private static final String REG_EX_FULLFILLMENT_OF_EDITOR_VARIABLE_GET_METHOD = "^\\s*[a-zA-Z]\\w*\\s*=\\s*\\w+\\s*(\\s*\\.\\s*\\w+)+" + KEY_PARAMETERS + ";\\s*$";
	
	
	
	
	
	
	
	
	
	
	
	// Výrazy pro rozpoznání syntaxe pro indkrementace či dekrementace čísla:
	private static final String REG_EX_INCREMENT_VALUE_BEHIND = "^\\s*-?\\s*\\d+\\s*\\.?\\s*\\d*\\s*\\+\\+\\s*;\\s*$";
	private static final String REG_EX_DECREMENT_VALUE_BEHIND = "^\\s*-?\\s*\\d+\\s*\\.?\\s*\\d*\\s*--\\s*;\\s*$";
	private static final String REG_EX_INCREMENT_VALUE_BEFORE = "^\\s*\\+\\+\\s*\\d+\\s*\\.?\\s*\\d*\\s*;\\s*$";
	private static final String REG_EX_DECREMENT_VALUE_BEFORE = "^\\s*--\\s*\\d+\\s*\\.?\\s*\\d*\\s*;\\s*$";
	
	
	
	
	// Proměnná v instancí třídy:
	private static final String REG_EX_INCREMENT_CLASS_VARIABLE_BEFORE = "^\\s*\\+\\+\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*\\s*;\\s*$";
	private static final String REG_EX_DECREMENT_CLASS_VARIABLE_BEFORE = "^\\s*--\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*\\s*;\\s*$";
	private static final String REG_EX_INCREMENT_CLASS_VARIABLE_BEHIND = "^\\s*-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*\\s*\\+\\+\\s*;\\s*$";
	private static final String REG_EX_DECREMENT_CLASS_VARIABLE_BEHIND = "^\\s*-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*\\s*--\\s*;\\s*$";
	
	
	
	
	// Proměnná v editoru příkazů:
	private static final String REG_EX_INCREMENT_EDITOR_VARIABLE_BEFORE = "^\\s*\\+\\+\\s*[a-zA-Z]\\w*\\s*;\\s*$";
	private static final String REG_EX_DECREMENT_EDITOR_VARIABLE_BEFORE = "^\\s*--\\s*[a-zA-Z]\\w*\\s*;\\s*$";
	private static final String REG_EX_INCREMENT_EDITOR_VARIABLE_BEHIND = "^\\s*-?\\s*[a-zA-Z]\\w*\\s*\\+\\+\\s*;\\s*$";
	private static final String REG_EX_DECREMENT_EDITOR_VARIABLE_BEHIND = "^\\s*-?\\s*[a-zA-Z]\\w*\\s*--\\s*;\\s*$";
	

	
	
	
	
	
	
	
	
	
	/**
	 * Regulární výraz pro odkaz na proměnnou intance třídy, Syntaxe:
	 * (-) referencename.variableName;
	 */
	private static final String REG_EX_CLASS_VARIABLE = "^\\s*-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*\\s*;\\s*$";
	
	/**
	 * Regulární výraz pro odaz na proměnnou, vytvořenou v editoru příkazů, Syntaxe: variableName
	 */
	private static final String REG_EX_VARIABLE_IN_EDITOR = "^\\s*-?\\s*[a-zA-Z]\\w*\\s*";
	
	/**
	 * Výraz pro zjištění, zda se jedná o konstantu - literál, v podobě textu, znaku
	 * nebo čísla - reálné desetinné, kladné / záporné:
	 */
	private static final String REG_EX_CONSTANT_LITERAL = "^\\s*('.'|\".*\"|true|false|(-?\\s*[\\d\\s*]+\\s*[lLdDfF]?)|(-?\\s*[\\d\\s*]+\\s*\\.\\s*[\\d\\s*]+\\s*[dDfF]?)|(((-?\\s*[\\d\\s]+)|(-?\\s*[\\d\\s]+\\s*\\.\\s*[\\d\\s*]+)|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*|-?\\s*[a-zA-Z]\\w*|[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*[(]\\s*('.'|\".*\"|true|false|[\\w\\s(),\\.'\"\\+-])*\\s*[)])\\s*\\^\\s*((-?\\s*[\\d\\s]+)|(-?\\s*[\\d\\s]+\\s*\\.\\s*[\\d\\s*]+)|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*|-?\\s*[a-zA-Z]\\w*|[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*[(]\\s*('.'|\".*\"|true|false|[\\w\\s(),\\.'\"\\+-])*\\s*[)])))+\\s*$";
	
	
	
	
	
	
	
	
	
	// Vytvoření "instance datového typu" v editoru a naplnění hodnotou:
	// např:
	// final Strin variableString = "some text";
	// apod s ostatními datovými typy: byte, int, double, float, short, long, char, boolean, string
	
	
	
	
	
	
	/**
	 * Regulární výraz pro deklaraci proměnné v editoru příkazů - pouze deklarace:
	 */
	private static final String REG_EX_DECLARATION_EDITOR_VARIABLE = "^\\s*(final\\s+)?" + KEY_JAVA_DATA_TYPES + "\\s+[a-zA-Z]\\w*\\s*;\\s*$";
	
	
	/**
	 * Regulární výraz pro deklaraci a zároveň naplnění proměnné, vytvořené v
	 * editoru příkazů jednou hodnotou: Ta hodnota může být konstanta - literál
	 * číslem, znakem, textem11
	 */
	private static final String REG_EX_DECLARATION_AND_FULFILLMENT_OF_EDITOR_VARIABLE_CONSTANT ="^\\s*(final\\s+)?" + KEY_JAVA_DATA_TYPES + "\\s+[a-zA-Z]\\w*\\s*=\\s*('.'|\".*\"|true|false|-?\\s*[\\d\\s*]+\\s*[lLfFdD]?|-?\\s*[\\d\\s*e]+\\s*\\.\\s*[\\d\\s*e]+\\s*[dDfF]?|" + ONLY_VALUES_FOR_MATHEMATICAL_EXPRESSION_WITH_SPACES + ")\\s*;\\s*$";
	
	
	
	/**
	 * Dále proměnná z editoru příkazů nebo z instance třídy:
	 */
	private static final String REG_EX_DECLARATION_AND_FULFILLMENT_OF_EDITOR_VARIABLE_VARIABLE = "^\\s*(final\\s+)?" + KEY_JAVA_DATA_TYPES + "\\s+[a-zA-Z]\\w*\\s*=\\s*(((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*|-?\\s*[a-zA-Z]\\w*\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]|-?\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\.\\s*length|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\.\\s*length\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}\\s*(--|\\+\\+)?))\\s*;\\s*$";
	
	
	/**
	 * Výraz pro vytvoření proměnné v editoru příkazů pomocí zavolání metody nad
	 * instancí třídy nebo statické metody nad třídou v diagramu tříd (ne nad
	 * instancí třídy), například v syntaxi:
	 * 
	 * - (final) dataType variableName = instance.methodName(parameters);
	 * (parameters jsou volitelné)
	 * 
	 * - (final) dataType variableName = packageName. (anotherPackageName.)
	 * ClassName.methodName(parameters); (parameters jsou volitelné)
	 */
	private static final String REG_EX_DECLARATION_AND_FULFILLMENT_OF_EDITOR_VARIABLE_GET_METHOD = "^\\s*(final\\s+)?" + KEY_JAVA_DATA_TYPES + "\\s+[a-zA-Z]\\w*\\s*=\\s*\\w+\\s*(\\s*\\.\\s*\\w+)+" + KEY_PARAMETERS + ";\\s*$";
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Regulární výrazy pro zjištění konkrétní početní operace pro naplnení hodnoty do proměnné v editoru příkazů nebo intanci třídy:	
	
	private static final String REG_EX_FULFILLMENT_OF_VARIABLE_USING_TOTAL = "^\\s*((final\\s+)?" + KEY_JAVA_DATA_TYPES + "\\s+[a-zA-Z]\\w*|[a-zA-Z]\\w*|[a-zA-Z]\\w*\\.[a-zA-Z]\\w*|[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\])\\s*=\\s*(\".*\"|[\\w\\s\\.()\\[\\],\\^\\+-])+\\s*\\+\\s*(\".*\"|[\\w\\s\\.()\\[\\],\\^\\+-])+\\s*(\\s*\\+\\s*(\".*\"|[\\w\\s\\.()\\[\\],\\^\\+-])+\\s*)*\\s*;\\s*$";
	private static final String REG_EX_FULFILLMENT_OF_VARIABLE_USING_DEFFERENCE = "^\\s*((final\\s+)?" + KEY_JAVA_DATA_TYPES + "\\s+[a-zA-Z]\\w*|[a-zA-Z]\\w*|[a-zA-Z]\\w*\\.[a-zA-Z]\\w*|[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\])\\s*=\\s*(\".*\"|[\\w\\s\\.()\\[\\],\\^\\+-])+\\s*-\\s*(\".*\"|[\\w\\s\\.()\\[\\],\\^\\+-])+\\s*(\\s*-\\s*(\".*\"|[\\w\\s\\.()\\[\\],\\^\\+-])+\\s*)*\\s*;\\s*$";
	private static final String REG_EX_FULFILLMENT_OF_VARIABLE_USING_PRODUCT = "^\\s*((final\\s+)?" + KEY_JAVA_DATA_TYPES + "\\s+[a-zA-Z]\\w*|[a-zA-Z]\\w*|[a-zA-Z]\\w*\\.[a-zA-Z]\\w*|[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\])\\s*=\\s*(\".*\"|[\\w\\s\\.()\\[\\],\\^\\+-])+\\s*\\*\\s*(\".*\"|[\\w\\s\\.()\\[\\],\\^\\+-])+\\s*(\\s*\\*\\s*(\".*\"|[\\w\\s\\.()\\[\\],\\^\\+-])+\\s*)*\\s*;\\s*$";
	private static final String REG_EX_FULFILLMENT_OF_VARIABLE_USING_SHARE = "^\\s*((final\\s+)?" + KEY_JAVA_DATA_TYPES + "\\s+[a-zA-Z]\\w*|[a-zA-Z]\\w*|[a-zA-Z]\\w*\\.[a-zA-Z]\\w*|[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\])\\s*=\\s*(\".*\"|[\\w\\s\\.()\\[\\],\\^\\+-])+\\s*/\\s*(\".*\"|[\\w\\s\\.()\\[\\],\\^\\+-])+\\s*(\\s*/\\s*(\".*\"|[\\w\\s\\.()\\[\\],\\^\\+-])+\\s*)*\\s*;\\s*$";
	private static final String REG_EX_FULFILLMENT_OF_VARIABLE_USING_MODULO = "^\\s*((final\\s+)?" + KEY_JAVA_DATA_TYPES + "\\s+[a-zA-Z]\\w*|[a-zA-Z]\\w*|[a-zA-Z]\\w*\\.[a-zA-Z]\\w*|[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\])\\s*=\\s*(\".*\"|[\\w\\s\\.()\\[\\],\\^\\+-])+\\s*%\\s*(\".*\"|[\\w\\s\\.()\\[\\],\\^\\+-])+\\s*(\\s*%\\s*(\".*\"|[\\w\\s\\.()\\[\\],\\^\\+-])+\\s*)*\\s*;\\s*$";
			
			
	
	
	
	
	
	
	// Regulární výraz, který rozpozná zda se jedná výpis hodnoty, například textu, hodnoty proměnné, návratové hodnoty metody, apod.
	// v syntaxi: print("some text.");   
	// print(referenceName.getMethod(p1, p2, ...));
	// nebo v kombinaci: print(referenceName.getMethod + ", " + variableName + ", variable in instance of class: " + referenceName.variableName, ...);
	// Hodnoty pro výpis je rozdělen dle symbolu plus, tak je třeba dávat pozor, kam je daný symbol umísteň
	// Note: nefungují v tomto příkazů aritmetické operace, například když se dají dva výrazy do závorky jako například v eclipse!!!
	private static final String REG_EX_PRINT_VALUE_LITERAL = "^\\s*print\\s*[(]\\s*('.'|\".*\"|[\\w\\s()\\[\\],\\.\\^'\"\\+\\*/%-]|-?\\s*[\\d\\s*]+|-?\\s*[\\d\\s*]+\\s*\\.\\s*[\\d\\s*]+|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*|-?\\s*[a-zA-Z]\\w*|[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*[(]\\s*('.'|\".*\"|[\\w\\s()\\[\\],\\.\\^'\"\\+\\*/%-])*\\s*[)]\\s*|-?\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]|	)*\\s*[)]\\s*;\\s*$";
	
	
	
	
	
	
	
	// Regulární výraz pro rozpoznání syntaxe:  (-) referenceName.variable++    nebo: ++referenceName.variable   
	// syntaxe pro inkrementace (postfixova nebo prefixova forma inkrementace) proměnne v instanci třídy
	private static final String REG_EX_INCREMENT_CLASS_VARIABLE = "^\\s*(-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*\\s*\\+\\+|\\+\\+\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*)\\s*$";
	
	
		
	
	// Regulární výraz pro rozpoznání syntaxe: (-) referenceName.variable--    nebo: --referenceName.variable
	// výraz pro dekrementaci hodnoty proměnné v instance třídy (postfixová nebo prefixová forma)
	private static final String REG_EX_DECREMENT_CLASS_VARIABLE = "^\\s*(-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*\\s*--|--\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*)\\s*$";
	
	
	
	
	
	
	// Regulární výraz pro rozpoznání syntaxe pro inkrementaci proměnné v editoru příkzaů:
	// Syntaxe: ++ editorVariable nebo (-) editorVariable ++
	private static final String REG_EX_INCREMENT_EDITOR_VARIABLE = "^\\s*(\\+\\+\\s*[a-zA-Z]\\w*|-?\\s*[a-zA-Z]\\w*\\s*\\+\\+)\\s*$";
	
	// Regulární výraz pro rozpoznání syntaxe pro dekremetnaci proměnné v editoru příkazů
	// syntaxe: -- variableName nebo (-) variableName--
	private static final String REG_EX_DECREMENT_EDITOR_VARIABLE = "^\\s*(--\\s*[a-zA-Z]\\w*|-?\\s*[a-zA-Z]\\w*\\s*--)\\s*$";
	
	
	
	
	
	
	
	
	
	
	/**
	 * Regulární výraz pro zjištění, zda se jedná o syntaxi pro vytvoření kolekce -
	 * ArrayListu (s parametry nebo bez nich) na tom ted nezáleží, pouze, zda se
	 * jedná u to vytvoření instance Arraylistu nebo ne:
	 */
	private static final String REG_EX_CREATE_AND_FULFILLMENT_LIST = "^\\s*(final\\s+)?List\\s*<\\s*(Byte|Short|Integer|Long|Float|Double|Character|Boolean|String)\\s*>\\s*[a-zA-Z]\\w*\\s*=\\s*new\\s+ArrayList\\s*<\\s*>\\s*[(]\\s*('.'|\".*\"|true|false|[\\w\\s()\\[\\],\\.'\"\\+-]|" + ONLY_VALUES_FOR_MATHEMATICAL_EXPRESSION_WITH_SPACES + ")*\\s*[)]\\s*;\\s*$";
	
	
	/**
	 * Regulární výraz, který otestuje, zda se jedná o deklaraci listu a obsahuje
	 * text final:
	 */
	private static final String REG_EX_DECLARATION_LIST_WITH_FINAL = "^\\s*final\\s+List\\s*<\\s*\\w+\\s*>\\s*[a-zA-Z]\\w*\\s*$";
	
	// regulární výraz pro přidání nové hodnoty na konec zadané kolekce:
	// Syntaxe: referenceToList.add(value);
	private static final String REG_EX_ADD_VALUE_TO_END_OF_LIST = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*add" + KEY_PARAMETERS + ";\\s*$";
	
	// Regulární výraz pro přidání hodnoty do kolekce na zadaném indexu:
	// Syntaxe: referenceToList.add(index, value);
	private static final String REG_EX_ADD_VALUE_TO_INDEX_AT_LIST = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*add\\s*[(]\\s*\\d+\\s*,\\s*('.'|\".*\"|[\\w\\s()\\[\\],\\.\\^'\"\\+\\*/%-])*\\s*[)]\\s*;\\s*$";
	
	
	// Regulární výraz pro odstranění obsahu kolekce pomocí metody clear()  nad zadanou kolekcí:
	// Syntaxe: referenceToList.clear();
	private static final String REG_EX_CLEAR_LIST = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*clear\\s*[(]\\s*[)]\\s*;\\s*$";
	
	
	/**
	 * Regulární výraz pro získání hodnoty z nějakého indexu nad koleci:
	 * list.get(index);
	 */
	private static final String REG_EX_GET_VALUE_AT_INDEX_OF_LIST = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*get\\s*[(]\\s*\\d+\\s*[)]\\s*;\\s*$";
	
	
	// Regulární výraz pro nastavení hodnoty v Listu - kolekce na zadaném indexu - pozici:
	// syntaxe: referenceToList.get(index, value);
	private static final String REG_EX_SET_VALUE_AT_INDEX_OF_LIST = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*set\\s*[(]\\s*\\d+\\s*,\\s*('.'|\".*\"|true|false|[\\w\\s()\\[\\]\\.'\"\\+-]|" + ONLY_VALUES_FOR_MATHEMATICAL_EXPRESSION_WITH_SPACES + ")*\\s*[)]\\s*;\\s*$";
	
	
	// Regulární výraz pro příkaz, který vrátí jednorozměrné pole z list
	// neboli převede List - kolekci do jednorozměrného pole pomocí metody referenceToList.toArray():
	private static final String REG_EX_LIST_TO_ONE_DIMENSIONAL_ARRAY = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*toArray\\s*[(]\\s*[)]\\s*;\\s*$";
	
	
	// Regulární výraz pro testování, zda se jedná o příkaz pro zjištění velikost / délky Listu - kolekce:
	// Syntaxe: referenceToList.size();
	private static final String REG_EX_LIST_SIZE = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*size\\s*[(]\\s*[)]\\s*;\\s*$";
	
	
	/**
	 * Regulární výraz pro zjištění, zda se jedná o příkaz pro odebrání - vymazání
	 * honoty na zadaném inexu v kolekce -Listu:
	 */
	private static final String REG_EX_REMOVE_VALUE_AT_INDEX_OF_LIST = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*remove\\s*[(]\\s*\\d+\\s*[)]\\s*;\\s*$";
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Jednorozměrné pole:
	// DataType = "základní datové typy Javy": byte|Byte|short|Short|int|Integer|long|Long|float|Float|double|Double|char|Character|boolean|Boolean|String
	// Deklarace jednorozměrného pole s naplněním, Syntaxe: (final) dataType[] variable = new dataType[]{values}
	private static final String REG_EX_DECLARATION_AND_FULFILLMENT_ARRAY = "^\\s*(final\\s+)?" + KEY_JAVA_DATA_TYPES + "\\s*\\[\\s*\\]\\s*[a-zA-Z]\\w*\\s*=\\s*new\\s+" + KEY_JAVA_DATA_TYPES + "\\s*\\[\\s*\\]\\s*\\{('.'|\".*\"|true|false|[\\w\\s()\\[\\],\\.'\"\\+-]|" + ONLY_VALUES_FOR_MATHEMATICAL_EXPRESSION_WITH_SPACES + ")*\\}\\s*;\\s*$";
	
	
	// syntaxe: (final) dataType[] variableName = {values};
	// DataType = "základní datové typy Javy": byte|Byte|short|Short|int|Integer|long|Long|float|Float|double|Double|char|Character|boolean|Boolean|String
	private static final String REG_EX_DECLARATION_AND_FULFILLMENT_ARRAY_SHORT = "^\\s*(final\\s+)?" + KEY_JAVA_DATA_TYPES + "\\s*\\[\\s*\\]\\s*[a-zA-Z]\\w*\\s*=\\s*\\{\\s*('.'|\".*\"|true|false|[\\w\\s()\\[\\],\\.'\"\\+-]|" + ONLY_VALUES_FOR_MATHEMATICAL_EXPRESSION_WITH_SPACES + ")*\\}\\s*;\\s*$";
	
	
	// Regulární výraz pro deklaraci jednorozměrného pole s definovanou velikostí:
	// Syntaxe: dataType[] variable = new dataType[size];
	private static final String REG_EX_DECLARATION_ARRAY_WITH_LENGTH = "^\\s*(final\\s+)?" + KEY_JAVA_DATA_TYPES + "\\s*\\[\\s*\\]\\s*[a-zA-Z]\\w*\\s*=\\s*new\\s+" + KEY_JAVA_DATA_TYPES + "\\s*\\[\\s*\\d+\\s*\\]\\s*;\\s*$";
	
	/**
	 * Regulární výraz pro rozpoznání syntaxe pro deklaraci jednorozměrného pole
	 * "základních" datových typů Javy s naplněním pomocí zavolání metody nad
	 * instancí třídy nebo statické metody nad třídou v diagramu tříd (ne instancí).
	 * 
	 * Jedna se o syntaxi:
	 * 
	 * - (final) dataType [] variableName = instance.methodName(parameters);
	 * (parameters jsou volitelné a dataType jsou výchozí datové typy Javy)
	 * 
	 * - (final) dataType [] variableName = packageName. (anotherPackageName.)
	 * ClassName.methodName(parameters); (parameters jsou volitelné a dataType jsou
	 * výchozí datové typy Javy)
	 */
	private static final String REG_EX_DECLARATION_ARRAY_WITH_METHOD = "^\\s*(final\\s+)?" + KEY_JAVA_DATA_TYPES + "\\s*\\[\\s*\\]\\s*[a-zA-Z]\\w*\\s*=\\s\\w+\\s*(\\s*\\.\\s*\\w+)+" + KEY_PARAMETERS + ";\\s*$";
	
	
	
	// Regulární výraz pro naplnění hodnoty na nějakém indexu v jednorozměrném poli konstanou - číslo, znak, text, apod.
	// Syntaxe: var[index] = value;
	private static final String REG_EX_FULFILLMENT_VALUES_AT_INDEX_OF_ARRAY_CONSTANT = "^\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]\\s*=\\s*('.'|\".*\"|true|false|-?\\s*[\\d\\s*]+\\s*[lLfFdD]?|-?\\s*[\\d\\s*]+\\s*\\.\\s*[\\d\\s*e]+\\s*[fFdD]?|" + ONLY_VALUES_FOR_MATHEMATICAL_EXPRESSION_WITH_SPACES + ")\\s*;\\s*$";
	
	
	// Regulární výraz pro naplnění nějaké hodnoty v poli na nějakém indexu pomocí jiné proměnné:
	// Syntaxe: var[index] = variable;
	// Syntaxe: var[index] = reference.variableArray[index];
	// ...
	private static final String REG_EX_FULFILLMENT_VALUE_AT_INDEX_OF_ARRAY_VARIABLE = "^\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]\\s*=\\s*(((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*|-?\\s*[a-zA-Z]\\w*\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]|-?\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\.\\s*length|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\.\\s*length\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}\\s*(--|\\+\\+)?))\\s*;\\s*$";
	
	
	
	
	/**
	 * Rewgulární výraz pro naplnění hodnoty na nějakém indexu v poli pomocí zavolání metody:
	 * 
	 *  Syntaxe: 
	 *  
	 *  - var[index] = referenceName.methodName(parameters); (parameters jsou volitelné)
	 *  nebo statickou metodou:
	 *  - var[index] = packageName. (anotherPackageName.) ClassName. methodName(parameters); (parameters jsou volitelné)
	 */
	private static final String REG_EX_FULFILLMENT_VALUE_AT_INDEX_OF_ARRAY_METHOD = "^\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]\\s*=\\s*\\w+\\s*(\\s*\\.\\s*\\w+)+\\s*[(]\\s*('.'|\".*\"|[\\w\\s()\\[\\],\\.'\"\\+-])*\\s*[)]\\s*;\\s*$";
	
	
	

	// Regulární výraz pro otestování, zda se jedná o získání hodnoty z jednorozměrného pole:
	// Syntaxe: (-) variableArray [index]
	private static final String REG_EX_GET_VALUE_FROM_ARRAY_AT_INDEX_IN_COMMAND = "^\\s*-?\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]\\s*$";
	

	
	// Regulární výraz pro získání hodnoty z pole na zadaném indexu:
	// Syntaxe: variableName [index];
	private static final String REG_EX_GET_VALUE_FROM_ARRAY_AT_INDEX = "^\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]\\s*;\\s*$";
	

	// Regulární výraz pro rozponání syntaxe příkazu pro inkrementaci hodnoty v jednorozmerném poli,
	// vytvořeném v editoru příkazů, syntaxe: ++arrayName[index];	(prefixová forma)
	private static final String REG_EX_INCREMENT_ARRAY_VALUE_BEFORE = "^\\s*\\+\\+\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]\\s*;\\s*$";
	
	
	// Regulární výraz pro rozponání syntaxe příkazu pro inkrementaci hodnoty v jednorozmerném poli,
	// vytvořeném v editoru příkazů, syntaxe: arrayName[index]++;    (postfixová forma)
	private static final String REG_EX_INCREMENT_ARRAY_VALUE_BEHIND = "^\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]\\s*\\+\\+\\s*;\\s*$";
	
	
	
	// Regulární výraz pro rozponání syntaxe příkazu pro dekrementaci hodnoty v jednorozmerném poli,
	// vytvořeném v editoru příkazů, syntaxe: arrayName[index]--;    (postfixová forma)
	private static final String REG_EX_DECREMENT_ARRAY_VALUE_BEHIND = "^\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]\\s*--\\s*;\\s*$";
	
	
	// Regulární výraz pro rozponání syntaxe příkazu pro dekrementaci hodnoty v jednorozmerném poli,
	// vytvořeném v editoru příkazů, syntaxe: --arrayName[index];    (prefixová forma)
	private static final String REG_EX_DECREMENT_ARRAY_VALUE_BEFORE = "^\\s*--\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]\\s*;\\s*$";
	
	
	// Regulární výraz pro inkrementaci hodnoty na zadaném indexu v poli, vytvořeným v editrou příkazů, v následující syntaxi:
	// (-) arrayName [index] ++
	// Syntaxe pro inkrementaci hodnoty v jednorozměrném poli, a tento příkaz se nachází v parametru například metody, konstruktoru, apod.
	private static final String REG_EX_INCREMENT_ARRAY_VALUE_IN_PARAMETER_BEHIND = "^\\s*-?\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]\\s*\\+\\+\\s*$";
	
	// Regulární výraz pro dekrementaci hodnoty na zadaném indexu v jednorozměrném poli, vytvořeným v editoru příkazů
	// Syntaxe: (-) arrayName [index] --
	private static final String REG_EX_DECREMENT_ARRAY_VALUE_IN_PARAMETER_BEHIND = "^\\s*-?\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]\\s*--\\s*$";
	
	
	// Regulární výraz pro získání délky pole, resp počtu položek - prvků v poli,
	// Syntaxe: arrayName.length; 	-> jedná se o vlastnost né metody, proto bez závorek!:
	private static final String REG_EX_ARRAY_LENGTH = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*length\\s*;\\s*$";
	
	
	// Regulární výraz pro získání délky pole v coby proměnné v editoru příkazů, a tento příkaz se nachází někde v parametru
	// metody, kontruktoru, naplnění proměnné, apod.
	// Syntaxe: (-) arrayName.length
	private static final String REG_EX_ARRAY_LENGTH_IN_PARAMETER = "^\\s*-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*length\\s*$";
	
	
	
	
	
	
	
	
	
	
	// Výrazy pro naplnění položky na nějakém indexu v jednorozměrném poli, které se
	// nachází v instanci nějaké třídy:
	
	/**
	 * Regulární výraz pro naplnění položky v jednorozměrném poli v instanci nějaké
	 * třídy návratovou hodnotu po zavolání metody.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.arrayVariableName[index] =
	 * model.data.ClassName.methodName(parames, ...);
	 * 
	 * referenceName.arrayVariableName[index] = referenceName.methodName(parames,
	 * ...);
	 */
	private static final String REG_EX_FULFILLMENT_OF_ARRAY_AT_INDEX_IN_INSTANCE_BY_METHOD = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]\\s*=\\s*\\w+\\s*(\\s*\\.\\s*\\w+)+" + KEY_PARAMETERS + ";\\s*$";
	
	/**
	 * Reguulární výraz pro naplnění položky v jednorozměrném poli v nějaké instanci
	 * třídy hodnotou, resp. novou instancí příslušné třídy, ve které se zavolá
	 * příslušný konstruktor.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.arrayVariableName[index] = new model.data.ClassName(parames,
	 * ...);
	 */
	private static final String REG_EX_FULFILLMENT_OF_ARRAY_AT_INDEX_IN_INSTANCE_BY_CONSTRUCTOR = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]\\s*=\\s*" + KEY_NEW + "\\s+\\w+\\s*(\\s*\\.\\s*\\w+)+" + KEY_PARAMETERS + ";\\s*$";
	
	/**
	 * Regulární výraz pro naplnění položky v jednorozměrném poli v nějaké instnaci
	 * nějaké třídy v diagramu instancí proměnnou vytvořenou v editoru příkazů nebo
	 * nějakou proměnnou v instanci nějaké třídy.
	 * 
	 * syntaxe:
	 * 
	 * referenceName.arrayVariableName[index] = variableName;
	 * 
	 * referenceName.arrayVariableName[index] = referenceName.variableName;
	 */
	private static final String REG_EX_FULFILLMENT_OF_ARRAY_AT_INDEX_IN_INSTANCE_BY_VARIABLE = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]\\s*=\\s*(((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*|-?\\s*[a-zA-Z]\\w*\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]|-?\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\.\\s*length|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\.\\s*length\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}\\s*(--|\\+\\+)?))\\s*;\\s*$";
		
	/**
	 * Regulární výraz pro naplnění položky v jednorozměrném poli v nějaké instanci
	 * třídy konstantou nebo početní operací.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.arrayVariableName[index] = true;
	 * 
	 * referenceName.arrayVariableName[index] = 5 * 5 + (2^2);
	 * 
	 * referenceName.arrayVariableName[index] = "some text";
	 * 
	 * ...
	 */
	private static final String REG_EX_FULFILLMENT_OF_ARRAY_AT_INDEX_IN_INSTANCE_BY_CONSTANT = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]\\s*=\\s*('.'|\".*\"|true|false|-?\\s*[\\d\\s*]+\\s*[lLfFdD]?|-?\\s*[\\d\\s*]+\\s*\\.\\s*[\\d\\s*e]+\\s*[fFdD]?|" + ONLY_VALUES_FOR_MATHEMATICAL_EXPRESSION_WITH_SPACES + ")\\s*;\\s*$";

	
	
	
	
	
	
	
	
	/**
	 * Regulární výraz pro otestování syntaxe pro získání hodnoty z jednorozměrného
	 * pole, které se nachází v instancí nějaké třídy.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.variableArrayname[index]
	 */
	private static final String REG_EX_GET_VALUE_AT_INDEX_OF_ARRAY_IN_INSTANCE = "^\\s*\\-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]\\s*;\\s*$";
			
	/**
	 * Regulární výraz pro otestování syntaxe pro získání velikost pole, které se
	 * nachází v instanci nějaké třídy.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.variableArryName.length;
	 */
	private static final String REG_EX_GET_LENGTH_OF_ARRAY_IN_INSTANCE = "^\\s*\\-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\.\\s*(length)\\s*;\\s*$";
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Regulární výraz pro prefixovou inkrementaci položky v jednorozměrném poli v
	 * instancí třídy.
	 * 
	 * Syntaxe: ++ referenceName.variableArrayname [index];
	 */
	private static final String REG_EX_PREFIX_INCREMENT_VALUE_AT_ARRAY_IN_INSTANCE = "^\\s*\\+\\+\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]\\s*;\\s*$";
	
	/**
	 * Regulární výraz pro prefixovou dekrementaci položky v jednorozměrném poli v
	 * instancí třídy.
	 * 
	 * Syntaxe: -- referenceName.variableArrayname [index];
	 */
	private static final String REG_EX_PREFIX_DECREMENT_VALUE_AT_ARRAY_IN_INSTANCE = "^\\s*--\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]\\s*;\\s*$";
	
	/**
	 * Regulární výraz pro postfixovou inkrementaci položky v jednorozměrném poli v
	 * instancí třídy.
	 * 
	 * Syntaxe: referenceName.variableArrayname [index] ++;
	 */
	private static final String REG_EX_POSTFIX_INCREMENT_VALUE_AT_ARRAY_IN_INSTANCE = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]\\s*\\+\\+\\s*;\\s*$";
	
	/**
	 * Regulární výraz pro postfixovou dekrementaci položky v jednorozměrném poli v
	 * instancí třídy.
	 * 
	 * Syntaxe: referenceName.variableArrayname [index] --;
	 */
	private static final String REG_EX_POSTFIX_DECREMENT_VALUE_AT_ARRAY_IN_INSTANCE = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]\\s*--\\s*;\\s*$";
	
	
	
	
	
	
	
	
	/**
	 * Regulární výraz pro postfixovou formu inkrementace položky v jednorozměrném
	 * poli v instanci nějaké třídy (na začátku může být výraz pro negace hodnoty,
	 * ale taková by se vrátila - znegovaná hodnota, ne uložila).
	 * 
	 * Syntaxe (bez středníku na konci):
	 * 
	 * (-) referenceName.variableName[index] ++
	 */
	private static final String REG_EX_POSTFIX_INCREMENT_VALUE_AT_ARRAY_IN_INSTANCE_WITH_MINUS = "^\\s*-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]\\s*\\+\\+\\s*$";
	
	/**
	 * Regulární výraz pro postfixovou formu dekrementace položky v jednorozměrném
	 * poli v instanci nějaké třídy (na začátku může být výraz pro negace hodnoty,
	 * ale taková by se vrátila - znegovaná hodnota, ne uložila).
	 * 
	 * Syntaxe (bez středníku na konci):
	 * 
	 * (-) referenceName.variableName[index] --
	 */
	private static final String REG_EX_POSTFIX_DECREMENT_VALUE_AT_ARRAY_IN_INSTANCE_WITH_MINUS = "^\\s*-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]\\s*--\\s*$";
	
	
	
	

	
	
	
	
	
	
	
	/**
	 * Regulární výraz pro otestování, zda se jedná o naplnění hodnoty ve
	 * drojrozměrném poli zavolání mmetody.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.twoDimArray [index] [index] = referenceName.methodName();
	 * 
	 * referenceName.twoDimArray [index] [index] = referenceName.methodName(params,
	 * ...);
	 * 
	 * referenceName.twoDimArray [index] [index] =
	 * model.data.ClassName.methodName(params, ...);
	 */
	private static final String  REG_EX_FULFILLMENT_INDEX_AT_TWO_DIM_ARRAY_IN_INSTANCE_BY_METHOD = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}\\s*=\\s*\\w+\\s*(\\s*\\.\\s*\\w+)+" + KEY_PARAMETERS + ";\\s*$";
	
	
	
	
	
	
	
	
	
	/**
	 * Regulární výraz pro otestování, zda se jedná o syntaxi pro naplnění položky
	 * ve dvourozměrném poli pomocí zavoláním konstruktoru.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName . variableName [0] [0] = new model.data.Classname (params);
	 */
	private static final String REG_EX_FULFILLMENT_INDEX_AT_TWO_DIM_ARRAY_IN_INSTANCE_BY_CONSTRUCTOR = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}\\s*=\\s*" + KEY_NEW + "\\s+\\w+\\s*(\\s*\\.\\s*\\w+)+" + KEY_PARAMETERS + ";\\s*$";
	
	
	
	/**
	 * Regulární výraz pro naplnění položky na zadaném indexu ve dvourozměrném poli
	 * v instanci nějaké třídy pomocí proměnné.
	 * 
	 * Syntaxe:
	 * 
	 * refereceName.variable[index][index] = referenceName.variable;
	 */
	private static final String REG_EX_FULFILLMENT_INDEX_AT_TWO_DIM_ARRAY_IN_INSTANCE_BY_VARIABLE = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}\\s*=\\s*(((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*[a-zA-Z]\\w*\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*|-?\\s*[a-zA-Z]\\w*\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]|-?\\s*[a-zA-Z]\\w*\\s*\\[\\s*\\d+\\s*\\]\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\[\\s*\\d+\\s*\\]\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\.\\s*length|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*\\.\\s*length\\s*(--|\\+\\+)?)|((--|\\+\\+)?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}|-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}\\s*(--|\\+\\+)?))\\s*;\\s*$";
	
	
	
	
	/**
	 * Regulrání vžraz pro otestování syntaxe pro naplnění položky na zadaném indexu
	 * ve dvourozměrném poli pomocí nějaké konstanty, tj. například hodnoty true,
	 * false, znak, text, matematický výpočet apod.
	 * 
	 * Syntaxe:
	 * 
	 * referencename.variableName[index][index] = true;
	 * 
	 * referencename.variableName[index][index] = "some text";
	 * 
	 * ...
	 */
	private static final String REG_EX_FULFILLMENT_INDEX_AT_TWO_DIM_ARRAY_IN_INSTANCE_BY_CONSTANT = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}\\s*=\\s*('.'|\".*\"|true|false|-?\\s*[\\d\\s*]+\\s*[lLfFdD]?|-?\\s*[\\d\\s*]+\\s*\\.\\s*[\\d\\s*e]+\\s*[fFdD]?|" + ONLY_VALUES_FOR_MATHEMATICAL_EXPRESSION_WITH_SPACES + ")\\s*;\\s*$";
	
	
	
	
	
	
	
	/**
	 * Regulární výraz pro rozpoznání syntaxe pro získání prvku z dvourozměrného
	 * pole, které se nachází v instanci nějaké třídy.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName . variableArrayname [index] [index];
	 */
	private static final String REG_EX_GET_VALUE_AT_INDEX_AT_TWO_DIM_ARRAY_IN_INSTANCE = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}\\s*;\\s*$";
	
	
	
	
	
	
	/**
	 * Regulární výraz pro rozpoznání příkazu pro převzetí hodnoty na indexu ve
	 * dvourozměrném pli v nějaké instanci třídy, na začátku může být mínus pro
	 * negaci hodnoty a na konci není středník.
	 * 
	 * Syntaxe:
	 * 
	 * (-) referenceName . variableName [index] [index]
	 */
	private static final String REG_EX_GET_VALUE_AT_INDEX_AT_TWO_DIM_ARRAY_IN_INSTANCE_WITH_MINUS = "^\\s*-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}\\s*$";
	
	
	
	
	
	
	
	
	
	
	/**
	 * Regulární výraz pro prefixovou inkrementaci hodnoty na zadaném indexu ve
	 * dvourozměrném poli v instanci nějaké třídy.
	 * 
	 * Syntaxe:
	 * 
	 * ++ reference.variableName[index] [index];
	 */
	private static final String REG_EX_PREFIX_INCREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE = "^\\s*\\+\\+\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}\\s*;\\s*$";
	
	/**
	 * Regulární výraz pro prefixovou dekrementaci hodnoty na zadaném indexu ve
	 * dvourozměrném poli v instanci nějaké třídy.
	 * 
	 * Syntaxe:
	 * 
	 * -- reference.variableName[index] [index];
	 */
	private static final String REG_EX_PREFIX_DECREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE  = "^\\s*--\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}\\s*;\\s*$";
	
	/**
	 * Regulární výraz pro postfixovou inkrementaci hodnoty na zadaném indexu ve
	 * dvourozměrném poli v instanci nějaké třídy.
	 * 
	 * Syntaxe:
	 * 
	 * reference.variableName[index] [index] ++;
	 */
	private static final String REG_EX_POSTFIX_INCREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE  = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}\\s*\\+\\+\\s*;\\s*$";
	
	/**
	 * Regulární výraz pro postfixovou dekrementaci hodnoty na zadaném indexu ve
	 * dvourozměrném poli v instanci nějaké třídy.
	 * 
	 * Syntaxe:
	 * 
	 * reference.variableName[index] [index] --;
	 */
	private static final String REG_EX_POSTFIX_DECREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE = "^\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}\\s*--\\s*;\\s*$";
	
	
	
	
	
	
	
	
	
	
	/**
	 * Regulrání výraz pro rozpoznání syntaxe pro postfixovou inkrementaci hodnoty
	 * na zadaném indexu ve dvourozměrném poli v instanci nějaké třídy v syntaxi:
	 * 
	 * Syntaxe: (-) referenceName . variableName [index] [index] ++
	 */
	private static final String REG_EX_POSTFIX_INCREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INTANCE = "^\\s*-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}\\s*\\+\\+\\s*$";

	/**
	 * Regulrání výraz pro rozpoznání syntaxe pro postfixovou dekrementaci hodnoty
	 * na zadaném indexu ve dvourozměrném poli v instanci nějaké třídy v syntaxi:
	 * 
	 * Syntaxe: (-) referenceName . variableName [index] [index] --
	 */
	private static final String REG_EX_POSTFIX_DECREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INTANCE = "^\\s*-?\\s*[a-zA-Z]\\w*\\s*\\.\\s*\\w+\\s*(\\[\\s*\\d+\\s*\\]\\s*){2}\\s*--\\s*$";
	
	
	
	
	
	
	
	
	
	
	
	
	
	


	@Override
	public boolean isPrintCommands(final String command) {
		return command.matches(REG_EX_PRINT_COMMANDS);
	}



	
	
	@Override
	public boolean isPrintTempVariables(final String command) {
		return command.matches(REG_EX_PRINT_TEMP_VAIABLES_IN_EDITOR);
	}
	
	
	
	
	
	
	
	@Override
	public boolean isCloseApplicationByExit(final String command) {
		return command.matches(REG_EX_CLOSE_APP_BY_EXIT_COMMAND);
	}

	
	@Override
	public boolean isCloseApplicationByExitWithCount(final String command) {
		return command.matches(REG_EX_CLOSE_APP_BY_EXIT_WITH_COUNT_COMMAND);
	}
	
	
	
	@Override
	public boolean isCloseApplicationBySystemExit(final String command) {
		return command.matches(REG_EX_CLOSE_APP_BY_SYSTEM_EXIT_COMMAND);
	}
	
	
	
	@Override
	public boolean isRestartApplication(final String command) {
		return command.matches(REG_EX_RESTART_APP_COMMAND);
	}
	
	
	@Override
	public boolean isRestartApplicationWithCount(final String command) {
		return command.matches(REG_EX_RESTART_APP_WITH_COUNT_COMMAND);
	}
	
	
	
	@Override
	public boolean isCancelCountdown(final String command) {
		return command.matches(REG_EX_CANCEL_COUNTDOWN_COMMAND);
	}
	
	
	
	
	
	


	
	@Override
	public boolean isWhiteSpace(final String command) {
		return command.matches(REG_EX_WHITE_SPACE);
	}


	
	
	
	@Override
	public boolean isTextInQuotes(final String text) {
		return text.matches(REG_EX_TEXT_IN_QUOTATION_MARKS);
	}


	
	
	
	
	

	@Override
	public boolean isClassName(final String text) {
		return text.matches(REG_EX_CLASS_NAME);
	}
	
	
	
	
	
	
	
	
	@Override
	public boolean isReferenceToInstanceOfClass(final String text) {
		return text.matches(REG_EX_REFERENCE_TO_INSTANCE_OF_CLASS);
	}





	

	@Override
	public boolean isConstantOrLiteral(final String text) {
		return text.matches(REG_EX_CONSTANT_LITERAL);
	}

	
	
	
	
	@Override
	public boolean isClassNameWithBracket(final String text) {
		return text.matches(REG_EX_CLASS_NAME_WITH_BRACKET);
	}


	
	
	
	
	
	
	@Override
	public boolean isInteger(final String number) {
		return number.matches(REG_EX_INTEGER_NUMBER);
	}
	
	
	

	@Override
	public boolean isDecimal(final String number) {
		return number.matches(REG_EX_DECIMAL_NUMBER);
	}
	
	
	
	
	
	
	
	@Override
	public boolean isMathematicalExpression(final String command) {
		return command.matches(REG_EX_MATHEMATICAL_EXPRESSION);
	}
	
	
	
	
	




	@Override
	public boolean isExponent(final String command) {
		return command.matches(REG_EX_EXPONENT_VALUE);
	}
	
	
	
	
	
	

	
	
	
	@Override
	public boolean isCallConstructor(final String command) {
		return command.matches(REG_EX_CALL_CONSTRUCTOR);
	}
	
	
	
	
	

	@Override
	public boolean isNewInstanceCommand(final String command) {
		return command.matches(REG_EX_NEW_INSTANCE_COMMAND);
	}


	
	
	
	@Override
	public boolean isNewInstance(final String command) {
		return command.matches(REG_EX_NEW_INSTANCE);
	}


	

	@Override
	public boolean isNewInstanceWithParameters(final String command) {
		return command.matches(REG_EX_NEW_INSTANCE_WITH_PARAMETERS);
	}

	
	

	@Override
	public boolean isCallTheMethod(final String command) {
		return command.matches(REG_EX_CALL_THE_METHOD);
	}


	
	
	@Override
	public boolean isCallStaticMethod(final String command) {
		return command.matches(REG_EX_CALL_STATIC_METHOD);
	}
	
	
	
	

	
	@Override
	public boolean isCallTheMethodWithoutParameters(final String command) {
		return command.matches(REG_EX_CALL_THE_METHOD_WITHOUT_PARAMETERS);
	}

	
	

	@Override
	public boolean isCallTheMethodWithParameters(final String command) {
		return command.matches(REG_EX_CALL_THE_METHOD_WITH_PARAMETERS);
	}

	


	
	
	
	@Override
	public boolean isFullfillmentOfVariablesNumber(final String command) {
		return command.matches(REG_EX_FULL_FILLMENT_OF_VARIABLE_NUMBER);
	}
	

	@Override
	public boolean isFullfillmentOfVariableVariable(final String command) {
		return command.matches(REG_EX_FULL_FILLMENT_OF_VARIABLE_VARIABLE);
	}

	
	
	@Override
	public boolean isFullfillmentOfVariableGetMethod(final String command) {
		return command.matches(REG_EX_FULLFILLMENT_OF_VARIABLE_GET_METHOD);
	}

	
	
	@Override
	public boolean isFullfilmentOfVariableByCallingConstructor(final String command) {
		return command.matches(REG_EX_FULLFILLMENT_OF_VARIABLE_BY_CALLING_CONSTRUCTOR);
	}
	

	
	@Override
	public boolean isIncrementValueBehind(final String command) {
		return command.matches(REG_EX_INCREMENT_VALUE_BEHIND);
	}

	
	
	@Override
	public boolean isDecrementValueBehind(final String command) {
		return command.matches(REG_EX_DECREMENT_VALUE_BEHIND);
	}

	
	
	@Override
	public boolean isIncrementValueBefore(final String command) {
		return command.matches(REG_EX_INCREMENT_VALUE_BEFORE);
	}

	
	
	@Override
	public boolean isDecrementValueBefore(final String command) {
		return command.matches(REG_EX_DECREMENT_VALUE_BEFORE);
	}

	
	
	@Override
	public boolean isIncrementClassVariableBefore(final String command) {
		return command.matches(REG_EX_INCREMENT_CLASS_VARIABLE_BEFORE);
	}
	
	

	@Override
	public boolean isDecrementClassVariableBefore(final String command) {
		return command.matches(REG_EX_DECREMENT_CLASS_VARIABLE_BEFORE);
	}
	
	

	@Override
	public boolean isIncrementClassVariableBehind(final String command) {
		return command.matches(REG_EX_INCREMENT_CLASS_VARIABLE_BEHIND);
	}

	
	
	@Override
	public boolean isDecrementClassVariableBehind(final String command) {
		return command.matches(REG_EX_DECREMENT_CLASS_VARIABLE_BEHIND);
	}
	
	
	
	
	@Override
	public boolean isIncrementCommandEditorVariableBehind(final String command) {
		return command.matches(REG_EX_INCREMENT_EDITOR_VARIABLE_BEHIND);
	}


	
	

	@Override
	public boolean isIncrementCommandEditorVariableBefore(final String command) {
		return command.matches(REG_EX_INCREMENT_EDITOR_VARIABLE_BEFORE);
	}

	


	@Override
	public boolean isDecrementCommandEditorVariableBehind(final String command) {
		return command.matches(REG_EX_DECREMENT_EDITOR_VARIABLE_BEHIND);
	}



	@Override
	public boolean isDecrementCommandEditorVariableBefore(final String command) {
		return command.matches(REG_EX_DECREMENT_EDITOR_VARIABLE_BEFORE);
	}
	
	


	@Override
	public boolean isPrintValueLiteral(final String command) {
		return command.matches(REG_EX_PRINT_VALUE_LITERAL);
	}

	
	
	@Override
	public boolean isClassVariable(final String command) {
		return command.matches(REG_EX_CLASS_VARIABLE);
	}
	
	
	

	@Override
	public boolean isVariableInEditor(final String command) {
		return command.matches(REG_EX_VARIABLE_IN_EDITOR);
	}







	@Override
	public boolean isDeclarationEditorVariable(final String command) {
		return command.matches(REG_EX_DECLARATION_EDITOR_VARIABLE);
	}

	

	@Override
	public boolean isDeclarationAndFulfillmentEditorVariableConstant(final String command) {
		return command.matches(REG_EX_DECLARATION_AND_FULFILLMENT_OF_EDITOR_VARIABLE_CONSTANT);
	}




	@Override
	public boolean isDeclarationAndFulfillmentEditorVariableVariable(final String command) {
		return command.matches(REG_EX_DECLARATION_AND_FULFILLMENT_OF_EDITOR_VARIABLE_VARIABLE);
	}





	@Override
	public boolean isDeclarationAndFulfillmentEditorVariableGetMethod(final String command) {
		return command.matches(REG_EX_DECLARATION_AND_FULFILLMENT_OF_EDITOR_VARIABLE_GET_METHOD);
	}

	
	
	


	
	
	
	
	@Override
	public boolean isFullfillmentOfEditorVariableGetMethod(final String command) {
		return command.matches(REG_EX_FULLFILLMENT_OF_EDITOR_VARIABLE_GET_METHOD);
	}



	@Override
	public boolean isFullfillmentOfEditorVariableVariable(final String command) {
		return command.matches(REG_EX_FULLFILLMENT_OF_EDITOR_VARIABLE_VARIABLE);
	}



	@Override
	public boolean isFullfillmentOfEditorVariableConstant(final String command) {
		return command.matches(REG_EX_FULLFILLMENT_OF_EDITOR_VARIABLE_CONSTANT);
	}



	
	
	
	



	
	








	@Override
	public boolean isFulfillmentOfVariableUsingTotal(final String command) {
		return command.matches(REG_EX_FULFILLMENT_OF_VARIABLE_USING_TOTAL);
	}



	@Override
	public boolean isFulfillmentOfVariableUsingDifference(final String command) {
		return command.matches(REG_EX_FULFILLMENT_OF_VARIABLE_USING_DEFFERENCE);
	}




	@Override
	public boolean isFulfillmentOfVariableUsingProduct(final String command) {
		return command.matches(REG_EX_FULFILLMENT_OF_VARIABLE_USING_PRODUCT);
	}



	@Override
	public boolean isFulfillmentOfVariableUsingShare(final String command) {
		return command.matches(REG_EX_FULFILLMENT_OF_VARIABLE_USING_SHARE);
	}



	@Override
	public boolean isFulfillmentOfVariableUsingModulo(final String command) {
		return command.matches(REG_EX_FULFILLMENT_OF_VARIABLE_USING_MODULO);
	}






	@Override
	public boolean isIncrementClassVariable(final String command) {
		return command.matches(REG_EX_INCREMENT_CLASS_VARIABLE);
	}






	@Override
	public boolean isDecrementClassVariable(final String command) {
		return command.matches(REG_EX_DECREMENT_CLASS_VARIABLE);
	}






	@Override
	public boolean isIncrementEditorVariable(final String command) {
		return command.matches(REG_EX_INCREMENT_EDITOR_VARIABLE);
	}






	@Override
	public boolean isDecrementEditorVariable(final String command) {
		return command.matches(REG_EX_DECREMENT_EDITOR_VARIABLE);
	}






	@Override
	public boolean isCreateAndFullfillmentList(final String command) {
		return command.matches(REG_EX_CREATE_AND_FULFILLMENT_LIST);
	}






	@Override
	public boolean isDeclarateListWithFinal(final String text) {
		return text.matches(REG_EX_DECLARATION_LIST_WITH_FINAL);
	}






	@Override
	public boolean isAddValueToEndOfList(final String command) {
		return command.matches(REG_EX_ADD_VALUE_TO_END_OF_LIST);
	}






	@Override
	public boolean isAddValueToIndexAtList(final String command) {
		return command.matches(REG_EX_ADD_VALUE_TO_INDEX_AT_LIST);
	}






	@Override
	public boolean isClearList(final String command) {
		return command.matches(REG_EX_CLEAR_LIST);
	}






	@Override
	public boolean isGetValueAtIndexOfList(final String command) {
		return command.matches(REG_EX_GET_VALUE_AT_INDEX_OF_LIST);
	}






	@Override
	public boolean isSetValueAtIndexOfList(final String command) {
		return command.matches(REG_EX_SET_VALUE_AT_INDEX_OF_LIST);
	}






	@Override
	public boolean isListToOneDimensionalArray(final String command) {
		return command.matches(REG_EX_LIST_TO_ONE_DIMENSIONAL_ARRAY);
	}






	@Override
	public boolean isListSize(final String command) {
		return command.matches(REG_EX_LIST_SIZE);
	}






	@Override
	public boolean isRemoveValueAtIndexOfList(final String command) {
		return command.matches(REG_EX_REMOVE_VALUE_AT_INDEX_OF_LIST);
	}






	@Override
	public boolean isDeclarationAndFullfillmentArray(final String command) {
		return command.matches(REG_EX_DECLARATION_AND_FULFILLMENT_ARRAY);
	}

	
	
	
	
	@Override
	public boolean isDeclarationAndFulfillmentArrayShort(final String command) {
		return command.matches(REG_EX_DECLARATION_AND_FULFILLMENT_ARRAY_SHORT);
	}
	
	





	@Override
	public boolean isDeclarationArrayWithLength(final String command) {
		return command.matches(REG_EX_DECLARATION_ARRAY_WITH_LENGTH);
	}

	
	
	
	
	
	@Override
	public boolean isDeclarationArrayByCallingMethod(final String command) {
		return command.matches(REG_EX_DECLARATION_ARRAY_WITH_METHOD);
	}
	
	
	
	





	@Override
	public boolean isFullfillmentValueAtIndexOfArrayConstant(final String command) {
		return command.matches(REG_EX_FULFILLMENT_VALUES_AT_INDEX_OF_ARRAY_CONSTANT);
	}






	@Override
	public boolean isFullfillmentValueAtIndexOfArrayVariable(final String command) {
		return command.matches(REG_EX_FULFILLMENT_VALUE_AT_INDEX_OF_ARRAY_VARIABLE);
	}






	@Override
	public boolean isFullfillmentValueAtIndexOfArrayMethod(final String command) {
		return command.matches(REG_EX_FULFILLMENT_VALUE_AT_INDEX_OF_ARRAY_METHOD);
	}






	@Override
	public boolean isGetValueFromArrayAtIndexInCommand(final String text) {
		return text.matches(REG_EX_GET_VALUE_FROM_ARRAY_AT_INDEX_IN_COMMAND);
	}






	@Override
	public boolean isGetValueFromArrayAtIndex(final String command) {
		return command.matches(REG_EX_GET_VALUE_FROM_ARRAY_AT_INDEX);
	}






	@Override
	public boolean isIncrementArrayValueBefore(final String command) {
		return command.matches(REG_EX_INCREMENT_ARRAY_VALUE_BEFORE);
	}






	@Override
	public boolean isIncrementArrayValueBehind(final String command) {
		return command.matches(REG_EX_INCREMENT_ARRAY_VALUE_BEHIND);
	}






	@Override
	public boolean isDecrementArrayValueBefore(final String command) {
		return command.matches(REG_EX_DECREMENT_ARRAY_VALUE_BEFORE);
	}






	@Override
	public boolean isDecrementArrayValueBehind(final String command) {
		return command.matches(REG_EX_DECREMENT_ARRAY_VALUE_BEHIND);
	}






	@Override
	public boolean isIncrementArrayValueInParameterBehind(final String text) {
		return text.matches(REG_EX_INCREMENT_ARRAY_VALUE_IN_PARAMETER_BEHIND);
	}






	@Override
	public boolean isDecrementArrayValueInParameterBehind(final String text) {
		return text.matches(REG_EX_DECREMENT_ARRAY_VALUE_IN_PARAMETER_BEHIND);
	}






	@Override
	public boolean isGetArrayLength(final String command) {
		return command.matches(REG_EX_ARRAY_LENGTH);
	}






	@Override
	public boolean isGetArrayLengthInParameter(final String command) {
		return command.matches(REG_EX_ARRAY_LENGTH_IN_PARAMETER);
	}


	
	
	
	

	@Override
	public boolean isFulfillmentOfArrayAtIndexInInstanceByMethod(String command) {
		return command.matches(REG_EX_FULFILLMENT_OF_ARRAY_AT_INDEX_IN_INSTANCE_BY_METHOD);
	}





	@Override
	public boolean isFulfillmentOfArrayAtIndexInInstanceByConstructor(String command) {
		return command.matches(REG_EX_FULFILLMENT_OF_ARRAY_AT_INDEX_IN_INSTANCE_BY_CONSTRUCTOR);
	}





	@Override
	public boolean isFulfillmentOfArrayAtIndexInInstanceByVariable(String command) {
		return command.matches(REG_EX_FULFILLMENT_OF_ARRAY_AT_INDEX_IN_INSTANCE_BY_VARIABLE);
	}





	@Override
	public boolean isFulfillmentOfArrayAtIndexInInstanceByConstant(String command) {
		return command.matches(REG_EX_FULFILLMENT_OF_ARRAY_AT_INDEX_IN_INSTANCE_BY_CONSTANT);
	}
	
	
	
	
	
	
	@Override
	public boolean isGetValueFromArrayAtIndexInInstance(final String command) {
		return command.matches(REG_EX_GET_VALUE_AT_INDEX_OF_ARRAY_IN_INSTANCE);
	}
	
	
	
	@Override
	public boolean isGetLengthOfArrayInInstance(final String command) {
		return command.matches(REG_EX_GET_LENGTH_OF_ARRAY_IN_INSTANCE);
	}



	
	
	
	
	

	@Override
	public boolean isPrefixIncrementValueAtArrayInInstance(String command) {
		return command.matches(REG_EX_PREFIX_INCREMENT_VALUE_AT_ARRAY_IN_INSTANCE);
	}

	@Override
	public boolean isPrefixDecrementValueAtArrayInInstance(String command) {
		return command.matches(REG_EX_PREFIX_DECREMENT_VALUE_AT_ARRAY_IN_INSTANCE);
	}

	@Override
	public boolean isPostfixIncrementValueAtArrayInInstance(String command) {
		return command.matches(REG_EX_POSTFIX_INCREMENT_VALUE_AT_ARRAY_IN_INSTANCE);
	}

	@Override
	public boolean isPostfixDecrementValueAtArrayInInstance(String command) {
		return command.matches(REG_EX_POSTFIX_DECREMENT_VALUE_AT_ARRAY_IN_INSTANCE);
	}
	
	
	
	
	
	
	@Override
	public boolean isPostFixIncrementWithMinus(final String command) {
		return command.matches(REG_EX_POSTFIX_INCREMENT_VALUE_AT_ARRAY_IN_INSTANCE_WITH_MINUS);
	}

	@Override
	public boolean isPostFixDecrementWithMinus(final String command) {
		return command.matches(REG_EX_POSTFIX_DECREMENT_VALUE_AT_ARRAY_IN_INSTANCE_WITH_MINUS);
	}
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public boolean isFulfillmentIndexAtTwoDimArrayInInstanceByMethod(final String command) {
		return command.matches(REG_EX_FULFILLMENT_INDEX_AT_TWO_DIM_ARRAY_IN_INSTANCE_BY_METHOD);
	}
	
	
	@Override
	public boolean isFulfillmentIndexAtTwoDimArrayInInstanceByConstructor(final String command) {
		return command.matches(REG_EX_FULFILLMENT_INDEX_AT_TWO_DIM_ARRAY_IN_INSTANCE_BY_CONSTRUCTOR);
	}
	
	
	@Override
	public boolean isFulfillmentIndexAtTwoDimArrayInInstanceByVariable(final String command) {
		return command.matches(REG_EX_FULFILLMENT_INDEX_AT_TWO_DIM_ARRAY_IN_INSTANCE_BY_VARIABLE);
	}
	
	@Override
	public boolean isFulfillmentIndexAtTwoDimArrayInInstanceByConstant(final String command) {
		return command.matches(REG_EX_FULFILLMENT_INDEX_AT_TWO_DIM_ARRAY_IN_INSTANCE_BY_CONSTANT);
	}
	
	
	@Override
	public boolean isGetValueAtIndexAtTwoDimArrayInInstance(final String command) {
		return command.matches(REG_EX_GET_VALUE_AT_INDEX_AT_TWO_DIM_ARRAY_IN_INSTANCE);
	}
	
	
	@Override
	public boolean isGetValueAtIndexAtTwoDimArrayInIntanceWithMinus(final String command) {
		return command.matches(REG_EX_GET_VALUE_AT_INDEX_AT_TWO_DIM_ARRAY_IN_INSTANCE_WITH_MINUS);
	}
	
	
	
	
	
	
	
	
	
	
	@Override
	public boolean isPrefixIncrementValueAtIndexInTwoDimArrayInInstance(final String command) {
		return command.matches(REG_EX_PREFIX_INCREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE);
	}

	@Override
	public boolean isPrefixDecrementValueAtIndexInTwoDimArrayInInstance(final String command) {
		return command.matches(REG_EX_PREFIX_DECREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE);
	}

	@Override
	public boolean isPostfixIncrementValueAtIndexInTwoDimArrayInInstance(final String command) {
		return command.matches(REG_EX_POSTFIX_INCREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE);
	}

	@Override
	public boolean isPostfixDecrementValueAtIndexInTwoDimArrayInInstance(final String command) {
		return command.matches(REG_EX_POSTFIX_DECREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE);
	}
	
	
	
	
	@Override
	public boolean isPostFixIncrementValueatIndexInTwoDimarrayInInstanceWithMinus(final String command) {
		return command.matches(REG_EX_POSTFIX_INCREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INTANCE);
	}
	

	@Override
	public boolean isPostFixDecrementValueatIndexInTwoDimarrayInInstanceWithMinus(final String command) {
		return command.matches(REG_EX_POSTFIX_DECREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INTANCE);
	}
}