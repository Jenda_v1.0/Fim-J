package cz.uhk.fim.fimj.commands_editor.values_completion_window;

import cz.uhk.fim.fimj.commands_editor.*;
import cz.uhk.fim.fimj.file.DataTypeOfList;
import cz.uhk.fim.fimj.file.DataTypeOfListEnum;
import cz.uhk.fim.fimj.reflection_support.ParameterToText;
import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;
import cz.uhk.fim.fimj.instances.Instances;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Třída, která slouží pouze pro implementaci metod, které slouží jako "výpomocné" metody pro nějaké operace /
 * zjišťování / doplňování apod.
 * <p>
 * Viz jednotlivé metody.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 29.07.2018 0:49
 */

final class ValuePreparerHelper {

    /**
     * Text "Paste into a" pro informaci pro uživatele, kam se mají vložit hodnoty. (Pouze textové proměnná)
     */
    private static final String TXT_PASTE_INTO = "Paste into a";

    private ValuePreparerHelper() {
    }


    /**
     * Vytvoření syntaxe pro inkrementaci a dekrementaci proměnné v prefixové a postfixové formě.
     *
     * @param input
     *         - proměnná, ke které se má přidat inkrementace nebo dekremantace v prefixové a postfixové formě.
     * @param shortDesc
     *         - popisek za pomlčkou
     * @param summary
     *         - komentář, který se zobrazí v okně s nápovědou pro označenou hodnotu v okně pro doplňování hodnot (může
     *         být mull)
     *
     * @return kolekci, která bude obsahovat výše popsané formy proměnných pro inkrementaci a dekrementaci.
     */
    static Set<ValueCompletionInfo> getIncrementDecrementValues(final String input, final String shortDesc,
                                                                final String summary) {
        final Set<ValueCompletionInfo> valueCompletionInfoList = new HashSet<>();

        final String prefixIncrement = "++" + input;
        valueCompletionInfoList.add(new ValueCompletionInfo(prefixIncrement, prefixIncrement, shortDesc, summary));

        final String postfixIncrement = input + "++";
        valueCompletionInfoList.add(new ValueCompletionInfo(postfixIncrement, postfixIncrement, shortDesc, summary));


        final String prefixDecrement = "--" + input;
        valueCompletionInfoList.add(new ValueCompletionInfo(prefixDecrement, prefixDecrement, shortDesc, summary));

        final String postfixDecrement = input + "--";
        valueCompletionInfoList.add(new ValueCompletionInfo(postfixDecrement, postfixDecrement, shortDesc, summary));

        return valueCompletionInfoList;
    }


    /**
     * Zjištění, zda je parametr clazz číselný datový typ.
     *
     * @param clazz
     *         - datový typ, o kterém se má zjistit, zda se jedná o číselný datový typ.
     *
     * @return true, pokud je clazz číselný datový typ, jinak false.
     */
    static boolean isNumberDataType(final Class<?> clazz) {
        if (clazz.equals(byte.class) || clazz.equals(Byte.class))
            return true;

        if (clazz.equals(short.class) || clazz.equals(Short.class))
            return true;

        if (clazz.equals(int.class) || clazz.equals(Integer.class))
            return true;

        if (clazz.equals(long.class) || clazz.equals(Long.class))
            return true;

        if (clazz.equals(float.class) || clazz.equals(Float.class))
            return true;

        return clazz.equals(double.class) || clazz.equals(Double.class);
    }


    /**
     * Získání dosud nevyužitého názvu reference.
     * <p>
     * Princip je, že se první písmenu v className parametru převede na malé písmo, poté se na konec textu bude přidávat
     * číslice, dokud se nanarazí na číslo, index / název, který dosud není využit. Ten se vrátí.
     *
     * @param className
     *         - text reference / názvu, jehož první písmeno se převede na malé a na knec se bude přidávat index, dokud
     *         se nenajde reference, která dosud neexistuje.
     * @param usedReferenceNames
     *         - list, který obsahuje aktuálně využité názvy referencí / proměnných. Výsledná - vrácená reference se
     *         nesmá nacházet v tomto listu.
     *
     * @return název proměnn=é / reference, který se dosud nenachází v listu usedReferenceNames.
     */
    static String getUnusedReferenceName(final String className, final List<String> usedReferenceNames) {
        long index = 1;

        // První písmeno v názvu převedu na malé písmeno:
        String classNameTemp = Character.toLowerCase(className.charAt(0)) + className.substring(1) + index;

        while (usedReferenceNames.contains(classNameTemp))
            classNameTemp =
                    classNameTemp.substring(0, classNameTemp.length() - String.valueOf(index).length()) + ++index;

        return classNameTemp;
    }


    /**
     * Dle datového typu dataType se vrátí výchozí hodnota pro příslušný datový typ, například pro int to bude nula
     * apod.
     * <p>
     * Pro String se vrátí prázdné uvozovky (\"\").
     * <p>
     * Pro char se vrátí mezera
     *
     * @param dataType
     *         - datový typ, dle kterého se vrátí výchozí hodnota pro specifický datový typ.
     *
     * @return výchozí hodnota pro datový typ dataType nebo String s prázdnými uvozovkami jako výchozí hodnota pro
     * kterýkoliv datový typ.
     */
    static Object getDefaultValue(final Class<?> dataType) {
        if (dataType.equals(byte.class) || dataType.equals(Byte.class))
            return 0;

        if (dataType.equals(short.class) || dataType.equals(Short.class))
            return 0;

        if (dataType.equals(int.class) || dataType.equals(Integer.class))
            return 0;

        if (dataType.equals(long.class) || dataType.equals(Long.class))
            return 0L;

        if (dataType.equals(float.class) || dataType.equals(Float.class))
            return 0.0f;

        if (dataType.equals(double.class) || dataType.equals(Double.class))
            return 0.0d;

        if (dataType.equals(char.class) || dataType.equals(Character.class))
            return ' ';

        if (dataType.equals(boolean.class) || dataType.equals(Boolean.class))
            return false;

        if (dataType.equals(String.class))
            return "\"\"";

        return "";
    }


    /**
     * Získání veřejných a chráněných metod, které se nachází v třídě clazz a všech jejich předků. Zde se načtou metody
     * pouze po třídu Object (bez ní). Předci se vyhledávají tak, že se vždy bere předek třídy clazz až do doby než není
     * předek objekct - popř. null.
     *
     * @param clazz
     *         - třída, ze které se mají vzít veřejné a chráněné metody. Metody musí být i statické v případě, že je
     *         proměnna mustBeStatic true.
     * @param mustBeStatic
     *         - true, pokud veškeré veřejné a chráněné metody, které se najdou v třídě clazz musí být i statické. Pokud
     *         je tato hodnota false, pak naopak veškeré metody nesmí být statické.
     * @param methodList
     *         - list, do kterého se vkládají veškeré nalezené výše zmíněné hledané metody. Tento list se na konec
     *         vrátí.
     *
     * @return list, methodList, který bude obsahovat veřejné a chráněné metody, které se nachází ve třídě clazz a dle
     * potřeby musí / nesmí být statické.
     */
    static List<Method> getPublicProtectedMethod(final Class<?> clazz, final boolean mustBeStatic,
                                                 final List<Method> methodList) {
        Arrays.stream(clazz.getDeclaredMethods()).forEach(m -> {
            // Pokud metoda není veřejná ani chráněná, pokračuje se dále, hledají se pouze veřejné a chráněné metody:
            if (!Modifier.isPublic(m.getModifiers()) && !Modifier.isProtected(m.getModifiers()))
                return;

            // Pokud má být statická a není statická, pak ji vynecháme:
            if (mustBeStatic && !Modifier.isStatic(m.getModifiers()))
                return;

            // Pokud nemá být statická a je statická, pak ji vynecháme:
            if (!mustBeStatic && Modifier.isStatic(m.getModifiers()))
                return;

            // Zde je veřejná nebo chráněná a dle potřeby statická, tak si ji uložíme:
            methodList.add(m);
        });

        if (clazz.getSuperclass() != null && !clazz.getSuperclass().equals(Object.class))
            getPublicProtectedMethod(clazz.getSuperclass(), mustBeStatic, methodList);

        return methodList;
    }


    /**
     * Převedení metod v listu methodList do kolekce typu {@link ValueCompletionInfo}.
     *
     * @param methodList
     *         - veškeré metody z třídy Object, které se mají převést do výše uvedeného objektu.
     *
     * @return kolekci obsahující metody v listu methodList převedené do kolekce výše zmíněného datového typu.
     */
    private static Set<ValueCompletionInfo> convertMethods(final List<Method> methodList) {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        methodList.forEach(method -> {
            // Název metody s parametry:
            final String input = ReflectionHelper.getMethodNameAndParametersInText(method.getName(),
                    method.getParameters());

            // Návratový datový typ metody:
            final String shortDesc = ParameterToText.getMethodReturnTypeInText(method, false);

            valueCompletionInfoSet.add(new ValueCompletionInfo(input, input, shortDesc));
        });

        return valueCompletionInfoSet;
    }


    /**
     * Získání veškerých veřejných a chráněných metod z třídy Object v podobě pro přidání do okna pro doplňování /
     * dokončování příkazů v editoru příkazů.
     *
     * @return kolekci obsahující veškeré veřejné a chráněné metody z třídy Object.
     */
    static Set<ValueCompletionInfo> getPublicProtectedMethodsFromObject() {
        final List<Method> methodList =
                Arrays.stream(Object.class.getDeclaredMethods()).filter(method -> Modifier.isPublic(method.getModifiers()) || Modifier.isProtected(method.getModifiers())).collect(Collectors.toList());

        return convertMethods(methodList);
    }


    /**
     * Získání veškerých statických vnitřních tříd třídy clazz, které jsou s viditelnosti public nebo protected.
     * <p>
     * Vyhledání probíhá rekurzivně, že se z každé třídy clazz načtou veškeré vnitřní třídy a pro každou vnitřní třídu
     * se proces opakuje, tedy pro každou vnitřní třídu se také vyhledávají její vnitřní třídy.
     *
     * @param classes
     *         - třída, ze které se mají ziskat veškeré vnitřní třídy s viditelností public nebo protected
     * @param innerClassList
     *         - kolekce vnitřních tříd, která se ve výsledku vrátí. Jedná se o kolekci, do které se vkládají veškeré
     *         nalezené vnitřní třídy.
     *
     * @return kolekc innerClassList, která bude obsahovat veškeré nalezené vnitřní třídy splňující výše uvedené
     * podmínky.
     */
    static List<Class<?>> getPublicProtectedStaticInnerClasses(final Class<?>[] classes,
                                                               final List<Class<?>> innerClassList) {
        // Nemělo by nastat:
        if (classes == null)
            return innerClassList;

        /*
         * Zde se vyhledají veškeré statické vnitřní třídy, které jsou s viditelností public nebo protected.
         */
        Arrays.stream(classes).forEach(c -> {
            if (!Modifier.isStatic(c.getModifiers()))
                return;

            if (!Modifier.isPublic(c.getModifiers()) && !Modifier.isProtected(c.getModifiers()))
                return;

            innerClassList.add(c);

            // Projdeme i vnitřní třídy aktuálně iterované vnitřní třídy:
            getPublicProtectedStaticInnerClasses(c.getDeclaredClasses(), innerClassList);
        });

        /*
         * Zde by bylo možné prohledat ještě předky, ale to není třeba, ty se budou prohledávat při získávání metod.
         * zde stačí získat pouze třídy, ze kterých se mají vyfiltrovat potřbné metody.
         */

        return innerClassList;
    }


    /**
     * Získání veškerých veřejných a chráněných proměnných, které se nachází v instanci instance. Jedná se o instanci,
     * která se nachází v diagramu instancí.
     *
     * @param instance
     *         - instance, která se nachází v diagramu instancí (vytvořena z třídy v diagramu tříd).
     *
     * @return kolekci, která bude obsahovat veškeré veřejné a chráněné proměnné, které se nachází v instanci instance.
     */
    static List<Field> getPublicProtectedFields(final Object instance) {
        return Arrays.stream(instance.getClass().getDeclaredFields()).filter(f -> Modifier.isPublic(f.getModifiers()) || Modifier.isProtected(f.getModifiers())).collect(Collectors.toList());
    }


    /**
     * Získání syntaxe pro prefixovou a postfixovou inkrementaci a dekrementaci v případě, že se jedná o proměnnou
     * datového typu jednorozměrné nebo dvourozměrné pole nebo "klasickou" proměnnou - ne datovou strukturu.
     *
     * @param modifier
     *         - hodnota pro zjištění, zda je proměnná final nebo ne. Toto je potřeba, pokud se jedná například o
     *         proměnnou typu int, pak pokud je final, tak nemůže být inkrementována / dekrementována apod.
     * @param dataType
     *         - datový typ proměnné, dle kterého se zjistí, zda se má doplnit syntaxe pro postfixovou a prefixovou
     *         inkrementaci a dekrementaci.
     * @param input
     *         - text, který má být vypsaný v okně před pomlčkou (název proměnné). Tato hodnota se po zvolení vloži do
     *         editoru.
     * @param shortDesc
     *         - text, který má být vypsaný za pomlčkou (datový typ).
     * @param summary
     *         - text, který se vloží do "druhého" okna s popiskem pro označenou hodnotu.
     *
     * @return kolekci, která bude obsahovat veškeré syntaxe inkrementaci a dekremetnaci v postfixové a prefixové formě
     * pro číselný datový typ proměnné (ne datové struktury) nebo jedno a dvou rozměrného pole číselného datového typu
     */
    static Set<ValueCompletionInfo> getSyntaxForIncrementDecrementValue(final int modifier, final Class<?> dataType,
                                                                        final String input, final String shortDesc,
                                                                        final String summary) {
        /*
         * Pokud se jedná o mapu nebo kolekci, pak je možné skončit, protože pro tyto typy se inkrementace ani
         * dekrementace doplňovat nebude.
         */
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        if (ReflectionHelper.isDataTypeOfList(dataType) || ReflectionHelper.isDataTypeOfMap(dataType))
            return valueCompletionInfoSet;


        /*
         * Pro pole si zjistí kolik má dimenzí, pokud je to více než dvě, pak se také může skončit, protože editor
         * příkazů podporuje pouze
         * inkrementaci a dekrementaci v jedno a dvou rozměrném poli, ne více.
         *
         * Pokud se jedná o jedno nebo dvou rozměrné pole, pak se vytvoří syntaxe pro "získání" hodnoty na nultém
         * indexu v poli (ref.varName[0]). K této syntaxi
         * se na začátek a na konec přidá inkrementace a dekremetnace.
         */
        if (dataType.isArray()) {
            final int count = ReflectionHelper.getCountOfDimensionArray(dataType);

            if (count > 2)
                return valueCompletionInfoSet;


            final StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < count; i++)
                stringBuilder.append("[0]");

            final String arrayInput = input + stringBuilder.toString();
            valueCompletionInfoSet.add(new ValueCompletionInfo(arrayInput, arrayInput, shortDesc, summary));


            // vytvorit postfix a prefix inc, dec
            final Class<?> arrayDataType = ReflectionHelper.getDataTypeOfArray(dataType);
            if (ValuePreparerHelper.isNumberDataType(arrayDataType))
                return ValuePreparerHelper.getIncrementDecrementValues(arrayInput, shortDesc, summary);
        }


        /*
         * Zde se jedná o "klasickou" proměnnou, tak k ní přidám pouze na začátek a na konec inkrementaci a
         * dekrementaci (v případě, že není proměnná final).
         */
        if (!Modifier.isFinal(modifier) && ValuePreparerHelper.isNumberDataType(dataType))
            return ValuePreparerHelper.getIncrementDecrementValues(input, shortDesc, summary);

        return valueCompletionInfoSet;
    }


    /**
     * Zjištění, zda je field proměnná jednoho ze základních datových typů jazyka Java (text, znak, boolean, číslo),
     * případně jednorozměrné pole nebo list, také zmíněných datových typů.
     * <p>
     * (Řeší se pouze to, zda je možné deklaraovat nebo naplnit proměnnou vytvořenou v editoru příkazů, to lze pouze z
     * proměnné, které je zmíněného datového typu.)
     *
     * @param field
     *         - proměnná, o které se má zjistit, zda se jedná o proměnnou základního datového typu jazyka Java, pole
     *         nebo list, také uvedených datových typů.
     *
     * @return True, pokud je proměnná field jeden ze základních datových typů jazyka Java, pole nebo list také
     * zmíněných datových typů. Jinak false.
     */
    static boolean isDataTypeForCreateVariable(final Field field) {
        return isAllowedVariableDataType(field.getType()) || isAllowedListVariableDataType(field) || isAllowedArrayVariableDataType(field.getType());
    }


    /**
     * Zjištění, zda je dataType jeden ze základních datových typů jazyka Java, které jsou podporovány v editoru
     * příkazů.
     *
     * @param dataType
     *         - datový typ proměnné, o kterém se má zjistit, zda se jedná o jeden z povolených datových typů, se
     *         kterýmy je možné pracovat v editoru příkazů.
     *
     * @return true, pokud je dataType jeden z povolených datových typů, se kterými lze pracovat v editoru příkazů,
     * jinak false.
     */
    private static boolean isAllowedVariableDataType(final Class<?> dataType) {
        if (dataType.equals(byte.class) || dataType.equals(Byte.class))
            return true;

        if (dataType.equals(short.class) || dataType.equals(Short.class))
            return true;

        if (dataType.equals(int.class) || dataType.equals(Integer.class))
            return true;

        if (dataType.equals(long.class) || dataType.equals(Long.class))
            return true;

        if (dataType.equals(float.class) || dataType.equals(Float.class))
            return true;

        if (dataType.equals(double.class) || dataType.equals(Double.class))
            return true;

        if (dataType.equals(char.class) || dataType.equals(Character.class))
            return true;

        if (dataType.equals(boolean.class) || dataType.equals(Boolean.class))
            return true;

        return dataType.equals(String.class);
    }


    /**
     * Zjištění, zda se jedná o jednorozměrné pole, které je datového typu jednoho ze základních datových typů jazyka
     * Java, se kterými je možné pracovat v editoru příkazů.
     *
     * @param array
     *         - typ proměnné, o které se má zjistit, zda se jedná o jednorozměrné pole jednoho z povolených datových
     *         typů.
     *
     * @return true, pokud je array jednorozměrné pole datového typu, se kterými je možné pracovat v editoru příkazů
     * (základní datové typy jazyka Java). Jinak false.
     */
    private static boolean isAllowedArrayVariableDataType(final Class<?> array) {
        /*
         * Note:
         *
         * Prncip je takový, že se zjistí, zda se jedná o pole, získá se jeho počet dimenzí, a případně datový typ,
         * která se testuje, zda se jedná o povolený datový typ.
         *
         * Ale bylo by možné rovnou textovat podmínku: "field.getType().equals(int[].class)" pro všechny povolená pole.
         */

        /*
         * Tato podmínka není nezbytná, u zjištění počtu dimenzí by se szjistilo, že je počet dimenzí je nula, tudíž
         * se nejedná o pole, zde pouze pro přehlednost.
         */
        if (!array.isArray())
            return false;

        // Zjištění počtu dimenzí:
        final int countOfDimensions = ReflectionHelper.getCountOfDimensionArray(array);

        if (countOfDimensions != 1)
            return false;

        // Získání datového typu pole field:
        final Class<?> dataType = ReflectionHelper.getDataTypeOfArray(array);

        return isAllowedVariableDataType(dataType);
    }


    /**
     * Zjištění, zda se jedná o list, který je datového typu jednoho v editoru příkazů povolených datový typů.
     *
     * @param field
     *         - proměnná, o které se zjistí, zda se jedná o list, který je datového typu jednoho z povolených datových
     *         typů v editoru příkazů.
     *
     * @return true, pokud je proměnná field list jednoho z povolených datových typů, jinak false.
     */
    private static boolean isAllowedListVariableDataType(final Field field) {
        if (!ReflectionHelper.isDataTypeOfList(field.getType()))
            return false;

        // Datový typ listu:
        final DataTypeOfList classTypeOfList = ReflectionHelper.getDataTypeOfList(field);

        /*
         * V editoru příkazů jsou povolené pouze "konkrétní" vybrané datové typy jazyka Java, takže pokud se jedná
         * například o nespecifikovaný generický datový typ listu nebo list bez uvedeného datového typu, tak nelze
         * vytvořit dekleraci proměnné, která je podporována v editoru příkazů:
         */
        if (classTypeOfList.getDataTypeOfListEnum() != DataTypeOfListEnum.SPECIFIC_DATA_TYPE)
            return false;

        final Object objDataType = classTypeOfList.getDataType();

        if (!(objDataType instanceof Class<?>))
            return false;

        final Class<?> dataType = (Class<?>) objDataType;

        if (dataType.equals(Byte.class))
            return true;

        if (dataType.equals(Short.class))
            return true;

        if (dataType.equals(Integer.class))
            return true;

        if (dataType.equals(Long.class))
            return true;

        if (dataType.equals(Float.class))
            return true;

        if (dataType.equals(Double.class))
            return true;

        if (dataType.equals(Character.class))
            return true;

        if (dataType.equals(Boolean.class))
            return true;

        return dataType.equals(String.class);
    }


    /**
     * Sestavení datového typu proměnné field do podoby textu tak, aby bylo možné jej vložit do editoru příkazů, aby
     * bylo možné deklarovat proměnnou.
     * <p>
     * Řeší se zde pouze sestavení "klasické" proměnné základních typů jazyka Java (String, int, Integer, Double,
     * boolean, ...).
     *
     * @param field
     *         - proměnná, ze která se má sestavit syntaxe pro její deklaraci v editoru příkazů.
     * @param referenceName
     *         - název reference na instanci v diagramu instancí, ze které se ta proměnná field vzala a má se naplnit do
     *         nově sestavené proměnné.
     * @param varName
     *         - nový název proměnné při deklaraci.
     *
     * @return syntaxi pro deklaraci proměnné výše zmíněných datových typů v editoru příkazů.
     */
    static String prepareVarDeclarationForEditor(final Field field, final String referenceName, final String varName) {
        // Pokud se jedná o povolený typ proměnné, přidá se její vytvoření:
        if (!isAllowedVariableDataType(field.getType()))
            return null;


        // Pro sestavení syntaxe:
        final StringBuilder stringBuilder = new StringBuilder();

        if (Editor.isAddFinalWordForFillingTheVariable())
            stringBuilder.append(ValuePreparerHelperConstants.TXT_FINAL_KEY_WORD + " ");

        stringBuilder.append(field.getType().getSimpleName());

        // Přádání názvu proměnné (dosud nevyužité):
        stringBuilder.append(" ").append(varName).append(" = ");
        // Přidání názvu reference na instanci a proměnné, ze které se má vzít hodnota:
        stringBuilder.append(referenceName).append(".").append(field.getName()).append(";");

        return stringBuilder.toString();
    }


    /**
     * Připravení / sestavení syntaxe pro naplnění proměnných, které uživatel vytvořil v editoru příkazů hodnotami z
     * proměnnných, které se nachází v instancích v diagramu instancí.
     * <p>
     * Je podoprováno naplnění pouze některých typů proměnných, které jsou podporovány editorem příkazů ("klasické"
     * proměnné základních datových typů, list a pole také uvedených datových typů).
     *
     * @param field
     *         - proměnná, ze které se má vzít hodnota a vložit do proměnné v editoru příkazů. Jedná se o nějakou
     *         proměnnou v nějaké instanci v diagramu instancí.
     * @param referenceName
     *         - název reference na instancí, kde se nachází proměnná field.
     *
     * @return kolekci, která bude obsahovat veškeré vytvořené syntaxe pro naplnění proměných vytvořených v editoru
     * příkazů.
     */
    static Set<ValueCompletionInfo> prepareSyntaxForFillVariableInEditor(final Field field,
                                                                         final String referenceName) {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        // Přidání datového typu "klasické" proměnné:
        if (isAllowedVariableDataType(field.getType())) {
            // Získání "klasických" proměnných vytvořených uživatelem v editoru příkazů:
            final Map<String, VariableAbstract> variablesWithDataType =
                    Instances.getVariablesWithSameDataType(new Variable(null, false, field.getType()), true);

            // Vytvoření syntaxe a vložení do kolekce pro okno:
            addValuesForInsertValuesIntoEditorVariables(valueCompletionInfoSet, field, referenceName,
                    variablesWithDataType.keySet());


            /*
             * Dále se zjistí, zda existuje nějaké jednorozměrné pole v editoru příkazů, které vytvořil uživatel a je
             * stejného datového typu jako field. Pokud ano, přidá se možnost pro vložení hodnoty z proměnné field
             * do pole na výchozím indexu nula:
             */
            // Získání polí vytvořených uživatelem v editoru příkazů:
            final Map<String, VariableAbstract> arraysWithDataType =
                    Instances.getVariablesWithSameDataType(new VariableArray<>(false, 0,
                            ReflectionHelper.getDataTypeOfArray(field.getType())), false);

            // Vytvoření syntaxe a vložení do kolekce pro okno:
            addValuesForInsertValueIntoArrayInEditorVariables(valueCompletionInfoSet, field, referenceName,
                    arraysWithDataType.keySet());


            /*
             * Dále se zjistí, zda se jedná o List, který je stejného datového typu, jako proměnná field. Pokud ano,
             * nabídnou se možnosti pro vložení hodnoty do listu.
             *
             * (Zde je ale třeba dávat na výběr "úplně stejné" datové typy. V aplikaci není doplněna podpora pro
             * rozlišení, že když se jedná o List<Integer>, tak do něj lze vložit hodnoty z proměnných jak int, tak
             * Integer. Proto se zde musí jednat pouze v tomto případě o Integer. Podobně pro ostatní objektové
             * datové typy.)
             */
            // Získání listů / kolekcí vytvořených uživatelem v editoru příkazů:
            final Map<String, VariableAbstract> listsWithDataType =
                    Instances.getVariablesWithSameDataType(new VariableList<>(null, false,
                            field.getType()), false);

            // Vytvoření syntaxe a vložení do kolekce pro okno:
            addValuesForInsertValueIntoListInEditorVariables(valueCompletionInfoSet, field, referenceName,
                    listsWithDataType.keySet());
        }


        // Přidání syntaxe pro jednorozměrné pole:
        else if (isAllowedArrayVariableDataType(field.getType())) {
            // Získání polí vytvořených uživatelem v editoru příkazů:
            final Map<String, VariableAbstract> arraysWithDataType =
                    Instances.getVariablesWithSameDataType(new VariableArray<>(false, 0,
                            ReflectionHelper.getDataTypeOfArray(field.getType())), true);

            // Vytvoření syntaxe a vložení do kolekce pro okno:
            addValuesForInsertValuesIntoEditorVariables(valueCompletionInfoSet, field, referenceName,
                    arraysWithDataType.keySet());
        }


        // Přidání syntaxe pro list:
        else if (ReflectionHelper.isDataTypeOfList(field.getType()) && isAllowedListVariableDataType(field)) {
            final DataTypeOfList classTypeOfList = ReflectionHelper.getDataTypeOfList(field);

            if (classTypeOfList.getDataTypeOfListEnum() == DataTypeOfListEnum.SPECIFIC_DATA_TYPE) {
                // Získání listů / kolekcí vytvořených uživatelem v editoru příkazů:
                final Map<String, VariableAbstract> listsWithDataType =
                        Instances.getVariablesWithSameDataType(new VariableList<>(null, false,
                                /*
                                 * Díky podmínce "isAllowedListVariableDataType" se ví, že datový typ Listu je typu
                                 * "Class<?>" a je tak povolen pro naplnění proměnné v editoru příkazů. Proto je
                                 * možné přetypovat příslušný typ na Class.
                                 */
                                (Class<?>) classTypeOfList.getDataType()), true);

                // Vytvoření syntaxe a vložení do kolekce pro okno:
                addValuesForInsertValuesIntoEditorVariables(valueCompletionInfoSet, field, referenceName,
                        listsWithDataType.keySet());
            }
        }

        return valueCompletionInfoSet;
    }


    /**
     * Vytvoření syntaxe pro naplnění proměnné v editoru příkazů hodnotou proměnné field. Jedná se o syntaxi pro vložení
     * do okna pro automatické doplnění / dokončení příkazů v editoru příkazů.
     *
     * @param valueCompletionInfoSet
     *         - kolekce, kam se mají vkládat hodnoty / vytvořené syntaxe.
     * @param field
     *         - proměnná, ze které se vezme hodnota a vloží se do proměnné
     * @param referenceName
     *         - reference na instanci, kde se nachází proměnná field - ze které se vezme hodnota.
     * @param keys
     *         - kolekce, která obsahuje názvy proměných v editoru příkazů, do kterých je možné vložit hodnotu z proměné
     *         field.
     */
    private static void addValuesForInsertValuesIntoEditorVariables(final Set<ValueCompletionInfo> valueCompletionInfoSet, final Field field, final String referenceName, final Set<String> keys) {
        keys.forEach(key -> {
            final String input = referenceName + "." + field.getName();

            final String shortDesc = TXT_PASTE_INTO + " '" + key + "' " + "variable in the editor";

            final String replacement = key + " = " + referenceName + "." + field.getName() + ";";

            valueCompletionInfoSet.add(new ValueCompletionInfo(input, replacement, shortDesc));

            /*
             * Vložení pouze textu s proměnnou, když uživatel zadá již referenci na instanci, kde se proměnná
             * nachází, tak by se již proměnna nezobrazila, proto je třeba ji zde zvlášť přidat:
             */
            final String input2 = field.getName();
            valueCompletionInfoSet.add(new ValueCompletionInfo(input2, replacement, shortDesc));
        });
    }


    /**
     * Vytvoření syntaxe pro naplnění proměnné typu jednorozměrné pole v editoru příkazů hodnotou proměnné field. Jedná
     * se o syntaxi pro vložení do okna pro automatické doplnění / dokončení příkazů v editoru příkazů.
     *
     * @param valueCompletionInfoSet
     *         - kolekce, kam se mají vkládat hodnoty / vytvořené syntaxe.
     * @param field
     *         - proměnná, ze které se vezme hodnota a vloží se do jednorozměrného pole (názvy polí jsou v kolekci
     *         keys)
     * @param referenceName
     *         - reference na instanci, kde se nachází proměnná field - ze které se vezme hodnota.
     * @param keys
     *         - kolekce, která obsahuje názvy proměných typu jednorozměrné pole vytvořených v editoru příkazů, do
     *         kterých je možné vložit hodnotu z proměné field.
     */
    private static void addValuesForInsertValueIntoArrayInEditorVariables(final Set<ValueCompletionInfo> valueCompletionInfoSet, final Field field,
                                                                          final String referenceName,
                                                                          final Set<String> keys) {
        keys.forEach(key -> {
            final String input = referenceName + "." + field.getName();

            final String shortDesc = TXT_PASTE_INTO + " '" + key + "' " + "array variable in the editor";

            final String replacement = key + "[0] = " + referenceName + "." + field.getName() + ";";

            valueCompletionInfoSet.add(new ValueCompletionInfo(input, replacement, shortDesc));

            /*
             * Vložení pouze textu s proměnnou, když uživatel zadá již referenci na instanci, kde se proměnná
             * nachází, tak by se již proměnna nezobrazila, proto je třeba ji zde zvlášť přidat:
             */
            final String input2 = field.getName();
            valueCompletionInfoSet.add(new ValueCompletionInfo(input2, replacement, shortDesc));
        });
    }


    /**
     * Vytvoření syntaxe pro naplnění proměnné typu List v editoru příkazů hodnotou proměnné field. Do listu se hodnota
     * z proměnné field vloží pomocí metody add - přidání hodnoty na konec listu, add(index, value) - přidání hodnoty na
     * v tomto případě začátek listu (index v listu) a set(index, value) - nastavení hodnoty na indexu v listu.
     * <p>
     * Jedná se o syntaxi pro vložení do okna pro automatické doplnění / dokončení příkazů v editoru příkazů.
     *
     * @param valueCompletionInfoSet
     *         - kolekce, kam se mají vkládat hodnoty / vytvořené syntaxe.
     * @param field
     *         - proměnná, ze které se vezme hodnota a vloží se do Listu pomocí jedné z výše uvedených metod (názvy
     *         listů jsou v kolekci keys)
     * @param referenceName
     *         - reference na instanci, kde se nachází proměnná field - ze které se vezme hodnota.
     * @param keys
     *         - kolekce, která obsahuje názvy proměných typu List vytvořených v editoru příkazů, do kterých je možné
     *         vložit hodnotu z proměné field.
     */
    private static void addValuesForInsertValueIntoListInEditorVariables(final Set<ValueCompletionInfo> valueCompletionInfoSet, final Field field,
                                                                         final String referenceName,
                                                                         final Set<String> keys) {
        keys.forEach(key -> {
            final String input = referenceName + "." + field.getName();

            final String txtListVariable = "List variable in the editor";

            final String shortDescAdd = TXT_PASTE_INTO + " '" + key + "' " + txtListVariable;
            final String shortDescAddAtIndex = "Paste at index into a" + " '" + key + txtListVariable;
            final String shortDescSet = "Set into a" + " '" + key + txtListVariable;

            final String replacementAdd = key + ".add(" + referenceName + "." + field.getName() + ");";
            final String replacementAddToIndex = key + ".add(0, " + referenceName + "." + field.getName() + ");";
            final String replacementSet = key + ".set(0, " + referenceName + "." + field.getName() + ");";

            valueCompletionInfoSet.add(new ValueCompletionInfo(input, replacementAdd, shortDescAdd));
            valueCompletionInfoSet.add(new ValueCompletionInfo(input, replacementAddToIndex, shortDescAddAtIndex));
            valueCompletionInfoSet.add(new ValueCompletionInfo(input, replacementSet, shortDescSet));

            /*
             * Vložení pouze textu s proměnnou, když uživatel zadá již referenci na instanci, kde se proměnná
             * nachází, tak by se již proměnna nezobrazila, proto je třeba ji zde zvlášť přidat:
             */
            final String input2 = field.getName();
            valueCompletionInfoSet.add(new ValueCompletionInfo(input2, replacementAdd, shortDescAdd));
            valueCompletionInfoSet.add(new ValueCompletionInfo(input2, replacementAddToIndex, shortDescAddAtIndex));
            valueCompletionInfoSet.add(new ValueCompletionInfo(input2, replacementSet, shortDescSet));
        });
    }


    /**
     * Získání / vytvoření výchozích ukázek deklarací proměnných v editoru příkazů.
     * <p>
     * ("Je to napsané tak, aby uživatel zadal pouze klíčové slovo 'example' a k tomu se vyfiltrují veškeré ukázky
     * deklarací.")
     *
     * @return kolekci, která bude obsahovat připravené ukázky deklarace proměnných.
     */
    static Set<ValueCompletionInfo> getExamplesOfVariableDeclaration() {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        // Připravené ukázky deklarací proměnných v editoru příkazů:
        final String shortDescByte = ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.BYTE;
        final String shortDescByteObj =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.BYTE_OBJ;
        final String shortDescShort =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.SHORT;
        final String shortDescShortObj =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.SHORT_OBJ;
        final String shortDescInt = ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.INT;
        final String shortDescInteger =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.INTEGER;
        final String shortDescLong = ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.LONG;
        final String shortDescLongObj =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.LONG_OBJ;
        final String shortDescFloat =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.FLOAT;
        final String shortDescFloatObj =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.FLOAT_OBJ;
        final String shortDescDouble =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.DOUBLE;
        final String shortDescDoubleObj =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.DOUBLE_OBJ;
        final String shortDescChar = ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.CHAR;
        final String shortDescCharacter =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.CHARACTER;
        final String shortDescBoolean =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.BOOLEAN;
        final String shortDescBooleanObj =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.BOOLEAN_OBJ;
        final String shortDescString =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.STRING;

        final String unusedVarName = getUnusedExampleVarName();

        // Vkládané hodnoty:
        final String replacementByte = ValuePreparerHelperConstants.BYTE + " " + unusedVarName + " = " + "0;";
        final String replacementByteObj = ValuePreparerHelperConstants.BYTE_OBJ + " " + unusedVarName + " = " + "0;";
        final String replacementShort = ValuePreparerHelperConstants.SHORT + " " + unusedVarName + " = " + "0;";
        final String replacementShortObj = ValuePreparerHelperConstants.SHORT_OBJ + " " + unusedVarName + " = 0;";
        final String replacementInt = ValuePreparerHelperConstants.INT + " " + unusedVarName + " = " + "0;";
        final String replacementInteger = ValuePreparerHelperConstants.INTEGER + " " + unusedVarName + " = " + "0;";
        final String replacementLong = ValuePreparerHelperConstants.LONG + " " + unusedVarName + " = " + "0;";
        final String replacementLongObj = ValuePreparerHelperConstants.LONG_OBJ + " " + unusedVarName + " = " + "0;";
        final String replacementFloat = ValuePreparerHelperConstants.FLOAT + " " + unusedVarName + " = " + "0.0;";
        final String replacementFloatObj = ValuePreparerHelperConstants.FLOAT_OBJ + " " + unusedVarName + " = 0.0;";
        final String replacementDouble = ValuePreparerHelperConstants.DOUBLE + " " + unusedVarName + " = " + "0.0;";
        final String replacementDoubleObj = ValuePreparerHelperConstants.DOUBLE_OBJ + " " + unusedVarName + " = 0.0;";
        final String replacementChar = ValuePreparerHelperConstants.CHAR + " " + unusedVarName + " = '\u0000';";
        final String replacementCharacter = ValuePreparerHelperConstants.CHARACTER + " " + unusedVarName + " = " +
                "'\u0000';";
        final String replacementBoolean = ValuePreparerHelperConstants.BOOLEAN + " " + unusedVarName + " = false;";
        final String replacementBooleanObj = ValuePreparerHelperConstants.BOOLEAN_OBJ + " " + unusedVarName + " = " +
                "false;";
        final String replacementString = ValuePreparerHelperConstants.STRING + " " + unusedVarName + " = \"My text.\";";

        // Příprava hodnot pro vkládání do okna:
        final String exampleVar = "example_var";
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleVar, replacementByte, shortDescByte));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleVar, replacementByteObj, shortDescByteObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleVar, replacementShort, shortDescShort));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleVar, replacementShortObj, shortDescShortObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleVar, replacementInt, shortDescInt));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleVar, replacementInteger, shortDescInteger));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleVar, replacementLong, shortDescLong));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleVar, replacementLongObj, shortDescLongObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleVar, replacementFloat, shortDescFloat));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleVar, replacementFloatObj, shortDescFloatObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleVar, replacementDouble, shortDescDouble));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleVar, replacementDoubleObj, shortDescDoubleObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleVar, replacementChar, shortDescChar));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleVar, replacementCharacter, shortDescCharacter));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleVar, replacementBoolean, shortDescBoolean));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleVar, replacementBooleanObj, shortDescBooleanObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleVar, replacementString, shortDescString));

        return valueCompletionInfoSet;
    }


    /**
     * Získání / vytvoření výchozích ukázek deklarací Listu v editoru příkazů.
     * <p>
     * ("Je to napsané tak, aby uživatel zadal pouze klíčové slovo 'example' a k tomu se vyfiltrují veškeré ukázky
     * deklarací.")
     *
     * @return kolekci, která bude obsahovat připravené ukázky deklarace Listu.
     */
    static Set<ValueCompletionInfo> getExamplesOfListDeclaration() {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        // Připravené ukázky deklarací proměnných v editoru příkazů:
        final String txtList = "List<";
        final String shortDescByte =
                ValuePreparerHelperConstants.DECLARATION_TEXT + txtList + ValuePreparerHelperConstants.BYTE_OBJ + ">";
        final String shortDescShort =
                ValuePreparerHelperConstants.DECLARATION_TEXT + txtList + ValuePreparerHelperConstants.SHORT_OBJ + ">";
        final String shortDescInteger =
                ValuePreparerHelperConstants.DECLARATION_TEXT + txtList + ValuePreparerHelperConstants.INTEGER + ">";
        final String shortDescLong =
                ValuePreparerHelperConstants.DECLARATION_TEXT + txtList + ValuePreparerHelperConstants.LONG_OBJ + ">";
        final String shortDescFloat =
                ValuePreparerHelperConstants.DECLARATION_TEXT + txtList + ValuePreparerHelperConstants.FLOAT_OBJ + ">";
        final String shortDescDouble =
                ValuePreparerHelperConstants.DECLARATION_TEXT + txtList + ValuePreparerHelperConstants.DOUBLE_OBJ + ">";
        final String shortDescCharacter =
                ValuePreparerHelperConstants.DECLARATION_TEXT + txtList + ValuePreparerHelperConstants.CHARACTER + ">";
        final String shortDescBoolean =
                ValuePreparerHelperConstants.DECLARATION_TEXT + txtList + ValuePreparerHelperConstants.BOOLEAN_OBJ +
                        ">";
        final String shortDescString =
                ValuePreparerHelperConstants.DECLARATION_TEXT + txtList + ValuePreparerHelperConstants.STRING + ">";

        // Vkládané hodnoty:
        final String unusedVarName = getUnusedExampleVarName();

        // Výchozí ukázky bez proměnných:
        final String txtNewArrayList = " = new ArrayList<>(";
        final String replacementByte =
                txtList + ValuePreparerHelperConstants.BYTE_OBJ + "> " + unusedVarName + txtNewArrayList + ");";
        final String replacementShort =
                txtList + ValuePreparerHelperConstants.SHORT_OBJ + "> " + unusedVarName + txtNewArrayList + ");";
        final String replacementInteger =
                txtList + ValuePreparerHelperConstants.INTEGER + "> " + unusedVarName + txtNewArrayList + ");";
        final String replacementLong =
                txtList + ValuePreparerHelperConstants.LONG_OBJ + "> " + unusedVarName + txtNewArrayList + ");";
        final String replacementFloat =
                txtList + ValuePreparerHelperConstants.FLOAT_OBJ + "> " + unusedVarName + txtNewArrayList + ");";
        final String replacementDouble =
                txtList + ValuePreparerHelperConstants.DOUBLE_OBJ + "> " + unusedVarName + txtNewArrayList + ");";
        final String replacementCharacter =
                txtList + ValuePreparerHelperConstants.CHARACTER + "> " + unusedVarName + txtNewArrayList + ");";
        final String replacementBoolean =
                txtList + ValuePreparerHelperConstants.BOOLEAN_OBJ + "> " + unusedVarName + txtNewArrayList + ");";
        final String replacementString =
                txtList + ValuePreparerHelperConstants.STRING + "> " + unusedVarName + txtNewArrayList + ");";

        // Příprava hodnot pro vkládání do okna:
        final String exampleList = "example_list";
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleList, replacementByte, shortDescByte));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleList, replacementShort, shortDescShort));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleList, replacementInteger, shortDescInteger));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleList, replacementLong, shortDescLong));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleList, replacementFloat, shortDescFloat));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleList, replacementDouble, shortDescDouble));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleList, replacementCharacter, shortDescCharacter));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleList, replacementBoolean, shortDescBoolean));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleList, replacementString, shortDescString));


        // Výchozí ukázky s naplněním:
        final String txtZeros = "0, 0";
        final String txtZerosDecimal = "0.0, 0.0";

        final String replacementByteParam =
                txtList + ValuePreparerHelperConstants.BYTE_OBJ + "> " + unusedVarName + txtNewArrayList + txtZeros + ");";
        final String replacementShortParam =
                txtList + ValuePreparerHelperConstants.SHORT_OBJ + "> " + unusedVarName + txtNewArrayList + txtZeros + ");";
        final String replacementIntegerParam =
                txtList + ValuePreparerHelperConstants.INTEGER + "> " + unusedVarName + txtNewArrayList + txtZeros +
                        ");";
        final String replacementLongParam =
                txtList + ValuePreparerHelperConstants.LONG_OBJ + "> " + unusedVarName + txtNewArrayList + txtZeros + ");";
        final String replacementFloatParam =
                txtList + ValuePreparerHelperConstants.FLOAT_OBJ + "> " + unusedVarName + txtNewArrayList + txtZerosDecimal + ");";
        final String replacementDoubleParam =
                txtList + ValuePreparerHelperConstants.DOUBLE_OBJ + "> " + unusedVarName + txtNewArrayList + txtZerosDecimal + ");";
        final String replacementCharacterParam =
                txtList + ValuePreparerHelperConstants.CHARACTER + "> " + unusedVarName + txtNewArrayList + "'\u0000" +
                        "', '\u0000'" + ");";
        final String replacementBooleanParam =
                txtList + ValuePreparerHelperConstants.BOOLEAN_OBJ + "> " + unusedVarName + txtNewArrayList + "true, " +
                        "false" + ");";
        final String replacementStringParam =
                txtList + ValuePreparerHelperConstants.STRING + "> " + unusedVarName + txtNewArrayList + "\"Value " +
                        "1\", \"Value 2\"" + ");";

        // Příprava hodnot pro vkládání do okna:
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleList, replacementByteParam, shortDescByte));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleList, replacementShortParam, shortDescShort));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleList, replacementIntegerParam, shortDescInteger));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleList, replacementLongParam, shortDescLong));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleList, replacementFloatParam, shortDescFloat));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleList, replacementDoubleParam, shortDescDouble));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleList, replacementCharacterParam, shortDescCharacter));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleList, replacementBooleanParam, shortDescBoolean));
        valueCompletionInfoSet.add(new ValueCompletionInfo(exampleList, replacementStringParam, shortDescString));

        return valueCompletionInfoSet;
    }


    /**
     * Získání / vytvoření výchozích ukázek deklarací jednorozměrného pole v editoru příkazů.
     * <p>
     * ("Je to napsané tak, aby uživatel zadal pouze klíčové slovo 'example' a k tomu se vyfiltrují veškeré ukázky
     * deklarací.")
     *
     * @return kolekci, která bude obsahovat připravené ukázky deklarace jednorozměrného pole.
     */
    static Set<ValueCompletionInfo> getExamplesOfArrayDeclaration() {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        final String brackets = "[]";
        // Připravené ukázky deklarací proměnných v editoru příkazů:
        final String shortDescByte =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.BYTE + brackets;
        final String shortDescByteObj =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.BYTE_OBJ + brackets;
        final String shortDescShort =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.SHORT + brackets;
        final String shortDescShortObj =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.SHORT_OBJ + brackets;
        final String shortDescInt =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.INT + brackets;
        final String shortDescInteger =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.INTEGER + brackets;
        final String shortDescLong =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.LONG + brackets;
        final String shortDescLongObj =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.LONG_OBJ + brackets;
        final String shortDescFloat =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.FLOAT + brackets;
        final String shortDescFloatObj =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.FLOAT_OBJ + brackets;
        final String shortDescDouble =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.DOUBLE + brackets;
        final String shortDescDoubleObj =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.DOUBLE_OBJ + brackets;
        final String shortDescChar =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.CHAR + brackets;
        final String shortDescCharacter =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.CHARACTER + brackets;
        final String shortDescBoolean =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.BOOLEAN + brackets;
        final String shortDescBooleanObj =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.BOOLEAN_OBJ + brackets;
        final String shortDescString =
                ValuePreparerHelperConstants.DECLARATION_TEXT + ValuePreparerHelperConstants.STRING + brackets;

        // Vkládané hodnoty:
        final String unusedVarName = getUnusedExampleVarName();
        final String txtNew = "new";
        final String txtDefaultSize = "[5]";

        final String replacementByte =
                ValuePreparerHelperConstants.BYTE + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.BYTE + txtDefaultSize + ";";
        final String replacementByteObj = ValuePreparerHelperConstants.BYTE_OBJ + brackets + " " + unusedVarName + " " +
                "= " + txtNew + " " + ValuePreparerHelperConstants.BYTE_OBJ + txtDefaultSize + ";";
        final String replacementShort =
                ValuePreparerHelperConstants.SHORT + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.SHORT + txtDefaultSize + ";";
        final String replacementShortObj = ValuePreparerHelperConstants.SHORT_OBJ + brackets + " " + unusedVarName +
                " = " + txtNew + " " + ValuePreparerHelperConstants.SHORT_OBJ + txtDefaultSize + ";";
        final String replacementInt =
                ValuePreparerHelperConstants.INT + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.INT + txtDefaultSize + ";";
        final String replacementInteger = ValuePreparerHelperConstants.INTEGER + brackets + " " + unusedVarName + " =" +
                " " + txtNew + " " + ValuePreparerHelperConstants.INTEGER + txtDefaultSize + ";";
        final String replacementLong =
                ValuePreparerHelperConstants.LONG + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.LONG + txtDefaultSize + ";";
        final String replacementLongObj = ValuePreparerHelperConstants.LONG_OBJ + brackets + " " + unusedVarName + " " +
                "= " + txtNew + " " + ValuePreparerHelperConstants.LONG_OBJ + txtDefaultSize + ";";
        final String replacementFloat =
                ValuePreparerHelperConstants.FLOAT + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.FLOAT + txtDefaultSize + ";";
        final String replacementFloatObj = ValuePreparerHelperConstants.FLOAT_OBJ + brackets + " " + unusedVarName +
                " = " + txtNew + " " + ValuePreparerHelperConstants.FLOAT_OBJ + txtDefaultSize + ";";
        final String replacementDouble =
                ValuePreparerHelperConstants.DOUBLE + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.DOUBLE + txtDefaultSize + ";";
        final String replacementDoubleObj =
                ValuePreparerHelperConstants.DOUBLE_OBJ + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.DOUBLE_OBJ + txtDefaultSize + ";";
        final String replacementChar =
                ValuePreparerHelperConstants.CHAR + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.CHAR + txtDefaultSize + ";";
        final String replacementCharacter =
                ValuePreparerHelperConstants.CHARACTER + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.CHARACTER + txtDefaultSize + ";";
        final String replacementBoolean = ValuePreparerHelperConstants.BOOLEAN + brackets + " " + unusedVarName + " =" +
                " " + txtNew + " " + ValuePreparerHelperConstants.BOOLEAN + txtDefaultSize + ";";
        final String replacementBooleanObj =
                ValuePreparerHelperConstants.BOOLEAN_OBJ + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.BOOLEAN_OBJ + txtDefaultSize + ";";
        final String replacementString =
                ValuePreparerHelperConstants.STRING + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.STRING + txtDefaultSize + ";";

        // Příprava hodnot pro vkládání do okna - deklarace pole s výchozí velikostí pole:
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementByte, shortDescByte));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementByteObj, shortDescByteObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementShort, shortDescShort));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementShortObj, shortDescShortObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY, replacementInt
                , shortDescInt));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementInteger, shortDescInteger));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementLong, shortDescLong));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementLongObj, shortDescLongObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementFloat, shortDescFloat));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementFloatObj, shortDescFloatObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementDouble, shortDescDouble));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementDoubleObj, shortDescDoubleObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementChar, shortDescChar));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementCharacter, shortDescCharacter));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementBoolean, shortDescBoolean));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementBooleanObj, shortDescBooleanObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementString, shortDescString));


        // Příprava hodnot pro vkládání do okna - deklarace pole s výchozími hodnotami:
        final String txtDefaultNumbers = "{0, 0};";
        final String txtDefaultNumbersDecimal = "{0.0, 0.0};";
        final String txtDefaultChars = "{'\u0000', '\u0000'};";
        final String txtDefaultBoolean = "{true, false};";
        final String txtDefaultString = "{\"My text.\", \"My text.\"};";

        final String replacementByteVal =
                ValuePreparerHelperConstants.BYTE + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.BYTE + brackets + txtDefaultNumbers;
        final String replacementByteObjVal =
                ValuePreparerHelperConstants.BYTE_OBJ + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.BYTE_OBJ + brackets + txtDefaultNumbers;
        final String replacementShortVal = ValuePreparerHelperConstants.SHORT + brackets + " " + unusedVarName + " = "
                + txtNew + " " + ValuePreparerHelperConstants.SHORT + brackets + txtDefaultNumbers;
        final String replacementShortObjVal =
                ValuePreparerHelperConstants.SHORT_OBJ + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.SHORT_OBJ + brackets + txtDefaultNumbers;
        final String replacementIntVal =
                ValuePreparerHelperConstants.INT + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.INT + brackets + txtDefaultNumbers;
        final String replacementIntegerVal = ValuePreparerHelperConstants.INTEGER + brackets + " " + unusedVarName +
                " = " + txtNew + " " + ValuePreparerHelperConstants.INTEGER + brackets + txtDefaultNumbers;
        final String replacementLongVal =
                ValuePreparerHelperConstants.LONG + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.LONG + brackets + txtDefaultNumbers;
        final String replacementLongObjVal =
                ValuePreparerHelperConstants.LONG_OBJ + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.LONG_OBJ + brackets + txtDefaultNumbers;
        final String replacementFloatVal = ValuePreparerHelperConstants.FLOAT + brackets + " " + unusedVarName + " = "
                + txtNew + " " + ValuePreparerHelperConstants.FLOAT + brackets + txtDefaultNumbersDecimal;
        final String replacementFloatObjVal =
                ValuePreparerHelperConstants.FLOAT_OBJ + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.FLOAT_OBJ + brackets + txtDefaultNumbersDecimal;
        final String replacementDoubleVal = ValuePreparerHelperConstants.DOUBLE + brackets + " " + unusedVarName + " " +
                "= " + txtNew + " " + ValuePreparerHelperConstants.DOUBLE + brackets + txtDefaultNumbersDecimal;
        final String replacementDoubleObjVal =
                ValuePreparerHelperConstants.DOUBLE_OBJ + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.DOUBLE_OBJ + brackets + txtDefaultNumbersDecimal;
        final String replacementCharVal =
                ValuePreparerHelperConstants.CHAR + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.CHAR + brackets + txtDefaultChars;
        final String replacementCharacterVal =
                ValuePreparerHelperConstants.CHARACTER + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.CHARACTER + brackets + txtDefaultChars;
        final String replacementBooleanVal = ValuePreparerHelperConstants.BOOLEAN + brackets + " " + unusedVarName +
                " = " + txtNew + " " + ValuePreparerHelperConstants.BOOLEAN + brackets + txtDefaultBoolean;
        final String replacementBooleanObjVal =
                ValuePreparerHelperConstants.BOOLEAN_OBJ + brackets + " " + unusedVarName + " = " + txtNew + " " + ValuePreparerHelperConstants.BOOLEAN_OBJ + brackets + txtDefaultBoolean;
        final String replacementStringVal = ValuePreparerHelperConstants.STRING + brackets + " " + unusedVarName + " " +
                "= " + txtNew + " " + ValuePreparerHelperConstants.STRING + brackets + txtDefaultString;

        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementByteVal, shortDescByte));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementByteObjVal, shortDescByteObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementShortVal, shortDescShort));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementShortObjVal, shortDescShortObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementIntVal, shortDescInt));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementIntegerVal, shortDescInteger));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementLongVal, shortDescLong));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementLongObjVal, shortDescLongObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementFloatVal, shortDescFloat));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementFloatObjVal, shortDescFloatObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementDoubleVal, shortDescDouble));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementDoubleObjVal, shortDescDoubleObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementCharVal, shortDescChar));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementCharacterVal, shortDescCharacter));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementBooleanVal, shortDescBoolean));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementBooleanObjVal,
                shortDescBooleanObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementStringVal, shortDescString));


        // Příprava hodnot pro vkládání do okna - deklarace pole s výchozími hodnotami vkládání pouze hodnot do závorek:
        final String replacementByteVal2 =
                ValuePreparerHelperConstants.BYTE + brackets + " " + unusedVarName + " = " + txtDefaultNumbers;
        final String replacementByteObjVal2 =
                ValuePreparerHelperConstants.BYTE_OBJ + brackets + " " + unusedVarName + " = " + txtDefaultNumbers;
        final String replacementShortVal2 = ValuePreparerHelperConstants.SHORT + brackets + " " + unusedVarName + " =" +
                " " + txtDefaultNumbers;
        final String replacementShortObjVal2 =
                ValuePreparerHelperConstants.SHORT_OBJ + brackets + " " + unusedVarName + " = " + txtDefaultNumbers;
        final String replacementIntVal2 =
                ValuePreparerHelperConstants.INT + brackets + " " + unusedVarName + " = " + txtDefaultNumbers;
        final String replacementIntegerVal2 =
                ValuePreparerHelperConstants.INTEGER + brackets + " " + unusedVarName + " = " + txtDefaultNumbers;
        final String replacementLongVal2 =
                ValuePreparerHelperConstants.LONG + brackets + " " + unusedVarName + " = " + txtDefaultNumbers;
        final String replacementLongObjVal2 =
                ValuePreparerHelperConstants.LONG_OBJ + brackets + " " + unusedVarName + " = " + txtDefaultNumbers;
        final String replacementFloatVal2 = ValuePreparerHelperConstants.FLOAT + brackets + " " + unusedVarName + " =" +
                " " + txtDefaultNumbersDecimal;
        final String replacementFloatObjVal2 =
                ValuePreparerHelperConstants.FLOAT_OBJ + brackets + " " + unusedVarName + " = " + txtDefaultNumbersDecimal;
        final String replacementDoubleVal2 = ValuePreparerHelperConstants.DOUBLE + brackets + " " + unusedVarName +
                " = " + txtDefaultNumbersDecimal;
        final String replacementDoubleObjVal2 =
                ValuePreparerHelperConstants.DOUBLE_OBJ + brackets + " " + unusedVarName + " = " + txtDefaultNumbersDecimal;
        final String replacementCharVal2 =
                ValuePreparerHelperConstants.CHAR + brackets + " " + unusedVarName + " = " + txtDefaultChars;
        final String replacementCharacterVal2 =
                ValuePreparerHelperConstants.CHARACTER + brackets + " " + unusedVarName + " = " + txtDefaultChars;
        final String replacementBooleanVal2 =
                ValuePreparerHelperConstants.BOOLEAN + brackets + " " + unusedVarName + " = " + txtDefaultBoolean;
        final String replacementBooleanObjVal2 =
                ValuePreparerHelperConstants.BOOLEAN_OBJ + brackets + " " + unusedVarName + " = " + txtDefaultBoolean;
        final String replacementStringVal2 = ValuePreparerHelperConstants.STRING + brackets + " " + unusedVarName +
                " = " + txtDefaultString;

        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementByteVal2, shortDescByte));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementByteObjVal2, shortDescByteObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementShortVal2, shortDescShort));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementShortObjVal2, shortDescShortObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementIntVal2, shortDescInt));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementIntegerVal2, shortDescInteger));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementLongVal2, shortDescLong));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementLongObjVal2, shortDescLongObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementFloatVal2, shortDescFloat));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementFloatObjVal2, shortDescFloatObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementDoubleVal2, shortDescDouble));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementDoubleObjVal2,
                shortDescDoubleObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementCharVal2, shortDescChar));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementCharacterVal2,
                shortDescCharacter));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementBooleanVal2, shortDescBoolean));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementBooleanObjVal2,
                shortDescBooleanObj));
        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                replacementStringVal2, shortDescString));

        return valueCompletionInfoSet;
    }


    /**
     * Získání názvu proměnné "example" s indexem čísla na konci. Jedná se o dosud nepoužívaný název proměnné /
     * reference na nějakou instanci či proměnnou apod.
     *
     * @return název nepoužívané proměnné / reference v syntaxi: "example(index)"
     */
    private static String getUnusedExampleVarName() {
        final String defaultVarName = "example";

        return ValuePreparerHelper.getUnusedReferenceName(defaultVarName, Instances
                .getListOfVariablesOfClassInstances());
    }


    /**
     * Získání syntaxe pro deklaraci jednorozměrného pole základních datových typů jazyka Java v editoru příkazů.
     * <p>
     * Pole se deklaruje tak, že se to pole získá z metody v instanci v digramu instancí, ze statické metody v třídě v
     * diagramu tříd nebo ze statické vnitřní třídy v diagramu tříd.
     * <p>
     * Veškeré metody muí být statické a veřejné nebo chráněné. To samé platí pro vnitřní třídy.
     *
     * @param classList
     *         - list obsahující načtené třídy z diagramu tříd (.class soubory). Z těchto tříd se načtou jednotlivé
     *         vnitřní třídy.
     * @param instancesInInstanceDiagram
     *         - instance, které se nachází v diagramu instancí. Z těchto instanci se získají potřebné metody (metody
     *         nebudou statické, ty se záskávají pouze z tříd v diagramu tříd nebo vnitřních tříd)
     *
     * @return kolekci, která bude obsahovat syntaxe pro deklaraci jednorozměrného pole v editoru příkazů tak, že se to
     * pole získá z návratové hodnoty jedné z výše zmíněných metod.
     */
    static Set<ValueCompletionInfo> getDeclarationArrayFromMethod(final List<Class<?>> classList, final Map<String,
            Object> instancesInInstanceDiagram) {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        final String unusedExampleVarName = getUnusedExampleVarName();

        valueCompletionInfoSet.addAll(getArrayDeclarationFromInstanceMethod(instancesInInstanceDiagram,
                unusedExampleVarName));

        valueCompletionInfoSet.addAll(getArrayDeclarationFromStaticMethod(classList, unusedExampleVarName));

        valueCompletionInfoSet.addAll(getArrayDeclarationFromStaticMethodInInnerClasses(classList,
                unusedExampleVarName));

        return valueCompletionInfoSet;
    }


    /**
     * Získání syntaxe pro deklaraci jednorozměrného pole z veřejných a chráněných statických metod, které se nachází ve
     * veřejných a chráněných statických vnitřních třídách v diagramu tříd.
     * <p>
     * (Metody musejí vracet jednorozměrné pole jednoho ze základních datových typů jazyka Java [číslo, text, znak,
     * logická hodnota].)
     *
     * @param classList
     *         - list, který obsahuje třídy z diagramu tříd (načtené .class soubory). Z nich se získají veškeré vnitřní
     *         třídy a z nich výše zmíněné metody.
     * @param unusedExampleVarName
     *         - název proměnné pro deklaraci příslušného pole. Jedná se o výchozí vygenerovaný název, který dosud
     *         nevyužívá žádná proměnná / reference na instanci apod.
     *
     * @return kolekci, který bude obsahovat syntaxi pro deklaraci jednorozměrného pole v editoru příkazů tak, že se to
     * pole získá z příslušné metody.
     */
    private static Set<ValueCompletionInfo> getArrayDeclarationFromStaticMethodInInnerClasses(final List<Class<?>> classList, final String unusedExampleVarName) {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        classList.forEach(c -> {
            // Získáme si veškeré vnitřní třídy v třídě c:
            final List<Class<?>> innerClassList =
                    ValuePreparerHelper.getPublicProtectedStaticInnerClasses(c.getDeclaredClasses(),
                            new ArrayList<>());

            innerClassList.forEach(ic -> {
                /*
                 * Získání veškerých veřejných a chráněných statických metod nacházející se ve vnitřních třídách
                 * třídy c.
                 */
                final List<Method> methodList = ValuePreparerHelper.getPublicProtectedMethod(ic, true,
                        new ArrayList<>());

                /*
                 * Zde se vyfiltrují pouze metody, které vracejí jedno z povolených jednorozměrných polí, které lze
                 * deklarovat v aplikaci:
                 */
                final List<Method> methodReturnArrayList =
                        methodList.stream().filter(m -> isAllowedArrayVariableDataType(m.getReturnType())).collect(Collectors.toList());


                // Pro každou výše získanou metodu se sestaví syntaxe pro zobrazení v okně pro doplňování příkazů:
                methodReturnArrayList.forEach(m -> {
                    // Název metody s parametry:
                    final String methodHeader = ReflectionHelper.getMethodNameAndParametersInText(m.getName(),
                            m.getParameters());
                    /*
                     * Název třídy s balíčky, kde se metoda nachází a text té metody - název a její parametry. Ale
                     * vnitří třídy se zobrazují s dolarem, proto tento textu bude v tom druhém okně s informacemi.
                     */
                    final String declaringClass = m.getDeclaringClass().getName() + "." + methodHeader;
                    // Ve vkládaném textu nesmí být dolary, ale místo nich desetinné tečky:
                    final String declaringClassText = declaringClass.replace("$", ".");
                    // Informace pro uživatele (za pomlčkou) pro deklaraci pole:
                    final String shortDesc =
                            ValuePreparerHelperConstants.DECLARATION_TEXT + m.getReturnType().getSimpleName();

                    final String replacement =
                            m.getReturnType().getSimpleName() + " " + unusedExampleVarName + " = " + declaringClassText + ";";

                    valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                            replacement, shortDesc));
                });
            });
        });

        return valueCompletionInfoSet;
    }


    /**
     * Získání syntaxe pro deklaraci jednorozměrného pole hodnotou (/ polem), které vrátí statická veřejná nebo chráněná
     * metoda nacházející se v třídě v diagramu tříd.
     * <p>
     * (Metoda musí vracet jednorozměrné pole základních datových typů jazyka Java [text, znak, logická hodnota
     * číslo].)
     *
     * @param classList
     *         - list načtených zkompilovaných tříd (souborů .class) z diagramu tříd.
     * @param unusedExampleVarName
     *         - název proměnné pro deklaraci - jedná se o výchozí název, který byl vygenerován, pro dosud neexistující
     *         proměnnou / referenci.
     *
     * @return kolekci, která bude obsahovat možnosti pro deklaraci proměnné typu jednorozměrné pole jednoho ze
     * základních datových typů jazyka Java.
     */
    private static Set<ValueCompletionInfo> getArrayDeclarationFromStaticMethod(final List<Class<?>> classList,
                                                                                final String unusedExampleVarName) {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        classList.forEach(c -> {
            final List<Method> methodList = ValuePreparerHelper.getPublicProtectedMethod(c, true, new ArrayList<>());

            /*
             * Zde se vyfiltrují pouze metody, které vracejí jedno z povolených jednorozměrných polí, které lze
             * deklarovat v aplikaci:
             */
            final List<Method> methodReturnArrayList =
                    methodList.stream().filter(m -> isAllowedArrayVariableDataType(m.getReturnType())).collect(Collectors.toList());

            methodReturnArrayList.forEach(m -> {
                // Název metody s parametry:
                final String methodHeader = ReflectionHelper.getMethodNameAndParametersInText(m.getName(),
                        m.getParameters());
                // Název třídy s balíčky, kde se metoda nachází a text té metody - název a její parametry:
                final String declaringClass = m.getDeclaringClass().getName() + "." + methodHeader;
                // Informace pro uživatele (za pomlčkou) pro deklaraci pole:
                final String shortDesc =
                        ValuePreparerHelperConstants.DECLARATION_TEXT + m.getReturnType().getSimpleName();
                // Syntaxe pro vložení:
                final String replacement =
                        m.getReturnType().getSimpleName() + " " + unusedExampleVarName + " = " + declaringClass + ";";

                valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                        replacement, shortDesc));
            });
        });

        return valueCompletionInfoSet;
    }


    /**
     * Získání syntaxe pro deklaraci jednorozměrného pole povolených (/ základních) datových typů jazyka Java.
     * <p>
     * Princip je takový, že se získají veškeré veřejné a chráněné metody ze všech instancí v diagramu instancí, z nich
     * se vyfiltrují pouze ty, které vracejí jednorozměrné pole výše zmíněného datového typu a z toho se vytvoří syntaxe
     * pro vložení do editoru.
     *
     * @param instancesInInstanceDiagram
     *         - instance v diagramu instancí, které vytvořil uživatel z tříd v diagramu tříd.
     * @param unusedExampleVarName
     *         - název proměnné, který se má využít pro deklaraci příslušného pole jako výchozí název proměnné, uživatel
     *         jej může změnit.
     *
     * @return kolekci, která bude obsahovat syntaxe pro deklaraci jednorozměrného pole z hodnoty ze zavolané metody.
     */
    private static Set<ValueCompletionInfo> getArrayDeclarationFromInstanceMethod(final Map<String, Object> instancesInInstanceDiagram, final String unusedExampleVarName) {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        // Metody v instancích (v diagramu instancí):
        instancesInInstanceDiagram.forEach((key, value) -> {
            // Získání veřejných a chráněných nestatických metod z instance value:
            final List<Method> methodList = ValuePreparerHelper.getPublicProtectedMethod(value.getClass(), false,
                    new ArrayList<>());


            /*
             * Zde se vyfiltrují pouze metody, které vracejí jedno z povolených jednorozměrných polí, které lze
             * deklarovat v aplikaci:
             */
            final List<Method> methodReturnArrayList =
                    methodList.stream().filter(m -> isAllowedArrayVariableDataType(m.getReturnType())).collect(Collectors.toList());


            // Převedení metod do požadované syntaxe:
            methodReturnArrayList.forEach(method -> {
                // Název metody s parametry:
                final String methodHeader = ReflectionHelper.getMethodNameAndParametersInText(method.getName(),
                        method.getParameters());
                // Informace pro uživatele (za pomlčkou):
                final String shortDesc =
                        ValuePreparerHelperConstants.DECLARATION_TEXT + method.getReturnType().getSimpleName();

                final String replacement = method.getReturnType().getSimpleName() + " " + unusedExampleVarName + " = "
                        + key + "." + methodHeader + ";";

                valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_ARRAY,
                        replacement, shortDesc));
            });
        });

        return valueCompletionInfoSet;
    }


    /**
     * Získání ukázkové syntaxe pro psaní podmínek.
     * <p>
     * V podstatě se jedná pouze o ukázku podmínek, které lze (v jaké syntaxi) zadávat do editoru příkazů, ale mělo by
     * to uživatelům pomoci k "pochopení" jak psát ty podmínky v jazyce Java.
     *
     * @return kolekci, která bude obsahovat připravené ukázky podmínek pro psaní v editoru příkazů.
     */
    static Set<ValueCompletionInfo> getConditionExamples() {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        // Ukázka jednoduché podmínky:
        final String replacement = "if (1 < 2, true, false)";
        final String shortDesc = "Simple condition";

        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_CONDITION,
                replacement, shortDesc));


        // Podmínka s nějakými výrazy podoporovanými aplikací:
        final String replacement2 = "if (min(1, 2, 3) <= max(1, 2, 3) && sin(1) < cos(1), -1, 1)";
        final String shortDesc2 = "Condition with multiple statements";

        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_CONDITION,
                replacement2, shortDesc2));


        // Podmínka s matematickými výrazy:
        final String replacement3 = "if (e <= pi && (random() >= 0.5 || (sqrt(16) % 2) != 1), true, false)";
        final String shortDesc3 = "Condition with math statements";

        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_CONDITION,
                replacement3, shortDesc3));

        return valueCompletionInfoSet;
    }


    /**
     * Získání ukázkové syntaxe pro psaní matematických výrazů v editoru příkazů. Jedná se v podstatě o ukázky vybraných
     * matematických výrazů / operací - jak je vypočítat / zadávat a ukázky složení výrazu do podmínky - testování
     * výsledku.
     *
     * @return kolekci, která bude obsahovat ukázky zadávání matematických výrazů. A jejich skládání do podmínek, tedy,
     * otestování výsledku matematického výrazu.
     */
    static Set<ValueCompletionInfo> getMathExamples() {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        // Ukázka matematického výpočtu:
        final String replacement = "sin(1) + sqrt(3) * cos(1)";
        final String shortDesc = "Mathematical calculation";

        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_MATH,
                replacement, shortDesc));


        // Ukázka matematického výpočtu se zaokrouhleným výsledkem:
        final String replacement2 = "round(sin(1) + sqrt(3) * cos(1), 2)";
        final String shortDesc2 = "Mathematical calculation with rounding result";

        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_MATH,
                replacement2, shortDesc2));


        // Ukázka matematického výpočtu s testováním výsledku:
        final String replacement3 = "round(sin(1) + sqrt(3) * cos(1), 2) == 1.75";
        final String shortDesc3 = "Mathematical calculation with test of the desired result";

        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_MATH,
                replacement3, shortDesc3));


        // Následuje v podstatě to samé, akorát s trochu jiným příkladem

        // Ukázka matematického výpočtu:
        final String replacemen4 = "4 * sin(-5)^3 + 4 * sin(-5)^2 - 3 * sin(-5)";

        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_MATH,
                replacemen4, shortDesc));


        // Ukázka matematického výpočtu se zaokrouhleným výsledkem:
        final String replacement5 = "round(4 * sin(-5)^3 + 4 * sin(-5)^2 - 3 * sin(-5), 2)";

        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_MATH,
                replacement5, shortDesc2));


        // Ukázka matematického výpočtu s testováním výsledku:
        final String replacement6 = "round(4 * sin(-5)^3 + 4 * sin(-5)^2 - 3 * sin(-5), 2) == 0.29";

        valueCompletionInfoSet.add(new ValueCompletionInfo(ValuePreparerHelperConstants.EXAMPLE_MATH,
                replacement6, shortDesc3));

        return valueCompletionInfoSet;
    }
}