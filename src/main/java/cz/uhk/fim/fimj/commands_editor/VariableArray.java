package cz.uhk.fim.fimj.commands_editor;

/**
 * Tato třída slouží jako reprezentace jednorozměrného pole.
 * <p>
 * Po vytvoření instance této třídy se vytvoří i instance jednorozměrného pole daného typu.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class VariableArray<T> extends VariableAbstract {

    /**
     * Jednorozměrné pole, "se kterým bude uživatel manipulovat" v editoru příkazů
     */
    private T[] array;


    /**
     * Proměnné, ve které bude uložen datový typ hodnoty, které lze do pole vkládat.
     * <p>
     * Například, když bude pole int[] var;, pak v této proměnné bude datový typ int.
     */
    private final Class<T> typeArray;


    /**
     * Proměnná, která bude udržovat datový typ jednorozměrného pole kvůli naplnění tohoto pole ('array') pomocí
     * zavolání metody nad instancí třídy.
     * <p>
     * Například, když bude pole int[] var;, pak v této prměnné bude [i -> což znači jednorozměrné pole typu int.
     * <p>
     * Když se zavolá metoda nad instancí třídy, tak vrácí přímo typ pole, jenže kdybych to testoval pomocí generických
     * typů nebo nad polem: array.class, apod. nikdy by se datové typy metody a pole nerovnaly, tak musím mít
     * následující proměnnou, která si bude udržovat jeden ze zvolených datových typů jednorozměrného pole, a pro
     * zavolání metody budu tuto proměnnou používat pro porovnání datových typů pole:
     */
    private final Class<?> classTypeOfArrayForMethod;


    /**
     * Konstruktor třídy.
     *
     * @param isFinal
     *         - logická hodnota o tom, zda má být pole final nebo ne
     * @param array
     *         - vytvořené pole
     * @param typeArray
     *         - datový typ hodnoty, které lze do pole vkládat
     */
    @SuppressWarnings("unchecked")
    public VariableArray(final boolean isFinal, T[] array, final Class<?> typeArray) {
        super(isFinal);

        this.array = array;
        this.typeArray = (Class<T>) typeArray;

        classTypeOfArrayForMethod = getClassTypeForMethod();
    }


    /**
     * Tento konstruktor slouží pro vytvoření jednorozměrného pole s definovanou velikostí a ne hodnotami
     *
     * @param isFinal
     *         - logická hodnota o tom, zda má být pole final nebo ne
     * @param size
     *         - velikost pole, které se má vytvořit
     * @param typeArray
     *         - datový typ hodnot, které lze do pole vkládat
     */
    @SuppressWarnings("unchecked")
    public VariableArray(final boolean isFinal, final int size, final Class<?> typeArray) {
        super(isFinal);

        array = (T[]) new Object[size];
        this.typeArray = (Class<T>) typeArray;

        classTypeOfArrayForMethod = getClassTypeForMethod();
    }


    /**
     * Metoda, která vrátí jednorozměrné pole, které reprezentuje tato třída v editoru příkazů
     *
     * @return jednorozměrné pole - array
     */
    public final T[] getArray() {
        return array;
    }


    /**
     * Metoda, která jistí a vrátí datový typ hodnot, které lze vkládat do jednorozměrného pole, které reprezentuje tato
     * tříd
     *
     * @return datový typ hodnot, které lze vkládat do tohoto pole ('proměnné - array')
     */
    public final Class<T> getTypeArray() {
        return typeArray;
    }


    /**
     * Metoda, která nastaví hodnotu na nějakém indexu v poli
     * <p>
     * Note: Zde nemohu přetypovat hodnotu pomocí metody cast u datového typu typeArray.cast... protože by mohla nastat
     * vyjímka, například při parsování objektového typu na primitivní, apod.
     *
     * @param index
     *         - index, na který se má nastavit hodnota
     * @param objValue
     *         - hodnota, která se má nastavit na zadaný index
     */
    @SuppressWarnings("unchecked")
    public final void setValueToIndex(final int index, final Object objValue) {
        array[index] = (T) objValue;
    }


    /**
     * Metoda, která vrátí hodnotu z pole, která se nachází na zadaném indexu
     *
     * @param index
     *         - index v poli, odkud se má vzít hodnota
     *
     * @return hodnotu z pole na zadaném indexu
     */
    public final T getValueAtIndex(final int index) {
        return array[index];
    }


    /**
     * Metoda, která "nastaví toto pole" za jiné
     *
     * @param array
     *         - nové pole, kterým se naplní toto stávající
     */
    @SuppressWarnings("unchecked")
    public final void setArray(final Object array) {
        new ParseToJavaTypes();

        final Object[] testArray = ParseToJavaTypes.convertToObjectArray(array);

        this.array = (T[]) testArray;
    }


    /**
     * Metoda, která vrátí délku pole, resp. počet prvků v poli
     *
     * @return velikost pole
     */
    public final int getArrayLength() {
        return array.length;
    }


    /**
     * Metoda, která vrátí datový typ pro porovnání datových typů pro metodu, protože pro návratovou hodnotu metody
     * nelze porovnávat pomocí generických typů z této třídy
     *
     * @return vrátí typ jednorozměrného pole pro porovnání, zda se může toto pole naplneit jiným - tím kterým vrátí
     * metoda
     */
    public final Class<?> getClassTypeOfArrayForMethod() {
        return classTypeOfArrayForMethod;
    }


    /**
     * Metoda, která vrátí datový typ jednorozměrného pole, který se následně nastaví do proměnné:
     * classTypeOfArrayForMethod, a pomocí této proměnné půjdou porovnávat datové typy pro návratovou hodnotu metody a
     * této třídy, aby šlo naplnit toho pole nějakým jiným polem, které vrátí daná zavolaná metoda
     *
     * @return datový typ jednorozměrného pole
     */
    private Class<?> getClassTypeForMethod() {
        switch (typeArray.getSimpleName()) {
            case "byte":
                return byte[].class;

            case "Byte":
                return Byte[].class;

            case "short":
                return short[].class;

            case "Short":
                return Short[].class;

            case "int":
                return int[].class;

            case "Integer":
                return Integer[].class;

            case "long":
                return long[].class;

            case "Long":
                return Long[].class;

            case "float":
                return float[].class;

            case "Float":
                return Float[].class;

            case "double":
                return double[].class;

            case "Double":
                return Double[].class;

            case "char":
                return char[].class;

            case "Character":
                return Character[].class;

            case "boolean":
                return boolean[].class;

            case "Boolean":
                return Boolean[].class;

            case "String":
                return String[].class;

            default:
                return null;
        }
    }
}