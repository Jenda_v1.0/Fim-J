package cz.uhk.fim.fimj.commands_editor.history.project_history;

import cz.uhk.fim.fimj.commands_editor.history.HistoryCompletion;

import javax.swing.*;
import java.awt.event.KeyEvent;

/**
 * Okno, které obsahuje historii uživatelem zadaných příkazů do editoru příkazů.
 * <p>
 * Okno se otevře po stisknutí klávesové zkratky: "Ctrl + H".
 *
 * <i>Tuto historii je možné smazat Ctrl + L nebo položkou "Vymazat" v kontextovém menu v editoru příkazů. Historie je
 * ukládána pouze pro konkrétní otevřený projekt. Pří změně či zavření projektu bude vymazána.</i>
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 22.09.2018 22:35
 */

public class ProjectHistoryCompletion extends HistoryCompletion {

    /**
     * Klávesová zkratka, která otevře okno s historií.
     */
    private static final KeyStroke TRIGGER_KEY = KeyStroke.getKeyStroke(KeyEvent.VK_H, KeyEvent.CTRL_DOWN_MASK);


    public ProjectHistoryCompletion(final boolean addIndexes) {
        super(TRIGGER_KEY, addIndexes);
    }


    /**
     * Vymazání historie zadaných příkazů.
     */
    public void clearHistory() {
        clearAutoCompletionProvider();
    }


    @Override
    public void addCompletion(final String command, final String result) {
        addCompletionToProvider(command, result);
    }
}