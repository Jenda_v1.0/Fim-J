package cz.uhk.fim.fimj.commands_editor.history;

import org.fife.ui.autocomplete.*;

import javax.swing.*;

/**
 * Komponenty pro "sestavení" okna, které obsahuje historii (potvrzených) příkazů v editoru příkazů.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 22.09.2018 22:35
 */

public abstract class HistoryCompletion {

    /**
     * Zda se mají do příkazů přidávat indexy / číslování.
     * <p>
     * Pokud se budou přidávat indexy (addIndexes = true), hodnoty budou seřazené, ale nebude je moci filtrovat dle
     * zadaných příkazů (jejich částí). Pokud se nebudou přídávat indexy (addIndexes = false), pak se nebudou zobrazovat
     * čísla před příkazem, bude možné filtrovat příkazy dle zadaných částí, ale příkazy nebudou seřazeny dle historie -
     * podle pořadí, v jakém je uživatel zadal v editoru.
     */
    private final boolean addIndexes;

    /**
     * Index příkazu, který značí pořadí zadaného příkazu.
     * <p>
     * Tento index bude zobrazen před příkazem v okně v případě, že addIndexes je true.
     */
    private int index;


    /**
     * A DefaultCompletionProvider is the simplest concrete implementation of CompletionProvider. This provider has no
     * understanding of language semantics. It simply checks the text entered up to the caret position for a match
     * against known completions. This is all that is needed in the majority of cases.
     */
    private DefaultCompletionProvider provider;


    /**
     * "Okno" s hodnotami (/ příkazy) pro doplnění do editoru.
     */
    private AutoCompletion autoCompletion;


    /**
     * Konstruktor třídy.
     *
     * @param triggerKey
     *         - klávesová zkratka, kterou uživatel zobrazí okno s příkazy pro doplnění.
     * @param addIndexes
     *         - true v případě, že se mají přidávat indexy před příkazy
     */
    public HistoryCompletion(final KeyStroke triggerKey, final boolean addIndexes) {
        this.addIndexes = addIndexes;

        // Nastavení indexu prvního příkazu:
        index = 1;

        createAutoCompletion(triggerKey);
    }


    /**
     * Vytvoření komponenty pro zobrazování příkazů.
     *
     * @param triggerKey
     *         - klávesová zkratka, která otevře okno s příkazy.
     */
    private void createAutoCompletion(final KeyStroke triggerKey) {
        provider = new DefaultCompletionProvider();

        autoCompletion = createAutoCompletion(provider);

        // Klávesová zkratka, která otevře okno:
        autoCompletion.setTriggerKey(triggerKey);
    }


    /**
     * Vytvoření komponenty pro zobrazování příkazů.
     *
     * @param provider
     *         - "klíčová slova", které se přidají do okna pro doplňování.
     *
     * @return prázdné okno s příkazy s výchozím nastavením.
     */
    private static AutoCompletion createAutoCompletion(final CompletionProvider provider) {
        final AutoCompletion autoCompletion = new AutoCompletion(provider);

        autoCompletion.setAutoActivationEnabled(true);

        autoCompletion.setShowDescWindow(true);

        autoCompletion.setAutoCompleteSingleChoices(false);

        autoCompletion.setParameterAssistanceEnabled(true);

        return autoCompletion;
    }


    /**
     * Doplnění položky do okna s příkazy.
     *
     * <i>"Přidání nového příkazu pro doplnění do editoru."</i>
     *
     * @param replacementText
     *         - příkaz (/ text), který se má vložit do editoru po zvolení položky. Zároveň je to text, který uživatel
     *         uvidí při volbě / filtrování příkazu.
     * @param shortDesc
     *         - popisek, který se má zobrazit v "druhém" okně. Jedná se o "doplňkovou informaci" k příkazu (výsledek
     *         operace).
     */
    protected void addCompletionToProvider(final String replacementText, final String shortDesc) {
        if (addIndexes)
            // Syntaxe: "index - replacementText -> v novém okně shortDesc"
            provider.addCompletion(new ShorthandCompletion(provider, String.valueOf(index++), replacementText,
                    replacementText, shortDesc));

            // Syntaxe: "replacementText -> v novém okně shortDesc"
        else
            provider.addCompletion(new ShorthandCompletion(provider, replacementText, replacementText, null,
                    shortDesc));
    }


    /**
     * Instalace / Nastavení okna s hodnotami pro doplnění ke konkrétní JTextArea, nad kterou se má zobrazit a případně
     * doplnit uživatelem zvolený příkaz.
     *
     * @param textArea
     *         - komponenta, ke které se má nastavit okno, tedy, "nad kterou" se má otevřít okno s příkazy pro doplnění
     *         a kam se budou doplňovat zvolené příkazy.
     */
    public void installAutoCompletion(final JTextArea textArea) {
        autoCompletion.install(textArea);
    }


    /**
     * Vymazaní veškerých příkazů, které jsou k dispozici pro doplnění.
     *
     * <i>Vymaže se veškerá historie příkazů, tudíž nebude k dispozici žádný příkaz pro doplnění.</i>
     */
    protected void clearAutoCompletionProvider() {
        provider.clear();
    }


    /**
     * Doplnění nového příkazu do okna s příkazy (historií).
     *
     * <i>Metoda se volá po potvrzení příkazu uživatelem, kdy se do historie přidá nový příkaz.</i>
     *
     * @param command
     *         - příkaz, který uživatel zadal a má se doplnit do historie.
     * @param result
     *         - výsledek příkazu, například "Úspěch / Neúspěch", chybová hláška apod.
     */
    public abstract void addCompletion(final String command, final String result);
}