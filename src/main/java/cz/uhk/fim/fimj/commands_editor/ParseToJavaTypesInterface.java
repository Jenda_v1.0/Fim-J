package cz.uhk.fim.fimj.commands_editor;

/**
 * Tento interface sloužípro deklaraci metody pro parsováná textu - String na některé potřebné datové typy Javy (v
 * pdstatě libovolný zadaný typ)
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface ParseToJavaTypesInterface<P> {

    /**
     * Metoda, která "naparsuje" text v parametru na zadaný datový typ pomocí generiky
     *
     * @param value
     *         - text - typu java.land.String, který je třeba naparsovat na zadaný datový typ
     *
     * @return Naparsovaná hodnota čí kolekce z parametru
     */
    P parse(final String value);
}