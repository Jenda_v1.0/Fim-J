package cz.uhk.fim.fimj.commands_editor;

/**
 * Tato třída slouží jako "proměnná" pro uchování v podstatě libovolné hodnoty, kterou lze načíst z instance třídy.
 * <p>
 * Dále lze tuto hodnotu změnit na null, či jinou hodnotu
 * <p>
 * Třída dále obsahuje logickou proměnnou (typu boolean), ve které bude uchována hondotu true / false, dle toho, zda mám
 * být proměnná typu objekt final nebo ne.
 * <p>
 * Pokud bude final, tak půjde pouze poprvé inicializovat - deklarovat na null hodnotu či naplnit nějakou hodnotou a
 * dále již nepůjde editovat, pokud bude logická hodnota false, tak proměnná typu objekt není final, tak lze liboolně
 * (dle možností editoru příkazů) editovat její hodnota
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class Variable extends VariableAbstract {

    private final Class<?> type;


    /**
     * Proměnná pro uchování hodnoty
     */
    private Object objValue;


    /**
     * Konstruktor pro vytvoření "proměnné" v editoru příkazů, do konsturktu se na první pozici parametru vloží hodnota
     * proměnné a na druhé logická informace o tom, zda je final nebo be, neboli, zda lze hodnotu editoru čí nikoliv
     *
     * @param objValue
     *         - hodnota proměnné
     * @param isFinal
     *         logická hodnoa, zda je proměnná final nebo ne
     */
    public Variable(final Object objValue, final boolean isFinal, final Class<?> type) {
        super(isFinal);

        this.objValue = objValue;
        this.type = type;
    }


    /**
     * Vrátí objekt, ve kterém je uchována hodnota proměnné
     *
     * @return objekt, ve kterém je uchována hodnota proměnné
     */
    public final Object getObjValue() {
        return objValue;
    }


    /**
     * Nastaví hodnotu proměnné
     *
     * @param objValue
     *         nová hodnota proměnné
     */
    public final void setObjValue(final Object objValue) {
        this.objValue = objValue;
    }


    public final Class<?> getDataTypeOfVariable() {
        return type;
    }
}