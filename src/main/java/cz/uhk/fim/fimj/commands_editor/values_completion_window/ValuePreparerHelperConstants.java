package cz.uhk.fim.fimj.commands_editor.values_completion_window;

/**
 * Třída obsahuje konstanty, které se využívají ve třídě {@link ValuePreparerHelper}. Jedná se pouze o konstanty
 * například pro sestavení syntaxe pro deklarací proměnných apod.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 04.08.2018 15:42
 */

class ValuePreparerHelperConstants {

    private ValuePreparerHelperConstants() {
    }

    /**
     * Klíčové slovo "example_", které značí nějakou ukázku.
     * <p>
     * Tato hodnota je zde pouze jednou, aby se nemusela opakovaně psát všude, kde se tato hodnota využívá.
     */
    private static final String EXAMPLE = "example_";

    /**
     * Text, která je vkládán do pole pro ukázky deklarace proměnných.
     */
    static final String DECLARATION_TEXT = "Declaration of variable type" + " ";


    static final String TXT_FINAL_KEY_WORD = "final";


    /**
     * Klíčové slovo pro filtrování deklarace proměnné typu jednorozměrné pole v editor příkazů.
     * <p>
     * Jedná se o klíčové slovo, které uživatel bude zadávat v editoru příkazů a v okně pro automatické doplňování /
     * dokončování příkazů se budou filtrovat hodnoty pro deklaraci proměnné typu jednorozměrné pole (mimo jiné).
     */
    static final String EXAMPLE_ARRAY = EXAMPLE + "array";

    /**
     * Klíčové slovo pro filtrování ukázky podmínky.
     */
    static final String EXAMPLE_CONDITION = EXAMPLE + "condition";

    /**
     * Klíčové slovo pro filtrování ukázky skládání matematických výrazů / výpočtů.
     */
    static final String EXAMPLE_MATH = EXAMPLE + "math";


    // Podporované datové typy pro pole a v případě objektových typů i pro List:
    static final String BYTE = "byte";
    static final String BYTE_OBJ = "Byte";
    static final String SHORT = "short";
    static final String SHORT_OBJ = "Short";
    static final String INT = "int";
    static final String INTEGER = "Integer";
    static final String LONG = "long";
    static final String LONG_OBJ = "Long";
    static final String FLOAT = "float";
    static final String FLOAT_OBJ = "Float";
    static final String DOUBLE = "double";
    static final String DOUBLE_OBJ = "Double";
    static final String CHAR = "char";
    static final String CHARACTER = "Character";
    static final String BOOLEAN = "boolean";
    static final String BOOLEAN_OBJ = "Boolean";
    static final String STRING = "String";
}
