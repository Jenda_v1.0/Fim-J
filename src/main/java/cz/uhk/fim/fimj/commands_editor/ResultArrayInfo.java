package cz.uhk.fim.fimj.commands_editor;

/**
 * Tato třída slouží pouze jako "prostředník" pro předání výsledků jedné metody do jiné metody pro "zhodnocení".
 * <p>
 * Konkrétně jde o to, že se při pokusu o inkrementace / dekrementaci nějaké hodnoty může dojít k chybě, že se nemusí
 * najít příslušné indexy v poli apod. Tak proto tato třída, abych si předal potřebné informace.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ResultArrayInfo {

    /**
     * Logická hodnota, která značí, zda se podařilo inkrementovat / dekrementovat hodnotu v poli. True značí, že se
     * hodnota úspěšně inkrementovala / dekrementovala. False, že někde došlo k nějaké chybě.
     */
    private final boolean set;


    /**
     * Tato proměnná se použije pouze v případě, že proměná set je false. Tato proměnná značí, zda došlo k té chybě, že
     * se nenašli indexy v poli pro příslušnou hodnotu k inkrementaci / dekrementaci (hodnota false).
     * <p>
     * Nebo, bude true, když se našly indexy hodnoty v poli, ale nepodařilo se tu hodnotu inkrementovat /
     * dekremetnovat,
     * například z toho důvodu, že je hodnota null, nebo se nejedná o čislo.
     * <p>
     * <p>
     * Note: Pro tuto proměnnou by bylo lepší vytvořit například nějaký výčet pro určení toho, zda se nepodařilo
     * nalezenou hodnotu v poli inkremetnovat / dekrementovat z toho důvodu, že se jedná o null hodnotu, nebo o jinou
     * hodnotu, ale ne o číslo apod.
     * <p>
     * Já v aplikaci testji puze tyto dvě hodnoty, a zdůvodním to přesně tak, že je to buď null hodnota nebo to není
     * číslo, proto si zde vystačím s touto logickou proměnnou, ale kdyby se chtěli upřesnit texty pro konkrétní chybu,
     * nebo nějaká doplnit, je třeba sem dát výčet, ale to už je práce navíc, a byly by potřeba dle toho dopsat i
     * chybová hlášení apod. Pro mě je to již trochu zbytečné.
     */
    private final boolean foundIndeces;


    /**
     * Konstruktor této třídy.
     *
     * @param set
     *         - logická hodnota o tom, zda se podařilo hodnotu inkrementovat / dekrementovat -> hodnota true, jinak,
     *         když se to nepovedlo, tak hodnota false.
     * @param foundIndeces
     *         - logická hodnota o tom, zda došlo k chybě, že se nenašly v poli zadané indexy (hodnota true), nebo, že
     *         se nepodařilo hodnotu inkrementovat / dekremetnovat (hodnota false).
     */
    public ResultArrayInfo(final boolean set, final boolean foundIndeces) {
        super();

        this.set = set;
        this.foundIndeces = foundIndeces;
    }


    /**
     * Zda se podařilo hodnotu na zadaném indexu ve dvourozměrném poli inkrementovat nebo dekrementovat.
     *
     * @return info výše.
     */
    public boolean isSet() {
        return set;
    }

    /**
     * Info o tom, že se nepodařilo hodnotu na zadaném indexu ve dvourozměrném poli inkrementovat / dekrementovat
     * protože je to null hodnota nebo to není číslo.
     *
     * @return info výše.
     */
    public boolean isFoundIndeces() {
        return foundIndeces;
    }
}
