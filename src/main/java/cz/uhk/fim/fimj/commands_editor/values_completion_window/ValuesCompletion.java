package cz.uhk.fim.fimj.commands_editor.values_completion_window;

import cz.uhk.fim.fimj.commands_editor.Editor;
import cz.uhk.fim.fimj.commands_editor.Variable;
import cz.uhk.fim.fimj.commands_editor.VariableArray;
import cz.uhk.fim.fimj.commands_editor.VariableList;
import org.fife.ui.autocomplete.AutoCompletion;
import org.fife.ui.autocomplete.CompletionProvider;
import org.fife.ui.autocomplete.DefaultCompletionProvider;
import org.fife.ui.autocomplete.ShorthandCompletion;

import javax.swing.*;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Třída, která slouží pro vytvoření okno pro automatické doplňmování textu / hodnot do editoru příkazů.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 28.07.2018 0:48
 */

public final class ValuesCompletion {

    /**
     * Získávání hodnot pro doplnění do okna pro automatické dokončení, ale získání v smyslu získání samotných "hodnot"
     * / objektů, například metod, proměnných apod. Tyto hodnoty se teprve budou převádět do podoby textu a zpracovávat
     * do okna.
     */
    private static final ValueGetr VALUE_GETTER = new ValueGetterImpl();

    /**
     * Zpracování hodnot získaných v cz.uhk.fim.fimj.commands_editor.values_completion_window
     * .ValuesCompletion#VALUE_GETTER do okna pro automatické dokončení v editoru příkazů. Resp. převedení hodnot
     * získaných v uvedené implementaci do podoby textu a zpracování jej do okna pro automatické doplnění uživatelem.
     */
    private static final ValuePreparer VALUE_PREPARER = new ValuePreparerImpl();


    /**
     * V podstatě okno, které se zobrazí po stisknutí kombinace klávest Ctrl + Space.
     */
    private static AutoCompletion autoCompletion;


    private ValuesCompletion() {
    }

    /**
     * Metoda, která zařídí vytvoření a nastavení okna pro doplňování hodnot.
     *
     * @param textArea
     *         - textové pole, kterému se má nastavit zpřístupnění ple pro automatické dokončování hodnot.
     */
    static void setAutoCompletion(final JTextArea textArea) {
        // A DefaultCompletionProvider is the simplest concrete implementation
        // of CompletionProvider. This provider has no understanding of
        // language semantics. It simply checks the text entered up to the
        // caret position for a match against known completions. This is all
        // that is needed in the majority of cases.
        final DefaultCompletionProvider provider = new DefaultCompletionProvider();

        // Vytvoření hodnot pro doplnění:
        createValuesForCompletion(provider);


        // Vytvoření a nastavení okna pro doplňování hodnot:
        createAutoCompletion(provider, textArea);
    }


    /**
     * Metoda, která vytvoří a nastaví to tzv. "automatické doplňování / dokončování", resp. to, že se po stisknutí
     * kombinace kláves (Ctrl + Space) otevře to dialogové okno se zkratkami pro doplnění hodnot do editoru příkazů.
     *
     * @param completionProvider
     *         - v podstatě klíčová slova, která se předají do toho okna s doplňováním, ze kterého si bude uživatel
     *         vybírat.
     * @param textArea
     *         - textové pole, do kterého se má "nainstalovat" pole pro automatické dokončování hodnot.
     */
    private static void createAutoCompletion(final CompletionProvider completionProvider, final JTextArea textArea) {
        autoCompletion = new AutoCompletion(completionProvider);

        autoCompletion.setAutoCompleteEnabled(true);

        // Zobrazí se popis u slova z nápovědy
        autoCompletion.setShowDescWindow(true);

        autoCompletion.setAutoActivationEnabled(true);

        autoCompletion.setAutoCompleteSingleChoices(true);

        /*
         * Note:
         *
         * Výchozí klávesová zkratka pro otevření okna je Ctrl + Space. V tomto případě to tak má být. Proto zde
         * nebude žádná zkratka nastavena.
         */

        autoCompletion.install(textArea);
    }


    /**
     * Nastavení okna s nápovědou ke konkrétní komponentě JTextArea, nad kterou se má otevřít.
     *
     * @param textArea
     *         - komponenta, nad kterou se má otevřít okno s příkazy pro doplnění.
     */
    public static void installAutoCompletion(final JTextArea textArea) {
        autoCompletion.install(textArea);
    }


    /**
     * Zrušení komponenty (okna) pro doplňování / dokončování příkazů v editoru příkazů.
     */
    public static void uninstallAutoCompletion() {
        /*
         * Note:
         *
         * Samotná metoda "uninstall" nefunguje, ošetřeno tak, že se nastaví "prázdné hodnoty" (prázdný provider,
         * který neobsahuje žádné hodnoty k doplnění). Už je toto by mělo způsobit, že se to okno nezobrazí, když
         * neobsahuje žádné hodnoty. Ale navíc je doplněna instalace na nové / jiné textové pole. Ne na editor příkazů.
         */
        if (autoCompletion != null) {
            autoCompletion.uninstall();

            autoCompletion = new AutoCompletion(new DefaultCompletionProvider());

            autoCompletion.install(new JTextArea());
        }
    }


    /**
     * Vytvoření hodnot do okna pro dokončování / doplňování.
     */
    private static void createValuesForCompletion(final DefaultCompletionProvider provider) {
        /*
         * Set, kam se budou vkládat veškeré příkazy, které se mají ve výsledku doplnit do okna pro automatické
         * dokončování / doplňování příkazů v editoru příkazů. Zde je takto
         * definován jako set, aby se nejprve přidaly veškeré hodnoty sem, aby se v tom okně nenacházely duplicitní
         * hodnoty.
         */
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        // Třídy z diagramu tříd (jejich .class soubory):
        final List<Class<?>> classesInClassDiagram = VALUE_GETTER.getClassesInClassDiagram();

        // Přidání tříd v diagramu tříd:
        addClasses(valueCompletionInfoSet, classesInClassDiagram);

        // Přidání konstruktorů tříd v diagramu tříd:
        addConstructors(valueCompletionInfoSet, classesInClassDiagram);


        // Instance v diagramu instancí:
        final Map<String, Object> instancesInInstanceDiagram = VALUE_GETTER.getInstancesInInstanceDiagram();
        addInstanceReferences(valueCompletionInfoSet, instancesInInstanceDiagram);

        // Veřejné a chráněné proměnné v instancích v diagramu instancí:
        addPublicProtectedFieldsInInstances(valueCompletionInfoSet, instancesInInstanceDiagram);


        // Veřejné a chráněné nestatické metody v instancích v diagramu instancí:
        addMethodsInInstances(valueCompletionInfoSet, instancesInInstanceDiagram);

        // Veřejné a chráněné statické metody v třídách v diagramu tříd:
        addStaticMethodsInClasses(valueCompletionInfoSet, classesInClassDiagram);

        // Veřejné a chráněné statické metody ve vnitřních veřejných a chráněných statických třídách v třídách v
        // diagramu tříd:
        addStaticMethodsInStaticInnerClasses(valueCompletionInfoSet, classesInClassDiagram);


        // "Klasické" proměnné vytvořené uživatelem v editoru příkazů (nejedná se o datové struktury):
        final Map<String, Variable> classicVariablesInCommandEditor = VALUE_GETTER
                .getClassicVariablesInCommandEditor();
        addVariablesInCommandEditor(valueCompletionInfoSet, classicVariablesInCommandEditor);


        // Jednorozměrné pole vytvořené uživatelem v editoru příkazů:
        final Map<String, VariableArray<?>> arrayVariablesInCommandEditorList = VALUE_GETTER
                .getArrayVariablesInCommandEditor();
        addArrayVariablesInCommandEditor(valueCompletionInfoSet, arrayVariablesInCommandEditorList);


        // Proměnné typu List, vytvořené uživatelem v editoru příkazů:
        final Map<String, VariableList<?>> listVariablesInCommandEditor = VALUE_GETTER
                .getListVariablesInCommandEditor();
        addListVariablesInCommandEditor(valueCompletionInfoSet, listVariablesInCommandEditor);


        // Ukázky deklarace proměnných:
        if (Editor.isShowExamplesOfSyntax())
            addExamplesOfDeclaration(valueCompletionInfoSet, classesInClassDiagram, instancesInInstanceDiagram);


        // Výchozí metody definované v editoru příkazů (pro výpis příkazů, restart, ukončení, výpis proměnných, ...):
        if (Editor.isShowDefaultMethods())
            addDefaultMethodsInEditor(valueCompletionInfoSet);

        addValuesToAutoCompletionProvider(provider, valueCompletionInfoSet);
    }


    /**
     * Přidání možností pro doplnění tříd do okna pro doplnění kódu do editoru.
     *
     * @param valueCompletionInfoSet
     *         - kolekce, kam se vkládají hodnoty, které se mají po převední do požadované podoby zobrazit v okně pro
     *         doplnění kódu.
     * @param classesInClassDiagram
     *         - list, který obsahuje načtené třídy z diagramu tříd, ze kterých se mají načíst syntaxe pro doplnění
     *         tříd.
     */
    private static void addClasses(final Set<ValueCompletionInfo> valueCompletionInfoSet, final List<Class<?>>
            classesInClassDiagram) {
        final Set<ValueCompletionInfo> classesSet = VALUE_PREPARER.getClasses(classesInClassDiagram);
        valueCompletionInfoSet.addAll(classesSet);

        final Set<ValueCompletionInfo> classesWithPackagesSet = VALUE_PREPARER.getClassesWithPackages
                (classesInClassDiagram);

        valueCompletionInfoSet.addAll(classesWithPackagesSet);
    }


    /**
     * Přidání možností pro doplnění konstruktorů do okna pro doplnění kódu / hodnot do editoru.
     *
     * @param valueCompletionInfoSet
     *         - kolekce, kam se vkládají hodnoty, které se mají po převední do požadované podoby zobrazit v okně pro
     *         doplnění kódu.
     * @param classesInClassDiagram
     *         - list, který obsahuje načtené třídy z diagramu tříd, ze kterých se mají načíst syntaxe pro doplnění
     *         tříd.
     */
    private static void addConstructors(final Set<ValueCompletionInfo> valueCompletionInfoSet, final List<Class<?>>
            classesInClassDiagram) {
        final Set<ValueCompletionInfo> constructorsSet = VALUE_PREPARER.getConstructors(classesInClassDiagram);
        valueCompletionInfoSet.addAll(constructorsSet);

        final Set<ValueCompletionInfo> completeConstructorSet = VALUE_PREPARER.getCompleteConstructors
                (classesInClassDiagram);

        valueCompletionInfoSet.addAll(completeConstructorSet);
    }


    /**
     * Přidání možností pro doplnění referenci na existující instance tříd z diagramu tříd do okna pro doplnění kódu /
     * hodnot do editoru.
     *
     * @param valueCompletionInfoSet
     *         - kolekce, kam se vkládají hodnoty, které se mají po převední do požadované podoby zobrazit v okně pro
     *         doplnění kódu.
     * @param instanceReferences
     *         - mapa obsahující uživatelem vytvořené instance tříd z diagramu tříd. Instance se nacházejí v diagramu
     *         instancí.
     */
    private static void addInstanceReferences(final Set<ValueCompletionInfo> valueCompletionInfoSet, final Map<String
            , Object> instanceReferences) {
        final Set<ValueCompletionInfo> instanceReferenceSet = VALUE_PREPARER.getInstanceReferences(instanceReferences);

        valueCompletionInfoSet.addAll(instanceReferenceSet);
    }


    /**
     * Přidání možností pro doplnění referencí na uživatelem vytvořené "klasické" proměnné v editoru příkazů.
     * <p>
     * Budou zde možnost pro vložení reference / názvu proměnné a v případě číselných datových typů proměnných budou
     * navíc doplněny možnosti pro inkrementaci a dekrementaci.
     *
     * @param valueCompletionInfoSet
     *         - kolekce, kam se vkládají hodnoty, které se mají po převední do požadované podoby zobrazit v okně pro
     *         doplnění kódu.
     * @param variables
     *         - mapa obsahující uživatelem vytvořené "klasické" proměnné v editoru příkazů. Nejedná se o proměnné typu
     *         datové struktury apod.
     */
    private static void addVariablesInCommandEditor(final Set<ValueCompletionInfo> valueCompletionInfoSet,
                                                    final Map<String, Variable> variables) {
        final Set<ValueCompletionInfo> commandEditorVariableSet = VALUE_PREPARER.getCommandEditorVariables(variables);

        valueCompletionInfoSet.addAll(commandEditorVariableSet);
    }


    /**
     * Přidání možností pro doplnění referencí na uživatelem vytvořené proměnné typu jednorozměrné pole v editoru
     * příkazů.
     * <p>
     * Budou zde možnosti pro vložení reference / názvu na pole (proměnou), syntaxe pro získání / naplnění hodnoty v
     * pole a v případě, že sejedná o pole číselného datového typu zde budou i k dispozici syntaxe pro inkrementaci a
     * dekrementaci v postfixové a prefixové formě.
     *
     * @param valueCompletionInfoSet
     *         - kolekce, kam se vkládají hodnoty, které se mají po převední do požadované podoby zobrazit v okně pro
     *         doplnění kódu.
     * @param variables
     *         - mapa obsahující uživatelem vytvořené proměné typu jednorozměrné pole v editoru příkazů.
     */
    private static void addArrayVariablesInCommandEditor(final Set<ValueCompletionInfo> valueCompletionInfoSet,
                                                         final Map<String, VariableArray<?>> variables) {
        final Set<ValueCompletionInfo> commandEditorArrayVariableSet =
                VALUE_PREPARER.getCommandEditorArrayVariables(variables);

        valueCompletionInfoSet.addAll(commandEditorArrayVariableSet);
    }


    /**
     * Přidání možností pro doplnění referencí na uživatelem vytvořené proměnné typu List v editoru příkazů.
     * <p>
     * Budou zde přidány možnosti pro doplnění reference na list a veškeré podporované metody v editoru příkazů (perace
     * nad listem).
     *
     * @param valueCompletionInfoSet
     *         - kolekce, kam se vkládají hodnoty, které se mají po převední do požadované podoby zobrazit v okně pro
     *         doplnění kódu.
     * @param variables
     *         - mapa obsahující uživatelem vytvořené proměnné typu List v editoru příkazů.
     */
    private static void addListVariablesInCommandEditor(final Set<ValueCompletionInfo> valueCompletionInfoSet,
                                                        final Map<String, VariableList<?>> variables) {
        final Set<ValueCompletionInfo> commandEditorListVariableSet =
                VALUE_PREPARER.getCommandEditorListVariables(variables);

        valueCompletionInfoSet.addAll(commandEditorListVariableSet);
    }


    /**
     * Přidání možností pro doplnění syntaxí pro zavolání nestatických veřejných a chráněných metod, které se nachází v
     * instancích nacházející se v diagramu instancí.
     *
     * @param valueCompletionInfoSet
     *         - kolekce, kam se vkládají hodnoty, které se mají po převední do požadované podoby zobrazit v okně pro
     *         doplnění kódu.
     * @param instances
     *         - instance, které uživatel vytvořil z tříd nacházející se v diagramu tříd. Mapa obshuje jako klíč zadané
     *         reference na instance a jako hodnota jsou vždy reference na konkrétní instanci.
     */
    private static void addMethodsInInstances(final Set<ValueCompletionInfo> valueCompletionInfoSet, final Map<String
            , Object> instances) {
        final Set<ValueCompletionInfo> methodsInInstanceSet = VALUE_PREPARER.getMethodsInInstances(instances);

        valueCompletionInfoSet.addAll(methodsInInstanceSet);
    }


    /**
     * Přidání možností pro doplnění syntaxí pro zavolání statických veřejných a chráněných metod, které se nachází v
     * třídách v diagramu tříd.
     *
     * @param valueCompletionInfoSet
     *         - kolekce, kam se vkládají hodnoty, které se mají po převední do požadované podoby zobrazit v okně pro
     *         doplnění kódu.
     * @param classList
     *         - List obsahující veškeré načtené třídy z diagramu tříd (soubory .class).
     */
    private static void addStaticMethodsInClasses(final Set<ValueCompletionInfo> valueCompletionInfoSet,
                                                  final List<Class<?>> classList) {
        final Set<ValueCompletionInfo> staticMethodsInClassesSet = VALUE_PREPARER.getStaticMethodsInClasses(classList);

        valueCompletionInfoSet.addAll(staticMethodsInClassesSet);
    }


    /**
     * Přidání možností pro doplnění syntaxí pro zavolání statických veřejných a chráněných metod, které se nachází ve
     * vnitřních třídách v třídách v diagramu tříd.
     *
     * @param valueCompletionInfoSet
     *         - kolekce, kam se vkládají hodnoty, které se mají po převední do požadované podoby zobrazit v okně pro
     *         doplnění kódu.
     * @param classList
     *         - List obsahující veškeré načtené třídy z diagramu tříd (soubory .class).
     */
    private static void addStaticMethodsInStaticInnerClasses(final Set<ValueCompletionInfo> valueCompletionInfoSet,
                                                             final List<Class<?>> classList) {
        final Set<ValueCompletionInfo> staticMethodsInStaticInnerClassesSet =
                VALUE_PREPARER.getStaticMethodsInStaticInnerClasses(classList);

        valueCompletionInfoSet.addAll(staticMethodsInStaticInnerClassesSet);
    }


    /**
     * Přidání možností pro doplnění syntaxí s ukázkami deklarace vybraných / povolených proměnných v editoru příkazů
     * (proměnné, List, jednorozměrné pole).
     *
     * @param valueCompletionInfoSet
     *         - kolekce, kam se vkládají hodnoty, které se mají po převední do požadované podoby zobrazit v okně pro
     *         doplnění kódu.
     * @param classList
     *         - List obsahující veškeré načtené třídy z diagramu tříd (soubory .class).
     * @param instancesInInstanceDiagram
     *         - mapa obsahující veškeré instance, které se nachází v diagramu instancí.
     */
    private static void addExamplesOfDeclaration(final Set<ValueCompletionInfo> valueCompletionInfoSet,
                                                 final List<Class<?>> classList,
                                                 final Map<String, Object> instancesInInstanceDiagram) {
        final Set<ValueCompletionInfo> examplesDeclaration = VALUE_PREPARER.getExamplesDeclaration(classList,
                instancesInInstanceDiagram);

        valueCompletionInfoSet.addAll(examplesDeclaration);
    }


    /**
     * Přidání výchozích metod, které jsou definované v editoru příkazů. Jedná se například o metody pro restart a
     * ukončení aplikace, pro výpis hodnot do editoru výstupů, výpis příkazů pro editoru příkazů (nápověda) apod.
     *
     * @param valueCompletionInfoSet
     *         - kolekce, kam se vkládají hodnoty, které se mají po převední do požadované podoby zobrazit v okně pro
     *         doplnění kódu.
     */
    private static void addDefaultMethodsInEditor(final Set<ValueCompletionInfo> valueCompletionInfoSet) {
        final Set<ValueCompletionInfo> defaultMethodsInEditor = VALUE_PREPARER.getDefaultMethodsInEditor();

        valueCompletionInfoSet.addAll(defaultMethodsInEditor);
    }


    /**
     * Přidání možností pro doplnění veřejných a chráněných proměnných, které se nachází v instancích v diagramu
     * instancí..
     *
     * @param valueCompletionInfoSet
     *         - kolekce, kam se vkládají hodnoty, které se mají po převední do požadované podoby zobrazit v okně pro
     *         doplnění kódu.
     * @param instancesInInstanceDiagram
     *         - mapa obsahující veškeré instance, které se nachází v diagramu instancí.
     */
    private static void addPublicProtectedFieldsInInstances(final Set<ValueCompletionInfo> valueCompletionInfoSet,
                                                            final Map<String, Object> instancesInInstanceDiagram) {
        final Set<ValueCompletionInfo> instancesInInstanceDiagramSet =
                VALUE_PREPARER.getVariablesFromInstances(instancesInInstanceDiagram);

        valueCompletionInfoSet.addAll(instancesInInstanceDiagramSet);
    }


    /**
     * Přidání položek / hodnot, které se nachází v listu valuesCompletionInfoList do provider - do okna, aby se
     * nabízeli pro doplnění do editoru.
     *
     * @param provider
     *         - objekt, kam se vkládají hodnoty, které se mají po převední do požadované podoby zobrazit v okně pro
     *         doplnění kódu.
     * @param valueCompletionInfoSet
     *         kolekce s hodnotami, které se mají přidat do provideru.
     */
    private static void addValuesToAutoCompletionProvider(final DefaultCompletionProvider provider, final
    Set<ValueCompletionInfo> valueCompletionInfoSet) {
        valueCompletionInfoSet.forEach(v -> {
            if (v.getSummary() != null)
                provider.addCompletion(new ShorthandCompletion(provider, v.getInputText(), v.getReplacementText(), v
                        .getShortDesc(), v.getSummary()));

            else
                provider.addCompletion(new ShorthandCompletion(provider, v.getInputText(), v.getReplacementText(), v
                        .getShortDesc()));
        });
    }
}