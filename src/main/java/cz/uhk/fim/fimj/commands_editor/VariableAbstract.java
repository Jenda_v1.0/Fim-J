package cz.uhk.fim.fimj.commands_editor;

/**
 * Tato abstraktní třída slouží pro pro deklaraci proměnných, které jsou "společné" pro další třídy, které reprezentují
 * nějakou proměnnou, kterou je možné vytvořit v editoru příkazů.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class VariableAbstract {

    /**
     * Proměnná, pro uchování logické hodnoty, která bude udávat, zda je proměnná objvalue final nebo ne.
     */
    protected final boolean isFinal;


    /**
     * Konstruktor této třídy.
     *
     * @param isFinal
     *         - logická proměnná o tom, zda má být tato "proměnná" final nenbo ne.
     */
    public VariableAbstract(final boolean isFinal) {
        this.isFinal = isFinal;
    }


    /**
     * Metoda, která zjistí, zda lze hodnotu proměnné editoru naplnit či nikoliv. Neboli, zda je promnná final nebo ne
     *
     * @return true, pokud je proměnné final, jinal false
     */
    public final boolean isFinal() {
        return isFinal;
    }
}
