package cz.uhk.fim.fimj.commands_editor.values_completion_window;

import cz.uhk.fim.fimj.commands_editor.Variable;
import cz.uhk.fim.fimj.commands_editor.VariableArray;
import cz.uhk.fim.fimj.commands_editor.VariableList;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Rozhraní obsahující metody pro přípravu hodnot do okna pro doplňování hodnot.
 * <p>
 * Jsou zde hlavičky metod, které přímo přípraví konkrétní hodnoty pro vložení do okna pro automatické doplňování
 * hodnot.
 * <p>
 * (Note: Toto rozhraní má pouze jednu implementaci a není nezbytné. Pouze za účelem budoucího potenciálního
 * znovupoužití. Metody by ale mohly být například zde, nebo v jiné třídě statické nebo nestatické s běžnou implementaci
 * v nějaké třídě.)
 * <p>
 * <p>
 * Poznámky k syntaxi uvedené v javaDoc:
 * <p>
 * Syntaxe v okně pro doplnění kódu: text_1 - text_2 -> text_3
 * <p>
 * text_1 = text před pomlčkou, dle které se filtrují hoodnoty při psaní.
 * <p>
 * text_2 = definovaná hodnota za pomlčkou (informace, například datový typ proměnné nebo návratová hodnota metody)
 * <p>
 * text_3 = hodnota, která je napsána v "druhém" okně, které se otevře ke každé zvolené hodnota v okně pro automatické
 * dokončení kódu. Zde senachází nějaké informace pro uživatele, například popis metody apod.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 28.07.2018 1:49
 */

interface ValuePreparer {

    /**
     * Získání hodnot pro doplnění názvů tříd.
     * <p>
     * Výsledná syntaxe: ClassName - packages.ClassName
     * <p>
     * Vkládaná syntaxe: ClassName
     *
     * @param classList
     *         - List načtených tříd z diagramu tříd, ze kterých se má vytvořit výše uvedená syntaxe pro doplnění.
     *
     * @return kolekcí, která bude obsahovat hodnoty pro sestavení výše uvedené syntaxe do okna pro automatické
     * dokončení příkazů v editoru příkazů.
     */
    Set<ValueCompletionInfo> getClasses(final List<Class<?>> classList);

    /**
     * Získání hodnot pro doplnění názvů tříd i s balíčky.
     * <p>
     * Výsledná syntaxe: packages.ClassName
     *
     * @param classList
     *         - List načtených tříd z diagramu tříd, ze kterých se má vytvořt výše uvedená syntaxe pro doplnění.
     *
     * @return kolekcí, která bude obsahovat hodnoty pro sestavení výše uvedené syntaxe do okna pro automatické
     * dokončení příkazů.
     */
    Set<ValueCompletionInfo> getClassesWithPackages(final List<Class<?>> classList);


    /**
     * Získání syntaxe pro zavolání konstruktorů.
     * <p>
     * Výsledná syntaxe: new ClassName() - package.ClassName -> vkládaný text
     * <p>
     * Výsledná syntaxe: new ClassName(dataType, anotherDataType, ...) - package.ClassName -> vkládaná syntaxe
     *
     * <p>
     * Výsledná syntaxe: new packages.ClassName()
     * <p>
     * Výsledná syntaxe: new packages.ClassName(dataType, anotherDataType, ...)
     *
     * @param classList
     *         - List načtených tříd z diagramu tříd, ze kterých se má vytvoři výše uvedená syntaxe pro doplnění.
     *
     * @return kolekcí, která bude obsahovat hodnoty pro sestavení výše uvedené syntaxe do okna pro automatické
     * dokončení příkazů.
     */
    Set<ValueCompletionInfo> getConstructors(final List<Class<?>> classList);

    /**
     * Získání syntaxe pro deklaraci a inicializaci konstruktorů
     * <p>
     * Výsledná syntaxe: packages.ClassName() - Declaration with initialization -> = vkládaná syntaxe
     * <p>
     * Výsledná syntaxe: packages.ClassName(dataType, anotherDataType, ...) - Declaration with initialization ->
     * vkládaná syntaxe
     *
     * <p>
     * Vkládaná syntaxe: packages.ClassName className1 = new packages.ClassName();
     * <p>
     * Vkládaná syntaxe: packages.ClassName className1 = new packages.ClassName(dataType, anotherDataType, ...);
     *
     * @param classList
     *         - List načtených tříd z diagramu tříd, ze kterých se má vytvoři výše uvedená syntaxe pro doplnění.
     *
     * @return kolekcí, která bude obsahovat hodnoty pro sestavení výše uvedené syntaxe do okna pro automatické
     * dokončení příkazů.
     */
    Set<ValueCompletionInfo> getCompleteConstructors(final List<Class<?>> classList);


    /**
     * Získání syntaxe pro reference na existující instance v diagramu instancí (instance tříd z diagramu tříd).
     * <p>
     * Výsledná syntaxe: referenceName - packages.ClassName -> vkládaná syntaxe
     * <p>
     * Vkládaná syntaxe: referenceName
     *
     * @param instanceReferences
     *         - mapa, která obsahuje veškeré uživatelem vytvořené instance tříd z diagramu tříd. Klíč je zvolená
     *         reference na instanci, hodnota je konkrétní instance.
     *
     * @return kolekcí, která bude obsahovat hodnoty pro sestavení výše uvedené syntaxe do okna pro automatické
     * dokončení příkazů.
     */
    Set<ValueCompletionInfo> getInstanceReferences(final Map<String, Object> instanceReferences);


    /**
     * Získání syntaxe pro názvy proměnných / reference na existující proměnné, které uživatel vytvořil pomocí editoru
     * příkazů. Jedná se pouze o "klasické" proměnné, nejedná se o datové struktury - pole, List apod.
     * <p>
     * Výsledná syntaxe: variableName - dataType -> value
     * <p>
     * Vkládaná syntaxe: variableName
     * <p>
     * <p>
     * Dále v případě, že se jedná o číselné proměnné se vytvoří i syntaxe pro inkrementaci a dekrementaci, tedy:
     * <p>
     * Výsledná syntaxe: (++variableName / variableName++) - dataType -> value
     * <p>
     * Vkládaná syntaxe (++variableName / variableName++)
     * <p>
     * (podobně pro dekrementaci)
     *
     * @param variables
     *         - mapa, která obsahuje veškeré uživatelem vytvořené "klasické "proměnné v editoru příkazů.
     *
     * @return kolekcí, která bude obsahovat hodnoty pro sestavení výše uvedené syntaxe do okna pro automatické
     * dokončení příkazů.
     */
    Set<ValueCompletionInfo> getCommandEditorVariables(final Map<String, Variable> variables);


    /**
     * Získání syntaxe pro reference na uživatelem vytvořené pole pomocí editoru příkazů.
     * <p>
     * Výsledná syntaxe: variableName - dataType -> value
     * <p>
     * Vkládaná syntaxe: variableName
     * <p>
     * <p>
     * Dále se bude řešit získání syntaxe pro získání / nastavení hodnoty:
     * <p>
     * Výsledná syntaxe: variableName[0] - dataType -> value
     * <p>
     * Vkládaná syntaxe: variableName[0]
     * <p>
     * <p>
     * V případě, že je pole číselného datového typu, doplní se i hodnoty pro inkrementaci a dekrementaci v prefixové a
     * postfixové formě.
     * <p>
     * Výsledná syntaxe: (++variableName[0] / variableName[0]++) - dataType -> value
     * <p>
     * Vkládaná syntaxe: (++variableName[0] / variableName[0]++)
     * <p>
     * (Podobně pro dekrementaci)
     *
     * @param arrayVariables
     *         - mapa, která obsahuje veškeré uživatelem vytvořené pole v editoru příkazů.
     *
     * @return kolekcí, která bude obsahovat hodnoty pro sestavení výše uvedené syntaxe do okna pro automatické
     * dokončení příkazů.
     */
    Set<ValueCompletionInfo> getCommandEditorArrayVariables(final Map<String, VariableArray<?>> arrayVariables);


    /**
     * Získání syntaxe pro reference na uživatelem vytvořené listy pomocí editoru příkazů. Dále pro metody, které je nad
     * příslušnými listy možné zavolat a jsou podporovány editorem příkazů.
     * <p>
     * Výsledná syntaxe: variableNme - dataType -> value
     * <p>
     * Vkládaná syntaxe: variableName
     * <p>
     * <p>
     * Pro podporované metody nad listem v editoru příkazů:
     * <p>
     * Výsledná syntaxe: variableName.add(value) - void -> value
     * <p>
     * Vkládaná syntaxe: variableName.add(value)
     * <p>
     * (podobně pro ostatní podporované metody nad listem v editoru příkazů)
     *
     * @param listVariables
     *         - mapa, která obsahuje veškeré uživatelem vytvořené Listy v editoru příkazů.
     *
     * @return kolekcí, která bude obsahovat hodnoty pro sestavení výše uvedené syntaxe do okna pro automatické
     * dokončení příkazů.
     */
    Set<ValueCompletionInfo> getCommandEditorListVariables(final Map<String, VariableList<?>> listVariables);


    /**
     * Získání syntaxe pro veřejné a chráněné nestatické metody, které se nachází v instancích v diagramu instancí.
     * <p>
     * Výsledná syntaxe: referenceName.methodName() - returnDataType -> referenceName (packages.ClassName) (info, z jaké
     * třídy ja ta instance)
     * <p>
     * Výsledná syntaxe: referenceName.methodName(dataType, anotherDataType, ...) - returnDataType -> referenceName
     * (packages.ClassName) (info, z jaké třídy ja ta instance)
     * <p>
     * <p>
     * Vkládaná syntaxe: referenceName.methodName()
     * <p>
     * Vkládaná syntaxe: referenceName.methodName(dataType, anotherDataType, ...)
     * <p>
     * (Zde by bylo možné nalezené metody nabízet pouze jako metody bez reference, ale metodu v nějaké instanci třídy
     * nelze zavolat jinde než nad instancí, protože bude reference na instanci vždy součástí syntaxe pro zavolání
     * metody.)
     * <p>
     * <p>
     * Dále se budou zobrazovat i samotné metody bez refrence na instanci, kde se nachází, ale vždy pouze jednou, tedy
     * každá metoda v tom okně bude zobrazeny pouze jednou, tedy nejprve v syntaxi referenceName.methodName, poté pouze
     * methoName. Jde o to, že v tom okně nefunguje filtrování hodnota za desetinnou tečkou, proto by se syntaxe
     * ref.method v části, kdy je již napsáno ref.m, už nevyhledala ta metoda.
     *
     * @param instancesInInstanceDiagram
     *         - mapa obsahující instance (reference na instance), které se nachází v diagramu instancí. Jedná se o
     *         instance, které užvatel vytvořil zavoláním konstruktorů v nějakých třídách v diagramu tříd.
     *
     * @return kolekcí, která bude obsahovat hodnoty pro sestavení výše uvedené syntaxe do okna pro automatické
     * dokončení příkazů.
     */
    Set<ValueCompletionInfo> getMethodsInInstances(final Map<String, Object> instancesInInstanceDiagram);


    /**
     * Získání syntaxe pro veřejné a chráněné statické metody, které se nachází v třídách v diagramu tříd.
     * <p>
     * Výsledná syntaxe: packages.ClassName.methodName() - returnDataType -> referenceName (packages.ClassName) (info, z
     * jaké třídy ja ta instance)
     * <p>
     * Výsledná syntaxe: packages.ClassName.methodName(dataType, anotherDataType, ...) - returnDataType -> referenceName
     * (packages.ClassName) (info, z jaké třídy ja ta instance)
     * <p>
     * <p>
     * Vkládaná syntaxe: packages.ClassName.methodName()
     * <p>
     * Vkládaná syntaxe: packages.ClassName.methodName(dataType, anotherDataType, ...)
     *
     * @param classList
     *         - List, který obsahuje načtené třídy z diagramu tříd (jejich .class soubory).
     *
     * @return kolekcí, která bude obsahovat hodnoty pro sestavení výše uvedené syntaxe do okna pro automatické
     * dokončení příkazů.
     */
    Set<ValueCompletionInfo> getStaticMethodsInClasses(final List<Class<?>> classList);


    /**
     * Získání syntaxe pro veřejné a chráněné statické metody, které se nachází ve veřejných a statických vnitřních
     * třídách v diagramu tříd.
     * <p>
     * Výsledná syntaxe: packages.ClassName.InnerClassName.methodName() - returnDataType -> referenceName
     * (packages.ClassName) (info, z jaké třídy ja ta instance)
     * <p>
     * Výsledná syntaxe: packages.ClassName.InnerClassName.methodName(dataType, anotherDataType, ...) - returnDataType
     * -> referenceName (packages.ClassName) (info, z jaké třídy ja ta instance)
     * <p>
     * <p>
     * Vkládaná syntaxe: packages.ClassName.InnerClassName.methodName()
     * <p>
     * Vkládaná syntaxe: packages.ClassName.InnerClassName.methodName(dataType, anotherDataType, ...)
     * <p>
     * <p>
     * Podobně pro metody ve vnitřní třídě nacházející se v jiné vnitřní třídě:
     * <p>
     * Výsledná syntaxe: packages.ClassName.InnerClassName.InnerInnerClassName.methodName() - returnDataType
     * <p>
     * <p>
     * Vkládaná syntaxe: packages.ClassName.InnerClassName.InnerInnerClassName.methodName()
     *
     * @param classList
     *         - List, který obsahuje načtené třídy z diagramu tříd (jejich .class soubory).
     *
     * @return kolekcí, která bude obsahovat hodnoty pro sestavení výše uvedené syntaxe do okna pro automatické
     * dokončení příkazů.
     */
    Set<ValueCompletionInfo> getStaticMethodsInStaticInnerClasses(final List<Class<?>> classList);


    /**
     * Získání syntaxe pro veřejné a chráněné proměnné, které se nachází v instancích v diagramu instancí. Jedná se o
     * získání pouze veřejných a chráněných proměnných, nehledě na to, zda jsou statické či final apod.
     * <p>
     * Výsledná syntaxe: referenceName.variableName - dataType -> value
     * <p>
     * Vkládaná syntaxe: referenceName.variableName
     * <p>
     * <p>
     * Dále se řeší případy, kdy se jedná o proměnné číselného datového typu, nebo pole také číselného datového typu.
     * <p>
     * V obou případech bude doplněna syntaxe pro inkrementaci a dekremetnaci příslušné hodnoty v postfixové a prefixové
     * formě.
     * <p>
     * <p>
     * Pro proměnné:
     * <p>
     * Výsledná syntaxe: (++referenceName.variableName / referenceName.variableName++) - dataType -> value (podobně pro
     * dekrementaci)
     * <p>
     * Vkládaná syntaxe: (++referenceName.variableName / referenceName.variableName++)
     * <p>
     * <p>
     * Pro pole:
     * <p>
     * Výsledná syntaxe: referenceName.variableName - dataType -> value
     * <p>
     * Výsledná syntaxe: referenceName.variableName[0] - dataType -> value
     * <p>
     * Výsledná syntaxe: (++referenceName.variableName[0] / referenceName.variableName[0]++) - dataType -> value
     * (podobně pro dekrementaci)
     * <p>
     * Výsledná syntaxe: (++referenceName.variableName[0] / referenceName.variableName[0]++)
     * <p>
     * <p>
     * Dále, za tečkou již okno pro dokončování / doplňování hodnot nenabízí vyhledávání hodnot s tečkou v "textu"
     * vyhledávané hodnoty. Proto se doplní ještě pouze proměnné - jejich názvy.
     * <p>
     * (Pouze název proměnné bez inkrementace či dekrementace apod. Protože to lze pouze nad proměnnou s referencí. I
     * když by zde byla možnost doplnit inkrementaci a dekrementaci v postfixové formě - to po přidání prměnné k
     * referenci lze, ale to zde již řešit nebudu, protože už by toho mohlo být "trochu moc" na jednu proměnnou.)
     * <p>
     * Výsledná syntaxe: variableName - dataType -> value (referenceName - název reference na instanci, kde se ta
     * proměnná nachází)
     *
     * @param instances
     *         - mapa obsahující instance (reference na instance), které se nachází v diagramu instancí. Jedná se o
     *         instance, které užvatel vytvořil zavoláním konstruktorů v nějakých třídách v diagramu tříd.
     *
     * @return kolekcí, která bude obsahovat hodnoty pro sestavení výše uvedené syntaxe do okna pro automatické
     * dokončení příkazů.
     */
    Set<ValueCompletionInfo> getVariablesFromInstances(final Map<String, Object> instances);


    /**
     * Získání syntaxe pro výchozí ukázky deklarace proměnných, Listů a polí v editoru příkazů.
     * <p>
     * Jedná se o "klasické" deklarace, například final int varName = value; pro list ty dva způsoby (prázdný nebo s
     * hodnotami). A pro ple ty 4 způsoby - prázdné s velikostí, s hodnotami (dvě verze) a nebo návratovou hodnotou z
     * metody.
     *
     * @param classList
     *         - kolekce tříd (souborů .class), které se nachází v diagramu tříd.
     * @param instancesInInstanceDiagram
     *         - mapa obsahující instance, které se nachází v diagramu instancí.
     *
     * @return připravené ukázky veškerých možností deklarace výše zmíněných hodnot v editoru příkazů.
     */
    Set<ValueCompletionInfo> getExamplesDeclaration(final List<Class<?>> classList,
                                                    final Map<String, Object> instancesInInstanceDiagram);


    /**
     * Získání syntaxe výchozích metod, které jsou definované pro editor příkazů. Jedná se například o metody pro výpis
     * hodnot do editoru výstupů, metoda pro zobrazení nápovedy pro editor příkazů. Metoda pro výpis proměnných, které
     * uživatel definoval v editoru příkazů apod.
     *
     * @return kolekci, která bude obsahovat syntaxe pro zavolání metod, které jsou definovýny v editoru příkazů.
     */
    Set<ValueCompletionInfo> getDefaultMethodsInEditor();
}
