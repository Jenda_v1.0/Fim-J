package cz.uhk.fim.fimj.commands_editor;

/**
 * Třída, která obsahuje signratury a hlavičky metod pro provedení operace dle rozpoznané syntexe pomocí regulárních
 * výrazů na příkaz z editoru příkazů
 * <p>
 * Například, pokud uživatel do editoru zadá, že chce provést operaci součet dvou čísel - syntaxe: X + Y to se dle
 * regulárních výrazů zjistí v třídě Editor a ta v této třídě zavolá metodu, pro příslušný výpočet, která vrátí výsledek
 * početní operace a vypíše tento výsledek do editoru příkazů
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface MakeOperationInterface {

	/**
	 * Metoda, která vrátí text, ve kterém budou sepsány všechny příkazy, které může
	 * editoru příkazů rozpoznat, např jaké lze zavolat konstuktory - informace k
	 * jejím parametrům, jak zavolat metodu, ...
	 * 
	 * @return výpis všech příkazů, které ovládá editor příkazů
	 */
	String writeCommands();
	
	
	
	
	/**
	 * Metoda, která spustí timer s odpočtem 3 vteřin, za které se ukončí aplikace.
	 * 
	 * @return Úspěch a odpočet těch tří vteřin nebo neúspěch, pokud je již nějaký
	 *         timer spuštěn - pro restart nebo ukončení aplikace..
	 */
	String closeAppByExitCommand();
	
	/**
	 * Metoda, která spustí timer s odpočtem 0 - 59 vteřin, za které se ukončí
	 * aplikace. Počet vteřin se vezme z parametru metody.
	 * 
	 * @return Úspěch a odpočet těch zadaných vteřin nebo neúspěch, pokud je již
	 *         nějaký timer spuštěn - pro restart nebo ukončení aplikace..
	 */
	String closeAppByExitWithCountCommand(final String command);

	/**
	 * Metoda, která spustí timer s odpočtem 3 vteřin, za které se ukončí aplikace.
	 * 
	 * @return Úspěch a odpočet těch 3 vteřin nebo neúspěch, pokud je již nějaký
	 *         timer spuštěn - pro restart nebo ukončení aplikace..
	 */
	String closeAppBySystemExitCommand();
	
	
	/**
	 * Metoda, která spustí timer s odpočtem 3 vteřin, za které se restartuje
	 * aplikace.
	 * 
	 * @return - Úspěch a odpočet těch 3 vteřin, nebo neúspěch, pokud je již nějaký
	 *         timer spuštěn - pro restart nebo ukončení aplikace.
	 */
	String restartAppByRestartCommand();

	/**
	 * Metoda, která spustí timer s odpočtem 0 - 59 vteřin, za které se restartuje aplikace. Počet zadaných vteřin se
	 * vezme z parametru zadané metody.
	 *
	 * @param command
	 *         - uživatelem zadaný příkaz v editoru příkazů, ze které se získá odpočet - přirození číslo v intervalu <0
	 *         ; 59>
	 * @return - Úspěch a odpočet těch 3 vteřin, nebo neúspěch, pokud je již nějaký timer spuštěn - pro restart nebo
	 * ukončení aplikace.
	 */
	String restartAppWithCountByRestartCommand(final String command);
	
	/**
	 * Metoda, která zjistí, zda nějaký timer je spuštěn, tj. Zda je spuště timer
	 * pro restart nebo ukončení aplikace. A pokud ano, pak jej zastaví a vypíše
	 * hlášené ohledně toho, zda byl zastavenjeden z těch odpočtů, případně že žádný
	 * odpočet nebyl spusštěň.
	 * 
	 * @return Buď, že žádný odpočet nebyl spuštěn, nebo že byl zastaven.
	 */
	String cancelCountdown();
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro vypsání do editoru výstupů všech proměnných, které
	 * byly vytvořeny v editoru příkazů jako tzv. dočasné proměnné.
	 * 
	 * @return Buď se vypíšou všechny vytvořené proměnné, nebo že žádné nebyly
	 *         vytvořeny.
	 */
	String printTempVariables();
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zkusí vypočítat matematický výraz v textu - command (pokud se
	 * tedy jedná o matematický výraz).
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch s výsledek matematického výrazu nebo Neúspěch s nějakým
	 *         hlášením o chybě.
	 */
	String computeMathematicalExpression(final String command);
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která získá hodnotu z nějaké proměnné, která se nachází v instanci
	 * třídy z diagramu tříd, která se nachází v diagramu instnací.
	 * 
	 * Syntaxe: (-) referenceName.variableName;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return Úspěch a hodnotu příslušné proměnné jinak Neúspěch a případně nějaké
	 *         hlášení o nastalé chybě.
	 */
	String makeGetValueFromVarInInstance(final String command);
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která se pokusí zavolat konstruktor příslušné třídy.
	 * 
	 * Příkaz (command) by měl obsahovat syntaxi:
	 * 
	 * new packageName. (anotherPackageName.) ClassName(parameters); (-> parameters
	 * jsou volitelné)
	 * 
	 * dle uvedené syntaxe výše se zjistí o jakou se jedná třídu a zda konstruktor
	 * obsahujep arametry, dle toho se pokusí příslušný konstruktor zavolat.
	 * 
	 * Pokud se úspěšně zavolá příslušný konstruktor, pak se vytvoření instance
	 * nikam neukládá pro pozdější použití apod. Toto je pouze zavolání
	 * konstruktoru, nic víc, nic míň.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se povede zavolat konstruktor, viz výše), jinak
	 *         Neúspěch a případně nějaká chybová hláška k tomu ohledně chyby, ke
	 *         které došlo, například, že se nepodařilo najít třídu, nebo
	 *         konstruktor s parametry apod.
	 */
	String callConstructor(final String command);
	
	
	
	
	
	
	
	/**
	 * Metoda, která si "rozebere" příkaz zadaný uživatelem do editoru příkazů, a z
	 * jednotlivých řástí si zjistí, instanci jaké třídy pod jakým názvem má
	 * vytvořit Neboli, jaký konstruktor dané třídy má zavolat (výchozí bez
	 * parametrů nebo nějaký s parametry)
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů pro vytvoření nové
	 *            instance
	 * 
	 * @return Succes - úspěch nebo Failure - neúspěch
	 */
	String makeInstance(final String command);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která se pokusí zavolat metodu nad instancí třídy z diagramu tříd
	 * nebo nad instancí třídy VariableList, protože refulární výrazy rozpoznají
	 * zavolání metody pro obě zmíněné instance.
	 * 
	 * Metoda zjistí, zda se jedná o instanci třídy v diagramu tříd nebo o instnaci
	 * třídy VariableList a dle toho zavolá zadanou metodu nad danou instancí.
	 * 
	 * Pokud nebude ani jedna instance nalezena, bude o tom vypsána chybová hláška
	 * do editoru výstupů
	 * 
	 * 
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz pro zjištění výše zmíněných údajů
	 * 
	 * @return Úspěch, pokud se metoda v pořádku zavolá, jinak Neúspěch
	 */
	String makeCallTheMethod(final String command);
	
	
	
	
	
	
	/**
	 * Metoda, která se pokusí zavolat zadanou statickou metodu v zadané třídě v
	 * diagramu tříd, ne v diagramu instancí.
	 * 
	 * Zkusí se načíst zkompilovaná (/ přeložená) třída dle zadaného názvu a
	 * balíčků, kde se nachází vzhledem k adresáři src, zjistí se, zda obsahuje
	 * příslušnou metodu a je statická a veřejná (jiné nejdou zavolat, chráněné
	 * metody mohou zavolat její potomci a privátní pouze třída sama), a případně se
	 * zjistí parametry, pokud budou podmínky splněny, výsledek se vypíše do editoru
	 * výstupů, jinak se vypíše chyba s informací, ke které došlo.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů, ze kterého se zjistí
	 *            potřebné informace, jako je třeba třída, metoda, parametry apod.
	 * 
	 * @return Úspěch v případě, že se metoda úspěšně vykoná, případně návratový
	 *         hodnota, jinak Neúspěch s případnou informací o chybě, ke ketré
	 *         došlo.
	 */
	String makeCallStaticMethod(final String command);
	
	
	
	
	
	
	
	/**
	 * Metoda, která si z příkazu z editoru zjisti, proměnnou třídy, do jejich
	 * proměnne ma vlozit jakou hodnotu a zda to vůbec jde a provede tuto operaci
	 * 
	 * - oerace pro naplnení proměnné instance třídy nějakou konstantou - číslem,
	 * znakem, textem
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return úspěch, pokud se proměnnou třídy podařilo naplnit, jinak Neúspěch
	 */
	String makeFullfillmentOfVariablesNumber(final String command);
	
	
	
	
	
	/**
	 * Metoda, která si z příkazu v parametru zjistí, promennou pro třídu, její
	 * promennou, do které má vložit zadanou hodnotu a tuto operace provede
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return spěch, pokud se provede operace pro naplnení proměnné zadanout
	 *         hodnou, jinak Neúspěch
	 */
	String makeFullfillmentOfVariableVariable(final String command);
	
	
	
	
	/**
	 * Metoda, která si z parametru zjistí název reference na instanci třídy, název
	 * proměnné do které má vložit hodnotu a název metody, kterou má zavolat, pro
	 * získání výsledku
	 * 
	 * Provede toto naplnění proměnné třídy vrácenou hodnotu z meotdy
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se operace provede Úspěšně, jinak Neúspěch
	 */
	String makeFullfillmentOfVariableGetMethod(final String command);
	
	
	
	
	
	/**
	 * Metoda, která naplní proměnnou nějaké třídy instancí nějaké třídy zavoláním
	 * konstruktoru.
	 * 
	 * Syntaxe:
	 * 
	 * instanceName.variableName = new packageName. (anotherPackageName.)
	 * ClassName(parameters); (-> parameters jsou volitlné)
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editour příkazů
	 * 
	 * @return Úspěch v případě úspěšného provedení operace, jinak Neúspěch s
	 *         popisem chyby, ke které došlo.
	 */
	String makeFulfillmentOfVariableByCallingConstructor(final String command);
	
	
	
	
	
	
	/**
	 * Metoda, deklaruje nějakou proměnnou v editoru příkazů.
	 * 
	 * Meotda si ze zadaného příkazu zjistí, proměnnou jakého typu má vytvořit a zda
	 * má být final ne ne
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se povede úspěšně deklarovat zadanou proměnnou, jinak
	 *         ne)spěch s příslušnou chybou
	 */
	String makeDeclarationEditorVariable(final String command);
	
	
	
	
	/**
	 * Metoda, která si z parametru zjistí název proměnné, do které má vložit
	 * hodnotu a proměnnou, ze které si má hodnotu vzít
	 * 
	 * Provede toto naplnění proměnné třídy vrácenou hodnotu z meotdy
	 * 
	 * @param command
	 *            - uživatelem zadanýpříkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se operace provede Úspěšně, jinak Neúspěch
	 */
	String makeFullfillmentOfEditorVariableGetMethod(final String command);
	
	
	/**
	 * Metoda, kterí si z příkazu zjist, název proměnné, vytvořené v editoru příkazů
	 * , do které má vložit hodnotu, a proměnnou, ze které má hodnotu vzít
	 * 
	 * a tuto operaci - naplnění prměnné provede
	 * 
	 * @param command
	 *            - uživatelem zadaý příkaz
	 * 
	 * @return Úspěch, pokud se operace provede, jinak Neúspěch
	 */
	String makeFullfillmentOfEditorVariableVariable(final String command);
	
	
	/**
	 * Metoda, která si z příkazu zjistí, název proměnné, vytvořené v editoru
	 * příkazů, a zkusí ji naplnit zadanou konstantou - textem, číslem, nebo znakem
	 * a zkusí tuto operaci provést
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se operace povede, jinak Neúspěch
	 */
	String makeFullfillmentOfEditorVariableConstant(final String command);
	
	
	
	
	
	
	
	/*
	 * NÁSLEDUJÍ:
	 * METODY PRO INKREMENTACE A DEKREMENTACI KONSTANTY - LITERÁLU, ZADANÉHO DO
	 * EDITORU PŘÍKAZŮ (vím, zbytečné, v Javě obecně nemožné, ... ale pouze v rámcí
	 * editoru příkazů pro možné ukázky)
	 */
	
	
	
	
	
	/**
	 * Metoda, která si ze zadaného textu v parametru comand zjistí, číslo, které má
	 * inkrementovat neboli, přisčíst k němu jedničku
	 * 
	 * Syntaxe: X++;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return inkrementované číslo, zadané uživatelem
	 */
	String makeIncrementValueBehind(final String command);
	
	
	/**
	 * Metoda, která si ze zadaného textu v parametru comand zjistí, číslo, které má
	 * dekrementovat neboli, odečíst od něj jedničku
	 * 
	 * Syntaxe: X--;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return dekrementované číslo, zadané uživatelem
	 */
	String makeDecrementValueBehind(final String command);
	
	
	
	/**
	 * Metoda, která si ze zadaného text v parametru comand zjistí, číslo, které má
	 * inkrementovat neboli, přisčíst k němu jedničku
	 * 
	 * Syntaxe: ++X;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return inkrementované číslo, zadané uživatelem
	 */
	String makeIncrementValueBefore(final String command);
	
	
	/**
	 * Metoda, která si ze zadaného text v parametru comand zjistí, číslo, které má
	 * dekrementovat neboli, odečíst od něj jedničku
	 * 
	 * Syntaxe: --X;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * @return inkrementované číslo, zadané uživatelem
	 */
	String makeDecrementValueBefore(final String command);
	
	
	
	
	
	
	/*
	 * Následují metody pro inkrementaci a dekrementaci proměnné v instanci tříy:
	 * 
	 * metody zadané do editoru příkazů
	 * 
	 * v sytanxi: (++/--) (-)referenceName.variableName (++/--);
	 */
	
	
	
	
	/**
	 * Metoda, která dostane jako parametr zadaný příkaz, zjistí si z něj instancí
	 * třídy, proměnnou - pokud k ní lze pristupovat a existuje a provede operaci
	 * inkrementaci (zvětšení o jedničku)
	 * 
	 * Syntaxe: ++referenceName.variableName;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkazv editoru příkazů
	 * 
	 * @return Úspech, pokud se operace provede úspěšně, jinak Neúspěch
	 */
	String makeIncrementClassVariableBefore(final String command);
	
	
	/**
	 * Metoda, která dostane jako parametr zadaný příkaz, zjistí si z něj instancí
	 * třídy, proměnnou - pokud k ní lze pristupovat a existuje a provede operaci
	 * dekrementaci (změnšení o jedničku)
	 * 
	 * Syntaxe: --referenceName.variableName;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkazv editoru příkazů
	 * 
	 * @return Úspech, pokud se operace provede úspěšně, jinak Neúspěch
	 */
	String makeDecrementClassVariableBefore(final String command);
	
	
	
	/**
	 * Metoda, která dostane jako parametr zadaný příkaz, zjistí si z něj instancí
	 * třídy, proměnnou - pokud k ní lze pristupovat a existuje a provede operaci
	 * inkrementaci (zvětšení o jedničku)
	 * 
	 * Syntaxe: referenceName.variableName++;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkazv editoru příkazů
	 * 
	 * @return Úspech, pokud se operace provede úspěšně, jinak Neúspěch
	 */
	String makeIncrementClassVariableBehind(final String command);
	
	
	
	
	/**
	 * Metoda, která dostane jako parametr zadaný příkaz, zjistí si z něj instancí
	 * třídy, proměnnou - pokud k ní lze pristupovat a existuje a provede operaci
	 * dekrementaci (zmenšení o jedničku)
	 * 
	 * Syntaxe: referenceName.variableName--;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkazv editoru příkazů
	 * 
	 * @return Úspech, pokud se operace provede úspěšně, jinak Neúspěch
	 */
	String makeDecrementClassVariableBehind(final String command);
	
	
	
	
	
	
	
	
	
	
	
	/*
	 * Metoda, pro inkrementaci a dekrementaci proměnné v editoru příkazů
	 * 
	 * Syntaxe: (++ / --) (-)editorVariableName(++ / --);
	 */
	
	
	/**
	 * Metoda, která si ze zadaného příkazu zjistí, proměnnou v editoru příkazů, na
	 * kterou ukazuje uživatelem zadaná reference, dáel pokud se jedná o číslo,
	 * které lze inkrementovat a provede příslušnou operaci - pokud to lze, jinak
	 * vypíše nějaké chybové hlášení, informující uživatele o vzniklé chybě
	 * 
	 * Syntaxe: (-) editorReferenceName++;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return chybovou hlášku - oznámení o chybě nebo Úspěch
	 */
	String makeIncrementCommandEditorVariableBehind(final String command);
	
	
	/**
	 * Metoda, která si ze zadaného příkazu zjistí, proměnnou v editoru příkazů, na
	 * kterou ukazuje uživatelem zadaná reference, dáel pokud se jedná o číslo,
	 * které lze inkrementovat a provede příslušnou operaci - pokud to lze, jinak
	 * vypíše nějaké chybové hlášení, informující uživatele o vzniklé chybě
	 * 
	 * Syntaxe: ++editorReferenceName;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return chybovou hlášku - oznámení o chybě nebo Úspěch
	 */
	String makeIncrementCommandEditorVariableBefore(final String command);
	
	
	/**
	 * Metoda, která si ze zadaného příkazu zjistí, proměnnou v editoru příkazů, na
	 * kterou ukazuje uživatelem zadaná reference, dáel pokud se jedná o číslo,
	 * které lze dekrementovat a provede příslušnou operaci - pokud to lze, jinak
	 * vypíše nějaké chybové hlášení, informující uživatele o vzniklé chybě
	 * 
	 * Syntaxe: (-) editorReferenceName--;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return chybovou hlášku - oznámení o chybě nebo Úspěch
	 */
	String makeDecrementCommandEditorVariableBehind(final String command);
	
	
	/**
	 * Metoda, která si ze zadaného příkazu zjistí, proměnnou v editoru příkazů, na
	 * kterou ukazuje uživatelem zadaná reference, dáel pokud se jedná o číslo,
	 * které lze dekrementovat a provede příslušnou operaci - pokud to lze, jinak
	 * vypíše nějaké chybové hlášení, informující uživatele o vzniklé chybě
	 * 
	 * Syntaxe: --editorReferenceName;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return chybovou hlášku - oznámení o chybě nebo Úspěch
	 */
	String makeDecrementCommandEditorVariableBefore(final String command);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	 * Následují metody pro naplnění proměnné, vytvořené v editoru příkazů pomocí aritmětických operací
	 * i s deklarací dané proměnné
	 */
	
	
	
	
	
	
	/**
	 * Metoda, která si "rozebere" uživatelem zadaný příkaz v parametru a zjistí
	 * proměnnou jakého typu má vytvořit, zda má být final a její hodnotu - jakou má
	 * danou proměnnou naplnit a provede tuto operaci
	 * 
	 * Může se jednat o naplnění proměnné textem, znakem, číslem, logickou hodnotou,
	 * apod.
	 * 
	 * Možné syntaxe: (final) dataType variableName = xxx;
	 * 
	 * (final) String variableName = "some text";
	 * 
	 * (final) char variableName = 'c';
	 * 
	 * (final) Integer variableName = 5;
	 * 
	 * (final) boolean variableName = true;
	 * 
	 * apod.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se vytvoření a naplnění dočasné proměnné v editoru
	 *         příkazů povede, jinak Neúspěch s informacemi o chybě
	 */
	String makeDeclarationAndFulfillmentOfEditorVariableConstant(final String command);
	
	
	
	
	/**
	 * Metoda, která si "rozebere" uživatelem zadaný příkaz v parametru a zjistí
	 * proměnnou jakého typu má vytvořit, zda má být final a její hodnotu - jakou má
	 * danou proměnnou naplnit a provede tuto operaci, naplnění lze provést hodnotou
	 * z jiné proměnné v editoru příkazů nebo z proměnné z instance třídy se
	 * stejnými datovými typy
	 * 
	 * Možné syntaxe: (final) dataType variableName = variable;
	 * 
	 * (final) dataType variableName = referencename.variableName;
	 *
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se vytvoření a naplnění dočasné proměnné v editoru
	 *         příkazů povede, jinak Neúspěch s informacemi o chybě
	 */
	String makeDeclarationAndFulfillmentOfEditorVariableVariable(final String command);
	
	
	
	
	
	
	/**
	 * Metoda, která si "rozebere" uživatelem zadaný příkaz v parametru a zjistí
	 * proměnnou jakého typu, zda má být final a její hodnotu - jakou má danou
	 * proměnnou naplnit a provede tuto operaci
	 * 
	 * Bude se jednat o naplnění proměnné návratovou hodnotou z zadané metody,
	 * metoda musí vracet hodnotu stejného datového typu, jako je prměnná
	 * 
	 * Možné syntaxe: (final) dataType variableName = referencename.methodName();
	 * 
	 * (final) dataType variableName = referencename.methodName(p1, p2, ...);
	 * 
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se vytvoření a naplnění dočasné proměnné v editoru
	 *         příkazů povede, jinak Neúspěch s informacemi o chybě
	 */
	String makeDeclarationAndFulfillmentOfEditorVariableGetMethod(final String command);
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která si zjistí, co má vypsat do editoru výstupů - případně si zjistí
	 * potřebnou hodnotu - pokud se jedná o například proměnnou v editoru příkazů
	 * nebo v instanci třídy či metodu a vypíše ji do zmíněného editoru, případně
	 * vypíše nějakou chybovou hlášku, pokud někde nastane chyba například při
	 * volání metody, získávání hodnoty z proměnné, apod. tato hodnota nebude
	 * započítána do konečného výpisu do editoru výstupů
	 * 
	 * Metoda si rozdělí text dle znaku plus (+) a otestuje jednotlivé hodnoty, "co
	 * jsou zač", například pro zavolání metody apod. případně ji zavolá a získá tak
	 * hodnotu
	 * 
	 * Metodě nejsou implementovýny aritmetické operace, akorát exponent (x ^ y, kde
	 * x a y mohou být metody, proměnné nebo čísla) jinak se používá podobně jako v
	 * eclipse rozdělení textu dle symbolu plus
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return výpis hodnot (text), který se má vypsat do editoru příkazů, například
	 *         získané hodnoty z metody, apod (viz výše), případně se nejprve vypíše
	 *         nějaká chyba, ke které by mohlo dojít, například nepovedlo se získat
	 *         hodnotu zadané proměnné z nějakého důvodu, apod., na konec se vypíše
	 *         Úspěch: konečná hodnota pro výpís, případně Neúspěch - pokud se
	 *         nepovede vypsat či získat konečnou hodnotu
	 */
	String makePrintValueLiteral(final String command);
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která si z tetu v parametru zjistí že má vytvořit instanci objektu
	 * List konkrétně instanci ArrayListu, dále jestli ho má již při deklaraci
	 * naplnit nějakýma hodnotama nebo ne.
	 * 
	 * Možná syntaxe:
	 * 
	 * (final) List<DataType> variableName = new ArrayList<>();
	 * 
	 * (final) List<DataType> variableName = new ArrayList<>(SomeParameters);
	 * 
	 * parametry jsou stejné jako u zavolání metody nebo konstruktoru
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se podařilo úspěšně vytvořil ArrayList nebo Neúspěch,
	 *         případně oznámění s chybou, která nastala při pokusu o vytvoření
	 *         instance zmíněného ArrayListu
	 */
	String makeCreateAndFullfillmentList(final String command);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která si z textu v parametru zjistí, o jaký list se jedná, pokud
	 * existuje, tak si zjistí hodnotu, kterou má přidat na jeho konec a pokudsi si
	 * tak učinit, pokud dojde k chybě bude o tom uživatel informován, pokud se
	 * operace povede, vypíše se do editoru výstupů Úspěch
	 * 
	 * @param command
	 *            - užiatelem zadaný příkaz
	 * 
	 * 
	 * @return Úspěch, pokud se výše zmíněná operace povede, jinak Neúspěch,
	 *         případně i informace o chybě, ke které došlo
	 */
	String makeAddValueToEndOfList(final String command);
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro přidání hodnoty do listu na zadanou pozici v daném
	 * listu
	 * 
	 * z následující syntaxe si zjistí o jaký list se jedná (dle reference) pak
	 * metoda add(index, value); z té si zjistí index a pokud "projde ověřením", pak
	 * se zjistí hodnota a pokud bude rozpoznána, přidá se do daného listu
	 * 
	 * Syntaxe:
	 * 
	 * var.add(index, specificValue);
	 * 
	 * index >= 0 && index < list.size
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se povede zmíněná operace, jinak Neúspěch s infomací o
	 *         chybě, ke které došlo
	 */
	String makeAddValueToIndexAtList(final String command);
	
	
	
	
	
	
	
	/**
	 * Metoda, která se pokusí vymazat obsah kolekce - listu pomocí metody clear();
	 * nad zadaou kolekcí
	 * 
	 * syntaxe:
	 * 
	 * referenceToList.clear();
	 * 
	 * Z výše uvedené syntaxe se pokusí získat název reference na list, pokd se tak
	 * povede, tak se ho pokusí vymazat pomocí metody clear, případně někde -. pokud
	 * dojde k nějaé chybě, bude oznámena
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se operace povede, jinak Neúspěch s infomací o chybě,
	 *         která nastala
	 */
	String makeClearList(final String command);
	
	
	
	
	
	
	
	/**
	 * Metoda, která se pokusí získat hodnotu na zadaném indexu v zadaném Listu
	 * (kolekci)
	 * 
	 * ze syntaxe:
	 * 
	 * referenceToList.ge(number >= 0 && number < listSize);
	 * 
	 * metoda si zjistí. zuda zadaný list existuje, pokud ano, tak zda je index
	 * zadaný ve správném intervalu a pokusí se vrátit hodnotu na zadaném indexu v
	 * listu
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspech s hodnotou nebo Neúspěch, pokud se nepovede získat danou
	 *         hodnotu
	 */
	String makeGetValueAtIndexOfList(final String command);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která nastaví hodnotu na zadaném indexu v kolekci coby instance třídy
	 * VariableList
	 * 
	 * Metoda si zjistí, zda se jedna o zadanou instnaci třídy, index a hodnotu,
	 * kterou má vložit na příslušný index, a pokusí se provést tuto operaci, pokud
	 * dojde někde k chybě, vypíše se chybová hláška s příslušnou chybou
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, nebo Neúspěch s chybovou hláškou, pokud se operace nepovede
	 */
	String makeSetValueAtIndexOfList(final String command);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která má za úkol převést List - kolekci do jednorozměrného pole
	 * stejného datového typu, jak je daná kolekce.
	 * 
	 * Metoda si zjistí název listu, a zda existuje, a pokusí se ho převést do pole,
	 * které následně vypíše, pokud se to nepovede, vypíše se chybové oznámení s
	 * informací o chybě, ke které došlo
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů (nebo jeho část, např
	 *            parametr metody, apod.)
	 * 
	 * @return Uspěch a výpis pole, pokud se povede převést zadaný List do
	 *         jednorozměrného pole daného datového typu, jinak Neúspěch, pokud se
	 *         daná operace nepovede dále se vytskne k Neúspěchu i příslušná chyba,
	 *         ke které došlo
	 */
	String makeListToOneDimensionalArray(final String command);
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která má za úkol zjistit délku neboli velikost Listu - kolekce.
	 * Metoda si zjistí název Listu, resp. reference na něj, otestuje, zda takový
	 * List existuje, pokud ano, tak si zjistí jeho délku a tu vrátí spolu s hláškou
	 * Úspěch, pokud by došlo nekde při nějakém kroku k nějaké chybě bude oznáměna
	 * uživateli.
	 * 
	 * @param command
	 *            - uživatelem zadaný přkaz v editoru příkazů, nebo jeho čáat
	 *            (například parametr metoda, konstruktoru, apod.)
	 * 
	 * @return Úspěch a velikost kolekce, nebo, pokud se výše zmíněná operace
	 *         nepovede, Neúspěch a případně i chybovou hlášku
	 */
	String makeListSize(final String command);
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která se pokusí odebrat hodnotu z Listu - kolekce na zadaném indexu.
	 * 
	 * Metoda si zjistí název / reference na List, pokud existuje, tak se pokusí
	 * zjistit index, ze kterého se má smazat hodnota v Listu a případně ho vymaže,
	 * pokud dojde k nějaké chybě, vypíše se chybová zprávy do editoru výstupů s
	 * informací o chybě, ke které došlo
	 * 
	 * @param command
	 *            - uživatelem zadaný přkaz do editoru přkazů
	 * 
	 * @return Úspěch, pokud se operace povede, jinak neúspech s infomací o chybě,
	 *         ke které došlo
	 */
	String makeRemoveValueAtIndexOfList(final String command);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která se pokusí vytvořit instanci třídy VariableArray a předá se do
	 * konstruktoru potřebná data, k vytvoření "reprezentace" jednorozměrného pole,
	 * se kterým bude uživatel moci pracovat v editoru příkazů, viz informace v
	 * třídě VariableArray.java
	 * 
	 * Metoda si zjistí jaký datový typ má být zmíněné pole, zda ho má naplnit
	 * nějakými hodnota mi nebo ne, zda má vytvořit pole né s hodnotami ale nějaké
	 * veliksti, apod. viz implementace metody
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se zmíněná operace povede, jinak Neúspěch, s informací
	 *         o chybě, ke které došlo
	 */
	String makeDeclarationAndFullfillmentArray(final String command);
	
	
	
	
	
	/**
	 * Metoda, která slouží pro vytvoření proměnné typu jednorozměrné pole v editoru
	 * příkazů.
	 * 
	 * Konkrétně se pokusí vytvořit instanci třídy VariableArray a předaji se do
	 * konstrukotruí potřebná data k vytvoření "reprezentace" jednorzměrného pole,
	 * se kterým bude uživatel schopen dále pracovat v editoru příkazů, viz třída
	 * VariableArray.java.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkaů, ze kterého se
	 *            rozpoznají potřebná data.
	 * 
	 * @return Úspěch, pokud se výše popsaná operace provede úspěšně, jinak Neúspěch
	 *         s informací o chybě, ke které došlo.
	 */
	String makeDeclarationAndFullfillmentArrayShort(final String command);
	
	
	
	
	
	
	
	/**
	 * Metoda, která má za úkol vytvořit instanci třídy VariableArray, která
	 * reprezentuje instanci jednorozměrného pole v editoru příkazů.
	 * 
	 * Metoda vytvoří instanci zmíněné třídy, a vytvoří tak instaci jednorozměérného
	 * pole zadaného datového typu, s definovanou délkou.
	 * 
	 * Metoda si zjistí, zda jsou správně zadány datové typy, název proměnné coby
	 * reference na zmíněné pole, velikost pole, a zda má být pole final nebo ne
	 * 
	 * @param command
	 *            = uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se zmíněná operace povede, jinak Neúspěch s informací o
	 *         chybě, ke které došlo
	 */
	String makeDeclarationArrayWithLength(final String command);
	
	
	
	
	
	/**
	 * Metoda, která se pokudsí vytvořit insanci třídy VariableArray, která
	 * reprezentuje jednorozměrné pole datového typu jednoho ze základních typů Javy
	 * (byte, String, int, ...).
	 * 
	 * Toto jednorozěrné pole, které se má vytvořit se rovnou naplní, resp. se
	 * převezme zavoláním metody. Metoda může být zavolána buď nad instancí třídy
	 * nebo se může jedna o zavolání statické metody nad třídou v diagramu tříd (ne
	 * nad instancí).
	 * 
	 * Vždy se ale musí shodovat datový typ pole a návratový typ metody.
	 * 
	 * možné syntaxe:
	 * 
	 * (final) dataType[] variableName = instance.methodName(parameters);
	 * (parameters jsou volitelné a datový typ je jeden z datových typů Javy)
	 * 
	 * (final) dataType[] variableName = packageName. (anotherPackageName.)
	 * ClassName.staticMethodName(parameters); (podmínky stejné jako výše)
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz z deditoru příkazů, ze kterého se
	 *            rozpoznají příslušné hodnoty (zda je pole final, datový typ pole,
	 *            název metody, ...)
	 * 
	 * @return 'ÚSpěch' v případě, že se výše popsaná operace úspěšně vykoná, tj.
	 *         bude vytvořené jednorzměrné pole získáním jej z nějaké metody. Pokud
	 *         se tato operace nepovede, vrátí se text v podobně například
	 *         'Ne[spěch' případně nějký popis chyby, ke které došlo.
	 */
	String makeDeclarationArrayByCallingMethod(final String command);
	
	
	
	
	
	
	/**
	 * Metoda, která naplní hodnotu na zadném indexu v poli nějakou hodnotou, kterou
	 * získá zavoláním nějaké metody nad instancí třídy
	 * 
	 * Metoda si zjistí, jakou metodu nad jakou instancí má zavolat, jestli jsou
	 * stené datové typy hodnot, aby šla hodnota v poli naplnit a zda nění pole
	 * final
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se výše zmíněná opereace povede, jinak Neúspech s
	 *         informací o chybě, ke které došlo
	 */
	String makeFullfillmentValueAtIndexOfArrayMethod(final String command);
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která naplní hodnotu v poli na zadaném indexu nějakou hodnotou,
	 * kterou si zjistí z konstanty - čísla, text, znak, apod.
	 * 
	 * Metoda si zjistí o jaké pole se jedná, o jaký index se jedná a konstantu,
	 * kterou má vložit na zadné místo (dle indexu) v poli
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se operace úspěšně povede, jinak Neúspěch s informací o
	 *         chybě, ke které došlo
	 */
	String makeFullfillmentValueAtIndexOfArrayConstant(final String command);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která naplní položku v poli na zadném indexu zadanou hodnotou z
	 * proměnné.
	 * 
	 * Metoda si zjsití pole a index pozice, kam má vložit hodnotu, kterou si načte
	 * buď z kolekce, proměnné v instanci třídy nebo proměnné vytvořené v editoru
	 * příkazů nebo z nějaké pozice v nějakém poli
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se výše uvedená operace povede, jinak Neúspěch s
	 *         informací o chybě, ke které došlo
	 */
	String makeFullfillmentValueAtIndexOfArrayVariable(final String command);
	
	
	
	
	
	
	
	/**
	 * Metoda, která získá a vypíše do editoru příkazů hodnotu z pole na zadaném
	 * indexu.
	 * 
	 * Metoda si nejprve zjistí o "jaké" pole se jedná a zda vůbec existuje, pak
	 * otestuje index hodnoty a pokud budou podmínky splněny hodnota bude vypsána,
	 * případně se vypíše nějaká chybová hláška s informací o chybě, ke které došlo
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz editoru příkazů
	 * 
	 * @return Uspěch, pokud se výše uvedená operace povede, jinak Neúspěch s
	 *         infomací o chybě, ke které došlo
	 */
	String makeGetValueFromArrayAtIndex(final String command);
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která inkrementuje hodnotu na zadaném indexu v jednorozměrném poli,
	 * vytvořeným uživatelem v editoru příkazů (prefixová forma pro inkrementaci)
	 * 
	 * Metoda si zjistí název pole, zda existuje, a hodnotu, kterou má
	 * inkrementovat, pokud nastane někde v průběhu nějaké operace nějaká chyba,
	 * bude vypsána chybová hláška užvteli do editoru výstupů
	 * 
	 * @param command
	 *            - uživatelem zadaný příákaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud výše zmíněná operace proběhne v pořádku, jinak Neúspěch
	 *         s informacemi o chybě, která nastala
	 */
	String makeIncrementArrayValueBefore(final String command);
	
	
	
	
	/**
	 * Metoda, která inkrementuje hodnotu na zadaném indexu v jednorozměrném poli,
	 * vytvořeným uživatelem v editoru příkazů (postfixová forma pro inkrementaci)
	 * 
	 * Metoda si zjistí název pole, zda existuje, a hodnotu, kterou má
	 * inkrementovat, pokud nastane někde v průběhu nějaké operace nějaká chyba,
	 * bude vypsána chybová hláška užvteli do editoru výstupů
	 * 
	 * @param command
	 *            - uživatelem zadaný příákaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud výše zmíněná operace proběhne v pořádku, jinak Neúspěch
	 *         s informacemi o chybě, která nastala
	 */
	String makeIncrementArrayValueBehind(final String command);
	
	
	/**
	 * Metoda, která dekrementuje hodnotu na zadaném indexu v jednorozměrném poli,
	 * vytvořeným uživatelem v editoru příkazů (prefixová forma pro dekrementaci)
	 * 
	 * Metoda si zjistí název pole, zda existuje, a hodnotu, kterou má
	 * dekrementovat, pokud nastane někde v průběhu nějaké operace nějaká chyba,
	 * bude vypsána chybová hláška užvteli do editoru výstupů
	 * 
	 * @param command
	 *            - uživatelem zadaný příákaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud výše zmíněná operace proběhne v pořádku, jinak Neúspěch
	 *         s informacemi o chybě, která nastala
	 */
	String makeDecrementArrayValueBefore(final String command);
	
	
	/**
	 * Metoda, která dekrementuje hodnotu na zadaném indexu v jednorozměrném poli,
	 * vytvořeným uživatelem v editoru příkazů (postfixová forma pro dekrementaci)
	 * 
	 * Metoda si zjistí název pole, zda existuje, a hodnotu, kterou má
	 * dekrementovat, pokud nastane někde v průběhu nějaké operace nějaká chyba,
	 * bude vypsána chybová hláška užvteli do editoru výstupů
	 * 
	 * @param command
	 *            - uživatelem zadaný příákaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud výše zmíněná operace proběhne v pořádku, jinak Neúspěch
	 *         s informacemi o chybě, která nastala
	 */
	String makeDecrementArrayValueBehind(final String command);
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí délku pole, resp. počet prvků v poli.
	 * 
	 * Metoda si zjistí, název pole, resp referenci na instanci třídy VariableArray,
	 * která dané pole reprezentuje v editoru příkazů a zjistí si jeho délku, pomocí
	 * vlastnoti nad polem .length
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Uspěch a délku pole, pokud se zmíněná operace povede, jinak Neúspěch
	 *         s informací o chybě, ke které došlo
	 */
	String makeGetLengthArray(final String command);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro naplění proměnné na zadaném indexu v jednorozměrném
	 * poli v příslušné instanci nějaké třídy návratovou hodnotu nějaké metody.
	 * 
	 * Metoda může být buď statické nebo zavolána nad nějakou instnací třídy.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se výše popsaná operace provede úspěšně, jinak Ne)spěch
	 *         a případně nějaké hlášení o nastalé chybě.
	 */
	String makeFulfillmentOfArrayAtIndexInInstanceByMethod(final String command);
	
	
	/**
	 * Metoda, která slouží pro naplění proměnné na zadaném indexu v jednorozměrném
	 * poli v příslušné instanci nějaké třídy pomocí objektu, který vznikne
	 * vytvořením instance nějaké třídy z diagramu tříd.
	 * 
	 * Metoda může být buď statické nebo zavolána nad nějakou instnací třídy.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se výše popsaná operace provede úspěšně, jinak Ne)spěch
	 *         a případně nějaké hlášení o nastalé chybě.
	 */
	String makeFulfillmentOfArrayAtIndexInInstanceByConstructor(final String command);
	
	/**
	 * Metoda, která slouží pro naplění proměnné na zadaném indexu v jednorozměrném
	 * poli v příslušné instanci nějaké třídy hodnotou, která nachází vnějaké
	 * proměnné buď v nějaké instanci třídy nebo proměnné vytvořené uživatelem v
	 * editoru příkazů.
	 * 
	 * Metoda může být buď statické nebo zavolána nad nějakou instnací třídy.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se výše popsaná operace provede úspěšně, jinak Ne)spěch
	 *         a případně nějaké hlášení o nastalé chybě.
	 */
	String makeFulfillmentOfArrayAtIndexInInstanceByVariable(final String command);
	
	/**
	 * Metoda, která slouží pro naplění proměnné na zadaném indexu v jednorozměrném
	 * poli v příslušné instanci nějaké třídy pomocí nějaké konstanty (například
	 * true, false, znak, text atd.) nebo početní operací.
	 * 
	 * Metoda může být buď statické nebo zavolána nad nějakou instnací třídy.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch, pokud se výše popsaná operace provede úspěšně, jinak Ne)spěch
	 *         a případně nějaké hlášení o nastalé chybě.
	 */
	String makeFulfillmentOfArrayAtIndexInInstanceByConstant(final String command);
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zkusí získat hodnotu, která se nachází v jednorozměrném poli v
	 * nějaké instanci třídy.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return Úspěch s příslušnou hodnotu, jiank Neúspěch a případně text chyby ke
	 *         které došlo.
	 */
	String makeGetValueFromArrayAtIndexInInstance(final String command);
	
	
	/**
	 * Metoda, která zkusí získat velikost X - rozměrného pole, které se nachází v
	 * nějaké instanci třídy.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return Úspěch s velikost zadaného pole, jiank Neúspěch a případně text chyby
	 *         ke které došlo.
	 */
	String makeGetLengthOfArrayInInstance(final String command);
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro prefixové inkrementování položky v jednorozměrném
	 * poli v instanci třídy.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return Úspěch, pokud se úspěšně inkrementuje, jinak neúspěch a případně
	 *         nějaký text k tomu
	 */
	String makePrefixIncrementValueAtArrayInInstance(final String command);
	
	/**
	 * Metoda, která slouží pro prefixové dekrementování položky v jednorozměrném
	 * poli v instanci třídy.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return Úspěch, pokud se úspěšně dekrementuje, jinak neúspěch a případně
	 *         nějaký text k tomu
	 */
	String makePrefixDecrementValueAtArrayInInstance(final String command);
	
	/**
	 * Metoda, která slouží pro postfixové inkrementování položky v jednorozměrném
	 * poli v instanci třídy.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return Úspěch, pokud se úspěšně inkrementuje, jinak neúspěch a případně
	 *         nějaký text k tomu
	 */
	String makePostfixIncrementValueAtArrayInInstance(final String command);
	
	/**
	 * Metoda, která slouží pro postfixové dekrementování položky v jednorozměrném
	 * poli v instanci třídy.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return Úspěch, pokud se úspěšně dekrementuje, jinak neúspěch a případně
	 *         nějaký text k tomu
	 */
	String makePostfixDecrementValueAtArrayInInstance(final String command);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Práce s dvourozměrným polem, ale pouze v instanci nějaké třídy, která se
	// nachází v diagramu instancí:
	
	
	/**
	 * Metoda, která slouží pro naplnění položky na nějakém indexu ve dvourozměrném
	 * poli v instancí nějaké třídy návratovou hodnotou, která vrátí nějaká metoda.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.variableArrayname [index] [index] =
	 * instance.methodName(params);
	 * 
	 * referenceName.variableArrayname [index] [index] =
	 * model.data.ClassName.methodName(params);
	 * 
	 * @param command
	 *            - uživatelem zadaná příkaz v editoru příkazů
	 * 
	 * @return Úspšch, pokud se operace vykoná úspěšně, jinak Neúspěch a případně
	 *         nějaký text chyby, ke které došlo.
	 */
	String makeFulfillmenIndexAtTwoDimArrayByMethod(final String command);
	
	/**
	 * Metoda, která slouží pro naplnění položky na nějakém indexu ve dvourozměrném
	 * poli v instancí nějaké třídy novou instancí nějaké třídy (pokud se povede
	 * její vytvoření, resp. povede se zavolání konstruktoru).
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.variableArrayname [index] [index] = new
	 * model.data.ClassName(params);
	 * 
	 * @param command
	 *            - uživatelem zadaná příkaz v editoru příkazů
	 * 
	 * @return Úspšch, pokud se operace vykoná úspěšně, jinak Neúspěch a případně
	 *         nějaký text chyby, ke které došlo.
	 */
	String makeFulfillmenIndexAtTwoDimArrayByConstructor(final String command);
	
	/**
	 * Metoda, která slouží pro naplnění položky na nějakém indexu ve dvourozměrném
	 * poli v instancí nějaké třídy hodnotou, která se získá z nějaké proměnné, může
	 * být v editoru zdrojového kódu nebo instanci nějaké třídy v diagramu instancí.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.variableArrayname [index] [index] = referenceName.variableName;
	 * 
	 * @param command
	 *            - uživatelem zadaná příkaz v editoru příkazů
	 * 
	 * @return Úspšch, pokud se operace vykoná úspěšně, jinak Neúspěch a případně
	 *         nějaký text chyby, ke které došlo.
	 */
	String makeFulfillmenIndexAtTwoDimArrayByVariable(final String command);
	
	/**
	 * Metoda, která slouží pro naplnění položky na nějakém indexu ve dvourozměrném
	 * poli v instancí nějaké třídy nějakou konstantou, například logickou hodnotou,
	 * znakem, textem nebo početní operací.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.variableArrayname [index] [index] = true;
	 * 
	 * referenceName.variableArrayname [index] [index] = ';';
	 * 
	 * referenceName.variableArrayname [index] [index] = (5*5) ^ 2 + cos(10);
	 * 
	 * ...
	 * 
	 * @param command
	 *            - uživatelem zadaná příkaz v editoru příkazů
	 * 
	 * @return Úspšch, pokud se operace vykoná úspěšně, jinak Neúspěch a případně
	 *         nějaký text chyby, ke které došlo.
	 */
	String makeFulfillmenIndexAtTwoDimArrayByConstant(final String command);
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro získání hodnoty z indexu ve dvourozměrném poli v
	 * instanci nějaké třídy.
	 * 
	 * Syntaxe:
	 * 
	 * referenceNam.variableName [index][index];
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return Úspěch, pokud se povede úspěšně získat hodnotu, jinak Neúspěch a
	 *         případně chybovou hlášku, kt které došlo.
	 */
	String makeGetValueAtIndexAtTwoDimArrayInInstance(final String command);
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro inkrementaci (v prefixové formě) hodnoty ve
	 * dvourozměrném poli v instanci nějaké třídy.
	 * 
	 * Syntaxe:
	 * 
	 * ++ referenceName.variableName [index][index];
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch v případě, že se hodnota na zadaném indexu ve dvourozměrném
	 *         poli provede úspěšně, v opačném případě Neúspěch a případně nějaká
	 *         chyba, ke které došlo.
	 */
	String makePrefixIncrementValueAtIndexInTwoDimArrayInInstance(final String command);
	
	/**
	 * Metoda, která slouží pro dekrementaci (v prefixové formě) hodnoty ve
	 * dvourozměrném poli v instanci nějaké třídy.
	 * 
	 * Syntaxe:
	 * 
	 * -- referenceName.variableName [index][index];
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch v případě, že se hodnota na zadaném indexu ve dvourozměrném
	 *         poli provede úspěšně, v opačném případě Neúspěch a případně nějaká
	 *         chyba, ke které došlo.
	 */
	String makePrefixDecrementValueAtIndexInTwoDimArrayInInstance(final String command);
	
	
	/**
	 * Metoda, která slouží pro inkrementaci (v postfixové formě) hodnoty ve
	 * dvourozměrném poli v instanci nějaké třídy.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.variableName [index][index] ++;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch v případě, že se hodnota na zadaném indexu ve dvourozměrném
	 *         poli provede úspěšně, v opačném případě Neúspěch a případně nějaká
	 *         chyba, ke které došlo.
	 */
	String makePostfixIncrementValueAtIndexInTwoDimArrayInInstance(final String command);
	
	
	/**
	 * Metoda, která slouží pro dekrementaci (v postfixové formě) hodnoty ve
	 * dvourozměrném poli v instanci nějaké třídy.
	 * 
	 * Syntaxe:
	 * 
	 * referenceName.variableName [index][index] --;
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return Úspěch v případě, že se hodnota na zadaném indexu ve dvourozměrném
	 *         poli provede úspěšně, v opačném případě Neúspěch a případně nějaká
	 *         chyba, ke které došlo.
	 */
	String makePostfixDecrementValueAtIndexInTwoDimArrayInInstance(final String command);
}