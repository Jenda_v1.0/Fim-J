package cz.uhk.fim.fimj.commands_editor;

/**
 * Tato třída slouží pouze pro "předání" hodnot z jedné metody do jiné.
 * <p>
 * Jedná se o to, že ve třídě MakeOperation je metoda 'getValueFromVar_Method_Constant', která slouží v podstatě pro
 * rozpoznání hodnoty a dle toho získá výslednou hodnotu. (Dále využito i pro jiné metody za účelem předání hodnot
 * ohledně vykonání nějaké operace a získání / vrácení výsledku operace apod.)
 * <p>
 * Například, když se do uvedené metody dostane syntaxe pro zavolání metody, tak tu metodu zavolá a výsledek se vrátí,
 * ale je dále potřeba testovat, zda se metoda zavolala a zda něco vrátila nebo se metoda nezavolala a pak se vrátí
 * "výchozí" výsledek, tedy v některých případěch null hodnota, nebo třeba -1 apod. To už záleží na konkrétní operaci.
 * <p>
 * Proto je zde tato třída, která obsahuje pouze dvě proměnné, jedna je logická hodnota, která slouží pro "určení" toho,
 * zda se metoda či nějaká operace provedla / vykonala apod.
 * <p>
 * Pak je zde druhá proměnná, která obshauje vždy výslednou hodnoty, popř. výchozí. Buď obsahuje výchozí hodnotu, v
 * případě, že nebyla například zmíněný metoda zavolána, tudíž nic nevrátila apod. Nebo obsahuje nějakou hodnotu, ať už
 * je to null hodnota nebo jiná apod. Prostě se ta metoda či operace provedla v pořádku, tak bude logická proměnná true
 * a druhá hodnota obsahuje výslednou / vrácenou hodnotu, která se může vzít.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ResultValue {

    /**
     * Logická proměnná, která slouží pro určení toho, zda se úspěšně vykonala nějaká operace nebo ne. Hodnota true
     * značí, že se příslušná operace vykonala úspěšně (například se vykonala nějaká metoda nebo získala hodnota z
     * proměnné apod.) a je možné si vzít hodnotu z proměnná objResult, která obsahuje hodnotu, kterou například vrátila
     * metoda apod. (výsledek operace).
     */
    private final boolean executed;


    /**
     * Tato proměnná obsahuje hodnotu, která se dostane jako výsledek nějaké operace, například zavolání metody, získání
     * hodnoty z proměnné apod.
     */
    private final Object objResult;


    /**
     * Konstruktor této třídy.
     *
     * @param executed
     *         - logická proměnná, která značí, zda se úspěšně vykonala nějaká operace a je možné si vzít druhou hodnotu
     *         (výsledek operace).
     * @param objResult
     *         - výsledek nějaké operace, naříklad zavolání metody, získání hodnoty z proměnné, výsledek početní
     *         operace, případně null, pokud nějaká operace selahala, pak se může jednat o výchozí hdonotu.
     */
    public ResultValue(final boolean executed, final Object objResult) {
        super();

        this.executed = executed;
        this.objResult = objResult;
    }


    /**
     * Getr na logickou hodnotu, která značí, zda se úspěšně vykonala nějaká operace nebo ne.
     *
     * @return true, pokud se úspěšně vykonala příslušná operace a je možné si vzít hodnotu z proměnné objResult, jinak
     * false.
     */
    public boolean isExecuted() {
        return executed;
    }


    /**
     * Getr na hodnotu, která obsahuje výsledek operace.
     *
     * @return výsledek nějaké operace.
     */
    public Object getObjResult() {
        return objResult;
    }
}
