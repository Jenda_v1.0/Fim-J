package cz.uhk.fim.fimj.commands_editor.values_completion_window;

/**
 * Třída obsahuje hodnoty, které se vkládají do okna pro automatické dokončení / doplnění kódu.
 * <p>
 * <p>
 * Syntaxe v okně pro doplnění kódu: text_1 - text_2 -> text_3
 * <p>
 * text_1 = text před pomlčkou, dle které se filtrují hoodnoty při psaní. (inputText)
 * <p>
 * text_2 = definovaná hodnota za pomlčkou (informace, například datový typ proměnné nebo návratová hodnota metody)
 * (shortDesc)
 * <p>
 * text_3 = hodnota, která je napsána v "druhém" okně, které se otevře ke každé zvolené hodnota v okně pro automatické
 * dokončení kódu. Zde senachází nějaké informace pro uživatele, například popis metody apod. (summary)
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 28.07.2018 12:05
 */

class ValueCompletionInfo {

    /**
     * Text, který se zobrazí před pomlčkou v okně pro automatické doplnění kódu.
     */
    private final String inputText;

    /**
     * Text, který se vloží do editoru ve chvíli, kdy uživatel potvrdí nějakou hodnotu v okně.
     */
    private final String replacementText;

    /**
     * Text, který je zobrazen za pomlčkou v okně pro automatické doplnění kódu.
     */
    private final String shortDesc;

    /**
     * Text, který se zobrazí v okně, které se zobrazí po označení nějaké hodnoty v první okně. Jedná se o stručný
     * popisek například k nějaké metodě apod.
     */
    private final String summary;


    /**
     * Konstruktor třídy.
     * <p>
     * Slouží pro naplnění proměnných.
     * <p>
     * Syntaxe v okně: text_1 - text_2 -> v druhém okně je popisek (desc).
     *
     * @param inputText
     *         - text, který se má zobrazit v okně pro doplňování (text_1)
     * @param replacementText
     *         - text, který se vloží na pozici kurzoru po zvolení nějaké hodnoty v okně.
     * @param shortDesc
     *         - text, který se má zobrazit za pomlčkou v okně pro doplňování (text_2).
     */
    ValueCompletionInfo(final String inputText, final String replacementText, final String shortDesc) {
        this.inputText = inputText;
        this.replacementText = replacementText;
        this.shortDesc = shortDesc;
        summary = null;
    }


    /**
     * Konstruktor třídy.
     * <p>
     * Slouží pro naplnění proměnných.
     * <p>
     * Syntaxe v okně: text_1 - text_2 -> v druhém okně je popisek (desc).
     *
     * @param inputText
     *         - text, který se má zobrazit v okně pro doplňování (text_1)
     * @param replacementText
     *         - text, který se vloží na pozici kurzoru po zvolení nějaké hodnoty v okně.
     * @param shortDesc
     *         - text, který se má zobrazit za pomlčkou v okně pro doplňování (text_2).
     * @param summary
     *         - text, který se má zobrazit v novém okně, které se zobrazí v druhém okně s nápovědou k hodnotě. Jedná se
     *         o krátky popisek (desc).
     */
    ValueCompletionInfo(final String inputText, final String replacementText, final String shortDesc, final
    String summary) {
        this.inputText = inputText;
        this.replacementText = replacementText;
        this.shortDesc = shortDesc;
        this.summary = summary;
    }


    String getInputText() {
        return inputText;
    }

    String getReplacementText() {
        return replacementText;
    }

    String getShortDesc() {
        return shortDesc;
    }

    String getSummary() {
        return summary;
    }


    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;

        if (!(obj instanceof ValueCompletionInfo))
            return false;

        final ValueCompletionInfo valueCompletionInfo = (ValueCompletionInfo) obj;

        // inputText, replacementText a shortDesc nesmí být null:

        if (inputText == null || !inputText.equals(valueCompletionInfo.getInputText()))
            return false;

        if (replacementText == null || !replacementText.equals(valueCompletionInfo.getReplacementText()))
            return false;

        if (shortDesc == null || !shortDesc.equals(valueCompletionInfo.getShortDesc()))
            return false;

        if (summary == null && valueCompletionInfo.getSummary() == null)
            return true;

        return summary == null || valueCompletionInfo.getSummary() == null || summary.equals(valueCompletionInfo.getSummary());
    }

    @Override
    public int hashCode() {
        // Zdroj: https://www.mkyong.com/java/java-how-to-overrides-equals-and-hashcode/

        final int prime = 31;

        int result = 17;

        result = prime * result + (inputText == null ? 0 : inputText.hashCode());
        result = prime * result + (replacementText == null ? 0 : replacementText.hashCode());
        result = prime * result + (shortDesc == null ? 0 : shortDesc.hashCode());
        result = prime * result + (summary == null ? 0 : summary.hashCode());

        return result;
    }
}
