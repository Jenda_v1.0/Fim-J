package cz.uhk.fim.fimj.commands_editor;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import cz.uhk.fim.fimj.instances.Instances;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;

/**
 * Třída slouží jako implementace metod pro parsování textu na Javoské datové typy, např character, integer, long, ...
 * viz metody níže
 * <p>
 * Konkrétně tato třída obsahuje "sub třídy", které implementují rozhraní s jednou metodou, a vždy se pomocí generiky
 * doplnit datový typ, na které se má text "Naprasovat"
 * <p>
 * <p>
 * Tyto metody se používají převážně v editoru příkazů, kde jet třeba zadaný text "Naparsovat" na jeden z datových typů
 * pro vložení například do proměnné, metody, apod
 * <p>
 * Dále jso uvyužívány v dialogu pro novou instanci a zavolání metody s parametry
 * <p>
 * <p>
 * Zdroj: http://codereview.stackexchange.com/questions/38145/parse-strings-and-convert-the-value-to-java-types
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class ParseToJavaTypes {

	/**
	 * Regulární výraz pro otestování v jakém "stavu" se nachází text pro naparsování na datový typ char / Character
	 * Může se nacházet bud v jednoduchých uvozovkách nebo bez nich:
	 * 
	 * Note: V regulárních výraz projdou dva znaky, je to jen proto, že se do dané metody "dostanou" pouze znaky, které 
	 * projdou již jinýi regulárními výrazy a testy, tak aby to byly opravdu jen znak, protože znakem může být i \n, apod.
	 * 
	 * Reguálární výraz pro otestování, zda se znak nachází v uvozovkách 'X':
	 */
	private static final String REGEX_FOR_CHAR = "^\\s*'.*'\\s*$";
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí přeparsované pole, které je zadané v parametru metody na
	 * pole objektů, neboli v parametru metody je proměnná - parametr typu Object,
	 * ve kterém se nachází jednorozměrné pole, a to se převede pomocí těla metody
	 * na pole objektů
	 * 
	 * @param array
	 *            - proměnná typu Object, které obsahuje jedorozměrné pole hodnot
	 * 
	 * @return jednorozměrné pole hodnot
	 */
	public static Object[] convertToObjectArray(final Object array) {
		final Class<?> ofArray = array.getClass().getComponentType();

		if (ofArray.isPrimitive()) {
			final int length = Array.getLength(array);

			final List<Object> ar = new ArrayList<>(length);

			for (int i = 0; i < length; i++)
				ar.add(Array.get(array, i));

			return ar.toArray();
		}

		return (Object[]) array;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí přeparsované pole, které je zadané v parametru metody na
	 * pole objektů, neboli v parametru metody je proměnná - parametr typu Object,
	 * ve kterém se nachází jednorozměrné pole, a to se převede pomocí těla metody
	 * na pole objektů a u každé položky v tom poli se otestuje, zda je ta položka
	 * instance nějaké třídy z diagramu tříd, která se aktuálně nachází v diagramu
	 * instancí, pokud ano, pak se za tu položku ještě přidá text té referenční
	 * proměnné (reference).
	 * 
	 * O parametru array už se musí vědět, že je to jednorozměrné pole nějakého
	 * objektového typu.
	 * 
	 * @param array
	 *            - proměnná typu Object, které obsahuje jedorozměrné pole hodnot
	 * 
	 * @param txtReferenceVar
	 *            - textová proměnná, která obsahuje text "Referenční proměnná" v
	 *            uživatelem zvoleném jazyce.
	 * 
	 * @return jednorozměrné pole hodnot
	 */	
	public static Object[] convertToObjectArrayAndReferenceVariable(final Object array,
			final String txtReferenceVar) {
	    
		/*
		 * Získám délku toho pole, abych jej mohl proiterovat.
		 */
	    final int length = Array.getLength(array);
	    
	    /*
		 * List typu Object, do kterého budu vkládat položky z pole, případně doplněné
		 * položky z pole o referenční proměnnou.
		 */
	    final List<Object> objList = new ArrayList<>(length);
	    
	    
		for (int i = 0; i < length; i++) {
			/*
			 * Získám si hodnotu z pole na příslušném / iterovaném indexu.
			 */
			final Object objValue = Array.get(array, i);

			// pokud je to null, tak jej vložím do listu a pokračuji další iterací:
			if (objValue == null) {
				objList.add(null);
				continue;
			}
	    		
	    	
	    	/*
			 * Zde ta hodnota v poli není null hodnota, tak zjistím, zda je to instancě
			 * nějaké třídy, která se aktuálně nachází v diagramu instancí.
			 */
			final String reference = Instances.getReferenceToInstanceFromCdInMap(objValue);

			/*
			 * Pokud je ta hodnota nějaké instance v diagramu instancí, tak za tu hodnotu
			 * doplním referenční proměnnou, jinak jen přidám tu hodnotu.
			 */
			if (reference != null)
				objList.add(objValue.toString() + " (" + txtReferenceVar + ": " + reference + ")");

			else
				objList.add(objValue);
	    }
	    
		// Vrátím získané pole (potenciálně doplněné o reference):
		return objList.toArray();
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Třída, která obsahuje metodu, která přetypuje hodnotu typu String n ahodnotu
	 * typu boolean.
	 * 
	 * @author Jan Krunčík
	 * @version 1.0
	 */
	public static final class BooleanTypeParser implements ParseToJavaTypesInterface<Boolean> {

		@Override
		public Boolean parse(String value) {
			final String editedValue = value.trim().toLowerCase();
			
			return Boolean.parseBoolean(editedValue);
		}		
	}
	
	
	
	
	/**
	 * Třída, která obsahuje metodu, která přetpuje její paparemtr typu String na
	 * hodnotu typu Character. Pokud se operace nepovede, vypadne vyjímka, která se
	 * zapíše do logu a vrátí se null
	 * 
	 * @author Jan Krunčík
	 * @version 1.0
	 */
	public static final class CharacterTypeParser implements ParseToJavaTypesInterface<Character> {

		@Override
		public Character parse(String value) {
			try {
				if (value.matches(REGEX_FOR_CHAR)) {
					// Zde se jedná o text s uvozovkami, tak je odeberu:
					final String parseToChar = value.substring(value.indexOf('\'') + 1, value.lastIndexOf('\''));

					return parseToChar.charAt(0);
				}
				
				// Zde zkusím naprsovat hodnotu na prvním znaku - bez mezer:
				return value.charAt(0);

            } catch (StringIndexOutOfBoundsException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při parsování datového typu String na datový typ Char, text: " + value
                                    + ". Třída ParseToJavaTypes.java, třída CharacterTypeParser. Tato chyba může " +
                                    "nastat napřílad v případě, kdy se pří parsování vrátí hodnota záporná, nebo " +
                                    "větší či stejná jako velikost textu.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
				
				return null;
			}
		}		
	}
	
	
	
	
	
	
	
	/**
	 * Třída, která obsahuje metodu, která přetpuje její paparemtr typu String na
	 * hodnotu typu Byte, Pokud se operace nepovede, vypadne vyjímka, která se
	 * zapíše do logu a vrátí se null
	 * 
	 * @author Jan Krunčík
	 * @version 1.0
	 */
	public static final class ByteTypeParser implements ParseToJavaTypesInterface<Byte> {

		@Override
		public Byte parse(String value) {
			final String editedValue = value.replaceAll("\\s", "");
			
			try {
				return Byte.parseByte(editedValue);	
				
			} catch (NumberFormatException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při parsování datového typu String na datový typ Byte, text: " + value
                                    + ", třída ParseToJavaTypes.java, třída ByteTypeParser. Tato chyba může nastat " +
                                    "například v případě, kdy zadaný řetězec neobsahuje hodnotu, kterou je možné " +
                                    "'naparsovat' na datový typ Byte.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
				
				return null;
			}			
		}	
	}
	
	
	
	
	
	
	
	
	/**
	 * Třída, která obsahuje metodu, která přetpuje její paparemtr typu String na
	 * hodnotu typu Integer, Pokud se operace nepovede, vypadne vyjímka, která se
	 * zapíše do logu a vrátí se null
	 * 
	 * @author Jan Krunčík
	 * @version 1.0
	 */
	public static final class IntegerTypeParser implements ParseToJavaTypesInterface<Integer> {

		@Override
		public Integer parse(String value) {
			
			final String editedValue = value.replaceAll("\\s", "");
						
			try {
				return Integer.parseInt(editedValue);

            } catch (NumberFormatException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při parsování datového typu String na datový typ Integer, text: " + value
                                    + ", třída ParseToJavaTypes.java, třída IntegerTypeParser. Tato chyba může nastat" +
                                    " například při pokusu o 'přeparsování' / převedení hodnoty"
                                    + " datového typu Stringna datový typ Integer, ale zadaný String neobsahuje " +
                                    "hodnotu / hodnoty, které je možní převést na datový typ Integer.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
				
				
				return null;
			}
		}		
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Třída, která obsahuje metodu, která přetpuje její paparemtr typu String na
	 * hodnotu typu Long, Pokud se operace nepovede, vypadne vyjímka, která se
	 * zapíše do logu a vrátí se null
	 * 
	 * @author Jan Krunčík
	 * @version 1.0
	 */
	public static final class LongTypeParser implements ParseToJavaTypesInterface<Long> {

		@Override
		public Long parse(String value) {
			final String editedValue = value.replaceAll("\\s", "");

			try {
				return Long.parseLong(editedValue);
				
			} catch (NumberFormatException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při parsování hodnoty z datového typu string na datový typ Long, text: "
                                    + value
                                    + ", třída ParseToJavaTypes.java, třída LongTypeParser. Tato chyba může nastat " +
                                    "například v případě, kdy zadaný řetězec (text) nebosahuje hodnotu, kterou je " +
                                    "možné převést na datový typ Long.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
				
				
				return null;
			}
		}
	}
	
	
	
	
	
	
	
	/**
	 * Třída, která obsahuje metodu, která přetpuje její paparemtr typu String na
	 * hodnotu typu Short, Pokud se operace nepovede, vypadne vyjímka, která se
	 * zapíše do logu a vrátí se null
	 * 
	 * @author Jan Krunčík
	 * @version 1.0
	 */
	public static final class ShortTypeParser implements ParseToJavaTypesInterface<Short> {

		@Override
		public Short parse(String value) {
			final String editedValue = value.replaceAll("\\s", "");
			
			try {

				return Short.parseShort(editedValue);

			} catch (NumberFormatException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při parsování hodnoty datového typu String na datový typ Short, text: "
                                    + value
                                    + ", třída ParseToJavaTypes.java, třída ShortTypeParser. Tato chyba může nastat " +
                                    "například v případě, kdy zadaný řetězec (text) nebosahuje hodnotu, kterou je " +
                                    "možné převést na datový typ Short.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
				
				return null;
			}
		}		
	}
	
	
	
	
	
	
	/**
	 * Třída, která obsahuje metodu, která přetpuje její paparemtr typu String na
	 * hodnotu typu Float, Pokud se operace nepovede, vypadne vyjímka, která se
	 * zapíše do logu a vrátí se null
	 * 
	 * @author Jan Krunčík
	 * @version 1.0
	 */
	public static final class FloatTypeParser implements ParseToJavaTypesInterface<Float> {

		@Override
		public Float parse(String value) {
			final String editedValue = value.replaceAll("\\s", "");
			
			try {

				return Float.parseFloat(editedValue);

            } catch (NumberFormatException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při parsování hodnoty datového typu String na datový typ float, text: "
                                    + value
                                    + ", třída ParseToJavaTypes.java, třída FloatTypeParser. Tato chyba může nastat " +
                                    "například v případě, kdy zadaný řetězec (text) nebosahuje hodnotu, kterou je " +
                                    "možné převést na datový typ Float.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
				
				return null;
			}
		}		
	}
	
	
	
	
	
	
	
	
	/**
	 * Třída, která obsahuje metodu, která přetpuje její paparemtr typu String na
	 * hodnotu typu Double, Pokud se operace nepovede, vypadne vyjímka, která se
	 * zapíše do logu a vrátí se null
	 * 
	 * @author Jan Krunčík
	 * @version 1.0
	 */
	public static final class DoubleTypeParser implements ParseToJavaTypesInterface<Double> {

		@Override
		public Double parse(String value) {
			final String editedValue = value.replaceAll("\\s", "");
			
			try {
				return Double.parseDouble(editedValue);
				
			} catch (NumberFormatException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při parsování hodnoty datového typu String na datový typ Double, text: "
                                    + value
                                    + ", třída ParseToJavaTypes.java, třída DoubleTypeParser. Tato chyba může nastat " +
                                    "například v případě, kdy zadaný řetězec (text) nebosahuje hodnotu, kterou je " +
                                    "možné převést na datový typ Double.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
				
				return null;
			}
		}		
	}
}