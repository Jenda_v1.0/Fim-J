package cz.uhk.fim.fimj.commands_editor.values_completion_window;

import javax.swing.*;

/**
 * Vlákno, které slouží pro aktualizaci hodnot v okně pro automatické doplnění / dokončení příkazů / hodnot zadávaných v
 * editoru příkazů.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 28.07.2018 15:28
 */

public class UpdateValueCompletion extends Thread {

    private final JTextArea textArea;

    public UpdateValueCompletion(final JTextArea textArea) {
        this.textArea = textArea;
    }

    @Override
    public void run() {
        ValuesCompletion.setAutoCompletion(textArea);
    }
}
