package cz.uhk.fim.fimj.commands_editor;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.*;
import javax.swing.text.*;

import cz.uhk.fim.fimj.code_editor_abstract.RSyntaxTextAreaKeyActionHelper;
import cz.uhk.fim.fimj.commands_editor.history.HistoryCompletion;
import cz.uhk.fim.fimj.commands_editor.history.complete_history.CompleteHistoryCompletion;
import cz.uhk.fim.fimj.commands_editor.history.project_history.ProjectHistoryCompletion;
import cz.uhk.fim.fimj.commands_editor.values_completion_window.FieldValueSyntaxCreator;
import cz.uhk.fim.fimj.commands_editor.values_completion_window.UpdateValueCompletion;
import cz.uhk.fim.fimj.commands_editor.values_completion_window.ValuesCompletion;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.clear_editor.ClearInterface;
import cz.uhk.fim.fimj.clear_editor.PopupMenu;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.output_editor.OutputEditor;

/**
 * Třída slouží jako editor kódu rozpoznává zadané příkazy pomocí regulárních výrazů a dále je zpracovává - vytváří
 * instance, apod. - další práce s ními
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class Editor extends RSyntaxTextArea implements LanguageInterface, ClearInterface {

    private static final long serialVersionUID = -1651243875549240192L;
	
	
	/**
	 * Tato proměnná obsahuje výchozí symbol, která se využije při prvním vytvoření
	 * tohoto editoru nebo při jeho čištění.
	 */
	private static final String DEFAULT_PROMPT = "> ";
	
	
	
	
	/**
	 * Třída pro otestování regulárních výrazů:
	 */
	private static final RegularExpressionsInterface regEx = new RegularExpressions();


    /**
	 * Třída pro vykonávání operací v tomto editoru příkazů
	 */
	private static MakeOperationInterface makeOperation;
	
	
	
	/**
	 * List, do kterého ukládám veškeré pozvrzené příkazy (slouží pro historii
	 * uživatelem zadaných příkazů).
	 */
	private static final List<String> commandEnteredList = new ArrayList<>();
	
	
	/**
	 * Index, pro zobrazení příkaů po stisknutí šípky nahoru / dolů
	 */
	private int commandIndex;
	
	
	
	/**
	 * Proměnná pro referenci na outputEditor, do kterého bude vypisovat hlášky o
	 * výsledku zadaného příkazu - v tomto případě - dále kompilace ale to je jiná
	 * část App
	 */
	private final OutputEditor outputEditor;
			
	
	
	/**
	 * Proměnná pro výpis oznámení v editoru výstupů, že příkaz nebyl rozpoznán:
	 */
	private static String txtCommandIsNotRecognized;
	
	
	
	
	/**
	 * Objekt, který obsahuje texty pro tuto aplikaci v uživatelem zvoleném jazyce.
	 */
	private final Properties languageProperties;


    /**
     * Vlákno, které slouží pro aktualizaci hodnot do okna pro doplňování kódu / příkazů do editoru příkazů.
     * <p>
     * Toto vlákno se spustí například při kompilaci tříd nebo při zavolání metody, naplnění proměnné apod. Prostě vždy,
     * kdy je potřeba přenačíst hodnoty v tom okně, aby byly aktuální, například, když se doplnily nějaké metody,
     * proměnné apod. - aby byly na výběr.
     */
    private transient UpdateValueCompletion updateValueCompletion;


    /**
     * Hodnota, která značí, zda se vůbec má po stisknutí kláves Ctrl + Space zobrazit okno s hodnotami pro automatické
     * doplnění / dokončení příkazů v editoru příkazů. Tedy, když uživatel stiskne klávesovou zkratku Ctrl + Space, tak
     * zda se to okno s možnými příkazy zobrazí nebo ne.
     */
    private static boolean showAutoCompleteWindow;

    /**
     * Zda se mají zobrazit referenční proměnné u hodnot, které jsou jsou uvedeny v okně pro automatické doplňování /
     * dokončování příkazů v editoru příkazů.
     * <p>
     * (Vždy v tom druhém okně, které obsahuje informace o zvolené položce.)
     */
    private static boolean showReferenceVariablesInCompleteWindow;

    /**
     * Zda se má do editoru příkazů přidat syntaxe pro naplnění proměnné. Jedná se o přidání syntaxe pro vytvoření
     * proměnné v editoru příkazů, do které se předá hodnota z proměnné v instanci, která je takového datového typu,
     * který je podporován v aplikaci (editoru příkazů). Jedná se o primitivní a objektové číselné datové typy, text,
     * znak, logická hodnota, pole a list uvedených datových typů. Pro list platí pouze naplnění existujícího listu,
     * který uživatel dříve vytvořil v editoru. Z proměnné nelze naplnit.
     * <p>
     * To samé pro naplnění již existující proměnné vytvořené v editoru příkazů, která není final.
     */
    private static boolean addSyntaxForFillingTheVariable;

    /**
     * Zda se má při vytváření syntaxe pro deklaraci proměnné s naplněním hodnoty z instance přidat k proměnné klíčové
     * slovo "final".
     */
    private static boolean addFinalWordForFillingTheVariable;

    /**
     * Zda se mají v okně pro automatické doplňování / dokončování zobrazovat připravené ukázky základních syntaxí
     * příkzů, které jsou podporovány v editoru příkazů.
     * <p>
     * Tato proměnná by ani nemusela být potřeba a mohli by být jako výchozí (definované) nastavení, ale pro případ, že
     * by se uživateli nějaké metody "pletly" nebo by toho bylo zobrazeno moc apod. Tak aby se mu zobrazovaly pouze ty
     * "důležité" věci ohledně uživatelova projektu. Tedy pouze metody, třídy a proměnné týkající se otevřeného
     * projektu.
     */
    private static boolean showExamplesOfSyntax;

    /**
     * Zda se v okně pro automatické doplňování / dokončování příkazů mají v editoru příkazů maji zobrazovat výchozí
     * definované metody. Tj. například metody pro vypsání příkazů, vypsání proměnných, restart a ukončení aplikace
     * apod.
     */
    private static boolean showDefaultMethods;

    /**
     * Zda se má zpřístupnit / ukládat historie zadaných příkazů v rámci otevřeného projektu pro zobrazení a doplnění v
     * okně.
     */
    private static boolean showProjectHistoryWindow;
    /**
     * Zda se má zpřístupnit / ukládat historie zadaných příkazů od spuštění aplikace.
     */
    private static boolean showCompleteHistoryWindow;


    /**
     * Historie uživatelem zadaných příkazů, které zadal od spuštění po ukončení aplikace.
     */
    private transient HistoryCompletion completeHistoryCompletion;

    /**
     * Historie uživatelem zadaných příkazů, které zadal v rámci otevřeného projektu. Při změně nebo uzavření projektu
     * bude vymazána.
     */
    private transient ProjectHistoryCompletion projectHistoryCompletion;


	public Editor(final OutputEditor outputEditor, final GraphInstance instanceDiagram,
				  final Properties languageProperties, final App app) {

		this.outputEditor = outputEditor;
		this.languageProperties = languageProperties;

        makeOperation = new MakeOperation(regEx, outputEditor, instanceDiagram, languageProperties, app);
		
		
		// Nastavení typ jazyka pro zvýrazňování kódu a povolení zvýrazňování:
		setCodeFoldingEnabled(true);
		setAntiAliasingEnabled(true);

		
		
		
		// Na začátku vypíše "větší" - aby uživatel věděl, kde se může zadávat kód
		// Za tímto znakem se může psát kód, to co je nad tím nelze editovat
		setText(DEFAULT_PROMPT);
		
		
		
		// Pokaždé, když uživatel stiskně Enter, tak se "provede příkaz"
		// a nad řádkem, kde byl stisknut Enter nepůjde již nic editovat:
		((AbstractDocument) getDocument()).setDocumentFilter(new NonEditableLineDocumentFilter());


		
		addKeyListener();
		

		
		addMouseWheelListener();
		
		
		
		setSettingsEditor();
		
		
		
		addMouseListener();
		
		
		
		setLanguage(languageProperties);




        /*
         * Zde je třeba "vypnout" klávesu Enter, ale v tom smyslu, aby nezadávalo nový řádek v následujícím příkaze
         * je možnost si vypsat, že je hodnota "insert-break" jako výchozí pro klávesu Enter, což značí, že se po
         * stisknutí má zadat nový řádek.
         *
         * Ale toto je možné "vypnout", pokud zadáme následující příkaz, čímž nastavíme událost, která se má stát po
         * stisknutí klávesy Enter. Když ji "nenastavíme nic", tak nepřidá nový řádek, ale samotná klávesa Enter bude
         * reagovat, například pro potvrzení přidání hodnoty v okně pro doplňování příkazů do editoru příkazů (Ctrl
         * + Space) apod. Pouze se nepřidá nový řádek.
         *
         * (příkaz pro získání události: getInputMap().get(KeyStroke.getKeyStroke("ENTER"));)
         *
         * (Také by na konec šlo míto prázdných uvozovek zadat napříkad číslo nula apod.)
         *
         *
         * Zdroj:
         *
         * https://docs.oracle.com/javase/tutorial/uiswing/misc/keybinding.html
         *
         * https://stackoverflow.com/questions/2162170/jtextarea-new-line-on-shift-enter
         *
         * https://stackoverflow.com/questions/19569302/jtextarea-pressing-enter-adds-unnecessary-new-line
         */
        getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "");

        // Aby nešel zadat nový řádek ani na Shift + Enter:
//        getInputMap().put(KeyStroke.getKeyStroke("shift ENTER"), "");





        /*
         *
         */
        // Nastavení komponenty, u které se mají zapínat / vypínat klávesové zkratky.
        RSyntaxTextAreaKeyActionHelper.setSyntaxTextArea(this);

        /*
         * Vypnutí klávesových zkratek pro editor příkazů. Ale je třeba je zapnout v případě, že uživatel otevře
         * jeden z editorů zdrojového kódu, aby v nich byly zkratky funkční / povoleny.
         *
         * Jedná se o zkratky Ctrl + Z (Zpět), Ctrl + Y (Vpřed) a Ctrl + Enter (Zvuk).
         *
         *
         * Ctrl + Enter:
         *
         * Vypnutí spuštění zvuku po stisknutí klávesové zkratky Ctrl + Enter.
         *
         * Zdroj:
         *
         * https://stackoverflow.com/questions/13427174/disable-beep-when-backspace-is-pressed-in-an-empty-jtextfield
         *
         * Na výše uvedeném zdroji jsou informace pro {@link JTextArea}, ale pro {@link RSyntaxTextArea} to bylo
         * potřeba provést trochu jinak.
         *
         * Bylo třeba projít veškeré předky až k mapě s akcemi pro tu {@link RSyntaxTextArea}, tu postupně celou
         * projít a jednotlivé vypínat příslušné akce až dokud jsem nenašel tu, která vypíná ten zvuk ("RTA
         * .DumbCompleteWordAction"). Tu stačí vypnout.
         *
         * (Jedná se o akci spojenou právě s touto klávesovou zkratkou - Ctrl + Enter.)
         *
         *
         * Ctrl + Z:
         *
         * Vypnutí / Zrušení funkce "Zpět".
         *
         * Když uživatel stiskne klávesovou zkratku Ctrl + Z, nic se nestane. Je to z toho důvodu, že když by byla
         * tato funkce povolena, bylo by pomocí této funcke možné odebrat většítko ('>') na začátku každého řádku. V
         * takovém případě by ale nešlo do editoru psát. To je povoleno až za většítkem.
         *
         *
         * Ctrl + Y:
         *
         * Vypnutí / Zrušení funkce "Vpřed" - Redo.
         *
         * V případě, že uživatel stiskne klávesovou zkratku Ctrl + Y, nic se nestane. Tuto funkci není nezbytné
         * zakázat, ale bez Ctrl + Z nemá smysl. Jelikož se vypla výše akce pro Ctrl + Z, tak funkce pro Ctrl + Y
         * nebude fungovat, protože nebude možné žádné kroky odebrat, tudíž nebudou k dispozici odebrané kroky pomocí
         * Ctrl + Z, aby se mohly pomocí Ctrl + Y opět vrátit.
         */
        RSyntaxTextAreaKeyActionHelper.enableSpecificAction(false);
    }







    /**
     * Metoda pro spuštění vlákna, které aktualizuje hodnoty v okně pro automatické doplňování kódu do editoru příkazů.
     * <p>
     * Otestuje se, zda je vlákno null, pokud ano, pak se vytvoří a spustí. To samé, pokud vlákno neběží.
     * <p>
     * Pokud vlákno běží, tak jej nechám doběhnout. Zde by byla alternativa jej v tomto případě ukončit a spustit znovu,
     * ale takto rychle lze kompilovat třídy pouze v případě, že uživatel opakované kliká na třídu v diagramu tříd
     * pravým tlačítkem myši, ale k žádné změně nějakých hodnot, které by se měli projevit v tom okně pro doplňování
     * hodnot do editoru nedojde. Proto není třeba vlákno ukončit, až doběhne, budou načteny aktuální hodnoty.
     */
    public void launchThreadForUpdateValueCompletion() {
        // Pokud se nemá zobrazovat okno s příkazy, není třeba pokračovat:
        if (!showAutoCompleteWindow) {
            // Zde je třeba okno "odinstalovat", jinak se bude pořád zobrazovat.
            ValuesCompletion.uninstallAutoCompletion();
            return;
        }

        if (updateValueCompletion == null || !updateValueCompletion.isAlive()) {
            updateValueCompletion = new UpdateValueCompletion(this);
            updateValueCompletion.start();
        }
    }



	
	
	
	
	
	/**
	 * Metoda, která této třídě - editoru přidá reakci na myš konkrétně na rolování
	 * kolečkem myši, pokud bude stisknuta klávesa CTRL pak se bude zvětšovat /
	 * změnšovat písmo editoru
	 */
	private void addMouseWheelListener() {
		addMouseWheelListener(e -> {

			if (e.getWheelRotation() < 0 && e.isControlDown()) { // Rotace kolečkem myši nahoru - zvětšování písma
				float fontSize = getFont().getSize();

				if (fontSize < 100)
					setFont(getFont().deriveFont(++fontSize));
			}

			else if (e.getWheelRotation() > 0 && e.isControlDown()) { // Rotování kolečkem myši dolů - zmenšování písma
				float fontSize = getFont().getSize();

				if (fontSize > 8)
					setFont(getFont().deriveFont(--fontSize));
			}
		});
	}
	
	

	
	
	
	
	/**
	 * Metoda, která přidá událost na uvolěné pravého tlačítko myší, že se otevře
	 * kontetové menu, kde bude položka 'Vymazat', která vymaže tento editor
	 * příkazů, resp. vymaže veškeré již zadané příkazy.
	 */
	private void addMouseListener() {
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (isEnabled() && SwingUtilities.isRightMouseButton(e))
					PopupMenu.createPopupMenu(languageProperties, Editor.this, Editor.this, e);
			}
		});
	}
	
	
	

	
	

	@Override
	public void clearEditor() {
		// Povolením editace v editoru:
		((AbstractDocument) getDocument()).setDocumentFilter(null);

		// Nastavím výchozí text:
		setText(DEFAULT_PROMPT);
		
		// Vymažu veškerou zadanou historii příkazů:
		commandEnteredList.clear();

		// Zakáže se editace již odentrovaného řádku.
		((AbstractDocument) getDocument()).setDocumentFilter(new NonEditableLineDocumentFilter());

        // Vymaže se historie příkazů v okně pro doplňování:
        if (projectHistoryCompletion != null)
            projectHistoryCompletion.clearHistory();
    }
	
	


	
	
	
	/**
	 * Metoda, která si přečte příslušný soubor, pokud existuje pokud soubor
	 * neexistuje, použije se výchozí nastavení
	 * 
	 * Soubour CommandEditor.properties si přečte výchozí nastavení pro tento editor
	 * metoda je volána při startu aplikace nebo při nastavení změn v editoru -
	 * třeba z dialogu pro nastavení
	 */
	public final void setSettingsEditor() {
		final Properties commandEditorProp = App.READ_FILE.getCommandEditorProperties();

        final boolean indexCommandsInProjectHistoryWindow;
        final boolean indexCommandsInCompleteHistoryWindow;

		if (commandEditorProp != null) {
			// Zde se padařilo načíst soubor bud s výchozím nastavením nebo ve workspace,
			// Tak nastavím hodnoty do editoru v app:
			final int fontStyle = Integer.parseInt(commandEditorProp.getProperty("FontStyle", Integer.toString(Constants.DEFAULT_FONT_FOR_COMMAND_EDITOR.getStyle())));
			final int fontSize = Integer.parseInt(commandEditorProp.getProperty("FontSize", Integer.toString(Constants.DEFAULT_FONT_FOR_COMMAND_EDITOR.getSize())));
			setFont(new Font(Constants.DEFAULT_FONT_FOR_COMMAND_EDITOR.getName(), fontStyle, fontSize));
			
			
			final String bgColor = commandEditorProp.getProperty("BackgroundColor", Integer.toString(Constants.COMMAND_EDITOR_BACKGROUND_COLOR.getRGB()));
			setBackground(new Color(Integer.parseInt(bgColor)));
			
			
			final String foregroundColor = commandEditorProp.getProperty("ForegroundColor", Integer.toString(Constants.COMMAND_EDITOR_FOREGROUND_COLOR.getRGB()));
			setForeground(new Color(Integer.parseInt(foregroundColor)));
			
			
			final boolean highlightingJavaCode = Boolean.parseBoolean(commandEditorProp.getProperty("HighlightingJavaCode", String.valueOf(Constants.COMMAND_EDITOR_HIGHLIGHTING_CODE)));
			if (highlightingJavaCode)
				setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
			// Zde se nebude zvýrazňovat kód - syntaxe Javy.
			else
				setSyntaxEditingStyle(null);



            /*
             * Následuje nastavení hodnot pro okno pro automatické doplňování / dokončování příkazů v editoru příkazů
             * (Ctrl + Space).
             */

            showAutoCompleteWindow = Boolean.parseBoolean(commandEditorProp.getProperty("ShowAutoCompleteWindow",
                    String.valueOf(Constants.CE_SHOW_AUTO_COMPLETE_WINDOW)));

            showReferenceVariablesInCompleteWindow = Boolean.parseBoolean(commandEditorProp.getProperty(
                    "ShowReferenceVariablesInCompleteWindow",
                    String.valueOf(Constants.CE_SHOW_REFERENCE_VARIABLES_IN_COMPLETE_WINDOW)));

            addSyntaxForFillingTheVariable = Boolean.parseBoolean(commandEditorProp.getProperty(
                    "AddSyntaxForFillingTheVariable",
                    String.valueOf(Constants.CE_ADD_SYNTAX_FOR_FILLING_THE_VARIABLE)));

            addFinalWordForFillingTheVariable = Boolean.parseBoolean(commandEditorProp.getProperty(
                    "AddFinalWordForFillingTheVariable",
                    String.valueOf(Constants.CD_ADD_FINAL_WORD_FOR_FILLING_THE_VARIABLE)));

            showExamplesOfSyntax = Boolean.parseBoolean(commandEditorProp.getProperty("ShowExamplesOfSyntax",
                    String.valueOf(Constants.CE_SHOW_EXAMPLES_OF_SYNTAX)));

            showDefaultMethods = Boolean.parseBoolean(commandEditorProp.getProperty("ShowDefaultMethods",
                    String.valueOf(Constants.CE_SHOW_DEFAULT_METHODS)));


            // Hodnoty pro historii příkazů v rámci otevřeného projektu:
            showProjectHistoryWindow = Boolean.parseBoolean(commandEditorProp.getProperty("ShowProjectHistoryWindow",
                    String.valueOf(Constants.CE_SHOW_PROJECT_HISTORY_WINDOW)));
            indexCommandsInProjectHistoryWindow = Boolean.parseBoolean(commandEditorProp.getProperty(
                    "IndexCommandsInProjectHistoryWindow",
                    String.valueOf(Constants.CE_INDEX_COMMANDS_IN_PROJECT_HISTORY_WINDOW)));

            // Hodnoty pro histirii příkazů od spuštění aplikace:
            showCompleteHistoryWindow = Boolean.parseBoolean(commandEditorProp.getProperty("ShowCompleteHistoryWindow"
                    , String.valueOf(Constants.CD_SHOW_COMPLETE_HISTORY_WINDOW)));
            indexCommandsInCompleteHistoryWindow = Boolean.parseBoolean(commandEditorProp.getProperty(
                    "IndexCommandsInCompleteHistoryWindow",
                    String.valueOf(Constants.CD_INDEX_COMMANDS_IN_COMPLETE_HISTORY_WINDOW)));
        }
		
		
		else {
			// Zde použiji výchozí nastavení:
			setFont(Constants.DEFAULT_FONT_FOR_COMMAND_EDITOR);
			
			setBackground(Constants.COMMAND_EDITOR_BACKGROUND_COLOR);
			
			setForeground(Constants.COMMAND_EDITOR_FOREGROUND_COLOR);

			if (Constants.COMMAND_EDITOR_HIGHLIGHTING_CODE)
				setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);

			else
				setSyntaxEditingStyle(null);



            /*
             * Následuje nastavení hodnot pro okno pro automatické doplňování / dokončování příkazů v editoru příkazů
             * (Ctrl + Space).
             */

            showAutoCompleteWindow = Constants.CE_SHOW_AUTO_COMPLETE_WINDOW;

            showReferenceVariablesInCompleteWindow = Constants.CE_SHOW_REFERENCE_VARIABLES_IN_COMPLETE_WINDOW;

            addSyntaxForFillingTheVariable = Constants.CE_ADD_SYNTAX_FOR_FILLING_THE_VARIABLE;

            addFinalWordForFillingTheVariable = Constants.CD_ADD_FINAL_WORD_FOR_FILLING_THE_VARIABLE;

            showExamplesOfSyntax = Constants.CE_SHOW_EXAMPLES_OF_SYNTAX;

            showDefaultMethods = Constants.CE_SHOW_DEFAULT_METHODS;

            // Hodnoty pro historii příkazů v rámci otevřeného projektu:
            showProjectHistoryWindow = Constants.CE_SHOW_PROJECT_HISTORY_WINDOW;
            indexCommandsInProjectHistoryWindow = Constants.CE_INDEX_COMMANDS_IN_PROJECT_HISTORY_WINDOW;

            // Hodnoty pro histirii příkazů od spuštění aplikace:
            showCompleteHistoryWindow = Constants.CD_SHOW_COMPLETE_HISTORY_WINDOW;
            indexCommandsInCompleteHistoryWindow = Constants.CD_INDEX_COMMANDS_IN_COMPLETE_HISTORY_WINDOW;
        }


        /*
         * Výše se mohlo změnit nějaké nastavení pro okno s doplňováním příkazů, tak je zde pro jistotu aktualizuji
         * (pokud je zobrazení okna povoleno):
         */
        launchThreadForUpdateValueCompletion();

        // Zda se má zaznamenat historie příkazů v rámci otevřeného projektu:
        if (showProjectHistoryWindow)
            projectHistoryCompletion = new ProjectHistoryCompletion(indexCommandsInProjectHistoryWindow);

        // Zda se má zaznamenávat historie příkazů od spuštění aplikace:
        if (showCompleteHistoryWindow)
            completeHistoryCompletion = new CompleteHistoryCompletion(indexCommandsInCompleteHistoryWindow);
    }
	
	
	
	
	
	
	
	/**
	 * Metoda, která přidá editoru reakce na klávesy
	 */
	private void addKeyListener() {
		addKeyListener(new KeyAdapter() {

			@Override
            public void keyPressed(KeyEvent e) {
                // Klávesová zkratka pro otevření okna s dostupnými příkazy:
                if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_SPACE && showAutoCompleteWindow) {
                    ValuesCompletion.installAutoCompletion(Editor.this);
                }

                // Klávesová zkratka pro otevření okna s historií veškerých zadaných příkazů:
                else if (e.isControlDown() && e.isShiftDown() && e.getKeyCode() == KeyEvent.VK_H && completeHistoryCompletion != null) {
                    completeHistoryCompletion.installAutoCompletion(Editor.this);
                }

                // Klávesová zkratka pro otevření okna s historií příkazů zadaných k aktuálně otevřenému projektu.
                else if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_H && projectHistoryCompletion != null) {
                    projectHistoryCompletion.installAutoCompletion(Editor.this);
                }


                // Potvrzení zadaného příkazu:
                else if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_ENTER) {
					// Zjistím si řádek na kterém uživatel "pracuje"
					// Vezmu si z něj příkaz, který zadal
					// a otestuji ho na známé - mnou vytvořené příkazy:
					
					try {
						final int offset = getCaretLineNumber();
						final int start = getLineStartOffset(offset);
						final int end = getLineEndOffset(offset);
						

						String line = getText(start, (end - start));

                        /*
                         * (nikdy by nemělo být null)
                         *
                         * Pro případ, že by uživatel byl schopen odebrat většítko ('>') na začátku řádku (němělo by
                         * nastat), tak je třeba otestovat, zda li je na řádku alesopň jeden znak. (Za účelem
                         * zabránění výjimky: "java.lang.StringIndexOutOfBoundsException: String index out of range:
                         * 0".)
                         *
                         * Pokud jsou podmínky splněny a na řádku se nachází alespoň jeden znak, tak je vhodné se
                         * ujistit, že se jedná o většítko a odebrat jej. Tím se na řádku bude nacházet pouze příkaz
                         * od uživatele.
                         */
                        if (line != null && line.length() > 0 && line.charAt(0) == '>') {
                            line = line.substring(1);
                        }


                        /*
                         * Otestuji, zda se nejedná o "prázdný příkaz", tj. zda nejsou zadány pouze bílé
                         * znaky, pokud ano, pak zde mohu skončit, nebudu nic vypisovat, například, že
                         * nebyl rozpoznán výraz apod. Proč, uživatel snad musí vědět, že potvrdil příkaz a
                         * nic nezadal.
                         */
                        if (!regEx.isWhiteSpace(line)) {
                            // Otestuji zadaný příkaz:
                            testRegularExpressions(line);

                            /*
                             * Zde musím přidat nový řádek, protože při stisknutí kláves Ctrl + Enter se automaticky
                             * nepřidá nový řádek.
                             *
                             * V tomto případě se výše otestovala zadaná syntaxe / příkaz, tak zde přidám nový řádek a
                             * nastavím na konecnového řádku kurzor.
                             */
                            append("\n");

                            /*
                             * Při stisknutí enteru se "potvrdí" příkaz na aktuálním řádku kde je kurzor a posune se
                             * kurzor na konec dokumentu:
                             */
                            setCaretPosition(getDocument().getLength());
                        }

					} catch (BadLocationException e1) {
						/*
						 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
						 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
						 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
						 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
						 */
						final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

						// Otestuji, zda se podařilo načíst logger:
						if (logger != null) {
							// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

							// Nejprve mou informaci o chybě:
							logger.log(Level.INFO,
									"Zachycena výjimka při získávání pozice kurzoru při stisknutí klávesy Enter - " +
											"historie příkazů, pro editor příkazů.");

							/*
							 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
							 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
							 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
							 */
							logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e1));
						}
						/*
						 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
						 */
						ExceptionLogger.closeFileHandler();
					}
				}
				
				
				
				
				else if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_UP) {
					// Odeberu text na posledním řádku:
					final Document document = getDocument();
					final Element rootElem = document.getDefaultRootElement();
					final int numLines = rootElem.getElementCount();
					final Element lineElem = rootElem.getElement(numLines - 1);
					final int lineStart = lineElem.getStartOffset();
					final int lineEnd = lineElem.getEndOffset();
					
					
					try {
						// Odeberu poslední příkaz dle jednotlivých indexů daného příkazuu:
						final String lineText = document.getText(lineStart, lineEnd - lineStart);
						
						for (int i = 0; i < lineText.length(); i++)
							document.remove(document.getLength() - 1, 1);
						
					} catch (BadLocationException e2) {
                        /*
                         * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                         * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                         * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                         * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                         */
                        final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                        // Otestuji, zda se podařilo načíst logger:
                        if (logger != null) {
                            // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                            // Nejprve mou informaci o chybě:
                            logger.log(Level.INFO,
                                    "Zachycena výjimka při špatné lokaci, třída Editor - commandEditor, událost při " +
                                            "sisknutí šíkpy nahoru.");

                            /*
                             * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                             * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                             * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                             */
                            logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e2));
                        }
                        /*
                         * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                         */
                        ExceptionLogger.closeFileHandler();
                    }
					
					
					// Pokud jsem došel na nulu, jedá se o první prvek v poli, tak posunu index na
					// poslední prvek v poli:
					if (--commandIndex < 0)
						commandIndex = commandEnteredList.size() - 1;
					
					
					
					// a vložím tam jeden ze zadaných příkazů:
					// Nejprve musím zjistit, zda byl již nějaký příkaz zadán:
					if (!commandEnteredList.isEmpty())
						append(commandEnteredList.get(commandIndex));
					
					
					// Vložím kurzor na poslední řádek:
					setCaretPosition(getDocument().getLength());
				}
				
				
				
				
				else if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_DOWN) {
					// Odeberu text na posledním řádku:
					final Document document = getDocument();
					final Element rootElem = document.getDefaultRootElement();
					final int numLines = rootElem.getElementCount();
					final Element lineElem = rootElem.getElement(numLines - 1);
					final int lineStart = lineElem.getStartOffset();
					final int lineEnd = lineElem.getEndOffset();
					
					
					try {
						final String lineText = document.getText(lineStart, lineEnd - lineStart);
						
						for (int i = 0; i < lineText.length(); i++)
							document.remove(document.getLength() - 1, 1);

					} catch (BadLocationException e2) {
                        /*
                         * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                         * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                         * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                         * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                         */
                        final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                        // Otestuji, zda se podařilo načíst logger:
                        if (logger != null) {
                            // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                            // Nejprve mou informaci o chybě:
                            logger.log(Level.INFO,
                                    "Zachycena výjimka při špatné lokaci, třída Editor - commandEditor, událost při " +
                                            "sisknutí šíkpy dolů!");

                            /*
                             * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                             * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                             * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                             */
                            logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e2));
                        }
                        /*
                         * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                         */
                        ExceptionLogger.closeFileHandler();
                    }
					
					
					
					
					if (++commandIndex >= commandEnteredList.size())
						commandIndex = 0;
					
					
					// a vložím tam jeden ze zadaných příkazů:
					if (!commandEnteredList.isEmpty())
						append(commandEnteredList.get(commandIndex));
					
					
					// Vložím kurzor na poslední řádek:
					setCaretPosition(getDocument().getLength());
				}
				

				
				
				/*
				 * Kombinace kláves, která slouží pro vymazání obsahuje editoru příkazů a zadané
				 * historie.
				 */
				else if (e.getKeyCode() == KeyEvent.VK_L && e.isControlDown())
					clearEditor();
			}
        });
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje zadaný příkaz na regulární výrazy, abych zjistil zda
	 * uživatelem zadaný příkaz je ve správné syntaxi pro Javovský příkaz např
	 * vytvoření instance, naplnení proměnné, apod ("Zda uživatel nezadal kravinu")
	 * 
	 * Metoda, nejprve zadaný příkaz otestuje na regulární výrazy, pokud nějaký
	 * najde, bude příkaz dále rozebrán na jednotlivé "složky", abych zjistil
	 * například do jaké proměnné mám naplnit jakou hodnotu, apod. pokud nebude
	 * příkaz rozpznán, vypíše se chabová hláška
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz do editoru
	 */
	private void testRegularExpressions(final String command) {
		// Nemělo by nastat:
		if (command == null)
			return;


        // Uložení výsledku operace / provedení příkazu:
        final String result;

		/*
		 * Note:
		 * 
		 * Původne zde byly i metody pro početní operace, jejíž výsledkem se mohla
		 * naplnit například nějaká proměnné apod. Teď už jsem importoval matematickou
		 * knihovnu EvalEx, a díky příslušným regulárním výrazům se pletou výrazy pro
		 * rozpoznání "klasické" matematické operace, a operace pro výpočet různých
		 * hodnot, například součet výsledku zavolání metody a nějaké proměnné z
		 * instance třídy a číslo (konstanta) apod.
		 * 
		 * Proto jsem tyto možnosti odebral a nechal pouze ty matematicé operace, takže
		 * už nelze například naplnit proměnnou výsledkem zavolání metody a sečíst s
		 * hodnotou nějaké proměnné apod.
		 * 
		 * Použité metody jsem nechal implementované, pouze jsem zakomentoval jejich
		 * testování v této metodě (pro případ, že bych já nebo někdo jiný v tomto
		 * pokračoval, a přišel na lepší způsob než přes regulání výrazy, nebo je napsal
		 * tak, aby bylo možné příslušné operace rozlišit, také dobrý dodate, že ani ty
		 * metody na sčítání výsledku metody aproměnné apod. nejsou dokonalé, vždy se
		 * rozpozná pouze jedna operace a ta se aplikuji, takže to je také třeba
		 * upravit, ale jak je uvedeno výše, nechám jej implementované pro potenciální
		 * vylepšení nebo poučení se z chyb apod..).
		 * 
		 * Jedná se o metody:
		 * 
		 * isDeclarationAndFulfillmentEditorVariableConstant se plete s metodou
		 * isDeclarationAndFulfillmentEditorVariableMoreValue
		 * 
		 * 
		 * Dále metody:
		 * 
		 * isFulfillmentOfEditorVariable a isFulfillmentOfClassVariable a
		 * isFulfillmentOfArrayAtIndex.
		 * 
		 * Tyto metody metody se nepletou spolu ale vždy s metodou pro stejný účel, tj.
		 * metoda isFulfillmentOfEditorVariable si nesedí kvůli regulárním výrazům s
		 * metodou isFullfillmentOfEditorVariableConstant, která umožňuje kontrolu s
		 * matamtickými výpočty.
		 * 
		 * Stejně tak jsou na tom metody isFulfillmentOfClassVariable, která se může
		 * splést s metodou isFullfillmentOfVariablesNumber. A na konec metoda
		 * isFulfillmentOfArrayAtIndex, která se může splést s metodou
		 * isFullfillmentValueAtIndexOfArrayConstant, vše kvůli regulárním výrazům.
		 */
		
		
		
		
		/*
		 * Dále v případě zavolání metody, zavolání statické metody, získání hodnoty z
		 * jednorozměrného nebo dvojrozměrného pole v instanci třídy se otestuje získaná
		 * / návratová hodnota a pokud je tato návratová hodnota instance nějaké třídy z
		 * diagramu tříd a tato instance se nachází v diagramu instancí, tak se za tu
		 * instanci vypíše ještě referenční proměnná - reference na tu instanci v
		 * diagramu instnací.
		 * 
		 * Je to možné, protože se příslušná návratová / získaná hodnota nikam dále
		 * nepředává, například se jí nenaplnění nějaká proměnná nebo se nepředává do
		 * parametru metody adpo.
		 *
		 * 
		 * Jedná se o metody u reguláních výrazů níže:
		 * isPrintValueLiteral
		 * isClassVariable
		 * isCallTheMethod
		 * isCallStaticMethod
		 * isGetValueFromArrayAtIndexInInstance
		 * isGetValueAtIndexAtTwoDimArrayInInstance
		 */
		
		
		

		// Ukončení apliakce příkazem: exit();
		if (regEx.isCloseApplicationByExit(command)) {
			result = makeOperation.closeAppByExitCommand();
		}
		
		// Ukončení apliakce příkazem: exit(0 - 59);
		else if (regEx.isCloseApplicationByExitWithCount(command)) {
			result = makeOperation.closeAppByExitWithCountCommand(command);
		}
				
		// Ukončení aplikace příkazem: System.exit(0);
		else if (regEx.isCloseApplicationBySystemExit(command)) {
			result = makeOperation.closeAppBySystemExitCommand();
		}
		
		else if (regEx.isRestartApplication(command)) {
			result = makeOperation.restartAppByRestartCommand();
		}
		
		else if (regEx.isRestartApplicationWithCount(command)) {
			result = makeOperation.restartAppWithCountByRestartCommand(command);
		}
		
		// Ukončení odpočtu do restartu nebo ukončení aplikace:
		else if (regEx.isCancelCountdown(command)) {
			result = makeOperation.cancelCountdown();
		}
		
		
		
		
		
		// Příkaz pro výpis příkazů, které umí editrou příkazů rozpoznat
		else if (regEx.isPrintCommands(command)) {
			result = makeOperation.writeCommands();
		}
		
		// Příkaz pro vypsání všech vytvořených proměnných v editoru příkazů:
		else if (regEx.isPrintTempVariables(command)) {
			result = makeOperation.printTempVariables();
		}
		
		
		// Příkazy pro výpis nějaké hodnoty do editoru příkazů
		else if (regEx.isPrintValueLiteral(command)) {
			result = makeOperation.makePrintValueLiteral(command);
		}
		
		
		
		
		
		
		
		
		
		
		
		/*
		 * Získání hodnoty z proměnné v instanci třídy.
		 * 
		 * Syntaxe: referenceName.variableName;
		 */
		else if (regEx.isClassVariable(command)) {
			result = makeOperation.makeGetValueFromVarInInstance(command);
		}
		
		
		
		
		
		
		
		
		
		
		/*
		 * Pouze vytvoření instance, ale bez reference, pouze se zavolá jeden z
		 * konstruktorů zadané třídy a to je v podstatě vše, příslušná instance se nikam
		 * neuloží apod.
		 * 
		 * V Podstatě se pouze vykoná konstruktor a to je vše.
		 */
		else if (regEx.isCallConstructor(command)) {
			result = makeOperation.callConstructor(command);
		}
		
		
		// Vytvoření instance s referencí:
		else if (regEx.isNewInstanceCommand(command)) {
			result = makeOperation.makeInstance(command);
		}

		
		
		/*
		 * U tohoto "pouhého" zavolání metody (bez naplnění proměnné či předání do
		 * parametru apod. se u výpisu otestuje, zda je to instance nějaké třídy z
		 * diagramu tříd a ta instance je v diagramu instnací, pokud ano, pak se do
		 * výpisu přidá název reference.
		 */
		else if (regEx.isCallTheMethod(command)) {
			result = makeOperation.makeCallTheMethod(command);
		}


        /*
         * Zavolání statické metody nad třídou v diagramu tříd nebo nad její vnitřní třídou, ne nad instancí třídy:
         *
         * syntaxe:
         * packageName.(othersPackageName.nextPackage.)ClassName.methodName(parameters);
         *
         * packageName.(othersPackageName.nextPackage.)ClassName.InnerClassName.methodName(parameters);
         *
         * (parametry v metodě mohou a nemusí být, zde nejsou povinné, to se řeší až
         * dál.)
         *
         * U tohoto "pouhého" zavální metody (bez naplnění proměnné či předání do
         * parametru apod. se u výpisu otestuje, zda je to instance nějaké třídy z
         * diagramu tříd a ta instance je v diagramu instnací, pokud ano, pak se do
         * výpisu přidá název reference.
         */
        else if (regEx.isCallStaticMethod(command)) {
            result = makeOperation.makeCallStaticMethod(command);
        }
		
		
		
		
		
		
		
		// Naplnění proměnné třídy:
		else if (regEx.isFullfillmentOfVariableGetMethod(command)) {
			result = makeOperation.makeFullfillmentOfVariableGetMethod(command);
		}
		
		/*
		 * Naplnění proměnné třídy pomocí instance jiné třídy v syntaxi.
		 * 
		 * instanceName.variableName = new packageName. (anotherPackageName.) ClassName
		 * (parameters); (-> parameters jsou volitelné)
		 */
		else if (regEx.isFullfilmentOfVariableByCallingConstructor(command)) {
			result = makeOperation.makeFulfillmentOfVariableByCallingConstructor(command);
		}
		
		// Naplnění proměnné v instanci třídy proměnnou v instanci třídy nebo v proměnnou
		// vytvořenou v editoru příkazů
		else if (regEx.isFullfillmentOfVariableVariable(command)) {
			result = makeOperation.makeFullfillmentOfVariableVariable(command);
		}		
		
		// Naplnění proměnné v instanci třídy proměnnou v instanci třídy nebo proměnnou
		// vytvořenou v editoru příkazů:
		else if (regEx.isFullfillmentOfVariablesNumber(command)) {
			result = makeOperation.makeFullfillmentOfVariablesNumber(command);
		}
		

		



		
		
		
		// Naplnění proměnné v editoru pomocí výsledku metody:
		else if (regEx.isFullfillmentOfEditorVariableGetMethod(command)) {
			result = makeOperation.makeFullfillmentOfEditorVariableGetMethod(command);
		}
		
		// Naplnění proměnné v editoru pomocí hodnoty jiné proměnné
		else if (regEx.isFullfillmentOfEditorVariableVariable(command)) {
			result = makeOperation.makeFullfillmentOfEditorVariableVariable(command);
		}
		
		// Naplnění proměnné v editoru pomocí zadané konstanty - znak, text, číslo
		else if (regEx.isFullfillmentOfEditorVariableConstant(command)) {
			result = makeOperation.makeFullfillmentOfEditorVariableConstant(command);
		}
		

		

		
		
		
		// Deklarace proměnné v editoru příkazů
		else if (regEx.isDeclarationEditorVariable(command)) {
			result = makeOperation.makeDeclarationEditorVariable(command);
		}
		
		
		
		
		
		// Deklarace a naplnění proměnné v editoru příkazů proměnnou (v editoru příkazů nebo instanci třídy):
		else if (regEx.isDeclarationAndFulfillmentEditorVariableVariable(command)) {
			result = makeOperation.makeDeclarationAndFulfillmentOfEditorVariableVariable(command);
		}
				
		// Deklarace a naplnění proměnné v editoru příkazů metodou, která vrací hodnotu stejného datového typu:
		else if (regEx.isDeclarationAndFulfillmentEditorVariableGetMethod(command)) {
			result = makeOperation.makeDeclarationAndFulfillmentOfEditorVariableGetMethod(command);
		}
		
		
		// Deklarace a naplnění proměnné v editoru příkazů konstantou (jednou hodnotou):
		else if (regEx.isDeclarationAndFulfillmentEditorVariableConstant(command)) {
			result = makeOperation.makeDeclarationAndFulfillmentOfEditorVariableConstant(command);
		}
		
		
		

		
		
		
		
		
		
		
		
		// Inkrementace a dekrementace konstanty - literálu:
		else if (regEx.isIncrementValueBehind(command)) {
			result = makeOperation.makeIncrementValueBehind(command);
		}
		
		else if (regEx.isDecrementValueBehind(command)) {
			result = makeOperation.makeDecrementValueBehind(command);
		}
		
		else if (regEx.isIncrementValueBefore(command)) {
			result = makeOperation.makeIncrementValueBefore(command);
		}
		
		else if (regEx.isDecrementValueBefore(command)) {
			result = makeOperation.makeDecrementValueBefore(command);
		}
		
		
		
		// Inkrementace a dekrementace proměnné třídy:
		else if (regEx.isIncrementClassVariableBefore(command)) {
			result = makeOperation.makeIncrementClassVariableBefore(command);
		}
		
		else if (regEx.isDecrementClassVariableBefore(command)) {
			result = makeOperation.makeDecrementClassVariableBefore(command);
		}
		
		else if (regEx.isIncrementClassVariableBehind(command)) {
			result = makeOperation.makeIncrementClassVariableBehind(command);
		}
		
		else if (regEx.isDecrementClassVariableBehind(command)) {
			result = makeOperation.makeDecrementClassVariableBehind(command);
		}
		
		
		
		
		// Inkrementace a dekrementace proměnné v editrou příkazů:
		else if (regEx.isIncrementCommandEditorVariableBehind(command)) {
			result = makeOperation.makeIncrementCommandEditorVariableBehind(command);
		}
		
		else if (regEx.isIncrementCommandEditorVariableBefore(command)) {
			result = makeOperation.makeIncrementCommandEditorVariableBefore(command);
		}
		
		else if (regEx.isDecrementCommandEditorVariableBehind(command)) {
			result = makeOperation.makeDecrementCommandEditorVariableBehind(command);
		}
		
		else if (regEx.isDecrementCommandEditorVariableBefore(command)) {
			result = makeOperation.makeDecrementCommandEditorVariableBefore(command);
		}
		
		
		
		
		

		
		
		
		// Operace s listem:
		

		
		// Syntaxe pro přidání hodnoty do Listu - pomocí metody add();
		
		// Note: ZDE NEMOHU DÁT PODMÍNKU 'else' PROTOŽE SE JEDNÁ O STEJNOU SYNTAXI, JAKO MÁ ZAVOLÁNÍ METODY
		// NAD INSTANCÍ TŘÍDY, PROTO SE NEJPRVE OTESTUJE ZDA SE JEDNÁ O ZAVOLÁNÍ METODY NAD INSTANCÍ TŘÍDY
		// A PAK NÁSLEDUJÍCÍ PŘÍKAZY PRO OTESTOVÁNÍ, ZDA SE JEDNÁ O PŘIDÁNÍ OBJEKTU DO KOLEKCE VYTVOŘENÉ V EDITORU PŘÍKAZŮ.
		// DÍKY TOMU, ŽE VYTVOŘENÉ INSTANCE A PROMĚNNÉ NEMOHOU MÍT STEJNÝ NÁZEV SE NA KONEC PROVEDE JEN JEDNO Z TOHO (tj. bud se zavolá
		// metoda nad instancí třídy nebo se přidá objekt do kolekce, popřípadě ani jedno - pokud se nenajde správný název - [např])
		
		
		
		// Syntaxe pro deklaraci - vytvoření a naplnění Listu - ArrayListu:
		else if (regEx.isCreateAndFullfillmentList(command)) {
			result = makeOperation.makeCreateAndFullfillmentList(command);
		}
		
		
		
	
		
		
		
		
		
		// Jednorozměrné pole:
		// Deklarace jednorozměrného pole a jeho naplnění hodnotami:
		else if (regEx.isDeclarationAndFullfillmentArray(command)) {
			result = makeOperation.makeDeclarationAndFullfillmentArray(command);
		}
		
		
		// Deklarace jednorozměrného pole ("krátná verze" - toto puze pro rozpoznání
		// způsobu deklarace) s naplnění hodnotami:
		else if (regEx.isDeclarationAndFulfillmentArrayShort(command)) {
			result = makeOperation.makeDeclarationAndFullfillmentArrayShort(command);
		}
		
				
		//Deklarace pole s definovanou délkou pole:
		else if (regEx.isDeclarationArrayWithLength(command)) {
			result = makeOperation.makeDeclarationArrayWithLength(command);
		}
		
		
		// Deklarace jednorozměrného pole pomocí získání jej z metody (zavoláním
		// statické metody nad třídou nebo instancí třídy.):
		else if (regEx.isDeclarationArrayByCallingMethod(command)) {
			result = makeOperation.makeDeclarationArrayByCallingMethod(command);
		}
		
		
		
		
		
		
		// Naplnění hodnoty v poli na zadaném indexu pomocí metody:
		else if (regEx.isFullfillmentValueAtIndexOfArrayMethod(command)) {
			result = makeOperation.makeFullfillmentValueAtIndexOfArrayMethod(command);
		}
				
		// Naplnění hodnoty v poli na zadném indexu pomocí proměnné:
		else if (regEx.isFullfillmentValueAtIndexOfArrayVariable(command)) {
			result = makeOperation.makeFullfillmentValueAtIndexOfArrayVariable(command);
		}
		
		// Naplnění hodnoty v poli na zadném indexu pomocí konstanty
		else if (regEx.isFullfillmentValueAtIndexOfArrayConstant(command)) {
			result = makeOperation.makeFullfillmentValueAtIndexOfArrayConstant(command);
		}


		
		
		// Příkaz pro získání a vypsání hodnoty z pole na zadném indexu do editoru výstupů:
		// Syntaxe: variableName[index];
		else if (regEx.isGetValueFromArrayAtIndex(command)) {
			result = makeOperation.makeGetValueFromArrayAtIndex(command);
		}
		
		
		
		
		
		// Inkrementace hodnoty na zadaném indexu v jednorozměrném poli, vytvořeným v
		// editoru příkazů (prefixová forma)
		else if (regEx.isIncrementArrayValueBefore(command)) {
			result = makeOperation.makeIncrementArrayValueBefore(command);
		}

		// Inkrementace hodnoty na zadaném indexu v jednorozměrném poli, vytvořeným v
		// editoru příkazů (postfixová forma)
		else if (regEx.isIncrementArrayValueBehind(command)) {
			result = makeOperation.makeIncrementArrayValueBehind(command);
		}
		
		// Dekrementace hodnoty na zadaném indexu v jednorozměrném poli, vytvořeným v
		// editoru příkazů (prefixová forma)
		else if (regEx.isDecrementArrayValueBefore(command)) {
			result = makeOperation.makeDecrementArrayValueBefore(command);
		}
		
		// Dekrementace hodnoty na zadaném indexu v jednorozměrném poli, vytvořeným v
		// editoru příkazů (postfixová forma)
		else if (regEx.isDecrementArrayValueBehind(command)) {
			result = makeOperation.makeDecrementArrayValueBehind(command);
		}
		
		
		
		
		
		
		
		
		
		
		

		/*
		 * Naplnění položky na zadaném indexu v poli v instanci nějaé třídy.
		 * 
		 * Syntaxe: 
		 * 
		 * referenceName.arrayVariableName [index] = model.data.ClassName.methodName(params);
		 * 
		 * referenceName.arrayVariableName [index] = referenceName.methodName(params);
		 */
		else if (regEx.isFulfillmentOfArrayAtIndexInInstanceByMethod(command)) {
			result = makeOperation.makeFulfillmentOfArrayAtIndexInInstanceByMethod(command);
		}
		
		// Naplnění hodnoty v poli na zadaném indexu v instanci třídy pomocí
		// konstruktoru:
		else if (regEx.isFulfillmentOfArrayAtIndexInInstanceByConstructor(command)) {
			result = makeOperation.makeFulfillmentOfArrayAtIndexInInstanceByConstructor(command);
		}
		
		// Naplnění hodnoty v poli na zadaném indexu v instanci třídy pomocí proměnné v
		// instanci třídy nebo editoru příkazů:
		else if (regEx.isFulfillmentOfArrayAtIndexInInstanceByVariable(command)) {
			result = makeOperation.makeFulfillmentOfArrayAtIndexInInstanceByVariable(command);
		}
		
		// Naplnění hodnoty v poli na zadaném indexu v instanci třídy pomocí konstanty
		// nebo počtení operace:
		else if (regEx.isFulfillmentOfArrayAtIndexInInstanceByConstant(command)) {
			result = makeOperation.makeFulfillmentOfArrayAtIndexInInstanceByConstant(command);
		}

		
		
		
		
		
		
		
		
		// Získání hodnoty v jednorozměrném poli, které se nachází v nějaké instanci:
		else if (regEx.isGetValueFromArrayAtIndexInInstance(command)) {
			result = makeOperation.makeGetValueFromArrayAtIndexInInstance(command);
		}
		
		
		// Získání velikost pole, které se nachází v nějaké instanci.
		else if (regEx.isGetLengthOfArrayInInstance(command)) {
			result = makeOperation.makeGetLengthOfArrayInInstance(command);
		}
		
		
		
		
		
		
		
		
		// Prefixová forma inkrementace / dekrementace hodnoty v jednorozměrném poli v
		// instancí třídy:
		else if (regEx.isPrefixIncrementValueAtArrayInInstance(command)) {
			result = makeOperation.makePrefixIncrementValueAtArrayInInstance(command);
		}

		// Postfixová forma inkrementace / dekrementace hodnoty v jednorozměrném poli v
		// instancí třídy:
		else if (regEx.isPrefixDecrementValueAtArrayInInstance(command)) {
			result = makeOperation.makePrefixDecrementValueAtArrayInInstance(command);
		}
		
		
		else if (regEx.isPostfixIncrementValueAtArrayInInstance(command)) {
			result = makeOperation.makePostfixIncrementValueAtArrayInInstance(command);
		}
		
		
		else if (regEx.isPostfixDecrementValueAtArrayInInstance(command)) {
			result = makeOperation.makePostfixDecrementValueAtArrayInInstance(command);
		}
		

		
		
		
		
		
		
		
		
		
		// Syntaxe pro zjistění delky pole, resp. počet prvků v poli:
		else if (regEx.isGetArrayLength(command)) {
			result = makeOperation.makeGetLengthArray(command);
		}	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		/*
		 * Práce s dvojrozměrným polem, ale pouze v instanci nějaké třídy, která se
		 * nachází v diagramu tříd.
		 */
		
		// Vložení návratové hodnoty z metody na index ve dvourozměrném poli, které se
		// nachází v instanci třídy:
		else if (regEx.isFulfillmentIndexAtTwoDimArrayInInstanceByMethod(command)) {
			result = makeOperation.makeFulfillmenIndexAtTwoDimArrayByMethod(command);
		}

		// Vložení návratové hodnoty ze zavolání konstruktoru (novou instanci) na index
		// ve dvojrozměrném poli, které se nachází v instanci třídy:
		else if (regEx.isFulfillmentIndexAtTwoDimArrayInInstanceByConstructor(command)) {
			result = makeOperation.makeFulfillmenIndexAtTwoDimArrayByConstructor(command);
		}	
		
		// Vložení hodnoty z proměnné na index ve dvojrozměrném poli, které se nachází v
		// instanci třídy:
		else if (regEx.isFulfillmentIndexAtTwoDimArrayInInstanceByVariable(command)) {
			result = makeOperation.makeFulfillmenIndexAtTwoDimArrayByVariable(command);
		}	
		
		// Vložení konstanty nebo výsledku početní operace na index ve dvojrozměrném
		// poli, které se nachází v instanci třídy:
		else if (regEx.isFulfillmentIndexAtTwoDimArrayInInstanceByConstant(command)) {
			result = makeOperation.makeFulfillmenIndexAtTwoDimArrayByConstant(command);
		}	
		
		
		
		
		
		
		
		// Získání prvku z dvojrozměrného pole na zadaném index, které se nachází v
		// instanci třídy:
		else if (regEx.isGetValueAtIndexAtTwoDimArrayInInstance(command)) {
			result = makeOperation.makeGetValueAtIndexAtTwoDimArrayInInstance(command);
		}
		
		
		
		
		
		
		
		
		
		
		// Prefixová inkrementace položky ve dvourozměrném poli v instanci třídy:
		else if (regEx.isPrefixIncrementValueAtIndexInTwoDimArrayInInstance(command)) {
			result = makeOperation.makePrefixIncrementValueAtIndexInTwoDimArrayInInstance(command);
		}

		// Prefixová dekrementace položky ve dvourozměrném poli v instanci třídy:
		else if (regEx.isPrefixDecrementValueAtIndexInTwoDimArrayInInstance(command)) {
			result = makeOperation.makePrefixDecrementValueAtIndexInTwoDimArrayInInstance(command);
		}

		// Postfixová inkrementace položky ve dvourozměrném poli v instanci třídy:
		else if (regEx.isPostfixIncrementValueAtIndexInTwoDimArrayInInstance(command)) {
			result = makeOperation.makePostfixIncrementValueAtIndexInTwoDimArrayInInstance(command);
		}

		// Postfixová dekrementace položky ve dvourozměrném poli v instanci třídy:
		else if (regEx.isPostfixDecrementValueAtIndexInTwoDimArrayInInstance(command)) {
			result = makeOperation.makePostfixDecrementValueAtIndexInTwoDimArrayInInstance(command);
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		/*
         * Zde se již otestovaly veškeré regulární výrazy pro možné syntaxe ohledně
         * jazyka Java, které dokáže editoru příkazů rozpoznat dle příslušných
         * regulárních výrazů, tak na konec otestuji, zda se nejedná o nějakou
         * matematickou operaci, ale navíc tam musím vložit podmínku, zda výraz obsahuje
         * alespoň nějaké číslo, protože zadaný výraz obsahuje alespoň jednu číslici,
         * ale toto už bych nemusel, protože se může jednat například o proměnné, které
         * budou obsahovat příslušná čísla, ale pro zjedušení, resp. "upřesnění", že se
         * jedná o matematický výraz budu požadovat alespoň jednu číslici nebo specifické "znaky" - true, false,
         * random, ....
         *
         * Jako další bych mohl otestovat, zda je ve výraze přítomen alespoň jeden
         * matematický nebo logkcý znak (operátor), například plus, mínus, logický and
         * apod. Ale to již testovat nebudu, protože je možné například zadat funkci:
         * min(1, 2, 3), která vrátí nejmenší číslo (v tomto případě 1), proto se v
         * daném matematickém výraze nemusí nacházet matematický nebo logkcý operátor,
         * ale pro případ, že bych změnil názor, nebo chtěl podmínky upřesnit apod. Tak
         * zde tu metodu nechám zakomentovatnou i s podmínkou, ale je třeba to hládat i
         * v metodě 'getValueFromVar_Method_Constant' ve třídě MakeOperation, kde se
         * tato podmínka také musí testovat, pokud je například zadáno, aby se příslušná
         * hodnota z výše uvedené funkce například vložila do proměné apod.
         */
//		else if (regEx.isMathematicalExpression(command.replaceAll("\\s", "")) && containsNumber(command) && checkMathOperands(command)) {
        else if (regEx.isMathematicalExpression(command.replaceAll("\\s", "")) && (containsNumber(command) || containsSpecificChars(command))) {
            result = makeOperation.computeMathematicalExpression(command);
        }
		
		
		


		else {
		    result = txtCommandIsNotRecognized + ": '" + command.trim() + "'";
        }

        outputEditor.addResult(result);

        // Přidání příkazů do historie:
        addCommandToHistory(command.trim(), result);
    }


    /**
     * Přidání zadaného příkazu do historie příkazů.
     *
     * @param command
     *         - uživatelem zadaný příkaz v editoru příkazů.
     * @param result
     *         - výsledek příkazu. Resp. výsledek provedení operace. Například "Úspěch / Neúspěch, chyba v provedení,
     *         příkaz nerozeznán" atd.
     */
    private void addCommandToHistory(final String command, final String result) {
        if (completeHistoryCompletion != null)
            completeHistoryCompletion.addCompletion(command, result);

        if (projectHistoryCompletion != null)
            projectHistoryCompletion.addCompletion(command, result);


        // Přidání příkazu do Listu pro historii, která se prochází pomocí šípek nahoru a dolů (s Ctrl):
        commandEnteredList.add(command);

        /*
         * Nastavím index pro procházení historie příkazů na velikost listu.
         *
         * Protože, když se stiskne šipka nahoru, tak se neprve dekrementuje tento index, tak dostanu poslední
         * příkaz, resp. poslední prvek v listu a zobrazím jej.
         *
         * Pokud uživatel stiskne šipku dolů, pak se nejprve inkremetnuje index, zjistí se, že je větší než velikost
         * listu se zadanými příkazy, nastaví se na nulu (první prvek v listu) - první příkaz a ten se zobrazí v
         * editoru.
         *
         * Podobně je tomu při střídání šipek nahoru a dolů, když se stiskne šipka nahoru, pak se nejprve
         * dekrementuje index - vezme se předchozí příkaz a nastaví se, u šipky dolů je to naopak.
         */
        commandIndex = commandEnteredList.size();
    }
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda,která zjistí, zda zadaná text v parametru metody (text) obsahuje
	 * nějaké číslo (alespoň jednu číslici).
	 * 
	 * @param text
	 *            - text, o kterém se má zjistit, zda obsahuje alespoň jednu
	 *            číslici.
	 * 
	 * @return true, pokud text obsahuje alespoň jednu číslici, jinak false.
	 */
	public static boolean containsNumber(final String text) {
		final Pattern p = Pattern.compile("([\\d])+");
        final Matcher m = p.matcher(text);
        
        return m.find();
	}


    /**
     * Otestování, zda se jedná o specifické "texty" například true, false, pi, e apod. Jedná se o hodnoty, které může
     * uživatel zadat a neobsahují nebo nemusí obsahovat číslice, ale i přesto by se měl provést "výpočet" s těmito
     * hodnotami pomocí využité knihovny, aby se vyhodnotila například podmínka apod.
     * <p>
     * (Toto je velice "omezený" doplňěk a výraz text je testovat tak, že může obsahovat v podstatě cokoli, ale v tom
     * textu se musí nacházet mimo jiné výše uvedený znaky.)
     *
     * @param text
     *         - text / příkaz, který uživatel zadal v editoru příkazů
     *
     * @return true, pokud výraz obsahuje mimo jiné výše uvedené znaky a nějaké další (viz kontrola výrazu), jinak
     * false.
     */
    private static boolean containsSpecificChars(final String text) {
        // (?i) -> ignorování velikosti písmen
        final Pattern p = Pattern.compile("(?i).*\\s*(e|pi|true|false|not|if|random)\\s*.*");
        final Matcher m = p.matcher(text);

        return m.find();
    }
	
	

	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda zadaný příkaz (nebo jeho část) v editoru příkazů
	 * obsahuje jeden ze zvolených znaků pro matematický výpočet, jako je třeba
	 * plus, mínus, děleno apod.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz nebo jeho část (záleží odkud se tato
	 *            metoda volá).
	 * 
	 * @return true, pokud text command obsahuje alespoň jeden ze zvolených znaků
	 *         (viz tělo metody). Jinak false.
	 */
	public static boolean checkMathOperands(final String command) {
		/*
		 * Zde testuji znaky, které by mohl výraz obsahovat, aby se opravdu jednalo o
		 * výraz pro matematický výpočet, úmyslně zde nejsou třeba závorky nebo
		 * vykřičník apod. Protože například závorky by mohly být u zavolání metody a
		 * vykřičník třeba někde v textu jako parametr metody.
		 * 
		 * Testuji pouze základní matematické znaky jako je plus, mínus, krát, děleno,
		 * modulo a pak nějaké znaky, se kterými umí knihovna pracovat, jako je
		 * například svislítko nebo amprsad pro podmínky apod.
		 */
		return command.contains("+") || command.contains("-") || command.contains("*") || command.contains("/")
				|| command.contains("%") || command.contains("&") || command.contains("|") || command.contains("<")
				|| command.contains(">") || command.contains("=") || command.contains("^");
	}




	@Override
    public void setLanguage(final Properties properties) {
        final String txtReferenceVariable;

        if (properties != null) {
            txtCommandIsNotRecognized = properties.getProperty("Ce_ErrorText_SyntaxIsNotRecognized", Constants
                    .CE_SYNTAX_IS_NOT_RECOGNIZED);
            txtReferenceVariable = properties.getProperty("Ce_Reference_Variable", Constants.CE_REFERENCE_VARIABLE);
        }

        else {
            txtCommandIsNotRecognized = Constants.CE_SYNTAX_IS_NOT_RECOGNIZED;
            txtReferenceVariable = Constants.CE_REFERENCE_VARIABLE;
        }

        FieldValueSyntaxCreator.setTxtReferenceVar(txtReferenceVariable);
    }


    /**
     * Getr na proměnnou, která značí, zda se v okně pro automaticé doplňování / dokončování příkazů v editoru příkazů
     * mají zobrazit referenční proměnné i příslušné hodnoty / instance.
     *
     * @return true, v případě, že se mají zobrazit referenční proměnné za hodnotou / referencí na instanci.
     */
    public static boolean isShowReferenceVariablesInCompleteWindow() {
        return showReferenceVariablesInCompleteWindow;
    }


    /**
     * Getr na proměnnou, která značí, zda se v okně pro autoamtické doplňování / dokončování příkazů v editoru příkazů
     * mají zobrazovat / přidávat syntaxe pro převedení hodnoty proměnné v instanci třídy do nové proměnné v editoru
     * příkazů.
     *
     * @return true, pokud e mají přidávat výše zmíněné syntaxe, jinak false
     */
    public static boolean isAddSyntaxForFillingTheVariable() {
        return addSyntaxForFillingTheVariable;
    }


    /**
     * Zda se má při deklaraci proměnné s naplněním proměnné hodnotou z nějaké proměnné v instanci třídy přidat klíčové
     * slovo "final" k deklarované proměnné.
     *
     * @return true, pokud má být deklarovaná proměnná implitině "final", jinak false.
     */
    public static boolean isAddFinalWordForFillingTheVariable() {
        return addFinalWordForFillingTheVariable;
    }


    /**
     * Getr, zda se mají v okně pro automatické doplňování / dokončování příkazů v editoru příkazů zobrazovat ukázky
     * syntaxe.
     *
     * @return true, pokud se v okně pro doplňování příkazů v editoru příkazů mají zobrazovat definované ukázky syntaxe,
     * jinak false. V takovém případě budou na výběr pouze hodnoty z otevřeného projektu.
     */
    public static boolean isShowExamplesOfSyntax() {
        return showExamplesOfSyntax;
    }


    /**
     * Getr, zda se mají v okně pro automatické doplňování / dokončování příkazů v editoru příkazů zobrazovat výchozí
     * definované metody. Například metody pro výpis proměnných, výpis příkazů, ...
     *
     * @return true, pokud se ve výše zmíněném okně mají zobrazovat výchozí definované metody pro práci s editoru
     * příkazů, jinak false.
     */
    public static boolean isShowDefaultMethods() {
        return showDefaultMethods;
    }


    /**
     * Getr, zda se má zpřístupnit po stisknutí příslušné klávesové zkratky okno s dostupnými příkazy pro editor
     * příkazů.
     *
     * @return true v případě, že se má výše popsané okno zpřístupnit, jinak false.
     */
    public static boolean isShowAutoCompleteWindow() {
        return showAutoCompleteWindow;
    }

    /**
     * Getr, zda se má ukládat historie zadaných příkazů od spuštění aplikace, aby jej mohl uživatel filtrovat a doplnit
     * přes klávesovou zkratku.
     *
     * @return true v případě, že se má ukládat historie do okna pro automatické doplňování příkazů, jinak false.
     */
    public static boolean isShowCompleteHistoryWindow() {
        return showCompleteHistoryWindow;
    }


    /**
     * Getr, zda se má ukládat historie zadaných příkazů v rámci otevřeného projektu, aby jej mohl uživatel filtrovat a
     * doplnit přes klávesovou zkratku.
     *
     * @return true v případě, že se má ukládat historie do okna pro automatické doplňování příkazů, jinak false.
     */
    public static boolean isShowProjectHistoryWindow() {
        return showProjectHistoryWindow;
    }
}