package cz.uhk.fim.fimj.commands_editor.values_completion_window;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.commands_editor.Variable;
import cz.uhk.fim.fimj.commands_editor.VariableArray;
import cz.uhk.fim.fimj.commands_editor.VariableList;
import cz.uhk.fim.fimj.instances.Instances;
import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Jan Krunčík
 * @version 2.0
 * @since 28.07.2018 9:46
 */

public class ValueGetterImpl implements ValueGetr {

    @Override
    public List<Class<?>> getClassesInClassDiagram() {
        final List<Class<?>> classList = new ArrayList<>();

        // Tento typ cyklu, abych přdešel výjimce concurent... exception:
        for (int i = 0; i < GraphClass.getCellsList().size(); i++) {
            final DefaultGraphCell cell = GraphClass.getCellsList().get(i);

            // Pouze, pokud se jedná o buňku reprezentující třídu v diagramu tříd:
            if (!(cell instanceof DefaultEdge) && GraphClass.KIND_OF_EDGE.isClassCell(cell)) {
                // Tak si načtu zkompilovanou třídu v diagramu tříd:
                final Class<?> clazz = App.READ_FILE.loadCompiledClass(cell.toString(), null);

                if (clazz != null)
                    classList.add(clazz);
            }
        }

        return classList;
    }


    @Override
    public Map<String, Object> getInstancesInInstanceDiagram() {
        return Instances.getAllInstancesOfClassInCd();
    }


    @Override
    public Map<String, Variable> getClassicVariablesInCommandEditor() {
        return Instances.getClassicVariablesFromCommandEditor();
    }


    @Override
    public Map<String, VariableList<?>> getListVariablesInCommandEditor() {
        return Instances.getListVariablesFromCommandEditor();
    }


    @Override
    public Map<String, VariableArray<?>> getArrayVariablesInCommandEditor() {
        return Instances.getArrayVariablesFromCommandEditor();
    }
}
