package cz.uhk.fim.fimj.commands_editor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.Timer;


import com.udojava.evalex.Expression;
import com.udojava.evalex.Expression.ExpressionException;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.commands_editor.ParseToJavaTypes.BooleanTypeParser;
import cz.uhk.fim.fimj.commands_editor.ParseToJavaTypes.ByteTypeParser;
import cz.uhk.fim.fimj.commands_editor.ParseToJavaTypes.CharacterTypeParser;
import cz.uhk.fim.fimj.commands_editor.ParseToJavaTypes.DoubleTypeParser;
import cz.uhk.fim.fimj.commands_editor.ParseToJavaTypes.FloatTypeParser;
import cz.uhk.fim.fimj.commands_editor.ParseToJavaTypes.IntegerTypeParser;
import cz.uhk.fim.fimj.commands_editor.ParseToJavaTypes.LongTypeParser;
import cz.uhk.fim.fimj.commands_editor.ParseToJavaTypes.ShortTypeParser;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;
import cz.uhk.fim.fimj.instance_diagram.CheckRelationShipsBetweenInstances;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;
import cz.uhk.fim.fimj.instances.Called;
import cz.uhk.fim.fimj.instances.Instances;
import cz.uhk.fim.fimj.instances.OperationsWithInstances;
import cz.uhk.fim.fimj.instances.OperationsWithInstancesInterface;
import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.output_editor.OutputEditor;
import cz.uhk.fim.fimj.swing_worker.ThreadSwingWorker;
import org.apache.commons.lang3.StringUtils;

/**
 * Tato třída slouží především pro implementaci metod, které jsou definované v rozhraní: MakeOperationInterface. Jedná
 * se o metody, jako jsou metody pro vytvoření instance třídy, zavolání metody, naplnění proměnné, apod. Prostě se zde
 * provádějí operace, které může uživatel v editoru příkazů provést
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class MakeOperation implements MakeOperationInterface, LanguageInterface {

	/**
	 * Proměnná, která obsahuje výchozí hodnotu pro odpočítávání pro ukončení a
	 * restart aplikace. Tato hodnota se využije, poud nebude v příkazu pro restart
	 * nebo ukončení aplikace zadána hodnota, resp. čas do ukončení nebo restartu.
	 */
	private static final int DEFAULT_VALUE_FOR_COUNTDOWN = 3;
	
	
	/**
	 * Timer, který slouží pro odpočet pro ukončení aplikace, vždy až se dojde s
	 * odpočtem na nulu, tak se uloží aktuálně otevřený projekt a ukončí se
	 * aplikace.
	 */
	private Timer timerForCloseApp;
	
	/**
	 * Timer, který slouží pro odpočet času do restartu aplikace, vždy když dojde
	 * odpočet na nulu, pak se restartuje aplikace. Ale jetě před tím se uloží
	 * potřebná data jako je třeba diagram tříd apod.
	 */
	private Timer timerForRestartApp;
	
	
	
	
	
	
	/**
	 * Pro rozpoznání některých částí příkazů
	 */
	private static RegularExpressionsInterface regEx;

	
	
	
	
	// Texty do hlášek pro výpis do editoru
	private static String txtSuccess, txtFailure, txtInstancesNotFound, txtMethodNotFound,
			txtBadNameOfMethodOrParameter, txtReferenceNameNotFound, txtFailedRetypeValue,
			txtMissingVariableInInstaceOfClass, txtErrorInVariableNameOfVisibilityVariable, txtInstanceOfClassNotFound,

			txtAtLeastOneInstanceOfClassNotFound, txtDataTypesAreNotEqual, txtErrorMethodOrParametersOrModifier,

			txtVariableNotFoundInReferenceToInstanceOfClass, txtFailedToFillVariable, txtPossibleErrorsInFillVariable,
			txtErrorsImmutableVisibilityPublic, txtVariableName, txtAlreadyExist, txtSrcDirNotFound, txtClassNotFound,
			txtFiledToLoadClassFile, txtDataTypesOrConstructorNotFound,

			// Texty puze pro statikou metodu:
			txtErrorWhileCallingStaticMethod_1, txtErrorWhileCallingStaticMethod_2,

			// Text pro zavolání konstruktoru:
			txtConstructorWasNotFound,

			txtSpecificMethodWasNotFound, txtPublicConstructorWasNotFound,

			// Texty, že nebyl nalezen soubor .class pro zavolaní staticke metody a nebo
            // konstruktoru:
            txtFileDotClass, txtFileDotClassWasNotFound, txtFailedToFindTheClass, txtFailedToFindBinDir,
                    txtFailedToFindInnerClass, txtInnerClassIsNotStatic, txtInnerClassIsNotPublicEitherProtected,

            txtErrorInIncrementValue, txtNunberOutOfInterval, txtErrorInDecrementValue, txtVariableDontContainsNumber,
            txtFailedToCastNumber, txtReferenceDontPointsToInstance, txtVariableIsNotPublicOrIsFinal,
            txtReferenceDontPointsToInstanceOfClass, txtReference, txtVariable, txtError, txtMethodWillNotBeInclude,
            txtMethod, txtMethodHasNotBennCalled,
            txtReferenceDoesNotPointToInstanceOfClass, txtValueWillNotBeCounted, txtValueOfVariableWillNotBeCounted,
            txtDataTypeOfField, txtDataTypeIsDifferent, txtInstance, txtErrorWhileConvertingValues, txtToType,

            txtVariableNameMustNotBeKeyWord, txtValueIsFinalAndCannotBeChanged, txtErrorInClassLoaderOrAccessToField,
            txtDoNotFound, txtFailedToCastValue, txtToDataType, txtErrorWhileParsingValue, txtProbablyIsAbstractClass,
            txtNotFoundConstructorToCastingDataType, txtPossibleParametersCountIsDifferent,
            txtExceptionWhileCallingConstructorToCasting, txtVariableFailedToLoad, txtVariableNotFoundForInsertValue,
            txtVariableNotFound, txtValue, txtVariableIsFinalIsNeedToFillInCreation, txtMustNotBeFinalAndNull,
            txtVariableNameIsNeedToChangeIsAlreadyTaken, txtVariableNameNotRecognized,
            txtDataTypeNotFoundOnlyPrimitiveDataTypes, txtDataType, txtFailedToCreateTemporaryVariable, txtData,
            txtValue2, txtMaybeError, txtDataTypeDoNotSupported,
            txtVariableIsNullCannotBeIncrementOrDecrement, txtVariableIsNull, txtValueWasNotRecognized,
            txtFailedToCreateVariable, txtDoNotRetrieveDataTypeOfVariable,
            txtFailedToGetDataAboutVariableWhileDeclaration, txtReferenceDontPointToInstanceOfList,
            txtValueAtIndexIsNull, txtConditionsForIndexOfList, txtSpecifiedIndexOfList,
            txtInstanceOfListWasNotFoundUnderReference, txtSizeOfTheList, txtDataTypeForListNotFound,
            txtDataTypeForArrayAreNotIdenticaly, txtDataTypeForArrayNotFound,
            txtInstanceOfArrayWasNotFoundUnderReference, txtArrayIsFinalDontChangeValues, txtArray, txtIndexOutOfRange,
            txtConditionForIndexInArray, txtIndex, txtSizeOfArray, txtName, txtDoesNotIncrementDecrement,
            txtIndexOfListIsOutOfRange, txtVariableIsFinalDontChangeValue,

			txtInstanceIsNotCorrectForParameter, txtArraySizeIsOutOfInteger, txtValueInArrayInIndexIsNull,
			txtValueAtIndexAtArrayCantIncreDecre, txtFailedToCalculateMathTerm_1, txtFailedToCalculateMathTerm_2,

			// Texty do metody writeComands:
			txtOverViewOfCommands, txtOverviewOfCommandsEditorVariables,
			   
			// Matematické operace:
			txtMathCalculationTitle, txtOperandsForMathCalculation,
			// Logické operaátory:
			txtLogicalOperationsInfo,
			// Konstanty:
			txtConstants,
			// Podporované funkce:
			txtSupportedFunctionsTitle, txtNamesOfFunctionsInfo,
			// Příkaldy:
			txtWithoutSemicolonAtTheEndsOfExpression, txtIncludeVariablesFromCommandEditorInfo,
			txtVarNamesCantConstainNumbers,

			txtDeclaredAndFulfilledVars, txtDeclaredAndFulfilledVar,

			// Matematické operace:
			txtUnaryPlus, txtUnaryMinus, txtMultiplicationOperator, txtDivisionOperator, txtModuloOperator,
			txtPowerOperator,

			// Logické operátory:
			txtEquals, txtNotEquals, txtLessThan, txtLessThanOrEqualTo, txtGreaterThan, txtGreaterThanOrEqualTo,
			txtBooleanAnd, txtBooleanOr,

			// Konstanty:
			txtValueOfE, txtValueOfPi, txtValueOne, txtValuezero,

			// podporované funkce:
			txtFceNot, txtCondition, txtFceRandom, txtFceMin, txtFceMax, txtFceAbs, txtFceRound, txtFceFloor,
			txtFceCeiling, txtFcelog, txtFceLog10, txtFceSqrt, txtFceSin, txtFceCos, txtFceTan, txtFceCot, txtFceAsin,
			txtFceAcos, txtFceAtan, txtFceAcot, txtFceATan2, txtFceASinh, txtFceCosh, txtFceTah, txtFceCoth, txtFceSec,
			txtFceCsc, txtFceSech, txtFceCsch, txtFceAsinh, txtFceAcosh, txtFceATanh, txtFceRad, txtFceDeg,

			// Nějkaé ukázky pro matematické výrazy,které je možné zadat (podmínky, funkce,
			// jejich využití, ...)
			txtExamplesOfMathExpressions,

            txtMakeInstance, txtClassName, txtMakeInstanceParameters_1, txtMakeInstanceParameters_2,
            txtMakeInstanceParameters_3, txtWithParameters, txtCallConstructorInfo_1, txtCallConstructorInfo_2,
            txtCallConstructorInfo_3, txtCallMethodInfo, txtCallStaticMethodInfo,
            txtCallStaticMethodInInnerClassInfo, txtCallMethodInfo_1,
            txtCallMethodInfo_2,

			// Info ohledně zavolání zděděných metod nad třídou coby potomkem příslušné
			// třídy s danou metodou:
			txtInfoAboutMethodsTitle, txtInfoAboutCallMethodFromObjectClass, txtCallMethodFromInheritedClassesInfo,
			txtCallStaticMethodFroomInheritedClassesInfo,

            txtIncrementationDecrementation, txtIncrementationDecrementation_1, txtVariableIncrementation,
            txtVariableDecrementation, txtIncrementationMyVariable, txtDecrementationMyVariable,
            txtIncrementationDecrementationConstatnts, txtIncrementationConstants, txtDecrementationConstants,
            txtDeclarationOfVariable, txtDeclarationVariableInfo, txtLikeThis, txtDeclarationVariableInfo_2, txtSyntaxe,
            txtForString, txtSyntaxeForChar, txtSyntaxeForDouble, txtSimilaryDataTypes, txtFullfillmentVariable,
            txtFullfilmentVariableInfo, txtFullfilmentVariableSyntaxeInfo, txtFullfilmentVariableText,
            txtFullfilmentMyVariable, txtFullfilmentReplaceInfo, txtFullfilmentReturnValue,
            txtFullfilmentByStaticMethod, txtFullfilmentByStaticMethodInInnerClass, txtFullfilmentVariableSameRules,
            txtFullfilmentVariableInfo_2,
            txtFullfilmentEtc, txtPrintMethodInfo, txtPrintMethodInfo_2, txtPrintMethodInfo_3, txtPrintMethodInfo_4,
            txtPrintMethodSyntaxeExample, txtPrintMethodWriteResult, txtPrintMethodReturnValue, txtDeclarationListInfo,
            txtDeclarationListInfo_2, txtDeclarationListInfo_3, txtNote, txtDeclarationListInfo_4,
            txtDeclarationListInfo_5, txtListMethodInfo_1, txtListMethodInfo_2, txtListMethodInfo_3,
            txtListMethodInfo_4, txtListMethodInfo_5, txtListMethodInfo_6, txtListMethodInfo_7, txtListMethodInfo_8,
            txtListMethodInfo_9, txtListMethodInfo_10, txtDeclarationOneDimensionalArray, txtOneDimArrayInfo_1,
            txtOneDimArrayInfo_2, txtOneDimArrayInfo_3, txtDataTypeArrayCanBe, txtOperationsWithArray,
            txtOperationsWithArray_1, txtOperationsWithArray_2, txtOperationsWithArray_3, txtOperationsWithArray_4,
            txtOperationsWithArray_5, txtOperationsWithArray_7, txtOperationsWithArray_8, txtTwoDimArrayTitle,
            txtWorkWithTwoDimArrayInfo, txtCannotCreateTwoDimArray, txtOperationsWithTwoDimArray,
            txtFullfilmentArrayInfo_1, txtFullfilmentArrayInfo_2, txtFullfilmentArrayInfo_3,
            txtOperationsWithArrayinIntanceTitle, txtOperationsWithArrayInInstanceInfo,
            txtOperationsWithArrayInInstanceDifference, txtInfoAboutNullValue,

			// Texty do metody pro vypsání proměnnych v editoru příkazů -
			// printTempVariables();
			txtNoVariablesWasCreated,

			// Ukončení a restart aplikace:

			// Texty pro ukončení odpočtu:
			txtNoCountdownIsRunning, txtCountdownIsStopped,
			// Texty pro spuštění timeru pro ukončení aplikace.
			txtCountdownForCloseAppIsRunning, txtCountdownForCloseAppIsNotPossibleToStart, txtToEndOfAppRemains_1,
			txtToEndOfAppRemains_2, txtToEndOfAppRemains_3, txtSecondsForClose_1, txtSecondsForClose_2,
			txtSecondsForClose_3,
			// Texty pro spuštění timeru pro restartování aplikace:
			txtCountdownForRestartAppIsRunning, txtCountdownForRestartAppIsNotPossibleToStart, txtToRestartAppRemains_1,
			txtToRestartAppRemains_2, txtToRestartAppRemains_3, txtSecondsForRestart_1, txtSecondsForRestart_2,
			txtSecondsForRestart_3,

			// Info ohledně metod pro restartování a ukončení aplikace:
			txtInfoForMethodsOverApp,

			// Restartování aplikace:
			txtRestartAppTitle,

			// Info k restartu - že se uložídiagram tříd apod.
			txtInfoAboutCloseOpenFilesBeforeRestartApp, txtInfoAboutLaunchCountdownWhenCommandIsEntered,
			txtInfoAboutSaveClassDiagramBeforeRestartApp, txtTwoWaysToRestartApp, txtFirstWayToRestartApp_1,
			txtFirstWayToRestartApp_2, txtSecondWayToRestartApp,

			// Texty pro chybové hlášky nad operace s jednorozměrným polem v instanci třídy:
			txtCastingToIntegerFailure_1, txtCastingToIntegerFailure_2, txtInstanceOfClassInIdNotFOund,
			txtEnteredVariable_1, txtEnteredVariable_2, txtIsNotOneDimArray, txtIndexValueAtArrayIsNotInInterval,
			txtValueInArrayIsNull,

			// Texty pro chabové hlášky nad dvourozměrným polem:
			txtIsNotTwoDimArray, txtFailedToSetValueToTwoDimArray, txtIndex_1Text, txtIndex_2Text,
			txtFailedToGetValueFromTwoDimArray, txtFailedToIncrementValueAtIndexInTwoDimArrayInInstance,
			txtFailedToDecrementValueAtIndexInTwoDimArrayInInstance, txtValueWasNotFoundAtIndexInTwoDimarrayInInstance,

			// Ukončení aplikace
			txtCloseAppTitle, txtInfoAboutCloseOpenFilesBeforeCloseApp,
			txtInfoAboutLaunchCountdownAfterCommandIsEntered, txtInfoAboutSaveClassDiagramBeforeCloseApp,
			txtThreeWaysToCloseApp, txtFirstWayToCloseApp_1, txtFirstWayToCloseApp_2, txtSecondWayToCloseApp,
			txtThirdWayToCloseApp_1, txtThirdWayToCloseApp_2, txtInfoAboutThirdWayToCloseApp_1,
			txtInfoAboutThirdWayToCloseApp_2,

			// Zrušení odpočtu při restartování nebo ukončení aplikace:
			txtCancelCountdownTitle, txtCancelCountdownInfo;
			
			
	
	/**
	 * Potřebuji referenci na instanci pro výpis chyb z kompilátoru tříd:
	 */
	private OutputEditor outputEditor;
	
	
	private static OperationsWithInstancesInterface operationWithInstanees;
	
	
	
	/**
	 * Pro vytváření reprezentace instancí:
	 */
	private final GraphInstance instanceDiagram;


	/**
	 * Reference na samotnou aplikaci, resp. na okno, které "tvoří" aplikaci. Je zde potřeba kvůli zavolání metody pro
	 * restartování aplikace.
	 */
	private final App app;
	
	
	
	
	
	
	

	public MakeOperation(final RegularExpressionsInterface regEx, final OutputEditor outputEditor,
			final GraphInstance instanceDiagram, final Properties languageProperties, final App app) {
		super();
		
		MakeOperation.regEx = regEx;	
		this.outputEditor = outputEditor;
		this.instanceDiagram = instanceDiagram;
		this.app = app;
		
		operationWithInstanees = new OperationsWithInstances(outputEditor, languageProperties, instanceDiagram);
		
		
		setLanguage(languageProperties);
	}

	
	
	
	
	
	

	
	
	

	@Override
	public String writeCommands() {
		return txtOverViewOfCommands + ":\n\n"
				
				+ OutputEditor.TWO_GAPS + txtOverviewOfCommandsEditorVariables + ": 'printTempVariables();'\n\n"				
			
				
				// Matematické operace:
				+ OutputEditor.TWO_GAPS + txtMathCalculationTitle + ":\n"
				+ OutputEditor.TWO_GAPS + txtOperandsForMathCalculation + "\n"				
				+ OutputEditor.TWO_GAPS + "'+'" + " -> " + txtUnaryPlus + "\n"
				+ OutputEditor.TWO_GAPS + "'-'" + " -> " + txtUnaryMinus + "\n"
				+ OutputEditor.TWO_GAPS + "'*'" + " -> " + txtMultiplicationOperator + "\n"
				+ OutputEditor.TWO_GAPS + "'/'" + " -> " + txtDivisionOperator + "\n"
				+ OutputEditor.TWO_GAPS + "'%'" + " -> " + txtModuloOperator + "\n"				
				+ OutputEditor.TWO_GAPS + "'^'" + " -> " + txtPowerOperator + "\n\n"
				
				
				// Logické operaátory:
				+ OutputEditor.TWO_GAPS + txtLogicalOperationsInfo + ":\n"
				+ OutputEditor.TWO_GAPS + "'='" + " -> " + txtEquals + "\n"
				+ OutputEditor.TWO_GAPS + "'=='" + " -> " + txtEquals + "\n"
				+ OutputEditor.TWO_GAPS + "'!='" + " -> " + txtNotEquals + "\n"
				+ OutputEditor.TWO_GAPS + "'<>'" + " -> " + txtNotEquals + "\n"
				+ OutputEditor.TWO_GAPS + "'<'" + " -> " + txtLessThan + "\n"
				+ OutputEditor.TWO_GAPS + "'<='" + " -> " + txtLessThanOrEqualTo + "\n"
				+ OutputEditor.TWO_GAPS + "'>'" + " -> " + txtGreaterThan + "\n"
				+ OutputEditor.TWO_GAPS + "'>='" + " -> " + txtGreaterThanOrEqualTo + "\n"
				+ OutputEditor.TWO_GAPS + "'&&'" + " -> " + txtBooleanAnd + "\n"
				+ OutputEditor.TWO_GAPS + "'||'" + " -> " + txtBooleanOr + "\n\n"
				
				
				// Konstanty:
				+ OutputEditor.TWO_GAPS + txtConstants + ":\n"				
				+ OutputEditor.TWO_GAPS + "e" + " -> " + txtValueOfE + "\n"
				+ OutputEditor.TWO_GAPS + "PI" + " -> " + txtValueOfPi + "\n"
				+ OutputEditor.TWO_GAPS + "True" + " -> " + txtValueOne + "\n"
				+ OutputEditor.TWO_GAPS + "False" + " -> " + txtValuezero + "\n\n"
				// Null hodnotu zde vynechám, v této aplikaci nejdsou v rámci editoru příkazů
				// podporovány, ale v rámci matematického výpočtu díky knihovně byto možné bylo,
				// nic méně, aby si to pak uživatel nepletl, tak jej zde neuvedu.
//				+ OutputEditor.TWO_GAPS + "Null" + " -> " + "The null value" + "\n"
				

				
				// podporované funkce:
				+ OutputEditor.TWO_GAPS + txtSupportedFunctionsTitle + ":\n"
				+ OutputEditor.TWO_GAPS + "(" + txtNamesOfFunctionsInfo + " 'case insensitive'." + ")" + "\n"			
				+ OutputEditor.TWO_GAPS + "NOT(expression)" + " -> " + txtFceNot + "\n"
				+ OutputEditor.TWO_GAPS + "IF(condition,value_if_true,value_if_false)" + " -> " + txtCondition + "\n"
				+ OutputEditor.TWO_GAPS + "RANDOM()" + " -> " + txtFceRandom + "\n"
				+ OutputEditor.TWO_GAPS + "MIN(e1,e2, ...)" + " -> " + txtFceMin + "\n"
				+ OutputEditor.TWO_GAPS + "MAX(e1,e2, ...)" + " -> " + txtFceMax + "\n"
				+ OutputEditor.TWO_GAPS + "ABS(expression)" + " -> " + txtFceAbs + "\n"
				+ OutputEditor.TWO_GAPS + "ROUND(expression,precision)" + " -> " + txtFceRound + "\n"
				+ OutputEditor.TWO_GAPS + "FLOOR(expression)" + " -> " + txtFceFloor + "\n"
				+ OutputEditor.TWO_GAPS + "CEILING(expression)" + " -> " + txtFceCeiling + "\n"
				+ OutputEditor.TWO_GAPS + "LOG(expression)" + " -> " + txtFcelog + "\n"
				+ OutputEditor.TWO_GAPS + "LOG10(expression)" + " -> " + txtFceLog10 + "\n"
				+ OutputEditor.TWO_GAPS + "SQRT(expression)" + " -> " + txtFceSqrt + "\n"
				+ OutputEditor.TWO_GAPS + "SIN(expression)" + " -> " + txtFceSin + "\n"
				+ OutputEditor.TWO_GAPS + "COS(expression)" + " -> " + txtFceCos + "\n"
				+ OutputEditor.TWO_GAPS + "TAN(expression)" + " -> " + txtFceTan + "\n"
				+ OutputEditor.TWO_GAPS + "COT(expression)" + " -> " + txtFceCot + "\n"
				+ OutputEditor.TWO_GAPS + "ASIN(expression)" + " -> " + txtFceAsin + "\n"
				+ OutputEditor.TWO_GAPS + "ACOS(expression)" + " -> " + txtFceAcos + "\n"
				+ OutputEditor.TWO_GAPS + "ATAN(expression)" + " -> " + txtFceAtan + "\n"
				+ OutputEditor.TWO_GAPS + "ACOT(expression)" + " -> " + txtFceAcot + "\n"
				+ OutputEditor.TWO_GAPS + "ATAN2(y,x)" + " -> " + txtFceATan2 + "\n"
				+ OutputEditor.TWO_GAPS + "SINH(expression)" + " -> " + txtFceASinh + "\n"
				+ OutputEditor.TWO_GAPS + "COSH(expression)" + " -> " + txtFceCosh + "\n"
				+ OutputEditor.TWO_GAPS + "TANH(expression)" + " -> " + txtFceTah + "\n"
				+ OutputEditor.TWO_GAPS + "COTH(expression)" + " -> " + txtFceCoth + "\n"
				+ OutputEditor.TWO_GAPS + "SEC(expression)" + " -> " + txtFceSec + "\n"
				+ OutputEditor.TWO_GAPS + "CSC(expression)" + " -> " + txtFceCsc + "\n"
				+ OutputEditor.TWO_GAPS + "SECH(expression)" + " -> " + txtFceSech + "\n"
				+ OutputEditor.TWO_GAPS + "CSCH(expression)" + " -> " + txtFceCsch + "\n"
				+ OutputEditor.TWO_GAPS + "ASINH(expression)" + " -> " + txtFceAsinh + "\n"
				+ OutputEditor.TWO_GAPS + "ACOSH(expression)" + " -> " + txtFceAcosh + "\n"
				+ OutputEditor.TWO_GAPS + "ATANH(expression)" + " -> " + txtFceATanh + "\n"
				+ OutputEditor.TWO_GAPS + "RAD(expression)" + " -> " + txtFceRad + "\n"
				+ OutputEditor.TWO_GAPS + "DEG(expression)" + " -> " + txtFceDeg + "\n\n"
				
				// Příkaldy:
				+ OutputEditor.TWO_GAPS + txtWithoutSemicolonAtTheEndsOfExpression + "\n"
				+ OutputEditor.TWO_GAPS + txtIncludeVariablesFromCommandEditorInfo + ":\n"				
				+ OutputEditor.TWO_GAPS + "(" + txtVarNamesCantConstainNumbers + ")" + "\n"
				+ OutputEditor.TWO_GAPS + "(final) dataType variableName = value;" + "\n"
				+ OutputEditor.TWO_GAPS + "(3.4 + -variableName)/2" + "\n\n"
				
				// Nějkaé ukázky pro matematické výrazy,které je možné zadat (podmínky, funkce, jejich využití, ...)
				+ OutputEditor.TWO_GAPS + txtExamplesOfMathExpressions + ":\n"
				+ OutputEditor.TWO_GAPS + "2.4/PI" + "\n"				
				+ OutputEditor.TWO_GAPS + "random() > 0.5" + "\n"
				+ OutputEditor.TWO_GAPS + "SQRT(a^2 + b^2)" + " (" + txtDeclaredAndFulfilledVars + ": 'a' a 'b'." + ")" + "\n"				
				+ OutputEditor.TWO_GAPS + "not(x<7 || sqrt(max(x,9,3,min(4,3))) <= 3)" + " (" + txtDeclaredAndFulfilledVar + ": 'x'." + ")" + "\n"
				+ OutputEditor.TWO_GAPS + "log10(100)" + "\n"
				+ OutputEditor.TWO_GAPS + "if (1 < 2, true, false)" + "\n\n"
				
				// Instance třídy:
				+ OutputEditor.TWO_GAPS + txtMakeInstance + ":" + "\n"
				+ OutputEditor.TWO_GAPS + txtClassName + "\n"
				+ OutputEditor.TWO_GAPS + "model.data.ClassName variableName = new model.data.ClassName();" + "\n"
				+ OutputEditor.TWO_GAPS + txtMakeInstanceParameters_1 + " \n"
				+ OutputEditor.TWO_GAPS + txtMakeInstanceParameters_2 + " \n"
				+ OutputEditor.TWO_GAPS + txtMakeInstanceParameters_3 + "\n"
				+ OutputEditor.TWO_GAPS + txtWithParameters + ": " + "\n"
				+ OutputEditor.TWO_GAPS + "ClassName variabeName = new ClassName(true, false, 'x', \"Some text.\", referenceName.variableName--, referenceName.methodName(++variableName), -5.95);"
				+ "\n\n"
			
			// Volání konstruktoru:
				+ OutputEditor.TWO_GAPS + txtCallConstructorInfo_1 + "\n"
				+ OutputEditor.TWO_GAPS + txtCallConstructorInfo_2 + ":\n"
				+ OutputEditor.TWO_GAPS + "new packageName. (anotherPackage.) ClassName();" + "\n"
				+ OutputEditor.TWO_GAPS + "(" + txtCallConstructorInfo_3 + ")" + "\n\n"
			
				// Volání metody:
				+ OutputEditor.TWO_GAPS + txtCallMethodInfo + ":\n"
				+ OutputEditor.TWO_GAPS + "referenceName.methodName();" + "\n"
				+ OutputEditor.TWO_GAPS + txtCallStaticMethodInfo + ":\n"
				+ OutputEditor.TWO_GAPS + "packageName. (anotherPackageName.) ClassName.methodName();" + "\n"
                + OutputEditor.TWO_GAPS + txtCallStaticMethodInInnerClassInfo + ":\n"
				+ OutputEditor.TWO_GAPS + "packageName. (anotherPackageName.) ClassName.InnerClassName.methodName();" + "\n"
				+ OutputEditor.TWO_GAPS + "packageName. (anotherPackageName.) ClassName.InnerClassName.InnerInnerClassName.methodName();" + "\n"
				+ OutputEditor.TWO_GAPS + txtCallMethodInfo_1 + "\n"
				+ OutputEditor.TWO_GAPS + txtCallMethodInfo_2 + "\n\n"
			
				// Info ohledně zavolání zděděných metod nad třídou coby potomkem příslušné
				// třídy s danou metodou:
				+ OutputEditor.TWO_GAPS + txtInfoAboutMethodsTitle + ":\n"
				+ OutputEditor.TWO_GAPS + txtInfoAboutCallMethodFromObjectClass + "\n"
				+ OutputEditor.TWO_GAPS + txtCallMethodFromInheritedClassesInfo + "\n"
				+ OutputEditor.TWO_GAPS + "(" + txtCallStaticMethodFroomInheritedClassesInfo + ")" + "\n\n"
				
				
				// Inkrementace / Dekrementace proměnných:
				+ OutputEditor.TWO_GAPS + txtIncrementationDecrementation + ":\n"
				+ OutputEditor.TWO_GAPS + txtIncrementationDecrementation_1 + "\n"
				+ OutputEditor.TWO_GAPS + txtVariableIncrementation + ":\n"
				+ OutputEditor.TWO_GAPS + "++referenceName.variableName;" + "\n"
				+ OutputEditor.TWO_GAPS + "referenceName.variableName++;" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtVariableDecrementation + ":\n"
				+ OutputEditor.TWO_GAPS + "--referenceName.variableName;" + "\n"
				+ OutputEditor.TWO_GAPS + "referenceName.variableName--;" + "\n\n"

				+ OutputEditor.TWO_GAPS + txtIncrementationMyVariable + ":\n"
				+ OutputEditor.TWO_GAPS + "++variableName;" + "\n"
				+ OutputEditor.TWO_GAPS + "variableName++;" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtDecrementationMyVariable + ":\n"
				+ OutputEditor.TWO_GAPS + "--variableName;" + "\n"
				+ OutputEditor.TWO_GAPS + "variableName--;" + "\n\n"
			
				// Inkrementace / dekremetnace čísla:
				+ OutputEditor.TWO_GAPS + txtIncrementationDecrementationConstatnts + "\n"
				+ OutputEditor.TWO_GAPS + txtIncrementationConstants + ":\n"
				+ OutputEditor.TWO_GAPS + "++50;" + "\n"
				+ OutputEditor.TWO_GAPS + "50++;" + "\n\n"
			
				+ OutputEditor.TWO_GAPS + txtDecrementationConstants + ":\n"
				+ OutputEditor.TWO_GAPS + "--50;" + "\n"
				+ OutputEditor.TWO_GAPS + "50--;" + "\n\n"
			

				// Deklarace a naplnění proměnné v editoru příkazů:
				+ OutputEditor.TWO_GAPS + txtDeclarationOfVariable + ":\n"
				+ OutputEditor.TWO_GAPS + txtDeclarationVariableInfo + "\n"
				+ OutputEditor.TWO_GAPS + txtLikeThis + ": "
				+ "Byte, byte, Short, short, Integer, int, Long, long, Float, float, Double, double, Character, char, Boolean, boolean a String."
				+ "\n"
				+ OutputEditor.TWO_GAPS + txtDeclarationVariableInfo_2 + ":\n"
				+ OutputEditor.TWO_GAPS + txtSyntaxe + ":\n"
				+ OutputEditor.TWO_GAPS + "dataType variableName;" + "\n"
				+ OutputEditor.TWO_GAPS + "(final) dataType variableName = value;" + "\n"
				+ OutputEditor.TWO_GAPS + txtForString + ": " + "final String var = \"Some text.\";" + "\n"
				+ OutputEditor.TWO_GAPS + txtSyntaxeForChar + ": " + "final char var = 'x';" + "\n"
				+ OutputEditor.TWO_GAPS + txtSyntaxeForDouble + ": " + "final double var = 125.55;" + "\n"
				+ OutputEditor.TWO_GAPS + txtSimilaryDataTypes + "\n\n"
			
			
				// Naplnění proměnných v editoru příkazů a instanci třídy:
				+ OutputEditor.TWO_GAPS + txtFullfillmentVariable + ":\n"
				+ OutputEditor.TWO_GAPS + txtFullfilmentVariableInfo + "\n"
				+ OutputEditor.TWO_GAPS + txtFullfilmentVariableSyntaxeInfo + ":\n"
				+ OutputEditor.TWO_GAPS + txtFullfilmentVariableText + ": referenceName.variableName = value;" + "\n"
				+ OutputEditor.TWO_GAPS + txtFullfilmentMyVariable + ": variableName = value;" + "\n"
				+ OutputEditor.TWO_GAPS + txtFullfilmentReplaceInfo + ":\n"
				+ OutputEditor.TWO_GAPS + txtFullfilmentReturnValue + "\n"
				+ OutputEditor.TWO_GAPS + txtSyntaxe + ": variableName = referenceName.methodName();" + "\n"
				+ OutputEditor.TWO_GAPS + txtFullfilmentByStaticMethod + ":\n"
				+ OutputEditor.TWO_GAPS + txtSyntaxe + ": variableName = packageName. (anotherPackageName.) ClassName.methodName();" + "\n"
                + OutputEditor.TWO_GAPS + txtFullfilmentByStaticMethodInInnerClass + ":\n"
				+ OutputEditor.TWO_GAPS + txtSyntaxe + ": variableName = packageName. (anotherPackageName.) ClassName.InnerClassName.methodName();" + "\n"
				+ OutputEditor.TWO_GAPS + txtFullfilmentVariableSameRules + "\n"
				+ OutputEditor.TWO_GAPS + txtFullfilmentVariableInfo_2 + ":\n"
				+ OutputEditor.TWO_GAPS + txtSyntaxe + ": variableName = referenceName.variableName;" + "\n"
				+ OutputEditor.TWO_GAPS + txtSyntaxe + ": (final) int var = var2;" + "\n"
				+ OutputEditor.TWO_GAPS + txtFullfilmentEtc + "\n\n"
			
			
				// Metoda print();
				+ OutputEditor.TWO_GAPS + txtPrintMethodInfo + ": print();" + "\n"
				+ OutputEditor.TWO_GAPS + txtPrintMethodInfo_2 + "\n"
				+ OutputEditor.TWO_GAPS + txtPrintMethodInfo_3 + "\n"
				+ OutputEditor.TWO_GAPS + txtPrintMethodInfo_4 + "\n"
				+ OutputEditor.TWO_GAPS + txtPrintMethodSyntaxeExample + ":\n"
				+ OutputEditor.TWO_GAPS + "print(5 + \" plus \" + 5 + \" = 10\");" + "\n"
				+ OutputEditor.TWO_GAPS + txtPrintMethodWriteResult + ": 5 plus 5 = 10" + "\n"
				+ OutputEditor.TWO_GAPS + "print(\"" + txtPrintMethodReturnValue + ": \" + instanceVariable.getInt());" + "\n"
				+ OutputEditor.TWO_GAPS + txtPrintMethodWriteResult + ": " + txtPrintMethodReturnValue + ": -1" + "\n\n"
			
			
				// Vytvoření kolekce - Listu a možné - podporované operace s ním:
				+ OutputEditor.TWO_GAPS + txtDeclarationListInfo + ":\n"
				+ OutputEditor.TWO_GAPS + "(final) List<DataType> referenceName = new ArrayList<>();" + "\n"
				+ OutputEditor.TWO_GAPS + txtDeclarationListInfo_2 + ":\n"
				+ OutputEditor.TWO_GAPS + "(final) List<DataType> referenceName = new ArrayList<>(parameter1, parameter2, ...);" + "\n"
				+ OutputEditor.TWO_GAPS + txtDeclarationListInfo_3 + ": "
				+ "Byte, Short, Integer, Long, Float, Double, Character, Boolean, String." + "\n"
				+ OutputEditor.TWO_GAPS + txtNote + ": " + txtDeclarationListInfo_4 + "\n"
				+ OutputEditor.TWO_GAPS + txtDeclarationListInfo_5 + "\n\n"
			
				// Metody nad kolekcí:
				+ OutputEditor.TWO_GAPS + txtListMethodInfo_1 + ":\n"
				+ OutputEditor.TWO_GAPS + txtListMethodInfo_2 + ":\n"
				+ OutputEditor.TWO_GAPS + "variableList.add(value);" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtListMethodInfo_3 + ":\n"
				+ OutputEditor.TWO_GAPS + "variableList.add(index, value);" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtListMethodInfo_4 + ":\n"
				+ OutputEditor.TWO_GAPS + "variableList.clear();" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtListMethodInfo_5 + ":\n"
				+ OutputEditor.TWO_GAPS + "variableList.get(index);" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtListMethodInfo_6 + ":\n"
				+ OutputEditor.TWO_GAPS + "variableList.set(index, value);" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtListMethodInfo_7 + ":\n"
				+ OutputEditor.TWO_GAPS + "variableList.toArray();" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtListMethodInfo_8 + ":\n"
				+ OutputEditor.TWO_GAPS + "variableList.size();" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtListMethodInfo_9 + ":\n"
				+ OutputEditor.TWO_GAPS + "variableList.remove(index);" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtNote + ": " + txtListMethodInfo_10 + "\n\n"
			
			
				+ OutputEditor.TWO_GAPS + txtDeclarationOneDimensionalArray + "\n"
				+ OutputEditor.TWO_GAPS + "1. " + txtOneDimArrayInfo_1 + ":\n"
				+ OutputEditor.TWO_GAPS + "(final) dataType[] arrayName = new dataType[size];" + "\n"
				+ OutputEditor.TWO_GAPS + "2. " + txtOneDimArrayInfo_2 + ":\n"
				+ OutputEditor.TWO_GAPS + "(final) dataType[] arrayName = new dataType[] {value1, value2, ...};" + "\n"
				+ OutputEditor.TWO_GAPS + "3. " + txtOneDimArrayInfo_2 + ":\n"
				+ OutputEditor.TWO_GAPS + "(final) dataType[] arrayName = {value1, value2, ...};" + "\n"
				+ OutputEditor.TWO_GAPS + "4. " + txtOneDimArrayInfo_3 + ":\n"
				+ OutputEditor.TWO_GAPS + "(final) dataType[] arrayName = referenceName.methodName();" + "\n"
				+ OutputEditor.TWO_GAPS + "(final) dataType[] arrayName = packageName. (anotherPackageName.) ClassName.methodName();" + "\n"
				+ OutputEditor.TWO_GAPS + "(final) dataType[] arrayName = packageName. (anotherPackageName.) ClassName.InnerClassName.methodName();" + "\n"
				+ "\n"
				+ OutputEditor.TWO_GAPS + txtDataTypeArrayCanBe
				+ ": byte, Byte, short, Short, int, Integer, long, Long, float, Float, double, Double, char, Character, boolean, Boolean, String."
				+ "\n\n"
			
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray + ":\n"
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_1 + ":\n"
				+ OutputEditor.TWO_GAPS + "arrayName[index] = value;" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_2 + ":\n"
				+ OutputEditor.TWO_GAPS + "arrayName[index] ++;" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_3 + ":\n"
				+ OutputEditor.TWO_GAPS + "++ arrayName[index];" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_4 + ":\n"
				+ OutputEditor.TWO_GAPS + "arrayName[index] --;" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_5 + ":\n"
				+ OutputEditor.TWO_GAPS + "-- arrayName[index];" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_7 + ":\n"
				+ OutputEditor.TWO_GAPS + "arrayName[index];" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_8 + ":\n"
				+ OutputEditor.TWO_GAPS + "arrayName.length;" + "\n\n"
			
				+ OutputEditor.TWO_GAPS + txtFullfilmentArrayInfo_1 + "\n"
				+ OutputEditor.TWO_GAPS + txtFullfilmentArrayInfo_2 + "\n"
				+ OutputEditor.TWO_GAPS + txtFullfilmentArrayInfo_3 + "\n\n"
			
			
				// Informace pro jednorozměrné pole v instanci třídy:
				+ OutputEditor.TWO_GAPS + txtOperationsWithArrayinIntanceTitle + ":\n"
				+ OutputEditor.TWO_GAPS + txtOperationsWithArrayInInstanceInfo + "\n"
				+ OutputEditor.TWO_GAPS + txtOperationsWithArrayInInstanceDifference + "\n\n"
				
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray + ":\n"
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_1 + ":\n"
				+ OutputEditor.TWO_GAPS + "referenceName.arrayName[index] = value;" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_2 + ":\n"
				+ OutputEditor.TWO_GAPS + "referenceName.arrayName[index] ++;" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_3 + ":\n"
				+ OutputEditor.TWO_GAPS + "++ referenceName.arrayName[index];" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_4 + ":\n"
				+ OutputEditor.TWO_GAPS + "referenceName.arrayName[index] --;" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_5 + ":\n"
				+ OutputEditor.TWO_GAPS + "-- referenceName.arrayName[index];" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_7 + ":\n"
				+ OutputEditor.TWO_GAPS + "referenceName.arrayName[index];" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_8 + ":\n"
				+ OutputEditor.TWO_GAPS + "referenceName.arrayName.length;" + "\n\n"
				
	
				// Informace pro dourozměrné pole v instanci třídy:
				+ OutputEditor.TWO_GAPS + txtTwoDimArrayTitle + ":\n"
				+ OutputEditor.TWO_GAPS + txtWorkWithTwoDimArrayInfo + "\n"
				+ OutputEditor.TWO_GAPS + txtCannotCreateTwoDimArray + "\n\n"
				
				+ OutputEditor.TWO_GAPS + txtOperationsWithTwoDimArray + ":\n"
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_1 + ":\n"				
				+ OutputEditor.TWO_GAPS + "referenceName.arrayName[index_1] [index_2] = value;" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_2 + ":\n"
				+ OutputEditor.TWO_GAPS + "referenceName.arrayName[index_1] [index_2] ++;" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_3 + ":\n"
				+ OutputEditor.TWO_GAPS + "++ referenceName.arrayName[index_1] [index_2];" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_4 + ":\n"
				+ OutputEditor.TWO_GAPS + "referenceName.arrayName[index_1] [index_2] --;" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_5 + ":\n"
				+ OutputEditor.TWO_GAPS + "-- referenceName.arrayName[index] [index_2];" + "\n"
			
				+ OutputEditor.TWO_GAPS + txtOperationsWithArray_7 + ":\n"
				+ OutputEditor.TWO_GAPS + "referenceName.arrayName[index_1] [index_2];" + "\n\n"
			
				
				
				// že nejsou podporovány null hodnoty:
				+ OutputEditor.TWO_GAPS + txtInfoAboutNullValue + "\n\n"
		
		
				// Info ohledně metod pro restartování a ukončení aplikace pomocí editoru příkazů:
				+ OutputEditor.TWO_GAPS + txtInfoForMethodsOverApp + "\n"
				
				// Restartování aplikace:
				+ OutputEditor.TWO_GAPS + txtRestartAppTitle + ":\n"
				// Info k restartu - že se uložídiagram tříd apod.
				+ OutputEditor.TWO_GAPS + txtInfoAboutCloseOpenFilesBeforeRestartApp + "\n"
				+ OutputEditor.TWO_GAPS + txtInfoAboutLaunchCountdownWhenCommandIsEntered + "\n"
				+ OutputEditor.TWO_GAPS + txtInfoAboutSaveClassDiagramBeforeRestartApp + "\n\n"
				
				+ OutputEditor.TWO_GAPS + txtTwoWaysToRestartApp + ":\n"
				+ OutputEditor.TWO_GAPS + "1. " + txtFirstWayToRestartApp_1 + " " + DEFAULT_VALUE_FOR_COUNTDOWN + " " + txtFirstWayToRestartApp_2 + "\n"
				+ OutputEditor.TWO_GAPS + "restart();" + "\n"
				
				+ OutputEditor.TWO_GAPS + "2. " + txtSecondWayToRestartApp + " <0 ; 59>." + "\n"			
				+ OutputEditor.TWO_GAPS + "restart(X);" + "\n\n"
				
				
				
				// Ukončení aplikace
				+ OutputEditor.TWO_GAPS + txtCloseAppTitle + ":\n"
				+ OutputEditor.TWO_GAPS + txtInfoAboutCloseOpenFilesBeforeCloseApp + "\n"										  
				+ OutputEditor.TWO_GAPS + txtInfoAboutLaunchCountdownAfterCommandIsEntered + "\n"
				+ OutputEditor.TWO_GAPS + txtInfoAboutSaveClassDiagramBeforeCloseApp + "\n\n"
				
				+ OutputEditor.TWO_GAPS + txtThreeWaysToCloseApp + ":\n"
				+ OutputEditor.TWO_GAPS + "1. " + txtFirstWayToCloseApp_1 + " " + DEFAULT_VALUE_FOR_COUNTDOWN + " " + txtFirstWayToCloseApp_2 + "\n"
				+ OutputEditor.TWO_GAPS + "exit();" + "\n"

				+ OutputEditor.TWO_GAPS + "2. " + txtSecondWayToCloseApp + " <0 ; 59>." + "\n"
				+ OutputEditor.TWO_GAPS + "exit(X);" + "\n"
				
				+ OutputEditor.TWO_GAPS + "3. " + txtThirdWayToCloseApp_1 + " " + DEFAULT_VALUE_FOR_COUNTDOWN + " " + txtThirdWayToCloseApp_2 + "\n"
				+ OutputEditor.TWO_GAPS + "(" + txtInfoAboutThirdWayToCloseApp_1 + "\n"
				+ OutputEditor.TWO_GAPS + txtInfoAboutThirdWayToCloseApp_2 + ")" + "\n"
				+ OutputEditor.TWO_GAPS + "System.exit(0);" + "\n\n"

				

				// Zrušení odpočtu při restartování nebo ukončení aplikace:
				+ OutputEditor.TWO_GAPS + txtCancelCountdownTitle + ":\n"
				+ OutputEditor.TWO_GAPS + txtCancelCountdownInfo + "\n"
				+ OutputEditor.TWO_GAPS + "cancelCountdown();" + "\n";
		
		
		
		
		/*
		 * AŽ ZDE BUDU PSAT TU KOLEKCI, TAK DOPLNIT POZNAMKU, ZE SE DO ZAVOLRKY V
		 * ARRAYLIST NEPRIDAVAJI PRIMO HODNOTY, ALE DAVAJI SE V DO POLE A TO SE PAK
		 * PREVEDE!!!, POUZE JSEM TO PRO TYTO UCELY ZJEDNODUSIL!!! SYNTAXE:
		 * List<Integer> var = new ArrayList<>(Arrays.asList(5, 10, 5)); a ne, tak jak
		 * to delam: List<Integer> var = new ArrayList<>(5, 10, 5);
		 * 
		 * 
		 * OPERACE S KOLEKCI: deklarace s naplnenim a bez pridani hodnoty na konec
		 * add(value); pridani hodnoty na index add(index, value); clear - vymaze obsah
		 * kolekce clear(); ziskani hodnoty z kolekce na indexu get(index); nastaveni
		 * hodnoty na indexu: set(index, value); konverze do jednorozmerneho pole:
		 * .toArray(); zjisteni velikosti kolekce - listu: .size(); odebrani hodnoty z
		 * indexu: .remove(index);
		 * 
		 * 
		 * - ZDURAZNIT, ZE PRO ZISKANI HODNOTY A PRIDANI HODNOTY NA INDEX, SE BEROU
		 * POUZE CISLA A NEM PROMENNE
		 * 
		 * 
		 * 
		 * JEDNOROZMERNE POLE A OPERACE: deklarace s velikost pole: dataType[] var = new
		 * dataType[size]; deklarace s naplnenm: dataType[] var = new dataType[] {value,
		 * valu2, ...}; deklarace s naplnenm: dataType[] var = {value, valu2, ...};
		 * 
		 * 
		 * 
		 * Note: u vypisování vteřinpro restartování aplikace jsem nedoplňoval pevné
		 * definování vteřin -> 1 vteřina, 2 vteřiny, ... Navíc je toto pouze v českém
		 * jazyce, pokud jej uživatel přepne na jakýkoliv jiný jazyk, byla by to práce
		 * navíc. Už jen když zvážím fakt, kdo to všechno bude číst.
		 */
	}
	
	
	


	
	
	
	
	@Override
	public String closeAppByExitCommand() {		
		// Spuštění odpočtu pro ukončení aplikace:
		return crateTimerForCloseApp(DEFAULT_VALUE_FOR_COUNTDOWN);
	}

	
	
	
	@Override
	public String closeAppByExitWithCountCommand(final String command) {
		// příkaz: exit( 0 - 59 );
		
		/*
		 * Získám si text s počtem vteřin - mezi kulatými závorkami:
		 */
		final String countInText = command.substring(command.indexOf('(') + 1, command.indexOf(')')).replaceAll("\\s",
				"");
		
		/*
		 * Přetypuji si zadaný počet vteřin v parametru metody na číslo datového typu
		 * int, zde již nemusím zachytývat výjimku NumberFormatException, protože již
		 * vím, že to bude OK, protože regulráním výrazem jiné hodnoty neprojdou.
		 */
		final int count = Integer.parseInt(countInText);
			
		// Spustím timer se zadanou hodnotou:
		return crateTimerForCloseApp(count); 
	}
	
	
	
	
	
	@Override
	public String closeAppBySystemExitCommand() {
		// Spuštění odpočtu pro ukončení aplikace:
		return crateTimerForCloseApp(DEFAULT_VALUE_FOR_COUNTDOWN);
	}
		
	
	
	
	@Override
	public String restartAppByRestartCommand() {
		// Spuštění odpočtu pro restartování aplikace:
		return crateTimerForRestartApp(DEFAULT_VALUE_FOR_COUNTDOWN);
	}

	
	
	
	@Override
	public String restartAppWithCountByRestartCommand(final String command) {
		// Syntaxe: restart( 0 - 59 );
		
		/*
		 * Vezmu si počet vteřin, který je mezi kulatými závorkami:
		 */
		final String countInText = command.substring(command.indexOf('(') + 1, command.indexOf(')')).replaceAll("\\s",
				"");

		/*
		 * Přetypuji si zadaný počet vteřin v parametru metody na číslo datového typu
		 * int, zde již nemusím zachytývat výjimku NumberFormatException, protože již
		 * vím, že to bude OK, protože regulráním výrazem jiné hodnoty neprojdou.
		 */
		final int count = Integer.parseInt(countInText);

		// Spustím odpočet:
		return crateTimerForRestartApp(count);
	}
	
	
	
	
	
	
	
	@Override
	public String cancelCountdown() {		
		String result = txtNoCountdownIsRunning;

		if (timerForCloseApp != null && timerForCloseApp.isRunning()) {
			timerForCloseApp.stop();			
			result = txtCountdownIsStopped;
		}

		if (timerForRestartApp != null && timerForRestartApp.isRunning()) {
			timerForRestartApp.stop();
			result = txtCountdownIsStopped;
		}

		return result;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří instanci objektu Timer, který bude odpočítávat čas do
	 * ukončení aplikace, a až odpočet dojde na nulu, tak se aplikace ukončí.
	 * 
	 * @param timeToEndTheApp
	 *            - odpočet,který se má nastavit, resp. čas ve vteřinách, který se
	 *            má nastavit. Tento čas značí počet vteřin, za který se ukončí
	 *            aplikace až dojde odpočet vtečin k nule.
	 * 
	 * @return Úspěch, pokud se timer vytvoří v pořádku, jiank neúspěch, pokud je
	 *         již třeba spuštěň udpočet pro restartování aplikace.
	 */
	private String crateTimerForCloseApp(final int timeToEndTheApp) {
		if (timerForCloseApp != null && timerForCloseApp.isRunning())
			return txtFailure + ": " + txtCountdownForCloseAppIsRunning;
		
		if (timerForRestartApp != null && timerForRestartApp.isRunning())
			return txtFailure + ": " + txtCountdownForCloseAppIsNotPossibleToStart;
		
		timerForCloseApp = new Timer(1000, new ActionListener() {
			
			int index = timeToEndTheApp;
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// Zde již vypršel čas, tak mohu ukončit aplikaci:
				if (index == 0) {
					/*
					 * Nejprve uložím potřebná data, aby bylo možné je opět při spuštění aplikace
					 * obnovit (Uloží se data jako je například, diagram tříd v otevřeném projektu -
					 * pokud je, a umístění otevřeného projektu apod.).
					 */
					App.saveDataBeforeCloseApp();
					
					// Ukončím aplikaci:
					System.exit(0);
				}
				
				// Zde zobrazím uživateli čas, který zbývá do ukončení aplikace:
				if (index > 4 && index < 60)
					outputEditor.addResult(txtToEndOfAppRemains_1 + ": " + index + " " + txtSecondsForClose_1);
				
				else if (index > 1)
					outputEditor.addResult(txtToEndOfAppRemains_2 + ": " + index + " " + txtSecondsForClose_2);

				else
					outputEditor.addResult(txtToEndOfAppRemains_3 + ": " + index + " " + txtSecondsForClose_3);
				
				// Dekrementuji odpočet:
				index--;
			}
		});
		
		timerForCloseApp.start();
		
		return txtSuccess;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří instanci objektu Timer, který bude odpočítávat čas do
	 * restartování aplikace, a až odpočet dojde na nulu, tak se aplikace
	 * restartuje.
	 * 
	 * @param timeToRestartTheApp
	 *            - odpočet,který se má nastavit, resp. čas ve vteřinách, který se
	 *            má nastavit. Tento čas značí počet vteřin, za který se restartuje
	 *            aplikace až dojde odpočet vtečin k nule.
	 * 
	 * @return Úspěch, pokud se timer vytvoří v pořádku, jiank neúspěch, pokud je
	 *         již třeba spuštěň udpočet pro ukončení aplikace.
	 */
	private String crateTimerForRestartApp(final int timeToRestartTheApp) {
		if (timerForRestartApp != null && timerForRestartApp.isRunning())
			return txtFailure + ": " + txtCountdownForRestartAppIsRunning;
		
		if (timerForCloseApp != null && timerForCloseApp.isRunning())
			return txtFailure + ": " + txtCountdownForRestartAppIsNotPossibleToStart;
		
		timerForRestartApp = new Timer(1000, new ActionListener() {
			
			int index = timeToRestartTheApp;
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// Zde již vypršel čas, tak mohu restartovat aplikaci:
				if (index == 0)
					app.restartApp();
				
				
				// Zde zobrazím uživateli čas, který zbývá do ukončení aplikace:
				if (index > 4 && index < 60)
					outputEditor.addResult(txtToRestartAppRemains_1 + ": " + index + " " + txtSecondsForRestart_1);
				
				else if (index > 1)
					outputEditor.addResult(txtToRestartAppRemains_2 + ": " + index + " " + txtSecondsForRestart_2);

				else
					outputEditor.addResult(txtToRestartAppRemains_3 + ": " + index + " " + txtSecondsForRestart_3);
				
				// Dekrementuji odpočet:
				index--;
			}
		});
		
		timerForRestartApp.start();
		
		return txtSuccess;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public String printTempVariables() {
		/*
		 * Získám si veškěré proměnné, které byly vytvořeny uživatelem v editoru
		 * příkazů.
		 */
		final Map<String, Object> variablesMap = Instances.getAllVariablesFromCommandEditor();
		
		if (variablesMap.isEmpty())
			return txtNoVariablesWasCreated;		
		
		
		
		/*
		 * Veškeré proměnné vytvořené v editoru příkazů vypíšu do editoru výstupů v syntaxi:
		 * 
		 * "Klasická" proměnné:
		 * (final) dataType name = value;
		 * 
		 * List:
		 * (final) List<dataType> name = [values];
		 * 
		 * Pole:
		 * (final) dataType[] name = [values];
		 */		
		
		
		variablesMap.forEach((key, value) -> {
            String textOfVariable = "";

            // Podmínka s instanceof by neměla být potřeba
            if (value instanceof VariableAbstract && ((VariableAbstract) value).isFinal) textOfVariable += "final ";


            /*
             * Nyní si zjistím o jaké konkrétní typ proměnné se jedná a dle toho si získám
             * hodnotu:
             */
            if (value instanceof Variable) {
                // Zde připojím název a hodnotu:
                final Variable variable = (Variable) value;

                textOfVariable += variable.getDataTypeOfVariable().getSimpleName() + " " + key + " = "
                        + variable.getObjValue() + ";";
            } else if (value instanceof VariableArray<?>) {
                final VariableArray<?> variable = (VariableArray<?>) value;

                textOfVariable += variable.getTypeArray().getSimpleName() + "[] " + key + " = "
                        + fromArrayToString(variable.getArray()) + ";";
            } else if (value instanceof VariableList<?>) {
                final VariableList<?> variable = (VariableList<?>) value;

                textOfVariable += "List<" + variable.getClassType().getSimpleName() + "> " + key + " = "
                        + variable.getList() + ";";
            }

            outputEditor.addResult(textOfVariable);
        });
		
		
		return txtSuccess;
	}
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	
	@Override
	public String computeMathematicalExpression(final String command) {
		/*
		 * Zde se jedná o nějaký matematický výraz, tak si pro začátek zkusím zjistit,
		 * zda obsahuje nějaké textové proměnné - nějaká písmena, pokud ano, tak zkusím
		 * zjistit, zda se jedná o "dočasné" proměnné vytvořené uživatelem v editoru
		 * příkazů, pokud ano, zjistím, zda obsahují čísla a pokud jsou tyto podmínky
		 * splněny, pak mohu za tyto proměnné doplnit příslušná čísla z daných
		 * proměnných a vypočítat příslušný výraz.
		 * 
		 * Pokud nebudou nalezeny žádné textové proměnné, pak mohu výraz vypočítat, nebo
		 * pokud budou nalezeny proměnné, ale nejedná se o vytvořeé proměnné z editoru
		 * příkazů, například sqrt pro odmocninu a další funkce (například
		 * goniometrické).
		 */
		

		/*
		 * Pro začátek si vytvořím výraz, který by seměl vypočítat, abych do měl
		 * případně mohl rovnou vkládat hodnoty za proměnné (následující cyklus).
		 */
		final Expression expression = new Expression(command);
		
		
		
		/*
		 * Získám si list, který obsahuje veškeré textové proměnné, a zjistím, zda se
		 * jedná o proměnné z editoru příkazů, případně si načtu jejich hodnoty.
		 */
		final List<String> variables = getGroupsOfText(command);
		
		
		// Vložím do výše vytvořeného výrazu proměnné vytvořené v editoru příkazů, které
		// se rozpoznají:
		setVariablesToExpression(expression, variables);
		
		
		
		/*
		 * Nyní jsem v pozici, kdy se mohu pokusit vypočítat zadaný výraz, tak nad
		 * výrazem zavolám metodu eval a pokud se výpočet povede, tak jej vrátím. Pokud
		 * se výpočet nepovede a dojde k nějakém chybě, npříklad chybí nebo přebývá
		 * závorka, nebo nebyla specifikována proměnná atd. Pak vrátím text s chybou, ke
		 * které došlo, kterou si získám z nastalé výjimky.
		 */
		try {
			final BigDecimal result = expression.eval();

			return txtSuccess + ": " + result;
		} catch (ExpressionException e) {
			return txtFailure + ": " + e.getMessage();
		}
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která do matematického výrazu - expression nastaví proměnné, které
	 * uživatel mohl dříve vytvořit v editoru příkazů. Názvy těchto proměnných jsou
	 * v list variables, ze kterého se zkusí získat z mapy se všemi instancemi všech
	 * tříd a proměnných v editoru příkazů.
	 * 
	 * @param expression
	 *            - matematický výraz zadaný uživatelem a vložený do objektu
	 *            Expression z použité knihovny.
	 * 
	 * @param variables
	 *            list typu String,který obsahuje veškeré textové názvy proměnných v
	 *            daném výrazu.
	 */
	private static void setVariablesToExpression(final Expression expression, final List<String> variables) {
		// Pokud je list prázdný, nemá smysl pokračovat:
		if (variables.isEmpty())
			return;
		
		variables.forEach(v -> {
			final Object objVariable = Instances.getVariableFromMap(v);
			
			/*
			 * zde testuji, zda se ve vytvořených instancích nachází nějaká "proměnná", s
			 * referencí v a je to instance Variable. Vzhledem k metodě getVariableFromMap
			 * by ani nemělo nastat, že by se nejednalo o instanci třídy Variable, tak to
			 * ani nemusím testoat.
			 */
			if (!(objVariable instanceof Variable))
				return;
			
			final Variable variable = (Variable) objVariable;
			
			/*
			 * Nyní otestuji, zda se jedná o vytvořenou "proměnnou" typu jednoho z
			 * primitivních datových typů pro číslo, protože jiné datové typy do objektu
			 * Variable ani uložit nelze - pak už jen text a znak, ale v tomto případě, bych
			 * testovatl stejně, zda se jedná o datový typ pro číslo, navíc bych mohl
			 * testovat, zda je to například BigDecimal apod. Ale to uživateli nedovoluji
			 * vytvořit, tak to zde ani nebudu estovat.
			 */
			if (isNumber(variable.getDataTypeOfVariable()))
				// Přidám do výrazu hodnotu proměnné:
				expression.and(v, variable.getObjValue().toString());
		});
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která si z peramaetru pattern vezme všechny "proměnné", tj. jedná se
	 * o alespoň jeden znak coby velké nebo malá písmeno bez diakritiky nebo
	 * podtržítko.
	 * 
	 * @param pattern
	 *            - zadaný matematický výpočet.
	 * 
	 * @return list, který obsahuje veškeré získané textvé hodnoty.
	 */
	private static List<String> getGroupsOfText(final String pattern) {
		final List<String> list = new ArrayList<>();
		
        final Pattern p = Pattern.compile("([a-zA-Z_])+");
        final Matcher m = p.matcher(pattern);
        
        while(m.find())
        	list.add(m.group());
        
        return list;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o jeden z Javovských datových typů pro
	 * číslo. Jedná se pouze o promitivní datové typy, protože v objektu Variable
	 * jiné typy ani nepoužívám, tak stačí jen hlídat ty.
	 * 
	 * @param clazz
	 *            - datový typ, který se má otestovat, zda se jedná o jeden z
	 *            datových typů pro číslo.
	 * 
	 * @return true, pokud clazz je jeden z primitivních datových typů pro číslo,
	 *         jinak false.
	 */
	private static boolean isNumber(final Class<?> clazz) {
        return clazz.equals(byte.class) || clazz.equals(Byte.class) || clazz.equals(short.class)
                || clazz.equals(Short.class) || clazz.equals(int.class) || clazz.equals(Integer.class)
                || clazz.equals(long.class) || clazz.equals(Long.class) || clazz.equals(float.class)
                || clazz.equals(Float.class) || clazz.equals(double.class) || clazz.equals(Double.class);
    }
	
	
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí (pokud najde) nalezený text s názvem třídy - případně i
	 * balíčky v podobě model.data.ClassName nebo ClassName apod.
	 * 
	 * @param partOfCommand
	 *            = jednorozměrné pole, ve kterém se nachází část uživatelem
	 *            zadaného příkazu (buď ta první nebo druhá - dle toho jakou část
	 *            chci najít) pro vytvoření instance třídy
	 * 
	 *            (první část - model.data.ClassName nebo ClassName , druhá část:
	 *            model.Datata.CLassName(), ClassName())
	 * 
	 * @param firstPart
	 *            = logická hodnota, pro to zda mám hledat text názvu třídy v první
	 *            části příkazu nebo v druhé
	 * 
	 * @return buď prázdný text, pokud nenajde název třídy (nemělo by nastat) nebo název
	 *         třídy
	 */
	private static String getTextOfClassName(final String[] partOfCommand, final boolean firstPart) {
		for (final String s : partOfCommand) {
			// Zde potřebuji zjistit, zda to neni jen mezera ci jiny bily znak ale třída případně i s balíčky
			if (firstPart) {
				if (regEx.isClassName(s))
					// Zde se jedná o název třídy
					return s.replaceAll("\\s", "");		
			}
			
			else {
				if (regEx.isClassNameWithBracket(s))
					// Zde se jedná o název třídy v druhé části příkazu (se závorkami)
					return s.trim();
			}
		}
		return "";
	}
	
	
	
	
	
	
	

	
	
	
	
	/**
	 * Metoda, která otestuje, zda již existuje instance třídy s daným názvem
	 * reference na ní nebo ne
	 * 
	 * @param referenceName
	 *            - název reference na instanci třídy, o níž chci vědět, zda již
	 *            existuje nebo ne
	 * 
	 * @return true, pokud název reference již existuje, jinak false
	 */
	private static boolean existReferenceName(final String referenceName) {
		// Otestuji, zda ještě neexistuje instance nějaké třídy s příslušným názvem reference:
		for (final String s : Instances.getListOfVariablesOfClassInstances()) {
			if (s.equalsIgnoreCase(referenceName))
				return true;
		}
		
		return false;
	}

	
	
	



	
	
	
	
	
	
	
	/**
	 * Metoda, která získá správné hodnoty do parametrů pro konstruktor či metodu,
	 * všechny parametry - oddělené čárkou otestuje, zda se jedná o proměnnou
	 * metodu, konstantu, ... případně ji zavolá a vrátí výsledek, porovná datové
	 * typy, a pokud se shodují přidá výsledek do kolekce, kterou vrátí
	 * 
	 * @param partsOfParameters
	 *            - parametry získané z textu příkazů pro zavolání metody či
	 *            konstruktoru (hodnoty oddělené čárkou mezi ())
	 * 
	 * @param c
	 *            - konstruktor, který se má zavolat, tato metoda si pouze vezme
	 *            jeho paramtry
	 * 
	 * @param m
	 *            - metody, která se má zavolat, tato metody si z ní pouze vezme
	 *            parametry pro otestování datových tapů
	 * 
	 * @return list výslednych hodnot z metody, ... nebo null, pokud se nenajde
	 *         shoda s datovými typy metod či konstruktoru a zadané hodnoty
	 */
	private List<Object> getListOfParsedParameters(final String[] partsOfParameters, final Constructor<?> c,
			final Method m) {
		// V této metodě se shoduje počet parametrů, tak nejprve zkontroluji zadaný příkaz jestli neobsahuje volání vytvoření instance
		// nějaké třídy, nebo metodu, která vrací nějakou hodnotu, nebo získání hodnoty z proměnné, vytvoření pole, listu, apod.
		
		final List<Object> parametersList = new ArrayList<>();
		
		
		
		// Jednorotzměnrné pole parametru, do kterého vložím parametru konstruktoru nebo metody:
		final Parameter[] parameters;
		
		if (c != null)
			parameters = c.getParameters();
		else parameters = m.getParameters();
		
		// Index pro pozici parametru v poli - výše - parameters
		int index = 0;
		
		
		
		for (final String s : partsOfParameters) {
			final ResultValue objValueInfo = getValueFromVar_Method_Constant(s, parameters[index].getType());

			// Zde otestuji, jestli se vyplnila hodnota nebo ne:
			if (objValueInfo.isExecuted())
				// Zde se vyplnila hodnota, tak ji mohu vložit do kolekce
				parametersList.add(objValueInfo.getObjResult());

			// Zde nebylo nic vyplneno, takže nebyla nalezena žádná hodnota pro daný
			// parametr, tak vrátím null, a jdu hledat
			// další parametr či konstruktor:
			else
				return null;

			index++;
		}

		return parametersList;
	}


	
	
	
	
	
	
	
	






	/**
	 * Metoda, která zjistí, zda se v proměnné objValue nachází celé nebo desetinné
	 * číslo, pokud ano, tak ho přetypuje na Big Integer nebo Decimal - dle toho,
	 * zda je desetinné nebo ne, a provede jeho "negaci" ze záporné hodnoty udělá
	 * kladnou a naopak, tuto hodnotu pak vrátí, s tím, že dané číslo pak přetypuje
	 * zpět na původní datový typ
	 * 
	 * @param dataType
	 *            - datový typ proměnné
	 * 
	 * @param objValue
	 *            - hodnota proměnné pro negaci
	 * 
	 * @return znegovanou hodnotu proměnné
	 */
	private Object getNegationOfValue(final Class<?> dataType, Object objValue) {
		// Otestuji, zda se jedná o celé číslo nebo destinné - dle toho mohu použít Big
		// Double nebo Integer
		
		if (objValue != null) {
			if (regEx.isInteger(objValue.toString())) {
				final BigInteger bi = new BigInteger(objValue.toString());
				Long l = bi.longValue();
				
				// V této podmínce mám dát mínus před hodnotu proměnné - znegovat jí 
				l = -l;
				objValue = getParsedParameter(dataType, l.toString());
			}
			
			else if (regEx.isDecimal(objValue.toString())) {									
				final BigDecimal bd = new BigDecimal(objValue.toString());
				Double d = bd.doubleValue();
				
				d = -d;
				objValue = getParsedParameter(dataType, d.toString());
			}
		}
		
		return objValue;
	}
	
	
	
	
	
	
	






	
	
	
	
	/**
	 * Metoda, která vrátí zadanou hodnotu v naparsováném typu
	 * 
	 * @param parameterType
	 *            - typ na který se má hodnota přetypovat
	 * 
	 * @param textToParse
	 *            - hodnoty typu String, která se má přetypovat na typ v parametru
	 *            výše
	 * 
	 * @return přetypovaný text v parametru
	 */
	private Object getParsedParameter(final Class<?> parameterType, final String textToParse) {
		
		if (parameterType != null) {
			if (parameterType.equals(Byte.class) || parameterType.equals(byte.class))
				return new ByteTypeParser().parse(textToParse);

			else if (parameterType.equals(Short.class) || parameterType.equals(short.class))
				return new ShortTypeParser().parse(textToParse);

			else if (parameterType.equals(Integer.class) || parameterType.equals(int.class))
				return new IntegerTypeParser().parse(textToParse);

			else if (parameterType.equals(Long.class) || parameterType.equals(long.class))
				return new LongTypeParser().parse(textToParse);

			else if (parameterType.equals(Float.class) || parameterType.equals(float.class))
				return new FloatTypeParser().parse(textToParse);

			else if (parameterType.equals(Double.class) || parameterType.equals(double.class))
				return new DoubleTypeParser().parse(textToParse);

			else if (parameterType.equals(Character.class) || parameterType.equals(char.class))
				return new CharacterTypeParser().parse(textToParse);

			else if (parameterType.equals(Boolean.class) || parameterType.equals(boolean.class))
				return new BooleanTypeParser().parse(textToParse);

			else if (parameterType.equals(String.class))
				return textToParse;


                /*
                 * Pokud se jedná o nějaký jiný datový typ, který jsem neuvedl, tak zkusím zavolat konstruktor s
                 * parametry typu String (pokud takový má)
                 * a tím převést hodnotu textToParse na příslušný datový typ, toto ale platí pouze pro objektové
                 * datové typy, primitivní jsou výše uvedeny.
                 *
                 * Ale k tomu by dojít nemělo.
                 */
            else {
                try {
                    final Constructor<?> constructor = parameterType.getDeclaredConstructor(String.class);

                    return constructor.newInstance(textToParse);

                } catch (NoSuchMethodException e) {
                    /*
                     * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                     * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                     * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                     * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                     */
                    final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                    // Otestuji, zda se podařilo načíst logger:
                    if (logger != null) {
                        // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                        // Nejprve mou informaci o chybě:
                        logger.log(Level.INFO, "Zachycena výjimka při pokusu o parsování hodnoty na typ: "
                                + parameterType + ", hodnota: " + textToParse
                                + ", třída: MakeOperation.java, metoda: getParsedParameter. K této chybě mohlo dojít " +
                                "například, že nebyla nalezen potřebná metoda, konkrétně v tomto případě " +
                                "konstruktor, která se má zavolat.");

                        /*
                         * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                         * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                         * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                         */
                        logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                    }
                    /*
                     * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                     */
                    ExceptionLogger.closeFileHandler();
					
					outputEditor.addResult(txtFailedToCastValue + ": " + textToParse + " " + txtToDataType + ": "
							+ parameterType + "!");
					
				} catch (SecurityException e) {
                    /*
                     * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                     * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                     * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                     * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                     */
                    final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                    // Otestuji, zda se podařilo načíst logger:
                    if (logger != null) {
                        // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                        // Nejprve mou informaci o chybě:
                        logger.log(Level.INFO, "Zachycena výjimka při pokusu o parsování hodnoty na typ: "
                                + parameterType + ", hodnota: " + textToParse
                                + ", třída: MakeOperation.java, metoda: getParsedParameter. K této chybě může dojít, " +
                                "například z důvodu toho, že konstruktor není přístupný, nebo chyba Class loaderu ->" +
                                " nemohl načíst aktuální třídu apod.");

                        /*
                         * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                         * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                         * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                         */
                        logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                    }
                    /*
                     * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                     */
                    ExceptionLogger.closeFileHandler();
					
					
					outputEditor.addResult(txtErrorWhileParsingValue + textToParse + " " + txtToDataType + parameterType
							+ ". " + txtErrorInClassLoaderOrAccessToField);
					
				} catch (InstantiationException e) {
                    /*
                     * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                     * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                     * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                     * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                     */
                    final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                    // Otestuji, zda se podařilo načíst logger:
                    if (logger != null) {
                        // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                        // Nejprve mou informaci o chybě:
                        logger.log(Level.INFO, "Zachycena výjimka při pokusu o parsování hodnoty na typ: "
                                + parameterType + ", hodnota: " + textToParse
                                + ", třída: MakeOperation.java, metoda: getParsedParameter. Tato chya může nastat " +
                                "například v případě, že se jedná o abstraktní tídu, ve které se zavolal konstruktor" +
                                ".");

                        /*
                         * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                         * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                         * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                         */
                        logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                    }
                    /*
                     * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                     */
                    ExceptionLogger.closeFileHandler();

					outputEditor.addResult(
							txtFailedToCastValue + ": " + textToParse + " " + txtToDataType + ": " + parameterType + "!"
									+ OutputEditor.NEW_LINE_TWO_GAPS + txtError + ": " + txtProbablyIsAbstractClass);

				} catch (IllegalAccessException e) {
                    /*
                     * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                     * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                     * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                     * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                     */
                    final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                    // Otestuji, zda se podařilo načíst logger:
                    if (logger != null) {
                        // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                        // Nejprve mou informaci o chybě:
                        logger.log(Level.INFO, "Zachycena výjimka při pokusu o parsování hodnoty na typ: "
                                + parameterType + ", hodnota: " + textToParse
                                + ", třída: MakeOperation.java, metoda: getParsedParameter. K této chybě může dojít " +
                                "například, pokud tento objekt Constructor vynucuje řízení přístupu jazyka Java a " +
                                "konstruktor, který je pod ním, je nepřístupný.");

                        /*
                         * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                         * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                         * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                         */
                        logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                    }
                    /*
                     * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                     */
                    ExceptionLogger.closeFileHandler();
					
					outputEditor.addResult(txtFailedToCastValue + ": " + textToParse + " " + txtToDataType + ": "
							+ parameterType + "!" + OutputEditor.NEW_LINE_TWO_GAPS + txtError + ": "
							+ txtNotFoundConstructorToCastingDataType);

				} catch (IllegalArgumentException e) {
                    /*
                     * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                     * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                     * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                     * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                     */
                    final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                    // Otestuji, zda se podařilo načíst logger:
                    if (logger != null) {
                        // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                        // Nejprve mou informaci o chybě:
                        logger.log(Level.INFO, "Zachycena výjimka při pokusu o parsování hodnoty na typ: "
                                + parameterType + ", hodnota: " + textToParse
                                + ", třída: MakeOperation.java, metoda: getParsedParameter. Tato chyba může nastat " +
                                "například z toho důvodu, že počet skutečných a požadovaných "
                                + "parametrů konstruktoru liší, nebo selhala konverze primitivních datových typů, " +
                                "pokud se konstruktor týká typu Enum apod.");

                        /*
                         * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                         * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                         * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                         */
                        logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                    }
                    /*
                     * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                     */
                    ExceptionLogger.closeFileHandler();

					outputEditor.addResult(txtFailedToCastValue + ": " + textToParse + " " + txtToDataType + ": "
							+ parameterType + "!" + OutputEditor.NEW_LINE_TWO_GAPS + txtError + ": "
							+ txtPossibleParametersCountIsDifferent);

				} catch (InvocationTargetException e) {
                    /*
                     * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                     * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                     * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                     * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                     */
                    final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                    // Otestuji, zda se podařilo načíst logger:
                    if (logger != null) {
                        // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                        // Nejprve mou informaci o chybě:
                        logger.log(Level.INFO, "Zachycena výjimka při pokusu o parsování hodnoty na typ: "
                                + parameterType + ", hodnota: " + textToParse
                                + ", třída: MakeOperation.java, metoda: getParsedParameter. Tato chyba může nastat v " +
                                "případě, že podkladový konstruktor vyhodí výjimku.");

                        /*
                         * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                         * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                         * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                         */
                        logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                    }
                    /*
                     * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                     */
                    ExceptionLogger.closeFileHandler();

					outputEditor.addResult(txtFailedToCastValue + ": " + textToParse + " " + txtToDataType + ": "
							+ parameterType + "!" + OutputEditor.NEW_LINE_TWO_GAPS + txtError + ": "
							+ txtExceptionWhileCallingConstructorToCasting);
				}
			}
		}


        /*
         * DO BUDOUCNA DOPLNIT PRETYPOVANI NA KOLEKCI - LIST JEDNOROZMERNE POLE, MOZNA I
         * VICEROZMERN POLE
         */

		return null;		
	}



	
	
	
	
	
	
	
	
	
	@Override
	public String makeGetValueFromVarInInstance(final String command) {
		// Syntaxe: (-) referenceName.variableName;
		
		// Odeberu se mezery proměnné : (-) referenceName . variableName 
		final String commandWithoutSpace = command.replaceAll("\\s", "");
		
		// Nyní se jedná o syntaxi bez mezer:   (-) referenceName.variableName;			
		final boolean isCharAtFirstIndexMinus = isMinusAtFirstChar(command);
		
		
		/*
		 * Refernce na instanci třídy.
		 */
		final String referenceName;

		if (isCharAtFirstIndexMinus)
			// Zjistím si název reference - od začátku (za prvním minusem) po první tečku:
			referenceName = commandWithoutSpace.substring(1, commandWithoutSpace.indexOf('.'));
		// Zjistím si název reference - od začátku po první tečku:
		else
			referenceName = commandWithoutSpace.substring(0, commandWithoutSpace.indexOf('.'));
		
		
		
		// Jelikož už v příkazu nejsou mezery, tak od první tečky po středník
		final String variableName = commandWithoutSpace.substring(commandWithoutSpace.indexOf('.') + 1,
				commandWithoutSpace.indexOf(';'));
		
		
		
		// Nyní mám název reference na instancí třídy: referenceName
		// a název prměnné: variableName
		
		// tak mohu otestovat, zda existuje nebo ne:
		final Object objInstance = Instances.getInstanceFromMap(referenceName);

		if (objInstance == null)
			return txtReferenceDoesNotPointToInstanceOfClass + ": " + referenceName + " = null"
					+ OutputEditor.NEW_LINE_TWO_GAPS + txtValueWillNotBeCounted;
			
		
		
		// Zde jsem našel hledanou instanci, tak zjistím její proměnnou:
		final Field field = getFieldFromInstance(objInstance, variableName);

		if (field == null)
			return txtValueOfVariableWillNotBeCounted + OutputEditor.NEW_LINE_TWO_GAPS + txtVariable + ": "
					+ variableName;

		// Zde jsem našel hledanou položku:

		/*
		 * Hodnota z příslušné proměnné field.
		 */
		final Object objFieldValue = ReflectionHelper.getValueFromField(field, objInstance);

		
		if (objFieldValue == null)
			return txtSuccess + ": null";

		
		
		// Otestuji, zda se má hodnota v proměnné
		// "znegovat":
		if (isCharAtFirstIndexMinus) {
			// Otestuji, zda se jedná o celé číslo nebo
			// destinné - dle toho mohu použít Big
			// Double nebo Integer
			final Object objValue = getNegationOfValue(objFieldValue.getClass(), objFieldValue);

			
			// Otestuji, zda se mají zvýraznit nějaké instance / buňky v diagramu instancí:
			instanceDiagram.checkHighlightOfInstances(objValue);

			
			// Zjistím, zda se mají vypsat hodnoty s potenciálními referencemi:
			if (OutputEditor.isShowReferenceVariables())
				// Výpis i s referenční proměnnou (pokud je to instance v diagramu instancí):
				return txtSuccess + ": "
						+ OperationsWithInstances.getReturnValueWithReferenceVariableForPrint(objValue);

			if (objValue != null && objValue.getClass().isArray())
				return txtSuccess + ": " + fromArrayToString(objFieldValue);

			return txtSuccess + ": " + objValue;
		}
		
		
		
		
		
		
		
		// Otestuji, zda se mají zvýraznit nějaké instance / buňky v diagramu instancí:
		instanceDiagram.checkHighlightOfInstances(objFieldValue);
		

		/*
		 * Zde nejprve otestuji, zda se mají vypisovat i reference za příslušnou
		 * instanci, pokud ano, pak ten výsledek ještě předám do metody, která to zjstí
		 * - zda je to instance z diagramu instancí a případně přidá reference /
		 * referenci.
		 * 
		 * Jinak pokračuji dále pro "běžné" vypsání návratové hodnoty.
		 */
		if (OutputEditor.isShowReferenceVariables())
			// Výpis i s referenční proměnnou (pokud je to instance v diagramu instancí):
			return txtSuccess + ": "
					+ OperationsWithInstances.getReturnValueWithReferenceVariableForPrint(objFieldValue);

		
		if (field.getType().isArray())
			return txtSuccess + ": " + fromArrayToString(objFieldValue);

		return txtSuccess + ": " + objFieldValue;
	}
	
	
	
	
	
	
	
	
	


	
	
	
	
	
	@Override	
	public String callConstructor(final String command) {
		// Syntaxe: new packageName. (anotherPackageName.) ClassName(parameters);
		
		/*
		 * Postup:
		 * 
		 * - Zjistit a načíst třídu
		 * 
		 * - Pokud nejsou zadány parametry, tak zkusit zavolat výchozí nullary
		 * konstruktor
		 * 
		 * - Pokud jsou zadány parametry, pak je cyklem projít a vždy zkusit zjistit
		 * příslušné parametry a před je do zavolání konstruktoru.
		 */
		
		/*
		 * Pro získání názvu třídy s balíčky, ve kterém / kterých se nachází je více
		 * možností, já využiji následující postup, odebrat všechny bílé znaky v řetezci
		 * a vzít text od 3. znaku (za w) až po první otevírací kulatou závorku.
		 */
		final String classWithPackages = command.replaceAll("\\s", "").substring(3,
				command.replaceAll("\\s", "").indexOf('('));
		
		/*
		 * Parametry si získám tak, že si vezmu text od první otevírací kulaté závorky,
		 * až po poslední uzavírací kulatou závorku a ty rozdělím dle desetinné čárky,
		 * ale pouze v případě, že text mezi uvedenými závorkami nebude prázdný, popř.
		 * jen bílé znaky, pak není zadán žádný parametr.
		 */
		final String textBetweenBrackets = getParametersOfMethodInTextFormat(command);
		
		/*
		 * Proměnná, typu jednorozměrné pole, která bude obsahovat parametry
		 * konstruktoru.
		 * 
		 * Pokud text mezí závorkami jsou jen bílé znaky, pak bude toto pole prázdné,
		 * tj. bude mít nula hodnot, tj. nula parametrů (resp. žádný parametr), jinak
		 * pokud text mezi závorkami nebudou jen bílé znaky, pak se tento text mezi
		 * závorkami rozdělí dle desetinných čárek, kterými se oddělují jednotlivé
		 * parametry konstruktoru.
		 */
		final String[] parameters;

		if (textBetweenBrackets.replaceAll("\\s", "").isEmpty())
			parameters = new String[] {};

		else
			parameters = textBetweenBrackets.split(",");
		
		
		
		
		/*
		 * Načtu si cestu k adresáři src, pokud neexistuje, pak vrátím příslušné
		 * oznámení, které se vypíše do editoru výstupů.
		 */
		final String pathToSrc = App.READ_FILE.getPathToSrc();

		if (pathToSrc == null)
			return txtFailure + ": " + txtSrcDirNotFound;

		
		/*
		 * Otestuji, zda existuje třída, jejíž konstruktor se má zavolat, protože je
		 * třeba jej pro jistotu znovu zkompilovat a pokud se třídy / třída zkompiluje v
		 * pořádku, pak si načtu její zkompilovanou verzi (soubor .class) v adresáři
		 * 'bin'.
		 */
		final String pathToJavaClassFile = pathToSrc + File.separator + classWithPackages.replace(".", File.separator)
				+ ".java";

		if (!ReadFile.existsFile(pathToJavaClassFile))
			return txtFailure + ": " + txtClassNotFound + OutputEditor.NEW_LINE_TWO_GAPS + pathToJavaClassFile;

		
		/*
		 * Zde si načtu přeloženoý / zkompilovaný soubor .class příslušné třídy, jejiž
		 * konstruktor se má zavolat.
		 */
        final Class<?> clazz = App.READ_FILE.getClassFromFile(classWithPackages, outputEditor,
                GraphClass.getCellsList(), true, false);

		if (clazz == null)
			return txtFailure + ": " + txtFiledToLoadClassFile;

		
		
		
		
		
		/*
		 * Note:
		 * Zde bych ještě mohl doplnit, že pokud není zadán žádný parametr, tak mohu
		 * zkusit vytvořit instanci pomocí metody clazz.newInstance(), ale raději si
		 * pohlídám všechny konstruktory, zda jsou veřejné, apod. Abych si nepřidával
		 * kód navíc
		 */
		
		
		
		
		/*
		 * Nyní je vše v pořádku načteno tak si načtu veškeré konstruktory příslušné třídy
		 * a zkusím najít ten, který je veřejný a kde se
		 * shoduje počet parametrů. Pokud takový najdu, pak zkusím všechny parametry
		 * přetypovat na příslušné typy a zavolat ten konstruktor.
		 */
		
		for (final Constructor<?> c : clazz.getDeclaredConstructors()) {
			/*
			 * Pokud není konstruktor veřejný, pak nepůjde zavolat, tak budu pokračovat v
			 * iteraci.
			 */
			if (!Modifier.isPublic(c.getModifiers()))
				continue;
			
			
			/*
			 * Pokud se nerovná počet parametrů konstruktoru a získané parametry, pak bude
			 * také pokračovat další iterací v cyklu.
			 */
			if (c.getParameterCount() != parameters.length)
				continue;
			
			/*
			 * V této části jsem našel veřejný konstruktor, který má stejný počet parametrů,
			 * tak zkusím zadané hodnoty přetypovat na požadované paraemtry, případně pokud
			 * jsou to metody či proměnné tak získat příslušnou hodnotu apod.
			 */
			
			
			/*
			 * Proměnná, do které se by vrátí hodnota coby vytvořená instance příslušné
			 * třídy díky zavolání konstruktoru, pokud dojde k nějaké chybě při vytváření
			 * konstruktoru, tak se do této hodnoty vrátí null.
			 */
			final Object objInstance;
			
			/*
			 * Nejprve otestuji, zda je vůbec nějaký parametr zadán, pokud ne, pak jej mohu
			 * rovnou zavolat.
			 */
			if (c.getParameterCount() == 0)
				objInstance = operationWithInstanees.callConstructor(c);

			
			else {
				/*
				 * List, který by měl obsahovat parametry pro příslušný konstruktor v příslušném
				 * datovém typu.
				 */
				final List<Object> parametersList = getListOfParsedParameters(parameters, c, null);

				if (parametersList != null)
					objInstance = operationWithInstanees.callConstructor(c, parametersList.toArray());

				else
					objInstance = null;				
			}
			
			
			/*
			 * Zde otestuji, zda byl úspěšně zavolán nějaký konstruktor, pokud ano, vrátím
			 * Úspěch, pokud ne, pak byly chybová hlášení o tom, že se nepodařilo zavolat
			 * konstruktor s příslušnými parametry nebo jiná chyba vypsány již do editoru
			 * výstupů, tak zde called podmínka neprojde, tak pokračuji další iterací.
			 */
			if (objInstance != null)
				return txtSuccess;
		}
		
		/*
		 * zde jsem prošel veškeré konstruktory v dané třídě a žádný se nepodařilo
		 * úspěšně zavolat, všech byly vypsány chyby, ke kterém došlo při pokusu o
		 * jejich zavolání, tak to zde jen "ujasním".
		 */
		return txtFailure + ": " + txtConstructorWasNotFound;
	}
	
	
	
	

	








	@Override
	public String makeInstance(final String command) {
		/*
		 * Postup:
		 * 
		 * Rozebrat příkaz dle =  (rovná se)
		 * První část - v proměnné textBeforeEquals:
		 * 		Pak rozebrat první číást dle mezer- NEZAPOMENOUT NA TRIM, díky této metodě se odeberou bíle znaky na začátku a na konci textu
		 * 				- hlavně na konci, díky tomu  mohu na jistotu začát od konce - tam je jedno slovo = název proměnné - resp. název instance dané třídy, pak mohu proje cyklem zbytek textu
		 * 				- A v každé části - pro případ, že by bylo více mezer, tak v každé= části testovat jestli se text != null a "", jak mile bude podmínka splněna, tak už dle regulárních výrazů vím, že
		 *  
		 * 				- se jedná o název třídy i s balíčky, tak otestuji její existenci v adresáři src v projektu a pokud existuje  mohu přejít k druhé části
		 * 		
		 * 
		 * Druhá část - v proměnné textAfterEquals:
		 * 		- rozdělit text dle slova new a nezapomenout na trim !!!
		 * 		-  zde mohou byt na začátku akorát mezery, takze trim a replace na tento index v poli - dat do promenne
		 * 		-  z teto proměnn vytahnout substring (0 - po oteviraci zavorku - bez ni) - zde mam název tridy, jejíz instanci chci vytvorit
		 * 	
		 * Z de tedy mám název promenne a tridu jejiz instanci chci vytvorit a nic víc zde nepotrřebuji parametry ze nejsou
		 * 
		 *  
		 *  Postup pro vytvoření instance:
		 *  Otestuji, zda zadaný název proměnné ještě neexistuje.
		 *  Zjistím si, zda třída, ze které se má vytvořit instance existuje, pokud ano, tak ji zkompiluji, pak načtu soubor .class
		 *  a vytvořím instanci dané třídy - zavolám příslušný konstruktor - dle parametrů, pokud nastane někde při postupu chyba, oznámí se neúspěch uživateli s chybou do editoru výstupů
		 */
	
	
		/*
		 * Note:
		 * Způsob rozdělení dle rovná se nepřipadá v úvahu, protože pokud se do
		 * parametru pole coby hodnota pro naplnění zadá rovnáse, pak příkaz selže.
		 */
		
		/*
		 * Proměnná, do které si vložím text příkazu od začátku po první rovná se.
		 * 
		 * (první část příkazu - model.data.ClassName variable)
		 */
		final String textBeforeEquals = command.substring(0, command.indexOf('='));
		
		/*
		 * Proměnná, do které si vložím text příkazu od rovná se do konce.
		 * 
		 * (a druhá: new model.data.ClassName();)
		 */
		final String textAfterEquals = command.substring(command.indexOf('=') + 1);
		
		

		// Zde si vezmu první část dle mezer: model.data.ClassName variable
		final String[] firstPartOfCommand = textBeforeEquals.trim().split("\\s");

		// Na posledním indexu v předchozím pole - firstPartOfCommand je
		// proměnná pro název instance třídy:
		final String instanceVariable = firstPartOfCommand[firstPartOfCommand.length - 1].replaceAll("\\s", "");
	
		// Z první části mám vše, mohu na druhou:

		// Rozeberu část dle slova new:
		final String[] secondPartOfCommand = textAfterEquals.trim().split("new");

		// Nyní mám výše uvedené pole s bílímy znaky a názvem třídy
		// Musím toto pole projít a zjistit, si název třídy

		// Proměnná pro název třídy - v druhé části příkazu
		final String textOfClassName2 = getTextOfClassName(secondPartOfCommand, false);

		// Z názvu třídy si vezmu pouze název třídy (bez závorek)
		final String classNameSecond = textOfClassName2.substring(0, textOfClassName2.indexOf('(')).replaceAll("\\s", "");

		// Zde si vezmu parametr konstruktoru mezi první ( a poslední ) - což
		// jsou závorku "kontruktoru" u dané třídy
		// vše co je mezi zavorkami je text parametru:
		final String textOfParameter = textAfterEquals.substring(textAfterEquals.indexOf('(') + 1,
				textAfterEquals.lastIndexOf(')'));

		// Nejprve si rozdělm parametry dle desetinné čátky, abych věděl kolik
		// tam je prametrů
		final String[] partsOfParameters = textOfParameter.split(",");

		// Vytvořit instanci pro názvem promenné: instanceVariable
		// První název třídy: classNameFirst
		// Vytvořit instanci třídy: classNameSecond

		// Zda název reference již existuje nebo ne:
		final boolean existReferenceName = existReferenceName(instanceVariable);

		if (!existReferenceName) {
			final String pathToSrc = App.READ_FILE.getPathToSrc();

			if (pathToSrc != null) {
				final String pathToJavaClassFile = pathToSrc + File.separator
						+ classNameSecond.replace(".", File.separator) + ".java";

				if (ReadFile.existsFile(pathToJavaClassFile)) {
                    final Class<?> clazz = App.READ_FILE.getClassFromFile(classNameSecond, outputEditor,
                            GraphClass.getCellsList(), true, false);

					if (clazz != null) {
						// Zde mám načtenou příslušnou třídu, tak zjistím, zda
						// mám zavolat výchozí konstukrot bez
						// parametrů nebo nějaký s parametry - dle počtu
						// parametrů, které zadal uživatel:
						if (regEx.isNewInstance(command)) {
							// Zde se jedná o zavolání výchozího konstuktoru bez
							// parametrů:
							final boolean isInstanceCreated = operationWithInstanees.createInstance(instanceVariable,
									clazz);

							if (isInstanceCreated)
								return txtSuccess;

							return txtFailure;
						}

						
						else if (regEx.isNewInstanceWithParameters(command)) {
							// Zjistím si všechny konstruktory, kde se rovná
							// počet parametrů:
							final Constructor<?>[] constructors = clazz.getDeclaredConstructors();

							// Do následující ho listu si předám konstruktory,
							// kde se shoduje počet jejich parametrů
							final List<Constructor<?>> constructorsList = new ArrayList<>();

							for (final Constructor<?> c : constructors) {
								if (c.getParameterCount() == partsOfParameters.length)
									constructorsList.add(c);
							}

							// Nyní projdu pole konstruktorů, kde se shoduje
							// počet parametrů, a zkusím zadané parametry v
							// editoru od uživatele naparsovat
							// na dané typy parametrů v konstuktoru

							// Dělám to takto protože to nemohu udělat naopak -
							// nejdríve zjistit jaká hodnota se nachází v
							// parametru v editoru
							// a pak dle toho najít konstruktor, protože když by
							// byla třeba hodnota 1, tak lze naprasovat jak na
							// int, na long, float,
							// double, text, atd. takže bych se nemusel nikdy
							// trefit

							for (final Constructor<?> c : constructorsList) {
								final List<Object> parametersList = getListOfParsedParameters(partsOfParameters, c,
										null);

								if (parametersList != null) {
									// Zde jsou parametry v pořádku, pokud se
									// vrátí null, nepodařilo se naparsovat
									// hodnotu, zde ano:

									// do instance je ale třeba předat ještě
									// balíčky, pokud by je chtěl uživatel
									// zobrazit, resp. má možnost
									// si je zobrazit tak je zkusím načíst:
									final String packageName;

									// Otestuji, zda se třída v nějakým
									// balíčcích nachází, pokud ano, tak si je
									// vložím do proměnné
									// packageName, jinak do této proměnné
									// vložím null hodnotu:
									if (clazz.getPackage() != null)
										packageName = clazz.getPackage().getName();

									else packageName = null;

									// Zde tedy mám potřebné parametry, tak mohu
									// zavolat konstruktor:
									final boolean isInstanceCreated = operationWithInstanees
											.createInstanceWithParameters(clazz.getSimpleName(), packageName,
													instanceVariable, c, false, parametersList.toArray());

									if (isInstanceCreated)
										return txtSuccess;
									
									// else - pokračuji v hledání
								}
							}
							return txtFailure + ": " + txtDataTypesOrConstructorNotFound;
						}
					} 
					else return txtFailure + ": " + txtFiledToLoadClassFile;
				} 
				else return txtFailure + ": " + txtClassNotFound + OutputEditor.NEW_LINE_TWO_GAPS + pathToJavaClassFile;
			}
			else return txtFailure + ": " + txtSrcDirNotFound;
		} 
		else return txtFailure + ": " + txtVariableName + ": '" + instanceVariable + "' " + txtAlreadyExist;

		
		return txtFailure;
	}

	
	

	
	




	
	/**
	 * Metoda, která vrátí název reference na instanci třídy z příkazu pro zavolání
	 * metody
	 * 
	 * @param command
	 *            - zadaný příkaz
	 * 
	 * @return pouze název reference na insatnci třídy
	 */
	private static String getReferenceNameClass(final String command) {
		return command.substring(0, command.indexOf('.')).replaceAll("\\s", "");
	}
	
	

	
	
	
	
	/**
	 * Metoda, která vrátí název metody nějaké instance třídy
	 * 
	 * @param command
	 *            - zadaný příkaz v editoru příkazů
	 * 
	 * @return název metody
	 */
	private static String getMethodName(final String command) {
		return command.substring(command.indexOf('.') + 1, command.indexOf('(')).replaceAll("\\s", "");
	}




	
	
	



	@Override
	public String makeCallTheMethod(final String command) {
		final String variableClassName = getReferenceNameClass(command);
				
		final String methodName = getMethodName(command);
				
		
		// Všechny parametry budou mezi první otevírací a poslední zavírací závorkou, akorát nesmím odebírat nezery, pouze bílé znaky
		// na začátku a na konci daného sub řetězce:
		final String textOfParameters = getParametersOfMethodInTextFormat(command);
		
		// Naní výše uvedený řetězec parametrů rozdělím dle desetinné čárky, toto pole "projdu" cyklem a nad každou hodnoutou pole
		// zjistím o jaký typ se jedý a zjistím si tím příslušné parametry metody:
		
		final String[] partsOfParameters = textOfParameters.split(",");				
		
		
		
		// Zjistím si instance dané třídy: 
		final Object objInstance = Instances.getInstanceFromMap(variableClassName);
		
		// Dále se pokusím získat instanci třídy VariableList:
		final VariableList<?> variableList = (VariableList<?>) Instances.getListFromMap(variableClassName);




        if (objInstance != null) {
            // Zde se podařilo nají instanci třídy z diagramu tříd:
            final Object[] returnDataArray = callTheMethod(command, objInstance, methodName, null,
                    partsOfParameters);

            final boolean wasFilling = Boolean.parseBoolean(String.valueOf(returnDataArray[0]));

            if (wasFilling) {
                final Object objReturnValue = returnDataArray[1];


                if (objReturnValue != null) {

                    // Otestuji, zda se mají zvýraznit nějaké instance / buňky v diagramu instancí:
                    instanceDiagram.checkHighlightOfInstances(objReturnValue);

                    /*
                     * Zde nejprve otestuji, zda se mají vypisovat i reference za příslušnou
                     * instanci, pokud ano, pak ten výsledek ještě předám do metody, která to zjstí
                     * - zda je to instance z diagramu instancí a případně přidá reference /
                     * referenci.
                     *
                     * Jinak pokračuji dále pro "běžné" vypsání návratové hodnoty.
                     */
                    if (OutputEditor.isShowReferenceVariables())
                        // Výpis i s referenční proměnnou (pokud je to instance v diagramu instancí):
                        return txtSuccess + ": " + OperationsWithInstances
                                .getReturnValueWithReferenceVariableForPrint(objReturnValue);


                    // Zde se testuje jestli je to pole pro převod do textu:
                    if (objReturnValue.getClass().isArray())
                        return txtSuccess + ": " + fromArrayToString(objReturnValue);

                    return txtSuccess + ": " + objReturnValue;
                }

                else {
                    /*
                     * Získám si třetí uloženou položku v získaném poli s hodnotami, pokud je to
                     * true, pak je metoda void, pak stačí vypsat pouze "Úspěch", protože metoda nic
                     * nevrací, jinak vypíšu "Úspěch: null", protože v této částí vím, že metoda
                     * vrátíla null.
                     */
                    final boolean isVoid = Boolean.parseBoolean(String.valueOf(returnDataArray[2]));

                    if (isVoid)
                        return txtSuccess;

                    return txtSuccess + ": null";
                }
            }
            // Zde se metody nezavolaly, takže vráceno null, a jelikož se nezavolaly, tak
            // nastala chyba, takže vrátím neúspěch:
            else
                return txtFailure;
        }
		
		
		
		
		
		
		// Zde se nepodařilo nají instanci třídy z diagramu tříd, tak otestuji, zda se
		// našla instnace třídy VariableList:
		else if (variableList != null) {
			// Zde se podařilo najít instanci třídy VariableList:

			// Nyní si zjistím, o jakou operaci s listem se jednáí pomocí regulárních výrazů
			// a pak když budu znát konkrétní operaci, o kterou se jedná, tak ji provedu:
			if (regEx.isAddValueToIndexAtList(command))
				return makeAddValueToIndexAtList(command);

			
			// Přehozeno kvůli rozpoznání dle regulárních výrazů:
			else if (regEx.isAddValueToEndOfList(command))
				return makeAddValueToEndOfList(command);
				
			
			
			else if (regEx.isClearList(command))
				return makeClearList(command);
			
			
			else if (regEx.isGetValueAtIndexOfList(command))
				return makeGetValueAtIndexOfList(command);
			
			
			// Metoda pro nastavní hodnoty v listu na zadané pozici
			else if (regEx.isSetValueAtIndexOfList(command))
				return makeSetValueAtIndexOfList(command);
			
			
			// Otestování syntaxe, zda se jedná o příkaz pro konverzi List (kolekce) do jednorozměrného pole
			// pomocí metody toArray();
			else if (regEx.isListToOneDimensionalArray(command))
				return makeListToOneDimensionalArray(command);
			
			
			// Zjištění - resp. výpis délky kolekce - Listu:
			else if (regEx.isListSize(command))
				return makeListSize(command);
			
			
			// Výraz pro odebrání hodnoty z Listu - kolekce na zadaném indexu:
			else if (regEx.isRemoveValueAtIndexOfList(command))
				return makeRemoveValueAtIndexOfList(command);
		}
		
		
		// Zde se nepodařilo najít instanci ani jedné požadované třídy:
		else return txtFailure + " " + txtInstancesNotFound + ": '" + variableClassName + "'";
		
		return txtFailure;
	}


	
	
	

	
	
	
	
	
	
	@Override
	public String makeCallStaticMethod(final String command) {
		/*
		 * Postup pro získání třídy s balíčky, názvu metody a případně její parametry:
		 * 
		 * - vezmu si text od začátku po první otevírací kulatou závorku a rozdělím jej
		 * dle desetinné tečky.
		 * - Z toho pole (hodnoty z textu rozděleného dle desetinných teček) je jako
		 * poslední hodnota název metody a jako předposlední název třídy a vše před
		 * třídou jsou balíčky.
		 * 
		 * - parametry metody jsou od první oevírací kulaté závorky po poslední zavírací
		 * kulatou závorku.
		 */
		
		/*
		 * Jednorozměrné pole obsahující hodnoty (po první otevírací kulatou závorku)
		 * oddělené desetinnou tečkou.
		 */
		final String[] valuesByDot = command.substring(0, command.indexOf('(')).split("\\.");
		
		/*
		 * Proměnná, která bude obsahovat název třídy s balíčky.
		 */
		String classWithPackages = "";
		
		for (int i = 0; i < valuesByDot.length - 1; i++)
			classWithPackages += valuesByDot[i].trim() + ".";
		
		// Odeberu poslední desetinnou tečku:
		classWithPackages = classWithPackages.substring(0, classWithPackages.length() - 1);
		
		
		/*
		 * Název metody je jako poslední položka v poli.
		 */
		final String methodName = valuesByDot[valuesByDot.length - 1].trim();
		
		
		/*
		 * Proměnná, do které si vložím text, který by měl být jako parametr / parametry
		 * metody. Tj. hodnoty, které se nacházejí mezi první otevírací a posladní
		 * zavírací kulatou závorkou.
		 */
		final String parametersInText = getParametersOfMethodInTextFormat(command);
		
		/*
		 * Proměnná, do které se vloží buď parametry metody v proměnné parametersInText
		 * oddělené desetinnou čárkou nebo se jen vytvoří instance tohoto pole - bude
		 * prázdné, nebude obsahovat žádný prvek v případě, že nebyl zadán žádný
		 * parametr.
		 */
		final String[] parameters;
		
		// Otestuji, zda byl zadán nějaký parametr:
		if (parametersInText.replaceAll("\\s", "").isEmpty())
			// de nebyl zadán žádný parametr, tak pole pouze inicializuji, ale žádný prvek
			// obsahovat nebude:
			parameters = new String[] {};
		// Zde pole naplním zadanými parametry v datovém typu String, jednotlivé hodnoty
		// budou odděleny desetinnou čárkou.
		else
			parameters = parametersInText.split(",");
			
		
		
		
		
		
		
		// Nyní si zkusím načíst příslušnou třídu a zavolat její metodu:
		final String pathToSrc = App.READ_FILE.getPathToSrc();

		// Otestuji adresář src v otevřeném projektu:
		if (pathToSrc == null)
			return txtFailure + ": " + txtSrcDirNotFound;

		
		/*
		 * Získám si cestu ke třídě, kterou mám načíst, resp. její zkompilovanou verzi.
		 */
		final String pathToJavaClassFile = pathToSrc + File.separator + classWithPackages.replace(".", File.separator)
				+ ".java";


        // Pro načtení zkompilované třídy z adresáře bin:
        final Class<?> clazz;

		/*
		 * Note:
		 * Toto testování, zda - li existuje ta třída je celkem zbytečné, protože ani
		 * není potřeba, ale pokud neexistuje třída ssmotná, je někde problém, měla by
		 * existovat, alespoň na to uživatele upozorním, jinak je důležitý soubor .class
		 * v příslušném adresáři v adresáři bin. Ale pokud se třídy zkompilují, tak se
		 * načte (alespoň její starší verze - pokud byla třída alespoň jednou zkompilována).
		 */
        if (!ReadFile.existsFile(pathToJavaClassFile)) {
            /*
             * V tomto případě se napodařilo nalézt třídu z diagramu tříd, takže uživatel buď chybně zadal syntaxi /
             * názvy balíčků, tříd nebo metod. Tak zde
             * zkusím prohledat ještě vnitřní třídy.
             */
            final String pathToBin = App.READ_FILE.getPathToBin();

            if (!ReadFile.existsDirectory(pathToBin))
                return txtFailure + ": " + txtFailedToFindTheClass + ": " + OutputEditor.NEW_LINE_TWO_GAPS +
                        pathToJavaClassFile + OutputEditor.NEW_LINE_TWO_GAPS + txtFailedToFindBinDir;

            // Získám si cestu k vnitřní třídě:
            final String pathToInnerClass = getPathToCompiledInnerClass(classWithPackages, pathToBin);

            if (pathToInnerClass == null)
                return txtFailure + ": " + txtFailedToFindTheClass + ": " + OutputEditor.NEW_LINE_TWO_GAPS +
                        pathToJavaClassFile + OutputEditor.NEW_LINE_TWO_GAPS + txtFailedToFindInnerClass;


            // Zde se našla existující vnitřní třída v adresáři bin, tak ji načtu:
            clazz = App.READ_FILE.loadCompiledClass(pathToInnerClass, outputEditor);

            // Uživateli zpřístupníme pouze veřejné a chráněné statické vnitřní třídy:
            if (clazz == null)
                return txtFailure + ": " + txtFiledToLoadClassFile;

            if (!Modifier.isStatic(clazz.getModifiers()))
                return txtFailure + ": " + txtInnerClassIsNotStatic;

            if (!Modifier.isPublic(clazz.getModifiers()) && !Modifier.isProtected(clazz.getModifiers()))
                return txtFailure + ": " + txtInnerClassIsNotPublicEitherProtected;
        }


        /*
         * Zde vím, že třída existuje, tak si načtu její zkompilovanou verzi:
         *
         * (Note, mohl bych využít metodu: App.READ_FILE.getClassFromFile, ale nebudu již třídy kompilovat, protože
         * se to mělo provést při poslední editaci kódu,
         * zde ale může nastat případ, když uživatel vymaže adresář bin, v tom případě stačí pouze třídy zkompilovat,
          * ale není moc elký důvod to provádět zde. Výše již mám
         * zjišťěno, že příslušný soubor .class existuje, tak jej zde pouze načtu)
         */
        else clazz = App.READ_FILE.loadCompiledClass(classWithPackages, outputEditor);

		
		if (clazz == null)
			return txtFailure + ": " + txtFiledToLoadClassFile;
		
		
		/*
		 * Note:
		 * Zde by bylo ještě možné doplnit možnost pro operace nad listem.
		 * 
		 * Pokud bych doplnil operace nad listem, resp. proměnnou typu list, která se
		 * nachází v instanci nějaké třídy, tak by se zadával příkaz například v
		 * následující podobě: referenceName.listVariableName.clear(); místo metody
		 * clear třeba metoda get, set, size, ...
		 * 
		 * Ježe jedná se o stejnou syntaxi jako pro zavolání statické metody. Proto by
		 * se zhruba v této části, kde se nachází tento komentář ještě bylo vhodné
		 * získat si z příkazu text od začátku po první desetinnou tečku a otestovat,
		 * zda je to instnace nějaké třídy, pokud ano, tak si z ní a z předků vzít
		 * proměnné a hledat proměnnou stejného názvu a testovat, zda je to list, pokud
		 * ano, tak dle konkrétích regulárních výrazů doplinit testování pro konkrétní
		 * operaci.
		 * 
		 * Ale další možnost by byla například vzít opet název třídy a ta poslední část
		 * příkazu od poslední desetinné tečky po otevírací kulatou závorku by byl název
		 * listu a vše před tím cesta k tředě, tak jako to je teď po načtení proměnné
		 * clazz, akorát to poslední by nebyla metoda, ale název proměnné typu List ->
		 * statická proměnná.
		 * 
		 * Veškeré výše popsané způsoby by se mezi sebou pletyl, tak jsem se rozhlodl
		 * list vynechat a nechat zde pouze statické metody, jinak bych musel rozeznávat
		 * názvy instanci od názvu tříd a nějak určovat pořadí, ale jak se rozhodnout,
		 * když se nenajde ani třída ani metoda nebo popřípadě ani příslušná statická
		 * proměnná, jak poznám, co uživatel chtěl hledat nebo zavolat nebo získat
		 * proměnnou apod.
		 * 
		 * Ještě bych také mohl vybrat jen jedno z toho, například tuto část doplnit
		 * pouze o list nebo pouze o proměnné apod. Ale z časových důvodů už to také
		 * nestíhám, tak možná v další verzi.
		 */



        /*
         * Získám si list s metodami, které je možné zavolat nad třídou clazz , jedná se
         * o všechny veřejné a chráněné metody v dané třídě, tak nad všemi jejími
         * předky. V tomto případě musí být všechny metody statické.
         */
        final List<Method> availableMethodList = getAvailableMethods(clazz, new ArrayList<>(), true);


		
		/*
		 * Cyklem projdu všechny metody a zavolám tu metodu, u které se najde jako první
		 * shoda s názvem, není privátní a má příslušný počet parametrů, který byl zadán
		 * v editoru příkazů a tyto hodnoty z editoru se podařilo přetypovat na
		 * příslušný, resp. potřebný datový typ v parametrech příslušné metody.
		 */
		// Původní cyklus ok, ale prepsano na nacteni metod i z predků:
		for (final Method m : availableMethodList) {
			// Otestuji název metody:
			if (!m.getName().equals(methodName))
				continue;

			// Otestuji počet parametrů metody:
			if (m.getParameterCount() != parameters.length)
				continue;
			

			/*
			 * Note:
			 * Vím, že bych mohl využít metodu 'callTheMethod' (kdybych ji trochu upravil,
			 * že když se nezadá reference ani příslušná instance, pak se provede jen to co
			 * je níže. Ale to je trochu práce navíc. A už by to nebylo moc přehledné.
			 */

			/*
			 * Proměnná, do které si uložím hodnotu, která se vrátila, případně null, pokud
			 * se nic nevrátilo, nebo vrátila právě null hodnota apod.
			 */
			Called objReturnValue = null;

			/*
			 * Zde jsem našel metodu se stejným názvem a počtem parametrů, tak zjistím, zda
			 * je ten počet parametrů nula, tj. metoda nemá žádný parametr (a zároveň žádný
			 * nebyl zadán), pak to metodu mohu rovnou zavolat, pokud metod má nějaké
			 * parametry, pak pokračuji dále.
			 */
			if (parameters.length == 0)
				objReturnValue = operationWithInstanees.callMethodWithoutParameters(m, null, false);
				

			else {
				/*
				 * Zde jsem našel metodu se stejným názvem a počtem parametrů (metoda má alespoň
				 * jeden parametr), tak zkusím parametry v datovém typu String přetypovat na
				 * hodnoty, který požaduje metoda, pokud se to povede, pak se zavolá, jinak se
				 * pokračuje.
				 */
				// Zde jsem našel metodu, která má stejný název a počet parametrů
				// tak zkusím zadané parametry na dané typy přetypovat:
				final List<Object> parametersList = getListOfParsedParameters(parameters, null, m);

				if (parametersList != null) {
					// Zde se podařilo přetypovat zadaný text v editoru na nějaké datové typy

					// Do následujícího listu vložím "otestované" parametry s tím, že otestuji, zda
					// nějaký z parametrů není
					// jednorozměrné pole, pokud ano, tak ho musím přetypovat na dané pole, protože
					// se v tomto případě
					// jedná pouze o tu "referenční" verzi či jak to nazvat, prostě to není klasické
					// - potřebné pole s hodnotami
					// ve stavu, abych ho mohl předat, tak ho převezu na "normální pole", abych ho
					// následně mohl předat do metody:
					final List<Object> listOfTestedParameters = getListOfTestedParameters(parametersList, m);

					// Zde mám pouze zavolat metodu:
					objReturnValue = operationWithInstanees.callMethodWithParameters(m, null, false,
							listOfTestedParameters.toArray());
				}
			}
			
			
			
			/*
			 * Otestuji, zda se nějaká metoda alespoň pokusila zavolat.
			 * 
			 * Pokud ano, tak se zjistí, zda se zavolala, případně vypíšu neúspěch. Pokud
			 * se zavolala, pak se zjistí, zda příslušná metoda něco vrací, pokud ano, tak
			 * se vypíše hodnota, která se vrátila (je v objektu Called v proměnné
			 * objReturnValue), pokud nic nevratí, tak se vypíše jen úspěch.
			 */
			if (objReturnValue == null)
				continue;

			if (objReturnValue.isWasCalled()) {
				/*
				 * Sem se dostanu pouze v případě, že se metoda zavolala a v proměnné
				 * objReturnValue mám vrácenou hodnotu dle toho, zda má metoda něco vrátit nebo
				 * nic nevrací - je void, tak vrátím text pro výpis do editoru výstupů s
				 * příslušným výsledkem.
				 */
				if (!m.getReturnType().equals(Void.TYPE)) {
					// Zde se metoda zavolala, a vrácenou hodnotu mám v proměnné objReturnValue, tak
					// jej vypíšu:

					// Zda se mají zvýraznit příslušné instance:
					instanceDiagram.checkHighlightOfInstances(objReturnValue.getObjReturnValue());



					/*
					 * Zde otestuji, zda se mají vypisovat i referenční proměnné v případě, že se
					 * jedná o nějakou instanci z diagramu instanci:
					 */
					if (OutputEditor.isShowReferenceVariables())
						// Zde se ještě testuji, že když je to instance z diagramu
						// instnací, tak se k ní doplní referenční proměnná.
						return txtSuccess + ": " + objReturnValue.getObjReturnValueWithTestedReference();


					// Zde otestuji, zda se jedná o pole, tak abych jej převedl na text:
					if (objReturnValue.getObjReturnValue().getClass().isArray())
						return txtSuccess + ": " + fromArrayToString(objReturnValue.getObjReturnValue());

					// Zde se jedná o "běžnou" hodnotu:
					return txtSuccess + ": " + objReturnValue.getObjReturnValue();
				}


				// Zde se metoda zavolala, ale nic nevrací, tak vypíšu pouze úspěch:
				return txtSuccess;
			}
			/*
			 * Sem se dostanu pouze v případě, že se nějaká metoda zavolala, takže proměnná
			 * objReturnValue je naplněna, ale došlo k nějaké chybě, vypadla nějaká výjimka
			 * a její info bylo vypsáno do editoru výstupů, tak stačí vypsat pouze slovo:
			 * Neúspěch (nebo nic, když už bylo vypsáno info o chybě) a tím ukončit běh
			 * metody.
			 */
			return txtFailure;
		}

		/*
		 * Sem bych se dostat neměl, pouze v případě, že nebyla metoda se zadaným názvem
		 * nebo parametry nalezena, tak o tom informuji uživatele:
		 */
		return txtFailure + ": " + txtErrorWhileCallingStaticMethod_1 + ": " + classWithPackages + " "
				+ txtErrorWhileCallingStaticMethod_2;
	}







    /**
     * Metoda, která zkusí získat cestu k vnitřní třídě (soubor .class) v adresáři bin v otevřeném projektu.
     *
     * @param callMethodText
     *         - text pro zavolání metody bez názvu metody. Ze zadané syntaxe uživatelem do editoru příkazů je odbrán
     *         všechen text na poslední desetinnou tečkou před otevírací závorkou pro parametry metody.
     * @param pathToBin
     *         - cesta k adresáři bin v otevřeném projektu.
     *
     * @return cestu k souboru .class = zkompilovaná vnitřní třída v nějaké třídě v diagramu tříd. Jedná se ale o cestu
     * od adresáře bin, tedy pouze balíčky oddělené desetinnou tečkou a název třídy a jeji / jejich vnitřních tříd
     * oddělené desetinnou dolarem.
     */
    private static String getPathToCompiledInnerClass(final String callMethodText, final String pathToBin) {
        /*
         * Princip
         *
         * - Na začátku si v textu callMethodText nahradím veškeré desetinné tečky oddělující třídy a balíčky za dolar
         *
         * - Poté postupně procházím celý text a postupně nahrazuji jeden dolar za druhým za lomítko (dle OS), dokud
         * nenajdu existující soubor .class reprezentující vnitřní třídu v adresáři bin.
         */

        // Nahrazení všech desetinných teček za dolar:
        String tempPath = callMethodText.replace(".", "$");

        /*
         * Celý text projdu a pro počet desetinných teček postupně nahradím jednu tečku za druhou za dolar a vždy
         * otestuji, zda příslušný soubor existuje, pokud ano, vrátím cestu, jinak pokračuji a na konec se vrátí null.
         */
        for (int i = 0; i < StringUtils.countMatches(callMethodText, "."); i++) {
            // Nahrazení vždy prvního výskytu dolaru za lomitko
            tempPath = tempPath.replaceFirst("\\$", Matcher.quoteReplacement(File.separator));

            // Sestavení cesty k souboru v adresáři bin:
            final String innerClass = pathToBin + File.separator + tempPath + ".class";

            // Otestuji existenci souboru:
            if (ReadFile.existsFile(innerClass))
                // Vrátím pouze cestu k třídě s balíčky oddělené desetinnou tečkou:
                return tempPath.replace(File.separator, ".");
        }

        return null;
    }

	
	





	
	
	/**
	 * Metoda, která vrátí texty parametru v textové podobně: Syntaxe metody s
	 * parametry je: classNameVariable.methodName(p1, p2, ...);
	 * 
	 * Tato metoda rozebere výše zmíněnou syntaxi od první otevírací závorky (+1 -
	 * bez otevírací závorky) až po poslední zavárací závorku všechen text v těchto
	 * závorkách jsou parametry, tak je vrátím v podobě techtu dále s nimi pracuje,
	 * že si je rozeberu podle desetinné čátky, apod. ale to je jižjiná část
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz nebo jeho část, kde se nachází zmíněná
	 *            syntaxe
	 * 
	 * @return text parametrů metody (neboli text mezi první otevírací a poslední
	 *         zavírací závorkou)
	 */
	private static String getParametersOfMethodInTextFormat(final String command) {
		return command.substring(command.indexOf('(') + 1, command.lastIndexOf(')')).trim();
	}









	
	/**
	 * Metoda, která vrátí název prpoměnné coby reference na instanci třídy z
	 * příkazu v syntaxi pro získání hodnoty z třídy - veřejné proměnné, ke které
	 * lze přistupovat přes referenci na instanci dané třídy
	 * 
	 * Syntaxe referenceName.variable = x;
	 * 
	 * Stručně řečeno ,metody vrátí název reference na instanci třídy ze syntaxe pro
	 * naplnění proměnné instance nějaké třídy
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return název reference na instanci třídy
	 */
	private static String getReferenceName(final String command) {
		// Název promené odkazující na třídu je od začátku výrazu po první tečku
		// oddělující proměnnou třídy a jeji proměnnou
		return command.substring(0, command.indexOf('.')).replaceAll("\\s", "");
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí název proměnné, instance nějaké třídy ze syntaxe pro
	 * naplnění proměnné třídy nějakou hodnotou
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 * 
	 * @return název proměnné instance třídy
	 */
	private static String getVariableName(final String command) {
		// Proměnná ve třídě pro naplnení je od tečky po rovnáse =		
		return command.substring(command.indexOf('.') + 1, command.indexOf('=')).replaceAll("\\s", "");
	}
	

	
	




	@Override
	public String makeFullfillmentOfVariablesNumber(final String command) {
		// Název promené odkazující na třídu  je od začátku výrazu po první tečku oddělující proměnnou třídy a jeji proměnnou
		final String classNameVariable = getReferenceName(command);
		
		// Proměnná ve třídě pro naplnení je od tečky po rovnáse = 
		final String variableName = getVariableName(command);
		
		
		// Hodnota pro naplnění - od rovná se do konce - po středník:
		final String valueForAsign = command.substring(command.indexOf('=') + 1, command.lastIndexOf(';')).trim();
		
		
		// Postup:
		// Zjistím si danou proměnnou - pokud existuje
		// pak její typ, a pokud bude existovat, tak zkusím zadanou hodnotu naparsovat na dany typ hodnoty
		// a naplnit danou proměnnou
		// Případně vypíšu nějaké chybové upozornění
		
		
		// Získám si instance třídy, na kterou ukazuje zadaná hodnota v editoru:
		final Object objInstance = Instances.getInstanceFromMap(classNameVariable);
		
		if (objInstance != null) {
			// Zde mám hledanou instanci, tak se pokusínm získat danou hodnotu:
			final Field field = getFieldFromInstance(objInstance, variableName);

			if (field != null) {
				if (!Modifier.isFinal(field.getModifiers())) {
					// Zde jsem našel požadovanou proměnou
					// Zkusím zadanou hodntou v editoru naparsovat na potřebný typ proměnné:
					final ResultValue objParsedField = getValueFromVar_Method_Constant(valueForAsign, field.getType());

					/*
					 * Pokud došlo k chybě a hodnotu se nepodařilo přetypovat, a někde došlo k
					 * naprostě chybě, že by se nevrátila ani výchozí hodnota (toto by nastat
					 * nemělo), pak to vypíšu uživateli do editoru výstupů:
					 */
					if (objParsedField == null)
						return txtFailure + ": " + txtFailedRetypeValue + OutputEditor.NEW_LINE_TWO_GAPS + txtValue
								+ ": " + valueForAsign;

					/*
					 * Otestji, zda byla operace provedena, pokud ne, už by měly být vypsána
					 * informace o chybě, ke které došlo do editoru výstupů.
					 */
					if (!objParsedField.isExecuted())
						return txtFailure;

					setValueToClassVariable(field, objParsedField.getObjResult(), objInstance);

					return txtSuccess;
				} 
				else
					return txtFailure + ": " + txtVariableIsFinalDontChangeValue + OutputEditor.NEW_LINE_TWO_GAPS
							+ txtVariable + ": " + field.getName();
			} 
			else
				return txtFailure + ": " + txtMissingVariableInInstaceOfClass + OutputEditor.NEW_LINE_TWO_GAPS
						+ txtErrorInVariableNameOfVisibilityVariable;
		} 
		else
			return txtFailure + ": " + txtInstanceOfClassNotFound;
	}










	
	
	
	@Override
	public String makeFullfillmentOfVariableVariable(final String command) {
		// Proměnná ukazující na instanci třídy: od začátku po první tečku:
		final String classNameVariable = getReferenceName(command);
		 
		
		// Proměnná, do které se má přiřadit hodnota: - od první desetinné tečky po rovná se
		final String classVariable = getVariableName(command);
		
		
		
		// Vezmu si text od rovnáse (=) po středník - konec příkazů, ten otestuji, zda se jedná o převzetí proměnné z 
		// instance třídy nebo proměnné, vytvořené v editoru příkazů:
		final String textWithVariable = command.substring(command.indexOf('=') + 1, command.lastIndexOf(';')).replaceAll("\\s", "");



		// Získám si instance třídy, na které ukazují zadané hodnoty v editoru:
		final Object objInstance1 = Instances.getInstanceFromMap(classNameVariable);
		
		
		if (objInstance1 != null) {
			final Field f1 = getFieldFromInstance(objInstance1, classVariable);

			if (f1 != null) {

				if (!Modifier.isFinal(f1.getModifiers())) {
					// Získám si hodnotu:
					final ResultValue objResult = getValueFromVar_Method_Constant(textWithVariable, f1.getType());

					/*
					 * Pouze vypíšu Neúspěch, protože by měly být veškeré informace vypsány již v
					 * metodě výše, při zjišťování příslušné proměnné apod.
					 * 
					 * Dále pokud s proměnnou nastala nějaká chyba, pak se vrátilo takě že nebyla
					 * hodnota naplněna, a také by měly být informace o chybě již vypsány.
					 */
					if (objResult == null || !objResult.isExecuted())
						return txtFailure;

					/*
					 * Do proměnné objValue si vložím žískanou hodnotu z proměnné, abych k ní pořád
					 * nemusel přistupovat.
					 */
					final Object objValue = objResult.getObjResult();

					if (objValue != null) {
						if (objValue.getClass().isArray()) {
							new ParseToJavaTypes();

							final Object[] objArray = ParseToJavaTypes.convertToObjectArray(objValue);

							final Object objArrayValue = getArrayToClassVariable(objArray,
									f1.getType().getComponentType());

							return setValueToClassVariable(f1, objArrayValue, objInstance1);
						}

						else
							setValueToClassVariable(f1, objValue, objInstance1);

						// Pokud nedojde k chybě, potvrdím úspěšné dokončení
						// operace:
						return txtSuccess;
					}
				} 
				else
					return txtFailure + ": " + txtVariableIsFinalDontChangeValue + OutputEditor.NEW_LINE_TWO_GAPS
							+ txtVariable + ": " + f1.getName();

			} 
			else
				return txtFailure + ": " + txtVariableFailedToLoad + OutputEditor.NEW_LINE_TWO_GAPS + txtVariable + ": "
						+ classVariable;

		} 
		else
			return txtFailure + ": " + txtReferenceNameNotFound + OutputEditor.NEW_LINE_TWO_GAPS + txtReference + ": "
					+ classNameVariable;

		return txtFailure;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která nastaví hodnotu objValue do zadané proměnné f v zadané instanci
	 * třídy objInstance
	 * 
	 * @param f
	 *            - proměnná v instanci nějaké třídy, do které se má vložit hodnota
	 * 
	 * @param objValue
	 *            - hodnota, která se má vložit do výše zadané proměnné (f)
	 * 
	 * @param objInstance
	 *            - instance třídy, ve které se nachází prměnná (f), kam se má
	 *            vložit hodnota (objvalue)
	 * 
	 * @return Úspěch, pokud se povede vložení hodnoty do proměnné, jiank Neúspěch s
	 *         informací o chybě, ke které došlo
	 */
	private String setValueToClassVariable(final Field f, final Object objValue, final Object objInstance) {
		try {
			if (Modifier.isFinal(f.getModifiers()))
				return txtFailure + ": " + txtVariableIsFinalDontChangeValue + OutputEditor.NEW_LINE_TWO_GAPS
						+ txtVariable + ": " + f.getName();

			
			/*
			 * V případě, že je proměnná f protected, tak ji musím zpřístupnit v Javě lze
			 * tyto proměnné zpřístupnit, ale zde v kódu je třeba využít metodu
			 * setAccessible.
			 */
			if (Modifier.isProtected(f.getModifiers()))
				f.setAccessible(true);

			f.set(objInstance, objValue);

			runThreadForCheckInstancesRelations();

            /*
             * Aktualizuji hodnoty v okně pro doplňování hodnot do editoru příkazů, zde se totiž naplnila nějaká
             * proměnna, tak je třeba aktualizovat zobrazenou hodnotu v okně pro banízení hodnot.
             */
            updateValueCompletionWindow();

			return txtSuccess;

		} catch (IllegalArgumentException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při naplňování proměnné: " + f.getName()
                        + ", třída: MakeOperatin.java, metoda: setValueToClassVariable. Tato chyba může nastat " +
                        "například v případě, že zadaný objekt není instancí třídy nebo rozhraní, které deklaruje " +
                        "zadanou hodnotu proměnnou.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			return txtFailure + ": " + txtFailedToFillVariable + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtPossibleErrorsInFillVariable;
			
		} catch (IllegalAccessException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při naplňování proměnné? " + f.getName()
                        + ", třída: MakeOperatin.java, metoda: setValueToClassVariable. Tato chyba může nastat " +
                        "například v případě, že proměnná je buď nepřístupná nebo final.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();

			return txtFailure + ": " + txtErrorsImmutableVisibilityPublic;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	


	
	
	
	/**
	 * Metoda, která přetypuje hodnoty na na daný datový typ a vložit jej do nově
	 * vytvořeného jednorozměrného pole. Takto je to udělané, protože když se z
	 * proměnné v instanci třídy či pole, vytvořené v editoru příkazů, apod. vezmou
	 * hodnoty, tak jsou v "podobě reference", a aby se s nimi mohlo dále pracovat,
	 * je třeba jej převést do konkrétního jednorozměrného pole, k tomu tato metoda:
	 * 
	 * Jelikož musí být stejného datového typu, tak musím pro každý datový typ,
	 * který aplikace umožňuje (viz v metodě) vytvořit nové jednorozměrné pole,
	 * hodnoty do něj přetypovat a pak toto nové pole vložit do zmíněné proměnné v
	 * instanci třídy, protože daná proměnná chce nějaký typ pole, i zde už se liší
	 * int[] a Iteger[], tak zde musím pro každý podporovaný typ vytvořit nové pole
	 * 
	 * Note: Už zde nemusím testovat parsování hodnot, protože vím, že dané pole je
	 * již vytvořeno, tudíž hodnoty v něm jsou "v pořádku", stačí pouze hlídat
	 * naplnění proměnné v instanci třídy
	 * 
	 * @param objArray
	 *            - jednorozměrné pole s hodnotami, které se mají přetypovat a
	 *            vložit do nově vytvořeného pole "konkrétního" datového typu
	 * 
	 * @param dataType
	 *            - daotvý typ pole, které se má vytvořit a vložit do něj hodnoty
	 * 
	 * @return nově vytvořené pole nebo null, pokud se "něco nepovede", například se
	 *         bude jednat o pole jiného datového typu, než jaké je podporováno,
	 *         apod.
	 */
	private static Object getArrayToClassVariable(final Object[] objArray, final Class<?> dataType) {		
		
		// Otestuji datový typ - jakého má být dané pole typu, pak ho vytvořím a vložím do něj
		// přetypované hodnoty na daný datoý typ ze získaného pole - v parametru metoduy
		
		if (dataType.equals(byte.class)) {
			final byte[] array = new byte[objArray.length];

			for (int i = 0; i < objArray.length; i++)
				if (objArray[i] != null)
					array[i] = new ByteTypeParser().parse(objArray[i].toString());
			
			return array;
		}
		
		
		
		else if (dataType.equals(Byte.class)) {
			final Byte[] array = new Byte[objArray.length];

			for (int i = 0; i < objArray.length; i++)
				if (objArray[i] != null)
					array[i] = new ByteTypeParser().parse(objArray[i].toString());

			return array;
		}
		
		
		
		else if (dataType.equals(short.class)) {
			final short[] array = new short[objArray.length];

			for (int i = 0; i < objArray.length; i++)
				if (objArray[i] != null)
					array[i] = new ShortTypeParser().parse(objArray[i].toString());

			return array;
		}
		
		
		
		else if (dataType.equals(Short.class)) {
			final Short[] array = new Short[objArray.length];

			for (int i = 0; i < objArray.length; i++)
				if (objArray[i] != null)
					array[i] = new ShortTypeParser().parse(objArray[i].toString());

			return array;
		}
		
		

		else if (dataType.equals(int.class)) {
			final int[] array = new int[objArray.length];

			for (int i = 0; i < objArray.length; i++)
				if (objArray[i] != null)
					array[i] = new IntegerTypeParser().parse(objArray[i].toString());

			return array;
		}

		
		
		else if (dataType.equals(Integer.class)) {
			final Integer[] array = new Integer[objArray.length];

			for (int i = 0; i < objArray.length; i++)
				if (objArray[i] != null)
					array[i] = new IntegerTypeParser().parse(objArray[i].toString());

			return array;
		}
		
		
		
		else if (dataType.equals(long.class)) {
			final long[] array = new long[objArray.length];

			for (int i = 0; i < objArray.length; i++)
				if (objArray[i] != null)
					array[i] = new LongTypeParser().parse(objArray[i].toString());

			return array;
		}
		
		
		
		else if (dataType.equals(Long.class)) {
			final Long[] array = new Long[objArray.length];

			for (int i = 0; i < objArray.length; i++)
				if (objArray[i] != null)
					array[i] = new LongTypeParser().parse(objArray[i].toString());

			return array;
		}
		
		
		
		else if (dataType.equals(float.class)) {
			final float[] array = new float[objArray.length];

			for (int i = 0; i < objArray.length; i++)
				if (objArray[i] != null)
					array[i] = new FloatTypeParser().parse(objArray[i].toString());

			return array;
		}
		
		
		
		else if (dataType.equals(Float.class)) {
			final Float[] array = new Float[objArray.length];

			for (int i = 0; i < objArray.length; i++)
				if (objArray[i] != null)
					array[i] = new FloatTypeParser().parse(objArray[i].toString());

			return array;
		}
		
		
		
		else if (dataType.equals(double.class)) {
			final double[] array = new double[objArray.length];

			for (int i = 0; i < objArray.length; i++)
				if (objArray[i] != null)
					array[i] = new DoubleTypeParser().parse(objArray[i].toString());

			return array;
		}
		
		
		
		else if (dataType.equals(Double.class)) {
			final Double[] array = new Double[objArray.length];

			for (int i = 0; i < objArray.length; i++)
				if (objArray[i] != null)
					array[i] = new DoubleTypeParser().parse(objArray[i].toString());

			return array;
		}
		
		
		
		else if (dataType.equals(char.class)) {
			final char[] array = new char[objArray.length];

			for (int i = 0; i < objArray.length; i++)
				if (objArray[i] != null)
					array[i] = new CharacterTypeParser().parse(objArray[i].toString());

			return array;
		}
		
		
		
		else if (dataType.equals(Character.class)) {
			final Character[] array = new Character[objArray.length];

			for (int i = 0; i < objArray.length; i++)
				if (objArray[i] != null)
					array[i] = new CharacterTypeParser().parse(objArray[i].toString());

			return array;
		}
		
		
		
		else if (dataType.equals(boolean.class)) {
			final boolean[] array = new boolean[objArray.length];

			for (int i = 0; i < objArray.length; i++)
				if (objArray[i] != null)
					array[i] = new BooleanTypeParser().parse(objArray[i].toString());

			return array;
		}
		
		
		
		else if (dataType.equals(Boolean.class)) {
			final Boolean[] array = new Boolean[objArray.length];

			for (int i = 0; i < objArray.length; i++)
				if (objArray[i] != null)
					array[i] = new BooleanTypeParser().parse(objArray[i].toString());

			return array;
		}
		
		
		else if (dataType.equals(String.class)) {
			final String[] array = new String[objArray.length];

			for (int i = 0; i < objArray.length; i++)
				if (objArray[i] != null)
					array[i] = objArray[i].toString();

			return array;
		}
		
		
		
		return null;
	}
	
	
	
	
	
	
	
	

	
	
	

	
	@Override
	public String makeFullfillmentOfEditorVariableGetMethod(final String command) {
		// Zde musím zvolit trochu jiný postup, protože v parametru může být vytvoření instance nové třídy (new model.data.ClassName())
		// a v takovém případě by selhala metoda pro získání názvu druhé třídy - proměnné dle tečky
		
		// syntaxe: variableName = referenceName.methodName(p1, p2, ...);
		
		
		/*
		 * Proměnná, do které si vložím text příkazu od začátku po první rovná se.
		 */
		final String textBeforeEquals = command.substring(0, command.indexOf('='));
		
		// název proměnné, do které se má přiřadit hodnota:
		final String variableName = textBeforeEquals.replaceAll("\\s", "");
		
	
		
		// Postup:
		// Otestuji existenci instance, na kterou by měla ukazovat zadaná reference u proměnné pro naplnění - případně vyhodím hlášku
		// Otestuji existenci proměnné, vytvořenou v editoru příkazů, do které se má vložit hodnota z metody
		// Otestuji existenci metody pro získání hodnoty
		// Pokud jsou podmínky splněny, otestuji datovy typy návratové hodnoty metody a proměnné do které se á 
		// hodnota vložit - případně vyhodím hlášku
		// Na konec  - pokud budou podmínky splněny vrátím oznámím úspěch uživateli - pokud se to povede 
		
		
		
		final Variable objVariable = (Variable) Instances.getVariableFromMap(variableName);
		
		
		final VariableList<?> variableList = (VariableList<?>) Instances.getListFromMap(variableName);
		
		
		final VariableArray<?> variableArray = (VariableArray<?>) Instances.getArrayFromMap(variableName);
		
		
		final String textWithVariable = command.substring(command.indexOf('=') + 1, command.lastIndexOf(';'));
		
		
		return fullfillmentVariable(objVariable, variableArray, variableList, variableName, textWithVariable);		
	}


	
	
	
	
	
	
	
	
	
	
	






	@Override
	public String makeFullfillmentOfEditorVariableVariable(final String command) {
		// Proměnná, do které se má vložit hodnota: (od začátku po rovnáse - bez mezer:)
		final String variableName = command.substring(0, command.indexOf('=')).replaceAll("\\s", "");
		
		// Vezmu si text od rovnáse (=) po středník - konec příkazů, ten otestuji, zda se jedná o převzetí proměnné z 
		// instance třídy nebo proměnné, vytvořené v editoru příkazů:
		final String textWithVariable = command.substring(command.indexOf('=') + 1, command.lastIndexOf(';')).replaceAll("\\s", "");
				
		
		// Proměnná, do které se má vložit hodnota:
		final Variable variable = (Variable) Instances.getVariableFromMap(variableName);
		
		
		// Zde si zkusím načíst i ostatní proměnné - list a pole, pro případ, že by se jednalo o naplnění zmíněného
		// polě nebo kolekce a ne proměnné, vytvořené v editrou příkazů:
		
		final VariableList<?> variableList = (VariableList<?>) Instances.getListFromMap(variableName);
		
		final VariableArray<?> variableArray = (VariableArray<?>) Instances.getArrayFromMap(variableName);
		
		
		return fullfillmentVariable(variable, variableArray, variableList, variableName, textWithVariable);
	}

	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která naplní proměnnou, vytvořenou v editoru příkazů ("klasickou"
	 * proměnnou, list nebo pole) jiným polem nebo listem, apod.
	 * 
	 * @param variable
	 *            - proměnná, která se má naplnit
	 * 
	 * @param variableArray
	 *            - proměnná, která v editoru příkazů reprezentuje jednrozměrné pole
	 *            - instnace třídy VariableArray
	 * 
	 * @param variableList
	 *            - proměnná, která v editoru příkazů reprezentuje list - instance
	 *            třídy VariableList
	 * 
	 * @param variableName
	 *            - název proměnné, která se má naplnit
	 * 
	 * @param textWithVariable
	 *            - text s proměnnou, je to text od =, které značí naplnení po
	 *            středník coby ukončení příkazu z tohoto textu pak metoda pozná,
	 *            zda se jedná o metodu, jiné pole, list, apod.
	 * 
	 * @return Úspěch, pokud se naplnění nějaké proměnné povede, jinak Neúspěch,
	 *         případně informaci o chybě, která nastala
	 */
	private String fullfillmentVariable(final Variable variable, final VariableArray<?> variableArray,
			final VariableList<?> variableList, final String variableName, final String textWithVariable) {

		if (variable != null) {
			if (!variable.isFinal()) {

				// Opraveno na testování získání (/ naplnění) hodnoty
				final ResultValue objResultValues = getValueFromVar_Method_Constant(textWithVariable,
						variable.getDataTypeOfVariable());

				if (!objResultValues.isExecuted())
					/*
					 * Zde se nepodařillo například zavolat metodu, získat hodnotou z proměnné apod.
					 * Záleží, co bylo v proměnné textWithVariale za operaci, která z nějakého
					 * důvodu selhala. Výpisy už by měly být vypsány u nepodvedení operace, proto
					 * zde vrátím pouze Neúspěch.
					 */
					return txtFailure;

				/*
				 * Zde se operace v proměnné textWithVariable povedla a vrátila se nějaká
				 * hodnota, tak ji vložím do proměnné:
				 */
				final Object objValue = objResultValues.getObjResult();

				if (objValue != null) {
					// naplním proměnnou:
					variable.setObjValue(objValue);

                    /*
                     * Výše se naplnila hodnota nějaké proměnné, je zde třeba aktualizovat okno s hodnotami pro
                     * automatické dokončování / doplňování hodnot do editoru příkazů, protože se může nějaká
                     * proměnná zobrazovat špatně, resp. její hodnota nemusí být aktuální, kdybych jej zde
                     * neaktualizoval.
                     */
                    updateValueCompletionWindow();

					// Pokud nedojde k chybě, potvrdím úspěšné dokončení operace:
					return txtSuccess;
				}
			} else return txtFailure + ": " + txtVariable + ": '" + variableName + "' " + txtValueIsFinalAndCannotBeChanged;
		}
		
		
		
		
		// Zde nebyla nalezena "klasická proměnná", vytvořená v editoru příkazů, tak otestuji, zda se nejedná o list, 
		// vytvořený v editrou příkazů, který se má naplnit:
		else if (variableList != null) {
			if (!variableList.isFinal()) {
				
				// Opraveno na testování získání, zda se podařilo získat hodnotu:
				final ResultValue objResultValues = getValueFromVar_Method_Constant(textWithVariable, VariableList.CLASS_LIST);
				
				if (!objResultValues.isExecuted())
					/*
					 * Zde se nepodařillo například zavolat metodu, získat hodnotou z proměnné apod.
					 * Záleží, co bylo v proměnné textWithVariale za operaci, která z nějakého
					 * důvodu selhala. Výpisy už by měly být vypsány u nepodvedení operace, proto
					 * zde vrátím pouze Neúspěch.
					 */
					return txtFailure;

				/*
				 * Zde se operace v proměnné textWithVariable povedla a vrátila se nějaká
				 * hodnota, tak ji vložím do proměnné:
				 */
				final List<?> objList = (List<?>) objResultValues.getObjResult();

				if (objList != null) {
					// zde nejak ziskat datovy typ listu?? asi jako na nasledujicim zdroji - nejde s
					// generikou?!!!:
					// http://stackoverflow.com/questions/1942644/get-generic-type-of-java-util-list

					// Zde mohu otestovat pouze datové typy tak, že se jedná o kolekce nebo ne, ale
					// už nemůžu otestovat,
					// hodnoty, které lze do dané kolekce (Listu) vkládat nebo ne například
					// List<Integer | String, apod>
					// proto pokud se rozhodne uživatel naplnit "list listem" (kolekce) tak si už
					// musí hlídat, zda jsou stejného
					// datového typu nebo ne, jinak pokud se bude jednat opravdu o kolekce - Listy,
					// tak se zmíněné naplnění
					// provede vždy

					// naplním proměnnou:
					variableList.setList(objList);

                    /*
                     * Aby se projevila naplněná proměnná v okně pro automatické doplňování / dokončování hodnot v
                     * editoru příkazů.
                     */
                    updateValueCompletionWindow();

					// Pokud nedojde k chybě, potvrdím úspěšné dokončení operace:
					return txtSuccess;
				}
			} else
				return txtFailure + ": " + txtVariable + ": '" + variableName + "' "
						+ txtValueIsFinalAndCannotBeChanged;
		}
		
		
		
		else if (variableArray != null) {
			if (!variableArray.isFinal()) {

				// Oprava kvůli povedení zavolání:
				final ResultValue objResultValues = getValueFromVar_Method_Constant(textWithVariable,
						variableArray.getClassTypeOfArrayForMethod());

				if (!objResultValues.isExecuted())
					/*
					 * Zde se nepodařillo například zavolat metodu, získat hodnotou z proměnné apod.
					 * Záleží, co bylo v proměnné textWithVariale za operaci, která z nějakého
					 * důvodu selhala. Výpisy už by měly být vypsány u nepodvedení operace, proto
					 * zde vrátím pouze Neúspěch.
					 */
					return txtFailure;

				/*
				 * Zde se operace v proměnné textWithVariable povedla a vrátila se nějaká
				 * hodnota, tak jí naplním pole:
				 */
				final Object objValue = objResultValues.getObjResult();

				if (objValue != null) {
					// naplním proměnnou:
					variableArray.setArray(objValue);

                    /*
                     * Aktualizuji okno s hodnotami pro automatické doplnění / dokončení do editoru příkazů, kdyby se
                     * zde neaktualizovaly hodnoty, nebyly by v okně aktuální hodnoty:
                     */
                    updateValueCompletionWindow();

					// Pokud nedojde k chybě, potvrdím úspěšné dokončení operace:
					return txtSuccess;
				}
			} else
				return txtFailure + ": " + txtVariable + ": '" + variableName + "' "
						+ txtValueIsFinalAndCannotBeChanged;
		}

		return txtFailure;
	}
	
	
	
	
	
	






	@Override
	public String makeFullfillmentOfEditorVariableConstant(final String command) {
		// Název proměnné je od začátku po rovnáse (=) - bez mezer:
		final String variableName = command.substring(0, command.indexOf('=')).replaceAll("\\s", "");
		
		
		// Hodnota pro naplnění - od rovná se do konce - po středník:
		final String valueFoAsign = command.substring(command.indexOf('=') + 1, command.lastIndexOf(';')).trim();
		
		
		// Postup:
		// Zjistím si danou proměnnou - pokud existuje
		// pak její typ, a pokud bude existovat, tak zkusím zadanou hodnotu naparsovat na dany typ hodnoty
		// a naplnit danou proměnnou
		// Případně vypíšu nějaké chybové upozornění
		
		
		
		final Variable variable = (Variable) Instances.getVariableFromMap(variableName);
				
		if (variable != null) {
			if (!variable.isFinal()) {
				// Zde jsem našel požadovanou proměnnou, tak zkusím přetypovat zadanou hodnotu
				// na danou proměnnou:
				final ResultValue objResultValues = getValueFromVar_Method_Constant(valueFoAsign,
						variable.getDataTypeOfVariable());

				/*
				 * Pokud se nevrátlo ResultValues (nemělo by nastat) nebo nebyla provedena
				 * příslušná operace pro získání konstanty, pak vypíšu neúspěch s tím, že by
				 * informace o nastalé chybě již měly být vypsány v editoru výstupů.
				 */
				if (objResultValues == null || !objResultValues.isExecuted())
					return txtFailure;

				if (objResultValues.getObjResult() != null) {
					// Zde se podařilo přetypovat hodnotu, tak jí mohu naplnit proměnnou:
					variable.setObjValue(objResultValues.getObjResult());

                    /*
                     * Potřebuji zde aktualizovat okno s hodnotami v okně pro automatické doplnění / dokončení v
                     * editoru příkazů, jinak by se v tom okně zobrazovaly neaktuální hodnoty.
                     */
                    updateValueCompletionWindow();

					return txtSuccess;
				} 
				else
					return txtFailure + ": " + txtFailedRetypeValue + OutputEditor.NEW_LINE_TWO_GAPS + txtError + ": "
							+ txtValue + ": '" + valueFoAsign + "' " + txtToType + ": "
							+ variable.getDataTypeOfVariable();
			} 
			else
				return txtFailure + ": " + txtVariable + ": '" + variableName + "' "
						+ txtValueIsFinalAndCannotBeChanged;
		}
		else
			return txtFailure + ": " + txtVariableNotFoundForInsertValue + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtVariableNotFound + ": " + variableName;
	}




	
	
	
	

	
	
	@Override
	public String makeDeclarationEditorVariable(final String command) {
		final Object[] objEditorVariableData = getEditorVariableData(command);

		final boolean isEditorVariableCreated = createEditorVariable(objEditorVariableData, null);

		if (isEditorVariableCreated) {
            /*
             * Aktualizuji zde hodnoty v okně pro automatické dokončení / doplnění hodnot do editoru příkazů, jinak
             * by se v tom okně nezobrazovala tato nově vytvořená hodnota.
             */
            updateValueCompletionWindow();

            return txtSuccess;
        }


		return txtFailure;
	}
	
	
	
	
	
	
	
	
	
	
	
	

	
	/**
	 * Metoda, která vytvoří instance třídy Variable coby proměnnou vytvořenou v
	 * editoru příkazů pro uchování hodnot
	 * 
	 * Dále pokud se povede vytvořit tuto instanci třídy Variable, s příslušným
	 * datovým typem, tak se přidá mezo ostatní instance třídy a proměnné - do
	 * hashMapy v Instances
	 * 
	 * Pokud dojde někde k chybě, vypíše se oznámíní s příslušnou chybou do editoru
	 * výstupů
	 * 
	 * @param objEditroVaribleData
	 *            - informace o proměnné, která se má deklarovat
	 * 
	 * @param objValue
	 *            - hodnota proměnné Variable
	 * 
	 * @return true, pokud se povede úspěšně vytvořit proměnnou, jinak false
	 */
	private boolean createEditorVariable(final Object[] objEditroVaribleData, final Object objValue) {
		if (objEditroVaribleData != null) {
			final boolean isFinal = (Boolean) objEditroVaribleData[0];
			
			final Class<?> dataType = (Class<?>) objEditroVaribleData[1];
			
			final String variableName = (String) objEditroVaribleData[2];
			
			if (dataType != null) {
				if (!variableName.equals("")) {
					// Otestuji, zda název proměnné ještě neexistuje
					if (!Instances.isReferenceNameInMap(variableName)) {
						// Zde název proměnné ještě neexistuje

						
						// Jestli je hodnota null, tak nemůže být final,
						// jinak když hodnota neni null, tak může být final
						if (objValue != null) {
							// Zde hodnota není null, tak už nemusím testovat, zda je final nebo ne, vytvořím proměnnou:
							return createVariable(variableName, dataType, isFinal, objValue);
						} 	
						
						
						else {
							// Zde je hodnota null, tak nesmí být final:
                            if (!isFinal) {
                                // Zde proměnná neni final, tak ji mohu vytvořit i s null hodnotou:
                                return createVariable(variableName, dataType, isFinal,
                                        objValue);
                            }
							// Zde je hodnota null a proměnná je final, tak nepůjde vytvořit:
							else
								outputEditor.addResult(txtFailure + ": " + txtVariableIsFinalIsNeedToFillInCreation
										+ OutputEditor.NEW_LINE_TWO_GAPS + txtVariable + ": '" + variableName + "' "
										+ txtMustNotBeFinalAndNull); 
						}					
					} 
					else
						outputEditor.addResult(txtFailure + ": " + txtVariableNameIsNeedToChangeIsAlreadyTaken
								+ OutputEditor.NEW_LINE_TWO_GAPS + txtVariable + ": " + variableName);
				}
				// Zde by tato možnost nastat něměla, ale pro jistotu:
				else
					outputEditor.addResult(txtFailure + ": " + txtVariableNameNotRecognized
							+ OutputEditor.NEW_LINE_TWO_GAPS + txtVariable + " '" + variableName + "'");
			}
			else
				outputEditor.addResult(txtFailure + ": " + txtDataTypeNotFoundOnlyPrimitiveDataTypes
						+ OutputEditor.NEW_LINE_TWO_GAPS + txtDataType + " = null");
		}
		// else - bud název proměnné je názvem nějakého slova Javy, jinak na zbytek příjdu v podmínce
		
		return false;
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvořít instance třídy Vairable coby proměnnou v editoru
	 * příkazů a naplní ji hodnottami v parametr pak vrátí true, jako že byla
	 * proměnná vytvořená, pokud ne, tak vrátí false
	 * 
	 * @param referenceName
	 *            - název proměnné, pod kterou se k dané hodnotě bud epřistupovat
	 * 
	 * @param dataType
	 *            - datový typ proměnné
	 * 
	 * @param isFinal
	 *            - logická hodnota, zda je proměnná final nebo ne
	 * 
	 * @param objValue
	 *            - hodnota proměnné
	 * 
	 * @return true, pokud se vytvoří Variable, jinak false
	 */
	private boolean createVariable(final String referenceName, final Class<?> dataType, final boolean isFinal,
			final Object objValue) {
		// Deklaruji si proměnnou, jejíž instanci a typ doplním dle podmínky:
		Variable variable = null;
		
		
		if (dataType.equals(byte.class))
			variable = new Variable(objValue, isFinal, byte.class);
		
		
		else if (dataType.equals(Byte.class))
			variable = new Variable(objValue, isFinal, Byte.class);
		
		
		else if (dataType.equals(short.class))
			variable = new Variable(objValue, isFinal, short.class);
		
		
		else if (dataType.equals(Short.class))
			variable = new Variable(objValue, isFinal, Short.class);

		
		else if (dataType.equals(int.class))
			variable = new Variable(objValue, isFinal, int.class);
		
		
		else if (dataType.equals(Integer.class))
			variable = new Variable(objValue, isFinal, Integer.class);
		
		
		else if (dataType.equals(long.class))
			variable = new Variable(objValue, isFinal, long.class);
		
		
		else if (dataType.equals(Long.class))
			variable = new Variable(objValue, isFinal, Long.class);
		
		
		else if (dataType.equals(float.class))
			variable = new Variable(objValue, isFinal, float.class);

		
		else if (dataType.equals(Float.class))
			variable = new Variable(objValue, isFinal, Float.class);
		
		
		else if (dataType.equals(double.class))
			variable = new Variable(objValue, isFinal, double.class);
		
		
		else if (dataType.equals(Double.class))
			variable = new Variable(objValue, isFinal, Double.class);
		
		
		else if (dataType.equals(char.class))
			variable = new Variable(objValue, isFinal, char.class);
		
		
		else if (dataType.equals(Character.class))
			variable = new Variable(objValue, isFinal, Character.class);

		
		else if (dataType.equals(boolean.class))
			variable = new Variable(objValue, isFinal, boolean.class);
		
		
		else if (dataType.equals(Boolean.class))
			variable = new Variable(objValue, isFinal, Boolean.class);
		
		
		else if (dataType.equals(String.class))
			variable = new Variable(objValue, isFinal, String.class);		
		
		
		if (variable != null) {
			// Přidám instanci proměnné di hashmapy s instancemi tříd a proměnných
			Instances.addInstanceToMap(referenceName, variable);
			return true;
		}

		else
			outputEditor.addResult(txtFailure + ": " + txtFailedToCreateTemporaryVariable
					+ OutputEditor.NEW_LINE_TWO_GAPS + txtData + ": " + txtVariableName + ": " + referenceName + ", "
					+ txtDataType + ": " + dataType + ", " + txtValue2 + ": " + objValue + "."
					+ OutputEditor.NEW_LINE_TWO_GAPS + txtMaybeError + ": " + txtDataTypeDoNotSupported);

		
		// Nenastala chyba, takže mohu vrátit true, jinak by někde nastala vyjímka - což by němelo
		return false;
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která si z textu v parametru zjistí, zda má být proměnná final,
	 * jakého má být datového typu, a jaký název má mít, resp. přesněji v tomto
	 * případě název reference na danou proměnnou
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů nebo jeho část
	 *            Konkrétně část v syntaxí buď pro deklarace proměnné nebo první
	 *            část z naplnění: Syntaxe: (final) dataType variableName(;)
	 * 
	 * @return jednorozměrné pole typu objekt se třemi hodnotami nebo null, pokud
	 *         dojde k nějaké chybě pole s hodnotami bude: na prvním místě bude
	 *         logická hodnota o tom, zda proměnná má být final, na druhém datový
	 *         typ proměnné a na třetím místě název proměnné
	 */
	private Object[] getEditorVariableData(final String command) {
		final String[] partsOfCommand = command.split("\\s");				
		
		// Zda má být proměnná final:
		boolean containFinal = false;

		// datový typ proměnné
		Class<?> dataType = null;

		// Název proměnné:
		String variableName = "";

		for (final String s : partsOfCommand) {
			if (s.equalsIgnoreCase("final"))
				containFinal = true;
			
			
			else if (s.equals("byte"))
				dataType = byte.class;
			else if (s.equals("Byte"))
				dataType = Byte.class;
			
			else if (s.equals("short"))
				dataType = short.class;
			else if (s.equals("Short"))
				dataType = Short.class;
			
			else if (s.equals("int"))
				dataType = int.class;
			else if (s.equals("Integer"))
				dataType = Integer.class;
			
			else if (s.equals("long"))
				dataType = long.class;
			else if (s.equals("Long"))
				dataType = Long.class;
			
			else if (s.equals("float"))
				dataType = float.class;
			else if (s.equals("Float"))
				dataType = Float.class;
			
			else if (s.equals("double"))
				dataType = double.class;
			else if (s.equals("Double"))
				dataType = Double.class;
			
			else if (s.equals("char"))
				dataType = char.class;
			else if (s.equals("Character"))
				dataType = Character.class;
			
			else if (s.equals("boolean"))
				dataType = boolean.class;
			else if (s.equals("Boolean"))
				dataType = Boolean.class;
			
			else if (s.equals("String"))
				dataType = String.class;
			
			
			// Jinak už jiné znaky nezbývají mezery tam nejsou final ani datový
			// typ to také není, tak už zbýva pouze název proměnné
			else if (regEx.isVariableInEditor(s))
				variableName = s;
			// Je možné, že na konci textu - výrazu se nachází středník, ten do názvu nepatří
			else if (s.contains(";") && regEx.isVariableInEditor(s.replace(";", ""))) variableName = s.replace(";", "");
		}							
		
		
		// Otestuji, zda se nejedná o klíčové slovo Javy:
		final boolean isKeyWordOfJava = isKeyWordOfJava(variableName);
		
		
		if (!isKeyWordOfJava)
			return new Object[] { containFinal, dataType, variableName };

		else {
			outputEditor.addResult(txtFailure + ": " + txtVariableNameMustNotBeKeyWord + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtName + ": " + variableName);
			return null;
		}
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda text v parametru této metody je klíčové slovo
	 * Javy nebo ne
	 * 
	 * @param variableName
	 *            název proměnné, o kterém se má zjistit, zda je to klíčové slovo
	 *            Javy nebo ne
	 * 
	 * @return true, pokud je text v parametru (variableName) klíčové slovo Javy,
	 *         jinak false, pokud tak není
	 */
	private static boolean isKeyWordOfJava(final String variableName) {
		boolean isKeyWordOfJava = false;
				
		// Otestuji, zda název proměnné v editoru příkazů není klíčové slovo Javy.
		for (final String s : Constants.getListOfKeyWords()) {
			if (s.equalsIgnoreCase(variableName)) {
				isKeyWordOfJava = true;
				break;
			}				
		}
		
		return isKeyWordOfJava;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	






	
	
	@Override
	public String makeFullfillmentOfVariableGetMethod(final String command) {
		// Zde musím zvolit trochu jiný postup, protože v parametru může být vytvoření instance nové třídy (new model.data.ClassName())
		// a v takovém případě by selhala metoda pro získání názvu druhé třídy - proměnné dle tečky
		
		// Tak musím text rozdělit do pole dle rovná se
		// v první části je jedn jedna tečka oddelující název proměnn a název proměné třídy
		// v druhé části pak může být více teček - díky parametru, ale metoda je v druhé části od první tečky po první otevárací závorku		
		
		/*
		 * Proměnná, do které si vložím text příkazu od začátku po první rovná se.
		 */
		final String textBeforeEquals = command.substring(0, command.indexOf('='));
		
		
		
		// Proměnná ukazující na instanci třídy: od začátku po první tečku:
		final String classNameVariable = textBeforeEquals.substring(0, textBeforeEquals.indexOf('.')).replaceAll("\\s", "");
		

		// Proměnná, do které se má přiřadit hodnota: - od první desetinné tečky po rovná se
		final String classVariable = textBeforeEquals.substring(textBeforeEquals.indexOf('.') + 1).replaceAll("\\s", "");
		
		
		
		// Postup:
		// Otestuji existenci instance, na kterou by měla ukazovat zadaná reference u proměnné pro naplnění - případně vyhodím hlášku
		// Otestuji existenci instance, na kterou by mela ukazovat zadaná reference u metody
		// otestuji existejnci proměnné, v dané instanci třídy - případně vyhodím hlášku
		// Otestuji existenci metody pro získání hodnoty
		// Pokud jsou podmínky splněny, otestuji datovy typy návratové hodnoty metody a proměnné do které se á 
		// hodnota vložit - případně vyhodím hlášku
		// Na konec  - pokud budou podmínky splněny vrátím oznámím úspěch uživateli - pokud se to povede 
		
		
		
		final Object objInstance1 = Instances.getInstanceFromMap(classNameVariable);
		
		
		
		
		if (objInstance1 != null) {
			// Zde byly v pořádku načteny obě instance, na které ukazují zadané reference v
			// editoru příkazů:
			// Načtu si proměnnou:
			final Field field = getFieldFromInstance(objInstance1, classVariable);

			// Otestuji null hodnotu, abych předešel vyjímkám:
			if (field != null) {

				// Nejdříve otestuji, zda není proměnná final:
				if (!Modifier.isFinal(field.getModifiers())) {
					// Zjistím si text z příkazu, který by měl obsahovat metodu:
					final String textWithMethod = command.substring(command.indexOf('=') + 1, command.lastIndexOf(';'));

					final ResultValue objResultValue = getValueFromVar_Method_Constant(textWithMethod, field.getType());

					/*
					 * Pokud se nepodařilo vrátit ReturnValue (nemělo by nastat) nebo nebyla
					 * provedena operace pro zavolání metody, pak vypíšu neúspěch s tím, že by
					 * informace ohledně chyby, ke které došlo měly být vypsány do editoru výstupů.
					 */
					if (objResultValue == null || !objResultValue.isExecuted())
						return txtFailure;

					/*
					 * Pro zjednodušení si zde vytvořím proměnnou, do které vložím získanou hodnotu
					 * z metody (abych jí nemusel pořád načítat).
					 */
					final Object objValue = objResultValue.getObjResult();

					if (objValue != null) {
						// Nejprve otestuji, zda se náhodou nejedná o pole:
						if (objValue.getClass().isArray()) {
							new ParseToJavaTypes();
							final Object[] objArray = ParseToJavaTypes.convertToObjectArray(objValue);

							final Object objArrayValue = getArrayToClassVariable(objArray,
									field.getType().getComponentType());

							return setValueToClassVariable(field, objArrayValue, objInstance1);
						}

						return setValueToClassVariable(field, objValue, objInstance1);
					}
				}
				else
					return txtFailure + ": " + txtVariable + ": " + field.getName() + " "
							+ txtVariableIsFinalDontChangeValue;
			} 
			else
				return txtFailure + ": " + txtVariableNotFoundInReferenceToInstanceOfClass;

		}
		else
			return txtFailure + ": " + txtAtLeastOneInstanceOfClassNotFound;

		return txtFailure;
	}


	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public String makeFulfillmentOfVariableByCallingConstructor(final String command) {
		// Syntaxe: instanceName.variableName = new packageName. (anotherPackageName.) ClassName(parameters);
		
		/*
		 * Proměnná, do které si vložím text příkazu od začátku po první rovná se.
		 */
		final String textBeforeEquals = command.substring(0, command.indexOf('='));
		
		
		
		// Proměnná ukazující na instanci třídy: od začátku po první tečku:
		final String classNameVariable = textBeforeEquals.substring(0, textBeforeEquals.indexOf('.')).replaceAll("\\s", "");
		

		// Proměnná, do které se má přiřadit hodnota: - od první desetinné tečky po rovná se
		final String classVariable = textBeforeEquals.substring(textBeforeEquals.indexOf('.') + 1).replaceAll("\\s", "");
		
		
		
		// Postup:
		// Otestuji existenci instance, na kterou by měla ukazovat zadaná reference u proměnné pro naplnění - případně vyhodím hlášku
		// Otestuji existenci instance, na kterou by mela ukazovat zadaná reference u metody
		// otestuji existejnci proměnné, v dané instanci třídy - případně vyhodím hlášku
		// Otestuji existenci metody pro získání hodnoty
		// Pokud jsou podmínky splněny, otestuji datovy typy návratové hodnoty metody a proměnné do které se á 
		// hodnota vložit - případně vyhodím hlášku
		// Na konec  - pokud budou podmínky splněny vrátím oznámím úspěch uživateli - pokud se to povede 
		
		
		final Object objInstance1 = Instances.getInstanceFromMap(classNameVariable);

		if (objInstance1 == null)
			return txtFailure + ": " + txtAtLeastOneInstanceOfClassNotFound;

		
		
		/*
		 * Nyní si mohu načíst proměnnou v dané instanci třídy.
		 */
		final Field field = getFieldFromInstance(objInstance1, classVariable);
		
		// Otestuji null hodnotu, abych předešel vyjímkám:
		if (field == null)
			return txtFailure + ": " + txtVariableNotFoundInReferenceToInstanceOfClass;

		
		
		// Nejdříve otestuji, zda není proměnná final:
		if (Modifier.isFinal(field.getModifiers()))
			return txtFailure + ": " + txtVariable + ": " + field.getName() + " " + txtVariableIsFinalDontChangeValue;

		
		
		// Zjistím si text z příkazu, který by měl obsahovat metodu:
		final String textWithMethod = command.substring(command.indexOf('=') + 1, command.lastIndexOf(';'));

		// Zkusím zavolat konstruktor a měla by se vrátit instance:
		final ResultValue objResultValue = getValueFromVar_Method_Constant(textWithMethod, field.getType());

		
		/*
		 * Pokud se nepodařilo vrátit ReturnValue (nemělo by nastat) nebo nebyla
		 * provedena operace pro zavolání metody, pak vypíšu neúspěch s tím, že by
		 * informace ohledně chyby, ke které došlo měly být vypsány do editoru výstupů.
		 */
		if (objResultValue == null || !objResultValue.isExecuted())
			return txtFailure;

		
		/*
		 * Pro zjednodušení si zde vytvořím proměnnou, do které vložím získanou hodnotu
		 * z metody (abych jí nemusel pořád načítat).
		 */
		final Object objValue = objResultValue.getObjResult();

		if (objValue != null)
			return setValueToClassVariable(field, objValue, objInstance1);

		return txtFailure;
	}
	



	
	
	






	
	
	
	/**
	 * Metoda, která si zjistí, zda se jedná o metodu bez parametrů nebo s
	 * parametry, zavolá ji a pokud vše dopadne dopře vrátí získanou hodnotu v
	 * podobně jednorozměrného pole, kde na prvním místě bude zda se metody zavolaly
	 * - kvůli vrácení null - metoda může vrátit null ale totéž je i výchozí hodnota
	 * proměnné, tak abych zbytečně neměnil hdontu proměnné, do které se vložít
	 * získaná hodnota z metody, apod.
	 * 
	 * a na druhém místě bude hodnota, kterou vrátí proměnná, null, konstanta, apod.
	 * 
	 * Note: toto jsem takto udělal, protože potřebuji zjistit, zda mám se metoda
	 * zavolala a nějakou hodnotu vrátla ať už null nebo něco jiného protože kdyby
	 * ne, tak získaná hodnota bude vždy null - výchozí hodnota objektu, a tou by se
	 * vždy mohla naplnit nějaka promnná, do které se vloží získaná hodnota, apod,
	 * pokud metoda vrátí null, je to v pořádku, ale poked nastane chyba, a metoda
	 * se nezavolá, tak výchozí hodnota bude sice null ale touto hodnotou by se
	 * neměla proměnná naplnit, protože ji nevrátila metoda
	 * 
	 * @param textOfTheMethod
	 *            - text metody z editoru příkazů v syntaxe:
	 *            referenceName.methodName();
	 * 
	 * @param objOwnerMethod
	 *            - instance třídy, ve které by se metoda měla nacházet
	 * 
	 * @param methodName
	 *            - název metody, která se má zavolat
	 * 
	 * @param dataTypeOfField
	 *            - datový typ, který by metoda měla vrátit
	 * 
	 * @param partsOfParameter
	 *            - pokud metoda obsahuje parametry, tak v tomto poli se budou
	 *            nacházet v podobě Stringu, dále se pokusí přetypovat na typ, který
	 *            vyžaduje metoda, ... viz metoda
	 * 
	 * @return jednorozměrné pole, na prvním místě bude logická hodnota, zda se
	 *         metoda zavolala a vrátila nějakou hodnotu - pak tam bude true jinak
	 *         false a na druhém místě bude hodnota, kterou metoda vrátí a na třetím
	 *         logická hodnota, která značí, zda metoda něco vrací nebo ne (true,
	 *         něco vrací, false - je void).
	 */
    private Object[] callTheMethod(final String textOfTheMethod, final Object objOwnerMethod, final String
            methodName, final Class<?> dataTypeOfField, final String[] partsOfParameter) {
		
		Object objReturnValue = null;
		
		// Prměnná, dle které zjistím, zda se má naplnit daná hodnota nebo ne:
		// Zda se metoda zavolala a něco vrátila:
		boolean wasFilling = false;
		
		/*
		 * proměnná, která značí, zda je metoda void nebo něco vrací.
		 */
		boolean isVoid = false;
		
		
		if (regEx.isCallTheMethodWithoutParameters(textOfTheMethod)) {
			// Zde se jedná o zavolání metody bez parametru, ta může být přetížená, ale už
			// musí obsahovat alespoň jeden
			// parametr, což není tento případ, takže ji mohu vklidu načíst

			/*
			 * Zde si získám list všech veřejných a chráněných metod, které se vyskytují jak
			 * v třídě samotné, tak i ve všech jeho předcích, protože i tyto metody lze
			 * zavolat.
			 * 
			 * Dále, jelikož se v tomto listu nachází v podstatě všech metody, které lze
			 * zavolat a v tom pořadí, jak je dědičnost, tj. nejprve jsou načteny všechny
			 * veřejné a chráněné metody z třídy samotné (objOwnerMethod), pak všechny
			 * metody (veřejné a chráněné) z předka objOwnerMethod a tak dále, takže pokud
			 * se najde i nějaká metoda, která je Override apod. Nebo jen stejného názvu
			 * apod. tak se vždy najde v té třídě, co je nejblíže.
			 * 
			 * Takže i když se zadá metoda xxx, který je i v předku dané třídy, tak se
			 * zavolá v té původní třídě objOwnerMethod, ne v předkovi, což je správně, tak
			 * by to mělo být. Takže nemusím zvláště řešit, která metoda by se měla zavolat
			 * nebo načíst apod.
			 */
			final List<Method> availableMethods = getAvailableMethods(objOwnerMethod.getClass(), new ArrayList<>(),
					false);
				
			/*
			 * Zde zkusím najít tu jednu zadanou metodu bez parametrů, proto je netestuji,
			 * jedná se pouze o nalezení metody, u které se shoduje její název. Bude vrácena
			 * metoda s příslušným názvem, která se najde jako první, viz hierarchie popsána
			 * výše.
			 */
			final Method method = getFirstMethod(methodName, availableMethods);

			/*
			 * Zde potřebuji otestovat, zda byla nějaká metoda nalezena, jiank musím skončit
			 * a ještě vypíšu oznámení, že nebyla nalezena příslušná metoda.
			 */
			if (method == null) {
				outputEditor.addResult(txtFailure + ": " + txtErrorMethodOrParametersOrModifier);
				return new Object[] { wasFilling, objReturnValue, isVoid };
			}

			// Pokud se proměnná dataTypeOfField == null, pak se nemají porovnávat datové
			// typy - pouze zavolat metoda:
			if (dataTypeOfField != null) {
				// Zde je zadán nějaký datový typ, takže je musím porovnat

				if (method.getReturnType().equals(dataTypeOfField)) {
					final Called called = operationWithInstanees.callMethodWithoutParameters(method, objOwnerMethod,
							false);

					if (called != null && called.isWasCalled()) {
						objReturnValue = called.getObjReturnValue();

						wasFilling = called.isWasCalled();
					}

					// metoda se zavolala a něco vrátila:
					else
						wasFilling = false;
				}

				else
					outputEditor.addResult(txtFailure + ": " + txtDataTypesAreNotEqual + ": " + method.getReturnType()
							+ " != " + dataTypeOfField);
				
				// Uložím si hodnotu o tom, zda metoda něco vrací nebo ne:
				isVoid = method.getReturnType().equals(Void.TYPE);
			}

			else {
				// Zde je datový typ kterému si má rovnat návratový typ metody null, takže se má
				// metoda jen zavolat bez ohledu na nějaké
				// testování typu návratové hodnoty:
				final Called called = operationWithInstanees.callMethodWithoutParameters(method, objOwnerMethod, false);

				if (called != null && called.isWasCalled()) {
					objReturnValue = called.getObjReturnValue();

					wasFilling = called.isWasCalled();
				}

				// metoda se zavolala a něco vrátila:
				else
					wasFilling = false;
				
				// Uložím si hodnotu o tom, zda metoda něco vrací nebo ne:
				isVoid = method.getReturnType().equals(Void.TYPE);
			}
		}
		
		
		
		
		
		
		// Zde ještě otestuji, zda se nejedná o získání hodnoty z listu - kolekce na nejakém indexu:
		else if (objOwnerMethod instanceof VariableList<?>) {
			
			// Příkaz prošel regulárním výrazem, tak se mohu pokusit přetypovat zadané číslo na Integer,
			// abych mohl získat hodnotu na daném indexu:
			final int index = Integer.parseInt(partsOfParameter[0]);
			
			
			final VariableList<?> variableList = (VariableList<?>) objOwnerMethod;
			
			
			// Nyní musím otestovat, zda je index v rozsahu hodnot kolekce:
			final int sizeOfList = variableList.getList().size();
			
			
			if (index >= 0 && index < sizeOfList) {
				// Pokud se proměnná dataTypeOfField == null, pak se nemají porovnávat datové typy - pouze zavolat metoda:
				if (dataTypeOfField != null) {
					// Zde je zadán nějaký datový typ, takže je musím porovnat
					if (variableList.getClassType().equals(dataTypeOfField)) {						
						objReturnValue = variableList.getValueAtIndex(index);
						
						// metoda se zavolala a něco vrátila:
						wasFilling = true;
					}
					else
						outputEditor.addResult(txtFailure + ": " + txtDataTypesAreNotEqual + ": "
								+ variableList.getClassType() + " != " + dataTypeOfField);
				}
				
				else {
					// Zde je datový typ kterému si má rovnat návratový typ metody null, takže se má metoda jen zavolat bez ohledu na nějaké 
					// testování typu návratové hodnoty:
					objReturnValue = variableList.getValueAtIndex(index);
					
					// metoda se zavolala a něco vrátila:
					wasFilling = true;
				}
				
			}
			else
				outputEditor.addResult(txtFailure + ": " + txtConditionsForIndexOfList + OutputEditor.NEW_LINE_TWO_GAPS
						+ txtSpecifiedIndexOfList + ": " + index);
		}
		
		
		
		
		
		
		
		// Zde otestuji zavolání metody s parametry:
		else if (regEx.isCallTheMethodWithParameters(textOfTheMethod)) {
			// Zde metoda obsahuje alespoň jeden parametr, takže si musím vzít všechny metdy a najít tu správnou se 
			// sprvným názvem a počtem parametrů:
			
			/*
			 * Získám si list s metodami, které je možné zavolat nad instancí třídy
			 * objOwnerMethod (instance třídy), jedná se o všechny veřejné a chráněné metody
			 * nad intancí samotnou, tak nad všemi jejími předky.
			 */
			final List<Method> availableMethodsList = getAvailableMethods(objOwnerMethod.getClass(), new ArrayList<>(),
					false);			
			
			// Náhrada, aby šlo zavolat i metody z předků.
			for (final Method m : availableMethodsList) {
				if (m.getName().equals(methodName) && m.getParameterCount() == partsOfParameter.length) {
					// Zde jsem našel metodu, která má stejný název a počet parametrů
					// tak zkusím zadané parametry na dané typy přetypovat:
					final List<Object> parametersList = getListOfParsedParameters(partsOfParameter, null, m);
					

					
					if (parametersList != null) {
						// Zde se podařilo přetypovat zadaný text v editoru na nějaké datové typy
						
						
						// Do následujícího listu vložím "otestované" parametry s tím, že otestuji, zda nějaký z parametrů není
						// jednorozměrné pole, pokud ano, tak ho musím přetypovat na dané pole, protože se v tomto případě
						// jedná pouze o tu "referenční" verzi či jak to nazvat, prostě to není klasické - potřebné pole s hodnotami
						// ve stavu, abych ho mohl předat, tak ho převezu na "normální pole", abych ho následně mohl předat do metody:
						final List<Object> listOfTestedParameters = getListOfTestedParameters(parametersList, m);
						
						
						// pokud je dataTypeOfField == null, pak se nemají porovnávat datové typy ale jen zavoalt metoda:
						if (dataTypeOfField != null) {
							// Otestuji, zda se rovná návratový typ metody a typ proměnné, do které se má umístit:
							if (m.getReturnType().equals(dataTypeOfField)) {
								// Zde je metoda, se stejným názvem, počtem parametrů a návratovým typem hodnoty jako proměnná
								// a podařlo se mi přetypovat hodnoty z editoru na přislušné typy této metody, zkusím ji zavolat:
								final Called called = operationWithInstanees.callMethodWithParameters(m, objOwnerMethod,
										false, listOfTestedParameters.toArray());

								if (called != null && called.isWasCalled()) {
									objReturnValue = called.getObjReturnValue();

									wasFilling = called.isWasCalled();
								}
								
								// Metoda se zavolala a něco vrátila:
								else
									wasFilling = false;
								
								// Uložím si hodnotu o tom, zda metoda něco vrací nebo ne:
								isVoid = m.getReturnType().equals(Void.TYPE);
								
								// Zde se mi podařilo zavoalat metodu a v proměnné objReturnValue mam návratovou
								// hodnotu, mohu ukončit cyklus:
								break;
							}

							else
								outputEditor.addResult(txtFailure + ": " + txtDataTypesAreNotEqual + ": "
										+ m.getReturnType() + " != " + dataTypeOfField);
						}
						
						
						else {
							// Zde mám poze zavolat metodu:
							final Called called = operationWithInstanees.callMethodWithParameters(m, objOwnerMethod,
									false, listOfTestedParameters.toArray());

							if (called != null && called.isWasCalled()) {
								objReturnValue = called.getObjReturnValue();

								wasFilling = called.isWasCalled();
							}

							// Metoda se zavolala a něco vrátila:
							else
								wasFilling = false;

							// Uložím si hodnotu o tom, zda metoda něco vrací nebo ne:
							isVoid = m.getReturnType().equals(Void.TYPE);
							
							// Zde se mi podařilo zavoalat metodu a v proměnné objReturnValue mam návratovou
							// hodnotu, mohu ukončit cyklus:
							break;
						}
					}
				}
			}
			
			// sem se dostanu, pokud je zadana metoda s více paramaetry
			// zde pouze otestuji, zda se metoda zavolala, pokud ano, v proměnné bude tru, když ne vypíšu oznámení uživateli o nenalezení metody:
			if (!wasFilling)
				outputEditor.addResult(txtFailure + ": " + txtMethodNotFound + " - " + methodName
						+ OutputEditor.NEW_LINE_TWO_GAPS + txtBadNameOfMethodOrParameter);
		}
	
		// N akonec vrátím jednorozměrné pole o dvou hodnotách, ve kterém bude na prvním místě, zda metoda vrátila správný datový typ
		// - tj. byla zavoálna a něco vrátila, a na druhém místě (index 1) bude hodnota, kterou metoda vrátila
		return new Object[] { wasFilling, objReturnValue, isVoid };
	}


	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje všechny parametry a zjistí, zda nějaký parametr není
	 * jednorozměrné pole, pokud ano, tak ho musí přetypovat na konkrétní pole a ne
	 * jako "reference", následně se všechny hodnoty vloži do nové kolekce a ta se
	 * vrátí, tato kolekce již bude obsahovat všechny "správné" hodnoty coby
	 * potřené, uživatelem zadané parametry
	 * 
	 * @param list
	 *            - kolekce, ve které jsou parametry, získané od uživatele
	 * 
	 * @param m
	 *            - metoda, jejiž parametry se mají otestovat na jednorozměrné pole
	 *            (viz odstavec výše)
	 * 
	 * @return novou kolekce se stejnými hodnotami, akorát pokud jě nějaká hodnota
	 *         jednorozměré pole, tak tyto hodnoty budou v konkrétním - nově
	 *         vytvořeném poli
	 */
	private static List<Object> getListOfTestedParameters(final List<Object> list, final Method m) {
		final List<Object> testedList = new ArrayList<>();
		
		// Parametry metody, pro zjištění správného datového typu pole, na kter¨é se mají dané hodnoty přetypovat:
		final Parameter[] parametersArray = m.getParameters();
		
		
		for (int i = 0; i < list.size(); i++) {
			final Object o = list.get(i);
			final Parameter p = parametersArray[i];
			
			if (o != null) {
				if (o.getClass().isArray()) {
					new ParseToJavaTypes();
					
					final Object[] objArray = ParseToJavaTypes.convertToObjectArray(o);

					final Object objArrayValue = getArrayToClassVariable(objArray, p.getType().getComponentType());

					testedList.add(objArrayValue);
				}

				else testedList.add(o);
			}
			// Zde se jedná o null hodnotu na aktualni pozici v kolekci, jelikoz by pri testovaní nastala vyjimka, tak ji musím přidat
			// následující způsobem, protože v kolekci tato hodnota "musí" být
			else testedList.add(null);
		}
		
		
		return testedList;
	}
	
	
	
	



	
	
	
	
	
	
	/**
	 * Náelsují metody pro inkrementaci a dekrementaci konstanty
	 * 
	 * 
	 * Postup:
	 * jelikož je to číslo, tak mohu odebrat všechny mezery a předejfu tak něžádoucím vyjímkám, pak se vezmu text od začátku po 
	 * plusy nebo mínusy (inkrementace / dekremetace) - tuto část dále individuálně přizpůsobím, zda se jedná o postfixovou nebo
	 * prefixovou formu inkrementace či dekrementace - takže si konstanty vezmu bud od začátku po ++/-- nebo od ++ -- - druhého znaku do konce
	 * 
	 * Dále otestuji, zda číslo obsahuje desetinnou tečku, 
	 * 		pokud ano, tak toto číslo přetypuji na typ double, protože jako desetinné - reálné číslo má nějvetčí rozsah
	 * 		pak dané číslo inkremetnuji / dekremetuji a v podobě textu vrátím - v tomto případě se číslo dále vypíše do editoru výstupů
	 * 		ale to je již jiná část aplikace
	 * 
	 *  	pokud ne, tak číslo přetypuji na typ long, protože jakké celé číslo má nejvetčí rozsah
	 *  	dále toto číslo inkremetuji / dekremetuji a vrátím v podobě Stringu
	 */
	
	
	
	
	@Override
	public String makeIncrementValueBehind(final String command) {		
		final String textOfCommand = command.replaceAll("\\s", "");
		
		// Zde se jedná o Syntaxe X++, takže si vezmu text od začátku do ++ na konci
		
		final String numbersText = textOfCommand.substring(0, textOfCommand.indexOf("++"));
		
		
		return getIncrementValue(numbersText);
	}



	

	@Override
	public String makeDecrementValueBehind(final String command) {
		// Odeberu mezery:
		final String textOfCommand = command.replaceAll("\\s", "");
		
		// Zde se jedná o Syntaxe X--, takže si vezmu text od začátku do -- na konci		
		final String numbersText = textOfCommand.substring(0, textOfCommand.indexOf("--"));
		
		
		return getDecrementValue(numbersText);
	}






	@Override
	public String makeIncrementValueBefore(final String command) {
		// Odeberu mezery:
		final String textOfCommand = command.replaceAll("\\s", "");
		
		// Zde se jedná o Syntaxe ++X, takže si vezmu text od ++ do konce - 1 (-1 protpže se musím zbavit středníku na konci textu):		
		final String numbersText = textOfCommand.substring(textOfCommand.indexOf("++") + 2, textOfCommand.length() - 1);
		
		
		return getIncrementValue(numbersText);
	}


	


	@Override
	public String makeDecrementValueBefore(final String command) {
		// Odeberu mezery:
		final String textOfCommand = command.replaceAll("\\s", "");
		
		// Zde se jedná o Syntaxe --X, takže si vezmu text od -- do konce - 1, (-1 protpže se musím zbavit středníku na konci textu:		
		final String numbersText = textOfCommand.substring(textOfCommand.indexOf("--") + 2, textOfCommand.length() - 1);
		
		return getDecrementValue(numbersText);
	}
	
	
	

	
	
	
	/**
	 * Metoda, která vrátí inkrementované číslo (zvětšené o jedničku) v podobě Stringu
	 * Metoda přetypuje text v parametru na typ Long nebo Double - dle toho, zda se v textu
	 * nachází desetinná tečka nebo ne - dle toho poznám, zda se jedná o desetinné číslo nebo ne.
	 * 
	 * Výjimka nastane pokud uživatel zadaná číslo mimo rozsah longu nebo doublu
	 * 
	 * @param numbersText = číslo k inkrementaci v typu String
	 * @return inkrementované číslo v podobě Stringu
	 */
	private static String getIncrementValue(final String numbersText) {
		try {
			if (numbersText.contains(".")) {
				// Zde se jedná o desetinné číslo:
				Double textDouble = Double.parseDouble(numbersText);
				// zde mám číslo, tak ho mohu inkremetovat a vrátit:
				
				return txtSuccess + ": " + ++textDouble;
			}
			else {
				// Zde zadané číslo neobshuje tečku, tak se nejdná o desetinné číslo:
				Long textLong = Long.parseLong(numbersText);
				
				return txtSuccess + ": " + ++textLong;
			}
		} catch (NumberFormatException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při parsování hodnoty (čísla) v datovém typu String: "
                        + numbersText
                        + " na hodnotu Long nebo Double. Tato chyba může nastat například v případě, že zadaná " +
                        "hodnota nelze přetypovat na jeden z uvedených datových typů, "
                        + "například se nejdená o požadované číslo, je mimo interval uvedených datových typů nebo je " +
                        "v chybné syntaxi apod.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			
			return txtFailure + ": " + txtErrorInIncrementValue + ": " + numbersText + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtNunberOutOfInterval;
		}
	}
	
	
	
	
	
	
	
	/**
	 * metoda, která vrátí dekremetované číslo (změnšené o jedničku) v podobě
	 * Stringu
	 * 
	 * Metoda přetypuje text v parametru na typ Long nebo Double - dle toho, zda se
	 * v textu nachází desetinná tečka nebo ne - dle toho ponám, zda se jedná o
	 * desetinné číslo nebo ne
	 * 
	 * vyjímka nastane pokud uživatel zadaná číslo mimo rozsah longu nebo doublu
	 * 
	 * @param numbersText
	 *            - číslo k dekrementaci v typu String
	 * 
	 * @return dekrementované číslo v podobě Stringu
	 */
	private static String getDecrementValue(final String numbersText) {
		try {
			if (numbersText.contains(".")) {
				// Zde se jedná o desetinné číslo:
				Double textDouble = Double.parseDouble(numbersText);
				// zde mám číslo, tak ho mohu inkremetovat a vrátit:
				
				return txtSuccess + ": " + --textDouble;
			}
			else {
				// Zde zadané číslo neobshuje tečku, tak se nejdná o desetinné číslo:
				Long textLong = Long.parseLong(numbersText);
				
				return txtSuccess + ": " + --textLong;
			}
			
		} catch (NumberFormatException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka při parsování hodnoty (čísla) v datovém typu String: "
                        + numbersText
                        + " na datový typ Double nebo Long. Tato chyba může nastat například v případě, že zadaná " +
                        "hodnota nelze přetypovat na jeden z uvedených datových typů, "
                        + "například se nejdená o požadované číslo, je mimo interval uvedených datových typů nebo je " +
                        "v chybné syntaxi apod.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			
			return txtFailure + ": " + txtErrorInDecrementValue + ": " + numbersText + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtNunberOutOfInterval;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metody pro operace - inkrementace a dekremetnace proměnné v instanci třídy
	 * 
	 * Postup:
	 * Od druhého + po první tečku se jedná o název reference na instanci třídy, tak otestuji zda existuje
	 * pokud ano, tak od první tečky po středník se jedná o proměnnou, tak pokud existuje, tak ji zvětším
	 * 
	 * případně někde vypíšu chybové oznámení do editoru výstupů, nebo výsledek
	 */
	
	

	
	@Override
	public String makeIncrementClassVariableBefore(final String command) {
		// Zde mohu odebrat veškeré mezery, abych předešel nechtěným chybám či vyjímkám:
		final String commandWithoutSpace = command.replaceAll("\\s", "");
		
		// Nyní se jedná o syntaxi bez mezer:   ++referenceName.variableName;
		// Zjistím si název reference - od druhého + po první tečku:
		final String referenceName = commandWithoutSpace.substring(commandWithoutSpace.indexOf("++") + 2,
				commandWithoutSpace.indexOf('.'));
		
		
		// Název proměnné instance třídy - od první tečky po středník:
		final String variableName = commandWithoutSpace.substring(commandWithoutSpace.indexOf('.') + 1,
				commandWithoutSpace.indexOf(';'));
		
		
		// Najdu si instanci třídy dle názve reference:
		final Object objInstance = Instances.getInstanceFromMap(referenceName);
		
		// Pokusím se inkrementovat hodnotu proměnné:
		return getIncrementOrDecrementVariable(variableName, objInstance, true, false);		
	}




	



	@Override
	public String makeDecrementClassVariableBefore(final String command) {
		// Zde mohu odebrat veškeré mezery, abych předešel nechtěným chybám či vyjímkám:
		final String commandWithoutSpace = command.replaceAll("\\s", "");
		
		// Nyní se jedná o syntaxi bez mezer:   --referenceName.variableName;
		// Zjistím si název reference - od druhého - po první tečku:
		final String referenceName = commandWithoutSpace.substring(commandWithoutSpace.indexOf("--") + 2,
				commandWithoutSpace.indexOf('.'));
		
		
		// Název proměnné instance třídy - od první tečky po středník:
		final String variableName = commandWithoutSpace.substring(commandWithoutSpace.indexOf('.') + 1,
				commandWithoutSpace.indexOf(';'));
		
		
		// Najdu si instanci třídy dle názve reference:
		final Object objInstance = Instances.getInstanceFromMap(referenceName);
		
		// Pokusím se inkrementovat hodnotu proměnné:
		return getIncrementOrDecrementVariable(variableName, objInstance, false, false);
	}



	
	
	
	@Override
	public String makeIncrementClassVariableBehind(final String command) {
		// Zde mohu odebrat veškeré mezery, abych předešel nechtěným chybám či vyjímkám:
		final String commandWithoutSpace = command.replaceAll("\\s", "");
		
		// Nyní se jedná o syntaxi bez mezer:   (-) referenceName.variableName++;
		
		final boolean isCharAtFirstIndexMinus = isMinusAtFirstChar(command);
		
		final String referenceName;
		
		// Otestuji, zda je na začátku mínus:		
		if (isCharAtFirstIndexMinus)
			// Zjistím si název reference - od začátku (za mínusem) po první tečku:
			referenceName = commandWithoutSpace.substring(1, commandWithoutSpace.indexOf('.'));
		
		// Zjistím si název reference - od začátku po první tečku:
		else referenceName = commandWithoutSpace.substring(0, commandWithoutSpace.indexOf('.'));
		
		
		// Název proměnné instance třídy - od první tečky po ++:
		final String variableName = commandWithoutSpace.substring(commandWithoutSpace.indexOf('.') + 1,
				commandWithoutSpace.indexOf("++"));
		
		
		// Najdu si instanci třídy dle názve reference:
		final Object objInstance = Instances.getInstanceFromMap(referenceName);
		
		// Pokusím se inkrementovat hodnotu proměnné:
		return getIncrementOrDecrementVariable(variableName, objInstance, true, isCharAtFirstIndexMinus);	
	}

	
	
	
	
	
	
	@Override
	public String makeDecrementClassVariableBehind(final String command) {
		// Zde mohu odebrat veškeré mezery, abych předešel nechtěným chybám či vyjímkám:
		final String commandWithoutSpace = command.replaceAll("\\s", "");
		
		// Nyní se jedná o syntaxi bez mezer:   (-) referenceName.variableName--;
		
		final boolean isCharAtFirstIndexMinus = isMinusAtFirstChar(command);
		
		final String referenceName;
		
		// Otestuji, zda je na začátku mínus:
		if (isCharAtFirstIndexMinus)
			// Zjistím si název reference - od začátku (za prvním minusem) po první tečku:
			referenceName = commandWithoutSpace.substring(1, commandWithoutSpace.indexOf('.'));
		// Zjistím si název reference - od začátku po první tečku:
		else referenceName = commandWithoutSpace.substring(0, commandWithoutSpace.indexOf('.'));
		
		
		
		// Název proměnné instance třídy - od první tečky po --:
		final String variableName = commandWithoutSpace.substring(commandWithoutSpace.indexOf('.') + 1,
				commandWithoutSpace.indexOf("--"));
		
		
		// Najdu si instanci třídy dle názve reference:
		final Object objInstance = Instances.getInstanceFromMap(referenceName);
		
		// Pokudsím se inkrementovat hodnotu proměnné:
		return getIncrementOrDecrementVariable(variableName, objInstance, false, isCharAtFirstIndexMinus);
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která si vyhledná zadanou položku instance třídy, pokud jí najde, tak
	 * se jí pokusi převést na celé nebo desetinné číslo a její hodnotu
	 * inkrementovat nebo dekrementovat (dle parametrů) a tuto hodnotu vrátí zpět do
	 * dané proměnné
	 * 
	 * @param variableName
	 *            - název proměnné, jejiž hodnotu chce uživatel inkrementoat /
	 *            dekrementovat
	 * 
	 * @param objInstance
	 *            - objekt instance třídy
	 * 
	 * @param increment
	 *            - logická hodnota, zda se má hodnota proměnné inkrementovat, pokud
	 *            ano, tak se inkrementuje - zvýší o jedničku, pokud je false, tak
	 *            se dekrementuje - sníží o jedničku
	 * 
	 * @param insertMinus
	 *            - logická hodnota, dle které poznám, zda mám hodnotu "znegovat" -
	 *            otočit její zápornou hodnotu - dát před ní mínus: x = -x;
	 * 
	 * @return text s chybových oznámením - pokud došlo k chybě nebo Úspěch, pokud
	 *         se hodnotu podařilo úspěšně inkrementovat / dekrementovat
	 */
	private String getIncrementOrDecrementVariable(final String variableName, final Object objInstance,
			final boolean increment, final boolean insertMinus) {
		if (objInstance != null) {
			// Zde jsem našel instanci nějaké třídy, tak si zjistím proměnnou:
			final Field field = getFieldFromInstance(objInstance, variableName);

			if (field != null) {

				if (Modifier.isFinal(field.getModifiers()))
					return txtFailure + ": " + txtVariableIsFinalDontChangeValue + OutputEditor.NEW_LINE_TWO_GAPS
							+ txtVariable + ": " + field.getName();

				final Object objFieldValue = ReflectionHelper.getValueFromField(field, objInstance);

				if (objFieldValue != null) {
					// Zde objekt není null - může se jednat např o String, ten při deklaraji je
					// null, apod.

					try {
						// Přetypované číslo nejspíše - může být i text, apod. to níže:
						final Object parsedObj = getParsedParameter(objFieldValue.getClass(), objFieldValue.toString());

						// Proměnná, do které vložím inkremetovanou hodnotu pro přetypování na původní
						// hodntou:
						String incrementedValue;

						if (parsedObj != null) {
							// Otestuji, zda se jedná o celé číslo nebo destinné - dle toho mohu použít Big
							// Double nebo Integer
							if (regEx.isInteger(parsedObj.toString())) {
								final BigInteger bi = new BigInteger(parsedObj.toString());
								Long l = bi.longValue();

								// Otestuji, zda mám před hodnotu vložit mínus - neboli "znegovat"
								if (insertMinus)
									l = -l;

								// Zde si zjistím, zda se má hodnota inkrementovat nebo dekremetovat:
								if (increment)
									incrementedValue = String.valueOf(++l);
								else
									incrementedValue = String.valueOf(--l);
							}

							else if (regEx.isDecimal(parsedObj.toString())) {
								final BigDecimal bd = new BigDecimal(parsedObj.toString());
								Double d = bd.doubleValue();

								// Otestuji, zda mám před hodnotu vložit mínus - neboli "znegovat"
								if (insertMinus)
									d = -d;

								// Zjistím si, zda mám hodnotu inkrementovat nebo dekrementovat:
								if (increment)
									incrementedValue = String.valueOf(++d);
								else
									incrementedValue = String.valueOf(--d);
							}

							// Nejedná se o číslo:
                            else
                                return txtFailure + ": " + txtVariableDontContainsNumber;

                            // Přetypuji inkremetovanou / dekrementovanou hodnotu na původní datový typ:
                            final Object parsedObjBack = getParsedParameter(objFieldValue.getClass(),
                                    incrementedValue);

                            // Pokud se podařilo úspěšné přetypování na původní datový typ:
                            if (parsedObjBack != null) {
                                setValueToClassVariable(field, parsedObjBack, objInstance);

                                // Oznámím, že se hodnota úspěšně inkrementovala / dekremetovala:
                                return txtSuccess;
                            }
                        }
						return txtFailure;

					} catch (NumberFormatException e) {
                        /*
                         * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                         * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                         * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                         * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                         */
                        final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                        // Otestuji, zda se podařilo načíst logger:
                        if (logger != null) {
                            // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                            // Nejprve mou informaci o chybě:
                            logger.log(Level.INFO, "Zachycena výjimka pri prsování hodnoty.");

                            /*
                             * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                             * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                             * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                             */
                            logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                        }
                        /*
                         * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                         */
                        ExceptionLogger.closeFileHandler();

						return txtFailure + ": " + txtFailedToCastNumber;
					}
				}

				else
					return txtFailure + ": " + variableName + " = null";

			} 
			else
				return txtFailure + ": " + variableName + " = null";

		} 
		else
			return txtFailure + ": " + txtReferenceDontPointsToInstanceOfClass + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtReference + " -> null";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Následují metody pro inkrementaci / dekrementaci proměnné vytvořené v editoru příkazů
	 * 
	 * Postup:
	 * Zjistím si zadanou referenci na proměnnou v editoru příkazů
	 * pokud neexistuje joznámím to uživateli výpisem do editoru výstupů
	 * 
	 * pokud se hledaná proměnna najde, otestuji, zda není null, a je final nebo ne
	 * pokud ano, oznámím uživateli že nelze provést požadovanou operaci
	 * 
	 * pokud není final, tak otestuji, zda se jedná o číslo, a pokud ano, provedu příslušnou operaci
	 */
	


	@Override
    public String makeIncrementCommandEditorVariableBehind(final String command) {
        final String[] variablesData = getEditorVariableDataFromCommand(command, true);

        final String variableName = variablesData[0];

        return incrementOrDecrementEditorVariable(variableName, true);
    }



	
	

	@Override
	public String makeIncrementCommandEditorVariableBefore(final String command) {
		// Odeberu si všechny mezery:
		final String commandWithoutSpace = command.replaceAll("\\s", "");
		
		// Název proměnné: od druhého + po středník:
		final String variableName = commandWithoutSpace.substring(commandWithoutSpace.indexOf("++") + 2,
				commandWithoutSpace.indexOf(';'));
				
		return incrementOrDecrementEditorVariable(variableName,  true);
	}


	
	

	@Override
	public String makeDecrementCommandEditorVariableBehind(final String command) {
        final String[] variablesData = getEditorVariableDataFromCommand(command, false);

        // Ani bych to nemusel testovat, nemělo by nastat:
        final String variableName = variablesData[0];

        return incrementOrDecrementEditorVariable(variableName, false);
    }



	
	
	
	@Override
	public String makeDecrementCommandEditorVariableBefore(final String command) {
		// Odeberu si všechny mezery:
		final String commandWithoutSpace = command.replaceAll("\\s", "");
		
		// Název proměnné: od druhého - po středník:
		final String variableName = commandWithoutSpace.substring(commandWithoutSpace.indexOf("--") + 2,
				commandWithoutSpace.indexOf(';'));

        return incrementOrDecrementEditorVariable(variableName, false);
	}
	
	
	
	
	
	
	
	
	

	
	
	
	/**
	 * Metoda, která inkrementuje nebo dekrementuje proměnnou, vytvořenou v editoru
	 * příkazů (pokud se jedná o proměnnou obsahující číslo - celé nebo reálné)
	 * 
	 * Metoda si načte promennou s daným názvem, zjistí, zda se jedná o číslo (celé
	 * nebo reálné) a provede na něm inkrementaci či dekrementaci a vrátí tuto
	 * upravenou hodnotu zpět do proměnné a vrátí )spěch, pokud někde dojde k nějaké
	 * chybě, se kterou "počítám", vypíše se do editrou výstupů chybové oznámení s
	 * informacemi o vzniklé chybě
	 * 
	 * @param variableName
	 *            - název proměnné
	 * 
	 * @param increment
	 *            - logická hodnota, dle které poznám, zda mám hodnotu proměnné
	 *            inkremetovat (zvýšit o jedničku), proměnná = true, nebo
	 *            dekrementovat (snížit o jedničku) - pokud je proměnná false
	 * 
	 * @return Úspěch nebo Neúspěch s chybovým oznámením
	 */
    private String incrementOrDecrementEditorVariable(final String variableName, final boolean increment) {
		
		final Variable variable = (Variable) Instances.getVariableFromMap(variableName);
		
		if (variable != null) {
			// Nejprve otestuji, zda není prměná final:
			if (!variable.isFinal()) {
				// Zde jsem našel hledanou proměnnou, tak otestuji, zda se jedná o číslo - pomocí regulárních výrazů
				// jinak bych musel testovat datové typy pro číslo nebo se pokusit převést na nějaký typ čísla a dle toho buď inkremetnvoat nebo
				// dekremetovat, ale v případě deseiných čísel bych ztratil přehled
				
				// Tak si otestuji, zda se jedná o celé nebo desetinné číslo, pokud ano,
				// tak to na jedno z nich přetpuji, provedu operaci a vrátím číslo zpět napůvodní datový typ a zpět do proměnné:
				
				
				// Proměnná, do které vložím inkremetovanou hodnotu pro přetypování na původní hodntou:
				String finalValue;
				
				
				if (variable.getObjValue() != null) {
					// Otestuji, zda se jedná o celé číslo nebo destinné - dle toho mohu použít Big Double nebo Integer
					if (regEx.isInteger(variable.getObjValue().toString())) {
						final BigInteger bi = new BigInteger(variable.getObjValue().toString());
						Long l = bi.longValue();

						// Zde si zjistím, zda se má hodnota inkrementovat nebo
						// dekremetovat:
						if (increment)
							finalValue = String.valueOf(++l);
						else
							finalValue = String.valueOf(--l);
					}

					
					
					else if (regEx.isDecimal(variable.getObjValue().toString())) {
						final BigDecimal bd = new BigDecimal(variable.getObjValue().toString());
						Double d = bd.doubleValue();

						// Zjistím si, zda mám hodnotu inkrementovat nebo dekrementovat:
						if (increment)
							finalValue = String.valueOf(++d);
						else
							finalValue = String.valueOf(--d);
					}


					// Nejedná se o číslo:
					else return txtFailure + ": " + txtVariableDontContainsNumber;


                    // Přetypuji inkremetovanou / dekrementovanou hodnotu na původní
                    // datový typ:
                    final Object parsedObjBack = getParsedParameter(variable.getDataTypeOfVariable(), finalValue);

                    // Pokud se podařilo úspěšné přetypování na původní datový typ:
                    if (parsedObjBack != null) {
                        // tak nastavím novou hodnotu do proměnné:
                        variable.setObjValue(parsedObjBack);

                        /*
                         * Aktualizuji zde okno pro automatické doplnění / dokončení kódu / příkazu v editoru
                         * příkazů, protože kdybych se to zde neprovedlo, v okně by nebyly zobrazeny aktuální
                         * hodnoty, resp. výše nově nastavená hodnota by se v tom okně neprojevila.
                         */
                        updateValueCompletionWindow();

                        // Oznámím, že se hodnota úspěšně inkrementovala /
                        // dekremetovala:
                        return txtSuccess;
                    }
                }
				else return txtFailure + ": " + txtVariableIsNullCannotBeIncrementOrDecrement;
			}
			else return txtFailure + ": " + txtVariable + ": '" + variableName + "' " + txtValueIsFinalAndCannotBeChanged;			

			return txtFailure;
		}
		else
			return txtFailure + ": " + txtVariableNotFoundForInsertValue + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtVariableNotFound + ": " + variableName;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která si ze zadaného příkazu zjistí, název proměnné v editoru příkazů
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příazů
	 * 
	 * @return jednorozměrné pole s dvěma hodnotami, na prvním bude název proměnné v
	 *         editoru a na druhé logická hodnota true / false, zda se má u dané
	 *         hodnoty prměnné otočit znaménko - zda se má dát před hodnotu minus
	 *         (-)
	 */
	private static String[] getEditorVariableDataFromCommand(final String command, final boolean increment) {
		// Odeberu si všechny mezery:
		final String commandWithoutSpace = command.replaceAll("\\s", "");
		
		
		// Na prvním indexu může být mínus:
		final boolean isCharAtFirstIndexMinus = isMinusAtFirstChar(commandWithoutSpace);
		
		// Zjistím si název proměnné - od začátku do konce:
		String variableName;
		
		if (isCharAtFirstIndexMinus) {
			// vezmu si název proměnno bez minusu na začátku:
			if (increment)
				variableName = commandWithoutSpace.substring(1, commandWithoutSpace.indexOf("++"));
			else
				variableName = commandWithoutSpace.substring(1, commandWithoutSpace.indexOf("--"));
		}
		
		else {
			if (increment)
				variableName = commandWithoutSpace.substring(0, commandWithoutSpace.indexOf("++"));
			else
				variableName = commandWithoutSpace.substring(0, commandWithoutSpace.indexOf("--"));
		}
		
		
		return new String[] {variableName, String.valueOf(isCharAtFirstIndexMinus)};		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	/**
	 * Metoda,která vytvoří a vrátí instanci třídy ResultValue, která slouží pro
	 * předání hodnot z operace.
	 * 
	 * Instance třídy ResultValue se rovnou naplní dvěma hodnotami (executed a
	 * objResult).
	 * 
	 * @param executed
	 *            - logická proměnná, která značí, zda došlo k převzetí hodnoty,
	 *            například z proměnné nebo z metody či početní operace apod. Pokud
	 *            je tato hodnota false, pak nedošlo k převzetí hodnoty a je na
	 *            druhém míste hodnota null
	 * 
	 * @param objResult
	 *            - buď null hodnota, pokud nedošlo k převzetí hodnoty z nějaké
	 *            proměnné, metody, apod., jinak hodnota, která se převzala (může
	 *            být také null).
	 * 
	 * @return výše uvedenou instanci třídy ResultValue s výše popsanými hodnotami.
	 */
	private static ResultValue returnResult(final boolean executed, final Object objResult) {
		return new ResultValue(executed, objResult);
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zkusí rozpoznat zadaný parametr (s) - zda je to konstanta,
	 * metoda, proměnné, apod - viz podmínky v metodě, zjistí výslednou hodnotu,
	 * kterou například metoda vrátí nebo se nachází v proměnné, apod, a tuto
	 * hodnotu vrátí v instanci třídy ResultValue s infomací zda se hodnota převzala
	 * nebo ne
	 * 
	 * @param s
	 *            - část text z příkazu zadaného uživatelem v editru příkazů, např
	 *            konstanta - číslo, text, znak, nebo proměnná apod.
	 * 
	 * @param variableDataType
	 *            - datový typ hodnoty, kterou musí příslušná "operace" vrátit,
	 *            neboli například metoda musí tento typ hodnoty vracet, dále se
	 *            musí tento typ hodnoty nacházet v proměnné, apod.
	 * 
	 *            Pokud je tento parametr null, tak se tato metoda volá z metody
	 *            getResultOfExponent, takže chci jen získat hodnotu například z
	 *            proměnné, metody, apod. a provést operaci exponent
	 * 
	 *            "Pokud je tato proměnné null, tak chci pouze získat hodnotu, či
	 *            výsledek exponentu, apod, nechci testovat o jaký datový typ se
	 *            jedná!"
	 * 
	 * 
	 * @return Instanci třídy ResultValue s dvěma hodnotami, první bude logická
	 *         hodnota o tom, zda se ta druhá hodnota převzala nebo ne, true, pokud
	 *         se druhá hodnota převzala, jinak false a druhá hodnota je null, nebo
	 *         převzatá hodnota, tj. hodnota vrácená z metody či získaná z proměnné
	 *         apod.
	 * 
	 */
	private ResultValue getValueFromVar_Method_Constant(final String s, final Class<?> variableDataType) {
		
		// Proměnná, do které uložím hodnotu bud null nebo nějakou instanci třídy níže, pokud bude rozpoznán název:
		
		// Proměnná pro otestování, zda se jedná o List nebo pole vytvořené v editoru příkazů:
		VariableList<?> variableList;
		
		
		VariableArray<?> variableArray;
		
		
		// Proměnná, ve které bude uložena instance třídy z diagramu insntancí - pokud bud enalezena
		// a bude sloužit pro předání reference na nějakou instanci třídy
		final Object objInstanceOfClass;
		
		
		
		// Konstanta (text, znak číslo):
		if (regEx.isConstantOrLiteral(s)) {
			/*
			 * Zde je trochu těžší rozlišit, zda se jedná přímo o "matematický výpočet",
			 * nebo jen o nějaké přetypování hodnoty, tak zde bude trochu delší podmínka,
			 * aby bylo možné to trochu "upřesnit".
			 */
			
			// Nová podmínka pro novou knihovnu:
			if (regEx.isExponent(s)
					|| (regEx.isMathematicalExpression(s.replaceAll("\\s", "")) && Editor.containsNumber(s))) {

				/*
				 * Zde se jedná o nějaký matematický výraz, tak si pro začátek zkusím zjistit,
				 * zda obsahuje nějaké textové proměnné - nějaká písmena, pokud ano, tak zkusím
				 * zjistit, zda se jedná o "dočasné" proměnné vytvořené uživatelem v editoru
				 * příkazů, pokud ano, zjistím, zda obsahují čísla a pokud jsou tyto podmínky
				 * splněny, pak mohu za tyto proměnné doplnit příslušná čísla z daných
				 * proměnných a vypočítat příslušný výraz.
				 * 
				 * Pokud nebudou nalezeny žádné textové proměnné, pak mohu výraz vypočítat, nebo
				 * pokud budou nalezeny proměnné, ale nejedná se o vytvořeé proměnné z editoru
				 * příkazů, například sqrt pro odmocninu a další funkce (například
				 * goniometrické).
				 */
				

				/*
				 * Pro začátek si vytvořím výraz, který by seměl vypočítat, abych do měl
				 * případně mohl rovnou vkládat hodnoty za proměnné (následující cyklus).
				 */
				final Expression expression = new Expression(s);
				
				
				
				/*
				 * Získám si list, který obsahuje veškeré textové proměnné, a zjistím, zda se
				 * jedná o proměnné z editoru příkazů, případně si načtu jejich hodnoty.
				 */
				final List<String> variables = getGroupsOfText(s);
				
				
				// Vložím do výše vytvořeného výrazu proměnné vytvořené v editoru příkazů, které
				// se rozpoznají:
				setVariablesToExpression(expression, variables);
				
				
				
				/*
				 * Nyní jsem v pozici, kdy se mohu pokusit vypočítat zadaný výraz, tak nad
				 * výrazem zavolám metodu eval a pokud se výpočet povede, tak otestuji datový
				 * typ, na který se to má převést a převedu na něj výsledek. Pokud dojde k
				 * nějakém chybě, npříklad chybí nebo přebývá závorka, nebo nebyla specifikována
				 * proměnná atd. Pak vrátím null hodontu s tím, že se nepovedl výpočet.
				 */
				try {
					final BigDecimal result = expression.eval();
					
					final Object objResultOfExponent;
					
					if (variableDataType != null) {
						if (variableDataType.equals(Double.class) || variableDataType.equals(double.class)
								|| variableDataType.equals(Float.class) || variableDataType.equals(float.class))

							// Zde se jedná o desetinné číslo, tak to stačí přetypovat:
							objResultOfExponent = getParsedParameter(variableDataType,
									String.valueOf(result.floatValue()));

						else {
							// Zde se jedná o celé číslo, tak musím výsledek převést na celé číslo, jelikož
							// nechci psát podmínku
							// pro každý typ, tak stačí vzít z prměnné datový typ long, a ten lze již na
							// ostatní datové typy přetypovat (číselné typy)
							final Long l = result.longValue();
							objResultOfExponent = getParsedParameter(variableDataType, l.toString());
						}
					}
									
						// Zde není uveden datový typ, takže chci jen získat hodnotu:
					else objResultOfExponent = result;

					if (objResultOfExponent != null)
						return returnResult(true, objResultOfExponent);

				} catch (ExpressionException e) {
					outputEditor.addResult(txtFailedToCalculateMathTerm_1 + ": " + e.getMessage()
							+ OutputEditor.NEW_LINE_TWO_GAPS + txtFailedToCalculateMathTerm_2 + ": " + s);

					return returnResult(false, null);
				}
			}
			
			
			
			
			
			
			// Zde se jedná o jinou konstantu např: 'X', "asdf" (znak, text, čísla, ...) -
			// nejedná se o exponent (případně početní operaci)
			else {
				// Opět otestuji, zda není variableDataType null, pro pouhé vrácení hodnoty:
				if (variableDataType != null) {
					final Object objRetypeValue = getParsedParameter(variableDataType, s);

					// Zde se jedná o konstanatu, a ta by neměla být po parsování null, naorzdíl
					// například od návratové hodnoty nějaké metody
					// takže někde nastala chyba, hodnotu tedy do kolekce nevložím:
					if (objRetypeValue != null)
						return returnResult(true, objRetypeValue);

					// Pokud se nebude jednat o nějaky "nedefinovaný" typ, nemělo by nastat:
					else
						outputEditor.addResult(txtErrorWhileConvertingValues + ": '" + s + "' " + txtToType + ": "
								+ variableDataType + OutputEditor.NEW_LINE_TWO_GAPS + txtValueWillNotBeCounted);
				}

				// Zde se jedná například o text, znak,apod. tak ho pouze vrátím:
				else
					return returnResult(true, s);
			}
		}
		
		
		
		
		
		
		
		
		
		// Otestuji, zda se jedná o předání reference na instanci třídy z diagramu instancí:
		else if (regEx.isReferenceToInstanceOfClass(s) && (objInstanceOfClass = getInstanceOfClass(s)) != null) {
			
		// Zde se jedná o instnaci nějaké třídy z diagramu tříd, tak otestuji, zda jsou "stejné datové typy"
		// pkud ano, tak danou instanci přídám do kolekce, jinak vrátím null a vypíšu chybové oznámení
		// do editrou výstupů pro informaování uživatele o vzniklé chybě:
			
			
		// Zdee již vím, že byla instance nalezena, tak mohu otestovat, zda se její typ shoduje s požadovaným parametrem:	
		if (variableDataType != null) {
			
			final String dataTypeArg = variableDataType.toString();
			final String dataTypeObj = objInstanceOfClass.getClass().toString();
			
				if (dataTypeArg.equals(dataTypeObj))
					return returnResult(true, objInstanceOfClass);

				else
					outputEditor.addResult(txtInstanceIsNotCorrectForParameter + OutputEditor.NEW_LINE_TWO_GAPS
							+ txtInstance + ": " + objInstanceOfClass.getClass() + " != " + variableDataType);
			}
			// else - tato možnost by nastat neměla, reference se předáváají jen v metodě nebo konstruktoru, a v obou případech budou
		}
			
		
		
		
		
		
		
		
		
		
		
		// Proměnná v editoru příkazů:
		else if (regEx.isVariableInEditor(s)) {
			// Postup:
			// Zjistím si název proměnné, pokud je před ní mínus - pro negaci hodnoty, případně ji zneguji, dále pokud existuje daná proměnná, tak 
			// porovnám datové typy a pokud jsou tyto podmínky splněny, přidám hodnotu do kolekce: (ne v tomto pořadí)
			
			
			// V následující proměnné budu mít text proměnné, ze které chci získat hodnotu: v dxntaxi: (-)varianleName
			final String variableText = s.replaceAll("\\s", "");
			
			// otestuji mínus:
			final boolean isCharAtFirstIndexMinus = isMinusAtFirstChar(s);
			
			// Otestuji, zda je na začátku mínus://		
			
			final String variableName;
			
			if (isCharAtFirstIndexMinus)
				// Zjistím si název proměnné - od mínusu (+1) do konce neboli od druhého indexu - protože na prvním je mínus:
				variableName = variableText.substring(1);
			else variableName = variableText;
			
			
			
			
			// Nyní si musím načíst proměnné jak instance třídy Variable, tak i VariableList, protože nevím, zda se 
			// jedná o proměnnou coby List nebo normální proměnnou primitivního nebo objektového typu,
			// tak si musím načíst oboje a otestovat, zda se alespoň jedna najde (tj. není null):
			final Variable variable = (Variable) Instances.getVariableFromMap(variableName);
			
			variableList = (VariableList<?>) Instances.getListFromMap(variableName);
			
			
			variableArray = (VariableArray<?>) Instances.getArrayFromMap(variableName);
			
			
			if (variable != null) {
				// Zde byla nalezena proměnná coby instance třídy Variable - "normální" proměnné:
				
				// Otestuji, zda se proměnná variableDataType nrovná null, pokud ano, ak získám hodnotu a vrátím ji, jinak 
				// otestuji i datové typy, apod:
				if (variableDataType != null) {
					// Otestuji, zda se rovnají datové typy hodnot - položky a konstruktoru nebo metody:
					if (variableDataType.equals(variable.getDataTypeOfVariable())) {
						Object objValue = variable.getObjValue();
						
						if (objValue != null) {
							if (isCharAtFirstIndexMinus)
								// Otestuji, zda se jedná o celé číslo nebo destinné - dle toho mohu použít Big Double nebo Integer
								objValue = getNegationOfValue(variable.getDataTypeOfVariable(), objValue);
							
							return returnResult(true, objValue);
						}
						else
							outputEditor.addResult(txtVariableIsNull + OutputEditor.NEW_LINE_TWO_GAPS
									+ txtValueOfVariableWillNotBeCounted + " " + txtVariable + ": " + variableName);

					}					
					else
						outputEditor.addResult(txtDataTypeOfField + ": '" + variableName + "' " + txtDataTypeIsDifferent
								+ OutputEditor.NEW_LINE_TWO_GAPS + txtValueWillNotBeCounted + " "
								+ variable.getDataTypeOfVariable() + " != " + variableDataType);	
				}
				
				
				else {
					// Zde je variabledataType = null, tak získám hodnotu a vrátím ji - bez testování datových typů:
					Object objValue = variable.getObjValue();
					
					if (objValue != null) {
						if (isCharAtFirstIndexMinus)
							// Otestuji, zda se jedná o celé číslo nebo destinné - dle toho mohu použít Big Double nebo Integer
							objValue = getNegationOfValue(variable.getDataTypeOfVariable(), objValue);
						
						return returnResult(true, objValue);
					}
					else
						outputEditor.addResult(txtVariableIsNull + OutputEditor.NEW_LINE_TWO_GAPS
								+ txtValueOfVariableWillNotBeCounted + " " + txtVariable + ": " + variableName);
				}
									
			}
			
			// Zde nebyla nalezena proměnná s normální hodnotou - instance třídy Variable, tak musím otestovat, zda se nejedná o 
			// proměnnou coby kolekci, nebo proměnná, která je instance třídy VariableList:
			else if (variableList != null) {
				// Zde byla nalezena proměnná, coby instance třídy VariableList, takže se jedná o list z balíčku java.util:
				
				if (variableDataType != null) {
					// Zde porovnám datové typy:
					
					if (variableDataType.equals(VariableList.CLASS_LIST)) {
						// Zde se shodují datové typy, tak mohu vrátit kolekci:
						if (variableList.getList() != null)
							return returnResult(true, variableList.getList());

						else
							outputEditor.addResult(txtVariableIsNull + OutputEditor.NEW_LINE_TWO_GAPS
									+ txtValueOfVariableWillNotBeCounted + " " + txtVariable + ": " + variableName);
					}
					
					else
						outputEditor.addResult(txtDataTypeOfField + ": '" + variableName + "' " + txtDataTypeIsDifferent
								+ OutputEditor.NEW_LINE_TWO_GAPS + txtValueWillNotBeCounted + " "
								+ variableList.getClassType().getClass() + " != " + variableDataType);
				}
				
				else {
					// Zde prostě vrátím kolekci (pokud není null, v takovém případě vypíšu hlášku do editoru výstupů):
					if (variableList.getList() != null)
						return returnResult(true, variableList.getList());												
					
					else
						outputEditor.addResult(txtVariableIsNull + OutputEditor.NEW_LINE_TWO_GAPS
								+ txtValueOfVariableWillNotBeCounted + " " + txtVariable + ": " + variableName);
				}
			}
			
			
			
			
			else if (variableArray != null) {
				// Zde byla nalezena proměnná coby instance třídy VaribaleArray:
				
				if (variableDataType != null) {
					// V této části podmínky se mají porovnat datové tppy, tak je porovnám:

					if (variableDataType.equals(variableArray.getClassTypeOfArrayForMethod())) {
						// Zde se shodují datové typy, tak mohu vrátit pole:
						if (variableArray.getArray() != null)
							return returnResult(true, variableArray.getArray());

						else
							outputEditor.addResult(txtVariableIsNull + OutputEditor.NEW_LINE_TWO_GAPS
									+ txtValueOfVariableWillNotBeCounted + " " + txtVariable + ": " + variableName);
					} 
					else
						outputEditor.addResult(txtDataTypeOfField + ": '" + variableName + "' " + txtDataTypeIsDifferent
								+ OutputEditor.NEW_LINE_TWO_GAPS + txtValueWillNotBeCounted + " "
								+ variableArray.getClassTypeOfArrayForMethod() + " != " + variableDataType);
				}
				
				
				else {
					// Zde neporovnávám datové typy, prostě vrátím pole:
					if (variableArray.getArray() != null)
						return returnResult(true, variableArray.getArray());

					else
						outputEditor.addResult(txtVariableIsNull + OutputEditor.NEW_LINE_TWO_GAPS
								+ txtValueOfVariableWillNotBeCounted + " " + txtVariable + ": " + variableName);
				}
			}
			
			
			// Zde se nejdná o "normální" promennou z editoru příkazů ani o proměnnou s kolekci, tak oznámí, že proměnná pod 
			// danou referencí nebyla nalezena:
			else  outputEditor.addResult(txtVariable + " '" + variableName + "' " + txtDoNotFound);
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		// Zde otestuji, zda se jedná o instanci třídy VariableList coby List, ze kterého chci získat hodnotu
		// na nějakém jeho indexu
		else if (regEx.isGetValueAtIndexOfList(s + ";") && (variableList = getVariableListInstance(s)) != null) {
			// Pokud se dostanu do této podmínky, tak v proměnné variableList mám uloženou hodnotu nějaké isntance
			// třídy VariableList a uživatel chce získat hodnotu na nějakém indexu, zde nepotřebuji testovat, 
			// zda je final nebo ne, apod. prostě danou se pokusím získat danou hodnotu s tím, že
			// potřebuji zjistit, zda uživatel zadal správný index, viz podmínky následujícího kódu:
			
			// Potřebuji si z příkazu dodatečně zjistit akorát název proměnné, abych o tom mohl informovat uživatele:
			final String variableName = s.substring(0, s.indexOf('.')).replaceAll("\\s", "");

			
			// Zjistím si zadaný index z příkazu:
			// Index je mezi první otevíraci a poslední zavírací závorkou:
			// Jelikož to má bý pouze celé číslo >= 0, mohu odebrat v daném čísle bíle znaky:
			final String indexInText = s.substring(s.indexOf('(') + 1, s.lastIndexOf(')')).replaceAll("\\s", "");
			
			
			// Příkaz prošel regulárním výrazem, tak se mohu pokusit přetypovat zadané číslo na Integer,
			// abych mohl získat hodnotu na daném indexu:
			final int index = getIndexOfList(indexInText);
			
			// Nejprve otestuji, zda byl získán index v rozsahu integeru, a pak zda je v rozsahu listu:
			if (index > -1) {
				
				// Nyní musím otestovat, zda je index v rozsahu hodnot kolekce:
				final int sizeOfList = variableList.getList().size();
				
				if (index < sizeOfList) {
					// Pokusím se získat hodnotu:
					final Object objValue = variableList.getValueAtIndex(index);
					
					if (variableDataType != null) {
						if (variableDataType.equals(variableList.getClassType())) {
							if (objValue != null)
								return returnResult(true, objValue);
															
							else
								outputEditor.addResult(txtVariableIsNull + OutputEditor.NEW_LINE_TWO_GAPS
										+ txtValueOfVariableWillNotBeCounted + " " + txtVariable + ": " + variableName);
						}
						else
							outputEditor.addResult(txtDataTypeOfField + ": '" + variableName + "' "
									+ txtDataTypeIsDifferent + OutputEditor.NEW_LINE_TWO_GAPS + txtValueWillNotBeCounted
									+ " " + variableList.getClassType() + " != " + variableDataType);
					}
					
					
					else {
						if (objValue != null)
							return returnResult(true, objValue);							
						
						else
							outputEditor.addResult(txtVariableIsNull + OutputEditor.NEW_LINE_TWO_GAPS
									+ txtValueOfVariableWillNotBeCounted + " " + txtVariable + ": " + variableName);
					}										
				}
				else
					outputEditor.addResult(txtConditionsForIndexOfList + OutputEditor.NEW_LINE_TWO_GAPS
							+ txtSpecifiedIndexOfList + ": " + index);
			}
			else outputEditor.addResult(txtIndexOfListIsOutOfRange); 
		}
		
		
		
		
		
		
		
		
		
		
		
		
		// Zde otestuji, zda mám převést List - kolekci do jednorozměrného pole:
		else if (regEx.isListToOneDimensionalArray(s + ";") && (variableList = getVariableListInstance(s)) != null) {
			// List, který mám převést do jednorozměrného pole již mám v proměnné variableList (pokud jsem v této podmínce)
			// tak mohu akorát zavolat metodu, která ho převede do pole a otestovat, datové typy:
			
			// Akorát si potřebuji dodatečně zjistit název proměnné ze zadaného příkazu, abych v případě chyby
			// mohl informovat uživatele o vzniklé chybe se správnou proměnou:
			final String variableName = s.substring(0, s.indexOf('.')).replaceAll("\\s", "");
			
			
			
			// Musím nejprve převést list do pole a vytvořit proměnnou coby jednorozměrné pole = instance třídy: VariableArray,
			// do které vložím dané pole z list, a v této instnaci třídy VariableArray se už v konstruktoru zjistí potřebné
			// hodnoty pro otestování typů pole, převodu do pole, apod., takže pak pomocí metody getClasTypeOfArrayForMethod si zjistím
			// typ pole a pokud bude podmínka pro datové typy splněna, tak si vezmu pole z této instanci třídy VariableArray a vrátím ho:
			final VariableArray<?> variableArrayTemp = new VariableArray<>(false, variableList.getArrayFromList(),
					variableList.getClassType());


            if (variableDataType != null) {
                if (variableDataType.equals(variableArrayTemp.getClassTypeOfArrayForMethod()))
                    return returnResult(true, variableArrayTemp.getArray());

                else
                    outputEditor.addResult(txtDataTypeOfField + ": '" + variableName + "' " + txtDataTypeIsDifferent
                            + OutputEditor.NEW_LINE_TWO_GAPS + txtValueWillNotBeCounted + " "
                            + variableArrayTemp.getClassTypeOfArrayForMethod() + " != " + variableDataType);
            }

            else return returnResult(true, variableArrayTemp.getArray());
        }
		
		
		
		
		
		
		
		
		
		
		
		// Zde otestuji, zda se jedná o získání velikosti Listu - kolekce:
		else if (regEx.isListSize(s + ";") && (variableList = getVariableListInstance(s)) != null) {
			// Kvůli výpisům o chybových hlášek si potřebuji znovu sískat název proměnné coby reference na List - kolekci:
			final String variableName = s.substring(0, s.indexOf('.')).replaceAll("\\s", "");
			
			// Proměnnou, ve které je požadovaný list mám již v proměnné variableList, tak mohu vrátit jeho délku:
			// Ale nejprve je třeba otestovat datové typy:
			final int sizeOfList = variableList.getList().size();
			
			if (variableDataType != null) {
				if (variableDataType.equals(int.class))
					return returnResult(true, sizeOfList);
										
				else
					outputEditor.addResult(txtDataTypeOfField + ": '" + variableName + "' " + txtDataTypeIsDifferent
							+ OutputEditor.NEW_LINE_TWO_GAPS + txtValueWillNotBeCounted + " " + int.class + " != "
							+ variableDataType);
			}		
			else return returnResult(true, sizeOfList);
		}
		
		
		
		
		
		
		
		
		
		// Otestuji, zda se jedná o získání velikost jednorozměrného pole:
		// v možné syntaxe i s negaci: (i) arrayname.length  -> zde nemusí být obsaže nstřednik:
		else if (regEx.isGetArrayLengthInParameter(s) && (variableArray = getVariableArrayInstance2(s)) != null) {
			// (-) arrayName.length
			
			// Postup:
			// Zjistím si, zda je na zařátku mínus - pro negaci hodnoty, dále otestuji datové typy,
			// zda se má vrátit stejný datový typ co je délka pole, a pak si zjistím délku pole
			// a bud ji zneguji a vrátím nebo jen vrátím:
			
			// Otestuji, zda je na začátku mínus, pro znegování hodnoty:
			final boolean isMinusAtFirstChar = isMinusAtFirstChar(s);
			
			final int length = variableArray.getArrayLength();
			
			
			// Dodatečně si zjistím název proměnné coby reference na instanci pole, abych mohl v případě chyby informovat
			// uživatele s "přesnější" infomací o chybě s polem:
			final String arrayName;
			
			final String textWithoutSpace = s.replaceAll("\\s", "");
			
			if (isMinusAtFirstChar)
				// Zde je na začátku mínus, tak je název pole až od druhého znaku:
				arrayName = textWithoutSpace.substring(1, textWithoutSpace.indexOf('.'));
			
			// Zde není na začátku příkazu mínus pro negaci hodnoty, tak je název pole od začátku po první tečku oddělující vlastnost length:
			else arrayName = textWithoutSpace.substring(0, textWithoutSpace.indexOf('.'));
			
			
			if (variableDataType != null) {
				if (variableDataType.equals(int.class)) {	// Zde by také šlo testova jako datový typ, length.class místo int.class
					// Zde ještě mýsím otestovat, zda mám znegovat hodnotu nebo ne:
					
					if (isMinusAtFirstChar)
						return returnResult(true, -length);						
						
					// jinak vrátím normální délku pole - bez negace hodnoty:
					return returnResult(true, length);
				}  										
				else
					outputEditor.addResult(txtDataTypeOfField + ": '" + arrayName + "' " + txtDataTypeIsDifferent
							+ OutputEditor.NEW_LINE_TWO_GAPS + txtValueWillNotBeCounted + " " + int.class + " != "
							+ variableDataType);
			}		
			else return returnResult(true, length);
		}
		
		
		
		
		
		
		
		
		
		
		
		
		// Otestuji, zda se jedná o syntaxi pro získání hodnoty z pole na nejakém indexu:
		// Syntaxe: (-) variableArray [index]
		else if (regEx.isGetValueFromArrayAtIndexInCommand(s) && (variableArray = getVariableArrayInstance(s)) != null) {
			// Zde se jedná o získání hodnoty z pole na zadaném indexu v poli:
			
			// Syntaxe:  (-) variableArray [index]
			
			// Postup:
			// Zjístím, zda mám hodnotu znegovat nebo ne, pak si opětovné zjistím hodnotu pole pro případně chybové hlášky,
			// pak index, ze kterého mám vzít hodnotu a otestuje datové typy, případně vrátím hodnotu nebo null na konci této metody:
			
			
			// Mohu odebrat všechny mezery:
			final String withoutSpace = s.replaceAll("\\s", "");
			
			// Zjistím, zda je na začátku mínus:
			final boolean isMinusAtFirstChar = isMinusAtFirstChar(withoutSpace);
			
			
			//Nyní si zjistím název / referenci pole:
			final String arrayName;
			
			if (isMinusAtFirstChar)
				// Zde se na začátku vyskytuje mínus, tak název začíná až za ním:
			 arrayName = withoutSpace.substring(1, withoutSpace.indexOf('['));
			
			else arrayName = withoutSpace.substring(0, withoutSpace.indexOf('['));
			
			
			// Nyní si mohu zjistit index, tj. mezi hranatýma závorkama:
			final String indexInText = withoutSpace.substring(withoutSpace.indexOf('[') + 1, withoutSpace.indexOf(']'));

			final int index = getIndexOfList(indexInText);

			if (index > -1) {
				if (index < variableArray.getArray().length) {
					// Nyní mám vše co potřebuji, zda mohu otestovat daotové typy:
					if (variableDataType != null) {
						if (variableDataType.equals(variableArray.getTypeArray())) {
							// Načtu si hodnotu z pole:
							final Object objValue = variableArray.getValueAtIndex(index);

							if (objValue != null) {
								if (isMinusAtFirstChar)
									return returnResult(true,
											getNegationOfValue(variableArray.getTypeArray(), objValue));

								return returnResult(true, objValue);
							} 
							else
								outputEditor.addResult(txtValueAtIndexIsNull + OutputEditor.NEW_LINE_TWO_GAPS
										+ txtValueOfVariableWillNotBeCounted);
						} 
						else
							outputEditor.addResult(txtDataTypeOfField + ": '" + arrayName + "' "
									+ txtDataTypeIsDifferent + OutputEditor.NEW_LINE_TWO_GAPS + txtValueWillNotBeCounted
									+ " " + variableArray.getTypeArray() + " != " + variableDataType);
					}

					else {
						// Zde prostě vrátím hodnotu:
						// Ale nejprve otestuji, zda ji mám znegovat neb ne:
						final Object objValue = variableArray.getValueAtIndex(index);

						if (isMinusAtFirstChar)
							return returnResult(true, getNegationOfValue(variableArray.getTypeArray(), objValue));

						return returnResult(true, objValue);
					}
				}
				else
					outputEditor.addResult(txtConditionsForIndexOfList + OutputEditor.NEW_LINE_TWO_GAPS
							+ txtSpecifiedIndexOfList + ": " + index);
			}
			else outputEditor.addResult(txtIndexOfListIsOutOfRange);
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		// Otestuji, zda se jedná o inkrementaci hodnoty (prefixová forma) v jednorozměrném poli, které bylo vytvořeno
		// uživatelem v editoru příkazů, syntaxe: ++ arrayName [index]  -> bez středníku, je třeba ho přidat, abych nemusel psát
		// nový regulární výraz, který by se lišil v absenci středníku na konci příkazu:
		else if (regEx.isIncrementArrayValueBefore(s + ";")) {
			// Zde musím nejprve inkrementovat hodnotu, vložit ji do pole a tuto inkrementovatnou hodnotu vrátit
			
			// Název pole je od ++ po první otevírací hranatou závorku:
			final String arrayName = s.substring(s.indexOf("++") + 2, s.indexOf('[')).replaceAll("\\s", "");
			
			final int index = getIndexOfArray(s);
			
			
			// Zde mám název pole, tak se mohu pokusít získat dané pole:
			variableArray = (VariableArray<?>) Instances.getArrayFromMap(arrayName);

			if (conditionForIncrementDecrement(variableArray, index, arrayName)) {
				if (variableDataType != null) {
					if (variableDataType.equals(variableArray.getTypeArray())) {
						// Zde jsou stejné i datové typy, tak mohu načíst
						// hodnotu, inkrementovat jí a vloit zpět do pole, pak
						// ji vrátím:
						final Object objValue = variableArray.getValueAtIndex(index);

						if (objValue != null) {
							// Zde není hodnota null, tak se mohu pokusit tuto
							// získanou hodnotu inkrementovat:
							final Object objFinalValue = getIncrementDecrementValue(objValue, true);

							if (objFinalValue != null) {
								// Zde se podařilo inkrementovat hodnotu na
								// indexu, tak ji mohu vložit do pole a vrátit:
								variableArray.setValueToIndex(index, objFinalValue);
								
								return returnResult(true, objFinalValue);
							}
							
							else outputEditor.addResult(txtError + ": " + txtDoesNotIncrementDecrement);
						} 
						else
							outputEditor.addResult(txtValueAtIndexIsNull + OutputEditor.NEW_LINE_TWO_GAPS
									+ txtValueOfVariableWillNotBeCounted);
					} 
					else
						outputEditor.addResult(txtDataTypeOfField + ": '" + arrayName + "' " + txtDataTypeIsDifferent
								+ OutputEditor.NEW_LINE_TWO_GAPS + txtValueWillNotBeCounted + " "
								+ variableArray.getTypeArray() + " != " + variableDataType);
				}

				else {
					// Zde nemusímm testovat datové typy:
					// Zde jsou stejné i datové typy, tak mohu načíst hodnotu,
					// inkrementovat jí a vloit zpět do pole, pak ji vrátím:
					final Object objValue = variableArray.getValueAtIndex(index);

					if (objValue != null) {
						// Zde není hodnota null, tak se mohu pokusit tuto
						// získanou hodnotu inkrementovat:
						final Object objFinalValue = getIncrementDecrementValue(objValue, true);

						if (objFinalValue != null) {
							// Zde se podařilo inkrementovat hodnotu na indexu,
							// tak ji mohu vložit do pole a vrátit:
							variableArray.setValueToIndex(index, objFinalValue);
							
							return returnResult(true, objFinalValue);
						}
						
						else outputEditor.addResult(txtError + ": " + txtDoesNotIncrementDecrement);
					} 
					else
						outputEditor.addResult(txtValueAtIndexIsNull + OutputEditor.NEW_LINE_TWO_GAPS
								+ txtValueOfVariableWillNotBeCounted);
				}
			}
		}
		
		
		
		
		// Otestuji, zda se jedná o inkrementaci hodnoty (postfixová forma) v jednorozměrném poli, které bylo vytvořeno
		// uživatelem v editoru příkazů, syntaxe: (-) arrayName [index] ++
		else if (regEx.isIncrementArrayValueInParameterBehind(s)) {
			// Otestji, zda je mínus na začátku textu:
			final boolean isMinusAtFirstChar = isMinusAtFirstChar(s);
			
			// Název pole:
			final String arrayName;
			
			// Mohu si z příkazu odebrat veškeré bílé znaky, to ničemu neuškodí, a nebudu mít dále probémy:
			final String textWithoutSpace = s.replaceAll("\\s", "");
			
			if (isMinusAtFirstChar)
				arrayName = textWithoutSpace.substring(1, textWithoutSpace.indexOf('['));
			
			else arrayName = textWithoutSpace.substring(0, textWithoutSpace.indexOf('['));
			
			
			// Index hodnoty na nějaké pozici v poli pro inkrementaci:
			final int index = getIndexOfArray(s);
			
			variableArray = (VariableArray<?>) Instances.getArrayFromMap(arrayName);
			
			
			if (conditionForIncrementDecrement(variableArray, index, arrayName)) {
				if (variableDataType != null) {
					if (variableDataType.equals(variableArray.getTypeArray())) {
						
						final Object objValue = variableArray.getValueAtIndex(index);
						
						if (objValue != null) {
							// Zde není hodnota null, tak se mohu pokusit tuto získanou hodnotu inkrementovat:
							final Object objFinalValue = getIncrementDecrementValue(objValue, true);
							
							if (objFinalValue != null) {
								// Zde se podařilo inkrementovat hodnotu na indexu, tak ji mohu vložit do pole a vrátit:
								variableArray.setValueToIndex(index, objFinalValue);
								
								return returnResult(true, objValue);
							}
							else outputEditor.addResult(txtError + ": " + txtDoesNotIncrementDecrement);
						}						
						else
							outputEditor.addResult(txtValueAtIndexIsNull + OutputEditor.NEW_LINE_TWO_GAPS
									+ txtValueOfVariableWillNotBeCounted);
					}
					else
						outputEditor.addResult(txtDataTypeOfField + ": '" + arrayName + "' " + txtDataTypeIsDifferent
								+ OutputEditor.NEW_LINE_TWO_GAPS + txtValueWillNotBeCounted + " "
								+ variableArray.getTypeArray() + " != " + variableDataType);
				}
				
				
				else {
					// Zde nemusímm testovat datové typy:
					// Zde jsou stejné i datové typy, tak mohu načíst hodnotu, inkrementovat jí a vloit zpět do pole, pak ji vrátím:
					final Object objValue = variableArray.getValueAtIndex(index);
					
					if (objValue != null) {
						// Zde není hodnota null, tak se mohu pokusit tuto získanou hodnotu inkrementovat:
						final Object objFinalValue = getIncrementDecrementValue(objValue, true);
						
						if (objFinalValue != null) {
							// Zde se podařilo inkrementovat hodnotu na indexu, tak ji mohu vložit do pole a vrátit:
							variableArray.setValueToIndex(index, objFinalValue);
							
							return returnResult(true, objValue);
						}						
						else outputEditor.addResult(txtError + ": " + txtDoesNotIncrementDecrement);
					}
					else
						outputEditor.addResult(txtValueAtIndexIsNull + OutputEditor.NEW_LINE_TWO_GAPS
								+ txtValueOfVariableWillNotBeCounted);
				}
			}
		}
		
		
		
		
		// Otestuji, zda se jedná o dekrementaci hodnoty (prefixová forma) v jednorozměrném poli, které bylo vytvořeno
		// uživatelem v editoru příkazů, syntaxe: -- arrayName [index]  -> bez středníku, je třeba ho přidat, abych nemusel psát
		// další regulární výraz, který by se lišil v absenxi středníku na konci příkazu:
		else if (regEx.isDecrementArrayValueBefore(s + ";")) {
			// Zde nejpve dekrementuji hodnotu na zadném indexu v poli, uložím dekrementovanou hodnotu zpět na daný index v poli
			// a tuto dekrementovanou hodnotu vrátím:
			
			// Název pole je od -- po první otevírací závorku:
			final String arrayName = s.substring(s.indexOf("--") + 2, s.indexOf('[')).replaceAll("\\s", "");
			
			// Index hodnoty na nějaké pozici v poli pro inkrementaci:
			final int index = getIndexOfArray(s);
			
			variableArray = (VariableArray<?>) Instances.getArrayFromMap(arrayName);
			
			if (conditionForIncrementDecrement(variableArray, index, arrayName)) {
				if (variableDataType != null) {
					if (variableDataType.equals(variableArray.getTypeArray())) {
						
						final Object objValue = variableArray.getValueAtIndex(index);
						
						if (objValue != null) {
							// Zde není hodnota null, tak se mohu pokusit tuto získanou hodnotu inkrementovat:
							final Object objFinalValue = getIncrementDecrementValue(objValue, false);
							
							if (objFinalValue != null) {
								// Zde se podařilo inkrementovat hodnotu na indexu, tak ji mohu vložit do pole a vrátit:
								variableArray.setValueToIndex(index, objFinalValue);
								
								return returnResult(true, objFinalValue);
							}
							else outputEditor.addResult(txtError + ": " + txtDoesNotIncrementDecrement);
						}						
						else
							outputEditor.addResult(txtValueAtIndexIsNull + OutputEditor.NEW_LINE_TWO_GAPS
									+ txtValueOfVariableWillNotBeCounted);
					}
					else
						outputEditor.addResult(txtDataTypeOfField + ": '" + arrayName + "' " + txtDataTypeIsDifferent
								+ OutputEditor.NEW_LINE_TWO_GAPS + txtValueWillNotBeCounted + " "
								+ variableArray.getTypeArray() + " != " + variableDataType);
				}
				
				
				else {
					// Zde nemusímm testovat datové typy:
					// Zde jsou stejné i datové typy, tak mohu načíst hodnotu, inkrementovat jí a vloit zpět do pole, pak ji vrátím:
					final Object objValue = variableArray.getValueAtIndex(index);
					
					if (objValue != null) {
						// Zde není hodnota null, tak se mohu pokusit tuto získanou hodnotu inkrementovat:
						final Object objFinalValue = getIncrementDecrementValue(objValue, false);
						
						if (objFinalValue != null) {
							// Zde se podařilo inkrementovat hodnotu na indexu, tak ji mohu vložit do pole a vrátit:
							variableArray.setValueToIndex(index, objFinalValue);
							
							return returnResult(true, objFinalValue);
						}
						else outputEditor.addResult(txtError + ": " + txtDoesNotIncrementDecrement);
					}
					else
						outputEditor.addResult(txtValueAtIndexIsNull + OutputEditor.NEW_LINE_TWO_GAPS
								+ txtValueOfVariableWillNotBeCounted);
				}
			}
		}
		
		
		
		// Otestuji, zda se jedná o dekrementaci hodnoty (postfixová forma) v jednorozměrném poli, které bylo vytvořeno
		// uživatelem v editoru příkazů, syntaxe: (-) arrayName [index] --
		else if (regEx.isDecrementArrayValueInParameterBehind(s)) {
			// Zjistím si, zda je na začátku 
			
			// Otestji, zda je mínus na začátku textu:
			final boolean isMinusAtFirstChar = isMinusAtFirstChar(s);
			
			// Název pole, otestuji dle mínusu:
			final String arrayName;
			
			final String textWithoutSpace = s.replaceAll("\\s", "");
			
			if (isMinusAtFirstChar)
				arrayName = textWithoutSpace.substring(1, textWithoutSpace.indexOf('['));
			else arrayName = textWithoutSpace.substring(0, textWithoutSpace.indexOf('['));
			
			
			// Index hodnoty na nějaké pozici v poli pro inkrementaci:
			final int index = getIndexOfArray(s);
			
			variableArray = (VariableArray<?>) Instances.getArrayFromMap(arrayName);
			
			if (conditionForIncrementDecrement(variableArray, index, arrayName)) {
				if (variableDataType != null) {
					if (variableDataType.equals(variableArray.getTypeArray())) {
						
						final Object objValue = variableArray.getValueAtIndex(index);
						
						if (objValue != null) {
							// Zde není hodnota null, tak se mohu pokusit tuto získanou hodnotu inkrementovat:
							final Object objFinalValue = getIncrementDecrementValue(objValue, false);
							
							if (objFinalValue != null) {
								// Zde se podařilo inkrementovat hodnotu na indexu, tak ji mohu vložit do pole a vrátit:
								variableArray.setValueToIndex(index, objFinalValue);
								
								return returnResult(true, objValue);
							}
							else outputEditor.addResult(txtError + ": " + txtDoesNotIncrementDecrement);
						}						
						else
							outputEditor.addResult(txtValueAtIndexIsNull + OutputEditor.NEW_LINE_TWO_GAPS
									+ txtValueOfVariableWillNotBeCounted);
					}
					else
						outputEditor.addResult(txtDataTypeOfField + ": '" + arrayName + "' " + txtDataTypeIsDifferent
								+ OutputEditor.NEW_LINE_TWO_GAPS + txtValueWillNotBeCounted + " "
								+ variableArray.getTypeArray() + " != " + variableDataType);
				}
				
				
				else {
					// Zde nemusímm testovat datové typy:
					// Zde jsou stejné i datové typy, tak mohu načíst hodnotu, inkrementovat jí a vloit zpět do pole, pak ji vrátím:
					final Object objValue = variableArray.getValueAtIndex(index);
					
					if (objValue != null) {
						// Zde není hodnota null, tak se mohu pokusit tuto získanou hodnotu inkrementovat:
						final Object objFinalValue = getIncrementDecrementValue(objValue, false);
						
						if (objFinalValue != null) {
							// Zde se podařilo inkrementovat hodnotu na indexu, tak ji mohu vložit do pole a vrátit:
							variableArray.setValueToIndex(index, objFinalValue);
							
							return returnResult(true, objValue);
						}
						else outputEditor.addResult(txtError + ": " + txtDoesNotIncrementDecrement);
					}
					else
						outputEditor.addResult(txtValueAtIndexIsNull + OutputEditor.NEW_LINE_TWO_GAPS
								+ txtValueOfVariableWillNotBeCounted);
				}
			}
		}
		
		
		
		
		
		
		
		
		


		
		
		
		
		
		
		// proměnná v instanci třídy:
		else if (regEx.isClassVariable(s + ";")) {
			// Odeberu se mezery proměnné : (-) referenceName . variableName 
			final String commandWithoutSpace = s.replaceAll("\\s", "");
			
			// Nyní se jedná o syntaxi bez mezer:   (-) referenceName.variableName;			
			final boolean isCharAtFirstIndexMinus = isMinusAtFirstChar(s);
			
			
			final String referenceName;
			
			if (isCharAtFirstIndexMinus)
				// Zjistím si název reference - od začátku (za prvním minusem) po první tečku:
				referenceName = commandWithoutSpace.substring(1, commandWithoutSpace.indexOf('.'));
			// Zjistím si název reference - od začátku po první tečku:
			else referenceName = commandWithoutSpace.substring(0, commandWithoutSpace.indexOf('.'));
			
			
			
			// Jelikož už v příkazu nejsou mezery, tak od první tečky do konce se jedná o název prměnné:
			final String variableName = commandWithoutSpace.substring(commandWithoutSpace.indexOf('.') + 1);
			
			
			// Nyní mám název reference na instancí třídy: referenceName
			// a název prměnné: variableName
			
			// tak mohu otestovat, zda existuje nebo ne:
			final Object objInstance = Instances.getInstanceFromMap(referenceName);
			
			if (objInstance != null) {
				// Zde jsem našel hledanou instanci, tak zjistím její proměnnou:
				final Field field = getFieldFromInstance(objInstance, variableName);
				
				if (field != null) {
					// Zde jsem našel hledanou položku:
					
					// Nejprve otestuji, zda není proměnná variableDataType null, pokud ano, získám hodnotu a vrátím ji, jinak otestuje
					// datové typy, a dle toho rozhodnnu, jak dále:
					
					if (variableDataType != null) {
						// Otestuji její datový typ:
						if (field.getType().equals(variableDataType)) {
							// Zde se shodují datové typy, mohu si vzít její
							// hodnotu:

							// Zde si získám její hodnotu:
							final Object objFieldValue = ReflectionHelper.getValueFromField(field, objInstance);

							if (objFieldValue != null) {
								// Otestuji, zda se má hodnota v proměnné
								// "znegovat":
								if (isCharAtFirstIndexMinus) {
									// Otestuji, zda se jedná o celé číslo nebo
									// destinné - dle toho mohu použít Big
									// Double nebo Integer
									final Object objValue = getNegationOfValue(objFieldValue.getClass(), objFieldValue);

									return returnResult(true, objValue);
								}

								// Zde před proměnnou není mínus, tak její
								// hodnotu prostě přidám do kolekce:
								else return returnResult(true, objFieldValue);
							}
						}
						else
							outputEditor.addResult(txtDataTypeOfField + ": '" + variableName + "' "
									+ txtDataTypeIsDifferent + OutputEditor.NEW_LINE_TWO_GAPS + txtValueWillNotBeCounted
									+ " " + field.getType() + " != " + variableDataType);
					}
					else {
						// Zde si získám její hodnotu:
						final Object objFieldValue = ReflectionHelper.getValueFromField(field, objInstance);

						if (objFieldValue != null) {
							// Otestuji, zda se má hodnota v proměnné
							// "znegovat":
							if (isCharAtFirstIndexMinus) {
								// Otestuji, zda se jedná o celé číslo nebo
								// destinné - dle toho mohu použít Big Double
								// nebo Integer
								final Object objValue = getNegationOfValue(objFieldValue.getClass(), objFieldValue);

								return returnResult(true, objValue);
							}

							// Zde před proměnnou není mínus, tak její hodnotu
							// prostě přidám do kolekce:
							else return returnResult(true, objFieldValue);
						}
					}

				}
				else
					outputEditor.addResult(txtValueOfVariableWillNotBeCounted + OutputEditor.NEW_LINE_TWO_GAPS
							+ txtVariable + ": " + variableName);
			}
			else
				outputEditor.addResult(txtReferenceDoesNotPointToInstanceOfClass + ": " + referenceName + " = null"
						+ OutputEditor.NEW_LINE_TWO_GAPS + txtValueWillNotBeCounted);
		}
		
		
		
		
		
		
		
		// Regulární výraz rozpoznává zavolání metody se středníkem  na konci, tak abych nemusel psát další výraz pro testování
		// syntaxe pro zavolání metody, který by se lišil tím, že na konci nebude středník, tak ho jednoduše do podmínky přídam
		else if (regEx.isCallTheMethod(s + ";")) {
			// Název reference na instanci třídy:
			final String variableClassName = getReferenceNameClass(s);
			
			// název metody:
			final String methodName = getMethodName(s);
					
			
			// Všechny parametry budou mezi první otevírací a poslední zavírací závorkou, akorát nesmím odebírat nezery, pouze bílé znaky
			// na začátku a na konci daného sub řetězce:
			final String textOfParameters = getParametersOfMethodInTextFormat(s);
			
			// Naní výše uvedený řetězec parametrů rozdělím dle desetinné čárky, toto pole "projdu" cyklem a nad každou hodnoutou pole
			// zjistím o jaký typ se jedý a zjistím si tím příslušné parametry metody:
			
			final String[] partsOfParameters = textOfParameters.split(",");				
			
			
			// Zjistím si instanci dané třídy:
			final Object objInstance = Instances.getInstanceFromMap(variableClassName);				
			
			if (objInstance != null) {
				// Do prvního parametru metody musím ještě přidat středník, protože regulární výraz, který testuje danou metodu
				// vyžaduje na konci příkazu středník, jinak by text metody neprošel výrazem
                final Object[] returnDataArray = callTheMethod(s + ";", objInstance, methodName,
                        variableDataType, partsOfParameters);

                final boolean wasFilling = Boolean.parseBoolean(String.valueOf(returnDataArray[0]));

                if (wasFilling) {
                    final Object objReturnValue = returnDataArray[1];
                    /*
                     * Zde mám ve výše uvedené proměnné návratovou hodnotu metody, ale u ní už
                     * nemohu testovat, zda je null nebo ne, protože null může metoda vrátit, někdy
                     * je to i třeba - záleží na uživatelově myšlence, tak tuto hodnotu prostě
                     * přidám do výsledků:
                     */
                    return returnResult(true, objReturnValue);
                }
                // Zde se metody nezavolaly, takže vráceno null, a jelikož se nezavolaly, tak nastala chyba, takže
                // vrátím neúspěch:
                else
                    outputEditor.addResult(txtMethodHasNotBennCalled + OutputEditor.NEW_LINE_TWO_GAPS + txtMethod
                            + ": " + methodName);
            }
			// Zde se nepodařilo najít instanci požadované třídy:
			else outputEditor.addResult(txtInstancesNotFound + ": '" + variableClassName + "'. " + txtMethodWillNotBeInclude);
		}
		
		
		
		
		
		
		
		
		
		/*
		 * Otestuji, zda se jedná o zavolání statické metody, oproti zavolání metody nad
		 * instancí třídy má o tečku navíc, protože obsahuje alespoň jeden balíček, pak
		 * třídu, pak metodu. Zavolání metody nad instancí třídy je jen instance třídy a
		 * název metody.
		 */
		else if (regEx.isCallStaticMethod(s + ";")) {
			/*
			 * Postup pro získání třídy s balíčky, názvu metody a případně její parametry:
			 * 
			 * - vezmu si text od začátku po první otevírací kulatou závorku a rozdělím jej
			 * dle desetinné tečky.
			 * - Z toho pole (hodnoty z textu rozděleného dle desetinných teček) je jako
			 * poslední hodnota název metody a jako předposlední název třídy a vše před
			 * třídou jsou balíčky.
			 * 
			 * - parametry metody jsou od první oevírací kulaté závorky po poslední zavírací
			 * kulatou závorku.
			 */
			
			/*
			 * Jednorozměrné pole obsahující hodnoty (po první otevírací kulatou závorku)
			 * oddělené desetinnou tečkou.
			 */
			final String[] valuesByDot = s.substring(0, s.indexOf('(')).split("\\.");
			
			/*
			 * Proměnná, která bude obsahovat název třídy s balíčky.
			 */
			String classWithPackages = "";
			
			for (int i = 0; i < valuesByDot.length - 1; i++)
				classWithPackages += valuesByDot[i].trim() + ".";
			
			// Odeberu poslední desetinnou tečku:
			classWithPackages = classWithPackages.substring(0, classWithPackages.length() - 1);
			
			
			/*
			 * Název metody je jako poslední položka v poli.
			 */
			final String methodName = valuesByDot[valuesByDot.length - 1].trim();
			
			
			/*
			 * Proměnná, do které si vložím text, který by měl být jako parametr / parametry
			 * metody. Tj. hodnoty, které se nacházejí mezi první otevírací a posladní
			 * zavírací kulatou závorkou.
			 */
			final String parametersInText = getParametersOfMethodInTextFormat(s);
			
			/*
			 * Proměnná, do které se vloží buď parametry metody v proměnné parametersInText
			 * oddělené desetinnou čárkou nebo se jen vytvoří instance tohoto pole - bude
			 * prázdné, nebude obsahovat žádný prvek v případě, že nebyl zadán žádný
			 * parametr.
			 */
			final String[] parameters;
			
			// Otestuji, zda byl zadán nějaký parametr:
			if (parametersInText.replaceAll("\\s", "").isEmpty())
				// de nebyl zadán žádný parametr, tak pole pouze inicializuji, ale žádný prvek
				// obsahovat nebude:
				parameters = new String[] {};
			// Zde pole naplním zadanými parametry v datovém typu String, jednotlivé hodnoty
			// budou odděleny desetinnou čárkou.
			else
				parameters = parametersInText.split(",");
				
			
			
			
			
			
			
			// Nyní si zkusím načíst příslušnou třídu a zavolat její metodu:
			final String pathToSrc = App.READ_FILE.getPathToSrc();

			// Otestuji adresář src v otevřeném projektu:
			if (pathToSrc == null)
				outputEditor.addResult(txtSrcDirNotFound);

			
			/*
			 * Získám si cestu ke třídě, kterou mám načíst, resp. její zkompilovanou verzi.
			 * 
			 * Note:
			 * Toto testování, zda příslušná třída existuje je zde zbytečné, protože mě
			 * zajímá její zkompilovaná verze, tj. soubor .class. Ale kdyby ani ta třída
			 * samotná neexistovala, bylo by to k ničemu, prootože při další kompilaci by se
			 * to zjistilo (že třída neexistuje), ale k tomuto by zde dojít nemělo.
			 * 
			 * Tak vypíšu pouze hlášku do editoru výstupů, abych uživatele informoval.
			 */
			final String pathToJavaClassFile = pathToSrc + File.separator + classWithPackages.replace(".", File.separator)
					+ ".java";


			// Pro načtení zkompilované třídy z adresáře bin
            final Class<?> clazz;

			
			/*
			 * Zde potřebuji otestovat, zda existuje soubor '.class' v adresáři 'bin' v aktuálně otevřeném projektu.
			 * Pokud ne, je teřba zde ukončit metodu, protože by se stejně nemohlo pokračovat, když by se nepodařilo načíst
			 * příslušný soubor .class (zkompilovanou třídu), ze které se má zavolat statická metoda.
			 */
            if (!existCompiledClass(classWithPackages)) {
                /*
                 * V tomto případě se napodařilo nalézt třídu z diagramu tříd, takže uživatel buď chybně zadal
                 * syntaxi / názvy balíčků, tříd nebo metod. Tak zde
                 * zkusím prohledat ještě vnitřní třídy.
                 */
                final String pathToBin = App.READ_FILE.getPathToBin();

                if (!ReadFile.existsDirectory(pathToBin)) {
                    outputEditor.addResult(txtFailedToFindTheClass + ": " + pathToJavaClassFile + ", " +
                            txtFailedToFindBinDir);
                    return returnResult(false, null);
                }

                // Získám si cestu k vnitřní třídě:
                final String pathToInnerClass = getPathToCompiledInnerClass(classWithPackages, pathToBin);

                if (pathToInnerClass == null) {
                    outputEditor.addResult(txtFailedToFindTheClass + ": " + pathToJavaClassFile + ", " +
                            txtFailedToFindInnerClass);
                    return returnResult(false, null);
                }


                // Načtu si vnitřní třídu:
                clazz = App.READ_FILE.loadCompiledClass(pathToInnerClass, outputEditor);

                // Uživateli zpřístupníme pouze veřejné a chráněné statické vnitřní třídy:
                if (clazz == null) {
                    outputEditor.addResult(txtFiledToLoadClassFile);
                    return returnResult(false, null);
                }

                if (!Modifier.isStatic(clazz.getModifiers())) {
                    outputEditor.addResult(txtInnerClassIsNotStatic);
                    return returnResult(false, null);
                }

                if (!Modifier.isPublic(clazz.getModifiers()) && !Modifier.isProtected(clazz.getModifiers())) {
                    outputEditor.addResult(txtInnerClassIsNotPublicEitherProtected);
                    return returnResult(false, null);
                }
            }
			
			/*
			 * Zde vím, že třída existuje, tak si načtu její zkompilovanou verzi.
			 * 
			 * Note:
			 * Zde zkompilovanou třídu nenačtu pomocí metody getClassFromFile, ale pomocí
			 * metody loadCompiledClass, protože by se jinak mohly všechny (nebo ta jedna)
			 * třída zkompilovat a když bych zrovna do ní třeba naplňoval nějakou proměnnou,
			 * tak by se nenaplnila, protože by se zde přenačetla instance a proměnnná by se
			 * vkládala někam jinam, než je v jiné metodě načtená příslušná instance pro
			 * vložení výsledné hodnoty z této metody getValueFromVar_Method...
			 */
			else clazz = App.READ_FILE.loadCompiledClass(classWithPackages, outputEditor);
							
			
			if (clazz == null) {
				outputEditor.addResult(txtFiledToLoadClassFile);
				
				/*
				 * Note: 
				 * Tato část by nikdy nastat neměla, už kvůli předchozí podmínce, ale kdyby náhodou.
				 * 
				 * Zde nebyl načten přeloženy soubor .class příslušné (potřebné třídy), takže si
				 * níže v následujícím cyklu nemohu načist metody příslušné třídy, takže zde
				 * musím metodu ukončit, protože došlo k vážné chybě, kdy není například
				 * zkompilována příslušná třída nebo byl příslušný soubor smazán apod. Jelikož
				 * jsem v této podmínce pro zavolání statické metody, tak mohu skončit, protože
				 * do jiné podmínky se stejně nepůjde.
				 */
				return returnResult(false, null);
			}


			
			
			/*
			 * Získám si list s metodami, které je možné zavolat nad načtenou třídöu clazz,
			 * jedná se o všechny veřejné a chráněné statické metody jak nad třídou
			 * samotnou, tak nad všemi jejími předky. Metody musí být statické.
			 */
			final List<Method> availableMethodsList = getAvailableMethods(clazz, new ArrayList<>(), true);
			
			
			
			/*
			 * Cyklem projdu všechny metody a zavolám tu metodu, u které se najde jako první
			 * shoda s názvem, není privátní a má příslušný počet parametrů, který byl zadán
			 * v editoru příkazů a tyto hodnoty z editoru se podařilo přetypovat na
			 * příslušný, resp. potřebný datový typ v parametrech příslušné metody.
			 */
			for (final Method m : availableMethodsList) {
				// Otestuji název metody:
				if (!m.getName().equals(methodName))
					continue;

				// Otestuji počet parametrů metody:
				if (m.getParameterCount() != parameters.length)
					continue;


				/*
				 * Note:
				 * Vím, že bych mohl využít metodu 'callTheMethod' (kdybych ji trochu upravil,
				 * že když se nezadá reference ani příslušná intance, pak se provede jen to co
				 * je níže. Ale to je trochu práce navíc. A už by to nebylo moc přehledné.
				 */

				
				
				/*
				 * Zde potřebuji otestovat ještě návratový typ metody, aby se rovnal s datovým
				 * typem, který požaduje nějaká "operace", například pokud je tato metoda
				 * (getValueFromVar_...) volána aby se získali nějaké parametry metody a tím
				 * parametrem je tato iterovaná metody, tak se její navratový datový typ musí
				 * rovnat tomu parametru.
				 */
				if (variableDataType != null && !m.getReturnType().equals(variableDataType))
					continue;

				
				/*
				 * Note:
				 * Zde, pokud byl zadán návratovy typ metody, tak prošel testováním, takže se
				 * návratový typ metody shoduje s požadovyným typem pro hodnotu, tak mohu zkusit
				 * zavolat metodu
				 */
				
				
				/*
				 * Proměnná, do které si uložím hodnotu, která se vrátila, případně null, pokud
				 * se nic nevrátilo, nebo vrátila právě null hodnota apod.
				 */
				Called objReturnValue = null;

				/*
				 * Zde jsem našel metodu se stejným názvem a počtem parametrů, tak zjistím, zda
				 * je ten počet parametrů nula, tj. metoda nemá žádný parametr (a zároveň žádný
				 * nebyl zadán), pak to metodu mohu rovnou zavolat, pokud metod má nějaké
				 * parametry, pak pokračuji dále.
				 */
				if (parameters.length == 0)
					objReturnValue = operationWithInstanees.callMethodWithoutParameters(m, null, false);
					

				else {
					/*
					 * Zde jsem našel metodu se stejným názvem a počtem parametrů (metoda má alespoň
					 * jeden parametr), tak zkusím parametry v datovém typu String přetypovat na
					 * hodnoty, který požaduje metoda, pokud se to povede, pak se zavolá, jinak se
					 * pokračuje.
					 */
					// Zde jsem našel metodu, která má stejný název a počet parametrů
					// tak zkusím zadané parametry na dané typy přetypovat:
					final List<Object> parametersList = getListOfParsedParameters(parameters, null, m);

					if (parametersList != null) {
						// Zde se podařilo přetypovat zadaný text v editoru na nějaké datové typy

						// Do následujícího listu vložím "otestované" parametry s tím, že otestuji, zda
						// nějaký z parametrů není
						// jednorozměrné pole, pokud ano, tak ho musím přetypovat na dané pole, protože
						// se v tomto případě
						// jedná pouze o tu "referenční" verzi či jak to nazvat, prostě to není klasické
						// - potřebné pole s hodnotami
						// ve stavu, abych ho mohl předat, tak ho převezu na "normální pole", abych ho
						// následně mohl předat do metody:
						final List<Object> listOfTestedParameters = getListOfTestedParameters(parametersList, m);

						// Zde mám poze zavolat metodu:
						objReturnValue = operationWithInstanees.callMethodWithParameters(m, null, false,
								listOfTestedParameters.toArray());
					}
				}
				
				
				
				/*
				 * Otestuji, zda se nějaká metoda alespoň pokusila zavolat.
				 * 
				 * Pokud ano, tak se zjistí, zda se zavolala, případně se vypíšu neúspěch. Pokud
				 * se zavolala, pak se zjistí, zda příslušná metoda něco vrací, pokud ano, tak
				 * se vypíšu hodnota, která se vrátila (je v objektu Called v proměnné
				 * objReturnValue), pokud nic nevratí, tak se vypíše jen úspěch.
				 */
				if (objReturnValue != null) {
					if (objReturnValue.isWasCalled())
						return returnResult(objReturnValue.isWasCalled(), objReturnValue.getObjReturnValue());

					else
						outputEditor.addResult(txtMethodHasNotBennCalled + OutputEditor.NEW_LINE_TWO_GAPS + txtMethod
								+ ": " + methodName);

					/*
					 * Sem se dostanu pouze v případě, že se nějaká metoda zavolala, takže proměnná
					 * objReturnValue je naplněna, ale došlo k nějaké chybě, vypadla nějaká výjimka
					 * a její info bylo vypsáno do editoru výstupů, tak zde již nic vypisovat nebudu
					 * (něco jako 'return txtFailure;' apod.).
					 */
				}
			}

			/*
			 * Sem bych se dostat neměl, pouze v případě, že nebyla metoda se zadaným názvem
			 * nebo parametry nalezena, nebo pokud se vůbec nenašla metoda, u které by se
			 * shodoval návratový typ hodnoty metody s tím požadovaným , tak o tom informuji
			 * uživatele:
			 */
			outputEditor.addResult(
					txtErrorWhileCallingStaticMethod_1 + ": " + classWithPackages + " " + txtSpecificMethodWasNotFound);
		}
		
		
		
		
		
		
		
		
		
		/*
		 * Zde otestuji, zda se jedná o zavolání konstruktoru, resp. vytvoření instance,
		 * ale tato intance se nebude nikde v aplikaci "skladovat / ukládat" pro
		 * pozdější využití (z hlediska aplikace). Jedna se pouze o zavolání
		 * konstruktoru (případně vypsání chybové hlášky, pokud dojde k nějaké chybě) a
		 * výsledná vytvořená instace se vráti, případně se ještě otestuji datové typy
		 * hodnot.
		 */
		else if (regEx.isCallConstructor(s + ";")) {
			// Syntaxe: new packageName. (anotherPackageName.) ClassName(parameters);
			
			/*
			 * Postup:
			 * 
			 * - Zjistit a načíst třídu
			 * 
			 * - Pokud nejsou zadány parametry, tak zkusit zavolat výchozí nullary
			 * konstruktor
			 * 
			 * - Pokud jsou zadány parametry, pak je cyklem projít a vždy zkusit zjistit
			 * příslušné parametry a předat je do zavolání konstruktoru.
			 */

			/*
			 * Pro získání názvu třídy s balíčky, ve kterém / kterých se nachází je více
			 * možností, já využiji následující postup, odebrat všechny bílé znaky v řetezci
			 * a vzít text od 3. znaku (za w) až po první otevírací kulatou závorku.
			 */
			final String classWithPackages = s.replaceAll("\\s", "").substring(3, s.replaceAll("\\s", "").indexOf('('));
			
			/*
			 * Parametry si získám tak, že si vezmu text od první otevírací kulaté závorky,
			 * až po poslední uzavírací kulatou závorku a ty rozdělím dle desetinné čárky,
			 * ale pouze v případě, že text mezi uvedenými závorkami nebude prázdný, popř.
			 * jen bílé znaky, pak není zadán žádný parametr.
			 */
			final String textBetweenBrackets = getParametersOfMethodInTextFormat(s);
			
			/*
			 * Proměnná, typu jednorozměrné pole, která bude obsahovat parametry
			 * konstruktoru.
			 * 
			 * Pokud text mezí závorkami jsou jen bílé znaky, pak bude toto pole prázdné,
			 * tj. bude mít nula hodnot, tj. nula parametrů (resp. žádný parametr), jinak
			 * pokud text mezi závorkami nebudou jen bílé znaky, pak se tento text mezi
			 * závorkami rozdělí dle desetinných čárek, kterými se oddělují jednotlivé
			 * parametry konstruktoru.
			 */
			final String[] parameters;

			if (textBetweenBrackets.replaceAll("\\s", "").isEmpty())
				parameters = new String[] {};

			else
				parameters = textBetweenBrackets.split(",");
			
			
			
			
			/*
			 * Načtu si cestu k adresáři src, pokud neexistuje, pak vrátím příslušné
			 * oznámení, které se vypíše do editoru výstupů.
			 */
			final String pathToSrc = App.READ_FILE.getPathToSrc();

			if (pathToSrc == null)
				outputEditor.addResult(txtSrcDirNotFound);

			
			/*
			 * Otestuji, zda existuje třída, jejíž konstruktor se má zavolat, protože je
			 * třeba jej pro jistotu znovu zkompilovat a pokud se třídy / třída zkompiluje v
			 * pořádku, pak si načtu její zkompilovanou verzi (soubor .class) v adresáři
			 * 'bin'.
			 */
			final String pathToJavaClassFile = pathToSrc + File.separator + classWithPackages.replace(".", File.separator)
					+ ".java";

			if (!ReadFile.existsFile(pathToJavaClassFile))
				outputEditor.addResult(txtClassNotFound + OutputEditor.NEW_LINE_TWO_GAPS + pathToJavaClassFile);

			
			/*
			 * Zde potřebuji otestovat, zda existuje soubor '.class' v adresáři 'bin' v aktuálně otevřeném projektu.
			 * Pokud ne, je teřba zde ukončit metodu, protože by se stejně nemohlo pokračovat, když by se nepodařilo načíst
			 * příslušný soubor .class (zkompilovanou třídu), ze které se má zavolat statická metoda.
			 */
			if (!existCompiledClass(classWithPackages)) {
				outputEditor.addResult(txtFileDotClass + " '" + classWithPackages + "' " + txtFileDotClassWasNotFound);
				
				/*
				 * Zde nebyl načten přeloženy soubor .class příslušné (potřebné třídy), takže si
				 * níže v následujícím cyklu nemohu načist metody příslušné třídy, takže zde
				 * musím metodu ukončit, protože došlo k vážné chybě, kdy není například
				 * zkompilována příslušná třída nebo byl příslušný soubor smazán apod. Jelikož
				 * jsem v této podmínce pro zavolání statické metody, tak mohu skončit, protože
				 * do jiné podmínky se stejně nepůjde.
				 */
				return returnResult(false, null);
			}
			
			
			
			/*
			 * Zde vím, že třída existuje, tak si načtu její zkompilovanou verzi.
			 * 
			 * Note: Zde zkompilovanou třídu nenačtu pomocí metody getClassFromFile, ale
			 * pomocí metody loadCompiledClass, protože by se jinak mohly všechny (nebo ta
			 * jedna) třída zkompilovat a když bych zrovna do ní třeba naplňoval nějakou
			 * proměnnou, tak by se nenaplnila, protože by se zde přenačetla instance a
			 * proměnnná by se vkládala někam jinam, než je v jiné metodě načtená příslušná
			 * instance pro vložení výsledné hodnoty z této metody getValueFromVar_Method...
			 * 
			 * Proto si načtu pouze tu zkompilovanou verzí třídy (soubor .class z adresáře
			 * 'bin') a mám tím zaručeno, že se ty instance nepřenačtou a naplnění
			 * proměnných apod. bude tak úspěšné.
			 */
			final Class<?> clazz = App.READ_FILE.loadCompiledClass(classWithPackages, outputEditor);
			
						

			if (clazz == null) {
				outputEditor.addResult(txtFiledToLoadClassFile);
				
				/*
				 * Note:
				 * Tato část by nikdy nastat neměla, už kvůli předchozí podmínce, ale kdyby
				 * náhodou.
				 * 
				 * Zde nebyl načten přeloženy soubor .class příslušné (potřebné třídy), takže si
				 * níže v následujícím cyklu nemohu načist metody příslušné třídy, takže zde
				 * musím metodu ukončit, protože došlo k vážné chybě, kdy není například
				 * zkompilována příslušná třída nebo byl příslušný soubor smazán apod. Jelikož
				 * jsem v této podmínce pro zavolání statické metody, tak mohu skončit, protože
				 * do jiné podmínky se stejně nepůjde.
				 */
				return returnResult(false, null);
			}				

			
			
			
			
			/*
			 * Zde potřebuji otestovat ještě "typ" třídy, resp. zda je instance této třídy
			 * skutečně "datový typ" pro potřebný parametr, například pokud je tato metoda
			 * (getValueFromVar_...) volána aby se získali nějaké parametry metody a tím
			 * parametrem je tato třída, resp. třídy, kde se má zavolat nějaký konstruktor,
			 * tak se "datový typ" této třídy musí rovnat tomu parametru.
			 */			
			
			// Z mě neznámého důvodu je třeba testovat rovnoust tříd pomocí textů:
			if (variableDataType != null && !clazz.toString().equals(variableDataType.toString())) {
				outputEditor.addResult(txtDataTypesAreNotEqual + ": " + clazz + " != " + variableDataType);
				
				return returnResult(false, null);
			}
				
			
			
			
			
			
			
			/*
			 * Note:
			 * Zde bych ještě mohl doplnit, že pokud není zadán žádný parametr, tak mohu
			 * zkusit vytvořit instanci pomocí metody clazz.newInstance(), ale raději si
			 * pohlídám všechny konstruktory, zda jsou veřejné, apod. Abych si nepřidával
			 * kód navíc
			 */
			
			
			
			
			/*
			 * Nyní je vše v pořádku načteno tak si načtu veškeré konstruktory příslušné třídy
			 * a zkusím najít ten, který je veřejný a kde se
			 * shoduje počet parametrů. Pokud takový najdu, pak zkusím všechny parametry
			 * přetypovat na příslušné typy a zavolat ten konstruktor.
			 */
			
			for (final Constructor<?> c : clazz.getDeclaredConstructors()) {
				/*
				 * Pokud není konstruktor veřejný, pak nepůjde zavolat, tak budu pokračovat v
				 * iteraci.
				 */
				if (!Modifier.isPublic(c.getModifiers()))
					continue;
				
				
				/*
				 * Pokud se nerovná počet parametrů konstruktoru a získané parametry, pak bude
				 * také pokračovat další iterací v cyklu.
				 */
				if (c.getParameterCount() != parameters.length)
					continue;							
				
				
				
				/*
				 * V této části jsem našel veřejný konstruktor, který má stejný počet parametrů,
				 * tak zkusím zadané hodnoty přetypovat na požadované paraemtry, případně pokud
				 * jsou to metody či proměnné tak získat příslušnou hodnotu apod.
				 */
				
						
				/*
				 * Proměnná, do které se by vrátí hodnota coby vytvořená instance příslušné
				 * třídy díky zavolání konstruktoru, pokud dojde k nějaké chybě při vytváření
				 * konstruktoru, tak se do této hodnoty vrátí null.
				 */
				final Object objInstance;
				
				/*
				 * Nejprve otestuji, zda je vůbec nějaký parametr zadán, pokud ne, pak jej mohu
				 * rovnou zavolat.
				 */
				if (c.getParameterCount() == 0)
					objInstance = operationWithInstanees.callConstructor(c);
					
				
				else {
					/*
					 * List, který by měl obsahovat parametry pro příslušný konstruktor v příslušném
					 * datovém typu.
					 */
					final List<Object> parametersList = getListOfParsedParameters(parameters, c, null);

					if (parametersList != null)
						objInstance = operationWithInstanees.callConstructor(c, parametersList.toArray());

					else
						objInstance = null;				
				}
				
				
				/*
				 * Zde otestuji, zda byl úspěšně zavolán nějaký konstruktor, pokud ano, vrátím
				 * příslušnou instanci, který byla zavoláním konstruktoru vytvořena, pokud ne,
				 * pak byly chybová hlášení o tom, že se nepodařilo zavolat konstruktor s
				 * příslušnými parametry nebo jiná chyba vypsány již do editoru výstupů, tak zde
				 * podmínka neprojde a nic se nevrátí, tak pokračuji další iterací.
				 */
				if (objInstance != null)
					return returnResult(true, objInstance);
			}
			
			/*
			 * zde jsem prošel veškeré konstruktory v dané třídě a žádný se nepodařilo
			 * úspěšně zavolat, všech byly vypsány chyby, ke kterém došlo při pokusu o
			 * jejich zavolání, tak to zde jen "ujasním".
			 */			
			outputEditor.addResult(txtPublicConstructorWasNotFound);
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		// Zde otestuji, zda se jedná o inkrementaci proměnné v instanci třídy:
		else if (regEx.isIncrementClassVariable(s)) {
			// Zde se jedná buď o syntaxi: (-) referenceName.variable++    nebo: ++referenceName.variable   
			
			// Zde otestuji, zda se jedná o inkrementaci před nebo za proměnnou:
			// Abych věděl, zda mám nejprve vrátit hodnotu a pak přičíst jedničku nebo nejprve přičíst jedničku a
			// po sléze vrátit danou hodnotu (inkremetovanou)
			
			
			// Abych nemusel psát další výraz, tak na konec přidám středník (výraz by se
			// lišil akorát, že by nebyl na koni příkazu středník):
			if (regEx.isIncrementClassVariableBefore(s + ";")) {
				// Zde se jedná o syntaxi: ++referenceName.variable
				
				// Takže nejprve inkrementuji proměnnou a pak ji vrátím:
				
				// Zde mohu odebrat veškeré mezery, abych předešel nechtěným chybám či vyjímkám:
				final String commandWithoutSpace = s.replaceAll("\\s", "");
				
				// Nyní se jedná o syntaxi bez mezer:   ++referenceName.variableName;
				// Zjistím si název reference - od druhého + po první tečku:
				final String referenceName = commandWithoutSpace.substring(commandWithoutSpace.indexOf("++") + 2,
						commandWithoutSpace.indexOf('.'));
				
				
				// Název proměnné instance třídy - od první tečky do konce (zde se na konci středník nenachází, a mezery jsou odebrané, 
				// takže zůstane jen název proměnné):
				final String variableName = commandWithoutSpace.substring(commandWithoutSpace.indexOf('.') + 1);
				
				
				// Najdu si instanci třídy dle názvu reference:
				final Object objInstance = Instances.getInstanceFromMap(referenceName);
				
				// Pokusím se inkrementovat hodnotu proměnné:
				final String result = getIncrementOrDecrementVariable(variableName, objInstance, true, false);	
				
				if (result.equals(txtSuccess)) {
					// Zde se podařilo inkrementovat hodnotu, tak ji mohu přidat do kolekce:
					final Field field = getFieldFromInstance(objInstance, variableName);
					
					final Object objValue = ReflectionHelper.getValueFromField(field, objInstance);
					
					return returnResult(true, objValue);
				}
			}
			
			
			// Abych nemusel psát další výraz, tak na konec přidám středník:
			else if (regEx.isIncrementClassVariableBehind(s + ";")) {
				// Zde se jedná o syntaxi: (-) referenceName.variable++ 
				
				// takže nejprve vrátím hodnotu a pak ji inkrementuji:
				
				// Zde mohu odebrat veškeré mezery, abych předešel nechtěným chybám či vyjímkám:
				final String commandWithoutSpace = s.replaceAll("\\s", "");
				
				// Nyní se jedná o syntaxi bez mezer:   (-) referenceName.variableName++;
				
				final boolean isCharAtFirstIndexMinus = isMinusAtFirstChar(s);
				
				final String referenceName;
				// Otestuji, zda je na začátku mínus:
				
				if (isCharAtFirstIndexMinus)
					// Zjistím si název reference - od začátku (za mínusem) po první tečku:
					referenceName = commandWithoutSpace.substring(1, commandWithoutSpace.indexOf('.'));
				// Zjistím si název reference - od začátku po první tečku:
				else referenceName = commandWithoutSpace.substring(0, commandWithoutSpace.indexOf('.'));
				
				
				// Název proměnné instance třídy - od první tečky po ++:
				final String variableName = commandWithoutSpace.substring(commandWithoutSpace.indexOf('.') + 1,
						commandWithoutSpace.indexOf("++"));
				
				
				// Najdu si instanci třídy dle názvu reference:
				final Object objInstance = Instances.getInstanceFromMap(referenceName);
				
				
				if (objInstance != null) {
					final Field field = getFieldFromInstance(objInstance, variableName);
					
					final Object objValue = ReflectionHelper.getValueFromField(field, objInstance);

					// Pokusím se inkrementovat hodnotu proměnné:
					getIncrementOrDecrementVariable(variableName, objInstance, true, isCharAtFirstIndexMinus);		
					
					return returnResult(true, objValue);
				}
				else outputEditor.addResult(txtReferenceNameNotFound + " - " + variableName + " = null");
			}
		}
		
		
		// Zde otestuji, zda se jedná dekrementaci proměnné v instanci třídy:
		else if (regEx.isDecrementClassVariable(s)) {
			// Zde se jedná buď o syntaxi: (-) referenceName.variable--    nebo: --referenceName.variable   
			
			if (regEx.isDecrementClassVariableBefore(s + ";")) {
				// Zde mohu odebrat veškeré mezery, abych předešel nechtěným chybám či vyjímkám:
				final String commandWithoutSpace = s.replaceAll("\\s", "");
				
				// Nyní se jedná o syntaxi bez mezer:   --referenceName.variableName;
				// Zjistím si název reference - od druhého - po první tečku:
				final String referenceName = commandWithoutSpace.substring(commandWithoutSpace.indexOf("--") + 2,
						commandWithoutSpace.indexOf('.'));
				
				
				// Název proměnné instance třídy - od první tečky po středník:
				final String variableName = commandWithoutSpace.substring(commandWithoutSpace.indexOf('.') + 1);
				
				
				// Najdu si instanci třídy dle názve reference:
				final Object objInstance = Instances.getInstanceFromMap(referenceName);
				
				// Pokusím se inkrementovat hodnotu proměnné:
				final String result = getIncrementOrDecrementVariable(variableName, objInstance, false, false);
				
				if (result.equals(txtSuccess)) {
					// Zde se podařilo inkrementovat hodnotu, tak ji mohu přidat do kolekce:
					final Field field = getFieldFromInstance(objInstance, variableName);
					
					final Object objValue = ReflectionHelper.getValueFromField(field, objInstance);
					
					return returnResult(true, objValue);
				}
			}
			
									
			else if (regEx.isDecrementClassVariableBehind(s + ";")) {
				// Zde mohu odebrat veškeré mezery, abych předešel nechtěným chybám či vyjímkám:
				final String commandWithoutSpace = s.replaceAll("\\s", "");
				
				// Nyní se jedná o syntaxi bez mezer:   (-) referenceName.variableName--;
				
				final boolean isCharAtFirstIndexMinus = isMinusAtFirstChar(s);
				
				final String referenceName;
				// Otestuji, zda je na začátku mínus:					
				
				if (isCharAtFirstIndexMinus)
					// Zjistím si název reference - od začátku (za prvním minusem) po první tečku:
					referenceName = commandWithoutSpace.substring(1, commandWithoutSpace.indexOf('.'));
				// Zjistím si název reference - od začátku po první tečku:
				else referenceName = commandWithoutSpace.substring(0, commandWithoutSpace.indexOf('.'));
				
				
				
				// Název proměnné instance třídy - od první tečky po --:
				final String variableName = commandWithoutSpace.substring(commandWithoutSpace.indexOf('.') + 1,
						commandWithoutSpace.indexOf("--"));
				
				
				// Najdu si instanci třídy dle názve reference:
				final Object objInstance = Instances.getInstanceFromMap(referenceName);
				
				
				if (objInstance != null) {
					final Field field = getFieldFromInstance(objInstance, variableName);
					
					final Object objValue = ReflectionHelper.getValueFromField(field, objInstance);
					
					// Pokusím se inkrementovat hodnotu proměnné:
					getIncrementOrDecrementVariable(variableName, objInstance, false, isCharAtFirstIndexMinus);
					
					return returnResult(true, objValue);
				}
				else outputEditor.addResult(txtReferenceNameNotFound + " - " + variableName + " = null");
			}
		}
		
		
		
		
		
		
		else if (regEx.isIncrementEditorVariable(s)) {
			
			if (regEx.isIncrementCommandEditorVariableBefore(s + ";")) {
				// Odeberu si všechny mezery:
				final String commandWithoutSpace = s.replaceAll("\\s", "");
				
				// Název proměnné: od druhého + do konce:
				final String variableName = commandWithoutSpace.substring(commandWithoutSpace.indexOf("++") + 2);


                final String result = incrementOrDecrementEditorVariable(variableName, true);
				
				if (result.equals(txtSuccess))
					// Zde se v pořádku provedla inkrementace, tak mohu vrátit inkrementovanou hodnotu:					
					return returnResult(true, ((Variable) Instances.getVariableFromMap(variableName)).getObjValue());				
			}
			
			
			else if (regEx.isIncrementCommandEditorVariableBehind(s + ";")) {
                final String[] variablesData = getEditorVariableDataFromCommand(s, true);

                final String variableName = variablesData[0];

                final boolean insertMinus = Boolean.parseBoolean(variablesData[1]);

                // Zde se jedná o inkrementaci hodnoty po použití, takže nejprve vrátím hodnotu (v tomto případně si
                // ji uložímt)
                // pak ji inkrementuji a pak vrátím tu uoženoiu hodnotu ještě před inkrementací:

                final Variable variable = (Variable) Instances.getVariableFromMap(variableName);

                if (variable != null) {
                    if (!variable.isFinal()) {
                        // Zde jsem našel proměnnou, tak si uložím její hodnotu:
                        Object objValue = variable.getObjValue();

                        // Otestuji, zda mám znegovat hodnotu:
                        if (insertMinus)
                            objValue = getNegationOfValue(variable.getDataTypeOfVariable(), objValue);

                        // Nyní ji mohu inkrementovat a pak vrátit výše uloženou hodnotu.
                        final String result = incrementOrDecrementEditorVariable(variableName, true);

                        if (result.equals(txtSuccess))
                            // Zde byla proměnná v pořádku inkrementována, tak mohu vrátit dříve uloženou hodnotu
                            // (hodnotu před inkrementací):
                            return returnResult(true, objValue);

                        // else - hlášky byly vypsány v metodě pro inkrementaci
                    } else outputEditor.addResult(txtFailure + ": " + txtVariable + ": '" + variableName + "' "
                            + txtValueIsFinalAndCannotBeChanged);
                } else
                    outputEditor.addResult(txtFailure + ": " + txtVariable + " '" + variableName + "' " + txtDoNotFound);

            }
        }
		
		
		else if (regEx.isDecrementEditorVariable(s)) {
			if (regEx.isDecrementCommandEditorVariableBefore(s + ";")) {
				// Odeberu si všechny mezery:
				
				final String commandWithoutSpace = s.replaceAll("\\s", "");
				
				// Název proměnné: od druhého - do konce:
				final String variableName = commandWithoutSpace.substring(commandWithoutSpace.indexOf("--") + 2);


                final String result = incrementOrDecrementEditorVariable(variableName, false);
				
				if (result.equals(txtSuccess))
					// Zde se podařilo dekrementovat hodnotu proměnné:					
					return returnResult(true, ((Variable) Instances.getVariableFromMap(variableName)).getObjValue());
				
			}


			else if (regEx.isDecrementCommandEditorVariableBehind(s + ";")) {
                final String[] variablesData = getEditorVariableDataFromCommand(s, false);

                final String variableName = variablesData[0];

                final boolean insertMinus = Boolean.parseBoolean(variablesData[1]);

                final Variable variable = (Variable) Instances.getVariableFromMap(variableName);

                if (variable != null) {
                    if (!variable.isFinal()) {
                        // Zde jsem našel proměnnou, tak si uložím její hodnotu:
                        Object objValue = variable.getObjValue();

                        // Otestuji, zda mám znegovat hodnotu:
                        if (insertMinus)
                            objValue = getNegationOfValue(variable.getDataTypeOfVariable(), objValue);

                        // Nyní ji mohu dekrementovat a pak vrátit výše uloženou hodnotu.
                        final String result = incrementOrDecrementEditorVariable(variableName, false);

                        if (result.equals(txtSuccess))
                            // Zde byla proměnná v pořádku inkrementována,
                            // tak mohu vrátit dříve uloženou hodnotu (hodnotu před inkrementací):
                            return returnResult(true, objValue);

                        // else - hlášky byly vypsány v metodě pro dekremetnaci
                    } else
                        outputEditor.addResult(txtFailure + ": " + txtVariable + ": '" + variableName + "' "
                                + txtValueIsFinalAndCannotBeChanged);
                } else
                    outputEditor.addResult(
                            txtFailure + ": " + txtVariable + " '" + variableName + "' " + txtDoNotFound);
            }
        }
		
		
		
		
		
		
		
		
		
		
		
		
		// Podmínka pro otestování, zda sejedná o získání hodnoty z jednorozměrného pole
		// v nějaké instanci třídy v diagramu instancí:
		else if (regEx.isGetValueFromArrayAtIndexInInstance(s + ";")) {
			/*
			 * Zjistím si, zda je na začátku mínus, protože pokud ano, pak potřebuji
			 * pracovat s příkazem bez mínusu, navíc až získám konečnou hodnotu z pole, pak
			 * jej musím znegovat (pokud to půjde, tj. bude se jednat o číselnou hodnotu).
			 */
			final boolean isMinus = isMinusAtFirstChar(s);
			
			/*
			 * Do této proěmnné si vložím hodnotu bez mínusu, pokud se nachází na začítku
			 * příkazu.
			 */
			final String editedCommand;

			if (isMinus)
				editedCommand = s.substring(s.indexOf('-') + 1);
			else
				editedCommand = s;
			
				
			/*
			 * Proměnná ukazující na instanci třídy: od začátku po první tečku:
			 */
			final String classNameVariable = editedCommand.substring(0, editedCommand.indexOf('.')).replaceAll("\\s",
					"");
			

			/*
			 * Proměnná, do které se má přiřadit hodnota: - od první desetinné tečky po
			 * první otevírací hranatou závorku:
			 */
			final String classVariable = editedCommand
					.substring(editedCommand.indexOf('.') + 1, editedCommand.indexOf('[')).replaceAll("\\s", "");
			
			
			/*
			 * Proměnná, do které se vloží index hodnoty v poli.
			 */
			final String indexInArray = editedCommand
					.substring(editedCommand.indexOf('[') + 1, editedCommand.lastIndexOf(']')).replaceAll("\\s", "");
			
			
			/*
			 * Zde si mohu index rovnou přetypovat na integer, protože vím, že se jedná o
			 * číslo, ale uživatel může například zadat číslo pro datový typ long apod. To
			 * je chyba.
			 */
			final int index;
			
			try {
				index = Integer.parseInt(indexInArray);
			} catch (NumberFormatException e) {
				outputEditor.addResult(
						txtCastingToIntegerFailure_1 + " '" + indexInArray + "' " + txtCastingToIntegerFailure_2);
				return returnResult(false, null);
			}
			
			
			
			/*
			 * Načtu si instanci pod zadanou referencí (pokud existuje).
			 */
			final Object objInstance1 = Instances.getInstanceFromMap(classNameVariable);
			
			
			
			if (objInstance1 == null) {
				outputEditor.addResult(txtInstanceOfClassInIdNotFOund + ": " + objInstance1);
				return returnResult(false, null);
			}

			
			
			/*
			 * Získám si položuk v zadané instanci pod zadaným jménem, zde ještě nevím, zda
			 * se jedná o jednorozměrné pole.
			 */
			final Field field = getFieldFromInstance(objInstance1, classVariable);

			if (field == null) {
				outputEditor.addResult(txtVariableNotFoundInReferenceToInstanceOfClass);
				return returnResult(false, null);
			}
				
			
			
			/*
			 * zde otestuji, zda se jedná o pole.
			 */
			if (!field.getType().isArray()) {
				outputEditor.addResult(txtEnteredVariable_1 + " '" + field.getName() + "' " + txtEnteredVariable_2);
				return returnResult(false, null);
			}
				
			
			
			/*
			 * Zde si ještě musím zjistit počet dimenzí, protože předchoí testování v
			 * případě, že je položka field pole vždy vrátí true, i v případě, že se jedná o
			 * například 4 - rozměrné pole apod.
			 */
			final int countOfDims = getCountOfDimensionsOfArray(field.getType());
			
			if (countOfDims != 1) {
				outputEditor.addResult(txtVariable + " '" + field.getName() + "' " + txtIsNotOneDimArray);
				return returnResult(false, null);
			}
					
			
			
			
			if (variableDataType != null
					&& !field.getType().getComponentType().toString().equals(variableDataType.toString())) {
				outputEditor.addResult(txtDataTypesAreNotEqual + ": " + field.getType().getComponentType() + " != "
						+ variableDataType);
				return returnResult(false, null);
			}
				
			
			
			
			/*
			 * Zde si získám hodnotu v zadané proměnné - zadané pole.
			 */
			final Object objArrayValue = ReflectionHelper.getValueFromField(field, objInstance1);
			
			if (objArrayValue == null)
				// Nemusí být viditelné apod., ale to se již vypsalo:
				return returnResult(false, null);
			
			/*
			 * Zde si zjistím velikost příslušného pole.
			 */
			final int arrayLength = Array.getLength(objArrayValue);
			
			if (index >= arrayLength) {
				outputEditor.addResult(txtIndexValueAtArrayIsNotInInterval + ": <0 ; " + arrayLength + ").");
				return returnResult(false, null);
			}
				
			
			
			/*
			 * Hískám si hodnotu z pole na zadaném indexu:
			 */
			final Object objValueAtindex = Array.get(objArrayValue, index);

			/*
			 * Zde ještě otestuji, zda se má hodnota znegovat, pokud ano, pak ji zneguji a
			 * poté vrátím, jinak pouze vrátím bez negace příslušné hodnoty.
			 */
			if (isMinus) {
				final Object negativeValue = getNegationOfValue(field.getType().getComponentType(), objValueAtindex);

				return returnResult(true, negativeValue);
			}
			
			/*
			 * Zde mohu vrátít příslušnou hodnotu, nemá se znegovat, pokud by to šlo.
			 */
			return returnResult(true, objValueAtindex);
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		// Podmínka pro získání velikost / délky pole, které se nachází v instanci třídy
		// v diagramu instancí:
		else if (regEx.isGetLengthOfArrayInInstance(s + ";")) {
			// Syntaxe: referenceName.variableArrayName.length;
			
			/*
			 * Zjistím si, zda se má hodnota znegovat, tj., zda se na začátku příkazu
			 * nachází mínus.
			 */
			final boolean isMinus = isMinusAtFirstChar(s);
			
			/*
			 * Do této proměnné si vložím celý zadaný příkaz od uživatele, ale pouze bez
			 * mínusu na začátku příkazu - pokud je tam uveden pro negaci hodnoty.
			 */
			final String commandEdited;

			if (isMinus)
				commandEdited = s.substring(s.indexOf('-') + 1);
			else
				commandEdited = s;
			
			/*
			 * Části příkazu rozdělené dle desetinné tečky.
			 */
			final String[] partsOfCommand = commandEdited.split("\\.");

			/*
			 * Název reference na instanci třídy, reference, která má ukazovat na příslušnou
			 * instanci v diagramu instancí.
			 */
			final String referenceName = partsOfCommand[0].replaceAll("\\s", "");

			/*
			 * Název proměnné, která má být typu X - rozměrné pole.
			 */
			final String fieldName = partsOfCommand[1].replaceAll("\\s", "");

			
			
			
			/*
			 * Načtu si instanci pod zadanou referencí (pokud existuje).
			 */
			final Object objInstance1 = Instances.getInstanceFromMap(referenceName);

			if (objInstance1 == null) {
				outputEditor.addResult(txtInstanceOfClassInIdNotFOund + ": " + objInstance1);
				return returnResult(false, null);
			}


			
			
			
			/*
			 * Získám si položuk v zadané instanci pod zadaným jménem, zde ještě nevím, zda
			 * se jedná o jednorozměrné pole.
			 */
			final Field field = getFieldFromInstance(objInstance1, fieldName);

			if (field == null) {				
				outputEditor.addResult(txtVariableNotFoundInReferenceToInstanceOfClass);
				return returnResult(false, null);
			}

				

			
			
			/*
			 * zde otestuji, zda se jedná o pole.
			 */
			if (!field.getType().isArray()) {
				outputEditor.addResult(txtEnteredVariable_1 + " '" + field.getName() + "' " + txtEnteredVariable_2);
				return returnResult(false, null);
			}

			
			
			
			/*
			 * Zde si získám hodnotu v zadané proměnné - zadané pole.
			 */
			final Object objArrayValue = ReflectionHelper.getValueFromField(field, objInstance1);

			if (objArrayValue == null)
				// Nemusí být viditelné apod., ale to se již vypsalo:
				return returnResult(false, null);

			
			/*
			 * Zde potřebuji otestovat, zda se má vrátit daotvý typ int, pokud ano, tak jej
			 * vrátím, pokud ne, vypíšu hlášení o chybě a mohu skončit.
			 * 
			 * Note:
			 * Ještě by zde bylo možná dobré doplnit testování, jestli se v proměnné
			 * variableDataType nachází celočíselný datový typ adpo. a dle toho například
			 * přetypovat velikost pole na příslušnou hodnotu, ale to už zde řešit nebudu,
			 * nenapadají mě konkrétní příklady kdy se to může a kdy ne, většinou by
			 * uživatel musel sám zadanou hodnotu přetypovat, což tato aplikace nepodporuje,
			 * takže nechám nadále testování konkrétních datových typů.
			 */
			if (variableDataType != null && !int.class.equals(variableDataType)) {
				outputEditor.addResult(txtDataTypesAreNotEqual + ": " + int.class + " != " + variableDataType);
				return returnResult(false, null);
			}
			
			
			
			
			/*
			 * Zde si zjistím velikost příslušného pole.
			 */
			int arrayLength = Array.getLength(objArrayValue);
			
			/*
			 * Zde ještě otestuji, zda se má hodnota znegovat.
			 */
			if (isMinus)
				arrayLength = (int) getNegationOfValue(int.class, arrayLength);

			// Zde vrátím Úspěch s příslušnou hodnotou:
			return returnResult(true, arrayLength);
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		// Zda se jedná o syntaxi pro inkrementaci položky v jednorozměrném poli v
		// instanci třídy v prefixové formě:
		else if (regEx.isPrefixIncrementValueAtArrayInInstance(s + ";")) {
			// Syntaxe: ++ referenceName.variableArrayName[index]
			
			/*
			 * Název reference na instanci třídy je od posledního plusu po první desetinnou
			 * tečku.
			 */
			final String referenceName = s.substring(s.lastIndexOf('+') + 1, s.indexOf('.')).replaceAll("\\s", "");

			/*
			 * Zavolám metodu, která rozezná potřebné hodnoty a inkrementuje hodnotu v poli
			 * na přílsušném indexu a vrátí jej
			 */
			return incrementDecrementValueAtIndexInArrayInInstance(referenceName, variableDataType, s, true);
		}

		
		
		
		// Zda se jedná o syntaxi pro dekrementaci položky v jednorozměrném poli v
		// instanci třídy v prefixové formě:
		else if (regEx.isPrefixDecrementValueAtArrayInInstance(s + ";")) {
			// Syntaxe: -- referenceName.variableArrayName[index]	
			
			/*
			 * Název reference na instanci třídy je od posledního plusu po první desetinnou
			 * tečku.
			 */
			final String referenceName = s.substring(s.lastIndexOf('-') + 1, s.indexOf('.')).replaceAll("\\s", "");

			/*
			 * Zavolám metodu, která rozezná potřebné hodnoty a dekrementuje hodnotu v poli
			 * na přílsušném indexu a vrátí jej
			 */
			return incrementDecrementValueAtIndexInArrayInInstance(referenceName, variableDataType, s, false);
		}
		
		
		
		
		
		
		
		
		
		
		
		// Postfixová inkrementace položky v jednorozměrném poli v instanci třídy,
		// syntaxe: (-) refer.var[index] ++
		else if (regEx.isPostFixIncrementWithMinus(s))
			/*
			 * Syntaxe: (-) reference.variableName[index] ++
			 * 
			 * Získám si hodnotu z pole, případně, poud je na začátku mínus, tak ji zneguji
			 * a vrátím.
			 * 
			 * Dále se načtená hodnota z pole inkrementuje a nastaví zpět do pole.
			 * 
			 * Toto vše pouze za podmínky, že se jedná o číselný datový typ, jinak se v
			 * podstatě nic nestane, akorát se na konec vrátí null s informací, že se
			 * nepovedla operace.
			 */
			return incrementDecrementValueInArrayAtIndexInInstance(s, variableDataType, true);
		
		
		
		
		
		
		// Postfixová dekrementace položky v jednorozměrném poli v instanci třídy,
		// syntaxe: (-) refer.var[index] --
		else if (regEx.isPostFixDecrementWithMinus(s))
			/*
			 * Syntaxe: (-) reference.variableName[index] --
			 * 
			 * Získám si hodnotu z pole, případně, poud je na začátku mínus, tak ji zneguji
			 * a vrátím.
			 * 
			 * Dále se načtená hodnota z pole dekrementuje a nastaví zpět do pole.
			 * 
			 * Toto vše pouze za podmínky, že se jedná o číselný datový typ, jinak se v
			 * podstatě nic nestane, akorát se na konec vrátí null s informací, že se
			 * nepovedla operace.
			 */
			return incrementDecrementValueInArrayAtIndexInInstance(s, variableDataType, false);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		else if (regEx.isGetValueAtIndexAtTwoDimArrayInIntanceWithMinus(s)) {
			// Syntaxe: referenceName . variableArrayname [index] [index];
			
			/*
			 * Zjistím si, zda je na začátku mínus, protože pokud ano, pak potřebuji
			 * pracovat s příkazem bez mínusu, navíc až získám konečnou hodnotu z pole, pak
			 * jej musím znegovat (pokud to půjde, tj. bude se jednat o číselnou hodnotu).
			 */
			final boolean isMinus = isMinusAtFirstChar(s);

			/*
			 * Do této proěmnné si vložím hodnotu bez mínusu, pokud se nachází na začítku
			 * příkazu.
			 */
			final String editedCommand;

			if (isMinus)
				editedCommand = s.substring(s.indexOf('-') + 1);
			else
				editedCommand = s;
			
			/*
			 * Proměnná ukazující na instanci třídy: od začátku po první tečku:
			 */
			final String classNameVariable = editedCommand.substring(0, editedCommand.indexOf('.')).replaceAll("\\s",
					"");

			/*
			 * Proměnná, do které se má přiřadit hodnota: - od první desetinné tečky po
			 * první otevírací hranatou závorku:
			 */
			final String classVariable = editedCommand
					.substring(editedCommand.indexOf('.') + 1, editedCommand.indexOf('[')).replaceAll("\\s", "");
			
			
			/*
			 * Proměnná, do které se vloží první index hodnoty v poli. Ten je mezi první
			 * otevírací a první zavíracá hranatou závorkou:
			 */
			final String indexInArray_1 = editedCommand
					.substring(editedCommand.indexOf('[') + 1, editedCommand.indexOf(']')).replaceAll("\\s", "");
			
			

			/*
			 * Proměnná, do které se vloží druhý index hodnoty v poli, ten se nachází mezi
			 * druhou, resp. poslední otevírací a druhou, resp. poslední zavírací závorkou.
			 */
			final String indexInArray_2 = editedCommand
					.substring(editedCommand.lastIndexOf('[') + 1, editedCommand.lastIndexOf(']'))
					.replaceAll("\\s", "");
			
			
			
			
			/*
			 * Zde si mohu index rovnou přetypovat na integer, protože vím, že se jedná o
			 * číslo, ale uživatel může například zadat číslo pro datový typ long apod. To
			 * je chyba.
			 */
			final int index_1, index_2;

			try {
				index_1 = Integer.parseInt(indexInArray_1);
			} catch (NumberFormatException e) {
				outputEditor.addResult(
						txtCastingToIntegerFailure_1 + " '" + indexInArray_1 + "' " + txtCastingToIntegerFailure_2);

				return returnResult(false, null);
			}
			
			
			try {
				/*
				 * Zde zkusím přetypovat i druhý index položky v poli, ale v jiném bloku
				 * try-catch, abych věděl, v jakém indexu je problém.
				 */
				index_2 = Integer.parseInt(indexInArray_2);
			} catch (NumberFormatException e) {
				outputEditor.addResult(
						txtCastingToIntegerFailure_1 + " '" + indexInArray_2 + "' " + txtCastingToIntegerFailure_2);

				return returnResult(false, null);
			}
			
			
			
			/*
			 * Načtu si instanci pod zadanou referencí (pokud existuje).
			 */
			final Object objInstance = Instances.getInstanceFromMap(classNameVariable);

			if (objInstance == null) {
				outputEditor.addResult(txtInstanceOfClassInIdNotFOund + ": " + classNameVariable);

				return returnResult(false, null);
			}
				
			
			
			/*
			 * Získám si položuk v zadané instanci pod zadaným jménem, zde ještě nevím, zda
			 * se jedná o jednorozměrné pole.
			 */
			final Field field = getFieldFromInstance(objInstance, classVariable);

			if (field == null) {
				outputEditor.addResult(txtVariableNotFoundInReferenceToInstanceOfClass);

				return returnResult(false, null);
			}
				
			
			
			/*
			 * zde otestuji, zda se jedná o pole.
			 */
			if (!field.getType().isArray()) {
				outputEditor.addResult(txtEnteredVariable_1 + " '" + field.getName() + "' " + txtEnteredVariable_2);

				return returnResult(false, null);
			}
			
			
			
			
			
			/*
			 * Získám si datový typ pole:
			 */
			final Class<?> dataTypeOfArray = ReflectionHelper.getDataTypeOfArray(field.getType());
			
			/*
			 * Zjistím si, zda se mají otestovat datové typy, pokud ano, pak potřebuji
			 * otestovat datový typ pole, protože se jedná o datový typ položky, která bude
			 * vrácena.
			 */
			if (variableDataType != null && !dataTypeOfArray.toString().equals(variableDataType.toString())) {
				outputEditor.addResult(txtDataTypesAreNotEqual + ": " + dataTypeOfArray + " != " + variableDataType);
				return returnResult(false, null);
			}
			
			
			
			
			
			
			/*
			 * Zde si ještě musím zjistit počet dimenzí, protože předchoí testování v
			 * případě, že je položka field pole vždy vrátí true, i v případě, že se jedná o
			 * například 4 - rozměrné pole apod.
			 */
			final int countOfDims = getCountOfDimensionsOfArray(field.getType());

			if (countOfDims != 2) {
				outputEditor.addResult(txtVariable + " '" + field.getName() + "' " + txtIsNotTwoDimArray);

				return returnResult(false, null);
			}
				
			
			
			
			
			/*
			 * Zde si získám hodnotu v zadané proměnné - zadané pole.
			 */
			final Object objArrayValue = ReflectionHelper.getValueFromField(field, objInstance);

			if (objArrayValue == null)
				// Nemusí být viditelné apod., ale to se již vypsalo:
				return returnResult(false, null);
			
			
			
			
			/*
			 * Nyní budu muset projít celé pole a získat hodnotu na zadaném indexu
			 */
			final ResultValue resultValue = getValueAtIndexInTwoDimArray(objArrayValue, index_1, index_2);

			if (!resultValue.isExecuted()) {
				outputEditor.addResult(txtFailedToGetValueFromTwoDimArray + OutputEditor.NEW_LINE_TWO_GAPS
						+ txtIndex_1Text + ": '" + index_1 + "', " + txtIndex_2Text + ": '" + index_2 + "'.");

				return returnResult(false, null);
			}

			
			
			/*
			 * Zde ještě otestuji, zda se má hodnota znegovat, pokud ano, pak ji zneguji a
			 * poté vrátím, jinak pouze vrátím bez negace příslušné hodnoty.
			 */
			if (isMinus) {
				final Object negativeValue = getNegationOfValue(dataTypeOfArray, resultValue.getObjResult());

				return returnResult(true, negativeValue);
			}


			
			/*
			 * Zde už mohu vrátit nalezenou hodnotu, ať už je to null nebo cokoliv jiného,
			 * podařilo se získat hodnotu na zadaném indexu:
			 */
			return returnResult(true, resultValue.getObjResult());
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		// Inkrementace položky na indecu ve dvourozměrném poli v instanci třídy v
		// prefixové formě:
		else if (regEx.isPrefixIncrementValueAtIndexInTwoDimArrayInInstance(s + ";")) {
			// Syntaxe: ++ referenceName . variableArrayName [index] [index];

			/*
			 * Název reference na instanci třídy je od posledního plusu po první desetinnou
			 * tečku.
			 */
			final String referenceName = s.substring(s.lastIndexOf('+') + 1, s.indexOf('.')).replaceAll("\\s", "");

			/*
			 * Zavolám metodu, která rozezná potřebné hodnoty a inkrementuje hodnotu v poli
			 * na přílsušném indexu a vrátí jej
			 */
			return incrementDecrementValueAtIndexInTwoDimArrayInInstance(referenceName, variableDataType, s, true);
		}
		
		
		
		// Dekrementace položky na indecu ve dvourozměrném poli v instanci třídy v
		// prefixové formě:
		else if (regEx.isPrefixDecrementValueAtIndexInTwoDimArrayInInstance(s + ";")) {
			// Syntaxe: -- referenceName . variableArrayName [index] [index];

			/*
			 * Název reference na instanci třídy je od posledního plusu po první desetinnou
			 * tečku.
			 */
			final String referenceName = s.substring(s.lastIndexOf('-') + 1, s.indexOf('.')).replaceAll("\\s", "");

			/*
			 * Zavolám metodu, která rozezná potřebné hodnoty a inkrementuje hodnotu v poli
			 * na přílsušném indexu a vrátí jej
			 */
			return incrementDecrementValueAtIndexInTwoDimArrayInInstance(referenceName, variableDataType, s, false);
		}
		
		
		
		
		
		
		

		
		
		
		
		
		
		
		/*
		 * Inkrementace hodnoty na zadaném indexu ve dvourozměrném poli v postfixové
		 * formě v instanci nějaké třídy.
		 */
		else if (regEx.isPostFixIncrementValueatIndexInTwoDimarrayInInstanceWithMinus(s))
			/*
			 * Syntaxe: (-) reference.variableName[index] [index] ++
			 * 
			 * Získám si hodnotu z pole, případně, poud je na začátku mínus, tak ji zneguji
			 * a vrátím.
			 * 
			 * Dále se načtená hodnota z pole inkrementuje a nastaví zpět do pole.
			 * 
			 * Toto vše pouze za podmínky, že se jedná o číselný datový typ, jinak se v
			 * podstatě nic nestane, akorát se na konec vrátí null s informací, že se
			 * nepovedla operace.
			 */
			return incrementDecrementValueInTwoDimArrayAtIndexInInstance(s, variableDataType, true);
		
		
		
		/*
		 * Dekrementace hodnoty na zadaném indexu ve dvourozměrném poli v postfixové
		 * formě v instanci nějaké třídy.
		 */
		else if (regEx.isPostFixDecrementValueatIndexInTwoDimarrayInInstanceWithMinus(s))
			/*
			 * Syntaxe: (-) reference.variableName[index] [index] ++
			 * 
			 * Získám si hodnotu z pole, případně, poud je na začátku mínus, tak ji zneguji
			 * a vrátím.
			 * 
			 * Dále se načtená hodnota z pole inkrementuje a nastaví zpět do pole.
			 * 
			 * Toto vše pouze za podmínky, že se jedná o číselný datový typ, jinak se v
			 * podstatě nic nestane, akorát se na konec vrátí null s informací, že se
			 * nepovedla operace.
			 */
			return incrementDecrementValueInTwoDimArrayAtIndexInInstance(s, variableDataType, false);

		
		
		
		

		
		
		
		
		
		
		
		
		
		
		
		
		

		/*
		 * Nyní otestuji, zda se jedná o nějaký matematický výraz, který by se mohl
		 * vypočítat.
		 */
		// Původní podmínka - OK, ale odebráno testování matematického nebo logického
		// operátoru, kvůli nové knihovně -> například funkce min(1, 2, 3) apod.
//		else if (regEx.isMathematicalExpression(s.replaceAll("\\s", "")) && Editor.containsNumber(s)
//				&& Editor.checkMathOperands(s)) {
			// Nová podmínka pro novou knihovnu:
		else if ((regEx.isMathematicalExpression(s.replaceAll("\\s", "")) && Editor.containsNumber(s))) {
			
			/*
			 * Zde se jedná o nějaký matematický výraz, tak si pro začátek zkusím zjistit,
			 * zda obsahuje nějaké textové proměnné - nějaká písmena, pokud ano, tak zkusím
			 * zjistit, zda se jedná o "dočasné" proměnné vytvořené uživatelem v editoru
			 * příkazů, pokud ano, zjistím, zda obsahují čísla a pokud jsou tyto podmínky
			 * splněny, pak mohu za tyto proměnné doplnit příslušná čísla z daných
			 * proměnných a vypočítat příslušný výraz.
			 * 
			 * Pokud nebudou nalezeny žádné textové proměnné, pak mohu výraz vypočítat, nebo
			 * pokud budou nalezeny proměnné, ale nejedná se o vytvořeé proměnné z editoru
			 * příkazů, například sqrt pro odmocninu a další funkce (například
			 * goniometrické).
			 */
			

			/*
			 * Pro začátek si vytvořím výraz, který by seměl vypočítat, abych do měl
			 * případně mohl rovnou vkládat hodnoty za proměnné (následující cyklus).
			 */
			final Expression expression = new Expression(s);
			
			
			
			/*
			 * Získám si list, který obsahuje veškeré textové proměnné, a zjistím, zda se
			 * jedná o proměnné z editoru příkazů, případně si načtu jejich hodnoty.
			 */
			final List<String> variables = getGroupsOfText(s);
			
			
			// Vložím do výše vytvořeného výrazu proměnné vytvořené v editoru příkazů, které
			// se rozpoznají:
			setVariablesToExpression(expression, variables);
			
			
			
			/*
			 * Nyní jsem v pozici, kdy se mohu pokusit vypočítat zadaný výraz, tak nad
			 * výrazem zavolám metodu eval a pokud se výpočet povede, tak otestuji datový
			 * typ, na který se to má převést a převedu na něj výsledek. Pokud dojde k
			 * nějakém chybě, npříklad chybí nebo přebývá závorka, nebo nebyla specifikována
			 * proměnná atd. Pak vrátím null hodontu s tím, že se nepovedl výpočet.
			 */
			try {
				final BigDecimal result = expression.eval();
				
				final Object objResultOfExponent;
				
				if (variableDataType != null) {
					if (variableDataType.equals(Double.class) || variableDataType.equals(double.class)
							|| variableDataType.equals(Float.class) || variableDataType.equals(float.class)) {

						// Zde se jedná o desetinné číslo, tak to stačí přetypovat:
						objResultOfExponent = getParsedParameter(variableDataType, String.valueOf(result.floatValue()));
					}
					
					
					else {
						// Zde se jedná o celé číslo, tak musím výsledek převést na celé číslo, jelikož
						// nechci psát podmínku
						// pro každý typ, tak stačí vzít z prměnné datový typ long, a ten lze již na
						// ostatní datové typy přetypovat (číselné typy)
						final Long l = result.longValue();
						objResultOfExponent = getParsedParameter(variableDataType, l.toString());
					}									
				}
								
					// Zde není uveden datový typ, takže chci jen získat hodnotu:
				else objResultOfExponent = result;
				
				
				if (objResultOfExponent != null)
					return returnResult(true, objResultOfExponent);
				
			} catch (ExpressionException e) {				
				outputEditor.addResult(txtFailedToCalculateMathTerm_1 + ": " + e.getMessage()
						+ OutputEditor.NEW_LINE_TWO_GAPS + txtFailedToCalculateMathTerm_2 + ": " + s);
				
				return returnResult(false, null);
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		// Nebyla rozpoznána operace (může nastat, například pokud se budou "míchat"
		// aritmeticé operae - +, -, *, ...):
		else
			outputEditor.addResult(txtValueWasNotRecognized + OutputEditor.NEW_LINE_TWO_GAPS + txtError + ": " + s);

		return returnResult(false, null);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro inkrementaci / dekrementaci hodnoty v poli v
	 * instanci třídy na zadaném indexu v prefixové formě, takže se získá hodnota z
	 * pole na zadaném indexu, ta se inkrementuje / dekrementuje a pak nastaví zpět
	 * do pole na původní pozici a zároveň se i tato inkrementovaná / dekrementovaná
	 * hodnota vrátí.
	 * 
	 * @param referenceName
	 *            - název reference na instanci třídy, která se nachází v diagramu
	 *            instancí a která obsahuje příslušné pole.
	 * 
	 * @param variableDataType
	 *            - datový typ hodnoty, která by se měla vrátit.
	 * 
	 * @param s
	 *            - část příkazu se syntaxí pro inkrementaci nebo dekrementaci pole,
	 *            například se může jednat o část příkazu pro naplnění proměnné
	 *            apod.
	 * 
	 * @param increment
	 *            - logická proměnná, která značí, zda se má hodnota v poli
	 *            inkrementovat nebo dekrementovat. Tru značí, že se mí hodnota
	 *            inkrementovat, false dekrementovat.
	 * 
	 * @return inkrementovanou nebo dekrementovanou hodoutu v poli na zadaném indexu
	 *         případně null, pokud někde dojde k nějaké chybě.
	 */
	private ResultValue incrementDecrementValueAtIndexInArrayInInstance(final String referenceName,
			final Class<?> variableDataType, final String s, final boolean increment) {
		/*
		 * Název jednorozměrného pole je za první desetinnou tečkou až po první
		 * otevírací hranatou závorku.
		 */
		final String arrayName = s.substring(s.indexOf('.') + 1, s.indexOf('[')).replaceAll("\\s", "");

		/*
		 * Index položky v poli se nachází mezi hranatými závorkami:
		 */
		final String indexInArray = s.substring(s.indexOf('[') + 1, s.indexOf(']')).replaceAll("\\s", "");
		
		
		/*
		 * Index, do kterého si vložím zadaný index v příkazu, ale naparsovaný na datový
		 * typ int.
		 */
		final int index;
		try {
			index = Integer.parseInt(indexInArray);
		} catch (NumberFormatException e) {
			outputEditor.addResult(
					txtCastingToIntegerFailure_1 + " '" + indexInArray + "' " + txtCastingToIntegerFailure_2);
			return returnResult(false, null);
		}
		
		
		/*
		 * Načtu si instanci pod zadanou referencí (pokud existuje).
		 */
		final Object objInstance = Instances.getInstanceFromMap(referenceName);

		if (objInstance == null) {
			outputEditor.addResult(txtInstanceOfClassInIdNotFOund + ": " + referenceName);
			return returnResult(false, null);
		}

		
		/*
		 * Získám si položuk v zadané instanci pod zadaným jménem, zde ještě nevím, zda
		 * se jedná o jednorozměrné pole.
		 */
		final Field field = getFieldFromInstance(objInstance, arrayName);

		if (field == null) {
			outputEditor.addResult(txtVariableNotFoundInReferenceToInstanceOfClass);
			return returnResult(false, null);
		}

		/*
		 * zde otestuji, zda se jedná o pole.
		 */
		if (!field.getType().isArray()) {
			outputEditor.addResult(txtEnteredVariable_1 + " '" + field.getName() + "' " + txtEnteredVariable_2);
			return returnResult(false, null);
		}

		/*
		 * Zde si ještě musím zjistit počet dimenzí, protože předchoí testování v
		 * případě, že je položka field pole vždy vrátí true, i v případě, že se jedná o
		 * například 4 - rozměrné pole apod.
		 */
		final int countOfDims = getCountOfDimensionsOfArray(field.getType());

		if (countOfDims != 1) {
			outputEditor.addResult(txtVariable + " '" + field.getName() + "' " + txtIsNotOneDimArray);
			return returnResult(false, null);
		}

		/*
		 * Zde si získám hodnotu v zadané proměnné - zadané pole.
		 */
		final Object objArrayValue = ReflectionHelper.getValueFromField(field, objInstance);

		if (objArrayValue == null)
			// Nemusí být viditelné apod., ale to se již vypsalo:
			// return txtFailure;
			return returnResult(false, null);

		
		/*
		 * Zde si zjistím velikost příslušného pole.
		 */
		final int arrayLength = Array.getLength(objArrayValue);

		if (index >= arrayLength) {
			outputEditor.addResult(txtIndexValueAtArrayIsNotInInterval + ": <0 ; " + arrayLength + ").");
			return returnResult(false, null);
		}
		

		/*
		 * Hískám si hodnotu z pole na zadaném indexu:
		 */
		Object objValueAtindex = Array.get(objArrayValue, index);

		// S null hodnotou nelze nic dělat:
		if (objValueAtindex == null)
			return returnResult(false, null);

		
		/*
		 * Zde otestuji, zda se hodnota z pole, resp. datový typ pole shoduje s
		 * hodnotou, která se má vrátit, tedy v přípdě, že se mají datové typy hodnot
		 * otestovat.
		 */
		if (variableDataType != null
				&& !field.getType().getComponentType().toString().equals(variableDataType.toString())) {
			outputEditor.addResult(
					txtDataTypesAreNotEqual + ": " + field.getType().getComponentType() + " != " + variableDataType);
			return returnResult(false, null);
		}
		
		
		
		/*
		 * Zde inkrementuji nebo dekrementuji získanou hodnotu - pokud se jedná o číslo,
		 * pokud ne, pak se hodnota nezmění, a vrátí se null, tak nebudu nic dělat, tj.
		 * nechám v tom poli původní hodnotu.
		 */
		final Object editedValue = getIncrementDecrementValue(objValueAtindex, increment);

		if (editedValue != null)
			// Zde se hodnotu podařilo inkrementovat / dekrementovat, tak ji natavím:
			Array.set(objArrayValue, index, editedValue);


		// Vrátím získanou hodnotu z pole, již inkrementovanou / dekrementovanou:
		return returnResult(true, editedValue);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro inkrementaci / dekrementaci hodnoty v dvourozměrném
	 * poli v instanci třídy na zadaném indexu v prefixové formě, takže se získá
	 * hodnota z pole na zadaném indexu, ta se inkrementuje / dekrementuje a pak
	 * nastaví zpět do pole na původní pozici a zároveň se i tato inkrementovaná /
	 * dekrementovaná hodnota vrátí.
	 * 
	 * @param referenceName
	 *            - název reference na instanci třídy, která se nachází v diagramu
	 *            instancí a která obsahuje příslušné pole.
	 * 
	 * @param variableDataType
	 *            - datový typ hodnoty, která by se měla vrátit.
	 * 
	 * @param s
	 *            - část příkazu se syntaxí pro inkrementaci nebo dekrementaci pole,
	 *            například se může jednat o část příkazu pro naplnění proměnné
	 *            apod.
	 * 
	 * @param increment
	 *            - logická proměnná, která značí, zda se má hodnota v poli
	 *            inkrementovat nebo dekrementovat. Tru značí, že se mí hodnota
	 *            inkrementovat, false dekrementovat.
	 * 
	 * @return inkrementovanou nebo dekrementovanou hodoutu v poli na zadaném indexu
	 *         případně null, pokud někde dojde k nějaké chybě.
	 */
	private ResultValue incrementDecrementValueAtIndexInTwoDimArrayInInstance(final String referenceName,
			final Class<?> variableDataType, final String s, final boolean increment) {
		/*
		 * Název jednorozměrného pole je za první desetinnou tečkou až po první
		 * otevírací hranatou závorku.
		 */
		final String arrayName = s.substring(s.indexOf('.') + 1, s.indexOf('[')).replaceAll("\\s", "");

		/*
		 * Proměnná, do které se vloží první index hodnoty v poli. Ten je mezi první
		 * otevírací a první zavíracá hranatou závorkou:
		 */
		final String indexInArray_1 = s.substring(s.indexOf('[') + 1, s.indexOf(']')).replaceAll("\\s", "");

		/*
		 * Proměnná, do které se vloží druhý index hodnoty v poli, ten se nachází mezi
		 * druhou, resp. poslední otevírací a druhou, resp. poslední zavírací závorkou.
		 */
		final String indexInArray_2 = s.substring(s.lastIndexOf('[') + 1, s.lastIndexOf(']')).replaceAll("\\s", "");
		
		
		
		
		/*
		 * Zde si mohu index rovnou přetypovat na integer, protože vím, že se jedná o
		 * číslo, ale uživatel může například zadat číslo pro datový typ long apod. To
		 * je chyba.
		 */
		final int index_1, index_2;

		try {
			index_1 = Integer.parseInt(indexInArray_1);
		} catch (NumberFormatException e) {
			outputEditor.addResult(
					txtCastingToIntegerFailure_1 + " '" + indexInArray_1 + "' " + txtCastingToIntegerFailure_2);

			return returnResult(false, null);
		}
		
		
		try {
			/*
			 * Zde zkusím přetypovat i druhý index položky v poli, ale v jiném bloku
			 * try-catch, abych věděl, v jakém indexu je problém.
			 */
			index_2 = Integer.parseInt(indexInArray_2);
		} catch (NumberFormatException e) {
			outputEditor.addResult(
					txtCastingToIntegerFailure_1 + " '" + indexInArray_2 + "' " + txtCastingToIntegerFailure_2);

			return returnResult(false, null);
		}
		
		
		/*
		 * Načtu si instanci pod zadanou referencí (pokud existuje).
		 */
		final Object objInstance = Instances.getInstanceFromMap(referenceName);

		if (objInstance == null) {
			outputEditor.addResult(txtInstanceOfClassInIdNotFOund + ": " + referenceName);
			return returnResult(false, null);
		}

		
		/*
		 * Získám si položuk v zadané instanci pod zadaným jménem, zde ještě nevím, zda
		 * se jedná o jednorozměrné pole.
		 */
		final Field field = getFieldFromInstance(objInstance, arrayName);

		if (field == null) {
			outputEditor.addResult(txtVariableNotFoundInReferenceToInstanceOfClass);
			return returnResult(false, null);
		}

		/*
		 * zde otestuji, zda se jedná o pole.
		 */
		if (!field.getType().isArray()) {
			outputEditor.addResult(txtEnteredVariable_1 + " '" + field.getName() + "' " + txtEnteredVariable_2);
			return returnResult(false, null);
		}
		
		
		
		/*
		 * Zde si ještě musím zjistit počet dimenzí, protože předchoí testování v
		 * případě, že je položka field pole vždy vrátí true, i v případě, že se jedná o
		 * například 4 - rozměrné pole apod.
		 */
		final int countOfDims = getCountOfDimensionsOfArray(field.getType());

		if (countOfDims != 2) {
			outputEditor.addResult(txtVariable + " '" + field.getName() + "' " + txtIsNotTwoDimArray);

			return returnResult(false, null);
		}

			

		
		
		
		/*
		 * Zde si získám hodnotu v zadané proměnné - zadané pole.
		 */
		final Object objArrayValue = ReflectionHelper.getValueFromField(field, objInstance);

		if (objArrayValue == null)
			// Nemusí být viditelné apod., ale to se již vypsalo:
			// return txtFailure;
			return returnResult(false, null);
		

		
		/*
		 * Zjistím si datový typ pole:
		 */
		final Class<?> dataTypeOfArray = ReflectionHelper.getDataTypeOfArray(field.getType());
		
		/*
		 * Zde otestuji, zda se hodnota z pole, resp. datový typ pole shoduje s
		 * hodnotou, která se má vrátit, tedy v přípdě, že se mají datové typy hodnot
		 * otestovat.
		 */
		if (variableDataType != null && !dataTypeOfArray.toString().equals(variableDataType.toString())) {
			outputEditor.addResult(txtDataTypesAreNotEqual + ": " + dataTypeOfArray + " != " + variableDataType);
			return returnResult(false, null);
		}
		
		
		
		
		
		
		
		/*
		 * Zde inkrementuji nebo dekrementuji získanou hodnotu - pokud se jedná o číslo,
		 * pokud ne, pak se hodnota nezmění, a vrátí se null, tak nebudu nic dělat, tj.
		 * nechám v tom poli původní hodnotu.
		 */
		final ResultValue editedValue = getIncrementedDecrementedValueAtIndexInTwoDimArray(objArrayValue, index_1,
				index_2, increment);
		

		/*
		 * V případě, že je editeValue null, tak to je úmyslně, abych nemusel vytvářet
		 * například další objekt apod. Tak v tomto případě to značí, že se v poli
		 * nenašla položka se zadanými indexy, takže uživatel zadal indexy úplně mimo
		 * pole, tak to oznámím.
		 */
		if (editedValue == null) {
			// Nebyly nalezeny indexy:
			outputEditor.addResult(txtValueWasNotFoundAtIndexInTwoDimarrayInInstance + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtIndex_1Text + ": '" + index_1 + "', " + txtIndex_2Text + ": '" + index_2 + "'.");

			return returnResult(false, null);
		}
		
		
		/*
		 * V tomto případě otestuji, zda se příslušná inkrementace / dekrementace
		 * nepovedla, to může nastat, když je hodnota v poli třeba null nebo nějaká
		 * hodnota, která není číslo.
		 */
		if (!editedValue.isExecuted()) {
			if (increment)
				outputEditor.addResult(txtFailedToIncrementValueAtIndexInTwoDimArrayInInstance);
			else
				outputEditor.addResult(txtFailedToDecrementValueAtIndexInTwoDimArrayInInstance);

			return returnResult(false, null);
		}

		// Zde už je vše v pořídku, hodnota se v pořídku inkrementovala /
		// dekrementovala, tak jej vrátím:
		return returnResult(true, editedValue.getObjResult());
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro inkrementování / dekrementování hodnoty ve
	 * dvourozměrném poli objArray na zadaném indexu v instanci nějaké třídy.
	 * 
	 * Informaci o tom, zda se to povedlo nebo ne se vrátí, případně i s
	 * inkrementovanou / dekrementovanou hodnotou.
	 * 
	 * @param objArray
	 *            - dvourozměrné pole, ve kterém se má inkrementovat položka na
	 *            zadaném indexu a tato inkrementovaná / dekremetnovaná položka se
	 *            má pak vrátit.
	 * 
	 * @param index_1
	 *            - index "prvního" pole.
	 * 
	 * @param index_2
	 *            - index "druhého / vnitřního" pole.
	 * 
	 * @param increment
	 *            - logická hodnota o tomm, zda se má nalezená hodnota inkremetnovat
	 *            (true), nebo dekrementovat (false).
	 * 
	 * @return Informce o tom, zda se příslušnou hodnotu na zadaném indexu v poli
	 *         podařilo inkrementovat / dekrementovat. Vrátí se null v případě, že
	 *         se nepodařilo najít položku na zadaných indexech, takže uživatel
	 *         zadal chybné indexy položky v poli. Jinak se vrátí objekt ResultValue
	 *         s tím, že pokud bude první hodnota true, pak se hodnota v poli na
	 *         zadanémindexu v pořádku inkrementovala / dekrementovala a vrátila,
	 *         pokud bude první hodnota false, pak došlo k chybě, ale takové, že
	 *         zadaná hodnota v poli na zadaném indexu je buď null, nebo taková
	 *         hodnota, která není číslo - nelze jejj inkrementovat / dekrementovat.
	 */
	private ResultValue getIncrementedDecrementedValueAtIndexInTwoDimArray(final Object objArray, final int index_1,
			final int index_2, final boolean increment) {
		
		/*
		 * Zjistím si velikost pole:
		 */
		final int size = Array.getLength(objArray);

		/*
		 * Projdu pole, v každé iteraci si získám příslušnou položku na daném indexu,
		 * otestji, zda je to pole (vždy by mělo být), pak projdu i to druhé pole a při
		 * každé položce otestuji, zda se jedná o požadovaný index a pokud ano, zkusím
		 * příslušnou hodnotu inkrementovat / dekrementovat. Dle toho, zda se to poveden
		 * nebo ne tu hodnotu nataví do pole na požadovaný index a vrátím ohledně toho
		 * nastavení nebo chybě informace.
		 */
		for (int i = 0; i < size; i++) {
			/*
			 * Hodnota na prvním indexu, vždy by se mělo jednat o "druhé" pole.
			 */
			final Object value = Array.get(objArray, i);

			// Projdu i to druhé pole:
			if (value != null && value.getClass().isArray()) {
				/*
				 * Zjistím si jeho velikost.
				 */
				final int size2 = Array.getLength(value);

				// Projdu tro druhé pole:
				for (int q = 0; q < size2; q++) {
					// Otestuji, zda jsem našel požadované indexy:
					if (i == index_1 && q == index_2) {
						/*
						 * Zde se shodují indexy, tak si získám hodnotu na požadovaném indexu:
						 */
						final Object objValue = Array.get(value, q);

						// V případě null hodnota nemá smysl inkrementovat / dekrementovat:
						if (objValue != null) {
							/*
							 * Do této proměnné si uložím hodnotu, která se měla inkrementovat /
							 * dekrementovat, pokud bude null, tak se nepodařilo nebo nemohla inkremetovat /
							 * dekrementovat.
							 */
							final Object objFinalValue = getIncrementDecrementValue(objValue, increment);

							if (objFinalValue != null) {
								/*
								 * Zde se úspěšně inkrementovala / dekrementovala příslušná hodnota, tak ji
								 * natavím do pole:
								 */
								Array.set(value, q, objFinalValue);

								/*
								 * Zde vrátím výsledek, v tomto případě je důležitý pouze první parametr - true,
								 * že se v pořádku hodnota nastavila a jako druhá hodnota to bude ta
								 * inkrementovaná / dekrementovaná hodnota.
								 */
								return returnResult(true, objFinalValue);
							}

							/*
							 * Zde se nepodařilo hodnotu inkrementovat / dekremetovat, tak bude první
							 * hodnota false a druhá hodnota null - není potřeba.
							 */
							return returnResult(false, null);
						}
						/*
						 * Zde je hodnota null, tak nemá smysl inkrementovat, dekrementovat, tak o tom
						 * vrátím informace, první false je, že se nepodařilo hodnotu inkrementovat /
						 * dekrementovat a druhá není potřeba, tak null.
						 */
						return returnResult(false, null);
					}
				}
			}
		}

		
		/*
		 * Zde jsem prošel celé pole, a nenašel jsem požadovanou položku na zadaných
		 * indexech, tak vrátím null.
		 */
		return null;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro inkrementování nebo dekrementování hodnoty v poli
	 * arrayName na indexu index v instanci třídy referenceName.
	 * 
	 * Dále vrátí hodnotu získanou z toho pole na zadaném indexu, ale ještě před
	 * dekrementací / inkrementací a pokud je proměnná isMinus true, pak se ještě
	 * načtená hodnota zneguje.
	 * 
	 * Tato metoda slouží pro to, že když se zavolá příkaz pro postfixovou
	 * inkrementaci nebo dekrementaci hodnoty v poli v nějaké instanci, například:
	 * (-) referenceName.variableArrayname[index] (++ | --);
	 * 
	 * Konkrétní postup je, že se načte hodnota z pole na zadaném indexu, pak se
	 * inkrementuje / dekrementuje (dle hodnoty v increment), a vloží se zpět do
	 * pole, pak se pokud je hodnota isMinus true, tak se získaná hodnota z pole
	 * ještě zneguje a vrátí se její hodnota, jinak se vrátí hodnota načtená z pole
	 * bez negace.
	 * 
	 * @param command
	 *            - část příkazu, která obsahuje syntaxi pro inkrementace nebo
	 *            dekrementaci hodnoty v jednorozměrném poli v instanci třídy.
	 * 
	 * @param variableDataType
	 *            - datový typ, kterého by mělo být pole nebo null, pokud se nemají
	 *            testovat datové typy hodnot.
	 * 
	 * @param increment
	 *            - logická proměnná, která značí, zda se má načtená hodnota z pole
	 *            na indexu index inkrementovat nebo dekrementovat. True značí
	 *            inkrementaci, false dekrementaci.
	 * 
	 * @return hodnotu načteou z pole beze změn, tj. pouze se načte hodnota z pole a
	 *         ta se vrátí, akorát se pokud je hodnota isMinus true, tak se načtená
	 *         hodnota z pole ještě zneguje, jinak se jen vrátí. Ta inkrementace a
	 *         dekrementace už tuto vrácenou hodnotu neovlivní.
	 */
	private ResultValue incrementDecrementValueInArrayAtIndexInInstance(final String command,
			final Class<?> variableDataType, final boolean increment) {
		
		final boolean isMinus = isMinusAtFirstChar(command);
		
		final String editedCommand;
		
		if (isMinus)
			editedCommand = command.substring(command.indexOf('-') + 1);
		else
			editedCommand = command;

		/*
		 * Reference na intanci nějaké třídy je od začátku po první desetinnou tečku:
		 */
		final String referenceName = editedCommand.substring(0, editedCommand.indexOf('.')).replaceAll("\\s", "");

		/*
		 * Název proměnné je od první desetinné tečky po první otevírací hranatou
		 * závorku.
		 */
		final String arrayName = editedCommand.substring(editedCommand.indexOf('.') + 1, editedCommand.indexOf('['))
				.replaceAll("\\s", "");

		/*
		 * Index položky v poli je mezi hranatými závorkami:
		 */
		final String indexInArray = editedCommand
				.substring(editedCommand.indexOf('[') + 1, editedCommand.indexOf(']')).replaceAll("\\s", "");

		
		/*
		 * Index, do které ho si naparsuji int - ovou hodnotu získanou z textu, už z
		 * regulárních výrazů vím, že je to číslo, ale také to může být například číslo
		 * typu Long apod. To je špatně.
		 */
		final int index;
		try {
			index = Integer.parseInt(indexInArray);
		} catch (NumberFormatException e) {
			outputEditor.addResult(
					txtCastingToIntegerFailure_1 + " '" + indexInArray + "' " + txtCastingToIntegerFailure_2);
			return returnResult(false, null);
		}
		
		/*
		 * Načtu si instanci pod zadanou referencí (pokud existuje).
		 */
		final Object objInstance = Instances.getInstanceFromMap(referenceName);

		if (objInstance == null) {
			outputEditor.addResult(txtInstanceOfClassInIdNotFOund + ": " + referenceName);
			return returnResult(false, null);
		}

		/*
		 * Získám si položuk v zadané instanci pod zadaným jménem, zde ještě nevím, zda
		 * se jedná o jednorozměrné pole.
		 */
		final Field field = getFieldFromInstance(objInstance, arrayName);

		if (field == null) {
			outputEditor.addResult(txtVariableNotFoundInReferenceToInstanceOfClass);
			return returnResult(false, null);
		}

		/*
		 * zde otestuji, zda se jedná o pole.
		 */
		if (!field.getType().isArray()) {
			outputEditor.addResult(txtEnteredVariable_1 + " '" + field.getName() + "' " + txtEnteredVariable_2);
			return returnResult(false, null);
		}

		/*
		 * Zde si ještě musím zjistit počet dimenzí, protože předchoí testování v
		 * případě, že je položka field pole vždy vrátí true, i v případě, že se jedná o
		 * například 4 - rozměrné pole apod.
		 */
		final int countOfDims = getCountOfDimensionsOfArray(field.getType());

		if (countOfDims != 1) {
			outputEditor.addResult(txtVariable + " '" + field.getName() + "' " + txtIsNotOneDimArray);
			return returnResult(false, null);
		}

		
		
		/*
		 * Zde otestuji, zda se má otestovat datový typ návratové hodnoty a pokud ano,
		 * tak musím zjistit, zda se ten požadovaný typ shoduje s datový typem pole.
		 */
		if (variableDataType != null
				&& !field.getType().getComponentType().toString().equals(variableDataType.toString())) {
			outputEditor.addResult(
					txtDataTypesAreNotEqual + ": " + field.getType().getComponentType() + " != " + variableDataType);
			return returnResult(false, null);
		}
		
		
		
		
		/*
		 * Zde si získám hodnotu v zadané proměnné - zadané pole.
		 */
		final Object objArrayValue = ReflectionHelper.getValueFromField(field, objInstance);

		if (objArrayValue == null)
			// Nemusí být viditelné apod., ale to se již vypsalo:
			// return txtFailure;
			return returnResult(false, null);

		/*
		 * Zde si zjistím velikost příslušného pole.
		 */
		final int arrayLength = Array.getLength(objArrayValue);

		if (index >= arrayLength) {
			outputEditor.addResult(txtIndexValueAtArrayIsNotInInterval + ": <0 ; " + arrayLength + ").");
			return returnResult(false, null);
		}

		/*
		 * Hískám si hodnotu z pole na zadaném indexu:
		 */
		Object objValueAtindex = Array.get(objArrayValue, index);

		// S null hodnotou nelze nic dělat:
		if (objValueAtindex == null)
			return returnResult(false, null);

		/*
		 * Zde inkrementuji nebo dekrementuji získanou hodnotu - pokud se jedná o číslo,
		 * pokud ne, pak se hodnota nezmění, a vrátí se null, tak nebudu nic dělat, tj.
		 * nechám v tom poli původní hodnotu.
		 */
		final Object editedValue = getIncrementDecrementValue(objValueAtindex, increment);

		if (editedValue != null)
			// Zde se hodnotu podařilo inkrementovat / dekrementovat, tak ji natavím:
			Array.set(objArrayValue, index, editedValue);

		/*
		 * Zde ještě otestuji, zda se má hodnota znegovat - pokud to lze.
		 */
		if (isMinus)
			objValueAtindex = getNegationOfValue(field.getType().getComponentType(), objValueAtindex);

		// Vrátím získanou hodnotu z pole, ale ne inkrementovanou / dekrementovanou:
		return returnResult(true, objValueAtindex);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro inkrementování nebo dekrementování hodnoty v poli
	 * arrayName na zadaném indexu v instanci třídy pod referencí referenceName.
	 * 
	 * Dále vrátí hodnotu získanou z toho pole na zadaném indexu, ale ještě před
	 * dekrementací / inkrementací a pokud je proměnná isMinus true, pak se ještě
	 * načtená hodnota zneguje.
	 * 
	 * Tato metoda slouží pro to, že když se zavolá příkaz pro postfixovou
	 * inkrementaci nebo dekrementaci hodnoty v poli v nějaké instanci, například:
	 * (-) referenceName.variableArrayname[index] [index] (++ | --);
	 * 
	 * Konkrétní postup je, že se načte hodnota z pole na zadaném indexu, pak se
	 * inkrementuje / dekrementuje (dle hodnoty v increment), a vloží se zpět do
	 * pole, pak se pokud je hodnota isMinus true, tak se získaná hodnota z pole
	 * ještě zneguje a vrátí se její hodnota, jinak se vrátí hodnota načtená z pole
	 * bez negace.
	 * 
	 * @param command
	 *            - část příkazu, která obsahuje syntaxi pro inkrementace nebo
	 *            dekrementaci hodnoty v jednorozměrném poli v instanci třídy.
	 * 
	 * @param variableDataType
	 *            - datový typ, kterého by mělo být pole nebo null, pokud se nemají
	 *            testovat datové typy hodnot.
	 * 
	 * @param increment
	 *            - logická proměnná, která značí, zda se má načtená hodnota z pole
	 *            na indexu index inkrementovat nebo dekrementovat. True značí
	 *            inkrementaci, false dekrementaci.
	 * 
	 * @return hodnotu načteou z pole beze změn, tj. pouze se načte hodnota z pole a
	 *         ta se vrátí, akorát se pokud je hodnota isMinus true, tak se načtená
	 *         hodnota z pole ještě zneguje, jinak se jen vrátí. Ta inkrementace a
	 *         dekrementace už tuto vrácenou hodnotu neovlivní, pouze se vloží do
	 *         pole.
	 */
	private ResultValue incrementDecrementValueInTwoDimArrayAtIndexInInstance(final String command,
			final Class<?> variableDataType, final boolean increment) {
		// Syntaxe: (-) referenceName . variableName [index] [index] (++|--)
		
		/*
		 * Logická proměnná, do které si uložím informaci o tom, zda je na začátku
		 * příkaz mínus pro negaci hodnoty nebo ne.
		 */
		final boolean isMinus = isMinusAtFirstChar(command);

		final String editedCommand;

		if (isMinus)
			editedCommand = command.substring(command.indexOf('-') + 1);
		else
			editedCommand = command;

		/*
		 * Reference na intanci nějaké třídy je od začátku po první desetinnou tečku:
		 */
		final String referenceName = editedCommand.substring(0, editedCommand.indexOf('.')).replaceAll("\\s", "");

		/*
		 * Název proměnné je od první desetinné tečky po první otevírací hranatou
		 * závorku.
		 */
		final String arrayName = editedCommand.substring(editedCommand.indexOf('.') + 1, editedCommand.indexOf('['))
				.replaceAll("\\s", "");

		
		
		
		/*
		 * První index se nachází mezi první otevírací a první zavárací hranatou
		 * závorkou:
		 */
		final String indexInArray_1 = command.substring(command.indexOf('[') + 1, command.indexOf(']'))
				.replaceAll("\\s", "");

		/*
		 * První index se nachází mezi poslední otevírací a poslední zavárací hranatou
		 * závorkou:
		 */
		final String indexInArray_2 = command.substring(command.lastIndexOf('[') + 1, command.lastIndexOf(']'))
				.replaceAll("\\s", "");

		/*
		 * Zde si mohu index rovnou přetypovat na integer, protože vím, že se jedná o
		 * číslo, ale uživatel může například zadat číslo pro datový typ long apod. To
		 * je chyba.
		 */
		final int index_1, index_2;

		try {
			index_1 = Integer.parseInt(indexInArray_1);
		} catch (NumberFormatException e) {
			outputEditor.addResult(
					txtCastingToIntegerFailure_1 + " '" + indexInArray_1 + "' " + txtCastingToIntegerFailure_2);

			return returnResult(false, null);
		}

		try {
			/*
			 * Zde zkusím přetypovat i druhý index položky v poli, ale v jiném bloku
			 * try-catch, abych věděl, v jakém indexu je problém.
			 */
			index_2 = Integer.parseInt(indexInArray_2);
		} catch (NumberFormatException e) {
			outputEditor.addResult(
					txtCastingToIntegerFailure_1 + " '" + indexInArray_2 + "' " + txtCastingToIntegerFailure_2);

			return returnResult(false, null);
		}
		
		
		
		/*
		 * Načtu si instanci pod zadanou referencí (pokud existuje).
		 */
		final Object objInstance = Instances.getInstanceFromMap(referenceName);

		if (objInstance == null) {
			outputEditor.addResult(txtInstanceOfClassInIdNotFOund + ": " + referenceName);
			return returnResult(false, null);
		}

		/*
		 * Získám si položuk v zadané instanci pod zadaným jménem, zde ještě nevím, zda
		 * se jedná o jednorozměrné pole.
		 */
		final Field field = getFieldFromInstance(objInstance, arrayName);

		if (field == null) {
			outputEditor.addResult(txtVariableNotFoundInReferenceToInstanceOfClass);
			return returnResult(false, null);
		}

		
		
		/*
		 * zde otestuji, zda se jedná o pole.
		 */
		if (!field.getType().isArray()) {
			outputEditor.addResult(txtEnteredVariable_1 + " '" + field.getName() + "' " + txtEnteredVariable_2);
			return returnResult(false, null);
		}
		
		
		
		/*
		 * Zde si ještě musím zjistit počet dimenzí, protože předchoí testování v
		 * případě, že je položka field pole vždy vrátí true, i v případě, že se jedná o
		 * například 4 - rozměrné pole apod.
		 */
		final int countOfDims = getCountOfDimensionsOfArray(field.getType());

		if (countOfDims != 2) {
			outputEditor.addResult(txtVariable + " '" + field.getName() + "' " + txtIsNotTwoDimArray);

			return returnResult(false, null);
		}

			
		
		
		/*
		 * Získám si datový typ pole:
		 */
		final Class<?> dataTypeOfArray = ReflectionHelper.getDataTypeOfArray(field.getType());

		/*
		 * Zde otestuji, zda se má otestovat datový typ návratové hodnoty a pokud ano,
		 * tak musím zjistit, zda se ten požadovaný typ shoduje s datový typem pole.
		 */
		if (variableDataType != null && !dataTypeOfArray.toString().equals(variableDataType.toString())) {
			outputEditor.addResult(txtDataTypesAreNotEqual + ": " + dataTypeOfArray + " != " + variableDataType);
			return returnResult(false, null);
		}
		
		
		
		
		/*
		 * Zde si získám hodnotu v zadané proměnné - zadané pole.
		 */
		final Object objArrayValue = ReflectionHelper.getValueFromField(field, objInstance);

		if (objArrayValue == null)
			// Nemusí být viditelné apod., ale to se již vypsalo:
			// return txtFailure;
			return returnResult(false, null);

		
		
		
		
		
		/*
		 * Zaolám metodu, která inkrementuje / dekrementuje hodnotu na zadaném indexu ve
		 * dvourozměrném pli. a dle výsledku, který metoda vrátí postupují dálě.
		 */
		final ResultValue resultValue = getPostfixIncrementedDecrementedValueAtIndexInTwoDimArray(objArrayValue,
				index_1, index_2, increment);

		
		/*
		 * V případě, že je editeValue null, tak to je úmyslně, abych nemusel vytvářet
		 * například další objekt apod. Tak v tomto případě to značí, že se v poli
		 * nenašla položka se zadanými indexy, takže uživatel zadal indexy úplně mimo
		 * pole, tak to oznámím.
		 */
		if (resultValue == null) {
			// Nebyly nalezeny indexy:
			outputEditor.addResult(txtValueWasNotFoundAtIndexInTwoDimarrayInInstance + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtIndex_1Text + ": '" + index_1 + "', " + txtIndex_2Text + ": '" + index_2 + "'.");

			return returnResult(false, null);
		}
		
		
		/*
		 * V tomto případě otestuji, zda se příslušná inkrementace / dekrementace
		 * nepovedla, to může nastat, když je hodnota v poli třeba null nebo nějaká
		 * hodnota, která není číslo.
		 */
		if (!resultValue.isExecuted()) {
			if (increment)
				outputEditor.addResult(txtFailedToIncrementValueAtIndexInTwoDimArrayInInstance);
			else
				outputEditor.addResult(txtFailedToDecrementValueAtIndexInTwoDimArrayInInstance);

			return returnResult(false, null);
		}
		
		

		/*
		 * Zde ještě otestuji, zda se má hodnota znegovat - pokud to lze.
		 */
		if (isMinus) {
			final Object objNegationValue = getNegationOfValue(dataTypeOfArray, resultValue.getObjResult());

			return returnResult(true, objNegationValue);
		}

			
		// Vrátím získanou hodnotu z pole, ale ne inkrementovanou / dekrementovanou, zde
		// se nemá ani znegovat:
		return returnResult(true, resultValue.getObjResult());
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro inkrementování / dekrementování hodnoty ve
	 * dvourozměrném poli objArray na zadaném indexu v instanci nějaké třídy.
	 * 
	 * @param objArray
	 *            - dvourozměrné pole, ve kterém se má inkrementovat položka na
	 *            zadaném indexu.
	 * 
	 * @param index_1
	 *            - index "prvního" pole.
	 * 
	 * @param index_2
	 *            - index "druhého / vnitřního" pole.
	 * 
	 * @param increment
	 *            - logická hodnota o tomm, zda se má nalezená hodnota inkremetnovat
	 *            (true), nebo dekrementovat (false).
	 * 
	 * @return objekt, který bude obsahovat informace o tom, zda se povedla
	 *         inkrementace nebo dekrementace a pokud ne, tak z jakého důvodu se to
	 *         nepovedlo. Vrátí se hodnota null v případě, že se nepodařilo nalézt
	 *         hodnotu v poli na zadaných indexech, tj. že se prošlo celé pole, ale
	 *         nenašly se indexy, jinak to vrátí instaci třídy ResultValue, kde
	 *         budou prví hodnota true pouze v případě, že se úspěšně nastaví
	 *         hodnota a vrátí se ta původní (neinkrementovaná / nedekrementovaná)
	 *         položka načtená na zadaném indexu. Jinak, když tato první honnota ve
	 *         vráceném objektu bude false, pak hodnota na zadaných indexech našla,
	 *         ale nepodařilo se hodnotu inkrementovat / dekrementovat, protože je
	 *         to buď hodnota null nebo se nejedná o číselný datový typ.
	 */
	private ResultValue getPostfixIncrementedDecrementedValueAtIndexInTwoDimArray(final Object objArray,
			final int index_1, final int index_2, final boolean increment) {
		
		/*
		 * Zjistím si velikost pole:
		 */
		final int size = Array.getLength(objArray);

		/*
		 * Projdu pole, v každé iteraci si získám příslušnou položku na daném indexu,
		 * otestji, zda je to pole (vždy by mělo být), pak projdu i to druhé pole a při
		 * každé položce otestuji, zda se jedná o požadovaný index a pokud ano, zkusím
		 * příslušnou hodnotu inkrementovat / dekrementovat. Dle toho, zda se to povede
		 * nebo ne tu hodnotu nataví do pole na požadovaný index a vrátím ohledně toho
		 * nastavení nebo chybě informace a vypíše se příslušné oznámení apod.
		 */
		for (int i = 0; i < size; i++) {
			/*
			 * Hodnota na prvním indexu, vždy by se mělo jednat o "druhé" pole.
			 */
			final Object value = Array.get(objArray, i);

			// Projdu i to druhé pole:
			if (value != null && value.getClass().isArray()) {
				/*
				 * Zjistím si jeho velikost.
				 */
				final int size2 = Array.getLength(value);

				// Projdu tro druhé pole:
				for (int q = 0; q < size2; q++) {
					// Otestuji, zda jsem našel požadované indexy:
					if (i == index_1 && q == index_2) {
						/*
						 * Zde se shodují indexy, tak si získám hodnotu na požadovaném indexu:
						 */
						final Object objValue = Array.get(value, q);

						// V případě null hodnota nemá smysl inkrementovat / dekrementovat:
						if (objValue != null) {
							/*
							 * Do této proměnné si uložím hodnotu, která se měla inkrementovat /
							 * dekrementovat, pokud bude null, tak se nepodařilo nebo nemohla inkremetovat /
							 * dekrementovat.
							 */
							final Object objFinalValue = getIncrementDecrementValue(objValue, increment);

							if (objFinalValue != null) {
								/*
								 * Zde se úspěšně inkrementovala / dekrementovala příslušná hodnota, tak ji
								 * natavím do pole:
								 */
								Array.set(value, q, objFinalValue);								
									

								/*
								 * Zde vrátím výsledek, v tomto případě je důležitý pouze první parametr - true,
								 * že se v pořádku hodnota nastavila.
								 */
								return returnResult(true, objValue);
							}

							/*
							 * Zde se nepodařilo hodnotu inkrementovat / dekremetovat, tak bude první
							 * hodnota false a druhá hodnota už není potřeba, tak null.
							 */
							return returnResult(false, null);
						}
						/*
						 * Zde je hodnota null, tak nemá smysl inkrementovat, dekrementovat, tak otom
						 * vrátím informace, první false je, že se nepodařilo hodnotu inkrementovat /
						 * dekrementovat a druhá není potřeba, takže null.
						 */
						return returnResult(false, null);
					}
				}
			}
		}

		/*
		 * zde vrátím informace o tom, že se nepodařilo najít zadané indexy v příslušném
		 * poli.
		 */
		return null;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otesuje "první 3" podmínky pro inkrementace či dekrementaci
	 * hodnoty na nějakém indexu v poli Metoda otestuji, zda bylo nalezeno zadané
	 * pole (instance třídy VariableArray), dále jestli pole není final, pak nelze
	 * editovat hodnoty, jsou pouze ke čtení, zda byl zadán u "určen" index hodnoty
	 * v poli a na konec, zda index odpovídá rozsahu hodnot v poli, tj musí být
	 * větší rovno nule a zároveň menší než velikost pole
	 * 
	 * @param variableArray
	 *            - reference na instanci třídy VariableArray
	 * 
	 * @param index
	 *            - index hodnoty v poli
	 * 
	 * @param arrayName
	 *            - název pole, resp. reference na něj
	 * 
	 * @return true, pokud jsou výše uvedené podmínky splněny, jinak false
	 */
	private boolean conditionForIncrementDecrement(final VariableArray<?> variableArray, final int index,
			final String arrayName) {
		if (variableArray != null) {
			// Otestuji zda se může inkrementovat hodnota:
			if (!variableArray.isFinal()) {
				if (index > -1) {
					if (index < variableArray.getArray().length)
						return true;
					
					else
						outputEditor.addResult(txtConditionForIndexInArray + OutputEditor.NEW_LINE_TWO_GAPS + txtIndex
								+ ": " + index + ", " + txtSizeOfArray + ": " + variableArray.getArray().length);
				}
				else
					outputEditor
							.addResult(txtIndexOutOfRange + OutputEditor.NEW_LINE_TWO_GAPS + txtValueWillNotBeCounted);
			} 
			else
				outputEditor.addResult(
						txtArrayIsFinalDontChangeValues + OutputEditor.NEW_LINE_TWO_GAPS + txtArray + ": " + arrayName);
		} 
		else
			outputEditor.addResult(txtInstanceOfArrayWasNotFoundUnderReference + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtReference + ": " + arrayName);
		
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která inkrementuje nebo dekrementuje (dle proměnné increment) hodnotu
	 * v parametru objValue (pokud tak lze provést)
	 * 
	 * @param objValue
	 *            - hodnota, která se má inkrementovat nebo dekrementovat
	 * 
	 * @param increment
	 *            - logická hodnota o tom, zda se má hodnota v parametru objValue
	 *            dekremetovat nebo inkrementovat, pokud je true, má se
	 *            inkrementovat, pokud je false, má se dekrementovat
	 * 
	 * @return inkrementovanou / dekrementovanou hodnotu nebo null, pokud jej nelze
	 *         nebo se nepodaří inkrementovat / dekrementovat.
	 */
	private Object getIncrementDecrementValue(final Object objValue, final boolean increment) {
		/*
		 * Pro začátek zde otestuji, zda se vůbec jedná o číselný datový typ (big
		 * decimal apod. zde neřeším, jedná se pouze o "základy"). Pokud se nejedná o
		 * číselný datový typ, ale je to například String a char apod. Tak mohu rovnou
		 * vrátit null, protože takové typy nelze inkremetnovat nebo dekrementovat.
		 */
		
		if (!objValue.getClass().equals(byte.class) && !objValue.getClass().equals(Byte.class)
				&& !objValue.getClass().equals(short.class) && !objValue.getClass().equals(Short.class)
				&& !objValue.getClass().equals(int.class) && !objValue.getClass().equals(Integer.class)
				&& !objValue.getClass().equals(long.class) && !objValue.getClass().equals(Long.class)
				&& !objValue.getClass().equals(float.class) && !objValue.getClass().equals(Float.class)
				&& !objValue.getClass().equals(double.class) && !objValue.getClass().equals(Double.class))
			return null;

		
		/*
		 * Proměnná, do které vložím finální hodnotu, tj, inkrementovanou /
		 * dekrementovanou hodnotu:
		 */
		String finalValue = "";
		
		
		// Otestuji, zda se jedná o celé číslo:
		if (regEx.isInteger(objValue.toString())) {
			final BigInteger bi = new BigInteger(objValue.toString());
			Long l = bi.longValue();
			
			if (increment)
				finalValue = String.valueOf(++l);
			
			else finalValue = String.valueOf(--l);
		}
		
		// Otestuji, zda se jedná o destinné číslo:
		else if (regEx.isDecimal(objValue.toString())) {
			final BigDecimal bd = new BigDecimal(objValue.toString());
			Double d = bd.doubleValue();
			
			if (increment)
				finalValue = String.valueOf(++d);
			
			else finalValue = String.valueOf(--d);
		}
		
		
		return getParsedParameter(objValue.getClass(), finalValue);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda se zadaný text obsahuje na úplně prvním znaku
	 * mínus (-) , tuto metodu nezajímá "obsah nebo smysl" daného textu, ale pouze
	 * zda je na prvním znaku mínus, proto odebere z příkazu všechny mezery a vrátí
	 * true, nebo false
	 * 
	 * @param text
	 *            - nějaký text u kterého chceme zjistit, zda se na prvním znaku
	 *            nachází mínus půjde spíše o text, ve kterém se nachází příkazy pro
	 *            inkrementaci nebo dekrementace nějaké hodnoty
	 * 
	 * @return true, pokud se na prvním znaku textu v parametru text nachízí mínus,
	 *         jinak false
	 */
	private static boolean isMinusAtFirstChar(final String text) {
		final String textWithoutSpace = text.replaceAll("\\s", "");

		return textWithoutSpace.charAt(0) == '-';
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která se pokusí najít instanci třídy VariableArray pod danám názvem.
	 * 
	 * @param s
	 *            - část uživatelem zadaného příkazu, ta, která by měla obsahovat
	 *            "příkaz", ze kterého se má vzít hodnota
	 * 
	 * @return null, nebo instanci třídy VariableArray, na kterou má ukazovat zadaný
	 *         náízev (dle toho, zda se zmíněná instance najde nebo ne)
	 */
	private static VariableArray<?> getVariableArrayInstance(final String s) {
		// Syntaxe: (-) variableArray [index]
		
		// Z příkazu mohu odebrat všechny bíle znaky, zde k chybě nedojde a mohu zjistit, zda se na začátku příkazu
		// vyskytuje znaménko mínus pro negaci kladné či záporné hodnoty z pole:
		final String textWithoutSpace = s.replaceAll("\\s", "");
		
		
		// Proměnné pro název pole, resp reference na něj:
		final String arrayName;
		
		
		// Otestuji, zda se na začátku výrazu nachází mínus nebo ne:
		if (textWithoutSpace.charAt(0) == '-')
			// Zde příkaz obsahuje mínus, tak bude název pole začínat za mínusem:
			arrayName = textWithoutSpace.substring(1, textWithoutSpace.indexOf('['));
		
		else arrayName = textWithoutSpace.substring(0, textWithoutSpace.indexOf('['));
				
		
		// Nyní znám název pole, resp referenci na něj, tak muhu zjistit, zda se vůbec existuje:
		return (VariableArray<?>) Instances.getArrayFromMap(arrayName);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, se pokusí získat název pole, resp referenci na instanci třídy VariableArray, která reprezentuje
	 * jednorozměrné pole v editoru příkazů.
	 * 
	 *  Pokud nebude nalezeno vrátí se null, jinak se vrátí daná instance třídy VariableArray.
	 *  
	 * @param s = příkaz v syntaxi pro získání délky jednorozměrného pole:
	 * Syntaxe: (-) arrayName.length
	 * 
	 * @return null, pokud nebude instance třídy VariableArray pod zadaným názvem, resp. referenci nalezena,
	 * jinak vrátí danou instanci třídy VariableArray
	 */
	private static VariableArray<?> getVariableArrayInstance2(final String s) {
		// Syntaxe: (-) arrayName.length
		
		final String textWithnoutSpace = s.replaceAll("\\s", "");
		
		// název pole, resp. reference na zmíněnou instanci třídy VariableArray:
		final String arrayName;
		
		
		
		if (isMinusAtFirstChar(s))
			arrayName = textWithnoutSpace.substring(1, textWithnoutSpace.indexOf('.'));
		
		// Zde není na začátku příkazu mínus, tak je název od začátku po první tečku:
		else arrayName = textWithnoutSpace.substring(0, textWithnoutSpace.indexOf('.'));
		
		
		return (VariableArray<?>) Instances.getArrayFromMap(arrayName);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která se pokusí získat ze zadaného příkazu dle syntaxe v regulárních
	 * výrazech intanci třídy VariableLIst, na která má ukazovat získaný název,
	 * pokud se tak povede, tak vrátit instanci této třídy, pokud ne vrátí se null
	 * hodnota
	 * 
	 * @param s
	 *            - uživatelem zadaný příkaz v editoru příkazů
	 *            
	 * @return instanci třídy VariableList, pokud bude nalazena pod získaným názvem
	 *         reference, jinak null
	 */
	private static VariableList<?> getVariableListInstance(final String s) {
		// referenceToList.get(X);   (x = celé číslo >= 0)
		
		// název reference je od začátku po první tečku:
		final String variableName = s.substring(0, s.indexOf('.')).replaceAll("\\s", "");
		
		
		return (VariableList<?>) Instances.getListFromMap(variableName);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která si z textu v parametru vezme pouze název tj. odebere přebytečné
	 * mezery a jiné bílé znaky, díky tomu se získá pouze název coby reference na
	 * instanci nějaké třídy, pak se zkusí najít tato instanci v HashMapě, ve které
	 * se udržují tyto instance a vrátí se, pokud se najde, tak se vrátí, pokud ne
	 * vrátí se null
	 * 
	 * @param text
	 *            - text, coby reference na instnaci třídy
	 * 
	 * @return insntanci třídy, pokud bude nalezena v HashMapě, jinak null, pokud se
	 *         nenajde
	 */
	private static Object getInstanceOfClass(final String text) {
		final String textWithoutSpace = text.replaceAll("\\s", "");
		
		return Instances.getInstanceFromMap(textWithoutSpace);
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která získá veškeré veřejné a chráněné proměnné z třídy clazz a všech
	 * jejich předků až po tředu Object.
	 * 
	 * note:
	 * 
	 * Také by to šlo napsat tak, že by se hledali pouze proměnné se stejným názvem,
	 * ale to jsem zde již nedával, protože dopředu nevím, k čemu se toho ještě může
	 * připadně hodnit, kdybych potřeboval najít více proměnných apod. Tak si vrátím
	 * z této metody veškeré proměné a až z těch filtruji pouze ty, které potřebuji
	 * (abych případně nemusel procházet všechny tyto třídy znovu pro příslušnou
	 * proměnnou).
	 * 
	 * @param clazz
	 *            - třída, ze které se mají načíst veškeré veřejné a chráněné
	 *            proměnné a pak se pokračuje rekurzi s předky této třídy až po
	 *            třídu Object, ta už se netestuje.
	 * 
	 * @param fieldsList
	 *            - list, do které se vkládají nalezené veřejné a chráněné proměnné
	 *            a tento list se na konec vrátí.
	 * 
	 * @return list fieldsList, který bude obsahovat veškeré nalezené veřejné a
	 *         chráněné proměnné z třídy clazz a všech jejich předků až po třídu
	 *         Object.
	 */
	private static List<Field> getFieldsFromInheritedClass(final Class<?> clazz, final List<Field> fieldsList) {
		/*
		 * Tato podmínka by neměla být splněna - jen pro jistotu.
		 */
		if (clazz == null || clazz.equals(Object.class))
			return fieldsList;

		if (clazz.getDeclaredFields() != null)
			Arrays.stream(clazz.getDeclaredFields()).forEach(f -> {
				if (Modifier.isPublic(f.getModifiers()) || Modifier.isProtected(f.getModifiers()))
					fieldsList.add(f);
			});

		/*
		 * Otestji předky třídy clazz a pokud to není null (nemělo by nastat) nebo
		 * Object, tak pokračuji rekurzi pro zjišťování proměnných, ale ze třídy
		 * Objectjiž brát proměnné nebudu.
		 */
		if (clazz.getSuperclass() != null && !clazz.getSuperclass().equals(Object.class))
			getFieldsFromInheritedClass(clazz.getSuperclass(), fieldsList);

		// Vrátím list nalezených proměnných.
		return fieldsList;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která se pokusí najít zadanou polozku v zadane instanci třídy
	 * 
	 * @param objInstance
	 *            - instance třídy
	 * 
	 * @param fieldName
	 *            - název položky, který je třeba vrátitk - najít
	 * 
	 * @return nalezenou položku, nebo null, pokud nebude nalezeny
	 */
	private static Field getFieldFromInstance(final Object objInstance, final String fieldName) {
		/*
		 * Do tohoto listu si vložím veškeré proměnné, kterou jsou veřejné a chráněné z
		 * třídy objInstance a ze všech jejich předků.
		 */
		final List<Field> fieldsList = getFieldsFromInheritedClass(objInstance.getClass(), new ArrayList<>());

		/*
		 * Nyní list výše s nalezenými proměnnými projdu a najdu proměnnou, kde se
		 * shodují název proměnné s fieldName, vrátí se vždy první shoda názvu
		 * proměnných.
		 */
		try {
			return fieldsList.stream().filter(f -> f.getName().equals(fieldName)).findFirst().get();
		} catch (NoSuchElementException e) {
			/*
			 * Tato výjimka nastane v případě, že nebude proměnná fieldName nalezena v listu
			 * fieldsList, tak vrátím null.
			 */
			return null;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metody pro naplnění proměnné i s deklarací dočasné proměnné v editoru příkazů,
	 * naplnění buď jednou hodnotou nebo více - tj. pomocí aritmetických operací
	 */
	




	


	@Override
	public String makeDeclarationAndFulfillmentOfEditorVariableConstant(final String command) {
		// syntaxe: (final) dataType variableName = text, znak, číslo, logická hodnota, apod.
		
		// Text s daty pro promennou - od začátku po rovná se (=):
		final String variableDataText = command.substring(0, command.indexOf('=')).trim();
		
		// Získám data o proměnné - z první části příkazu (před =)
		final Object[] objEditroVaribleData = getEditorVariableData(variableDataText);
		
		
		// Text s hodnotou pro naplnění proměnné je od rovnáse po poslední středník - coby prázdný příkaz pro ukončení příkazu:
		final String textOfValueForVariable = command.substring(command.indexOf('=') + 1, command.lastIndexOf(';')).trim();
		
		
		if (objEditroVaribleData != null) {
			// Z druhé částí príkazu si zjistím, o jakou hodnotu se jedná - metoda, konstanta, exponent, 
			// proměnná v editoru příkazů nebo instanci třídy
			final Class<?> dataTypeOfVariable = (Class<?>) objEditroVaribleData[1];
			
			// Zkusím zadanou hodntou v editoru naparsovat na potřebný typ proměnné:
			final ResultValue objResultValues = getValueFromVar_Method_Constant(textOfValueForVariable, dataTypeOfVariable);
			
			/*
			 * Pokud se nevrátila žádná hodnota (nemělo by nastat) nebo pokud se
			 * nerozpoznala operace, pak vrátím 'Neúspěch' s tím, že by měla již být
			 * informace o nastalé chybě (důvodu proč se nevykonala nějaká opoerace nebo
			 * nerozponal výraz) již vypsána do editoru výstupů.
			 */
			if (objResultValues == null || !objResultValues.isExecuted())
				return txtFailure;
			
			if (objResultValues.getObjResult() != null) {
				// Vytvořím proměnnou a vložím ji do hasMapy s instancemi, a pokud se to povede, vrátím úspěch:
				final boolean isEditorVariableCreated = createEditorVariable(objEditroVaribleData, objResultValues.getObjResult());
				
				if (isEditorVariableCreated) {
                    /*
                     * Zde je třeba aktualizovat hodnoty v okně pro automatické doplnění / dokončení hodnot v editoru
                      * příkazů, protože se výše vytvořila nová proměnná, který by v tomto okně nebyla zobrazena.
                     */
                    updateValueCompletionWindow();

                    return txtSuccess;
                }

				
				else return txtFailure + ": " + txtFailedToCreateVariable;
			}
			else
				return txtFailure + ": " + txtFailedRetypeValue + OutputEditor.NEW_LINE_TWO_GAPS + txtValue + ": '"
						+ textOfValueForVariable + "' " + txtToDataType + ": " + dataTypeOfVariable;
		}
		else return txtFailure + ": " + txtFailedToGetDataAboutVariableWhileDeclaration;		
	}








	
	@Override
	public String makeDeclarationAndFulfillmentOfEditorVariableVariable(final String command) {
		// Syntaxe: (final) dataType variableName = (referencename.)variableName;
	
		// Text s daty pro promennou - od začátku po rovná se (=):
		final String variableDataText = command.substring(0, command.indexOf('=')).trim();
		
		// Získám data o proměnné - z první části příkazu (před =)
		final Object[] objEditroVaribleData = getEditorVariableData(variableDataText);
	
		// Text s proměnnou, abych otestovatl, zda se jedná o proměnnou z intance třídy nebo z editru příkazů (bez mezer)
		final String textVariable = command.substring(command.indexOf('=') + 1, command.lastIndexOf(';'))
				.replaceAll("\\s", "");

		
		if (objEditroVaribleData != null) {
			// Zde se podařilo získat hodnoty o proměnné, vezmu si datový typ, abych je mohl porovnat z proměnnou pro získání hodntoy:
			final Class<?> dataType = (Class<?>) objEditroVaribleData[1];
			
			if (dataType != null) {
				final ResultValue objResultValue = getValueFromVar_Method_Constant(textVariable, dataType);
				
				/*
				 * Pokud se nevrátila žádná hodnota (nemělo by nastat) nebo pokud se
				 * nerozpoznala operace, pak vrátím 'Neúspěch' s tím, že by měla již být
				 * informace o nastalé chybě (důvodu proč se nevykonala nějaká opoerace nebo
				 * nerozponal výraz) již vypsána do editoru výstupů.
				 */
				if (objResultValue == null || !objResultValue.isExecuted())
					return txtFailure;
				
				if (objResultValue.getObjResult() != null) {
					// Mohu vytvořít proměnnou a předat jí získanou hodnotu:
					// Vytvořím proměnnou a vložím ji do hasMapy s instancemi, a pokud se to povede,
					// vrátím úspěch:
					final boolean isEditorVariableCreated = createEditorVariable(objEditroVaribleData,
							objResultValue.getObjResult());

					// Pokud nedojde k chybě, potvrdím úspěšné dokončení operace:
					if (isEditorVariableCreated) {
                        /*
                         * Aktualizace hodnot v okně pro doplňování / dokončování hodnot do editoru příkazů, jinak by
                          * se v tom okně zobrazovaly chybné hodnoty, resp. nebyla by v něm zobrazena tato hodnota.
                         */
                        updateValueCompletionWindow();

                        return txtSuccess;
                    }


					else
						return txtFailure + ": " + txtFailedToCreateVariable;
				}
			}
			else return txtFailure + ": " + txtDoNotRetrieveDataTypeOfVariable;
		}
		else return txtFailure + ": " + txtFailedToGetDataAboutVariableWhileDeclaration;
		
		return txtFailure;
	}
	








	@Override
	public String makeDeclarationAndFulfillmentOfEditorVariableGetMethod(final String command) {
		// Syntaxe: (final) dataType variableName = referenceName.getMethod();
		// Syntaxe: (final) dataType variableName = referenceName.getMethod(p1, p2, ...);
		
		// Nebo pro statickou metodu:
		// Syntaxe: (final) dataType variableName = packageName. (anotherPackageName.) ClassName.getMethod();
		// Syntaxe: (final) dataType variableName = packageName. (anotherPackageName.) ClassName.getMethod(p1, p2, ...);
		
		// Text s daty pro promennou - od začátku po rovná se (=):
		final String variableDataText = command.substring(0, command.indexOf('=')).trim();
		
		// Získám data o proměnné - z první části příkazu (před =)
		final Object[] objEditorVaribleData = getEditorVariableData(variableDataText);
		
		
		// Název reference na instanci třídy je od rovná se po první tečku (bez mezer):
		final String referenceName = command.substring(command.indexOf('=') + 1, command.indexOf('.')).replaceAll("\\s", "");
		
		// Název metody je od první tečky, za názvem reference na instanci třídy po první otevíraí závoku u názvu metody:
		final String methodName = command.substring(command.indexOf('.') + 1, command.indexOf('(')).replaceAll("\\s", "");
		
		
		// Parametry metody - mezi první otevárací a poslední zavírací závorkou:
		final String parametersText = command.substring(command.indexOf('(') + 1, command.lastIndexOf(')'));

		// Parametry metody - oddělené dle desetinné čárky - jednotlivé parametry:
		final String[] partsOfParameters = parametersText.split(",");
		
		
		
		if (objEditorVaribleData != null) {
			// Zde byly v pořádku získány data o proměnné:
			final Class<?> dataType = (Class<?>) objEditorVaribleData[1];
			
			if (dataType != null) {
				/*
				 * Proměnná, která obsahuje text metody, tj. veškerý texty za prvním rovnáse.
				 */
				final String textOfMethod = command.substring(command.indexOf('=') + 1);
				
				/*
				 * Zde nejprve otestuji, zda se jedná o zavolání statické metody, protože pokud
				 * ano, bude postup jiný, než u zavolání "klasické" metody nad nějakou instancí
				 * třídy.
				 */
				if (regEx.isCallStaticMethod(textOfMethod)) {
					/*
					 * Note:
					 * Zde bych mohl napsat podobnou metodu jako je metoda: 'makeCallStaticMethod',
					 * akorát upravit trochu tělo této metody a dát jej do nové metody, pak tuto
					 * novou metodu pouze volat odtud a z metody 'makeCallStaticMethod', ale to samé
					 * je v metodě 'getValueFromVar_Method_Constant', kterou používám pro rozpoznání
					 * parametrů metod, ale lze ji využít i takto, abych si ušetřil trochu práci,
					 * tak ji využiji pro zavolání příslušné metody abych si zde získal návratovou
					 * hodnotu a případně ji vložil do nové vytvořené proměnné.
					 */
					
					final String textOfMethodWithoutSemicolon = textOfMethod.substring(0, textOfMethod.lastIndexOf(';'));
					
					/*
					 * Proměnná, do které si vložím hodnoty z metody, která měla být zavolána.
					 */
					final ResultValue returnValues = getValueFromVar_Method_Constant(textOfMethodWithoutSemicolon, dataType);
					
					if (returnValues != null) {
						if (returnValues.isExecuted()) {
							// Zde alespoň jedna z metod proběhla, tak mohu vytvořit proměnnou a naplnit ji vrácenou hodnotou z metody:
							// Mohu vytvořít proměnnou a předat jí získanou hodnotu:
							// Vytvořím proměnnou a vložím ji do hasMapy s instancemi, a pokud se to povede, vrátím úspěch:
							final boolean isEditorVariableCreated = createEditorVariable(objEditorVaribleData,
									returnValues.getObjResult());
							
							// Pokud nedojde k chybě, potvrdím úspěšné dokončení operace:								
							if (isEditorVariableCreated) {
                                /*
                                 * Potřebuji zde aktualizovat okno s hodnotami pro automatické dokončení / doplnění
                                 * kódu / příkazů v editoru příkazů, jinak by v něm nebyly zobrazeny aktuální
                                 * hodnoty, resp. nebyla by v něm zobrazena tato nově vytvořená proměnná.
                                 */
                                updateValueCompletionWindow();

                                return txtSuccess;
                            }
						}
						return txtFailure;
					}
					/*
					 * Note:
					 * Zde bych mohl doplnit do výpisu něco jako že se nepodařilo získat hodnoty z
					 * metody apod. Ale ta metoda se v tomto případě alespoň pokusila zavolat, takže
					 * v případě chyby byly nějaké hodnoty vypsány.
					 */
					return txtFailure;
				}
				
				
				
				else {
					// Otestuji existenci instance třídy, na kterou ukazuje zadaná reference:
					final Object objInstance = Instances.getInstanceFromMap(referenceName);
					
					// Zde musím otestovat, zda se nejedná o zavolání metody pro získání hodnoty z listu:
					final VariableList<?> variableList = (VariableList<?>) Instances.getListFromMap(referenceName);
					
					
					if (objInstance != null) {
						// zde byla nalezna instance třídy, zavolám metodu:						
						
						// Zkusím zavolat zadanou metodu - pokud ji najdu, a vrácenou hodntou vložím do proměnné:
                        final Object[] objReturnData = callTheMethod(textOfMethod, objInstance, methodName,
                                dataType, partsOfParameters);

							//Z prvního místa v poli si zjistím, zda metody něco vrátily, takže mohu aktualizovat položku a z 
							// druhého místa v poli - pokud metody něco vrátliy - na prvním místě v poli bude true, tak na druhém je
                        // vrácena hodnota, kterou mohu vyplnit paroměnnou:

                        final boolean wasFilling = Boolean.parseBoolean(String.valueOf(objReturnData[0]));

                        if (wasFilling) {
                            // Zde alespoň jedna z metod proběhla, tak mohu vytvořit proměnnou a naplnit ji vrácenou
                            // hodnotou z metody:
                            // Mohu vytvořít proměnnou a předat jí získanou hodnotu:
                            // Vytvořím proměnnou a vložím ji do hasMapy s instancemi, a pokud se to povede, vrátím
                            // úspěch:
                            final boolean isEditorVariableCreated = createEditorVariable(objEditorVaribleData,
                                    objReturnData[1]);

                            // Pokud nedojde k chybě, potvrdím úspěšné dokončení operace:
                            if (isEditorVariableCreated)
                                return txtSuccess;

                            else return txtFailure + ": " + txtFailedToCreateVariable;
                        }

                        return txtFailure;
                    }
					
					
					
					else if (variableList != null
							&& regEx.isGetValueAtIndexOfList(command.substring(command.indexOf('=') + 1))) {

						// Zkusím zavolat zadanou metodu - pokud ji najdu, a
						// vrácenou hodntou vložím do proměnné:
                        final Object[] objReturnData = callTheMethod("", variableList, null, dataType,
                                partsOfParameters);

						return makeEditorVariable(objReturnData, objEditorVaribleData);
					}							
					else
						return txtFailure + ": " + txtReferenceDontPointsToInstanceOfClass
								+ OutputEditor.NEW_LINE_TWO_GAPS + txtReference + ": " + referenceName; 
				}
				

			}
			else return txtFailure + ": " + txtDoNotRetrieveDataTypeOfVariable;
		}
		else return txtFailure + ": " + txtFailedToGetDataAboutVariableWhileDeclaration;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytrvoří proměnnou v editoru příkazů
	 * 
	 * @param data
	 *            - data o metodě, která se měla zavolat - zda byla zavolána,
	 *            návratová hodnota, apod.
	 * 
	 * @param editorVariableData
	 *            - informace o proměnné, která se má deklarovat
	 * 
	 * @return Úspěch nebu Neúspěch, dle toho, zda se povede vytvořit proměnnou nebo
	 *         ne
	 */
	private String makeEditorVariable(final Object[] data, final Object[] editorVariableData) {
		// Nemělo by nastat, že by se vrátlo null, ale kvůli prevenci:
		if (data != null) {
			//Z prvního místa v poli si zjistím, zda metody něco vrátily, takže mohu aktualizovat položku a z 
			// druhého místa v poli - pokud metody něco vrátliy - na prvním místě v poli bude true, tak na druhém je
			// vrácena hodnota, kterou mohu vyplnit paroměnnou:
			
			final boolean wasFilling = Boolean.parseBoolean(String.valueOf(data[0]));
			
			if (wasFilling) {
				// Zde alespoň jedna z metod proběhla, tak mohu vytvořit proměnnou a naplnit ji vrácenou hodnotou z metody:
				// Mohu vytvořít proměnnou a předat jí získanou hodnotu:
				// Vytvořím proměnnou a vložím ji do hasMapy s instancemi, a pokud se to povede, vrátím úspěch:
				final boolean isEditorVariableCreated = createEditorVariable(editorVariableData, data[1]);													
				
				// Pokud nedojde k chybě, potvrdím úspěšné dokončení operace:								
				if (isEditorVariableCreated)
					return txtSuccess;
				
				else return txtFailure + ": " + txtFailedToCreateVariable;				
			}
			
			return txtFailure;
		}
		return txtFailure;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


	@Override
	public String makePrintValueLiteral(final String command) {
		// Syntaxe: print(values + values 2 + ...);
		
		// Postup:
		// Nejprve se vezmu "text" mezi první otevírací a poslední zavírací závorkou = hodnoty pro výpis
		// Dále je rozdělím dle symbolu plus a otestuji jednotlivé hodnoty, dále (pokud se povede získat hodnotu),
		// přidám ji do konečného výsledku pro výpis
		// až takto otestuji všechny hodnoty vrátím konečnou hodnotu
		
		final String textOfValues = command.substring(command.indexOf('(') + 1, command.lastIndexOf(')')).trim();
		
		
		final String[] partsOfValues = textOfValues.split("\\+");
		
		
		// Proměnná, do které vložím konečný výpis do editoru výstupů:
		String finalResult = "";
		
		
		for (final String s : partsOfValues) {
			// Otestuji, zda se text nerovná bílým znakům:
			if (!regEx.isWhiteSpace(s)) {
				
				final ResultValue objResultValue = getValueFromVar_Method_Constant(s, null);
				
				/*
				 * Pokud se nevrátila žádná hodnota (nemělo by nastat) nebo pokud se
				 * nerozpoznala operace, pak budu pokračovat další iterací cyklu, protože zde
				 * nemám žádnou hodnotu, kterou bych mohl přiřadit do výsledku, navíc info
				 * ohledně nastalé chyby by již mělo být vypsáno v editoru výstupů při provádení
				 * příslušné operace, napřílad provádění metody, získávání hodnoty z proměnné
				 * apod.
				 */
				if (objResultValue == null || !objResultValue.isExecuted())
					continue;
				
				
				
				
				
				
				/*
				 * Otestuji, zda se mají zvýraznit nějaké instance / buňky v diagramu instancí.
				 * 
				 * Toto provádím v každé iteraci pro každou získanou hodnotu zvlášť, i když bych
				 * mohl v této části kódu místo volání metody checkHighlightOfInstances nejprve
				 * zjistit, co je to za objekt a dle toho zjistit zda obsahuje instanci třídy z
				 * diagramu tříd, která se aktuálně nachází v diagramu instancí, ale to bych
				 * musel udělat tak, že bych níže musel upravit úplně všechny metody, tj. pokud
				 * je to "klasická" proměnná, tak zjistit, zda je to instance a nachází se i v
				 * diagramu instancí, pokud je to pole, tak jej celé projít a u každé položky
				 * zjistit, zda je to instance atd. pro všechny typy položek (těch testovaných).
				 * 
				 * Ale další problém by byl, že by to bylo pomalé, protože pro každou iteraci
				 * bych musel projít všechny instance v mapě a hledat jejich reprezentaci v
				 * diagramu instancí, což by bylo na každou iteraci tohoto cyklu další dvě
				 * iterace pro všechny objekty v diagramu instancí a mapu s instancemi, navíc
				 * pokud by bylo třeba iterovat pole, ještě by se to "protáhlo".
				 * 
				 * Tímto způsobem, tj. voláním metody checkHighlightOfInstances jsem si
				 * zajistil, že vše výše popsané o tom, zda se jedná o instanci, je v daigramu
				 * instancí ... se bude zjišťovat v externím vlákně, jediná nevýhoda je, že se
				 * nebudou zobrazovat úplně najednou, tj. ve stejný okamžit, takže když zde bude
				 * například 20 objektů, tak už bude znát, že se to zvýraznění trochu
				 * "protáhne", tj. budou se zobrazovat jakoby postupně, ale spíše to je i trochu
				 * hezčí mi přijde.
				 */ 
				instanceDiagram.checkHighlightOfInstances(objResultValue.getObjResult());
				
				
				
				
				
				/*
				 * Pokud se mají vypisovat instance i s referencemi, tak zavolám pouze metodu v
				 * následující podínce (getReturnValueWithReferenceVariableForPrint) a ta už si
				 * zjistí, zda je to pole, list, ... a dle toho si zjistí, zda se jedná o
				 * instanci nějaké třídy z diagramu tříd, která má svou reprezentaci v diagramu
				 * instancí a pokud ano, tak do proměnné finalResult přidá příslušnou hodnotu i
				 * s referencí.
				 */
				if (OutputEditor.isShowReferenceVariables()) {
					// Výpis i s referenční proměnnou (pokud je to instance v diagramu instancí):
					finalResult += OperationsWithInstances
							.getReturnValueWithReferenceVariableForPrint(objResultValue.getObjResult());
					
					// pokračuji další iterací, výše se přidali potřebné hodnoty:
					continue;
				}

				
				
				
				
				
				// Pokud se jedná o text v syntaxi:
				// Syntaxe: "some text"
				// Tak musím odebrat uvozovky, aby se nevypsaly do konečného výsledku:
				if (objResultValue.getObjResult() != null && regEx.isTextInQuotes(s))
					// Zde se jedná o text v uvozovkách, tak ho odeberu:
					// Nebli do konečného výsledku přídám text mezi první a poslední uvozovkou:
					finalResult += s.substring(s.indexOf('"') + 1, s.lastIndexOf('"'));
				
				
				
				// Otestuji, zda se jedná o jednorozměrné pole pomocí metody isArray nad .class objektu,
				// pokud se jedná o pole musím ho převést na string, pro výpis hodnot,
				// takže "projedu" cyklem načtené pole v podobně objektu, všechny položky vložím do kolekce
				// a to převedu do pole a následně do textu:		

				else if (objResultValue.getObjResult() != null && objResultValue.getObjResult().getClass().isArray())
					finalResult += fromArrayToString(objResultValue.getObjResult());
					
					
				
				
				// Zde se nejedá o text v uvozovkách, tak ho normálné přidám:
				else finalResult += objResultValue.getObjResult();
			}
		}
		
		
		
		return txtSuccess + ": " + finalResult;
	}


	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která převede X - rozměrné pole, které se nachází v proměnné typu
	 * Object (v parametru) do podoby textu - Stringu, abych mohl jeho hodnotu
	 * vytisknout do editoru výstupů
	 * 
	 * @param objValue
	 *            - proměnná typu Object, ve které se nachází X - rozměrné pole
	 * 
	 * @return pole ve výše uvedené proměnné převedené do textu - Stringu
	 */
	public static String fromArrayToString(final Object objValue) {
		final int countOfDim = getCountOfDimensionsOfArray(objValue.getClass());

		if (countOfDim == 1)
			// Toto nemohu "provést" s primitivními datovými typy v objValue:
//			return Arrays.toString((Object[]) objvalue);
			/*
			 * Jelikož hodnota v objValue může být pole jednoho z primitivních datový typů,
			 * takže nemohu prostě přetypovat primitivní datový typ na objektový, proto jsem
			 * si pro tyto účely připravil metodu 'convertToObjectArray', která otestuje,
			 * zda se jedná o pole které je jednoho z primitivních datový typů a pokud ano,
			 * pak jej převede na pole objektů, jinak prostě vrátí pole objektů, které je
			 * možné převést do textu.
			 */
			return Arrays.toString(ParseToJavaTypes.convertToObjectArray(objValue));

		
		/*
		 * Zde mohu "převést" více rozměrné pole do textové podoby například pro vypsání
		 * někde do editoru apod. (Dle konkrétní potřeby).
		 * 
		 * Jelikož objValue může být X - rozměrné pole jak primitivního tak i
		 * objektového datové typu, tak jej nelze převést na pole objektů, což je důvod
		 * proč se výše využívá metoda 'convertToObjectArray', ale v případě více
		 * rozměrného pole lze využít následující metodu 'deepToString', i v případě, že
		 * je hodnota v objValue i pole primitivního datového typu.
		 * 
		 * Už jsem vyzkoušel, že to funguje, ale kdyby to náhodou z nějakého důvodu
		 * nešlo, tak je možné využít následující zakomantovaný kód i s příslušnou
		 * metodou, která je níže také zakomentována. jedná se o metodu, která slouží
		 * pro převod více rozměrného pole do textové podoby.
		 */
		return Arrays.deepToString((Object[]) objValue);
		
		
		/*
		 * Následují kód pro převod více rozměrného pole do podoby textu (spíše
		 * "nouzová" varianta, kdyby nefungovalo výše deepToString.
		 */
		
//		tempForArray = "[";
//
//		convertArrayIntoText(objvalue);
//
//		if (tempForArray.endsWith(", "))
//			tempForArray = tempForArray.substring(0, tempForArray.length() - 2);
//
//		tempForArray += "]";
//
//		return tempForArray;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
//	/**
//	 * Proměnná, která slouží pro "dočasné" uložení textu. Jde o to, že když metoda
//	 * convertArrayIntoText "přepisuje" více rozměrné pole, resp. jeho položky do
//	 * podoby textu, tak je někam potřeba "odkládat" již vypsáné položky v příslušné
//	 * syntaxi, proto zde je tato proměnná.
//	 */
//	private static String tempForArray;
//	
//	/**
//	 * Metoda, která slouží pro "převod" více rozměrného pole libovolného datového
//	 * typu (jak primitivního tak objektového) do podoby textu - například pro
//	 * vypsání do editoru apod.
//	 * 
//	 * @param objArray
//	 *            - O této hodnotě už se musí vědět, že není null a je to X -
//	 *            rozměrné pole.
//	 */
//	private static final void convertArrayIntoText(final Object objArray) {
//		/**
//		 * Získám si velikost pole, abych jím mohl iterovat.
//		 */
//		final int length = Array.getLength(objArray);
//
//		
//		// Projdu celé pole:
//		for (int i = 0; i < length; i++) {
//			/**
//			 * Získám si hodnotu v poli na příslušném indexu.
//			 */
//			final Object objValue = Array.get(objArray, i);
//
//			/**
//			 * Pokud je ta hodnota null, pak nemá smysl pokračovat, prostě do textu přidám
//			 * null a desetinnou čárku pro oddělení hodnot a pokračuji další iterací.
//			 */
//			if (objValue == null) {
//				tempForArray += "null, ";
//				continue;
//			}
//
//			
//			/**
//			 * V případě, že se jedná o další pole / dimenzi, pak si do textu přidám
//			 * otevírací hranatou závorku a projdu celé pole rekuzí. Pak stačí jen
//			 * otestovat, zda se na konci toho textu nachází desetinná čárka, pokud ano, tak
//			 * ji odebrat a přidat uzavírací hranatou závorku.
//			 */
//			else if (objValue.getClass().isArray()) {
//				tempForArray += "[";
//				
//				// Rekurzí projdu celé pole / další dimezi:
//				convertArrayIntoText(objValue);
//
//				if (tempForArray.endsWith(", "))
//					tempForArray = tempForArray.substring(0, tempForArray.length() - 2);
//
//				tempForArray += "], ";
//			}
//
//			/**
//			 * Zde se jedná o "nějakou" hodnotu v příslušném poli na příslušném indexu,
//			 * která není null, tak ji pouze přidám do textu.
//			 */
//			else
//				tempForArray += objValue + ", ";
//		}
//	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	


	@Override
	public String makeCreateAndFullfillmentList(final String command) {
		// Note: Zde nemusím hlídat, zda má být list final nebo ne, zde se list msí rovnou deklarovat, takže 
		// se muže naplnit prázdný, ale tak s hodnotami, ať tak či tak, vytvoří se a pokud bude final,
		// tak s ním nepůjde teémř nic dělat - až na výpisy a získávání hodnot
		
		// parametr se může nacházet v syntaxi:
		// (final) List<DataType> variableName = new ArrayList<>(SomeParameters); nebo bez parametrů
		
		// Pro začátek si zjistím, datový typ hodnot, které lze do daného listu vložit:
		
		// Abych nemusel zjišťovat "složitě" druhý výskyt znaku: '>', abych zjistil datový typ:
		// tak si text rozdělím dle rovná se, a vezmu si text mezi <> - což bud edatový typ, který potřebuji
		
		
		/*
		 * Proměnná, do které si vložím text příkazu od začátku po první rovná se.
		 */
		final String textBeforeEquals = command.substring(0, command.indexOf('='));
				
		
		
		// Pro začátek zjistím, zda má být kolekce final nebo ne:
		final boolean isFinal;

		isFinal = regEx.isDeclarateListWithFinal(textBeforeEquals);
		
		
		
		
		// Nyní si mohu zjistit název proměnné, pod kterým se bude přistupovat k danému listu:
		// název proměnné se nachází v poli: partsOfCommand na prvním indexu od posledního '>' do konce:
		final String variableName = textBeforeEquals.substring(textBeforeEquals.lastIndexOf('>') + 1).replaceAll("\\s", "");
						
		
		
		// Do následující proměnné si vložím datový typ hodnot, které půjdou do dané kolekce vkládat:
		final String dataTypeOfList = textBeforeEquals.substring(textBeforeEquals.indexOf('<') + 1, textBeforeEquals.lastIndexOf('>'));
		
		final Class<?> dataTypeForList = getClassType(dataTypeOfList.replaceAll("\\s", ""));
		
		
		// Nyní si mohu zjistit parametry (hodnota), které se mají příipadně vložit do dané kolekce při vytvoření:
		// Stačí si vzít text mezi prní otevírací kulatou závrokou a poslední zavírací kulatou závorkou, a rozělit je dle desetinných čárek:
		final String textOfParameters = command.substring(command.indexOf('(') + 1, command.lastIndexOf(')'));
		final String[] partsOfParameters = textOfParameters.split(",");
		
		
		final List<Object> parametersList = new ArrayList<>();
		
		
		// Nejprve otestuji, zda název listu není kličové slovo Javy:
		if (!isKeyWordOfJava(variableName)) {
			if (!Instances.isReferenceNameInMap(variableName)) {
				if (dataTypeForList != null) {
					for (final String s : partsOfParameters) {
						// Otestuji, zda se nejedná o výraz s bílímy znaky:
						if (!regEx.isWhiteSpace(s)) {
							// Nove:
							/*
							 * Získám hodnoty dle aktuálně iterovaného zadaného parametru (syntxe), pokud se
							 * rozpozná a vrátí se příslušná hodnota, jaká by měla.
							 */
							final ResultValue objResultValue = getValueFromVar_Method_Constant(s.trim(), dataTypeForList);
							
							/*
							 * Otestuji, zda se nějaká hodnota vůbec vrátila (měla by vždy) a zda byla
							 * syntaxe rozpznána a přílsušná operace provedena, pokud ne, pak budu
							 * pokračovat následující iterací cyklu s tím, že byla informace ohldně nastalé
							 * chyba již vypsána do editoru výstupů, takže by o ní uživatel už měl vědět.
							 */
							if (objResultValue == null || !objResultValue.isExecuted())
								continue;

							/*
							 * Zde se příslušná operace provedal a i kdyby se vrátila null hodnota, tak ji
							 * prostě do výsledného listu přídám, třeba je to i úmysl.
							 */
							parametersList.add(objResultValue.getObjResult());
						}							
					}
						
					
					// Nyní mám v kolekci: parametersList přetypované hodntoy získané z editoru příkazů,
					// takže mohu vytvořit příslušnou kolekci, a dát do ní hodnoty
					
					// Resp, mohu vytvořit příslušný objekt, na který se bude odkazovat přes danou proměnnou:
					
					Instances.addInstanceToMap(variableName, new VariableList<>(parametersList, isFinal, dataTypeForList));

                    /*
                     * Výše se vytvořil nový list, tak je třeba aktualizovat hodnoty v okně pro automatické
                     * doplňování / dokončování příkazů v editoru příkazů, jinak by v něm nebyl na výběr výše
                     * vytvořený list.
                     */
                    updateValueCompletionWindow();

					// Pokud se dostanu až sem, tak mohu vrátit Úspěch, protože nenastala žádná chyba a v tuto chvíli mám vytvořenou 
					// proměnnou a je přidáná do mapy, kde k ním může uživatel přistupovat
					return txtSuccess;
				}
				else return txtFailure + ": " + txtDataTypeForListNotFound;
			}
			else
				return txtFailure + ": " + txtVariableNameIsNeedToChangeIsAlreadyTaken + OutputEditor.NEW_LINE_TWO_GAPS
						+ txtVariable + ": " + variableName;
		}
		else
			return txtFailure + ": " + txtVariableNameMustNotBeKeyWord + OutputEditor.NEW_LINE_TWO_GAPS + txtName + ": "
					+ variableName;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která se pokusí zjistit, o jaký datový typ se jedná a vrátí ho v
	 * podobně typu Class<?>
	 * 
	 * například bude v parametru zadáno String, tak se vrátí String.class, apod.
	 * pro ostatní primitivní a základní objektové datové typy
	 * 
	 * Note: java.util.List podporuje pouze objektové datové typy. Proto tato metoda
	 * vrátí pouze objektové datové typy.
	 * 
	 * @param textOfClassType
	 *            - text, ve kterém se nachází datový typ, jeho Class type chci
	 *            vrátit, resp. získat
	 * 
	 * @return Class<?> pokud se najde správny datový typ nebo null - pokud se
	 *         nenajde datový typ
	 */
	private static Class<?> getClassType(final String textOfClassType) {		
		
		if (textOfClassType.equals("Byte"))
			return Byte.class;		
		
		
		else if (textOfClassType.equals("Short"))
			return Short.class;		
		
		
		else if (textOfClassType.equals("Integer"))
			return Integer.class;
				
		
		else if (textOfClassType.equals("Long"))
			return Long.class;
		
		
		else if (textOfClassType.equals("Float"))
			return Float.class;
		
		
		else if (textOfClassType.equals("Double"))
			return Double.class;
		
				
		else if (textOfClassType.equals("Character"))
			return Character.class;
		
		
		else if (textOfClassType.equals("Boolean"))
			return Boolean.class;
		
		
		else if (textOfClassType.equals("String"))
			return String.class;
		
		
		return null;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která se pokusí přetypovat zadaný text coby index pozice hodnoty v
	 * listu, který byl zadán uživatelem v editoru příkazů na Integer, abych z něj
	 * mohl získat hodnotu.
	 * 
	 * @param indexInText
	 *            - index hodnoty v listu, který se má přetypovat na Integer
	 * 
	 * @return -1, pokud se zmíněný operace nepovede, jinak zadaný index v datovém
	 *         typu Integer
	 */
	private static int getIndexOfList(final String indexInText) {
		try {
			return Integer.parseInt(indexInText);

		} catch (NumberFormatException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka pří parsování zadaného indexu pro získání hodnoty ze zadaného indexu v " +
                                "listu: "
                                + indexInText
                                + ", na Integer, třída MakeOperation.java, metoda: getIndexOfList. Tato chyba může " +
                                "nastat v případě, že je index zadán v chybné syntaxi nebo je mimo rozsah datového " +
                                "typu Integer apod.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			
			return -1;
		}
	}
	
	
	
	
	
	
	
	
	
	


	@Override
	public String makeAddValueToEndOfList(final String command) {
		// Jedná se o syntaxi: variableName.add(specificValue);
		
		// Postup:
		// od začátku po první textu si zjisím název proměnné pro list,
		// pak od první otevírací závorky po poslední zavárací závorku si zjistím hodnotu
		// a pokud nedojde k chybě zkusím zjistit danou hodnotu a vložit ji do 
		
		// Název proměnné coby reference na daný list:
		final String variableName = command.substring(0, command.indexOf('.')).replaceAll("\\s", "");
		
		// hodnota, která se má vložit na konec listu:
		final String valueForInsert = command.substring(command.indexOf('(') + 1, command.lastIndexOf(')'));
		
		
		
		
		// Načtu si list, který potřebuji:		
		final VariableList<?> variableList = (VariableList<?>) Instances.getListFromMap(variableName);
		
		// Otestuji, zda byl načten:
		if (variableList != null) {

			// Pokusím se přetypovat hodnotu na příslušný datový typ kolekce:
			final ResultValue objResultValue = getValueFromVar_Method_Constant(valueForInsert.trim(),
					variableList.getClassType());

			/*
			 * Pokud se nevrátila žádná hodnota (nemělo by nastat) nebo pokud se
			 * nerozpoznala operace, pak vrátím 'Neúspěch' s tím, že by měla již být
			 * informace o nastalé chybě (důvodu proč se nevykonala nějaká opoerace nebo
			 * nerozponal výraz) již vypsána do editoru výstupů.
			 */
			if (objResultValue == null || !objResultValue.isExecuted())
				return txtFailure;
											
				
				// Otestuji, zda se povedlo přetypování:
			if (objResultValue.getObjResult() != null) {
				// Zde se podařilo přetypovat hodnotu na datový typ proměnné nebo byla daná
				// hodnota získána, apod.
				// mohu ji vložit na konec kolekce:

                final boolean result = variableList.addValueToEndOfList(objResultValue.getObjResult());

                /*
                 * Aktualizace okna pro automatické doplňování / dokončován příkazů v editoru příkazů, výše se
                 * přidala nová hodnota na konec listu, tak je třeba aktualizovt list, aby se v tom okně změny
                 * projevily.
                 */
                updateValueCompletionWindow();

				return txtSuccess + ": " + result;
			} 
			else
				return txtFailure + ": " + txtFailedRetypeValue + OutputEditor.NEW_LINE_TWO_GAPS + txtValue + ": '"
						+ valueForInsert + "' " + txtToDataType + ": " + variableList.getClassType();
		} 
		else
			return txtFailure + ": " + txtReferenceDontPointToInstanceOfList + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtReference + ": " + variableName;
	}


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


	
	

	@Override
	public String makeAddValueToIndexAtList(final String command) {
		// Syntaxe: var.add(index, specificValue);
		
		// Postup:
		// Zjistím si, zda existuje list, pod názvem reference, pak ověřím, zda je zadaný index "správný" - nula a více, ale menší jak velikost
		// dané kolekce, a pak se pokudsím získat hodnotu a vložit jí do kolekce, případně někde kde dojde k nějaké chybě vypíšu
		// "chybové oznámění" o chybvě, ke které došlo:
		
		
		// název reference = od začátku po první tečku:
		final String variableName = command.substring(0, command.indexOf('.')).replaceAll("\\s", "");
		
		// index = od první otevírací závorku po první desetinnou čátku:
		final String indexOfList = command.substring(command.indexOf('(') + 1, command.indexOf(',')).replaceAll("\\s", "");
		
		// hodnota = od první desetinné čárky po poslední zavárací závorku:
		final String value = command.substring(command.indexOf(',') + 1, command.lastIndexOf(')'));
		
		
		// Otestuji, zda existuje list pod daným názvem reference:
		final VariableList<?> variableList = (VariableList<?>) Instances.getListFromMap(variableName);
		
		if (variableList != null) {
			
			// Zde byl nalezen hledany list, takže už vím, že mohu přidat
			// proměnnou, nyní otestuji index, na které se má přidat
			final int index = getIndexOfList(indexOfList);

			if (index > -1) {

				if (index < variableList.getList().size()) {
					// Zde je index ve správném rozsahu, mohu si zjistit hodnotu:
					final ResultValue objResultValue = getValueFromVar_Method_Constant(value.trim(),
							variableList.getClassType());

					/*
					 * Pokud se nevrátila žádná hodnota (nemělo by nastat) nebo pokud se
					 * nerozpoznala operace, pak vrátím 'Neúspěch' s tím, že by měla již být
					 * informace o nastalé chybě (důvodu proč se nevykonala nějaká opoerace nebo
					 * nerozponal výraz) již vypsána do editoru výstupů.
					 */
					if (objResultValue == null || !objResultValue.isExecuted())
						return txtFailure;

					if (objResultValue.getObjResult() != null) {
						// Zde se podařilo získat hdonotu, tak ji mohu
						// vložiz na index v kolekci - listu:
						variableList.addValueToIndexAtList(index, objResultValue.getObjResult());

                        /*
                         * Výše byla doplněna nová hodnota do listu, tak je třeba aktualizovat okno pro automatické
                         * doplňování / dokončování příkazů / hodnot v editoru příkazů, jinak by se v tom okně
                         * zobrazovaly chybné hodnoty.
                         */
                        updateValueCompletionWindow();

						return txtSuccess;
					}
					// Tato část else by už neměla být potřeba:
					else
						return txtFailure + ": " + txtFailedRetypeValue + OutputEditor.NEW_LINE_TWO_GAPS + txtValue
								+ ": '" + value + "' " + txtToDataType + ": " + variableList.getClassType();
				} 
				else
					return txtConditionsForIndexOfList + OutputEditor.NEW_LINE_TWO_GAPS + txtSpecifiedIndexOfList + ": "
							+ index + ", " + txtSizeOfTheList + ": " + variableList.getList().size();
			}
			return txtFailure + ": " + txtIndexOfListIsOutOfRange;
		} 
		else
			return txtFailure + ": " + txtReferenceDontPointToInstanceOfList + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtReference + ": " + variableName;
	}





	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	




	@Override
	public String makeClearList(final String command) {
		// Syntaxe: referenceTolist.clear();
		
		// Postup:
		// zjistím si název reference na kolekce, otestuji, zda existuje a pokusím se na ní zavolat metodu clear,
		// pokud se to povede vrátím Úspěch, pokud ne, vypíše se chybové oznámení s informací o chybě, ke které došlo:
		
		
		// Název reference = od začátku po první tečku:
		final String variableName = command.substring(0, command.indexOf('.')).replaceAll("\\s", "");
		
		
		// Otestuji, zda zadaný list existuje:
		final VariableList<?> variableList = (VariableList<?>) Instances.getListFromMap(variableName);

		if (variableList != null) {

			// Zde list existuje, pokusím se nad ním zavolat metodu clear:
			variableList.clearList();

            /*
             * Zde je třeba aktualizovat okno pro automatické doplňování / dokončování příkazů v editoru příkazů,
             * výše se vymazal obsah listu, tak aby se v tom okně změny projevily, jinak by se v tom okně zobrazovaly
             * pořád předchozí stav / obsah listu.
             */
            updateValueCompletionWindow();

			// pokud se dostanu sem, kolekce se vymazala, mohu vrátit Úspěch
			return txtSuccess;
		}
		else
			return txtFailure + ": " + txtReferenceDontPointToInstanceOfList + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtReference + ": " + variableName;
	}




	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	




	@Override
	public String makeGetValueAtIndexOfList(final String command) {
		// Syntaxe: referenteToList.get(index);
		
		// Zkusím získat hledanou instnaci třídy VariableList:
		final VariableList<?> variableList = getVariableListInstance(command);
		
		if (variableList != null) {
			// Zde se podařilo najít hledanou instnaci, zkusím zjistit, zda je index ve správném intervalue
			final String indexInText = command.substring(command.indexOf('(') + 1, command.lastIndexOf(')'))
					.replaceAll("\\s", "");

			final int index = getIndexOfList(indexInText);

			if (index > -1) {
				// Nyní musím otestovat, zda je index v rozsahu hodnot kolekce:
				final int sizeOfList = variableList.getList().size();

				if (index < sizeOfList) {
					// Zde jsou hodnoty v intervalu, tak se mohuu pokusit získat
					// hodnotu:
					final Object objValue = variableList.getValueAtIndex(index);

					if (objValue != null)
						return txtSuccess + ": " + objValue;

					else
						return txtValueAtIndexIsNull + OutputEditor.NEW_LINE_TWO_GAPS
								+ txtValueOfVariableWillNotBeCounted;
				} 
				else
					return txtConditionsForIndexOfList + OutputEditor.NEW_LINE_TWO_GAPS + txtSpecifiedIndexOfList + ": "
							+ index + ", " + txtSizeOfTheList + ": " + sizeOfList;
			} 
			else return txtFailure + ": " + txtIndexOfListIsOutOfRange;
		}
		// Pokud se dostanu sem, tak nebyla nalezena instance zadané třídy, vypíšu o tom hlášku do editoru výstupů:
		else return txtFailure + ": " + txtInstanceOfListWasNotFoundUnderReference;
	}






	
	
	
	
	
	
	
	
	
	
	
	




	@Override
	public String makeSetValueAtIndexOfList(final String command) {
		// Syntaxe: referenceToList.set(index, value);
		
		// Zjistím si, zda existuje list coby instance třídy VariableList na kterou má ukazovat zadaná reference:
		// název reference je od začátku po první tečku:
		final String referenceName = command.substring(0, command.indexOf('.')).replaceAll("\\s", "");
		
		// Index je od první otevírací závorky po první desetinnou čárku:
		final String indexInText = command.substring(command.indexOf('(') + 1, command.indexOf(',')).replaceAll("\\s", "");
		
		
		// Hodnota, která se má vložit na zadaná index je od první desetinné čárky po poslední zavírací závorku:
		final String valueToInsert = command.substring(command.indexOf(',') + 1, command.lastIndexOf(')'));
		
		
		
		final VariableList<?> variableList = (VariableList<?>) Instances.getListFromMap(referenceName);
		
		if (variableList != null) {

			// Zde jsem našel instanci listu, zjistím si index:
			final int index = getIndexOfList(indexInText);

			if (index > -1) {
				final int listSize = variableList.getList().size();

				if (index < listSize) {
					// Zde se index nachází ve správném rozsahu, mohu si zjistit hodnotu:
					final ResultValue objResultValue = getValueFromVar_Method_Constant(valueToInsert.trim(),
							variableList.getClassType());

					/*
					 * Pokud se nevrátila žádná hodnota (nemělo by nastat) nebo pokud se
					 * nerozpoznala operace, pak vrátím 'Neúspěch' s tím, že by měla již být
					 * informace o nastalé chybě (důvodu proč se nevykonala nějaká opoerace nebo
					 * nerozponal výraz) již vypsána do editoru výstupů.
					 */
					if (objResultValue == null || !objResultValue.isExecuted())
						return txtFailure;

					if (objResultValue.getObjResult() != null) {
						// Zde se podařilo získat hdonotu, tak ji mohu
						// vložit na index v kolekci - listu:

                        final Object value = variableList.setValueToIndex(index, objResultValue.getObjResult());

                        /*
                         * Aktualizace okna pro automatické doplňování / dokončování příkazů v editoru příkazů, aby
                         * se v něm projevila výše nastavená hodnota v listu.
                         */
                        updateValueCompletionWindow();

						return txtSuccess + ": " + value;
					}
					// Tato část else by neměla být potřeba:
					else
						return txtFailure + ": " + txtFailedRetypeValue + OutputEditor.NEW_LINE_TWO_GAPS + txtValue
								+ ": '" + valueToInsert + "' " + txtToDataType + ": " + variableList.getClassType();
				}

				else
					return txtConditionsForIndexOfList + OutputEditor.NEW_LINE_TWO_GAPS + txtSpecifiedIndexOfList + ": "
							+ index + ", " + txtSizeOfTheList + ": " + listSize;

			} 
			else
				return txtFailure + ": " + txtIndexOfListIsOutOfRange;
		}
		else
			return txtFailure + ": " + txtInstanceOfListWasNotFoundUnderReference + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtReference + ": " + referenceName;
	}




	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


	@Override
	public String makeListToOneDimensionalArray(final String command) {
		// Syntaxe: referenceToList.toArray();
		
		// Zjistím si název pole, otestuji, zda existuje, a zavolám metodu, která 
		// daný list převede na pole a to se vypíše do editoru výstupů.
		
		// Nejprve si zjistím název - referenci na list = od začátku po první tečku
		final String referenceName = command.substring(0, command.indexOf('.')).replaceAll("\\s", "");
		
		// Zde již vím, podle regulárních výrazů, že mám převést list do pole, tak už potřebuji pouze zjistit,
		// zda daný list pod daným názem - referenci existuje
		
		final VariableList<?> variableList = (VariableList<?>) Instances.getListFromMap(referenceName);		
		
		
		if (variableList != null)
			// Zde jsem našel hledaný list, tak ho mohu převést do pole a zrovna pole do textu, v této metodě ho vypísu:			
			return txtSuccess + ": " + Arrays.toString(variableList.getArrayFromList());
			
		
		else
			return txtFailure + ": " + txtInstanceOfListWasNotFoundUnderReference + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtReference + ": " + referenceName;
	}



	
	
	
	
	
	
	
	
	
	
	
	



	@Override
	public String makeListSize(final String command) {
		// Syntaxe: referenceToList.size();
		
		// Nejprve si zjistím, zda zadaný list vůbec existuje:
		// Od začátku po první tečku:
		final String referenceName = command.substring(0, command.indexOf('.')).replaceAll("\\s", "");
		
		final VariableList<?> variableList = (VariableList<?>) Instances.getListFromMap(referenceName);
		
		
		if (variableList != null)
			// Zde byla nalezena požadovaná kolekce, mohu zavolat metodu a vrátit její délku / velikost			
			return txtSuccess + ": " + variableList.getList().size();
		
		else
			return txtFailure + ": " + txtInstanceOfListWasNotFoundUnderReference + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtReference + ": " + referenceName;
	}


	
	
	
	
	
	
	
	
	
	
	
	
	
	

	@Override
	public String makeRemoveValueAtIndexOfList(final String command) {
		// Syntaxe: referenceToList.remove(index);
		
		// název reference / List:
		final String referenceName = command.substring(0, command.indexOf('.')).replaceAll("\\s", "");
		
		// index, ze kterého se má smazat hodnota (od první otevírací závorky do poslední):
		final String indexInText = command.substring(command.indexOf('(') + 1, command.lastIndexOf(')')).replaceAll("\\s", "");
		
		final int index = getIndexOfList(indexInText);

		if (index > -1) {
			final VariableList<?> variableList = (VariableList<?>) Instances.getListFromMap(referenceName);

			if (variableList != null) {
				// Zde byl list nalezen, mohu si zjistit délku pro otestování instance:
				if (index < variableList.getList().size()) {
					// Zde byl index nalezen, mohu smazat hodnotu na daném indexu:

                    final Object removedValue = variableList.removeValueAtIndex(index);

                    /*
                     * Aktualizace okna pro automatické doplňování / dokončování příkazů v editoru příkazů, aby se v
                     * tom okně provily změny, resp. aby se v tom okně zobrazil list s aktuálními hodnotami, s výše
                     * vymazanou hodnotou.
                     */
                    updateValueCompletionWindow();

					return txtSuccess + ": " + removedValue;
				} 
				else
					return txtConditionsForIndexOfList + OutputEditor.NEW_LINE_TWO_GAPS + txtSpecifiedIndexOfList + ": "
							+ index + ", " + txtSizeOfTheList + ": " + variableList.getList().size();
			}
			else
				return txtFailure + ": " + txtInstanceOfListWasNotFoundUnderReference + OutputEditor.NEW_LINE_TWO_GAPS
						+ txtReference + ": " + referenceName;
		}
		else
			return txtFailure + ": " + txtIndexOfListIsOutOfRange;
	}



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	@Override
	public String makeDeclarationAndFullfillmentArray(final String command) {
		// Syntaxe: (final) dataType (výše)[] variableName = new dataType[] {values1, values2, ...};
		
		/*
		 * Note:
		 * Způsob rozdělení dle rovná se nepřipadá v úvahu, protože pokud se do
		 * parametru pole coby hodnota pro naplnění zadá rovnáse, pak příkaz selže.
		 */
		
		/*
		 * Proměnná, do které si vložím text příkazu od začátku po první rovná se.
		 */
		final String textBeforeEquals = command.substring(0, command.indexOf('='));
		
		/*
		 * Proměnná, do které si vložím text příkazu od rovná se do konce.
		 */
		final String textAfterEquals = command.substring(command.indexOf('=') + 1);
		
		
		// Logická hodnota o tom, zda má být pole final nebo ne:
		final boolean isFinal = isArrayFinal(textBeforeEquals);
		
		// Datový typ pole v textu - získaný z příkazu:
		final String dataTypeOfArrayInText = getDataTypeOfArrayInText(textBeforeEquals, isFinal);
		
		// Zde si zjistím datový typ z výše zadaného textu:
		final Class<?> dataType = getClassTypeOfArray(dataTypeOfArrayInText);
		
		
		// Název promenné = od první uzavírací hranaté závorky po první rovnáse:
		final String referenceName = command.substring(command.indexOf(']') + 1, command.indexOf('=')).replaceAll("\\s",
				"");
		
		// Otestuje, zda uživatel zadal stejné datové typy:
		final boolean areDataTypesIdenticaly = areDataTypesIdenticaly(dataType, textAfterEquals);
		
		// Zjistím text parametrů od první otevírací špičaté závorky po poslední zavírací špičatou závorku v druhé
		// částí příkazu - za rovnáse
		final String parametersInText = textAfterEquals.substring(textAfterEquals.indexOf('{') + 1,
				textAfterEquals.lastIndexOf('}'));
		
		// rozdělím si parametry dle desetinné čárky:
		final String[] parameters = parametersInText.split(",");
		
		
		// Otestuji, zda není název pole klíčové slovo Javy:
		if (!isKeyWordOfJava(referenceName)) {
			// Pro začátek otestuji, zda se zadaný název proměnné ještě nevyskytuje v instancích:
			if (!Instances.isReferenceNameInMap(referenceName)) {
				// Otetuji datový typ, zda byl zadán jeden z "možných" datových typů:
				if (dataType != null) {
					// Otestuji, zda jsou datové typy pro deklaraci pole a napplnění pole stejné, tj.
					// dataType1[] var = new dataType2[]{};   aby se dataType1 a 2 rovnaly:
					if (areDataTypesIdenticaly) {
						// Zde znám datový typ hodnot, který může pole obsahovat a je zadána správná syntaxe pro vytvoření, nyní si mohu
						// zkusit zjistit hodnoty, kterými se má naplnit pole:
						
						// Kolekce, do které vložím získané hodnoty:
						final List<Object> parameterList = new ArrayList<>();

						// naplním kolekce získanými hodnotami:
						for (final String s : parameters) {
							if (!regEx.isWhiteSpace(s)) {
								// NOVE:
								final ResultValue resultValue = getValueFromVar_Method_Constant(s.trim(), dataType);

								/*
								 * Pokud se nevrátila žádná hodnota (nemělo by nastat) nebo pokud se
								 * nerozpoznala operace, pak přeskočím na další iteraci v cyklu, protože došlo k
								 * nějaké chybě a operace se buď neprovedla vůbec, že se nerozpozanala, nebo v
								 * ní došlo k nějaké chybe.
								 * 
								 * Jinak pokud se nějaká hodnota vrátila, tak i když to bude null, tak tu
								 * hodnotu vložím do listu.
								 */
								if (resultValue == null || !resultValue.isExecuted())
									continue;

								parameterList.add(resultValue.getObjResult());
							}
						}
						
						
						// Nyní mám kolekce objektů coby získaných hodnot,
						// mohu zkusit vytvořit instanci třídy VariableArray, pro reprezentaci pole v editoru příkazů:
						Instances.addInstanceToMap(referenceName, new VariableArray<>(isFinal, parameterList.toArray(), dataType));

                        /*
                         * Je třeba aktualizovat hodnoty v okně pro automatické dopplňování / dokončování příkazů v
                         * editoru příkazů, jinak by se v něm nezobrazilo výše vytvořené pole.
                         */
                        updateValueCompletionWindow();

						// Pokud se dostanu sem, vše se úspěšně podařilo, tak mohu vrátit Úspěch, jako že se operace povedla: 
						return txtSuccess;
					}
					else return txtFailure + ": " + txtDataTypeForArrayAreNotIdenticaly;
				}
				else
					return txtFailure + ": " + txtDataTypeForArrayNotFound + OutputEditor.NEW_LINE_TWO_GAPS + txtError
							+ ": " + dataTypeOfArrayInText;
			}
			else
				return txtFailure + ": " + txtVariableNameIsNeedToChangeIsAlreadyTaken + OutputEditor.NEW_LINE_TWO_GAPS
						+ txtVariable + ": " + referenceName;
		}
		else
			return txtFailure + ": " + txtVariableNameMustNotBeKeyWord + OutputEditor.NEW_LINE_TWO_GAPS + txtName + ": "
					+ referenceName;
	}
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public String makeDeclarationAndFullfillmentArrayShort(final String command) {
		// Syntaxe: (final) dataType (výše)[] variableName = {values1, values2, ...};
		
		/*
		 * Note:
		 * Způsob rozdělení dle rovná se nepřipadá v úvahu, protože pokud se do
		 * parametru pole coby hodnota pro naplnění zadá rovnáse, pak příkaz selže.
		 */
		
		/*
		 * Proměnná, do které si vložím text příkazu od začátku po první rovná se.
		 */
		final String textBeforeEquals = command.substring(0, command.indexOf('='));
		
		/*
		 * Proměnná, do které si vložím text příkazu od rovná se do konce.
		 */
		final String textAfterEquals = command.substring(command.indexOf('=') + 1);
		
		
		// Logická hodnota o tom, zda má být pole final nebo ne:
		final boolean isFinal = isArrayFinal(textBeforeEquals);
		
		// Datový typ pole v textu - získaný z příkazu:
		final String dataTypeOfArrayInText = getDataTypeOfArrayInText(textBeforeEquals, isFinal);
		
		// Zde si zjistím datový typ z výše zadaného textu:
		final Class<?> dataType = getClassTypeOfArray(dataTypeOfArrayInText);
		
		
		// Název promenné = od první uzavírací hranaté závorky po první rovnáse:
		final String referenceName = command.substring(command.indexOf(']') + 1, command.indexOf('=')).replaceAll("\\s",
				"");
		
		
		// Zjistím text parametrů od první otevírací špičaté závorky po poslední zavírací špičatou závorku v druhé
		// částí příkazu - za rovnáse
		final String parametersInText = textAfterEquals.substring(textAfterEquals.indexOf('{') + 1,
				textAfterEquals.lastIndexOf('}'));
		
		// rozdělím si parametry dle desetinné čárky:
		final String[] parameters = parametersInText.split(",");
		
		
		
		// Otestuji, zda není název pole klíčové slovo Javy:
		if (isKeyWordOfJava(referenceName))
			return txtFailure + ": " + txtVariableNameMustNotBeKeyWord + OutputEditor.NEW_LINE_TWO_GAPS + txtName + ": "
					+ referenceName;
		
		// Pro začátek otestuji, zda se zadaný název proměnné ještě nevyskytuje v instancích:
		if (Instances.isReferenceNameInMap(referenceName))
			return txtFailure + ": " + txtVariableNameIsNeedToChangeIsAlreadyTaken + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtVariable + ": " + referenceName;
		
		// Otetuji datový typ, zda byl zadán jeden z "možných" datových typů:
		if (dataType == null)
			return txtFailure + ": " + txtDataTypeForArrayNotFound + OutputEditor.NEW_LINE_TWO_GAPS + txtError + ": "
					+ dataTypeOfArrayInText;
		
		
		
		// Zde znám datový typ hodnot, který může pole obsahovat a je zadána správná syntaxe pro vytvoření, nyní si mohu
		// zkusit zjistit hodnoty, kterými se má naplnit pole:
		
		// Kolekce, do které vložím získané hodnoty:
		final List<Object> parameterList = new ArrayList<>();
		
		// naplním kolekce získanými hodnotami:
		for (final String s : parameters) {
			if (!regEx.isWhiteSpace(s)) {
				final ResultValue resultValue = getValueFromVar_Method_Constant(s.trim(), dataType);
				
				/*
				 * Pokud se nevrátila žádná hodnota (nemělo by nastat) nebo pokud se
				 * nerozpoznala operace, pak přeskočím na další iteraci v cyklu, protože nevím,
				 * zda se operace nerozponala vůbec, nebo se nepovedlo její vykonání, tak či
				 * tak, měly by již být vypsány výpisy o nastalé chybě v editoru výstupů, tak
				 * zde nebudu hodnotu započítávat.
				 * 
				 * Hodnota null se započítá pouze v případě úspěšného provedení operace, pak je
				 * to třeba úmysl, alespoň je zde možnost.
				 */
				if (resultValue == null || !resultValue.isExecuted())
					continue;
				
				parameterList.add(resultValue.getObjResult());
			}				
		}
		
		
		// Nyní mám kolekce objektů coby získaných hodnot,
		// mohu zkusit vytvořit instanci třídy VariableArray, pro reprezentaci pole v editoru příkazů:
		Instances.addInstanceToMap(referenceName, new VariableArray<>(isFinal, parameterList.toArray(), dataType));

        /*
         * Zde je třeba aktualizovat hodnoty v okně pro automatické doplňování / dokončování příkazů v editoru
         * příkazů, jinak by se v něm neprojevilo výše vytvořené pole.
         */
        updateValueCompletionWindow();

		// Pokud se dostanu sem, vše se úspěšně podařilo, tak mohu vrátit Úspěch, jako že se operace povedla: 
		return txtSuccess;
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí datový typ, který může pole obsahovat, ze zadaného
	 * příkazu - jeho části text který obsahuje datový typ si zjistím, o jaký datový
	 * typ se má jednat a vrátím ho v podobně Class
	 * 
	 * Jelikož metoda: getClassType obsahuje pouze objektové datové typy pro List,
	 * který jiné typy obsahovat nemůže musel jsem dopsat následující metodu, která
	 * rozpozná i primitivní datové typy
	 * 
	 * @param dataTypeInText
	 *            - datový typ v podobně textu
	 * 
	 * @return datový typ typu CLass nebo null, pokud nebude datový typ rozpoznán
	 */
	private static Class<?> getClassTypeOfArray(final String dataTypeInText) {
		
		if (dataTypeInText.equals("byte"))
			return byte.class;		
		
		
		else if (dataTypeInText.equals("short"))
			return short.class;		
		
		
		else if (dataTypeInText.equals("int"))
			return int.class;
				
		
		else if (dataTypeInText.equals("long"))
			return long.class;
		
		
		else if (dataTypeInText.equals("float"))
			return float.class;
		
		
		else if (dataTypeInText.equals("double"))
			return double.class;
		
				
		else if (dataTypeInText.equals("char"))
			return char.class;
		
		
		else if (dataTypeInText.equals("boolean"))
			return boolean.class;
		
		
		return getClassType(dataTypeInText);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda byly zadány stejné datové typy pro deklaraci pole
	 * 
	 * Metoda si z druhé části příkau (od prvního rovnáse do konce) zjistí druhý
	 * zadaný datový typ a otestuje, zda jsou stejné i s prvním zadaným, dle toho
	 * buď vrátí true nebo false
	 * 
	 * @param dataType
	 *            - datový typ, který byl zadán
	 * 
	 * @param text
	 *            - druhá část přkazu pro zjištění druhého zadaného datového typu
	 *            (příkaz od prvního rovnáse do konce)
	 * 
	 * @return true, pokud jsou oba datové typy stejné, jinak false
	 */
	private static boolean areDataTypesIdenticaly(final Class<?> dataType, final String text) {
		// Druhý datový typ je od klíčového slova new po první otevírací závorku v druhé
		// části příkazu - za prvním rovnáse:
		
		final String withoutNew = text.replace("new", "");
		final String dataTypeInText2 = withoutNew.substring(0, withoutNew.indexOf('[')).replaceAll("\\s", "");
		
		
		if (dataType != null) {
			final Class<?> dataType2 = getClassTypeOfArray(dataTypeInText2);


			return dataType.equals(dataType2);
		}
		
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí datový typ hodnot, které půjde vkládat do pole
	 * 
	 * Metoda bude vědět, zda je v příkazu obsaženo slovo final nebo ne, a dle toho
	 * si zjistí buď text od začátku po první otevírací hranatou závorku, která
	 * značí, že se jedná o jednorozměrné pole, nebo od slova final po první
	 * otevírací hranatou závorku. Tímto způsobem si zjistím jaký uživatel napsal
	 * datový typ pro pole
	 * 
	 * @param text
	 *            - část zadaného příkazu - od začátku příkazu po první rovnáse
	 * 
	 * @param isFinal
	 *            - logická hodnota o tom, zda příkaz na začátku obsahuje slovo
	 *            final nebo ne
	 * 
	 * @return datový typ v podobně textu - "napsaný datový typ"
	 */
	private static String getDataTypeOfArrayInText(final String text, final boolean isFinal) {
		if (isFinal) {
			final String withoutFinal = text.replace("final", "");
			
			return withoutFinal.substring(0, withoutFinal.indexOf('[')).replaceAll("\\s", "");
		}
		
		return text.substring(0, text.indexOf('[')).replaceAll("\\s", "");
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda je pole final nebo ne, pozná to z první části
	 * příkazu (od začátku po rovnáse) zda obsahuje slovo final nebo ne protože může
	 * byt slovo final na začátku, pak vybrané datove typy a pak po [
	 * 
	 * Metoda si vezme první část příkazu po první [ - otevírací závorka, která
	 * značí, že se jedná o jednorozměrné pole
	 * 
	 * @param command
	 *            - část uživatelem zadaného příkazu - od začátku po první rovnáse
	 * 
	 * @return true, pokud přkaz obsahuje na začátku slovo final, jinak false
	 */
	private static boolean isArrayFinal(final String command) {
		final String partOfStart = command.substring(0, command.indexOf('['));

		return partOfStart.contains("final");
	}






	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


	@Override
	public String makeDeclarationArrayWithLength(final String command) {
		// Syntaxe příkazu: dataType[] variableName = new dataType[size];
		
		final String[] partsOfCommand = command.split("=");
		
		// Logická hodnota o tom, zda má být pole final nebo ne:
		final boolean isFinal = isArrayFinal(partsOfCommand[0]);
		
		// Datový typ pole v textu - získaný z příkazu:
		final String dataTypeOfArrayInText = getDataTypeOfArrayInText(partsOfCommand[0], isFinal);
		
		// Zde si zjistím datový typ z výše zadaného textu:
		final Class<?> dataType = getClassTypeOfArray(dataTypeOfArrayInText);
		
		
		// Název promenné = od první uzavírací hranaté závorky po první rovnáse:
		final String referenceName = command.substring(command.indexOf(']') + 1, command.indexOf('=')).replaceAll("\\s",
				"");
		
		// Otestuje, zda uživatel zadal stejné datové typy:
		final boolean areDataTypesIdenticaly = areDataTypesIdenticaly(dataType, partsOfCommand[1]);
		
		
		// jistím si velikos pole v podobě textu, velikost je v hranatých závorkách:
		final String sizeInText = partsOfCommand[1]
				.substring(partsOfCommand[1].indexOf('[') + 1, partsOfCommand[1].indexOf(']')).replaceAll("\\s", "");
		
		try {
			// Následující příkaz potřebuji zachytávat v bloku Try - catch, 
			// Příkaz prošel regulárními výrazy, takže mohu text naparsovat na int pro délku pole:
			final int size = Integer.parseInt(sizeInText);
			
			
			// Nejprve zjistím, zda název pole není klíčové slovo Javy:
			if (!isKeyWordOfJava(referenceName)) {
				// Pro začátek otestuji, zda se zadaný název proměnné ještě nevyskytuje v instancích:
				if (!Instances.isReferenceNameInMap(referenceName)) {
					// Otetuji datový typ, zda byl zadán jeden z "možných" datových typů:
					if (dataType != null) {
						// Otestuji, zda jsou datové typy pro deklaraci pole a napplnění pole stejné, tj.
						// dataType1[] var = new dataType2[]{};   aby se dataType1 a 2 rovnaly:
						if (areDataTypesIdenticaly) {
							// Zde znám datový typ hodnot, který může pole obsahovat a je zadána správná syntaxe pro vytvoření
							
							// Regulárním výrazem neprojdou záporná čísla, takže je ani testovat nemsuím,
							// sem se dostanu, pokud se povede naparsování hodnoty na integer coby index jako velikost pole
							// tak ho mohu vytvořit:
							
							Instances.addInstanceToMap(referenceName, new VariableArray<>(isFinal, size, dataType));

                            /*
                             * Zde je třeba aktualizovat hodnoty v okně pro doplňování / dokončování hodnot v editoru
                              * příkazů, kdyby se to zde neaktualizovalo, v tom okně by nebyla možnost pro doplnění
                              * výše vytvořeného pole.
                             */
                            updateValueCompletionWindow();
							
							// Pokud se dostanu sem, vše se úspěšně podailo, tak mohu vrátit Úspěch, jako že se operace povedla: 
							return txtSuccess;
						}
						else return txtFailure + ": " + txtDataTypeForArrayAreNotIdenticaly;
					}
					else
						return txtFailure + ": " + txtDataTypeForArrayNotFound + OutputEditor.NEW_LINE_TWO_GAPS
								+ txtError + ": " + dataTypeOfArrayInText;
				}
				else
					return txtFailure + ": " + txtVariableNameIsNeedToChangeIsAlreadyTaken
							+ OutputEditor.NEW_LINE_TWO_GAPS + txtVariable + ": " + referenceName;
			}
			else
				return txtFailure + ": " + txtVariableNameMustNotBeKeyWord + OutputEditor.NEW_LINE_TWO_GAPS + txtName
						+ ": " + referenceName;


        } catch (NumberFormatException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka pří parsování zadané velikosti pole: " + sizeInText
                        + " na datový typ Integer, třída MakeOperation.java, metoda: makeDeclarationArrayWithLength. " +
                        "Tato chyba může nastat v případě, že zadané číslo není ve správném formátu nebo je mimo " +
                        "rozsah datového typu Integer apod.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			return txtFailure + ": " + txtArraySizeIsOutOfInteger;
		}		
	}







	
	
	
	
	@Override
	public String makeDeclarationArrayByCallingMethod(final String command) {
		// Možná syntaxe:
		// (final) dataType[] variableName = instance.methodName(parameters);
		// (final) dataType[] variableName = packageName. (anotherPackageName.) ClassName.staticMethodName(parameters);
			
		/*
		 * Note:
		 * Způsob rozdělení dle rovná se nepřipadá v úvahu, protože pokud se do
		 * parametru pole coby hodnota pro naplnění zadá rovnáse, pak příkaz selže.
		 */
		
		/*
		 * Proměnná, do které si vložím text příkazu od začátku po první rovná se.
		 */
		final String textBeforeEquals = command.substring(0, command.indexOf('='));
		
		/*
		 * Proměnná, do které si vložím text příkazu od rovná se do konce.
		 */
		final String textAfterEquals = command.substring(command.indexOf('=') + 1);
		
		
		// Logická hodnota o tom, zda má být pole final nebo ne:
		final boolean isFinal = isArrayFinal(textBeforeEquals);
		
		// Datový typ pole v textu - získaný z příkazu:
		final String dataTypeOfArrayInText = getDataTypeOfArrayInText(textBeforeEquals, isFinal);
		
		// Zde si zjistím datový typ z výše zadaného textu:
		final Class<?> dataType = getClassTypeOfArray(dataTypeOfArrayInText);
		
		
		// Název promenné = od první uzavírací hranaté závorky po první rovnáse:
		final String referenceName = command.substring(command.indexOf(']') + 1, command.indexOf('=')).replaceAll("\\s",
				"");



		/*
		 * Proměnná, do které si vložím text metody, ten je od prvního rovná se po
		 * poslední středník.
		 * 
		 * V tomto případě jej odeberu, protože v metodě getValueFromVar_Method_Constant
		 * se přidává na konec středník.
		 */
		final String methodText = textAfterEquals.substring(0, textAfterEquals.lastIndexOf(';'));
		
		
		
		// Otestuji, zda není název pole klíčové slovo Javy:
		if (isKeyWordOfJava(referenceName))
			return txtFailure + ": " + txtVariableNameMustNotBeKeyWord + OutputEditor.NEW_LINE_TWO_GAPS + txtName + ": "
					+ referenceName;
		
		// Pro začátek otestuji, zda se zadaný název proměnné ještě nevyskytuje v instancích:
		if (Instances.isReferenceNameInMap(referenceName))
			return txtFailure + ": " + txtVariableNameIsNeedToChangeIsAlreadyTaken + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtVariable + ": " + referenceName;
		
		
		/*
		 * Otetuji datový typ, zda byl zadán jeden z "možných" datových typů.
		 * 
		 * Tato podmínka by ale nikdy být splněna nebyla, protože konkrétně u
		 * jednorozměrného pole jsou datové typy pevně definované, ale kdyby náhodou ???
		 * Tak aby se dále nedostalo, protože pak by se netestoval datový typ hodnoty,
		 * kterou by například vrátila metoda nebo se vzala z proměnné apod. a rovnou by
		 * se jí pole mohlo naplnit a potenciálně by mohlo dojít k nějaké chybě apod.
		 */
		if (dataType == null)
			return txtFailure + ": " + txtDataTypeForArrayNotFound + OutputEditor.NEW_LINE_TWO_GAPS + txtError + ": "
					+ dataTypeOfArrayInText;
			
		
		
		/*
		 * Vytvořím si proměnnou typu VariableArray, která reprezentuje jednorozměrné
		 * pole. Nejprve do ní musím nastavit hodnota, zda je final na true a datový typ pole například
		 * String, int apod. viz dataType, Zároveň se v konstruktoru nastaví o datový
		 * typ Přímo toho pole, například pokud je v proměnné dataType hdonota String,
		 * pak se pro porovnávání datových typů pole v konstruktoru objektu
		 * VariableArray nastaví i hodnota pro datový typ přímo pole typu String.
		 * 
		 * Hodnota isFinal bude true, protože jinak by se nemohla naplnit.
		 */
		final VariableArray<?> varArray = new VariableArray<>(false, null, dataType);
		
		/*
		 * Zavolám příslušnou operaci, v tomto případě zavolám metodu, která by měla
		 * vráti příslušné jednorozměrné pole příslušného datového typu a to rovnou
		 * naplním do příslušné proměnné.
		 * 
		 * Pak se vrátí nějaký text do proměnné result a ten se později vypíše do
		 * editoru výstupů.
		 */
		final String result = fullfillmentVariable(null, varArray, null, referenceName, methodText);
		
		
		/*
		 * Nyní mohu otestovat, zda se příslušné pole naplnilo a pokud ano (tj. není
		 * null), pak mohu vytvořit novou instanci objektu VariableArray a vložit jej do
		 * mapy. Do té instance již předám veškeré hodnoty, které byly získány.
		 */
		if (varArray.getArray() != null) {
            Instances.addInstanceToMap(referenceName, new VariableArray<>(isFinal, varArray.getArray(), dataType));

            /*
             * Zde je třeba aktualizovat hodnoty v okně v editoru příkazů pro doplňování / dokončování příkazů, jinak
              * by v tom okně nebylo ny výběr výše vytvořené pole.
             */
            updateValueCompletionWindow();
        }


		// Vrátím výsledek aby se vypsal do editoru výstupů:
		return result;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	@Override
	public String makeFullfillmentValueAtIndexOfArrayMethod(final String command) {
		// Syntaxe: var[index] = referenceName.methodName(parameters);
		
		// Od začátku po první otevírací hranatou závorku si zjistím název pole,
		// pak index, pak název reference na instanci třídy, pak metodu a její parametry:
		
		
		// Název pole je od začátku příkazu po první otevírací hranatou závorku:
		final String arrayName = command.substring(0, command.indexOf('[')).replaceAll("\\s", "");
		
		
		// Zjištění indexu hodnoty v poli:		
		// Do metody předám začátek příkazu, kde chci zjistit indexu hodnoty v polie pro přidání hodnoty:
		final int index = getIndexOfArray(command.substring(0, command.indexOf('=')));
		
		if (index > -1) {
			// Zkusím získat instanci třídy VariableArray, kde je požadované pole:
			final VariableArray<?> variableArray = (VariableArray<?>) Instances.getArrayFromMap(arrayName);
			
			if (variableArray != null) {
				// Zde jsou splněny podmínky pro naplnní hodnoty, tak mohu zavolat metodu

				// Nejprve otestuji, zda je index >=0 a mensi nez velikost ple:
				if (index < variableArray.getArray().length) {
					// Zde vím, že se jedná o zavolání, metody, tak ybch to nemusel rozepisovat,
					// zavolám metodu getValueFromVarMethodConstant
					// kteá už mi vrátí požadovanou hodnotu:

					// Nejprve si ale získám část příkazu, kde je metoda, tj od rovnáse po středník,
					// protože následující metoda si ho přídává:
					final String textOfTheMethod = command.substring(command.indexOf('=') + 1,
							command.lastIndexOf(';'));

					final ResultValue objResultValue = getValueFromVar_Method_Constant(textOfTheMethod.trim(),
							variableArray.getTypeArray());

					/*
					 * Pokud se nevrátila žádná hodnota (nemělo by nastat) nebo pokud se
					 * nerozpoznala operace, pak vrátím 'Neúspěch' s tím, že by měla již být
					 * informace o nastalé chybě (důvodu proč se nevykonala nějaká opoerace nebo
					 * nerozponal výraz) již vypsána do editoru výstupů.
					 */
					if (objResultValue == null || !objResultValue.isExecuted())
						return txtFailure;

					if (objResultValue.getObjResult() != null) {
						// Zde mám hodnotu z metody, tak mohu nastavit hodnotu na zadný index v poli:
						variableArray.setValueToIndex(index, objResultValue.getObjResult());

                        /*
                         * Zde je třeba aktualizovat hodnoty v okně pro automatické doplňování / dokončování příkazů
                         * v editoru příkazů, jinak by v tom okně byly zobrazeny chybné hodnoty, resp. nebyla by v
                         * tom okně zobrazena výše naplněná hodnota.
                         */
                        updateValueCompletionWindow();

						return txtSuccess;
					}
				} 
				else
					return txtFailure + ": " + txtConditionForIndexInArray + OutputEditor.NEW_LINE_TWO_GAPS + txtIndex
							+ ": " + index + ", " + txtSizeOfArray + ": " + variableArray.getArray().length;
			}
			else
				return txtFailure + ": " + txtInstanceOfArrayWasNotFoundUnderReference + OutputEditor.NEW_LINE_TWO_GAPS
						+ txtReference + ": " + arrayName;
		} 
		else
			return txtFailure + ": " + txtIndexOutOfRange;

		return txtFailure;
	}



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	



	@Override
	public String makeFullfillmentValueAtIndexOfArrayConstant(final String command) {
		// Syntaxe: var(index) = value;  value = znak, číslo, text, apod.
		
		// Postup:
		// Zjistím si pole, index a hodnotu:
		
		// název - reference na pole je od začátku po první otevírací hranatou závorku:
		final String arrayName = command.substring(0, command.indexOf('[')).replaceAll("\\s", "");
		
		// Hodnota je za prvním rovnáse po středník:
		final String valueForInsert = command.substring(command.indexOf('=') + 1, command.lastIndexOf(';')).trim();
		
		
		final VariableArray<?> variableArray = (VariableArray<?>) Instances.getArrayFromMap(arrayName);
		
		
		final int index = getIndexOfArray(command.substring(0, command.indexOf('=')));
		
		
		if (index > -1) {
			if (variableArray != null) {
				// Zde jsem našel hledané pole, mohu zkusit zjistit index:
				// Nejprve otestuji, zda nění pole final:
				if (index < variableArray.getArray().length) {
					// Zde je v pořádku index pole i index, tak mohu zjistit
					// hodnotu pro vložení:
					final ResultValue objResultValue = getValueFromVar_Method_Constant(valueForInsert,
							variableArray.getTypeArray());

					/*
					 * Pokud se nevrátila žádná hodnota (nemělo by nastat) nebo pokud se
					 * nerozpoznala operace, pak vrátím 'Neúspěch' s tím, že by měla již být
					 * informace o nastalé chybě (důvodu proč se nevykonala nějaká opoerace nebo
					 * nerozponal výraz) již vypsána do editoru výstupů.
					 */
					if (objResultValue == null || !objResultValue.isExecuted())
						return txtFailure;

					if (objResultValue.getObjResult() != null) {
						// Zde mohu naplnit hodnotu na zadném indexu:
						variableArray.setValueToIndex(index, objResultValue.getObjResult());

                        /*
                         * Zde je třeba aktualizovt okno s doplňováním / dokončování příkazů v editoru příkazů, jinak
                         * by v tom okně byly zobrazeny neaktuální hodnoty, nebyla by v tom okně zobrazena výše
                         * změnaná / naplněná hodnota.
                         */
                        updateValueCompletionWindow();

						// Zde proběhlo vše v pořádku, tak mohu vrátit
						// Úspěch:
						return txtSuccess;
					}
					// Zde byly hlášky vypsány v metodě
					// getValueFromVar_Method_Constant
				} 
				else
					return txtFailure + ": " + txtConditionForIndexInArray + OutputEditor.NEW_LINE_TWO_GAPS + txtIndex
							+ ": " + index + ", " + txtSizeOfArray + ": " + variableArray.getArray().length;
			}
			else
				return txtFailure + ": " + txtInstanceOfArrayWasNotFoundUnderReference + OutputEditor.NEW_LINE_TWO_GAPS
						+ txtReference + ": " + arrayName;
		} 
		else
			return txtFailure + ": " + txtIndexOutOfRange;

		return txtFailure;
	}




	
	
	
	
	
	
	
	
	
	



	@Override
	public String makeFullfillmentValueAtIndexOfArrayVariable(final String command) {
		// Syntaxe:
		// var[index] = variable;
		// var[index] = referenceName.variable;
		// var[index] = variable[index];
		
		
		// Postup:
		// zjistím si pole a pozici kam mám vložit hodnotu,
		// pak si zjistím proměnnou, ze které si mám vzít tu hodnotu, dále ještě porovnám datové typy, apod.
		// případně vypíšu chybu, ke které došlo:
		
		
		// Zjistím si název pole, resp referenci na něj a zda vůbec existuje
		// název pole je od začátku po první otevírací závorku:
		final String arrayName = command.substring(0, command.indexOf('[')).replaceAll("\\s", "");
		
		
		// Zjistím si text pro zjištění o jaký typ proměnné se jedná, tj od rovnáse po středník:
		final String valueForInsert = command.substring(command.indexOf('=') + 1, command.lastIndexOf(';')).trim();
		
		
		final int index = getIndexOfArray(command.substring(0, command.indexOf('=')));
		
		
		// Nyní mám ndex a název pole, tak mohu otestovat, zda existuje:
		final VariableArray<?> variableArray = (VariableArray<?>) Instances.getArrayFromMap(arrayName);
		
		
		if (index > -1) {
			if (variableArray != null) {
				// zde jsem našel pole, pro začátek otestuji, zda není final:
				if (index < variableArray.getArray().length) {
					// Zde je v pořádku i index, tak už ohu zjistit hodnotu:
					final ResultValue objResultValue = getValueFromVar_Method_Constant(valueForInsert,
							variableArray.getTypeArray());

					/*
					 * Pokud se nevrátila žádná hodnota (nemělo by nastat) nebo pokud se
					 * nerozpoznala operace, pak vrátím 'Neúspěch' s tím, že by měla již být
					 * informace o nastalé chybě (důvodu proč se nevykonala nějaká opoerace nebo
					 * nerozponal výraz) již vypsána do editoru výstupů.
					 */
					if (objResultValue == null || !objResultValue.isExecuted())
						return txtFailure;

					if (objResultValue.getObjResult() != null) {
						variableArray.setValueToIndex(index, objResultValue.getObjResult());

                        /*
                         * Zde je třeba aktualizovat hodnoty v okně pro automatické doplňování / dokončování příkazů
                         * v editoru příkazů, jinak by v něm byly zobrazeny neaktuální hodnoty, resp. neprojevila by
                         * se v něm výše změněná hodnota.
                         */
                        updateValueCompletionWindow();

						return txtSuccess;
					}
					// else informace byly vypsány v metodě
					// getValueFromVar...
				} 
				else
					return txtFailure + ": " + txtConditionForIndexInArray + OutputEditor.NEW_LINE_TWO_GAPS + txtIndex
							+ ": " + index + ", " + txtSizeOfArray + ": " + variableArray.getArray().length;
			} 
			else
				return txtFailure + ": " + txtInstanceOfArrayWasNotFoundUnderReference + OutputEditor.NEW_LINE_TWO_GAPS
						+ txtReference + ": " + arrayName;
		}
		else
			return txtFailure + ": " + txtIndexOutOfRange;

		return txtFailure;
	}







	
	
	
	
	
	
	
	



	@Override
	public String makeGetValueFromArrayAtIndex(final String command) {
		// Syntaxe: variableName [index];
		
		// Postup:
		// zjistím si název pole, index pozice v poli, dále zda pole vůbec existuje a je index v pořádku a případně
		// vypíšu hodnotu, nebo hlášku s chybou, ke které došlo:
		
		
		// Název pole, resp. reference na něj je od začátku po první otevírací hranatou závorku:
		final String arrayName = command.substring(0, command.indexOf('[')).replaceAll("\\s", "");
		
		final int index = getIndexOfArray(command);
		
		
		final VariableArray<?> variableArray = (VariableArray<?>) Instances.getArrayFromMap(arrayName);
		
		
		if (index > -1) {
			if (variableArray != null) {
				// Zde byla nalezena instance pro pole, mohu otestovat index:
				if (index < variableArray.getArray().length)
					// Zde je index v pořádku, mohu získat a vypsat hodnotu:
					return txtSuccess + ": " + variableArray.getValueAtIndex(index);

				else
					return txtFailure + ": " + txtConditionForIndexInArray + OutputEditor.NEW_LINE_TWO_GAPS + txtIndex
							+ ": " + index + ", " + txtSizeOfArray + ": " + variableArray.getArray().length;
			}
			else
				return txtFailure + ": " + txtInstanceOfArrayWasNotFoundUnderReference + OutputEditor.NEW_LINE_TWO_GAPS
						+ txtReference + ": " + arrayName;
		}
		else return txtFailure + ": " + txtIndexOutOfRange; 
	}



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	






	@Override
	public String makeIncrementArrayValueBefore(final String command) {
		// Syntaxe:  ++ arrayName [index];
		
		
		// Zjistím si, názevm - referenci na pole, index hodnoty a zda se má hodnota na indexu inkrementovat
		// nebo dekrementovat a zavolám na to metodu: incrementDecrementValueInArray, která se "o to postará":
		
		// Název pole je od ++ do první otevírací hranaté závorky:
		final String arrayName = command.substring(command.indexOf("++") + 2, command.indexOf('[')).replaceAll("\\s", "");
		
		//index hodnoty je mezi hranatýma závorkama:
		
		// získání indexu:
		final int index = getIndexOfArray(command);
		
		if (index > -1)
			return incrementDecrementValueInArray(arrayName, index, true);
		
		return txtFailure + ": " + txtIndexOutOfRange;
	}

	
	
	

	@Override
	public String makeIncrementArrayValueBehind(String command) {
		// Syntaxe:  arrayName [index] ++;
		
		// Název pole je od začátku po první otevírací hranatou závorku:
		final String arrayName = command.substring(0, command.indexOf('[')).replaceAll("\\s", "");
		
		// získání indexu:
		final int index = getIndexOfArray(command);
		
		if (index > -1)
			return incrementDecrementValueInArray(arrayName, index, true);
		
		return txtFailure + ": " + txtIndexOutOfRange;
	}

	
	

	@Override
	public String makeDecrementArrayValueBefore(final String command) {
		// Syntaxe:   -- arrayName [index];
		
		// Název pole je od -- po první otevírací hranatou závorku:
		final String arrayName = command.substring(command.indexOf("--") + 2, command.indexOf('[')).replaceAll("\\s", "");
		
		
		// získání indexu:
		final int index = getIndexOfArray(command);
		
		if (index > -1)
			return incrementDecrementValueInArray(arrayName, index, false);
		
		return txtFailure + ": " + txtIndexOutOfRange;
	}


	
	
	
	@Override
	public String makeDecrementArrayValueBehind(final String command) {
		// Syntaxe:  arrayName [index] --;
		
		// Název pole je o začátku po první otevírací hranatou závorku:
		final String arrayName = command.substring(0, command.indexOf('[')).replaceAll("\\s", "");
		
		
		final int index = getIndexOfArray(command);
		
		if (index > -1)
			return incrementDecrementValueInArray(arrayName, index, false);
		
		return txtFailure + ": " + txtIndexOutOfRange;
	}

	
	
	
	
	
	
	
	
	/**
	 * Metoda, která se pokusí najít mezi první otevírací hranatou závorku a první
	 * zavírací hranatou závorkou index jednorozměrného pole, ten se dále pokusí
	 * přetypovat na Integer, pokud se to povede, vrátí tu hodnotu, jinak -1
	 * 
	 * Note: Metoda je volána pouze pro zjištění indexu při dekrementaci a
	 * inkrementaci hodnoty v jednorozměrném poli, takže by nemělo dojít k chybě
	 * 
	 * @param text
	 *            - příkaz pro inkrementaci či dekrementaci hodnoty v jednorozměrném
	 *            poli
	 * 
	 * @return pokud se podaří výše zmíněné přetypování, tak daný index, jinak -1
	 */
	private static int getIndexOfArray(final String text) {
		final String indexInText = text.substring(text.indexOf('[') + 1, text.indexOf(']')).replaceAll("\\s", "");
		
		try {
			return Integer.parseInt(indexInText);
			
		} catch (NumberFormatException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka pří parsování zadaného indexu: " + indexInText
                        + " na datový typ Integer, třída MakeOperation.java, metoda: getIndexOfArray. Tato chyba může" +
                        " nastat například v případě, že zadaný index není ve správné syntaxi, nebo je mimo rozsah " +
                        "datového typu Integer apod.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
			
			return -1;
		}
	}

	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která inkrementuje nebo dekrementuje hodnotu na zadaná pozici
	 * (indexu) v jednorozměrném poli - pokud to lze a informace o tom, zda se
	 * operace povedla nebo ne se vypíšu uživateli do editrou výstupů
	 * 
	 * @param arrayName
	 *            - název / reference na pole, resp. na instanci třídy VariableArray
	 * 
	 * @param increment
	 *            - logická hodnota o tom, zda se má hodnota na zadaném indexu
	 *            dekrementovat nebo inkrementovat, true = inkrementovat, false =
	 *            dekrementovat
	 * 
	 * @param index
	 *            - hodnota typu int = index pozice v poli, na které se má
	 *            dekrementovat / inkrementovat hodnota
	 * 
	 * @return Úspěch, pokud se výše zmíněná operace povede, jinak Neúspěch s
	 *         infoamcí o chybě, ke které došlo
	 */
	private String incrementDecrementValueInArray(final String arrayName, final int index, final boolean increment) {
		
		// zkusím si načíst instanci třídy VariableArray, zda vůbec existuje pole:
		final VariableArray<?> variableArray = (VariableArray<?>) Instances.getArrayFromMap(arrayName);
		
		
		if (variableArray != null) {
			// Otestuji, zda není index mimo rozsah pole:
			if (index >= 0 && index < variableArray.getArray().length) {
				// Zde jsem našel hledanou instanci třídy, tak si mohu načíst hodnotu na zadaném
				// indexu:
				final Object objValue = variableArray.getValueAtIndex(index);

				if (objValue != null) {
					// Zde jsem našel nějakou hodnotu na zadném indexu v poli, tak mohu zjistit, zda
					// se jedná o číslo,
					// abych ho mohl inkrementovat nebo dekrementovat:

					final Object objFinalValue = getIncrementDecrementValue(objValue, increment);

					// Proměnná, do které vložím finální hodnotu, tj, inkrementovanou /
					// dekrementovanou hodnotu:
					if (objFinalValue != null) {
						// tak nastavím novou hodnotu do proměnné:
						variableArray.setValueToIndex(index, objFinalValue);

                        /*
                         * Zde je třeba aktualizovat hodnoty pro automatické doplňování / dokončování příkazů v
                         * editoru příkazů, jinak by se v tomto okně nezobrazovaly aktuální hodnoty, resp. výše
                         * změněná hodnota.
                         */
                        updateValueCompletionWindow();

						// Oznámím, že se hodnota úspěšně inkrementovala /
						// dekremetovala:
						return txtSuccess;
					} 
					else
						return txtFailure + ": " + txtValueAtIndexAtArrayCantIncreDecre;
				} 
				else
					return txtFailure + ": " + txtValueInArrayInIndexIsNull;
			} 
			else
				return txtFailure + ": " + txtConditionForIndexInArray + OutputEditor.NEW_LINE_TWO_GAPS + txtIndex
						+ ": " + index + ", " + txtSizeOfArray + ": " + variableArray.getArray().length;
		} 
		else
			return txtFailure + ": " + txtInstanceOfArrayWasNotFoundUnderReference + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtReference + ": " + arrayName;
	}




	
	
	
	
	
	
	
	
	
	
	
	
	


	@Override
	public String makeGetLengthArray(final String command) {
		// Syntaxe: arrayName.length;
		
		
		// název pole je od začátku do konce:
		final String arrayName = command.substring(0, command.indexOf('.')).replaceAll("\\s", "");
		
		
		final VariableArray<?> variableArray = (VariableArray<?>) Instances.getArrayFromMap(arrayName);
		
		if (variableArray != null)
			// Zde bylo nalazeno pole, tak mohu zjistit jeho délku:
			return txtSuccess + ": " + variableArray.getArrayLength();
		
		else
			return txtFailure + ": " + txtInstanceOfArrayWasNotFoundUnderReference + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtReference + ": " + arrayName;
	}





	
	
	
	
	
	
	
	/**
	 * metoda, která otestuje, zda existuje příslušný soubor 'class' v příslušných
	 * balíčcích / balíčku v adresáři bin v aktuálně otevřeném projektu.
	 * 
	 * @param classWithPackages
	 *            - název třídy s balíčky oddělené desetinnou tečkou, například v
	 *            syntaxi: packageName. (anotherPackageName.) ClassName
	 * 
	 * @return true, pokud se podaří najít cestu k adresáři 'bin' v aktuálně
	 *         otevřeném projektu a v něm v příslušném adresáři se podaří najít
	 *         příslušný soubor .class (zkompilovaná třída), jinak false.
	 */
	private static boolean existCompiledClass(final String classWithPackages) {
		final String pathToBin = App.READ_FILE.getPathToBin();

		if (pathToBin != null)
			return ReadFile
					.existsFile(pathToBin + File.separator + classWithPackages.replace(".", File.separator) + ".class");

		return false;
	}







	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která do vloží do do listu methodsList veškeré metody, které jsou
	 * veřejné nebo chráněné. Vždy se ze třídy clazz načte i třída, ze které dědí a
	 * z ní si také načte veškeré metody, které deklaruje a jsou opět věřejné nebo
	 * chráněné, protože pouze tyto metody lze zavolat i z potomka.
	 *
	 * Jedná se o to, že editor příkazů jsem napsal tak, aby již dodržoval
	 * "pravidla" jazyka Java, až na nějaké výjimky pro zjednodušení. Takže nad
	 * třídou lze volat pouze veřejné a chráněné metody, ne soukromé, ty lze volat
	 * pouze uvnitř třídy.
	 * 
	 * Note:
	 * Tato metoda je velice podobná metodě getAllMethods ve třídě PopupMenuAbstract
	 * v baličku classDiagram, ale je trochu upravena, třeba aby netestovala main
	 * metodu, verou se i metody z třídy Object apod. Proto jsem tu metodu napsal
	 * "znovu a trochu jinak" než abych upravil tu zmíněnou metodu pro více
	 * možností.
	 * 
	 * 
	 * @param clazz
	 *            - Třída, ze které se mají vzít metody, až se všechny její metody
	 *            projdou, tak se zjistí, zda z něčeho dědí a pak se rekurzí
	 *            pokračuje až dokud existuje nějaká dědičnost, protože v potomkovi
	 *            lze zavolat všechny veřejné a chráněné metody z předka, resp. ze
	 *            všech předků.
	 * 
	 * @param methodsList
	 *            - Jedná se o list typu Method, do kterého se vkládají všechny
	 *            veřejné a chráněné metody (případně statické - dle hodnoty v
	 *            mustBeStatic) ze všech testovaných tříd (v proěmnné clazz).
	 * 
	 * @param mustBeStatic
	 *            - logická proměnná, dle které se pozná, zda se mají "získávat" ->
	 *            tj. vkládat do výše uvedeného listu (methodsList) pouze metody,
	 *            které jsou statické (hodnota true). Toto je potřeba, pokud se
	 *            jedná o získání metod nad třídou v diagramu tříd, tam lze zavolat
	 *            pouze statické metody, které nemusí být volány nad instancí
	 *            příslušné třídy. Pokud je hodnota false, pak se jedná o načtení
	 *            veřejných či chráněných metod nad instancí třídy, pak se mohou
	 *            brát jak metody, které jsou, tak i metody, které nejsou statické.
	 * 
	 * @return v podstatě se vrátí list methodsList, který je popsán výše, ale bude
	 *         již obsahovat všechny získané metody ze všech testovaných tříd.
	 */
    private static List<Method> getAvailableMethods(final Class<?> clazz, final List<Method> methodsList,
			final boolean mustBeStatic) {
		/*
		 * Zde netestovat, zda je trida v diagramu trid !!! i kdyby byla nebo ne, stejne
		 * by meli jit zavolat ty metody, protože je to předek / rodič té třídy !!!
		 */
		if (clazz == null)
			return methodsList;

		if (clazz.getDeclaredMethods() != null)
			Arrays.stream(clazz.getDeclaredMethods()).forEach(m -> {
				if (mustBeStatic && !Modifier.isStatic(m.getModifiers()))
					return;

                // Nemělo by nastat (duplicitní metody)
                if ((Modifier.isPublic(m.getModifiers()) || Modifier.isProtected(m.getModifiers())) && !methodsList
                        .contains(m))
                    methodsList.add(m);
			});

		// rekurzi pokracuji:
		if (clazz.getSuperclass() != null)
			getAvailableMethods(clazz.getSuperclass(), methodsList, mustBeStatic);

		// Na konec vrátím list, který již obsahuje veškeré metody:
		return methodsList;
	}


	
	
	
	
	
	
	/**
	 * Metoda, která prohledá list methodsList, který obsahuje nějaký seznam
	 * dostupných metod pro nějakou třídu (instanci) a hledá v ní metodu s názvem
	 * methodName. Jedná se pouze o testování názvu metody, ne typ nebo počet
	 * parametrů, viditelnost apod. Pouze název metody v listu metod.
	 * 
	 * @param methodName
	 *            - název metody, která se má hledat v listu methodList
	 * 
	 * @param methodsList
	 *            - seznam metod, ve kterých se bude hledatmetoda methodName
	 * 
	 * @return null, pokud nebude metoda s názvem methodName nalezena v listu metod
	 *         methodsList, jinak se vrátí příslušná metoda (první výskyt metody s
	 *         daným názvem).
	 */
	private static Method getFirstMethod(final String methodName, final List<Method> methodsList) {
		try {
			/*
			 * Note:
			 * 
			 * potřebuji zachytávat výjimku NoSuchElementException, která může nastat v
			 * případě, že nebude metoda se zadaným názvem (methodName) nalezena, pak se
			 * vrátí null.
			 */
//			return methodsList.stream().filter(m -> m.getName().equals(methodName)).findFirst().get();
			return methodsList.stream().filter(m -> m.getName().equals(methodName) && m.getParameterCount() == 0)
					.findFirst().get();
			
		} catch (NoSuchElementException e) {
			return null;
		}	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	 @Override
	public String makeFulfillmentOfArrayAtIndexInInstanceByMethod(final String command) {
		// Syntaxe: reference.variableName[index] = model.data.ClassName.methodName(params);
		
		 return makeFulfillmentArrayAtIndexInInstance(command);
	}
	
		
	@Override
	public String makeFulfillmentOfArrayAtIndexInInstanceByConstructor(final String command) {
		// Syntaxe: reference.variableName[index] = new model.data.ClassName(params);

		return makeFulfillmentArrayAtIndexInInstance(command);
	}

	@Override
	public String makeFulfillmentOfArrayAtIndexInInstanceByVariable(final String command) {
		// Syntaxe: reference.variableName[index] = referenceName.variableName;
		// Syntaxe: reference.variableName[index] = variableName;
		
		return makeFulfillmentArrayAtIndexInInstance(command);
	}

	@Override
	public String makeFulfillmentOfArrayAtIndexInInstanceByConstant(final String command) {
		// Syntaxe: reference.variableName[index] = value; (true, false, text, znak, početní operace)
		
		return makeFulfillmentArrayAtIndexInInstance(command);
	}
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	
	
	
	
	
	/**
	 * Toto je taková "univerzální" metoda, jelikož jsem si již vše potřebné ohledně
	 * získávání hodnot přepsal do metody 'getValueFromVar_Method_Constant', tak
	 * stačí zavolat tuto metodu, která získá hodnotu například ze zavolání metody
	 * nebo konstruktoru, nebo výpočítá výraz, získá proměnnou apod. Prostě sloužýí
	 * pro získání hodnoty za rovná se, resp. hodnota, terou se má naplnit příslušná
	 * pozice v jednorozměrném poli v instanci.
	 * 
	 * To nastavení pozice a zjišťování, zda se jendá o pole, které má jednu
	 * dimenzi, dále zjištění indexu, velikost, ... to je vše stejné, takže pro
	 * nastavení hodnoty do jednorozměrného pole v instanci nějaké třídy na
	 * konkrétní index pomocí návratové hodnoty z metody nebo konstrukotru nebo
	 * proměnnou nebo konstantou případně matematickým výpočtem stačí využít tuto
	 * metodu.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz / výraz v editoru příkazů.
	 * 
	 * @return Úspěch ohledně provedení výše popsané operace (jedné z nich) nebo
	 *         Neúspěch a případně hlášení o nastalé chybě.
	 */
	private String makeFulfillmentArrayAtIndexInInstance(final String command) {
		/*
		 * Proměnná, do které si vložím text příkazu od začátku po první rovná se.
		 */
		final String textBeforeEquals = command.substring(0, command.indexOf('='));

		/*
		 * Proměnná ukazující na instanci třídy: od začátku po první tečku:
		 */
		final String classNameVariable = textBeforeEquals.substring(0, textBeforeEquals.indexOf('.')).replaceAll("\\s",
				"");
		

		/*
		 * Proměnná, do které se má přiřadit hodnota: - od první desetinné tečky po
		 * první otevírací hranatou závorku:
		 */
		final String classVariable = textBeforeEquals
				.substring(textBeforeEquals.indexOf('.') + 1, textBeforeEquals.indexOf('[')).replaceAll("\\s", "");
		
		
		/*
		 * Proměnná, do které se vloží index hodnoty v poli.
		 */
		final String indexInArray = textBeforeEquals
				.substring(textBeforeEquals.indexOf('[') + 1, textBeforeEquals.lastIndexOf(']')).replaceAll("\\s", "");
		
		
		/*
		 * Zde si mohu index rovnou přetypovat na integer, protože vím, že se jedná o
		 * číslo, ale uživatel může například zadat číslo pro datový typ long apod. To
		 * je chyba.
		 */
		final int index;
		
		try {
			index = Integer.parseInt(indexInArray);
		} catch (NumberFormatException e) {
			return txtFailure + ": " + txtCastingToIntegerFailure_1 + " '" + indexInArray + "' "
					+ txtCastingToIntegerFailure_2;
		}
		
		
		
		/*
		 * Načtu si instanci pod zadanou referencí (pokud existuje).
		 */
		final Object objInstance = Instances.getInstanceFromMap(classNameVariable);
		
		
		
		if (objInstance == null)
			return txtFailure + ": " + txtInstanceOfClassInIdNotFOund + ": " + classNameVariable;
		
		
		/*
		 * Získám si položuk v zadané instanci pod zadaným jménem, zde ještě nevím, zda
		 * se jedná o jednorozměrné pole.
		 */
		final Field field = getFieldFromInstance(objInstance, classVariable);
		
		if (field == null)
			return txtFailure + ": " + txtVariableNotFoundInReferenceToInstanceOfClass;
		
		
		/*
		 * Zde otestuji, zda se jedná o pole.
		 */
		if (!field.getType().isArray())
			return txtFailure + ": " + txtEnteredVariable_1 + " '" + field.getName() + "' " + txtEnteredVariable_2;
		
		
		/*
		 * Zde si ještě musím zjistit počet dimenzí, protože předchoí testování v
		 * případě, že je položka field pole vždy vrátí true, i v případě, že se jedná o
		 * například 4 - rozměrné pole apod.
		 */
		final int countOfDims = getCountOfDimensionsOfArray(field.getType());
		
		if (countOfDims != 1)
			return txtFailure + ": " + txtVariable + " '" + field.getName() + "' " + txtIsNotOneDimArray;
				
		
		/*
		 * Zde si získám hodnotu v zadané proměnné - zadané pole.
		 */
		final Object objArrayValue = ReflectionHelper.getValueFromField(field, objInstance);
		
		if (objArrayValue == null)
			// Nemusí být viditelné apod., ale to se již vypsalo:
			return txtFailure;
		
		/*
		 * Zde si zjistím velikost příslušného pole.
		 */
		final int arrayLength = Array.getLength(objArrayValue);
		
		if (index >= arrayLength)
			return txtFailure + ": " + txtIndexValueAtArrayIsNotInInterval + ": <0 ; " + arrayLength + ").";
		
		
		
		
		/*
		 * Zde si získám část, kde je zadána metoda, tj. od prvního rovná se po poslední
		 * středník - konec příkazu.
		 */
		final String textWithMethod = command.substring(command.indexOf('=') + 1, command.lastIndexOf(';'));

		
		/*
		 * Zde zkusím zavolat příslušnou metodu, musí vrátit stejný datový typ jako je
		 * datový typ zadaného pole.
		 */
		final ResultValue objResultValue = getValueFromVar_Method_Constant(textWithMethod,
				field.getType().getComponentType());

		
		/*
		 * Pokud se nepodařilo vrátit ReturnValue (nemělo by nastat) nebo nebyla
		 * provedena operace pro zavolání metody, pak vypíšu neúspěch s tím, že by
		 * informace ohledně chyby, ke které došlo měly být vypsány do editoru výstupů.
		 */
		if (objResultValue == null || !objResultValue.isExecuted())
			return txtFailure;

		
		/*
		 * Pro zjednodušení si zde vytvořím proměnnou, do které vložím získanou hodnotu
		 * z metody (abych jí nemusel pořád načítat).
		 */
		final Object objValue = objResultValue.getObjResult();

		/*
		 * Pokud byla získána hodnota, pak v případě, že se nejedná o null hodnotu jej
		 * zkusím zadat do proměnné, v opačném případě vrátím null.
		 */
		if (objValue != null) {
			/*
			 * Zde se vrátila nějaká hodnota z příslušné metody, tak jej vložím na zadaný
			 * index v zadaném poli:
			 */
			Array.set(objArrayValue, index, objValue);

			/*
			 * Zde se podařilo úspěšně naplnit nějakou hodnotu v poli v nějaké instanci v
			 * diagramu instancí. Takže je potřeba, aby se přenačetly vztahy, protože se ta
			 * položka v poli mohla například naplnit nějakou intanci apod. Tak potřebuji
			 * znovu zkontrolovat vztahy mezi instancemi:
			 */
			runThreadForCheckInstancesRelations();

            /*
             * Zde je třeba aktualizovat hodnoty v okně pro automatické dokončení / doplnění příkazů v editoru
             * příkazů, jinak by se v tom okně neprojevila výše nastavená hodnota.
             */
            updateValueCompletionWindow();

			/*
			 * V této části se do polš výše již vložila příslušná hodnota, a nenastala žádná
			 * chyba, tak mohu vrátí Úspěch pro oznámení úspěšného výsledku operace
			 * uživateli.
			 */
			return txtSuccess;
		}

		return txtFailure;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public String makeGetValueFromArrayAtIndexInInstance(final String command) {
		// Syntaxe: referenceName.variableArrayName[index];
		
		/*
		 * Zde ještě otestuji, zda se na začátku příkazu nachází mínus, pokud ano, pak
		 * jej musím odebrat, abych si pak při zjišťování jednotlivých hodnot
		 * nepřidělával práci, navíc si jej potřebuji uložit, abych věděl, zda mám po
		 * získání hodnoty znegovat.
		 */
		final boolean isMinus = isMinusAtFirstChar(command);
		
		
		/*
		 * Přikaz, kam se uložím uživatelem zadaný příkaz ale bez mínusu na začátku -
		 * pokud jej uživatel uvedl.
		 */
		final String commandEdited;

		if (isMinus)
			commandEdited = command.substring(command.indexOf('-') + 1);
		else
			commandEdited = command;
			

		/*
		 * Proměnná ukazující na instanci třídy: od začátku po první tečku:
		 */
		final String classNameVariable = commandEdited.substring(0, commandEdited.indexOf('.')).replaceAll("\\s", "");

		/*
		 * Proměnná, do které se má přiřadit hodnota: - od první desetinné tečky po
		 * první otevírací hranatou závorku:
		 */
		final String classVariable = commandEdited.substring(commandEdited.indexOf('.') + 1, commandEdited.indexOf('['))
				.replaceAll("\\s", "");
		
		
		/*
		 * Proměnná, do které se vloží index hodnoty v poli.
		 */
		final String indexInArray = commandEdited.substring(commandEdited.indexOf('[') + 1, commandEdited.lastIndexOf(']'))
				.replaceAll("\\s", "");
		
		
		/*
		 * Zde si mohu index rovnou přetypovat na integer, protože vím, že se jedná o
		 * číslo, ale uživatel může například zadat číslo pro datový typ long apod. To
		 * je chyba.
		 */
		final int index;
		try {
			index = Integer.parseInt(indexInArray);
		} catch (NumberFormatException e) {
			return txtFailure + ": " + txtCastingToIntegerFailure_1 + " '" + indexInArray + "' "
					+ txtCastingToIntegerFailure_2;
		}
		
		
		
		/*
		 * Načtu si instanci pod zadanou referencí (pokud existuje).
		 */
		final Object objInstance = Instances.getInstanceFromMap(classNameVariable);
		
		
		
		if (objInstance == null)
			return txtFailure + ": " + txtInstanceOfClassInIdNotFOund + ": " + classNameVariable;
		
		
		/*
		 * Získám si položuk v zadané instanci pod zadaným jménem, zde ještě nevím, zda
		 * se jedná o jednorozměrné pole.
		 */
		final Field field = getFieldFromInstance(objInstance, classVariable);
		
		if (field == null)
			return txtFailure + ": " + txtVariableNotFoundInReferenceToInstanceOfClass;
		
		
		/*
		 * zde otestuji, zda se jedná o pole.
		 */
		if (!field.getType().isArray())
			return txtFailure + ": " + txtEnteredVariable_1 + " '" + field.getName() + "' " + txtEnteredVariable_2;
		
		
		/*
		 * Zde si ještě musím zjistit počet dimenzí, protože předchoí testování v
		 * případě, že je položka field pole vždy vrátí true, i v případě, že se jedná o
		 * například 4 - rozměrné pole apod.
		 */
		final int countOfDims = getCountOfDimensionsOfArray(field.getType());
		
		if (countOfDims != 1)
			return txtFailure + ": " + txtVariable + " '" + field.getName() + "' " + txtIsNotOneDimArray;
				
		
		/*
		 * Zde si získám hodnotu v zadané proměnné - zadané pole.
		 */
		final Object objArrayValue = ReflectionHelper.getValueFromField(field, objInstance);
		
		if (objArrayValue == null)
			// Nemusí být viditelné apod., ale to se již vypsalo:
			return txtFailure;
		
		/*
		 * Zde si zjistím velikost příslušného pole.
		 */
		final int arrayLength = Array.getLength(objArrayValue);
		
		if (index >= arrayLength)
			return txtFailure + ": " + txtIndexValueAtArrayIsNotInInterval + ": <0 ; " + arrayLength + ").";
		
		
		/*
		 * Hískám si hodnotu z pole na zadaném indexu:
		 */
		Object objValueAtindex = Array.get(objArrayValue, index);
		
		/*
		 * Zde ještě otestuji, zda se má hodnota znegovat - pokud to lze.
		 */
		if (isMinus)
			objValueAtindex = getNegationOfValue(field.getType().getComponentType(), objValueAtindex);

		
		
		
		
		
		
		// Otestuji, zda se mají zvýraznit nějaké instance / buňky v diagramu instancí:
		instanceDiagram.checkHighlightOfInstances(objValueAtindex);

		// Otestuji, zda semají vypisovat reference za instance:
		if (OutputEditor.isShowReferenceVariables())
			/*
			 * Tento kód testuje, zda je příslušná hodnota instance, která se nachází v
			 * diagramu instnací a pokud ano, pak se za tu instnaci (při výpisu) ještě
			 * doplní referenční proměnná, která značí tu referenci na tu instanci v
			 * diagramu instancí.
			 */
			return txtSuccess + ": "
					+ OperationsWithInstances.getReturnValueWithReferenceVariableForPrint(objValueAtindex);
		
		
		/*
		 * Zde se nemají vypisovat reference za instance.
		 * 
		 * Zde mohu vrátit úspěch, ještě bych zde mohl otestovat, zda se nejedná náhodou
		 * o pole, ale zde vím, že je to jednorozměrné pole a není to další pole, takže
		 * by se hodnota měla "normálně" vypsat a pokud je to nějaký objekt a né hodnota
		 * primitivního datového typu, pak se vypíše text z metody toString().
		 */
		return txtSuccess + ": " + objValueAtindex;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public String makeGetLengthOfArrayInInstance(final String command) {
		// Syntaxe: referenceName.variableArrayName.length;
		
		/*
		 * Zjistím si, zda se má hodnota znegovat, tj., zda se na začátku příkazu
		 * nachází mínus.
		 */
		final boolean isMinus = isMinusAtFirstChar(command);
		
		/*
		 * Do této proměnné si vložím celý zadaný příkaz od uživatele, ale pouze bez
		 * mínusu na začátku příkazu - pokud je tam uveden pro negaci hodnoty.
		 */
		final String commandEdited;

		if (isMinus)
			commandEdited = command.substring(command.indexOf('-') + 1);
		else
			commandEdited = command;
		
		/*
		 * Části příkazu rozdělené dle desetinné tečky.
		 */
		final String[] partsOfCommand = commandEdited.split("\\.");

		/*
		 * Název reference na instanci třídy, reference, která má ukazovat na příslušnou
		 * instanci v diagramu instancí.
		 */
		final String referenceName = partsOfCommand[0].replaceAll("\\s", "");

		/*
		 * Název proměnné, která má být typu X - rozměrné pole.
		 */
		final String fieldName = partsOfCommand[1].replaceAll("\\s", "");

		
		
		
		/*
		 * Načtu si instanci pod zadanou referencí (pokud existuje).
		 */
		final Object objInstance = Instances.getInstanceFromMap(referenceName);

		if (objInstance == null)
			return txtFailure + ": " + txtInstanceOfClassInIdNotFOund + ": " + referenceName;

		
		
		
		/*
		 * Získám si položuk v zadané instanci pod zadaným jménem, zde ještě nevím, zda
		 * se jedná o jednorozměrné pole.
		 */
		final Field field = getFieldFromInstance(objInstance, fieldName);

		if (field == null)
			return txtFailure + ": " + txtVariableNotFoundInReferenceToInstanceOfClass;

		
		
		/*
		 * zde otestuji, zda se jedná o pole.
		 */
		if (!field.getType().isArray())
			return txtFailure + ": " + txtEnteredVariable_1 + " '" + field.getName() + "' " + txtEnteredVariable_2;

		
		
		/*
		 * Zde si získám hodnotu v zadané proměnné - zadané pole.
		 */
		final Object objArrayValue = ReflectionHelper.getValueFromField(field, objInstance);

		if (objArrayValue == null)
			// Nemusí být viditelné apod., ale to se již vypsalo:
			return txtFailure;

		
		
		/*
		 * Zde si zjistím velikost příslušného pole.
		 */
		int arrayLength = Array.getLength(objArrayValue);
		
		/*
		 * Zde ještě otestuji, zda se má hodnota znegovat.
		 */
		if (isMinus)
			arrayLength = (int) getNegationOfValue(int.class, arrayLength);

		// Zde vrátím Úspěch s příslušnou hodnotou:
		return txtSuccess + ": " + arrayLength;
	}
	
	
	
	
	
	
	
	
	
	 
	 
	 
	 
	 





	/**
	 * Metoda, která slouží pro zjištění kolik dimenzí má pole v parametru této
	 * metody ("array").
	 * 
	 * @param array
	 *            - X - rozměrné pole o kterém se má zjistit, kolika rozměrné to
	 *            pole je, tj. kolik má dimehí.
	 * 
	 * @return hodnotu typu int,která značí počet dimenzí v poli array, nebo likol
	 *         má pole array dimenzí (kolika rozměrné je).
	 */
	private static int getCountOfDimensionsOfArray(final Class<?> array) {
		/*
		 * Proměnná, do které se budou postupně ukládat počet zjištěných dimenzí.
		 */
		int count = 0;

		Class<?> clazz = array;

		while (clazz.isArray()) {
			count++;
			clazz = clazz.getComponentType();
		}

		return count;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public String makePrefixIncrementValueAtArrayInInstance(final String command) {
		// Syntaxe: ++ referenceName.variableArrayname[index];
		
		/*
		 * Název reference na instanci třídy v diagramu tříd se nachází od posledního
		 * plusu (+) po první desetinnou tečku.
		 */
		final String referenceName = command.substring(command.lastIndexOf('+') + 1, command.indexOf('.'))
				.replaceAll("\\s", "");

		/*
		 * Název proměnné se nachází od první desetinné tečky po první otevírací
		 * hranatou závorku.
		 */
		final String arrayName = command.substring(command.indexOf('.') + 1, command.indexOf('[')).replaceAll("\\s",
				"");

		/*
		 * Index se nachází mezi hranatými závorkami.
		 */
		final String indexInArray = command.substring(command.indexOf('[') + 1, command.indexOf(']')).replaceAll("\\s",
				"");

		final int index;

		try {
			index = Integer.parseInt(indexInArray);
		} catch (NumberFormatException e) {
			return txtFailure + ": " + txtCastingToIntegerFailure_1 + " '" + indexInArray + "' "
					+ txtCastingToIntegerFailure_2;
		}
		

		// zkusím inkrementovat hodnotu:
		return increment_decrementValueAtArrayInInstance(referenceName, index, arrayName, true);
	}
	
	

	@Override
	public String makePrefixDecrementValueAtArrayInInstance(final String command) {
		// Syntaxe: -- referenceName.variableArrayname[index];
		
		/*
		 * Název reference na instanci třídy v diagramu tříd se nachází od posledního
		 * plusu (+) po první desetinnou tečku.
		 */
		final String referenceName = command.substring(command.lastIndexOf('-') + 1, command.indexOf('.'))
				.replaceAll("\\s", "");

		/*
		 * Název proměnné se nachází od první desetinné tečky po první otevírací
		 * hranatou závorku.
		 */
		final String arrayName = command.substring(command.indexOf('.') + 1, command.indexOf('[')).replaceAll("\\s",
				"");

		/*
		 * Index se nachází mezi hranatými závorkami.
		 */
		final String indexInArray = command.substring(command.indexOf('[') + 1, command.indexOf(']')).replaceAll("\\s",
				"");

		final int index;

		try {
			index = Integer.parseInt(indexInArray);
		} catch (NumberFormatException e) {
			return txtFailure + ": " + txtCastingToIntegerFailure_1 + " '" + indexInArray + "' "
					+ txtCastingToIntegerFailure_2;
		}
		

		// zkusím inkrementovat hodnotu:
		return increment_decrementValueAtArrayInInstance(referenceName, index, arrayName, false);
	}

	
	@Override
	public String makePostfixIncrementValueAtArrayInInstance(final String command) {
		// Syntaxe: referenceName.variableArrayname[index] ++;
		
		/*
		 * Název reference na instanci třídy v diagramu tříd se nachází od začátku
		 * (prvního znaku) po první desetinnou tečku.
		 */
		final String referenceName = command.substring(0, command.indexOf('.')).replaceAll("\\s", "");

		/*
		 * Název proměnné se nachází od první desetinné tečky po první otevírací
		 * hranatou závorku.
		 */
		final String arrayName = command.substring(command.indexOf('.') + 1, command.indexOf('[')).replaceAll("\\s",
				"");

		/*
		 * Index se nachází mezi hranatými závorkami.
		 */
		final String indexInArray = command.substring(command.indexOf('[') + 1, command.indexOf(']')).replaceAll("\\s",
				"");

		final int index;

		try {
			index = Integer.parseInt(indexInArray);
		} catch (NumberFormatException e) {
			return txtFailure + ": " + txtCastingToIntegerFailure_1 + " '" + indexInArray + "' "
					+ txtCastingToIntegerFailure_2;
		}
		

		// zkusím inkrementovat hodnotu:
		return increment_decrementValueAtArrayInInstance(referenceName, index, arrayName, true);
	}

	
	@Override
	public String makePostfixDecrementValueAtArrayInInstance(final String command) {
		// Syntaxe: referenceName.variableArrayname[index] --;

		/*
		 * Název reference na instanci třídy v diagramu tříd se nachází od začátku
		 * (prvního znaku) po první desetinnou tečku.
		 */
		final String referenceName = command.substring(0, command.indexOf('.')).replaceAll("\\s", "");

		/*
		 * Název proměnné se nachází od první desetinné tečky po první otevírací
		 * hranatou závorku.
		 */
		final String arrayName = command.substring(command.indexOf('.') + 1, command.indexOf('[')).replaceAll("\\s",
				"");

		/*
		 * Index se nachází mezi hranatými závorkami.
		 */
		final String indexInArray = command.substring(command.indexOf('[') + 1, command.indexOf(']')).replaceAll("\\s",
				"");

		final int index;

		try {
			index = Integer.parseInt(indexInArray);
		} catch (NumberFormatException e) {
			return txtFailure + ": " + txtCastingToIntegerFailure_1 + " '" + indexInArray + "' "
					+ txtCastingToIntegerFailure_2;
		}
		

		// zkusím inkrementovat hodnotu:
		return increment_decrementValueAtArrayInInstance(referenceName, index, arrayName, false);
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro inkrementování nebo dekrementování hodnoty v
	 * jednorozměrném poli v nějaké instanci třídy.
	 * 
	 * @param referenceName
	 *            - reference na instanci třídy, kde se nachází pole s hodnotou pro
	 *            inkrementaci nebo dekrementaci (dle proměnné increment). Jedná se
	 *            o referenci na intanci v diagramu instancí.
	 * 
	 * @param index
	 *            - jedná se o hodnotu typu int, která značí index hodnoty v
	 *            příslušném jednorozměrném poli, kde se má inkrementovat nebo
	 *            dekrementovat hodnota.
	 * 
	 * @param arrayName
	 *            - název proměnné v insanci třídy referenceName. Mělo by se jednat
	 *            o pole, kde se má inkrementovat nebo dekremetnovat nějaká položka.
	 * 
	 * @param increment
	 *            - logická proměnná, která značí, zda se má inkrementovat nebo
	 *            dekrementovat příslušná hodnota v poli. True značí inkrementace a
	 *            false dekremetaci.
	 * 
	 * @return Úspěch pokud se úspěšně inkrementuje nebo dekrementuje příslušná
	 *         hodnota v poli, jinak neúspěch a případně nějaké hlášení o nastalé
	 *         chybě.
	 */
	private String increment_decrementValueAtArrayInInstance(final String referenceName, final int index,
			final String arrayName, final boolean increment) {
		/*
		 * Načtu si instanci pod zadanou referencí (pokud existuje).
		 */
		final Object objInstance = Instances.getInstanceFromMap(referenceName);

		if (objInstance == null)
			return txtFailure + ": " + txtInstanceOfClassInIdNotFOund + ": " + referenceName;

		
		
		/*
		 * Získám si položku v zadané instanci pod zadaným jménem, zde ještě nevím, zda
		 * se jedná o jednorozměrné pole.
		 */
		final Field field = getFieldFromInstance(objInstance, arrayName);

		if (field == null)
			return txtFailure + ": " + txtVariableNotFoundInReferenceToInstanceOfClass;

		
		
		/*
		 * zde otestuji, zda se jedná o pole.
		 */
		if (!field.getType().isArray())
			return txtFailure + ": " + txtEnteredVariable_1 + " '" + field.getName() + "' " + txtEnteredVariable_2;

		
		/*
		 * Zde si ještě musím zjistit počet dimenzí, protože předchoí testování v
		 * případě, že je položka field pole vždy vrátí true, i v případě, že se jedná o
		 * například 4 - rozměrné pole apod.
		 */
		final int countOfDims = getCountOfDimensionsOfArray(field.getType());

		if (countOfDims != 1)
			return txtFailure + ": " + txtVariable + " '" + field.getName() + "' " + txtIsNotOneDimArray;

		
		
		/*
		 * Zde si získám hodnotu v zadané proměnné - zadané pole.
		 */
		final Object objArrayValue = ReflectionHelper.getValueFromField(field, objInstance);

		if (objArrayValue == null)
			// Nemusí být viditelné apod., ale to se již vypsalo:
			return txtFailure;

		
		
		/*
		 * Zde si zjistím velikost příslušného pole.
		 */
		final int arrayLength = Array.getLength(objArrayValue);

		if (index >= arrayLength)
			return txtFailure + ": " + txtIndexValueAtArrayIsNotInInterval + ": <0 ; " + arrayLength + ").";

		
		/*
		 * Získám si hodnotu z pole na zadaném indexu:
		 */
		Object objValueAtindex = Array.get(objArrayValue, index);

		if (objValueAtindex == null)
			return txtFailure + ": " + txtValueInArrayIsNull;

		
		/*
		 * Do této proměnné si uložím hodnotu, která se měla inkrementovat /
		 * dekrementovat, pokud bude null, tak se nepodařilo nebo nemohla inkremetovat /
		 * dekrementovat.
		 */
		final Object objFinalValue = getIncrementDecrementValue(objValueAtindex, increment);

		if (objFinalValue == null)
			return txtFailure + ": " + txtValueAtIndexAtArrayCantIncreDecre;

		
		// tak nastavím novou hodnotu do proměnné:
		Array.set(objArrayValue, index, objFinalValue);

        // Aktualizace hodnot zobrazené v instanci v diagramu instancí:
        instanceDiagram.updateInstanceCellValues();

        /*
         * Zde je třeba aktualizovat hodnoty v okně pro automatické doplňění / dokončení hodnot v editoru příkazů.
         * Jinak by se v tom okně nezobrazily aktuální hodnoty ve výše nově nastavené hodnotě v poli.
         */
        updateValueCompletionWindow();
		
		// Oznámím, že se hodnota úspěšně inkrementovala / dekrementovala:
		return txtSuccess;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Následují metody pro dvourozměrné pole v instanci nějaké třídy, která
	// senachází v diagramu instancí.
	
	
	
	
	@Override
	public String makeFulfillmenIndexAtTwoDimArrayByMethod(final String command) {
		return makeFulfillmentIndexAtTwoDimArrayInInstance(command);
	}

	@Override
	public String makeFulfillmenIndexAtTwoDimArrayByConstructor(final String command) {
		return makeFulfillmentIndexAtTwoDimArrayInInstance(command);
	}

	@Override
	public String makeFulfillmenIndexAtTwoDimArrayByVariable(final String command) {
		return makeFulfillmentIndexAtTwoDimArrayInInstance(command);
	}

	@Override
	public String makeFulfillmenIndexAtTwoDimArrayByConstant(final String command) {
		return makeFulfillmentIndexAtTwoDimArrayInInstance(command);
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro nastavení nějaké položky ve dvourozměrném poli v
	 * nějaké instanci třídy.
	 * 
	 * @param command
	 *            - uživatelem zadaný příkaz v editoru příkazů.
	 * 
	 * @return Úspěch, pokud se úspěšně nastaví hodnota za zadaném indexu příslušnou
	 *         hodnotou, jinak Neúspěch a případně i hlášení o nastalé chybě.
	 */
	private String makeFulfillmentIndexAtTwoDimArrayInInstance(final String command) {
		/*
		 * Syntaxe: referenceName . variableArrayname [index] [index] = someValue;
		 * 
		 * SomeValue může být například zavoání konstruktoru, metody, konstanta,
		 * proměnná apod.
		 */
		
		/*
		 * Proměnná, do které si vložím text příkazu od začátku po první rovná se.
		 * 
		 * Tj. Bude zde pouze syntaxe: referenceName . variableArrayname [index] [index]
		 */
		final String textBeforeEquals = command.substring(0, command.indexOf('='));

		/*
		 * Proměnná ukazující na instanci třídy: od začátku po první tečku:
		 */
		final String classNameVariable = textBeforeEquals.substring(0, textBeforeEquals.indexOf('.')).replaceAll("\\s",
				"");
		

		/*
		 * Proměnná, do které se má přiřadit hodnota: - od první desetinné tečky po
		 * první otevírací hranatou závorku:
		 */
		final String classVariable = textBeforeEquals
				.substring(textBeforeEquals.indexOf('.') + 1, textBeforeEquals.indexOf('[')).replaceAll("\\s", "");
		
		
		/*
		 * Proměnná, do které se vloží první index hodnoty v poli. Ten je mezi první
		 * otevírací a první zavíracá hranatou závorkou:
		 */
		final String indexInArray_1 = textBeforeEquals
				.substring(textBeforeEquals.indexOf('[') + 1, textBeforeEquals.indexOf(']')).replaceAll("\\s", "");
		
		

		/*
		 * Proměnná, do které se vloží druhý index hodnoty v poli, ten se nachází mezi
		 * druhou, resp. poslední otevírací a druhou, resp. poslední zavírací závorkou.
		 */
		final String indexInArray_2 = textBeforeEquals
				.substring(textBeforeEquals.lastIndexOf('[') + 1, textBeforeEquals.lastIndexOf(']'))
				.replaceAll("\\s", "");
		
		
		
		
		/*
		 * Zde si mohu index rovnou přetypovat na integer, protože vím, že se jedná o
		 * číslo, ale uživatel může například zadat číslo pro datový typ long apod. To
		 * je chyba.
		 */
		final int index_1, index_2;
		
		try {
			index_1 = Integer.parseInt(indexInArray_1);
		} catch (NumberFormatException e) {
			return txtFailure + ": " + txtCastingToIntegerFailure_1 + " '" + indexInArray_1 + "' "
					+ txtCastingToIntegerFailure_2;
		}
		
		
		try {
			/*
			 * Zde zkusím přetypovat i druhý index položky v poli, ale v jiném bloku
			 * try-catch, abych věděl, v jakém indexu je problém.
			 */
			index_2 = Integer.parseInt(indexInArray_2);
		} catch (NumberFormatException e) {
			return txtFailure + ": " + txtCastingToIntegerFailure_1 + " '" + indexInArray_2 + "' "
					+ txtCastingToIntegerFailure_2;
		}
		
		
		
		/*
		 * Načtu si instanci pod zadanou referencí (pokud existuje).
		 */
		final Object objInstance = Instances.getInstanceFromMap(classNameVariable);
		
		
		
		if (objInstance == null)
			return txtFailure + ": " + txtInstanceOfClassInIdNotFOund + ": " + classNameVariable;
		
		
		/*
		 * Získám si položuk v zadané instanci pod zadaným jménem, zde ještě nevím, zda
		 * se jedná o jednorozměrné pole.
		 */
		final Field field = getFieldFromInstance(objInstance, classVariable);
		
		if (field == null)
			return txtFailure + ": " + txtVariableNotFoundInReferenceToInstanceOfClass;
		
		
		/*
		 * zde otestuji, zda se jedná o pole.
		 */
		if (!field.getType().isArray())
			return txtFailure + ": " + txtEnteredVariable_1 + " '" + field.getName() + "' " + txtEnteredVariable_2;
		
		
		/*
		 * Zde si ještě musím zjistit počet dimenzí, protože předchoí testování v
		 * případě, že je položka field pole vždy vrátí true, i v případě, že se jedná o
		 * například 4 - rozměrné pole apod.
		 */
		final int countOfDims = getCountOfDimensionsOfArray(field.getType());
		
		if (countOfDims != 2)			
			return txtFailure + ": " + txtVariable + " '" + field.getName() + "' " + txtIsNotTwoDimArray;
				
		
		
		/*
		 * Zde si získám hodnotu v zadané proměnné - zadané pole.
		 */
		final Object objArrayValue = ReflectionHelper.getValueFromField(field, objInstance);
		
		if (objArrayValue == null)
			// Nemusí být viditelné apod., ale to se již vypsalo:
			return txtFailure;
		
		
		
		/*
		 * Note:
		 * 
		 * Zde nemohu testovat zadané indexy s velikostí pole, protože pole může být například vytvořeno tak, že se napíše:
		 * 
		 * int[][] array = {{1, 2, 3, 4}, {4, 5, 6}};
		 * 
		 * pak by bylo například 2x4 nebo 2x3, záleží, jak bych hledal velikost pole, pokud se deklarují
		 * s velikostí pole, pak je to OK, ale v tomto případě ne, proto zde nebude testovat velikosti pole,
		 * ale až při nastavování hodnoty si zjistím, zda se hodnota nastavila nebo ne, pokud ano, pak je to OK, ale
		 * pokud ne, pak v případě, že nedošlo k nejaké chybě, jen se prošlo pole a neshodovaly se indexy, tak vypíšu, že byly
		 * chybně zadány indexy.
		 */
		
		
		
		/*
		 * Zde si získám část, kde je zadána hodnota kterou se má naplnit index v poli,
		 * tj. od prvního rovná se po poslední středník - konec příkazu.
		 */
		final String textWithMethod = command.substring(command.indexOf('=') + 1, command.lastIndexOf(';'));

		
		
		
		
		/*
		 * Zjistím si datový typ pole:
		 */
		final Class<?> dataTypeOfArray = ReflectionHelper.getDataTypeOfArray(field.getType());
		
		/*
		 * Zde zjistím hodnotu - metodu, proměnnou, ..., musí vrátit stejný datový typ jako je
		 * datový typ zadaného pole.
		 */
		final ResultValue objResultValue = getValueFromVar_Method_Constant(textWithMethod,
				dataTypeOfArray);

		
		/*
		 * Pokud se nepodařilo vrátit ReturnValue (nemělo by nastat) nebo nebyla
		 * provedena operace pro zavolání metody, pak vypíšu neúspěch s tím, že by
		 * informace ohledně chyby, ke které došlo měly být vypsány do editoru výstupů.
		 */
		if (objResultValue == null || !objResultValue.isExecuted())
			return txtFailure;

		
		/*
		 * Pro zjednodušení si zde vytvořím proměnnou, do které vložím získanou hodnotu
		 * z metody (abych jí nemusel pořád načítat).
		 */
		final Object objValue = objResultValue.getObjResult();

		/*
		 * Pokud byla získána hodnota, pak v případě, že se nejedná o null hodnotu jej
		 * zkusím zadat do proměnné, v opačném případě vrátím null.
		 */
		if (objValue != null) {
			/*
			 * Zavolám metodu, která proiteruje pole objArrayValue a na zadané indexy vloží
			 * hodnotu objValue, pokud se nepodaří projít pole a najít příslušné indexy, pak
			 * se vypíše chybová hláška, že nebylo nastavena hodnota v poli, že je možné, že
			 * nebyly zadány správné indexy.
			 */
			final boolean set = setValueAtIndexInTwoDimArray(objArrayValue, index_1, index_2, objValue);

			if (!set)
				return txtFailure + ": " + txtFailedToSetValueToTwoDimArray + OutputEditor.NEW_LINE_TWO_GAPS
						+ txtIndex_1Text + ": '" + index_1 + "', " + txtIndex_2Text + ": '" + index_2 + "', " + txtValue
						+ ": " + objValue;

		
			/*
			 * Zde se podařilo úspěšně naplnit nějakou hodnotu ve dvou rozměrném poli v poli
			 * v nějaké instanci v diagramu instancí. Takže je potřeba, aby se přenačetly
			 * vztahy, protože se ta položka v poli mohla například naplnit nějakou intanci
			 * apod. Tak potřebuji znovu zkontrolovat vztahy mezi instancemi:
			 */
			runThreadForCheckInstancesRelations();

            /*
             * Zde je třeba aktualizovat hodnoty v okně pro automatické doplnění / dokončení příkazu v editoru
             * příkazů. Jinak by se v tom okně neprojevila výše změněná hodnota.
             */
            updateValueCompletionWindow();

			/*
			 * Výše se již do pole vložila hodnota a v proměnné set je true, takže vím, že
			 * je vše v pořádku, tak mohu vrátit úspěch pro oznámení úsěpšné provedení
			 * operace.
			 */
			return txtSuccess;
		}

		return txtFailure;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která projde dvourozměrné pole objArray (už se musí vědět, že se
	 * jedná o dvourozměrné pole, jinak by mohlo dojít k nějaké chybě). Celé
	 * objArray - dvourozměrné pole se projde a vždy se bude testovat, zda se
	 * aktuálně iteruje index index_1 a index_2, pokud ano, pak se na tento index v
	 * poli objArray nastaví hodnota objNewValue.
	 * 
	 * @param objArray
	 *            - Objekt -> dvourozměrné pole, ve kterém se má nastavit hodnota na
	 *            indexu [index_1][index_2] na objNewValue
	 * 
	 * @param index_1
	 *            - index "prvního" pole.
	 * 
	 * @param index_2
	 *            - index "druhého / vnitřního" pole.
	 * 
	 * @param objNewValue
	 *            - Hodnota, která se má nastavit na příslušný index v poli
	 *            objArray.
	 * 
	 * @return true, pokud se při procházení pole nastaví hodnoty objNewValue na
	 *         příslušný index v poli objArray, false v případě, že se celé pole
	 *         objArray projde ale zadaný index / index se nenajdou, takže uživatel
	 *         zadal chybné indexy, proto nebylo možné nastavit hodnotu v poli.
	 */
	private static boolean setValueAtIndexInTwoDimArray(final Object objArray, final int index_1, final int index_2,
			final Object objNewValue) {
		/*
		 * Zjistím si velikost pole:
		 */
		final int size = Array.getLength(objArray);

		/*
		 * Projdu celé pole a v každé iteraci si načtu pole v něm a u to projdu a při
		 * každé iteraci testuji zda se shodují indexy, na které se má vložit hodnota.
		 */
		for (int i = 0; i < size; i++) {
			final Object value = Array.get(objArray, i);

			if (value != null && value.getClass().isArray()) {
				/*
				 * Získám si pole na příslušném indexu:
				 */
				final int size2 = Array.getLength(value);				

				// Projdu i druhé pole a testuji indexy pro vložení hodnoty:
				for (int q = 0; q < size2; q++) {
					/*
					 * Otestuji, zda se nacházím na požadovaném indexu, pokud ano, nastavím na něj
					 * požadovanou hodnotu.
					 */
					if (i == index_1 && q == index_2) {
						Array.set(value, q, objNewValue);
						
						// Mohu vrátit true -> nastavila se položka na zadaném indexu:
						return true;
					}
				}
			}
		}
		
		/*
		 * Zde vrátím false, prošel jsem celé pole, ale nebyla nastavena požadovaná
		 * položka, nejspíše byly zadány chybné indexy.
		 */
		return false;
	}
	
	
	
	





	
	
	
	
	
	
	
	@Override
	public String makeGetValueAtIndexAtTwoDimArrayInInstance(final String command) {
		// Syntaxe: referenceName . variableArrayname [index] [index];
		
		/*
		 * Proměnná ukazující na instanci třídy: od začátku po první tečku:
		 */
		final String classNameVariable = command.substring(0, command.indexOf('.')).replaceAll("\\s", "");

		/*
		 * Proměnná, do které se má přiřadit hodnota: - od první desetinné tečky po
		 * první otevírací hranatou závorku:
		 */
		final String classVariable = command.substring(command.indexOf('.') + 1, command.indexOf('[')).replaceAll("\\s",
				"");

		/*
		 * Proměnná, do které se vloží první index hodnoty v poli. Ten je mezi první
		 * otevírací a první zavíracá hranatou závorkou:
		 */
		final String indexInArray_1 = command.substring(command.indexOf('[') + 1, command.indexOf(']'))
				.replaceAll("\\s", "");

		/*
		 * Proměnná, do které se vloží druhý index hodnoty v poli, ten se nachází mezi
		 * druhou, resp. poslední otevírací a druhou, resp. poslední zavírací závorkou.
		 */
		final String indexInArray_2 = command.substring(command.lastIndexOf('[') + 1, command.lastIndexOf(']'))
				.replaceAll("\\s", "");
		
		
		
		
		/*
		 * Zde si mohu index rovnou přetypovat na integer, protože vím, že se jedná o
		 * číslo, ale uživatel může například zadat číslo pro datový typ long apod. To
		 * je chyba.
		 */
		final int index_1, index_2;
		
		try {
			index_1 = Integer.parseInt(indexInArray_1);
		} catch (NumberFormatException e) {
			return txtFailure + ": " + txtCastingToIntegerFailure_1 + " '" + indexInArray_1 + "' "
					+ txtCastingToIntegerFailure_2;
		}
		
		
		try {
			/*
			 * Zde zkusím přetypovat i druhý index položky v poli, ale v jiném bloku
			 * try-catch, abych věděl, v jakém indexu je problém.
			 */
			index_2 = Integer.parseInt(indexInArray_2);
		} catch (NumberFormatException e) {
			return txtFailure + ": " + txtCastingToIntegerFailure_1 + " '" + indexInArray_2 + "' "
					+ txtCastingToIntegerFailure_2;
		}
		
		
		
		/*
		 * Načtu si instanci pod zadanou referencí (pokud existuje).
		 */
		final Object objInstance = Instances.getInstanceFromMap(classNameVariable);
		
		
		if (objInstance == null)
			return txtFailure + ": " + txtInstanceOfClassInIdNotFOund + ": " + classNameVariable;
		
		
		/*
		 * Získám si položuk v zadané instanci pod zadaným jménem, zde ještě nevím, zda
		 * se jedná o jednorozměrné pole.
		 */
		final Field field = getFieldFromInstance(objInstance, classVariable);

		if (field == null)
			return txtFailure + ": " + txtVariableNotFoundInReferenceToInstanceOfClass;
		
		
		/*
		 * zde otestuji, zda se jedná o pole.
		 */
		if (!field.getType().isArray())
			return txtFailure + ": " + txtEnteredVariable_1 + " '" + field.getName() + "' " + txtEnteredVariable_2;
		
		
		/*
		 * Zde si ještě musím zjistit počet dimenzí, protože předchoí testování v
		 * případě, že je položka field pole vždy vrátí true, i v případě, že se jedná o
		 * například 4 - rozměrné pole apod.
		 */
		final int countOfDims = getCountOfDimensionsOfArray(field.getType());

		if (countOfDims != 2)
			return txtFailure + ": " + txtVariable + " '" + field.getName() + "' " + txtIsNotTwoDimArray;
		
		
		/*
		 * Zde si získám hodnotu v zadané proměnné - zadané pole.
		 */
		final Object objArrayValue = ReflectionHelper.getValueFromField(field, objInstance);

		if (objArrayValue == null)
			// Nemusí být viditelné apod., ale to se již vypsalo:
			return txtFailure;
		
		
		
		
		/*
		 * Nyní budu muset projít celé pole a získat hodnotu na zadaném indexu
		 */
		final ResultValue resultValue = getValueAtIndexInTwoDimArray(objArrayValue, index_1, index_2);

		if (!resultValue.isExecuted())
			return txtFailure + ": " + txtFailedToGetValueFromTwoDimArray + OutputEditor.NEW_LINE_TWO_GAPS
					+ txtIndex_1Text + ": '" + index_1 + "', " + txtIndex_2Text + ": '" + index_2 + "'.";

		
		
		
		
		// Otestuji, zda se mají zvýraznit nějaké instance / buňky v diagramu instancí:
		instanceDiagram.checkHighlightOfInstances(resultValue.getObjResult());
		
		
		// Zda se mají vypisovat i reference za instance:
		if (OutputEditor.isShowReferenceVariables())
			/*
			 * Následující kód navíc testuje, zda je získaná hodnota instance, která se
			 * nachází v diagramu instancí a pokud ano, pak se při výpisu ještě na konec,
			 * resp. za tu instanci se přidá název reference neboli referenční proměnná na
			 * tu instanci v diagramu instnací, kterou zadal uživatel při jejím vytvoření.
			 */
			return txtSuccess + ": "
					+ OperationsWithInstances.getReturnValueWithReferenceVariableForPrint(resultValue.getObjResult());

		
		
		
		/*
		 * Zde se nemají vypisovat i reference za instance.
		 * 
		 * Zde už mohu vrátit nalezenou hodnotu, ať už je to null nebo cokoliv jiného,
		 * podařilo se získat hodnotu na zadaném indexu:
		 */
		return txtSuccess + ": " + resultValue.getObjResult();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která projde dvourozměrné pole objArray (už se musí vědět, že se
	 * jedná o dvourozměrné pole, jinak by mohlo dojít k nějaké chybě). Celé
	 * objArray - dvourozměrné pole se projde a vždy se bude testovat, zda se
	 * aktuálně iteruje index index_1 a index_2, pokud ano, pak se z tohoto indexu
	 * vezme hodnota a vrátí se.
	 * 
	 * @param objArray
	 *            - Objekt -> dvourozměrné pole, ze kterého se má načíst hodnota na
	 *            indexu [index_1][index_2]
	 * 
	 * @param index_1
	 *            - index "prvního" pole.
	 * 
	 * @param index_2
	 *            - index "druhého / vnitřního" pole.
	 * 
	 * @return objekt ResultValue, který obsahuje true a příslušnou hodnotu v
	 *         případě, že byla nalezena hodnota v poli objArray na zadaném indexu,
	 *         jinak vrátí příslušný objekt s false a null, null, že nebyla nalezena
	 *         hodnota, a hlavně false, že nebyly nalezeny index v příslušném poli.
	 */
	private static ResultValue getValueAtIndexInTwoDimArray(final Object objArray, final int index_1,
			final int index_2) {
		/*
		 * Zjistím si velikost pole:
		 */
		final int size = Array.getLength(objArray);

		/*
		 * Projdu celé pole a v každé iteraci si načtu pole v něm a u to projdu a při
		 * každé iteraci testuji zda se shodují indexy, které obsahují hodnotu pro
		 * získání / vrácení.
		 */
		for (int i = 0; i < size; i++) {
			final Object value = Array.get(objArray, i);

			if (value != null && value.getClass().isArray()) {
				/*
				 * Získám si pole na příslušném indexu:
				 */
				final int size2 = Array.getLength(value);				

				// Projdu i druhé pole a testuji indexy pro získání / vrácení hodnoty:
				for (int q = 0; q < size2; q++) {
					/*
					 * Otestuji, zda se nacházím na požadovaném indexu, pokud ano, získám si z něj
					 * příslušnou hodnotu.
					 */
					if (i == index_1 && q == index_2) {
						/*
						 * Zde se shodují indexy, tak si získám hodnotu na příslušných indexech:
						 */
						final Object objValueAtIndex = Array.get(value, q);
						
						// Zde vrátím true a nalezenou hodnotu na indexu:
						return returnResult(true, objValueAtIndex);
					}
				}
			}
		}
		
		/*
		 * Pokud se dostanu sem, tak jsem prošel celé pole a nenašel jsem hodnoty na
		 * zadaných indexech, tak vrátím false a null, null, že jsem nenašel hodnotu a
		 * false, v tomto případě značí, že nebyly nalezeny indexy, resp. položka v poli
		 * na zadaných indecech.
		 */
		return returnResult(false, null);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public String makePrefixIncrementValueAtIndexInTwoDimArrayInInstance(final String command) {
		// Syntaxe: ++ referenceName.variableName [index][index];
		
		/*
		 * Název reference na instanci třídy v diagramu tříd se nachází od posledního
		 * plusu (+) po první desetinnou tečku.
		 */
		final String referenceName = command.substring(command.lastIndexOf('+') + 1, command.indexOf('.'))
				.replaceAll("\\s", "");

		/*
		 * Název proměnné se nachází od první desetinné tečky po první otevírací
		 * hranatou závorku.
		 */
		final String arrayName = command.substring(command.indexOf('.') + 1, command.indexOf('[')).replaceAll("\\s",
				"");

		/*
		 * První index se nachází mezi první otevírací a první zavárací hranatou
		 * závorkou:
		 */
		final String indexInArray_1 = command.substring(command.indexOf('[') + 1, command.indexOf(']'))
				.replaceAll("\\s", "");

		/*
		 * První index se nachází mezi poslední otevírací a poslední zavárací hranatou
		 * závorkou:
		 */
		final String indexInArray_2 = command.substring(command.lastIndexOf('[') + 1, command.lastIndexOf(']'))
				.replaceAll("\\s", "");

		/*
		 * Zde si mohu index rovnou přetypovat na integer, protože vím, že se jedná o
		 * číslo, ale uživatel může například zadat číslo pro datový typ long apod. To
		 * je chyba.
		 */
		final int index_1, index_2;
		
		try {
			index_1 = Integer.parseInt(indexInArray_1);
		} catch (NumberFormatException e) {
			return txtFailure + ": " + txtCastingToIntegerFailure_1 + " '" + indexInArray_1 + "' "
					+ txtCastingToIntegerFailure_2;
		}
		
		
		try {
			/*
			 * Zde zkusím přetypovat i druhý index položky v poli, ale v jiném bloku
			 * try-catch, abych věděl, v jakém indexu je problém.
			 */
			index_2 = Integer.parseInt(indexInArray_2);
		} catch (NumberFormatException e) {
			return txtFailure + ": " + txtCastingToIntegerFailure_1 + " '" + indexInArray_2 + "' "
					+ txtCastingToIntegerFailure_2;
		}

		/*
		 * Zavolám metodu, která se postará o inkrementaci příslušné položky - pokud
		 * byly zadány správné indexy
		 */
		return increment_decrementValueAtTwoDimArrayInInstance(referenceName, arrayName, index_1, index_2, true);
	}
	
	
	
	
	
	@Override
	public String makePrefixDecrementValueAtIndexInTwoDimArrayInInstance(final String command) {
		// Syntaxe: - referenceName.variableName [index][index];
		
		/*
		 * Název reference na instanci třídy v diagramu tříd se nachází od posledního
		 * plusu (+) po první desetinnou tečku.
		 */
		final String referenceName = command.substring(command.lastIndexOf('-') + 1, command.indexOf('.'))
				.replaceAll("\\s", "");

		/*
		 * Název proměnné se nachází od první desetinné tečky po první otevírací
		 * hranatou závorku.
		 */
		final String arrayName = command.substring(command.indexOf('.') + 1, command.indexOf('[')).replaceAll("\\s",
				"");

		/*
		 * První index se nachází mezi první otevírací a první zavárací hranatou
		 * závorkou:
		 */
		final String indexInArray_1 = command.substring(command.indexOf('[') + 1, command.indexOf(']'))
				.replaceAll("\\s", "");

		/*
		 * První index se nachází mezi poslední otevírací a poslední zavárací hranatou
		 * závorkou:
		 */
		final String indexInArray_2 = command.substring(command.lastIndexOf('[') + 1, command.lastIndexOf(']'))
				.replaceAll("\\s", "");

		/*
		 * Zde si mohu index rovnou přetypovat na integer, protože vím, že se jedná o
		 * číslo, ale uživatel může například zadat číslo pro datový typ long apod. To
		 * je chyba.
		 */
		final int index_1, index_2;
		
		try {
			index_1 = Integer.parseInt(indexInArray_1);
		} catch (NumberFormatException e) {
			return txtFailure + ": " + txtCastingToIntegerFailure_1 + " '" + indexInArray_1 + "' "
					+ txtCastingToIntegerFailure_2;
		}
		
		
		try {
			/*
			 * Zde zkusím přetypovat i druhý index položky v poli, ale v jiném bloku
			 * try-catch, abych věděl, v jakém indexu je problém.
			 */
			index_2 = Integer.parseInt(indexInArray_2);
		} catch (NumberFormatException e) {
			return txtFailure + ": " + txtCastingToIntegerFailure_1 + " '" + indexInArray_2 + "' "
					+ txtCastingToIntegerFailure_2;
		}

		/*
		 * Zavolám metodu, která se postará o inkrementaci příslušné položky - pokud
		 * byly zadány správné indexy
		 */
		return increment_decrementValueAtTwoDimArrayInInstance(referenceName, arrayName, index_1, index_2, false);
	}
	
	
	
	@Override
	public String makePostfixIncrementValueAtIndexInTwoDimArrayInInstance(final String command) {
		// Syntaxe: referenceName.variableName [index][index] ++;
		
		/*
		 * Název reference na instanci třídy v diagramu tříd se nachází od začátku po
		 * první desetinnou tečku.
		 */
		final String referenceName = command.substring(0, command.indexOf('.')).replaceAll("\\s", "");

		/*
		 * Název proměnné se nachází od první desetinné tečky po první otevírací
		 * hranatou závorku.
		 */
		final String arrayName = command.substring(command.indexOf('.') + 1, command.indexOf('[')).replaceAll("\\s",
				"");

		/*
		 * První index se nachází mezi první otevírací a první zavárací hranatou
		 * závorkou:
		 */
		final String indexInArray_1 = command.substring(command.indexOf('[') + 1, command.indexOf(']'))
				.replaceAll("\\s", "");

		/*
		 * První index se nachází mezi poslední otevírací a poslední zavárací hranatou
		 * závorkou:
		 */
		final String indexInArray_2 = command.substring(command.lastIndexOf('[') + 1, command.lastIndexOf(']'))
				.replaceAll("\\s", "");

		/*
		 * Zde si mohu index rovnou přetypovat na integer, protože vím, že se jedná o
		 * číslo, ale uživatel může například zadat číslo pro datový typ long apod. To
		 * je chyba.
		 */
		final int index_1, index_2;
		
		try {
			index_1 = Integer.parseInt(indexInArray_1);
		} catch (NumberFormatException e) {
			return txtFailure + ": " + txtCastingToIntegerFailure_1 + " '" + indexInArray_1 + "' "
					+ txtCastingToIntegerFailure_2;
		}
		
		
		try {
			/*
			 * Zde zkusím přetypovat i druhý index položky v poli, ale v jiném bloku
			 * try-catch, abych věděl, v jakém indexu je problém.
			 */
			index_2 = Integer.parseInt(indexInArray_2);
		} catch (NumberFormatException e) {
			return txtFailure + ": " + txtCastingToIntegerFailure_1 + " '" + indexInArray_2 + "' "
					+ txtCastingToIntegerFailure_2;
		}

		/*
		 * Zavolám metodu, která se postará o inkrementaci příslušné položky - pokud
		 * byly zadány správné indexy
		 */
		return increment_decrementValueAtTwoDimArrayInInstance(referenceName, arrayName, index_1, index_2, true);
	}
	
	
	@Override
	public String makePostfixDecrementValueAtIndexInTwoDimArrayInInstance(final String command) {
		// Syntaxe: referenceName.variableName [index][index] --;
		
		/*
		 * Název reference na instanci třídy v diagramu tříd se nachází od začátku po
		 * první desetinnou tečku.
		 */
		final String referenceName = command.substring(0, command.indexOf('.')).replaceAll("\\s", "");

		/*
		 * Název proměnné se nachází od první desetinné tečky po první otevírací
		 * hranatou závorku.
		 */
		final String arrayName = command.substring(command.indexOf('.') + 1, command.indexOf('[')).replaceAll("\\s",
				"");

		/*
		 * První index se nachází mezi první otevírací a první zavárací hranatou
		 * závorkou:
		 */
		final String indexInArray_1 = command.substring(command.indexOf('[') + 1, command.indexOf(']'))
				.replaceAll("\\s", "");

		/*
		 * První index se nachází mezi poslední otevírací a poslední zavárací hranatou
		 * závorkou:
		 */
		final String indexInArray_2 = command.substring(command.lastIndexOf('[') + 1, command.lastIndexOf(']'))
				.replaceAll("\\s", "");

		/*
		 * Zde si mohu index rovnou přetypovat na integer, protože vím, že se jedná o
		 * číslo, ale uživatel může například zadat číslo pro datový typ long apod. To
		 * je chyba.
		 */
		final int index_1, index_2;
		
		try {
			index_1 = Integer.parseInt(indexInArray_1);
		} catch (NumberFormatException e) {
			return txtFailure + ": " + txtCastingToIntegerFailure_1 + " '" + indexInArray_1 + "' "
					+ txtCastingToIntegerFailure_2;
		}
		
		
		try {
			/*
			 * Zde zkusím přetypovat i druhý index položky v poli, ale v jiném bloku
			 * try-catch, abych věděl, v jakém indexu je problém.
			 */
			index_2 = Integer.parseInt(indexInArray_2);
		} catch (NumberFormatException e) {
			return txtFailure + ": " + txtCastingToIntegerFailure_1 + " '" + indexInArray_2 + "' "
					+ txtCastingToIntegerFailure_2;
		}
		
		
		/*
		 * Zavolám metodu, která se postará o inkrementaci příslušné položky - pokud byly zadány správné indexy
		 */
		return increment_decrementValueAtTwoDimArrayInInstance(referenceName, arrayName, index_1, index_2, false);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro inkrementaci / dekrementaci hodnoty ve dvourozměrném
	 * poli, tato metoda ale pouze zjistí potřebná data, jako jsou například v jaké
	 * instanci třídy je to pole s hodnotami, indexy v poli, ... a pak se zavolá
	 * metoda, která tu hodnotu opravdu inkrementuje / dekrementuje.
	 * 
	 * @param referenceName
	 *            - název reference na instanci třídy.
	 * 
	 * @param arrayName
	 *            - název proměnné, která má být dvourozměrné pole v nějaké instanci
	 *            třídy s referencí referenceName.
	 * 
	 * @param index_1
	 *            - index hodnoty v poli (první pole)
	 * 
	 * @param index_2
	 *            - index hodnoty v poli (druhé pole / vnitřní pole)
	 * 
	 * @param increment
	 *            - true -> hodnota se má inkrementovat, false -> hodnota se má
	 *            dekrementovat.
	 * 
	 * @return text, který značí výsledek průběhu operace, buď Úspěch nebo Neúspěch
	 *         a případně nějaké info o nastalé chybě.
	 */
	private String increment_decrementValueAtTwoDimArrayInInstance(final String referenceName, final String arrayName,
			final int index_1, final int index_2, final boolean increment) {
		/*
		 * Načtu si instanci pod zadanou referencí (pokud existuje).
		 */
		final Object objInstance = Instances.getInstanceFromMap(referenceName);

		if (objInstance == null)
			return txtFailure + ": " + txtInstanceOfClassInIdNotFOund + ": " + referenceName;

		
		
		/*
		 * Získám si položku v zadané instanci pod zadaným jménem, zde ještě nevím, zda
		 * se jedná o jednorozměrné pole.
		 */
		final Field field = getFieldFromInstance(objInstance, arrayName);

		if (field == null)
			return txtFailure + ": " + txtVariableNotFoundInReferenceToInstanceOfClass;
		
		
		/*
		 * zde otestuji, zda se jedná o pole.
		 */
		if (!field.getType().isArray())
			return txtFailure + ": " + txtEnteredVariable_1 + " '" + field.getName() + "' " + txtEnteredVariable_2;
		
		
		/*
		 * Zde si ještě musím zjistit počet dimenzí, protože předchoí testování v
		 * případě, že je položka field pole vždy vrátí true, i v případě, že se jedná o
		 * například 4 - rozměrné pole apod.
		 */
		final int countOfDims = getCountOfDimensionsOfArray(field.getType());

		if (countOfDims != 2)
			return txtFailure + ": " + txtVariable + " '" + field.getName() + "' " + txtIsNotTwoDimArray;

		
		
		/*
		 * Zde si získám hodnotu v zadané proměnné - zadané pole.
		 */
		final Object objArrayValue = ReflectionHelper.getValueFromField(field, objInstance);

		if (objArrayValue == null)
			// Nemusí být viditelné apod., ale to se již vypsalo:
			return txtFailure;

		

		/*
		 * Zkusím inkrementovat / dekrementovat příslušnou hodnotu a výsledek o této
		 * operaci si uložím do následující proměnné.
		 */
		final ResultArrayInfo resultInfo = incrementDecrementValueAtIndexInTwoDimArray(objArrayValue, index_1, index_2,
				increment);
		
		
		/*
		 * Zde otestuji, zda byla hodnota nastavena v poli nebo ne, pokud ne, tak
		 * zjistím, zda se je to kůvli nenalezeným indexům, tj., že se prošlo celé pole,
		 * ale nebyly nalezeny indexy, takže je uživatel zadal chybně, nebo se
		 * nepodařilo inkrementovat / dekrementovat hodnotu, takže je buď null, nebo se
		 * nejedná o číselnou hodnotu.
		 */
		if (!resultInfo.isSet()) {
			if (resultInfo.isFoundIndeces()) {
				if (increment)
					return txtFailure + ": " + txtFailedToIncrementValueAtIndexInTwoDimArrayInInstance;
				return txtFailure + ": " + txtFailedToDecrementValueAtIndexInTwoDimArrayInInstance;
			}
			return txtFailure + ": " + txtValueWasNotFoundAtIndexInTwoDimarrayInInstance
					+ OutputEditor.NEW_LINE_TWO_GAPS + txtIndex_1Text + ": '" + index_1 + "', " + txtIndex_2Text + ": '"
					+ index_2 + "'.";
		}

		
		
		/*
		 * V této části se podařilo hodnotu inkremetnovat / dekrementovat, tak oznámí
		 * uživateli úspěšné provedení operace.
		 */
		return txtSuccess;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro inkrementování / dekrementování hodnoty ve
	 * dvourozměrném poli objArray na zadaném indexu v instanci nějaké třídy.
	 * 
	 * @param objArray
	 *            - dvourozměrné pole, ve kterém se má inkrementovat položka na
	 *            zadaném indexu.
	 * 
	 * @param index_1
	 *            - index "prvního" pole.
	 * 
	 * @param index_2
	 *            - index "druhého / vnitřního" pole.
	 * 
	 * @param increment
	 *            - logická hodnota o tomm, zda se má nalezená hodnota inkremetnovat
	 *            (true), nebo dekrementovat (false).
	 * 
	 * @return objekt, který bude obsahovat informace o tom, zda se povedla
	 *         inkrementace nebo dekrementace a pokud ne, tak z jakého důvodu se to
	 *         nepovedlo.
	 */
	private ResultArrayInfo incrementDecrementValueAtIndexInTwoDimArray(final Object objArray, final int index_1,
			final int index_2, final boolean increment) {
		
		/*
		 * Zjistím si velikost pole:
		 */
		final int size = Array.getLength(objArray);

		/*
		 * Projdu pole, v každé iteraci si získám příslušnou položku na daném indexu,
		 * otestji, zda je to pole (vždy by mělo být), pak projdu i to druhé pole a při
		 * každé položce otestuji, zda se jedná o požadovaný index a pokud ano, zkusím
		 * příslušnou hodnotu inkrementovat / dekrementovat. Dle toho, zda se to poveden
		 * nebo ne tu hodnotu nataví do pole na požadovaný index a vrátím ohledně toho
		 * nastavení nebo chybě informace a vypíše se příslušné oznámení apod.
		 */
		for (int i = 0; i < size; i++) {
			/*
			 * Hodnota na prvním indexu, vždy by se mělo jednat o "druhé" pole.
			 */
			final Object value = Array.get(objArray, i);

			// Projdu i to druhé pole:
			if (value != null && value.getClass().isArray()) {
				/*
				 * Zjistím si jeho velikost.
				 */
				final int size2 = Array.getLength(value);

				// Projdu tro druhé pole:
				for (int q = 0; q < size2; q++) {
					// Otestuji, zda jsem našel požadované indexy:
					if (i == index_1 && q == index_2) {
						/*
						 * Zde se shodují indexy, tak si získám hodnotu na požadovaném indexu:
						 */
						final Object objValue = Array.get(value, q);

						// V případě null hodnota nemá smysl inkrementovat / dekrementovat:
						if (objValue != null) {
							/*
							 * Do této proměnné si uložím hodnotu, která se měla inkrementovat /
							 * dekrementovat, pokud bude null, tak se nepodařilo nebo nemohla inkremetovat /
							 * dekrementovat.
							 */
							final Object objFinalValue = getIncrementDecrementValue(objValue, increment);

							if (objFinalValue != null) {
								/*
								 * Zde se úspěšně inkrementovala / dekrementovala příslušná hodnota, tak ji
								 * natavím do pole:
								 */
								Array.set(value, q, objFinalValue);

                                /*
                                 * Aktualizace hodnot, které jsou zobrazeny v instanci / buňce v diagramu instancí,
                                 * to je potřeba, aby se překlreslili hodnoty v instanci (pokud jsou v ní zobrazené),
                                 * jinak by se neprojevily výše provedené změny v proměnné.
                                 */
                                instanceDiagram.updateInstanceCellValues();

                                /*
                                 * Zde je třeba aktualizovat okno pro automatické doplňování / dokončování příkazů v
                                 * editoru příkazů, jinak by se v tomto okně neprojevily výše nastavené změny.
                                 */
                                updateValueCompletionWindow();

								/*
								 * Zde vrátím výsledek, v tomto případě je důležitý pouze první parametr - true,
								 * že se v pořádku hodnota nastavila.
								 */
								return new ResultArrayInfo(true, true);
							}

							/*
							 * Zde se nepodařilo hodnotu inkrementovat / dekremetovat, tak bude první
							 * hodnota false a druhá hodnota true, že se našl požadované indexy.
							 */
							return new ResultArrayInfo(false, true);
						}
						/*
						 * Zde je hodnota null, tak nemá smysl inkrementovat, dekrementovat, tak otom
						 * vrátím informace, první false je, že se nepodařilo hodnotu inkrementovat /
						 * dekrementovat a druhá, je že se našli indexy, takže chyba bude v konrktétní
						 * hodnotě.
						 */
						return new ResultArrayInfo(false, true);
					}
				}
			}
		}

		/*
		 * zde vrátím informace o tom, že se nepodařilo najít zadané indexy v příslušném
		 * poli.
		 */
		return new ResultArrayInfo(false, false);
	}







    /**
     * Metoda, která aktualizuje hodnoty v okně pro automatické doplňování hodnot / kódu do editoru příkazů.
     * <p>
     * (Spustí vlákno, které si načte hodnoty a vloží jej do okna s doplňování hodnot do editoru příkazů.)
     */
    private static void updateValueCompletionWindow() {
        App.getCommandEditor().launchThreadForUpdateValueCompletion();
    }







	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	



	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			txtSuccess = properties.getProperty("Mo_Txt_Success", Constants.MO_TXT_SUCCESS);
			txtFailure = properties.getProperty("Mo_Txt_Failure", Constants.MO_TXT_FAILURE); 
			txtInstancesNotFound = properties.getProperty("Mo_Txt_InstanceNotFound", Constants.MO_TXT_INSTANCE_NOT_FOUND);
			txtMethodNotFound = properties.getProperty("Mo_Txt_MethodNotFound", Constants.MO_TXT_METHOD_NOT_FOUND);
			txtBadNameOfMethodOrParameter = properties.getProperty("Mo_Txt_BadNameOfMethodOrParameter", Constants.MO_TXT_BAD_NAME_OF_METHOD_OR_PARAMETER);
			txtReferenceNameNotFound = properties.getProperty("Mo_Txt_ReferenceNameNotFound", Constants.MO_TXT_REFERENCE_NAME_NOT_FOUND);
			txtFailedRetypeValue = properties.getProperty("Mo_Txt_FailedRetypeValue", Constants.MO_TXT_FAILED_RETYPE_VALUE);
			txtMissingVariableInInstaceOfClass = properties.getProperty("Mo_Txt_MissingVariableInInstanceOfClass", Constants.MO_TXT_MISSING_VARIABLE_IN_INSTANCE_OF_CLASS);
			txtErrorInVariableNameOfVisibilityVariable = properties.getProperty("Mo_Txt_ErrorInVariableNameOfVisibilityVariable", Constants.MO_TXT_ERROR_IN_VARIABLE_NAME_OF_VISIBILITY_VARIABLE);			
			
			txtInstanceOfClassNotFound = properties.getProperty("Mo_Txt_InstanceOfClassNotFound", Constants.MO_TXT_INSTANCE_OF_CLASS_NOT_FOUND);
			txtAtLeastOneInstanceOfClassNotFound = properties.getProperty("Mo_Txt_AtLeastOneInstanceOfClassNotFoune", Constants.MO_TXT_AT_LEAST_ONE_INSTANCE_OF_CLASS_NOT_FOUND);
			txtDataTypesAreNotEqual = properties.getProperty("Mo_Txt_DataTypesAreNotEqual", Constants.MO_TXT_DATA_TYPES_ARE_NOT_EQUAL);
			txtErrorMethodOrParametersOrModifier = properties.getProperty("Mo_Txt_ErrorMethodOrParametersOrModifiers", Constants.MO_TXT_ERROR_METHOD_OR_PARAMETERS_OR_MODIFIER);						
			
			txtVariableNotFoundInReferenceToInstanceOfClass = properties.getProperty("Mo_Txt_VariableNotFoundInReferenceToInstanceOfClass", Constants.MO_TXT_VARIABLE_NOT_FOUND_IN_REFERENCE_TO_INSTANCE_OF_CLASS);
			txtFailedToFillVariable = properties.getProperty("Mo_Txt_FailedToFillVariable", Constants.MO_TXT_FAILED_TO_FILL_VARIABLE);
			txtPossibleErrorsInFillVariable = properties.getProperty("Mo_Txt_PossibleErrorsInFillVariable", Constants.MO_TXT_POSSIBLE_ERRORS_IN_FILL_VARIABLE);
			txtErrorsImmutableVisibilityPublic = properties.getProperty("Mo_Txt_ErrorsImmutableVisibilityPublic", Constants.MO_TXT_ERRORS_IMMUTABLE_VISIBILITY_PUBLIC);
			txtVariableName = properties.getProperty("Mo_Txt_VariableName", Constants.MO_TXT_VARIABLE_NAME);
			txtAlreadyExist = properties.getProperty("Mo_Txt_AlreadyExist", Constants.MO_TXT_ALREADY_EXIST);
			txtSrcDirNotFound = properties.getProperty("Mo_Txt_SrcDirNotFound", Constants.MO_TXT_SRC_DIR_NOT_FOUND);
			txtClassNotFound = properties.getProperty("Mo_Txt_ClassNotFound", Constants.MO_TXT_CLASS_NOT_FOUND);
			txtFiledToLoadClassFile = properties.getProperty("Mo_Txt_FailedToLoadClassFile", Constants.MO_TXT_FILED_TO_LOAD_CLASS_FILE);
			txtDataTypesOrConstructorNotFound = properties.getProperty("Mo_Txt_DataTypesOrConstructorNotFound", Constants.MO_TXT_DATA_TYPES_OR_CONSTRUCTOR_NOT_FOUND);
			
			// Texty pro zavolání statické metody:
			txtErrorWhileCallingStaticMethod_1 =  properties.getProperty("Mo_Txt_ErrorWhileCallingStaticMethod_1", Constants.TXT_ERROR_WHILE_CALLING_STATIC_METHOD_1);
			txtErrorWhileCallingStaticMethod_2 = properties.getProperty("Mo_Txt_ErrorWhileCallingStaticMethod_2", Constants.TXT_ERROR_WHILE_CALLING_STATIC_METHOD_2);
			
			// zbylé texty:
			txtOverViewOfCommands = properties.getProperty("Mo_Txt_OverViewOfCommands", Constants.MO_TXT_OVER_VIEW_OF_COMMANDS);			
			txtOverviewOfCommandsEditorVariables = properties.getProperty("Mo_Txt_OverviewOfCommandsEditorVariables", Constants.MO_TXT_OVERVIEW_OF_COMMANDS_EDITOR_VARIABLES);			
			txtErrorInIncrementValue = properties.getProperty("Mo_Txt_ErrorInIncrementValue", Constants.MO_TXT_ERROR_IN_INCREMENT_VALUE);
			txtNunberOutOfInterval = properties.getProperty("Mo_Txt_NumberOutOfInterval", Constants.MO_TXT_NUMBER_OUT_OF_INTERVAL);
			txtErrorInDecrementValue = properties.getProperty("Mo_Txt_ErrorInDecrementValue", Constants.MO_TXT_ERROR_IN_DECREMENT_VALUE);
			txtVariableDontContainsNumber = properties.getProperty("Mo_Txt_VariableDontContainsNumber", Constants.MO_TXT_VARIABLE_DONT_CONTAINS_NUMBER);
			txtFailedToCastNumber = properties.getProperty("Mo_Txt_FailedToCastNumber", Constants.MO_TXT_FAILED_TO_CAST_NUMBER);
			txtReferenceDontPointsToInstance = properties.getProperty("Mo_Txt_ReferenceDontPointsToInstance", Constants.MO_TXT_REFERENCE_DONT_POINTS_TO_INSTANCE);
			txtVariableIsNotPublicOrIsFinal = properties.getProperty("Mo_Txt_VariableIsNotPublicOrIsFinal", Constants.MO_TXT_VARIABLE_IS_NOT_PUBLIC_OR_IS_FINAL);
			txtReferenceDontPointsToInstanceOfClass = properties.getProperty("Mo_Txt_ReferenceDontPointsToInstanceOfClass", Constants.MO_TXT_REFERENCE_DOUNT_POINTS_TO_INSTANCE_OF_CLASS);
			txtReference = properties.getProperty("Mo_Txt_Reference", Constants.MO_TXT_REFERENCE);
			txtVariable = properties.getProperty("Mo_Txt_Variable", Constants.MO_TXT_VARIABLE);
			txtError = properties.getProperty("Mo_Txt_Error", Constants.MO_TXT_ERROR);
			txtMethodWillNotBeInclude = properties.getProperty("Mo_Txt_MethodWillNotBeInclude", Constants.MO_TXT_METHOD_WILL_NOT_BE_INCLUDE);
			txtMethod = properties.getProperty("Mo_Txt_Method", Constants.MO_TXT_METHOD);
			txtMethodHasNotBennCalled = properties.getProperty("Mo_Txt_MethodHasNotBeenCalled", Constants.MO_TXT_METHOD_HAS_NOT_BEEN_CALLED);
			txtReferenceDoesNotPointToInstanceOfClass = properties.getProperty("Mo_Txt_ReferenceDoesNotPointsToInstanceOfClass", Constants.MO_TXT_REFERENCE_DOES_NOT_POINTS_TO_INSTANCE_OF_CLASS);
			txtValueWillNotBeCounted = properties.getProperty("Mo_Txt_ValueWillNotBeCounted", Constants.MO_TXT_VALUE_WILL_NOT_BE_COUNTED);
			txtValueOfVariableWillNotBeCounted = properties.getProperty("Mo_Txt_ValueOfVariableWillNotBeCounted", Constants.MO_TXT_VALUE_OF_VARIABLE_WILL_NOT_BE_COUNTED);
			txtDataTypeOfField = properties.getProperty("Mo_Txt_DataTypeOfField", Constants.MO_TXT_DATA_TYPE_OF_FIELD);
			txtDataTypeIsDifferent = properties.getProperty("Mo_Txt_DataTypeIsDifferent", Constants.MO_TXT_DATA_TYPE_IS_DIFFERENT);
			txtInstance = properties.getProperty("Mo_Txt_Instance", Constants.MO_TXT_INSTANCE);
			txtErrorWhileConvertingValues = properties.getProperty("Mo_Txt_ErrorWhileConvertingValues", Constants.MO_TXT_ERROR_WHILE_CONVERTING_VALUES);
			txtToType = properties.getProperty("Mo_Txt_ToType", Constants.MO_TXT_TO_TYPE);
			txtVariableNameMustNotBeKeyWord = properties.getProperty("Mo_Txt_VariableNameMustBeKeyWork", Constants.MO_TXT_VARIABLE_NAME_MUST_BE_KEY_WORD);
			txtValueIsFinalAndCannotBeChanged = properties.getProperty("Mo_Txt_ValueIsFinalAndCannotBeChanged", Constants.MO_TXT_VALUE_IS_FINAL_AND_CANNOT_BE_CHANGED);
			txtErrorInClassLoaderOrAccessToField = properties.getProperty("Mo_Txt_ErrorInClassLoaderOrAccessToField", Constants.MO_TXT_ERROR_IN_CLASS_LOADER_OR_ACCESS_TO_FIELD);
			txtDoNotFound = properties.getProperty("Mo_Txt_DoNotFound", Constants.MO_TXT_DO_NOT_FOUND);
			txtFailedToCastValue = properties.getProperty("Mo_Txt_FailedToCastValue", Constants.MO_TXT_FAILED_TO_CAST_VALUE);
			txtToDataType = properties.getProperty("Mo_Txt_ToDataType", Constants.MO_TXT_TO_DATA_TYPE);
			txtErrorWhileParsingValue = properties.getProperty("Mo_Txt_ErrorWhileParsingValue", Constants.MO_TXT_ERROR_WHILE_PARSING_VALUE);
			txtProbablyIsAbstractClass = properties.getProperty("Mo_Txt_ProbablyIsAbstractClass", Constants.MO_TXT_PROBABLY_IS_ABSTRACT_CLASS);
			txtNotFoundConstructorToCastingDataType = properties.getProperty("Mo_Txt_NotFoundConstructorToCastingDataType", Constants.MO_TXT_NOT_FOUND_CONSTRUCTOR_TO_CASTING_DATA_TYPE);
			txtPossibleParametersCountIsDifferent = properties.getProperty("Mo_Txt_PossibleParametersCountIsDifferent", Constants.MO_TXT_POSSIBLE_PARAMETERS_COUNT_IS_DIFFERENT);
			txtExceptionWhileCallingConstructorToCasting = properties.getProperty("Mo_Txt_ExceptionWhileCallingConstructorToCasting", Constants.MO_TXT_EXCEPTION_WHILE_CALLING_CONSTRUCTOR_TO_CASTING);
			txtVariableFailedToLoad = properties.getProperty("Mo_Txt_VariableFailedToLoad", Constants.MO_TXT_VARIABLE_FAILED_TO_LOAD);
			txtVariableNotFoundForInsertValue = properties.getProperty("Mo_Txt_VariableNotFoundForInsertValue", Constants.MO_TXT_VARIABLE_NOT_FOUND_FOR_INSERT_VALUE);
			txtVariableNotFound = properties.getProperty("Mo_Txt_VariableNotFound", Constants.MO_TXT_VARIABLE_NOT_FOUND);
			txtValue = properties.getProperty("Mo_Txt_Value", Constants.MO_TXT_VALUE);
			txtVariableIsFinalIsNeedToFillInCreation = properties.getProperty("Mo_Txt_VariableIsFinalIsNeedToFillInCreation", Constants.MO_TXT_VARIABLE_IS_FINAL_IS_NEED_TO_FILL_IN_CREATION);
			txtMustNotBeFinalAndNull = properties.getProperty("Mo_Txt_MustNotBeFinalAndNull", Constants.MO_TXT_MUST_NOT_BE_FINAL_AND_NULL);
			txtVariableNameIsNeedToChangeIsAlreadyTaken = properties.getProperty("Mo_Txt_VariableNameIsNeedToChangeIsAlreadyTaken", Constants.MO_TXT_VARIABLE_NAME_IS_NEED_TO_CHANGE_IS_ALREADY_TAKEN);
			txtVariableNameNotRecognized = properties.getProperty("Mo_Txt_VariableNameNotRecognized", Constants.MO_TXT_VARIABLE_NAME_NOT_RECOGNIZED);
			txtDataTypeNotFoundOnlyPrimitiveDataTypes = properties.getProperty("Mo_Txt_DataTypeNotFoundOnlyPrimitiveDataType", Constants.MO_TXT_DATA_TYPE_NOT_FOUND_ONLY_PRIMITIVE_DATA_TYPE);
			txtDataType = properties.getProperty("Mo_Txt_DataType", Constants.MO_TXT_DATA_TYPE);
			txtFailedToCreateTemporaryVariable = properties.getProperty("Mo_Txt_FailedToCreateTemporaryVariable", Constants.MO_TXT_FAILED_TO_CREATE_TEMPORARY_VARIABLE);
			txtData = properties.getProperty("Mo_Txt_Data", Constants.MO_TXT_DATA);
			txtValue2 = properties.getProperty("Mo_Txt_Value_2", Constants.MO_TXT_VALUE_2);
			txtMaybeError = properties.getProperty("Mo_Txt_MayBeError", Constants.MO_TXT_MAY_BE_ERROR);
			txtDataTypeDoNotSupported = properties.getProperty("Mo_Txt_DataTypeDoNotSupported", Constants.MO_TXT_DATA_TYPE_DO_NOT_SUPPORTED);
			txtVariableIsNullCannotBeIncrementOrDecrement = properties.getProperty("Mo_Txt_VariableIsNullCannotBeIncrementOrDecrement", Constants.MO_TXT_VARIABLE_IS_NULL_CANNOT_BE_INCREMENT_OR_DECREMENT);
			txtVariableIsNull = properties.getProperty("Mo_Txt_VariableIsNull", Constants.MO_TXT_VARIABLE_IS_NULL);
			txtValueWasNotRecognized = properties.getProperty("Mo_Txt_ValueWasNotRecognized", Constants.MO_TXT_VALUE_WAS_NOT_RECOGNIZED);
			txtFailedToCreateVariable = properties.getProperty("Mo_Txt_FailedToCreateVariable", Constants.MO_TXT_FAILED_TO_CREATE_VARIABLE);
			txtDoNotRetrieveDataTypeOfVariable = properties.getProperty("Mo_Txt_DoNotRetrieveDataTypeOfVariable", Constants.MO_TXT_DO_NOT_RETRIEVE_DATA_TYPE_OF_VARIABLE);
			txtFailedToGetDataAboutVariableWhileDeclaration = properties.getProperty("Mo_Txt_FailedToGetDataAboutVariableWhileDeclaration", Constants.MO_TXT_FAILED_TO_GET_DATA_ABOUT_VARIABLE_WHILE_DECLARATION);
			txtReferenceDontPointToInstanceOfList =properties.getProperty("Mo_Txt_ReferenceDontPointToInstanceOfList", Constants.MO_TXT_REFERENCE_DONT_POINT_TO_INSTANCE_OF_LIST);
			txtValueAtIndexIsNull = properties.getProperty("Mo_Txt_ValueAtIndexIsNull", Constants.MO_TXT_VALUE_AT_INDEX_IS_NULL);
			txtConditionsForIndexOfList = properties.getProperty("Mo_Txt_ConditionsForIndexOfList", Constants.MO_TXT_CONDITIONS_FOR_INDEX_OF_LIST);
			txtSpecifiedIndexOfList = properties.getProperty("Mo_Txt_SpecifiedIndexOfList", Constants.MO_TXT_SPECIFIED_INDEX_OF_LIST);
			txtInstanceOfListWasNotFoundUnderReference = properties.getProperty("Mo_Txt_InstanceOfListWasNotFoundUnderReference", Constants.MO_TXT_INSTANCE_OF_LIST_WAS_NOT_FOUND_UNDER_REFERENCE);
			txtSizeOfTheList = properties.getProperty("Mo_Txt_SizeOfTheList", Constants.MO_TXT_SIZE_OF_THE_LIST);
			txtDataTypeForListNotFound = properties.getProperty("Mo_Txt_DataTypeForListNotFound", Constants.MO_TXT_DATA_TYPE_FOR_LIST_NOT_FOUND);
			txtDataTypeForArrayAreNotIdenticaly = properties.getProperty("Mo_Txt_DataTypeForArrayAreNotIdenticaly", Constants.MO_TXT_DATA_TYPE_FOR_ARRAY_ARE_NOT_IDENTICALY);
			txtDataTypeForArrayNotFound = properties.getProperty("Mo_Txt_DataTypeForArrayNotFound", Constants.MO_TXT_DATA_TYPE_FOR_ARRAY_NOT_FOUND);
			txtInstanceOfArrayWasNotFoundUnderReference = properties.getProperty("Mo_Txt_InstanceOfArrayWasNotFoundUnderReference", Constants.MO_TXT_INSTANCE_OF_ARRAY_WAS_NOT_FOUND_UNDER_REFERENCE);
			txtArrayIsFinalDontChangeValues =properties.getProperty("Mo_Txt_ArrayIsFinalDontChangeValues", Constants.MO_TXT_ARRAY_IS_FINAL_DONT_CHANGE_VALUES);
			txtArray = properties.getProperty("Mo_Txt_Array", Constants.MO_TXT_ARRAY);
			txtIndexOutOfRange = properties.getProperty("Mo_Txt_IndexOutOfRange", Constants.MO_TXT_INDEX_OUT_OF_RANGE);
			txtConditionForIndexInArray = properties.getProperty("Mo_Txt_ConditionalForIndexInArray", Constants.MO_TXT_CONDITIONAL_FOR_INDEX_IN_ARRAY);
			txtIndex = properties.getProperty("Mo_Txt_Index", Constants.MO_TXT_INDEX);
			txtSizeOfArray = properties.getProperty("Mo_Txt_SizeOfArray", Constants.MO_TXT_SIZE_OF_ARRAY);
			txtName = properties.getProperty("Mo_Txt_Name", Constants.MO_TXT_NAME);
			txtDoesNotIncrementDecrement = properties.getProperty("Mo_Txt_DoesNotIncrementDecrement", Constants.MO_TXT_DOES_NOT_INCREMENT_DECREMET);
			txtIndexOfListIsOutOfRange = properties.getProperty("Mo_Txt_IndexOfListIsOutOfRange", Constants.MO_TXT_INDEX_OF_LIST_IS_OUT_OF_RANGE);
			txtVariableIsFinalDontChangeValue = properties.getProperty("Mo_Txt_VariableIsFinalDontChangeValue", Constants.MO_TXT_VARIABLE_IS_FINAL_DONT_CHANGE_VALUE);
			txtInstanceIsNotCorrectForParameter = properties.getProperty("Mo_Txt_InstanceIsNotCorrectForParameter", Constants.MO_TXT_INSTANCE_IS_NOT_CORRECT_FOR_PARAMETER);
			txtArraySizeIsOutOfInteger =properties.getProperty("Mo_Txt_ArraySizeIsOutOfInteger", Constants.MO_TXT_ARRAY_SIZE_IS_OUT_OF_INTEGER);
			txtValueInArrayInIndexIsNull = properties.getProperty("Mo_Txt_ValueInArrayInIndexIsNull", Constants.MO_TXT_VALUE_IN_ARRAY_IN_INDEX_IS_NULL);
			txtValueAtIndexAtArrayCantIncreDecre = properties.getProperty("Mo_Txt_ValueAtIndexAtArrayCantIncrementDecrement", Constants.MO_TXT_VALUE_AT_INDEX_AT_ARRAY_CANT_INCRE_DECRE);
			txtFailedToCalculateMathTerm_1 = properties.getProperty("Mo_Txt_FailedToCalculateMathTerm_1", Constants.MO_TXT_FAILED_TO_CALCULATE_MATH_TERM_1);
			txtFailedToCalculateMathTerm_2 = properties.getProperty("Mo_Txt_FailedToCalculateMathTerm_2", Constants.MO_TXT_FAILED_TO_CALCULATE_MATH_TERM_2);

			
			// Matematické operace:
			txtMathCalculationTitle = properties.getProperty("Mo_Wc_Txt_MathCalculationTitle", Constants.MO_WC_TXT_MATH_CALCULATION_TITLE);
			txtOperandsForMathCalculation = properties.getProperty("Mo_Wc_Txt_OperandsForMathCalculation", Constants.MO_WC_TXT_OPERANDS_FOR_MATH_CALCULATION);
			// Logické operaátory:
			txtLogicalOperationsInfo = properties.getProperty("Mo_Wc_Txt_LogicalOperationsInfo", Constants.MO_WC_TXT_LOGICAL_OPERATIONS_INFO);
			// Konstanty:
			txtConstants = properties.getProperty("Mo_Wc_Txt_Constants", Constants.MO_WC_TXT_CONSTANTS);
			// Podporované funkce:
			txtSupportedFunctionsTitle = properties.getProperty("Mo_Wc_Txt_SupportedFunctionsTitle", Constants.MO_WC_TXT_SUPPORTED_FUNCTIONS_TITLE);
			txtNamesOfFunctionsInfo = properties.getProperty("Mo_Wc_Txt_NamesOfFunctionsInfo", Constants.MO_WC_TXT_NAMES_OF_FUNCTIONS_INFO);
			// Příkaldy:
			txtWithoutSemicolonAtTheEndsOfExpression = properties.getProperty("Mo_Wc_Txt_WithoutSemicolonAtEndsOfExpression", Constants.MO_WC_TXT_WITHOUT_SEMICOLON_AT_ENDS_OF_EXPRESSION);
			txtIncludeVariablesFromCommandEditorInfo = properties.getProperty("Mo_Wc_Txt_IncludeVariablesFromCommandsEditorInfo", Constants.MO_WC_TXT_INCLUDE_VARIABLES_FROM_COMMANDS_EDITOR_INFO);		
			txtVarNamesCantConstainNumbers = properties.getProperty("Mo_Wc_Txt_VarNamesCantContainsNumbers", Constants.MO_WC_TXT_VAR_NAMES_CANT_CONTAINS_NUMBERS);
			
			txtDeclaredAndFulfilledVars = properties.getProperty("Mo_Wc_Txt_DeclaredAndFulfilledVars", Constants.MO_WC_TXT_DECLARED_AND_FULFILLED_VARS);
			txtDeclaredAndFulfilledVar = properties.getProperty("Mo_Wc_Txt_DeclaredAndFulfilledVar", Constants.MO_WC_TXT_DECLARED_AND_FULFILLED_VAR);

			
			// Matematické operace:
			txtUnaryPlus = properties.getProperty("Mo_Wc_Txt_UnaryPlus", Constants.MO_WC_TXT_UNARY_PLUS);
			txtUnaryMinus = properties.getProperty("Mo_Wc_Txt_UnaryMinus", Constants.MO_WC_TXT_UNARY_MINUS);
			txtMultiplicationOperator = properties.getProperty("Mo_Wc_Txt_MultiplicationOperator", Constants.MO_WC_TXT_MULTIPLICATION_OPERATOR);
			txtDivisionOperator = properties.getProperty("Mo_Wc_Txt_DivisionOperator", Constants.MO_WC_TXT_DIVISION_OPERATOR); 
			txtModuloOperator = properties.getProperty("Mo_Wc_Txt_ModuloOperator", Constants.MO_WC_TXT_MODULO_OPERATOR);
			txtPowerOperator = properties.getProperty("Mo_Wc_Txt_PoweredOperator", Constants.MO_WC_TXT_POWERED_OPERATOR); 


			// Logické operátory:
			txtEquals = properties.getProperty("Mo_Wc_Txt_Equals", Constants.MO_WC_TXT_EQUALS); 
			txtNotEquals = properties.getProperty("Mo_Wc_Txt_NotEquals", Constants.MO_WC_TXT_NOT_EQUALS); 
			txtLessThan = properties.getProperty("Mo_Wc_Txt_LessThan", Constants.MO_WC_TXT_LESS_THAN); 
			txtLessThanOrEqualTo = properties.getProperty("Mo_Wc_Txt_LessThanOrEqualTo", Constants.MO_WC_TXT_LESS_THAN_OR_EQUAL_TO); 
			txtGreaterThan = properties.getProperty("Mo_Wc_Txt_GreaterThan", Constants.MO_WC_TXT_GREATER_THAN);
			txtGreaterThanOrEqualTo = properties.getProperty("Mo_Wc_Txt_GreaterThanOrEqualTo", Constants.MO_WC_TXT_GREATER_THAN_OR_EQUAL_TO);  
			txtBooleanAnd = properties.getProperty("Mo_Wc_Txt_BooleanAnd", Constants.MO_WC_TXT_BOOLEAN_AND); 
			txtBooleanOr = properties.getProperty("Mo_Wc_Txt_BooleanOr", Constants.MO_WC_TXT_BOOLEAN_OR); 

			// Konstanty:
			txtValueOfE = properties.getProperty("Mo_Wc_Txt_ValueOfE", Constants.MO_WC_TXT_VALUE_OF_E);
			txtValueOfPi = properties.getProperty("Mo_Wc_Txt_ValueOfPI", Constants.MO_WC_TXT_VALUE_OF_PI); 
			txtValueOne = properties.getProperty("Mo_Wc_Txt_ValueOne", Constants.MO_WC_TXT_VALUE_ONE);
			txtValuezero = properties.getProperty("Mo_Wc_Txt_ValueZero", Constants.MO_WC_TXT_VALUE_ZERO); 

			// podporované funkce:
			txtFceNot = properties.getProperty("Mo_Wc_Txt_FceNot", Constants.MO_WC_TXT_FCE_NOT); 
			txtCondition = properties.getProperty("Mo_Wc_Txt_Condition", Constants.MO_WC_TXT_CONDITION); 
			txtFceRandom = properties.getProperty("Mo_Wc_Txt_FceRandom", Constants.MO_WC_TXT_FCE_RANDOM); 
			txtFceMin = properties.getProperty("Mo_Wc_Txt_FceMin", Constants.MO_WC_TXT_FCE_MIN); 
			txtFceMax = properties.getProperty("Mo_Wc_Txt_FceMax", Constants.MO_WC_TXT_FCE_MAX); 
			txtFceAbs = properties.getProperty("Mo_Wc_Txt_FceAbs", Constants.MO_WC_TXT_FCE_ABS);
			txtFceRound = properties.getProperty("Mo_Wc_Txt_FceRound", Constants.MO_WC_TXT_FCE_ROUND); 
			txtFceFloor = properties.getProperty("Mo_Wc_Txt_FceFloor", Constants.MO_WC_TXT_FCE_FLOOR); 
			txtFceCeiling = properties.getProperty("Mo_Wc_Txt_FceCelling", Constants.MO_WC_TXT_FCE_CELLING); 
			txtFcelog =  properties.getProperty("Mo_Wc_Txt_FceLog", Constants.MO_WC_TXT_FCE_LOG);
			txtFceLog10 = properties.getProperty("Mo_Wc_Txt_FceLog10", Constants.MO_WC_TXT_FCE_LOG_10); 
			txtFceSqrt = properties.getProperty("Mo_Wc_Txt_FceSqrt", Constants.MO_WC_TXT_FCE_SQRT);
			txtFceSin = properties.getProperty("Mo_Wc_Txt_FceSin", Constants.MO_WC_TXT_FCE_SIN); 
			txtFceCos = properties.getProperty("Mo_Wc_Txt_FceCos", Constants.MO_WC_TXT_FCE_COS);
			txtFceTan = properties.getProperty("Mo_Wc_Txt_FceTan", Constants.MO_WC_TXT_FCE_TAN);
			txtFceCot = properties.getProperty("Mo_Wc_Txt_FceCot", Constants.MO_WC_TXT_FCE_COT); 
			txtFceAsin = properties.getProperty("Mo_Wc_Txt_FceAsin", Constants.MO_WC_TXT_FCE_ASIN); 
			txtFceAcos = properties.getProperty("Mo_Wc_Txt_FceAcos", Constants.MO_WC_TXT_FCE_ACOS);
			txtFceAtan = properties.getProperty("Mo_Wc_Txt_FceAtan", Constants.MO_WC_TXT_FCE_ATAN); 
			txtFceAcot = properties.getProperty("Mo_Wc_Txt_FceAcot", Constants.MO_WC_TXT_FCE_ACOT); 
			txtFceATan2 = properties.getProperty("Mo_Wc_Txt_FceAtan2", Constants.MO_WC_TXT_FCE_ATAN2); 
			txtFceASinh = properties.getProperty("Mo_Wc_Txt_FceSinh", Constants.MO_WC_TXT_FCE_SINH);
			txtFceCosh = properties.getProperty("Mo_Wc_Txt_FceCosh", Constants.MO_WC_TXT_FCE_COSH); 
			txtFceTah = properties.getProperty("Mo_Wc_Txt_FceTah", Constants.MO_WC_TXT_FCE_TAH); 
			txtFceCoth = properties.getProperty("Mo_Wc_Txt_FceCoth", Constants.MO_WC_TXT_FCE_COTH); 
			txtFceSec = properties.getProperty("Mo_Wc_Txt_FceSec", Constants.MO_WC_TXT_FCE_SEC); 
			txtFceCsc = properties.getProperty("Mo_Wc_Txt_FceCsc", Constants.MO_WC_TXT_FCE_CSC);
			txtFceSech = properties.getProperty("Mo_Wc_Txt_FceSech", Constants.MO_WC_TXT_FCE_SECH); 
			txtFceCsch = properties.getProperty("Mo_Wc_Txt_FceCsch", Constants.MO_WC_TXT_FCE_CSCH);
			txtFceAsinh = properties.getProperty("Mo_Wc_Txt_FceAsinh", Constants.MO_WC_TXT_FCE_ASINH); 
			txtFceAcosh = properties.getProperty("Mo_Wc_Txt_FceAcosh", Constants.MO_WC_TXT_FCE_ACOSH); 
			txtFceATanh = properties.getProperty("Mo_Wc_Txt_FceTanh", Constants.MO_WC_TXT_FCE_TANH); 
			txtFceRad = properties.getProperty("Mo_Wc_Txt_FceRad", Constants.MO_WC_TXT_FCE_RAD); 
			txtFceDeg = properties.getProperty("Mo_Wc_Txt_FceDeg", Constants.MO_WC_TXT_FCE_DEG);
			
			
			// Nějkaé ukázky pro matematické výrazy,které je možné zadat (podmínky, funkce,
			// jejich využití, ...)
			txtExamplesOfMathExpressions = properties.getProperty("Mo_Wc_Txt_ExamplesOfMathExpressions", Constants.MO_WC_TXT_EXAMPLES_OF_MATH_EXPRESSIONS);

			txtMakeInstance = properties.getProperty("Mo_Wc_Txt_MakeInstance", Constants.MO_WC_TXT_MAKE_INSTANCE);
			txtClassName = properties.getProperty("Mo_Wc_Txt_ClassName", Constants.MO_WC_TXT_CLASS_NAME);
			txtMakeInstanceParameters_1 = properties.getProperty("Mo_Wc_Txt_MakeInstanceParameters_1", Constants.MO_WC_TXT_MAKE_INSTANCE_PARAMETERS_1);
			txtMakeInstanceParameters_2 = properties.getProperty("Mo_Wc_Txt_MakeInstanceParameters_2", Constants.MO_WC_TXT_MAKE_INSTANCE_PARAMETERS_2);
			txtMakeInstanceParameters_3 = properties.getProperty("Mo_Wc_Txt_MakeInstanceParameters_3", Constants.MO_WC_TXT_MAKE_INSTANCE_PARAMETERS_3);
			txtWithParameters = properties.getProperty("Mo_Wc_Txt_WithParameters", Constants.MO_WC_TXT_WITH_PARAMETERS);
			txtCallConstructorInfo_1 = properties.getProperty("Mo_Wc_Txt_Call_Constructor_Info_1", Constants.MO_WC_TXT_CALL_CONSTRUCTOR_INFO_1);
			txtCallConstructorInfo_2 = properties.getProperty("Mo_Wc_Txt_Call_Constructor_Info_2", Constants.MO_WC_TXT_CALL_CONSTRUCTOR_INFO_2);
			txtCallConstructorInfo_3 = properties.getProperty("Mo_Wc_Txt_Call_Constructor_Info_3", Constants.MO_WC_TXT_CALL_CONSTRUCTOR_INFO_3);
			txtCallMethodInfo = properties.getProperty("Mo_Wc_Txt_CallMethodInfo", Constants.MO_WC_TXT_CALL_METHOD_INFO);
			txtCallStaticMethodInfo = properties.getProperty("Mo_Wc_Txt_Call_Static_Method_Info", Constants.MO_WC_TXT_CALL_STATIC_METHOD_INFO);
            txtCallStaticMethodInInnerClassInfo = properties.getProperty("Mo_Wc_Txt_Call_Static_Method_In_Inner_Class_Info", Constants.MO_WC_TXT_CALL_STATIC_METHOD_IN_INNER_CLASS_INFO);
			txtCallMethodInfo_1 = properties.getProperty("Mo_Wc_Txt_CallMethodInfo_1", Constants.MO_WC_TXT_CALL_METHOD_INFO_1);
			txtCallMethodInfo_2 = properties.getProperty("Mo_Wc_Txt_CallMethodInfo_2", Constants.MO_WC_TXT_CALL_METHOD_INFO_2);
			
			// Info ohledně zavolání zděděných metod nad třídou coby potomkem příslušné
			// třídy s danou metodou:
			txtInfoAboutMethodsTitle = properties.getProperty("Mo_Wc_Txt_InfoAboutMethodsTitle", Constants.MO_WC_TXT_INFO_ABOUT_METHODS_TITLE);
			txtInfoAboutCallMethodFromObjectClass = properties.getProperty("Mo_Wc_Txt_InfoAboutCallMethodsFromObjectClass", Constants.MO_WC_TXT_INFO_ABOUT_CALL_METHODS_FROM_OBJECT_CLASS);
			txtCallMethodFromInheritedClassesInfo = properties.getProperty("Mo_Wc_Txt_CallMethodFromInheritedClassInfo", Constants.MO_WC_TXT_CALL_METHOD_FROM_INHERITED_CLASS_INFO);
			txtCallStaticMethodFroomInheritedClassesInfo = properties.getProperty("Mo_Wc_Txt_CallStaticMethodFromInheritedClassesInfo", Constants.MO_WC_TXT_CALL_STATIC_METHOD_FROM_INHERITED_CLASSES_INFO);
			
			txtIncrementationDecrementation = properties.getProperty("Mo_Wc_Txt_IncrementationDecrementation", Constants.MO_WC_TXT_INCREMENTATION_DCREMENTATION);
			txtIncrementationDecrementation_1 = properties.getProperty("Mo_Wc_Txt_IncrementationDecrementation_1", Constants.MO_WC_TXT_INCREMENTATION_DECREMENTATION_1);
			txtVariableIncrementation = properties.getProperty("Mo_Wc_Txt_VariableIncrementation", Constants.MO_WC_TXT_VARIABLE_INCREMENTATION);
			txtVariableDecrementation = properties.getProperty("Mo_Wc_Txt_VariableDecrementation", Constants.MO_WC_TXT_VARIABLE_DECREMENTATION);
			txtIncrementationMyVariable = properties.getProperty("Mo_Wc_Txt_IncrementationMyVariable", Constants.MO_WC_TXT_INCREMENTATION_MY_VARIABLE);
			txtDecrementationMyVariable = properties.getProperty("Mo_Wc_Txt_DecrementationMyVariable", Constants.MO_WC_TXT_DECREMENTATION_MY_VARIABLE);
			txtIncrementationDecrementationConstatnts = properties.getProperty("Mo_Wc_Txt_IncrementationDecrementationConstants", Constants.MO_WC_TXT_INCREMENTATION_DECREMENTATION_CONSTANTS);
			txtIncrementationConstants = properties.getProperty("Mo_Wc_Txt_IncrementationConstants", Constants.MO_WC_TXT_INCREMENTATION_CONSTANTS);
			txtDecrementationConstants = properties.getProperty("Mo_Wc_Txt_DecrementationConstants", Constants.MO_WC_TXT_DECREMENTATION_CONSTANTS);
			txtDeclarationOfVariable = properties.getProperty("Mo_Wc_Txt_DeclarationOfVariable", Constants.MO_WC_TXT_DECLARATION_OF_VARIABLE); 
			txtDeclarationVariableInfo = properties.getProperty("Mo_Wc_Txt_DeclarationVariableInfo", Constants.MO_WC_TXT_DECLARATION_VARIABLE_INFO);
			txtLikeThis = properties.getProperty("Mo_Wc_Txt_LikeThis", Constants.MO_WC_TXT_LIKE_THIS);
			txtDeclarationVariableInfo_2 = properties.getProperty("Mo_Wc_Txt_DeclarationVariableInfo_2", Constants.MO_WC_TXT_DECLARATION_VARIABLE_INFO_2);
			txtSyntaxe = properties.getProperty("Mo_Wc_Txt_Syntaxe", Constants.MO_WC_TXT_SYNTAXE);
			txtForString = properties.getProperty("Mo_Wc_Txt_SyntaxeForString", Constants.MO_WC_TXT_SYNTAXE_FOR_STRING);
			txtSyntaxeForChar = properties.getProperty("Mo_Wc_Txt_SyntaxeForChar", Constants.MO_WC_TXT_SYNTAXE_FOR_CHAR);
			txtSyntaxeForDouble = properties.getProperty("Mo_Wc_Txt_SyntaxeForDouble", Constants.MO_WC_TXT_SYNTAXE_FOR_DOUBLE);
			txtSimilaryDataTypes = properties.getProperty("Mo_Wc_Txt_SimilaryDataTypes", Constants.MO_WC_TXT_SIMILARY_DATA_TYPES);
			txtFullfillmentVariable = properties.getProperty("Mo_Wc_Txt_FullfilmentVariable", Constants.MO_WC_TXT_FULLFILMENT_VARIABLE);
			txtFullfilmentVariableInfo = properties.getProperty("Mo_Wc_Txt_FullfilmentVariableInfo", Constants.MO_WC_TXT_FULLFILMENT_VARIABLE_INFO);
			txtFullfilmentVariableSyntaxeInfo = properties.getProperty("Mo_Wc_Txt_FullfilmentVariableSyntaxeInfo", Constants.MO_WC_TXT_FULLFILMENT_VARIABLE_SYNTAXE_INFO);
			txtFullfilmentVariableText = properties.getProperty("Mo_Wc_Txt_FullfilmentVariableText", Constants.MO_WC_TXT_FULLFILMENT_VARIABLE_TEXT);
			txtFullfilmentMyVariable = properties.getProperty("Mo_Wc_Txt_FullfilmentMyVariable", Constants.MO_WC_TXT_FULLFILMENT_MY_VARIABLE);
			txtFullfilmentReplaceInfo = properties.getProperty("Mo_Wc_Txt_FullfilmentReplaceInfo", Constants.MO_WC_TXT_FULFILMENT_REPLACE_INFO);
			txtFullfilmentReturnValue = properties.getProperty("Mo_Wc_Txt_FullfilmentReturnValue", Constants.MO_WC_TXT_FULLFILMENT_RETURN_VALUE);
			txtFullfilmentByStaticMethod = properties.getProperty("Mo_Wc_Txt_Fullfilment_By_Static_Method", Constants.MO_WC_TXT_FULLFILMENT_BY_STATIC_METHOD);
            txtFullfilmentByStaticMethodInInnerClass = properties.getProperty("Mo_Wc_Txt_Fullfilment_By_Static_Method_In_Inner_Class", Constants.MO_WC_TXT_FULLFILMENT_BY_STATIC_METHOD_IN_INNER_CLASS);
			txtFullfilmentVariableSameRules = properties.getProperty("Mo_Wc_Txt_FullfilmentVariableSameRules", Constants.MO_WC_TXT_FULLFILMENT_VARIABLE_SAME_RULES);
			txtFullfilmentVariableInfo_2 = properties.getProperty("Mo_Wc_Txt_FullfilmentVariableInfo_2", Constants.MO_WC_TXT_FULLFILMENT_VARIABLE_INFO_2);
			txtFullfilmentEtc = properties.getProperty("Mo_Wc_Txt_FullfilmentEtc", Constants.MO_WC_TXT_FULLFILMENT_ETC);
			txtPrintMethodInfo = properties.getProperty("Mo_Wc_Txt_PrintMethodInfo", Constants.MO_WC_TXT_PRINT_METHOD_INFO);
			txtPrintMethodInfo_2 = properties.getProperty("Mo_Wc_Txt_PrintMethodInfo_2", Constants.MO_WC_TXT_PRINT_METHOD_INFO_2);
			txtPrintMethodInfo_3 = properties.getProperty("Mo_Wc_Txt_PrintMethodInfo_3", Constants.MO_WC_TXT_PRINT_METHOD_INFO_3);
			txtPrintMethodInfo_4 = properties.getProperty("Mo_Wc_Txt_PrintMethodInfo_4", Constants.MO_WC_TXT_PRINT_METHOD_INFO_4);
			txtPrintMethodSyntaxeExample = properties.getProperty("Mo_Wc_Txt_PrintMethodSyntaxeExample", Constants.MO_WC_TXT_PRINT_METHOD_SYNTAXE_EXAMPLE);
			txtPrintMethodWriteResult = properties.getProperty("Mo_Wc_Txt_PrintMethodWriteResult", Constants.MO_WC_TXT_PRINT_METHOD_WRITE_RESULT) ;
			txtPrintMethodReturnValue = properties.getProperty("Mo_Wc_Txt_PrintMethodReturnValue", Constants.MO_WC_TXT_PRINT_METHOD_RETURN_VALUE);
			txtDeclarationListInfo = properties.getProperty("Mo_Wc_Txt_DeclarationListInfo", Constants.MO_WC_TXT_DECLARATION_LIST_INFO);
			txtDeclarationListInfo_2 = properties.getProperty("Mo_Wc_Txt_DeclarationListInfo_2", Constants.MO_WC_TXT_DECLARATION_LIST_INFO_2);
			txtDeclarationListInfo_3 = properties.getProperty("Mo_Wc_Txt_DeclarationListInfo_3", Constants.MO_WC_TXT_DECLARATION_LIST_INFO_3);
			txtNote = properties.getProperty("Mo_Wc_Txt_Note", Constants.MO_WC_TXT_NOTE);
			txtDeclarationListInfo_4 = properties.getProperty("Mo_Wc_Txt_DeclarationListInfo_4", Constants.MO_WC_TXT_DECLARATION_LIST_INFO_4);	
			txtDeclarationListInfo_5 = properties.getProperty("Mo_Wc_Txt_DeclarationListInfo_5", Constants.MO_WC_TXT_DECLARATION_LIST_INFO_5);
			txtListMethodInfo_1 = properties.getProperty("Mo_Wc_Txt_ListMethodInfo_1", Constants.MO_WC_TXT_LIST_METHOD_INFO_1);
			txtListMethodInfo_2 =properties.getProperty("Mo_Wc_Txt_ListMethodInfo_2", Constants.MO_WC_TXT_LIST_METHOD_INFO_2);
			txtListMethodInfo_3 = properties.getProperty("Mo_Wc_Txt_ListMethodInfo_3", Constants.MO_WC_TXT_LIST_METHOD_INFO_3);
			txtListMethodInfo_4 = properties.getProperty("Mo_Wc_Txt_ListMethodInfo_4", Constants.MO_WC_TXT_LIST_METHOD_INFO_4);
			txtListMethodInfo_5 = properties.getProperty("Mo_Wc_Txt_ListMethodInfo_5", Constants.MO_WC_TXT_LIST_METHOD_INFO_5);
			txtListMethodInfo_6 = properties.getProperty("Mo_Wc_Txt_ListMethodInfo_6", Constants.MO_WC_TXT_LIST_METHOD_INFO_6);
			txtListMethodInfo_7 =properties.getProperty("Mo_Wc_Txt_ListMethodInfo_7", Constants.MO_WC_TXT_LIST_METHOD_INFO_7);
			txtListMethodInfo_8 = properties.getProperty("Mo_Wc_Txt_ListMethodInfo_8", Constants.MO_WC_TXT_LIST_METHOD_INFO_8);
			txtListMethodInfo_9 = properties.getProperty("Mo_Wc_Txt_ListMethodInfo_9", Constants.MO_WC_TXT_LIST_METHOD_INFO_9);
			txtListMethodInfo_10 = properties.getProperty("Mo_Wc_Txt_ListMethodInfo_10", Constants.MO_WC_TXT_LIST_METHOD_INFO_10);
			txtDeclarationOneDimensionalArray = properties.getProperty("Mo_Wc_Txt_DeclarationOneDimensionalArray", Constants.MO_WC_TXT_DECLARATION_ONE_DIMENSIONAL_ARRAY);
			txtOneDimArrayInfo_1 = properties.getProperty("Mo_Wc_Txt_OneDimArrayInfo_1", Constants.MO_WC_TXT_ONE_DIM_ARRAY_INFO_1);
			txtOneDimArrayInfo_2 = properties.getProperty("Mo_Wc_Txt_OneDimArrayInfo_2", Constants.MO_WC_TXT_ONE_DIM_ARRAY_INFO_2);			
			txtOneDimArrayInfo_3 = properties.getProperty("Mo_Wc_Txt_OneDimArrayInfo_3", Constants.MO_WC_TXT_ONE_DIM_ARRAY_INFO_3);			
			txtDataTypeArrayCanBe = properties.getProperty("Mo_Wc_Txt_DataTypeArrayCanBe", Constants.MO_WC_TXT_DATA_TYPE_ARRAY_CAN_BE);
			txtOperationsWithArray = properties.getProperty("Mo_Wc_Txt_OperationsWithArray", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY);
			txtOperationsWithArray_1 = properties.getProperty("Mo_Wc_Txt_OperationsWithArray_1", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_1);
			txtOperationsWithArray_2 = properties.getProperty("Mo_Wc_Txt_OperationsWithArray_2", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_2);
			txtOperationsWithArray_3 = properties.getProperty("Mo_Wc_Txt_OperationsWithArray_3", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_3);
			txtOperationsWithArray_4 = properties.getProperty("Mo_Wc_Txt_OperationsWithArray_4", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_4);
			txtOperationsWithArray_5 = properties.getProperty("Mo_Wc_Txt_OperationsWithArray_5", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_5);	
			txtOperationsWithArray_7 = properties.getProperty("Mo_Wc_Txt_OperationsWithArray_6", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_6);
			txtOperationsWithArray_8 = properties.getProperty("Mo_Wc_Txt_OperationsWithArray_7", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_7);						
			txtTwoDimArrayTitle = properties.getProperty("Mo_Wc_Txt_TwoDimArrayTitle", Constants.MO_WC_TXT_TWO_DIM_ARRAY_TITLE);
			txtWorkWithTwoDimArrayInfo = properties.getProperty("Mo_Wc_Txt_WorkWithTwoDimArrayInfo", Constants.MO_WC_TXT_WORK_WITH_TWO_DIM_ARRAY_INFO);
			txtCannotCreateTwoDimArray =  properties.getProperty("Mo_Wc_Txt_CannotCreateTwoDimarray", Constants.MO_WC_TXT_CANNOT_CREATE_TWO_DIM_ARRAY);
			txtOperationsWithTwoDimArray =  properties.getProperty("Mo_Wc_Txt_OperationsWithTwoDimArray", Constants.MO_WC_TXT_OPERATIONS_WITH_TWO_DIM_ARRAY);			
			txtFullfilmentArrayInfo_1 = properties.getProperty("Mo_Wc_Txt_FullfilmentArrayInfo_1", Constants.MO_WC_TXT_FULLFILMENT_ARRAY_INFO_1);
			txtFullfilmentArrayInfo_2 = properties.getProperty("Mo_Wc_Txt_FullfilmentArrayInfo_2", Constants.MO_WC_TXT_FULLFILMENT_ARRAY_INFO_2);
			txtFullfilmentArrayInfo_3 = properties.getProperty("Mo_Wc_Txt_FullfilmentArrayInfo_3", Constants.MO_WC_TXT_FULLFILMENT_ARRAY_INFO_3);			
			txtOperationsWithArrayinIntanceTitle = properties.getProperty("Mo_Wc_Txt_OperationsWithArrayInInstanceTitle", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_IN_INSTANCE_TITLE); 
			txtOperationsWithArrayInInstanceInfo =  properties.getProperty("Mo_Wc_Txt_OperationsWithArrayInInstanceInfo", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_IN_INSTANCE_INFO);
			txtOperationsWithArrayInInstanceDifference = properties.getProperty("Mo_Wc_Txt_OperationsWithArrayInInstanceDifference", Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_IN_INSTANCE_DIFFERENCE);			
			txtInfoAboutNullValue = properties.getProperty("Mo_Wc_Txt_InfoAboutNullValue", Constants.MO_WC_TXT_INFO_ABOUT_NULL_VALUE);
			
			
			
			// Info ohledně metod pro restartování a ukončení aplikace:
			txtInfoForMethodsOverApp = properties.getProperty("Mo_Wc_Txt_InfoForMethodsOverApp", Constants.MO_WC_TXT_INFO_FOR_METHODS_OVER_APP);

			// Restartování aplikace:
			txtRestartAppTitle = properties.getProperty("Mo_Wc_Txt_RestartAppTitle", Constants.MO_WC_TXT_RESTART_APP_TITLE);

			// Info k restartu - že se uložídiagram tříd apod.
			txtInfoAboutCloseOpenFilesBeforeRestartApp = properties.getProperty("Mo_Wc_Txt_InfoAboutCloseOpenFilesBeforeRestartApp", Constants.MO_WC_TXT_INFO_ABOUT_CLOSE_OPEN_FILES_BEFORE_RESTART_APP);
			txtInfoAboutLaunchCountdownWhenCommandIsEntered = properties.getProperty("Mo_Wc_Txt_InfoAboutLaunchCountdownWithCommandIsEntered", Constants.MO_WC_TXT_INFO_ABOUT_LAUNCH_COUNTDOWN_WHEN_COMMAND_IS_ENTERED);
			txtInfoAboutSaveClassDiagramBeforeRestartApp = properties.getProperty("Mo_Wc_Txt_InfoAboutSaveClassDiagramBeforeRestartApp", Constants.MO_WC_TXT_INFO_ABOUT_SAVE_CLASS_DIAGRAM_BEFORE_RESTART_APP);
			txtTwoWaysToRestartApp = properties.getProperty("Mo_Wc_Txt_TwoWaysToRestartApp", Constants.MO_WC_TXT_TWO_WAYS_TO_RESTART_APP);
			txtFirstWayToRestartApp_1 = properties.getProperty("Mo_Wc_Txt_FirstWayToRestartApp_1", Constants.MO_WC_TXT_FIRST_WAY_TO_RESTART_APP_1);				
			txtFirstWayToRestartApp_2 = properties.getProperty("Mo_Wc_Txt_FirstWayToRestartApp_2", Constants.MO_WC_TXT_FIRST_WAY_TO_RESTART_APP_2);
			txtSecondWayToRestartApp = properties.getProperty("Mo_Wc_Txt_SecondWayToRestartApp", Constants.MO_WC_TXT_SECOND_WAY_TO_RESTART_APP);


			// Ukončení aplikace
			txtCloseAppTitle = properties.getProperty("Mo_Wc_Txt_CloseAppTitle", Constants.MO_WC_TXT_CLOSE_APP_TITLE);
			txtInfoAboutCloseOpenFilesBeforeCloseApp = properties.getProperty("Mo_Wc_Txt_InfoAboutCloseOpenFilesBeforeCloseApp", Constants.MO_WC_TXT_INFO_ABOUT_CLOSE_OPEN_FILES_BEFORE_CLOSE_APP) ;
			txtInfoAboutLaunchCountdownAfterCommandIsEntered = properties.getProperty("Mo_Wc_Txt_InfoAboutLaunchCountdownAfterCommandIsEntered", Constants.MO_WC_TXT_INFO_ABOUT_LAUNCH_COUNTDOWN_AFTER_COMMAND_IS_ENTERED);
			txtInfoAboutSaveClassDiagramBeforeCloseApp = properties.getProperty("Mo_Wc_Txt_InfoAboutSaveClassDiagramBeforeCloseApp", Constants.MO_WC_TXT_INFO_ABOUT_SAVE_CLASS_DIAGRAM_BEFORE_CLOSE_APP);
			txtThreeWaysToCloseApp = properties.getProperty("Mo_Wc_Txt_ThreeWaysToCloseApp", Constants.MO_WC_TXT_THREE_WAYS_TO_CLOSE_APP);
			txtFirstWayToCloseApp_1 = properties.getProperty("Mo_Wc_Txt_FirstWayToCloseApp_1", Constants.MO_WC_TXT_FIRST_WAY_TO_CLOSE_APP_1);
			txtFirstWayToCloseApp_2 =properties.getProperty("Mo_Wc_Txt_FirstWayToCloseApp_2", Constants.MO_WC_TXT_FIRST_WAY_TO_CLOSE_APP_2) ;
			txtSecondWayToCloseApp = properties.getProperty("Mo_Wc_Txt_SecondWayToCloseApp", Constants.MO_WC_TXT_SECOND_WAY_TO_CLOSE_APP);
			txtThirdWayToCloseApp_1 = properties.getProperty("Mo_Wc_Txt_ThirdayToCloseApp_1", Constants.MO_WC_TXT_THIRD_WAY_TO_CLOSE_APP_1) ;
			txtThirdWayToCloseApp_2 = properties.getProperty("Mo_Wc_Txt_ThirdayToCloseApp_2", Constants.MO_WC_TXT_THIRD_WAY_TO_CLOSE_APP_2);
			txtInfoAboutThirdWayToCloseApp_1 = properties.getProperty("Mo_Wc_Txt_InfoAboutThirdWayToCloseApp_1", Constants.MO_WC_TXT_INFO_ABOUT_THIRD_WAY_TO_CLOSE_APP_1);
			txtInfoAboutThirdWayToCloseApp_2 = properties.getProperty("Mo_Wc_Txt_InfoAboutThirdWayToCloseApp_2", Constants.MO_WC_TXT_INFO_ABOUT_THIRD_WAY_TO_CLOSE_APP_2) ;



			// Zrušení odpočtu při restartování nebo ukončení aplikace:
			txtCancelCountdownTitle = properties.getProperty("Mo_Wc_Txt_CancelCountdownTitle", Constants.MO_WC_TXT_CANCEL_COUNTDOWN_TITLE);
			txtCancelCountdownInfo = properties.getProperty("Mo_Wc_Txt_CancelCountdownInfo", Constants.MO_WC_TXT_CANCEL_COUNTDOWN_INFO);
			
			
			
			// Texty, že nebyl nalezen soubor .class pro zavolaní staticke metody a nebo konstruktoru:
			txtFileDotClass = properties.getProperty("Mo_Wc_Txt_File_Dot_Class", Constants.MO_WC_TXT_FILE_DOT_CLASS); 
			txtFileDotClassWasNotFound = properties.getProperty("Mo_Wc_Txt_File_Dot_Class_Was_Not_Found", Constants.MO_WC_TXT_FILE_DOT_CLASS_WAS_NOT_FOUND);
            txtFailedToFindTheClass = properties.getProperty("Mo_Wc_Txt_Failed_To_Find_The_Class", Constants.MO_WC_TXT_FAILED_TO_FIND_THE_CLASS);
            txtFailedToFindBinDir = properties.getProperty("Mo_Wc_Txt_Failed_To_Find_Bin_Dir", Constants.MO_WC_TXT_FAILED_TO_FIND_BIN_DIR);
            txtFailedToFindInnerClass = properties.getProperty("Mo_Wc_Txt_Failed_To_Find_Inner_Class", Constants.MO_WC_TXT_FAILED_TO_FIND_INNER_CLASS);
            txtInnerClassIsNotStatic = properties.getProperty("Mo_Wc_Txt_Inner_Class_Is_Not_Static", Constants.MO_WC_TXT_INNER_CLASS_IS_NOT_STATIC);
            txtInnerClassIsNotPublicEitherProtected = properties.getProperty("Mo_Wc_Txt_Inner_Class_Is_Not_Public_Either_Protected", Constants.MO_WC_TXT_INNER_CLASS_IS_NOT_PUBLIC_EITHER_PROTECTED);
			txtSpecificMethodWasNotFound = properties.getProperty("Mo_Wc_Txt_Specific_Method_Was_Not_Found", Constants.MO_WC_TXT_SPECIFIC_METHOD_WAS_NOT_FOUND);
			txtPublicConstructorWasNotFound = properties.getProperty("Mo_Wc_Txt_Public_Constructor_Was_Not_Found", Constants.MO_WC_TXT_PUBLIC_CONSTRUCTOR_WAS_NOT_FOUND);
					
			// Text pro zavolání konstruktoru:
			txtConstructorWasNotFound = properties.getProperty("Mo_Wc_Txt_Constructor_Was_Not_Found", Constants.MO_WC_TXT_CONSTRUCTOR_WAS_NOT_FOUND);
			
			// Text do metody printTempVariables();:
			txtNoVariablesWasCreated = properties.getProperty("Mo_Wc_Txt_No_Variable_Was_Created", Constants.MO_WC_TXT_NO_VARIABLE_WAS_CREATED);
			
			
			
			
			// Texty pro chybové hlášky nad operace s jednorozměrným polem v instanci třídy:
			txtCastingToIntegerFailure_1 = properties.getProperty("Mo_Wc_Txt_CastingToIntegerFailure_1", Constants.MO_WC_TXT_CASTING_TO_INTEGER_FAILURE_1);
			txtCastingToIntegerFailure_2 = properties.getProperty("Mo_Wc_Txt_CastingToIntegerFailure_2", Constants.MO_WC_TXT_CASTING_TO_INTEGER_FAILURE_2);
			txtInstanceOfClassInIdNotFOund = properties.getProperty("Mo_Wc_Txt_InstanceOfClassInIdNotFound", Constants.MO_WC_TXT_INSTANCE_OF_CLASS_IN_ID_NOT_FOUND);
			txtEnteredVariable_1 = properties.getProperty("Mo_Wc_Txt_EnteredVariable_1", Constants.MO_WC_TXT_ENTERED_VARIABLE_1);
			txtEnteredVariable_2 = properties.getProperty("Mo_Wc_Txt_EnteredVariable_2", Constants.MO_WC_TXT_ENTERED_VARIABLE_2);
			txtIsNotOneDimArray = properties.getProperty("Mo_Wc_Txt_IsNotOneDimArray", Constants.MO_WC_TXT_IS_NOT_ONE_DIM_ARRAY);
			txtIndexValueAtArrayIsNotInInterval = properties.getProperty("Mo_Wc_Txt_IndexValueAtArrayIsNotInInterval", Constants.MO_WC_TXT_INDEX_VALUE_AT_ARRAY_IS_NOT_IN_INTERVAL);
			txtValueInArrayIsNull = properties.getProperty("Mo_Wc_Txt_ValueInArrayIsNull", Constants.MO_WC_TXT_VALUE_IN_ARRAY_IS_NULL);
			
			
			// Texty pro chybové hlášky nad operacemi s dvourozměrným polem:
			txtIsNotTwoDimArray = properties.getProperty("Mo_Wc_Txt_IsNotTwoDimArray", Constants.MO_WC_TXT_IS_NOT_TWO_DIM_ARRAY);
			txtFailedToSetValueToTwoDimArray = properties.getProperty("Mo_Wc_Txt_FailedToSetValueToTwoDimArray", Constants.MO_WC_TXT_FAILED_TO_SET_VALUE_TO_TWO_DIM_ARRAY);
			txtIndex_1Text = properties.getProperty("Mo_Wc_Txt_Index_1_Text", Constants.MO_WC_TXT_INDEX_1_TEXT);
			txtIndex_2Text = properties.getProperty("Mo_Wc_Txt_Index_2_Text", Constants.MO_WC_TXT_INDEX_2_TEXT);
			txtFailedToGetValueFromTwoDimArray = properties.getProperty("Mo_Wc_Txt_FailedToGetValueFromTwoDimArray", Constants.MO_WC_TXT_FAILED_TO_GET_VALUE_FROM_TWO_DIM_ARRAY);			
			txtFailedToIncrementValueAtIndexInTwoDimArrayInInstance = properties.getProperty("Mo_Wc_Txt_FailedToIncrementValueAtIndexInTwoDimArrayInIntance", Constants.MO_WC_TXT_FAILED_TO_INCREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE); 
			txtFailedToDecrementValueAtIndexInTwoDimArrayInInstance = properties.getProperty("Mo_Wc_Txt_FailedToDecrementValueAtIndexInTwoDimArrayInIntance", Constants.MO_WC_TXT_FAILED_TO_DECREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE);
			txtValueWasNotFoundAtIndexInTwoDimarrayInInstance = properties.getProperty("Mo_Wc_Txt_ValueWasnotFoundAtindexinTwoDimArrayInInstance", Constants.MO_WC_TXT_VALUE_WAS_NOT_FOUND_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE);
			
			
			
			
			
			// Ukončení a restart aplikace:
			
			// Texty pro ukončení odpočtu:
			txtNoCountdownIsRunning = properties.getProperty("Mo_Wc_Txt_NoCountdownIsRunning", Constants.MO_WC_TXT_NO_COUNTDOWN_IS_RUNNING);
			txtCountdownIsStopped = properties.getProperty("Mo_Wc_Txt_CountdownIsStopped", Constants.MO_WC_TXT_COUNTDOWN_IS_STOPPED);

			// Texty pro spuštění timeru pro ukončení aplikace.
			txtCountdownForCloseAppIsRunning = properties.getProperty("Mo_Wc_Txt_CountdownForClosAppIsRunning", Constants.MO_WC_TXT_COUNTDOWN_FOR_CLOSE_APP_IS_RUNNING);
			txtCountdownForCloseAppIsNotPossibleToStart = properties.getProperty("Mo_Wc_Txt_CountdownForCloseAppIsNotPossibleToStart", Constants.MO_WC_TXT_COUNTDOWN_FOR_CLOSE_APP_IS_NOT_POSSIBLE_TO_START);
			
			txtToEndOfAppRemains_1 = properties.getProperty("Mo_Wc_Txt_ToEndOfAppRemains_1", Constants.MO_WC_TXT_TO_END_OF_APP_REMAINS_1);
			txtToEndOfAppRemains_2 = properties.getProperty("Mo_Wc_Txt_ToEndOfAppRemains_2", Constants.MO_WC_TXT_TO_END_OF_APP_REMAINS_2);
			txtToEndOfAppRemains_3 = properties.getProperty("Mo_Wc_Txt_ToEndOfAppRemains_3", Constants.MO_WC_TXT_TO_END_OF_APP_REMAINS_3);
			
			txtSecondsForClose_1 = properties.getProperty("Mo_Wc_Txt_SecondsForClose_1", Constants.MO_WC_TXT_SECONDS_FOR_CLOSE_1);
			txtSecondsForClose_2 = properties.getProperty("Mo_Wc_Txt_SecondsForClose_2", Constants.MO_WC_TXT_SECONDS_FOR_CLOSE_2) ;
			txtSecondsForClose_3 = properties.getProperty("Mo_Wc_Txt_SecondsForClose_3", Constants.MO_WC_TXT_SECONDS_FOR_CLOSE_3);

			// Texty pro spuštění timeru pro restartování aplikace:
			txtCountdownForRestartAppIsRunning = properties.getProperty("Mo_Wc_Txt_CountdownForRestartAppIsRunning", Constants.MO_WC_TXT_COUNTDOWN_FOR_RESTART_APP_IS_RUNNING);
			txtCountdownForRestartAppIsNotPossibleToStart = properties.getProperty("Mo_Wc_Txt_CountdownForRestartAppIsNotPossibleToStart", Constants.MO_WC_TXT_COUNTDOWN_FOR_RESTART_APP_IS_NOT_POSSIBLE_TO_START);
			
			txtToRestartAppRemains_1 = properties.getProperty("Mo_Wc_Txt_ToRestartAppRemains_1", Constants.MO_WC_TXT_TO_RESTART_APP_REMAINS_1);
			txtToRestartAppRemains_2 = properties.getProperty("Mo_Wc_Txt_ToRestartAppRemains_2", Constants.MO_WC_TXT_TO_RESTART_APP_REMAINS_2);
			txtToRestartAppRemains_3 = properties.getProperty("Mo_Wc_Txt_ToRestartAppRemains_3", Constants.MO_WC_TXT_TO_RESTART_APP_REMAINS_3);
			
			txtSecondsForRestart_1 = properties.getProperty("Mo_Wc_Txt_SecondsForRestart_1", Constants.MO_WC_TXT_SECONDS_FOR_RESTART_1);
			txtSecondsForRestart_2 = properties.getProperty("Mo_Wc_Txt_SecondsForRestart_2", Constants.MO_WC_TXT_SECONDS_FOR_RESTART_2);
			txtSecondsForRestart_3 = properties.getProperty("Mo_Wc_Txt_SecondsForRestart_3", Constants.MO_WC_TXT_SECONDS_FOR_RESTART_3);
		}
		
		
		else {
			txtSuccess = Constants.MO_TXT_SUCCESS;
			txtFailure = Constants.MO_TXT_FAILURE; 
			txtInstancesNotFound = Constants.MO_TXT_INSTANCE_NOT_FOUND;
			txtMethodNotFound = Constants.MO_TXT_METHOD_NOT_FOUND;
			txtBadNameOfMethodOrParameter = Constants.MO_TXT_BAD_NAME_OF_METHOD_OR_PARAMETER;
			txtReferenceNameNotFound = Constants.MO_TXT_REFERENCE_NAME_NOT_FOUND;
			txtFailedRetypeValue = Constants.MO_TXT_FAILED_RETYPE_VALUE;
			txtMissingVariableInInstaceOfClass = Constants.MO_TXT_MISSING_VARIABLE_IN_INSTANCE_OF_CLASS;
			txtErrorInVariableNameOfVisibilityVariable = Constants.MO_TXT_ERROR_IN_VARIABLE_NAME_OF_VISIBILITY_VARIABLE;			
			
			txtInstanceOfClassNotFound = Constants.MO_TXT_INSTANCE_OF_CLASS_NOT_FOUND;
			txtAtLeastOneInstanceOfClassNotFound = Constants.MO_TXT_AT_LEAST_ONE_INSTANCE_OF_CLASS_NOT_FOUND;
			txtDataTypesAreNotEqual = Constants.MO_TXT_DATA_TYPES_ARE_NOT_EQUAL;
			txtErrorMethodOrParametersOrModifier = Constants.MO_TXT_ERROR_METHOD_OR_PARAMETERS_OR_MODIFIER;					
		
			txtVariableNotFoundInReferenceToInstanceOfClass = Constants.MO_TXT_VARIABLE_NOT_FOUND_IN_REFERENCE_TO_INSTANCE_OF_CLASS;
			txtFailedToFillVariable = Constants.MO_TXT_FAILED_TO_FILL_VARIABLE;
			txtPossibleErrorsInFillVariable = Constants.MO_TXT_POSSIBLE_ERRORS_IN_FILL_VARIABLE;
			txtErrorsImmutableVisibilityPublic = Constants.MO_TXT_ERRORS_IMMUTABLE_VISIBILITY_PUBLIC;
			txtVariableName = Constants.MO_TXT_VARIABLE_NAME;
			txtAlreadyExist =  Constants.MO_TXT_ALREADY_EXIST;
			txtSrcDirNotFound = Constants.MO_TXT_SRC_DIR_NOT_FOUND;
			txtClassNotFound = Constants.MO_TXT_CLASS_NOT_FOUND;
			txtFiledToLoadClassFile = Constants.MO_TXT_FILED_TO_LOAD_CLASS_FILE;
			txtDataTypesOrConstructorNotFound = Constants.MO_TXT_DATA_TYPES_OR_CONSTRUCTOR_NOT_FOUND;
			
			// Texty pro zavolání statické metody:
			txtErrorWhileCallingStaticMethod_1 =  Constants.TXT_ERROR_WHILE_CALLING_STATIC_METHOD_1;
			txtErrorWhileCallingStaticMethod_2 = Constants.TXT_ERROR_WHILE_CALLING_STATIC_METHOD_2;
			
			// zbylé texty:
			txtOverViewOfCommands = Constants.MO_TXT_OVER_VIEW_OF_COMMANDS;
			txtOverviewOfCommandsEditorVariables = Constants.MO_TXT_OVERVIEW_OF_COMMANDS_EDITOR_VARIABLES;
			txtErrorInIncrementValue = Constants.MO_TXT_ERROR_IN_INCREMENT_VALUE;
			txtNunberOutOfInterval = Constants.MO_TXT_NUMBER_OUT_OF_INTERVAL;
			txtErrorInDecrementValue = Constants.MO_TXT_ERROR_IN_DECREMENT_VALUE;
			txtVariableDontContainsNumber = Constants.MO_TXT_VARIABLE_DONT_CONTAINS_NUMBER;
			txtFailedToCastNumber = Constants.MO_TXT_FAILED_TO_CAST_NUMBER;
			txtReferenceDontPointsToInstance = Constants.MO_TXT_REFERENCE_DONT_POINTS_TO_INSTANCE;
			txtVariableIsNotPublicOrIsFinal = Constants.MO_TXT_VARIABLE_IS_NOT_PUBLIC_OR_IS_FINAL;
			txtReferenceDontPointsToInstanceOfClass = Constants.MO_TXT_REFERENCE_DOUNT_POINTS_TO_INSTANCE_OF_CLASS;
			txtReference = Constants.MO_TXT_REFERENCE;
			txtVariable = Constants.MO_TXT_VARIABLE;
			txtError = Constants.MO_TXT_ERROR;
			txtMethodWillNotBeInclude = Constants.MO_TXT_METHOD_WILL_NOT_BE_INCLUDE;
			txtMethod = Constants.MO_TXT_METHOD;
			txtMethodHasNotBennCalled = Constants.MO_TXT_METHOD_HAS_NOT_BEEN_CALLED;
			txtReferenceDoesNotPointToInstanceOfClass = Constants.MO_TXT_REFERENCE_DOES_NOT_POINTS_TO_INSTANCE_OF_CLASS;
			txtValueWillNotBeCounted = Constants.MO_TXT_VALUE_WILL_NOT_BE_COUNTED;
			txtValueOfVariableWillNotBeCounted = Constants.MO_TXT_VALUE_OF_VARIABLE_WILL_NOT_BE_COUNTED;
			txtDataTypeOfField = Constants.MO_TXT_DATA_TYPE_OF_FIELD;
			txtDataTypeIsDifferent = Constants.MO_TXT_DATA_TYPE_IS_DIFFERENT;
			txtInstance = Constants.MO_TXT_INSTANCE;
			txtErrorWhileConvertingValues = Constants.MO_TXT_ERROR_WHILE_CONVERTING_VALUES;
			txtToType = Constants.MO_TXT_TO_TYPE;
			txtVariableNameMustNotBeKeyWord = Constants.MO_TXT_VARIABLE_NAME_MUST_BE_KEY_WORD;
			txtValueIsFinalAndCannotBeChanged = Constants.MO_TXT_VALUE_IS_FINAL_AND_CANNOT_BE_CHANGED;
			txtErrorInClassLoaderOrAccessToField = Constants.MO_TXT_ERROR_IN_CLASS_LOADER_OR_ACCESS_TO_FIELD;
			txtDoNotFound = Constants.MO_TXT_DO_NOT_FOUND;
			txtFailedToCastValue = Constants.MO_TXT_FAILED_TO_CAST_VALUE;
			txtToDataType = Constants.MO_TXT_TO_DATA_TYPE;
			txtErrorWhileParsingValue = Constants.MO_TXT_ERROR_WHILE_PARSING_VALUE;
			txtProbablyIsAbstractClass = Constants.MO_TXT_PROBABLY_IS_ABSTRACT_CLASS;
			txtNotFoundConstructorToCastingDataType = Constants.MO_TXT_NOT_FOUND_CONSTRUCTOR_TO_CASTING_DATA_TYPE;
			txtPossibleParametersCountIsDifferent = Constants.MO_TXT_POSSIBLE_PARAMETERS_COUNT_IS_DIFFERENT;
			txtExceptionWhileCallingConstructorToCasting = Constants.MO_TXT_EXCEPTION_WHILE_CALLING_CONSTRUCTOR_TO_CASTING;
			txtVariableFailedToLoad = Constants.MO_TXT_VARIABLE_FAILED_TO_LOAD;
			txtVariableNotFoundForInsertValue = Constants.MO_TXT_VARIABLE_NOT_FOUND_FOR_INSERT_VALUE;
			txtVariableNotFound = Constants.MO_TXT_VARIABLE_NOT_FOUND;
			txtValue = Constants.MO_TXT_VALUE;
			txtVariableIsFinalIsNeedToFillInCreation = Constants.MO_TXT_VARIABLE_IS_FINAL_IS_NEED_TO_FILL_IN_CREATION;
			txtMustNotBeFinalAndNull = Constants.MO_TXT_MUST_NOT_BE_FINAL_AND_NULL;
			txtVariableNameIsNeedToChangeIsAlreadyTaken = Constants.MO_TXT_VARIABLE_NAME_IS_NEED_TO_CHANGE_IS_ALREADY_TAKEN;
			txtVariableNameNotRecognized = Constants.MO_TXT_VARIABLE_NAME_NOT_RECOGNIZED;
			txtDataTypeNotFoundOnlyPrimitiveDataTypes = Constants.MO_TXT_DATA_TYPE_NOT_FOUND_ONLY_PRIMITIVE_DATA_TYPE;
			txtDataType = Constants.MO_TXT_DATA_TYPE;
			txtFailedToCreateTemporaryVariable = Constants.MO_TXT_FAILED_TO_CREATE_TEMPORARY_VARIABLE;
			txtData = Constants.MO_TXT_DATA;
			txtValue2 = Constants.MO_TXT_VALUE_2;
			txtMaybeError = Constants.MO_TXT_MAY_BE_ERROR;
			txtDataTypeDoNotSupported = Constants.MO_TXT_DATA_TYPE_DO_NOT_SUPPORTED;
			txtVariableIsNullCannotBeIncrementOrDecrement = Constants.MO_TXT_VARIABLE_IS_NULL_CANNOT_BE_INCREMENT_OR_DECREMENT;
			txtVariableIsNull = Constants.MO_TXT_VARIABLE_IS_NULL;
			txtValueWasNotRecognized = Constants.MO_TXT_VALUE_WAS_NOT_RECOGNIZED;
			txtFailedToCreateVariable = Constants.MO_TXT_FAILED_TO_CREATE_VARIABLE;
			txtDoNotRetrieveDataTypeOfVariable = Constants.MO_TXT_DO_NOT_RETRIEVE_DATA_TYPE_OF_VARIABLE;
			txtFailedToGetDataAboutVariableWhileDeclaration = Constants.MO_TXT_FAILED_TO_GET_DATA_ABOUT_VARIABLE_WHILE_DECLARATION;
			txtReferenceDontPointToInstanceOfList = Constants.MO_TXT_REFERENCE_DONT_POINT_TO_INSTANCE_OF_LIST;
			txtValueAtIndexIsNull = Constants.MO_TXT_VALUE_AT_INDEX_IS_NULL;
			txtConditionsForIndexOfList = Constants.MO_TXT_CONDITIONS_FOR_INDEX_OF_LIST;
			txtSpecifiedIndexOfList = Constants.MO_TXT_SPECIFIED_INDEX_OF_LIST;
			txtInstanceOfListWasNotFoundUnderReference = Constants.MO_TXT_INSTANCE_OF_LIST_WAS_NOT_FOUND_UNDER_REFERENCE;
			txtSizeOfTheList = Constants.MO_TXT_SIZE_OF_THE_LIST;
			txtDataTypeForListNotFound = Constants.MO_TXT_DATA_TYPE_FOR_LIST_NOT_FOUND;
			txtDataTypeForArrayAreNotIdenticaly = Constants.MO_TXT_DATA_TYPE_FOR_ARRAY_ARE_NOT_IDENTICALY;
			txtDataTypeForArrayNotFound = Constants.MO_TXT_DATA_TYPE_FOR_ARRAY_NOT_FOUND;
			txtInstanceOfArrayWasNotFoundUnderReference = Constants.MO_TXT_INSTANCE_OF_ARRAY_WAS_NOT_FOUND_UNDER_REFERENCE;
			txtArrayIsFinalDontChangeValues = Constants.MO_TXT_ARRAY_IS_FINAL_DONT_CHANGE_VALUES;
			txtArray = Constants.MO_TXT_ARRAY;
			txtIndexOutOfRange = Constants.MO_TXT_INDEX_OUT_OF_RANGE;
			txtConditionForIndexInArray = Constants.MO_TXT_CONDITIONAL_FOR_INDEX_IN_ARRAY;
			txtIndex = Constants.MO_TXT_INDEX;
			txtSizeOfArray = Constants.MO_TXT_SIZE_OF_ARRAY;
			txtName = Constants.MO_TXT_NAME;
			txtDoesNotIncrementDecrement = Constants.MO_TXT_DOES_NOT_INCREMENT_DECREMET;
			txtIndexOfListIsOutOfRange = Constants.MO_TXT_INDEX_OF_LIST_IS_OUT_OF_RANGE;
			txtVariableIsFinalDontChangeValue = Constants.MO_TXT_VARIABLE_IS_FINAL_DONT_CHANGE_VALUE;	
			txtInstanceIsNotCorrectForParameter = Constants.MO_TXT_INSTANCE_IS_NOT_CORRECT_FOR_PARAMETER;
			txtArraySizeIsOutOfInteger = Constants.MO_TXT_ARRAY_SIZE_IS_OUT_OF_INTEGER;
			txtValueInArrayInIndexIsNull = Constants.MO_TXT_VALUE_IN_ARRAY_IN_INDEX_IS_NULL;
			txtValueAtIndexAtArrayCantIncreDecre = Constants.MO_TXT_VALUE_AT_INDEX_AT_ARRAY_CANT_INCRE_DECRE;
			txtFailedToCalculateMathTerm_1 = Constants.MO_TXT_FAILED_TO_CALCULATE_MATH_TERM_1;
			txtFailedToCalculateMathTerm_2 = Constants.MO_TXT_FAILED_TO_CALCULATE_MATH_TERM_2;
			
			// Matematické operace:
			txtMathCalculationTitle = Constants.MO_WC_TXT_MATH_CALCULATION_TITLE;
			txtOperandsForMathCalculation = Constants.MO_WC_TXT_OPERANDS_FOR_MATH_CALCULATION;
			// Logické operaátory:
			txtLogicalOperationsInfo = Constants.MO_WC_TXT_LOGICAL_OPERATIONS_INFO;
			// Konstanty:
			txtConstants = Constants.MO_WC_TXT_CONSTANTS;
			// Podporované funkce:
			txtSupportedFunctionsTitle = Constants.MO_WC_TXT_SUPPORTED_FUNCTIONS_TITLE;
			txtNamesOfFunctionsInfo = Constants.MO_WC_TXT_NAMES_OF_FUNCTIONS_INFO;
			// Příkaldy:
			txtWithoutSemicolonAtTheEndsOfExpression = Constants.MO_WC_TXT_WITHOUT_SEMICOLON_AT_ENDS_OF_EXPRESSION;
			txtIncludeVariablesFromCommandEditorInfo = Constants.MO_WC_TXT_INCLUDE_VARIABLES_FROM_COMMANDS_EDITOR_INFO;
			txtVarNamesCantConstainNumbers = Constants.MO_WC_TXT_VAR_NAMES_CANT_CONTAINS_NUMBERS;

			txtDeclaredAndFulfilledVars = Constants.MO_WC_TXT_DECLARED_AND_FULFILLED_VARS;
			txtDeclaredAndFulfilledVar = Constants.MO_WC_TXT_DECLARED_AND_FULFILLED_VAR;
			
			// Matematické operace:
			txtUnaryPlus = Constants.MO_WC_TXT_UNARY_PLUS;
			txtUnaryMinus = Constants.MO_WC_TXT_UNARY_MINUS;
			txtMultiplicationOperator = Constants.MO_WC_TXT_MULTIPLICATION_OPERATOR;
			txtDivisionOperator = Constants.MO_WC_TXT_DIVISION_OPERATOR; 
			txtModuloOperator = Constants.MO_WC_TXT_MODULO_OPERATOR;
			txtPowerOperator = Constants.MO_WC_TXT_POWERED_OPERATOR;


			// Logické operátory:
			txtEquals = Constants.MO_WC_TXT_EQUALS; 
			txtNotEquals = Constants.MO_WC_TXT_NOT_EQUALS; 
			txtLessThan = Constants.MO_WC_TXT_LESS_THAN; 
			txtLessThanOrEqualTo = Constants.MO_WC_TXT_LESS_THAN_OR_EQUAL_TO; 
			txtGreaterThan = Constants.MO_WC_TXT_GREATER_THAN;
			txtGreaterThanOrEqualTo = Constants.MO_WC_TXT_GREATER_THAN_OR_EQUAL_TO;  
			txtBooleanAnd = Constants.MO_WC_TXT_BOOLEAN_AND; 
			txtBooleanOr = Constants.MO_WC_TXT_BOOLEAN_OR; 

			// Konstanty:
			txtValueOfE = Constants.MO_WC_TXT_VALUE_OF_E;
			txtValueOfPi = Constants.MO_WC_TXT_VALUE_OF_PI; 
			txtValueOne = Constants.MO_WC_TXT_VALUE_ONE;
			txtValuezero = Constants.MO_WC_TXT_VALUE_ZERO; 

			// podporované funkce:
			txtFceNot = Constants.MO_WC_TXT_FCE_NOT; 
			txtCondition = Constants.MO_WC_TXT_CONDITION; 
			txtFceRandom = Constants.MO_WC_TXT_FCE_RANDOM; 
			txtFceMin = Constants.MO_WC_TXT_FCE_MIN; 
			txtFceMax = Constants.MO_WC_TXT_FCE_MAX; 
			txtFceAbs = Constants.MO_WC_TXT_FCE_ABS;
			txtFceRound = Constants.MO_WC_TXT_FCE_ROUND; 
			txtFceFloor = Constants.MO_WC_TXT_FCE_FLOOR; 
			txtFceCeiling = Constants.MO_WC_TXT_FCE_CELLING; 
			txtFcelog =  Constants.MO_WC_TXT_FCE_LOG;
			txtFceLog10 = Constants.MO_WC_TXT_FCE_LOG_10; 
			txtFceSqrt = Constants.MO_WC_TXT_FCE_SQRT;
			txtFceSin = Constants.MO_WC_TXT_FCE_SIN; 
			txtFceCos = Constants.MO_WC_TXT_FCE_COS;
			txtFceTan = Constants.MO_WC_TXT_FCE_TAN;
			txtFceCot = Constants.MO_WC_TXT_FCE_COT; 
			txtFceAsin = Constants.MO_WC_TXT_FCE_ASIN; 
			txtFceAcos = Constants.MO_WC_TXT_FCE_ACOS;
			txtFceAtan = Constants.MO_WC_TXT_FCE_ATAN; 
			txtFceAcot = Constants.MO_WC_TXT_FCE_ACOT;
			txtFceATan2 = Constants.MO_WC_TXT_FCE_ATAN2; 
			txtFceASinh = Constants.MO_WC_TXT_FCE_SINH;
			txtFceCosh = Constants.MO_WC_TXT_FCE_COSH; 
			txtFceTah = Constants.MO_WC_TXT_FCE_TAH;
			txtFceCoth = Constants.MO_WC_TXT_FCE_COTH; 
			txtFceSec = Constants.MO_WC_TXT_FCE_SEC; 
			txtFceCsc = Constants.MO_WC_TXT_FCE_CSC;
			txtFceSech = Constants.MO_WC_TXT_FCE_SECH; 
			txtFceCsch = Constants.MO_WC_TXT_FCE_CSCH;
			txtFceAsinh = Constants.MO_WC_TXT_FCE_ASINH; 
			txtFceAcosh = Constants.MO_WC_TXT_FCE_ACOSH; 
			txtFceATanh = Constants.MO_WC_TXT_FCE_TANH; 
			txtFceRad = Constants.MO_WC_TXT_FCE_RAD; 
			txtFceDeg = Constants.MO_WC_TXT_FCE_DEG;
			
			
			// Nějkaé ukázky pro matematické výrazy,které je možné zadat (podmínky, funkce,
			// jejich využití, ...)
			txtExamplesOfMathExpressions = Constants.MO_WC_TXT_EXAMPLES_OF_MATH_EXPRESSIONS;
			txtMakeInstance = Constants.MO_WC_TXT_MAKE_INSTANCE;
			txtClassName = Constants.MO_WC_TXT_CLASS_NAME;
			txtMakeInstanceParameters_1 = Constants.MO_WC_TXT_MAKE_INSTANCE_PARAMETERS_1;
			txtMakeInstanceParameters_2 = Constants.MO_WC_TXT_MAKE_INSTANCE_PARAMETERS_2;
			txtMakeInstanceParameters_3 = Constants.MO_WC_TXT_MAKE_INSTANCE_PARAMETERS_3;
			txtWithParameters = Constants.MO_WC_TXT_WITH_PARAMETERS;
			txtCallConstructorInfo_1 = Constants.MO_WC_TXT_CALL_CONSTRUCTOR_INFO_1;
			txtCallConstructorInfo_2 = Constants.MO_WC_TXT_CALL_CONSTRUCTOR_INFO_2;
			txtCallConstructorInfo_3 = Constants.MO_WC_TXT_CALL_CONSTRUCTOR_INFO_3;
			txtCallMethodInfo = Constants.MO_WC_TXT_CALL_METHOD_INFO;
			txtCallStaticMethodInfo = Constants.MO_WC_TXT_CALL_STATIC_METHOD_INFO;
            txtCallStaticMethodInInnerClassInfo = Constants.MO_WC_TXT_CALL_STATIC_METHOD_IN_INNER_CLASS_INFO;
			txtCallMethodInfo_1 = Constants.MO_WC_TXT_CALL_METHOD_INFO_1;
			txtCallMethodInfo_2 = Constants.MO_WC_TXT_CALL_METHOD_INFO_2;
			// Info ohledně zavolání zděděných metod nad třídou coby potomkem příslušné
			// třídy s danou metodou:
			txtInfoAboutMethodsTitle = Constants.MO_WC_TXT_INFO_ABOUT_METHODS_TITLE;
			txtInfoAboutCallMethodFromObjectClass = Constants.MO_WC_TXT_INFO_ABOUT_CALL_METHODS_FROM_OBJECT_CLASS;
			txtCallMethodFromInheritedClassesInfo = Constants.MO_WC_TXT_CALL_METHOD_FROM_INHERITED_CLASS_INFO;
			txtCallStaticMethodFroomInheritedClassesInfo = Constants.MO_WC_TXT_CALL_STATIC_METHOD_FROM_INHERITED_CLASSES_INFO;
			txtIncrementationDecrementation = Constants.MO_WC_TXT_INCREMENTATION_DCREMENTATION;
			txtIncrementationDecrementation_1 = Constants.MO_WC_TXT_INCREMENTATION_DECREMENTATION_1;
			txtVariableIncrementation = Constants.MO_WC_TXT_VARIABLE_INCREMENTATION;
			txtVariableDecrementation = Constants.MO_WC_TXT_VARIABLE_DECREMENTATION;
			txtIncrementationMyVariable = Constants.MO_WC_TXT_INCREMENTATION_MY_VARIABLE;
			txtDecrementationMyVariable = Constants.MO_WC_TXT_DECREMENTATION_MY_VARIABLE;
			txtIncrementationDecrementationConstatnts = Constants.MO_WC_TXT_INCREMENTATION_DECREMENTATION_CONSTANTS;
			txtIncrementationConstants = Constants.MO_WC_TXT_INCREMENTATION_CONSTANTS;
			txtDecrementationConstants = Constants.MO_WC_TXT_DECREMENTATION_CONSTANTS;
			txtDeclarationOfVariable = Constants.MO_WC_TXT_DECLARATION_OF_VARIABLE;
			txtDeclarationVariableInfo = Constants.MO_WC_TXT_DECLARATION_VARIABLE_INFO;
			txtLikeThis = Constants.MO_WC_TXT_LIKE_THIS;
			txtDeclarationVariableInfo_2 = Constants.MO_WC_TXT_DECLARATION_VARIABLE_INFO_2;
			txtSyntaxe = Constants.MO_WC_TXT_SYNTAXE;
			txtForString = Constants.MO_WC_TXT_SYNTAXE_FOR_STRING;
			txtSyntaxeForChar = Constants.MO_WC_TXT_SYNTAXE_FOR_CHAR;
			txtSyntaxeForDouble = Constants.MO_WC_TXT_SYNTAXE_FOR_DOUBLE;
			txtSimilaryDataTypes = Constants.MO_WC_TXT_SIMILARY_DATA_TYPES;
			txtFullfillmentVariable = Constants.MO_WC_TXT_FULLFILMENT_VARIABLE;
			txtFullfilmentVariableInfo = Constants.MO_WC_TXT_FULLFILMENT_VARIABLE_INFO;
			txtFullfilmentVariableSyntaxeInfo = Constants.MO_WC_TXT_FULLFILMENT_VARIABLE_SYNTAXE_INFO;
			txtFullfilmentVariableText = Constants.MO_WC_TXT_FULLFILMENT_VARIABLE_TEXT;
			txtFullfilmentMyVariable = Constants.MO_WC_TXT_FULLFILMENT_MY_VARIABLE;
			txtFullfilmentReplaceInfo = Constants.MO_WC_TXT_FULFILMENT_REPLACE_INFO;
			txtFullfilmentReturnValue = Constants.MO_WC_TXT_FULLFILMENT_RETURN_VALUE;
			txtFullfilmentByStaticMethod = Constants.MO_WC_TXT_FULLFILMENT_BY_STATIC_METHOD;
            txtFullfilmentByStaticMethodInInnerClass = Constants.MO_WC_TXT_FULLFILMENT_BY_STATIC_METHOD_IN_INNER_CLASS;
			txtFullfilmentVariableSameRules = Constants.MO_WC_TXT_FULLFILMENT_VARIABLE_SAME_RULES;
			txtFullfilmentVariableInfo_2 = Constants.MO_WC_TXT_FULLFILMENT_VARIABLE_INFO_2;
			txtFullfilmentEtc = Constants.MO_WC_TXT_FULLFILMENT_ETC;
			txtPrintMethodInfo = Constants.MO_WC_TXT_PRINT_METHOD_INFO;
			txtPrintMethodInfo_2 = Constants.MO_WC_TXT_PRINT_METHOD_INFO_2;
			txtPrintMethodInfo_3 = Constants.MO_WC_TXT_PRINT_METHOD_INFO_3;
			txtPrintMethodInfo_4 = Constants.MO_WC_TXT_PRINT_METHOD_INFO_4;
			txtPrintMethodSyntaxeExample = Constants.MO_WC_TXT_PRINT_METHOD_SYNTAXE_EXAMPLE;
			txtPrintMethodWriteResult = Constants.MO_WC_TXT_PRINT_METHOD_WRITE_RESULT;
			txtPrintMethodReturnValue = Constants.MO_WC_TXT_PRINT_METHOD_RETURN_VALUE;
			txtDeclarationListInfo = Constants.MO_WC_TXT_DECLARATION_LIST_INFO;
			txtDeclarationListInfo_2 = Constants.MO_WC_TXT_DECLARATION_LIST_INFO_2;
			txtDeclarationListInfo_3 = Constants.MO_WC_TXT_DECLARATION_LIST_INFO_3;
			txtNote = Constants.MO_WC_TXT_NOTE;
			txtDeclarationListInfo_4 = Constants.MO_WC_TXT_DECLARATION_LIST_INFO_4;
			txtDeclarationListInfo_5 = Constants.MO_WC_TXT_DECLARATION_LIST_INFO_5;
			txtListMethodInfo_1 = Constants.MO_WC_TXT_LIST_METHOD_INFO_1;
			txtListMethodInfo_2 = Constants.MO_WC_TXT_LIST_METHOD_INFO_2;
			txtListMethodInfo_3 = Constants.MO_WC_TXT_LIST_METHOD_INFO_3;
			txtListMethodInfo_4 = Constants.MO_WC_TXT_LIST_METHOD_INFO_4;
			txtListMethodInfo_5 = Constants.MO_WC_TXT_LIST_METHOD_INFO_5;
			txtListMethodInfo_6 = Constants.MO_WC_TXT_LIST_METHOD_INFO_6;
			txtListMethodInfo_7 = Constants.MO_WC_TXT_LIST_METHOD_INFO_7;
			txtListMethodInfo_8 = Constants.MO_WC_TXT_LIST_METHOD_INFO_8;
			txtListMethodInfo_9 = Constants.MO_WC_TXT_LIST_METHOD_INFO_9;
			txtListMethodInfo_10 = Constants.MO_WC_TXT_LIST_METHOD_INFO_10;
			txtDeclarationOneDimensionalArray = Constants.MO_WC_TXT_DECLARATION_ONE_DIMENSIONAL_ARRAY;
			txtOneDimArrayInfo_1 = Constants.MO_WC_TXT_ONE_DIM_ARRAY_INFO_1;
			txtOneDimArrayInfo_2 = Constants.MO_WC_TXT_ONE_DIM_ARRAY_INFO_2;
			txtOneDimArrayInfo_3 = Constants.MO_WC_TXT_ONE_DIM_ARRAY_INFO_3;
			txtDataTypeArrayCanBe = Constants.MO_WC_TXT_DATA_TYPE_ARRAY_CAN_BE;
			txtOperationsWithArray = Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY;
			txtOperationsWithArray_1 = Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_1;
			txtOperationsWithArray_2 = Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_2;
			txtOperationsWithArray_3 = Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_3;
			txtOperationsWithArray_4 = Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_4;
			txtOperationsWithArray_5 = Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_5;
			txtOperationsWithArray_7 = Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_6;
			txtOperationsWithArray_8 = Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_7;
			txtTwoDimArrayTitle = Constants.MO_WC_TXT_TWO_DIM_ARRAY_TITLE;
			txtWorkWithTwoDimArrayInfo = Constants.MO_WC_TXT_WORK_WITH_TWO_DIM_ARRAY_INFO;
			txtCannotCreateTwoDimArray =  Constants.MO_WC_TXT_CANNOT_CREATE_TWO_DIM_ARRAY;
			txtOperationsWithTwoDimArray =  Constants.MO_WC_TXT_OPERATIONS_WITH_TWO_DIM_ARRAY;
			txtFullfilmentArrayInfo_1 = Constants.MO_WC_TXT_FULLFILMENT_ARRAY_INFO_1;
			txtFullfilmentArrayInfo_2 = Constants.MO_WC_TXT_FULLFILMENT_ARRAY_INFO_2;
			txtFullfilmentArrayInfo_3 = Constants.MO_WC_TXT_FULLFILMENT_ARRAY_INFO_3;
			txtOperationsWithArrayinIntanceTitle = Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_IN_INSTANCE_TITLE; 
			txtOperationsWithArrayInInstanceInfo =  Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_IN_INSTANCE_INFO;
			txtOperationsWithArrayInInstanceDifference = Constants.MO_WC_TXT_OPERATIONS_WITH_ARRAY_IN_INSTANCE_DIFFERENCE;
			txtInfoAboutNullValue = Constants.MO_WC_TXT_INFO_ABOUT_NULL_VALUE;
			
			// Info ohledně metod pro restartování a ukončení aplikace:
			txtInfoForMethodsOverApp = Constants.MO_WC_TXT_INFO_FOR_METHODS_OVER_APP;

			// Restartování aplikace:
			txtRestartAppTitle = Constants.MO_WC_TXT_RESTART_APP_TITLE;

			// Info k restartu - že se uložídiagram tříd apod.
			txtInfoAboutCloseOpenFilesBeforeRestartApp = Constants.MO_WC_TXT_INFO_ABOUT_CLOSE_OPEN_FILES_BEFORE_RESTART_APP;
			txtInfoAboutLaunchCountdownWhenCommandIsEntered = Constants.MO_WC_TXT_INFO_ABOUT_LAUNCH_COUNTDOWN_WHEN_COMMAND_IS_ENTERED;
			txtInfoAboutSaveClassDiagramBeforeRestartApp = Constants.MO_WC_TXT_INFO_ABOUT_SAVE_CLASS_DIAGRAM_BEFORE_RESTART_APP;
			txtTwoWaysToRestartApp = Constants.MO_WC_TXT_TWO_WAYS_TO_RESTART_APP;
			txtFirstWayToRestartApp_1 = Constants.MO_WC_TXT_FIRST_WAY_TO_RESTART_APP_1;				
			txtFirstWayToRestartApp_2 = Constants.MO_WC_TXT_FIRST_WAY_TO_RESTART_APP_2;
			txtSecondWayToRestartApp = Constants.MO_WC_TXT_SECOND_WAY_TO_RESTART_APP;

			
			// Texty pro chybové hlášky nad operace s jednorozměrným polem v instanci třídy:
			txtCastingToIntegerFailure_1 = Constants.MO_WC_TXT_CASTING_TO_INTEGER_FAILURE_1;
			txtCastingToIntegerFailure_2 = Constants.MO_WC_TXT_CASTING_TO_INTEGER_FAILURE_2;
			txtInstanceOfClassInIdNotFOund = Constants.MO_WC_TXT_INSTANCE_OF_CLASS_IN_ID_NOT_FOUND;
			txtEnteredVariable_1 = Constants.MO_WC_TXT_ENTERED_VARIABLE_1;
			txtEnteredVariable_2 = Constants.MO_WC_TXT_ENTERED_VARIABLE_2;
			txtIsNotOneDimArray = Constants.MO_WC_TXT_IS_NOT_ONE_DIM_ARRAY;
			txtIndexValueAtArrayIsNotInInterval = Constants.MO_WC_TXT_INDEX_VALUE_AT_ARRAY_IS_NOT_IN_INTERVAL;
			txtValueInArrayIsNull = Constants.MO_WC_TXT_VALUE_IN_ARRAY_IS_NULL;
			
			
			// Texty pro chybové hlášky nad operacemi s dvourozměrným polem:
			txtIsNotTwoDimArray =  Constants.MO_WC_TXT_IS_NOT_TWO_DIM_ARRAY;
			txtFailedToSetValueToTwoDimArray = Constants.MO_WC_TXT_FAILED_TO_SET_VALUE_TO_TWO_DIM_ARRAY;
			txtIndex_1Text = Constants.MO_WC_TXT_INDEX_1_TEXT;
			txtIndex_2Text = Constants.MO_WC_TXT_INDEX_2_TEXT;
			txtFailedToGetValueFromTwoDimArray = Constants.MO_WC_TXT_FAILED_TO_GET_VALUE_FROM_TWO_DIM_ARRAY;
			txtFailedToIncrementValueAtIndexInTwoDimArrayInInstance = Constants.MO_WC_TXT_FAILED_TO_INCREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE; 
			txtFailedToDecrementValueAtIndexInTwoDimArrayInInstance = Constants.MO_WC_TXT_FAILED_TO_DECREMENT_VALUE_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE;
			txtValueWasNotFoundAtIndexInTwoDimarrayInInstance = Constants.MO_WC_TXT_VALUE_WAS_NOT_FOUND_AT_INDEX_IN_TWO_DIM_ARRAY_IN_INSTANCE;
			
			
			// Ukončení aplikace
			txtCloseAppTitle = Constants.MO_WC_TXT_CLOSE_APP_TITLE;
			txtInfoAboutCloseOpenFilesBeforeCloseApp = Constants.MO_WC_TXT_INFO_ABOUT_CLOSE_OPEN_FILES_BEFORE_CLOSE_APP;
			txtInfoAboutLaunchCountdownAfterCommandIsEntered = Constants.MO_WC_TXT_INFO_ABOUT_LAUNCH_COUNTDOWN_AFTER_COMMAND_IS_ENTERED;
			txtInfoAboutSaveClassDiagramBeforeCloseApp = Constants.MO_WC_TXT_INFO_ABOUT_SAVE_CLASS_DIAGRAM_BEFORE_CLOSE_APP;
			txtThreeWaysToCloseApp = Constants.MO_WC_TXT_THREE_WAYS_TO_CLOSE_APP;
			txtFirstWayToCloseApp_1 = Constants.MO_WC_TXT_FIRST_WAY_TO_CLOSE_APP_1;
			txtFirstWayToCloseApp_2 = Constants.MO_WC_TXT_FIRST_WAY_TO_CLOSE_APP_2;
			txtSecondWayToCloseApp = Constants.MO_WC_TXT_SECOND_WAY_TO_CLOSE_APP;
			txtThirdWayToCloseApp_1 = Constants.MO_WC_TXT_THIRD_WAY_TO_CLOSE_APP_1;
			txtThirdWayToCloseApp_2 = Constants.MO_WC_TXT_THIRD_WAY_TO_CLOSE_APP_2;
			txtInfoAboutThirdWayToCloseApp_1 = Constants.MO_WC_TXT_INFO_ABOUT_THIRD_WAY_TO_CLOSE_APP_1;
			txtInfoAboutThirdWayToCloseApp_2 = Constants.MO_WC_TXT_INFO_ABOUT_THIRD_WAY_TO_CLOSE_APP_2;

			// Zrušení odpočtu při restartování nebo ukončení aplikace:
			txtCancelCountdownTitle = Constants.MO_WC_TXT_CANCEL_COUNTDOWN_TITLE;
			txtCancelCountdownInfo = Constants.MO_WC_TXT_CANCEL_COUNTDOWN_INFO;
			
			// Texty, že nebyl nalezen soubor .class pro zavolaní staticke metody a nebo konstruktoru:
			txtFileDotClass = Constants.MO_WC_TXT_FILE_DOT_CLASS; 
			txtFileDotClassWasNotFound = Constants.MO_WC_TXT_FILE_DOT_CLASS_WAS_NOT_FOUND;
            txtFailedToFindTheClass = Constants.MO_WC_TXT_FAILED_TO_FIND_THE_CLASS;
            txtFailedToFindBinDir = Constants.MO_WC_TXT_FAILED_TO_FIND_BIN_DIR;
            txtFailedToFindInnerClass = Constants.MO_WC_TXT_FAILED_TO_FIND_INNER_CLASS;
            txtInnerClassIsNotStatic = Constants.MO_WC_TXT_INNER_CLASS_IS_NOT_STATIC;
            txtInnerClassIsNotPublicEitherProtected = Constants.MO_WC_TXT_INNER_CLASS_IS_NOT_PUBLIC_EITHER_PROTECTED;
			txtSpecificMethodWasNotFound = Constants.MO_WC_TXT_SPECIFIC_METHOD_WAS_NOT_FOUND;
			txtPublicConstructorWasNotFound = Constants.MO_WC_TXT_PUBLIC_CONSTRUCTOR_WAS_NOT_FOUND;
					
			// Text pro zavolání konstruktoru:
			txtConstructorWasNotFound = Constants.MO_WC_TXT_CONSTRUCTOR_WAS_NOT_FOUND;
			
			// Text do metody printTempVariables();:
			txtNoVariablesWasCreated = Constants.MO_WC_TXT_NO_VARIABLE_WAS_CREATED;
			
			
			// Ukončení a restart aplikace:
			
			// Texty pro ukončení odpočtu:
			txtNoCountdownIsRunning = Constants.MO_WC_TXT_NO_COUNTDOWN_IS_RUNNING;
			txtCountdownIsStopped = Constants.MO_WC_TXT_COUNTDOWN_IS_STOPPED;

			// Texty pro spuštění timeru pro ukončení aplikace.
			txtCountdownForCloseAppIsRunning = Constants.MO_WC_TXT_COUNTDOWN_FOR_CLOSE_APP_IS_RUNNING;
			txtCountdownForCloseAppIsNotPossibleToStart = Constants.MO_WC_TXT_COUNTDOWN_FOR_CLOSE_APP_IS_NOT_POSSIBLE_TO_START;
			
			txtToEndOfAppRemains_1 = Constants.MO_WC_TXT_TO_END_OF_APP_REMAINS_1;
			txtToEndOfAppRemains_2 = Constants.MO_WC_TXT_TO_END_OF_APP_REMAINS_2;
			txtToEndOfAppRemains_3 = Constants.MO_WC_TXT_TO_END_OF_APP_REMAINS_3;
			
			txtSecondsForClose_1 = Constants.MO_WC_TXT_SECONDS_FOR_CLOSE_1;
			txtSecondsForClose_2 = Constants.MO_WC_TXT_SECONDS_FOR_CLOSE_2;
			txtSecondsForClose_3 = Constants.MO_WC_TXT_SECONDS_FOR_CLOSE_3;

			// Texty pro spuštění timeru pro restartování aplikace:
			txtCountdownForRestartAppIsRunning = Constants.MO_WC_TXT_COUNTDOWN_FOR_RESTART_APP_IS_RUNNING;
			txtCountdownForRestartAppIsNotPossibleToStart = Constants.MO_WC_TXT_COUNTDOWN_FOR_RESTART_APP_IS_NOT_POSSIBLE_TO_START;
			
			txtToRestartAppRemains_1 = Constants.MO_WC_TXT_TO_RESTART_APP_REMAINS_1;
			txtToRestartAppRemains_2 = Constants.MO_WC_TXT_TO_RESTART_APP_REMAINS_2;
			txtToRestartAppRemains_3 = Constants.MO_WC_TXT_TO_RESTART_APP_REMAINS_3;
			
			txtSecondsForRestart_1 = Constants.MO_WC_TXT_SECONDS_FOR_RESTART_1;
			txtSecondsForRestart_2 = Constants.MO_WC_TXT_SECONDS_FOR_RESTART_2;
			txtSecondsForRestart_3 = Constants.MO_WC_TXT_SECONDS_FOR_RESTART_3;
		}
	}














	/**
	 * Metoda, která spustí vlákno, které otestuje vztahy mezi jednotlivými
	 * instancemi v diagram instancí.
	 * 
	 * Toto vlákno je třeba spustit po naplnění proměnné nebo zavolání metoda nad
	 * instancí třídy, protože každá metodä v instanci třídy může provést například
	 * nějaké naplnění proměnné, pomocí které se naplní nějaký vztah například
	 * asociace mezi třídami, apod. Více ve vlákně
	 */
	private void runThreadForCheckInstancesRelations() {
		// Vytvořím si ThreadSwingWorker pro provedení nějaké úlohy na pozadí aplikace:
		ThreadSwingWorker.runMyThread(
				new CheckRelationShipsBetweenInstances(instanceDiagram, instanceDiagram.isShowRelationShipsToItself(),
						GraphInstance.isUseFastWay(), GraphInstance.isShowRelationshipsInInheritedClasses(),
						instanceDiagram.isShowAssociationThroughAggregation()));
	}
}