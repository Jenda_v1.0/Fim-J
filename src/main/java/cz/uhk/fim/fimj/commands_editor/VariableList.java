package cz.uhk.fim.fimj.commands_editor;

import java.util.List;

/**
 * Tato třída slouží pro vytvoření proměnné v editoru příkazů coby proměnné typu List, tato třída budou obsahovat
 * proměnnou List, informace o tom, zda je final nebo ne, a metody pro získání nějaké hodnoty v kolekci, její vamazání a
 * přidání hodnoty (na index, nebo konec).
 * <p>
 * Instance této třídy se vytvoří pokaždé, když buzde chctít uživatel v editrou příkazů vytvořit proměnou typu List
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class VariableList<T> extends VariableAbstract {

    /**
     * Jelikož když se dotážu na datový typ (Class) listu, tak se mi vždy vrátí jeho instance, například: pokud bude
     * List<String> var = new ArrayList<>(); tak se mi vrátí var.getClass() = java.util.ArrayList jenže jáí potřebuji
     * ještě typ java.util.List, tak proto vytvořím následující proměnnou, která mi tento typ vždy vrátí Toto jsem
     * napsal kvůli testování datového typu pro předávání reference na tento list do parametru například metody či
     * konstruktoru, apod.
     */
    public static final Class<?> CLASS_LIST = List.class;


    /**
     * Kolekce, do které budou vkládány hodnoty z editoru příkazů:
     */
    private List<T> list;


    /**
     * Proměnná, ve které bude uložen datový typ hodnot, které je možné vkládat do kolekce:
     */
    private final Class<T> classType;


    /**
     * Konstruktor třídy, který naplní výše uvedené proměnné
     *
     * @param list
     *         - kolekce získaná z editoru, samozřejmě po upravé kódu a získání hodnot z parametrů, ...
     * @param isFinal
     *         - logická proměnná o tom, zda je list final nebo ne
     * @param classType
     *         - datový typ hodnot, který bude v kolekci v této třídě, potřebuji to předat zde, takto, abych mohl při
     *         přidávání hodnot do kolekce danou hodnotu přetypovat na příslušný datový typ
     */
    @SuppressWarnings("unchecked")
    public VariableList(final List<T> list, final boolean isFinal, final Class<?> classType) {
        super(isFinal);

        this.list = list;
        this.classType = (Class<T>) classType;
    }


    /**
     * Metoda, která přidá hodnotu v parametru na konec kolekce v instanci této třídy.
     * <p>
     * Metoda si nejprve přetypuje hodnotu na potřebný datový typ, aby mohla být vložena do kolekce
     *
     * @return true pokud se povode přidání položky do listu, jinak false.
     */
    public final boolean addValueToEndOfList(final Object o) {
        return list.add(classType.cast(o));
    }


    /**
     * Metoda, která zjistí a vrátí datový typ hodnot, které lze vkládat do dané kolekce v této třídě
     *
     * @return vrátí datový typ hodnot, které lze vkládat do kolekce
     */
    public final Class<?> getClassType() {
        return classType;
    }


    /**
     * Metoda, která vrátí referenci na daný list v této konkrétní instanci třídy
     *
     * @return list s hodnotami
     */
    public final List<T> getList() {
        return list;
    }


    /**
     * Metoda, která přidá zadanou hodnotu v parametru na zadaný index v kolekci
     *
     * @param index
     *         - index, kam se má přidat hodnota
     * @param objvalue
     *         - hodnota, která se má přidat na zadaný index v kolekci - listu
     */
    public final void addValueToIndexAtList(final int index, final Object objvalue) {
        list.add(index, classType.cast(objvalue));
    }


    /**
     * Metoda, která se pokudí vymazat obsah kolekci pomocí metody clear, kterou zavolá nad danou kolekcí
     */
    public final void clearList() {
        list.clear();
    }


    /**
     * Metoda, která vrátí hodnotu z Listu - kolekce na zadaném indexu
     *
     * @param index
     *         - index, ze kterého se má vrátit hodnota
     *
     * @return hodnotu na zadaném indexu
     */
    public final T getValueAtIndex(final int index) {
        return list.get(index);
    }


    /**
     * Metoda, která nastaví hodnotu na zadaném indexu v Listu - kolekci
     *
     * @param index
     *         - index v listu, kde se má nastavit - přepsat nějaká hodnota
     * @param objValue
     *         - hodnota, která se má nastavit na daný index
     *
     * @return objekt, který byl na té pozici před nastavením nové hodnoty.
     */
    public final T setValueToIndex(final int index, final Object objValue) {
        return list.set(index, classType.cast(objValue));
    }


    /**
     * Metoda, která převede list do jednorozměrného pole stejného typu a vrátí ho
     *
     * @return list převedený do jednorozměrného pole stejného datového typu
     */
    @SuppressWarnings("unchecked")
    public final T[] getArrayFromList() {
        return (T[]) list.toArray();
    }


    @Override
    public String toString() {
        return list.toString();
    }


    /**
     * Metoda, která odebere hodnotu v listu (kolekci) na zadaném indexu
     *
     * @param index
     *         - index hodnoty, která se má z listu odebrat
     *
     * @return element na té pozici, co se má odebrat (vrátí odebraný objekt / element).
     */
    public final T removeValueAtIndex(final int index) {
        return list.remove(index);
    }


    /**
     * Metoda, která naplní list, který je reprezentován touto třídou a "je v ní umístěň", tak ho naplění listem v
     * parametru
     * <p>
     * Note: Jelikož v nejsou známy generické typy "předem" - nemohu si zjistit datový typ hodnot, které lze vkládat do
     * kolekce, tak nemohu otestovat, zda lze tuto kolekce naplnit tou v parametru metody nebo ne, proto testuji pouze,
     * zda se jedná o kolekci - List a pokud ano, tak "tento list naplním".
     * <p>
     * Takže i když se v editoru příkazů definuje například List typu Integer a List typu String tak do listu se
     * Stringem půjde vložit list s Integerem a naopak, podobně je to i pro ostatní datové typy, které zmíněné editor
     * příkazů podporuje
     *
     * @param list
     *         - list hodnot, kterým se naplnit kolekce - list (list v této třídě)
     */
    @SuppressWarnings("unchecked")
    public void setList(final List<?> list) {
        this.list = (List<T>) list;
    }
}