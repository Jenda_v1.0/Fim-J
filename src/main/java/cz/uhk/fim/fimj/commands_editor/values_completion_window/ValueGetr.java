package cz.uhk.fim.fimj.commands_editor.values_completion_window;

import cz.uhk.fim.fimj.commands_editor.Variable;
import cz.uhk.fim.fimj.commands_editor.VariableArray;
import cz.uhk.fim.fimj.commands_editor.VariableList;

import java.util.List;
import java.util.Map;

/**
 * Rozhraní obsahující hlavičky metod, které slouží pro získání hodnot, které se posléze zpracují do textové podoby a
 * vloží se do okna pro automatické doplňování hodnot.
 * <p>
 * (Note: Rozhraní má pouze jednu implementaci a není nezbyté toto rozhraní. Pouze pro lepší přehlednost a potenciální
 * znovu použitelnost někde jindě - možná v budoucnu. Ale stačilo by třeba zde, nebo v "klasické třídě" mít pouze
 * statické metody)
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 28.07.2018 1:49
 */

interface ValueGetr {

    /**
     * Získání / načtení zkompilovaných tříd (souborů .class) v diagramu tříd.
     *
     * @return načtené soubory (.class) z adresáře bin v otevřeném projektu. Jedná se pouze o zkompilované třídy, které
     * se nachází v diagramu tříd, ostatní budou ignorovány (budou li nějaké jiné v adresáři bin).
     */
    List<Class<?>> getClassesInClassDiagram();


    /**
     * Zísání veškerých instancí, které se nachází v diagramu instancí.
     * <p>
     * Jedná se o instance tříd, které se nachází v diagramu tříd.
     *
     * @return - instance tříd z digramu tříd, instance se nachází v diagramu instancí. Jedná se o mapu, kde klíč je
     * reference a hodnota konkrétní instance.
     */
    Map<String, Object> getInstancesInInstanceDiagram();


    /**
     * Získání veškerých proměnných "běžného" datového typu, které uživatel vytvořil v editoru příkazů.
     *
     * @return Mapu, která bude obsahovat veškeré proměnné například typu int, boolean, String apod. (ne datové
     * struktury), které uživatel vytvořil v editoru příkazů. Klíč je reference na proměnnou, hodnota je konkrétní
     * objekt obsahující příslušnou uživatelem vytvořenouproměnnou.
     */
    Map<String, Variable> getClassicVariablesInCommandEditor();


    /**
     * Získání veškerých proměnných typu List, které uživatel vytvořil v editoru příkazů.
     *
     * @return Mapu, která bude obsahovat veškeré proměnné typu List, které uživatel vytvořil v editoru příkazů. Klíč je
     * reference na proměnnou typu List, hodnota je konkrétní objekt obsahující List.
     */
    Map<String, VariableList<?>> getListVariablesInCommandEditor();


    /**
     * Získání veškerých proměnných typu jednorozměrné pole, které uživatel vytvořil v editoru příkazů.
     *
     * @return Mapu, která bude obsahovat veškeré proměnné typu jednorozměné pole, které uživatel vytvořil v editoru
     * příkazů. Klíč je reference na proměnnou jednorozměrné pole, hodnota je konkrétní objekt obsahující uživatelem
     * vytvořené pole.
     */
    Map<String, VariableArray<?>> getArrayVariablesInCommandEditor();
}
