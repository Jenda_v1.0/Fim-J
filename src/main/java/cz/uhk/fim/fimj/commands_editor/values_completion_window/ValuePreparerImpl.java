package cz.uhk.fim.fimj.commands_editor.values_completion_window;

import cz.uhk.fim.fimj.commands_editor.Editor;
import cz.uhk.fim.fimj.commands_editor.Variable;
import cz.uhk.fim.fimj.commands_editor.VariableArray;
import cz.uhk.fim.fimj.commands_editor.VariableList;
import cz.uhk.fim.fimj.reflection_support.ParameterToText;
import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;
import cz.uhk.fim.fimj.instances.Instances;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Třída, kde jsou implementovány metody pro přípravu hodnot do okna pro automatické dokončování / doplňování.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 28.07.2018 9:44
 */

class ValuePreparerImpl implements ValuePreparer {

    @Override
    public Set<ValueCompletionInfo> getClasses(final List<Class<?>> classList) {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        classList.forEach(c -> valueCompletionInfoSet.add(new ValueCompletionInfo(c.getSimpleName(), c.getSimpleName
                (), c.getName())));

        return valueCompletionInfoSet;
    }


    @Override
    public Set<ValueCompletionInfo> getClassesWithPackages(final List<Class<?>> classList) {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        classList.forEach(c -> valueCompletionInfoSet.add(new ValueCompletionInfo(c.getName(), c.getName(), c
                .getName())));

        return valueCompletionInfoSet;
    }


    @Override
    public Set<ValueCompletionInfo> getConstructors(final List<Class<?>> classList) {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        classList.forEach(clazz -> Arrays.stream(clazz.getDeclaredConstructors()).forEach(c -> {
            // Pouze parametry konstruktoru v závorce:
            final String classHeader = ReflectionHelper.getParametersInText(c.getParameters());

            // Před pomlčkou uživatel uvidí pouze new a název třídy:
            final String input = "new " + clazz.getSimpleName() + classHeader;
            // Vkládat se bude název třídy s balíčky:
            final String replacement = "new " + clazz.getName() + classHeader;
            // Za pomlčkou bude název třídy s balíčky, kde se třída nachází:
            final String shortDesc = c.getName();

            valueCompletionInfoSet.add(new ValueCompletionInfo(input, replacement, shortDesc,
                    replacement));
        }));

        return valueCompletionInfoSet;
    }


    @Override
    public Set<ValueCompletionInfo> getCompleteConstructors(final List<Class<?>> classList) {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        classList.forEach(clazz -> Arrays.stream(clazz.getDeclaredConstructors()).forEach(c -> {
            // Název třídy (bez balíčků) s parametry konstruktoru:
            final String classHeader = ReflectionHelper.getMethodNameAndParametersInText(clazz.getName(),
                    c.getParameters());

            // Získání nepoužívaného indexu pro název reference - ref1, ref2, ref3, ...
            final String referenceName = ValuePreparerHelper.getUnusedReferenceName(clazz.getSimpleName(), Instances
                    .getListOfVariablesOfClassInstances());
            final String replacement = clazz.getName() + " " + referenceName + " = new " + classHeader + ";";

            // Pevný popisek s informací, že se jedná o deklaraci a inicializaci:
            final String shortDesc = "Declaration with initialization";

            valueCompletionInfoSet.add(new ValueCompletionInfo(classHeader, replacement, shortDesc,
                    replacement));
        }));

        return valueCompletionInfoSet;
    }


    @Override
    public Set<ValueCompletionInfo> getInstanceReferences(final Map<String, Object> instanceReferences) {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        instanceReferences.forEach((key, value) -> {
            final String shortDesc = value.getClass().getName();

            valueCompletionInfoSet.add(new ValueCompletionInfo(key, key, shortDesc));
        });

        /*
         * Přidání metod z třídy Object - ty lze zavolat nad instancí.
         *
         * Ale tyto metody se přidají pouze v případě, že se do okna přidá alespoň jedna instance, jinak by ty metody
         * nemohly být nad žádnou instancí zavolány.
         */
        if (!valueCompletionInfoSet.isEmpty())
            valueCompletionInfoSet.addAll(ValuePreparerHelper.getPublicProtectedMethodsFromObject());

        return valueCompletionInfoSet;
    }


    @Override
    public Set<ValueCompletionInfo> getCommandEditorVariables(final Map<String, Variable> variables) {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        variables.forEach((key, value) -> {
            // Pouze název proměnné s datovým typem a hodnotou:
            valueCompletionInfoSet.add(new ValueCompletionInfo(key, key, value.getDataTypeOfVariable().getSimpleName(),
                    String.valueOf(value.getObjValue())));


            /*
             * Pokud se jedná o proměnnou číselného datového typu a není final, tak přidám i inkrementaci a
             * dekrementaci v postfixové a prefixové formě
             */
            if (!value.isFinal() && ValuePreparerHelper.isNumberDataType(value.getDataTypeOfVariable()))
                valueCompletionInfoSet.addAll(ValuePreparerHelper.getIncrementDecrementValues(key, value
                        .getDataTypeOfVariable().getName(), String.valueOf(value.getObjValue())));
        });

        return valueCompletionInfoSet;
    }


    @Override
    public Set<ValueCompletionInfo> getCommandEditorArrayVariables(final Map<String, VariableArray<?>> arrayVariables) {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        arrayVariables.forEach((key, value) -> {
            final String replacement = key + "[0]";
            final String shortDesc = value.getTypeArray().getSimpleName() + "[]";
            final String summaryValue = Arrays.toString(value.getArray());

            // Pouze název proměnné:
            valueCompletionInfoSet.add(new ValueCompletionInfo(key, key, shortDesc, summaryValue));
            // Proměnná s hranatými závorkami pro přístup k položce:
            valueCompletionInfoSet.add(new ValueCompletionInfo(replacement, replacement, shortDesc, summaryValue));

            // Inkrementace a dekrementace položky v poli v případě, že se jedná o pole číselného datového typu
            if (ValuePreparerHelper.isNumberDataType(value.getTypeArray()))
                valueCompletionInfoSet.addAll(ValuePreparerHelper.getIncrementDecrementValues(replacement, shortDesc
                        , summaryValue));
        });

        return valueCompletionInfoSet;
    }


    @Override
    public Set<ValueCompletionInfo> getCommandEditorListVariables(final Map<String, VariableList<?>> listVariables) {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        listVariables.forEach((key, value) -> {
            final String dataTypeOfList = value.getClassType().getSimpleName();

            // Datový typ bude v syntaxi: java.util.List<dataType>:
            final String shortDesc = value.getList().getClass().getSimpleName() + "<" + dataTypeOfList + ">";

            // Hodnoty v listu v podobě textu:
            final String summaryValue = value.toString();

            valueCompletionInfoSet.add(new ValueCompletionInfo(key, key, shortDesc, summaryValue));


            // Doplnění metod, které lze volat nad listem a jsou podporovány v editoru příkazů:

            // Získání výchozí hodnoty pro vybrané datové typy:
            final String defaultValue = ValuePreparerHelper.getDefaultValue(value.getClassType()).toString();

            final String add = key + ".add(" + defaultValue + ")";
            valueCompletionInfoSet.add(new ValueCompletionInfo(add, add, "boolean", summaryValue));

            final String addToIndex = key + ".add(0, " + defaultValue + ")";
            valueCompletionInfoSet.add(new ValueCompletionInfo(addToIndex, addToIndex, "void", summaryValue));

            final String clear = key + ".clear()";
            valueCompletionInfoSet.add(new ValueCompletionInfo(clear, clear, "void", summaryValue));

            final String get = key + ".get(0)";
            valueCompletionInfoSet.add(new ValueCompletionInfo(get, get, dataTypeOfList, summaryValue));

            final String setValueAtIndex = key + ".set(0, " + defaultValue + ")";
            valueCompletionInfoSet.add(new ValueCompletionInfo(setValueAtIndex, setValueAtIndex, dataTypeOfList,
                    summaryValue));

            final String listToArray = key + ".toArray()";
            valueCompletionInfoSet.add(new ValueCompletionInfo(listToArray, listToArray, "Object[]", summaryValue));

            final String size = key + ".size()";
            valueCompletionInfoSet.add(new ValueCompletionInfo(size, size, "int", summaryValue));

            final String remove = key + ".remove(0)";
            valueCompletionInfoSet.add(new ValueCompletionInfo(remove, remove, dataTypeOfList, summaryValue));
        });

        return valueCompletionInfoSet;
    }


    @Override
    public Set<ValueCompletionInfo> getMethodsInInstances(final Map<String, Object> instancesInInstanceDiagram) {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        instancesInInstanceDiagram.forEach((key, value) -> {
            // Získání veřejných a chráněných nestatických metod z instance value:
            final List<Method> methodList = ValuePreparerHelper.getPublicProtectedMethod(value.getClass(), false,
                    new ArrayList<>());

            // Převedení metod do požadované syntaxe:
            methodList.forEach(method -> {
                // Název metody s parametry:
                final String methodHeader = ReflectionHelper.getMethodNameAndParametersInText(method.getName(),
                        method.getParameters());
                // Reference na instanci a za tečkou název metody s parametry:
                final String input = key + "." + methodHeader;
                // Návratový datový typ metody:
                final String shortDesc = ParameterToText.getMethodReturnTypeInText(method, false);
                // Reference a v závorce název třídy s balíčky, ze které byla instance vytvořena:
                final String summary = key + " (" + value.getClass().getName() + ")";

                valueCompletionInfoSet.add(new ValueCompletionInfo(input, input, shortDesc, summary));


                /*
                 * Zde je třeba doplnit i text samotné metody. Protože, v tom okně se filtrují hodnoty v pořádku, ale
                 * když se zadá desetinná tečka, tak od této tečky se filtrují hodnoty zvlášť v tom okně (jakoby celý
                 * text znovu dle toho okna).
                 *
                 * Takže, když se zadá například syntaxe: reference.methodName, tak se v okně budou filtrovat hodnoty
                 * v pořádku do doby než se zadá tečka pro oddělení reference a metody, poté
                 * už se nebude nic zobrazovat, jako by se nic nenašlo, proto se budou do setu přidávat pouze takové
                 * metody, aby nevznikly duplicity. Díky tomu
                 * se po zadání tečky pro oddělení reference a metody budou správně zobrazovat nejprve reference a
                 * pak pouze správné metody (z hlediska syntaxe).
                 */
                valueCompletionInfoSet.add(new ValueCompletionInfo(methodHeader, methodHeader, shortDesc, summary));
            });
        });

        return valueCompletionInfoSet;
    }


    @Override
    public Set<ValueCompletionInfo> getStaticMethodsInClasses(final List<Class<?>> classList) {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        classList.forEach(c -> {
            final List<Method> methodList = ValuePreparerHelper.getPublicProtectedMethod(c, true, new ArrayList<>());

            methodList.forEach(m -> {
                // Název metody s parametry:
                final String methodHeader = ReflectionHelper.getMethodNameAndParametersInText(m.getName(),
                        m.getParameters());
                // Název třídy s balíčky, kde se metoda nachází a text té metody - název a její parametry:
                final String input = m.getDeclaringClass().getName() + "." + methodHeader;
                // Návratový datový typ metody:
                final String shortDesc = ParameterToText.getMethodReturnTypeInText(m, false);

                valueCompletionInfoSet.add(new ValueCompletionInfo(input, input, shortDesc));


                /*
                 * Třída s balíčky, kde se nachází metoda - aby se vědělo, v jaké třídě se ta
                 * metoda nachází, pro případ, že by bylo více statických metod stejného názvu:
                 */
                final String summary = m.getDeclaringClass().getName();

                /*
                 * Zde je třeba doplnit pouze název metody, protože, když uživatel zadává / píše balíčky a název
                 * třídy, pak
                 * se za desetinnou tečkou již nebude nabízet "celá" syntaxe pro zavolání statické metody, proto je
                 * třeba, aby
                 * se zadalí zvláště ještě metody, které by měly být na výběr.
                 */
                valueCompletionInfoSet.add(new ValueCompletionInfo(methodHeader, methodHeader, shortDesc, summary));
            });
        });

        return valueCompletionInfoSet;
    }


    @Override
    public Set<ValueCompletionInfo> getStaticMethodsInStaticInnerClasses(final List<Class<?>> classList) {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        classList.forEach(c -> {
            // Získáme si veškeré vnitřní třídy v třídě c:
            final List<Class<?>> innerClassList =
                    ValuePreparerHelper.getPublicProtectedStaticInnerClasses(c.getDeclaredClasses(),
                            new ArrayList<>());

            innerClassList.forEach(ic -> {
                /*
                 * Získání veškerých veřejných a chráněných statických metod nacházející se ve vnitřních třídách
                 * třídy c.
                 */
                final List<Method> methodList = ValuePreparerHelper.getPublicProtectedMethod(ic, true,
                        new ArrayList<>());

                // Pro každou výše získanou metodu se sestaví syntaxe pro zobrazení v okně pro doplňování příkazů:
                methodList.forEach(m -> {
                    // Název metody s parametry:
                    final String methodHeader = ReflectionHelper.getMethodNameAndParametersInText(m.getName(),
                            m.getParameters());
                    /*
                     * Název třídy s balíčky, kde se metoda nachází a text té metody - název a její parametry. Ale
                     * vnitří třídy se zobrazují s dolarem, proto tento textu bude v tom druhém okně s informacemi.
                     */
                    final String summary = m.getDeclaringClass().getName() + "." + methodHeader;
                    /*
                     * Ve vkládaném textu a v textu, který se zobrazí před pomlčkou (aby to nemátlo) nesmí být
                     * dolary, ale místo nich desetinné tečky:
                     */
                    final String input = summary.replace("$", ".");
                    // Návratový datový typ metody:
                    final String shortDesc = ParameterToText.getMethodReturnTypeInText(m, false);

                    valueCompletionInfoSet.add(new ValueCompletionInfo(input, input, shortDesc, summary));


                    /*
                     * Třída s balíčky, kde se nachází metoda - aby se vědělo, v jaké třídě se ta
                     * metoda nachází, pro případ, že by bylo více statických metod stejného názvu:
                     */
                    final String summary2 = m.getDeclaringClass().getName();

                    /*
                     * Zde je třeba doplnit pouze název metody, protože, když uživatel zadává / píše balíčky a název
                     * třídy, pak
                     * se za desetinnou tečkou již nebude nabízet "celá" syntaxe pro zavolání statické metody, proto je
                     * třeba, aby
                     * se zadalí zvláště ještě metody, které by měly být na výběr.
                     */
                    valueCompletionInfoSet.add(new ValueCompletionInfo(methodHeader, methodHeader, shortDesc,
                            summary2));
                });
            });
        });

        return valueCompletionInfoSet;
    }


    @Override
    public Set<ValueCompletionInfo> getVariablesFromInstances(final Map<String, Object> instances) {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        instances.forEach((key, value) -> {
            final List<Field> fieldList = ValuePreparerHelper.getPublicProtectedFields(value);

            fieldList.forEach(f -> {
                // Reference . název_proměnné
                final String input = key + "." + f.getName();
                // Datový typ proměnné:
                final String shortDesc = ParameterToText.getFieldInText(f, false);
                // Získání hodnoty proměnné:
                final Object objValue = ReflectionHelper.getValueFromField(f, value);
                // Sestavení textu z hodnoty proměnné:
                final String summary = FieldValueSyntaxCreator.getValueOfFieldInText(objValue);

                // Syntaxe: referenceName.variable - dataType -> value
                valueCompletionInfoSet.add(new ValueCompletionInfo(input, input, shortDesc, summary));


                // Název proměnné:
                final String input2 = f.getName();
                // Hodnota proměnná a za ní v závorce reference na instanci, kde se proměnná nachází:
                final String summary2 = summary + " (" + key + ")";

                // Syntaxe: variable - dataType -> value (refereceName)
                valueCompletionInfoSet.add(new ValueCompletionInfo(input2, input2, shortDesc, summary2));


                /*
                 * Zjištění, zda se jedná o "klasickou" proměnnou, která je číselného datového typu nebo jedno
                 * rozměrné nebo dvou rozměrné pole
                 * také čáselného datového typu, pokud ano, pak se přidají hodnoty pro syntaxi prefixové a postfixové
                 * inkrementace a dekrementace:
                 */
                valueCompletionInfoSet.addAll(ValuePreparerHelper.getSyntaxForIncrementDecrementValue(f.getModifiers(), f.getType(), input, shortDesc, summary));


                /*
                 * V případě, že se jedná o proměnnou, která je datového typu, který je podporován v editoru příkazů a
                 * je tak povoleno,
                 * tak se přidá syntaxe pro vytvoření proměnné v editoru příkazů, do které se převede / vloží hodnota
                 * v proměnné f.
                 */
                if (Editor.isAddSyntaxForFillingTheVariable() && ValuePreparerHelper.isDataTypeForCreateVariable(f)) {
                    final String unusedReferenceName = ValuePreparerHelper.getUnusedReferenceName(f.getName(), Instances
                            .getListOfVariablesOfClassInstances());

                    final String replacementFill;

                    /*
                     * Zde se otestuje, zda se jedná o "klasickou" proměnnou základního datového typu jazyka Java.
                     * Pokud ano,
                     * tak se vytvoři syntaxe pro vytvoření nové proměnné v editoru příkazů, do které se ta hodnota z
                     * proměnné vloží.
                     */
                    if ((replacementFill = ValuePreparerHelper.prepareVarDeclarationForEditor(f, key,
                            unusedReferenceName)) != null) {

                        final String inputFill = key + "." + f.getName();

                        final String shortDescFill = "Create a '" + unusedReferenceName + "' variable in the editor";

                        valueCompletionInfoSet.add(new ValueCompletionInfo(inputFill, replacementFill, shortDescFill));


                        /*
                         * Zde je vhodné přidat ještě syntaxi, kdy se začíná pouze proměnnou, protože, když uživatel
                         * zadá
                         * referenci na instanci a tečku, tak se v okně filtrují hodnoty od začátku, takže když začne
                         * uživatel v této fází psát za tečku název proměnné, už by se nenabízela možnost pro deklaraci
                         * proměnné.
                         */
                        final String inputFill2 = f.getName();
                        valueCompletionInfoSet.add(new ValueCompletionInfo(inputFill2, replacementFill, shortDescFill));
                    }

                    /*
                     * Zde se zjistí, zda existuje proměná vytvořená uživatelem v editoru příkazů, pokud ano, přidá
                     * se možnost pro její naplnění.
                     *
                     * Toto platí pro "klasické" proměnné, jednorozměrné pole a list. Protože jednorozměrné pole a
                     * list v editoru příkazů nelze vytvořit tak, že
                     * se do deklarace pole v editoru předá hodnota (pole) z té proměnné. proto je zde třeba řešit
                     * pouze naplnění proměnných typu list a pole.
                     */
                    valueCompletionInfoSet.addAll(ValuePreparerHelper.prepareSyntaxForFillVariableInEditor(f, key));
                }
            });
        });

        return valueCompletionInfoSet;
    }


    @Override
    public Set<ValueCompletionInfo> getExamplesDeclaration(final List<Class<?>> classList,
                                                           final Map<String, Object> instancesInInstanceDiagram) {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        // Ukázky deklarace proměnných:
        valueCompletionInfoSet.addAll(ValuePreparerHelper.getExamplesOfVariableDeclaration());

        // Ukázky deklarace Listu:
        valueCompletionInfoSet.addAll(ValuePreparerHelper.getExamplesOfListDeclaration());

        // Ukázky deklarace jednorozměrného pole:
        valueCompletionInfoSet.addAll(ValuePreparerHelper.getExamplesOfArrayDeclaration());

        /*
         * Ukázky deklarace jednorozměrného pole tak, že se pole získá z návratové hodnoty metody. Nestatické metody
         * se získají z instancí v diagramu instancí. Statické metody se získají z tříd v diagramu tříd a vnitřních
         * tříd v třídách v diagramu tříd.
         *
         * Metody musí být veřejné nebo chráněné. To samé platí pro vnitřní třídy.
         */
        valueCompletionInfoSet.addAll(ValuePreparerHelper.getDeclarationArrayFromMethod(classList,
                instancesInInstanceDiagram));

        // Ukázky podmínek:
        valueCompletionInfoSet.addAll(ValuePreparerHelper.getConditionExamples());

        // Ukázky matematických výrazů / výpočtů:
        valueCompletionInfoSet.addAll(ValuePreparerHelper.getMathExamples());

        return valueCompletionInfoSet;
    }


    @Override
    public Set<ValueCompletionInfo> getDefaultMethodsInEditor() {
        final Set<ValueCompletionInfo> valueCompletionInfoSet = new HashSet<>();

        // Restart aplikace za 3 vteřiny:
        final String inputRestart = "restart();";
        final String shortDescDescRestart = "Restarts the application";
        final String summaryRestart = "Restarts the application in 3 seconds.";
        valueCompletionInfoSet.add(new ValueCompletionInfo(inputRestart, inputRestart, shortDescDescRestart,
                summaryRestart));

        // Restart aplikace za 59 vteřin:
        final String inputRestart2 = "restart(59);";
        final String summaryRestart2 = "Restarts the application in 59 seconds.";
        valueCompletionInfoSet.add(new ValueCompletionInfo(inputRestart2, inputRestart2, shortDescDescRestart,
                summaryRestart2));


        // Ukončení aplikace za 3 vteřiny:
        final String inputExit = "exit();";
        final String shortDescExit = "Closes the application";
        final String summaryExit = "Closes the application in 3 seconds.";
        valueCompletionInfoSet.add(new ValueCompletionInfo(inputExit, inputExit, shortDescExit, summaryExit));

        // Ukončení aplikace za 59 vteřin:
        final String inputExit2 = "exit(59);";
        final String summaryExit2 = "Closes the application in 59 seconds.";
        valueCompletionInfoSet.add(new ValueCompletionInfo(inputExit2, inputExit2, shortDescExit, summaryExit2));

        // Ukončení aplikace za 3 vteřiny:
        final String inputExit3 = "System.exit(0);";
        valueCompletionInfoSet.add(new ValueCompletionInfo(inputExit3, inputExit3, shortDescExit, summaryExit));


        // Zrušení odpočítávání do ukončení nebo restartu aplikace:
        final String inputCancelCountdown = "cancelCountdown();";
        final String shortDescCancelCountdown = "Cancel the countdown";
        final String summaryCancelCountdown = "Cancels the countdown to reboot or exit the application.";
        valueCompletionInfoSet.add(new ValueCompletionInfo(inputCancelCountdown, inputCancelCountdown,
                shortDescCancelCountdown, summaryCancelCountdown));


        // Výpis nějaké hodnoty:
        final String inputPrint = "print();";
        final String shortDescPrint = "Print value";
        final String summaryPrint = "Prints the value to the output editor.";
        valueCompletionInfoSet.add(new ValueCompletionInfo(inputPrint, inputPrint, shortDescPrint, summaryPrint));

        // Výpis nějaké hodnoty s ukázkou - parametrem:
        final String inputPrintVal = "print(\"example\");";
        valueCompletionInfoSet.add(new ValueCompletionInfo(inputPrintVal, inputPrintVal, shortDescPrint, summaryPrint));


        // Výpis veškerých dostupných příkazů v editoru příkazů (informace) do editoru výstupů:
        final String inputPrintCommands = "printCommands();";
        final String shortPrintCommands = "Prints an overview of commands";
        final String summaryPrintCommands = "Prints an overview of the commands available in the command editor.";
        valueCompletionInfoSet.add(new ValueCompletionInfo(inputPrintCommands, inputPrintCommands, shortPrintCommands
                , summaryPrintCommands));


        // Výpis veškerých proměnných, které uživatel vytvořil v editoru příkazů:
        final String inputPrintVariables = "printTempVariables();";
        final String shortPrintVariables = "Prints the created variables.";
        final String summaryPrintVariables = "Prints the created variables that were created in the command editor.";
        valueCompletionInfoSet.add(new ValueCompletionInfo(inputPrintVariables, inputPrintVariables,
                shortPrintVariables, summaryPrintVariables));

        return valueCompletionInfoSet;
    }
}
