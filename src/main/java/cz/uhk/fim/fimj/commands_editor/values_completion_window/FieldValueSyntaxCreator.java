package cz.uhk.fim.fimj.commands_editor.values_completion_window;

import cz.uhk.fim.fimj.commands_editor.Editor;
import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;
import cz.uhk.fim.fimj.instances.Instances;

import java.lang.reflect.Array;
import java.util.List;

/**
 * Třída slouží pro sestavení textu z hodnoty nějaké proměnné (získané z nějaké instance).
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 01.08.2018 18:10
 */

public final class FieldValueSyntaxCreator {

    /**
     * Komponenta pro sestavení textu z hodnot pole.
     * <p>
     * (Převedení hodnoty N - rozměrného pole do textové podoby.)
     */
    private static final StringBuilder arrayStringBuilder = new StringBuilder();

    /**
     * Textová proměnná pro text "referenční proměnná" v uživatelem zvoleném jazyce.
     */
    private static String txtReferenceVar;


    private FieldValueSyntaxCreator() {
    }

    /**
     * Získání / převedení hodnoty proměnné (v proměnné objValue) do textové podoby.
     *
     * @param objValue
     *         - hodnota proměnná, která se má převést do textové podoby.
     *
     * @return text, který bude obsahovat hodnotu proměnné (v textové podobě). Dále v případě, že se jedná o hodnotu,
     * která je reference na instanci v diagramu instancí, doplní se i referenční proměnná (pokud je tak nastaveno.
     */
    static String getValueOfFieldInText(final Object objValue) {
        if (objValue == null)
            return "null";

        return buildTextFromValue(objValue);
    }


    /**
     * Sestavení textu z proměnné objValue.
     * <p>
     * Zde se testuje, zda se jedná o pole, pokud ano, sestaví se z něj text a případně přidají referenční proměnné k
     * jednotlivým položkám / referencím v poli.
     * <p>
     * Dále se zjistí, zda se jendá o kolekci, pokud ano, proiteruje se a pro každou položku se také zjistí, zda se
     * jedná o instanci a pokud je povoleno, přidá se referenční proměnná k té referenci / instanci v listu.
     * <p>
     * Na konec se jedná o konkrétní hodnoty, případně mapu, ale v té se netestují reference na instance (nedoplňěno.
     * Pro tyto "konkrétní hodnoty" se zjistí, zda se jedná o instanci v diagramu instancí, případně se přidají za tyto
     * hodnoty referene na instance v diagramu instnací a vrátí se.
     *
     * @param objValue
     *         - hodnota z nějaké proměnné, která se má převést na text.
     *
     * @return hodnotu objValue v textové podobě, případně doplněno o reference na instance v diagramu instancí.
     */
    private static String buildTextFromValue(final Object objValue) {
        final StringBuilder valueStringBuilder = new StringBuilder();

        // Zda se jedná o pole:
        if (objValue.getClass().isArray()) {
            /*
             * Zde je třeba "sestavit" text tak, aby se prošli veškeré hodnoty v poli a pro každou hodnotu se
             * zjistilo, zda se
             * jedná o referenci na nějakou instanci, která se nachází v diagramu instancí.
             */
            valueStringBuilder.append("[");

            // Nejprve je třeba "vyprázdnit" builder, aby v něm nebyly předchozí texty:
            arrayStringBuilder.setLength(0);

            // Sestaví se text z pole:
            final String arrayText = getArrayValueInText(objValue);

            if (arrayText.endsWith(", "))
                valueStringBuilder.append(arrayText, 0, arrayText.length() - 2);

            else valueStringBuilder.append(arrayText);

            valueStringBuilder.append("]");

            return valueStringBuilder.toString();
        }


        else if (ReflectionHelper.isDataTypeOfList(objValue.getClass())) {
            /*
             * Pokud se nemají zobrazovat referenční proměnné u jednotlivých hodnot v instanci, tak stačí převést
             * list do stringu, zde už je vyřešen převod to listu do podoby textu.
             */
            if (!Editor.isShowReferenceVariablesInCompleteWindow())
                return objValue.toString();

            /*
             * Zde je třeba projít veškeré hodnoty v listu a pro každou hodnotu zjistit, zda se jedná o referenci na
             * instanci, která senachází v diagramu instancí.
             */
            return getCollectionValueInText(objValue);
        }




        /*
         * V případě, že se jedná o hodnotu typu String, tak se ten text bude zobrazovat ve dvojitých
         * uvozovkách.
         *
         * Například, když se bude jednat o prázdný text, tak aby se zobrazily prázdné uvozovky a ne prázdné pole.
         */
        else if (objValue.getClass().equals(String.class))
            return "\"" + objValue + "\"";


            /*
             * Zde se jedná o stejný případ, jako výše u textové proměnné. Tj. Pokud zde
             * bude proměnná typu char nebo Character, a ten znak bude nějaký bílý znak,
             * například mezera apod. Tak by to nebylo vidět, proto ten znak bude ještě "obalen"
             * jednoduchými uvozovkami.
             */
        else if (objValue.getClass().equals(Character.class) || objValue.getClass().equals(char.class))
            return "'" + objValue + "'";


        /*
         * Pokud se nejedná o proměnnou, která by splňovala výše uvedené podnmínky, pak se pouze otestuje, zda se
         * jedná o referenci na instanci, která se nachází v diagramu instancí. Pokud ano, za vrácenou hodnotu se
         * přidá text s referenční proměnnou. Pokud se nejedná o referenci, tak se vrátí pouze ta hodnota.
         */
        return getVariableInText(objValue);
    }


    /**
     * Získání hodnoty objValue v textové podobě. Pouze se v případě, že je tak nastaveno otestuje, zda je objValue
     * reference na nějakou instanci, která se nachází v diagramu instancí, pokud ano, pak se za vrácenou hodnotu přidá
     * ještě referenční proměnná té instance, jinak se vrátí pouze ta hodnota objValue v textu.
     *
     * @param objValue
     *         - hodnota, která se má vrátit v textu, případně se za ní přidá referenční proměnná (pokud je tak
     *         nastaveno)
     *
     * @return objValue v textu, za ní můžebýt referenční proměnná.
     */
    private static String getVariableInText(final Object objValue) {
        if (Editor.isShowReferenceVariablesInCompleteWindow()) {
            // Zda se jedná o instanci (pokud se vrátí reference):
            final String reference = Instances.getReferenceToInstanceFromCdInMap(objValue);

            if (reference != null)
                return objValue + " (" + txtReferenceVar + ": " + reference + ")";
        }

        /*
         * Important:
         *
         * Zde může nastat problém v případě, že se násleudjící objekt převede do textu, ale ten text bude null, tedy
         * v té třídě bude metoda toString, která vrací null, pak se zde ta null hodnota nepřevede do textu, ale
         * bude vrácena null hodnota a pak při vytváření objektu {@link ValueCompletionInfo} se atribut summary
         * nastaví na null, takže nebude zobrazena ani zobrazena hodnota null, ale text, která se po potvrzení z
         * toho okna vloži do editoru. Viz metoda cz.uhk.fim.fimj.commands_editor.values_completion_window
         * .ValuesCompletion#addValuesToAutoComppletionProvider(org.fife.ui.autocomplete.DefaultCompletionProvider,
         * java.util.Set)
         */
        return objValue.toString();
    }


    /**
     * Získání textu z listu / kolekce objCollection.
     * <p>
     * Jedná se o převední hodnot v listu objCollection do textové podoby, takže do hranatých závorek se vypíšou veškeré
     * hodnoty v listu a pokud je nějaká hodnota reference na instanci, která je reprezentována v diagramu instancí, dle
     * potřeby se k příslušné hodnotě přidá.
     *
     * @param objCollection
     *         - kolekce, která se má převést do podoby textu.
     *
     * @return text, který bude obsahovat veškeré hodnoty kolekce v textové podobě, případně přidané referenční proměnné
     * - pokud je tak nastaveno.
     */
    private static String getCollectionValueInText(final Object objCollection) {
        // Pro sestavení textu z kolekce:
        final StringBuilder collectionStringBuilder = new StringBuilder();

        collectionStringBuilder.append("[");

        /*
         * Převedení kolekce objCollection na list objektů místo "jen" objektu
         */
        @SuppressWarnings("unchecked") final List<Object> list = (List<Object>) objCollection;

        /*
         * Projdeme veškeré hodnoty v listu, pokud se jedná o null hodnotu, stačí přidat textu "null". Pokud v
         * ostatních případech stačí přidat
         * příslušnou hodnotu do collectionStringBuilder. Dále otestovat, zda se mají zobrazovat referenční proměnné
         * a pokud ano,
         * pak stačí zjistit, zda se jedná o referenci na nějakou instanci v diagramu instancí a případně přidat
         * název reference.
         */
        list.forEach(v -> {
            if (v == null) {
                collectionStringBuilder.append("null, ");
                return;
            }

            // Nejedná se o null hodnotu, tak stačí přidat do builderu:
            collectionStringBuilder.append(v);


            // Zda se jedná o instanci (pokud se vrátí reference):
            final String reference = Instances.getReferenceToInstanceFromCdInMap(v);

            if (reference == null) {
                collectionStringBuilder.append(", ");
                return;
            }

            // Přidání hodnoty (/ reference):
            collectionStringBuilder.append(" (").append(txtReferenceVar).append(": ").append(reference).append("), ");
        });

        if (collectionStringBuilder.toString().endsWith(", "))
            collectionStringBuilder.setLength(collectionStringBuilder.length() - 2);

        collectionStringBuilder.append("]");

        return collectionStringBuilder.toString();
    }


    /**
     * Získání textu z pole (objArray).
     * <p>
     * Proiteruje se celé pole a pro každou hodnotu se zjistí, zda se jedná o pole nebo ne, pokud ano, rekurzí se
     * pokračuje, pokud se nejedná o pole, jedná se o konkrétní hodnotu. Pro tu se otestuje, zda se jedná o referenci na
     * nějakou instnaci v diagramu instancí, pokud ano, a je tak nastaveno, doplní se za tuto hodnotu do závorek
     * referenční proměnná na tu instanci.
     * <p>
     * (Neřeší se případy, kdy je pole datového typu třeba nějaké datové struktury, například listu apod.)
     *
     * @param objArray
     *         - N - rozměrné pole, které se má převést do podoby textu.
     *
     * @return text, který bude obsahovt pole objArray v textové podobě, případně u referencí na instance i jejich
     * referenční proměnné.
     */
    private static String getArrayValueInText(final Object objArray) {
        // Velikosti pole:
        final int length = Array.getLength(objArray);

        for (int i = 0; i < length; i++) {
            // Hodnota na indexu v poli:
            final Object objValue = Array.get(objArray, i);

            if (objValue == null)
                arrayStringBuilder.append("null, ");


                // Pokud se jedná o pole, rekurzí se pokračuje:
            else if (objValue.getClass().isArray()) {
                arrayStringBuilder.append("[");

                // Rekurzi - pro další pole / dimenzi:
                getArrayValueInText(objValue);

                if (arrayStringBuilder.toString().endsWith(", "))
                    arrayStringBuilder.setLength(arrayStringBuilder.length() - 2);

                arrayStringBuilder.append("], ");
            }


            // Přidání iterované hodnoty v poli:
            else {
                arrayStringBuilder.append(objValue);
                arrayStringBuilder.append(", ");


                if (Editor.isShowReferenceVariablesInCompleteWindow()) {
                    // Zda se jedná o instanci (pokud se vrátí reference):
                    final String reference = Instances.getReferenceToInstanceFromCdInMap(objValue);

                    if (reference == null)
                        continue;

                    // U přidání hodnoty (v tomto případě reference) se přidala i čárka na konci, zde je třeba ji
                    // odebrat:
                    if (arrayStringBuilder.toString().endsWith(", "))// Není nezbytná, stačí odebrat poslední dva znaky
                        arrayStringBuilder.setLength(arrayStringBuilder.length() - 2);

                    arrayStringBuilder.append(" (").append(txtReferenceVar).append(": ").append(reference).append(")," +
                            " ");
                }
            }
        }

        return arrayStringBuilder.toString();
    }


    /**
     * Setr na proměnnou, která obsahuje text "referenční proměnná" v uživatelem zvoleném jazyce. Jedná se o text, která
     * je zobrazen u hodnotu proměnných v okně pro automaticé doplňování / dokončování příkazů v editoru příkazů v
     * případě, že se jedná o referenci na instanci, která senachází v diagramu instancí.
     *
     * @param txtReferenceVar
     *         Výše uvedený text ve zvoleném jazyce.
     */
    public static void setTxtReferenceVar(final String txtReferenceVar) {
        FieldValueSyntaxCreator.txtReferenceVar = txtReferenceVar;
    }
}
