package cz.uhk.fim.fimj.commands_editor.history.complete_history;

import cz.uhk.fim.fimj.commands_editor.history.HistoryCompletion;

import javax.swing.*;

/**
 * Okno, které obsahuje historii uživatelem zadaných příkazů do editoru příkazů.
 * <p>
 * Okno se otevře po stisknutí klávesové zkratky: "Ctrl + Shift + H".
 *
 * <i>Tuto historii nelze smazat. Ukládají se veškeré příkazy, které uživatel zadal od spuštění aplikace. Je to z tho
 * důvodu, že by mohlo přijít vhod, když uživatel otevře jiný projekt, tak se podívat i na příkazy, které zadal v tom
 * předchozím projektu. Například pro ukázku rozdílů apod.</i>
 *
 * <i>V této historii se nachází veškeré uživatelem zadané příkazy v editoru příkazů od spuštění do ukončení
 * aplikace.</i>
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 22.09.2018 22:35
 */

public class CompleteHistoryCompletion extends HistoryCompletion {

    /**
     * Klávesová zkratka, která otevře okno s historií.
     */
    private static final KeyStroke TRIGGER_KEY = KeyStroke.getKeyStroke("control shift H");


    public CompleteHistoryCompletion(final boolean addIndexes) {
        super(TRIGGER_KEY, addIndexes);
    }


    @Override
    public void addCompletion(final String command, final String result) {
        addCompletionToProvider(command, result);
    }
}