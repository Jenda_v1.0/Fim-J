package cz.uhk.fim.fimj.commands_editor;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.Element;

/**
 * Tato třída slouží pro to, aby když uživatel v CommandEditoru zadá příkaz a stiskne Enter, tak se stane vše co je nad
 * tímto řádkem needitovatelné (pomocí této třídy)
 * <p>
 * Zdroj jsem našel na následujícím odkazu, (+ menší úprava) http://stackoverflow
 * .com/questions/10030477/make-parts-of-a-jtextarea-non-editable-not-the-whole-jtextarea
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class NonEditableLineDocumentFilter extends DocumentFilter {

    private static final String PROMPT = ">";


    // Metoda, která se zavolá před vložením textu:
    @Override
    public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr)
            throws BadLocationException {
        if (string != null)
            replace(fb, offset, 0, string, attr);
    }


    // Metoda, která se zavolá před odebráním:
    @Override
    public void remove(FilterBypass fb, int offset, int length) throws BadLocationException {
        replace(fb, offset, length, "", null);
    }


    // Metoda, která se zavolá před editací textu:
    @Override
    public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs)
            throws BadLocationException {

        final Document document = fb.getDocument();
        final Element root = document.getDefaultRootElement();
        final int count = root.getElementCount();
        final int index = root.getElementIndex(offset);

        final Element cur = root.getElement(index);
        final int promptPosition = cur.getStartOffset() + PROMPT.length();

        if (index == count - 1 && offset - promptPosition >= 0) {
            if (text.endsWith("\n"))
                text = "\n" + PROMPT + " ";

            fb.replace(offset, length, text, attrs);
        }
    }
}