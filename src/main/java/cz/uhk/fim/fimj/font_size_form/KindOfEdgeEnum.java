package cz.uhk.fim.fimj.font_size_form;

/**
 * Tento výčet obsahuje hodnoty, které značí některé z typů hran reprezentující vztahy, které se mohou v diagramu tříd
 * vyskytovat.
 * <p>
 * Jde o to, že když uživatel změní velikosti písma pomocí dialogu pro "rychlou" změnu velikosti písma, tak se musí
 * otestovat, zda nejsou nějaké duplicity, protože by se jinak nemohly rozeznat jednotlivé objekty v diagramu tříd (ani
 * instancí, ale to je pořešeno jinak).
 * <p>
 * Takže tento výčet obsahuje hodnoty, které reprezentují typy hran / vztahů, které se mohou v diagramu tříd vyskytovat
 * a zároveň je u nich třeba testovat duplicitní vlastnosti s jinými hranami.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public enum KindOfEdgeEnum {

    ASSOCIATION, AGGREGATION_1_1, AGGREGATION_1_N, EXTENDS, COMMENT
}
