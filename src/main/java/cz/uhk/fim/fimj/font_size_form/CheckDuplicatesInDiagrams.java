package cz.uhk.fim.fimj.font_size_form;

import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.JOptionPane;

import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.file.Constants;

/**
 * Tato třída obsahuje pouze metody, které slouží pro zjištění duplicit v diagramu tříd a v diagramu instancí.
 * <p>
 * Jde o to, že když uživatel nastaví napřiklad všechny velikosti písma / fontu pro všechny objekty v diagramu tříd nebo
 * v diagramu instancí stejné, a zároveň budou mít třeba buňky reprezentující třídu a komentář úplně stejné nastavení,
 * že se budou lišit jen ve velikosti písma, tak v případě, kdy uživatel nastaví stejné písmo se buňky reprezentující
 * třídu a komentřád nebudou nijak lišit, takže by se ani v diagramu tříd nerozeznaly.
 * <p>
 * Proto zde potřebuji metody, které po tom, co uživatel nastaví nějaké velikosti písma pro nějaké objekty, tak musím
 * hlídat, aby nebyly nějaké objekty úplně identické, pokud ano, tak se využije různé indexování pro velikosti písma,
 * aby se předešlo nesrovnalostem, kdy se nerozeznají jednotlivé objekty v diagramech.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class CheckDuplicatesInDiagrams implements CheckDuplicatesInDiagramsInterface {

	// Texty pro chybová hlášení:
	private static String txtJopSameEdgesErrorText, txtJopSameEdgesErrorTitle,

			txtAggregation_1_1, txtAggregation_1_N, txtAssociation, txtCommentEdge, txtInheritance, txtUnknown;
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param languageProperties
	 *            - objekt, který obsahuje texty pro tuto aplikaci ve zvoleném
	 *            jazyce.
	 */
	public CheckDuplicatesInDiagrams(final Properties languageProperties) {
		super();
		
		/*
		 * Note:
		 * Třída by mohla implementovat rozhraní pro metodu setLanguage, ale to už je
		 * detail, stejně by se volala zde v konstruktoru.
		 */
		
		if (languageProperties != null) {
			txtJopSameEdgesErrorText = languageProperties.getProperty("FSF_CDID_TXT_JopSameEdgesErrorText", Constants.FSF_CDID_TXT_JOP_SAME_EDGES_ERROR_TEXT);
			txtJopSameEdgesErrorTitle = languageProperties.getProperty("FSF_CDID_TXT_JopSameEdgesErrorTitle", Constants.FSF_CDID_TXT_JOP_SAME_EDGES_ERROR_TITLE);
			
			txtAggregation_1_1 = languageProperties.getProperty("FSF_CDID_TXT_Aggregation_1_1", Constants.FSF_CDID_TXT_AGGREGATION_1_1);
			txtAggregation_1_N = languageProperties.getProperty("FSF_CDID_TXT_Aggregation_1_N", Constants.FSF_CDID_TXT_AGGREGATION_1_N);
			txtAssociation = languageProperties.getProperty("FSF_CDID_TXT_Association", Constants.FSF_CDID_TXT_ASSOCIATION);
			txtCommentEdge = languageProperties.getProperty("FSF_CDID_TXT_CommentEdge", Constants.FSF_CDID_TXT_COMMENT_EDGE);
			txtInheritance = languageProperties.getProperty("FSF_CDID_TXT_Inheritance", Constants.FSF_CDID_TXT_INHERITANCE);
			txtUnknown = languageProperties.getProperty("FSF_CDID_TXT_Unknown", Constants.FSF_CDID_TXT_UNKNOWN);
		}
		
		else {
			txtJopSameEdgesErrorText = Constants.FSF_CDID_TXT_JOP_SAME_EDGES_ERROR_TEXT;
			txtJopSameEdgesErrorTitle = Constants.FSF_CDID_TXT_JOP_SAME_EDGES_ERROR_TITLE;
			
			txtAggregation_1_1 = Constants.FSF_CDID_TXT_AGGREGATION_1_1;
			txtAggregation_1_N = Constants.FSF_CDID_TXT_AGGREGATION_1_N;
			txtAssociation = Constants.FSF_CDID_TXT_ASSOCIATION;
			txtCommentEdge = Constants.FSF_CDID_TXT_COMMENT_EDGE;
			txtInheritance = Constants.FSF_CDID_TXT_INHERITANCE;
			txtUnknown = Constants.FSF_CDID_TXT_UNKNOWN;
		}
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Postup:
	 * 
	 * Načtu si aktuálně využívaný objekt Properties, který je aktuálně využíván pro
	 * zjišťování objektů v diagramu tříd nebo instancí a z tohoto objektu
	 * Properties si vezmu vlastnosti v případě diagramu tříd buňky reprezentující
	 * třídu a komentář nebo hrany asociace, agregace (obě), dědičnost a komentář a
	 * zjistím, zda jsou stejné, pokud ano, pak vyhodím oznámení uživateli a vrátím
	 * true.
	 * 
	 * Jde o to, že v dialogu nastavení aplikace si uživatel může nastavit vzhled
	 * objektů v diagramu tříd a intancí, a když si nastaví pro nějaké objekty úplně
	 * stejný vzhled, který se bude lišit pouze ve velikosti písma a uživatel pomocí
	 * dialogu pro "rychlou" změnu velikosti písma změní všechny ty velikosti písma
	 * na stejnou velikost, tak už se některé ty objekty nebudou v ničem lišit a pro
	 * aplikaci nebude možné tyto objekty rozlišit, navíc by se na to nepřišlo,
	 * protože by to nebyla chyba, resp. je to validní chyba. Proto zde, vždy když
	 * uživatel nastaví velikosti písma, tak musím otestovat, zda jsou ty objekty
	 * různé, resp. zda se každý objekt liší alespoň v jedné vlastnosti, aby je
	 * aplikace mohla rozeznat. Veškeré nastavení je uloženo právě v objektu
	 * Properties v diagramu tříd.
	 * 
	 * Když uživatel něco nastaví v dialogu nastavení, tak musí restartovat
	 * aplikaci, pak se načtou do toho diagramu tříd a instancí aktuální hodnoty a
	 * pokud změní velikosti písma, tak se zapíše jak do souboru ve workspace tak i
	 * do toho souboru v diagramu tříd, proto změny velikosti písma budou vždy
	 * aktuální a vždy sebude pracovat s aktuálním souborem (pro příslušnou relaci),
	 * tak stačí jen vytáhnout ty vlastnosti.
	 * 
	 * 
	 * 
	 * Note:
	 * Nemůžou se testovat objekty v diagramu, protože když budou mít stejné
	 * hodnoty, tak se ani tak nerozeznají, co je co.
	 */
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public boolean areClassesAndCommentsSameInCd(final Properties diagramProp) {		
		/*
		 * Tento případ by nikdy neměl nastat, ale kdyby ano, tak aby nedošlo k chybě,
		 * tak vrátím false, zde by ještě bylo možné otestovat výchozí hodnoty, ale o
		 * těch je jasné, že jsou jiné.
		 */
		if (diagramProp == null)
			return false;
		
		
		
		/*
		 * Zde otestuji, zda jsou vlastnosti pro buňku reprezentující třídu a buňku reprezentující komentář
		 * v diagramu tříd různé.
		 */
		
		
		// Zde otestuji, zda se liší velikost písma pro buňky reprezentující třídu a
		// komentář:
		
		// Hodnoty třídy:
		final int fontSizeClass = Integer.parseInt(diagramProp.getProperty("ClassFontSize", Integer.toString(Constants.CD_FONT_CLASS.getSize())));
		final int fontStyleClass = Integer.parseInt(diagramProp.getProperty("ClassFontType", Integer.toString(Constants.CD_FONT_CLASS.getStyle())));
		
		// Hodnoty komentáře:
		final int fontSizeComment = Integer.parseInt(diagramProp.getProperty("CommentFontSize", Integer.toString(Constants.CD_FONT_COMMENT.getSize())));
		final int fontStyleComment = Integer.parseInt(diagramProp.getProperty("CommentFontType", Integer.toString(Constants.CD_FONT_COMMENT.getStyle())));		
		
		/*
		 * Note:
		 * 
		 * Zde nemohu testovat "celé" fonty - jejich instance, protože každý bymohl mít
		 * jiný název.
		 */
		if (fontSizeClass != fontSizeComment)
			return false;
		
		if (fontStyleClass != fontStyleComment)
			return false;
		
		
		
		// Zarovnání písma a obrázku:
		final int verticalAlignmentClass = Integer.parseInt(diagramProp.getProperty("VerticalAlignment", String.valueOf(Constants.CD_VERTICAL_ALIGNMENT)));
		final int verticalAlignmentComment = Integer.parseInt(diagramProp.getProperty("CommentVerticalAlignment", String.valueOf(Constants.CD_COM_VERTICAL_ALIGNMENT)));
		
		if (verticalAlignmentClass != verticalAlignmentComment)
			return false;
		
		
		final int horizontalAlignmentClass = Integer.parseInt(diagramProp.getProperty("HorizontalAlignment", String.valueOf(Constants.CD_HORIZONTAL_ALIGNMENT)));
		final int horizontalAlignmentComment = Integer.parseInt(diagramProp.getProperty("CommentHorizontalAlignment", String.valueOf(Constants.CD_COM_HORIZONTAL_ALIGNMENT)));
		
		if (horizontalAlignmentClass != horizontalAlignmentComment)
			return false;
		
		
		// Barva písma:
		final String textColorClass = diagramProp.getProperty("ForegroundColor", Integer.toString(Constants.CD_TEXT_COLOR.getRGB()));
		final String textColorComment = diagramProp.getProperty("CommentForegroundColor", Integer.toString(Constants.CD_COM_FOREGROUND_COLOR.getRGB()));
		
		if (Integer.parseInt(textColorClass) != Integer.parseInt(textColorComment))
			return false;
		
		
		
		
		
		// Nyní si pro nastavení barvy pozadí musím načíst logickou proměnnou, dle které poznám, zda se má obarvit
		// pozadí buňky  jednou barvou nebo dvěmí:
		final boolean oneColorClass = Boolean.parseBoolean(diagramProp.getProperty("ClassOneBgColorBoolean", String.valueOf(Constants.CD_ONE_BG_COLOR_BOOLEAN)));
		final boolean oneBgColorComment = Boolean.parseBoolean(diagramProp.getProperty("CommentOneBgColorBoolean", String.valueOf(Constants.CD_COM_ONE_BG_COLOR_BOOLEAN)));
		
		// Pokud má jedna buňka jednu barvu a druhá dvě, tak se také nerovnají:
		if (oneColorClass != oneBgColorComment)
			return false;
		
		// Zde mají obě jednu nebo dvě barvy pozadí:
		if (oneColorClass) {
			final String bgColorClass = diagramProp.getProperty("ClassOneBackgroundColor", Integer.toString(Constants.CD_ONE_BG_COLOR.getRGB()));
			final String bgColorComment = diagramProp.getProperty("CommentOneBackgroundColor", Integer.toString(Constants.CD_COM_ONE_BG_COLOR.getRGB()));
			
			// Barva pozadí - levý horní roh a pravý dolní roh by byly stejně v případě jedné barvy pro pozadí:
			if (Integer.parseInt(bgColorClass) != Integer.parseInt(bgColorComment))
				return false;			
		}
		
		else {
			// Barva pozadí - levý horní roh:
			final String backgroundColorClass = diagramProp.getProperty("ClassBackgroundColor", Integer.toString(Constants.CD_CLASS_BACKGROUND_COLOR.getRGB()));
			final String backgroundColorComment = diagramProp.getProperty("CommentBackgroundColor", Integer.toString(Constants.CD_COM_BACKGROUND_COLOR.getRGB()));
			
			if (Integer.parseInt(backgroundColorClass) != Integer.parseInt(backgroundColorComment))
				return false;
			
			
			// Barva pozadí - pravý dolní roh:
			final String gradientColorClass = diagramProp.getProperty("GradientColor", Integer.toString(Constants.CD_GRADIENT_COLOR.getRGB()));
			final String gradientColorComment = diagramProp.getProperty("CommentGradientColor", Integer.toString(Constants.CD_COM_GRADIENT_COLOR.getRGB()));
			
			if (Integer.parseInt(gradientColorClass) != Integer.parseInt(gradientColorComment))
				return false;
		}
		
		
		
		
		
		// Nastavení neprůhlednosti buňky - aby nebyla průhledná
		final boolean opaqueClass = !Boolean.parseBoolean(diagramProp.getProperty("ClassCellOpaque", String.valueOf(Constants.CD_OPAQUE_CLASS_CELL)));
		final boolean opaqueComment = !Boolean.parseBoolean(diagramProp.getProperty("CommentCellOpaque", String.valueOf(Constants.CD_OPAQUE_COMMENT_CELL)));


		return opaqueClass == opaqueComment;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public boolean areEdgesInCdSame(final Properties diagramProp, final boolean writeDupliciteInfo) {
		/*
		 * Tato podmínka byměla být zbytečná a nikdy by neměla nastat, ale kdyby k tomu náhodou došlo,
		 * tak zde předejdu chybě, rovnou mohu vrátit false, protože by meli být využity výchozí hodnoty z Constants,
		 * a ty nejsou duplicitní, ale to už záleží na konkrétní chybě, ke které by moho dojít, ale žádná mě nenapdaá.
		 */
		if (GraphClass.KIND_OF_EDGE.getProperties() == null)
			return false;
		
		
		/*
		 * Získám si list s informacemi o hranách, které se mohou v diagramu tříd
		 * nacházet a je třeba u nich testovat duplicity.
		 */
		final List<EdgeOfClassDiagramInfo> edgesinfoList = getEdgesInfoFromProperties(diagramProp);
		
		
		/*
		 * Cyklus, který projde získané vlastnosti hran a otestuje, zda se v něm
		 * vyskytují nějaké duplicity, resp. nějaké hrany, které mají stejně nastavený
		 * design.
		 */
		for (final EdgeOfClassDiagramInfo e1 : edgesinfoList) {
			for (final EdgeOfClassDiagramInfo e2 : edgesinfoList) {
				/*
				 * nemohu testovat stejné hrany, podmínka by vždy selhala, resp. vždy by se
				 * našli duplicity.
				 */
				if (e1 == e2)
					continue;
				
				
				/*
				 * V případě, že je e1 hrana agregace 1 : 1 a e2 agregace 1 : N nebo obráceně
				 * (e1 = agregace 1 : N a e2 = agregace 1 : 1), pak taky nemusím (ale mohu)
				 * pokračovat, resp. tuto iteraci přeskočit, protože tyto dvě hrany / vztahy
				 * (agregace 1 : 1 a agregace 1 : N) se nikdy nebudou rovnat, i když budou mít
				 * stejná nastavení, protože každá z těch hran má jinou multiplicitu, agregace 1
				 * : N bude mít vždy1 : N na hraně v diagramu tříd a agregace 1 : 1 (asymetrická
				 * asocaice) bude mít vždy 1 : 1, proto není třeba je testovat, protože tyto dva
				 * vztahy / hrany nebudou nikdy stejné tak, aby se nerozeznali.
				 */
				if (!(e1.getKindOfEdge().equals(KindOfEdgeEnum.AGGREGATION_1_1)
						&& e2.getKindOfEdge().equals(KindOfEdgeEnum.AGGREGATION_1_N)
						|| e1.getKindOfEdge().equals(KindOfEdgeEnum.AGGREGATION_1_N)
								&& e2.getKindOfEdge().equals(KindOfEdgeEnum.AGGREGATION_1_1))) {
					/*
					 * Zde už mohu otestovat všechny vlastnosti tak, že vždy testuji, jestli se
					 * příslušné vlastnosti rovnají a pokud se rovnají úplně všechny "hlídané"
					 * vlastnosti, pak jsem našel duplicitní nastavení.
					 */
					if (e1.getLineColor() == e2.getLineColor() && e1.isLabelsAlongEdge() == e2.isLabelsAlongEdge()
							&& e1.getLineStyle() == e2.getLineStyle() && e1.getLineEnd() == e2.getLineEnd()
							&& e1.getLineBegin() == e2.getLineBegin() && e1.getLineWidth() == e2.getLineWidth()
							&& e1.isEndFill() == e2.isEndFill() && e1.isBeginFill() == e2.isBeginFill()
							&& e1.getFontColor() == e2.getFontColor()
							&& e1.getFont().getSize() == e2.getFont().getSize()
							&& e1.getFont().getStyle() == e2.getFont().getStyle()) {
						
						/*
						 * Informaci vypíšu jen v případě, že se jedná o změnu velikosti písma v
						 * příslušném dialogu, jinak, poud se jedná o změnu v souborech, tak se tyto
						 * informace vypisují do příslušného dialogu, tak není potřeba jej zde
						 * vypisovat.
						 */
						if (writeDupliciteInfo) {
							final String text = txtJopSameEdgesErrorText + ":\n" + getEdgeName(e1.getKindOfEdge())
									+ "\n" + getEdgeName(e2.getKindOfEdge());

							JOptionPane.showMessageDialog(null, text, txtJopSameEdgesErrorTitle,
									JOptionPane.ERROR_MESSAGE);
						}

						return true;
					}
				}
			}
		}
		
		
		// Zde jsem proše získané vlastnosti a nenašel jsem duplicitu:
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí text v ve zvoleném jazyce pro typ hrany kindOfEdge pro
	 * výpis do chybové hlášky.
	 * 
	 * @param kindOfEdge
	 *            - výčtová hodnota, která značí typ hrany, jejižnázev se má vrátit
	 *            v příslušném jazyce.
	 * 
	 * @return název hrany ve zvoleném jazyce.
	 */
	private static String getEdgeName(final KindOfEdgeEnum kindOfEdge) {
		switch (kindOfEdge) {
		case AGGREGATION_1_1:
			return txtAggregation_1_1;

		case AGGREGATION_1_N:
			return txtAggregation_1_N;

		case ASSOCIATION:
			return txtAssociation;

		case COMMENT:
			return txtCommentEdge;

		case EXTENDS:
			return txtInheritance;

		default:
			return txtUnknown;// Nemělo by nastat
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která získá informace o všech hraných z diagramu tříd, u kterých je
	 * třeba testovat duplicitní nastavení.
	 * 
	 * @param diagramProperties
	 *            - objekt, který obsahuje hodnoty z ClassDiagram.properties, akorát
	 *            se jedná buď o načtený soubor z adresáře configuration ve
	 *            workspace v případě testování změn v souboru, nebo o objekt
	 *            Properties z aplikace - runtimove - v případě, že se jedná o
	 *            testování duplicit při změně velikosti písma.
	 * 
	 * @return list, který bude obsahovat informace o nekterých hranách z diagramu
	 *         tříd.
	 */
	private static List<EdgeOfClassDiagramInfo> getEdgesInfoFromProperties(final Properties diagramProperties) {
		/*
		 * Vytovřím si list, do kterého níže vložím objekty, které budou obsahovat
		 * aktuální vlastnosti některých hran, které se v diagramu tříd mohou
		 * vyskytovat. Vlastnosti se týkají vždy aktuální relace aplikace.
		 */
		final List<EdgeOfClassDiagramInfo> edgeInfoList = new ArrayList<>(4);

		// Přidám informace o hraně typu asociace (symetrická):
		edgeInfoList.add(getAssociationEdgeInfo(diagramProperties));

		// Přidám informace o hraně typu dědičnost:
		edgeInfoList.add(getExtendsEdgeInfo(diagramProperties));

		// Přidám informace o hraně typu agregace 1 : 1 neboli asymetrická asociace:
		edgeInfoList.add(getAggregation_1_1_EdgeInfo(diagramProperties));

		// Přidám informace o hraně typu agregace : N:
		edgeInfoList.add(getAggregation_1_N_EdgeInfo(diagramProperties));

		// Přidám informace o hraně, která reprezentuje spojení třídy s komentářem:
		edgeInfoList.add(getCommentEdgeInfo(diagramProperties));

		// Vrátím list se získanými hodnotami / vlastnostmi hran:
		return edgeInfoList;
	}
	
	
	
	
	
	
	/**
	 * Metoda, která vytoří nový objekt, který bude obsahovat informace o hraně,
	 * která v diagramu tříd reprezentuje vztah typu asociace (symetrická asociace).
	 * 
	 * @param diagramProperties
	 *            - objekt Properties, ze kterého se mají získat hodnoty pro
	 *            příslušnou hranu, jde o to, že testování duplicit se volá z více
	 *            míst, a všude se využívá různý soubor, někdy v případě změny
	 *            velikosti písma se ten objekt Properties načte z aplikace za běhu,
	 *            ale v případě testování duplicit v souboru při jeho změně se ten
	 *            objekt Properties načte z adresáře confugration, tak abych se vzal
	 *            správné hodnoty.
	 * 
	 * @return objekt, který bude obshovat informace / vlastnosti hrany, který v
	 *         diagramu tříd reprezentuje vztah typu asociace.
	 */
	private static EdgeOfClassDiagramInfo getAssociationEdgeInfo(final Properties diagramProperties) {
		// Níže si zjistím potřebné vlastnosti pro otestování hran:

		final String lineColor = diagramProperties.getProperty("AscEdgeLineColor",
				Integer.toString(Constants.CD_ASC_EDGE_COLOR.getRGB()));

		final boolean labelsAlongEdge = Boolean.parseBoolean(diagramProperties.getProperty("AscEdgeLabelAlongEdge",
				String.valueOf(Constants.CD_ASC_LABEL_ALONG_EDGE)));

		final int lineStyle = Integer.parseInt(
				diagramProperties.getProperty("AscEdgeLineStyle", Integer.toString(Constants.CD_ASC_EDGE_LINE_STYLE)));

		final int lineEnd = Integer.parseInt(
				diagramProperties.getProperty("AscEdgeLineEnd", Integer.toString(Constants.CD_ASC_EDGE_LINE_END)));

		final int lineBegin = Integer.parseInt(
				diagramProperties.getProperty("AscEdgeLineBegin", Integer.toString(Constants.CD_ASC_EDGE_LINE_BEGIN)));

		final float lineWidth = Float.parseFloat(
				diagramProperties.getProperty("AscEdgeLineWidth", String.valueOf(Constants.CD_ASC_EDGE_LINE_WIDTH)));

		final boolean endFill = Boolean.parseBoolean(
				diagramProperties.getProperty("AscEdgeEndFill", String.valueOf(Constants.CD_ASC_EDGE_END_FILL)));

		final boolean beginFill = Boolean.parseBoolean(
				diagramProperties.getProperty("AscEdgeBeginFill", String.valueOf(Constants.CD_ASC_EDGE_BEGIN_FILL)));

		// Barva textu:
		final String fontColor = diagramProperties.getProperty("AscEdgeLineTextColor",
				Integer.toString(Constants.CD_ASC_EDGE_FONT_COLOR.getRGB()));

		// Font poznámky na hraně:
		final int fontSize = Integer.parseInt(diagramProperties.getProperty("AscEdgeLineFontSize",
				Integer.toString(Constants.CD_ASC_EDGE_LINE_FONT.getSize())));
		final int fontStyle = Integer.parseInt(diagramProperties.getProperty("AscEdgeLineFontStyle",
				Integer.toString(Constants.CD_ASC_EDGE_LINE_FONT.getStyle())));
		
		
		
		
		// Vytvořím nový objekt pro testování:
		return new EdgeOfClassDiagramInfo(KindOfEdgeEnum.ASSOCIATION, Integer.parseInt(lineColor), labelsAlongEdge,
				lineStyle, lineEnd, lineBegin, lineWidth, endFill, beginFill, Integer.parseInt(fontColor),
				new Font(Constants.CD_ASC_EDGE_LINE_FONT.getName(), fontStyle, fontSize));
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která získá informace o hraně, která v diagramu tříd reprezentuje
	 * vztah typu dědičnost a vrátí jej v podoně příslušného objektu, díky kterému
	 * je možné vztahy "snáze" otestovat.
	 * 
	 * @param diagramProperties
	 *            - objekt Properties, ze kterého se mají získat hodnoty pro
	 *            příslušnou hranu, jde o to, že testování duplicit se volá z více
	 *            míst, a všude se využívá různý soubor, někdy v případě změny
	 *            velikosti písma se ten objekt Properties načte z aplikace za běhu,
	 *            ale v případě testování duplicit v souboru při jeho změně se ten
	 *            objekt Properties načte z adresáře confugration, tak abych se vzal
	 *            správné hodnoty.
	 * 
	 * @return objekt, který bude obsahovat informace o hraně, která v diagramu tříd
	 *         reprezentuje vztah typu dědičnost.
	 */
	private static EdgeOfClassDiagramInfo getExtendsEdgeInfo(final Properties diagramProperties) {
		// Získám si potřebné informace pro otestování:
		final String colorLine = diagramProperties.getProperty("ExtEdgeLineColor",
				Integer.toString(Constants.CD_EXT_EDGE_COLOR.getRGB()));

		final boolean labelAlongEdge = Boolean.parseBoolean(diagramProperties.getProperty("ExtEdgeLabelAlongEdge",
				String.valueOf(Constants.CD_EXT_LABEL_ALONG_EDGE)));

		final int lineStyle = Integer.parseInt(
				diagramProperties.getProperty("ExtEdgeLineStyle", Integer.toString(Constants.CD_EXT_EDGE_LINE_STYLE)));

		final int lineEnd = Integer.parseInt(
				diagramProperties.getProperty("ExtEdgeLineEnd", Integer.toString(Constants.CD_EXT_EDGE_LINE_END)));

		final int lineBegin = Integer.parseInt(
				diagramProperties.getProperty("ExtEdgeLineBegin", Integer.toString(Constants.CD_EXT_EDGE_LINE_BEGIN)));

		final float lineWidth = Float.parseFloat(
				diagramProperties.getProperty("ExtEdgeLineWidth", String.valueOf(Constants.CD_EXT_EDGE_LINE_WIDTH)));

		final boolean endFill = Boolean.parseBoolean(
				diagramProperties.getProperty("ExtEdgeEndLineFill", String.valueOf(Constants.CD_EXT_EDGE_END_FILL)));

		final boolean beginFill = Boolean.parseBoolean(diagramProperties.getProperty("ExtEdgeBeginLineFill",
				String.valueOf(Constants.CD_EXT_EDGE_BEGIN_FILL)));

		final String fontColor = diagramProperties.getProperty("ExtEdgeLineTextColor",
				Integer.toString(Constants.CD_EXT_EDGE_FONT_COLOR.getRGB()));

		// Font poznámky na hraně:
		final int fontSize = Integer.parseInt(diagramProperties.getProperty("ExtEdgeLineFontSize",
				Integer.toString(Constants.CD_EXT_EDGE_LINE_FONT.getSize())));
		final int fontStyle = Integer.parseInt(diagramProperties.getProperty("ExtEdgeLineFontStyle",
				Integer.toString(Constants.CD_EXT_EDGE_LINE_FONT.getStyle())));
		
		
		
		// Vrátím objekt s informacemi o hraně, která reprezentuje vztah typu dědičnost
		// v diagramu tříd:
		return new EdgeOfClassDiagramInfo(KindOfEdgeEnum.EXTENDS, Integer.parseInt(colorLine), labelAlongEdge,
				lineStyle, lineEnd, lineBegin, lineWidth, endFill, beginFill, Integer.parseInt(fontColor),
				new Font(Constants.CD_EXT_EDGE_LINE_FONT.getName(), fontStyle, fontSize));
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která získá informace o hraně, která v diagramu tříd reprezentuje
	 * vztah typu agregace 1 : 1 nebo asymetrická asociace a tyto informace vrátí v
	 * podobně objektu, díky kterému je možné lépe otestovat duplicity.
	 * 
	 * @param diagramProperties
	 *            - objekt Properties, ze kterého se mají získat hodnoty pro
	 *            příslušnou hranu, jde o to, že testování duplicit se volá z více
	 *            míst, a všude se využívá různý soubor, někdy v případě změny
	 *            velikosti písma se ten objekt Properties načte z aplikace za běhu,
	 *            ale v případě testování duplicit v souboru při jeho změně se ten
	 *            objekt Properties načte z adresáře confugration, tak abych se vzal
	 *            správné hodnoty.
	 * 
	 * @return objekt, který bude obsahovat informace o hraně, která v diagramu tříd
	 *         reprezentuje vztah typu agregace 1 : 1 nebo asymetrická asociace.
	 */
	private static EdgeOfClassDiagramInfo getAggregation_1_1_EdgeInfo(final Properties diagramProperties) {
		// Získám si potřebné informace pro otestování:

		final String lineColor = diagramProperties.getProperty("AgrEdgeLineColor",
				Integer.toString(Constants.CD_AGR_EDGE_COLOR.getRGB()));

		final boolean labelsAlongEdge = Boolean.parseBoolean(diagramProperties.getProperty("AgrEdgeLabelAlongEdge",
				String.valueOf(Constants.CD_AGR_LABEL_ALONG_EDGE)));

		final int lineStyle = Integer.parseInt(
				diagramProperties.getProperty("AgrEdgeLineStyle", Integer.toString(Constants.CD_AGR_EDGE_LINE_STYLE)));

		final int lineEnd = Integer.parseInt(
				diagramProperties.getProperty("AgrEdgeLineEnd", Integer.toString(Constants.CD_AGR_EDGE_LINE_END)));

		final int lineBegin = Integer.parseInt(
				diagramProperties.getProperty("AgrEdgeLineBegin", Integer.toString(Constants.CD_AGR_EDGE_LINE_BEGIN)));

		final float lineWidth = Float.parseFloat(
				diagramProperties.getProperty("AgrEdgeLineWidth", String.valueOf(Constants.CD_AGR_EDGE_LINE_WIDTH)));

		final boolean endFill = Boolean.parseBoolean(
				diagramProperties.getProperty("AgrEdgeEndLineFill", String.valueOf(Constants.CD_AGR_EDGE_END_FILL)));

		final boolean beginFill = Boolean.parseBoolean(diagramProperties.getProperty("AgrEdgeBeginLineFill",
				String.valueOf(Constants.CD_AGR_EDGE_BEGIN_FILL)));

		final String fontColor = diagramProperties.getProperty("AgrEdgeLineTextColor",
				Integer.toString(Constants.CD_AGR_EDGE_FONT_COLOR.getRGB()));

		// Font poznámky na hraně:
		final int fontSize = Integer.parseInt(diagramProperties.getProperty("AgrEdgeLineFontSize",
				Integer.toString(Constants.CD_AGR_EDGE_LINE_FONT.getSize())));
		final int fontStyle = Integer.parseInt(diagramProperties.getProperty("AgrEdgeLineFontStyle",
				Integer.toString(Constants.CD_AGR_EDGE_LINE_FONT.getStyle())));

		
		
		// Vrátím objekt s informacemi o hraně, která v diagramu tříd reprezentuje vztah
		// typu agregace 1 : 1, nebo asymetrická asociace.
		return new EdgeOfClassDiagramInfo(KindOfEdgeEnum.AGGREGATION_1_1, Integer.parseInt(lineColor), labelsAlongEdge,
				lineStyle, lineEnd, lineBegin, lineWidth, endFill, beginFill, Integer.parseInt(fontColor),
				new Font(Constants.CD_AGR_EDGE_LINE_FONT.getName(), fontStyle, fontSize));
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytovří nový objekt, do kterého vloží informace o hraně, která
	 * v diagramu tříd reprezentuje vztah typu agregace 1 : N a tento objekt vrátí.
	 * 
	 * @param diagramProperties
	 *            - objekt Properties, ze kterého se mají získat hodnoty pro
	 *            příslušnou hranu, jde o to, že testování duplicit se volá z více
	 *            míst, a všude se využívá různý soubor, někdy v případě změny
	 *            velikosti písma se ten objekt Properties načte z aplikace za běhu,
	 *            ale v případě testování duplicit v souboru při jeho změně se ten
	 *            objekt Properties načte z adresáře confugration, tak abych se vzal
	 *            správné hodnoty.
	 * 
	 * @return objekt, který bude obsahovat informace o hraně, která v diagramu tříd
	 *         reprezentuje vztah typu agregace 1 : N.
	 */
	private static EdgeOfClassDiagramInfo getAggregation_1_N_EdgeInfo(final Properties diagramProperties) {
		// Získám si potřebné informace pro otestování:

		final String lineColor = diagramProperties.getProperty("Agr_1_N_EdgeLineColor",
				Integer.toString(Constants.CD_AGR_N_EDGE_COLOR.getRGB()));

		final boolean labelsAlongEdge = Boolean.parseBoolean(diagramProperties.getProperty("Agr_1_N_EdgeLabelAlongEdge",
				String.valueOf(Constants.CD_AGR_N_LABEL_ALONG_EDGE)));

		final int lineStyle = Integer.parseInt(diagramProperties.getProperty("Agr_1_N_EdgeLineStyle",
				Integer.toString(Constants.CD_AGR_EDGE_N_LINE_STYLE)));

		final int lineEnd = Integer.parseInt(diagramProperties.getProperty("Agr_1_N_EdgeLineEnd",
				Integer.toString(Constants.CD_AGR_N_EDGE_LINE_END)));

		final int lineBegin = Integer.parseInt(diagramProperties.getProperty("Agr_1_N_EdgeLineBegin",
				Integer.toString(Constants.CD_AGR_N_EDGE_LINE_BEGIN)));

		final float lineWidth = Float.parseFloat(diagramProperties.getProperty("Agr_1_N_EdgeLineWidth",
				String.valueOf(Constants.CD_AGR_EDGE_N_LINE_WIDTH)));

		final boolean endFill = Boolean.parseBoolean(diagramProperties.getProperty("Agr_1_N_EdgeEndLineFill",
				String.valueOf(Constants.CD_AGR_N_EDGE_END_FILL)));

		final boolean beginFill = Boolean.parseBoolean(diagramProperties.getProperty("Agr_1_N_EdgeBeginLineFill",
				String.valueOf(Constants.CD_AGR_N_EDGE_BEGIN_FILL)));

		final String fontColor = diagramProperties.getProperty("Agr_1_N_EdgeLineTextColor",
				Integer.toString(Constants.CD_AGR_N_EDGE_FONT_COLOR.getRGB()));

		// Font poznámky na hraně:
		final int fontSize = Integer.parseInt(diagramProperties.getProperty("Agr_1_N_EdgeLineFontSize",
				Integer.toString(Constants.CD_AGR_EDGE_N_LINE_FONT.getSize())));
		final int fontStyle = Integer.parseInt(diagramProperties.getProperty("Agr_1_N_EdgeLineFontStyle",
				Integer.toString(Constants.CD_AGR_EDGE_N_LINE_FONT.getStyle())));
		
		
		// Vrátím objekt, který bude obsahovat informace o hraně, která v diagramu tříd
		// aktuálně reprezentuje vztah typu agregace 1 : N:
		return new EdgeOfClassDiagramInfo(KindOfEdgeEnum.AGGREGATION_1_N, Integer.parseInt(lineColor), labelsAlongEdge,
				lineStyle, lineEnd, lineBegin, lineWidth, endFill, beginFill, Integer.parseInt(fontColor),
				new Font(Constants.CD_AGR_EDGE_N_LINE_FONT.getName(), fontStyle, fontSize));
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří nový objekt, do kterého vloží informace o hraně, která
	 * v diagramu tříd reprezentuje spojení třídy s komentářem.
	 * 
	 * @param diagramProperties
	 *            - objekt Properties, ze kterého se mají získat hodnoty pro
	 *            příslušnou hranu, jde o to, že testování duplicit se volá z více
	 *            míst, a všude se využívá různý soubor, někdy v případě změny
	 *            velikosti písma se ten objekt Properties načte z aplikace za běhu,
	 *            ale v případě testování duplicit v souboru při jeho změně se ten
	 *            objekt Properties načte z adresáře confugration, tak abych se vzal
	 *            správné hodnoty.
	 * 
	 * @return nový objekt, který bude obsahovat informace o hraně, která v diagramu
	 *         tříd reprezentuje spojení třídy s komentářem.
	 */
	private static EdgeOfClassDiagramInfo getCommentEdgeInfo(final Properties diagramProperties) {
		// Získám si potřebné informace pro otestování:

		final String lineColor = diagramProperties.getProperty("CommentEdgeColor",
				Integer.toString(Constants.CD_COM_EDGE_COLOR.getRGB()));

		final boolean labelsAlongEdge = Boolean.parseBoolean(diagramProperties.getProperty("CommentEdgeLabelAlongEdge",
				String.valueOf(Constants.CD_COM_LABEL_ALONG_EDGE)));

		final int lineStyle = Integer.parseInt(diagramProperties.getProperty("CommentEdgeLineStyle",
				Integer.toString(Constants.CD_COM_EDGE_LINE_STYLE)));

		final int lineEnd = Integer.parseInt(
				diagramProperties.getProperty("CommentEdgeLineEnd", Integer.toString(Constants.CD_COM_LINE_END)));

		final int lineBegin = Integer.parseInt(
				diagramProperties.getProperty("CommentEdgeLineBegin", Integer.toString(Constants.CD_COM_LINE_BEGIN)));

		final float lineWidth = Float.parseFloat(
				diagramProperties.getProperty("CommentEdgeLineWidth", String.valueOf(Constants.CD_COM_LINE_WIDTH)));

		final boolean endFill = Boolean.parseBoolean(
				diagramProperties.getProperty("CommentEdgeEndFill", String.valueOf(Constants.CD_COM_LINE_END_FILL)));

		final boolean beginFill = Boolean.parseBoolean(diagramProperties.getProperty("CommentEdgeBeginFill",
				String.valueOf(Constants.CD_COM_LINE_BEGIN_FILL)));

		// Barva textu:
		final String fontColor = diagramProperties.getProperty("CommentEdgeTextColor",
				Integer.toString(Constants.CD_COM_EDGE_FONT_COLOR.getRGB()));

		// Font poznámky na hraně:
		final int fontSize = Integer.parseInt(diagramProperties.getProperty("CommentEdgeLineFontSize",
				Integer.toString(Constants.CD_COM_EDGE_FONT.getSize())));
		final int fontStyle = Integer.parseInt(diagramProperties.getProperty("CommentEdgeLineFontStyle",
				Integer.toString(Constants.CD_COM_EDGE_FONT.getStyle())));
		
		
		// Vrátím objekt, který bude obsahovat informace o hraně, která v diagramu tříd
		// reprezentuje spojení mezi třídou a komentářem.
		return new EdgeOfClassDiagramInfo(KindOfEdgeEnum.COMMENT, Integer.parseInt(lineColor), labelsAlongEdge,
				lineStyle, lineEnd, lineBegin, lineWidth, endFill, beginFill, Integer.parseInt(fontColor),
				new Font(Constants.CD_COM_EDGE_FONT.getName(), fontStyle, fontSize));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public boolean areAssociationAndAggregationSameInId(final Properties diagramProp) {		
		/*
		 * Tento případ by nikdy neměl nastat, ale kdyby ano, tak aby nedošlo k chybě,
		 * tak vrátím false, zde by ještě bylo možné otestovat výchozí hodnoty, ale o
		 * těch je jasné, že jsou jiné.
		 */
		if (diagramProp == null)
			return false;
		
		
		final String lineColorAsc = diagramProp.getProperty("AssociationEdgeLineColor", Integer.toString(Constants.ID_ASSOCIATION_EDGE_COLOR.getRGB()));
		final String lineColorAgr = diagramProp.getProperty("AggregationEdgeLineColor", Integer.toString(Constants.ID_AGGREGATION_EDGE_COLOR.getRGB()));
		
		if (Integer.parseInt(lineColorAsc) != Integer.parseInt(lineColorAgr))
			return false;
		
		
		
		final boolean labelsAlongEdgeAsc = Boolean.parseBoolean(diagramProp.getProperty("AssociationEdgeLabelsAlongEdge", String.valueOf(Constants.ID_ASSOCIATION_LABELS_ALONG_EDGE)));
		final boolean labelsAlongEdgeAgr = Boolean.parseBoolean(diagramProp.getProperty("AggregationEdgeLabelsAlongEdge", String.valueOf(Constants.ID_AGGREGATION_LABELS_ALONG_EDGE)));
		
		if (labelsAlongEdgeAsc != labelsAlongEdgeAgr)
			return false;
		
		
		final int lineEndAsc = Integer.parseInt(diagramProp.getProperty("AssociationEdgeLineEnd", Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_END)));
		final int lineEndAgr = Integer.parseInt(diagramProp.getProperty("AggregationEdgeLineEnd", Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_END)));
		
		if (lineEndAsc != lineEndAgr)
			return false;
		
		
		
		
		final int lineBeginAsc = Integer.parseInt(diagramProp.getProperty("AssociationEdgeLineBegin", Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_BEGIN)));
		final int lineBeginAgr = Integer.parseInt(diagramProp.getProperty("AggregationEdgeLineBegin", Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_BEGIN)));
		
		if (lineBeginAsc != lineBeginAgr)
			return false;
		
		
		
		final float lineWidthAsc = Float.parseFloat(diagramProp.getProperty("AssociationEdgeLineWidth", String.valueOf(Constants.ID_ASSOCIATION_EDGE_LINE_WIDTH)));
		final float lineWidthAgr = Float.parseFloat(diagramProp.getProperty("AggregationEdgeLineWidth", String.valueOf(Constants.ID_AGGREGATION_EDGE_LINE_WIDTH)));
		
		if (lineWidthAsc != lineWidthAgr)
			return false;
		
		
		
		final boolean endFillAsc = Boolean.parseBoolean(diagramProp.getProperty("AssociationEdgeLineEndFill", String.valueOf(Constants.ID_ASSOCIATION_EDGE_LINE_END_FILL)));
		final boolean endFillAgr = Boolean.parseBoolean(diagramProp.getProperty("AggregationEdgeLineEndFill", String.valueOf(Constants.ID_AGGREGATION_EDGE_LINE_END_FILL)));
		
		if (endFillAsc != endFillAgr)
			return false;
		
		
		
		final boolean beginFillAsc = Boolean.parseBoolean(diagramProp.getProperty("AssociationEdgeLineBeginFill", String.valueOf(Constants.ID_ASSOCIATION_EDGE_LINE_BEGIN_FILL)));
		final boolean beginFillagr = Boolean.parseBoolean(diagramProp.getProperty("AggregationEdgeLineBeginFill", String.valueOf(Constants.ID_AGGREGATION_EDGE_LINE_BEGIN_FILL)));
		
		if (beginFillAsc != beginFillagr)
			return false;
		
		
		
		final String fontColorAsc = diagramProp.getProperty("AssociationEdgeLineTextColor", Integer.toString(Constants.ID_ASSOCIATION_EDGE_FONT_COLOR.getRGB()));
		final String fontColorAgr = diagramProp.getProperty("AggregationEdgeLineTextColor", Integer.toString(Constants.ID_AGGREGATION_EDGE_FONT_COLOR.getRGB()));
		
		if (Integer.parseInt(fontColorAsc) != Integer.parseInt(fontColorAgr))
			return false;
		
		
		
		
		// Fonty:
		final int fontSizeAsc = Integer.parseInt(diagramProp.getProperty("AssociationEdgeLineFontSize", Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getSize())));
		final int fontStyleAsc = Integer.parseInt(diagramProp.getProperty("AssociationEdgeLineFontStyle", Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getStyle())));
		
		final int fontSizeAgr = Integer.parseInt(diagramProp.getProperty("AggregationEdgeLineFontSize", Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getSize())));
		final int fontStyleAgr = Integer.parseInt(diagramProp.getProperty("AggregationEdgeLineFontStyle", Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getStyle())));
		
		if (fontSizeAsc != fontSizeAgr)
			return false;

		/*
		 * Vrátí se true v případě, že jsem prošel všechny potřebné vlastnosti a všechny jsou stejné, takže mám
		 * duplicitní hrany. Takže i tato podmínka bude true, jinak se nejedná o duplicitní hrany.
		 */
		return fontStyleAsc == fontStyleAgr;
	}
}
