package cz.uhk.fim.fimj.font_size_form;

import java.awt.GridBagLayout;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;

/**
 * Tato třída slouží jako panel, který obsahuje komponenty pro nastavení velikosti písma pro objekty v diagramu tříd a v
 * diagramu instancí, ale bude se nastavovat pouze velikost písma u těch objektů, které si uživatel označí.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class SameSizeSelectedObjectsPanel extends FontSizePanelAbstract implements LanguageInterface {

	private static final long serialVersionUID = 1L;

	
	
	
	/**
	 * Panel s komponentami pro nastavení toho, u jakých objektů v diagramu tříd se
	 * má nastavit velikost písma
	 */
	private final SizeSelectedObjectCdPanel pnlWithChcbsForCd;
	
	/**
	 * Panel, který obsahuje komponenty pro nastavení toho, jakým objektům se má
	 * nastavit velikosti písma v diagramu instancí.
	 */
	private final SizeSelectedObjectIdPanel pnlWithChcbsForId;
	
	
	
	
	
	
	/**
	 * Proměnná, do které se uloží hodnota, která značí velikosti písma v buňce
	 * reprezentující třídu v dagramu tříd. Jedná se pouze o výchozí hodnotu.
	 */
	private final int defaultValue;
	
	
	
	
	
	
	

	/**
	 * Konstruktor této třídy.
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd.
	 * 
	 * @param instanceDiagram
	 *            - reference na diagram instancí.
	 */
	SameSizeSelectedObjectsPanel(final GraphClass classDiagram, final GraphInstance instanceDiagram) {
		super();
		
		setLayout(new GridBagLayout());
		
		
		/*
		 * Načtu si soubor Default.properties buď z workspace nebo z adresáře aplikaci
		 * pokud nebude ve workspace nalezen, zde nebudu řešit neexistující hodnoty v
		 * configuration.
		 */
		final Properties defaultProp = ReadFile.getDefaultPropAnyway();
		
		
		index = 0;
		
		gbc = createGbc();
		
		
		
		gbc.gridwidth = 2;
		
		lblInfo = new JLabel();
		lblInfo.setHorizontalAlignment(JLabel.CENTER);
		setGbc(index, 0, lblInfo);
		
		
		
		
		addSpace(++index);
		
		
		
		
		gbc.gridwidth = 1;
		
		pnlWithChcbsForCd = new SizeSelectedObjectCdPanel(defaultProp);
		setGbc(++index, 0, pnlWithChcbsForCd);
		
		
		
		pnlWithChcbsForId = new SizeSelectedObjectIdPanel(defaultProp);
		setGbc(index, 1, pnlWithChcbsForId);
		
		
		
		
		
		
		
		
		gbc.gridwidth = 2;
		
		addSpace(++index);
		
		
		
		
		
		
		lblFontSizeText = new JLabel();
		setGbc(++index, 0, lblFontSizeText);
		
		
		
		
		
		
		
		/*
		 * Získám si výchozí hodnotu. Využiji hodnotu, která je nastavena pro aktuální
		 * velikost buňky reprezentující třídu v diagramu tříd, ale je to jedno, při
		 * pohybu budou všechny velikost stejné.
		 */
		defaultValue = getIntFromProp(classDiagram.getDefaultProperties(), "ClassFontSize",
				Integer.toString(Constants.CD_FONT_CLASS.getSize()));
		
		
		
		
		sdFontSize = createSlider(defaultValue);
		setGbc(++index, 0, sdFontSize);
		
		sdFontSize.addChangeListener(arg0 -> {
			/*
			 * Nejprve otestuji, zda se jedná pouze o nastavení hodnoty pro tento slider,
			 * pokud ano, tak zde skončím, protože by se jinak opakovalo volání, což je
			 * zbytečné a chybné.
			 */
			if (onlyUpdateValue)
				return;


			/*
			 * Tato podmínka je, že se bude pokračovat v metodě, až když uživatel přestane
			 * hýbat posuvníkem, dokud hýbět, tak se nic nestane
			 */
			if (!FontSizeForm.isSetValuesWhileMoving() &&  sdFontSize.getValueIsAdjusting())
				return;


			/*
			 * Nastavená hodnota:
			 */
			final int value = sdFontSize.getValue();

			// Nastavím do labelu nastavenou velikost.
			setSizeInLabel(lblFontSizeText, value);

			// Nastavím velikost pro diagram tříd a instancí:
			FONT_SIZE_OPERATION_CD.quickSetupOfFontSize(value, pnlWithChcbsForCd.isChcbAssociationSelected(),
					pnlWithChcbsForCd.isChcbExtendsSelected(), pnlWithChcbsForCd.isChcbImplementsSelected(),
					pnlWithChcbsForCd.isChcbClassSelected(), pnlWithChcbsForCd.isChcbCommentSelected(),
					pnlWithChcbsForCd.isChcbCommentEdgeSelected(),
					pnlWithChcbsForCd.isChcbAggregation_1_1_Selected(),
					pnlWithChcbsForCd.isChcbAggregation_1_N_Selected(), classDiagram);


			FONT_SIZE_OPERATION_ID.quickSetupOfFontSize(value, pnlWithChcbsForId.isChcbInstanceSelected(),
					pnlWithChcbsForId.isChcbInstanceAttributesSelected(),
					pnlWithChcbsForId.isChcbInstanceMethodsSelected(),
					pnlWithChcbsForId.isChcbAssociationSelected(), pnlWithChcbsForId.isChcbAggregationSelected(),
					instanceDiagram);



			// Nastavím velikost písma do proměnné kvůli klávesovým zkratkám Ctrl + a -:
			if (pnlWithChcbsForCd.isChcbClassSelected())
				GraphClass.setClassFontSize(value);

			if (pnlWithChcbsForId.isChcbInstanceSelected())
				GraphInstance.setInstanceFontSize(value);




			// Testování duplicit:
			if (!sdFontSize.getValueIsAdjusting()) {
				/*
				 * Jestli je označena třída nebo komentář, tak potřebuji hlídat, aby nebyly
				 * jejich vlastnosti identické.
				 */
                if ((pnlWithChcbsForCd.isChcbClassSelected() || pnlWithChcbsForCd.isChcbCommentSelected()) &&
                        checkDuplicateClassAndCommentProperties(classDiagram,
                                FontSizeForm.CHECK_DUPLICATES_IN_DIAGRAMS)) {
					// Nastavím i výchozí font pro klávesové zkratky
					GraphClass.setClassFontSize(Constants.CD_FONT_CLASS.getSize());

					/*
					 * Nastavím následující proměnnou na true, protože jinak by se při nastavení
					 * hodnoty slideru znovu zavolala tato metoda což je chybné, mohly by se
					 * nastavit chyná data, navíc by se znovu celý postup opakoval.
					 */
					onlyUpdateValue = true;
					// Zde zavolám metodu, která nastaví hodnoty ve slideru bez opakování této
					// metody:
					setSliderValue(sdFontSize, Constants.CD_FONT_CLASS.getSize(), lblFontSizeText);

					onlyUpdateValue = false;
				}





				/*
				 * V případě, že se má nastavovat hrany asociace, agregace 1 : 1, agregace 1 :
				 * N, dědičnost nebo hrana spojující komentář s třídou, tak také otestuji, zda
				 * žádná z těchto hran nemá duplicitní nastavení s jinou.
				 */
				if (pnlWithChcbsForCd.isChcbAssociationSelected() || pnlWithChcbsForCd.isChcbExtendsSelected()
						|| pnlWithChcbsForCd.isChcbCommentEdgeSelected()
						|| pnlWithChcbsForCd.isChcbAggregation_1_1_Selected()
						|| pnlWithChcbsForCd.isChcbAggregation_1_N_Selected())
					checkDuplicateEdgesInCd(classDiagram, FontSizeForm.CHECK_DUPLICATES_IN_DIAGRAMS);






				/*
				 * V případě, že je označeno nastavení hrany typu asociace nebo agregace v
				 * diagramu instancí, tak musím otestovat, zda nejsou identické - zda nemají
				 * identické veškeré vlastnosti.
				 */
				if (pnlWithChcbsForId.isChcbAggregationSelected() || pnlWithChcbsForId.isChcbAssociationSelected())
					checkDuplicateEdgesInId(instanceDiagram, FontSizeForm.CHECK_DUPLICATES_IN_DIAGRAMS);
			}
		});
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Getr na panel, který obsahuje komponenty typu JCheckBox, které představují
	 * komponenty v diagramu tříd.
	 * 
	 * @return getr na panel s komponentami JChechBox, které představují objekty v
	 *         diagramu tříd.
	 */
	final SizeSelectedObjectCdPanel getPnlWithChcbsForCd() {
		return pnlWithChcbsForCd;
	}
	

	
	
	/**
	 * Getr na panel, který obsahuje komponenty typu JCheckBox, které představují
	 * komponenty v diagramu instancí.
	 * 
	 * @return getr na panel s komponentami JChechBox, které představují objekty v
	 *         diagramu instancí.
	 */
	final SizeSelectedObjectIdPanel getPnlWithChcbsForid() {
		return pnlWithChcbsForId;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public void setLanguage(final Properties properties) {
		setLanguageForErrorText(properties);
		
		pnlWithChcbsForCd.setLanguage(properties);
		pnlWithChcbsForId.setLanguage(properties);
		
		if (properties != null) {
			txtSize = properties.getProperty("FSF_SSSOP_TxtSize", Constants.FSF_SSSOP_TXT_SIZE);
			
			lblInfo.setText(properties.getProperty("FSF_SSSOP_LblInfo", Constants.FSF_SSSOP_LBL_INFO));
			
			sdFontSize.setBorder(BorderFactory.createTitledBorder(properties.getProperty("FSF_SSSOP_SliderFontSize", Constants.FSF_SSSOP_SLIDER_FONT_SIZE)));
		}

		else {
			txtSize = Constants.FSF_SSSOP_TXT_SIZE;

			lblInfo.setText(Constants.FSF_SSSOP_LBL_INFO);

			sdFontSize.setBorder(BorderFactory.createTitledBorder(Constants.FSF_SSSOP_SLIDER_FONT_SIZE));
		}
		
		// Nastavím do labelu nastavenou velikost.
		setSizeInLabel(lblFontSizeText, defaultValue);
	}
}
