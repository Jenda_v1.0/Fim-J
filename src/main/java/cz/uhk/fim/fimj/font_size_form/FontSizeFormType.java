package cz.uhk.fim.fimj.font_size_form;

/**
 * Tetno výčet slouží pro určení panelu s komponentami pro nastavení velikost fontu / písma v diagramu tříd a instancí.
 * <p>
 * Dle těchto výčtových hodnot se pozná, jaký panel / panely s komponentami se mají v dialogu pro nastavení velikosti
 * písma nastavit, tj. například se do dialogu pro nastavení velikosti písma má vložit pouze jeden panel, který bude
 * obsahovat komponenty pro nastavení jedné velikosti písma / fontu pro všechny objekty v diagramu tříd i instancí, nebo
 * se do toho dialogu má vložit panel, který bude obsahovat komponenty, pro nastavení jednotlivých velikostí fontu /
 * písma pro jednotlivé objekty v diagramu tříd a instancí zvlášť apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public enum FontSizeFormType {

    /**
     * Tato hodnota značí, že se má vložit do dialogu pro nastavení velikosti písma / fontu panel, který bude obsahovat
     * komponenty pro nastavení jedé velikosti fontu / písma pro všechny objekty jak v diagramu tříd tak v diagramu
     * instancí.
     */
    ONE_SAME_SIZE("One_Font_Size_For_All_Objects"),

    /**
     * Tato hodnota značí, že se má do dialogu pro nastavení velikosti písma vložit panel, který bude obsahovat
     * komponenty, pomocí kterých si uživatel bude moci nastavit velikost písma pro pouze pro vybrané objekty, například
     * bude moci vybrat velikost pouze pro třídy z diagramu tříd, nebo pouze pro instance v diagramu instancí, nebo
     * oboje apod. Pro ostatní objekty v diagramu tříd a v diagramu instancí.
     */
    ONE_SIZE_FOR_CHOOSED_OBJECTS("Font_Size_For_Selected_Objects"),

    /**
     * Tato hodnota značí, že se do dialogu pro nastavení velikosti písma má vložit panel, který bude obsahovat
     * komponenty pro nastavení velikosti písma / fontu pro každý objekt v diagramu tříd tak v diagramu instancí. Tzn.
     * že ve zmíněném dialogu bude komponenta pro nastavení velikosti písma / fontu ve třídě, další komponenta pro
     * nastavení velikost fontu / písma v komentáře, další pro velikost písma / fontu textu na hraně vztahu asociace,
     * ...
     */
    ONE_SIZE_FOR_EACH_OBJECT("One_Font_Size_For_Each_Object");


    /**
     * Proměnná, která obsahuje textovou hodnotu, která je využita pro uložení tohoto výčtu do souboru, je možné využit
     * jen metodu toString, ale pro lepší převod do textu a zpět.
     */
    private final String value;


    /**
     * Konstruktor této třídy.
     *
     * @param value
     *         - text, který bude příslušná výčtová hodnota reprezentovat.
     */
    FontSizeFormType(final String value) {
        this.value = value;
    }


    /**
     * Getr na proměnnou value, která obsahuje text vložená do příslušné výčtové hodnoty pro uložení a načtení do
     * konfiguračních souborů.
     *
     * @return výše popsanou textovou hodnotu z příslušné výčtové hodnoty.
     */
    public String getValue() {
        return value;
    }
}
