package cz.uhk.fim.fimj.font_size_form;

import java.awt.GridBagLayout;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;

/**
 * Tato třída slouží jako panel, který obsahuje komponenty pro nastavení stejné velikosti pro všechny objekty v diagramu
 * tříd a instancí.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class SameSizePanel extends FontSizePanelAbstract implements LanguageInterface {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Tato proměnná slouží pouze pro první naplnění - výchozí načtená hodnota ze
	 * souboru při vytváření instance této třídy.
	 */
	private final int defaultValue;

	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd.
	 * 
	 * @param instanceDiagram
	 *            - referencena diagram instancí.
	 */
	SameSizePanel(final GraphClass classDiagram, final GraphInstance instanceDiagram) {
		super();

		setLayout(new GridBagLayout());

		gbc = createGbc();
		
		index = 0;
		
		
		
		
		
		lblInfo = new JLabel();
		lblInfo.setHorizontalAlignment(JLabel.CENTER);
		setGbc(index, 0, lblInfo);
		
		
		
		
		addSpace(++index);
		
		

		
		
		lblFontSizeText = new JLabel();
		setGbc(++index, 0, lblFontSizeText);
		
		
		
		
		
		
		/*
		 * Získám si výchozí hodnotu. Využiji hodnotu, která je nastavena pro aktuální
		 * velikost buňky reprezentující třídu v diagramu tříd, ale je to jedno, při
		 * pohybu budou všechny velikost stejné.
		 */
		defaultValue = getIntFromProp(classDiagram.getDefaultProperties(), "ClassFontSize",
				Integer.toString(Constants.CD_FONT_CLASS.getSize()));
		
		
		sdFontSize = createSlider(defaultValue);
		setGbc(++index, 0, sdFontSize);

		sdFontSize.addChangeListener(e -> {
			/*
			 * Nejprve otestuji, zda se jedná pouze o nastavení hodnoty pro tento slider,
			 * pokud ano, tak zde skončím, protože by se jinak opakovalo volání, což je
			 * zbytečné a chybné.
			 */
			if (onlyUpdateValue)
				return;

			/*
			 * Tato podmínka je, že se bude pokračovat v metodě, až když uživatel přestane
			 * hýbat posuvníkem, dokud hýbět, tak se nic nestane
			 */
			if (!FontSizeForm.isSetValuesWhileMoving() &&  sdFontSize.getValueIsAdjusting())
				return;


			/*
			 * Nastavená hodnota:
			 */
			final int value = sdFontSize.getValue();

			// Nastavím do labelu nastavenou velikost.
			setSizeInLabel(lblFontSizeText, value);

			// Nastavím velikost pro diagram tříd a instancí:
			FONT_SIZE_OPERATION_CD.quickSetupOfFontSize(value, classDiagram);
			FONT_SIZE_OPERATION_ID.quickSetupOfFontSize(value, instanceDiagram);

			// Nastavím velikost písma do proměnné kvůli klávesovým zkratkám Ctrl + a -:
			GraphClass.setClassFontSize(value);
			GraphInstance.setInstanceFontSize(value);




			// Nyní potřebuji otestovat, zda nebyly nastaveny nějaké duplicitní hodnoty:
			if (!sdFontSize.getValueIsAdjusting()) {
				// Zda jsou stejné buňky pro třídu a komentář v CD:
				if (checkDuplicateClassAndCommentProperties(classDiagram,
						FontSizeForm.CHECK_DUPLICATES_IN_DIAGRAMS)) {
					// Nastavím i výchozí font pro klávesové zkratky
					GraphClass.setClassFontSize(Constants.CD_FONT_CLASS.getSize());

					/*
					 * Nastavím následující proměnnou na true, protože jinak by se při nastavení
					 * hodnoty slideru znovu zavolala tato metoda což je chybné, mohly by se
					 * nastavit chyná data, navíc by se znovu celý postup opakoval.
					 */
					onlyUpdateValue = true;
					// Zde zavolám metodu, která nastaví hodnoty ve slideru bez opakování této
					// metody:
					setSliderValue(sdFontSize, Constants.CD_FONT_CLASS.getSize(), lblFontSizeText);

					onlyUpdateValue = false;
				}


				// zde otestuji, zda jsou vybrané / některé hrany stejné:
				checkDuplicateEdgesInCd(classDiagram, FontSizeForm.CHECK_DUPLICATES_IN_DIAGRAMS);

				// Zde otestuji hrany pro asociace a agregaci v diagramu instancí:
				checkDuplicateEdgesInId(instanceDiagram, FontSizeForm.CHECK_DUPLICATES_IN_DIAGRAMS);
			}
		});
	}
	
	
	
		
	
	
	
	
	
	@Override
	public void setLanguage(final Properties properties) {
		setLanguageForErrorText(properties);
		
		if (properties != null) {
			lblInfo.setText(properties.getProperty("FSF_SSP_LblInfo", Constants.FSF_SSP_LBL_INFO));

			txtSize = properties.getProperty("FSF_SSP_TxtSize", Constants.FSF_SSP_TXT_SIZE);

			sdFontSize.setBorder(BorderFactory
					.createTitledBorder(properties.getProperty("FSF_SSP_FontSize", Constants.FSF_SSP_FONT_SIZE)));
		}
		
		
		else {
			lblInfo.setText(Constants.FSF_SSP_LBL_INFO);

			txtSize = Constants.FSF_SSP_TXT_SIZE;

			sdFontSize.setBorder(BorderFactory.createTitledBorder(Constants.FSF_SSP_FONT_SIZE));
		}
		
		
		// Nastavení textu do labelu:
		setSizeInLabel(lblFontSizeText, defaultValue);
	}
}
