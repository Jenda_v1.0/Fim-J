package cz.uhk.fim.fimj.font_size_form;

import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;

/**
 * Tato třída slouží jako panel, který obsahuje komponenty typu JCheckBox a tyto chcbs značí, u jakých objektů v
 * diagramu tříd se bude manipulovat s velikostí písma / fontu.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class SizeSelectedObjectCdPanel extends FontSizePanelAbstract implements LanguageInterface, ActionListener {

	private static final long serialVersionUID = 1L;

	
	// Komponenty typu jCheckBox, které značí u jakých komponent se bude nastavovat
	// velikost písma
	private final JCheckBox chcbClass, chcbComment, chcbCommentEdge, chcbAssociation, chcbAggregation_1_1,
			chcbAggregation_1_N, chcbImplements, chcbExtends;

	
	// Tlačítka pro označit a odznačit vše:
	private final JButton btnSelectAll, btnDeselectAll;
	

	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param defaultProp
	 *            - načtený soubor Default.properties buď z workspace nebo z
	 *            adresáře aplikace, je zde potřeba pouze kvůli načtení výchozích
	 *            hodnot.
	 */
	SizeSelectedObjectCdPanel(final Properties defaultProp) {
		super();
		
		
		setLayout(new GridBagLayout());
		
		index = 0;
		
		gbc = createGbc();
		
		
		chcbClass = new JCheckBox();
		chcbClass.addActionListener(this);
		setGbc(index, 0, chcbClass);
		
		chcbComment = new JCheckBox();
		chcbComment.addActionListener(this);
		setGbc(++index, 0, chcbComment);
		
		chcbCommentEdge = new JCheckBox();
		chcbCommentEdge.addActionListener(this);
		setGbc(++index, 0, chcbCommentEdge);
		
		chcbExtends = new JCheckBox();
		chcbExtends.addActionListener(this);
		setGbc(++index, 0, chcbExtends);
		
		chcbImplements = new JCheckBox();
		chcbImplements.addActionListener(this);
		setGbc(++index, 0, chcbImplements);
		
		chcbAssociation = new JCheckBox();
		chcbAssociation.addActionListener(this);
		setGbc(++index, 0, chcbAssociation);
		
		chcbAggregation_1_1 = new JCheckBox();
		chcbAggregation_1_1.addActionListener(this);
		setGbc(++index, 0, chcbAggregation_1_1);
		
		chcbAggregation_1_N = new JCheckBox();
		chcbAggregation_1_N.addActionListener(this);
		setGbc(++index, 0, chcbAggregation_1_N);
		
		
		
		
		
		// nyní označím hodnoty dle posledního nastavení - tak jak byly nastaveny při
		// zavření dialogu:
		if (defaultProp != null) {
			chcbClass.setSelected(Boolean.parseBoolean(
					defaultProp.getProperty("FontSizeForm_CD_ClassCell", String.valueOf(Constants.FSF_CD_CLASS_CELL))));

			chcbComment.setSelected(Boolean.parseBoolean(defaultProp.getProperty("FontSizeForm_CD_CommentCell",
					String.valueOf(Constants.FSF_CD_COMMENT_CELL))));

			chcbCommentEdge.setSelected(Boolean.parseBoolean(defaultProp.getProperty("FontSizeForm_CD_CommentEdge",
					String.valueOf(Constants.FSF_CD_COMMENT_EDGE))));

			chcbExtends.setSelected(Boolean.parseBoolean(defaultProp.getProperty("FontSizeForm_CD_ExtendsEdge",
					String.valueOf(Constants.FSF_CD_EXTENDS_EDGE))));

			chcbImplements.setSelected(Boolean.parseBoolean(defaultProp.getProperty("FontSizeForm_CD_ImplementsEdge",
					String.valueOf(Constants.FSF_CD_IMPLEMENTS_EDGE))));

			chcbAssociation.setSelected(Boolean.parseBoolean(defaultProp.getProperty("FontSizeForm_CD_AssociationEdge",
					String.valueOf(Constants.FSF_CD_ASSOCIATION_EDGE))));

			chcbAggregation_1_1
					.setSelected(Boolean.parseBoolean(defaultProp.getProperty("FontSizeForm_CD_Aggregation_1_1_Edge",
							String.valueOf(Constants.FSF_CD_AGGREGATION_1_1_EDGE))));

			chcbAggregation_1_N
					.setSelected(Boolean.parseBoolean(defaultProp.getProperty("FontSizeForm_CD_Aggregation_1_N_Edge",
							String.valueOf(Constants.FSF_CD_AGGREGATION_1_N_EDGE))));
		}
		
		
		else {
			// Zde se využijí výchozí hodnoty z Constants:

			chcbClass.setSelected(Constants.FSF_CD_CLASS_CELL);

			chcbComment.setSelected(Constants.FSF_CD_COMMENT_CELL);

			chcbCommentEdge.setSelected(Constants.FSF_CD_COMMENT_EDGE);

			chcbExtends.setSelected(Constants.FSF_CD_EXTENDS_EDGE);

			chcbImplements.setSelected(Constants.FSF_CD_IMPLEMENTS_EDGE);

			chcbAssociation.setSelected(Constants.FSF_CD_ASSOCIATION_EDGE);

			chcbAggregation_1_1.setSelected(Constants.FSF_CD_AGGREGATION_1_1_EDGE);

			chcbAggregation_1_N.setSelected(Constants.FSF_CD_AGGREGATION_1_N_EDGE);
		}
		
		
		
		
		
		
		addSpace(++index);
		
		
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.CENTER));
		
		btnSelectAll = new JButton();
		btnDeselectAll = new JButton();
		
		btnSelectAll.addActionListener(this);
		btnDeselectAll.addActionListener(this);
		
		pnlButtons.add(btnSelectAll);
		pnlButtons.add(btnDeselectAll);
		
		setGbc(++index, 0, pnlButtons);
		
		
		
		checkSelectedItems();
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí logickou hodnotu o tom, zda je označena komponenty typu
	 * JCheckbox, která značí, zda se má nastavovat velikosti písma / fontu pro
	 * buňku reprezentující třídu v diagramu tříd.
	 * 
	 * @return popsáno výše.
	 */
	final boolean isChcbClassSelected() {
		return chcbClass.isSelected();
	}

	/**
	 * Metoda, která vrátí logickou hodnotu o tom, zda je označena komponenty typu
	 * JCheckbox, která značí, zda se má nastavovat velikosti písma / fontu pro
	 * buňku reprezentující komentářd v diagramu tříd.
	 * 
	 * @return popsáno výše.
	 */
	final boolean isChcbCommentSelected() {
		return chcbComment.isSelected();
	}
	
	/**
	 * Metoda, která vrátí logickou hodnotu o tom, zda je označena komponenty typu
	 * JCheckbox, která značí, zda se má nastavovat velikosti písma / fontu pro
	 * hranu reprezentující spojední třídy s komentářem v diagramu tříd.
	 * 
	 * @return popsáno výše.
	 */
	final boolean isChcbCommentEdgeSelected() {
		return chcbCommentEdge.isSelected();
	}
	
	/**
	 * Metoda, která vrátí logickou hodnotu o tom, zda je označena komponenty typu
	 * JCheckbox, která značí, zda se má nastavovat velikosti písma / fontu pro
	 * hranu reprezentující vztah typu dědičnost v diagramu tříd.
	 * 
	 * @return popsáno výše.
	 */
	final boolean isChcbExtendsSelected() {
		return chcbExtends.isSelected();
	}

	/**
	 * Metoda, která vrátí logickou hodnotu o tom, zda je označena komponenty typu
	 * JCheckbox, která značí, zda se má nastavovat velikosti písma / fontu pro
	 * hranu reprezentující vztah typu implementace rozhraní v diagramu tříd.
	 * 
	 * @return popsáno výše.
	 */
	final boolean isChcbImplementsSelected() {
		return chcbImplements.isSelected();
	}
	
	/**
	 * Metoda, která vrátí logickou hodnotu o tom, zda je označena komponenty typu
	 * JCheckbox, která značí, zda se má nastavovat velikosti písma / fontu pro
	 * hranu reprezentující vztah typu asociace v diagramu tříd.
	 * 
	 * @return popsáno výše.
	 */
	final boolean isChcbAssociationSelected() {
		return chcbAssociation.isSelected();
	}
	
	/**
	 * Metoda, která vrátí logickou hodnotu o tom, zda je označena komponenty typu
	 * JCheckbox, která značí, zda se má nastavovat velikosti písma / fontu pro
	 * hranu reprezentující vztah typu agregace 1 : 1 v diagramu tříd.
	 * 
	 * @return popsáno výše.
	 */
	final boolean isChcbAggregation_1_1_Selected() {
		return chcbAggregation_1_1.isSelected();
	}
	
	/**
	 * Metoda, která vrátí logickou hodnotu o tom, zda je označena komponenty typu
	 * JCheckbox, která značí, zda se má nastavovat velikosti písma / fontu pro
	 * hranu reprezentující vztah typu agregace 1 : N v diagramu tříd.
	 * 
	 * @return popsáno výše.
	 */
	final boolean isChcbAggregation_1_N_Selected() {
		return chcbAggregation_1_N.isSelected();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public void setLanguage(final Properties properties) {
		if (properties != null) {
			setBorder(BorderFactory.createTitledBorder(
					properties.getProperty("FSF_SSOCP_BorderTitle", Constants.FSF_SSOCP_BORDER_TITLE)));

			chcbClass.setText(properties.getProperty("FSF_SSOCP_Chcb_ClassText", Constants.FSF_SSOCP_CHCB_CLASS_TEXT));
			chcbComment.setText(
					properties.getProperty("FSF_SSOCP_Chcb_CommentText", Constants.FSF_SSOCP_CHCB_COMMENT_TEXT));
			chcbCommentEdge.setText(properties.getProperty("FSF_SSOCP_Chcb_CommentEdgeText",
					Constants.FSF_SSOCP_CHCB_COMMENT_EDGE_TEXT));
			chcbExtends.setText(
					properties.getProperty("FSF_SSOCP_Chcb_ExtendsText", Constants.FSF_SSOCP_CHCB_EXTENDS_TEXT));
			chcbImplements.setText(
					properties.getProperty("FSF_SSOCP_Chcb_ImplementsText", Constants.FSF_SSOCP_CHCB_IMPLEMENTS_TEXT));
			chcbAssociation.setText(properties.getProperty("FSF_SSOCP_Chcb_AssociationText",
					Constants.FSF_SSOCP_CHCB_ASSOCIATION_TEXT));
			chcbAggregation_1_1.setText(properties.getProperty("FSF_SSOCP_Chcb_Aggregation_1_1_Text",
					Constants.FSF_SSOCP_CHCB_AGGREGATION_1_1_TEXT));
			chcbAggregation_1_N.setText(properties.getProperty("FSF_SSOCP_Chcb_Aggregation_1_N_Text",
					Constants.FSF_SSOCP_CHCB_AGGREGATION_1_N_TEXT));

			chcbClass.setToolTipText(
					properties.getProperty("FSF_SSOCP_Chcb_Class_TT", Constants.FSF_SSOCP_CHCB_CLASS_TT));
			chcbComment.setToolTipText(
					properties.getProperty("FSF_SSOCP_Chcb_Comment_TT", Constants.FSF_SSOCP_CHCB_COMMENT_TT));
			chcbCommentEdge.setToolTipText(
					properties.getProperty("FSF_SSOCP_Chcb_CommentEdge_TT", Constants.FSF_SSOCP_CHCB_COMMENT_EDGE_TT));
			chcbExtends.setToolTipText(
					properties.getProperty("FSF_SSOCP_Chcb_Extends_TT", Constants.FSF_SSOCP_CHCB_EXTENDS_TT));
			chcbImplements.setToolTipText(
					properties.getProperty("FSF_SSOCP_Chcb_Implements_TT", Constants.FSF_SSOCP_CHCB_IMPLEMENTS_TT));
			chcbAssociation.setToolTipText(
					properties.getProperty("FSF_SSOCP_Chcb_Association_TT", Constants.FSF_SSOCP_CHCB_ASSOCIATION_TT));
			chcbAggregation_1_1.setToolTipText(properties.getProperty("FSF_SSOCP_Chcb_Aggregation_1_1_TT",
					Constants.FSF_SSOCP_CHCB_AGGREGATION_1_1_TT));
			chcbAggregation_1_N.setToolTipText(properties.getProperty("FSF_SSOCP_Chcb_Aggregation_1_N_TT",
					Constants.FSF_SSOCP_CHCB_AGGREGATION_1_N_TT));

			btnSelectAll.setText(properties.getProperty("FSF_SSOCP_BtnSelectAll", Constants.FSF_SSOCP_BTN_SELECT_ALL));
			btnDeselectAll
					.setText(properties.getProperty("FSF_SSOCP_BtnDeselectAll", Constants.FSF_SSOCP_BTN_DESELECT_ALL));
		}

		
		else {
			setBorder(BorderFactory.createTitledBorder(Constants.FSF_SSOCP_BORDER_TITLE));

			chcbClass.setText(Constants.FSF_SSOCP_CHCB_CLASS_TEXT);
			chcbComment.setText(Constants.FSF_SSOCP_CHCB_COMMENT_TEXT);
			chcbCommentEdge.setText(Constants.FSF_SSOCP_CHCB_COMMENT_EDGE_TEXT);
			chcbExtends.setText(Constants.FSF_SSOCP_CHCB_EXTENDS_TEXT);
			chcbImplements.setText(Constants.FSF_SSOCP_CHCB_IMPLEMENTS_TEXT);
			chcbAssociation.setText(Constants.FSF_SSOCP_CHCB_ASSOCIATION_TEXT);
			chcbAggregation_1_1.setText(Constants.FSF_SSOCP_CHCB_AGGREGATION_1_1_TEXT);
			chcbAggregation_1_N.setText(Constants.FSF_SSOCP_CHCB_AGGREGATION_1_N_TEXT);

			chcbClass.setToolTipText(Constants.FSF_SSOCP_CHCB_CLASS_TT);
			chcbComment.setToolTipText(Constants.FSF_SSOCP_CHCB_COMMENT_TT);
			chcbCommentEdge.setToolTipText(Constants.FSF_SSOCP_CHCB_COMMENT_EDGE_TT);
			chcbExtends.setToolTipText(Constants.FSF_SSOCP_CHCB_EXTENDS_TT);
			chcbImplements.setToolTipText(Constants.FSF_SSOCP_CHCB_IMPLEMENTS_TT);
			chcbAssociation.setToolTipText(Constants.FSF_SSOCP_CHCB_ASSOCIATION_TT);
			chcbAggregation_1_1.setToolTipText(Constants.FSF_SSOCP_CHCB_AGGREGATION_1_1_TT);
			chcbAggregation_1_N.setToolTipText(Constants.FSF_SSOCP_CHCB_AGGREGATION_1_N_TT);

			btnSelectAll.setText(Constants.FSF_SSOCP_BTN_SELECT_ALL);
			btnDeselectAll.setText(Constants.FSF_SSOCP_BTN_DESELECT_ALL);
		}
		
		
		chcbClass.setText("? " + chcbClass.getText());
		chcbComment.setText("? " + chcbComment.getText());
		chcbCommentEdge.setText("? " + chcbCommentEdge.getText());
		chcbExtends.setText("? " + chcbExtends.getText());
		chcbImplements.setText("? " + chcbImplements.getText());
		chcbAssociation.setText("? " + chcbAssociation.getText());
		chcbAggregation_1_1.setText("? " + chcbAggregation_1_1.getText());
		chcbAggregation_1_N.setText("? " + chcbAggregation_1_N.getText());
	}









	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JCheckBox)
			checkSelectedItems();
		
		
		
		else if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnSelectAll)
				setSelectedChcbs(true);
			
			
			else if (e.getSource() == btnDeselectAll)
				setSelectedChcbs(false);
			
			// Otestuji označené položky pro zpřístupnení / zneřístupnění tlačítek pro
			// označení:
			checkSelectedItems();
		}
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda jsou označené všechny chcbs, pokud ano, pak
	 * znepřístupní tlačítko označit vše.
	 * 
	 * Dále pokud není označen žádný chcb, tak bude tlačítko pro odznačení všech
	 * položek také nepřístupné, jinak budou ty tlačítka přístupná.
	 */
	private void checkSelectedItems() {
		// Je vše označeno:
		if (chcbClass.isSelected() && chcbComment.isSelected() && chcbCommentEdge.isSelected()
				&& chcbExtends.isSelected() && chcbImplements.isSelected() && chcbAssociation.isSelected()
				&& chcbAggregation_1_1.isSelected() && chcbAggregation_1_N.isSelected()) {
			btnSelectAll.setEnabled(false);
			btnDeselectAll.setEnabled(true);
		}

		// Nic není označeno:
		else if (!chcbClass.isSelected() && !chcbComment.isSelected() && !chcbCommentEdge.isSelected()
				&& !chcbExtends.isSelected() && !chcbImplements.isSelected() && !chcbAssociation.isSelected()
				&& !chcbAggregation_1_1.isSelected() && !chcbAggregation_1_N.isSelected()) {
			btnDeselectAll.setEnabled(false);
			btnSelectAll.setEnabled(true);
		}
		
		else {
			// Zde je alespoň nějaký chcb označen, ale nejsou označeny všechny:
			btnSelectAll.setEnabled(true);
			btnDeselectAll.setEnabled(true);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která označí nebo odznačí veškeré chcbs v tomto panelu, které značí
	 * nastavení velikosti písma pro nějaký objekt v diagramu tříd.
	 * 
	 * @param selected
	 *            true, pokud se mají všechny položky odznačit, jinak false.
	 */
	private void setSelectedChcbs(final boolean selected) {
		chcbClass.setSelected(selected);
		chcbComment.setSelected(selected);
		chcbCommentEdge.setSelected(selected);
		chcbExtends.setSelected(selected);
		chcbImplements.setSelected(selected);
		chcbAssociation.setSelected(selected);
		chcbAggregation_1_1.setSelected(selected);
		chcbAggregation_1_N.setSelected(selected);
	}
}
