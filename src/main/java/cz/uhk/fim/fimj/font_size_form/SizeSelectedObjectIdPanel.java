package cz.uhk.fim.fimj.font_size_form;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;

/**
 * Tato třída slouží jako panel, který obsahuje komponenty typu JCheckBox a tyto chcbs značí objekty v diagramu instancí
 * u kterých se bude manipulovat s velikostí písma / fontu.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class SizeSelectedObjectIdPanel extends FontSizePanelAbstract implements LanguageInterface, ActionListener {

	private static final long serialVersionUID = 1L;

	

	// Komponenty, které značí, v jakém objektu v diagramu instancí se má nastavit
	// velikost písma.
	private final JCheckBox chcbInstance;
	private final JCheckBox chcbAssociation;
	private final JCheckBox chcbAggregation;
	
	
	// Komponenty pro nastavení velikosti písma pro atributy a metody v buňce /
	// instanci:
	private final JCheckBox chcbInstanceAttributes;
	private final JCheckBox chcbInstancesMethods;
	
	
	
	
	
	// Tlačítka pro označit a odznačit vše:
    private final JButton btnSelectAll;
    private final JButton btnDeselectAll;
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param defaultProp
	 *            - načtený soubor Default.properties, který je zde potřeba akorát
	 *            pro načtení výchozích hodnot.
	 */
	SizeSelectedObjectIdPanel(final Properties defaultProp) {
		super();
				
		setLayout(new GridBagLayout());
		
		index = 0;
		
		gbc = createGbc();
		
		
		chcbInstance = new JCheckBox();
		chcbInstance.addActionListener(this);
		setGbc(index, 0, chcbInstance);
		
		/*
		 * Panel, do kterého vložím komponenty pro označení, zda se mají nastavovat i
		 * velikosti písma pro font a metody v buňce pro reprezentaci instance v
		 * diagramu instancí.
		 * 
		 * Tento panel je potřeba pouze pro to, že chci, aby ty komponenty byly o trochu
		 * posunuté od kraje.
		 */
		final JPanel pnlChcbsAttributesMethods = new JPanel();
		pnlChcbsAttributesMethods.setLayout(new BoxLayout(pnlChcbsAttributesMethods, BoxLayout.PAGE_AXIS));
		
		chcbInstanceAttributes = new JCheckBox();
		chcbInstanceAttributes.addActionListener(this);
		
		chcbInstancesMethods = new JCheckBox();
		chcbInstancesMethods.addActionListener(this);
		
		pnlChcbsAttributesMethods.add(Box.createRigidArea(new Dimension(15,0)));
		pnlChcbsAttributesMethods.add(chcbInstanceAttributes);
		pnlChcbsAttributesMethods.add(Box.createRigidArea(new Dimension(15,0)));
		pnlChcbsAttributesMethods.add(chcbInstancesMethods);
		
		setGbc(++index, 0, pnlChcbsAttributesMethods);
		
		
		
		
		chcbAssociation = new JCheckBox();
		chcbAssociation.addActionListener(this);
		setGbc(++index, 0, chcbAssociation);
		
		chcbAggregation = new JCheckBox();
		chcbAggregation.addActionListener(this);
		setGbc(++index, 0, chcbAggregation);
		
		
		
		
		
		
		
		
		// Zde nastavím výchozí hodnoty:
		if (defaultProp != null) {
			chcbInstance.setSelected(Boolean.parseBoolean(defaultProp.getProperty("FontSizeForm_ID_InstanceCell",
					String.valueOf(Constants.FSF_ID_INSTANCE_CELL))));

			chcbInstanceAttributes
					.setSelected(Boolean.parseBoolean(defaultProp.getProperty("FontSizeForm_ID_InstanceCell_Attributes",
							String.valueOf(Constants.FSF_ID_INSTANCE_CELL_ATTRIBUTES))));

			chcbInstancesMethods
					.setSelected(Boolean.parseBoolean(defaultProp.getProperty("FontSizeForm_ID_InstanceCell_Methods",
							String.valueOf(Constants.FSF_ID_INSTANCE_CELL_METHODS))));

			chcbAssociation.setSelected(Boolean.parseBoolean(defaultProp.getProperty("FontSizeForm_ID_AssociationEdge",
					String.valueOf(Constants.FSF_ID_ASSOCIATION_EDGE))));

			chcbAggregation.setSelected(Boolean.parseBoolean(defaultProp.getProperty("FontSizeForm_ID_AggregationEdge",
					String.valueOf(Constants.FSF_ID_AGGREGATION_EDGE))));
		}
		
		
		else {
			// Zde využiji honoty z Constants:

			chcbInstance.setSelected(Constants.FSF_ID_INSTANCE_CELL);

			chcbInstanceAttributes.setSelected(Constants.FSF_ID_INSTANCE_CELL_ATTRIBUTES);

			chcbInstancesMethods.setSelected(Constants.FSF_ID_INSTANCE_CELL_METHODS);

			chcbAssociation.setSelected(Constants.FSF_ID_ASSOCIATION_EDGE);

			chcbAggregation.setSelected(Constants.FSF_ID_AGGREGATION_EDGE);
		}
		
		
		
		
		
		
		
		addSpace(++index);
		
		
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.CENTER));
		
		btnSelectAll = new JButton();
		btnDeselectAll = new JButton();
		
		btnSelectAll.addActionListener(this);
		btnDeselectAll.addActionListener(this);
		
		pnlButtons.add(btnSelectAll);
		pnlButtons.add(btnDeselectAll);
		
		setGbc(++index, 0, pnlButtons);
		
		
		
		checkSelectedItems();
	}
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro vrácení logické hodnoty, která značí, zda je
	 * komponenta typu JCheckBox označena nebo ne. Značí, zda se má nastavit
	 * velikost písma / fontu pro buňku reprezentující instanci v diagramu instancí.
	 * 
	 * @return popsáno výše.
	 */
	final boolean isChcbInstanceSelected() {
		return chcbInstance.isSelected();
	}

	/**
	 * Metoda, která slouží pro vrácení logické hodnoty, která značí, zda je
	 * komponenta typu JCheckBox označena nebo ne. Značí, zda se má nastavit
	 * velikost písma / fontu pro atributy, které mohou být vypsány v buňce
	 * reprezentující instanci v diagramu instancí.
	 * 
	 * @return popsáno výše.
	 */
	final boolean isChcbInstanceAttributesSelected() {
		return chcbInstanceAttributes.isSelected();
	}

	/**
	 * Metoda, která slouží pro vrácení logické hodnoty, která značí, zda je
	 * komponenta typu JCheckBox označena nebo ne. Značí, zda se má nastavit
	 * velikost písma / fontu pro metody, které mohou být vypsány v buňce
	 * reprezentující instanci v diagramu instancí.
	 * 
	 * @return popsáno výše.
	 */
	final boolean isChcbInstanceMethodsSelected() {
		return chcbInstancesMethods.isSelected();
	}

	/**
	 * Metoda, která slouží pro vrácení logické hodnoty, která značí, zda je
	 * komponenta typu JCheckBox označena nebo ne. Značí, zda se má nastavit
	 * velikost písma / fontu pro hranu reprezentující vztah typu asociace v
	 * diagramu instancí.
	 * 
	 * @return popsáno výše.
	 */
	final boolean isChcbAssociationSelected() {
		return chcbAssociation.isSelected();
	}

	/**
	 * Metoda, která slouží pro vrácení logické hodnoty, která značí, zda je
	 * komponenta typu JCheckBox označena nebo ne. Značí, zda se má nastavit
	 * velikost písma / fontu pro hranu reprezentující vztah typu agregace v
	 * diagramu instancí.
	 * 
	 * @return popsáno výše.
	 */
	final boolean isChcbAggregationSelected() {
		return chcbAggregation.isSelected();
	}
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public void setLanguage(Properties properties) {
		if (properties != null) {
			setBorder(BorderFactory.createTitledBorder(
					properties.getProperty("FSF_SSOIP_BorderTitle", Constants.FSF_SSOIP_BORDER_TITLE)));

			chcbInstance.setText(
					properties.getProperty("FSF_SSOIP_Chcb_Instance_Text", Constants.FSF_SSOIP_CHCB_INSTANCE_TEXT));
			chcbInstanceAttributes.setText(properties.getProperty("FSF_SSOIP_Chcb_InstanceAttributes_Text",
					Constants.FSF_SSOIP_CHCB_INSTANCE_ATTRIBUTES_TEXT));
			chcbInstancesMethods.setText(properties.getProperty("FSF_SSOIP_Chcb_InstanceMethods_Text",
					Constants.FSF_SSOIP_CHCB_INSTANCE_METHODS_TEXT));

			chcbAssociation.setText(properties.getProperty("FSF_SSOIP_Chcb_Association_Text",
					Constants.FSF_SSOIP_CHCB_ASSOCIATION_TEXT));
			chcbAggregation.setText(properties.getProperty("FSF_SSOIP_Chcb_Aggregation_Text",
					Constants.FSF_SSOIP_CHCB_AGGREGATION_TEXT));

			chcbInstance.setToolTipText(
					properties.getProperty("FSF_SSOIP_Chcb_Instance_TT", Constants.FSF_SSOIP_CHCB_INSTANCE_TT));
			chcbInstanceAttributes.setToolTipText(properties.getProperty("FSF_SSOIP_Chcb_InstanceAttributes_TT",
					Constants.FSF_SSOIP_CHCB_INSTANCE_ATTRIBUTES_TT));
			chcbInstancesMethods.setToolTipText(properties.getProperty("FSF_SSOIP_Chcb_InstanceMethods_TT",
					Constants.FSF_SSOIP_CHCB_INSTANCE_METHODS_TT));

			chcbAssociation.setToolTipText(
					properties.getProperty("FSF_SSOIP_Chcb_Association_TT", Constants.FSF_SSOIP_CHCB_ASSOCIATION_TT));
			chcbAggregation.setToolTipText(
					properties.getProperty("FSF_SSOIP_Chcb_Aggregation_TT", Constants.FSF_SSOIP_CHCB_AGGREGATION_TT));

			btnSelectAll.setText(properties.getProperty("FSF_SSOIP_BtnSelectAll", Constants.FSF_SSOIP_BTN_SELECT_ALL));
			btnDeselectAll.setText(properties.getProperty("FSF_SSOIP_DeselectAll", Constants.FSF_SSOIP_BTN_DELECT_ALL));
		}

		else {
			setBorder(BorderFactory.createTitledBorder(Constants.FSF_SSOIP_BORDER_TITLE));

			chcbInstance.setText(Constants.FSF_SSOIP_CHCB_INSTANCE_TEXT);
			chcbInstanceAttributes.setText(Constants.FSF_SSOIP_CHCB_INSTANCE_ATTRIBUTES_TEXT);
			chcbInstancesMethods.setText(Constants.FSF_SSOIP_CHCB_INSTANCE_METHODS_TEXT);
			chcbAssociation.setText(Constants.FSF_SSOIP_CHCB_ASSOCIATION_TEXT);
			chcbAggregation.setText(Constants.FSF_SSOIP_CHCB_AGGREGATION_TEXT);

			chcbInstance.setToolTipText(Constants.FSF_SSOIP_CHCB_INSTANCE_TT);
			chcbInstanceAttributes.setToolTipText(Constants.FSF_SSOIP_CHCB_INSTANCE_ATTRIBUTES_TT);
			chcbInstancesMethods.setToolTipText(Constants.FSF_SSOIP_CHCB_INSTANCE_METHODS_TT);
			chcbAssociation.setToolTipText(Constants.FSF_SSOIP_CHCB_ASSOCIATION_TT);
			chcbAggregation.setToolTipText(Constants.FSF_SSOIP_CHCB_AGGREGATION_TT);

			btnSelectAll.setText(Constants.FSF_SSOIP_BTN_SELECT_ALL);
			btnDeselectAll.setText(Constants.FSF_SSOIP_BTN_DELECT_ALL);
		}
		
		
		chcbInstance.setText("? " + chcbInstance.getText());
		chcbInstanceAttributes.setText("? " + chcbInstanceAttributes.getText());
		chcbInstancesMethods.setText("? " + chcbInstancesMethods.getText());
		chcbAssociation.setText("? " + chcbAssociation.getText());
		chcbAggregation.setText("? " + chcbAggregation.getText());
	}


	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda jsou označené všechny chcbs, pokud ano, pak
	 * znepřístupní tlačítko označit vše.
	 * 
	 * Dále pokud není označen žádný chcb, tak bude tlačítko pro odznačení všech
	 * položek také nepřístupné, jinak budou ty tlačítka přístupná.
	 */
	private void checkSelectedItems() {
		// Je vše označeno:
		if (chcbInstance.isSelected() && chcbInstanceAttributes.isSelected() && chcbInstancesMethods.isSelected()
				&& chcbAssociation.isSelected() && chcbAggregation.isSelected()) {
			btnSelectAll.setEnabled(false);
			btnDeselectAll.setEnabled(true);
		}

		// Nic není označeno:
		else if (!chcbInstance.isSelected() && !chcbInstanceAttributes.isSelected()
				&& !chcbInstancesMethods.isSelected() && !chcbAssociation.isSelected()
				&& !chcbAggregation.isSelected()) {
			btnDeselectAll.setEnabled(false);
			btnSelectAll.setEnabled(true);
		}
		
		else {
			// Zde je alespoň nějaký chcb označen, ale nejsou označeny všechny:
			btnSelectAll.setEnabled(true);
			btnDeselectAll.setEnabled(true);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která označí nebo odznačí veškeré chcbs v tomto panelu, které značí
	 * nastavení velikosti písma pro nějaký objekt v diagramu tříd.
	 * 
	 * @param selected
	 *            true, pokud se mají všechny položky odznačit, jinak false.
	 */
	private void setSelectedChcbs(final boolean selected) {
		chcbInstance.setSelected(selected);
		chcbInstanceAttributes.setSelected(selected);
		chcbInstancesMethods.setSelected(selected);
		chcbAggregation.setSelected(selected);
		chcbAssociation.setSelected(selected);
	}
	
	
	
	
	




	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JCheckBox)
			checkSelectedItems();

		
		else if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnSelectAll)
				setSelectedChcbs(true);

			
			else if (e.getSource() == btnDeselectAll)
				setSelectedChcbs(false);

			
			// Otestuji označené položky pro zpřístupnení / zneřístupnění tlačítek pro
			// označení:
			checkSelectedItems();
		}
	}
}
