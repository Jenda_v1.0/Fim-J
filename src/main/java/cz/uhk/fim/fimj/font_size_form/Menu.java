package cz.uhk.fim.fimj.font_size_form;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Properties;

import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.KeyStroke;

import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.GetIconInterface;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;

/**
 * Tato třída slouží jako menu pro dialog, ve kterém si uživatel může nastavit "rychlým" způsobem velikost fontu / písma
 * vybraných objektů v diagramu tříd a instancí.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class Menu extends JMenuBar implements LanguageInterface, ActionListener {

	private static final long serialVersionUID = 1L;

	
	
	/**
	 * Tato komponenta je v podstatě Menu, které se vkládá do tohoto konkrétního
	 * menu v dialogu, tato položka bude obsahovat níže uvedené položky pro zavření
	 * a rozervírací menu pro nastavení komponent pro nastavení velkosti fontu /
	 * písma
	 */
	private final JMenu menuDialog;
	
	
	
	
	/**
	 * Rozevírací menu, které bude obsahovat komponenty pro označení aktuálně
	 * zvoleného okna, resp. okna, které bude obsahovat nějaké komponenty pro nějaké
	 * nastavení velikost.
	 * 
	 * Tj. velikost písma / fontu pro vechny objekty stejně, nebo pro označené
	 * objekty, ...
	 */
	private final JMenu menuKindOfForm;
	

	
	
	// Položky pro nastavení komponent v dialogu pro nastavení příslušných
	// velikostí.
	private final JRadioButtonMenuItem rbKindItem_OneSameSize, rbKindItem_OneSizeForChoosedObjects,
			rbKindItem_OneSizeForEachObject;
	
	
	
	
	
	
	/**
	 * Položka pro uzavření dialogu pro nastavení velikosti písma / fontu objektům v
	 * diagramu tříd a instancí.
	 */
	private final JMenuItem itemClose;
	
	
	
	
	/**
	 * Komponenta, která slouží pro nastavení toho, zda se má nastavit velikost
	 * písma příslušných objektů až po tom, co uživatel přestane hýbat s posuvníkem,
	 * tj. nastaví velikost nebo při každém posunutí posuvníku.
	 */
	private final JRadioButtonMenuItem rbSetFontsWhileMoving;
	
	
	
	
	
	/**
	 * Položka, která slouží pro obnovení výchozích velikostí pro veškeré velikosti
	 * písem ve všech objektech v diagramu tříd a v diagram instancí.
	 */
	private final JMenuItem itemRestoreDefault;
	
	
	
	
	
	
	
	
	/**
	 * Reference na dialog, ve kterém je toto menu, je zde potřeba pouze kvůli
	 * zavolání metody pro zavření dialogu a změny panelu s komponentami pro
	 * nastavení velikostí - dle označeného rb.
	 */
	private final FontSizeForm dialog;
	
	
	
	
	
	
	/**
	 * Reference na diagram tříd.
	 */
	private final GraphClass classDiagram;

	/**
	 * Referencena diagram instancí.
	 */
	private final GraphInstance instanceDiagram;
	
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param formType
	 *            - výčtová hodnota, která značí sadu komponent, která je v okně
	 *            dialogu.
	 * 
	 * @param dialog
	 *            - dialog, ve kterém je toto menu, je zde potřeba pouze kvůli
	 *            metodě pro zavření dialogu a nastavení komponent pro nastavení
	 *            velikostí - když uživatel změní uspořádání komponent, že chce jiné
	 *            komponenty apod.
	 * 
	 * @param defaultValueForMoving
	 *            - logická hodnota, která značí pouze výchozí nastavenou hodnotu o
	 *            tom, zda se má aktualizovat velikost písma v příslušných objektech
	 *            při každém posunutí posuvníku, nebo až při finálním nastavení, tj.
	 *            až s ním uživatel přestane hýbat.
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd.
	 * 
	 * @param instanceDiagram
	 *            - reference na diagram instancí.
	 */
	public Menu(final FontSizeFormType formType, final FontSizeForm dialog, final boolean defaultValueForMoving,
			final GraphClass classDiagram, final GraphInstance instanceDiagram) {
		super();
		
		
		this.dialog = dialog;
		this.classDiagram = classDiagram;
		this.instanceDiagram = instanceDiagram;
		
		
		
		// inicializace rozevíracích menu:
		menuDialog = new JMenu();
		menuKindOfForm = new JMenu();
		
		
		
		// Vytvoření položek do rozevíracího menu:
		rbKindItem_OneSameSize = new JRadioButtonMenuItem();
		rbKindItem_OneSizeForChoosedObjects = new JRadioButtonMenuItem();
		rbKindItem_OneSizeForEachObject = new JRadioButtonMenuItem();
		
		// Přidání posluchačům pro komponenty v rozevíracím menu:
		rbKindItem_OneSameSize.addActionListener(this);
		rbKindItem_OneSizeForChoosedObjects.addActionListener(this);
		rbKindItem_OneSizeForEachObject.addActionListener(this);
		
		// přidání položek do rozervíracího menu:
		menuKindOfForm.add(rbKindItem_OneSameSize);
		menuKindOfForm.addSeparator();
		menuKindOfForm.add(rbKindItem_OneSizeForChoosedObjects);
		menuKindOfForm.addSeparator();
		menuKindOfForm.add(rbKindItem_OneSizeForEachObject);
		
		
		/*
		 * Díky této skupině půjde označit vždy jen jeden rb.
		 */
		final ButtonGroup group = new ButtonGroup();
		group.add(rbKindItem_OneSameSize);
		group.add(rbKindItem_OneSizeForChoosedObjects);
		group.add(rbKindItem_OneSizeForEachObject);
		
		
		/*
		 * Výchozí označení.
		 */
		if (formType == FontSizeFormType.ONE_SAME_SIZE)
			rbKindItem_OneSameSize.setSelected(true);
		
		else if (formType == FontSizeFormType.ONE_SIZE_FOR_CHOOSED_OBJECTS)
			rbKindItem_OneSizeForChoosedObjects.setSelected(true);
		
		else if (formType == FontSizeFormType.ONE_SIZE_FOR_EACH_OBJECT)
			rbKindItem_OneSizeForEachObject.setSelected(true);
		
		
		
		
		
		
		// Položka pro nastavení toho, zda se mají nastavovat velikosti písem během
		// posouvání posuvníku
		rbSetFontsWhileMoving = new JRadioButtonMenuItem();
		rbSetFontsWhileMoving.setSelected(defaultValueForMoving);

		rbSetFontsWhileMoving.addActionListener(this);	
		
		
		
		
		
		
		
		// Inicializace položky pro obnovení výchozích velikostí:
		itemRestoreDefault = new JMenuItem();

		// Ikona:
		itemRestoreDefault.setIcon(GetIconInterface.getMyIcon("/icons/app/RestoreDefaultIcon.png", true));

		// Zkratka: Ctrl + R
		itemRestoreDefault.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_DOWN_MASK));

		// Přidání posluchače na kliknutí:
		itemRestoreDefault.addActionListener(this);
		
		
		
		
		
		
		
		
		
		
		// Inicializace položky pro zavření dialogu.
		itemClose = new JMenuItem();

		// Ikona:
		itemClose.setIcon(GetIconInterface.getMyIcon("/icons/app/CloseIcon.png", true));

		// Zkratka: Ctrl + W
		itemClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_DOWN_MASK));

		// Přidání události pro položku na zavření dialgou.
		itemClose.addActionListener(this);
		
		
		
		
		
		// Přidání položek do menu:		
		menuDialog.add(menuKindOfForm);
		menuDialog.addSeparator();
		menuDialog.add(rbSetFontsWhileMoving);
		menuDialog.addSeparator();
		menuDialog.add(itemRestoreDefault);
		menuDialog.addSeparator();
		menuDialog.add(itemClose);
		
		
		
		// Přidání menu do menu dialogu:
		add(menuDialog);
	}


	
	
	
	
	
	


	@Override
	public void setLanguage(final Properties properties) {
		FontSizePanelAbstract.setLanguageForErrorText(properties);
		
		if (properties != null) {
			menuDialog.setText(properties.getProperty("FSF_Menu_MenuDialog", Constants.FSF_MENU_MENU_DIALOG));
			menuKindOfForm.setText(properties.getProperty("FSF_Menu_KindOfForm", Constants.FSF_MENU_KIND_OF_FORM));
			
			
			rbKindItem_OneSameSize.setText(properties.getProperty("FSF_Menu_Rb_KindItemOneSameSizeText", Constants.FSF_MENU_RB_KIND_ITEM_ONE_SAME_SIZE_TEXT));
			rbKindItem_OneSizeForChoosedObjects.setText(properties.getProperty("FSF_Menu_Rb_KindItemOneSizeForChoosedObjectsText", Constants.FSF_MENU_RB_KIND_ITEM_ONE_SIZE_FOR_CHOOSED_OBJECTS_TEXT));
			rbKindItem_OneSizeForEachObject.setText(properties.getProperty("FSF_Menu_Rb_KindItemOneSizeForEachObjectText", Constants.FSF_MENU_RB_KIND_ITEM_ONE_SIZE_FOR_EACH_OBJECT_TEXT));
			
			rbKindItem_OneSameSize.setToolTipText(properties.getProperty("FSF_Menu_Rb_KindItemOneSameSize_TT", Constants.FSF_MENU_RB_KIND_ITEM_ONE_SAME_SIZE_TT));
			rbKindItem_OneSizeForChoosedObjects.setToolTipText(properties.getProperty("FSF_Menu_Rb_KindItemOneSizeForChoosedObjects_TT", Constants.FSF_MENU_RB_KIND_ITEM_ONE_SIZE_FOR_CHOOSED_OBJECTS_TT));
			rbKindItem_OneSizeForEachObject.setToolTipText(properties.getProperty("FSF_Menu_Rb_KindItemOneSizeForEachObject_TT", Constants.FSF_MENU_RB_KIND_ITEM_ONE_SIZE_FOR_EACH_OBJECT_TT));
			
			
			
			
			rbSetFontsWhileMoving.setText(properties.getProperty("FSF_Menu_Rb_SetFontsWhileMovingText", Constants.FSF_MENU_RB_SET_FONTS_WHILE_MOVING_TEXT));			
			rbSetFontsWhileMoving.setToolTipText(properties.getProperty("FSF_Menu_Rb_SetFontsWhileMoving_TT", Constants.FSF_MENU_RB_SET_FONTS_WHILE_MOVING_TT));
			
			
			
			itemRestoreDefault.setText(properties.getProperty("FSF_Menu_ItemRestoreDefault_Text", Constants.FSF_MENU_ITEM_RESTORE_DEFAULT_TEXT));
			itemRestoreDefault.setToolTipText(properties.getProperty("FSF_Menu_ItemRestoreDefault_TT", Constants.FSF_MENU_ITEM_RESTORE_DEFAULT_TT));
			
			
			
			itemClose.setText(properties.getProperty("FSF_Menu_ItemClose_Text", Constants.FSF_MENU_ITEM_CLOSE_TEXT));
			itemClose.setToolTipText(properties.getProperty("FSF_Menu_ItemClose_TT", Constants.FSF_MENU_ITEM_CLOSE_TT));
		}
		
		
		
		else {
			menuDialog.setText(Constants.FSF_MENU_MENU_DIALOG);
			menuKindOfForm.setText(Constants.FSF_MENU_KIND_OF_FORM);
			
			
			rbKindItem_OneSameSize.setText(Constants.FSF_MENU_RB_KIND_ITEM_ONE_SAME_SIZE_TEXT);
			rbKindItem_OneSizeForChoosedObjects.setText(Constants.FSF_MENU_RB_KIND_ITEM_ONE_SIZE_FOR_CHOOSED_OBJECTS_TEXT);
			rbKindItem_OneSizeForEachObject.setText(Constants.FSF_MENU_RB_KIND_ITEM_ONE_SIZE_FOR_EACH_OBJECT_TEXT);
			
			rbKindItem_OneSameSize.setToolTipText(Constants.FSF_MENU_RB_KIND_ITEM_ONE_SAME_SIZE_TT);
			rbKindItem_OneSizeForChoosedObjects.setToolTipText(Constants.FSF_MENU_RB_KIND_ITEM_ONE_SIZE_FOR_CHOOSED_OBJECTS_TT);
			rbKindItem_OneSizeForEachObject.setToolTipText(Constants.FSF_MENU_RB_KIND_ITEM_ONE_SIZE_FOR_EACH_OBJECT_TT);
			
			
			
			
			rbSetFontsWhileMoving.setText(Constants.FSF_MENU_RB_SET_FONTS_WHILE_MOVING_TEXT);	
			rbSetFontsWhileMoving.setToolTipText(Constants.FSF_MENU_RB_SET_FONTS_WHILE_MOVING_TT);
			
			
			
			itemRestoreDefault.setText(Constants.FSF_MENU_ITEM_RESTORE_DEFAULT_TEXT);
			itemRestoreDefault.setToolTipText(Constants.FSF_MENU_ITEM_RESTORE_DEFAULT_TT);
			
			
			
			itemClose.setText(Constants.FSF_MENU_ITEM_CLOSE_TEXT);
			itemClose.setToolTipText(Constants.FSF_MENU_ITEM_CLOSE_TT);
		}
	}



	
	
	





	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JRadioButtonMenuItem) {
			/*
			 * Dle příslušné komponenty odeberu z dialogu příslušný panel a nastavím do něj
			 * ten zvolený.
			 */
			if (e.getSource() == rbKindItem_OneSameSize) {
				dialog.setSameSizePanel();

				FontSizeForm.setFormType(FontSizeFormType.ONE_SAME_SIZE);
			}

			else if (e.getSource() == rbKindItem_OneSizeForChoosedObjects) {
				dialog.setSameSizeForSelectedObjectsPnl();

				FontSizeForm.setFormType(FontSizeFormType.ONE_SIZE_FOR_CHOOSED_OBJECTS);
			}

			else if (e.getSource() == rbKindItem_OneSizeForEachObject) {
				dialog.setSameSizeForEachObjectPnl();

				FontSizeForm.setFormType(FontSizeFormType.ONE_SIZE_FOR_EACH_OBJECT);
			}
			
			
			
			
			else if (e.getSource() == rbSetFontsWhileMoving)
				FontSizeForm.setSetValuesWhileMoving(rbSetFontsWhileMoving.isSelected());
		}
		
		
		
		else if (e.getSource() instanceof JMenuItem) {
			if (e.getSource() == itemClose)
				dialog.dispose();
			
			
			else if (e.getSource() == itemRestoreDefault) {
				FontSizePanelAbstract.FONT_SIZE_OPERATION_CD.quickSetupDefaultFontSizes(classDiagram);
				FontSizePanelAbstract.FONT_SIZE_OPERATION_ID.quickSetupDefaultFontSizes(instanceDiagram);
				
				GraphClass.setClassFontSize(Constants.CD_FONT_CLASS.getSize());
				GraphInstance.setInstanceFontSize(Constants.ID_FONT.getSize());
				
				
				
				
				/*
				 * Testování duplicit.
				 * 
				 * Výše se mohly nastavit výchozí velkosti písma, jenže v případě, že jsou
				 * stejné všechny vlastnosti některých hran, tak se nastaví na výchozí velikosti
				 * a budou některé vztahy identické, tak se níže musí opět otestovat duplicity a
				 * případně nastavit "nouzové" výchozí velikosti písma, aby byly různé -
				 * identické.
				 */
				
				// Zda jsou stejné buňky pro třídu a komentář v CD:
				if (FontSizePanelAbstract.checkDuplicateClassAndCommentProperties(classDiagram,
						FontSizeForm.CHECK_DUPLICATES_IN_DIAGRAMS))
					// Nastavím i výchozí font pro klávesové zkratky
					GraphClass.setClassFontSize(Constants.CD_FONT_CLASS.getSize());					


				// zde otestuji, zda jsou vybrané / některé hrany stejné:
				FontSizePanelAbstract.checkDuplicateEdgesInCd(classDiagram, FontSizeForm.CHECK_DUPLICATES_IN_DIAGRAMS);

				// Zde otestuji hrany pro asociace a agregaci v diagramu instancí:
				FontSizePanelAbstract.checkDuplicateEdgesInId(instanceDiagram,
						FontSizeForm.CHECK_DUPLICATES_IN_DIAGRAMS);
				
				
				
				
				/*
				 * Následuje část pro přenačtení dialogu, jelikož se výše nastavily výchozí
				 * hodnoty a nebo ty "nouzové" výchozí hodnoty, tak potřebuji přenačíst označený
				 * panel v dialogu, aby si načetl ty aktuální hodnoty.
				 */
				final FontSizeFormType type = FontSizeForm.getFormType();

				if (type.equals(FontSizeFormType.ONE_SAME_SIZE))
					dialog.setSameSizePanel();

				else if (type.equals(FontSizeFormType.ONE_SIZE_FOR_CHOOSED_OBJECTS))
					dialog.setSameSizeForSelectedObjectsPnl();

				else if (type.equals(FontSizeFormType.ONE_SIZE_FOR_EACH_OBJECT))
					dialog.setSameSizeForEachObjectPnl();
			}
		}
	}
}
