package cz.uhk.fim.fimj.font_size_form;

import java.awt.Font;

/**
 * Tato třída slouží jako "objekt", který obsahuje informace o některých hranách, které se mohou vyskytovat v diagramu
 * tříd.
 * <p>
 * Jde o to, že když uživatel využije "rychlý" způsob pro změnu veliosti písma, tak potřebuji otestovat, zda se v
 * diagramu tříd (a také instnací) nevyskytují objekty, které mají stejné vlastnosti, protože by se jinak nerozeznali,
 * proto je zde tento objekt, který jsem využil pro testování duplicit některých typů hran, resp. jejich vlastností.
 * <p>
 * Jedná se o objekt, který obsahuje informace o hranách, které lze nastavit uživatelem a díky kterým se rozeznávají v
 * diagramu tříd, například na jaký objekt uživatel klikl apod.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class EdgeOfClassDiagramInfo {

    /**
     * Výčtová hodnota, která značí typ hrany / vztahu, který se může vyskytovat v diagramu tříd a zároveň je u něj
     * třeba testovat jeho vlastnosti a srovnávat je s ostatními kvůli duplicitním hodnotám, aby nevypadaly nějaké
     * hrany
     * stejně, pak by pro aplikaci nebylo možné je rozeznat.
     */
    private final KindOfEdgeEnum kindOfEdge;

    /**
     * Barva hrany.
     */
    private final int lineColor;

    /**
     * Zda jsou texty / labely umístěny podél hrany.
     */
    private final boolean labelsAlongEdge;

    /**
     * Styl hrany.
     */
    private final int lineStyle;

    /**
     * Typ konce hrany, například šipka, nic, kosočtverec, ...
     */
    private final int lineEnd;

    /**
     * Typ začátku hrany.
     */
    private final int lineBegin;

    /**
     * Šířka hrany.
     */
    private final float lineWidth;

    /**
     * Zda je konec hrany vyplněn, například v případě šipky nebo kosočtverce apod. Tak, zda je ten tvar vyplněn nebo
     * ne.
     */
    private final boolean endFill;

    /**
     * Zda je začátek hrany vyplněn. Například, když je typ začátku hrany šipka nebo kosočtverec apod. Tak, zda je ten
     * tvar vyplněn nebo ne.
     */
    private final boolean beginFill;

    /**
     * Barva písma na hraně.
     */
    private final int fontColor;

    /**
     * Font písma hrany.
     */
    private final Font font;


    /**
     * Konstruktor této třídy.
     *
     * @param kindOfEdge
     *         - typ hrany.
     * @param lineColor
     *         - barva hrany.
     * @param labelsAlongEdge
     *         - zda jsou texty na hraně umístěny podél hrany nebo ne.
     * @param lineStyle
     *         - index stylu hrany.
     * @param lineEnd
     *         - typ, resp. index typu / stylu konce hrany.
     * @param lineBegin
     *         - typ, resp. index typu / stylu začátku hrany.
     * @param lineWidth
     *         - šířka hrany.
     * @param endFill
     *         - zda je konec hrany, resp. tvar nebo objekt na konci hrany vyplněn nebo ne.
     * @param beginFill
     *         - zda je tvar / objekt na začátku hrany vyplněn nebo ne.
     * @param fontColor
     *         - barva písma na hraně.
     * @param font
     *         - font písma hrany.
     */
    EdgeOfClassDiagramInfo(final KindOfEdgeEnum kindOfEdge, final int lineColor, final boolean labelsAlongEdge,
                           final int lineStyle, final int lineEnd, final int lineBegin, final float lineWidth, final
						   boolean endFill,
                           final boolean beginFill, final int fontColor, final Font font) {

        super();

        this.kindOfEdge = kindOfEdge;
        this.lineColor = lineColor;
        this.labelsAlongEdge = labelsAlongEdge;
        this.lineStyle = lineStyle;
        this.lineEnd = lineEnd;
        this.lineBegin = lineBegin;
        this.lineWidth = lineWidth;
        this.endFill = endFill;
        this.beginFill = beginFill;
        this.fontColor = fontColor;
        this.font = font;
    }


    KindOfEdgeEnum getKindOfEdge() {
        return kindOfEdge;
    }

    int getLineColor() {
        return lineColor;
    }

    boolean isLabelsAlongEdge() {
        return labelsAlongEdge;
    }

    public int getLineStyle() {
        return lineStyle;
    }

    public int getLineEnd() {
        return lineEnd;
    }

    public int getLineBegin() {
        return lineBegin;
    }

    public float getLineWidth() {
        return lineWidth;
    }

    boolean isEndFill() {
        return endFill;
    }

    boolean isBeginFill() {
        return beginFill;
    }

    int getFontColor() {
        return fontColor;
    }

    public Font getFont() {
        return font;
    }
}
