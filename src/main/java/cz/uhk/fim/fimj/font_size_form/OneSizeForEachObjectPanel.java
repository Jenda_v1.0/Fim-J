package cz.uhk.fim.fimj.font_size_form;

import java.awt.GridBagLayout;
import java.util.Properties;

import javax.swing.JLabel;
import javax.swing.JSlider;

import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;

/**
 * Tato třída slouží jako panel, který slouží pro nastavení velikostí pro veškeré komponenty v diagramu tříd a v
 * diagramu instnací.
 * <p>
 * Velikost příslušných objektů v uvedených diagramech lze nastavit individuálně pro každý objekt v diagramu.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class OneSizeForEachObjectPanel extends FontSizePanelAbstract implements LanguageInterface {

	private static final long serialVersionUID = 1L;

	
	
	
	// Labely s texty diagram tříd a instancí:
	private final JLabel lblClassDiagram, lblInstanceDiagram;
	
	
	
	// Typy komponent v diagramu tříd:
	private final JLabel lblClass, lblComment, lblCommentEdge, lblAssociation, lblExtends, lblImplements,
			lblAggregation_1_1, lblAggregation_1_N;
	
	
	
	
	// Typy komponent v diagramu instancí:
	private final JLabel lblInstance, lblInstanceAttributes, lblInstanceMethods, lblInstanceAssociation,
			lblInstanceAggregation;
	
	
	
	
	
	// Labely pro texty s nastavenou velikostí pro diagram tříd:
	private final JLabel lblClassSize, lblCommentSize, lblCommentEdgeSize, lblAssociationSize, lblExtendsSize,
			lblImplementsSize, lblAggregation_1_1_Size, lblAggregation_1_N_Size;
	
	
	
	
	// Labely pro texty s nastavenou velikosti pro diagram instancí:
	private final JLabel lblInstanceSize, lblInstanceAttributesSize, lblInstanceMethodsSize, lblInstanceAssociationSize,
			lblAggregationSize;
	
	
	
	
	
	
	
	/**
	 * reference na diagram tříd.
	 */
	private final GraphClass classDiagram;

	/**
	 * Reference na diagram intancí.
	 */
	private final GraphInstance instanceDiagram;
	
	
	
	
	
	
	
	
	
	// Výchozí hodnoty - při prvním načtení dialogu pro diagram tříd:
	private final int varClassSize, varCommentSize, varCommentEdgeSize, varAssociationSize, varExtendsSize,
			varImplementsSize, varAggregation_1_1_Size, varAggregation_1_N_Size;
	
	
	
	// Výchozí hodnoty pro diagram instancí - první načtení hodnot při spuštení dialogu:
	private final int varInstanceSize, varInstanceAttributesSize, varInstanceMethodsSize, varInstanceAssociationSize,
			varInstanceAggregationSize;
	
	
	
	
	
	
	
	
	// Komponenty pro nastavení konkrétní velikosti písma v diagramu tříd:
	private final JSlider sliderClass, sliderComment, sliderCommentEdge, sliderAssociation, sliderExtends,
			sliderImplements, sliderAgggregation_1_1, sliderAggregation_1_N;
	
	// Komponenty pro nastavení konkrétní velikosti písma v diagramu instancí:
	private final JSlider sliderInstance, sliderInstanceAttributes, sliderInstanceMethods, sliderInstanceAssociation,
			sliderInstanceAggregation;
	
	
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd, aby bylo možné nastavit příslušné
	 *            velikosti písem.
	 * 
	 * @param instanceDiagram
	 *            - reference na diagram instancí, aby bylo možné nastavit příslušné
	 *            velikosti písem.
	 */
	OneSizeForEachObjectPanel(final GraphClass classDiagram, final GraphInstance instanceDiagram) {
		super();

		this.classDiagram = classDiagram;
		this.instanceDiagram = instanceDiagram;
		
		
		setLayout(new GridBagLayout());
		
		gbc = createGbc();
		
		index = 0;
		
		
		
		

		
	
		
		// Získám si výchozí hodnoty, resp. ty aktuální pro diagram tříd:
		varClassSize = getIntFromProp(classDiagram.getDefaultProperties(), "ClassFontSize",
				Integer.toString(Constants.CD_FONT_CLASS.getSize()));
		
		varCommentSize = getIntFromProp(classDiagram.getDefaultProperties(), "CommentFontSize",
				Integer.toString(Constants.CD_FONT_COMMENT.getSize()));
		
		varCommentEdgeSize = getIntFromProp(classDiagram.getDefaultProperties(), "CommentEdgeLineFontSize",
				Integer.toString(Constants.CD_COM_EDGE_FONT.getSize()));
		
		varAssociationSize = getIntFromProp(classDiagram.getDefaultProperties(), "AscEdgeLineFontSize",
				Integer.toString(Constants.CD_ASC_EDGE_LINE_FONT.getSize()));
		
		varExtendsSize = getIntFromProp(classDiagram.getDefaultProperties(), "ExtEdgeLineFontSize",
				Integer.toString(Constants.CD_EXT_EDGE_LINE_FONT.getSize()));
		
		varImplementsSize = getIntFromProp(classDiagram.getDefaultProperties(), "ImpEdgeLineFontSize",
				Integer.toString(Constants.CD_IMP_EDGE_LINE_FONT.getSize()));
		
		varAggregation_1_1_Size = getIntFromProp(classDiagram.getDefaultProperties(), "AgrEdgeLineFontSize",
				Integer.toString(Constants.CD_AGR_EDGE_LINE_FONT.getSize()));
		
		varAggregation_1_N_Size = getIntFromProp(classDiagram.getDefaultProperties(), "Agr_1_N_EdgeLineFontSize",
				Integer.toString(Constants.CD_AGR_EDGE_N_LINE_FONT.getSize()));

		
		
		// Získám si výchozí hodnoty, resp. ty aktuální pro diagram instancí:
		varInstanceSize = getIntFromProp(instanceDiagram.getDefaultProperties(), "FontSize",
				Integer.toString(Constants.ID_FONT.getSize()));

		varInstanceAttributesSize = getIntFromProp(instanceDiagram.getDefaultProperties(), "AttributesFontSize",
				Integer.toString(Constants.ID_ATTRIBUTES_FONT.getSize()));

		varInstanceMethodsSize = getIntFromProp(instanceDiagram.getDefaultProperties(), "MethodsFontSize",
				Integer.toString(Constants.ID_METHODS_FONT.getSize()));

		varInstanceAssociationSize = getIntFromProp(instanceDiagram.getDefaultProperties(),
				"AssociationEdgeLineFontSize", Integer.toString(Constants.ID_ASSOCIATION_EDGE_LINE_FONT.getSize()));

		varInstanceAggregationSize = getIntFromProp(instanceDiagram.getDefaultProperties(),
				"AggregationEdgeLineFontSize", Integer.toString(Constants.ID_AGGREGATION_EDGE_LINE_FONT.getSize()));
		
		
		
		
		
		
		
		lblInfo = new JLabel();
		lblInfo.setHorizontalAlignment(JLabel.CENTER);
		setGbc(index, 0, lblInfo);
		
		
		
		addSpace(++index);
		
		
		
		
		
		
		
		
		// Nastavení pro diagram tříd:
		lblClassDiagram = new JLabel();
		setGbc(++index, 0, lblClassDiagram);
		
		
		
		lblClass = new JLabel();	
		setGbc(++index, 0, lblClass);
		
		lblClassSize = new JLabel();		
		setGbc(++index, 0, lblClassSize);
		
		sliderClass = createSlider(varClassSize);
		setGbc(++index, 0, sliderClass);
		
		
		
		
		addSpace(++index);
		
		
		
		
		lblComment = new JLabel();
		setGbc(++index, 0, lblComment);
		
		lblCommentSize = new JLabel();		
		setGbc(++index, 0, lblCommentSize);

		sliderComment = createSlider(varCommentSize);
		setGbc(++index, 0, sliderComment);
		
		
		
		addSpace(++index);
		
		
		
		
		lblCommentEdge = new JLabel();
		setGbc(++index, 0, lblCommentEdge);
		
		lblCommentEdgeSize = new JLabel();		
		setGbc(++index, 0, lblCommentEdgeSize);
		
		sliderCommentEdge = createSlider(varCommentEdgeSize);
		setGbc(++index, 0, sliderCommentEdge);
		
		
		
		addSpace(++index);
		
		
		
		
		lblAssociation = new JLabel();
		setGbc(++index, 0, lblAssociation);
		
		lblAssociationSize = new JLabel();		
		setGbc(++index, 0, lblAssociationSize);
		
		sliderAssociation = createSlider(varAssociationSize);
		setGbc(++index, 0, sliderAssociation);
		
		
		
		
		addSpace(++index);
		
		
		
		
		
		lblAggregation_1_1 = new JLabel();
		setGbc(++index, 0, lblAggregation_1_1);
		
		lblAggregation_1_1_Size = new JLabel();
		setGbc(++index, 0, lblAggregation_1_1_Size);
		
		sliderAgggregation_1_1 = createSlider(varAggregation_1_1_Size);
		setGbc(++index, 0, sliderAgggregation_1_1);
		
		
		
		
		addSpace(++index);
		
		
		
		
		
		
		lblExtends = new JLabel();
		setGbc(++index, 0, lblExtends);
				
		lblExtendsSize = new JLabel();		
		setGbc(++index, 0, lblExtendsSize);
		
		sliderExtends  = createSlider(varExtendsSize);
		setGbc(++index, 0, sliderExtends);
		
		
		
		addSpace(++index);
		
		
		
		
		lblImplements = new JLabel();
		setGbc(++index, 0, lblImplements);
		
		lblImplementsSize = new JLabel();		
		setGbc(++index, 0, lblImplementsSize);
		
		sliderImplements  = createSlider(varImplementsSize);
		setGbc(++index, 0, sliderImplements);
		
		
		
		
		
		
		addSpace(++index);
		
		
		
		
		lblAggregation_1_N = new JLabel();
		setGbc(++index, 0, lblAggregation_1_N);
		
		lblAggregation_1_N_Size = new JLabel();		
		setGbc(++index, 0, lblAggregation_1_N_Size);
		
		sliderAggregation_1_N = createSlider(varAggregation_1_N_Size);
		setGbc(++index, 0,sliderAggregation_1_N );
		
		
		
		
		
		
		addSpace(++index);
		
		
		
		
		
		
		// Nastavení pro diagram instancí:
		lblInstanceDiagram = new JLabel();
		setGbc(++index, 0, lblInstanceDiagram);
		
		
		lblInstance = new JLabel();		
		setGbc(++index, 0, lblInstance);
		
		lblInstanceSize = new JLabel();		
		setGbc(++index, 0, lblInstanceSize);
		
		sliderInstance = createSlider(varInstanceSize);
		setGbc(++index, 0, sliderInstance);
		
		
		
		addSpace(++index);
		
		
		
		
		lblInstanceAttributes = new JLabel();
		setGbc(++index, 0, lblInstanceAttributes);
		
		lblInstanceAttributesSize = new JLabel();
		setGbc(++index, 0, lblInstanceAttributesSize);
		
		sliderInstanceAttributes = createSlider(varInstanceAttributesSize);
		setGbc(++index, 0, sliderInstanceAttributes);
		
				
		
		
		addSpace(++index);
		
		
		
		
		
		lblInstanceMethods = new JLabel();
		setGbc(++index, 0, lblInstanceMethods);
		
		lblInstanceMethodsSize = new JLabel();
		setGbc(++index, 0, lblInstanceMethodsSize);
		
		sliderInstanceMethods = createSlider(varInstanceMethodsSize);
		setGbc(++index, 0, sliderInstanceMethods);
		
		
		
		
		addSpace(++index);
		
		
		
		
		lblInstanceAssociation = new JLabel();
		setGbc(++index, 0, lblInstanceAssociation);
		
		lblInstanceAssociationSize = new JLabel();		
		setGbc(++index, 0, lblInstanceAssociationSize);
		
		sliderInstanceAssociation = createSlider(varInstanceAssociationSize);
		setGbc(++index, 0, sliderInstanceAssociation);
		
		
		
		
		addSpace(++index);
		
		
		
		
		lblInstanceAggregation = new JLabel();
		setGbc(++index, 0, lblInstanceAggregation);
		
		lblAggregationSize = new JLabel();		
		setGbc(++index, 0, lblAggregationSize);
		
		sliderInstanceAggregation = createSlider(varInstanceAggregationSize);
		setGbc(++index, 0, sliderInstanceAggregation);
		
		
		
		
		createListenerForSliders();
	}
	
	
	
	
		
	
	
	
	
	
	
	
	
	
	/**
	 * metoda, která vytvoří pro každý slider posluchač na pohyb posuvníku a vždy se
	 * provede příslušná akce.
	 */
	private void createListenerForSliders() {
		// Pro diagram tříd:
		sliderClass.addChangeListener(e -> {
			/*
			 * Nejprve otestuji, zda se jedná pouze o nastavení hodnoty pro tento slider,
			 * pokud ano, tak zde skončím, protože by se jinak opakovalo volání, což je
			 * zbytečné a chybné.
			 */
			if (onlyUpdateValue)
				return;


			/*
			 * Tato podmínka je, že se bude pokračovat v metodě, až když uživatel přestane
			 * hýbat posuvníkem, dokud hýbět, tak se nic nestane
			 */
			if (!FontSizeForm.isSetValuesWhileMoving() &&  sliderClass.getValueIsAdjusting())
				return;


			/*
			 * Nastavená hodnota:
			 */
			final int value = sliderClass.getValue();

			// Nastavím do labelu nastavenou velikost.
			setSizeInLabel(lblClassSize, value);

			// Nastavím velikost pro diagram tříd
			FONT_SIZE_OPERATION_CD.quickSetupOfClassFontSize(value, classDiagram);

			// Nastavím velikost písma do proměnné kvůli klávesovým zkratkám Ctrl + a -:
			GraphClass.setClassFontSize(value);



			// Otestování duplicitních hodnot třídy a komentáře:
			if (!sliderClass.getValueIsAdjusting()) {
				// Zda jsou stejné buňky pro třídu a komentář v CD:
				if (checkDuplicateClassAndCommentProperties(classDiagram,
						FontSizeForm.CHECK_DUPLICATES_IN_DIAGRAMS)) {
					// Nastavím i výchozí font pro klávesové zkratky
					GraphClass.setClassFontSize(Constants.CD_FONT_CLASS.getSize());

					/*
					 * Nastavím následující proměnnou na true, protože jinak by se při nastavení
					 * hodnoty slideru znovu zavolala tato metoda což je chybné, mohly by se
					 * nastavit chyná data, navíc by se znovu celý postup opakoval.
					 */
					onlyUpdateValue = true;
					// Zde zavolám metodu, která nastaví hodnoty ve slideru bez opakování této
					// metody:
					setSliderValue(sliderClass, Constants.CD_FONT_CLASS.getSize(), lblClassSize);
					setSliderValue(sliderComment, Constants.CD_FONT_COMMENT.getSize(), lblCommentSize);

					onlyUpdateValue = false;
				}
			}
		});
		
		
		sliderComment.addChangeListener(e -> {
			/*
			 * Nejprve otestuji, zda se jedná pouze o nastavení hodnoty pro tento slider,
			 * pokud ano, tak zde skončím, protože by se jinak opakovalo volání, což je
			 * zbytečné a chybné.
			 */
			if (onlyUpdateValue)
				return;


			/*
			 * Tato podmínka je, že se bude pokračovat v metodě, až když uživatel přestane
			 * hýbat posuvníkem, dokud hýbět, tak se nic nestane
			 */
			if (!FontSizeForm.isSetValuesWhileMoving() &&  sliderComment.getValueIsAdjusting())
				return;


			/*
			 * Nastavená hodnota:
			 */
			final int value = sliderComment.getValue();

			// Nastavím do labelu nastavenou velikost.
			setSizeInLabel(lblCommentSize, value);

			// Nastavím velikost pro diagram tříd:
			FONT_SIZE_OPERATION_CD.quickSetupOfCommentFontSize(value, classDiagram);



			// Otestování duplicitních hodnot třídy a komentáře:
			if (!sliderComment.getValueIsAdjusting()) {
				// Zda jsou stejné buňky pro třídu a komentář v CD:
				if (checkDuplicateClassAndCommentProperties(classDiagram,
						FontSizeForm.CHECK_DUPLICATES_IN_DIAGRAMS)) {
					// Nastavím i výchozí font pro klávesové zkratky
					GraphClass.setClassFontSize(Constants.CD_FONT_CLASS.getSize());

					/*
					 * Nastavím následující proměnnou na true, protože jinak by se při nastavení
					 * hodnoty slideru znovu zavolala tato metoda což je chybné, mohly by se
					 * nastavit chyná data, navíc by se znovu celý postup opakoval.
					 */
					onlyUpdateValue = true;
					// Zde zavolám metodu, která nastaví hodnoty ve slideru bez opakování této
					// metody:
					setSliderValue(sliderComment, Constants.CD_FONT_COMMENT.getSize(), lblCommentSize);
					setSliderValue(sliderClass, Constants.CD_FONT_CLASS.getSize(), lblClassSize);

					onlyUpdateValue = false;
				}
			}
		});
		
		
		sliderCommentEdge.addChangeListener(e -> {
			/*
			 * Nejprve otestuji, zda se jedná pouze o nastavení hodnoty pro tento slider,
			 * pokud ano, tak zde skončím, protože by se jinak opakovalo volání, což je
			 * zbytečné a chybné.
			 */
			if (onlyUpdateValue)
				return;


			/*
			 * Tato podmínka je, že se bude pokračovat v metodě, až když uživatel přestane
			 * hýbat posuvníkem, dokud hýbět, tak se nic nestane
			 */
			if (!FontSizeForm.isSetValuesWhileMoving() &&  sliderCommentEdge.getValueIsAdjusting())
				return;


			/*
			 * Nastavená hodnota:
			 */
			final int value = sliderCommentEdge.getValue();

			// Nastavím do labelu nastavenou velikost.
			setSizeInLabel(lblCommentEdgeSize, value);

			// Nastavím velikost pro diagram tříd:
			FONT_SIZE_OPERATION_CD.quickSetupOfCommentEdgeFontSize(value, classDiagram);



			if (!sliderCommentEdge.getValueIsAdjusting()) {
				// zde otestuji, zda jsou vybrané / některé hrany stejné:
				if (checkDuplicateEdgesInCd(classDiagram, FontSizeForm.CHECK_DUPLICATES_IN_DIAGRAMS)) {
					/*
					 * Nastavím následující proměnnou na true, protože jinak by se při nastavení
					 * hodnoty slideru znovu zavolala tato metoda což je chybné, mohly by se
					 * nastavit chyná data, navíc by se znovu celý postup opakoval.
					 */
					onlyUpdateValue = true;

					// Nastavím výchozí "nouzové" hodnoty:
					setDefaultValuesToEdgeSliders();

					onlyUpdateValue = false;
				}
			}
		});
		
		
		sliderAssociation.addChangeListener(e -> {
			/*
			 * Nejprve otestuji, zda se jedná pouze o nastavení hodnoty pro tento slider,
			 * pokud ano, tak zde skončím, protože by se jinak opakovalo volání, což je
			 * zbytečné a chybné.
			 */
			if (onlyUpdateValue)
				return;


			/*
			 * Tato podmínka je, že se bude pokračovat v metodě, až když uživatel přestane
			 * hýbat posuvníkem, dokud hýbět, tak se nic nestane
			 */
			if (!FontSizeForm.isSetValuesWhileMoving() &&  sliderAssociation.getValueIsAdjusting())
				return;


			/*
			 * Nastavená hodnota:
			 */
			final int value = sliderAssociation.getValue();

			// Nastavím do labelu nastavenou velikost.
			setSizeInLabel(lblAssociationSize, value);

			// Nastavím velikost pro diagram tříd:
			FONT_SIZE_OPERATION_CD.quickSetupOfAssociationEdgeFontSize(value, classDiagram);



			if (!sliderAssociation.getValueIsAdjusting()) {
				// zde otestuji, zda jsou vybrané / některé hrany stejné:
				if (checkDuplicateEdgesInCd(classDiagram, FontSizeForm.CHECK_DUPLICATES_IN_DIAGRAMS)) {
					/*
					 * Nastavím následující proměnnou na true, protože jinak by se při nastavení
					 * hodnoty slideru znovu zavolala tato metoda což je chybné, mohly by se
					 * nastavit chyná data, navíc by se znovu celý postup opakoval.
					 */
					onlyUpdateValue = true;

					// Nastavím výchozí "nouzové" hodnoty:
					setDefaultValuesToEdgeSliders();

					onlyUpdateValue = false;
				}
			}
		});
		
		
		sliderExtends.addChangeListener(e -> {
			/*
			 * Nejprve otestuji, zda se jedná pouze o nastavení hodnoty pro tento slider,
			 * pokud ano, tak zde skončím, protože by se jinak opakovalo volání, což je
			 * zbytečné a chybné.
			 */
			if (onlyUpdateValue)
				return;


			/*
			 * Tato podmínka je, že se bude pokračovat v metodě, až když uživatel přestane
			 * hýbat posuvníkem, dokud hýbět, tak se nic nestane
			 */
			if (!FontSizeForm.isSetValuesWhileMoving() && sliderExtends .getValueIsAdjusting())
				return;


			/*
			 * Nastavená hodnota:
			 */
			final int value = sliderExtends.getValue();

			// Nastavím do labelu nastavenou velikost.
			setSizeInLabel(lblExtendsSize, value);

			// Nastavím velikost pro diagram tříd:
			FONT_SIZE_OPERATION_CD.quickSetupOfExtendsEdgeFontSize(value, classDiagram);



			if (!sliderExtends.getValueIsAdjusting()) {
				// zde otestuji, zda jsou vybrané / některé hrany stejné:
				if (checkDuplicateEdgesInCd(classDiagram, FontSizeForm.CHECK_DUPLICATES_IN_DIAGRAMS)) {
					/*
					 * Nastavím následující proměnnou na true, protože jinak by se při nastavení
					 * hodnoty slideru znovu zavolala tato metoda což je chybné, mohly by se
					 * nastavit chyná data, navíc by se znovu celý postup opakoval.
					 */
					onlyUpdateValue = true;

					// Nastavím výchozí "nouzové" hodnoty:
					setDefaultValuesToEdgeSliders();

					onlyUpdateValue = false;
				}
			}
		});
		
		
		sliderImplements.addChangeListener(e -> {
			/*
			 * Tato podmínka je, že se bude pokračovat v metodě, až když uživatel přestane
			 * hýbat posuvníkem, dokud hýbět, tak se nic nestane
			 */
			if (!FontSizeForm.isSetValuesWhileMoving() &&  sliderImplements.getValueIsAdjusting())
				return;


			/*
			 * Nastavená hodnota:
			 */
			final int value = sliderImplements.getValue();

			// Nastavím do labelu nastavenou velikost.
			setSizeInLabel(lblImplementsSize, value);

			// Nastavím velikost pro diagram tříd:
			FONT_SIZE_OPERATION_CD.quickSetupOfImplementsEdgeFontSize(value, classDiagram);
		});
		
		
		sliderAgggregation_1_1.addChangeListener(e -> {
			/*
			 * Nejprve otestuji, zda se jedná pouze o nastavení hodnoty pro tento slider,
			 * pokud ano, tak zde skončím, protože by se jinak opakovalo volání, což je
			 * zbytečné a chybné.
			 */
			if (onlyUpdateValue)
				return;


			/*
			 * Tato podmínka je, že se bude pokračovat v metodě, až když uživatel přestane
			 * hýbat posuvníkem, dokud hýbět, tak se nic nestane
			 */
			if (!FontSizeForm.isSetValuesWhileMoving() &&  sliderAgggregation_1_1.getValueIsAdjusting())
				return;


			/*
			 * Nastavená hodnota:
			 */
			final int value = sliderAgggregation_1_1.getValue();

			// Nastavím do labelu nastavenou velikost.
			setSizeInLabel(lblAggregation_1_1_Size, value);

			// Nastavím velikost pro diagram tříd:
			FONT_SIZE_OPERATION_CD.quickSetupOfAggregation_1_1_EdgeFontSize(value, classDiagram);



			if (!sliderAgggregation_1_1.getValueIsAdjusting()) {
				// zde otestuji, zda jsou vybrané / některé hrany stejné:
				if (checkDuplicateEdgesInCd(classDiagram, FontSizeForm.CHECK_DUPLICATES_IN_DIAGRAMS)) {
					/*
					 * Nastavím následující proměnnou na true, protože jinak by se při nastavení
					 * hodnoty slideru znovu zavolala tato metoda což je chybné, mohly by se
					 * nastavit chyná data, navíc by se znovu celý postup opakoval.
					 */
					onlyUpdateValue = true;

					// Nastavím výchozí "nouzové" hodnoty:
					setDefaultValuesToEdgeSliders();

					onlyUpdateValue = false;
				}
			}
		});
		
		
		sliderAggregation_1_N.addChangeListener(e -> {
			/*
			 * Nejprve otestuji, zda se jedná pouze o nastavení hodnoty pro tento slider,
			 * pokud ano, tak zde skončím, protože by se jinak opakovalo volání, což je
			 * zbytečné a chybné.
			 */
			if (onlyUpdateValue)
				return;


			/*
			 * Tato podmínka je, že se bude pokračovat v metodě, až když uživatel přestane
			 * hýbat posuvníkem, dokud hýbět, tak se nic nestane
			 */
			if (!FontSizeForm.isSetValuesWhileMoving() &&  sliderAggregation_1_N.getValueIsAdjusting())
				return;


			/*
			 * Nastavená hodnota:
			 */
			final int value = sliderAggregation_1_N.getValue();

			// Nastavím do labelu nastavenou velikost.
			setSizeInLabel(lblAggregation_1_N_Size, value);

			// Nastavím velikost pro diagram tříd:
			FONT_SIZE_OPERATION_CD.quickSetupOfAggregation_1_N_EdgeFontSize(value, classDiagram);



			if (!sliderAggregation_1_N.getValueIsAdjusting()) {
				// zde otestuji, zda jsou vybrané / některé hrany stejné:
				if (checkDuplicateEdgesInCd(classDiagram, FontSizeForm.CHECK_DUPLICATES_IN_DIAGRAMS)) {
					/*
					 * Nastavím následující proměnnou na true, protože jinak by se při nastavení
					 * hodnoty slideru znovu zavolala tato metoda což je chybné, mohly by se
					 * nastavit chyná data, navíc by se znovu celý postup opakoval.
					 */
					onlyUpdateValue = true;

					// Nastavím výchozí "nouzové" hodnoty:
					setDefaultValuesToEdgeSliders();

					onlyUpdateValue = false;
				}
			}
		});
		
		

		
		
		// Komponenty pro nastavení konkrétní velikosti písma v diagramu instancí:
		sliderInstance.addChangeListener(e -> {
			/*
			 * Tato podmínka je, že se bude pokračovat v metodě, až když uživatel přestane
			 * hýbat posuvníkem, dokud hýbět, tak se nic nestane
			 */
			if (!FontSizeForm.isSetValuesWhileMoving() &&  sliderInstance.getValueIsAdjusting())
				return;


			/*
			 * Nastavená hodnota:
			 */
			final int value = sliderInstance.getValue();

			// Nastavím do labelu nastavenou velikost.
			setSizeInLabel(lblInstanceSize, value);

			// Nastavím velikost pro diagram instancí:
			FONT_SIZE_OPERATION_ID.quickSetupOfInstanceFontSize(value, instanceDiagram);

			// Nastavím velikost písma do proměnné kvůli klávesovým zkratkám Ctrl + a -:
			GraphInstance.setInstanceFontSize(value);
		});
		
		
		
		// Komponenta pro nastavení velikosti písma pro atributy v buňce reprezentující
		// instanci v diagramu instancí:
		sliderInstanceAttributes.addChangeListener(e -> {
			/*
			 * Tato podmínka je, že se bude pokračovat v metodě, až když uživatel přestane
			 * hýbat posuvníkem, dokud hýbět, tak se nic nestane
			 */
			if (!FontSizeForm.isSetValuesWhileMoving() && sliderInstanceAttributes.getValueIsAdjusting())
				return;


			/*
			 * Nastavená hodnota:
			 */
			final int value = sliderInstanceAttributes.getValue();

			// Nastavím do labelu nastavenou velikost.
			setSizeInLabel(lblInstanceAttributesSize, value);

			// Nastavím velikost pro atributy v buňce pro instanci:
			FONT_SIZE_OPERATION_ID.quickSetupOfAttributesFontSizeInInstance(value, instanceDiagram);
		});
		
		
		
		
		// Komponenta pro nastavení velikosti písma pro metody v buňce reprezentující
		// instanci v diagramu instancí:
		sliderInstanceMethods.addChangeListener(e -> {
			/*
			 * Tato podmínka je, že se bude pokračovat v metodě, až když uživatel přestane
			 * hýbat posuvníkem, dokud hýbět, tak se nic nestane
			 */
			if (!FontSizeForm.isSetValuesWhileMoving() && sliderInstanceMethods.getValueIsAdjusting())
				return;


			/*
			 * Nastavená hodnota:
			 */
			final int value = sliderInstanceMethods.getValue();

			// Nastavím do labelu nastavenou velikost.
			setSizeInLabel(lblInstanceMethodsSize, value);

			// Nastavím velikost pro atributy v buňce pro instanci:
			FONT_SIZE_OPERATION_ID.quickSetupOfMethodsFontSizeInInstance(value, instanceDiagram);
		});
		
		
		
		
		
		sliderInstanceAssociation.addChangeListener(e -> {
			/*
			 * Nejprve otestuji, zda se jedná pouze o nastavení hodnoty pro tento slider,
			 * pokud ano, tak zde skončím, protože by se jinak opakovalo volání, což je
			 * zbytečné a chybné.
			 */
			if (onlyUpdateValue)
				return;


			/*
			 * Tato podmínka je, že se bude pokračovat v metodě, až když uživatel přestane
			 * hýbat posuvníkem, dokud hýbět, tak se nic nestane
			 */
			if (!FontSizeForm.isSetValuesWhileMoving() &&  sliderInstanceAssociation.getValueIsAdjusting())
				return;


			/*
			 * Nastavená hodnota:
			 */
			final int value = sliderInstanceAssociation.getValue();

			// Nastavím do labelu nastavenou velikost.
			setSizeInLabel(lblInstanceAssociationSize, value);

			// Nastavím velikost pro diagram instancí:
			FONT_SIZE_OPERATION_ID.quickSetupOfAssociationFontSize(value, instanceDiagram);



			if (!sliderInstanceAssociation.getValueIsAdjusting()) {
				// zde otestuji, zda jsou vybrané / některé hrany stejné:
				if (checkDuplicateEdgesInId(instanceDiagram, FontSizeForm.CHECK_DUPLICATES_IN_DIAGRAMS)) {
					/*
					 * Nastavím následující proměnnou na true, protože jinak by se při nastavení
					 * hodnoty slideru znovu zavolala tato metoda což je chybné, mohly by se
					 * nastavit chyná data, navíc by se znovu celý postup opakoval.
					 */
					onlyUpdateValue = true;

					// Nastavím výchozí "nouzové" hodnoty:
					setSliderValue(sliderInstanceAssociation, ASSOCIATION_EDGE_DEFAULT_FONT_SIZE_ID,
							lblInstanceAssociationSize);
					setSliderValue(sliderInstanceAggregation, AGGREGATION_EDGE_DEFAULT_FONT_SIZE,
							lblAggregationSize);

					onlyUpdateValue = false;
				}
			}
		});
		
		
		sliderInstanceAggregation.addChangeListener(e -> {
			/*
			 * Nejprve otestuji, zda se jedná pouze o nastavení hodnoty pro tento slider,
			 * pokud ano, tak zde skončím, protože by se jinak opakovalo volání, což je
			 * zbytečné a chybné.
			 */
			if (onlyUpdateValue)
				return;


			/*
			 * Tato podmínka je, že se bude pokračovat v metodě, až když uživatel přestane
			 * hýbat posuvníkem, dokud hýbět, tak se nic nestane
			 */
			if (!FontSizeForm.isSetValuesWhileMoving() &&  sliderInstanceAggregation.getValueIsAdjusting())
				return;


			/*
			 * Nastavená hodnota:
			 */
			final int value = sliderInstanceAggregation.getValue();

			// Nastavím do labelu nastavenou velikost.
			setSizeInLabel(lblAggregationSize, value);

			// Nastavím velikost pro diagram instancí:
			FONT_SIZE_OPERATION_ID.quickSetupOfAgregationFontSize(value, instanceDiagram);



			if (!sliderInstanceAggregation.getValueIsAdjusting()) {
				// zde otestuji, zda jsou vybrané / některé hrany stejné:
				if (checkDuplicateEdgesInId(instanceDiagram, FontSizeForm.CHECK_DUPLICATES_IN_DIAGRAMS)) {
					/*
					 * Nastavím následující proměnnou na true, protože jinak by se při nastavení
					 * hodnoty slideru znovu zavolala tato metoda což je chybné, mohly by se
					 * nastavit chyná data, navíc by se znovu celý postup opakoval.
					 */
					onlyUpdateValue = true;

					// Nastavím výchozí "nouzové" hodnoty:
					setSliderValue(sliderInstanceAggregation, AGGREGATION_EDGE_DEFAULT_FONT_SIZE,
							lblAggregationSize);
					setSliderValue(sliderInstanceAssociation, ASSOCIATION_EDGE_DEFAULT_FONT_SIZE_ID,
							lblInstanceAssociationSize);

					onlyUpdateValue = false;
				}
			}
		});
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která nastaví "nouzové" výchozí hodnoty do sliderů pro nastavení hran
	 * s výjimkou hrany, která reprezentuje implementaci rozhraní.
	 */
	private void setDefaultValuesToEdgeSliders() {
		// Zde zavolám metodu, která nastaví hodnoty ve slideru bez opakování této
		// metody:
		setSliderValue(sliderCommentEdge, COMMENT_EDGE_DEFAULT_FONT_SIZE, lblCommentEdgeSize);
		setSliderValue(sliderAssociation, ASSOCIATION_EDGE_DEFAULT_FONT_SIZE, lblAssociationSize);
		setSliderValue(sliderExtends, EXTENDS_EDGE_DEFAULT_FONT_SIZE, lblExtendsSize);
		setSliderValue(sliderAgggregation_1_1, AGGREGATION_1_1_EDGE_DEFAULT_FONT_SIZE, lblAggregation_1_1_Size);
		setSliderValue(sliderAggregation_1_N, AGGREGATION_1_N_EDGE_DEFAULT_FONT_SIZE, lblAggregation_1_N_Size);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public void setLanguage(final Properties properties) {
		setLanguageForErrorText(properties);
		
		if (properties != null) {
			lblInfo.setText(properties.getProperty("FSF_OSFEOP_LblInfo", Constants.FSF_OSFEOP_LBL_INFO));

			lblClassDiagram.setText(
					properties.getProperty("FSF_OSFEOP_LblClassDiagram", Constants.FSF_OSFEOP_LBL_CLASS_DIAGRAM));
			lblInstanceDiagram.setText(
					properties.getProperty("FSF_OSFEOP_LblInstanceDiagram", Constants.FSF_OSFEOP_LBL_INSTANCE_DIAGRAM));

			txtSize = properties.getProperty("FSF_OSFEOP_TxtSize", Constants.FSF_OSFEOP_TXT_SIZE);

			// Typy komponent pro diagram tříd:
			lblClass.setText(properties.getProperty("FSF_OSFEOP_LblClass", Constants.FSF_OSFEOP_LBL_CLASS));
			lblComment.setText(properties.getProperty("FSF_OSFEOP_LblComment", Constants.FSF_OSFEOP_LBL_COMMENT));
			lblCommentEdge.setText(
					properties.getProperty("FSF_OSFEOP_LblCommentEdge", Constants.FSF_OSFEOP_LBL_COMMENT_EDGE));
			lblAssociation
					.setText(properties.getProperty("FSF_OSFEOP_LblAssociation", Constants.FSF_OSFEOP_LBL_ASSOCIATION));
			lblAggregation_1_1.setText(
					properties.getProperty("FSF_OSFEOP_LblAggregation_1_1", Constants.FSF_OSFEOP_LBL_AGGREGATION_1_1));
			lblExtends.setText(properties.getProperty("FSF_OSFEOP_LblExtends", Constants.FSF_OSFEOP_LBL_EXTENDS));
			lblImplements
					.setText(properties.getProperty("FSF_OSFEOP_LblImplements", Constants.FSF_OSFEOP_LBL_IMPLEMENTS));
			lblAggregation_1_N.setText(
					properties.getProperty("FSF_OSFEOP_LblAggregation_1_N", Constants.FSF_OSFEOP_LBL_AGGREGATION_1_N));

			// Typy komponent pro diagram instancí:
			lblInstance.setText(properties.getProperty("FSF_OSFEOP_LblInstance", Constants.FSF_OSFEOP_LBL_INSTANCE));
			lblInstanceAttributes.setText(properties.getProperty("FSF_OSFEOP_LblInstanceAttributes",
					Constants.FSF_OSFEOP_LBL_INSTANCE_ATTRIBUTES));
			lblInstanceMethods.setText(
					properties.getProperty("FSF_OSFEOP_LblInstanceMethods", Constants.FSF_OSFEOP_LBL_INSTANCE_METHODS));
			lblInstanceAssociation.setText(properties.getProperty("FSF_OSFEOP_LblInstanceAssociation",
					Constants.FSF_OSFEOP_LBL_INSTANCE_ASSOCIATION));
			lblInstanceAggregation.setText(properties.getProperty("FSF_OSFEOP_LblInstanceAggregation",
					Constants.FSF_OSFEOP_LBL_INSTANCE_AGGREGATION));
		}

		
		else {
			lblInfo.setText(Constants.FSF_OSFEOP_LBL_INFO);

			lblClassDiagram.setText(Constants.FSF_OSFEOP_LBL_CLASS_DIAGRAM);
			lblInstanceDiagram.setText(Constants.FSF_OSFEOP_LBL_INSTANCE_DIAGRAM);

			txtSize = Constants.FSF_OSFEOP_TXT_SIZE;

			// Typy komponent pro diagram tříd:
			lblClass.setText(Constants.FSF_OSFEOP_LBL_CLASS);
			lblComment.setText(Constants.FSF_OSFEOP_LBL_COMMENT);
			lblCommentEdge.setText(Constants.FSF_OSFEOP_LBL_COMMENT_EDGE);
			lblAssociation.setText(Constants.FSF_OSFEOP_LBL_ASSOCIATION);
			lblAggregation_1_1.setText(Constants.FSF_OSFEOP_LBL_AGGREGATION_1_1);
			lblExtends.setText(Constants.FSF_OSFEOP_LBL_EXTENDS);
			lblImplements.setText(Constants.FSF_OSFEOP_LBL_IMPLEMENTS);
			lblAggregation_1_N.setText(Constants.FSF_OSFEOP_LBL_AGGREGATION_1_N);

			// Typy komponent pro diagram instancí:
			lblInstance.setText(Constants.FSF_OSFEOP_LBL_INSTANCE);
			lblInstanceAttributes.setText(Constants.FSF_OSFEOP_LBL_INSTANCE_ATTRIBUTES);
			lblInstanceMethods.setText(Constants.FSF_OSFEOP_LBL_INSTANCE_METHODS);
			lblInstanceAssociation.setText(Constants.FSF_OSFEOP_LBL_INSTANCE_ASSOCIATION);
			lblInstanceAggregation.setText(Constants.FSF_OSFEOP_LBL_INSTANCE_AGGREGATION);
		}
		
		
		
		
		// Výchozí velikosti pro diagram tříd:
		setSizeInLabel(lblClassSize, varClassSize);
		setSizeInLabel(lblCommentSize, varCommentSize);
		setSizeInLabel(lblCommentEdgeSize, varCommentEdgeSize);
		setSizeInLabel(lblAssociationSize, varAssociationSize);
		setSizeInLabel(lblExtendsSize, varExtendsSize);
		setSizeInLabel(lblImplementsSize, varImplementsSize);
		setSizeInLabel(lblAggregation_1_1_Size, varAggregation_1_1_Size);
		setSizeInLabel(lblAggregation_1_N_Size, varAggregation_1_N_Size);

		// Výchozí velikosti pro diagram instancí:
		setSizeInLabel(lblInstanceSize, varInstanceSize);
		setSizeInLabel(lblInstanceAttributesSize, varInstanceAttributesSize);
		setSizeInLabel(lblInstanceMethodsSize, varInstanceMethodsSize);
		setSizeInLabel(lblInstanceAssociationSize, varInstanceAssociationSize);
		setSizeInLabel(lblAggregationSize, varInstanceAggregationSize);
	}
}
