package cz.uhk.fim.fimj.font_size_form;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Properties;

import javax.swing.JScrollPane;

import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.LanguageInterface;
import cz.uhk.fim.fimj.forms.SourceDialogForm;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;

/**
 * Tato třída slouží jako dialog, ve kterém si uživatel může "rychlým" způsobem nastavit velikost fontu / písma pro
 * některé vybrané objekty v diagramu tříd a v diagramu instancí.
 * <p>
 * V tomto dialogu je na výběr, zda uživatel chce nastavovat všechny velikost stejně, nebo každou zvlášť, nebo jen
 * některé označené objekty v diagramu instancí nebo tříd.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class FontSizeForm extends SourceDialogForm implements LanguageInterface {

	private static final long serialVersionUID = 1L;

	
	// Velikost okna v případě panelu pro stejnou velikost všech objektů v diagramu
	// tříd a instancí:
	private static final int PNL_SAME_SIZE_WIDTH = 700, PNL_SAME_SIZE_HEIGHT = 190;
	

	// Velikost okna v případě panelu pro nastavení velikosti pouze vybraných
	// objektů:
	private static final int PNL_ONE_SIZE_FOR_CHOOSED_OBJECTS_WIDTH = 700,
			PNL_ONE_SIZE_FOR_CHOOSED_OBJECTS_HEIGHT = 565;
	
	

	// Velikost okna v případě panelu pro nastavení velikosti písma individuálně pro
	// každý objekt v diagramu tříd a instancí
	private static final int PNL_ONE_SIZE_FOR_EACH_OBJECT_WIDTH = 700, PNL_ONE_SIZE_FOR_EACH_OBJECT_HEIGHT = 580;
	
	
	
	
	

	/**
	 * "Komponenta", která obsahuje metody, které slouží pro zjištění duplicit v
	 * diagramu tříd a instancí.
	 */
	static CheckDuplicatesInDiagramsInterface CHECK_DUPLICATES_IN_DIAGRAMS;
	
		
	
	
	
	
	
	
	/**
	 * Logická proměnná, která znčí, zda se má nastavovat velikost písma i během
	 * toho, co se hýbe s posuvníkem nebo ne.
	 */
	private static boolean setValuesWhileMoving;
	
	
	
	
	
	
	
	/**
	 * Výčtová hodnota, která značí, o jaké komponenty se mají vložit do tohoto
	 * dialogu. Například komponenty pro nastavení jedné velikosti pro všechny
	 * objekty v diagramu tříd a v diagramu instancí, nebo jen pro některé apod.
	 */
	private static FontSizeFormType formType;
	
	
	
	
	
	/**
	 * Komponenta, která slouží jako menu pro tento dialog.
	 */
	private final Menu menu;
	
	
	
	
	
	
	
	/**
	 * Objekt, obsahuje texty pro tuto aplikace ve zvoleném jazyce.
	 */
	private final Properties languageProperties;
	
	
	/**
	 * Reference na diagram tříd.
	 */
	private final GraphClass classDiagram;
	
	/**
	 * Reference na diagram instancí.
	 */
	private final GraphInstance instanceDiagram;
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Proměnná, která obsahuje referenci na panel s komponentami pro nastavení
	 * stejné velikosti písma pro všechny objekty v diagramu tříd a instancí.
	 */
	private SameSizePanel pnlSameSize;
	
		
	/**
	 * Scrolovací komponenta, do které se vkládá panel pro nastavení velikostí písma
	 * / fontu, ale pro každý objekt jak v diagramu tříd, tak v diagramu instnací
	 * zvlášť - pro každý objekt individuálně.
	 */
	private JScrollPane jspPnlSameSizeForEachObject;
	
	
	/**
	 * Panel, který obsahuje komponenty pro nastavení velikosti písma pro objekty v
	 * diagramu tříd a v diagramu instnací, ale vždy se budou nastavovat pouze
	 * velikosti písma u těch komponent, které budou označeny.
	 */
	private SameSizeSelectedObjectsPanel pnlSameSizeForSelectedObjects;
	
	
	

	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param languageProperties
	 *            - objekt Properties, který obsahuje tecty pro tuto aplikaci ve
	 *            zvoleném jazyce.
	 * 
	 * @param formType
	 *            - výčtová hodnota, dle které se pozná, jaký panel s komponentami
	 *            se má vložit do tohodo dialogu (pro začátek).
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd kvůli zavolání metod pro nastavení
	 *            velikosti písma / fontu.
	 * 
	 * @param instanceDiagram
	 *            - reference na diagram instancí kvůli zavolání metod pro nastavení
	 *            velikosti písma / fontu.
	 * 
	 * @param valueForMoving
	 *            - logická hodnota o tom, zda se má při pohybu posuvníku nastavovat
	 *            velikost písma (hodnota true), nebo zda se má natavit až při
	 *            finálním nastavení posuvníku, tj. až sním uživatel přestane hýbat
	 *            (hodnota false).
	 */
	public FontSizeForm(final Properties languageProperties, final FontSizeFormType formType,
			final GraphClass classDiagram, final GraphInstance instanceDiagram, final boolean valueForMoving) {

		super();
		
		
		this.languageProperties = languageProperties;
		
		this.classDiagram = classDiagram;
		
		this.instanceDiagram = instanceDiagram;
		
		FontSizeForm.formType = formType;
		
		setValuesWhileMoving = valueForMoving;
		
		CHECK_DUPLICATES_IN_DIAGRAMS = new CheckDuplicatesInDiagrams(languageProperties);
		
		
		
		
		
		
		
		// Nastavení panelů pro nastavení velikosti písma:
		if (formType == FontSizeFormType.ONE_SAME_SIZE) {
			initGui(DISPOSE_ON_CLOSE, new BorderLayout(), "/icons/app/FontSizeIcon.png",
					new Dimension(PNL_SAME_SIZE_WIDTH, PNL_SAME_SIZE_HEIGHT), true);

			setSameSizePanel();
		}

		else if (formType == FontSizeFormType.ONE_SIZE_FOR_CHOOSED_OBJECTS) {
			initGui(DISPOSE_ON_CLOSE, new BorderLayout(), "/icons/app/FontSizeIcon.png",
					new Dimension(PNL_ONE_SIZE_FOR_CHOOSED_OBJECTS_WIDTH, PNL_ONE_SIZE_FOR_CHOOSED_OBJECTS_HEIGHT),
					true);

			setSameSizeForSelectedObjectsPnl();
		}

		else if (formType == FontSizeFormType.ONE_SIZE_FOR_EACH_OBJECT) {
			initGui(DISPOSE_ON_CLOSE, new BorderLayout(), "/icons/app/FontSizeIcon.png",
					new Dimension(PNL_ONE_SIZE_FOR_EACH_OBJECT_WIDTH, PNL_ONE_SIZE_FOR_EACH_OBJECT_HEIGHT), true);

			setSameSizeForEachObjectPnl();
		}
		
		
		
		
		
		
		// Nastavím menu do okna tohoto dialogu.
		menu = new Menu(formType, this, valueForMoving, classDiagram, instanceDiagram);
		menu.setLanguage(languageProperties);
		
		setJMenuBar(menu);
		
		
		
		
		// Přidám posluchač na zavření okna dialogu, aby uložil některé informace
		// ohledně nastavených vlastnotí v okně dialogu.
		addWindowListener(new FontSizeFormWindowListener(this));
		
		
		
		
		
		
		pack();
		setLocationRelativeTo(null);
	}









	
	
	/**
	 * Metoda, která odebere aktuální panel s komponentami pro nastavení velikosti
	 * písma a přida do něj panel s nastavením velkosti písma s komponentami pro
	 * nastavení jedné stejné velikosti písma pro všechny objekty v diagramu tříd i
	 * v diagramu instancí.
	 */
	void setSameSizePanel() {
		removePanelsFromDialog();

		setSize(new Dimension(PNL_SAME_SIZE_WIDTH, PNL_SAME_SIZE_HEIGHT));

		pnlSameSize = new SameSizePanel(classDiagram, instanceDiagram);
		pnlSameSize.setLanguage(languageProperties);

		add(pnlSameSize, BorderLayout.CENTER);

		revalidate();
		
		// Zde je možnost okno vždy vycentrovat:
//		setLocationRelativeTo(null);
	}
	
	
	
	
	
	
	
	
	

	
	/**
	 * Metoda, která slouží pro přidání panelu do okna dialogu pro nastavení
	 * velikosti písma jen vybraných objektů v diagramu tříd a v diagramu instnací.
	 */
	void setSameSizeForSelectedObjectsPnl() {
		removePanelsFromDialog();
		
		setSize(new Dimension(PNL_ONE_SIZE_FOR_CHOOSED_OBJECTS_WIDTH, PNL_ONE_SIZE_FOR_CHOOSED_OBJECTS_HEIGHT));

		pnlSameSizeForSelectedObjects = new SameSizeSelectedObjectsPanel(classDiagram, instanceDiagram);
		pnlSameSizeForSelectedObjects.setLanguage(languageProperties);

		add(pnlSameSizeForSelectedObjects, BorderLayout.CENTER);

		revalidate();
		
		// Zde je možnost okno vždy vycentrovat:
//		setLocationRelativeTo(null);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro přidání panelu do okna dialogu, který obsahuje
	 * komponenty pro nastavení velikosti písma pro objekty v diagramu tříd a v
	 * diagramu instancí tak, že pro každý objekt se nastavují velikosti
	 * individuálně.
	 */
	void setSameSizeForEachObjectPnl() {
		removePanelsFromDialog();
		
		setSize(new Dimension(PNL_ONE_SIZE_FOR_EACH_OBJECT_WIDTH, PNL_ONE_SIZE_FOR_EACH_OBJECT_HEIGHT));

		final OneSizeForEachObjectPanel pnlSameSizeForEachObject = new OneSizeForEachObjectPanel(classDiagram,
				instanceDiagram);
		
		pnlSameSizeForEachObject.setLanguage(languageProperties);

		jspPnlSameSizeForEachObject = new JScrollPane(pnlSameSizeForEachObject);
		add(jspPnlSameSizeForEachObject, BorderLayout.CENTER);

		revalidate();
		
		// Zde je možnost okno vždy vycentrovat:
//		setLocationRelativeTo(null);
	}

	
	
	
	
	
	
	

	
	/**
	 * Metoda, která odebere panely z okna dialogu.
	 * 
	 * Panely, které slouží pro nastavení velikosti písma.
	 */
	private void removePanelsFromDialog() {
		if (pnlSameSize != null)
			remove(pnlSameSize);

		if (pnlSameSizeForSelectedObjects != null)
			remove(pnlSameSizeForSelectedObjects);

		if (jspPnlSameSizeForEachObject != null)
			remove(jspPnlSameSizeForEachObject);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Getr na logickou proměnnou, která značí, zda se má nastavovat velikost písma
	 * / fontu během toho, co se hýbe s posuvníkem nebo ne.
	 * 
	 * @return getr na výše popsanou proměnnou.
	 */
	static boolean isSetValuesWhileMoving() {
		return setValuesWhileMoving;
	}
	
	
	
	/**
	 * Setr na proměnnou, která značí, zda se má nastavovat velikost písma / fontu
	 * během toho, co se hýbe s posuvníkem nebo ne.
	 * 
	 * @param setValuesWhileMoving
	 *            - logická hodnota, která znčí, zda se má nastavovat písmo během
	 *            toho, co se hýbe s posuvníkem nebo ne.
	 */
	static void setSetValuesWhileMoving(boolean setValuesWhileMoving) {
		FontSizeForm.setValuesWhileMoving = setValuesWhileMoving;
	}
	
	
	
	
	
	/**
	 * Setr na proměnnou formType, která značí jaká sada komponent je aktuálně
	 * využívána v tomto dialogu, napřílad sada komponent pro nastavení jedné
	 * velikosti pro všechny komponnty apod.
	 * 
	 * @param formType
	 *            - výčtová hodnota, která značí o jakou ssdu komponent pro
	 *            nastavení fontu / velikosti písma jde.
	 */
	static void setFormType(FontSizeFormType formType) {
		FontSizeForm.formType = formType;
	}
	
	
	
	
	
	
	/**
	 * Getr na výčtovou hodnotu, která značí, jaký je aktuálně v okně tohoto dialogu
	 * zvolen panel s komponentami pro nastavení velikosti písma. jestli panel pro
	 * individuální nastavení velikostí písma pro objekty v diagramu tříd a v
	 * diagramu instancí, nebo panel s jedním sliderem, který nastavuje stejnou
	 * velikost písma pro všechny objekty v diagramu tříd a v diagramu instancí
	 * adpod.
	 * 
	 * @return Getr na výše popsanou výčtovou hodnotu.
	 */
	static FontSizeFormType getFormType() {
		return formType;
	}
	
	
	
	
	
	
	
	/**
	 * Getr na panel, který obsahuje komponenty pro nastavení velikosti písma jen u
	 * těch objektů v diagramu tříd a v diagramu instancí, které uživatel označí.
	 * 
	 * @return gvýše popsaný panel.
	 */
	SameSizeSelectedObjectsPanel getPnlSameSizeForSelectedObjects() {
		return pnlSameSizeForSelectedObjects;
	}
	
	
	
	
	
	
	
	
	


	@Override
	public void setLanguage(final Properties properties) {
		menu.setLanguage(properties);

		if (properties != null)
			setTitle(properties.getProperty("FSF_DialogTitle", Constants.FSF_DIALOG_TITLE));

		else
			setTitle(Constants.FSF_DIALOG_TITLE);
	}
}
