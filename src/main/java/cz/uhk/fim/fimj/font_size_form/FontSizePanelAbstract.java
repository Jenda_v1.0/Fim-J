package cz.uhk.fim.fimj.font_size_form;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;

import cz.uhk.fim.fimj.class_diagram.FontSizeOperation_CD;
import cz.uhk.fim.fimj.class_diagram.FontSizeOperation_CD_Interface;
import cz.uhk.fim.fimj.class_diagram.GraphClass;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.instance_diagram.FontSizeOperation_ID;
import cz.uhk.fim.fimj.instance_diagram.FontSizeOperation_ID_Interface;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;
import cz.uhk.fim.fimj.logger.ExceptionLogger;
import cz.uhk.fim.fimj.logger.ExtractEnum;

/**
 * Tato abstraktní třída slouží jako panel pro společné potomky a také obsahuje společné metody pro několik potomků.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class FontSizePanelAbstract extends JPanel {

	private static final long serialVersionUID = 1L;
	
	
	
	/**
	 * "Komponenta", která obsahuje metody, pomocí kterých je možné nastavit
	 * velikosti písma v objektům v diagramu tříd.
	 */
	static final FontSizeOperation_CD_Interface FONT_SIZE_OPERATION_CD = new FontSizeOperation_CD();

	/**
	 * "Komponenta", která obsahuje metody, pomocí kterých je možné nastavit
	 * velikosti písma v objektům v diagramu instancí.
	 */
	static final FontSizeOperation_ID_Interface FONT_SIZE_OPERATION_ID = new FontSizeOperation_ID();
	
	
	
	
	/**
	 * Konstanta, která obsahuje minimální hodnotu, kterou lze nastavit jako
	 * velikost písma / fontu v objektech v diagramu tříd a v diagramu instancí.
	 */
	private static final int FONT_SIZE_MIN_VALUE = 8;

	/**
	 * Konstanta, která obsahuje maximání hodnotu, kterou lze nastavit jako velikost
	 * písma / fontu v objektech v diagramu tříd a v diagramu instancí.
	 */
	private static final int FONT_SIZE_MAX_VALUE = 150;
	
	
	
	
	
	/**
	 * "Nouzová" výchozí velikost písma pro hranu, která v diagramu instancí
	 * reprezentuje vztah typu asociace.
	 *
	 * Tato hodnota je využita v případě, že jsou obě hrany stejně - mají stejné
	 * vlastnosti, třeba měli vše stejné kromě velikosti textu a když uživatel v
	 * dialogu pro změnu velikosti písma nastavil stejné písmo, tak se musí
	 * pohlídat, aby ty hrany nebyly stejné, tak se nastaví výchozí hodnoty.
	 * 
	 * Jedná se o to, že když uživatel změní velikosti písma některým objektům v
	 * diagramu instancí (mimo jiné), tak se musí pohlídat, duplicitní hodnoty, tj.
	 * aby nebyly obě hrany (pro asociaci a agregaci v diagramu instancí) stejné,
	 * protože pokud by byly, pak by se nepodařilo je rozeznat.
	 */
	static final int ASSOCIATION_EDGE_DEFAULT_FONT_SIZE_ID = 12;

	/**
	 * "Nouzová" výchozí velikost písma pro hranu, která v diagramu instancí
	 * reprezentuje vztah typu agregace.
	 * 
	 * Tato hodnota je využita v případě, že jsou obě hrany stejně - mají stejné
	 * vlastnosti, třeba měli vše stejné kromě velikosti textu a když uživatel v
	 * dialogu pro změnu velikosti písma nastavil stejné písmo, tak se musí
	 * pohlídat, aby ty hrany nebyly stejné, tak se nastaví výchozí hodnoty.
	 * 
	 * Jedná se o to, že když uživatel změní velikosti písma některým objektům v
	 * diagramu instancí (mimo jiné), tak se musí pohlídat, duplicitní hodnoty, tj.
	 * aby nebyly obě hrany (pro asociaci a agregaci v diagramu instancí) stejné,
	 * protože pokud by byly, pak by se nepodařilo je rozeznat.
	 */
	static final int AGGREGATION_EDGE_DEFAULT_FONT_SIZE = 13;
	
	
	
	
	/**
	 * "Nouzová" výchozí velikost pro hranu, která v diagramu tříd reprezentuje
	 * vztah typu asociace (symetrická). Jde o to, že když uživatel změní písmo
	 * pomocí "rychlého" způsobu - dialogu pro změnu písma, tak se musí otestovat
	 * duplicity, jinak by mohlo dojít k chybám při rozpoznávání objektů v diagramu
	 * tříd, proto, když se tyto duplicity najdou, tak se nastaví výchozí hodnoty
	 * pro příslušné objekty, čímž se duplicity odstraní.
	 */
	static final int ASSOCIATION_EDGE_DEFAULT_FONT_SIZE = 10;
	
	/**
	 * "Nouzová" výchozí velikost pro hranu, která v diagramu tříd reprezentuje
	 * vztah typu dědičnost. Jde o to, že když uživatel změní písmo pomocí
	 * "rychlého" způsobu - dialogu pro změnu písma, tak se musí otestovat
	 * duplicity, jinak by mohlo dojít k chybám při rozpoznávání objektů v diagramu
	 * tříd, proto, když se tyto duplicity najdou, tak se nastaví výchozí hodnoty
	 * pro příslušné objekty, čímž se duplicity odstraní.
	 */
	static final int EXTENDS_EDGE_DEFAULT_FONT_SIZE = 12;
	
	/**
	 * "Nouzová" výchozí velikost pro hranu, která v diagramu tříd reprezentuje
	 * vztah typu agregace 1 : 1 (asymetrická asociace). Jde o to, že když uživatel
	 * změní písmo pomocí "rychlého" způsobu - dialogu pro změnu písma, tak se musí
	 * otestovat duplicity, jinak by mohlo dojít k chybám při rozpoznávání objektů v
	 * diagramu tříd, proto, když se tyto duplicity najdou, tak se nastaví výchozí
	 * hodnoty pro příslušné objekty, čímž se duplicity odstraní.
	 */
	static final int AGGREGATION_1_1_EDGE_DEFAULT_FONT_SIZE = 11;
	
	/**
	 * "Nouzová" výchozí velikost pro hranu, která v diagramu tříd reprezentuje
	 * vztah typu agregace 1 : N. Jde o to, že když uživatel změní písmo pomocí
	 * "rychlého" způsobu - dialogu pro změnu písma, tak se musí otestovat
	 * duplicity, jinak by mohlo dojít k chybám při rozpoznávání objektů v diagramu
	 * tříd, proto, když se tyto duplicity najdou, tak se nastaví výchozí hodnoty
	 * pro příslušné objekty, čímž se duplicity odstraní.
	 */
	static final int AGGREGATION_1_N_EDGE_DEFAULT_FONT_SIZE = 13;
	
	/**
	 * "Nouzová" výchozí velikost pro hranu, která v diagramu tříd reprezentuje
	 * spojení třídy s komentářem. Jde o to, že když uživatel změní písmo pomocí
	 * "rychlého" způsobu - dialogu pro změnu písma, tak se musí otestovat
	 * duplicity, jinak by mohlo dojít k chybám při rozpoznávání objektů v diagramu
	 * tříd, proto, když se tyto duplicity najdou, tak se nastaví výchozí hodnoty
	 * pro příslušné objekty, čímž se duplicity odstraní.
	 */
	static final int COMMENT_EDGE_DEFAULT_FONT_SIZE = 14;
	
	
	
	
	
	// Texty do cybových hláše:
	private static String
	// Duplicita bunky a komentare v diagramu tříd:
	txtJopSameCellsText, txtJopSameCellsTitle,

			// Texty pro duplicitní hrany v diagramu tříd:
			txtJopSameEdgesText_1, txtJopSameEdgesText_2, txtJopSameEdgesText_3, txtJopSameEdgesText_4,
			txtJopSameEdgesText_5, txtJopSameEdgesText_6, txtJopSameEdgesTitle,

			// Texty pro duplicitní hrany v diagramu instancí:
			txtJopSameEdgesInIdText_1, txtJopSameEdgesInIdText_2, txtJopSameEdgesInIdText_3, txtJopSameEdgesInIdTitle;
	
	
	
	
	
	/**
	 * Proměnná, která se používá pouze v případě, že se nastavuje hodnota nějakého
	 * slideru, v případě, že se ta hodnota nastaví, tak se znovu zavolá metoda
	 * stateChanged v příslušném posluchači změn, ale to právě nechci, protože by se
	 * opakovala ta samá metoda, tak v ní pouze otestuji tuto proměnnou a pokud bude
	 * true, tak se ta metoda stateChanged ukončí hned na začátku, pak se tato
	 * proměnná nastaví opět na false a metoda stateChanged semůže vykonávat vesele
	 * dále.
	 */
	boolean onlyUpdateValue = false;
	
	
	
	
	
	/**
	 * Label, který obsahuje "úvodní" informace pro příslušný panel. Například k
	 * čemu slouží jaký slider apod.
	 */
	protected JLabel lblInfo;
	
	
	
	
	/**
	 * Komponenta, která slouží pro zobrazení textu 'Velikost' (v CZ). Vedle této
	 * komponenty bude zobrazena nastavená velikost v příslušném slideru.
	 */
	JLabel lblFontSizeText;

	
	
	/**
	 * Komponenta, která je potřeba pro rozmístění komponent v příslušném panelu.
	 */
	protected GridBagConstraints gbc;

	
	
	
	/**
	 * Proměnná, která slouží pro indexování rozmístění komponent v příslušných
	 * panelech (potomcích).
	 */
	protected int index;
	
	
	
	
	/**
	 * Tato komponenta slouží pro nastavení velikosti fontu / písma.
	 */
	JSlider sdFontSize;
	
	
	
	
	
	/**
	 * Proměnná pro text ve zvleném jazyce, text z této proměnné se využije v labelu
	 * pro zobrazení nastavené velikosti.
	 */
	String txtSize;
	
	
	
	
	
	
	
	
	
	

	/**
	 * Metoda, která vytvoří slider s hodnotami od FONT_SIZE_MIN_VALUE po
	 * FONT_SIZE_MAX_VALUE (8 - 150) a nastaví číslování po 7 a vrátí jej.
	 * 
	 * @param defaultValue
	 *            - výchozí hodnota, na která se nastaví jako označená v příslušném
	 *            slideru.
	 * 
	 * @return výše popsaný slider s nastavenou hodnotou defaultValue a výše
	 *         popsanými vlastnostmi.
	 */
	static JSlider createSlider(final int defaultValue) {
		final JSlider slider = new JSlider(JSlider.HORIZONTAL, FONT_SIZE_MIN_VALUE, FONT_SIZE_MAX_VALUE, defaultValue);

		slider.setMajorTickSpacing(7);
		// slider.setMinorTickSpacing(1);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);

		return slider;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří instanci objktu GridBagConstraint pro rozvržení
	 * komponent a doplní mu výchozí nastavení, které potřebui
	 * 
	 * @return vrátí instanci výše zmíněného objektu se základním nastavením
	 */
	protected static GridBagConstraints createGbc() {
		final GridBagConstraints gbc = new GridBagConstraints();

		gbc.fill = GridBagConstraints.HORIZONTAL;

		gbc.insets = new Insets(5, 5, 5, 5);

		gbc.weightx = 0.2;
		gbc.weighty = 0.2;

		// Následující není povinné, je to myslím implicitně, ale kdybych to náhodou
		// v nějakém dialogu zapoměl změnit: ,..
		gbc.gridwidth = 1;
		gbc.gridheight = 1;

		return gbc;
	}
	
	
	
	
	

	/**
	 * Metoda, která vloží komponentu v parametru na zadnou pozici v dialogu - dle
	 * zadaných parametrů pro řádek a sloupec
	 * 
	 * @param rowIndex
	 *            - index řádku, na který se má vložit komponenta v posledním
	 *            parametru
	 * 
	 * @param columnIndex
	 *            - index sloupce, na který se má vložit komponenta v posledním
	 *            parametru
	 * 
	 * @param component
	 *            - komponenta, která se má vložit do dialogu na výše zadanou pozici
	 *            v layoutu pro GridBagConstraint
	 */
	protected final void setGbc(final int rowIndex, final int columnIndex, final JComponent component) {
		gbc.gridx = columnIndex;
		gbc.gridy = rowIndex;

		add(component, gbc);
	}
	
	
	
	
	
	

	
	/**
	 * Metoda, která slouží pro získání hodnoty z objektu prop, pokud se ji nepodaří
	 * získat vezme se hodnota defaultValue, a když dojde k nějaké chybě, například
	 * se nepodaří přetypovat získanou hodnotu na číslo, tak se také vrátí
	 * defaultValue.
	 * 
	 * @param prop
	 *            - objekt Properties, který obsahuje nastavení pro diagram tříd
	 *            nebo instancí, resp. objekt Properties, ze kterého se má vytáhnout
	 *            nějaká hodnota a přetypovat na Integer.
	 * 
	 * @param key
	 *            - klíč int - ové hodnoty v objektu prop.
	 * 
	 * @param defaultValue
	 *            - výchozí hodnota přetypovaná na String, ale vždy se přetypuje na
	 *            integer (když bude potřeba)
	 * 
	 * @return buď přetypovanou hodnotu defaultValue na Integer pokud dojde k nějaké
	 *         chybě, nebo se nepodaří nalézt čí přetypovat získanou hodnotu z prop
	 *         pod key key. Jinak se vrátí přetypovaná hodnota pod klíčem key na
	 *         Integer.
	 */
	static Integer getIntFromProp(final Properties prop, final String key, final String defaultValue) {
		try {
			return Integer.parseInt(prop.getProperty(key, defaultValue));
		} catch (NumberFormatException e) {
			/*
			 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
			 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
			 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
			 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
			 */
			final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

			// Otestuji, zda se podařilo načíst logger:
			if (logger != null) {
				// Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

				// Nejprve mou informaci o chybě:
				logger.log(Level.INFO,
						"Zachycena výjimka při přetypování hodnoty: '" + key + "' na datový typ Integer. Byla vrácena " +
								"výchozí hodnota: '" + defaultValue + "'.");

				/*
				 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
				 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
				 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
				 */
				logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
			}
			/*
			 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
			 */
			ExceptionLogger.closeFileHandler();

			// Vrácení výchozí hodnoty:
			return Integer.parseInt(defaultValue);
		}
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro nastavení textu do labelu, který zobrazuje
	 * nastavenou velikost ve slideru.
	 * 
	 * @param lbl
	 *            - label, kterému se má nastavit text: Velikost: size.
	 * 
	 * @param size
	 *            - velikost, která se aktuálně nastavuje ve slideru.
	 */
	final void setSizeInLabel(final JLabel lbl, final int size) {
		lbl.setText(txtSize + ": " + size);
	}
	
	
	
	
	
	
	

	
	/**
	 * Metoda, která slouží pro přidání mezery do panelu, je to uděláno tak, že se
	 * pouze přidá jako mezera prázdný JLabel.
	 * 
	 * @param row
	 *            - index řádku, kam se má přidat mezer / prázdný JLabel.
	 */
	final void addSpace(final int row) {
		setGbc(row, 0, new JLabel());
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro nastavení označené hodnoty ve slideru slider na
	 * hodnotu value a to samé udělá i pro objekt - Jlabel label, tedy do labelu
	 * nastaví text -> Velikost: value, jedná se o label pro zobrazení aktuální
	 * označené hodnoty v příslušném slideru.
	 * 
	 * @param slider
	 *            - slider s nastavenou velikosti písma pro nějakou / nějaké
	 *            objekty.
	 * 
	 * @param value
	 *            - nastavená velikosti písma, která se má nastavit jako označená
	 *            hodnota ve slideru tato hodnota se má nastavit i do labelu label.
	 * 
	 * @param label
	 *            - objekt Jlabel, do kterého se má nastavit velikosti value.
	 */
	final void setSliderValue(final JSlider slider, final int value, final JLabel label) {
		slider.setValue(value);
		setSizeInLabel(label, value);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda má buňka pro reprezentaci třídy a buňka pro
	 * reprezentaci komentáře v diagramu tříd stejné vlastnosti, tj. jsou stejně,
	 * pokud ano, pak se buňce pro komentář a třídu nastaví výchozí velikost písma /
	 * fontu a oznámí se to uživateli.
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd.
	 * 
	 * @param checkDuplicatesInDiagrams
	 *            - "komponenta", která obsahuje metody pr ozjištění duplicit v
	 *            diagramu tříd a instancí, například buňky reprezentující třídu a
	 *            komentář mají stejné vlastnosti, nebo nějaké hrany mají stejné
	 *            vlastnosti apod. Toto by mohla být někde statická proměnná, ale
	 *            tyto metody se volají i z jednotlivých diagramů, tak jej zde musím
	 *            předávat, protože je třeba vždy naplnit texty příslušné proměnné
	 *            kvůli chybovým hláškám.
	 * 
	 * @return true v případě, že jsou vlastnosti pro buňku reprezentující třídu a
	 *         komentář stejné, tj. byla nalezena duplicita a byla jim nastavena
	 *         výchozí hodnota, jinak false.
	 */
	public static boolean checkDuplicateClassAndCommentProperties(final GraphClass classDiagram,
			final CheckDuplicatesInDiagramsInterface checkDuplicatesInDiagrams) {
		
		// Zde otestuji, zda jsou buňka pro komentář a třídu stejné:
		if (checkDuplicatesInDiagrams.areClassesAndCommentsSameInCd(GraphClass.KIND_OF_EDGE.getProperties())) {
			/*
			 * Zde se shodují buňky reprezentující třídu a buňky reprezentující komentář -
			 * jejich vlastnosti / design, tak změním jejich velikosti písma na defaultní
			 * velikosti dle Constants, protože tam nemají stejné velikosti písma.
			 */
			FONT_SIZE_OPERATION_CD.quickSetupOfClassFontSize(Constants.CD_FONT_CLASS.getSize(), classDiagram);
			FONT_SIZE_OPERATION_CD.quickSetupOfCommentFontSize(Constants.CD_FONT_COMMENT.getSize(), classDiagram);

			JOptionPane.showMessageDialog(null, txtJopSameCellsText, txtJopSameCellsTitle, JOptionPane.ERROR_MESSAGE);

			return true;
		}
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuji, zda se v diagramu tříd nachází některé hrany, které
	 * mají identické vlastnosti, například jsou testovány hrany pro dědičnost,
	 * asociaci, agregaci 1 : 1, agregaci 1 : N a hrana spojující komentář s třídou.
	 * 
	 * Pokud se najde alespoň jedna shoda, tak se všem výše popsaným hranám nastaví
	 * výchozí velikosti, ale jiná než definovná pro ně, každé se nastaví jiná, aby
	 * byla jistota, že se již nebudou rovnat - nebudou mít stejné vlastnosti.
	 * 
	 * Dále v případě, že se najde nějaká duplicita mezi hranami, tak se to jště
	 * hned po tom, co se jim nastaví "nouzová" výchozí velikost pro zajištění toho,
	 * aby mezi hranami nebyly duplicity, tak se to ještě oznámí uživateli, aby
	 * věděl, proč se ty hrany nenastavily.
	 * 
	 * @param classDiagram
	 *            - reference na diagram tříd.
	 * 
	 * @param checkDuplicatesInDiagrams
	 *            - "komponenta", která obsahuje metody pr ozjištění duplicit v
	 *            diagramu tříd a instancí, například buňky reprezentující třídu a
	 *            komentář mají stejné vlastnosti, nebo nějaké hrany mají stejné
	 *            vlastnosti apod. Toto by mohla být někde statická proměnná, ale
	 *            tyto metody se volají i z jednotlivých diagramů, tak jej zde musím
	 *            předávat, protože je třeba vždy naplnit texty příslušné proměnné
	 *            kvůli chybovým hláškám.
	 * 
	 * @return true v případě, že se najde nějaká duplicita mezi testovanými hranami
	 *         v diagramu tříd, jinak false.
	 */
	public static boolean checkDuplicateEdgesInCd(final GraphClass classDiagram,
			final CheckDuplicatesInDiagramsInterface checkDuplicatesInDiagrams) {
		
		// zde otestuji, zda jsou vybrané / některé hrany stejné:
		if (checkDuplicatesInDiagrams.areEdgesInCdSame(GraphClass.KIND_OF_EDGE.getProperties(), true)) {
			/*
			 * Zde mají alespoň dvě hrany stejné vlastnosti, tak nastavím každé hraně jinou
			 * velikost písma.
			 * 
			 * Jelikož mají všechny hrany v diagramu tříd mají stejnou výchozí velikosti
			 * písma, tak jsem zavedl pro každou hranu jinou velikost.
			 * 
			 * 
			 * Note:
			 * Zde bych mohl třeba obnovit výchozí nastavení hran apod. ale uživatel chce
			 * pouze změnit veliikost písma, tak mu nebudu "měnit" celé nastavení.
			 */
									
			
			// Nastavím "nouzové" výchozí velikosti písma:
			FONT_SIZE_OPERATION_CD.quickSetupOfAssociationEdgeFontSize(ASSOCIATION_EDGE_DEFAULT_FONT_SIZE,
					classDiagram);
			FONT_SIZE_OPERATION_CD.quickSetupOfExtendsEdgeFontSize(EXTENDS_EDGE_DEFAULT_FONT_SIZE, classDiagram);
			FONT_SIZE_OPERATION_CD.quickSetupOfAggregation_1_1_EdgeFontSize(AGGREGATION_1_1_EDGE_DEFAULT_FONT_SIZE,
					classDiagram);
			FONT_SIZE_OPERATION_CD.quickSetupOfAggregation_1_N_EdgeFontSize(AGGREGATION_1_N_EDGE_DEFAULT_FONT_SIZE,
					classDiagram);
			FONT_SIZE_OPERATION_CD.quickSetupOfCommentEdgeFontSize(COMMENT_EDGE_DEFAULT_FONT_SIZE, classDiagram);
			
			
									

			
			// Oznámím to uživateli:
			final String text = txtJopSameEdgesText_1 + ":\n"
					+ txtJopSameEdgesText_2 + ": " + ASSOCIATION_EDGE_DEFAULT_FONT_SIZE + "\n"
					+ txtJopSameEdgesText_3 + ": " + AGGREGATION_1_1_EDGE_DEFAULT_FONT_SIZE + "\n"
					+ txtJopSameEdgesText_4 + ": " + EXTENDS_EDGE_DEFAULT_FONT_SIZE + "\n"
					+ txtJopSameEdgesText_5 + ": " + AGGREGATION_1_N_EDGE_DEFAULT_FONT_SIZE + "\n"
					+ txtJopSameEdgesText_6 + ": " + COMMENT_EDGE_DEFAULT_FONT_SIZE;
			
			JOptionPane.showMessageDialog(null, text, txtJopSameEdgesTitle, JOptionPane.INFORMATION_MESSAGE);
			
			return true;
		}
		
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje, zda mají hrany pro reprezentaci vztahu typu asociace
	 * a vztahu typu agregace mezi instancemi v diagramu instancí stejné vlastnosti,
	 * tj. jsou úplně stejné (stejná barva hrany, barva písma, velikost písma, ...).
	 * 
	 * Tak pokud se najdou duplicity, tak se oběma hranám nastaví "nouzová" výchozí
	 * velikost písma, aby se zajistilo, že nebudou stejné a oznámí se to uživateli,
	 * aby věděl, proč se nenastavila jeho požadovaná velikost.
	 * 
	 * Note:
	 * Ještě by bylo možné obnovit výchozí nastavení, ale v případě, že by si ten
	 * design hran nějak upravil, tak by o vše přišel, což nechci, proto nastavím
	 * jen jinou velikosti písma, protože to je to "nejméně" vidět, to nejméně
	 * důležitá věc na příslušné hraně.
	 * 
	 * @param instanceDiagram
	 *            - reference na diagram instancí.
	 * 
	 * @param checkDuplicatesInDiagrams
	 *            - "komponenta", která obsahuje metody pr ozjištění duplicit v
	 *            diagramu tříd a instancí, například buňky reprezentující třídu a
	 *            komentář mají stejné vlastnosti, nebo nějaké hrany mají stejné
	 *            vlastnosti apod. Toto by mohla být někde statická proměnná, ale
	 *            tyto metody se volají i z jednotlivých diagramů, tak jej zde musím
	 *            předávat, protože je třeba vždy naplnit texty příslušné proměnné
	 *            kvůli chybovým hláškám.
	 * 
	 * @return true v případě, že byla nalezena duplicita mezi hranou asociace a
	 *         agregace v daigramu tříd - jsou stejné, jinak false.
	 */
	public static boolean checkDuplicateEdgesInId(final GraphInstance instanceDiagram,
			final CheckDuplicatesInDiagramsInterface checkDuplicatesInDiagrams) {		
		// Zde otestuji hrany pro asociace a agregaci v diagramu instancí:
		
		if (checkDuplicatesInDiagrams
				.areAssociationAndAggregationSameInId(GraphInstance.KIND_OF_EDGE.getProperties())) {

			// Nastavím "nouzové" výchozí hodnoty:
			FONT_SIZE_OPERATION_ID.quickSetupOfAssociationFontSize(ASSOCIATION_EDGE_DEFAULT_FONT_SIZE_ID,
					instanceDiagram);
			FONT_SIZE_OPERATION_ID.quickSetupOfAgregationFontSize(AGGREGATION_EDGE_DEFAULT_FONT_SIZE, instanceDiagram);

			
			// Oznámím to uživateli:
			final String text = txtJopSameEdgesInIdText_1 + "\n"
					+ txtJopSameEdgesInIdText_2 + ": " + ASSOCIATION_EDGE_DEFAULT_FONT_SIZE_ID + "\n"
					+ txtJopSameEdgesInIdText_3 + ": " + AGGREGATION_EDGE_DEFAULT_FONT_SIZE;

			
			JOptionPane.showMessageDialog(null, text, txtJopSameEdgesInIdTitle, JOptionPane.ERROR_MESSAGE);
			
			return true;
		}
		
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro naplnění některých textových proměnných, které
	 * potřebuji obsahovat text ve zvoleném jazyce. Jedná se o textové proměnné
	 * obsahující texty do chybových hlášek, například, že byly nalezeny duplicitní
	 * hrany nebo třída s komentářem apod.
	 * 
	 * @param properties
	 *            - objekt Properties obsahující texty pro tuto aplikaci v
	 *            uživatelem zvoleném jazyce.
	 */
	public static void setLanguageForErrorText(final Properties properties) {
		if (properties != null) {
			// Duplicita bunky a komentare v diagramu tříd:
			txtJopSameCellsText = properties.getProperty("FSF_FSPA_Txt_Jop_SameCellsText",
					Constants.FSF_FSPA_TXT_JOP_SAME_CELLS_TEXT);
			txtJopSameCellsTitle = properties.getProperty("FSF_FSPA_Txt_Jop_SameCellsTitle",
					Constants.FSF_FSPA_TXT_JOP_SAME_CELLS_TITLE);

			// Texty pro duplicitní hrany v diagramu tříd:
			txtJopSameEdgesText_1 = properties.getProperty("FSF_FSPA_Txt_Jop_SameEdgesText_1",
					Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_1);
			txtJopSameEdgesText_2 = properties.getProperty("FSF_FSPA_Txt_Jop_SameEdgesText_2",
					Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_2);
			txtJopSameEdgesText_3 = properties.getProperty("FSF_FSPA_Txt_Jop_SameEdgesText_3",
					Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_3);
			txtJopSameEdgesText_4 = properties.getProperty("FSF_FSPA_Txt_Jop_SameEdgesText_4",
					Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_4);
			txtJopSameEdgesText_5 = properties.getProperty("FSF_FSPA_Txt_Jop_SameEdgesText_5",
					Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_5);
			txtJopSameEdgesText_6 = properties.getProperty("FSF_FSPA_Txt_Jop_SameEdgesText_6",
					Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_6);
			txtJopSameEdgesTitle = properties.getProperty("FSF_FSPA_Txt_Jop_SameEdgesTitle",
					Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TITLE);

			// Texty pro duplicitní hrany v diagramu instancí:
			txtJopSameEdgesInIdText_1 = properties.getProperty("FSF_FSPA_Txt_Jop_SameEdgesInIdText_1",
					Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_IN_ID_TEXT_1);
			txtJopSameEdgesInIdText_2 = properties.getProperty("FSF_FSPA_Txt_Jop_SameEdgesInIdText_2",
					Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_IN_ID_TEXT_2);
			txtJopSameEdgesInIdText_3 = properties.getProperty("FSF_FSPA_Txt_Jop_SameEdgesInIdText_3",
					Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_IN_ID_TEXT_3);
			txtJopSameEdgesInIdTitle = properties.getProperty("FSF_FSPA_Txt_Jop_SameEdgesInIdTitle",
					Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_IN_ID_TITLE);
		}
		
		
		else {
			// Duplicita bunky a komentare v diagramu tříd:
			txtJopSameCellsText = Constants.FSF_FSPA_TXT_JOP_SAME_CELLS_TEXT;
			txtJopSameCellsTitle = Constants.FSF_FSPA_TXT_JOP_SAME_CELLS_TITLE;

			// Texty pro duplicitní hrany v diagramu tříd:
			txtJopSameEdgesText_1 = Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_1;
			txtJopSameEdgesText_2 = Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_2;
			txtJopSameEdgesText_3 = Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_3;
			txtJopSameEdgesText_4 = Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_4;
			txtJopSameEdgesText_5 = Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_5;
			txtJopSameEdgesText_6 = Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TEXT_6;
			txtJopSameEdgesTitle = Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_TITLE;

			// Texty pro duplicitní hrany v diagramu instancí:
			txtJopSameEdgesInIdText_1 = Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_IN_ID_TEXT_1;
			txtJopSameEdgesInIdText_2 = Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_IN_ID_TEXT_2;
			txtJopSameEdgesInIdText_3 = Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_IN_ID_TEXT_3;
			txtJopSameEdgesInIdTitle = Constants.FSF_FSPA_TXT_JOP_SAME_EDGES_IN_ID_TITLE;
		}
	}
}
