package cz.uhk.fim.fimj.font_size_form;

import java.util.Properties;

/**
 * Rozhraní, které obsahuje metody, které jsou potřeba pro zjištění duplicit v diagramu tříd a v diagramu instancí, k
 * tomu může dojít například když jsou třeba pro buňky reprezentující třídu a komentář úplně stejně, jen se liší ve
 * velikosti písma, pak když uživatel nataví písmo pomocí dialogu pro "rychlé" nastavení písma, tak budou ty buňky
 * stejné, tak by je nebylo možné rozeznat, podobně pro hrany v obou diagramech, tak zde jsou tyto metody, které
 * otestují shody.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public interface CheckDuplicatesInDiagramsInterface {

    /**
     * Metoda, která otestuje, zda jsou hodnoty vlastnosti pro buňku reprezentující třídu a vlastnosti pro buňku
     * reprezentující komentář v diagramu tříd stejné nebo ne.
     *
     * @param diagramProp
     *         - objekt Properties, u kterého se má zjistit, zda hodnoty, které se v něm nacházejí pro třídu a komentář
     *         jsou stejné nebo ne. Tj. tento objekt je odněkud načtený objekt Properties, resp. soubor
     *         ClassDiagram.properties, ve kterém se má zjistit, zda obsahuje stejné hodnoty pro buňku, která v
     *         diagramu
     *         tříd reprezentuje třídu a komentář.
     *         <p>
     *         Toto zde bylo potřeba takto udělat, protože se toto testování nachází, resp. provádí z více míst a
     *         jediné
     *         co se liší je ten objekt Properties, ve kterém se mají testovat ty hodnoty, tak jej zde pouze předávám.
     *         Ten objekt properties je získám z aplikace z GraphClass v případě testování duplicit při změně velikosti
     *         písma, nebo je to soubor ClassDiagram.properties načtený z workspace v případě testování duplicit při
     *         změně souboru.
     * @return true, pokud jsou buňky pro třídu a komentář v diagramu tříd stejné, jinak false.
     */
    boolean areClassesAndCommentsSameInCd(final Properties diagramProp);


    /**
     * Metoda, která zjistí, zda se ve vlastnostech vybraných hran z diagramu tříd vyskytují nějaké duplicity.
     *
     * @param diagramProp
     *         - objekt Properties, u kterého se má zjistit, zda hodnoty, které se v něm nacházejí pro hrany jsou
     *         stejné
     *         nebo ne. Tj. tento objekt je odněkud načtený objekt Properties, resp. soubor ClassDiagram.properties, ve
     *         kterém se má zjistit, zda obsahuje stejné hodnoty pro hrany, které v diagramu tříd reprezentují nějaké
     *         vztahy mezi třídami nebo přípojení komentáře ke třídě.
     *         <p>
     *         Toto zde bylo potřeba takto udělat, protože se toto testování nachází, resp. provádí z více míst a
     *         jediné
     *         co se liší je ten objekt Properties, ve kterém se mají testovat ty hodnoty, tak jej zde pouze předávám.
     *         Ten objekt properties je získám z aplikace z GraphClass v případě testování duplicit při změně velikosti
     *         písma, nebo je to soubor ClassDiagram.properties načtený z workspace v případě testování duplicit při
     *         změně souboru.
     * @param writeDupliciteInfo
     *         - logická proměnná, která značí, zda se v případě, že se najdou duplicity v nastavení mezi vybranými
     *         hranami, tak zda se mají vypsat informace o duplicitě, mezi kterými hranmi byla ta duplicita nalezena,
     *         toto je potřeba akorát protože se tato metoda volá z více míst a pokud uživatel natavuje velikost písma
     *         pomocí příslušného dialogu, tak chci, aby to okamžitě veděl, ale pokud se jedná o testování duplicit při
     *         změně v souborech, tak se tyto informace vypíšou do přílušného dialogu, takže to zde není potřeba
     *         vypisovat.
     * @return true, pokud byla nalezena duplicity v nastavení designu hran (minimálně dvě hrany jsou stejné), jinak se
     * vrátí false, pokud nebude nalezena žádná duplicita.
     */
    boolean areEdgesInCdSame(final Properties diagramProp, final boolean writeDupliciteInfo);


    /**
     * Metoda, která zjistí, zda jsou vlastnosti pro hrany reprezentující asociaci a agregaci v diagramu instnací
     * stejné, tj. Zda jsou stejné hrany pro reprezentaci vztahu typu asociace a agregace v diagramu instancí.
     *
     * @param diagramProp
     *         - objekt Properties, u kterého se má zjistit, zda hodnoty, které se v něm nacházejí pro hrany jsou
     *         stejné
     *         nebo ne. Tj. tento objekt je odněkud načtený objekt Properties, resp. soubor InstanceDiagram.properties,
     *         ve kterém se má zjistit, zda obsahuje stejné hodnoty pro hrany, které v diagramu instancí reprezentují
     *         vztahy pro asociaci a agregaci - zda jsou ty hrany stejné nebo ne.
     *         <p>
     *         Toto zde bylo potřeba takto udělat, protože se toto testování nachází, resp. provádí z více míst a
     *         jediné
     *         co se liší je ten objekt Properties, ve kterém se mají testovat ty hodnoty, tak jej zde pouze předávám.
     *         Ten objekt properties je získám z aplikace z GraphInstance v případě testování duplicit při změně
     *         velikosti písma, nebo je to soubor IntanceDiagram.properties načtený z workspace v případě testování
     *         duplicit při změně souboru.
     * @return true, pokud jsou vlastnosti pro hrany reprezentující asociaci a agregaci v diagramu instancí stejné,
     * jinak false.
     */
    boolean areAssociationAndAggregationSameInId(final Properties diagramProp);
}
