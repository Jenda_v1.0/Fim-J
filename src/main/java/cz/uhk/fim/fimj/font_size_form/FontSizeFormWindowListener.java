package cz.uhk.fim.fimj.font_size_form;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import cz.uhk.fim.fimj.app.App;
import cz.uhk.fim.fimj.file.ConfigurationFiles;
import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.ReadFile;
import cz.uhk.fim.fimj.file.WriteToFile;
import cz.uhk.fim.fimj.language.EditProperties;

/**
 * Tato třída slouží jako "posluchač", který čeká na zavření dialogu pro "rychlé" nastavení velikosti písma.
 * <p>
 * Jde to to, že si uživatel mmůže v dialogu nastavit některé možnosti, jako je třeba panel s komponentami pro nastaveí
 * velkosti, tj. jestli použit ten panel s jednou velikostí pro všehny komponenty nebo individuální velikost pro každou
 * komponentu apod. Tak chci, aby si aplikace pamatovala, co v ní uživatel nastavil.
 * <p>
 * Pouze poznamenám, že v dialogu, v panelu pro nastavení velikosti písma individuálně dle výběru - označení tak tam
 * bych nemusel dávat to ukládání, protože si jednou uživatel může nastavit třeba "něco" - třeba jen velikost písma ve
 * třídě a pak přepne na instnaci a nastaví velikost písma jen pro instanci a to by tam zůstalo, tak to být nemusí,
 * protože při dalším otevření dialogu bude uživatel chtít třeba nastavit něco jiného, to nelze dopředu předpovědět, tak
 * nechám vždy poslední označené hodnoty, při nejhorším může uživatel využít ta tlačítka pro odznačení nebo označení, a
 * nebo je naklikat ručně.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class FontSizeFormWindowListener extends WindowAdapter {
	
	/**
	 * Reference na dialog pro "rychlé" nastavení písma.
	 */
	private final FontSizeForm dialog;
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param dialog
	 *            - refere na dialog FontSizeForm, který slouží pro "rychlý" způsob
	 *            editace velikosti písma v objektech v diagramu tříd a v diagramu
	 *            instancí.
	 * 
	 *            Je zde potřeba kvůli metodám pro získání nastavených hodnot.
	 */
	FontSizeFormWindowListener(final FontSizeForm dialog) {
		super();
		
		this.dialog = dialog;
	}
	
	
	
	
	
	
	
	@Override
	public void windowClosed(WindowEvent e) {
		/*
		 * Tato metoda se zavolá pokaždé - jak při zavření pomocí tlačítka v menu, tak
		 * při zavření pomocí křížku nebo Alt + F4 apod.
		 */
		
		/*
		 * Postup:
		 * Načtu si soubor Default.properties (se zachováním komentářů), vložím do něj
		 * hodnoty a zse jej zapíšu na původní umístění.
		 * 
		 * Zde budu (podobně jako v dialogu nastaveníú řešit případ, kdy neexistuje
		 * příslušný soubor Default.properties v adresáři workspace. Sice to není zrovna
		 * akutní, nastavení tohoto dialogu pro natavení velikosti písma není moc
		 * důležité, ale dále se do tohoto souboru ukládájí informace například o
		 * jazyce, workspace apod. Tak jej pro jistotu vytvořím.
		 */
		
		EditProperties defaultPropertiesInWorkspace = App.READ_FILE
				.getPropertiesInWorkspacePersistComments(Constants.DEFAULT_PROPERTIES_NAME);
		
		if (defaultPropertiesInWorkspace == null) {
			final String pathToWorkspace = ReadFile.getPathToWorkspace();

			/*
			 * Zde pouze zjisšťuji, zda existují veškeré adresáře k cestě pro soubor
			 * Default.properties v adresáři configuration ve workspace. Pokud ne, pak v
			 * rámci možností nekteré adresáře mohu vytvořit, jinak není kam uložit soubory,
			 * pokud například byl smazan workspace apod. Pak už zbývá jedině znovu vytvořit
			 * adresář workspace s projekty apod. Ale tuto část zde vynechávám, protože o
			 * tom uživatel musí vědět - že si smazal projekty případně workspace apod. Tak
			 * snad musí vědět, že nelze pracovat s projekty / soubory, které byly smazány.
			 * 
			 * Otestuji, zda existuje workspace, pak configuration, pak příslušný soubor,
			 * případně některé z nich mohu vytvořit:
			 */
			if (pathToWorkspace != null) {
				/*
				 * Pokud neexsistuje adresář configuration ve workspace, bude vytvořen i s
				 * potřebnými soubory s výchozím nastavením.
				 * 
				 * Pokud adresář configuration ve workspace existuje, pak stačí vytvořit jen
				 * soubor Default.properties, protože vím, že neexistuje, když se jej nepodařilo
				 * načíst.
				 */
				if (ReadFile.existsDirectory(pathToWorkspace + File.separator + Constants.CONFIGURATION_NAME))
					ConfigurationFiles.writeDefaultPropertiesAllNew(
							pathToWorkspace + File.separator + Constants.PATH_TO_DEFAULT_PROPERTIES);

				else
					WriteToFile.createConfigureFilesInPath(pathToWorkspace);

				// Znovu načtu soubor Default.properties z workspace z configuration, protože
				// výše byl znovu vytvořn:
				defaultPropertiesInWorkspace = App.READ_FILE
						.getPropertiesInWorkspacePersistComments(Constants.DEFAULT_PROPERTIES_NAME);
			}
			/*
			 * část else:
			 * 
			 * Zde by bylo možné vytvořit dříve zadaný adresář workspace - pokud byl již
			 * zadán, ale byl smazán, ale to by v rámci aplikace nemělo nastat, proto tuto
			 * část vynechám, uživatel jej musí smazat sám, pak o tom musí vědět.
			 */
		}
		
		
		
		
		// Pokud ani tak nebyl soubor načten, mohu skončit:
		if (defaultPropertiesInWorkspace == null)
			return;
		
		
		
		// Zde zapíšu data do objektu Properties a uložím jej:
		
		// Zda se má písmo nastavovat i během posouvání slideru nebo ne:
		defaultPropertiesInWorkspace.setProperty("FontSizeForm_UpdateDiagramsWhileMovingWithSlider",
				String.valueOf(FontSizeForm.isSetValuesWhileMoving()));

		
		// zvolený panel v dialogu:
		defaultPropertiesInWorkspace.setProperty("FontSizeForm_SelectedPanel", FontSizeForm.getFormType().getValue());

		
		
		
		// Chcbs z panelu pro nastavení písma v označených komponentách:
		
		/*
		 * Panel, který obsahuje komponenty JCheckBox, které představují objekty v
		 * diagramu tříd. Právě to, zda jsou tyto komponenty označeny potřebuji ukládat
		 * do souboru.
		 */
		final SizeSelectedObjectCdPanel pnlWithChcbsForCd;

		if (dialog.getPnlSameSizeForSelectedObjects() != null)
			pnlWithChcbsForCd = dialog.getPnlSameSizeForSelectedObjects().getPnlWithChcbsForCd();

		else
			pnlWithChcbsForCd = null;
		
		

		
		
		
		/*
		 * Panel, který obsahuje komponenty JCheckBox, které představují objekty v
		 * diagramu instancí. Právě to, zda jsou tyto komponenty označeny potřebuji
		 * ukládat do souboru.
		 */
		final SizeSelectedObjectIdPanel pnlWithChcbsForId;

		if (dialog.getPnlSameSizeForSelectedObjects() != null)
			pnlWithChcbsForId = dialog.getPnlSameSizeForSelectedObjects().getPnlWithChcbsForid();

		else
			pnlWithChcbsForId = null;
		
			
			
		

		// Pro diagram tříd:
		
		/*
		 * Tato podmínka je potřeba, protože se jednotlivé panely s komponentami vytváří
		 * až v případě potřeby, takže kdyby na ně třeba uživatel vůbec neklikl, tak se
		 * ani nevytvoři, pak není třeba nic ukládat, měli by být v souboru původní nebo
		 * výchozí hodnoty) a v této konkrétní relaci nic nezměnil.
		 */
		if (pnlWithChcbsForCd != null) {
			defaultPropertiesInWorkspace.setProperty("FontSizeForm_CD_ClassCell",
					String.valueOf(pnlWithChcbsForCd.isChcbClassSelected()));

			defaultPropertiesInWorkspace.setProperty("FontSizeForm_CD_CommentCell",
					String.valueOf(pnlWithChcbsForCd.isChcbCommentSelected()));

			defaultPropertiesInWorkspace.setProperty("FontSizeForm_CD_CommentEdge",
					String.valueOf(pnlWithChcbsForCd.isChcbCommentEdgeSelected()));

			defaultPropertiesInWorkspace.setProperty("FontSizeForm_CD_ExtendsEdge",
					String.valueOf(pnlWithChcbsForCd.isChcbExtendsSelected()));

			defaultPropertiesInWorkspace.setProperty("FontSizeForm_CD_ImplementsEdge",
					String.valueOf(pnlWithChcbsForCd.isChcbImplementsSelected()));

			defaultPropertiesInWorkspace.setProperty("FontSizeForm_CD_AssociationEdge",
					String.valueOf(pnlWithChcbsForCd.isChcbAssociationSelected()));

			defaultPropertiesInWorkspace.setProperty("FontSizeForm_CD_Aggregation_1_1_Edge",
					String.valueOf(pnlWithChcbsForCd.isChcbAggregation_1_1_Selected()));

			defaultPropertiesInWorkspace.setProperty("FontSizeForm_CD_Aggregation_1_N_Edge",
					String.valueOf(pnlWithChcbsForCd.isChcbAggregation_1_N_Selected()));
		}


		// Pro diagram instancí:

		// Podmínka popsána výše.
		if (pnlWithChcbsForId != null) {
			defaultPropertiesInWorkspace.setProperty("FontSizeForm_ID_InstanceCell",
					String.valueOf(pnlWithChcbsForId.isChcbInstanceSelected()));

			defaultPropertiesInWorkspace.setProperty("FontSizeForm_ID_InstanceCell_Attributes",
					String.valueOf(pnlWithChcbsForId.isChcbInstanceAttributesSelected()));

			defaultPropertiesInWorkspace.setProperty("FontSizeForm_ID_InstanceCell_Methods",
					String.valueOf(pnlWithChcbsForId.isChcbInstanceMethodsSelected()));

			defaultPropertiesInWorkspace.setProperty("FontSizeForm_ID_AssociationEdge",
					String.valueOf(pnlWithChcbsForId.isChcbAssociationSelected()));

			defaultPropertiesInWorkspace.setProperty("FontSizeForm_ID_AggregationEdge",
					String.valueOf(pnlWithChcbsForId.isChcbAggregationSelected()));
		}

		
		
		
		
		// Na konec mohu uložit soubor zpět na původní umístění:
		defaultPropertiesInWorkspace.save();
	}
}
