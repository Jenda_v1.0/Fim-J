package cz.uhk.fim.fimj.popup_menu;

import java.awt.Color;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.swing.*;
import javax.swing.border.BevelBorder;

import cz.uhk.fim.fimj.commands_editor.MakeOperation;
import cz.uhk.fim.fimj.file.OutputEditorInterface;
import cz.uhk.fim.fimj.reflection_support.ParameterToText;
import cz.uhk.fim.fimj.reflection_support.ReflectionHelper;
import cz.uhk.fim.fimj.forms.SelectInstancesForCreateReference;
import cz.uhk.fim.fimj.instance_diagram.GraphInstance;
import cz.uhk.fim.fimj.instance_diagram.overview_of_instance.VariableInfo;
import cz.uhk.fim.fimj.instances.OperationsWithInstancesInterface;
import cz.uhk.fim.fimj.output_editor.OutputEditor;

/**
 * Tato třída slouží jako abstraktní třída, která obsahuje "společné" proměnné pro kontextová menu pro instanci v
 * diagramu instancí a třídu v diagramu tříd, abych tyto proměnné nemusel inicializovat vícekrát.
 * <p>
 * Tj., když se klikne na buňku, která reprezentuje instnaci v diagramu instancí pravým tlačítkem myši, tak se zobrazí
 * "toto" kontextové menu s příslušnými možnostmi. To samé platí pro buňku reprezentující třídu v diagramu tříd.
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class PopupMenuAbstract extends JPopupMenu {

	private static final long serialVersionUID = 1L;


	/**
	 * Konstanta / značka, která se používá pro oddělení jednotlivých tříd v tooltipu - v položkách pro zděděné metody
	 * v kontextovém menu nad třídou / instancí.
	 */
	private static final String ARROW = " -> ";




	/**
	 * Výchozí barva pro položku "Odebrat" nad třídou v diagramu tříd a nad instancí
	 * v diagramu instancí.
	 */
	protected static final Color REMOVE_ITEM_FOREGROUND_COLOR = new Color(139, 0, 0);
	
	
	
	
	
	/**
	 * Proměnná, která značí počet položek, které se mají scrolovat ve scrolovacím
	 * menu. Tato proměnná je využita pro "vnitřní" menu, tj. je PopupMenu nad
	 * třídou nebo instancí třídy, a to obsahuje menu s metodami, tak to menu s
	 * metodami bezu jako "vnitřní" a pro tato "vnitřní" menu je tato proměnná.
	 * 
	 * Například:
	 * 
	 * item 1
	 * item 2
	 * arrow up
	 * item 3
	 * item 4
	 * item 5
	 * item 6
	 * item 7
	 * item 8
	 * item 9
	 * item 10
	 * arrow down
	 * item 11
	 * item 12
	 * 
	 * položky. item 3 - item 10 budou ve scrolovacím menu, takže v tomto příkladě
	 * jich je celkem 8, takže když bud tato proměnná třeba 3, tak budou zobrazené
	 * vždy jen 3 položky a mezi zbývajícími 5 se bude Scrolovat.
	 */
	protected static final int SCROLL_COUNT = 15;
	
	
	/**
	 * Tato položka je to samé co položka SCROLL_COUNT, akorát se jedná o proměnnou,
	 * která značí počet konstruktorů pro rolování v popupMenu nad třídou v diagramu
	 * tříd.
	 */
	protected static final int SCROLL_COUNT_CONSTRUCTORS = 10;

	
	/**
	 * Tato položka je to samé co položka SCROLL_COUNT, akorát se jedná o proměnnou,
	 * která značí počet metod pro rolování v popupMenu nad instancí v diagramu
	 * instancí.
	 */
	protected static final int SCROLL_COUNT_METHODS = 20;
	
	
	
	/**
	 * Interval ve scrolování pro menu, v podstatě rychlost scrolování mezi
	 * položkami.
	 */
	protected static final int SCROLL_INTERVAL = 100;
	
	
	/**
	 * Proměnná, která obsahuje počet pevně zafixovaných položek v horní části menu,
	 * například v příkladu u proměnné výše s položkami. jsou upevněné položky item
	 * 1 a item 2, takže počet je 2.
	 */
//	protected static final int TOP_FIXED_COUNT = 3;
	protected static final int TOP_FIXED_COUNT = 0;
	
		
	/**
	 * Proměnná, která obsahuje počet pevně zafixovaných položek v dolní části menu,
	 * například v příkladu u proměnné výše s položkami. jsou upevněné položky item
	 * 11 a item 12, takže počet je 2.
	 */
//	protected static final int BOTTOM_FIXED_COUNT = 3;
	protected static final int BOTTOM_FIXED_COUNT = 0;
	
	
	
	
	
	
	
	

	
	
	/**
	 * Proměnná, která slouží pro uložení reference pro objekt, který obsahuje texty
	 * pro tuto aplikaci v příslušném (zvoleném) jazyce.
	 */
	protected Properties languageProperties;
	
	
	/**
	 * Reference pro objekt pro manipulaci s instancemi tříd (například zavolání
	 * metody apod.).
	 */
	protected static OperationsWithInstancesInterface operationWithInstances;
	
	
	
	/**
	 * Komponenta pro zobrazení výstupních textů, resp. nějakých výstupů od
	 * aplikace, například po zavolání metody, provedení nějakého příkazu apod.
	 * 
	 * Do tohoto editoru se budou vypisovat některá hlášení ohledně výsledku
	 * provedení některých operací apod.
	 */
	protected OutputEditor outputEditor;
	
	
	
	
	
	
	
	
	
	/**
	 * Referencena diagram instací, abych jej nemusel předávat do parametrů několika
	 * metod, ktak jej zde uložím jako globální neboli instanční proměnnou.
	 */
	protected GraphInstance instanceDiagram;
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Logická proměnné, dle které se pozná, zda se mají v dialogu pro zavolání
	 * metody zpřístupnít veřejné a chráněné proměnné pro předání do příslušného
	 * parametru metody. Jednalo by se pouze o veřejné a chráněné (popř. staticé)
	 * proměnné, které se nachází v třídách v diagramu tříd, v instancích v diagramu
	 * instancí a proměnné vytvořené uživatelem v editoru příkazů. A vždy by se
	 * jedna o proměnné, které se si vyfiltrovaly ze všech nalezených proměnných,
	 * které jsou stejného datového typu, který potřebuje příslušný parametr metody.
	 */
	protected boolean makeVariablesAvailableForCallMethod;
	
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy. Tento konstruktor je zde pouze pro to, abych mohl
	 * nastavit jakoby "zvýšený" vzhled toho menu, resp. jeho tlačítek.
	 */
	public PopupMenuAbstract() {
		super();

		setBorder(new BevelBorder(BevelBorder.RAISED));
	}










	/**
	 * Metoda, která získané veřejné a chráněné metody z předků třídy clazz (té první předané). Ale vyfiltruje se
	 * metoda
	 * "main", ta nebude zahrnuta. Vždy se ze třídy clazz načte i třída, ze které dědí a z ní si také načte veškeré
	 * metody, které deklaruje a jsou opět statické věřejné nebo chráněné, protože pouze tyto metody lze zavolat i z
	 * potomka.
	 *
	 * @param clazz
	 *         - Třída, ze které se mají vzít metody, až se všechny její metody projdou, tak se zjistí, zda z něčeho
	 *         dědí a tento předek třídy clazz není Object, pak se rekurzí pokračuje.
	 * @param methods
	 *         - List, do kterého se budou vkládat informace o nalezené metodě.
	 * @param mustBeStatic
	 *         - proměnná, která definuje, zda musí být metoda statická nebo ne.
	 * @param tooltip
	 *         - text, který bude značit tooltip / popisek v kontextovém menu - reprezentuje řetězec dědičnosti - "jaká
	 *         třída z jaké dědí"
	 * @return list methods, která bude obsahovat veškeré povolené metody.
	 */
	protected static List<MethodInfo> getInheritedMethods(final Class<?> clazz, final
	List<MethodInfo> methods, final boolean mustBeStatic, String tooltip) {

		// Část s null by nastat neměla, Object by mohl nad třídou v diagramu tříd:
		if (clazz == null || clazz.equals(Object.class))
			return methods;


		/*
		 * Získám si veškeré metody, které se nachází v předkovi označené třídy.
		 */
		final Method[] methodsTempArray = clazz.getDeclaredMethods();

		/*
		 * Pokud byly nějaké metodynačteny, pokračuji tak, že si uložím text do tooltipu, tj. aktuální řetěc, v jakém
		 * je implementována dědičnost metod.
		 *
		 * Poté projdu všechny naštěné metody (v methodsTempArray) a vyfiltruji pouze ty, které jsou statické veřejné
		 * nebo chráněné.
		 */
		if (methodsTempArray != null) {
			tooltip += ARROW + clazz.getName();


			final List<Method> tempMethodsList = new ArrayList<>();

			Arrays.stream(methodsTempArray).forEach(m -> {
				if (mustBeStatic && !Modifier.isStatic(m.getModifiers()))
					return;

				if ((Modifier.isPublic(m.getModifiers()) || Modifier.isProtected(m.getModifiers())) && !tempMethodsList
						.contains(m) && !isMainMethod(m))
					tempMethodsList.add(m);
			});

			methods.add(new MethodInfo(clazz, tempMethodsList, tooltip));
		}


		final Class<?> superClass = clazz.getSuperclass();

		if (superClass != null && !superClass.equals(Object.class))
			getInheritedMethods(superClass, methods, mustBeStatic, tooltip);

		return methods;
	}










	/**
	 * Metoda, která slouží pro vytvoření položky do rozevíracího menu. Bude se jednat o položku, která bude
	 * reprezentovat nějakou metodu (zděděnou, ve vnitřní třídě apod.) a to rozevírací menu, do kterého vrácená položka
	 * z této metody přijde je například nějaký předek označené třídy / instance v diagramu tříd nebo instancí nebo
	 * statická metoda ve vnitřní třídě apod..
	 *
	 * @param method
	 *         - metoda, kterou bude reprezentovat vrácená položka
	 * @return položku, která bude reprezentovat nějakou metodu.
	 */
	protected static JMenuItem getMethodMenuItem(final Method method) {
		/*
		 * Získám si metodu v textu. Resp. tato proměnná bude obsahovat metody v podobě textu:
		 * návratováHodnota názevMetody(datovýTypParametru, p2, ...);
		 *
		 * Metoda getMethodReturnDataType mi vrátí návratový datový typ metody v podobě textu (již zpracované
		 * genericé datové typy apod.).
		 *
		 * Pak se jen přidá název metody.
		 */
		final String textToMenuItem = ParameterToText.getMethodReturnTypeInText(method, false) + " " + method.getName();

		/*
		 * K metodě ještě přidám její parametry v kulatých závorkách a středník na konec.
		 */
		final String text = ReflectionHelper.getMethodNameAndParametersInText(textToMenuItem, method.getParameters()) + ";";

		/*
		 * Položka, která bude reprezentovat konkrétní (aktuálně ierovanou) metodu. Po kliknutí na toto
		 * tlačítko se metoda zkusí zavolat.
		 */
		return new JMenuItem(text);
	}











	/**
	 * Metoda, která vytvoří rozevírací menu, které bude reprezentovat nějakou třídu (předka, vnitřní třídu apod.).
	 * <p>
	 * (Tato meto je pouze pro zjednodušení, abych neopakoval stejný kód vícekrát s jinými hodnotami. Viz místa použítí
	 * této metody)
	 *
	 * @param clazz
	 *         - třída, která je potřeba pro získání názvu třídy samotné nebo třídy i s jejími balíčky.
	 * @param showClassNameWithPackage
	 *         - zda má v tom rozevíracím menu být pouze název třídy nebo název třídy s balíčky.
	 * @param tooltip
	 *         - tooltip - volitelný, aplikuje se pouze v případě, že není null.
	 * @return vytvořené výše popsané rozevírací menu.
	 */
	protected static JMenu getMenuForClass(final Class<?> clazz, final boolean showClassNameWithPackage, final
	String tooltip) {
		final JMenu menu;

		if (showClassNameWithPackage)
			menu = new JMenu(clazz.getName());
		else menu = new JMenu(clazz.getSimpleName());

		if (tooltip != null)
			menu.setToolTipText(tooltip);

		new PopupMenuScroller(menu, SCROLL_COUNT, SCROLL_INTERVAL, TOP_FIXED_COUNT, BOTTOM_FIXED_COUNT);

		return menu;
	}


	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda se jedná o hlavní / spuštěcí metodu 'main' nebo ne.
	 * 
	 * To se zjistí tak, že se otestuje, zda je metoda statická, veřejná, nic
	 * nevrací a má jako parametr jednorozměrné pole typu String.
	 * 
	 * Note:
	 * To že je metoda statická už bych testovat nemusel, to se zjišťuje v jiné
	 * metodě, ale pro případ, že bych se někdy rozhodl tuto metodu využít i jinde,
	 * raději to zde doplním abych předešel nějakým "komplikacím", kdybych někde
	 * něco změnil a na něco případně zapoměl - někde něco otestovat apod.
	 * 
	 * @param method
	 *            - Metoda z nějaké třídy, o které se zjistí, zda se jedná o metodu
	 *            'main' dle výše uvedeného postupu.
	 * 
	 * @return true, pokud se jedná o metodu main dle výše uvedeného postupu, jinak
	 *         false.
	 */
	private static boolean isMainMethod(final Method method) {
		if (Modifier.isPublic(method.getModifiers()) && Modifier.isStatic(method.getModifiers())
				&& method.getName().equals("main") && method.getReturnType().equals(Void.TYPE)) {

			// Otestuji parametry metody:
			final Type[] typesArray = method.getParameterTypes();
			final Type[] stringType = { String[].class };

			return Arrays.deepEquals(typesArray, stringType);
		}

		return false;
	}










	/**
	 * Metoda, která otestuje, zda odpovídají modifikátory viditenosti (modifiers) povoleným hodnotám. Tzn. Že dle
	 * ostatních parametrů metody zjistím, zda ty modifikátory nemohou být privátní, ale jsou privátní, tak vrátím
	 * false. Podobně pro ostatní typy viditelností.
	 *
	 * @param modifiers
	 *         - modifikátory viditelnosti nějakého "objektu", například metody, konstruktoru, třídy apod.
	 * @param includePrivateMethods
	 *         - proměnná, která značí, zda modifikátory mohou být soukromé (private), pokud ne, a modifiers značí
	 *         soukromou hodnotu viditelnosti, pak se vrátí false.
	 * @param includeProtectedMethods
	 *         - proměnná, která značí, zda modifikátory mohou být chráněné (protected), pokud ne, a modifiers značí
	 *         chráněnou hodnotu viditelnosti, pak se vrátí false.
	 * @param includePackagePrivateMethods
	 *         - proměnná, která značí, zda modifikátory mohou být v rámci balíčku (package-private), pokud ne, a
	 *         modifiers značí package-private hodnotu viditelnosti, pak se vrátí false.
	 * @return true se vrátí pouze v případě, že se jedná o veřejný modifikátor viditelnosti nebo jeden z výše
	 * zvmíněných, který bude zrovna true.
	 */
	protected static boolean modifiersCorrespond(final int modifiers, final boolean includePrivateMethods, final
	boolean includeProtectedMethods, final boolean includePackagePrivateMethods) {

		if (!includePrivateMethods && Modifier.isPrivate(modifiers))
			return false;

		if (!includeProtectedMethods && Modifier.isProtected(modifiers))
			return false;

		if (!includePackagePrivateMethods && ReflectionHelper.isPackagePrivateVisibility(modifiers))
			return false;

		return true;
	}




	
	
	
	
	
	
	
	
	/**
	 * Metoda, která do editoru výstupů vypíše návratovou hodnotu metody, která byla
	 * zavolána. Ale ještě před výpisem otestuje, zda se jedná o nějaké pole nebo
	 * ne, pokud ano, tak to dané pole převede na text.
	 * 
	 * @param result
	 *            - výsledek uperace - Úspěch, případně podobný text, který se
	 *            vypíše před návratovou hodnotou returnedValue.
	 * 
	 * @param returnedValue
	 *            - návratová hodnota metody
	 * 
	 * @param outputEditor
	 *            - editor, kam se mají vypsat výsledné hodnoty.
	 */
	public static void printReturnedValueToEditor(final String result, final Object returnedValue,
			final OutputEditorInterface outputEditor) {
		
		if (returnedValue != null && returnedValue.getClass().isArray())
			/*
			 * Zde bych mohl znovu napsat metodu, která se bude nacházet v této třídě a bude
			 * sloužít pro převod X - rozměrného pole do podoby textu pro vypsání do
			 * editoru, ale už ji mám, tak ji zavolám:
			 */
			outputEditor.addResult(MakeOperation.fromArrayToString(returnedValue));
		
		/*
		 * Při zavolání metody toString by mohla vyskočit výjimka NullPointerEx, když by
		 * byl objekt returnedValue null, proto jej pouze vypíši, kde se volá uvedená
		 * metoda "automaticky".
		 */
//		else outputEditor.addResult(result + returnedValue.toString());
		else outputEditor.addResult(result + returnedValue);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která otestuje získanou hodnotu ze zavolání nějaké metody (hodnota
	 * objValue), zda to není null hodnota, je to třída, která se nachází v diagramu
	 * tříd a zároveň ta hodnota objValue je instance nějaké třídy (z diagramu
	 * tříd), která se dosud nenachází v diagramu intancí. D8le, pokud je objValue
	 * pole nebo list, tak se celé projde a pro každou položku se zjistí, zda se
	 * jedná o instance třídy z diagramu tříd, která se ještě nenachází v diagramu
	 * instancí.
	 * 
	 * Veškeré položky / instance, které se najdou, tak se vloží do listu a až se
	 * objekt objValu "prohledá", tak se zjistí, zda obsahuje jen jednu položku,
	 * pokud ano, pak se rovnou otevře dialog s dotazem, zda se na tu vrácenou
	 * instanci má vytvořit reference a vložit se tak do diagramu instancí. Pokud se
	 * ale těchinstancí najde více, tj. instanceList bude větší jak jedna, pak se
	 * otevře dialog, kde si uživatel bude moci vybrat, na jaké instance se má
	 * vytvořit reference a na jeké ne.
	 * 
	 * @param objValue
	 *            - návratová hodnota metody o které se má zjistit, zda je to
	 *            instnace nějaké třídy z diagramu tříd a tato instance ještě není v
	 *            diagramu instancí.
	 * 
	 * @param instanceDiagram
	 *            - reference na diagram instancí.
	 * 
	 * @param languageProperties
	 *            - objekt s texty pro aplikaci ve zvoleném jazyce.
	 */
	public static void checkReturnedValue(final Object objValue, final GraphInstance instanceDiagram,
			final Properties languageProperties) {

		/*
		 * V případě, že se jedná o null hodnotu, pak nemá smysl pokračovat, žádná
		 * instance to není.
		 */
		if (objValue == null)
			return;
		
		
		/*
		 * Vytvořím si list, do kterého se vloží veškeré nalezené instance tříd z
		 * diagramu tříd, které se ještě nenachází v diagramu isntancí.
		 */
		final List<Object> instancesList = new ArrayList<>();	
		
		
		
		// Pokud je to pole, pak jej celé projdu a hodnoty vložím do instancesList:
		if (objValue.getClass().isArray())
			checkInstancesInArray(objValue, instancesList);
		
		
		
		
		// Pokud je to kolekce, tak ji projdu a hodnoty vložím do listu instancesList:
		else if (ReflectionHelper.isDataTypeOfList(objValue.getClass())) {
			/*
			 * Přetypuji si získaný list na list objektů:
			 */
			@SuppressWarnings("unchecked")
			final List<Object> valuesList = (List<Object>) objValue;

			for (final Object o : valuesList) {
				if (o == null)
					continue;

				/*
				 * Zjistím, zda je tato konkrétní položka v listu instance třídy z diagramu tříd
				 * a ještě není reprezentována v diagramu instancí, pokud ano, pak jej přidám do
				 * listu instancesList, protože na ni je možné vytvořit referenci.
				 */
				if (VariableInfo.checkObject(o))
					instancesList.add(o);
			}
		}
		
		
		
		
		
		else {
			/*
			 * Zde se jedná o "klasickou" proměnnou nebo nějakou z těch netestovaných
			 * (například mapa apod.), tak pouze zjistím zda se jedná o instanci nějaké tříy
			 * z diagramu tříd, která se ještě nenachází v diagramu instancí a pokud ano,
			 * přidám ji do listu pro potenciální vytvoření reference.
			 */
			if (VariableInfo.checkObject(objValue))
				instancesList.add(objValue);
		}
		
		
	
		
		// Pokud se žádná instance nenašla, nemá smysl pokračovat.
		if (instancesList.isEmpty())
			return;
		
		
		

		/*
		 * V případě, že byla nalezena pouze jedna instance třídy z diagramu třid, která
		 * se ještě nenachází v diagramu instancí, pak rovnou mohu zobrazit dialog s
		 * dotazem, zda se má vytvořit reference na příslušnou instanci.
		 */
		if (instancesList.size() == 1)
			VariableInfo.createNewReferenceForInstance(instancesList.get(0), instanceDiagram, languageProperties);

		
		
		else {
			/*
			 * Zde se našli alespoň dvě instance, pak zobrazím nejprve dialog, kde si bude
			 * uživatel moci vybrat, na jaké ze všech nalezených instancí chce vytvořit
			 * reference a na jaké ne. Toto je pro případě, že by list obsahovat například
			 * 10 instancí, tak aby na uživatele nevyskočil desetkrát ten samý dialog s
			 * dotazem na to, zda se má vytvořit reference na tu instanci apod.
			 */
			
			/*
			 * Vytvořím si dialog a předám mu dostupné instance, na které je možné vytvořit
			 * referenci.
			 */
			final SelectInstancesForCreateReference sifcr = new SelectInstancesForCreateReference(
					instancesList);
			// Nastavím texty:
			sifcr.setLanguage(languageProperties);

			
			/*
			 * Zde jej zobrazím a nechám si vrátit list označených instancí, na které se má
			 * vytvořit reference.
			 */
			final List<Object> selectedInstancesList = sifcr.getInstances();

			/*
			 * Pokud uživatel označil alespoň jednu instanci, tak projdu celý list s
			 * označenými instancemi a pro každou nabídnu uživatel vytvoření nové reference.
			 */
			if (!selectedInstancesList.isEmpty())
				selectedInstancesList.forEach(
						i -> VariableInfo.createNewReferenceForInstance(i, instanceDiagram, languageProperties));
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která prohledá celé X - rozměrné pole objArray a pro každou nalezenou
	 * položku zjistí, zda se jedná o instanci třídy z diagramu tříd, která se ještě
	 * nenachází v diagramu instancí, pokud takovou položku najde, pak ji přidá do
	 * listu instancesList, který bude obsahovat veškeré položky, resp. instance, ke
	 * kterým je možné vytvořit nové reference a zobrazit je tak v diagramu
	 * instancí.
	 * 
	 * @param objArray
	 *            - X - rozměrné pole, které se má prohledat a případě, pokud se
	 *            najde instance třídy z diagramu tříd, která ještě není v diagramu
	 *            instancí, tak ji přidat do listu instancesList.
	 * 
	 * @param instancesList
	 *            - list, do kterého se budou vkládat nalezené instance tříd z
	 *            diagramu tříd, které se ještě nenachází v diagramu instancí, proto
	 *            je možné k ním vytvořit reference.
	 * 
	 * @return instancesList, který již bude obsahovat získané instance, popřípadě
	 *         bude prázdný.
	 */
	private static List<Object> checkInstancesInArray(final Object objArray, final List<Object> instancesList) {
		/*
		 * Zjistím si velikost / délku pole objArray:
		 */
		final int length = Array.getLength(objArray);

		// Projdu celé pole:
		for (int i = 0; i < length; i++) {
			/*
			 * Získám si hodnotu z pole na právě iterovaném indexu.
			 */
			final Object objValue = Array.get(objArray, i);

			// Pokud je to null hodnota, pak nemá smysl pokračovat.
			if (objValue == null)
				continue;

			
			// Pokud je to další pole, pak pokračuji rekurzí:
			if (objValue.getClass().isArray())
				checkInstancesInArray(objValue, instancesList);

			
			/*
			 * Zde je to nějaká hodnota, která není null ani další pole / dimeze, tak
			 * zjistím, zda je to instance třídy z digramu tříd, která ještě není v diagramu
			 * instancí a pokud ano, pak ji přidám do listu instancesList.
			 */
			else if (VariableInfo.checkObject(objValue))
				instancesList.add(objValue);
		}

		return instancesList;
	}
}
