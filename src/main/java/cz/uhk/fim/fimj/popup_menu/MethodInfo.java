package cz.uhk.fim.fimj.popup_menu;

import java.lang.reflect.Method;
import java.util.List;

/**
 * Třída slouží pro uložení informací ohledně (zděděné / vnitřní) třídy a metod, které obsahuje. Tato třída se využívá
 * pro předání informací při hledání zděděných metod nad označenou třídou / instancí nebo statických metod ve vnitřních
 * třídách v označené třídě v diagramu tříd.
 * <p>
 * Tato třída obsahuje informace jako je List metod, které dědí označená třída / instance nebo se nachází v vnitřní
 * třídě, v jaké třídě se ty metody nachází a tooltip obsahují řetězec cest zděděných tříd.
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 07.07.2018 14:24
 */

public class MethodInfo {

    /**
     * zvolený separátor pro oddělování vnitřních tříd v tooltipu.
     */
    private static final String INNER_CLASSES_SEPARATOR = " \\$ ";

    /**
     * Třída, která je jeden z"předků" označené třídy / instance. V tomto předkovi se nachází metody (list methods),
     * které dědí označneí třída / instance v diagramu tříd nebo instancí.
     */
    private final Class<?> clazz;

    /**
     * List vybraných metod, které může označení třída / instance v diagramu tříd nebo instancí dědit. Tzn. List
     * veřejných nebo chráněných metod nacházející se v třídě ancestor.
     */
    private final List<Method> methods;

    /**
     * Text, který se vloží do tooltipu v kontextovém menu. Tento text obsahuje "cestu", resp. řetězec, který znázorňuje
     * ceou dědičnost od označené třídy / instance v diagramu tříd nebo instancí.
     */
    private final String tooltip;


    /**
     * Konstruktor této třídy.
     *
     * @param clazz
     *         - třída, která slouží jako předek označení třídy nebo instance, která obsahuje metody, které může
     *         označená třída nebo instance zavolat.
     * @param methods
     *         - List veřejných nebo chráněných metod, které obsahuje označená třída nebo instance v diagramu dědí.
     * @param tooltip
     *         - popsisek pro pložky v kontextovém menu obsahující řetězec zděděných tříd.
     */
    MethodInfo(final Class<?> clazz, final List<Method> methods, final String tooltip) {
        this.clazz = clazz;
        this.methods = methods;
        this.tooltip = tooltip;
    }


    /**
     * Tento konstruktor se využívá v případě, že se potřebují předat položky pro metody, které senachází ve vnitřních
     * třídách. V takovém případě se sestaví cesta k těm vnitřním třídám.
     *
     * @param clazz
     *         - nějaká vnitřní třída, která obsahuje metody (methods), které může uživatel nad označenou třídou v
     *         diagramu tříd zavolat.
     * @param methods
     *         - List veřejných nebo chráněných metod, které obsahuje označená třída - její vnitřní třída.
     */
    public MethodInfo(final Class<?> clazz, final List<Method> methods) {
        this.clazz = clazz;
        this.methods = methods;

        /*
         * Zde si sestavím tooltip, který bude v takové syntaxi, že jako první bude ta třída, která se nachází v
         * diagramu tříd. Ostatní třídy budou ty vnořené oddělené dolarem a mezerou kolem něj.
         *
         * Ze  začátku musím pouze odebrat balíčky, ve kterých / kterém se ta třída označená v diagramu tříd nachází.
         */
        final String temp = clazz.toString().substring(clazz.toString().lastIndexOf('.') + 1);
        tooltip = temp.replaceAll(INNER_CLASSES_SEPARATOR.replaceAll("\\s*", ""), INNER_CLASSES_SEPARATOR);
    }


    public Class<?> getClazz() {
        return clazz;
    }

    public List<Method> getMethods() {
        return methods;
    }

    public String getTooltip() {
        return tooltip;
    }
}
