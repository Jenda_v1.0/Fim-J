package cz.uhk.fim.fimj.print;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import cz.uhk.fim.fimj.logger.ExtractEnum;
import cz.uhk.fim.fimj.logger.ExceptionLogger;

/**
 * Tato třída slouží pro možnost vytisknutí zdrojového kódu třídy, který je otevřen v editoru zdrojového kódu, z tohoto
 * dialogu - konkrétně z jeho menu lze "tuto třídu zavolat"
 * <p>
 * Zdroj:
 * <p>
 * https://www.experts-exchange.com/questions/28580761/Print-Text-File-to-Specific-Windows-Printer-in-Java-with-wrap-around-and-multi-page.html
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class Printer {

	/**
	 * Konstruktor této třídy, který potřebuje jeden parametr, a sice cestu, k
	 * souboru, který se má vytisknout
	 * 
	 * @param pathToClass
	 *            - cesta k souboru, který se má vytisknout
	 */
	public Printer(final String pathToClass) {
		super();
		
		final List<String> fileContentsList = new ArrayList<>();

        // Get the latest file in folder, Open the file:
        try (final FileInputStream fStream = new FileInputStream(pathToClass)) {

            // Get the object of DataInputStream
            final DataInputStream in = new DataInputStream(fStream);
            final BufferedReader br = new BufferedReader(new InputStreamReader(in));

            // Read File Line By Line
            String strLine;

            while ((strLine = br.readLine()) != null)
                fileContentsList.add(strLine);

            in.close();

            br.close();

        } catch (FileNotFoundException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka pří získávání souboru na zadané cestě: " + pathToClass
                        + ", třída Printer, v konstruktoru. Tato výjimka může nastat například v případě, že uvedený " +
                        "soubor"
                        + " neexistuje, je to adresář a ne 'běžný' / potřebný soubor nebo z nějakého jiného důvodu " +
                        "jej nelzeotevřít pro čtení.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO,
                        "Zachycena výjimka při čtení - získávání obsahu souboru pro výtisk, soubor: " + pathToClass
                                + ", třída Printer, v konstruktoru.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }

        printToPrinter(fileContentsList);
	}
	
	
	
	
	
	// Print to printer method uses OutputPrinter class
	// it will print the contents of jtxtAreaDiagnostic text area
	public static void printToPrinter(final List<String> listOfFileContents) {
		// Calling a method for getting all diag info out of jlstDiagnostic
		// loaded contents:

		// Make a String out of the Arraylist<String>:
		String printData = "";

		for (final String listOfFileContent : listOfFileContents) {
			printData = printData + listOfFileContent;
			printData = printData + "\r\n";
		}

		// Feed the data to be printed to the PrinterJob instance:
		final PrinterJob job = PrinterJob.getPrinterJob();
		
		job.setPrintable(new OutputPrinter(printData));
		
		boolean doPrint = job.printDialog();
		
		if (doPrint) {
			try {
				job.print();
            } catch (PrinterException e) {
                /*
                 * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
                 * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
                 * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
                 * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
                 */
                final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

                // Otestuji, zda se podařilo načíst logger:
                if (logger != null) {
                    // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                    // Nejprve mou informaci o chybě:
                    logger.log(Level.INFO,
                            "Zachycena výjimka při pokusu o vytisknutí souboru, třída Printer, metodxa: " +
                                    "printToPrinter. Tato výjimka může nastat "
                                    + "například v případě, kdy chyba v tiskovém systému způsobí přerušení úlohy.");

                    /*
                     * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                     * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                     * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                     */
                    logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
                }
                /*
                 * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
                 */
                ExceptionLogger.closeFileHandler();
            }
		}
	}
}