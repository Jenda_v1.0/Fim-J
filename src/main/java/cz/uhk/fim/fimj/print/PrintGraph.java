package cz.uhk.fim.fimj.print;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.Printable;

import javax.swing.JFrame;
import javax.swing.RepaintManager;

import org.jgraph.JGraph;

/**
 * Tato třída slouží pro získání vlastní ohledne diagramu pro vytisknutí Lze pomocí této třídy vytisknout diagram tříd
 * nebo diagram instancí
 * <p>
 * Zdroj (str. 100): https://www.jgraph.com/downloads/jgraph/jgraphmanual.pdf
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class PrintGraph extends JFrame implements Printable {

	private static final long serialVersionUID = 1L;
	

	/**
	 * Proměnná, díky které nastavím "velikost okrajů" od poslední buňky v diagramu, 
	 * tatéž hodnota je použita i pro uložení obrázku daného grafu - resp. diagramu tříd či intancí
	 */
	private static final int PAGE_SCALE = 20;
	
	
	/**
	 * Reference na graph, jehož "PrintScreen" chce užitel vytsiknout
	 */
	private final JGraph graph;
	
	
	
	
	public PrintGraph(final JGraph graph) {
		super();
		
		this.graph = graph;
	}
	
	
	
	
	@Override
	public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) {

		final Dimension pSize = graph.getPreferredSize();

		final int w = (int) (pageFormat.getWidth() * PAGE_SCALE);
		final int h = (int) (pageFormat.getHeight() * PAGE_SCALE);

		final int cols = (int) Math.max(Math.ceil((double) (pSize.width - 5) / (double) w), 1);
		final int rows = (int) Math.max(Math.ceil((double) (pSize.height - 5) / (double) h), 1);

		if (pageIndex < cols * rows) {
			// Configures graph for printing
			final RepaintManager currentManager = RepaintManager.currentManager(this);
			currentManager.setDoubleBufferingEnabled(false);
			final double oldScale = graph.getScale();

			graph.setScale(1 / PAGE_SCALE);

			final int dx = (int) ((pageIndex % cols) * pageFormat.getWidth());
			final int dy = (int) ((pageIndex % rows) * pageFormat.getHeight());

			graphics.translate(-dx, -dy);
			graphics.setClip(dx, dy, (int) (dx + pageFormat.getWidth()), (int) (dy + pageFormat.getHeight()));
			// Prints the graph on the graphics.

			graph.paint(graphics);

			// Restores graph
			graphics.translate(dx, dy);
			graph.setScale(oldScale);
			currentManager.setDoubleBufferingEnabled(true);

			return PAGE_EXISTS;
		}

		return NO_SUCH_PAGE;
	}
}