package cz.uhk.fim.fimj.print;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import cz.uhk.fim.fimj.logger.ExtractEnum;

import cz.uhk.fim.fimj.logger.ExceptionLogger;

/**
 * Tato třída slouží v podstatě pro definici "desingnu" vytisknutého dokumentu se zdrojovým kódem, který je možné
 * vytisknout z editoru zdrojového kódu - z dialogu, pomocí položky v jeho menu "Tisk" (v češtině)
 * <p>
 * Zdroj:
 * <p>
 * https://www.experts-exchange.com/questions/28580761/Print-Text-File-to-Specific-Windows-Printer-in-Java-with-wrap
 * -around-and-multi-page.html
 *
 * @author Jan Krunčík
 * @version 1.0
 */

public class OutputPrinter implements Printable {

    private final String printData;


    public OutputPrinter(final String printData) {
        super();

        this.printData = printData;
    }


    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) {
        // Should only have one page, and page # is zero-based.
        if (pageIndex > 0) {
            return NO_SUCH_PAGE;
        }

        // Adding the "Imageable" to the x and y puts the margins on the page.
        // To make it safe for printing.
        final Graphics2D g2d = (Graphics2D) graphics;
        int x = (int) pageFormat.getImageableX();
        int y = (int) pageFormat.getImageableY();
        g2d.translate(x, y);

        // Calculate the line height
        final Font font = new Font("Serif", Font.PLAIN, 10);
        final FontMetrics metrics = graphics.getFontMetrics(font);
        final int lineHeight = metrics.getHeight();

        final BufferedReader br = new BufferedReader(new StringReader(printData));

        // Draw the page:
        try {
            String line;
            // Just a safety net in case no margin was added.
            x += 50;
            y += 50;

            while ((line = br.readLine()) != null) {
                y += lineHeight;
                g2d.drawString(line, x, y);
            }

        } catch (IOException e) {
            /*
             * Do proměnné Logger si vložím objekt Logger, který slouží pro zápis nastalých
             * chyb do souboru. Pak otestuji, zda se jej podařilo načíst (logger není null),
             * pak do něj zapíši potřebná data. Jako první informací ohledně výjimky, která
             * nastala a jako druhou informaci text samotné výjimy. Na konec Logger zavřu.
             */
            final Logger logger = ExceptionLogger.getLogger(ExtractEnum.EXTRACT_EXCEPTION);

            // Otestuji, zda se podařilo načíst logger:
            if (logger != null) {
                // Zde se podařilo načíst logger, tak mohu zapsat hlášení o chybě

                // Nejprve mou informaci o chybě:
                logger.log(Level.INFO, "Zachycena výjimka pří tisknutí souboru, třída OutputPrinter, metoda: print.");

                /*
                 * Mohl bych vypisovat pouze "Úvodní" informaci o nastalé výjimce pomocí
                 * e.getMessage, ale pak bych nevěděl kde přesně k chybe došlo, případně v jaké
                 * metodě apod. Proto vypisuji celou "informaci" o výjimce.
                 */
                logger.log(Level.WARNING, ExceptionLogger.getStackTrace(e));
            }
            /*
             * Zde je vhodné zavřít stream pro další úpravy, aby si jej aplikace "nedržela".
             */
            ExceptionLogger.closeFileHandler();
        }

        return PAGE_EXISTS;
    }
}