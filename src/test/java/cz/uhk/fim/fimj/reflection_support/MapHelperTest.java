package cz.uhk.fim.fimj.reflection_support;

import org.junit.Test;

import java.lang.reflect.*;
import java.util.*;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class MapHelperTest {

    @Test
    public void getKeyTypeOfMapTest() {
        /*
         * Test, zda se správně získají datové typy klíče mapy z parametrů metody.
         */

        final List<Method> methodList = new ArrayList<>();
        IntStream.range(0, FieldMethodConstant.COUNT_OF_TEST_MAP_METHODS + 1).forEach(i -> methodList.add(FieldMethodConstant.getMethodForTest("testMap_" + i, Map.class)));

        assertEquals(methodList.size(), FieldMethodConstant.COUNT_OF_TEST_MAP_METHODS + 1);

        int index = 0;


        final Parameter parameterType_0 = getParameterType(methodList.get(index));
        assertNotNull(parameterType_0);
        assertNull(MapHelper.getKeyTypeOfMap(parameterType_0));


        final Parameter parameterType_1 = getParameterType(methodList.get(++index));
        assertNotNull(parameterType_1);
        assertTrue(MapHelper.getKeyTypeOfMap(parameterType_1) instanceof WildcardType);

        final Parameter parameterType_2 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_2 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_2);
        assertTrue(MapHelper.getKeyTypeOfMap(parameterType_2) instanceof Class<?>);

        final Parameter parameterType_3 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_3 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_3);
        assertTrue(MapHelper.getKeyTypeOfMap(parameterType_3) instanceof ParameterizedType);

        final Parameter parameterType_4 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_4 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_4);
        assertTrue(MapHelper.getKeyTypeOfMap(parameterType_4) instanceof ParameterizedType);

        final Parameter parameterType_5 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_5 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_5);
        assertTrue(MapHelper.getKeyTypeOfMap(parameterType_5) instanceof ParameterizedType);

        final Parameter parameterType_6 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_6 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_6);
        assertTrue(MapHelper.getKeyTypeOfMap(parameterType_6) instanceof GenericArrayType);

        final Parameter parameterType_7 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_7 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_7);
        assertTrue(MapHelper.getKeyTypeOfMap(parameterType_7) instanceof ParameterizedType);

        final Parameter parameterType_8 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_8 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_8);
        assertTrue(MapHelper.getKeyTypeOfMap(parameterType_8) instanceof Class<?>);

        final Parameter parameterType_9 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_9 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_9);
        assertTrue(MapHelper.getKeyTypeOfMap(parameterType_9) instanceof ParameterizedType);

        final Parameter parameterType_10 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_10 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_10);
        assertTrue(MapHelper.getKeyTypeOfMap(parameterType_10) instanceof TypeVariable);

        final Parameter parameterType_11 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_11 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_11);
        assertTrue(MapHelper.getKeyTypeOfMap(parameterType_11) instanceof ParameterizedType);

        final Parameter parameterType_12 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_12 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_12);
        assertTrue(MapHelper.getKeyTypeOfMap(parameterType_12) instanceof TypeVariable);

        assertEquals(FieldMethodConstant.COUNT_OF_TEST_MAP_METHODS, index);
    }


    @Test
    public void getValueTypeOfMapTest() {
        /*
         * Test, zda se správně získají datové typy hodnoty mapy.
         */

        final List<Method> methodList = new ArrayList<>();
        IntStream.range(0, FieldMethodConstant.COUNT_OF_TEST_MAP_METHODS + 1).forEach(i -> methodList.add(FieldMethodConstant.getMethodForTest("testMap_" + i, Map.class)));

        assertEquals(methodList.size(), FieldMethodConstant.COUNT_OF_TEST_MAP_METHODS + 1);

        int index = 0;


        final Parameter parameterType_0 = getParameterType(methodList.get(index));
        assertNotNull(parameterType_0);
        assertNull(MapHelper.getValueTypeOfMap(parameterType_0));


        final Parameter parameterType_1 = getParameterType(methodList.get(++index));
        assertNotNull(parameterType_1);
        assertTrue(MapHelper.getValueTypeOfMap(parameterType_1) instanceof WildcardType);

        final Parameter parameterType_2 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_2 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_2);
        assertTrue(MapHelper.getValueTypeOfMap(parameterType_2) instanceof Class<?>);

        final Parameter parameterType_3 = getParameterType(methodList.get(++index));
        System.out.println("parametr: " + parameterType_3 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_3);
        assertTrue(MapHelper.getValueTypeOfMap(parameterType_3) instanceof ParameterizedType);

        final Parameter parameterType_4 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_4 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_4);
        assertTrue(MapHelper.getValueTypeOfMap(parameterType_4) instanceof ParameterizedType);

        final Parameter parameterType_5 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_5 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_5);
        assertTrue(MapHelper.getValueTypeOfMap(parameterType_5) instanceof GenericArrayType);

        final Parameter parameterType_6 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_6 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_6);
        assertTrue(MapHelper.getValueTypeOfMap(parameterType_6) instanceof GenericArrayType);

        final Parameter parameterType_7 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_7 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_7);
        assertTrue(MapHelper.getValueTypeOfMap(parameterType_7) instanceof ParameterizedType);

        final Parameter parameterType_8 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_8 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_8);
        assertTrue(MapHelper.getValueTypeOfMap(parameterType_8) instanceof ParameterizedType);

        final Parameter parameterType_9 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_9 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_9);
        assertTrue(MapHelper.getValueTypeOfMap(parameterType_9) instanceof ParameterizedType);

        final Parameter parameterType_10 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_10 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_10);
        assertTrue(MapHelper.getValueTypeOfMap(parameterType_10) instanceof GenericArrayType);

        final Parameter parameterType_11 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_11 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_11);
        assertTrue(MapHelper.getValueTypeOfMap(parameterType_11) instanceof ParameterizedType);

        final Parameter parameterType_12 = getParameterType(methodList.get(++index));
        System.out.println("Parameter: " + parameterType_12 + ", method: " + methodList.get(index));
        assertNotNull(parameterType_12);
        assertTrue(MapHelper.getValueTypeOfMap(parameterType_12) instanceof TypeVariable);

        assertEquals(FieldMethodConstant.COUNT_OF_TEST_MAP_METHODS, index);
    }


    @Test
    public void compareDataTypeTest_1() {
        /*
         * Test pro porovnávnání typů mapy (typy klíče a hodnoty mapy).
         */
        final Method testMap_0 = FieldMethodConstant.getMethodForTest("testMap_0", Map.class);
        assertNotNull(testMap_0);

        // Mapa bez parametrů:
        final Parameter parameterType_0_0 = getParameterType(testMap_0);
        final Parameter parameterType_0_1 = getParameterType(testMap_0);

        assertNotNull(parameterType_0_0);
        assertNotNull(parameterType_0_1);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(parameterType_0_0);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(parameterType_0_1);

        System.out.println("key: " + keyTypeOfMap);
        System.out.println("value: " + valueTypeOfMap);

        assertNull(keyTypeOfMap);
        assertNull(valueTypeOfMap);

        /*
         * (Je možné porovnat oba parametry mapy.)
         *
         * Mapa nemá Parametry. Pro null hodnoty se vrátí false.
         */
        assertFalse(ReflectionHelper.compareDataType(keyTypeOfMap, valueTypeOfMap));
    }


    @Test
    public void compareDataTypeTest_2() {
        /*
         * Test pro porovnávání typů mapy (typy klíče a hodnoty mapy).
         */
        final Method testMap_0 = FieldMethodConstant.getMethodForTest("testMap_1", Map.class);
        assertNotNull(testMap_0);

        // Mapa bez parametrů:
        final Parameter parameterType_0_0 = getParameterType(testMap_0);
        final Parameter parameterType_0_1 = getParameterType(testMap_0);
        assertNotNull(parameterType_0_0);
        assertNotNull(parameterType_0_1);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(parameterType_0_0);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(parameterType_0_1);

        System.out.println("key: " + keyTypeOfMap);
        System.out.println("value: " + valueTypeOfMap);

        assertNotNull(keyTypeOfMap);
        assertNotNull(valueTypeOfMap);

        assertTrue(keyTypeOfMap instanceof WildcardType);
        assertTrue(valueTypeOfMap instanceof WildcardType);

        /*
         * (Je možné porovnat oba parametry mapy.)
         *
         * Mělo by se vrátit true. Když jsou oba parametry null, tak druhý typ parametru mapu lze vložit do prvního.
         * První parametr je mapa bez parametrů nebo generická.
         */
        assertTrue(ReflectionHelper.compareDataType(keyTypeOfMap, valueTypeOfMap));
    }


    @Test
    public void compareDataTypeTest_3() {
        /*
         * Test pro porovnávání typů mapy (typy klíče a hodnoty mapy).
         */
        final Method testMap_0 = FieldMethodConstant.getMethodForTest("testMap_2", Map.class);
        assertNotNull(testMap_0);

        // Mapa bez parametrů:
        final Parameter parameterType_0_0 = getParameterType(testMap_0);
        final Parameter parameterType_0_1 = getParameterType(testMap_0);
        assertNotNull(parameterType_0_0);
        assertNotNull(parameterType_0_1);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(parameterType_0_0);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(parameterType_0_1);

        System.out.println("key: " + keyTypeOfMap);
        System.out.println("value: " + valueTypeOfMap);

        assertNotNull(keyTypeOfMap);
        assertNotNull(valueTypeOfMap);

        /*
         * (Je možné porovnat oba parametry mapy.)
         *
         * Mělo by být true, do objektu lze vložit String:
         */
        assertTrue(ReflectionHelper.compareDataType(keyTypeOfMap, valueTypeOfMap));
    }


    @Test
    public void compareDataTypeTest_4() {
        /*
         * Test pro porovnávání typů mapy (typy klíče a hodnoty mapy).
         */
        final Method testMap_0 = FieldMethodConstant.getMethodForTest("testMap_3", Map.class);
        assertNotNull(testMap_0);

        // Mapa bez parametrů:
        final Parameter parameterType_0_0 = getParameterType(testMap_0);
        final Parameter parameterType_0_1 = getParameterType(testMap_0);
        assertNotNull(parameterType_0_0);
        assertNotNull(parameterType_0_1);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(parameterType_0_0);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(parameterType_0_1);

        System.out.println("key: " + keyTypeOfMap);
        System.out.println("value: " + valueTypeOfMap);

        assertNotNull(keyTypeOfMap);
        assertNotNull(valueTypeOfMap);

        /*
         * (Je možné porovnat oba parametry mapy.)
         *
         * Mělo by být false - parametry se liší.
         *
         * (není doplněna podpora pro testování i typů těch typů mapy.)
         */
        assertFalse(ReflectionHelper.compareDataType(keyTypeOfMap, valueTypeOfMap));
    }


    @Test
    public void compareDataTypeTest_5() {
        /*
         * Test pro porovnávání typů mapy (typy klíče a hodnoty mapy).
         */
        final Method testMap_0 = FieldMethodConstant.getMethodForTest("testMap_4", Map.class);
        assertNotNull(testMap_0);

        // Mapa bez parametrů:
        final Parameter parameterType_0_0 = getParameterType(testMap_0);
        final Parameter parameterType_0_1 = getParameterType(testMap_0);
        assertNotNull(parameterType_0_0);
        assertNotNull(parameterType_0_1);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(parameterType_0_0);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(parameterType_0_1);

        System.out.println("key: " + keyTypeOfMap);
        System.out.println("value: " + valueTypeOfMap);

        assertNotNull(keyTypeOfMap);
        assertNotNull(valueTypeOfMap);

        /*
         * (Je možné porovnat oba parametry mapy.)
         *
         * Mělo by být false - parametry se liší:
         */
        assertFalse(ReflectionHelper.compareDataType(keyTypeOfMap, valueTypeOfMap));
    }


    @Test
    public void compareDataTypeTest_6() {
        /*
         * Test pro porovnávání typů mapy (typy klíče a hodnoty mapy).
         */
        final Method testMap_0 = FieldMethodConstant.getMethodForTest("testMap_5", Map.class);
        assertNotNull(testMap_0);

        // Mapa bez parametrů:
        final Parameter parameterType_0_0 = getParameterType(testMap_0);
        final Parameter parameterType_0_1 = getParameterType(testMap_0);
        assertNotNull(parameterType_0_0);
        assertNotNull(parameterType_0_1);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(parameterType_0_0);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(parameterType_0_1);

        System.out.println("key: " + keyTypeOfMap);
        System.out.println("value: " + valueTypeOfMap);

        assertNotNull(keyTypeOfMap);
        assertNotNull(valueTypeOfMap);

        /*
         * (Je možné porovnat oba parametry mapy.)
         *
         * Mělo by být false - parametry se liší:
         */
        assertFalse(ReflectionHelper.compareDataType(keyTypeOfMap, valueTypeOfMap));
    }


    @Test
    public void compareDataTypeTest_7() {
        /*
         * Test pro porovnávání typů mapy (typy klíče a hodnoty mapy).
         */
        final Method testMap_0 = FieldMethodConstant.getMethodForTest("testMap_6", Map.class);
        assertNotNull(testMap_0);

        // Mapa bez parametrů:
        final Parameter parameterType_0_0 = getParameterType(testMap_0);
        final Parameter parameterType_0_1 = getParameterType(testMap_0);
        assertNotNull(parameterType_0_0);
        assertNotNull(parameterType_0_1);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(parameterType_0_0);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(parameterType_0_1);

        System.out.println("key: " + keyTypeOfMap);
        System.out.println("value: " + valueTypeOfMap);

        assertNotNull(keyTypeOfMap);
        assertNotNull(valueTypeOfMap);

        /*
         * (Je možné porovnat oba parametry mapy.)
         *
         * Mělo by být false - typy se liší:
         */
        assertFalse(ReflectionHelper.compareDataType(keyTypeOfMap, valueTypeOfMap));
    }


    @Test
    public void compareDataTypeTest_8() {
        /*
         * Test pro porovnávání typů mapy (typy klíče a hodnoty mapy).
         */
        final Method testMap_0 = FieldMethodConstant.getMethodForTest("testMap_9", Map.class);
        assertNotNull(testMap_0);

        // Mapa bez parametrů:
        final Parameter parameterType_0_0 = getParameterType(testMap_0);
        final Parameter parameterType_0_1 = getParameterType(testMap_0);
        assertNotNull(parameterType_0_0);
        assertNotNull(parameterType_0_1);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(parameterType_0_0);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(parameterType_0_1);

        System.out.println("key: " + keyTypeOfMap);
        System.out.println("value: " + valueTypeOfMap);

        assertNotNull(keyTypeOfMap);
        assertNotNull(valueTypeOfMap);

        /*
         * (Je možné porovnat oba parametry mapy.)
         *
         * Mělo by být true - jsou stejné
         */
        assertTrue(ReflectionHelper.compareDataType(keyTypeOfMap, valueTypeOfMap));
    }


    @Test
    public void compareDataTypeTest_12() {
        /*
         * Test pro porovnávání typů mapy (typy klíče a hodnoty mapy).
         */
        final Method testMap = FieldMethodConstant.getMethodForTest("testMap_12", Map.class);
        assertNotNull(testMap);

        final Parameter parameterType_0 = getParameterType(testMap);
        final Parameter parameterType_1 = getParameterType(testMap);
        assertNotNull(parameterType_0);
        assertNotNull(parameterType_1);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(parameterType_0);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(parameterType_1);

        System.out.println("key: " + keyTypeOfMap);
        System.out.println("value: " + valueTypeOfMap);

        assertNotNull(keyTypeOfMap);
        assertNotNull(valueTypeOfMap);

        /*
         * (Je možné porovnat oba parametry mapy.)
         *
         * Mělo by být true - jsou stejné
         */
        assertTrue(ReflectionHelper.compareDataType(keyTypeOfMap, valueTypeOfMap));
    }


    /**
     * Získání typu parametru metody method.
     *
     * @param method
     *         - metoda, jejíž první parametr se má získat. Metoda musí obsahovat pouze jeden parametr.
     *
     * @return parametr metody method.
     */
    private static Parameter getParameterType(final Method method) {
        final Parameter[] parameters = method.getParameters();
        assertNotNull(parameters);
        assertEquals(1, parameters.length);

        return parameters[0];
    }


    @Test
    public void getKeyAndValueOfMapForNonMapParameter() {
        /*
         * Účelem testů je zjistit, zda nedojde k chybě při předání parametru do metody pro zjištění typu klíče a
         * hodnoty takovou hodnotu, která není mapa.
         */

        // testová metoda pro získání parametrů:
        final Method declaredMethodTest = FieldMethodConstant.getMethodForTest("testMethod_1",
                FieldMethodConstant.TEST_METHOD_1_PARAMETERS);
        assertNotNull(declaredMethodTest);

        final Parameter[] parameters = declaredMethodTest.getParameters();
        assertNotNull(parameters);
        assertEquals(parameters.length, FieldMethodConstant.TEST_METHOD_1_PARAMETERS.length);

        Arrays.stream(parameters).forEach(parameter -> {
            final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(parameter);
            final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(parameter);

            System.out.println("parameter: " + parameter);
            System.out.println("key: " + keyTypeOfMap);
            System.out.println("value: " + valueTypeOfMap);
            System.out.println("\n");
        });
    }


    @Test
    public void getKeyAndValueOfMapForNullValue() {
        /*
         * Účelem je zjistit, aby test nespadl při předání null hodnoty jako parametru pro zjištění typu klíče a
         * hodnoty mapy.
         */

        final Parameter parameter = null;
        final Field field = null;
        final Method method = null;

        final Object keyTypeOfMapPar = MapHelper.getKeyTypeOfMap(parameter);
        final Object valueTypeOfMapPar = MapHelper.getValueTypeOfMap(parameter);

        final Object keyTypeOfMapFi = MapHelper.getKeyTypeOfMap(field);
        final Object valueTypeOfMapFi = MapHelper.getValueTypeOfMap(field);

        final Object keyTypeOfMapMe = MapHelper.getKeyTypeOfMap(method);
        final Object valueTypeOfMapMe = MapHelper.getValueTypeOfMap(method);

        assertNull(keyTypeOfMapPar);
        assertNull(valueTypeOfMapPar);

        assertNull(keyTypeOfMapFi);
        assertNull(valueTypeOfMapFi);

        assertNull(keyTypeOfMapMe);
        assertNull(valueTypeOfMapMe);
    }


    @Test
    public void compareDataTypeInFieldTest_0() {
        /*
         * Test pro porovnávání typů mapy (typy klíče a hodnoty mapy).
         */
        final Field fieldForTest = FieldMethodConstant.getFieldForTest("testMapField_0");
        assertNotNull(fieldForTest);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(fieldForTest);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(fieldForTest);

        assertNull(keyTypeOfMap);
        assertNull(valueTypeOfMap);
    }


    @Test
    public void compareDataTypeInFieldTest_1() {
        /*
         * Test pro porovnávání typů mapy (typy klíče a hodnoty mapy).
         */
        final Field fieldForTest = FieldMethodConstant.getFieldForTest("testMapField_1");
        assertNotNull(fieldForTest);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(fieldForTest);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(fieldForTest);

        assertNotNull(keyTypeOfMap);
        assertNotNull(valueTypeOfMap);

        assertTrue(keyTypeOfMap instanceof WildcardType);
        assertTrue(valueTypeOfMap instanceof WildcardType);

        assertTrue(ReflectionHelper.compareDataType(keyTypeOfMap, valueTypeOfMap));
    }


    @Test
    public void compareDataTypeInFieldTest_2() {
        /*
         * Test pro porovnávání typů mapy (typy klíče a hodnoty mapy).
         */
        final Field fieldForTest = FieldMethodConstant.getFieldForTest("testMapField_2");
        assertNotNull(fieldForTest);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(fieldForTest);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(fieldForTest);

        assertNotNull(keyTypeOfMap);
        assertNotNull(valueTypeOfMap);

        System.out.println("Field: testMapField_2");
        System.out.println("key: " + keyTypeOfMap);
        System.out.println("value: " + valueTypeOfMap);

        // String a Object se liší:
        assertFalse(ReflectionHelper.compareDataType(keyTypeOfMap, valueTypeOfMap));
    }


    @Test
    public void compareDataTypeInFieldTest_3() {
        /*
         * Test pro porovnávání typů mapy (typy klíče a hodnoty mapy).
         */
        final Field fieldForTest = FieldMethodConstant.getFieldForTest("testMapField_3");
        assertNotNull(fieldForTest);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(fieldForTest);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(fieldForTest);

        assertNotNull(keyTypeOfMap);
        assertNotNull(valueTypeOfMap);

        System.out.println("Field: testMapField_3");
        System.out.println("key: " + keyTypeOfMap);
        System.out.println("value: " + valueTypeOfMap);

        assertTrue(ReflectionHelper.compareDataType(keyTypeOfMap, valueTypeOfMap));
    }


    @Test
    public void compareDataTypeInFieldTest_4() {
        /*
         * Test pro porovnávání typů mapy (typy klíče a hodnoty mapy).
         */
        final Field fieldForTest = FieldMethodConstant.getFieldForTest("testMapField_4");
        assertNotNull(fieldForTest);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(fieldForTest);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(fieldForTest);

        assertNotNull(keyTypeOfMap);
        assertNotNull(valueTypeOfMap);

        System.out.println("Field: testMapField_4");
        System.out.println("key: " + keyTypeOfMap);
        System.out.println("value: " + valueTypeOfMap);

        assertFalse(ReflectionHelper.compareDataType(keyTypeOfMap, valueTypeOfMap));
    }


    @Test
    public void compareDataTypeInFieldTest_5() {
        /*
         * Test pro porovnávání typů mapy (typy klíče a hodnoty mapy).
         */
        final Field fieldForTest = FieldMethodConstant.getFieldForTest("testMapField_5");
        assertNotNull(fieldForTest);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(fieldForTest);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(fieldForTest);

        assertNotNull(keyTypeOfMap);
        assertNotNull(valueTypeOfMap);

        System.out.println("Field: testMapField_5");
        System.out.println("key: " + keyTypeOfMap);
        System.out.println("value: " + valueTypeOfMap);

        assertFalse(ReflectionHelper.compareDataType(keyTypeOfMap, valueTypeOfMap));
    }


    @Test
    public void compareDataTypeInFieldTest_6() {
        /*
         * Test pro porovnávání typů mapy (typy klíče a hodnoty mapy).
         */
        final Field fieldForTest = FieldMethodConstant.getFieldForTest("testMapField_6");
        assertNotNull(fieldForTest);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(fieldForTest);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(fieldForTest);

        assertNotNull(keyTypeOfMap);
        assertNotNull(valueTypeOfMap);

        System.out.println("Field: testMapField_6");
        System.out.println("key: " + keyTypeOfMap);
        System.out.println("value: " + valueTypeOfMap);

        assertTrue(ReflectionHelper.compareDataType(keyTypeOfMap, valueTypeOfMap));
    }


    @Test
    public void compareDataTypeInFieldTest_7() {
        /*
         * Test pro porovnávání typů mapy (typy klíče a hodnoty mapy).
         */
        final Field fieldForTest = FieldMethodConstant.getFieldForTest("testMapField_7");
        assertNotNull(fieldForTest);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(fieldForTest);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(fieldForTest);

        assertNotNull(keyTypeOfMap);
        assertNotNull(valueTypeOfMap);

        System.out.println("Field: testMapField_7");
        System.out.println("key: " + keyTypeOfMap);
        System.out.println("value: " + valueTypeOfMap);

        assertTrue(ReflectionHelper.compareDataType(keyTypeOfMap, valueTypeOfMap));
    }


    @Test
    public void compareDataTypeInFieldTest_8() {
        /*
         * Test pro porovnávání typů mapy (typy klíče a hodnoty mapy).
         */
        final Field fieldForTest = FieldMethodConstant.getFieldForTest("testMapField_8");
        assertNotNull(fieldForTest);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(fieldForTest);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(fieldForTest);

        assertNotNull(keyTypeOfMap);
        assertNotNull(valueTypeOfMap);

        System.out.println("Field: testMapField_8");
        System.out.println("key: " + keyTypeOfMap);
        System.out.println("value: " + valueTypeOfMap);

        assertFalse(ReflectionHelper.compareDataType(keyTypeOfMap, valueTypeOfMap));
    }


    @Test
    public void compareDataTypeInFieldTest_12() {
        /*
         * Test pro porovnávání typů mapy (typy klíče a hodnoty mapy).
         */
        final Field fieldForTest = FieldMethodConstant.getFieldForTest("testMapField_12");
        assertNotNull(fieldForTest);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(fieldForTest);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(fieldForTest);

        assertNotNull(keyTypeOfMap);
        assertNotNull(valueTypeOfMap);

        System.out.println("Field: testMapField_8");
        System.out.println("key: " + keyTypeOfMap);
        System.out.println("value: " + valueTypeOfMap);

        assertFalse(ReflectionHelper.compareDataType(keyTypeOfMap, valueTypeOfMap));
    }


    @Test
    public void compareNotMapDataTypeInFieldTest() {
        /*
         * Test pro porovnávání typů mapy (typy klíče a hodnoty mapy).
         */
        final Field fieldForTest = FieldMethodConstant.getFieldForTest("testField_0");
        assertNotNull(fieldForTest);

        final Object keyTypeOfMap = MapHelper.getKeyTypeOfMap(fieldForTest);
        final Object valueTypeOfMap = MapHelper.getValueTypeOfMap(fieldForTest);

        assertNull(keyTypeOfMap);
        assertNull(valueTypeOfMap);
    }


    @Test
    public void isFieldTypeOfMapTest_1() {
        final Field fieldForTest = FieldMethodConstant.getFieldForTest("testMapField_8");
        assertNotNull(fieldForTest);

        assertTrue(MapHelper.isFieldTypeOfMap(fieldForTest));
    }

    @Test
    public void isFieldTypeOfMapTest_2() {
        final Field fieldForTest = FieldMethodConstant.getFieldForTest("testMapField_9");
        assertNotNull(fieldForTest);

        assertTrue(MapHelper.isFieldTypeOfMap(fieldForTest));
    }

    @Test
    public void isFieldTypeOfMapTest_3() {
        final Field fieldForTest = FieldMethodConstant.getFieldForTest("testMapField_10");
        assertNotNull(fieldForTest);

        assertTrue(MapHelper.isFieldTypeOfMap(fieldForTest));
    }

    @Test
    public void isFieldTypeOfMapTest_4() {
        final Field fieldForTest = FieldMethodConstant.getFieldForTest("testField_0");
        assertNotNull(fieldForTest);

        assertFalse(MapHelper.isFieldTypeOfMap(fieldForTest));
    }

    @Test
    public void isFieldTypeOfMapTest_5() {
        final Field fieldForTest = FieldMethodConstant.getFieldForTest("testMapField_11");
        assertNotNull(fieldForTest);

        assertTrue(MapHelper.isFieldTypeOfMap(fieldForTest));
    }

    @Test
    public void isParameterTypeOfMapTest() {
        final Method declaredMethodTest = FieldMethodConstant.getMethodForTest("testMethod_1",
                FieldMethodConstant.TEST_METHOD_1_PARAMETERS);
        assertNotNull(declaredMethodTest);

        final Parameter[] parameters = declaredMethodTest.getParameters();
        assertNotNull(parameters);
        assertEquals(parameters.length, FieldMethodConstant.TEST_METHOD_1_PARAMETERS.length);

        Arrays.stream(parameters).forEach(parameter -> {
            final boolean parameterTypeOfMap = MapHelper.isParameterTypeOfMap(parameter);

            if (ReflectionHelper.isDataTypeOfMap(parameter.getType()))
                assertTrue(parameterTypeOfMap);
            else assertFalse(parameterTypeOfMap);

            System.out.println("Parameter: " + parameter + " is map: " + parameterTypeOfMap);
        });
    }


    @Test
    public void methodReturnsMapTest() {
        final List<Method> methodList = new ArrayList<>();

        // počet metod musí být o jednu větší (kvůli cyklu):
        IntStream.range(0, FieldMethodConstant.COUNT_OF_TEST_RETURN_TYPE_METHOD + 1).forEach(i -> {
            final Method method = FieldMethodConstant.getMethodForTest("testReturnTypeMethod_" + i);
            methodList.add(method);
        });

        methodList.forEach(method -> {
            final boolean returnsMap = MapHelper.methodReturnsMap(method);

            if (ReflectionHelper.isDataTypeOfMap(method.getReturnType()))
                assertTrue(returnsMap);
            else assertFalse(returnsMap);

            System.out.println(method.getReturnType() + " is Map: " + returnsMap);
        });
    }


    @Test
    public void getKeyTypeOfMapInMethodReturnTypeTest() {
        final List<Method> methodList = new ArrayList<>();

        // počet metod musí být o jednu větší (kvůli cyklu):
        IntStream.range(0, FieldMethodConstant.COUNT_OF_TEST_RETURN_TYPE_METHOD + 1).forEach(i -> methodList.add(FieldMethodConstant.getMethodForTest("testReturnTypeMethod_" + i)));
        assertEquals(FieldMethodConstant.COUNT_OF_TEST_RETURN_TYPE_METHOD + 1, methodList.size());

        int index = 0;


        final Object keyTypeOfMap_0 = MapHelper.getKeyTypeOfMap(methodList.get(index));
        System.out.println("Return type: " + keyTypeOfMap_0 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_0);

        final Object keyTypeOfMap_1 = MapHelper.getKeyTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_1 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_1);

        final Object keyTypeOfMap_2 = MapHelper.getKeyTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_2 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNotNull(keyTypeOfMap_2);
        assertTrue(keyTypeOfMap_2 instanceof WildcardType);

        final Object keyTypeOfMap_3 = MapHelper.getKeyTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_3 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNotNull(keyTypeOfMap_3);
        assertTrue(keyTypeOfMap_3 instanceof GenericArrayType);

        final Object keyTypeOfMap_4 = MapHelper.getKeyTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_4 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_4);

        final Object keyTypeOfMap_5 = MapHelper.getKeyTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_5 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_5);

        final Object keyTypeOfMap_6 = MapHelper.getKeyTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_6 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_6);

        final Object keyTypeOfMap_7 = MapHelper.getKeyTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_7 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_7);

        final Object keyTypeOfMap_8 = MapHelper.getKeyTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_8 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_8);

        final Object keyTypeOfMap_9 = MapHelper.getKeyTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_9 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_9);

        final Object keyTypeOfMap_10 = MapHelper.getKeyTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_10 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_10);

        final Object keyTypeOfMap_11 = MapHelper.getKeyTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_11 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_11);

        final Object keyTypeOfMap_12 = MapHelper.getKeyTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_12 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_12);

        final Object keyTypeOfMap_13 = MapHelper.getKeyTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_13 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_13);

        final Object keyTypeOfMap_14 = MapHelper.getKeyTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_14 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_14);

        final Object keyTypeOfMap_15 = MapHelper.getKeyTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_15 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNotNull(keyTypeOfMap_15);
        assertTrue(keyTypeOfMap_15 instanceof WildcardType);

        final Object keyTypeOfMap_16 = MapHelper.getKeyTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_16 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNotNull(keyTypeOfMap_16);
        assertTrue(keyTypeOfMap_16 instanceof Class<?>);

        final Object keyTypeOfMap_17 = MapHelper.getKeyTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_17 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNotNull(keyTypeOfMap_17);
        assertTrue(keyTypeOfMap_17 instanceof ParameterizedType);

        final Object keyTypeOfMap_18 = MapHelper.getKeyTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_18 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNotNull(keyTypeOfMap_18);
        assertTrue(keyTypeOfMap_18 instanceof Class<?>);

        final Object keyTypeOfMap_19 = MapHelper.getKeyTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_19 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_19);

        assertEquals(FieldMethodConstant.COUNT_OF_TEST_RETURN_TYPE_METHOD, index);
    }


    @Test
    public void getValueTypeOfMapInMethodReturnTypeTest() {
        final List<Method> methodList = new ArrayList<>();

        // počet metod musí být o jednu větší (kvůli cyklu):
        IntStream.range(0, FieldMethodConstant.COUNT_OF_TEST_RETURN_TYPE_METHOD + 1).forEach(i -> methodList.add(FieldMethodConstant.getMethodForTest("testReturnTypeMethod_" + i)));
        assertEquals(FieldMethodConstant.COUNT_OF_TEST_RETURN_TYPE_METHOD + 1, methodList.size());

        int index = 0;


        final Object keyTypeOfMap_0 = MapHelper.getValueTypeOfMap(methodList.get(index));
        System.out.println("Return type: " + keyTypeOfMap_0 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_0);

        final Object keyTypeOfMap_1 = MapHelper.getValueTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_1 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_1);

        final Object keyTypeOfMap_2 = MapHelper.getValueTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_2 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNotNull(keyTypeOfMap_2);
        assertTrue(keyTypeOfMap_2 instanceof WildcardType);

        final Object keyTypeOfMap_3 = MapHelper.getValueTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_3 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNotNull(keyTypeOfMap_3);
        assertTrue(keyTypeOfMap_3 instanceof GenericArrayType);

        final Object keyTypeOfMap_4 = MapHelper.getValueTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_4 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_4);

        final Object keyTypeOfMap_5 = MapHelper.getValueTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_5 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_5);

        final Object keyTypeOfMap_6 = MapHelper.getValueTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_6 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_6);

        final Object keyTypeOfMap_7 = MapHelper.getValueTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_7 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_7);

        final Object keyTypeOfMap_8 = MapHelper.getValueTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_8 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_8);

        final Object keyTypeOfMap_9 = MapHelper.getValueTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_9 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_9);

        final Object keyTypeOfMap_10 = MapHelper.getValueTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_10 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_10);

        final Object keyTypeOfMap_11 = MapHelper.getValueTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_11 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_11);

        final Object keyTypeOfMap_12 = MapHelper.getValueTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_12 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_12);

        final Object keyTypeOfMap_13 = MapHelper.getValueTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_13 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_13);

        final Object keyTypeOfMap_14 = MapHelper.getValueTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_14 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_14);

        final Object keyTypeOfMap_15 = MapHelper.getValueTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_15 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNotNull(keyTypeOfMap_15);
        assertTrue(keyTypeOfMap_15 instanceof Class<?>);

        final Object keyTypeOfMap_16 = MapHelper.getValueTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_16 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNotNull(keyTypeOfMap_16);
        assertTrue(keyTypeOfMap_16 instanceof Class<?>);

        final Object keyTypeOfMap_17 = MapHelper.getValueTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_17 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNotNull(keyTypeOfMap_17);
        assertTrue(keyTypeOfMap_17 instanceof ParameterizedType);

        final Object keyTypeOfMap_18 = MapHelper.getValueTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_18 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNotNull(keyTypeOfMap_18);
        assertTrue(keyTypeOfMap_18 instanceof Class<?>);

        final Object keyTypeOfMap_19 = MapHelper.getValueTypeOfMap(methodList.get(++index));
        System.out.println("Return type: " + keyTypeOfMap_19 + ", method: " + methodList.get(index).getGenericReturnType());
        assertNull(keyTypeOfMap_19);

        assertEquals(FieldMethodConstant.COUNT_OF_TEST_RETURN_TYPE_METHOD, index);
    }
}