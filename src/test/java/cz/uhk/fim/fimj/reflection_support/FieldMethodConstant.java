package cz.uhk.fim.fimj.reflection_support;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

import static org.junit.Assert.assertNotNull;

/**
 * Třída obsahuje pouze metody a proměnné, které se využívají v testech.
 *
 * <i>Třída byla napsána pouze proto, že se stejné metody a proměnné využívají pro testování ve více testech.</i>
 *
 * @author Jan Krunčík
 * @version 2.0
 * @since 03.09.2018 22:51
 */

class FieldMethodConstant<T> {

    static final int COUNT_OF_TEST_FIELDS = 17;
    private int testField_0;
    private Integer testField_1;
    private String testField_2;
    private int[][][] testField_3;
    private Double[][][][] testField_4;
    private List<ArrayList<Float>[]>[] testField_5;
    private List testField_6;
    private List<?> testField_7;
    private List<String> testField_8;
    private List<ArrayList<Float>[]> testField_9;
    private Map testField_10;
    private Map<?, ?> testField_11;
    private Map<ArrayList<?>, List<Object>> testField_12;
    private Map<ArrayList<?>[], LinkedList<Object>[][][]> testField_13;
    private T testField_14;
    private T[] testField_15;
    private List<T> testField_16;
    private Map<T, T[]> testField_17;


    /**
     * Parametry, které obsahuje metoda testArray_1, aby bylo možné ji načíst pro testování:
     */
    final Class<?>[] TEST_ARRAY_METHOD_1_PARAMETERS = {Map[].class, boolean[].class, Integer[][][].class,
            List[][].class, List[].class, List[].class, Map[].class, Map[][][].class, Object.class, Object[].class};

    // Metody, ze kterých se berou parametry pro testy:
    private void testArray_1(Map<List<Object>[][][], ?>[] m, boolean[] booleans, Integer[][][] integers,
                             List[][] list, List<String>[] stringList, List<List<?>>[] listList, Map[] maps,
                             Map<String, List<ArrayList<String>>>[][][] stringListMap, T t, T[] tt) {}


    /**
     * Parametry, které obsahuje metoda testMethod_1, aby bylo možné ji načíst pro testování.
     */
    static final Class<?>[] TEST_METHOD_1_PARAMETERS = {Map.class, Map.class, int.class, Double.class,
            Integer[][][].class, List.class, List.class, List.class, List[].class, Map[].class, Map[][][].class,
            Map.class, HashMap.class, LinkedHashMap.class};

    private void testMethod_1(Map<List<Object>[][][], ?> mmmm, Map<List<Object>[], ?> mmm, int i, Double d,
                              Integer[][][] integers, List l, List<Object> list, List<Map<List<?>,
            ArrayList<List<Float>>>> lm, List<List<?>>[] listList, Map[] maps,
                              Map<String, List<ArrayList<String>>>[][][] stringListMap, Map<?, ?> mm, HashMap hashMap
            , LinkedHashMap<?, List<HashMap<?, ?>>> listLinkedHashMap) {}


    /**
     * Počet metod pro testování návratového typu metody.
     */
    static final int COUNT_OF_TEST_RETURN_TYPE_METHOD = 19;

    private static Integer testReturnTypeMethod_0() {return null;}

    private static Map testReturnTypeMethod_1() {return null;}

    private static Map<?, ?> testReturnTypeMethod_2() {return null;}

    private static Map<List<Object>[], List<Object>[]> testReturnTypeMethod_3() {return null;}

    private static List testReturnTypeMethod_4() {return null;}

    private static List<?> testReturnTypeMethod_5() {return null;}

    private static List<Object> testReturnTypeMethod_6() {return null;}

    private static List<Map<ArrayList<Double>, ?>> testReturnTypeMethod_7() {return null;}

    private static List<Map<ArrayList<Double>[], ?>[]> testReturnTypeMethod_8() {return null;}

    private static int[] testReturnTypeMethod_9() {return null;}

    private static int[][][] testReturnTypeMethod_10() {return null;}

    private static List<Map<ArrayList<Double>[], ?>[]>[][][][] testReturnTypeMethod_11() {return null;}

    private static Map<ArrayList<Double>[], ?>[][][][] testReturnTypeMethod_12() {return null;}

    private void testReturnTypeMethod_13() {}

    private static LinkedHashMap testReturnTypeMethod_14() {return null;}

    private static HashMap<?, Double> testReturnTypeMethod_15() {return null;}

    private static HashMap<String, Double> testReturnTypeMethod_16() {return null;}

    private static HashMap<List<Float>, ArrayList<Double>> testReturnTypeMethod_17() {return null;}

    private static HashMap<String[], int[][]> testReturnTypeMethod_18() {return null;}

    private T testReturnTypeMethod_19() {return null;}


    /**
     * Počet metod pro testování typu Map.
     */
    static final int COUNT_OF_TEST_MAP_METHODS = 12;

    private void testMap_0(final Map map) {}

    private void testMap_1(final Map<?, ?> map) {}

    private void testMap_2(final Map<Object, String> map) {}

    private void testMap_3(final Map<List<String>, ArrayList<?>> map) {}

    private void testMap_4(final Map<List<?>, ArrayList<?>> map) {}

    private void testMap_5(final Map<LinkedList<ArrayList<String>>, HashSet<?>[]> map) {}

    private void testMap_6(final Map<LinkedList<ArrayList<String>>[][][], HashSet<?>[][][]> map) {}

    private void testMap_7(final Map<Map<ArrayList<Double>, LinkedList<?>>, Map<?, TreeSet<?>>> map) {}

    private void testMap_8(final Map<List, Map<?, TreeSet<?>>> map) {}

    private void testMap_9(final Map<Map<?, TreeSet<?>>, Map<?, TreeSet<?>>> map) {}

    private void testMap_10(final Map<T, T[]> map) {}

    private void testMap_11(final Map<Map<T, TreeSet<T>>, Map<?, T[][]>> map) {}

    private void testMap_12(final Map<T, T> map) {}

    private Map testMapField_0;
    private Map<?, ?> testMapField_1;
    private Map<String, Object> testMapField_2;
    private Map<Float, Float> testMapField_3;
    private Map<List<String>, ArrayList<?>> testMapField_4;
    private Map<List<Object>[][], HashSet<?>[][][]> testMapField_5;
    private Map<HashSet<?>[][][], HashSet<?>[][][]> testMapField_6;
    private Map<List<Object[]>, List<Object[]>> testMapField_7;
    private Map<Map<?, ?>, Map<int[], ?>[][]> testMapField_8;
    private HashMap<Map<?, ?>, Map<int[], ?>[][]> testMapField_9;
    private LinkedHashMap<Map<?, ?>, Map<int[], ?>[][]> testMapField_10;
    private LinkedHashMap testMapField_11;
    private LinkedHashMap<T, T[]> testMapField_12;


    /**
     * Počet položek pro testování Listu.
     */
    static final int COUNT_OF_LIST_FIELDS = 17;
    private List testList_0;
    private List<?> testList_1;
    private List<String> testList_2;
    private List<String[]> testList_3;
    private List<Map<?, String>> testList_4;
    private List<ArrayList<LinkedList<String>>[]> testList_5;
    private ArrayList testList_6;
    private ArrayList<String> testList_7;
    private ArrayList<int[]> testList_8;
    private ArrayList<Map<Object, ?>> testList_9;
    private LinkedList<List> testList_10;
    private int testList_11;
    private String[] testList_12;
    private String testList_13;
    private Map<?, ?> testList_14;
    private ArrayList<T> testList_15;
    private ArrayList<T[]> testList_16;
    private ArrayList<LinkedList<T>> testList_17;

    static final int COUNT_OF_METHODS_RETURNS_LIST = 8;

    private List testListReturnTypeMethod_0() {return null;}

    private List<?> testListReturnTypeMethod_1() {return null;}

    private List<String> testListReturnTypeMethod_2() {return null;}

    private List<String[]> testListReturnTypeMethod_3() {return null;}

    private List<Map<?, ?>[]> testListReturnTypeMethod_4() {return null;}

    private List<ArrayList<Map<?, Double[]>>> testListReturnTypeMethod_5() {return null;}

    private Float testListReturnTypeMethod_6() {return null;}

    private Map testListReturnTypeMethod_7() {return null;}

    /**
     * Parametry, které obsahuje metoda testListMethod_1, aby bylo možné ji načíst pro testování.
     */
    static final Class<?>[] TEST_LIST_METHOD_1_PARAMETERS = {List.class, List.class, List.class, List.class,
            List.class, ArrayList.class, int.class, Map.class};

    private void testListMethod_1(List l, List<?> ll, List<String> la, List<List<Object>[]> lo, List<Map<?, ?>> lm,
                                  ArrayList<int[]> li, int i, Map m) {}


    /**
     * Počet proměnných pro testování pole.
     */
    static final int COUNT_OF_ARRAY_TEST_FIELDS = 14;

    private int testArray_0;
    private int[] testArray_1;
    private int[][] testArray_2;
    private String[][][] testArray_3;
    private List testArray_4;
    private List[][][] testArray_5;
    private ArrayList<String>[] testArray_6;
    private ArrayList<String>[][] testArray_7;
    private LinkedList<ArrayList<?>[]>[] testArray_8;
    private LinkedList<ArrayList<?>[]>[][][][][] testArray_9;
    private Map[] testArray_10;
    private Map[][] testArray_11;
    private Map<String[], ArrayList<?>[]>[][] testArray_12;
    private Map<String[][][], ArrayList<LinkedList[]>[]> testArray_13;
    private Map<String[][][], ArrayList<LinkedList[]>[]>[][][][] testArray_14;


    /**
     * Získání metody pro testování parametrů.
     *
     * @param methodName
     *         - název metody, která se má vrátit.
     * @param parameterTypes
     *         - datové typy parametrů metody, která se má vrátit.
     *
     * @return metodu, která se nachází v této třídě pro účely testování.
     */
    static Method getMethodForTest(final String methodName, final Class<?>... parameterTypes) {
        try {
            final Method method = FieldMethodConstant.class.getDeclaredMethod(methodName, parameterTypes);

            assertNotNull(method);
            method.setAccessible(true);

            return method;

        } catch (final NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Získání proměnné pro testování.
     *
     * @param fieldName
     *         - název proměnné, která se má načíst.
     *
     * @return načtenou proměnnou s názvem fieldName.
     */
    static Field getFieldForTest(final String fieldName) {
        try {
            final Field field = FieldMethodConstant.class.getDeclaredField(fieldName);

            assertNotNull(fieldName);
            field.setAccessible(true);

            return field;

        } catch (final NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }
}
