package cz.uhk.fim.fimj.reflection_support;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.*;
import java.util.*;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class ParameterToTextTest {

    private FieldMethodConstant fieldMethodConstant;

    @Before
    public void setUp() {
        fieldMethodConstant = new FieldMethodConstant();
    }


    @Test
    public void getDataTypeOfArrayInTextTest() {
        /*
         * Test pro převedení pole různého datového typu a počtu dimenzí do textové podoby.
         */
        try {
            final Method getDataTypeOfArrayInTextMethod = ParameterToText.class.getDeclaredMethod(
                    "getDataTypeOfArrayInText", Parameter.class);
            assertNotNull(getDataTypeOfArrayInTextMethod);
            getDataTypeOfArrayInTextMethod.setAccessible(true);


            final Method declaredMethodTest = FieldMethodConstant.getMethodForTest("testArray_1",
                    fieldMethodConstant.TEST_ARRAY_METHOD_1_PARAMETERS);
            assertNotNull(declaredMethodTest);

            final Parameter[] parameters = declaredMethodTest.getParameters();
            assertNotNull(parameters);
            assertEquals(parameters.length, fieldMethodConstant.TEST_ARRAY_METHOD_1_PARAMETERS.length);


            // Test výsledků:
            int parameterIndex = 0;

            for (final Parameter parameter : parameters) {
                // Parametr v textu:
                final Object gainedParameter = getDataTypeOfArrayInTextMethod.invoke(null, parameter);
                assertNotNull(gainedParameter);
                final String parameterInText = (String) gainedParameter;

                assertEquals(fieldMethodConstant.TEST_ARRAY_METHOD_1_PARAMETERS[parameterIndex].getSimpleName(),
                        parameterInText);
                System.out.println("Parameter: " + ++parameterIndex + " -> " + parameterInText);
            }

        } catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void getParameterInTextTest() {
        /*
         * Test pro převedení veškerých parametrů metody do textové podoby s a bez balíčků u datových typů parametrů.
         */
        try {
            // testová metoda pro získání parametrů:
            final Method declaredMethodTest = FieldMethodConstant.getMethodForTest("testMethod_1",
                    FieldMethodConstant.TEST_METHOD_1_PARAMETERS);
            assertNotNull(declaredMethodTest);

            final Parameter[] parameters = declaredMethodTest.getParameters();
            assertNotNull(parameters);
            assertEquals(parameters.length, FieldMethodConstant.TEST_METHOD_1_PARAMETERS.length);


            final Method parameterToTextWithPackagesMethod = ParameterToText.class.getDeclaredMethod(
                    "parametersToText", Parameter.class);
            assertNotNull(parameterToTextWithPackagesMethod);
            parameterToTextWithPackagesMethod.setAccessible(true);


            // Test výsledků:
            int parameterIndex = 0;
            System.out.println("With packages:");
            for (final Parameter parameter : parameters) {
                // Parametr v textu:
                final String parameterInText = ParameterToText.getParameterInText(parameter, true);
                assertNotNull(parameterInText);

                // Parametr, jak by měl vypadat:
                final Object parameterWithPackages = parameterToTextWithPackagesMethod.invoke(null, parameter);
                assertNotNull(parameterWithPackages);
                final String parameterInTextWithPackages = (String) parameterWithPackages;

                assertEquals(parameterInTextWithPackages, parameterInText);
                System.out.println("Parameter: " + ++parameterIndex + " -> " + parameterInText);
            }

            parameterIndex = 0;
            System.out.println("\n");
            System.out.println("Without packages:");
            for (final Parameter parameter : parameters) {
                // Parametr v textu:
                final String parameterInText = ParameterToText.getParameterInText(parameter, false);
                assertNotNull(parameterInText);

                // Je třeba kontrolovat výpis, nevím, jak otestovat výpsané parametry bez balíčků:
                System.out.println("Parameter: " + ++parameterIndex + " -> " + parameterInText);
            }

        } catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void getMethodReturnTypeInTextTest() {
        /*
         * Test pro převedení návratového datového typu metody do podoby textu.
         */

        // Testové metody:
        final List<Method> methodList = new ArrayList<>();

        // počet metod musí být o jednu větší (kvůli cyklu):
        IntStream.range(0, FieldMethodConstant.COUNT_OF_TEST_RETURN_TYPE_METHOD + 1).forEach(i -> {
            final Method method = FieldMethodConstant.getMethodForTest("testReturnTypeMethod_" + i);
            methodList.add(method);
        });


        // Získání návratového datového typu metody:
        methodList.forEach(method -> {
            final String gainedReturnDataTypeOfMethodPack = ParameterToText.getMethodReturnTypeInText(method, true);
            assertNotNull(gainedReturnDataTypeOfMethodPack);
            assertEquals(method.getGenericReturnType().getTypeName(), gainedReturnDataTypeOfMethodPack);
            System.out.println(method.getName() + " with packages -> " + gainedReturnDataTypeOfMethodPack);

            final String gainedReturnDataTypeOfMethod = ParameterToText.getMethodReturnTypeInText(method, false);
            assertNotNull(gainedReturnDataTypeOfMethod);
            System.out.println(method.getName() + " without packages -> " + gainedReturnDataTypeOfMethod);

            System.out.println("\n");
        });
    }


    @Test
    public void getFieldInTextTest() {
        /*
         * Test pro převedení datového typu proměnné do textové podoby s / bez balíčků u datových typů.
         */

        // Testové proměnné:
        final List<Field> fieldList = new ArrayList<>();

        IntStream.range(0, FieldMethodConstant.COUNT_OF_TEST_FIELDS + 1).forEach(i -> {
            final Field field = FieldMethodConstant.getFieldForTest("testField_" + i);
            fieldList.add(field);
        });


        fieldList.forEach(field -> {
            final String fieldInTextPac = ParameterToText.getFieldInText(field, true);
            assertNotNull(fieldInTextPac);
            assertEquals(field.getGenericType().getTypeName(), fieldInTextPac);
            System.out.println(field.getName() + " with packages -> " + fieldInTextPac);

            final String fieldInText = ParameterToText.getFieldInText(field, false);
            assertNotNull(fieldInText);
            System.out.println(field.getName() + " without packages -> " + fieldInText);

            System.out.println("\n");
        });
    }


    @Test
    public void getParametersInTextTest() {
        /*
         * Test pro převedení veškerých parametrů metody do textové podoby s a bez balíčků u datových typů parametrů.
         */

        // testová metoda pro získání parametrů:
        final Method declaredMethodTest = FieldMethodConstant.getMethodForTest("testMethod_1",
                FieldMethodConstant.TEST_METHOD_1_PARAMETERS);
        assertNotNull(declaredMethodTest);

        final Parameter[] parameters = declaredMethodTest.getParameters();
        assertNotNull(parameters);
        assertEquals(parameters.length, FieldMethodConstant.TEST_METHOD_1_PARAMETERS.length);


        final String parametersWithPackages = ParameterToText.getParametersInText(parameters, true);
        assertNotNull(parametersWithPackages);
        System.out.println("Parameters with packages:\n" + parametersWithPackages);

        final String parametersWithoutPackages = ParameterToText.getParametersInText(parameters, false);
        assertNotNull(parametersWithoutPackages);
        System.out.println("Parameters without packages:\n" + parametersWithoutPackages);
    }


    @Test
    public void getDataTypeInText_WithPackagesTest() {
        /*
         * Test pro převedení datového typu z typu Type do textové podoby s balíčky.
         */

        final List<Field> fieldList = new ArrayList<>();
        IntStream.range(0, FieldMethodConstant.COUNT_OF_LIST_FIELDS + 1).forEach(i -> fieldList.add(FieldMethodConstant.getFieldForTest("testList_" + i)));
        assertEquals(FieldMethodConstant.COUNT_OF_LIST_FIELDS + 1, fieldList.size());

        int index = 0;


        final Object typeOfList_0 = ListHelper.getTypeOfList(fieldList.get(index));
        assertNull(typeOfList_0);
        final String dataTypeInText_0 = ParameterToText.getDataTypeInText(typeOfList_0, true);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_0);
        assertNull(dataTypeInText_0);

        final Object typeOfList_1 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_1);
        assertTrue(typeOfList_1 instanceof WildcardType);
        final String dataTypeInText_1 = ParameterToText.getDataTypeInText(typeOfList_1, true);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_1);
        assertEquals("?", dataTypeInText_1);

        final Object typeOfList_2 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_2);
        assertTrue(typeOfList_2 instanceof Class<?>);
        assertEquals(typeOfList_2, String.class);
        final String dataTypeInText_2 = ParameterToText.getDataTypeInText(typeOfList_2, true);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_2);
        assertEquals(String.class.getTypeName(), dataTypeInText_2);
        assertEquals("java.lang.String", dataTypeInText_2);

        final Object typeOfList_3 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_3);
        assertTrue(typeOfList_3 instanceof Class<?>);
        final String dataTypeInText_3 = ParameterToText.getDataTypeInText(typeOfList_3, true);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_3);
        assertEquals(String[].class.getTypeName(), dataTypeInText_3);
        assertEquals("java.lang.String[]", dataTypeInText_3);

        final Object typeOfList_4 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_4);
        assertTrue(typeOfList_4 instanceof ParameterizedType);
        final String dataTypeInText_4 = ParameterToText.getDataTypeInText(typeOfList_4, true);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_4);
        assertEquals(((ParameterizedType) typeOfList_4).getTypeName(), dataTypeInText_4);
        assertEquals("java.util.Map<?, java.lang.String>", dataTypeInText_4);

        final Object typeOfList_5 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_5);
        assertTrue(typeOfList_5 instanceof GenericArrayType);
        final String dataTypeInText_5 = ParameterToText.getDataTypeInText(typeOfList_5, true);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_5);
        assertEquals(((GenericArrayType) typeOfList_5).getTypeName(), dataTypeInText_5);
        assertEquals("java.util.ArrayList<java.util.LinkedList<java.lang.String>>[]", dataTypeInText_5);

        final Object typeOfList_6 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNull(typeOfList_6);
        final String dataTypeInText_6 = ParameterToText.getDataTypeInText(typeOfList_6, true);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_6);
        assertNull(dataTypeInText_6);

        final Object typeOfList_7 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_7);
        assertTrue(typeOfList_7 instanceof Class<?>);
        assertEquals(typeOfList_7, String.class);
        final String dataTypeInText_7 = ParameterToText.getDataTypeInText(typeOfList_7, true);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_7);
        assertEquals(String.class.getTypeName(), dataTypeInText_7);
        assertEquals("java.lang.String", dataTypeInText_7);

        final Object typeOfList_8 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_8);
        assertTrue(typeOfList_8 instanceof Class<?>);
        final String dataTypeInText_8 = ParameterToText.getDataTypeInText(typeOfList_8, true);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_8);
        assertEquals(int[].class.getTypeName(), dataTypeInText_8);
        assertEquals("int[]", dataTypeInText_8);

        final Object typeOfList_9 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_9);
        assertTrue(typeOfList_9 instanceof ParameterizedType);
        final String dataTypeInText_9 = ParameterToText.getDataTypeInText(typeOfList_9, true);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_9);
        assertEquals(((ParameterizedType) typeOfList_9).getTypeName(), dataTypeInText_9);
        assertEquals("java.util.Map<java.lang.Object, ?>", dataTypeInText_9);

        final Object typeOfList_10 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_10);
        assertTrue(typeOfList_10 instanceof Class<?>);
        assertEquals(typeOfList_10, List.class);
        final String dataTypeInText_10 = ParameterToText.getDataTypeInText(typeOfList_10, true);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_10);
        assertEquals(List.class.getTypeName(), dataTypeInText_10);
        assertEquals("java.util.List", dataTypeInText_10);

        final Object typeOfList_11 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNull(typeOfList_11);
        final String dataTypeInText_11 = ParameterToText.getDataTypeInText(typeOfList_11, true);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_11);
        assertNull(dataTypeInText_11);

        final Object typeOfList_12 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNull(typeOfList_12);
        final String dataTypeInText_12 = ParameterToText.getDataTypeInText(typeOfList_12, true);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_12);
        assertNull(dataTypeInText_12);

        final Object typeOfList_13 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNull(typeOfList_13);
        final String dataTypeInText_13 = ParameterToText.getDataTypeInText(typeOfList_13, true);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_13);
        assertNull(dataTypeInText_13);

        final Object typeOfList_14 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNull(typeOfList_14);
        final String dataTypeInText_14 = ParameterToText.getDataTypeInText(typeOfList_14, true);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_14);
        assertNull(dataTypeInText_14);

        final Object typeOfList_15 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_15);
        assertTrue(typeOfList_15 instanceof TypeVariable<?>);
        final String dataTypeInText_15 = ParameterToText.getDataTypeInText(typeOfList_15, true);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_15);
        assertEquals("T", dataTypeInText_15);

        final Object typeOfList_16 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_16);
        final String dataTypeInText_16 = ParameterToText.getDataTypeInText(typeOfList_16, true);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_16);
        assertEquals("T[]", dataTypeInText_16);

        final Object typeOfList_17 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_17);
        final String dataTypeInText_17 = ParameterToText.getDataTypeInText(typeOfList_17, true);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_17);
        assertEquals("java.util.LinkedList<T>", dataTypeInText_17);

        assertEquals(FieldMethodConstant.COUNT_OF_LIST_FIELDS, index);
    }


    @Test
    public void getDataTypeInText_WithoutPackagesTest() {
        /*
         * Test pro převedení datového typu z typu Type do textové podoby bez balíčků.
         */

        final List<Field> fieldList = new ArrayList<>();
        IntStream.range(0, FieldMethodConstant.COUNT_OF_LIST_FIELDS + 1).forEach(i -> fieldList.add(FieldMethodConstant.getFieldForTest("testList_" + i)));
        assertEquals(FieldMethodConstant.COUNT_OF_LIST_FIELDS + 1, fieldList.size());

        int index = 0;

        final Object typeOfList_0 = ListHelper.getTypeOfList(fieldList.get(index));
        assertNull(typeOfList_0);
        final String dataTypeInText_0 = ParameterToText.getDataTypeInText(typeOfList_0, false);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_0);
        assertNull(dataTypeInText_0);

        final Object typeOfList_1 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_1);
        assertTrue(typeOfList_1 instanceof WildcardType);
        final String dataTypeInText_1 = ParameterToText.getDataTypeInText(typeOfList_1, false);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_1);
        assertEquals("?", dataTypeInText_1);

        final Object typeOfList_2 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_2);
        assertTrue(typeOfList_2 instanceof Class<?>);
        assertEquals(typeOfList_2, String.class);
        final String dataTypeInText_2 = ParameterToText.getDataTypeInText(typeOfList_2, false);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_2);
        assertEquals(String.class.getSimpleName(), dataTypeInText_2);
        assertEquals("String", dataTypeInText_2);

        final Object typeOfList_3 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_3);
        assertTrue(typeOfList_3 instanceof Class<?>);
        final String dataTypeInText_3 = ParameterToText.getDataTypeInText(typeOfList_3, false);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_3);
        // U Pole nebyl nalezen způsob pro jeho převedení do textu bez balíčků:
        assertEquals(String[].class.getTypeName(), dataTypeInText_3);
        assertEquals("java.lang.String[]", dataTypeInText_3);

        final Object typeOfList_4 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_4);
        assertTrue(typeOfList_4 instanceof ParameterizedType);
        final String dataTypeInText_4 = ParameterToText.getDataTypeInText(typeOfList_4, false);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_4);
        assertEquals("Map<?, String>", dataTypeInText_4);

        final Object typeOfList_5 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_5);
        assertTrue(typeOfList_5 instanceof GenericArrayType);
        final String dataTypeInText_5 = ParameterToText.getDataTypeInText(typeOfList_5, false);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_5);
        assertEquals("ArrayList<LinkedList<String>>[]", dataTypeInText_5);

        final Object typeOfList_6 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNull(typeOfList_6);
        final String dataTypeInText_6 = ParameterToText.getDataTypeInText(typeOfList_6, false);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_6);
        assertNull(dataTypeInText_6);

        final Object typeOfList_7 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_7);
        assertTrue(typeOfList_7 instanceof Class<?>);
        assertEquals(typeOfList_7, String.class);
        final String dataTypeInText_7 = ParameterToText.getDataTypeInText(typeOfList_7, false);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_7);
        assertEquals(String.class.getSimpleName(), dataTypeInText_7);
        assertEquals("String", dataTypeInText_7);

        final Object typeOfList_8 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_8);
        assertTrue(typeOfList_8 instanceof Class<?>);
        final String dataTypeInText_8 = ParameterToText.getDataTypeInText(typeOfList_8, false);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_8);
        assertEquals(int[].class.getSimpleName(), dataTypeInText_8);
        assertEquals("int[]", dataTypeInText_8);

        final Object typeOfList_9 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_9);
        assertTrue(typeOfList_9 instanceof ParameterizedType);
        final String dataTypeInText_9 = ParameterToText.getDataTypeInText(typeOfList_9, false);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_9);
        assertEquals("Map<Object, ?>", dataTypeInText_9);

        final Object typeOfList_10 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_10);
        assertTrue(typeOfList_10 instanceof Class<?>);
        assertEquals(typeOfList_10, List.class);
        final String dataTypeInText_10 = ParameterToText.getDataTypeInText(typeOfList_10, false);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_10);
        assertEquals(List.class.getSimpleName(), dataTypeInText_10);
        assertEquals("List", dataTypeInText_10);

        final Object typeOfList_11 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNull(typeOfList_11);
        final String dataTypeInText_11 = ParameterToText.getDataTypeInText(typeOfList_11, false);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_11);
        assertNull(dataTypeInText_11);

        final Object typeOfList_12 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNull(typeOfList_12);
        final String dataTypeInText_12 = ParameterToText.getDataTypeInText(typeOfList_12, false);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_12);
        assertNull(dataTypeInText_12);

        final Object typeOfList_13 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNull(typeOfList_13);
        final String dataTypeInText_13 = ParameterToText.getDataTypeInText(typeOfList_13, false);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_13);
        assertNull(dataTypeInText_13);

        final Object typeOfList_14 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNull(typeOfList_14);
        final String dataTypeInText_14 = ParameterToText.getDataTypeInText(typeOfList_14, false);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_14);
        assertNull(dataTypeInText_14);

        final Object typeOfList_15 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_15);
        assertTrue(typeOfList_15 instanceof TypeVariable<?>);
        final String dataTypeInText_15 = ParameterToText.getDataTypeInText(typeOfList_15, false);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_15);
        assertEquals("T", dataTypeInText_15);

        final Object typeOfList_16 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_16);
        final String dataTypeInText_16 = ParameterToText.getDataTypeInText(typeOfList_16, false);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_16);
        assertEquals("T[]", dataTypeInText_16);

        final Object typeOfList_17 = ListHelper.getTypeOfList(fieldList.get(++index));
        assertNotNull(typeOfList_17);
        final String dataTypeInText_17 = ParameterToText.getDataTypeInText(typeOfList_17, false);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + dataTypeInText_17);
        assertEquals("LinkedList<T>", dataTypeInText_17);

        assertEquals(FieldMethodConstant.COUNT_OF_LIST_FIELDS, index);
    }


    @Test
    public void getFirstArrayTypeOfFieldInTextTest() {
        /*
         * Test získání prvního typu / dimenze N - rozměrného pole.
         *
         * Například při proměnné typu "int[]" se vrátí "int", pří proměnné typu "int[][]" se vrátí "int[]" apod. pro
         * ostatní typy.
         */

        final List<Field> fieldList = new ArrayList<>();

        IntStream.range(0, FieldMethodConstant.COUNT_OF_ARRAY_TEST_FIELDS + 1).forEach(i -> {
            final Field fieldForTest = FieldMethodConstant.getFieldForTest("testArray_" + i);
            assertNotNull(fieldForTest);
            fieldList.add(fieldForTest);
        });

        final boolean withPackages = false;
        int index = 0;


        final String arrayInText_0 = ParameterToText.getFirstArrayTypeOfFieldInText(fieldList.get(index), withPackages);
        assertNull(arrayInText_0);

        final String arrayInText_1 = ParameterToText.getFirstArrayTypeOfFieldInText(fieldList.get(++index),
                withPackages);
        assertNotNull(arrayInText_1);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + arrayInText_1);
        assertEquals("int", arrayInText_1);

        final String arrayInText_2 = ParameterToText.getFirstArrayTypeOfFieldInText(fieldList.get(++index),
                withPackages);
        assertNotNull(arrayInText_2);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + arrayInText_2);
        assertEquals("int[]", arrayInText_2);

        final String arrayInText_3 = ParameterToText.getFirstArrayTypeOfFieldInText(fieldList.get(++index),
                withPackages);
        assertNotNull(arrayInText_3);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + arrayInText_3);
        assertEquals("java.lang.String[][]", arrayInText_3);

        final String arrayInText_4 = ParameterToText.getFirstArrayTypeOfFieldInText(fieldList.get(++index),
                withPackages);
        assertNull(arrayInText_4);

        final String arrayInText_5 = ParameterToText.getFirstArrayTypeOfFieldInText(fieldList.get(++index),
                withPackages);
        assertNotNull(arrayInText_5);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + arrayInText_5);
        assertEquals("java.util.List[][]", arrayInText_5);

        final String arrayInText_6 = ParameterToText.getFirstArrayTypeOfFieldInText(fieldList.get(++index),
                withPackages);
        assertNotNull(arrayInText_6);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + arrayInText_6);
        assertEquals("ArrayList<String>", arrayInText_6);

        final String arrayInText_7 = ParameterToText.getFirstArrayTypeOfFieldInText(fieldList.get(++index),
                withPackages);
        assertNotNull(arrayInText_7);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + arrayInText_7);
        assertEquals("ArrayList<String>[]", arrayInText_7);

        final String arrayInText_8 = ParameterToText.getFirstArrayTypeOfFieldInText(fieldList.get(++index),
                withPackages);
        assertNotNull(arrayInText_8);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + arrayInText_8);
        assertEquals("LinkedList<ArrayList<?>[]>", arrayInText_8);

        final String arrayInText_9 = ParameterToText.getFirstArrayTypeOfFieldInText(fieldList.get(++index),
                withPackages);
        assertNotNull(arrayInText_9);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + arrayInText_9);
        assertEquals("LinkedList<ArrayList<?>[]>[][][][]", arrayInText_9);

        final String arrayInText_10 = ParameterToText.getFirstArrayTypeOfFieldInText(fieldList.get(++index),
                withPackages);
        assertNotNull(arrayInText_10);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + arrayInText_10);
        assertEquals("java.util.Map", arrayInText_10);

        final String arrayInText_11 = ParameterToText.getFirstArrayTypeOfFieldInText(fieldList.get(++index),
                withPackages);
        assertNotNull(arrayInText_11);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + arrayInText_11);
        assertEquals("java.util.Map[]", arrayInText_11);

        final String arrayInText_12 = ParameterToText.getFirstArrayTypeOfFieldInText(fieldList.get(++index),
                withPackages);
        assertNotNull(arrayInText_12);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + arrayInText_12);
        assertEquals("Map<java.lang.String[], ArrayList<?>[]>[]", arrayInText_12);

        final String arrayInText_13 = ParameterToText.getFirstArrayTypeOfFieldInText(fieldList.get(++index),
                withPackages);
        assertNull(arrayInText_13);

        final String arrayInText_14 = ParameterToText.getFirstArrayTypeOfFieldInText(fieldList.get(++index),
                withPackages);
        assertNotNull(arrayInText_14);
        System.out.println("Field: " + fieldList.get(index) + ", text: " + arrayInText_14);
        assertEquals("Map<java.lang.String[][][], ArrayList<java.util.LinkedList[]>[]>[][][]", arrayInText_14);
    }
}