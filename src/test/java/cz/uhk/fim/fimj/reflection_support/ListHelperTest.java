package cz.uhk.fim.fimj.reflection_support;

import org.junit.Test;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class ListHelperTest {

    @Test
    public void getTypeOfListInParameterTest() {
        /*
         * Test pro získání datového typu Listu.
         */

        final List<Field> fieldList = new ArrayList<>();
        IntStream.range(0, FieldMethodConstant.COUNT_OF_LIST_FIELDS + 1).forEach(i -> fieldList.add(FieldMethodConstant.getFieldForTest("testList_" + i)));
        assertEquals(FieldMethodConstant.COUNT_OF_LIST_FIELDS + 1, fieldList.size());

        int index = 0;


        final Object typeOfList_0 = ListHelper.getTypeOfList(fieldList.get(index));
        System.out.println("Field: " + fieldList.get(index) + ", type: " + typeOfList_0);
        assertNull(typeOfList_0);

        final Object typeOfList_1 = ListHelper.getTypeOfList(fieldList.get(++index));
        System.out.println("Field: " + fieldList.get(index) + ", type: " + typeOfList_1);
        assertNotNull(typeOfList_1);
        assertTrue(typeOfList_1 instanceof WildcardType);
        assertEquals("?", typeOfList_1.toString());

        final Object typeOfList_2 = ListHelper.getTypeOfList(fieldList.get(++index));
        System.out.println("Field: " + fieldList.get(index) + ", type: " + typeOfList_2);
        assertNotNull(typeOfList_2);
        assertTrue(typeOfList_2 instanceof Class<?>);
        assertEquals(String.class, typeOfList_2);

        final Object typeOfList_3 = ListHelper.getTypeOfList(fieldList.get(++index));
        System.out.println("Field: " + fieldList.get(index) + ", type: " + typeOfList_3);
        assertNotNull(typeOfList_3);
        assertTrue(typeOfList_3 instanceof Class<?>);
        assertEquals(String[].class, typeOfList_3);

        final Object typeOfList_4 = ListHelper.getTypeOfList(fieldList.get(++index));
        System.out.println("Field: " + fieldList.get(index) + ", type: " + typeOfList_4);
        assertNotNull(typeOfList_4);
        assertTrue(typeOfList_4 instanceof ParameterizedType);

        final Object typeOfList_5 = ListHelper.getTypeOfList(fieldList.get(++index));
        System.out.println("Field: " + fieldList.get(index) + ", type: " + typeOfList_5);
        assertNotNull(typeOfList_5);
        assertTrue(typeOfList_5 instanceof GenericArrayType);

        final Object typeOfList_6 = ListHelper.getTypeOfList(fieldList.get(++index));
        System.out.println("Field: " + fieldList.get(index) + ", type: " + typeOfList_6);
        assertNull(typeOfList_6);

        final Object typeOfList_7 = ListHelper.getTypeOfList(fieldList.get(++index));
        System.out.println("Field: " + fieldList.get(index) + ", type: " + typeOfList_7);
        assertNotNull(typeOfList_7);
        assertTrue(typeOfList_7 instanceof Class<?>);
        assertEquals(String.class, typeOfList_7);

        final Object typeOfList_8 = ListHelper.getTypeOfList(fieldList.get(++index));
        System.out.println("Field: " + fieldList.get(index) + ", type: " + typeOfList_8);
        assertNotNull(typeOfList_8);
        assertTrue(typeOfList_8 instanceof Class);
        assertEquals(int[].class, typeOfList_8);

        final Object typeOfList_9 = ListHelper.getTypeOfList(fieldList.get(++index));
        System.out.println("Field: " + fieldList.get(index) + ", type: " + typeOfList_9);
        assertNotNull(typeOfList_9);
        assertTrue(typeOfList_9 instanceof ParameterizedType);

        final Object typeOfList_10 = ListHelper.getTypeOfList(fieldList.get(++index));
        System.out.println("Field: " + fieldList.get(index) + ", type: " + typeOfList_10);
        assertNotNull(typeOfList_10);
        assertTrue(typeOfList_10 instanceof Class<?>);
        assertEquals(List.class, typeOfList_10);

        final Object typeOfList_11 = ListHelper.getTypeOfList(fieldList.get(++index));
        System.out.println("Field: " + fieldList.get(index) + ", type: " + typeOfList_11);
        assertNull(typeOfList_11);

        final Object typeOfList_12 = ListHelper.getTypeOfList(fieldList.get(++index));
        System.out.println("Field: " + fieldList.get(index) + ", type: " + typeOfList_12);
        assertNull(typeOfList_12);

        final Object typeOfList_13 = ListHelper.getTypeOfList(fieldList.get(++index));
        System.out.println("Field: " + fieldList.get(index) + ", type: " + typeOfList_13);
        assertNull(typeOfList_13);

        final Object typeOfList_14 = ListHelper.getTypeOfList(fieldList.get(++index));
        System.out.println("Field: " + fieldList.get(index) + ", type: " + typeOfList_14);
        assertNull(typeOfList_14);

        final Object typeOfList_15 = ListHelper.getTypeOfList(fieldList.get(++index));
        System.out.println("Field: " + fieldList.get(index) + ", type: " + typeOfList_15);
        assertNotNull(typeOfList_15);
        assertTrue(typeOfList_15 instanceof TypeVariable<?>);
        assertEquals("T", typeOfList_15.toString());

        final Object typeOfList_16 = ListHelper.getTypeOfList(fieldList.get(++index));
        System.out.println("Field: " + fieldList.get(index) + ", type: " + typeOfList_16);
        assertNotNull(typeOfList_16);
        assertTrue(typeOfList_16 instanceof GenericArrayType);
        assertEquals("T[]", typeOfList_16.toString());

        final Object typeOfList_17 = ListHelper.getTypeOfList(fieldList.get(++index));
        System.out.println("Field: " + fieldList.get(index) + ", type: " + typeOfList_17);
        assertNotNull(typeOfList_17);
        assertTrue(typeOfList_17 instanceof ParameterizedType);
        assertEquals("java.util.LinkedList<T>", typeOfList_17.toString());

        assertEquals(FieldMethodConstant.COUNT_OF_LIST_FIELDS, index);
    }


    @Test
    public void getTypeOfListInParametersOfMethodTest() {
        /*
         * Test pro získání datového typu listu z parametrů metody.
         */

        final Method methodForTest = FieldMethodConstant.getMethodForTest("testListMethod_1",
                FieldMethodConstant.TEST_LIST_METHOD_1_PARAMETERS);
        assertNotNull(methodForTest);

        final Parameter[] parameters = methodForTest.getParameters();
        assertEquals(FieldMethodConstant.TEST_LIST_METHOD_1_PARAMETERS.length, parameters.length);

        int index = 0;


        final Object typeOfListInParameter_0 = ListHelper.getTypeOfList(parameters[index]);
        System.out.println("Parameter: " + parameters[index] + ", type: " + typeOfListInParameter_0);
        assertNull(typeOfListInParameter_0);

        final Object typeOfListInParameter_1 = ListHelper.getTypeOfList(parameters[++index]);
        System.out.println("Parameter: " + parameters[index] + ", type: " + typeOfListInParameter_1);
        assertNotNull(typeOfListInParameter_1);
        assertTrue(typeOfListInParameter_1 instanceof WildcardType);

        final Object typeOfListInParameter_2 = ListHelper.getTypeOfList(parameters[++index]);
        System.out.println("Parameter: " + parameters[index] + ", type: " + typeOfListInParameter_2);
        assertNotNull(typeOfListInParameter_2);
        assertTrue(typeOfListInParameter_2 instanceof Class<?>);

        final Object typeOfListInParameter_3 = ListHelper.getTypeOfList(parameters[++index]);
        System.out.println("Parameter: " + parameters[index] + ", type: " + typeOfListInParameter_3);
        assertNotNull(typeOfListInParameter_3);
        assertTrue(typeOfListInParameter_3 instanceof GenericArrayType);

        final Object typeOfListInParameter_4 = ListHelper.getTypeOfList(parameters[++index]);
        System.out.println("Parameter: " + parameters[index] + ", type: " + typeOfListInParameter_4);
        assertNotNull(typeOfListInParameter_4);
        assertTrue(typeOfListInParameter_4 instanceof ParameterizedType);

        final Object typeOfListInParameter_5 = ListHelper.getTypeOfList(parameters[++index]);
        System.out.println("Parameter: " + parameters[index] + ", type: " + typeOfListInParameter_5);
        assertNotNull(typeOfListInParameter_5);
        assertTrue(typeOfListInParameter_5 instanceof Class<?>);

        final Object typeOfListInParameter_6 = ListHelper.getTypeOfList(parameters[++index]);
        System.out.println("Parameter: " + parameters[index] + ", type: " + typeOfListInParameter_6);
        assertNull(typeOfListInParameter_6);

        final Object typeOfListInParameter_7 = ListHelper.getTypeOfList(parameters[++index]);
        System.out.println("Parameter: " + parameters[index] + ", type: " + typeOfListInParameter_7);
        assertNull(typeOfListInParameter_7);

        assertEquals(FieldMethodConstant.TEST_LIST_METHOD_1_PARAMETERS.length - 1, index);
    }


    @Test
    public void getTypeOfListReturnTypeMethodTest() {
        /*
         * Test pro získání datového typu Listu z návratového datového typu metody.
         */

        final List<Method> methodList = new ArrayList<>();
        IntStream.range(0, FieldMethodConstant.COUNT_OF_METHODS_RETURNS_LIST).forEach(i -> methodList.add(FieldMethodConstant.getMethodForTest("testListReturnTypeMethod_" + i)));
        assertEquals(FieldMethodConstant.COUNT_OF_METHODS_RETURNS_LIST, methodList.size());

        int index = 0;


        final Object typeOfList_0 = ListHelper.getTypeOfList(methodList.get(index));
        System.out.println("Return type: " + methodList.get(index).getReturnType() + ", type: " + typeOfList_0);
        assertNull(typeOfList_0);

        final Object typeOfList_1 = ListHelper.getTypeOfList(methodList.get(++index));
        System.out.println("Return type: " + methodList.get(index).getReturnType() + ", type: " + typeOfList_1);
        assertNotNull(typeOfList_1);
        assertTrue(typeOfList_1 instanceof WildcardType);

        final Object typeOfList_2 = ListHelper.getTypeOfList(methodList.get(++index));
        System.out.println("Return type: " + methodList.get(index).getReturnType() + ", type: " + typeOfList_2);
        assertNotNull(typeOfList_2);
        assertTrue(typeOfList_2 instanceof Class<?>);
        assertEquals(String.class, typeOfList_2);

        final Object typeOfList_3 = ListHelper.getTypeOfList(methodList.get(++index));
        System.out.println("Return type: " + methodList.get(index).getReturnType() + ", type: " + typeOfList_3);
        assertNotNull(typeOfList_3);
        assertTrue(typeOfList_3 instanceof Class<?>);
        assertEquals(String[].class, typeOfList_3);

        final Object typeOfList_4 = ListHelper.getTypeOfList(methodList.get(++index));
        System.out.println("Return type: " + methodList.get(index).getReturnType() + ", type: " + typeOfList_4);
        assertNotNull(typeOfList_4);
        assertTrue(typeOfList_4 instanceof GenericArrayType);

        final Object typeOfList_5 = ListHelper.getTypeOfList(methodList.get(++index));
        System.out.println("Return type: " + methodList.get(index).getReturnType() + ", type: " + typeOfList_5);
        assertNotNull(typeOfList_5);
        assertTrue(typeOfList_5 instanceof ParameterizedType);

        final Object typeOfList_6 = ListHelper.getTypeOfList(methodList.get(++index));
        System.out.println("Return type: " + methodList.get(index).getReturnType() + ", type: " + typeOfList_6);
        assertNull(typeOfList_6);

        final Object typeOfList_7 = ListHelper.getTypeOfList(methodList.get(++index));
        System.out.println("Return type: " + methodList.get(index).getReturnType() + ", type: " + typeOfList_7);
        assertNull(typeOfList_7);

        assertEquals(FieldMethodConstant.COUNT_OF_METHODS_RETURNS_LIST - 1, index);
    }
}