package cz.uhk.fim.fimj.file;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.*;

public class WriteToFileTest {

    /**
     * Získání cesty k adresáři: target/test-classes
     */
    private File pathToTestClasses;

    @Before
    public void setUp() {
        pathToTestClasses = new File(WriteToFileTest.class.getResource("/").getPath());
    }


    @Test
    public void createConfigurationDirWithConfigurationFilesTest() {
        /*
         * Na cestě: file:/C:/Users/krunc/IdeaProjects/Fim-J/target/test-classes/configDirName se vytvoří adresář
         * configuration s příslušnými soubory.
         */

        final String configDirName = "configDirName";

        final File desPath = new File(pathToTestClasses + File.separator + configDirName);

        WriteToFile.createConfigureFilesInPath(desPath.getAbsolutePath());
    }


    @Test
    public void createConfigurationDirTest() {
        /*
         * Test na vytvoření adresáře - pouze vytvoření adreáře na zadaném umístění.
         */

        //        final String dirName = "testDirName";
        final String dirName = "testDirName" + File.separator + "anotherDir";

        final File desPath = new File(pathToTestClasses + File.separator + dirName);

        try {
            final Method createDirectories = WriteToFile.class.getDeclaredMethod("createDirectories", File.class);
            createDirectories.setAccessible(true);

            createDirectories.invoke(null, desPath);

        } catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void hideFileTest() {
        /*
         * Test pro skrytí souboru na zadaném umístění.
         *
         * Jedná se pouze o nastavení atributu "skrytý" pro OS Windows.
         */

        final File testFile = new File(pathToTestClasses + File.separator + "hiddenFile.txt");

        try {
            if (!testFile.exists() || !testFile.isFile()) {
                if (!testFile.createNewFile())
                    return;
            }

            final Method hideFile = WriteToFile.class.getDeclaredMethod("hideFile", File.class);
            assertNotNull(hideFile);

            hideFile.setAccessible(true);

            hideFile.invoke(null, testFile);

        } catch (final IOException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void hideDirTest() {
        /*
         * Test pro skrytí adresáře na zadaném umístění.
         *
         * Jedná se pouze o nastavení atributu "skrytý" pro OS Windows.
         */

        final File testFile = new File(pathToTestClasses + File.separator + "hiddenDir");

        try {
            if (!testFile.exists() || !testFile.isDirectory()) {
                if (!testFile.mkdirs())
                    return;
            }

            final Method hideFile = WriteToFile.class.getDeclaredMethod("hideFile", File.class);
            assertNotNull(hideFile);

            hideFile.setAccessible(true);

            hideFile.invoke(null, testFile);

        } catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void writeLanguagesTest() {
        /*
         * Test pro vytvoření adresáře "language" i s texty (soubory .properties) pro aplikaci.
         */

        WriteToFile.writeLanguages(pathToTestClasses);
    }
}