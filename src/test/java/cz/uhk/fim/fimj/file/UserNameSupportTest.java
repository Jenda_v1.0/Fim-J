package cz.uhk.fim.fimj.file;

import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Properties;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class UserNameSupportTest {

    @Test
    public void getDefaultUserNameInOSTest() {
        /*
         * Jedná se pouze o získání uživatelského jména, které je nastavené v OS - příhlášený uživatel.
         *
         * Účel testu, je zjišťění / otestování, zda se správně získalo uživatelské jméno příhlášeného uživatele.
         */
        try {
            final Field userName = UserNameSupport.class.getDeclaredField("USER_NAME_IN_OS");
            assertNotNull(userName);
            userName.setAccessible(true);

            final Object objUserNameValue = userName.get(null);

            assertNotNull(objUserNameValue);

            System.out.println(objUserNameValue);

        } catch (final NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void checkValidUserNameTest() {
        /*
         * Test validních uživatelských jmen.
         *
         * Musí začínat písmenem a může obsahovat pouze velká a malá písmena bez / s diakritikou, číslice, podtržítka
         * a tabulátory.
         */

        assertTrue(UserNameSupport.isUserNameValid(" Jan "));
        assertTrue(UserNameSupport.isUserNameValid(" Jan Krunčík "));
        assertTrue(UserNameSupport.isUserNameValid(" Jan - Krunčík "));
        assertTrue(UserNameSupport.isUserNameValid(" Jan _  Krunčík v 1_0 "));
        assertTrue(UserNameSupport.isUserNameValid(" d 123456789   123 456 789  "));
        assertTrue(UserNameSupport.isUserNameValid(" š 1 2 3 4 5 6 7 8 9"));
        assertTrue(UserNameSupport.isUserNameValid(" é í á ý ž ř č š ě ó ň ť "));
        assertTrue(UserNameSupport.isUserNameValid(" É Í Á Ý Ž Ř Č Š Ě Ó Ň Ť "));
    }


    @Test
    public void checkInvalidUserNameTest() {
        /*
         * Test nevalidních uživatelských jmen.
         */
        assertFalse(UserNameSupport.isUserNameValid("           "));
        assertFalse(UserNameSupport.isUserNameValid(" q    \t "));
        assertFalse(UserNameSupport.isUserNameValid(" q    \n "));
        assertFalse(UserNameSupport.isUserNameValid(" q    \r "));
        assertFalse(UserNameSupport.isUserNameValid("č"));
        assertFalse(UserNameSupport.isUserNameValid("gf"));
        assertFalse(UserNameSupport.isUserNameValid(" "));

        // Nepovolené znaky:
        assertFalse(UserNameSupport.isUserNameValid(" d . ¨´=; "));
        assertFalse(UserNameSupport.isUserNameValid(" d . = *-* /+  "));
        assertFalse(UserNameSupport.isUserNameValid(" q ( ) {} [] -.,\" ! "));
        assertFalse(UserNameSupport.isUserNameValid(" name ( ) {} [] -.,\" ! "));


        // Maximální velikost textu:
        final StringBuilder stringBuilder = new StringBuilder();
        IntStream.range(0, 256).forEach(i -> stringBuilder.append("a"));

        assertFalse(UserNameSupport.isUserNameValid(stringBuilder.toString()));
    }


    @Test
    public void saveUserNameToConfigFileTest() {
        /*
         * Test uložení nového uživatelského jména do konfiguračního souboru ve workspace.
         */
        final String userName = "TestUserName";

        final boolean saved = UserNameSupport.saveUserNameToConfigFile(userName);
        assertTrue(saved);


        // Načtení ze souboru - zda se podařilo uložení:
        final Properties defaultPropertiesInWorkspace = ReadFile.getDefaultPropertiesInWorkspace();
        assertNotNull(defaultPropertiesInWorkspace);

        final String loadedUserName = defaultPropertiesInWorkspace.getProperty("UserName", null);
        assertNotNull(loadedUserName);
        assertEquals(userName, loadedUserName);
    }


    @Test
    public void loadAndSetValidUserNameFromDefaultPropTest() {
        // Uložení názvu do konfiguračního souboru:
        final String userName = "TestUserName";

        final boolean saved = UserNameSupport.saveUserNameToConfigFile(userName);
        assertTrue(saved);


        // Test, zda se název načte do příslušné proměnné:
        UserNameSupport.loadAndSetUserNameFromDefaultProp();
        assertEquals(UserNameSupport.getUsedUserName(), userName);
    }


    @Test
    public void loadAndSetInvalidUserNameFromDefaultPropTest() {
        // Uložení názvu do konfiguračního souboru:
        final String userName = "Invalid name () !";

        final boolean saved = UserNameSupport.saveUserNameToConfigFile(userName);
        assertTrue(saved);


        // Získání jména z proměnné, které by se nemělo změnit:
        final String userNameSet = UserNameSupport.getUsedUserName();


        // Test, zda se název načte do příslušné proměnné:
        UserNameSupport.loadAndSetUserNameFromDefaultProp();
        assertNotEquals(UserNameSupport.getUsedUserName(), userName);

        assertEquals(userNameSet, UserNameSupport.getUsedUserName());
    }
}