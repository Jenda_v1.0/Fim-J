package cz.uhk.fim.fimj.app;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.ReadFile;
import org.junit.Test;

import javax.swing.*;
import java.util.Arrays;
import java.util.Properties;

import static org.junit.Assert.*;

public class LookAndFeelSupportTest {

    @Test
    public void setLookAndFeel_DebuggingNull() {
        /*
         * Je třeba krokovat postup, zda se načte výchozí hodnota z Constants.
         */
        LookAndFeelSupport.setLookAndFeel(null);
    }


    @Test
    public void setLookAndFeel_DebuggingWithPropSetValue() {
        /*
         * Je třeba krokovat postup, zda se načte hodnota z properties
         */
        final Properties defaultProp = ReadFile.getDefaultPropAnyway();
        defaultProp.setProperty("LookAndFeel", "javax.swing.plaf.metal.MetalLookAndFeel");

        LookAndFeelSupport.setLookAndFeel(defaultProp);
    }


    @Test
    public void setLookAndFeel_DebuggingWithPropRemovedValue() {
        /*
         * Je třeba krokovat postup, zda se načte výchozí hodnota z Constants místo z properties, která byla odebrána.
         */
        final Properties defaultProp = ReadFile.getDefaultPropAnyway();
        defaultProp.remove("LookAndFeel");

        LookAndFeelSupport.setLookAndFeel(defaultProp);
    }


    @Test
    public void setLookAndFeel_DebuggingWithoutLookAndFeel() {
        /*
         * Je třeba krokovat postup, zda se načte hodnota z properties a aplikuje se výchozí styl (Look and Feel).
         */
        final Properties defaultProp = ReadFile.getDefaultPropAnyway();
        defaultProp.setProperty("LookAndFeel", Constants.WITHOUT_SPECIFIC_LOOK_AND_FEEL);

        LookAndFeelSupport.setLookAndFeel(defaultProp);
    }


    @Test
    public void setLookAndFeel_DebuggingWithUnknownValue() {
        /*
         * Je třeba krokovat postup, zda se načte hodnota z properties, ale nenajde se v existujících / instalovaných
         * Look and Feel pro využívanou platformu, proto se neaplikuje žádný styl.
         */
        final Properties defaultProp = ReadFile.getDefaultPropAnyway();
        defaultProp.setProperty("LookAndFeel", "Unknown");

        LookAndFeelSupport.setLookAndFeel(defaultProp);
    }


    @Test
    public void getInstalledLookAndFeels() {
        /*
         * Test pro výpis instalovaných Look and Feels (pro kontrolu syntaxe a zjišťování hodnot pro testování apod.).
         */
        final UIManager.LookAndFeelInfo[] installedLookAndFeels = LookAndFeelSupport.getInstalledLookAndFeels();

        Arrays.stream(installedLookAndFeels).forEach(lookAndFeelInfo -> {
            System.out.println(lookAndFeelInfo.getClassName());
            System.out.println(lookAndFeelInfo.getName());
            System.out.println("\n");
        });
    }


    @Test
    public void checkExistLookAndFeel() {
        /*
         * Zjištění, zda se vrátí, že neexistuje příslušná hodnota.
         */
        final boolean b = LookAndFeelSupport.checkExistLookAndFeel("javax.swing.plaf.metal.Unknown");

        assertFalse(b);
    }


    @Test
    public void checkExistingLookAndFeels() {
        /*
         * Pro veškeré instalované Look and Feels pro konkrétní využívanou platformu se zjistí, zda se pro všechny
         * vrátí, že existují.
         */
        final UIManager.LookAndFeelInfo[] installedLookAndFeels = LookAndFeelSupport.getInstalledLookAndFeels();

        Arrays.stream(installedLookAndFeels).forEach(lookAndFeelInfo -> assertTrue(LookAndFeelSupport.checkExistLookAndFeel(lookAndFeelInfo.getClassName())));
    }
}