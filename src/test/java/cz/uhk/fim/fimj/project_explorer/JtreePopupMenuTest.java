package cz.uhk.fim.fimj.project_explorer;

import cz.uhk.fim.fimj.file.Constants;
import cz.uhk.fim.fimj.file.WriteToFileTest;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.*;

public class JtreePopupMenuTest {

    /**
     * Získání cesty k adresáři: target/test-classes
     */
    private File pathToTestClasses;

    private static final String PACKAGES = "pack1" + File.separator + "pack2";


    @Before
    public void setUp() {
        pathToTestClasses = new File(WriteToFileTest.class.getResource("/").getPath());
    }


    @Test
    public void getPathToSrcDirFromSrcTest() {
        /*
         * Test získání adresáře "src" z cesty k souboru, který se nachází právě v tomto adresáři "src".
         */
        final File pathToSrc = new File(pathToTestClasses + File.separator + "src");
        final File pathToFile = new File(pathToSrc + File.separator + "JavaClassName.java");

        // Vytvoření adresářů:
        final File parentDir = pathToFile.getParentFile();
        if (!parentDir.exists() || !parentDir.exists())
            assertTrue(parentDir.mkdirs());


        if (pathToFile.exists())
            assertTrue(pathToFile.delete());


        try {
            assertTrue(pathToFile.createNewFile());
            assertTrue(pathToFile.exists());
            assertTrue(pathToFile.isFile());

            final Method getPackagesMethod = JtreePopupMenu.class.getDeclaredMethod("getPathToSrcDir", File.class);
            assertNotNull(getPackagesMethod);
            getPackagesMethod.setAccessible(true);

            final Object gainedObj = getPackagesMethod.invoke(null, pathToFile);
            assertNotNull(gainedObj);

            assertTrue(gainedObj instanceof File);
            final File gainedSrcDir = (File) gainedObj;

            assertEquals(pathToSrc, gainedSrcDir);

        } catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException | IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void getPathToSrcDirTest() {
        /*
         * Test získání adresáře "src" z cesty k souboru.
         */
        final File pathToSrc = new File(pathToTestClasses + File.separator + "src");
        final File pathToFile = new File(pathToSrc + File.separator + PACKAGES + File.separator + "JavaClassName.java");

        // Vytvoření adresářů:
        final File parentDir = pathToFile.getParentFile();
        if (!parentDir.exists() || !parentDir.exists())
            assertTrue(parentDir.mkdirs());


        if (pathToFile.exists())
            assertTrue(pathToFile.delete());


        try {
            assertTrue(pathToFile.createNewFile());
            assertTrue(pathToFile.exists());
            assertTrue(pathToFile.isFile());

            final Method getPackagesMethod = JtreePopupMenu.class.getDeclaredMethod("getPathToSrcDir", File.class);
            assertNotNull(getPackagesMethod);
            getPackagesMethod.setAccessible(true);

            final Object gainedObj = getPackagesMethod.invoke(null, pathToFile);
            assertNotNull(gainedObj);

            assertTrue(gainedObj instanceof File);
            final File gainedSrcDir = (File) gainedObj;

            assertEquals(pathToSrc, gainedSrcDir);

        } catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException | IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void getPathToSrcDir_ResultIsNullTest() {
        /*
         * Test získání adresáře "src" z cesty k souboru.
         *
         * Ale v tomto případě by se neměl získat adresář "src", metoda níže by měla vrátit null.
         *
         * Jde o to, že metoda níže hledá na cestě v parametru adresář src, pokud ho nenajde, tak vrátí null -> toto
         * je účel tohoto testu.
         */
        final File pathToSrc = new File(pathToTestClasses + File.separator + "nonSrcDir");
        final File pathToFile = new File(pathToSrc + File.separator + PACKAGES + File.separator + "JavaClassName.java");

        // Vytvoření adresářů:
        final File parentDir = pathToFile.getParentFile();
        if (!parentDir.exists() || !parentDir.exists())
            assertTrue(parentDir.mkdirs());


        if (pathToFile.exists())
            assertTrue(pathToFile.delete());


        try {
            assertTrue(pathToFile.createNewFile());
            assertTrue(pathToFile.exists());
            assertTrue(pathToFile.isFile());

            final Method getPackagesMethod = JtreePopupMenu.class.getDeclaredMethod("getPathToSrcDir", File.class);
            assertNotNull(getPackagesMethod);
            getPackagesMethod.setAccessible(true);

            final Object gainedObj = getPackagesMethod.invoke(null, pathToFile);
            assertNull(gainedObj);

        } catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException | IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void getSrcPackageFromPathToFileTest() {
        /*
         * Test na získání balíčků třídy (od adresáře "src" - bez něj).
         *
         * Jedná se o to, že se metodě getPackages předá jako parametru cesta k třídě (obecně souboru) a měla by se
         * vrátit balíčky, což jsou soubory na cestě od adresáře src k zadanému souboru.
         */
        final File pathToFile =
                new File(pathToTestClasses + File.separator + "src" + File.separator + PACKAGES + File.separator +
                        "JavaClassName.java");

        // Vytvoření adresářů:
        final File parentDir = pathToFile.getParentFile();
        if (!parentDir.exists() || !parentDir.exists())
            assertTrue(parentDir.mkdirs());


        if (pathToFile.exists())
            assertTrue(pathToFile.delete());


        try {
            assertTrue(pathToFile.createNewFile());
            assertTrue(pathToFile.exists());
            assertTrue(pathToFile.isFile());

            final Method getPackagesMethod = JtreePopupMenu.class.getDeclaredMethod("getPackages", File.class);
            assertNotNull(getPackagesMethod);
            getPackagesMethod.setAccessible(true);

            final Object gainedObj = getPackagesMethod.invoke(null, pathToFile);
            assertNotNull(gainedObj);

            final String gainedPackages = (String) gainedObj;

            final String packagesWithDots = PACKAGES.replace(File.separator, ".");
            final String gainedPackagesWithDots = gainedPackages.replace(File.separator, ".");
            assertEquals(packagesWithDots, gainedPackagesWithDots);

        } catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException | IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void getDefaultPackageFromPathToFileTest() {
        /*
         * Test na získání výchozího balíčku (defaultPackage).
         *
         * Jedná se o to, že se metodě getPackages předá jako parametru cesta k třídě (obecně souboru). Metoda by
         * měla vrátit balíčky od adresáře "src", ale tento test ja zaměřen na to, že když se na cestě nenajde
         * adresář "src", tak se vrátí výchozí balíček.
         */
        final File pathToFile =
                new File(pathToTestClasses + File.separator + "nonSrcDir" + File.separator + PACKAGES + File.separator + "JavaClassName.java");

        // Vytvoření adresářů:
        final File parentDir = pathToFile.getParentFile();
        if (!parentDir.exists() || !parentDir.exists())
            assertTrue(parentDir.mkdirs());

        if (pathToFile.exists())
            assertTrue(pathToFile.delete());


        try {
            assertTrue(pathToFile.createNewFile());
            assertTrue(pathToFile.exists());
            assertTrue(pathToFile.isFile());

            final Method getPackagesMethod = JtreePopupMenu.class.getDeclaredMethod("getPackages", File.class);
            assertNotNull(getPackagesMethod);
            getPackagesMethod.setAccessible(true);

            final Object gainedObj = getPackagesMethod.invoke(null, pathToFile);
            assertNotNull(gainedObj);

            final String gainedPackages = (String) gainedObj;
            assertEquals(Constants.DEFAULT_PACKAGE, gainedPackages);

        } catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException | IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void getDefaultPackageFromPathToFileInSrcDirTest() {
        /*
         * Test na získání výchozího balíčku (defaultPackage).
         *
         * Jedná se o to, že se metodě getPackages předá jako parametru cesta k třídě (obecně souboru). Metoda by
         * měla vrátit balíčky od adresáře "src", ale tento test ja zaměřen na to, aby se vrátil výchozí balíček i
         * když se ten soubor nachází přáve v adresáři "src". Tedy žádné adresáře / balíčky by se nevrátily, tak se
         * vrátí výchozí balíček.
         */
        final File pathToFile = new File(pathToTestClasses + File.separator + "src" + File.separator + "JavaClassName" +
                ".java");

        // Vytvoření adresářů:
        final File parentDir = pathToFile.getParentFile();
        if (!parentDir.exists() || !parentDir.exists())
            assertTrue(parentDir.mkdirs());

        if (pathToFile.exists())
            assertTrue(pathToFile.delete());


        try {
            assertTrue(pathToFile.createNewFile());
            assertTrue(pathToFile.exists());
            assertTrue(pathToFile.isFile());

            final Method getPackagesMethod = JtreePopupMenu.class.getDeclaredMethod("getPackages", File.class);
            assertNotNull(getPackagesMethod);
            getPackagesMethod.setAccessible(true);

            final Object gainedObj = getPackagesMethod.invoke(null, pathToFile);
            assertNotNull(gainedObj);

            final String gainedPackages = (String) gainedObj;
            assertEquals(Constants.DEFAULT_PACKAGE, gainedPackages);

        } catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException | IOException e) {
            e.printStackTrace();
        }
    }
}