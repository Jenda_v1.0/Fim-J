# Author: Jan Krunčík



# Introduction

# Only the branches that are known to be valid are merged into the master. When merging a branch into a master, the runner will run, ending with the first error found.

# In the other branches a runner will run, which will not end when a production code error occurs. Additionally, test errors and errors when generating an executable file (s) will be ignored. If at least one of these phases ends with an error, this will be verified with the maven profile "verify".

# This is because when an error occurs when compiling the production code, it would be good to see if there is any error in the test or in the production code. Similarly for the other phases. If the first error occurred, for example, when compiling the production code, it would not be ascertained whether the tests are correct or contain an error.



# Stages:

# (The syntax: "profileName_master" means that the appropriate job is performed only if it is an action above the master branch.)

# clean_up - Delete the target directory.
# validate - Validate the project is correct and all necessary information is available.
# compile, compile_master - Compile the source code of the project. For a master branch it must pass, for others not.
# clean_up_compile - Delete the target directory. It is not necessary at this stage.
# test, test_master - Test the compiled source code using a unit testing. For a master branch it must pass, for others not.
# clean_up_test - Delete the target directory. It is not necessary at this stage.
# package, package_master - Take the compiled code and package it in its distributable format, such as a JAR. For a master branch it must pass, for others not.
# verify - Run any checks on results of integration tests to ensure quality criteria are met. (It is not necessary to guard. Will pass if the package profile passes.)
# clean_up_after - Delete the target directory. At this stage, it is useful to free memory.



stages:
  - clean_up
  - validate
  - compile
  - compile_master
  # Not necessary:
  - clean_up_compile
  - test
  - test_master
  # Not necessary:
  - clean_up_test
  - package
  - package_master
  - verify
  - clean_up_after



# Fetch is faster as it re-uses the project workspace (falling back to clone if it doesn't exist).
variables:
  GIT_STRATEGY: fetch
  # Number of attempts to fetch sources running a job.
  GET_SOURCES_ATTEMPTS: 3



# Files and directories in "resources" directory will be cached.
cache:
  paths:
    - src/main/resources



clean_up_job:
  stage: clean_up
  # Restrict the job to runners with a certain tag.
  tags:
    - maven
  # Here we can execute arbitrate terminal commands.
  script:
    - echo "clean up"
    - mvn clean



validate_job:
  stage: validate
  tags:
    - maven
  script:
    - echo "validate"
    - mvn validate



compile_job:
  stage: compile
  # The job runs only if validate_job (/ previous job) succeeds.
  when: on_success
  # if this job fails, it will not stop the next stage from running
  allow_failure: true
  # Defines the names of branch for which the job will not run.
  except:
    - master
  tags:
    - maven
  script:
    - echo "compile"
    - mvn compile



compile_master_job:
  stage: compile_master
  when: on_success
  # Defines the names of branch for which the job will run.
  only:
    - master
  tags:
    - maven
  script:
    - echo "compile"
    - mvn compile



clean_up_compile_job:
  stage: clean_up_compile
  when: on_success
  tags:
    - maven
  script:
    - echo "clean up after compile"
    - mvn clean



test_job:
  stage: test
  allow_failure: true
  except:
    - master
  tags:
    - maven
  script:
    - echo "tests"
    - mvn test



test_master_job:
  stage: test_master
  only:
    - master
  tags:
    - maven
  script:
    - echo "tests"
    - mvn test



clean_up_test_job:
  stage: clean_up_test
  when: on_success
  tags:
    - maven
  script:
    - echo "clean up after tests"
    - mvn clean



package_job:
  stage: package
  allow_failure: true
  except:
    - master
  tags:
    - maven
  script:
    - echo "create executable file(s)"
    - mvn package



package_master_job:
  stage: package_master
  only:
    - master
  tags:
    - maven
  script:
    - echo "create executable file(s)"
    - mvn package



verify_job:
  stage: verify
  when: on_success
  tags:
    - maven
  script:
    - echo "verify"
    - mvn verify



clean_up_after_job:
  stage: clean_up_after
  # Always execute clean_up_after_job as the last step in pipeline regardless of success or failure.
  when: always
  tags:
    - maven
  script:
    - echo "clean up"
    - mvn clean