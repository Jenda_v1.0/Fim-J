# Základní informace #

V níže popsaných adresářích se nachází vygenerovaná aplikace a skripty pro její spuštění (napsané pro OS Windows a Linux).



## exe ##

Adresář obsahuje soubory:

* **Fim-J.exe**

    * Spustitelný soubor (vygenerovaná aplikace).


* **RunApp.bat**

    * Dávkový soubor pro spuštění souborů s příponou ".exe" na OS Windows.



## jar ##

Adresář obsahuje soubory:

* **Fim-J.jar**

    * Spustitelný soubor (vygenerovaná aplikace).


* **RunApp.bat**
 
     * Dávkový soubor pro spuštění souborů s příponou ".jar" na OS Windows.


* **RunApp.sh**

    * Shell skript pro spuštění souborů s příponou ".jar" na OS Linux.

        * *Skript je vhodné spustit v terminálu.*



# Skripty ##

*(Do skriptů zadáváme pouze číselné hodnoty / možnosti.)*


Po spuštění jednoho z niže popsaných skriptů pro spuštění aplikace (dle OS) bude prohledán adresář (včetně podadresářů), ve kterém byl skript spuštěn. V každém adresáři budou vyhledány soubory, které v názvu obsahují text "Fim-J" a mají příponu ".jar" nebo ".exe".


* Skript "RunApp.bat" v adresáři "exe" bude vyhledávat soubory s příponou ".exe".

    * Pro každý nalezený soubor bude nabídnuta možnost pro jeho spuštění (možnost "1"). Možnost "2" pro vyhledání dalšího souboru splňující výše uvedená kritéria a poslední možnost (ostatní čísla) slouží pro ukončení skriptu.


* Skripty "RunApp.bat" a "RunApp.sh" v adresáři "jar" budou vyhledávat soubory s příponou ".jar".

    * Pro každý nalezený soubor bude nabídnuta možnost pro jeho spuštění (možnost "1").
    
        * V dalším kroku bude na výběr, zda chceme spustit aplikaci:
        
            * s nastavením minimální a maximální velikostí paměti pro JVM (možnost 1).
            
                * Následují dva kroky (druhý je volitelný), ve kterých nastavíme velikost paměti. *(Jsou k dispozici možnosti v intervalu 250 MB - 2 GB.)*
                
            * s výchozí nastavenou velikostí paměti pro JVM (možnost 2 - doporučeno).
        
            * bez možnosti nastavení paměti. Aplikace bude spuštěna bez nastavení parametrů.

    * Možnost "2" pro vyhledání dalšího souboru splňující výše uvedená kritéria.
    
    * Ostatní čísla pro ukončení skriptu.





# Basic information #

In the folders described below, the generated application and scripts for running it (written for Windows and Linux) are located.



## exe ##

The directory contains files:

* **Fim-J.exe**

    * Executable file (generated application).


* **RunApp.bat**

    * Batch file to run files with the extension '.exe' on Windows.



## jar ##

The directory contains files:

* **Fim-J.jar**

    * Executable file (generated application).


* **RunApp.bat**
 
     * Batch file to run files with the extension '.jar' on Windows.


* **RunApp.sh**

    * Shell script to run files with '.jar' extension on Linux.

        * *The script should be run in the terminal.*



# Scripts ##

*(We only enter numeric values ​​/ options into scripts.)*


After running one of the scripts described below for running the application (according to OS), the directory (including the subdirectories) in which the script was launched will be searched. In each directory, files that contain the text 'Fim-J' in the name and have the extension '.jar' or '.exe' will be searched for.


* The 'RunApp.bat' script in the 'exe' directory will search for files with the '.exe' extension.

    * For each file found, the option to run it (option '1') will be offered. The option '2' to find the next file that meets the criteria above and the last option (other numbers) is used to end the script.


* The 'RunApp.bat' and 'RunApp.sh' scripts in the 'jar' directory will search for files with the '.jar' extension.

    * For each file found, the option to run it (option '1') will be offered.
    
        * The next step is to choose whether we want to run the application:
        
            * with the minimum and maximum memory settings for JVM (option '1').
            
                * Here are two steps (second is optional) in which we set the amount of memory. *(Options are available from 250 MB to 2 GB.)*
                
            * with the default memory size setting for JVM (option '2' - recommended).
        
            * without memory option. The application will run without parameter setting.

    * Option '2' to find another file that meets the criteria above.
    
    * Other numbers to end the script.