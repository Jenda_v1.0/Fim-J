#!/usr/bin/env bash

# Author: Jan Krunčík

# Search for files with the '.jar' extension containing the 'Fim-J' text in the file name. Files will be searched in the directory and all its subdirectories where the file was run.





# Basic information
echo "Files with the name contains 'Fim-J' and extension '.jar' will be searched for in the current directory and all subdirectories."
echo "Current directory: $PWD"





# Files with spaces in the file name will be accepted:
OIFS="$IFS"
IFS=$'\n'

# Get all the files and directories that are in the directory where the script was started:
files=($(find -type f -name "*Fim-J*.jar"))





# Default values ​​to run the application ('.jar' file):
initialHeapSize=500
maxHeapSize=1500





# Run an application with default or set values:
function runApp() {
if [ -f "${pathToJar}" ] ; then
    echo
    java -Xms${initialHeapSize}M -Xmx${maxHeapSize}M -jar ${pathToJar}
else
    echo
    echo "File "${pathToJar}" not found."
    echo
    echo "Press the Enter key to exit."
    echo
    read
fi
}





function runAppWithoutParameters() {
if [ -f "${pathToJar}" ] ; then
    echo
    java -jar ${pathToJar}
else
    echo
    echo "File "${pathToJar}" not found."
    echo
    echo "Press the Enter key to exit."
    echo
    read
fi
}





# Run an application with the default Java heap size set:
function runAppWithInitialHeapSize() {
if [ -f "${pathToJar}" ] ; then
    echo
    java -Xms${initialHeapSize}M -jar ${pathToJar}
else
    echo
    echo "File "${pathToJar}" not found."
    echo
    echo "Press the Enter key to exit."
    echo
    read
fi
}





function checkHeapSizeValues() {
if [ ${initialHeapSize} -gt ${maxHeapSize} ] ; then
    echo
    echo "Initial heap size is greater then maximum heap size, set sizes again?"
    echo
    echo "1 - Set heap sizes again."
    echo "- Otherwise end."
    echo

    echo -n "Choice: "
    read choice

    if [ "$choice" -eq 1 ] ; then
        specifyInitialHeapSize
    else
        echo
        exit 0
    fi
fi

runApp
}





function specifyMaxHeapSize() {
echo
echo "Specify the maximum Java heap size (Xmx):"
echo
echo "0 - Run application only with set initial Java heap size in the previous step (${initialHeapSize} MB)."
echo "1 - 250 MB"
echo "2 - 500 MB"
echo "3 - 750 MB"
echo "4 - 1 GB"
echo "5 - 1250 MB"
echo "6 - 1500 MB"
echo "7 - 1750 MB"
echo "8 - 2 GB"
echo "- Otherwise end."
echo

echo -n "Choice: "
read choice

if [ "$choice" -eq 0 ] ; then
    runAppWithInitialHeapSize
fi

if [ "$choice" -eq 1 ] ; then
    maxHeapSize=250
    checkHeapSizeValues
fi

if [ "$choice" -eq 2 ] ; then
    maxHeapSize=500
    checkHeapSizeValues
fi

if [ "$choice" -eq 3 ] ; then
    maxHeapSize=750
    checkHeapSizeValues
fi

if [ "$choice" -eq 4 ] ; then
    maxHeapSize=1000
    checkHeapSizeValues
fi

if [ "$choice" -eq 5 ] ; then
    maxHeapSize=1250
    checkHeapSizeValues
fi

if [ "$choice" -eq 6 ] ; then
    maxHeapSize=1500
    checkHeapSizeValues
fi

if [ "$choice" -eq 7 ] ; then
    maxHeapSize=1750
    checkHeapSizeValues
fi

if [ "$choice" -eq 8 ] ; then
    maxHeapSize=2000
    checkHeapSizeValues
else
    echo
    exit 0
fi
}





function specifyInitialHeapSize() {
echo
echo "Specify the initial Java heap size (Xms):"
echo
echo "1 - 250 MB"
echo "2 - 500 MB"
echo "3 - 750 MB"
echo "4 - 1 GB"
echo "5 - 1250 MB"
echo "6 - 1500 MB"
echo "7 - 1750 MB"
echo "8 - 2 GB"
echo "- Otherwise end."
echo

echo -n "Choice: "
read choice

if [ "$choice" -eq 1 ] ; then
    initialHeapSize=250
    specifyMaxHeapSize
fi

if [ "$choice" -eq 2 ] ; then
    initialHeapSize=500
    specifyMaxHeapSize
fi

if [ "$choice" -eq 3 ] ; then
    initialHeapSize=750
    specifyMaxHeapSize
fi

if [ "$choice" -eq 4 ] ; then
    initialHeapSize=1000
    specifyMaxHeapSize
fi

if [ "$choice" -eq 5 ] ; then
    initialHeapSize=1250
    specifyMaxHeapSize
fi

if [ "$choice" -eq 6 ] ; then
    initialHeapSize=1500
    specifyMaxHeapSize
fi

if [ "$choice" -eq 7 ] ; then
    initialHeapSize=1750
    specifyMaxHeapSize
fi

if [ "$choice" -eq 8 ] ; then
    initialHeapSize=2000
    specifyMaxHeapSize
else
    echo
    exit 0
fi
}





function startApp() {
echo
echo "1 - Specify the minimum and maximum memory allocation pool for a Java virtual machine (JVM)."
echo "2 - Run the application with the default values (Xms = $initialHeapSize MB, Xmx = $maxHeapSize MB)."
echo "3 - Run an application without setting parameters."
echo - Otherwise end
echo

echo -n "Choice: "
read choice

if [ "$choice" -eq 1 ] ; then
    specifyInitialHeapSize
fi

if [ "$choice" -eq 2 ] ; then
    runApp
fi

if [ "$choice" -eq 3 ] ; then
    runAppWithoutParameters
else
    echo
    exit 0
fi
}





function ask() {
echo
echo "1 - Starts the application."
echo "2 - Search for another '*Fim-J*.jar' file."
echo "- Otherwise end."
echo

echo -n "Choice: "
read choice

if [ "$choice" -eq 1 ] ; then
    startApp
fi

if [ "$choice" -ne 2 ] ; then
    echo
    exit 0
fi
}





# Initiation:
# Checking if any required files were found.
if [ ${#files[@]} -ne 0 ]; then
    for item in ${files[*]}
    do
        pathToJar=${item}
        echo
        echo "Run the app: ${pathToJar} ?"
        ask
    done

else
    echo
    echo "No files with the name 'Fim-J' and '.jar' extension found."
    echo
fi