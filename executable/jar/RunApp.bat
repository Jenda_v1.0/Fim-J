:: Author: Jan Krunčík

:: Search for files with the '.jar' extension containing the 'Fim-J' text in the file name. Files will be searched in the directory and all its subdirectories where the file was run.



@echo off

:: Saving to local variables:
setlocal enabledelayedexpansion enableextensions





:: Basic information
echo Files with the name contains 'Fim-J' and extension '.jar' will be searched for in the current directory and all subdirectories.
echo Current directory: %cd%
echo.





:: Clear the variables / set default values:
set index=-1
set foundFiles=
set found=

:: Default values ​​to run the application ('.jar' file):
set initialHeapSize=500
set maxHeapSize=1500





:: Initiation:
:search
:: Loop to search the current directory (and all its files and directories):
for /R %%G in (*Fim-J*.jar) do (
    if exist "%%G" (
        set found=false

        :: To determine if a file has already been offered to run is available only if it is not already in the array ('foundFiles') and array is not empty.
        if index gtr 0 (
            for /l %%j in (0, 1, !index!) do (
                :: Determining whether the currently-iterated '.jar' file has not been offered to run (not previously found):
                if "!foundFiles[%%j]!" == "%%G" (
                    set found=true
                )
            )
        )

        if "!found!" == "false" (
            :: Add a newly found '.jar' file to the array 'foundFiles' so it will not be offered anymore:
            set /A index += 1
            set foundFiles[!index!]=%%G

            :: Set the file path to be displayed / offered to run:
            set pathToJar="%%G"
            goto ask
        )
    )
)





:ask
:: Determining whether at least one file with the '.jar' extension was found:
if !index! == -1 (
    goto fileNotFound
)

echo.
set /A count=!index!+1
echo Files found with the name contains 'Fim-J' and extension '.jar' so far (!count!):
for /L %%f in (0, 1, !index!) do (
    echo !foundFiles[%%f]!
)
echo.

echo Run the app: %pathToJar% ?
echo.

echo 1 - Starts the application.
echo 2 - Search for another '*Fim-J*.jar' file.
echo - Otherwise end.
echo.

set /P choice="Choice: "

:: Test the answer.
if %choice% == 1 (
    goto startApp
)

if %choice% == 2 (
    goto search
)

goto end





:fileNotFound
echo No files with the name containing the 'Fim-J' text and '.jar' extension found.
echo.
echo 1 (or otherwise) - End.
echo.

set /P choice="Choice: "
goto end





:startApp
echo.
echo 1 - Specify the minimum and maximum memory allocation pool for a Java virtual machine (JVM).
echo 2 - Run the application with the default values (Xms = !initialHeapSize! MB, Xmx = !maxHeapSize! MB).
echo 3 - Run an application without setting parameters.
echo - Otherwise end.
echo.

set /P choice="Choice: "

:: Test the answer.
if %choice% == 1 (
    goto specifyInitialHeapSize
)

if %choice% == 2 (
    goto runApp
)

if %choice% == 3 (
    goto runAppWithoutParameters
)

goto end





:specifyInitialHeapSize
echo.
echo Specify the initial Java heap size (Xms):
echo.
echo 1 - 250 MB
echo 2 - 500 MB
echo 3 - 750 MB
echo 4 - 1 GB
echo 5 - 1250 MB
echo 6 - 1500 MB
echo 7 - 1750 MB
echo 8 - 2 GB
echo - Otherwise end.
echo.

set /P choice="Choice: "

if %choice% == 1 (
    set initialHeapSize=250
    goto specifyMaxHeapSize
)

if %choice% == 2 (
    set initialHeapSize=500
    goto specifyMaxHeapSize
)

if %choice% == 3 (
    set initialHeapSize=750
    goto specifyMaxHeapSize
)

if %choice% == 4 (
    set initialHeapSize=1000
    goto specifyMaxHeapSize
)

if %choice% == 5 (
    set initialHeapSize=1250
    goto specifyMaxHeapSize
)

if %choice% == 6 (
    set initialHeapSize=1500
    goto specifyMaxHeapSize
)

if %choice% == 7 (
    set initialHeapSize=1750
    goto specifyMaxHeapSize
)

if %choice% == 8 (
    set initialHeapSize=2000
    goto specifyMaxHeapSize
)

goto end





:specifyMaxHeapSize
echo.
echo Specify the maximum Java heap size (Xmx):
echo.
echo 0 - Run application only with set initial Java heap size in the previous step (!initialHeapSize! MB).
echo 1 - 250 MB
echo 2 - 500 MB
echo 3 - 750 MB
echo 4 - 1 GB
echo 5 - 1250 MB
echo 6 - 1500 MB
echo 7 - 1750 MB
echo 8 - 2 GB
echo - Otherwise end.
echo.

set /P choice="Choice: "

if %choice% == 0 (
    goto runAppWithInitialHeapSize
)

if %choice% == 1 (
    set maxHeapSize=250
    goto checkHeapSizeValues
)

if %choice% == 2 (
    set maxHeapSize=500
    goto checkHeapSizeValues
)

if %choice% == 3 (
    set maxHeapSize=750
    goto checkHeapSizeValues
)

if %choice% == 4 (
    set maxHeapSize=1000
    goto checkHeapSizeValues
)

if %choice% == 5 (
    set maxHeapSize=1250
    goto checkHeapSizeValues
)

if %choice% == 6 (
    set maxHeapSize=1500
    goto checkHeapSizeValues
)

if %choice% == 7 (
    set maxHeapSize=1750
    goto checkHeapSizeValues
)

if %choice% == 8 (
    set maxHeapSize=2000
    goto checkHeapSizeValues
)

goto end





:checkHeapSizeValues
if !initialHeapSize! gtr !maxHeapSize! (
echo.
echo Initial heap size is greater then maximum heap size, set sizes again?
echo.
echo 1 - Set heap sizes again.
echo - Otherwise end.
echo.

set /P choice="Choice: "

if !choice! == 1 (
    goto specifyInitialHeapSize
)

goto end
)

goto runApp





:: Run an application with default or set values:
:runApp
if exist "!pathToJar!" (
    echo.
    java -Xms!initialHeapSize!M -Xmx!maxHeapSize!M -jar %pathToJar%
    goto end
) else (
    echo.
    echo "File %pathToJar% not found."
    echo.
    pause
    goto end
)





:runAppWithoutParameters
if exist "!pathToJar!" (
    echo.
    java -jar %pathToJar%
    goto end
) else (
    echo.
    echo "File %pathToJar% not found."
    echo.
    pause
    goto end
)





::Run an application with the default Java heap size set:
:runAppWithInitialHeapSize
if exist "!pathToJar!" (
    echo.
    java -Xms!initialHeapSize!M -jar %pathToJar%
    goto end
) else (
    echo.
    echo "File %pathToJar% not found."
    echo.
    pause
    goto end
)





:end
echo.