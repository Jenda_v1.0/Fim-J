:: Author: Jan Krunčík

:: Search for files with the '.exe' extension containing the 'Fim-J' text in the file name. Files will be searched in the directory and all its subdirectories where the file was run.



@echo off

:: Saving to local variables:
setlocal enabledelayedexpansion enableextensions





:: Basic information:
echo Files with the name contains 'Fim-J' and extension '.exe' will be searched for in the current directory and all subdirectories.
echo Current directory: %cd%
echo.





:: Clear the variables / set default values:
set index=-1
set foundFiles=
set found=





:: Initiation:
:search
:: Loop to search the current directory (and all its files and directories):
for /R %%G in (*Fim-J*.exe) do (
    if exist "%%G" (
        set found=false

        :: Determine if a file has already been offered to run is available only if it is not already in the array ('foundFiles') and array is not empty.
        if index gtr 0 (
            for /l %%j in (0, 1, !index!) do (
                :: Determining whether the currently-iterated '.exe' file has not been offered to run (not previously found):
                if "!foundFiles[%%j]!" == "%%G" (
                    set found=true
                )
            )
        )

        if "!found!" == "false" (
            :: Add a newly found '.exe' file to the array 'foundFiles' so it will not be offered anymore:
            set /A index += 1
            set foundFiles[!index!]=%%G

            :: Set the file path to be displayed / offered to run:
            set pathToExe="%%G"
            goto ask
        )
    )
)





:ask
:: Determining whether at least one file with the .exe extension was found:
if !index! == -1 (
    goto fileNotFound
)

echo.
set /A count=!index!+1
echo Files found with the name contains 'Fim-J' and extension '.exe' so far (!count!):
for /L %%f in (0, 1, !index!) do (
    echo !foundFiles[%%f]!
)
echo.

echo Run the app: %pathToExe% ?
echo.

echo 1 - Starts the application.
echo 2 - Search for another '*Fim-J*.exe' file.
echo - Otherwise end.
echo.

set /P choice="Choice: "

:: Test the answer.
if %choice% == 1 (
    goto runApp
)

if %choice% == 2 (
    goto search
)

goto end





:fileNotFound
echo No files with the name containing the 'Fim-J' text and '.exe' extension found.
echo.
echo 1 (or otherwise) - End.
echo.

set /P choice="Choice: "
goto end





:runApp
if exist "!pathToExe!" (
    echo.
    %pathToExe%
    goto end
) else (
    echo.
    echo "File %pathToExe% not found."
    echo.
    pause
    goto end
)





:end
echo.